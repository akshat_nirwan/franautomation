package com.builds.test.fs;

import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminAreaRegionAddAreaRegionPageTest;
import com.builds.test.admin.AdminConfigurationConfigureCallStatusPageTest;
import com.builds.test.admin.AdminConfigurationConfigureCommunicationTypePageTest;
import com.builds.test.admin.AdminConfigurationConfigureStoreTypePageTest;
import com.builds.test.admin.AdminDivisionAddDivisionPageTest;
import com.builds.test.admin.AdminFranchiseLocationAddFranchiseLocationPageTest;
import com.builds.test.admin.AdminPageTest;
import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.admin.AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest;
import com.builds.test.admin.AdminUsersManageManageFranchiseUsersPageTest;
import com.builds.test.admin.AdminUsersManageRegionalUsersAddRegionalUserPageTest;
import com.builds.test.common.CorporateUser;
import com.builds.test.infomgr.FranchiseesCommonMethods;
import com.builds.test.opener.OpenerStoreSummaryStoreListPageTest;
import com.builds.test.salesTest.PrimaryInfoUI;
import com.builds.test.salesTest.Sales_Common_New;
import com.builds.uimaps.admin.AdminFDDManagementConfigureEmailsentPriortoFDDExpirationPage;
import com.builds.uimaps.admin.AdminFDDManagementFDDEmailTemplateSummaryAddTemplatePage;
import com.builds.uimaps.admin.AdminFDDManagementFDDManagementPage;
import com.builds.uimaps.admin.AdminFDDManagementITEM23RECEIPTSummaryAddITEM23RECEIPTPage;
import com.builds.uimaps.admin.AdminFDDManagementITEM23RECEIPTSummaryPage;
import com.builds.uimaps.admin.AdminFDDManagementLogonCredentialsDurationPage;
import com.builds.uimaps.admin.AdminFDDmanagementConfigureEmailsentPriortoFDDEmailPage;
import com.builds.uimaps.admin.AdminFranchiseLocationAddFranchiseLocationPage;
import com.builds.uimaps.admin.AdminFranchiseLocationManageFranchiseLocationsAddUserPage;
import com.builds.uimaps.admin.AdminUsersManageCorporateUsersAddCorporateUserPage;
import com.builds.uimaps.campaigncentercrm.CRMCampaignCenterCampaignsPage;
import com.builds.uimaps.crm.LeadsummarypageUI;
import com.builds.uimaps.fs.AdminFranchiseSalesBrokersAgencySummaryAddBrokersAgencyPage;
import com.builds.uimaps.fs.AdminFranchiseSalesBrokersAgencySummaryPage;
import com.builds.uimaps.fs.AdminFranchiseSalesBrokersTypeConfigurationPage;
import com.builds.uimaps.fs.AdminFranchiseSalesManageWebFormGeneratorPage;
import com.builds.uimaps.fs.AdminSalesConfigureEmailContentPage;
import com.builds.uimaps.fs.FSBrokersPage;
import com.builds.uimaps.fs.FSCampaignCenterPage;
import com.builds.uimaps.fs.FSGroupsPage;
import com.builds.uimaps.fs.FSHomePage;
import com.builds.uimaps.fs.FSLeadSummaryAddLeadPage;
import com.builds.uimaps.fs.FSLeadSummaryCompliancePage;
import com.builds.uimaps.fs.FSLeadSummaryMergeLeadsPage;
import com.builds.uimaps.fs.FSLeadSummaryPersonalProfilePage;
import com.builds.uimaps.fs.FSLeadSummaryPrimaryInfoPage;
import com.builds.uimaps.fs.FSLeadSummaryQualificationDetailsPage;
import com.builds.uimaps.fs.FSLeadSummaryRealEstatePage;
import com.builds.uimaps.fs.FSLeadSummarySendEmailPage;
import com.builds.uimaps.fs.FSLeadSummaryVisitPage;
import com.builds.uimaps.fs.FSSitesPage;
import com.builds.uimaps.fs.FsExportPage;
import com.builds.uimaps.fs.LeadManagementUI;
import com.builds.uimaps.fs.LeadSummaryUI;
import com.builds.uimaps.fs.SearchUI;
import com.builds.uimaps.infomgr.InfoMgrCenterInfoPage;
import com.builds.uimaps.infomgr.InfoMgrFranchiseesPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

import io.restassured.RestAssured;

public class FSLeadSummaryPageTest {

	FranconnectUtil fc = new FranconnectUtil();

	// Verified

	@Test(groups = { "sales" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FS_LeadManagement_ChangeStatus_001", testCaseDescription = "Verify Change Status of of Lead Through Batch")
	void changeStatusActionMenu_WithoutRemarks() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String firstName = dataSet.get("FirstName");
		String lastName = dataSet.get("LastName");

		try {
			driver = fc.loginpage().login(driver);
			
			fc.utobj().printTestStep("Add Lead Status");
			AdminFranchiseSalesLeadStatusPageTest p1 = new AdminFranchiseSalesLeadStatusPageTest();
			String leadStatus = p1.addNewLeadStatus(driver);

			String randomChar = fc.utobj().generateRandomChar();

			String lastNameUnique = lastName + randomChar;
			String leadNameSearch = lastNameUnique;

			fc.sales().sales_common().fsModule(driver);
			String leadName = firstName + " " + lastNameUnique;
			fc.utobj().printTestStep("Add a lead");
			addLeadLeadSummaryWithLeadName(driver, config, firstName, lastNameUnique);
			fc.utobj().printTestStep("Add another lead");
			addLeadLeadSummaryWithLeadName(driver, config, firstName, lastNameUnique);

			FSSearchPageTest p3 = new FSSearchPageTest();
			p3.searchByLeadName(driver, firstName, leadNameSearch);

			fc.utobj().printTestStep("Change Status through batch");
			LeadSummaryUI pobj = new LeadSummaryUI(driver);
			fc.utobj().clickElement(driver, pobj.checkBoxAll);
			fc.utobj().selectActionMenuItems(driver, "Change Status");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().selectDropDownByVisibleText(driver, pobj.leadStatusIDDrp, leadStatus);
			fc.utobj().clickElement(driver, pobj.changeStatusCboxBtn);

			fc.utobj().clickElement(driver, pobj.cboxCloseBtn);
			driver.switchTo().defaultContent();

			p3.searchByLeadName(driver, firstName, lastNameUnique);
			fc.utobj().clickLinkIgnoreCase(driver, leadName);

			FSLeadSummaryPrimaryInfoPageTest pi = new FSLeadSummaryPrimaryInfoPageTest();
			FSLeadSummaryPrimaryInfoPage pobj2 = new FSLeadSummaryPrimaryInfoPage(driver);
			String option = fc.utobj().getSelectedOptioninDropDown(driver, pobj2.leadStatusDrp);
			if (!option.equalsIgnoreCase(leadStatus)) {
				fc.utobj().throwsException("Lead Status remarks did not get created.");
			}

			boolean isStatusChanged = pi.verifySubjectCommentActivityHistory(driver,
					"Lead Status has been changed to " + leadStatus);

			if (isStatusChanged == false) {
				fc.utobj().throwsException("Lead Status remarks did not get created.");
			}
			pi.clickNext(driver);
			option = fc.utobj().getSelectedOptioninDropDown(driver, pobj2.leadStatusDrp);
			if (!option.equalsIgnoreCase(leadStatus)) {
				fc.utobj().throwsException("Lead Status did not get change.");
			}
			isStatusChanged = pi.verifySubjectCommentActivityHistory(driver,
					"Lead Status has been changed to " + leadStatus);
			if (isStatusChanged == false) {
				fc.utobj().throwsException("Lead Status did not get change.");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales12345" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FS_LeadManagement_ChangeOwner_001", testCaseDescription = "Verify Change Owner of Lead Through Batch")
	void changeOwnerActionMenu() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		String firstName = dataSet.get("FirstName");
		String lastName = dataSet.get("LastName");

		try {
			driver = fc.loginpage().login(driver);

			AdminUsersManageCorporateUsersAddCorporateUserPageTest p4 = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			fc.utobj().printTestStep("Add Corporate User");
			/*
			 * Map<String,String> userInfo =
			 * p4.addCorporateUserWithBasicInfo(driver, config);
			 */
			String emailId = "salesautomation@staffex.com";

			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			p4.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			LeadSummaryUI pobj = new LeadSummaryUI(driver);

			String randomChar = fc.utobj().generateRandomChar();

			String lastNameUnique = lastName + randomChar;
			String leadNameSearch = lastNameUnique;

			String leadName = firstName + " " + lastNameUnique;

			fc.utobj().printTestStep("Add two leads in lead management");

			fc.sales().sales_common().fsModule(driver);
			addLeadLeadSummaryWithLeadName(driver, config, firstName, lastNameUnique);
			addLeadLeadSummaryWithLeadName(driver, config, firstName, lastNameUnique);

			FSSearchPageTest p3 = new FSSearchPageTest();
			p3.searchByLeadName(driver, firstName, leadNameSearch);

			pobj = new LeadSummaryUI(driver);
			fc.utobj().clickElement(driver, pobj.checkBoxAll);

			fc.utobj().printTestStep("Change lead owner");
			fc.utobj().selectActionMenuItems(driver, "Change Owner");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			String userName = corpUser.getuserFullName();
			fc.utobj().selectDropDownByVisibleText(driver, pobj.leadOwnerDrp, userName);
			fc.utobj().clickElement(driver, pobj.changeOwnerBtn);
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().clickElement(driver, pobj.cboxCloseBtn);

			p3.searchByLeadName(driver, firstName, lastNameUnique);
			fc.utobj().clickLinkIgnoreCase(driver, leadName);

			FSLeadSummaryPrimaryInfoPageTest pi = new FSLeadSummaryPrimaryInfoPageTest();
			String changedOwnerName = pi.getOwnerName(driver);

			if (!changedOwnerName.equalsIgnoreCase(userName)) {
				fc.utobj().throwsException("Lead Owner did not get change.");
			}

			pi.clickOwnerChangeHistory_DetailedHistory(driver);
			boolean isTrue = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + userName + "')]");
			if (isTrue == false) {
				fc.utobj().printBugStatus("Lead Owner Changed Remarks not created in Detailed History");
			}
			fc.sales().sales_common().closeColorBox(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales333" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FS_LeadManagement_LogATask_001", testCaseDescription = "Verify Log a Task Feature Lead Through Batch")
	public void logATaskActionMenu() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		String firstName = dataSet.get("FirstName");
		String lastName = dataSet.get("LastName");

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Add lead Status");
			AdminFranchiseSalesLeadStatusPageTest p1 = new AdminFranchiseSalesLeadStatusPageTest();
			p1.addNewLeadStatus(driver);

			LeadSummaryUI pobj = new LeadSummaryUI(driver);

			String randomChar = fc.utobj().generateRandomChar();

			String lastNameUnique = lastName + randomChar;
			String leadNameSearch = lastNameUnique;

			String leadName = firstName + " " + lastNameUnique;
			fc.utobj().printTestStep("Add two leads");
			addLeadLeadSummaryWithLeadName(driver, config, firstName, lastNameUnique);
			addLeadLeadSummaryWithLeadName(driver, config, firstName, lastNameUnique);

			FSSearchPageTest p3 = new FSSearchPageTest();
			p3.searchByLeadName(driver, firstName, leadNameSearch);
			fc.utobj().clickElement(driver, pobj.checkBoxAll);
			fc.utobj().printTestStep("Log a Task from batch");

			fc.utobj().selectActionMenuItems(driver, "Log a Task");
			FSCommonMethods cm = new FSCommonMethods();
			Map<String, String> mapInfo = cm.fillTaskDetails_Mandatory(driver, config);

			try {

				for (String winHandle : driver.getWindowHandles()) {
					driver.switchTo().window(winHandle);
					List<String> popInfo = new ArrayList<String>();
					popInfo.add(mapInfo.get("assignTo"));
					popInfo.add(mapInfo.get("status"));
					popInfo.add(mapInfo.get("subject"));
					popInfo.add(mapInfo.get("taskType"));
					popInfo.add(mapInfo.get("comments"));
					fc.utobj().clickLink(driver, leadName);
					boolean isInfoCorrect = fc.utobj().assertPageSourceWithMultipleRecords(driver, popInfo);
					if (isInfoCorrect == false) {
						fc.utobj().printBugStatus("Info of task mismatch in the task pop up");
					}
				}
			} catch (Exception e) {
				fc.utobj().printBugStatus("Unable to validate Reminder Pop Up functionality");
			}

			p3.searchByLeadName(driver, firstName, lastNameUnique);
			fc.utobj().clickLinkIgnoreCase(driver, leadName);

			FSLeadSummaryPrimaryInfoPageTest pi = new FSLeadSummaryPrimaryInfoPageTest();

			boolean isStatusChanged = pi.verifySubjectOpenTasks(driver, mapInfo);

			if (isStatusChanged == false) {
				fc.utobj().throwsException("Task did not get added");
			}

			pi.clickStatusChangeHistory_DetailedHistory(driver);

			boolean isTrue = fc.utobj().assertPageSource(driver, mapInfo.get("subject"));
			if (isTrue == false) {
				fc.utobj().printBugStatus("Lead Status Changed Remarks not created in Detailed History");
			}

			fc.sales().sales_common().closeColorBox(driver);

			pi.clickNext(driver);

			isStatusChanged = pi.verifySubjectOpenTasks(driver, mapInfo);

			if (isStatusChanged == false) {
				fc.utobj().throwsException("Task did not get added");
			}

			pi.clickStatusChangeHistory_DetailedHistory(driver);

			isTrue = fc.utobj().assertPageSource(driver, mapInfo.get("subject"));
			if (isTrue == false) {
				fc.utobj().printBugStatus("Lead Status Changed Remarks not created in Detailed History");
			}

			fc.sales().sales_common().closeColorBox(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales" , "TC_FS_LeadManagement_014" }) // Verified : Akshat
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FS_LeadManagement_014", testCaseDescription = "Verify Merge lead through Merge Button")
	void mergeLeadsActionMenu() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		int recordCount = 0;

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String firstName = dataSet.get("leadName");
		String lastName = dataSet.get("leadName");

		try {
			driver = fc.loginpage().login(driver);

			String randomChar = fc.utobj().generateRandomChar();

			String lastNameUnique = lastName + randomChar;
			String leadNameSearch = lastNameUnique;

			fc.utobj().printTestStep("Add two leads");

			fc.sales().sales_common().fsModule(driver);
			addLeadLeadSummaryWithLeadName(driver, config, firstName, lastNameUnique);
			addLeadLeadSummaryWithLeadName(driver, config, firstName, lastNameUnique);

			FSSearchPageTest p3 = new FSSearchPageTest();
			p3.searchByLeadName(driver, firstName, leadNameSearch);
			LeadSummaryUI pobj = new LeadSummaryUI(driver);
			pobj = new LeadSummaryUI(driver);
			fc.utobj().clickElement(driver, pobj.checkBoxAll);
			fc.utobj().printTestStep("Merge these two leads");

			fc.utobj().selectActionMenuItems(driver, "Merge Leads");
			FSLeadSummaryMergeLeadsPage pobj2 = new FSLeadSummaryMergeLeadsPage(driver);
			fc.utobj().clickElement(driver, pobj2.mergeBtn);
			fc.utobj().assertPageSource(driver, "Lead added through merging of two leads");
			p3.searchByLeadName(driver, firstName, lastNameUnique);
			pobj = new LeadSummaryUI(driver);
			fc.utobj().printTestStep("Search if the leads are merged or not");

			recordCount = fc.utobj().findSingleLinkPartialText(driver, lastNameUnique);
			if (recordCount > 1) {
				fc.utobj().throwsException("More than one lead found after merge - record mismatch!");
			} else if (recordCount < 1) {
				fc.utobj().throwsException("No leads found after merge - record mismatch!");
			}
			p3.archivedLeadSearchByName(driver, firstName, lastNameUnique);
			boolean isLeadMerged = fc.utobj().assertLinkPartialText(driver, lastNameUnique);
			if (isLeadMerged == false) {
				fc.utobj().throwsException("Leads not found in archived section!");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			// e.printStackTrace();
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales", "salesarchivelead", "sales_failed" , "TC_FS_LeadManagement_015" }) // Verified : Akshat
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FS_LeadManagement_015", testCaseDescription = "Verify Archive lead through Archive Button")
	void archiveLeadsActionMenu() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String firstName = "Test";
		String lastName = fc.utobj().generateTestData("Archive");

		try {
			driver = fc.loginpage().login(driver);
			LeadSummaryUI pobj = new LeadSummaryUI(driver);

			fc.utobj().printTestStep("Add a lead");

			addLeadLeadSummaryWithLeadName(driver, config, firstName, lastName);

			FSSearchPageTest p3 = new FSSearchPageTest();
			p3.searchByLeadName(driver, firstName, lastName);
			fc.utobj().clickElement(driver, pobj.checkBoxAll);
			fc.utobj().printTestStep("Archive this lead");

			/* fc.utobj().selectActionMenuItems(driver,"Archive Leads"); */
			fc.utobj().clickElement(driver, pobj.archiveBottomBtn);
			fc.utobj().acceptAlertBox(driver);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.notesArchive, "Test Leads Archived!");
			fc.utobj().clickElement(driver, pobj.notesSubmitBtn);
			fc.utobj().clickElement(driver, pobj.cboxCloseBtn);
			p3.searchByLeadName(driver, firstName, lastName);
			fc.utobj().assertLinkPartialTextNotPresent(driver, lastName);
			fc.utobj().printTestStep("Search if leads are archived or not");

			p3.archivedLeadSearchByName(driver, firstName, lastName);
			boolean isLeadArchived = fc.utobj().assertLinkPartialText(driver, lastName);
			if (isLeadArchived == false) {
				fc.utobj().throwsException("Leads not archived!");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			// e.printStackTrace();
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void deleteLeadActionMenu() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String firstName = dataSet.get("FirstName");
		String lastName = dataSet.get("LastName");

		try {
			driver = fc.loginpage().login(driver);

			LeadSummaryUI pobj = new LeadSummaryUI(driver);

			String randomChar = fc.utobj().generateRandomChar();

			String lastNameUnique = lastName + randomChar;
			String leadNameSearch = lastNameUnique;

			fc.utobj().printTestStep("Add two leads");

			addLeadLeadSummaryWithLeadName(driver, config, firstName, lastNameUnique);
			addLeadLeadSummaryWithLeadName(driver, config, firstName, lastNameUnique);

			FSSearchPageTest p3 = new FSSearchPageTest();
			p3.searchByLeadName(driver, firstName, leadNameSearch);
			fc.utobj().clickElement(driver, pobj.checkBoxAll);
			String windowHandle = driver.getWindowHandle();
			fc.utobj().printTestStep("Delete these leads");

			fc.utobj().selectActionMenuItems(driver, "Delete");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.okCboxBtn);
			driver.switchTo().window(windowHandle);
			p3.searchByLeadName(driver, leadNameSearch, leadNameSearch);
			fc.utobj().assertNotInPageSource(driver, leadNameSearch);
			AdminPageTest p4 = new AdminPageTest();
			p4.openDeletedLogsLnk(driver);
			fc.utobj().assertPageSource(driver, leadNameSearch);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FS_LeadManagement_017", testCaseDescription = "Verify Change Status of lead through Change Status Button")
	public void changeStatusBtn() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String firstName = dataSet.get("leadName");
		String lastName = dataSet.get("leadName");

		try {
			driver = fc.loginpage().login(driver);
			AdminFranchiseSalesLeadStatusPageTest p1 = new AdminFranchiseSalesLeadStatusPageTest();
			String leadStatus = p1.addNewLeadStatus(driver);

			String randomChar = fc.utobj().generateRandomChar();

			String lastNameUnique = lastName + randomChar;
			String leadNameSearch = lastNameUnique;

			fc.utobj().printTestStep("Add two leads");

			fc.sales().sales_common().fsModule(driver);
			addLeadLeadSummaryWithLeadName(driver, config, firstName, lastNameUnique);
			addLeadLeadSummaryWithLeadName(driver, config, firstName, lastNameUnique);

			FSSearchPageTest p3 = new FSSearchPageTest();
			p3.searchByLeadName(driver, firstName, leadNameSearch);
			LeadSummaryUI pobj = new LeadSummaryUI(driver);
			fc.utobj().clickElement(driver, pobj.checkBoxAll);
			fc.utobj().printTestStep("Change status of the leads through button");

			fc.utobj().clickElement(driver, pobj.changeStatusBottomBtn);
			String windowHandle = driver.getWindowHandle();
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().selectDropDownByVisibleText(driver, pobj.leadStatusIDDrp, leadStatus);
			fc.utobj().clickElement(driver, pobj.changeStatusCboxBtn);
			fc.utobj().clickElement(driver, pobj.cboxCloseBtn);
			driver.switchTo().window(windowHandle);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales", "saleschecking001", "sales_correction" , "TC_FS_LeadManagement_018" }) //
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FS_LeadManagement_018", testCaseDescription = "Verify Change Owner of lead through Change Owner Button")
	void changeOwnerBtn() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		String firstName = dataSet.get("FirstName");
		String lastName = dataSet.get("LastName");

		try {
			driver = fc.loginpage().login(driver);
			String emailId = "salesautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest p4 = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			p4.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			String randomChar = fc.utobj().generateRandomChar();

			String lastNameUnique = lastName + randomChar;
			String leadNameSearch = lastNameUnique;

			String leadName = firstName + " " + lastNameUnique;
			fc.utobj().printTestStep("Add two leads");

			fc.sales().sales_common().fsModule(driver);
			addLeadLeadSummaryWithLeadName(driver, config, firstName, lastNameUnique);
			addLeadLeadSummaryWithLeadName(driver, config, firstName, lastNameUnique);

			FSSearchPageTest p3 = new FSSearchPageTest();
			p3.searchByLeadName(driver, firstName, leadNameSearch);

			LeadSummaryUI pobj = new LeadSummaryUI(driver);
			fc.utobj().clickElement(driver, pobj.checkBoxAll);
			fc.utobj().printTestStep("Change owner of the leads");
			fc.utobj().clickElement(driver, pobj.changeOwnerBottomBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			String userName = corpUser.getuserFullName();
			fc.utobj().selectDropDownByVisibleText(driver, pobj.leadOwnerDrp, userName);
			fc.utobj().clickElement(driver, pobj.changeOwnerBtn);
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().clickElement(driver, pobj.cboxCloseBtn);

			p3.searchByLeadName(driver, firstName, lastNameUnique);
			fc.utobj().clickLinkIgnoreCase(driver, leadName);

			FSLeadSummaryPrimaryInfoPageTest pi = new FSLeadSummaryPrimaryInfoPageTest();
			String changedOwnerName = pi.getOwnerName(driver);

			if (!changedOwnerName.equalsIgnoreCase(userName)) {
				fc.utobj().throwsException("Lead Owner did not get change.");
			}

			pi.clickOwnerChangeHistory_DetailedHistory(driver);
			boolean isTrue = fc.utobj().assertPageSource(driver, userName);
			if (isTrue == false) {
				fc.utobj().printBugStatus("Lead Owner Changed Remarks not created in Detailed History");
			}

			fc.sales().sales_common().closeColorBox(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			e.printStackTrace();
			// fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales" , "TC_FS_LeadManagement_024" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FS_LeadManagement_024", testCaseDescription = "Verify Merge leads through Merge Button")
	void mergeLeadsBtn() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		boolean isLeadArchived = false;

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String firstName = dataSet.get("leadName");
		String lastName = dataSet.get("leadName");

		try {
			driver = fc.loginpage().login(driver);

			LeadSummaryUI pobj = new LeadSummaryUI(driver);

			String randomChar = fc.utobj().generateRandomChar();

			String lastNameUnique = lastName + randomChar;
			String leadNameSearch = lastNameUnique;

			fc.utobj().printTestStep("Add two leads");

			addLeadLeadSummaryWithLeadName(driver, config, firstName, lastNameUnique);
			addLeadLeadSummaryWithLeadName(driver, config, firstName, lastNameUnique);

			FSSearchPageTest p3 = new FSSearchPageTest();
			p3.searchByLeadName(driver, firstName, leadNameSearch);
			fc.utobj().clickElement(driver, pobj.checkBoxAll);
			fc.utobj().printTestStep("Merge leads through button");

			fc.utobj().clickElement(driver, pobj.mergeLeadBottomBtn);
			FSLeadSummaryMergeLeadsPage pobj2 = new FSLeadSummaryMergeLeadsPage(driver);
			fc.utobj().clickElement(driver, pobj2.mergeBtn);
			fc.utobj().assertPageSource(driver, "Lead added through merging of two leads");
			p3.searchByLeadName(driver, firstName, leadNameSearch);
			int recordCount = fc.utobj().findSingleLinkPartialText(driver, lastNameUnique);
			if (recordCount > 1) {
				fc.utobj().throwsException("More than one lead found after merge - record mismatch!");
			} else if (recordCount < 1) {
				fc.utobj().throwsException("No lead found after merge- record mismatch!");
			}

			p3.archivedLeadSearchByName(driver, firstName, leadNameSearch);
			isLeadArchived = fc.utobj().assertLinkPartialText(driver, lastNameUnique);
			if (isLeadArchived == false) {
				fc.utobj().throwsException("Lead not present in archived section!");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "saleskajgsejkv" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-10-03", testCaseId = "TC_FS_LeadManagement_025", testCaseDescription = "Verify Archive leads through Archive Button")
	void archiveLeadsBtn() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		boolean isLeadArchived = false;
		String firstName = "Test";
		String lastName = fc.utobj().generateTestData("Archive");

		try {
			driver = fc.loginpage().login(driver);
			LeadSummaryUI pobj = new LeadSummaryUI(driver);
			fc.utobj().printTestStep("Add a lead");

			addLeadLeadSummaryWithLeadName(driver, config, firstName, lastName);

			FSSearchPageTest p3 = new FSSearchPageTest();
			p3.searchByLeadName(driver, firstName, lastName);
			fc.utobj().clickElement(driver, pobj.checkBoxAll);
			fc.utobj().printTestStep("Archive this lead");

			fc.utobj().clickElement(driver, pobj.archiveBottomBtn);
			fc.utobj().acceptAlertBox(driver);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.notesArchive, "Test Leads Archived!");
			fc.utobj().clickElement(driver, pobj.notesSubmitBtn);
			fc.utobj().clickElement(driver, pobj.cboxCloseBtn);
			// p3.searchByLeadName(driver,firstName,lastName);
			// fc.utobj().assertLinkPartialTextNotPresent(driver,lastName);
			p3.archivedLeadSearchByName(driver, firstName, lastName);
			isLeadArchived = fc.utobj().assertLinkPartialText(driver, lastName);
			if (isLeadArchived == false) {
				fc.utobj().throwsException("Leads not archived!");
			}
			fc.utobj().clickElement(driver, pobj.selectAllCheckBox);
			fc.utobj().clickElement(driver,
					driver.findElement(
							By.xpath(".//td[contains(text(),'" + fc.utobj().translateString("Archived Leads Summary")
									+ "')]/ancestor::tbody/tr[4]/td/table/tbody/tr/td/input[@name='active']")));
			p3.searchByLeadName(driver, firstName, lastName);
			boolean isLeadPresent = fc.utobj().assertLinkPartialText(driver, firstName + " " + lastName);
			if (isLeadPresent == false) {
				fc.utobj().throwsException("Lead not getting active from archived leads");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FS_LeadManagement_030", testCaseDescription = "Verify Log a Task through Action Menu")
	void logATaskActionIcon() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			Sales fs = new Sales();
			fs.leadManagement(driver);
			clickAddLeadLink(driver);
			FSLeadSummaryPrimaryInfoPageTest p1 = new FSLeadSummaryPrimaryInfoPageTest();
			fc.utobj().printTestStep("Add a lead");

			Map<String, String> leadInfo = p1.fillLeadBasicInfo(driver);
			String firstName = leadInfo.get("firstName");
			String lastName = leadInfo.get("lastName");
			FSSearchPageTest p3 = new FSSearchPageTest();
			p3.searchByLeadName(driver, firstName, lastName);
			LeadSummaryUI pobj = new LeadSummaryUI(driver);

			fc.utobj().printTestStep("Log a Task");

			fc.utobj().actionImgOption(driver, firstName + " " + lastName, "Log a Task");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String taskSubject = "Task Subject ";
			taskSubject = fc.utobj().generateTestData(taskSubject);
			fc.utobj().sendKeys(driver, pobj.taskSubject, taskSubject);
			fc.utobj().clickElement(driver, pobj.addTaskBtn);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales" , "TC_FS_LeadManagement_031" }) // Fixed : Verified // Akshat
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-19", testCaseId = "TC_FS_LeadManagement_031", testCaseDescription = "Verify Log a Call through Action Menu")
	void logACallActionIcon() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String callStatus = dataSet.get("callStatus");
		String communicationType = dataSet.get("communicationType");

		try {
			driver = fc.loginpage().login(driver);
			AdminConfigurationConfigureCallStatusPageTest p1 = new AdminConfigurationConfigureCallStatusPageTest();
			fc.utobj().printTestStep("Add Call Status");

			callStatus = p1.addCallStatus(driver, callStatus);

			AdminConfigurationConfigureCommunicationTypePageTest p2 = new AdminConfigurationConfigureCommunicationTypePageTest();
			communicationType = p2.addCommunicationType(driver, communicationType);
			Sales fs = new Sales();
			FsExportPage exportPage = new FsExportPage(driver);
			fc.sales().sales_common().fsModule(driver);
			fs.leadManagement(driver);
			clickAddLeadLink(driver);
			FSLeadSummaryPrimaryInfoPageTest p3 = new FSLeadSummaryPrimaryInfoPageTest();
			fc.utobj().printTestStep("Add a lead");

			Map<String, String> leadInfo = p3.fillLeadBasicInfo(driver);
			String firstName = leadInfo.get("firstName");
			String lastName = leadInfo.get("lastName");
			FSSearchPageTest p4 = new FSSearchPageTest();
			p4.searchByLeadName(driver, firstName, lastName);
			LeadSummaryUI pobj = new LeadSummaryUI(driver);

			// List<WebElement> list = pobj.listing;
			fc.utobj().printTestStep("Log a Call");

			fc.utobj().actionImgOption(driver, firstName + " " + lastName, "Log a Call");
			String callSubject = "Call Subject";
			callSubject = fc.utobj().generateTestData(callSubject);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.callSubject, callSubject);
			fc.utobj().selectDropDown(driver, pobj.callStatus, callStatus);
			fc.utobj().selectDropDown(driver, pobj.communicationType, communicationType);
			fc.utobj().selectDropDown(driver, pobj.callTimeHr, "10");
			fc.utobj().selectDropDown(driver, pobj.callTimeMin, "15 Min");
			try {
				fc.utobj().selectDropDown(driver, pobj.callTimeAPM, "AM");
			} catch (Exception e) {

			}
			fc.utobj().clickElement(driver, pobj.addCallBtn);

			fc.utobj().clickElement(driver, pobj.callConfirmScheduleTaskNoBtn);
			fc.utobj().printTestStep("Go to Export ");
			fc.utobj().printTestStep(
					"Enter value in the field in the call section > Select Fields > Select Primary Info First Name and Last Name > View data");
			fc.utobj().printTestStep("All Call Details should be displayed in the view page.");
			fs.exportPage(driver);
			fc.utobj().clickElement(driver, exportPage.chkbxActHistoryCall);
			fc.utobj().clickElement(driver, pobj.searchData);
			fc.utobj().selectValFromMultiSelect(driver, exportPage.loggedBy, "FranConnect Administrator");
			
			fc.utobj().sendKeys(driver, exportPage.callSubject, callSubject);
			
			/*
			 * String callDateFrom=fc.utobj().getCurrentDateUSFormat();
			 * fc.utobj().sendKeysDateField(driver, exportPage.callDateFrom,
			 * callDateFrom); fc.utobj().sendKeysDateField(driver,
			 * exportPage.callDateTo, callDateFrom);
			 */
			// fc.utobj().selectValFromMultiSelect(driver,
			// exportPage.callStatus, callStatus);
			
			fc.utobj().selectValFromMultiSelect(driver, exportPage.callStatus, callStatus);
			// fc.utobj().selectValFromMultiSelect(driver,
			// exportPage.callcommunicationType, communicationType);
			fc.utobj().selectValFromMultiSelect(driver, exportPage.callcommunicationType, communicationType);

			// fc.utobj().sendKeys(driver, exportPage.callComments, "comments");
			fc.utobj().clickElement(driver, exportPage.selectFields);
			fc.utobj().clickElement(driver, exportPage.selectFieldsFirstName);
			fc.utobj().clickElement(driver, exportPage.selectFieldsLastName);
			fc.utobj().clickElement(driver, exportPage.selectFieldsCall);
			fc.utobj().clickElement(driver, exportPage.selectFieldsVeiw);
			// fc.utobj().clickElement(driver, pobj.searchData);
			boolean isLeadNameFirstPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text(),'" + firstName + "')]");
			if (isLeadNameFirstPresent == false) {
				fc.utobj().throwsException("Lead  First Name is not present in export data ");
			}
			boolean isLeadLastNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text(),'" + lastName + "')]");
			if (isLeadLastNamePresent == false) {
				fc.utobj().throwsException("Lead  Last Name is not present in export data ");
			}

			boolean isLeadOwnerNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text(),'FranConnect Administrator')]");
			if (isLeadOwnerNamePresent == false) {
				fc.utobj().throwsException("Lead  Owner Name is not present in export data ");
			}

			boolean isLeadCallSubjectPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text(),'" + callSubject + "')]");
			if (isLeadCallSubjectPresent == false) {
				fc.utobj().throwsException("Lead  call Subject present is not present in export data ");
			}

			boolean isLeadCTypePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text(),'" + communicationType + "')]");
			if (isLeadCTypePresent == false) {
				fc.utobj().throwsException("Lead  Communication Type is not present in export data ");
			}

			boolean isLeadTimePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text(),'10:15:00 AM')]");
			if (isLeadTimePresent == false) {
				fc.utobj().throwsException("Lead  Communication Type is not present in export data ");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			e.printStackTrace();
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	// Fully Verified on 20 sep 2016
	public void clickAddLeadLink(WebDriver driver) throws Exception {
		LeadSummaryUI pobj = new LeadSummaryUI(driver);

		try {
			fc.utobj().clickElement(driver, pobj.addLeadLnkAlternate);
		} catch (Exception e) {
			fc.utobj().clickElement(driver, pobj.addLeadLnk);
		}
		WebElement leadNameTxtBox = null;

		FSLeadSummaryAddLeadPage pobj2 = new FSLeadSummaryAddLeadPage(driver);

		try {
			leadNameTxtBox = pobj2.firstName;
			if (leadNameTxtBox.isDisplayed() == false) {
				fc.utobj().throwsException("Add Lead Page Not Opened!");
			}
		} catch (Exception e) {

		}
	}

	@Test(groups = { "sales" , "TC_AddLeadWithFullInfo_001" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_AddLeadWithFullInfo_001", testCaseDescription = "Lead is getting added with All fields of primary info filled")
	public void addLeadWithFullInfo() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		// Map<String,String> dataSet = fc.utobj().readTestData("sales",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.sales().sales_common().fsModule(driver);

			Sales fs = new Sales();
			fs.leadManagement(driver);

			FSLeadSummaryPageTest fslpt = new FSLeadSummaryPageTest();
			fslpt.clickAddLeadLink(driver);

			FSLeadSummaryPrimaryInfoPageTest fslpipt = new FSLeadSummaryPrimaryInfoPageTest();
			fc.utobj().printTestStep("Add a lead with full info");

			Map<String, String> leadInfo = fslpipt.fillLeadBasicInfo(driver);

			Reporter.log("Lead Added with lead name : " + leadInfo.get("firstName") + " " + leadInfo.get("lastName"));

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_AddLeadWithBasicInfo_002", testCaseDescription = "Lead is getting added with Only Mandatory Fields filled.")
	void addLeadWithBasicInfo() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.sales().sales_common().fsModule(driver);

			Sales fs = new Sales();
			fs.leadManagement(driver);
			fc.utobj().printTestStep("Add a lead with only mandatory fields filled");

			FSLeadSummaryPageTest fslpt = new FSLeadSummaryPageTest();
			fslpt.clickAddLeadLink(driver);
			FSLeadSummaryPrimaryInfoPageTest fslpipt = new FSLeadSummaryPrimaryInfoPageTest();
			Map<String, String> leadInfo = fslpipt.fillLeadBasicInfo(driver);

			Reporter.log("Lead Added with lead name : " + leadInfo.get("firstName") + " " + leadInfo.get("lastName"));

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	// Fully Verified on 20 sep 2016

	@Test(groups = { "sales302010" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FS_LeadQualificationDetails_001", testCaseDescription = "Add Lead Qualification Details")
	void addLeadQualificationDetails() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			driver = fc.loginpage().login(driver);
			fc.sales().sales_common().fsModule(driver);
			Sales fs = new Sales();
			fs.leadManagement(driver);
			FSLeadSummaryPageTest fsPage = new FSLeadSummaryPageTest();
			fc.utobj().printTestStep("Add a lead and fill and validate full qualification details");

			fsPage.clickAddLeadLink(driver);
			FSLeadSummaryPrimaryInfoPageTest pInfoPage = new FSLeadSummaryPrimaryInfoPageTest();
			Map<String, String> leadInfo = pInfoPage.fillLeadBasicInfo(driver);
			Reporter.log("Lead Added with lead name : " + leadInfo.get("firstName") + " " + leadInfo.get("lastName"));

			pInfoPage.gotoQualificationDetailsTab(driver);
			FSLeadSummaryQualificationDetailsPageTest qdPage = new FSLeadSummaryQualificationDetailsPageTest();
			qdPage.fillFullQualificationDetailsTab(driver, config);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales" , "TC_FS_LeadPersonalProfileDetails_001" }) // passed
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FS_LeadPersonalProfileDetails_001", testCaseDescription = "Verify Add Lead Personal Profile Details")
	void addLeadPersonalProfileDetails() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.sales().sales_common().fsModule(driver);
			Sales fs = new Sales();
			fs.leadManagement(driver);
			FSLeadSummaryPageTest fsPage = new FSLeadSummaryPageTest();
			fsPage.clickAddLeadLink(driver);
			FSLeadSummaryPrimaryInfoPageTest pInfoPage = new FSLeadSummaryPrimaryInfoPageTest();
			pInfoPage.fillLeadBasicInfo(driver);
			fc.utobj().printTestStep("Add a lead");

			pInfoPage.gotoPersonalProfileTab(driver);
			FSLeadSummaryPersonalProfilePageTest ppPage = new FSLeadSummaryPersonalProfilePageTest();
			fc.utobj().printTestStep("Fill Personal Profile");
			ppPage.fillPersonalProfileTab(driver, config);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales" , "TC_FS_LeadPersonalProfileDetails_002" }) // new ui // passed
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FS_LeadPersonalProfileDetails_002", testCaseDescription = "Verify Add And Modify of Lead Personal Profile Details")
	void modifyLeadPersonalProfileDetails() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.sales().sales_common().fsModule(driver);
			Sales fs = new Sales();
			fs.leadManagement(driver);
			FSLeadSummaryPageTest fsPage = new FSLeadSummaryPageTest();
			fc.utobj().printTestStep("Add a lead");
			fsPage.clickAddLeadLink(driver);
			FSLeadSummaryPrimaryInfoPageTest pInfoPage = new FSLeadSummaryPrimaryInfoPageTest();
			Map<String, String> leadInfo = pInfoPage.fillLeadBasicInfo(driver);
			Reporter.log("Lead Added with lead name : " + leadInfo.get("firstName") + " " + leadInfo.get("lastName"));

			pInfoPage.gotoPersonalProfileTab(driver);
			FSLeadSummaryPersonalProfilePageTest ppPage = new FSLeadSummaryPersonalProfilePageTest();
			fc.utobj().printTestStep("Add personal profile info");
			ppPage.fillPersonalProfileTab(driver, config);
			fc.utobj().printTestStep("Modify personal profile info");
			fc.utobj().clickLink(driver, "Modify");
			ppPage.fillPersonalProfileTab(driver, config);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FS_LeadPersonalProfileDetails_003", testCaseDescription = "Verify Add And Delete of Lead Personal Profile Details")
	void deleteLeadPersonalProfileDetails() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.sales().sales_common().fsModule(driver);
			Sales fs = new Sales();
			fs.leadManagement(driver);
			FSLeadSummaryPageTest fsPage = new FSLeadSummaryPageTest();
			fc.utobj().printTestStep("Add lead");
			fsPage.clickAddLeadLink(driver);
			FSLeadSummaryPrimaryInfoPageTest pInfoPage = new FSLeadSummaryPrimaryInfoPageTest();
			Map<String, String> leadInfo = pInfoPage.fillLeadBasicInfo(driver);
			Reporter.log("Lead Added with lead name : " + leadInfo.get("firstName") + " " + leadInfo.get("lastName"));

			pInfoPage.gotoPersonalProfileTab(driver);
			FSLeadSummaryPersonalProfilePageTest ppPage = new FSLeadSummaryPersonalProfilePageTest();
			fc.utobj().printTestStep("Fill Personal Profile");
			ppPage.fillPersonalProfileTab(driver, config);
			fc.utobj().printTestStep("Delete Personal Profile");
			fc.utobj().clickLink(driver, "Delete");
			fc.utobj().acceptAlertBox(driver);
			ppPage.fillPersonalProfileTab(driver, config);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales", "TC_Sales_LeadManagement_AddLead" }) // Fixed : Verified // Akshat
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Sales_LeadManagement_AddLead", testCaseDescription = "Verify Add lead from Lead Management Page and check all details")
	void verifyAddLead() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		String firstName = fc.utobj().generateTestData(dataSet.get("FirstName"));
		String lastName = fc.utobj().generateTestData(dataSet.get("LastName"));

		try {
			driver = fc.loginpage().login(driver);
			SearchUI search_page = new SearchUI(driver);
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String userName = fc.utobj().generateTestData(dataSet.get("userName"));
			String userNameCorp = userName;
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "salesautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser.setUserName(userNameCorp);
			corpUser = corpTest.createDefaultUser(driver, corpUser);

			fc.home_page().logout(driver);
			fc.loginpage().loginWithParameter(driver, userNameCorp, corpUser.getPassword());
			fc.sales().sales_common().fsModule(driver);
			LeadSummaryUI pobj = new LeadSummaryUI(driver);
			fc.utobj().printTestStep("Add a lead");

			addLeadSummaryWithLeadNameOwnerName(driver, firstName, lastName, corpUser.getuserFullName());

			String leadName = firstName + " " + lastName;

			fc.utobj().printTestStep("Verify The Added Lead Search By Top Search");
			boolean isSearchTrue = false;
			for (int i = 0; i < 3; i++) {
				if (isSearchTrue == false) {
					fc.utobj().sendKeys(driver, search_page.topSearchField, leadName);

					if (i == 2) {

					}
					fc.utobj().clickEnterOnElement(driver, search_page.topSearchField);
					fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//i/following-sibling::span[contains(text(),'Sales')]"));
					try {
						fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
								".//custom[contains(text () , '" + leadName + "')]"));
						isSearchTrue = true;
					} catch (Exception e) {
						Reporter.log(e.getMessage());
						System.out.println(e.getMessage());
					}
				}
			}

			if (isSearchTrue == false) {
				fc.utobj().throwsException("was not able to search Lead through Top Search");
			}

			FSLeadSummaryPrimaryInfoPageTest pobj1 = new FSLeadSummaryPrimaryInfoPageTest();
			fc.utobj().printTestStep("Go to Acvity History");
			driver = pobj1.gotoActivityHistorySection(driver);

			boolean isRemarkPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text () ,'Lead added through FranConnect Application')]");
			if (isRemarkPresent == false) {
				fc.utobj().throwsException(
						"At Activity History 'Lead added through FranConnect Application' not verfied");
			}
			driver = pobj1.clickDetailedHistoryFromInsideFrame(driver);

			try {

				driver.switchTo().defaultContent();
			} catch (Exception e) {
				fc.commonMethods().switch_cboxIframe_frameId(driver);
			}
			boolean isDetailedHistoryRemarkPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text () ,'Lead added through FranConnect Application')]");
			if (isDetailedHistoryRemarkPresent == false) {
				fc.utobj().throwsException(
						"At Detailed History 'Lead added through FranConnect Application' not verfied");
			}
			fc.sales().sales_common().closeColorBox(driver);

			// fc.utobj().clickElement(driver,pobj.closeCBox);
			// fc.utobj().switchFrameToDefault(driver);
			FSSearchPageTest p2 = new FSSearchPageTest();
			fc.utobj().printTestStep("Search Lead");
			p2.searchByLeadName(driver, firstName, lastName);
			boolean isLeadPresent = fc.utobj().assertLinkPartialText(driver, firstName + " " + lastName);
			if (isLeadPresent == false) {
				fc.utobj().throwsException("was not able to verify lead from search page");
			}
			boolean isOwnerPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text(),'" + corpUser.getuserFullName() + "')]");
			if (isOwnerPresent == false) {
				fc.utobj().throwsException("was not able to verify LEAD OWNER from search page");
			}

			// notification Search
			try {
				fc.utobj().clickElement(driver, pobj.notificationBarShow);
			} catch (Exception e) {

			}
			fc.utobj().clickElement(driver, pobj.clkNotification);
			fc.utobj().clickElement(driver, pobj.viewAllNotification);

			isLeadPresent = fc.utobj().assertLinkPartialText(driver, firstName + " " + lastName);
			if (isLeadPresent == false) {
				fc.utobj().throwsException("Lead not visible from search page");
			}
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='notificationData']//*[contains(text(),'" + firstName + " " + lastName + "')]"));
			isLeadPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text () , '" + firstName + "')]");
			if (isLeadPresent == false) {
				fc.utobj().throwsException("Lead not visible from notification link search");
			}
			isOwnerPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () ,'" + corpUser.getuserFullName() + "')]");
			if (isOwnerPresent == false) {
				fc.utobj().throwsException("Lead Owner not present , from notification link search");
			}

			fc.utobj().printTestStep("Verifying new lead addition mail");
			String expectedSubject = "New Lead Added.";
			String expectedMessageBody = firstName + " " + lastName;

			Map<String, String> mailData = fc.utobj().readMailBox(expectedSubject, expectedMessageBody, emailId,
					"sdg@1a@Hfs");
			System.out.println(mailData);
			if (!mailData.get("mailBody").contains(firstName)) {
				fc.utobj().throwsSkipException("was not able to verify Lead First Name in owner assignment email");
			}

			if (!mailData.get("mailBody").contains(lastName)) {
				fc.utobj().throwsSkipException("was not able to verify lead Last Name in owner assignment email");
			}
			if (!mailData.get("subject").contains(expectedSubject)) {
				fc.utobj().throwsSkipException("was not able to verify lead Last Name in owner assignment email");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			e.printStackTrace();
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	// Anukaran

	@Test(groups = { "sales" , "TC_Sales_LeadManagement_CheckDuplicateLeads" }) // Verified : Akshat
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Sales_LeadManagement_CheckDuplicateLeads", testCaseDescription = "Verify Duplicate Lead Icon at primary Info of lead")
	void verifyDuplicateAddLead() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		String firstName = fc.utobj().generateTestData(dataSet.get("FirstName"));
		String lastName = fc.utobj().generateTestData(dataSet.get("LastName"));

		try {
			driver = fc.loginpage().login(driver);

			String userName = "FranConnect Administrator";
			LeadSummaryUI pobj = new LeadSummaryUI(driver);
			fc.utobj().printTestStep("Add a lead");
			// Map<String,String> leadName =
			// addLeadSummaryWithLeadNameOwnerName(driver,config,firstName,lastName,userName);
			Sales fsmod = new Sales();
			fsmod.leadManagement(driver);
			clickAddLeadLink(driver);
			String country = "USA";
			String state = "Alabama";
			String email = "testautomation@franqa.net";
			// String leadOwner = dataSet.get("leadOwner");
			String leadSourceCategory = "Advertisement";
			String leadSourceDetails = "Magazine";
			String companyName = "New Comapany Limited";

			FSLeadSummaryAddLeadPage pobj1 = new FSLeadSummaryAddLeadPage(driver);

			fc.utobj().sendKeys(driver, pobj1.firstName, firstName);
			fc.utobj().sendKeys(driver, pobj1.lastName, lastName);
			fc.utobj().selectDropDownByVisibleText(driver, pobj1.country, country);

			fc.utobj().selectDropDownByVisibleText(driver, pobj1.state, state);
			fc.utobj().sendKeys(driver, pobj1.emailID, email);
			/* fc.utobj().sendKeys(driver, pobj.zip, "123456"); */
			fc.utobj().sendKeys(driver, pobj1.phone, "1234567891");
			fc.utobj().selectDropDownByVisibleText(driver, pobj1.leadOwnerID, userName);
			fc.utobj().selectDropDownByVisibleText(driver, pobj1.leadSource2ID, leadSourceCategory);

			fc.utobj().selectDropDownByVisibleText(driver, pobj1.leadSource3ID, leadSourceDetails);

			fc.utobj().clickElement(driver, pobj1.save);

			// Adding second lead
			fc.utobj().printTestStep(
					"Add Another lead with same values in first name, last name, email, phone and company name (Dont submit the lead)");
			fc.sales().sales_common().fsModule(driver);
			fsmod.leadManagement(driver);
			fc.utobj().printTestStep("Click Add Lead Link");
			clickAddLeadLink(driver);

			fc.utobj().sendKeys(driver, pobj1.firstName, firstName);
			fc.utobj().sendKeys(driver, pobj1.lastName, lastName);
			fc.utobj().selectDropDownByVisibleText(driver, pobj1.country, country);

			fc.utobj().selectDropDownByVisibleText(driver, pobj1.state, state);
			fc.utobj().sendKeys(driver, pobj1.emailID, email);
			/* fc.utobj().sendKeys(driver, pobj.zip, "123456"); */
			fc.utobj().sendKeys(driver, pobj1.phone, "1234567891");
			fc.utobj().selectDropDownByVisibleText(driver, pobj1.leadOwnerID, userName);
			fc.utobj().selectDropDownByVisibleText(driver, pobj1.leadSource2ID, leadSourceCategory);

			fc.utobj().selectDropDownByVisibleText(driver, pobj1.leadSource3ID, leadSourceDetails);

			// fc.utobj().clickElement(driver, pobj1.save);

			// String parentWindow=driver.getWindowHandle();
			fc.utobj().clickElement(driver, pobj1.duplcateIcon);

			fc.utobj().printTestStep("Checking duplicate lead on next window");
			/*
			 * Set<String> allWindows2=driver.getWindowHandles(); for (String
			 * currentWindow : allWindows2) { if
			 * (!currentWindow.equalsIgnoreCase(parentWindow)) {
			 * driver.switchTo().window(currentWindow);
			 * //driver.switchTo().window(parentWindow); boolean
			 * isLeadFirstnamePresent=fc.utobj().verifyCase(driver,
			 * ".//*[contains(text () , '"+firstName+"')]"); boolean
			 * isLeadLastnamePresent=fc.utobj().verifyCase(driver,
			 * ".//*[contains(text () , '"+lastName+"')]"); if
			 * (isLeadFirstnamePresent==false || isLeadLastnamePresent==false) {
			 * fc.utobj().throwsException("Lead duplicates icon not working"); }
			 * driver.close(); driver.switchTo().window(parentWindow); } }
			 */
			String parentWindowsHandle = driver.getWindowHandle();
			Set<String> allWindows = driver.getWindowHandles();
			for (String currentWindow : allWindows) {

				if (!currentWindow.equalsIgnoreCase(parentWindowsHandle)) {

					driver.switchTo().window(currentWindow);
					try {

						boolean isLeadFirstnamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
								".//*[contains(text () , '" + firstName + "')]");
						boolean isLeadLastnamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
								".//*[contains(text () , '" + lastName + "')]");
						if (isLeadFirstnamePresent == false || isLeadLastnamePresent == false) {
							fc.utobj().throwsException("Lead duplicates icon not working");
						}
						driver.close();
					} catch (Exception e) {
						driver.close();
					}
				}
				driver.switchTo().window(parentWindowsHandle);
			}

			// Adding third lead
			fc.utobj().printTestStep(
					"Add Another lead with different values in first name, last name, email, phone and company name (Dont submit the lead)");
			fc.utobj().clickElement(driver, pobj1.save);
			String firstName1 = fc.utobj().generateTestData(dataSet.get("FirstName"));
			String lastName1 = fc.utobj().generateTestData(dataSet.get("LastName"));
			String email1 = "salesautomation@staffex.com";
			fsmod.leadManagement(driver);
			clickAddLeadLink(driver);

			fc.utobj().sendKeys(driver, pobj1.firstName, firstName1);
			fc.utobj().sendKeys(driver, pobj1.lastName, lastName1);
			fc.utobj().selectDropDownByVisibleText(driver, pobj1.country, country);

			fc.utobj().selectDropDownByVisibleText(driver, pobj1.state, state);
			fc.utobj().sendKeys(driver, pobj1.emailID, email1);
			/* fc.utobj().sendKeys(driver, pobj.zip, "123456"); */
			fc.utobj().sendKeys(driver, pobj1.phone, "1234056789");
			fc.utobj().selectDropDownByVisibleText(driver, pobj1.leadOwnerID, userName);
			fc.utobj().selectDropDownByVisibleText(driver, pobj1.leadSource2ID, leadSourceCategory);

			fc.utobj().selectDropDownByVisibleText(driver, pobj1.leadSource3ID, leadSourceDetails);

			// fc.utobj().clickElement(driver, pobj1.save);

			String parentWindow1 = driver.getWindowHandle();
			fc.utobj().clickElement(driver, pobj1.duplcateIcon);

			fc.utobj().printTestStep("Checking duplicate lead not present on next window");
			Set<String> allWindows3 = driver.getWindowHandles();
			for (String currentWindow1 : allWindows3) {
				if (!currentWindow1.equalsIgnoreCase(parentWindow1)) {
					driver.switchTo().window(currentWindow1);
					// driver.switchTo().window(parentWindow1);
					// boolean
					// isLeadFirstnamePresent=fc.utobj().verifyCase(driver,
					// ".//*[contains(text () , '"+firstName+"')]");
					boolean isLeadNotPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
							".//*[contains(text () , '" + firstName + " " + lastName + "')]");
					if (isLeadNotPresent == true) {
						fc.utobj().throwsException("Lead duplicates icon not working");
					}
					// driver.close();
					driver.switchTo().window(parentWindow1);
				}
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	// Anukaran

	@Test(groups = { "salesankkjhf123", "sales" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Sales_BrokersAgencySummary", testCaseDescription = "Verify Add,Modify and Deletion of Broker Agency Summary")
	void verifyBrokerAgency() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			String agencyName = fc.utobj().generateTestData(dataSet.get("agencyName"));
			String firstName = fc.utobj().generateTestData(dataSet.get("FirstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("LastName"));

			AdminFranchiseSalesBrokersAgencySummaryPage pobj = new AdminFranchiseSalesBrokersAgencySummaryPage(driver);
			AdminFranchiseSalesBrokersAgencySummaryAddBrokersAgencyPage pobj1 = new AdminFranchiseSalesBrokersAgencySummaryAddBrokersAgencyPage(
					driver);
			fc.utobj().printTestStep("Go to Admin > Brokers Agency");

			AdminSales adsales = new AdminSales();
			adsales.adminFranchiseSalesBrokersAgencyPage(driver);

			fc.utobj().printTestStep("Add Broker Agency");
			fc.utobj().printTestStep("Fill Complete Details of Broker Agency");
			fc.utobj().clickElement(driver, pobj.addBrokerAgencyLnk);
			fc.utobj().sendKeys(driver, pobj1.agencyName, agencyName);

			fc.utobj().selectDropDownByPartialText(driver, pobj1.salutation, "Dr.");
			fc.utobj().sendKeys(driver, pobj1.ownerFirstName, firstName);
			fc.utobj().sendKeys(driver, pobj1.ownerLastName, lastName);
			fc.utobj().sendKeys(driver, pobj1.address1, "A 965");
			fc.utobj().sendKeys(driver, pobj1.address2, "Park View Appartment");
			fc.utobj().clickElement(driver, pobj1.submitBtn);
			boolean isbrokerAgencyPresent = fc.utobj().assertLinkPartialText(driver, agencyName);
			if (isbrokerAgencyPresent == false) {
				fc.utobj().throwsException("Problem in adding Broker Agency Name");
			}

			// Modify Check
			fc.utobj().printTestStep("Modify Broker Agency");
			String agencyName1 = fc.utobj().generateTestData(dataSet.get("agencyName"));
			String firstName1 = fc.utobj().generateTestData(dataSet.get("FirstName"));
			String lastName1 = fc.utobj().generateTestData(dataSet.get("LastName"));
			fc.utobj().actionImgOption(driver, agencyName, "Modify");
			fc.utobj().sendKeys(driver, pobj1.agencyName, agencyName1);

			fc.utobj().selectDropDownByPartialText(driver, pobj1.salutation, "Dr.");
			fc.utobj().sendKeys(driver, pobj1.ownerFirstName, firstName1);
			fc.utobj().sendKeys(driver, pobj1.ownerLastName, lastName1);
			fc.utobj().sendKeys(driver, pobj1.address1, "B 965");
			fc.utobj().sendKeys(driver, pobj1.address2, "Park New Appartment");
			fc.utobj().clickElement(driver, pobj1.submitBtn);

			isbrokerAgencyPresent = fc.utobj().assertLinkPartialText(driver, agencyName1);
			if (isbrokerAgencyPresent == false) {
				fc.utobj().throwsException("Problem in modifying Broker Agency Name");
			}

			// Delete Check
			fc.utobj().printTestStep("Delete Broker Agency");
			fc.utobj().actionImgOption(driver, agencyName1, "Delete");
			fc.utobj().acceptAlertBox(driver);
			isbrokerAgencyPresent = fc.utobj().assertLinkPartialText(driver, agencyName1);
			if (isbrokerAgencyPresent == true) {
				fc.utobj().throwsException("Problem in Deleting Broker Agency Name");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	// Anukaran

	@Test(groups = { "salesfail", "sales" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-06", testCaseId = "TC_Sales_BrokerType", testCaseDescription = "Verify Add, Modify and Delete of Broker Type")
	void verifyBrokerType() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			String brokerType = fc.utobj().generateTestData(dataSet.get("brokerType"));
			String brokerType1 = fc.utobj().generateTestData(dataSet.get("brokerType"));

			fc.utobj().printTestStep("Go to Brokers type link in Admin");
			AdminSales adsales = new AdminSales();
			adsales.adminFranchiseSalesBrokersTypeConfigurationPage(driver);

			AdminFranchiseSalesBrokersTypeConfigurationPage pobj3 = new AdminFranchiseSalesBrokersTypeConfigurationPage(
					driver);
			//
			// Anukaran starts
			if (!fc.utobj().getElement(driver, pobj3.isSubMenu).isSelected()) {
				fc.utobj().clickElement(driver, pobj3.isSubMenu);
			}
			// Anukaran ends
			fc.utobj().printTestStep("Add Broker Type");
			fc.utobj().clickElement(driver, pobj3.addBrokerType);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().sendKeys(driver, pobj3.brokerType, brokerType);
			fc.utobj().clickElement(driver, pobj3.addBtn);

			//
			fc.utobj().clickElement(driver, pobj3.closeBtn);
			boolean isbrokerTypePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[@value='" + brokerType + "']");
			if (isbrokerTypePresent == false) {
				fc.utobj().throwsException("Problem in adding Broker type");
			}
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@value='" + brokerType + "']"),
					brokerType1);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					(".//input[@value='" + brokerType + "']/ancestor::tr/td/input[@name='checkb']")));
			fc.utobj().printTestStep("Modify Brokers Type");
			fc.utobj().clickElement(driver, pobj3.modifybtn);
			// p1.modifyBrokerType(brokerType);
			isbrokerTypePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[@value='" + brokerType1 + "']");
			if (isbrokerTypePresent == false) {
				fc.utobj().throwsException("Problem in modifying Broker Type");
			}

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					(".//input[@value='" + brokerType1 + "']/ancestor::tr/td/input[@name='checkb']")));
			fc.utobj().printTestStep("Delete Brokers Type");
			fc.utobj().clickElement(driver, pobj3.deletebtn);
			fc.utobj().acceptAlertBox(driver);
			isbrokerTypePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[@value='" + brokerType1 + "']");
			if (isbrokerTypePresent == true) {
				fc.utobj().throwsException("Problem in deleting Broker type");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesank", "sales" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Sales_AddBrokers", testCaseDescription = "Verify Add Brokers Type,Modify and Delete")
	void verifyBrokerAgencyTypeAddBroker() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			String agencyName = fc.utobj().generateTestData(dataSet.get("agencyName"));
			String firstName = fc.utobj().generateTestData(dataSet.get("FirstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("LastName"));
			String brokerType = fc.utobj().generateTestData(dataSet.get("brokerType"));
			String brokerName = fc.utobj().generateTestData(dataSet.get("brokerName"));

			AdminFranchiseSalesBrokersAgencySummaryPage pobj = new AdminFranchiseSalesBrokersAgencySummaryPage(driver);
			AdminFranchiseSalesBrokersAgencySummaryAddBrokersAgencyPage pobj1 = new AdminFranchiseSalesBrokersAgencySummaryAddBrokersAgencyPage(
					driver);
			fc.utobj().printTestStep("Go to Admin > Brokers Agency Page");

			AdminSales adsales = new AdminSales();
			adsales.adminFranchiseSalesBrokersAgencyPage(driver);

			fc.utobj().clickElement(driver, pobj.addBrokerAgencyLnk);
			fc.utobj().sendKeys(driver, pobj1.agencyName, agencyName);

			fc.utobj().selectDropDownByPartialText(driver, pobj1.salutation, "Dr.");
			fc.utobj().sendKeys(driver, pobj1.ownerFirstName, firstName);
			fc.utobj().sendKeys(driver, pobj1.ownerLastName, lastName);
			fc.utobj().sendKeys(driver, pobj1.address1, "A 965");
			fc.utobj().sendKeys(driver, pobj1.address2, "Park View Appartment");
			fc.utobj().clickElement(driver, pobj1.submitBtn);

			boolean isbrokerAgencyPresent = fc.utobj().assertLinkPartialText(driver, agencyName);
			if (isbrokerAgencyPresent == false) {
				fc.utobj().throwsException("Problem in adding Broker Agency Name");
			}

			/*
			 * AdminFranchiseSalesBrokersTypeConfigurationPageTest p1=new
			 * AdminFranchiseSalesBrokersTypeConfigurationPageTest();
			 * p1.addBrokerType(driver,brokerType);
			 */
			fc.utobj().printTestStep("Go to Admin > Brokers Type Configuration");
			adsales.adminFranchiseSalesBrokersTypeConfigurationPage(driver);

			AdminFranchiseSalesBrokersTypeConfigurationPage pobj3 = new AdminFranchiseSalesBrokersTypeConfigurationPage(
					driver);

			// Anukaran starts
			if (!fc.utobj().getElement(driver, pobj3.isSubMenu).isSelected()) {
				fc.utobj().clickElement(driver, pobj3.isSubMenu);
			}
			fc.utobj().clickElement(driver, pobj3.moduleSave);
			// Anukaran ends

			fc.utobj().clickElement(driver, pobj3.addBrokerType);

			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().sendKeys(driver, pobj3.brokerType, brokerType);
			fc.utobj().clickElement(driver, pobj3.addBtn);

			//
			fc.utobj().clickElement(driver, pobj3.closeBtn);

			boolean isbrokerTypePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[@value='" + brokerType + "']");
			if (isbrokerTypePresent == false) {
				fc.utobj().throwsException("Problem in adding Broker Agency Name");
			}

			// FSBrokersSummaryPageTest p2 = new FSBrokersSummaryPageTest();
			// p2.addBrokers(driver,brokerName);

			fc.sales().sales_common().fsModule(driver);
			FSBrokersPage pobj2 = new FSBrokersPage(driver);
			//
			fc.utobj().moveToElementThroughAction(driver, pobj2.brokersLnk);

			fc.utobj().printTestStep("Go to Brokers Tab");
			fc.utobj().clickElement(driver, pobj2.brokersLnk);
			try {
				fc.utobj().getElement(driver, pobj2.addBrokersBtn).isEnabled();
			} catch (Exception e) {
				fc.utobj().clickElement(driver, pobj2.brokersLnk);
			}
			fc.utobj().printTestStep("Add Brokers");
			fc.utobj().printTestStep("Select Broker Agency Details and Brokers Type and submit the Details");
			fc.utobj().clickElement(driver, pobj2.addBrokersBtn);
			fc.utobj().selectDropDownByPartialText(driver, pobj2.brokerType, brokerType);
			brokerName = fc.utobj().generateTestData(brokerName);
			fc.utobj().sendKeys(driver, pobj2.firstName, brokerType);
			fc.utobj().sendKeys(driver, pobj2.firstName, brokerName);
			fc.utobj().sendKeys(driver, pobj2.lastName, brokerName);
			fc.utobj().sendKeys(driver, pobj2.emailID, "salesautoamtion@franqa.com");

			fc.utobj().selectDropDown(driver, pobj2.brokerAgencyId, agencyName);
			fc.utobj().clickElement(driver, pobj2.submitBtn);

			fc.utobj().sendKeys(driver, pobj2.brokerNameSearch, brokerName);
			fc.utobj().printTestStep("Search Broker");

			fc.utobj().clickElement(driver, pobj2.searchBtn);
			boolean isbrokerNamePresent = fc.utobj().assertLinkPartialText(driver, brokerName);
			if (isbrokerNamePresent == false) {
				fc.utobj().throwsException("Broker Name not found");
			}
			isbrokerAgencyPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + agencyName + "')]");
			if (isbrokerAgencyPresent == false) {
				fc.utobj().throwsException("Broker Agency Name not found");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesfail1232", "sales" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-07-19", testCaseId = "TC_Sales_SendEmailToLead", testCaseDescription = "Verify Send Email to the lead")
	void verifySendEmailToLead() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		String firstName = fc.utobj().generateTestData(dataSet.get("FirstName"));
		String lastName = fc.utobj().generateTestData(dataSet.get("LastName"));

		try {
			driver = fc.loginpage().login(driver);

			String userName = "FranConnect Administrator";
			String leadFullName = firstName + " " + lastName;
			LeadSummaryUI pobj = new LeadSummaryUI(driver);
			fc.utobj().printTestStep("Add a lead");

			configureAllEmailContentLead(driver);
			fc.sales().sales_common().fsModule(driver);
			Map<String, String> leadName = addLeadSummaryWithLeadNameOwnerName(driver, firstName, lastName, userName);
			// fc.utobj().clickElement(driver,fc.utobj().getElementByXpath(driver,".//*contains(text(),'"+leadFullName+"')")));
			fc.utobj().clickElement(driver, pobj.sendEmailTopLnk);
			fc.utobj().printTestStep("Send Email from top link");
			String mailSubject = fc.utobj().generateTestData("Test TopLink Email Subject");
			String mailBody = "Test Email By FranConnect System " + leadFullName;
			mailBody = mailBody + " " + fc.utobj().generateTestData(mailBody);
			FSLeadSummarySendEmailPage pobj1 = new FSLeadSummarySendEmailPage(driver);
			fc.utobj().sendKeys(driver, pobj1.subject, mailSubject);
			String windowHandle = driver.getWindowHandle();
			driver.switchTo().frame(fc.utobj().getElement(driver, pobj1.htmlFrame));
			fc.utobj().sendKeys(driver, pobj1.mailText, mailBody);
			driver.switchTo().window(windowHandle);
			fc.utobj().clickElement(driver, pobj1.sendBtn);
			// System.out.println(mailSubject);

			// verify top link email
			fc.utobj().printTestStep("Verifying new lead addition mail");
			String expectedSubject = mailSubject;
			String expectedMessageBody = mailBody;
			//
			String emailId = "salesautomation@staffex.com";
			Map<String, String> mailData = fc.utobj().readMailBox(expectedSubject, expectedMessageBody, emailId,
					"sdg@1a@Hfs");
			System.out.println(mailData);
			if (!mailData.get("mailBody").contains(leadFullName)) {
				fc.utobj().throwsSkipException("was not able to verify Lead First Name in owner assignment email");
			}
			if (!mailData.get("subject").contains(expectedSubject)) {
				fc.utobj().throwsSkipException("was not able to verify lead Last Name in owner assignment email");
			}

			// Sending email frm action menu
			Sales fsmod = new Sales();
			fsmod.leadManagement(driver);
			FSSearchPageTest p3 = new FSSearchPageTest();
			p3.searchByLeadName(driver, firstName, lastName);
			fc.utobj().actionImgOption(driver, leadFullName, "Send Email");

			String mailSubject1 = fc.utobj().generateTestData("Test ActionMenu Email Subject");
			String mailBody1 = "Test Email By FranConnect System " + leadFullName;
			mailBody1 = mailBody1 + fc.utobj().generateTestData(mailBody1);
			fc.utobj().sendKeys(driver, pobj1.subject, mailSubject1);
			String windowHandle1 = driver.getWindowHandle();
			driver.switchTo().frame(fc.utobj().getElement(driver, pobj1.htmlFrame));
			fc.utobj().sendKeys(driver, pobj1.mailText, mailBody1);
			driver.switchTo().window(windowHandle1);
			fc.utobj().clickElement(driver, pobj1.sendBtn);

			// verify action menu email
			fc.utobj().printTestStep("Verifying new lead addition mail2");
			String expectedSubject1 = mailSubject1;
			String expectedMessageBody1 = mailBody1;
			//
			Map<String, String> mailData1 = fc.utobj().readMailBox(expectedSubject1, expectedMessageBody1, emailId,
					"sdg@1a@Hfs");
			System.out.println(mailData1);
			if (!mailData1.get("mailBody").contains(leadFullName)) {
				fc.utobj().throwsSkipException("was not able to verify Lead First Name in  email");
			}
			if (!mailData1.get("subject").contains(expectedSubject1)) {
				fc.utobj().throwsSkipException("was not able to verify lead Last Name in  email");
			}

			// Sending email from action button
			// fsmod.leadManagement(driver);
			p3.searchByLeadName(driver, firstName, lastName);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					(".//a[contains(text(),'" + leadFullName + "')]/ancestor::tr/td/input[@name='checkbox']")));
			fc.utobj().clickElement(driver, driver
					.findElement(By.xpath(".//*[@id='fsLeadTable']//input[@class='cm_new_button_action showAction']")));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					(".//*[@id='actionListButtons']/table/tbody/tr[2]/td[2]/table/tbody/tr[3]/td")));

			// Sending email from action button

			String mailSubject2 = fc.utobj().generateTestData("Test Action Button Email Subject");
			String mailBody2 = "Test Email By FranConnect System " + leadFullName;
			mailBody2 = mailBody2 + fc.utobj().generateTestData(mailBody2);
			fc.utobj().sendKeys(driver, pobj1.subject, mailSubject2);
			String windowHandle2 = driver.getWindowHandle();
			driver.switchTo().frame(fc.utobj().getElement(driver, pobj1.htmlFrame));
			fc.utobj().sendKeys(driver, pobj1.mailText, mailBody2);
			driver.switchTo().window(windowHandle2);
			fc.utobj().clickElement(driver, pobj1.sendBtn);

			fc.utobj().printTestStep("Verifying new lead addition mail3");
			String expectedSubject2 = mailSubject2;
			String expectedMessageBody2 = mailBody2;
			//
			Map<String, String> mailData2 = fc.utobj().readMailBox(expectedSubject2, expectedMessageBody2, emailId,
					"sdg@1a@Hfs");
			System.out.println(mailData2);
			if (!mailData2.get("mailBody").contains(leadFullName)) {
				fc.utobj().throwsSkipException("was not able to verify Lead First Name in email");
			}
			if (!mailData2.get("subject").contains(expectedSubject2)) {
				fc.utobj().throwsSkipException("was not able to verify subject in email");
			}

			// fsmod.leadManagement(driver);
			p3.searchByLeadName(driver, firstName, lastName);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					(".//a[contains(text(),'" + leadFullName + "')]/ancestor::tr/td/input[@name='checkbox']")));
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='fsLeadTable']//input[@name='sendMail2']"));

			// Sending email from Bottom Button

			String mailSubject3 = fc.utobj().generateTestData("Test Bottom Button Email Subject");
			String mailBody3 = "Test Email By FranConnect System " + leadFullName;
			mailBody3 = mailBody3 + fc.utobj().generateTestData(mailBody3);
			fc.utobj().sendKeys(driver, pobj1.subject, mailSubject3);
			String windowHandle3 = driver.getWindowHandle();
			driver.switchTo().frame(fc.utobj().getElement(driver, pobj1.htmlFrame));
			fc.utobj().sendKeys(driver, pobj1.mailText, mailBody3);
			driver.switchTo().window(windowHandle3);
			fc.utobj().clickElement(driver, pobj1.sendBtn);

			fc.utobj().printTestStep("Verifying new lead addition mail4");
			String expectedSubject3 = mailSubject3;
			String expectedMessageBody3 = mailBody3;

			Map<String, String> mailData3 = fc.utobj().readMailBox(expectedSubject3, expectedMessageBody3, emailId,
					"sdg@1a@Hfs");
			System.out.println(mailData3);
			if (!mailData3.get("mailBody").contains(leadFullName)) {
				fc.utobj().throwsSkipException("was not able to verify Lead First Name in email");
			}
			if (!mailData3.get("subject").contains(expectedSubject3)) {
				fc.utobj().throwsSkipException("was not able to verify subject in email");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales", "sales_failed" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Sales_DocumentsTab", testCaseDescription = "Verify the Documents upload in Document tab of lead")
	void verifySalesDocumentTab() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		String firstName = fc.utobj().generateTestData(dataSet.get("FirstName"));
		String lastName = fc.utobj().generateTestData(dataSet.get("LastName"));

		try {
			driver = fc.loginpage().login(driver);

			String userName = "FranConnect Administrator";
			String leadFullName = firstName + " " + lastName;
			String documentTitle = fc.utobj().generateTestData(dataSet.get("DocumentTitle"));
			String documentTitle1 = fc.utobj().generateTestData(dataSet.get("DocumentTitle"));
			String documentTitle2 = fc.utobj().generateTestData(dataSet.get("DocumentTitle"));
			LeadSummaryUI pobj = new LeadSummaryUI(driver);
			fc.utobj().printTestStep("Add a lead");

			Map<String, String> leadName = addLeadSummaryWithLeadNameOwnerName(driver, firstName, lastName, userName);
			// fc.utobj().clickElement(driver,fc.utobj().getElementByXpath(driver,".//*contains(text(),'"+leadFullName+"')")));
			fc.utobj().printTestStep("Enter Document title and upload document > and click reset");
			fc.utobj().clickElement(driver, pobj.documentsTab);
			fc.utobj().sendKeys(driver, pobj.documentTitle, documentTitle);
			String fileName = fc.utobj().getFilePathFromTestData(dataSet.get("fileName"));
			String fileName1 = fc.utobj().getFilePathFromTestData(dataSet.get("fileName1"));
			fc.utobj().sendKeys(driver, pobj.documentAttachment, fileName);
			fc.utobj().clickElement(driver, pobj.documentReset);

			// Verifying reset button
			String text = fc.utobj().getElementByXpath(driver, ".//*[@id='documentTitle']").getText();
			String docText = fc.utobj().getElementByXpath(driver, ".//*[@id='fsDocumentAttachment']").getText();
			if (!("".equals(text)) && !("".equals(docText))) {
				fc.utobj().throwsSkipException("Reset button not working on Sales document tab");
			}
			fc.utobj().printTestStep("Click on Save without entering document title and document ");
			fc.utobj().clickElement(driver, pobj.saveDoc);
			try {
				fc.utobj().acceptAlertBox(driver);
			} catch (Exception e) {
				fc.utobj().throwsSkipException("Blank document and doctitle is not showing alert on save");
			}
			fc.utobj().printTestStep("Enter document title and document > Click Save");
			fc.utobj().sendKeys(driver, pobj.documentTitle, documentTitle);
			fc.utobj().sendKeys(driver, pobj.documentAttachment, fileName);
			fc.utobj().clickElement(driver, pobj.saveDoc);

			boolean isDocumentPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + documentTitle + "')]");
			if (isDocumentPresent == false) {
				fc.utobj().throwsException("Document not getting saved at sales page");
			}
			fc.utobj().printTestStep("Click Add More and add another document");
			fc.utobj().clickElement(driver, pobj.addMoreBtn);
			fc.utobj().sendKeys(driver, pobj.documentTitle, documentTitle1);
			fc.utobj().sendKeys(driver, pobj.documentAttachment, fileName);
			fc.utobj().clickElement(driver, pobj.saveDoc);

			isDocumentPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[@id='siteMainTable']//td[contains(text(),'"
					+ documentTitle1 + "')]/following-sibling::td/a[contains(text(),'taskFile')]");
			try {
				if (isDocumentPresent == false) {
					fc.utobj().throwsException("Add  more button not working");
				}
			} catch (Exception e) {
			}
			fc.utobj().printTestStep("Modify Document Name / Attachment");
			fc.utobj().actionImgOption(driver, documentTitle, "Modify");
			fc.utobj().sendKeys(driver, pobj.documentTitle, documentTitle2);
			fc.utobj().sendKeys(driver, pobj.documentAttachment, fileName1);
			fc.utobj().clickElement(driver, pobj.saveDoc);
			fc.utobj().acceptAlertBox(driver);
			isDocumentPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[@id='siteMainTable']//td[contains(text(),'"
					+ documentTitle2 + "')]/following-sibling::td/a[contains(text(),'pictureFile')]");
			if (isDocumentPresent == false) {
				fc.utobj().throwsException("Document modify not working");
			}

			// delete document
			fc.utobj().printTestStep("Delete Document");
			fc.utobj().actionImgOption(driver, documentTitle1, "Delete");
			fc.utobj().acceptAlertBox(driver);
			isDocumentPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[@id='siteMainTable']//td[contains(text(),'"
					+ documentTitle1 + "')]/following-sibling::td/a[contains(text(),'taskFile')]");
			if (isDocumentPresent == true) {
				fc.utobj().throwsException("Add  more button not working");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales", "sales_failed_1" , "TC_Sales_AssociateCoApplicant_001" }) // Fixed : Verified // Akshat
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-06", testCaseId = "TC_Sales_AssociateCoApplicant_001", testCaseDescription = "Verify Association of Co-Applicant to the lead")
	void verifySalesAssociateCoApplicant() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			String firstName = fc.utobj().generateTestData(dataSet.get("FirstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("LastName"));
			String firstName1 = fc.utobj().generateTestData(dataSet.get("FirstName"));
			String lastName1 = fc.utobj().generateTestData(dataSet.get("LastName"));
			String userName = "FranConnect Administrator";

			String leadFullName = firstName + " " + lastName;
			String leadFullName1 = firstName1 + " " + lastName1;
		//	LeadSummaryUI pobj = new LeadSummaryUI(driver);
			fc.utobj().printTestStep("Add a lead");

			Map<String, String> leadName = addLeadSummaryWithLeadNameOwnerName(driver, firstName, lastName, userName);
			fc.utobj().printTestStep("Add another lead");
			addLeadSummaryWithLeadNameOwnerName(driver, firstName1, lastName1, userName);
		//	Sales fsmod = new Sales();
			// fsmod.leadManagement(driver);

			associateCoApplicant(driver, firstName, lastName, leadFullName1);
			/*
			 * FSSearchPageTest p3 = new FSSearchPageTest();
			 * p3.searchByLeadName(driver,firstName,lastName);
			 * fc.utobj().printTestStep(testCaseId,
			 * "Right Action Icon of 1st lead > Associate Co-Applicant > Enter the name of the 2nd lead > Save"
			 * ); fc.utobj().actionImgOption(driver, leadFullName,
			 * "Associate Co-Applicant");
			 * fc.commonMethods().switch_cboxIframe_frameId(driver);
			 * fc.utobj().sendKeys(driver, pobj.coApplicantSearch,
			 * leadFullName1); fc.utobj().clickElement(driver,
			 * fc.utobj().getElementByXpath(driver,
			 * ".//*[@id='contentdivTopSearch']//span/u")));
			 * fc.utobj().selectDropDownByPartialText(driver,
			 * pobj.coApplicantRelationshipIDDrp, "Relative");
			 * fc.utobj().clickElement(driver, pobj.addCboxBtn);
			 * fc.utobj().clickElement(driver, pobj.closeBtn);
			 * fc.utobj().switchFrameToDefault(driver);
			 * fc.utobj().clickElement(driver,fc.utobj().getElementByXpath(
			 * driver, ".//*[contains(text(),'"+leadFullName+"')]")));
			 * fc.utobj().clickElement(driver, pobj.coApplicantsTab);
			 */
			
			// akshat
			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, leadFullName));
			PrimaryInfoUI primaryInfoUI = new PrimaryInfoUI(driver);
			fc.utobj().clickElement(driver, primaryInfoUI.CoApplicants_tab_link);
			
			boolean isCoApplicantNamePresent = fc.utobj().assertLinkPartialText(driver, leadFullName1);
			if (isCoApplicantNamePresent == false) {
				fc.utobj().throwsException("Co-Applicant Lead not Visible");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			// e.printStackTrace();
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void associateCoApplicant(WebDriver driver, String leadFirstName, String leadLastName,
			String coApplicantName) throws Exception {
		FSSearchPageTest p3 = new FSSearchPageTest();
		LeadSummaryUI pobj = new LeadSummaryUI(driver);
		p3.searchByLeadName(driver, leadFirstName, leadLastName);
		fc.utobj().actionImgOption(driver, leadFirstName + " " + leadLastName, "Associate Co-Applicant");
		fc.commonMethods().switch_cboxIframe_frameId(driver);
		fc.utobj().sendKeys(driver, pobj.coApplicantSearch, coApplicantName);
		fc.utobj().sleep();
		fc.utobj().clickElement(driver,fc.utobj().getElementByXpath(driver, ".//*[@id='contentdivTopSearch']//span/u"));
		fc.utobj().selectDropDownByPartialText(driver, pobj.coApplicantRelationshipIDDrp, "Relative");
		fc.utobj().clickElement(driver, pobj.addCboxBtn);
		fc.utobj().clickElement(driver, pobj.closeBtn);
		fc.utobj().switchFrameToDefault(driver);
	}

	@Test(groups = { "sales_old", "salesSolrLeadsCo1" })
	@TestCase(createdOn = "2017-11-15", updatedOn = "2017-11-15", testCaseId = "TC_Solr_Search_CoApplicant_001", testCaseDescription = " Verify the Solr search working for Lead added by corporate user")
	void verifySolrSearchCoApplicant() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminUsersManageCorporateUsersAddCorporateUserPageTest p4 = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			SearchUI search_page = new SearchUI(driver);

			String leadName1 = fc.utobj().generateTestData(dataSet.get("leadName1"));
			String leadName2 = fc.utobj().generateTestData(dataSet.get("leadName2"));
			String leadName3 = fc.utobj().generateTestData(dataSet.get("leadName3"));
			String leadName4 = fc.utobj().generateTestData(dataSet.get("leadName4"));
			String leadName5 = fc.utobj().generateTestData(dataSet.get("leadName5"));
			String leadName6 = fc.utobj().generateTestData(dataSet.get("leadName6"));
			String leadName7 = fc.utobj().generateTestData(dataSet.get("leadName7"));
			String leadName8 = fc.utobj().generateTestData(dataSet.get("leadName8"));

			String firstNamePre = fc.utobj().generateTestData(dataSet.get("firstName"));
			String firstName1 = firstNamePre + dataSet.get("firstName1");
			String firstName2 = firstNamePre + dataSet.get("firstName2");
			String firstName3 = firstNamePre + dataSet.get("firstName3");
			String firstName4 = firstNamePre + dataSet.get("firstName4");
			String firstName5 = firstNamePre + dataSet.get("firstName5");
			String firstName6 = firstNamePre + dataSet.get("firstName6");
			String firstName7 = firstNamePre + dataSet.get("firstName7");
			String firstName8 = firstNamePre + dataSet.get("firstName8");
			String lastName = dataSet.get("lastName");
			String userName = "FranConnect Administrator";

			addLeadSummaryWithLeadNameOwnerName(driver, firstName1, lastName, userName);
			addLeadSummaryWithLeadNameOwnerName(driver, firstName2, lastName, userName);
			addLeadSummaryWithLeadNameOwnerName(driver, firstName3, lastName, userName);
			addLeadSummaryWithLeadNameOwnerName(driver, firstName4, lastName, userName);
			addLeadSummaryWithLeadNameOwnerName(driver, firstName5, lastName, userName);
			addLeadSummaryWithLeadNameOwnerName(driver, firstName6, lastName, userName);
			addLeadSummaryWithLeadNameOwnerName(driver, firstName7, lastName, userName);
			addLeadSummaryWithLeadNameOwnerName(driver, firstName8, lastName, userName);

			addLeadSummaryWithLeadNameOwnerName(driver, leadName1, leadName1, userName);
			addLeadSummaryWithLeadNameOwnerName(driver, leadName2, leadName2, userName);
			addLeadSummaryWithLeadNameOwnerName(driver, leadName3, leadName3, userName);
			addLeadSummaryWithLeadNameOwnerName(driver, leadName4, leadName4, userName);
			addLeadSummaryWithLeadNameOwnerName(driver, leadName5, leadName5, userName);
			addLeadSummaryWithLeadNameOwnerName(driver, leadName6, leadName6, userName);
			addLeadSummaryWithLeadNameOwnerName(driver, leadName7, leadName7, userName);
			addLeadSummaryWithLeadNameOwnerName(driver, leadName8, leadName8, userName);

			associateCoApplicant(driver, leadName1, leadName1, firstName1 + " " + lastName);
			associateCoApplicant(driver, leadName2, leadName2, firstName2 + " " + lastName);
			associateCoApplicant(driver, leadName3, leadName3, firstName3 + " " + lastName);
			associateCoApplicant(driver, leadName4, leadName4, firstName4 + " " + lastName);
			associateCoApplicant(driver, leadName5, leadName5, firstName5 + " " + lastName);
			associateCoApplicant(driver, leadName6, leadName6, firstName6 + " " + lastName);
			associateCoApplicant(driver, leadName7, leadName7, firstName7 + " " + lastName);
			associateCoApplicant(driver, leadName8, leadName8, firstName8 + " " + lastName);

			boolean isSearchTrue = false;
			boolean isVeiwAll = false;
			for (int i = 0; i < 3; i++) {
				if (isSearchTrue == false) {
					fc.utobj().sendKeys(driver, search_page.topSearchField, firstNamePre);

					if (i == 2) {

					}
					fc.utobj().clickEnterOnElement(driver, search_page.topSearchField);

					fc.utobj().moveToElement(driver, driver.findElement(
							By.xpath(".//span/*[contains(text(),'" + firstName8 + " " + lastName + "')]")));
					isSearchTrue = true;
				}
			}
			if (isSearchTrue == false) {
				fc.utobj().throwsException("was not able to search Lead through Top Search");
			}
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[@ng-click='viewAll()']"));

			isVeiwAll = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + firstName1 + " " + lastName + "')]");
			isVeiwAll = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + firstName2 + " " + lastName + "')]");
			isVeiwAll = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + firstName3 + " " + lastName + "')]");
			isVeiwAll = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + firstName4 + " " + lastName + "')]");
			isVeiwAll = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + firstName5 + " " + lastName + "')]");
			isVeiwAll = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + firstName6 + " " + lastName + "')]");
			isVeiwAll = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + firstName7 + " " + lastName + "')]");
			isVeiwAll = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + firstName8 + " " + lastName + "')]");
			if (isVeiwAll == false) {
				fc.utobj().throwsException("All elements not visible on clicking view all");
			}
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'" + firstName8 + lastName + "')]"));
			// fc.utobj().switchFrame(driver,
			// fc.CommonMethods().switch_frameClass_newLayoutcboxIframe(driver));
			boolean isBrokerVerified = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text(),'" + firstName8 + lastName + "')]");
			if (isBrokerVerified == false) {
				fc.utobj().throwsException("Leads not verified on click at search");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesankjh45" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Sales_EmailsentPriortoFDDExpiration", testCaseDescription = "Verify the Email Sent Prior to FDD Expiration")
	void verifySalesEmailsentPriortoFDDExpiration() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Admin > FDD Management  >  Configure Email sent Prior to FDD Expiration");
			fc.adminpage().adminPage(driver);

			String mailSub = fc.utobj().generateTestData("FDD Expiration Notification Email");
			fc.utobj().clickLink(driver, "Configure Email sent Prior to FDD Expiration");
			AdminFDDManagementConfigureEmailsentPriortoFDDExpirationPage pobj = new AdminFDDManagementConfigureEmailsentPriortoFDDExpirationPage(
					driver);
			fc.utobj().clickElement(driver, pobj.sendEmailNotificationRadioYes);
			fc.utobj().sendKeys(driver, pobj.subjectTxt, mailSub);
			fc.utobj().sendKeys(driver, pobj.timeInterval, "0");
			// List<String> keywordList = new ArrayList<>();
			// fc.utobj().switchFrameById(driver, "emailContent_ifr");
			// fc.utobj().switchFrameById(driver, "progressIframe");
			/*
			 * fc.utobj().clickElement(driver,pobj.addKeywordBtn);
			 * //fc.utobj().selectDropDownByIndex(fc.utobj().getElementByXpath(
			 * driver, ".//*[@id='select']")), 1);
			 * keywordList.add(fc.utobj().getSelectedDropDownValue(driver,
			 * fc.utobj().getElementByXpath(driver,
			 * ".//*[@id='select']/option[2]")))+" " );
			 * keywordList.add(fc.utobj().getSelectedDropDownValue(driver,
			 * fc.utobj().getElementByXpath(driver,
			 * ".//*[@id='select']/option[3]")))+" " );
			 * keywordList.add(fc.utobj().getSelectedDropDownValue(driver,
			 * fc.utobj().getElementByXpath(driver,
			 * ".//*[@id='select']/option[4]")))+" " );
			 * keywordList.add(fc.utobj().getSelectedDropDownValue(driver,
			 * fc.utobj().getElementByXpath(driver,
			 * ".//*[@id='select']/option[5]")))+" " );
			 * 
			 */
			fc.utobj().printTestStep("Fill all details with all keywords");
			String windowHandle = driver.getWindowHandle();
			driver.switchTo().frame(fc.utobj().getElement(driver, pobj.htmlFrame1));
			fc.utobj().getElement(driver, pobj.mailText).clear();
			fc.utobj().sendKeys(driver, pobj.mailText,
					"This is body of email $FDD_NAME$ , $EXPIRY_DATE$, $VERSION$, $UPLOADING_DATE$");
			driver.switchTo().window(windowHandle);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			boolean isAdminPage = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'Admin')]");
			if (isAdminPage == false) {
				fc.utobj().throwsException("Admin page not redirected");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales_failed" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Sales_EmailsentPriortoFDDEmail", testCaseDescription = "verify Email sent Prior to FDD eMail")
	void verifySalesEmailsentPriortoFDDeMail() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Admin > FDD Management > Configure Email sent Prior to FDD Email");
			fc.adminpage().adminPage(driver);

			fc.utobj().clickLink(driver, "Configure Email sent Prior to FDD Email");
			AdminFDDmanagementConfigureEmailsentPriortoFDDEmailPage pobj = new AdminFDDmanagementConfigureEmailsentPriortoFDDEmailPage(
					driver);

			String mailBody = "$FIRST_NAME_33_1$ $LAST_NAME_33_1$ $ADDRESS1_33_1$  $ADDRESS2_33_1$ $CITY_33_1$ $COUNTRY_33_1$ $STATE_PROVINCE_33_1$ $CENTER_NAME_1_1$"
					+ "$STREET_ADDRESS_1_1$ $CITY_1_1$ $STATE_PROVINCE_1_1$ $ZIP_POSTAL_CODE_1_1$ $EMAIL_1_1$ $FAX_1_1$ $PHONE_1_1$ $MOBILE_1_1$ $FIRST_NAME_1_2$ $LAST_NAME_1_2$"
					+ "$OWNERS_FIRST_NAME$ $OWNERS_LAST_NAME$ $OWNERS_TITLE$ $OWNERS_ADDRESS$ $OWNERS_CITY$ $OWNERS_STATE$  $OWNERS_ZIP$ $OWNERS_EMAIL$  $OWNERS_PHONE$"
					+ "$OWNERS_PHONE_EXTENSION$ $OWNERS_MOBILE$ $OWNERS_FAX$ $OWNER_SIGNATURE$ $SENDER_NAME$ $NEXT_LINE$ 11111";
			// mailBody = mailBody + fc.utobj().generateTestData(mailBody);
			fc.utobj().clickElement(driver, pobj.sendEmailNotificationYes);
			fc.utobj().sendKeys(driver, pobj.mailSubject, "Shortly you will receive the FDD mail.");
			fc.utobj().sendKeys(driver, pobj.timeInterval, "0");
			String windowHandle = driver.getWindowHandle();
			driver.switchTo().frame(fc.utobj().getElement(driver, pobj.htmlFrame));
			fc.utobj().getElement(driver, pobj.mailText).clear();
			fc.utobj().printTestStep("Enter all Keywords for replacement");
			fc.utobj().sendKeys(driver, pobj.mailText, mailBody);

			driver.switchTo().window(windowHandle);
			// fc.utobj().clickElement(driver,pobj.saveBtn);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesank" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Sales_ITEM23RECEIPTSummary", testCaseDescription = "Verify Add ITEM 23 - RECEIPT settings")
	void verifyITEM23RECEIPTSummary() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Admin > FDD Management > ITEM 23 - RECEIPT Summary");
			fc.adminpage().item23ReceiptSummaryLnk(driver);
			AdminFDDManagementITEM23RECEIPTSummaryPage pobj = new AdminFDDManagementITEM23RECEIPTSummaryPage(driver);

			fc.utobj().printTestStep("Add ITEM 23 - RECEIPT");
			fc.utobj().clickElement(driver, pobj.addItem23ReceiptBtn);
			AdminFDDManagementITEM23RECEIPTSummaryAddITEM23RECEIPTPage pobj2 = new AdminFDDManagementITEM23RECEIPTSummaryAddITEM23RECEIPTPage(
					driver);
			String item23Title = fc.utobj().generateTestData(dataSet.get("item23Title"));
			fc.utobj().sendKeys(driver, pobj2.templateTitle, item23Title);
			fc.utobj().printTestStep("Enter all the details and keyword for replacement");
			String content = "$BROKER_NAME$ \n$BROKER_AGENCY$ \n$FRANCHISE_SELLER_NAME$ \n$BUSINESS_NAME$ \n$ADDRESS$ \n$STATE$ \n $COUNTRY$ \n$PHONE$ \n$DATE$";
			fc.utobj().sendKeys(driver, pobj2.item23ReceiptTextArea, content);
			fc.utobj().sendKeys(driver, pobj2.businessName, "FranConnect Inc");
			fc.utobj().sendKeys(driver, pobj2.addressTxt, "B-5, Park Avenue");
			fc.utobj().selectDropDown(driver, pobj2.countryDrp, "USA");

			fc.utobj().selectDropDown(driver, pobj2.stateDrp, "Alaska");
			fc.utobj().sendKeys(driver, pobj2.phone, "9998887777");
			fc.utobj().printTestStep("Preview and then save");
			fc.utobj().clickElement(driver, pobj2.previewBtn);
			fc.utobj().clickElement(driver, pobj2.saveBtn);

			fc.utobj().clickElement(driver, driver
					.findElement(By.xpath(".//*[@id='siteMainTable']//a[contains(text(),'" + item23Title + "')]")));
			boolean isTitlePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text(),'Title')]/following-sibling::td[contains(text(),'" + item23Title + "')]");
			if (isTitlePresent == false) {
				fc.utobj().throwsException("Item 23 RECEIPT not verified");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesank" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Sales_Logoncredentialsduration", testCaseDescription = "Verify FDD Management > Log on credentials duration")
	void verifyLogoncredentialsduration() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Go to Admin > FDD Management > Log on credentials duration");
			fc.adminpage().logOnCredentialsDurationLnk(driver);
			AdminFDDManagementLogonCredentialsDurationPage pobj = new AdminFDDManagementLogonCredentialsDurationPage(
					driver);
			fc.utobj().printTestStep("Select Log on credentials duration > Save");
			fc.utobj().selectDropDownByPartialText(driver, pobj.durationDrpDwn, "9 Days");
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.adminpage().logOnCredentialsDurationLnk(driver);
			String durationValue = fc.utobj().getSelectedDropDownValue(driver, pobj.durationDrpDwn);
			if (!("9 Days".equals(durationValue))) {
				fc.utobj().throwsException("Log on credentials duration value not getting saved");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesank", "sales" , "TC_Sales_FDDEmailTemplatesummary_001" }) // verified // Akshat
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Sales_FDDEmailTemplatesummary_001", testCaseDescription = "Verify FDD Management > FDD Email Template Summary")
	void verifyFDDEmailTemplatesummary() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Go to Admin > FDD Management > FDD Email Template summary");
			fc.adminpage().adminPage(driver);
			/*
			 * AdminPageTest p2 = new AdminPageTest();
			 * p2.openFddEmailTemplateSummaryLnk(driver);
			 */
			fc.utobj().clickLink(driver, "FDD Email Template summary");
			String mailTitle = fc.utobj().generateTestData("FDD Email Template Summary Title");
			String mailSubject = fc.utobj().generateTestData("FDD Email Template Summary");
			AdminFDDManagementFDDEmailTemplateSummaryAddTemplatePage pobj2 = new AdminFDDManagementFDDEmailTemplateSummaryAddTemplatePage(
					driver);
			fc.utobj().clickElement(driver, pobj2.addTemplateBtn);
			fc.utobj().printTestStep("Add Graphical Template");
			fc.utobj().clickElement(driver, pobj2.graphicalRadio);

			fc.utobj().sendKeys(driver, pobj2.mailTitle, mailTitle);
			fc.utobj().sendKeys(driver, pobj2.mailSubject, mailSubject + fc.utobj().generateRandomNumber());
			String windowHandle = driver.getWindowHandle();
			driver.switchTo().frame(fc.utobj().getElement(driver, pobj2.htmlFrame));

			fc.utobj().sendKeys(driver, pobj2.mailText, "Dear $FIRST_NAME$ ,"

					+ "We are pleased to send you a copy of our Franchise Disclosure Document (FDD) , Set forth below is a Username, Password and Website information for accessing and downloading our Franchise Disclosure Document."

					+ "The FDD is an important document and provides you with some basic information about our company, the franchise that we offer, and our franchise system Please carefully read the entire FDD (including all of the exhibits)."

					+ "The FDD is in PDF format. You will need a copy of Adobe Reader. You probably already have this on your computer, but in case you don't, You can download a free version of Adobe Acrobat Reader by going to www.adobe.com.)"

					+ "Username: $USERID$"

					+ "Password: $PASSWORD$"

					+ "Website: $URL_TO_DOWNLOAD_FDD$"

					+ "When you open the FDD, please click on the link on the first page. That page is the receipt, which just confirms that you have received the document."

					+ "Alternatively, you can receive the FDD (a) as an Adobe Portable Document Format (PDF) File on a CD ROM disc, (b) as a PDF attachment to an e-mail message, or (c) on paper as a two-sided hardcopy. If you prefer to have the FDD delivered in one of these other ways, please reply to this e-mail indicating which method you prefer."

					+ "If you choose to download the PDF or receive it via email, you will need internet access and a valid e-mail account, in addition to Adobe Acrobat Reader. You will also need access to a printer to print the Receipt exhibit, and also to print a paper copy for yourself."

					+ "The FDD is confidential, subject to the mutual confidentiality agreement you have signed, and contains proprietary information, including trade secrets. Neither the FDD nor any of the information contained in the FDD may be reproduced or disclosed to any person under any circumstances without our written permission."

					+ "$SENDER_NAME$");

			driver.switchTo().window(windowHandle);

			fc.utobj().clickElement(driver, pobj2.submitBtn);

			try {
				fc.utobj().clickElement(driver, pobj2.showAllBtn);
			} catch (Exception e) {
			}
			boolean isTitlePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + mailTitle + "')]");
			if (isTitlePresent == false) {
				fc.utobj().throwsException("FDD Email Template summary not verified");
			}

			String mailTitle1 = fc.utobj().generateTestData("FDD Email Template Summary Title Plain Text");
			String mailSubject1 = fc.utobj().generateTestData("FDD Email Template Summary plain Text");
			fc.utobj().clickElement(driver, pobj2.addTemplateBtn);
			fc.utobj().printTestStep("Add Plain Text Template");
			fc.utobj().clickElement(driver, pobj2.typePlainText);

			fc.utobj().sendKeys(driver, pobj2.mailTitle, mailTitle1);
			fc.utobj().sendKeys(driver, pobj2.mailSubject, mailSubject1 + fc.utobj().generateRandomNumber());

			// driver.switchTo().frame(pobj2.htmlFrame);
			fc.utobj().sendKeys(driver, pobj2.plainTxtMailBody, "Dear $FIRST_NAME$ ,"

					+ "We are pleased to send you a copy of our Franchise Disclosure Document (FDD) , Set forth below is a Username, Password and Website information for accessing and downloading our Franchise Disclosure Document."

					+ "The FDD is an important document and provides you with some basic information about our company, the franchise that we offer, and our franchise system Please carefully read the entire FDD (including all of the exhibits)."

					+ "The FDD is in PDF format. You will need a copy of Adobe Reader. You probably already have this on your computer, but in case you don't, You can download a free version of Adobe Acrobat Reader by going to www.adobe.com.)"

					+ "Username: $USERID$"

					+ "Password: $PASSWORD$"

					+ "Website: $URL_TO_DOWNLOAD_FDD$"

					+ "When you open the FDD, please click on the link on the first page. That page is the receipt, which just confirms that you have received the document."

					+ "Alternatively, you can receive the FDD (a) as an Adobe Portable Document Format (PDF) File on a CD ROM disc, (b) as a PDF attachment to an e-mail message, or (c) on paper as a two-sided hardcopy. If you prefer to have the FDD delivered in one of these other ways, please reply to this e-mail indicating which method you prefer."

					+ "If you choose to download the PDF or receive it via email, you will need internet access and a valid e-mail account, in addition to Adobe Acrobat Reader. You will also need access to a printer to print the Receipt exhibit, and also to print a paper copy for yourself."

					+ "The FDD is confidential, subject to the mutual confidentiality agreement you have signed, and contains proprietary information, including trade secrets. Neither the FDD nor any of the information contained in the FDD may be reproduced or disclosed to any person under any circumstances without our written permission."

					+ "$SENDER_NAME$");

			// driver.switchTo().window(windowHandle2);

			fc.utobj().clickElement(driver, pobj2.submitBtn);

			try {
				fc.utobj().clickElement(driver, pobj2.showAllBtn);
			} catch (Exception e) {
			}
			boolean isTitlePresent1 = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text(),'" + mailTitle1 + "')]");
			if (isTitlePresent1 == false) {
				fc.utobj().throwsException("FDD Email Template summary not verified");
			}

			String mailTitle2 = fc.utobj().generateTestData("FDD Email Template Summary Title Plain Text");
			String mailSubject2 = fc.utobj().generateTestData("FDD Email Template Summary plain Text");
			fc.utobj().clickElement(driver, pobj2.addTemplateBtn);
			fc.utobj().printTestStep("Case 3: Upload HTML / ZIP file");
			fc.utobj().clickElement(driver, pobj2.typeUploadHtml);
			fc.utobj().sendKeys(driver, pobj2.mailTitle, mailTitle2);
			fc.utobj().sendKeys(driver, pobj2.mailSubject, mailSubject2 + fc.utobj().generateRandomNumber());
			String fileName = fc.utobj().getFilePathFromTestData(dataSet.get("fileName"));

			fc.utobj().sendKeys(driver, pobj2.htmlFileAttachment, fileName);

			fc.utobj().clickElement(driver, pobj2.submitBtn);

			try {
				fc.utobj().clickElement(driver, pobj2.showAllBtn);
			} catch (Exception e) {
			}
			boolean isTitlePresent2 = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text(),'" + mailTitle2 + "')]");
			if (isTitlePresent2 == false) {
				fc.utobj().throwsException("FDD Email Template summary not verified");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesank", "sales" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Sales_FDDEmailTemplatesummary_002", testCaseDescription = "Verify Add and Modify FDD Email Template")
	void verifyModifyFDDEmailTemplatesummary() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Go to Admin > FDD Management > FDD Email Template summary");
			fc.adminpage().adminPage(driver);
			fc.utobj().clickLink(driver, "FDD Email Template summary");
			String mailTitle = fc.utobj().generateTestData("FDD Email Template Summary Title");
			String mailSubject = fc.utobj().generateTestData("FDD Email Template Summary");
			AdminFDDManagementFDDEmailTemplateSummaryAddTemplatePage pobj2 = new AdminFDDManagementFDDEmailTemplateSummaryAddTemplatePage(
					driver);
			fc.utobj().printTestStep("Add Template");
			fc.utobj().clickElement(driver, pobj2.addTemplateBtn);
			fc.utobj().clickElement(driver, pobj2.graphicalRadio);
			fc.utobj().sendKeys(driver, pobj2.mailTitle, mailTitle);
			fc.utobj().sendKeys(driver, pobj2.mailSubject, mailSubject);
			String windowHandle = driver.getWindowHandle();
			driver.switchTo().frame(fc.utobj().getElement(driver, pobj2.htmlFrame));

			fc.utobj().sendKeys(driver, pobj2.mailText, "Dear $FIRST_NAME$,"

					+ "We are pleased to send you a copy of our Franchise Disclosure Document (FDD) , Set forth below is a Username, Password and Website information for accessing and downloading our Franchise Disclosure Document."

					+ "The FDD is an important document and provides you with some basic information about our company, the franchise that we offer, and our franchise system Please carefully read the entire FDD (including all of the exhibits)."

					+ "The FDD is in PDF format. You will need a copy of Adobe Reader. You probably already have this on your computer, but in case you don't, You can download a free version of Adobe Acrobat Reader by going to www.adobe.com.)"

					+ "Username: $USERID$"

					+ "Password: $PASSWORD$"

					+ "Website: $URL_TO_DOWNLOAD_FDD$"

					+ "When you open the FDD, please click on the link on the first page. That page is the receipt, which just confirms that you have received the document."

					+ "Alternatively, you can receive the FDD (a) as an Adobe Portable Document Format (PDF) File on a CD ROM disc, (b) as a PDF attachment to an e-mail message, or (c) on paper as a two-sided hardcopy. If you prefer to have the FDD delivered in one of these other ways, please reply to this e-mail indicating which method you prefer."

					+ "If you choose to download the PDF or receive it via email, you will need internet access and a valid e-mail account, in addition to Adobe Acrobat Reader. You will also need access to a printer to print the Receipt exhibit, and also to print a paper copy for yourself."

					+ "The FDD is confidential, subject to the mutual confidentiality agreement you have signed, and contains proprietary information, including trade secrets. Neither the FDD nor any of the information contained in the FDD may be reproduced or disclosed to any person under any circumstances without our written permission."

					+ "$SENDER_NAME$");

			driver.switchTo().window(windowHandle);
			fc.utobj().clickElement(driver, pobj2.submitBtn);
			try {
				fc.utobj().clickElement(driver, pobj2.showAllBtn);

			} catch (Exception e) {

			} /*
				 * .utobj().selectActionIcon(driver, listing, mailTitle,
				 * "Modify");
				 */
			fc.utobj().printTestStep("Modify Template");
			fc.utobj().actionImgOption(driver, mailTitle, "Modify");
			String mailTitle1 = fc.utobj().generateTestData("FDD Email Template Summary Title");
			fc.utobj().sendKeys(driver, pobj2.mailTitle, mailTitle1);
			fc.utobj().printTestStep("Save Template");
			fc.utobj().clickElement(driver, pobj2.submitBtn);
			try {
				fc.utobj().clickElement(driver, pobj2.showAllBtn);
			} catch (Exception e) {

			}
			boolean isTitlePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text(),'" + mailTitle1 + "')]");
			if (isTitlePresent == false) {
				fc.utobj().throwsException("FDD Email Template summary not verified");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesjulyrt457", "sales" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-06", testCaseId = "TC_Sales_SendFDD_001", testCaseDescription = "Verify fdd login and download")
	void verifySalesSendFDD() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Configure Email sent Prior to FDD Email");
			fc.adminpage().adminPage(driver);

			fc.utobj().clickLink(driver, "Configure Email sent Prior to FDD Email");
			AdminFDDmanagementConfigureEmailsentPriortoFDDEmailPage pobj = new AdminFDDmanagementConfigureEmailsentPriortoFDDEmailPage(
					driver);

			String mailBody = "Sales prior to FDD sent email "

					+ "First Name $FIRST_NAME_33_1$ Last Name $LAST_NAME_33_1$ Address1 $ADDRESS1_33_1$ Address2 $ADDRESS2_33_1$ Lead City $CITY_33_1$ Country $COUNTRY_33_1$ State / Province $STATE_PROVINCE_33_1$"

					+ "Lead Owners First Name $OWNERS_FIRST_NAME$ Lead Owners Last Name $OWNERS_LAST_NAME$ Lead Owners Title $OWNERS_TITLE$ Lead Owners Address $OWNERS_ADDRESS$"

					+ "Lead Owners City $OWNERS_CITY$ Lead Owners State $OWNERS_STATE$ Lead Owners Zip/Postal Code $OWNERS_ZIP$"

					+ "Lead Owners Email $OWNERS_EMAIL$ Lead Owners Phone $OWNERS_PHONE$ Lead Owners Phone Extension $OWNERS_PHONE_EXTENSION$"

					+ "Lead Owners Mobile $OWNERS_MOBILE$ Lead Owners Fax $OWNERS_FAX$ Lead Owner Signature $OWNER_SIGNATURE$ "

					+ "Sender's Name $SENDER_NAME$ New Line $NEXT_LINE$"

					+ " Username: $USERID$"

					+ " Password: $PASSWORD$"

					+ " Website: $URL_TO_DOWNLOAD_FDD$";

			// mailBody = mailBody + fc.utobj().generateTestData(mailBody);
			fc.utobj().clickElement(driver, pobj.sendEmailNotificationYes);
			fc.utobj().sendKeys(driver, pobj.mailSubject, "Shortly you will receive the FDD mail.");
			fc.utobj().sendKeys(driver, pobj.timeInterval, "0");
			String windowHandle = driver.getWindowHandle();
			driver.switchTo().frame(fc.utobj().getElement(driver, pobj.htmlFrame));
			fc.utobj().getElement(driver, pobj.mailText).clear();
			fc.utobj().sendKeys(driver, pobj.mailText, mailBody);
			driver.switchTo().window(windowHandle);
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("ITEM 23 - RECEIPT Summary");
			fc.adminpage().item23ReceiptSummaryLnk(driver);
			AdminFDDManagementITEM23RECEIPTSummaryPage npobj = new AdminFDDManagementITEM23RECEIPTSummaryPage(driver);
			//
			fc.utobj().clickElement(driver, npobj.addItem23ReceiptBtn);
			AdminFDDManagementITEM23RECEIPTSummaryAddITEM23RECEIPTPage pobj2 = new AdminFDDManagementITEM23RECEIPTSummaryAddITEM23RECEIPTPage(
					driver);
			String item23Title = fc.utobj().generateTestData(dataSet.get("item23Title"));
			fc.utobj().sendKeys(driver, pobj2.templateTitle, item23Title);

			String content = " Item23 Email Broker Name $BROKER_NAME$ \n Broker Agency $BROKER_AGENCY$ \n Broker Address1 $BROKER_ADDRESS1$ \n "
					+ "Broker Address2 $BROKER_ADDRESS2$ \n Broker City $BROKER_CITY$ \n Broker State $BROKER_STATE$ "
					+ " $FRANCHISE_SELLER_NAME$ \n$BUSINESS_NAME$ \n$ADDRESS$ "
					+ "\n Broker ZipCode $BROKER_ZIPCODE$ \nBroker Home Phone $BROKER_HOME_PHONE$ \n Broker Work Phone $BROKER_WORK_PHONE$ \n"
					+ "Broker Mobile $BROKER_MOBILE$ \n Lead Name $LEAD_NAME$ "
					+ "Lead Address1 $LEAD_ADDRESS1$ \n Lead Address2 $LEAD_ADDRESS2$ \n Lead City $CITY$ \n Lead State $LEAD_STATE$ \n Lead Zip/ Postal Code $ZIP$ \n "
					+ "Lead Home Phone $HOME_PHONE$ \n Lead Work Phone $WORK_PHONE$ \n Lead Mobile $MOBILE$ Seller Name $FRANCHISE_SELLER_NAME$"
					+ "Business Name $BUSINESS_NAME$ \n Address $ADDRESS$ \n State $STATE$ \n Country $COUNTRY$ \n Phone $PHONE$ \n Date $DATE$";

			fc.utobj().sendKeys(driver, pobj2.item23ReceiptTextArea, content);
			fc.utobj().sendKeys(driver, pobj2.businessName, "FranConnect Inc");
			fc.utobj().sendKeys(driver, pobj2.addressTxt, "B-5, Park Avenue");
			fc.utobj().selectDropDown(driver, pobj2.countryDrp, "USA");

			fc.utobj().selectDropDown(driver, pobj2.stateDrp, "Alabama");
			fc.utobj().sendKeys(driver, pobj2.phone, "9998887777");
			fc.utobj().clickElement(driver, pobj2.previewBtn);
			fc.utobj().clickElement(driver, pobj2.saveBtn);

			fc.utobj().printTestStep("Log on credentials duration");
			fc.adminpage().logOnCredentialsDurationLnk(driver);
			AdminFDDManagementLogonCredentialsDurationPage nnpobj = new AdminFDDManagementLogonCredentialsDurationPage(
					driver);
			fc.utobj().selectDropDownByPartialText(driver, nnpobj.durationDrpDwn, "9 Days");
			fc.utobj().clickElement(driver, nnpobj.saveBtn);

			fc.adminpage().adminPage(driver);
			fc.utobj().printTestStep("FDD Email Template summary");
			fc.utobj().clickLink(driver, "FDD Email Template summary");
			String mailTitle = fc.utobj().generateTestData("FDD Email Template Summary Title");
			String mailSubject = fc.utobj().generateTestData("FDD Email Template Summary");
			AdminFDDManagementFDDEmailTemplateSummaryAddTemplatePage pobj3 = new AdminFDDManagementFDDEmailTemplateSummaryAddTemplatePage(
					driver);
			fc.utobj().clickElement(driver, pobj3.addTemplateBtn);
			fc.utobj().clickElement(driver, pobj3.graphicalRadio);
			fc.utobj().sendKeys(driver, pobj3.mailTitle, mailTitle);
			fc.utobj().sendKeys(driver, pobj3.mailSubject, mailSubject);
			String windowHandle2 = driver.getWindowHandle();
			driver.switchTo().frame(fc.utobj().getElement(driver, pobj3.htmlFrame));
			fc.utobj().sendKeys(driver, pobj3.mailText, "Dear $FIRST_NAME$ $LAST_NAME$,"

					+ "We are pleased to send you a copy of our Franchise Disclosure Document (FDD) , Set forth below is a Username, Password and Website information for accessing and downloading our Franchise Disclosure Document."

					+ "First Name $FIRST_NAME_33_1$ Last Name $LAST_NAME_33_1$ Address1 $ADDRESS1_33_1$ Address2 $ADDRESS2_33_1$ Lead City $CITY_33_1$ Country $COUNTRY_33_1$ State / Province $STATE_PROVINCE_33_1$"

					+ "Lead Owners First Name $OWNERS_FIRST_NAME$ Lead Owners Last Name $OWNERS_LAST_NAME$ Lead Owners Title $OWNERS_TITLE$ Lead Owners Address $OWNERS_ADDRESS$"

					+ "Lead Owners City $OWNERS_CITY$ Lead Owners State $OWNERS_STATE$ Lead Owners Zip/Postal Code $OWNERS_ZIP$"

					+ "Lead Owners Email $OWNERS_EMAIL$ Lead Owners Phone $OWNERS_PHONE$ Lead Owners Phone Extension $OWNERS_PHONE_EXTENSION$"

					+ "Lead Owners Mobile $OWNERS_MOBILE$ Lead Owners Fax $OWNERS_FAX$ Lead Owner Signature $OWNER_SIGNATURE$ "

					+ "Sender's Name $SENDER_NAME$ New Line $NEXT_LINE$"

					+ " Username: $USERID$"

					+ " Password: $PASSWORD$"

					+ " Website: $URL_TO_DOWNLOAD_FDD$");

			driver.switchTo().window(windowHandle2);

			fc.utobj().clickElement(driver, pobj3.submitBtn);
			fc.utobj().printTestStep("FDD Management");
			AdminFDDManagementFDDManagementPage pobj4 = new AdminFDDManagementFDDManagementPage(driver);
			String fddName = fc.utobj().generateTestData(dataSet.get("fddName"));
			String fddVersion = fc.utobj().generateTestData(dataSet.get("fddVersion"));
			fc.adminpage().fddManagementLnk(driver);
			fc.utobj().clickElement(driver, pobj4.uploadFDDbtn);
			fc.utobj().sendKeys(driver, pobj4.fddName, fddName);
			fc.utobj().sendKeys(driver, pobj4.fddVersion, fddVersion);
			fc.utobj().sendKeys(driver, pobj4.fddDate, "03/10/2021");
			fc.utobj().selectDropDownByPartialText(driver, pobj4.fddItem23ReceiptSales, item23Title);
			fc.utobj().doubleClickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='ufocAction_countryID']/option[contains(text(),'USA')]"));
			fc.utobj().selectDropDownByPartialText(driver, pobj4.fddItem23ReceiptInfoMgr, item23Title);
			// String divisionNAme=fc.utobj().getSelectedDropDownValue(driver,
			// fc.utobj().getElementByXpath(driver,".//*[@id='ufocAction_brandID']/option[2]")));
			// fc.utobj().selectDropDownByPartialText(driver, pobj4.fddDivision,
			// divisionNAme);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='countryID1_0']"));
			try {
				fc.utobj().acceptAlertBox(driver);
			} catch (Exception e) {
			}
			String fileName = fc.utobj().getFilePathFromTestData(dataSet.get("fileName"));
			fc.utobj().sendKeys(driver, pobj4.fddUploadFile, fileName);
			// fc.utobj().sendKeys(driver,pobj4.fddComments, "Comments");
			fc.utobj().clickElement(driver, pobj4.fddAddFdd);
			// add lead
			String firstName = fc.utobj().generateTestData(dataSet.get("FirstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("LastName"));
			String firstName1 = fc.utobj().generateTestData(dataSet.get("FirstName"));
			String lastName1 = fc.utobj().generateTestData(dataSet.get("LastName"));
			String defaultSeller = fc.utobj().generateTestData(dataSet.get("defaultSeller"));
			String agencyName = fc.utobj().generateTestData(dataSet.get("AgencyName"));
			String brokerName = fc.utobj().generateTestData(dataSet.get("BrokerName"));
			String brokerType = fc.utobj().generateTestData(dataSet.get("BrokerType"));

			LeadSummaryUI pobj5 = new LeadSummaryUI(driver);
			fc.utobj().printTestStep("Add a Broker Type");
			// fc.sales().sales_common().fsModule(fc.sales(), driver);
			// Map<String,String> leadName =
			// addLeadSummaryWithLeadNameOwnerName(driver,config,firstName,lastName,userName);

			AdminSales adsales = new AdminSales();
			adsales.adminFranchiseSalesBrokersTypeConfigurationPage(driver);

			AdminFranchiseSalesBrokersTypeConfigurationPage newpobj = new AdminFranchiseSalesBrokersTypeConfigurationPage(
					driver);

			// Anukaran starts
			if (!fc.utobj().getElement(driver, newpobj.isSubMenu).isSelected()) {
				fc.utobj().clickElement(driver, newpobj.isSubMenu);
			}
			fc.utobj().clickElement(driver, newpobj.moduleSave);
			// Anukaran ends

			fc.utobj().clickElement(driver, newpobj.addBrokerType);

			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().sendKeys(driver, newpobj.brokerType, brokerType);
			fc.utobj().clickElement(driver, newpobj.addBtn);

			//
			fc.utobj().clickElement(driver, newpobj.closeBtn);

			AdminFranchiseSalesBrokersAgencySummaryPage salesBrokerAgency = new AdminFranchiseSalesBrokersAgencySummaryPage(
					driver);
			AdminFranchiseSalesBrokersAgencySummaryAddBrokersAgencyPage pobj9 = new AdminFranchiseSalesBrokersAgencySummaryAddBrokersAgencyPage(
					driver);
			adsales.adminFranchiseSalesBrokersAgencyPage(driver);

			fc.utobj().printTestStep("Add a Broker Agency");
			fc.utobj().clickElement(driver, salesBrokerAgency.addBrokerAgencyLnk);
			fc.utobj().sendKeys(driver, pobj9.agencyName, agencyName);

			fc.utobj().selectDropDownByPartialText(driver, pobj9.salutation, "Dr.");
			fc.utobj().sendKeys(driver, pobj9.ownerFirstName, firstName);
			fc.utobj().sendKeys(driver, pobj9.ownerLastName, lastName);
			fc.utobj().sendKeys(driver, pobj9.address1, "A 965");
			fc.utobj().sendKeys(driver, pobj9.address2, "Park View Appartment");
			fc.utobj().clickElement(driver, pobj9.submitBtn);

			fc.sales().sales_common().fsModule(driver);
			FSBrokersPage brokerPage = new FSBrokersPage(driver);
			//

			fc.utobj().moveToElementThroughAction(driver, brokerPage.brokersLnk);

			fc.utobj().clickElement(driver, brokerPage.brokersLnk);
			try {
				fc.utobj().getElement(driver, brokerPage.addBrokersBtn).isEnabled();
			} catch (Exception e) {
				fc.utobj().clickElement(driver, brokerPage.brokersLnk);
			}
			String brokerAddress = fc.utobj().generateTestData("Adrress1");
			String brokerCity = fc.utobj().generateTestData("City");

			fc.utobj().printTestStep("Add a Broker");
			fc.utobj().clickElement(driver, brokerPage.addBrokersBtn);
			fc.utobj().selectDropDownByPartialText(driver, brokerPage.brokerType, brokerType);
			brokerName = fc.utobj().generateTestData(brokerName);
			fc.utobj().sendKeys(driver, brokerPage.firstName, brokerType);
			fc.utobj().sendKeys(driver, brokerPage.firstName, brokerName);
			fc.utobj().sendKeys(driver, brokerPage.lastName, brokerName);
			fc.utobj().sendKeys(driver, brokerPage.address1, brokerAddress);
			fc.utobj().sendKeys(driver, brokerPage.city, brokerCity);
			fc.utobj().selectDropDownByVisibleText(driver, brokerPage.stateID, "Alaska");
			fc.utobj().sendKeys(driver, brokerPage.zipcode, "234123");
			fc.utobj().sendKeys(driver, brokerPage.homePhone, "1234567898");
			fc.utobj().sendKeys(driver, brokerPage.workPhone, "9876543212");
			fc.utobj().sendKeys(driver, brokerPage.emailID, "salesautoamtion@franqa.com");

			fc.utobj().selectDropDown(driver, brokerPage.brokerAgencyId, agencyName);
			fc.utobj().clickElement(driver, brokerPage.submitBtn);

			String country = dataSet.get("country");
			String state = dataSet.get("state");
			String email = "salesautomation@staffex.com";
			String leadAddress = fc.utobj().generateTestData("New test Adresss");
			String leadAddress2 = fc.utobj().generateTestData("New test second Adresss");
			String leadSourceCategory = "Brokers";
			String leadSourceDetails = agencyName;
			String leadCity = "Test City";
			String leadState = "Alabama";
			String leadCountry = "USA";
			String leadPhone = "8416825736";
			String leadHomePhone = "9999999999";
			String leadMobilePhone = "9123456789";

			fc.utobj().printTestStep("Add lead with same FDD as configured for FDD");
			String leadzipCode = fc.utobj().generateRandomNumber6Digit();
			// String userName="FranConnect Administrator";
			/*
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest p4 = new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest(); String
			 * corpUserName= fc.utobj().generateTestData("corpUserName");
			 * fc.utobj().printTestStep(testCaseId, "Add Corporate User");
			 * String userName= p4.addCorporateUser(driver, corpUserName,
			 * config, emailId);
			 */
			String corppEmailId = "commonautomation@staffex.com";
			String userName = fc.utobj().generateTestData("CorpUser");
			String CorpUserFirstName = fc.utobj().generateTestData("CorpUserFirstName");
			String CorpUserLastName = fc.utobj().generateTestData("CorpUserLastName");

			fc.adminpage().adminUsersManageCorporateUsersAddCorporateUserPage(driver);
			AdminUsersManageCorporateUsersAddCorporateUserPage userPage = new AdminUsersManageCorporateUsersAddCorporateUserPage(
					driver);

			fc.utobj().sendKeys(driver, userPage.userName, userName);
			fc.utobj().sendKeys(driver, userPage.password, "T0n1ght1");
			fc.utobj().sendKeys(driver, userPage.confirmPassword, "T0n1ght1");
			fc.utobj().selectDropDown(driver, userPage.type, "Normal User");

			fc.utobj().clickElement(driver, userPage.rolesBtn);
			fc.utobj().sendKeys(driver, userPage.searchRoles, "Corporate Administrator");
			fc.utobj().clickElement(driver, userPage.selectAll);

			fc.utobj().selectDropDown(driver, userPage.timeZone, "GMT -06:00 US/Canada/Central");
			fc.utobj().sendKeys(driver, userPage.jobTitle, "Job Title");
			fc.utobj().sendKeys(driver, userPage.firstName, CorpUserFirstName);
			fc.utobj().sendKeys(driver, userPage.lastName, CorpUserLastName);
			fc.utobj().sendKeys(driver, userPage.address, "Corp User Address");

			fc.utobj().sendKeys(driver, userPage.city, "Corp User City");
			fc.utobj().selectDropDown(driver, userPage.country, "USA");
			fc.utobj().sendKeys(driver, userPage.zipcode, leadzipCode);
			fc.utobj().selectDropDown(driver, userPage.state, "Colorado");

			fc.utobj().sendKeys(driver, userPage.phone1, "1234567890");
			fc.utobj().sendKeys(driver, userPage.phoneExt1, "22");
			fc.utobj().sendKeys(driver, userPage.fax, "12345");
			fc.utobj().sendKeys(driver, userPage.email, corppEmailId);
			fc.utobj().sendKeys(driver, userPage.mobile, "9999999999");

			fc.utobj().clickElement(driver, userPage.submit);

			fc.sales().sales_common().fsModule(driver);
			Sales fsmod = new Sales();
			fsmod.leadManagement(driver);

			clickAddLeadLink(driver);

			FSLeadSummaryAddLeadPage leadAddPage = new FSLeadSummaryAddLeadPage(driver);

			fc.utobj().selectDropDown(driver, leadAddPage.salutation, "Mr.");
			fc.utobj().sendKeys(driver, leadAddPage.firstName, firstName);
			fc.utobj().sendKeys(driver, leadAddPage.lastName, lastName);
			fc.utobj().sendKeys(driver, leadAddPage.address, leadAddress);
			fc.utobj().sendKeys(driver, leadAddPage.address2, leadAddress2);
			fc.utobj().sendKeys(driver, leadAddPage.city, leadCity);
			fc.utobj().sendKeys(driver, leadAddPage.country, leadCountry);
			fc.utobj().sendKeys(driver, leadAddPage.state, leadState);
			fc.utobj().sendKeys(driver, leadAddPage.zip, leadzipCode);
			fc.utobj().sendKeys(driver, leadAddPage.county, "Bibb");
			fc.utobj().sendKeys(driver, leadAddPage.preferredModeofContact, "Email");
			fc.utobj().sendKeys(driver, leadAddPage.bestTimeToContact, "Morning");
			fc.utobj().sendKeys(driver, leadAddPage.phone, leadPhone);
			fc.utobj().sendKeys(driver, leadAddPage.phoneExt, "91");
			fc.utobj().sendKeys(driver, leadAddPage.homePhone, leadHomePhone);
			fc.utobj().sendKeys(driver, leadAddPage.homePhoneExt, "91");
			fc.utobj().sendKeys(driver, leadAddPage.fax, "1234567891");
			fc.utobj().sendKeys(driver, leadAddPage.mobile, leadMobilePhone);
			fc.utobj().sendKeys(driver, leadAddPage.emailID, email);
			fc.utobj().sendKeys(driver, leadAddPage.companyName, "Company Name");
			fc.utobj().sendKeys(driver, leadAddPage.comments, "These are comments");
			fc.utobj().selectDropDownByVisibleText(driver, leadAddPage.leadOwnerID,
					CorpUserFirstName + " " + CorpUserLastName);
			fc.utobj().selectDropDownByVisibleText(driver, leadAddPage.leadRatingID, "Hot");
			// fc.utobj().selectDropDownByVisibleText(driver,
			// leadAddPage.marketingCode, marketingCode);
			fc.utobj().selectDropDownByVisibleText(driver, leadAddPage.leadSource2ID, leadSourceCategory);
			fc.utobj().selectDropDownByVisibleText(driver, leadAddPage.leadSource3ID, leadSourceDetails);
			fc.utobj().selectDropDownByVisibleText(driver, leadAddPage.brokerDetails, brokerName + " " + brokerName);

			fc.utobj().sendKeys(driver, leadAddPage.currentNetWorth, "6000000");
			fc.utobj().sendKeys(driver, leadAddPage.cashAvailableForInvestment, "9000000");
			fc.utobj().sendKeys(driver, leadAddPage.investTimeframe, "2 Months");
			fc.utobj().sendKeys(driver, leadAddPage.background, "Background");
			fc.utobj().sendKeys(driver, leadAddPage.sourceOfFunding, "Source of Funding");
			String nextCallDate = fc.utobj().getFutureDateUSFormat(7);
			fc.utobj().sendKeys(driver, leadAddPage.nextCallDate, nextCallDate);
			fc.utobj().sendKeys(driver, leadAddPage.noOfUnitReq, "1");
			fc.utobj().clickElement(driver, leadAddPage.save);

			// fc.utobj().clickElement(driver, pobj5.moreActionsBtn);
			fc.utobj().printTestStep("Send FDD");
			fc.utobj().clickElement(driver, pobj5.sendFddBtn);

			try {
				fc.utobj().selectDropDown(driver, pobj5.fddEmailTemplate, mailTitle);
			} catch (Exception e) {
			}
			fc.utobj().sendKeys(driver, pobj5.defaultSellerBtn, defaultSeller);
			// fc.utobj().clickElement(driver, pobj5.fddAdditionalContact);
			fc.utobj().clickElement(driver, pobj5.sendbtn);

			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			fc.utobj().clickElement(driver, pobj5.cnfrmbtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Go to Candidate Portal");
			fc.utobj().clickElement(driver, pobj5.candidatePortalBtn);
			fc.utobj().printTestStep("Click on Show Lead Details");
			fc.utobj().clickElement(driver, pobj5.showleadDetailsBtn);

			fc.utobj().printTestStep("Get Candidate Portal Password");
			fc.utobj().printTestStep("Open FDD Url");
			// String fddurl=config.get("buildUrl").concat("welcome.jsp");

			String buildUrl = config.get("buildUrl");
			String fddurl = null;
			if ((buildUrl.charAt(buildUrl.length() - 1)) == '/') {
				fddurl = config.get("buildUrl").concat("welcome.jsp");
			} else {
				fddurl = config.get("buildUrl").concat("/welcome.jsp");
			}
			String signature = firstName;
			String signatureName = firstName;
			/*
			 * WebDriver driver_welcome=null; try { if
			 * (config.get("browserName").equalsIgnoreCase("chrome")) {
			 * 
			 * System.setProperty("webdriver.chrome.driver",
			 * config.get("inputDirectory")+"\\exe\\chromedriver.exe");
			 * ChromeOptions options = new ChromeOptions();
			 * options.addArguments("disable-popup-blocking");
			 * DesiredCapabilities capabilities = DesiredCapabilities.chrome();
			 * capabilities.setCapability(ChromeOptions.CAPABILITY, options);
			 * //Browser Logs LoggingPreferences logPrefs = new
			 * LoggingPreferences(); logPrefs.enable(LogType.BROWSER,
			 * Level.ALL);
			 * capabilities.setCapability(CapabilityType.LOGGING_PREFS,
			 * logPrefs); driver_welcome = new ChromeDriver(capabilities);
			 * 
			 * driver_welcome.manage().window().maximize(); TimeUnit.SECONDS);
			 * driver_welcome.get(fddurl);
			 * 
			 */

			fc.utobj().printTestStep("Enter email and password");
			String fddpassword = getCandidatePortal_FDD_Password(driver);
			driver.navigate().to(fddurl);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='Username']"),
					"salesautomation@staffex.com");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='Password']"), fddpassword);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='ulogin']"));
			/*
			 * try { fc.utobj().clickElement(driver_welcome,
			 * driver_welcome.findElement(By.xpath(
			 * ".//a[contains(text(),'My Dashboard')]"))); } catch (Exception e)
			 * {}
			 */
			fc.utobj().sendKeys(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='leadUfocAction_leadSignature']"),
					"/" + signature + "/");
			fc.utobj().sendKeys(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='leadUfocAction_signatoryName']"), signatureName);
			try {
				fc.utobj().selectValFromMultiSelect(driver, fc.utobj().getElementByID(driver, "ms-parentsellerCombo"),
						CorpUserFirstName + " " + CorpUserLastName);
			} catch (Exception e) {

			}
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					(".//*[@id='siteMainDiv']//input[@name='leadConfirmation' and @value='Yes']")));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//input[@value='Submit']"));

			fc.utobj().printTestStep("Download FDD(Still working on this)");
			/*
			 * try { fc.utobj().clickElement(driver_welcome,
			 * driver_welcome.findElement(By.xpath(".//*[@id='submit12']"))); }
			 * catch (Exception e) { }
			 */

			/*
			 * ArrayList<String> tabs2 = new ArrayList<String>
			 * (driver_welcome.getWindowHandles());
			 * driver_welcome.switchTo().window(tabs2.get(1));
			 * fc.utobj().clickElement(driver_welcome,
			 * driver_welcome.findElement(By.xpath(
			 * ".//*[@id='pageContainer1']/xhtml:div[3]/xhtml:section/xhtml:a"))
			 * );
			 * fc.utobj().sendKeys(driver_welcome,driver_welcome.findElement(By.
			 * xpath(".//*[@id='signature']")) , "/"+signature+"/");
			 * fc.utobj().sendKeys(driver_welcome,driver_welcome.findElement(By.
			 * xpath(".//*[@id='signatureName']")), signatureName);
			 * fc.utobj().clickElement(driver_welcome,driver_welcome.findElement
			 * (By.xpath(".//*[@id='subbutton']"))); boolean isTextPresent =
			 * fc.utobj().verifyCase(driver_welcome,
			 * ".//*[contains(text () ,'Thank you. Please print a copy of this receipt for your records.')]"
			 * ); if( isTextPresent==false) { fc.utobj().throwsException(
			 * "Fdd login and download not verified "); }
			 * driver_welcome.close();
			 * driver_welcome.switchTo().window(tabs2.get(0));
			 */
			// driver_welcome.quit();
			// }
			driver = fc.loginpage().login(driver);
			fc.sales().sales_common().fsModule(driver);
			fsmod.leadManagement(driver);
			fc.utobj().printTestStep("Go to primary info > Activity history > Validate fdd email history");
			FSLeadSummaryPrimaryInfoPageTest pobj1 = new FSLeadSummaryPrimaryInfoPageTest();
			FSSearchPageTest p3 = new FSSearchPageTest();
			p3.searchByLeadName(driver, firstName, lastName);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[contains(text(),'" + firstName + " " + lastName + "')]"));
			driver = pobj1.gotoActivityHistorySection(driver);

			boolean isRemarkPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text () ,'" + mailSubject + "')]");
			// isRemarkPresent=fc.utobj().assertLinkPartialText(driver_welcome,
			// mailSubject);
			if (isRemarkPresent == false) {
				fc.utobj().throwsException("At Activity History FDD Email not verfied");
			}

			String emailId = "salesautomation@staffex.com";

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Verifying prior to FDD sent Mail"); String expectedSubject2 =
			 * item23Title; String expectedMessageBody2 = brokerName;
			 * 
			 * 
			 * Map<String,
			 * String>mailData2=fc.utobj().readMailBox(expectedSubject2,
			 * expectedMessageBody2, emailId , "sdg@1a@Hfs");
			 * System.out.println(mailData2);
			 */
			fc.utobj().printTestStep("Verifying prior to FDD sent Mail");
			String expectedSubject1 = "Shortly you will receive the FDD mail.";
			String expectedMessageBody1 = firstName;

			Map<String, String> mailData1 = fc.utobj().readMailBox(expectedSubject1, expectedMessageBody1, emailId,
					"sdg@1a@Hfs");
			System.out.println(mailData1);

			if (!(mailData1.get("mailBody").indexOf(firstName) != -1)) {
				fc.utobj().throwsSkipException("was not able to verify mailbody/keywords");
			}
			if (!(mailData1.get("mailBody").indexOf(lastName) != -1)) {
				fc.utobj().throwsSkipException("was not able to verify mailbody/keywords");
			}
			if (!(mailData1.get("mailBody").indexOf(leadAddress) != -1)) {
				fc.utobj().throwsSkipException("was not able to verify mailbody/keywords");
			}
			if (!(mailData1.get("mailBody").indexOf(leadAddress2) != -1)) {
				fc.utobj().throwsSkipException("was not able to verify mailbody/keywords");
			}
			if (!(mailData1.get("mailBody").indexOf(leadCity) != -1)) {
				fc.utobj().throwsSkipException("was not able to verify mailbody/keywords");
			}
			if (!(mailData1.get("mailBody").indexOf(leadState) != -1)) {
				fc.utobj().throwsSkipException("was not able to verify mailbody/keywords");
			}
			if (!(mailData1.get("mailBody").indexOf(leadCountry) != -1)) {
				fc.utobj().throwsSkipException("was not able to verify mailbody/keywords");
			}
			if (!(mailData1.get("mailBody").indexOf(CorpUserFirstName) != -1)) {
				fc.utobj().throwsSkipException("was not able to verify mailbody/keywords");
			}
			if (!(mailData1.get("mailBody").indexOf(CorpUserLastName) != -1)) {
				fc.utobj().throwsSkipException("was not able to verify mailbody/keywords");
			}
			if (!(mailData1.get("mailBody").indexOf("Job Title") != -1)) {
				fc.utobj().throwsSkipException("was not able to verify mailbody/keywords");
			}
			if (!(mailData1.get("mailBody").indexOf("Corp User Address") != -1)) {
				fc.utobj().throwsSkipException("was not able to verify mailbody/keywords");
			}
			if (!(mailData1.get("mailBody").indexOf("Corp User City") != -1)) {
				fc.utobj().throwsSkipException("was not able to verify mailbody/keywords");
			}
			if (!(mailData1.get("mailBody").indexOf("Colorado") != -1)) {
				fc.utobj().throwsSkipException("was not able to verify mailbody/keywords");
			}
			if (!(mailData1.get("mailBody").indexOf("(123) 456-7890") != -1)) {
				fc.utobj().throwsSkipException("was not able to verify mailbody/keywords");
			}
			if (!(mailData1.get("mailBody").indexOf("12345") != -1)) {
				fc.utobj().throwsSkipException("was not able to verify mailbody/keywords");
			}
			if (!(mailData1.get("mailBody").indexOf(corppEmailId) != -1)) {
				fc.utobj().throwsSkipException("was not able to verify mailbody/keywords");
			}
			if (!(mailData1.get("mailBody").indexOf("(999) 999-9999") != -1)) {
				fc.utobj().throwsSkipException("was not able to verify mailbody/keywords");
			}
			if (!(mailData1.get("mailBody").indexOf("22") != -1)) {
				fc.utobj().throwsSkipException("was not able to verify mailbody/keywords");
			}
			/*
			 * if(!(mailData1.get("mailBody").indexOf(leadzipCode)!=-1)){
			 * fc.utobj().throwsSkipException(
			 * "was not able to verify mailbody/keywords"); }
			 */

			fc.utobj().printTestStep("Verifying FDD Mail");
			String expectedSubject = mailSubject;
			String expectedMessageBody = "We are pleased to send you a copy of our Franchise Disclosure Document (FDD)";

			Map<String, String> mailData = fc.utobj().readMailBox(expectedSubject, expectedMessageBody, emailId,
					"sdg@1a@Hfs");
			System.out.println(mailData);
			//

			if (!mailData.get("mailBody")
					.contains("We are pleased to send you a copy of our Franchise Disclosure Document (FDD)")) {
				fc.utobj().throwsSkipException("was not able to verify mailbody/keywords");
			}

			if (!(mailData.get("mailBody").indexOf(firstName) != -1)) {
				fc.utobj().throwsSkipException("was not able to verify mailbody/keywords");
			}
			if (!(mailData.get("mailBody").indexOf(lastName) != -1)) {
				fc.utobj().throwsSkipException("was not able to verify mailbody/keywords");
			}
			if (!(mailData.get("mailBody").indexOf(leadAddress) != -1)) {
				fc.utobj().throwsSkipException("was not able to verify mailbody/keywords");
			}
			if (!(mailData.get("mailBody").indexOf(leadAddress2) != -1)) {
				fc.utobj().throwsSkipException("was not able to verify mailbody/keywords");
			}
			if (!(mailData.get("mailBody").indexOf(leadCity) != -1)) {
				fc.utobj().throwsSkipException("was not able to verify mailbody/keywords");
			}
			if (!(mailData.get("mailBody").indexOf(leadState) != -1)) {
				fc.utobj().throwsSkipException("was not able to verify mailbody/keywords");
			}
			if (!(mailData.get("mailBody").indexOf(leadCountry) != -1)) {
				fc.utobj().throwsSkipException("was not able to verify mailbody/keywords");
			}
			if (!(mailData.get("mailBody").indexOf(CorpUserFirstName) != -1)) {
				fc.utobj().throwsSkipException("was not able to verify mailbody/keywords");
			}
			if (!(mailData.get("mailBody").indexOf(CorpUserLastName) != -1)) {
				fc.utobj().throwsSkipException("was not able to verify mailbody/keywords");
			}
			if (!(mailData.get("mailBody").indexOf("Job Title") != -1)) {
				fc.utobj().throwsSkipException("was not able to verify mailbody/keywords");
			}
			if (!(mailData.get("mailBody").indexOf("Corp User Address") != -1)) {
				fc.utobj().throwsSkipException("was not able to verify mailbody/keywords");
			}
			if (!(mailData.get("mailBody").indexOf("Corp User City") != -1)) {
				fc.utobj().throwsSkipException("was not able to verify mailbody/keywords");
			}
			if (!(mailData.get("mailBody").indexOf("Colorado") != -1)) {
				fc.utobj().throwsSkipException("was not able to verify mailbody/keywords");
			}
			if (!(mailData.get("mailBody").indexOf("(123) 456-7890") != -1)) {
				fc.utobj().throwsSkipException("was not able to verify mailbody/keywords");
			}
			if (!(mailData.get("mailBody").indexOf("12345") != -1)) {
				fc.utobj().throwsSkipException("was not able to verify mailbody/keywords");
			}
			if (!(mailData.get("mailBody").indexOf(corppEmailId) != -1)) {
				fc.utobj().throwsSkipException("was not able to verify mailbody/keywords");
			}
			if (!(mailData.get("mailBody").indexOf("(999) 999-9999") != -1)) {
				fc.utobj().throwsSkipException("was not able to verify mailbody/keywords");
			}
			if (!(mailData.get("mailBody").indexOf("22") != -1)) {
				fc.utobj().throwsSkipException("was not able to verify mailbody/keywords");
			}
			if (!(mailData.get("mailBody").indexOf(leadzipCode) != -1)) {
				fc.utobj().throwsSkipException("was not able to verify mailbody/keywords");
			}
			if (!mailData.get("subject").contains(expectedSubject)) {
				fc.utobj().throwsSkipException("was not able to verify lead Last Name in owner assignment email");
			}
			/*
			 * } catch (Exception e) { fc.utobj().quitBrowserOnCatch(driver,
			 * config, e, testCaseId); }
			 */
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesank", "sales1" }) // sales1 test cases are failing
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Sales_MoveToOpener_001", testCaseDescription = "Verify Move To Opener with document and check document checklist")
	void verifyMoveToOpener() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		String firstName = fc.utobj().generateTestData(dataSet.get("FirstName"));
		String lastName = fc.utobj().generateTestData(dataSet.get("LastName"));

		try {
			driver = fc.loginpage().login(driver);

			String userName = "FranConnect Administrator";
			String leadFullName = firstName + " " + lastName;
			String documentTitle = fc.utobj().generateTestData(dataSet.get("DocumentTitle"));
			String franchiseId = fc.utobj().generateTestData(dataSet.get("FranId"));
			String centerName = fc.utobj().generateTestData(dataSet.get("CenterName"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String loginId = fc.utobj().generateTestData(dataSet.get("loginId"));
			String password = "T0n1ght1";
			String confirmPassword = "T0n1ght1";
			String openingDate = fc.utobj().getCurrentDateUSFormat();
			LeadSummaryUI pobj = new LeadSummaryUI(driver);
			AdminAreaRegionAddAreaRegionPageTest p1 = new AdminAreaRegionAddAreaRegionPageTest();
			regionName = p1.addAreaRegion(driver, regionName);
			AdminConfigurationConfigureStoreTypePageTest p2 = new AdminConfigurationConfigureStoreTypePageTest();
			storeType = p2.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Add a lead");
			Map<String, String> leadName = addLeadSummaryWithLeadNameOwnerName(driver, firstName, lastName, userName);
			fc.utobj().printTestStep("Add documents in document tab");
			// fc.utobj().clickElement(driver,fc.utobj().getElementByXpath(driver,".//*contains(text(),'"+leadFullName+"')")));
			fc.utobj().clickElement(driver, pobj.documentsTab);
			fc.utobj().sendKeys(driver, pobj.documentTitle, documentTitle);
			String fileName = fc.utobj().getFilePathFromTestData(dataSet.get("fileName"));
			fc.utobj().sendKeys(driver, pobj.documentAttachment, fileName);
			fc.utobj().clickElement(driver, pobj.saveDoc);
			fc.utobj().printTestStep("Go to Primary Info ");
			fc.utobj().clickElement(driver, pobj.leadPrimaryInfo);
			fc.utobj().printTestStep("Click on move to Opener from Botton Button");
			fc.utobj().clickElement(driver, pobj.moveToOpener);
			// fc.utobj().sendKeys(driver, pobj.StroeNo, FranchiseId);
			// fc.utobj().sendKeys(driver, pobj.centreName, centerName);
			AdminFranchiseLocationAddFranchiseLocationPage pobj1 = new AdminFranchiseLocationAddFranchiseLocationPage(
					driver);
			fc.utobj().printTestStep("Fill location details");
			fc.utobj().sendKeys(driver, pobj1.franchiseeID, franchiseId);
			fc.utobj().selectDropDownByVisibleTextTrimed(driver, pobj1.areaRegion, regionName);
			fc.utobj().sendKeys(driver, pobj1.centerName, centerName);
			fc.utobj().selectDropDownByVisibleTextTrimed(driver, pobj1.storeTypeId, storeType);
			fc.utobj().sendKeys(driver, pobj1.openerOpeningDate, openingDate);
			fc.utobj().sendKeys(driver, pobj1.city, "Test City");
			fc.utobj().selectDropDown(driver, pobj1.stateID, 1);
			fc.utobj().sendKeys(driver, pobj1.storePhone, "8989898989");
			fc.utobj().clickElement(driver, pobj1.submit);
			AdminFranchiseLocationManageFranchiseLocationsAddUserPage pobj2 = new AdminFranchiseLocationManageFranchiseLocationsAddUserPage(
					driver);
			fc.utobj().sendKeys(driver, pobj2.loginId, loginId);
			fc.utobj().sendKeys(driver, pobj2.password, password);
			fc.utobj().sendKeys(driver, pobj2.confirmPassword, confirmPassword);
			// fc.utobj().selectDropDown(driver, pobj2.type, "Normal User");
			// fc.utobj().selectDropDown(driver, pobj2.expiryDays, "Not
			// Applicable");
			
			/*fc.utobj().clickElement(driver, pobj2.rolesBtn);
			fc.utobj().sendKeys(driver, pobj2.searchRoles, "Default Franchise Role");
			fc.utobj().clickElement(driver, pobj2.selectAll);*/
			
			
			fc.utobj().selectValFromMultiSelect(driver, pobj2.rolesId, "Default Franchise Role");
			
			
			fc.utobj().selectDropDown(driver, pobj2.state, 1);
			fc.utobj().sendKeys(driver, pobj2.phone1, "1234567890");
			fc.utobj().sendKeys(driver, pobj2.phoneExt1, "2");
			fc.utobj().sendKeys(driver, pobj2.email, "salesautomation@staffex.com");
			fc.utobj().clickElement(driver, pobj2.submit);

			// InfoMgrReportsCenterDetailReportsPageTest pobj3=new
			// InfoMgrReportsCenterDetailReportsPageTest();
			// pobj3.searchFranchise(driver, franchiseId);
			fc.utobj().sendKeys(driver, pobj2.searchKey, franchiseId);
			fc.utobj().clickElement(driver, pobj2.searchBtn);
			fc.utobj().clickElement(driver, driver
					.findElement(By.xpath(".//*[@id='siteMainTable']//a[contains(text(),'" + franchiseId + "')]")));
			fc.utobj().printTestStep("Go to Document checklist of the location");
			fc.utobj().clickElement(driver, pobj2.documentsTab);
			boolean isLeadMerged = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text(),'" + documentTitle + "')]");
			if (isLeadMerged == false) {
				fc.utobj().throwsException("Leads not found in archived section!");
			}
			Sales fsmod = new Sales();
			fsmod.leadManagement(driver);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[contains(text(),'" + firstName + " " + lastName + "')]"));
			try {
				fc.utobj().clickElement(driver, pobj.moveToOpener);
				fc.utobj().throwsException("Move To opener button visible in lead info");
			} catch (Exception e) {
			}
			try {
				fc.utobj().clickElement(driver, pobj.moveToInfoMgr);
				fc.utobj().throwsException("Move To Info Manager button visible in lead info");
			} catch (Exception e) {
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesank", "sales1" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-13", testCaseId = "TC_Sales_MoveToOpener_003", testCaseDescription = "Create a Lead>Send Email from top link>From Summary Page > Action Menu Send Email> From Summary Page > Action Button Send Email> From Summary Page > Bottom Button Send Email")
	void verifyMoveToOpenerFromActionMenu() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		String firstName = fc.utobj().generateTestData(dataSet.get("FirstName"));
		String lastName = fc.utobj().generateTestData(dataSet.get("LastName"));

		try {
			driver = fc.loginpage().login(driver);

			Sales fsmod = new Sales();
			String userName = "FranConnect Administrator";
			String leadFullName = firstName + " " + lastName;
			String documentTitle = fc.utobj().generateTestData(dataSet.get("DocumentTitle"));
			String franchiseId = fc.utobj().generateTestData(dataSet.get("FranId"));
			String centerName = fc.utobj().generateTestData(dataSet.get("CenterName"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String loginId = fc.utobj().generateTestData(dataSet.get("loginId"));
			String password = "T0n1ght1";
			String confirmPassword = "T0n1ght1";
			String openingDate = fc.utobj().getCurrentDateUSFormat();
			LeadSummaryUI pobj = new LeadSummaryUI(driver);
			AdminAreaRegionAddAreaRegionPageTest p1 = new AdminAreaRegionAddAreaRegionPageTest();
			regionName = p1.addAreaRegion(driver, regionName);
			AdminConfigurationConfigureStoreTypePageTest p2 = new AdminConfigurationConfigureStoreTypePageTest();
			storeType = p2.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Add a lead");
			Map<String, String> leadName = addLeadSummaryWithLeadNameOwnerName(driver, firstName, lastName, userName);

			// fc.utobj().clickElement(driver,fc.utobj().getElementByXpath(driver,".//*contains(text(),'"+leadFullName+"')")));
			fc.utobj().printTestStep("Add documents in document tab");
			fc.utobj().clickElement(driver, pobj.documentsTab);
			fc.utobj().sendKeys(driver, pobj.documentTitle, documentTitle);
			String fileName = fc.utobj().getFilePathFromTestData(dataSet.get("fileName"));
			fc.utobj().sendKeys(driver, pobj.documentAttachment, fileName);
			fc.utobj().clickElement(driver, pobj.saveDoc);
			// fc.utobj().clickElement(driver,pobj.leadPrimaryInfo);
			// fc.utobj().clickElement(driver,pobj.moveToOpener);
			fc.utobj().printTestStep("Go to lead summary");
			fsmod.leadManagement(driver);
			fc.utobj().printTestStep("Click on move to Opener from More Actions Menu");
			fc.utobj().actionImgOption(driver, leadFullName, "Move to Opener");
			fc.utobj().printTestStep("Fill location details");
			// fc.utobj().sendKeys(driver, pobj.centreName, centerName);
			AdminFranchiseLocationAddFranchiseLocationPage pobj1 = new AdminFranchiseLocationAddFranchiseLocationPage(
					driver);
			fc.utobj().sendKeys(driver, pobj1.franchiseeID, franchiseId);
			fc.utobj().selectDropDownByPartialText(driver, pobj1.areaRegion, regionName);
			fc.utobj().sendKeys(driver, pobj1.centerName, centerName);
			fc.utobj().selectDropDownByPartialText(driver, pobj1.storeTypeId, storeType);
			fc.utobj().sendKeys(driver, pobj1.openerOpeningDate, openingDate);
			fc.utobj().sendKeys(driver, pobj1.city, "Test City");
			fc.utobj().selectDropDown(driver, pobj1.stateID, 1);
			fc.utobj().sendKeys(driver, pobj1.storePhone, "8989898989");
			// fc.utobj().sendKeys(driver, pobj1.fax, "9988668899");
			// fc.utobj().sendKeys(driver, pobj1.ownerFirstName,firstName);
			// fc.utobj().sendKeys(driver, pobj1.ownerLastName,lastName);
			fc.utobj().clickElement(driver, pobj1.submit);
			AdminFranchiseLocationManageFranchiseLocationsAddUserPage pobj2 = new AdminFranchiseLocationManageFranchiseLocationsAddUserPage(
					driver);
			fc.utobj().sendKeys(driver, pobj2.loginId, loginId);
			fc.utobj().sendKeys(driver, pobj2.password, password);
			fc.utobj().sendKeys(driver, pobj2.confirmPassword, confirmPassword);
			// fc.utobj().selectDropDown(driver, pobj2.type, "Normal User");
			// fc.utobj().selectDropDown(driver, pobj2.expiryDays, "Not
			// Applicable");
			
			/*fc.utobj().clickElement(driver, pobj2.rolesBtn);
			fc.utobj().sendKeys(driver, pobj2.searchRoles, "Default Franchise Role");
			fc.utobj().clickElement(driver, pobj2.selectAll);*/
			
			fc.utobj().selectValFromMultiSelect(driver, pobj2.rolesId, "Default Franchise Role");
			
			
			
			fc.utobj().selectDropDown(driver, pobj2.state, 1);
			fc.utobj().sendKeys(driver, pobj2.phone1, "1234567890");
			fc.utobj().sendKeys(driver, pobj2.phoneExt1, "2");
			// fc.utobj().sendKeys(driver, pobj2.phone2, "4444455555");
			// fc.utobj().sendKeys(driver, pobj2.phoneExt2, "3");
			// fc.utobj().sendKeys(driver, pobj2.fax, "3333344444");
			// fc.utobj().sendKeys(driver, pobj2.mobile, "9988776655");
			fc.utobj().sendKeys(driver, pobj2.email, "salesautomation@staffex.com");
			fc.utobj().clickElement(driver, pobj2.submit);

			fc.utobj().sendKeys(driver, pobj2.searchKey, franchiseId);
			fc.utobj().clickElement(driver, pobj2.searchBtn);
			fc.utobj().clickElement(driver, driver
					.findElement(By.xpath(".//*[@id='siteMainTable']//a[contains(text(),'" + franchiseId + "')]")));
			// fc.utobj().clickElement(driver,
			// fc.utobj().getElementByXpath(driver,".//*[contains(text(),'"+centerName+"')]")));
			fc.utobj().printTestStep("Go to Document checklist of the location");
			fc.utobj().clickElement(driver, pobj2.documentsTab);
			boolean isLeadMerged = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text(),'" + documentTitle + "')]");
			if (isLeadMerged == false) {
				fc.utobj().throwsException("Leads not found in archived section!");
			}
			fc.sales().sales_common().fsModule(driver);
			// FsModulePageTest fsmod1 = new FsModulePageTest(driver);
			fsmod.leadManagement(driver);
			try {
				fc.utobj().actionImgOption(driver, firstName + " " + lastName, "Move to Opener");
				fc.utobj().throwsSkipException("Move to opener still visible for lead from action menu");
			} catch (Exception e) {
			}
			try {
				fc.utobj().actionImgOption(driver, firstName + " " + lastName, "Move to Info Mgr");
				fc.utobj().throwsSkipException("Move to Info Mgr still visible for lead from action menu");
			} catch (Exception e) {
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales", "sales_failed" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Sales_MoveToOpener_002", testCaseDescription = "Verify Move To Opener From More Action Menu with documents")
	void verifyMoveToOpenerFromMoreActionMenu() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		String firstName = fc.utobj().generateTestData(dataSet.get("FirstName"));
		String lastName = fc.utobj().generateTestData(dataSet.get("LastName"));

		try {
			driver = fc.loginpage().login(driver);

			Sales fsmod = new Sales();
			String userName = "FranConnect Administrator";
			String leadFullName = firstName + " " + lastName;
			String documentTitle = fc.utobj().generateTestData(dataSet.get("DocumentTitle"));
			String franchiseId = fc.utobj().generateTestData(dataSet.get("FranId"));
			String centerName = fc.utobj().generateTestData(dataSet.get("CenterName"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String loginId = fc.utobj().generateTestData(dataSet.get("loginId"));
			String password = "T0n1ght1";
			String confirmPassword = "T0n1ght1";
			String openingDate = fc.utobj().getCurrentDateUSFormat();
			LeadSummaryUI pobj = new LeadSummaryUI(driver);
			AdminAreaRegionAddAreaRegionPageTest p1 = new AdminAreaRegionAddAreaRegionPageTest();
			regionName = p1.addAreaRegion(driver, regionName);
			AdminConfigurationConfigureStoreTypePageTest p2 = new AdminConfigurationConfigureStoreTypePageTest();
			storeType = p2.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Add a lead");

			fc.sales().sales_common().fsModule(driver);
			Map<String, String> leadName = addLeadSummaryWithLeadNameOwnerName(driver, firstName, lastName, userName);

			// fc.utobj().clickElement(driver,fc.utobj().getElementByXpath(driver,".//*contains(text(),'"+leadFullName+"')")));
			fc.utobj().printTestStep("Add documents in document tab");
			fc.utobj().clickElement(driver, pobj.documentsTab);
			fc.utobj().sendKeys(driver, pobj.documentTitle, documentTitle);
			String fileName = fc.utobj().getFilePathFromTestData(dataSet.get("fileName"));
			fc.utobj().sendKeys(driver, pobj.documentAttachment, fileName);
			fc.utobj().clickElement(driver, pobj.saveDoc);
			fc.utobj().printTestStep("Go to Primary Info ");
			fc.utobj().printTestStep("Click on move to Opener from More Actions Menu");

			fc.utobj().selectMoreActionMenu(driver, pobj.moreActionsLink, pobj.menu, "Move To Opener");
			AdminFranchiseLocationAddFranchiseLocationPage pobj1 = new AdminFranchiseLocationAddFranchiseLocationPage(
					driver);
			fc.utobj().printTestStep("Fill location details");
			fc.utobj().sendKeys(driver, pobj1.franchiseeID, franchiseId);
			fc.utobj().selectDropDownByVisibleTextTrimed(driver, pobj1.areaRegion, regionName);
			fc.utobj().sendKeys(driver, pobj1.centerName, centerName);
			fc.utobj().selectDropDownByVisibleTextTrimed(driver, pobj1.storeTypeId, storeType);
			fc.utobj().sendKeys(driver, pobj1.openerOpeningDate, openingDate);
			fc.utobj().sendKeys(driver, pobj1.city, "Test City");
			fc.utobj().selectDropDown(driver, pobj1.stateID, 1);
			fc.utobj().sendKeys(driver, pobj1.storePhone, "8989898989");
			fc.utobj().clickElement(driver, pobj1.submit);

			AdminFranchiseLocationManageFranchiseLocationsAddUserPage pobj2 = new AdminFranchiseLocationManageFranchiseLocationsAddUserPage(
					driver);
			fc.utobj().sendKeys(driver, pobj2.loginId, loginId);
			fc.utobj().sendKeys(driver, pobj2.password, password);
			fc.utobj().sendKeys(driver, pobj2.confirmPassword, confirmPassword);
			// fc.utobj().clickElement(driver, pobj2.rolesBtn);
			// fc.utobj().sendKeys(driver,
			// pobj2.searchRoles,fc.utobj().translateString("Default Franchise
			// Role"));
			fc.utobj().selectValFromMultiSelect(driver, pobj2.rolesId, "Default Franchise Role");
			// fc.utobj().clickElement(driver, pobj2.selectAll);
			fc.utobj().selectDropDown(driver, pobj2.state, 1);
			fc.utobj().sendKeys(driver, pobj2.phone1, "1234567890");
			fc.utobj().sendKeys(driver, pobj2.phoneExt1, "2");
			fc.utobj().sendKeys(driver, pobj2.email, "salesautomation@staffex.com");
			fc.utobj().clickElement(driver, pobj2.submit);

			/*
			 * fc.utobj().sendKeys(driver, pobj2.searchKey, franchiseId);
			 * fc.utobj().clickElement(driver, pobj2.searchBtn);
			 */
			// OpenerSearchPage searchPageOpener=new OpenerSearchPage(driver);
			// fc.utobj().clickElement(driver, searchPageOpener.searchTab);
			OpenerStoreSummaryStoreListPageTest storeSummaryPage = new OpenerStoreSummaryStoreListPageTest();
			storeSummaryPage.setDefaultFilterOpener(driver, franchiseId);

			fc.utobj().clickElement(driver, driver
					.findElement(By.xpath(".//*[@id='siteMainTable']//a[contains(text(),'" + franchiseId + "')]")));
			// fc.utobj().clickElement(driver,
			// fc.utobj().getElementByXpath(driver,".//*[contains(text(),'"+centerName+"')]")));
			fc.utobj().printTestStep("Go to Document checklist of the location");
			fc.utobj().clickElement(driver, pobj2.documentsTab);

			boolean isDocumentFound = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + documentTitle + "')]");
			if (isDocumentFound == false) {
				fc.utobj().throwsException("Document not found on documents Tab");
			}
			fc.sales().sales_common().fsModule(driver);
			fsmod.leadManagement(driver);
			try {
				fc.utobj().actionImgOption(driver, firstName + " " + lastName, "Move to Opener");
				fc.utobj().throwsSkipException("Move to opener still visible for lead from action menu");
			} catch (Exception e) {
			}
			try {
				fc.utobj().actionImgOption(driver, firstName + " " + lastName, "Move to Info Mgr");
				fc.utobj().throwsSkipException("Move to Info Mgr still visible for lead from action menu");
			} catch (Exception e) {
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales", "TC_Sales_MoveToInfoMgr_001" }) // Fixed : Verified // Sales
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-07-19", testCaseId = "TC_Sales_MoveToInfoMgr_001", testCaseDescription = "Verify Move To Info Manager from More Action")
	void verifyMoveToInfoManagerMoreAction() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			// add lead
			String firstName = fc.utobj().generateTestData(dataSet.get("FirstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("LastName"));
			String firstName1 = fc.utobj().generateTestData(dataSet.get("FirstName"));
			String lastName1 = fc.utobj().generateTestData(dataSet.get("LastName"));
			String documentTitle = fc.utobj().generateTestData(dataSet.get("DocumentTitle"));
			String franchiseName = fc.utobj().generateTestData(dataSet.get("franchiseName"));
			String centerName = fc.utobj().generateTestData(dataSet.get("centerName"));
			String areaRegion = fc.utobj().generateTestData(dataSet.get("areaRegion"));
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String loginId = fc.utobj().generateTestData(dataSet.get("loginId"));
			FSSearchPageTest searchPage = new FSSearchPageTest();
			String password = "T0n1ght1";
			String confirmPassword = "T0n1ght1";

			String userName = "FranConnect Administrator";
			String leadFullName = firstName + " " + lastName;
			String leadFullName1 = firstName1 + " " + lastName1;
			Sales fsmod = new Sales();
			LeadSummaryUI pobj = new LeadSummaryUI(driver);
			AdminAreaRegionAddAreaRegionPageTest p1 = new AdminAreaRegionAddAreaRegionPageTest();
			areaRegion = p1.addAreaRegion(driver, areaRegion);
			AdminConfigurationConfigureStoreTypePageTest p2 = new AdminConfigurationConfigureStoreTypePageTest();
			storeType = p2.addStoreType(driver, storeType);
			fc.utobj().printTestStep("Add a lead");

			fc.sales().sales_common().fsModule(driver);
			Map<String, String> leadName = addLeadSummaryWithLeadNameOwnerName(driver, firstName, lastName, userName);
			fc.utobj().printTestStep("Add coapplicant");
			addLeadSummaryWithLeadNameOwnerName(driver, firstName1, lastName1, userName);

			// fsmod.leadManagement(driver);
			searchPage.searchByLeadName(driver, firstName, lastName);
			fc.utobj().actionImgOption(driver, leadFullName, "Associate Co-Applicant");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.coApplicantSearch, leadFullName1);
			fc.utobj().sleep();
			try {
				fc.utobj().clickPartialLinkText(driver, leadFullName1);
			} catch (Exception e) {
				try {
					// fc.utobj().clickElement(driver,
					// fc.utobj().getElementByXpath(driver,".//*[@id='contentdivTopSearch']//u[contains(text(),'"+leadFullName1+"')]")));
					fc.utobj().clickElement(driver, driver
							.findElement(By.xpath(".//*[@id='contentdivTopSearch']/table/tbody/tr[1]/td/a/span/u")));
				} catch (Exception e2) {

				}
			}
			fc.utobj().selectDropDownByPartialText(driver, pobj.coApplicantRelationshipIDDrp, "Relative");
			fc.utobj().clickElement(driver, pobj.addCboxBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().printTestStep("Search Lead");
			searchPage.searchByLeadName(driver, firstName, lastName);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'" + leadFullName + "')]"));
			fc.utobj().printTestStep("Add document");
			fc.utobj().clickElement(driver, pobj.documentsTab);
			fc.utobj().sendKeys(driver, pobj.documentTitle, documentTitle);
			String fileName = fc.utobj().getFilePathFromTestData(dataSet.get("fileName"));
			fc.utobj().sendKeys(driver, pobj.documentAttachment, fileName);
			fc.utobj().clickElement(driver, pobj.saveDoc);
			fc.utobj().printTestStep("Submit personal profile");
			fc.utobj().clickElement(driver, pobj.personalProfileTab);
			FSLeadSummaryQualificationDetailsPage npobj = new FSLeadSummaryQualificationDetailsPage(driver);
			fc.utobj().clickElement(driver, npobj.gender_male);
			fc.utobj().sendKeys(driver, npobj.homeCity, "City");
			fc.utobj().selectDropDownByVisibleText(driver, npobj.homeCountry, "USA");
			fc.utobj().selectDropDownByVisibleText(driver, npobj.homeState, "Alabama");
			fc.utobj().sendKeys(driver, npobj.Email, "salesautomation@staffex.com");
			fc.utobj().clickElement(driver, npobj.homeOwnership_own);
			fc.utobj().clickElement(driver, npobj.maritalStatus_married);
			fc.utobj().sendKeys(driver, npobj.spouseName, "SpouseName");
			fc.utobj().clickElement(driver, npobj.saveButton);
			fc.utobj().printTestStep("Add real estate");
			fc.utobj().clickElement(driver, pobj.realEstate);
			// Real estate
			fc.utobj().sendKeys(driver, npobj.siteAddress, "site Address");
			fc.utobj().sendKeys(driver, npobj.siteCity, "Site City");
			fc.utobj().clickElement(driver, npobj.saveButton);
			fc.utobj().printTestStep("Add compliance");
			fc.utobj().clickElement(driver, pobj.complianceTab);
			// fc.utobj().sendKeys(driver, pobj.documentAttachment, fileName);

			String fddDate = fc.utobj().getFutureDateUSFormat(5);
			String fddRecievedDate = fc.utobj().getFutureDateUSFormat(6);
			String fddRequestedDate = fc.utobj().getFutureDateUSFormat(1);

			fc.utobj().sendKeys(driver, pobj.fddDate, fddDate);
			fc.utobj().sendKeys(driver, pobj.fddRecievedDate, fddRecievedDate);
			fc.utobj().sendKeys(driver, pobj.dateHoldingPeriodRequirementsExpireForFDD, "09/30/2019");
			fc.utobj().clickElement(driver, pobj.francCommiteeApprovalYes);
			fc.utobj().sendKeys(driver, pobj.fddRequestedDate, fddRequestedDate);
			fc.utobj().sendKeys(driver, pobj.franFeeAmt, "1000");
			fc.utobj().clickElement(driver, pobj.francCommiteeApprovalYes);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			// fsmod.leadManagement(driver);
			searchPage.searchByLeadName(driver, firstName, lastName);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[contains(text(),'" + leadFullName + "')]"));
			fc.utobj().printTestStep("Move lead to info mgr from primary info > more action ");
			fc.utobj().selectMoreActionMenu(driver, pobj.moreActionsLink, pobj.menu, "Move To Info Mgr");
			fc.utobj().printTestStep("Submit Location Details > Save");
			fc.utobj().sendKeys(driver, pobj.franchiseeName, franchiseName);
			fc.utobj().sendKeys(driver, pobj.centerName, centerName);
			fc.utobj().selectDropDownByPartialText(driver, pobj.areaRegion, areaRegion);
			fc.utobj().selectDropDownByPartialText(driver, pobj.storeTypeId, storeType);
			fc.utobj().sendKeys(driver, pobj.openingDate, fddDate);
			fc.utobj().selectDropDownByVisibleText(driver, pobj.state, "Alabama");
			fc.utobj().sendKeys(driver, pobj.InfoMgrcity, "City");
			fc.utobj().sendKeys(driver, pobj.storePhone, "1234567898");
			fc.utobj().clickElement(driver, pobj.moveDocuments);
			fc.utobj().clickElement(driver, pobj.addBtn);

			AdminFranchiseLocationManageFranchiseLocationsAddUserPage pobj2 = new AdminFranchiseLocationManageFranchiseLocationsAddUserPage(
					driver);
			fc.utobj().sendKeys(driver, pobj2.loginId, loginId);
			fc.utobj().sendKeys(driver, pobj2.password, password);
			fc.utobj().sendKeys(driver, pobj2.confirmPassword, confirmPassword);
			
			/*fc.utobj().clickElement(driver, pobj2.rolesBtn);
			fc.utobj().sendKeys(driver, pobj2.searchRoles, "Default Franchise Role");
			fc.utobj().clickElement(driver, pobj2.selectAll);*/
			
			fc.utobj().selectValFromMultiSelect(driver, pobj2.rolesId, "Default Franchise Role");
			
			
			fc.utobj().sendKeys(driver, pobj2.email, "salesautomation@staffex.com");
			fc.utobj().clickElement(driver, pobj2.submit);
			//
			// fc.utobj().sendKeys(driver, pobj.searchBar, franchiseName);
			// fc.utobj().clickElement(driver, pobj.searchBtn);
			fc.utobj().clickElement(driver, pobj2.showFilter);
			fc.utobj().selectValFromMultiSelect(driver, pobj2.filterFranchiseeIdDrpDwn, franchiseName);
			fc.utobj().clickElement(driver, pobj2.filterSearchBtn);
			fc.utobj().clickElement(driver, pobj2.showFilter);
			try {
				fc.utobj().clickElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'" + franchiseName + "')]"));
			} catch (Exception e) {
				fc.utobj().clickPartialLinkText(driver, franchiseName);
			}
			fc.utobj().clickElement(driver, pobj.infoMgrDocumentsTab);
			boolean verifyDocumentPersonalProfile = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text(),'Personal Profile')]");
			if (verifyDocumentPersonalProfile == false) {
				fc.utobj().throwsSkipException("Document tab Personal Profile details not visible");
			}
			boolean verifyDocumentQualification = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text(),'" + documentTitle + "')]");
			if (verifyDocumentQualification == false) {
				fc.utobj().throwsSkipException("Document tab Personal Profile details not visible");
			}
			fc.utobj().clickElement(driver, pobj.ownersTab);
			boolean verifyAdditionalContact = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text(),'" + leadFullName1 + "')]");
			if (verifyAdditionalContact == false) {
				fc.utobj().throwsSkipException("Additional contact not verified at Owners tab");
			}
			fc.utobj().clickElement(driver, pobj.realEstateTab);
			boolean verifyRealState = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + "Site City" + "')]");
			if (verifyRealState == false) {
				fc.utobj().throwsSkipException("Real State data not verified at real state tab");
			}
			boolean verifyRealState1 = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + "site Address" + "')]");
			if (verifyRealState1 == false) {
				fc.utobj().throwsSkipException("Real State data not verified at real state tab");
			}

			fc.utobj().clickElement(driver, pobj.contractSigning);
			boolean verifyContratSingning = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + fddDate + "')]");
			boolean verifyContratSingning1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text(),'" + fddRecievedDate + "')]");
			// boolean verifyContratSingning2=fc.utobj().verifyCase(driver,
			// ".//*[contains(text(),'"+fddRequestedDate+"')]");
			if (verifyContratSingning == false && verifyContratSingning1 == false) {
				fc.utobj().throwsSkipException("compliance data not verified at contract singning tab");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			// e.printStackTrace();
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesank", "sales1" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Sales_MoveToInfoMgr_002", testCaseDescription = "Verify Move to Info Mgr from primary info bottom button")
	void verifyMoveToInfoManagerBottomButton() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			// add lead
			String firstName = fc.utobj().generateTestData(dataSet.get("FirstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("LastName"));
			String firstName1 = fc.utobj().generateTestData(dataSet.get("FirstName"));
			String lastName1 = fc.utobj().generateTestData(dataSet.get("LastName"));
			String documentTitle = fc.utobj().generateTestData(dataSet.get("DocumentTitle"));
			String franchiseName = fc.utobj().generateTestData(dataSet.get("franchiseName"));
			String centerName = fc.utobj().generateTestData(dataSet.get("centerName"));
			String areaRegion = fc.utobj().generateTestData(dataSet.get("areaRegion"));
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String loginId = fc.utobj().generateTestData(dataSet.get("loginId"));
			String password = "T0n1ght1";
			String confirmPassword = "T0n1ght1";

			String userName = "FranConnect Administrator";
			String leadFullName = firstName + " " + lastName;
			String leadFullName1 = firstName1 + " " + lastName1;
			Sales fsmod = new Sales();
			LeadSummaryUI pobj = new LeadSummaryUI(driver);
			AdminAreaRegionAddAreaRegionPageTest p1 = new AdminAreaRegionAddAreaRegionPageTest();
			areaRegion = p1.addAreaRegion(driver, areaRegion);
			AdminConfigurationConfigureStoreTypePageTest p2 = new AdminConfigurationConfigureStoreTypePageTest();
			storeType = p2.addStoreType(driver, storeType);
			fc.utobj().printTestStep("Add a lead");

			Map<String, String> leadName = addLeadSummaryWithLeadNameOwnerName(driver, firstName, lastName, userName);
			fc.utobj().printTestStep("Add coapplicant");
			addLeadSummaryWithLeadNameOwnerName(driver, firstName1, lastName1, userName);

			fsmod.leadManagement(driver);
			fc.utobj().actionImgOption(driver, leadFullName, "Associate Co-Applicant");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.coApplicantSearch, leadFullName1);
			//
			try {
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[@id='contentdivTopSearch']/table/tbody/tr[1]/td/a/span/u"));

			} catch (Exception e) {
				try {
					fc.utobj().clickElement(driver, driver.findElement(
							By.xpath(".//*[@id='contentdivTopSearch']//u[contains(text(),'" + leadFullName1 + "')]")));
				} catch (Exception e2) {

				}
			}
			fc.utobj().selectDropDownByPartialText(driver, pobj.coApplicantRelationshipIDDrp, "Relative");
			fc.utobj().clickElement(driver, pobj.addCboxBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[contains(text(),'" + leadFullName + "')]"));
			fc.utobj().printTestStep("Add document");
			fc.utobj().clickElement(driver, pobj.documentsTab);
			fc.utobj().sendKeys(driver, pobj.documentTitle, documentTitle);
			String fileName = fc.utobj().getFilePathFromTestData(dataSet.get("fileName"));
			fc.utobj().sendKeys(driver, pobj.documentAttachment, fileName);
			fc.utobj().clickElement(driver, pobj.saveDoc);
			fc.utobj().printTestStep("Submit personal profile");
			fc.utobj().clickElement(driver, pobj.personalProfileTab);
			FSLeadSummaryQualificationDetailsPage npobj = new FSLeadSummaryQualificationDetailsPage(driver);
			fc.utobj().clickElement(driver, npobj.gender_male);
			fc.utobj().sendKeys(driver, npobj.homeCity, "City");
			fc.utobj().selectDropDownByVisibleText(driver, npobj.homeCountry, "USA");
			fc.utobj().selectDropDownByVisibleText(driver, npobj.homeState, "Alabama");
			fc.utobj().sendKeys(driver, npobj.Email, "salesautomation@staffex.com");
			fc.utobj().clickElement(driver, npobj.homeOwnership_own);
			fc.utobj().clickElement(driver, npobj.maritalStatus_married);
			fc.utobj().sendKeys(driver, npobj.spouseName, "SpouseName");
			fc.utobj().clickElement(driver, npobj.saveButton);
			fc.utobj().printTestStep("Add real estate");
			fc.utobj().clickElement(driver, pobj.realEstate);

			// Real estate
			fc.utobj().sendKeys(driver, npobj.siteAddress, "site Address");
			fc.utobj().sendKeys(driver, npobj.siteCity, "Site City");
			fc.utobj().clickElement(driver, npobj.saveButton);
			fc.utobj().printTestStep("Add compliance");
			fc.utobj().clickElement(driver, pobj.complianceTab);
			// fc.utobj().sendKeys(driver, pobj.documentAttachment, fileName);

			String fddDate = fc.utobj().getFutureDateUSFormat(5);
			String fddRecievedDate = fc.utobj().getFutureDateUSFormat(6);
			String fddRequestedDate = fc.utobj().getFutureDateUSFormat(1);

			fc.utobj().sendKeys(driver, pobj.fddDate, fddDate);
			fc.utobj().sendKeys(driver, pobj.fddRecievedDate, fddRecievedDate);
			fc.utobj().clickElement(driver, pobj.francCommiteeApprovalYes);
			fc.utobj().sendKeys(driver, pobj.fddRequestedDate, fddRequestedDate);
			fc.utobj().sendKeys(driver, pobj.franFeeAmt, "1000");
			fc.utobj().clickElement(driver, pobj.francCommiteeApprovalYes);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			// fsmod.leadManagement(driver);
			// fc.utobj().clickElement(driver,
			// fc.utobj().getElementByXpath(driver,".//*[contains(text(),'"+leadFullName+"')]")));
			// fc.utobj().selectMoreActionMenu(driver,pobj.moreActionsLink,
			// pobj.menu, "Move To Info Mgr");
			fc.utobj().clickElement(driver, pobj.leadPrimaryInfo);
			fc.utobj()
					.printTestStep("Move lead to info mgr from primary info > move to info mgr through bottom button");
			fc.utobj().clickElement(driver, pobj.moveToInfoMgr);
			fc.utobj().sendKeys(driver, pobj.franchiseeName, franchiseName);
			fc.utobj().sendKeys(driver, pobj.centerName, centerName);
			fc.utobj().selectDropDownByPartialText(driver, pobj.areaRegion, areaRegion);
			fc.utobj().selectDropDownByPartialText(driver, pobj.storeTypeId, storeType);
			fc.utobj().sendKeys(driver, pobj.openingDate, fddDate);
			fc.utobj().selectDropDownByVisibleText(driver, pobj.state, "Alabama");
			fc.utobj().sendKeys(driver, pobj.InfoMgrcity, "City");
			fc.utobj().sendKeys(driver, pobj.storePhone, "1234567898");
			fc.utobj().clickElement(driver, pobj.moveDocuments);
			fc.utobj().clickElement(driver, pobj.addBtn);

			AdminFranchiseLocationManageFranchiseLocationsAddUserPage pobj2 = new AdminFranchiseLocationManageFranchiseLocationsAddUserPage(
					driver);
			fc.utobj().printTestStep("Submit Location Details > Save");
			fc.utobj().sendKeys(driver, pobj2.loginId, loginId);
			fc.utobj().sendKeys(driver, pobj2.password, password);
			fc.utobj().sendKeys(driver, pobj2.confirmPassword, confirmPassword);
			
			/*fc.utobj().clickElement(driver, pobj2.rolesBtn);
			fc.utobj().sendKeys(driver, pobj2.searchRoles, "Default Franchise Role");
			fc.utobj().clickElement(driver, pobj2.selectAll);*/
			
			fc.utobj().selectValFromMultiSelect(driver, pobj2.rolesId, "Default Franchise Role");
			
			fc.utobj().sendKeys(driver, pobj2.email, "salesautomation@staffex.com");
			fc.utobj().clickElement(driver, pobj2.submit);

			fc.utobj().sendKeys(driver, pobj.searchBar, franchiseName);
			fc.utobj().clickElement(driver, pobj.searchBtn);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[contains(text(),'" + franchiseName + "')]"));
			fc.utobj().clickElement(driver, pobj.infoMgrDocumentsTab);
			boolean verifyDocumentPersonalProfile = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text(),'Personal Profile')]");
			if (verifyDocumentPersonalProfile == false) {
				fc.utobj().throwsSkipException("Document tab Personal Profile details not visible");
			}
			boolean verifyDocumentQualification = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text(),'" + documentTitle + "')]");
			if (verifyDocumentQualification == false) {
				fc.utobj().throwsSkipException("Document tab Personal Profile details not visible");
			}
			fc.utobj().clickElement(driver, pobj.ownersTab);
			boolean verifyAdditionalContact = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text(),'" + leadFullName1 + "')]");
			if (verifyAdditionalContact == false) {
				fc.utobj().throwsSkipException("Additional contact not verified at Owners tab");
			}
			fc.utobj().clickElement(driver, pobj.realEstateTab);
			boolean verifyRealState = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + "Site City" + "')]");
			if (verifyRealState == false) {
				fc.utobj().throwsSkipException("Real State data not verified at real state tab");
			}
			boolean verifyRealState1 = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + "site Address" + "')]");
			if (verifyRealState1 == false) {
				fc.utobj().throwsSkipException("Real State data not verified at real state tab");
			}

			fc.utobj().clickElement(driver, pobj.contractSigning);
			boolean verifyContratSingning = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + fddDate + "')]");
			boolean verifyContratSingning1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text(),'" + fddRecievedDate + "')]");
			// boolean verifyContratSingning2=fc.utobj().verifyCase(driver,
			// ".//*[contains(text(),'"+fddRequestedDate+"')]");
			if (verifyContratSingning == false && verifyContratSingning1 == false) {
				fc.utobj().throwsSkipException("compliance data not verified at contract singning tab");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesank", "sales1" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Sales_MoveToInfoMgr_003", testCaseDescription = "Verify Move to Info Manager from Summary Page")
	void verifyMoveToInfoManagerFromSummaryPage() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			// add lead
			String firstName = fc.utobj().generateTestData(dataSet.get("FirstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("LastName"));
			String firstName1 = fc.utobj().generateTestData(dataSet.get("FirstName"));
			String lastName1 = fc.utobj().generateTestData(dataSet.get("LastName"));
			String documentTitle = fc.utobj().generateTestData(dataSet.get("DocumentTitle"));
			String franchiseName = fc.utobj().generateTestData(dataSet.get("franchiseName"));
			String centerName = fc.utobj().generateTestData(dataSet.get("centerName"));
			String areaRegion = fc.utobj().generateTestData(dataSet.get("areaRegion"));
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String loginId = fc.utobj().generateTestData(dataSet.get("loginId"));
			String password = "T0n1ght1";
			String confirmPassword = "T0n1ght1";

			String userName = "FranConnect Administrator";
			String leadFullName = firstName + " " + lastName;
			String leadFullName1 = firstName1 + " " + lastName1;
			Sales fsmod = new Sales();
			LeadSummaryUI pobj = new LeadSummaryUI(driver);
			AdminAreaRegionAddAreaRegionPageTest p1 = new AdminAreaRegionAddAreaRegionPageTest();
			areaRegion = p1.addAreaRegion(driver, areaRegion);
			AdminConfigurationConfigureStoreTypePageTest p2 = new AdminConfigurationConfigureStoreTypePageTest();
			storeType = p2.addStoreType(driver, storeType);
			fc.utobj().printTestStep("Add a lead");
			addLeadSummaryWithLeadNameOwnerName(driver, firstName, lastName, userName);
			fc.utobj().printTestStep("Add coapplicant");
			addLeadSummaryWithLeadNameOwnerName(driver, firstName1, lastName1, userName);

			fsmod.leadManagement(driver);
			fc.utobj().actionImgOption(driver, leadFullName, "Associate Co-Applicant");
			fc.sales();
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.coApplicantSearch, leadFullName1);
			//
			try {
				fc.utobj().clickEnterOnElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[@id='contentdivTopSearch']/table/tbody/tr[1]/td/a/span/u"));

			} catch (Exception e) {
				try {
					fc.utobj().clickElement(driver,
							fc.utobj().getElementByXpath(driver, ".//*[contains(text(),'" + leadFullName1 + "')]"));
				} catch (Exception e2) {

				}
			}
			fc.utobj().selectDropDownByPartialText(driver, pobj.coApplicantRelationshipIDDrp, "Relative");
			fc.utobj().clickElement(driver, pobj.addCboxBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[contains(text(),'" + leadFullName + "')]"));
			fc.utobj().printTestStep("Add document");
			fc.utobj().clickElement(driver, pobj.documentsTab);
			fc.utobj().sendKeys(driver, pobj.documentTitle, documentTitle);
			String fileName = fc.utobj().getFilePathFromTestData(dataSet.get("fileName"));
			fc.utobj().sendKeys(driver, pobj.documentAttachment, fileName);
			fc.utobj().clickElement(driver, pobj.saveDoc);
			fc.utobj().printTestStep("Submit personal profile");
			fc.utobj().clickElement(driver, pobj.personalProfileTab);
			FSLeadSummaryQualificationDetailsPage npobj = new FSLeadSummaryQualificationDetailsPage(driver);
			fc.utobj().clickElement(driver, npobj.gender_male);
			fc.utobj().sendKeys(driver, npobj.homeCity, "City");
			fc.utobj().selectDropDownByVisibleText(driver, npobj.homeCountry, "USA");
			fc.utobj().selectDropDownByVisibleText(driver, npobj.homeState, "Alabama");
			fc.utobj().sendKeys(driver, npobj.Email, "salesautomation@staffex.com");
			fc.utobj().clickElement(driver, npobj.homeOwnership_own);
			fc.utobj().clickElement(driver, npobj.maritalStatus_married);
			fc.utobj().sendKeys(driver, npobj.spouseName, "SpouseName");
			fc.utobj().clickElement(driver, npobj.saveButton);
			fc.utobj().printTestStep("Add real estate");
			fc.utobj().clickElement(driver, pobj.realEstate);

			// Real estate
			fc.utobj().sendKeys(driver, npobj.siteAddress, "site Address");
			fc.utobj().sendKeys(driver, npobj.siteCity, "Site City");
			fc.utobj().clickElement(driver, npobj.saveButton);
			fc.utobj().printTestStep("Add compliance");
			fc.utobj().clickElement(driver, pobj.complianceTab);
			// fc.utobj().sendKeys(driver, pobj.documentAttachment, fileName);

			String fddDate = fc.utobj().getFutureDateUSFormat(5);
			String fddRecievedDate = fc.utobj().getFutureDateUSFormat(6);
			String fddRequestedDate = fc.utobj().getFutureDateUSFormat(1);

			fc.utobj().sendKeys(driver, pobj.fddDate, fddDate);
			fc.utobj().sendKeys(driver, pobj.fddRecievedDate, fddRecievedDate);
			fc.utobj().clickElement(driver, pobj.francCommiteeApprovalYes);
			fc.utobj().sendKeys(driver, pobj.fddRequestedDate, fddRequestedDate);
			fc.utobj().sendKeys(driver, pobj.franFeeAmt, "1000");
			fc.utobj().clickElement(driver, pobj.francCommiteeApprovalYes);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fsmod.leadManagement(driver);
			// fc.utobj().clickElement(driver,
			// fc.utobj().getElementByXpath(driver,".//*[contains(text(),'"+leadFullName+"')]")));
			// fc.utobj().selectMoreActionMenu(driver,pobj.moreActionsLink,
			// pobj.menu, "Move To Info Mgr");
			/*
			 * fc.utobj().clickElement(driver, pobj.leadPrimaryInfo);
			 * fc.utobj().clickElement(driver, pobj.moveToInfoMgr);
			 */
			fc.utobj().printTestStep("Move lead to info mgr from primary info > move to info mgr from summary page");
			fc.utobj().actionImgOption(driver, leadFullName, "Move to Info Mgr");
			fc.utobj().sendKeys(driver, pobj.franchiseeName, franchiseName);
			fc.utobj().sendKeys(driver, pobj.centerName, centerName);
			fc.utobj().selectDropDownByPartialText(driver, pobj.areaRegion, areaRegion);
			fc.utobj().selectDropDownByPartialText(driver, pobj.storeTypeId, storeType);
			fc.utobj().sendKeys(driver, pobj.openingDate, fddDate);
			fc.utobj().selectDropDownByVisibleText(driver, pobj.state, "Alabama");
			fc.utobj().sendKeys(driver, pobj.InfoMgrcity, "City");
			fc.utobj().sendKeys(driver, pobj.storePhone, "1234567898");
			fc.utobj().clickElement(driver, pobj.moveDocuments);
			fc.utobj().clickElement(driver, pobj.addBtn);

			AdminFranchiseLocationManageFranchiseLocationsAddUserPage pobj2 = new AdminFranchiseLocationManageFranchiseLocationsAddUserPage(
					driver);
			fc.utobj().printTestStep("Submit Location Details > Save");
			fc.utobj().sendKeys(driver, pobj2.loginId, loginId);
			fc.utobj().sendKeys(driver, pobj2.password, password);
			fc.utobj().sendKeys(driver, pobj2.confirmPassword, confirmPassword);
			
			/*fc.utobj().clickElement(driver, pobj2.rolesBtn);
			fc.utobj().sendKeys(driver, pobj2.searchRoles, "Default Franchise Role");
			fc.utobj().clickElement(driver, pobj2.selectAll);*/
			
			fc.utobj().selectValFromMultiSelect(driver, pobj2.rolesId, "Default Franchise Role");
			
			fc.utobj().sendKeys(driver, pobj2.email, "salesautomation@staffex.com");
			fc.utobj().clickElement(driver, pobj2.submit);

			fc.utobj().sendKeys(driver, pobj.searchBar, franchiseName);
			fc.utobj().clickElement(driver, pobj.searchBtn);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[contains(text(),'" + franchiseName + "')]"));
			fc.utobj().clickElement(driver, pobj.infoMgrDocumentsTab);
			boolean verifyDocumentPersonalProfile = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text(),'Personal Profile')]");
			if (verifyDocumentPersonalProfile == false) {
				fc.utobj().throwsSkipException("Document tab Personal Profile details not visible");
			}
			boolean verifyDocumentQualification = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text(),'" + documentTitle + "')]");
			if (verifyDocumentQualification == false) {
				fc.utobj().throwsSkipException("Document tab Personal Profile details not visible");
			}
			fc.utobj().clickElement(driver, pobj.ownersTab);
			boolean verifyAdditionalContact = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text(),'" + leadFullName1 + "')]");
			if (verifyAdditionalContact == false) {
				fc.utobj().throwsSkipException("Additional contact not verified at Owners tab");
			}
			fc.utobj().clickElement(driver, pobj.realEstateTab);
			boolean verifyRealState = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + "Site City" + "')]");
			if (verifyRealState == false) {
				fc.utobj().throwsSkipException("Real State data not verified at real state tab");
			}
			boolean verifyRealState1 = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + "site Address" + "')]");
			if (verifyRealState1 == false) {
				fc.utobj().throwsSkipException("Real State data not verified at real state tab");
			}

			fc.utobj().clickElement(driver, pobj.contractSigning);
			boolean verifyContratSingning = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + fddDate + "')]");
			boolean verifyContratSingning1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text(),'" + fddRecievedDate + "')]");
			// boolean verifyContratSingning2=fc.utobj().verifyCase(driver,
			// ".//*[contains(text(),'"+fddRequestedDate+"')]");
			if (verifyContratSingning == false && verifyContratSingning1 == false) {
				fc.utobj().throwsSkipException("compliance data not verified at contract singning tab");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales", "sales_failed" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-13", testCaseId = "TC_Sales_LogATask", testCaseDescription = "Verify Log A task, Modify and delete functionality")
	void verifySalesLogATask() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			// add lead
			String firstName = fc.utobj().generateTestData(dataSet.get("FirstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("LastName"));

			// String userName="FranConnect Administrator";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String corpUserName = fc.utobj().generateTestData("corpUserName");
			String emailId = "salesautomation@staffex.com";
			fc.utobj().printTestStep("Add Corporate User");
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = corpTest.createDefaultUser(driver, corpUser);
			String leadFullName = firstName + " " + lastName;
			Sales fsmod = new Sales();
			LeadSummaryUI pobj = new LeadSummaryUI(driver);
			fc.utobj().printTestStep("Add a lead");
			fc.sales().sales_common().fsModule(driver);
			Map<String, String> leadName = addLeadSummaryWithLeadNameOwnerName(driver, firstName, lastName,
					corpUser.getuserFullName());
			fc.utobj().printTestStep("Go to summary page");
			fsmod.leadManagement(driver);
			/*
			 * FSSearchPageTest p2 = new FSSearchPageTest();
			 * fc.utobj().printTestStep(testCaseId, "Search Lead");
			 * p2.searchByLeadName(driver,firstName,lastName);
			 */
			fc.utobj().printTestStep("Search the lead");
			/*
			 * fc.utobj().sendKeys(driver, pobj.leadSearch, leadFullName);
			 * fc.utobj().clickElement(driver, pobj.leadSearchBtn);
			 */
			FSSearchPageTest p3 = new FSSearchPageTest();
			p3.searchByLeadName(driver, firstName, lastName);
			fc.utobj().printTestStep("Right Action Icon > Click on Log A task");
			fc.utobj().actionImgOption(driver, leadFullName, "Log a Task");

			// fc.utobj().actionImgOption(driver, firstName+" "+lastName, "Log a
			// Task");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String taskSubject = "Task Subject";
			taskSubject = fc.utobj().generateTestData(taskSubject);
			fc.utobj().printTestStep("Add Task Details");
			fc.utobj().sendKeys(driver, pobj.taskSubject, taskSubject);
			fc.utobj().clickElement(driver, pobj.addTaskBtn);

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'" + leadFullName + "')]"));

			fc.utobj().switchFrameById(driver, "leadOpenActivitesSummary");

			boolean isTaskAdded = fc.utobj().assertPageSource(driver, taskSubject);

			if (isTaskAdded == false) {
				fc.utobj().throwsException("Task Not Added Successfully");
			}
			fc.utobj().switchFrameToDefault(driver);
			fsmod.tasks(driver);
			try {
				fc.utobj().sendKeys(driver, pobj.emailSubject, taskSubject);
			} catch (Exception e) {
				fc.utobj().clickElement(driver, pobj.showFilter);
				fc.utobj().sendKeys(driver, pobj.emailSubject, taskSubject);
			}
			fc.utobj().clickElement(driver, pobj.searchFilterBtn);
			boolean isTaskPresentatTaskTab = fc.utobj().assertLinkPartialText(driver, taskSubject);
			boolean isLeadPresentatTaskTab = fc.utobj().assertLinkPartialText(driver, leadFullName);
			if (isTaskPresentatTaskTab == false || isLeadPresentatTaskTab == false) {
				fc.utobj().throwsException("Task Not present at task tab");
			}
			fc.utobj().printTestStep("Verifying task Email");
			String expectedSubject = "New Task Added";
			String expectedMessageBody = taskSubject;

			// String emailId="salesautomation@staffex.com";

			Map<String, String> mailData = fc.utobj().readMailBox(expectedSubject, expectedMessageBody, emailId,
					"sdg@1a@Hfs");
			System.out.println(mailData);
			if (!mailData.get("mailBody").contains(taskSubject)) {
				fc.utobj().throwsSkipException("was not able to verify task email body sent to the lead owner.");
			}
			if (!mailData.get("subject").contains(expectedSubject)) {
				fc.utobj().throwsSkipException("was not able to verify email subject sent to the lead owner.");
			}

			fsmod.tasks(driver);
			/*
			 * fc.utobj().printTestStep(testCaseId, "Modify the task details");
			 * fc.utobj().clickElement(driver,pobj.viewPerPage);
			 * fc.utobj().clickElement(driver,fc.utobj().getElementByXpath(
			 * driver,
			 * ".//select[@id='resultsPerPage']/option[contains(text(),'200')]")
			 * ));
			 */
			// fc.utobj().clickElement(driver, pobj.showFilter);
			try {
				fc.utobj().sendKeys(driver, pobj.emailSubject, taskSubject);
			} catch (Exception e) {
				fc.utobj().clickElement(driver, pobj.showFilter);
				fc.utobj().sendKeys(driver, pobj.emailSubject, taskSubject);
			}
			fc.utobj().clickElement(driver, pobj.searchFilterBtn);
			fc.utobj().actionImgOption(driver, taskSubject, "Modify");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String newTaskSubject = "New Task Subject";
			newTaskSubject = fc.utobj().generateTestData(newTaskSubject);
			fc.utobj().sendKeys(driver, pobj.taskSubject, newTaskSubject);
			fc.utobj().clickElement(driver, pobj.addTaskBtn);

			try {
				fc.utobj().sendKeys(driver, pobj.emailSubject, newTaskSubject);
			} catch (Exception e) {
				fc.utobj().clickElement(driver, pobj.showFilter);
				fc.utobj().sendKeys(driver, pobj.emailSubject, newTaskSubject);
			}
			fc.utobj().clickElement(driver, pobj.searchFilterBtn);

			boolean isTaskPresentatTaskTab1 = fc.utobj().assertLinkPartialText(driver, newTaskSubject);
			boolean isLeadPresentatTaskTab1 = fc.utobj().assertLinkPartialText(driver, leadFullName);
			if (isTaskPresentatTaskTab1 == false && isLeadPresentatTaskTab1) {
				fc.utobj().throwsException("Task modifiacton not verified");
			}
			/*
			 * fsmod.tasks(driver); try { fc.utobj().sendKeys(driver,
			 * pobj.emailSubject, newTaskSubject); } catch (Exception e) {
			 * fc.utobj().clickElement(driver, pobj.showFilter);
			 * fc.utobj().sendKeys(driver, pobj.emailSubject, newTaskSubject); }
			 * fc.utobj().clickElement(driver, pobj.searchFilterBtn);
			 */

			fc.utobj().printTestStep("Delete the task");
			fc.utobj().actionImgOption(driver, newTaskSubject, "Delete");
			fc.utobj().acceptAlertBox(driver);
			fc.utobj().sendKeys(driver, pobj.emailSubject, newTaskSubject);
			fc.utobj().clickElement(driver, pobj.searchFilterBtn);
			isTaskPresentatTaskTab1 = fc.utobj().assertLinkPartialText(driver, newTaskSubject);
			if (isTaskPresentatTaskTab1 == true) {
				fc.utobj().throwsException("Task deletion not verified");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales", "TC_Sales_DeleteLead_001" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Sales_DeleteLead_001", testCaseDescription = "Verify delete lead functionality of sales")
	void verifySalesDeletedLead() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			// add lead
			String firstName = fc.utobj().generateTestData(dataSet.get("FirstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("LastName"));

			String userName = "FranConnect Administrator";
			String leadFullName = firstName + " " + lastName;
			Sales fsmod = new Sales();
			fc.utobj().printTestStep("Add a lead");

			Map<String, String> leadName = addLeadSummaryWithLeadNameOwnerName(driver, firstName, lastName, userName);
			fc.utobj().printTestStep("Go to lead summary page");
			fsmod.leadManagement(driver);
			fc.utobj().printTestStep("Click on right action ,Delete lead");
			fc.utobj().actionImgOption(driver, leadFullName, "Delete");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//input[@name='Ok']"));
			fc.utobj().switchFrameToDefault(driver);
			boolean isLeadPresent = fc.utobj().assertLinkPartialText(driver, leadFullName);
			if (isLeadPresent == true) {
				fc.utobj().throwsException("Lead not getting deleted");
			}
			fc.utobj().printTestStep("Restore the lead from deleted logs	");
			AdminPageTest p4 = new AdminPageTest();
			p4.openDeletedLogsLnk(driver);

			fc.utobj().switchFrameById(driver, "reportiframe");
			isLeadPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text(),'" + leadFullName + "')]");
			if (isLeadPresent == false) {
				fc.utobj().throwsException("Deleted lead not found in Deleted logs");
			}
			fc.utobj().actionImgOption(driver, leadFullName, "Restore");
			fc.utobj().acceptAlertBox(driver);
			fc.utobj().switchFrameToDefault(driver);
			fc.sales().sales_common().fsModule(driver);
			// fsmod.leadManagement(driver);
			FSSearchPageTest p3 = new FSSearchPageTest();
			p3.searchByLeadName(driver, firstName, lastName);
			isLeadPresent = fc.utobj().assertLinkPartialText(driver, leadFullName);
			if (isLeadPresent == false) {
				fc.utobj().throwsException("Lead not getting restored from deleted logs");
			}
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[contains(text(),'" + leadFullName + "')]"));
			FSLeadSummaryPrimaryInfoPageTest pobj1 = new FSLeadSummaryPrimaryInfoPageTest();
			driver = pobj1.gotoActivityHistorySection(driver);
			String deletionDate = fc.utobj().currentDate();
			boolean isRemarkPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text () ,'Lead was deleted on " + deletionDate + "')]");
			if (isRemarkPresent == false) {
				fc.utobj().throwsException("Deletion remarks not present at lead acticity history");
			}
			isRemarkPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text () ,'Lead was restored on " + deletionDate + "')]");
			if (isRemarkPresent == false) {
				fc.utobj().throwsException("Restoration remarks not present at lead acticity history");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Sales_Search_001", testCaseDescription = "Verify Sales search lead functionality")
	void verifySalesSearch() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			// add lead
			String firstName = fc.utobj().generateTestData(dataSet.get("FirstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("LastName"));
			String firstName1 = fc.utobj().generateTestData(dataSet.get("FirstName"));
			String lastName1 = fc.utobj().generateTestData(dataSet.get("LastName"));
			LeadSummaryUI pobj = new LeadSummaryUI(driver);
			String userName = "FranConnect Administrator";
			String leadFullName = firstName + " " + lastName;
			Sales fsmod = new Sales();
			fc.utobj().printTestStep("Add a lead");

			Map<String, String> leadName = addLeadSummaryWithLeadNameOwnerName(driver, firstName, lastName, userName);
			// fc.utobj().selectDropDownByVisibleText(driver,
			// pobj.leadStatusIDDrp, "Closed Lead ");

			FSSearchPageTest p3 = new FSSearchPageTest();
			p3.searchByLeadName(driver, firstName, lastName);
			fc.utobj().clickElement(driver, pobj.checkBoxAll);
			fc.utobj().printTestStep("Change status of the leads through button");

			fc.utobj().clickElement(driver, pobj.changeStatusBottomBtn);

			String windowHandle = driver.getWindowHandle();
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().selectDropDownByVisibleText(driver, pobj.leadStatusIDDrp, "Closed Lead");
			fc.utobj().clickElement(driver, pobj.changeStatusCboxBtn);
			fc.utobj().clickElement(driver, pobj.cboxCloseBtn);
			driver.switchTo().window(windowHandle);

			// FsModulePageTest p1 = new FsModulePageTest(driver);
			fsmod.groups(driver);
			FSGroupsPage pobj1 = new FSGroupsPage(driver);

			String groupName = fc.utobj().generateTestData("GroupName");
			fc.utobj().clickElement(driver, pobj1.addGroupsBtn);

			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			fc.utobj().sendKeys(driver, pobj1.groupName, groupName);
			fc.utobj().sendKeys(driver, pobj1.groupDescription, groupName + " Description");
			fc.utobj().clickElement(driver, pobj1.addBtn);
			fc.utobj().switchFrameToDefault(driver);
			fsmod.leadManagement(driver);
			p3.searchByLeadName(driver, firstName, lastName);
			fc.utobj().clickElement(driver, pobj.checkBoxAll);
			fc.utobj().printTestStep("Add to Group from bottom button");
			fc.utobj().clickElement(driver, pobj.addToGroupBottomBtn);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					(".//td[contains(text(),'" + groupName + "')]/ancestor::tr/td/input[@name='groups']")));
			fc.utobj().clickElement(driver, pobj.addToGroupCboxBtn);
			fc.utobj().clickElement(driver, pobj.okCboxBtn);
			fsmod.search(driver);

			SearchUI p2 = new SearchUI(driver);
			fc.utobj().sendKeys(driver, p2.firstName, firstName);
			fc.utobj().sendKeys(driver, p2.lastName, lastName);
			fc.utobj().clickElement(driver, p2.submit);
			boolean isLeadPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text(),'" + firstName + " " + lastName + "')]");
			if (isLeadPresent == false) {
				fc.utobj().throwsException("Lead not found in search");
			}
			addLeadSummaryWithLeadNameOwnerName(driver, firstName1, lastName1, userName);
			fc.utobj().printTestStep("Go to groups tab");
			fsmod.groups(driver);
			String groupName1 = fc.utobj().generateTestData("GroupName");
			fc.utobj().clickElement(driver, pobj1.addGroupsBtn);

			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);

			fc.utobj().sendKeys(driver, pobj1.groupName, groupName1);
			fc.utobj().sendKeys(driver, pobj1.groupDescription, groupName1 + " Description");

			fc.utobj().clickElement(driver, pobj1.addBtn);

			fc.utobj().switchFrameToDefault(driver);
			FSGroupsPageTest npoj = new FSGroupsPageTest();
			fc.utobj().printTestStep("Associate with leads");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[contains(text () , '" + groupName + "')]/ancestor::tr/td/div/div[@id='detailActionMenu']"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'" + groupName
					+ "')]/ancestor::td/following-sibling::td/div/ul/li/a[contains(text(),'Associate with Leads')]"));
			fc.utobj().printTestStep("Search window will popup ");
			fc.utobj().printTestStep("Fill all the details");
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			fc.utobj().sendKeys(driver, p2.firstName, firstName1);
			fc.utobj().sendKeys(driver, p2.lastName, lastName1);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//input[@name='submitButton']"));

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//input[@value='Yes']"));

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//input[@value='Close']"));
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'" + groupName
					+ "')]/ancestor::tr/td[@width='16%']/a[contains(text(),'2')]"));

			isLeadPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text(),'" + firstName1 + " " + lastName1 + "')]");
			if (isLeadPresent == false) {
				fc.utobj().throwsException("Lead not Prresent");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesfail", "sales1" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-06", testCaseId = "TC_Sales_ConfigureHeatIndexElement_001", testCaseDescription = "Verify Heat Index of lead")
	void verifyHeatIndexElement() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			String firstName = fc.utobj().generateTestData(dataSet.get("FirstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("LastName"));
			String callSubject = fc.utobj().generateTestData(dataSet.get("CallSubject"));
			String lastName1 = fc.utobj().generateTestData(dataSet.get("LastName"));
			String userName = "FranConnect Administrator";
			LeadSummaryUI pobj = new LeadSummaryUI(driver);
			fc.utobj().printTestStep("Go to Admin > Sales > Configure Heat Index Element");
			fc.adminpage().adminPage(driver);
			fc.utobj().clickLink(driver, "Configure Heat Meter");
			fc.utobj().printTestStep("Modify");
			fc.utobj().clickElement(driver, pobj.modifyBtn);
			fc.utobj().printTestStep(
					"Configure score for Phone Calls, Marketing Emails, Virtual Brochure, Qualification Form, FDD Received, Qualification Met, Franchise Agreement Signed, Discovery Day Visit");
			fc.utobj().doubleClickElement(driver, pobj.isPhoneEnable);
			fc.utobj().sendKeys(driver, pobj.phoneMaxScore, "50");
			fc.utobj().sendKeys(driver, pobj.phoneOutboundCall, "10");
			fc.utobj().sendKeys(driver, pobj.phoneInboundCall, "20");

			fc.utobj().doubleClickElement(driver, pobj.isEmailEnable);
			fc.utobj().sendKeys(driver, pobj.marketingMaxScore, "50");
			fc.utobj().sendKeys(driver, pobj.marketingOpenRead, "10");
			fc.utobj().sendKeys(driver, pobj.marketingLinkClicked, "20");

			fc.utobj().doubleClickElement(driver, pobj.isBrochureEnable);
			fc.utobj().sendKeys(driver, pobj.brochureMaxscore, "50");
			fc.utobj().sendKeys(driver, pobj.brochureSectionVisited, "10");

			fc.utobj().doubleClickElement(driver, pobj.isDetailedQualificationEnable);
			fc.utobj().sendKeys(driver, pobj.formCompletionScore, "10");
			fc.utobj().sendKeys(driver, pobj.BckChckApprvlScore, "5");
			fc.utobj().sendKeys(driver, pobj.under199999, "1");
			fc.utobj().sendKeys(driver, pobj.under399999, "2");
			fc.utobj().sendKeys(driver, pobj.under599999, "3");
			fc.utobj().sendKeys(driver, pobj.under600000, "4");

			fc.utobj().sendKeys(driver, pobj.networth499999, "1");
			fc.utobj().sendKeys(driver, pobj.networth2499999, "2");
			fc.utobj().sendKeys(driver, pobj.networth4999999, "3");
			fc.utobj().sendKeys(driver, pobj.networth5000000, "4");

			fc.utobj().doubleClickElement(driver, pobj.isFddReceipt);
			fc.utobj().sendKeys(driver, pobj.fddRecievedScore, "20");

			fc.utobj().doubleClickElement(driver, pobj.isFinancingApproved);
			fc.utobj().sendKeys(driver, pobj.qualificationMetScore, "20");

			fc.utobj().doubleClickElement(driver, pobj.isAgreementSigned);
			fc.utobj().sendKeys(driver, pobj.agreementSignedScore, "10");

			fc.utobj().doubleClickElement(driver, pobj.isDiscoveryDayVisit);
			fc.utobj().sendKeys(driver, pobj.discoveryDayVisitScore, "10");
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().printTestStep("Add a Lead");
			Map<String, String> leadName = addLeadSummaryWithLeadNameOwnerName(driver, firstName, lastName, userName);
			fc.utobj().printTestStep("Now perform log a call");
			// log a call
			fc.utobj().clickElement(driver, pobj.LogACallLnk);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.callSubject, callSubject);
			fc.utobj().clickElement(driver, pobj.addCallBtn);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//input[@value='No']"));

			fc.utobj().clickElement(driver, pobj.heatMeter);
			boolean heatIndexForLogCall = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[@id='hMeter']//div[contains(text(),'Heat Index')]/u[contains(text(),'4')]");
			if (heatIndexForLogCall == false) {
				fc.utobj().throwsException("Heat index is not increasing on logging a call");
			}

			fc.utobj().printTestStep("send campaign emails");
			CRMCampaignCenterCampaignsPage pobj2 = new CRMCampaignCenterCampaignsPage(driver);
			FSCampaignCenterPage campaignCenterObj = new FSCampaignCenterPage(driver);
			Sales fsmod = new Sales();
			fsmod.emailCampaignTab(driver);
			// fc.utobj().clickElement(driver, pobj2.dashBoradLnk);
			// fc.utobj().clickElement(driver, pobj2.campaignLnk);

			fc.utobj().clickElement(driver, campaignCenterObj.createCampaign);

			// WebElement frame
			// =fc.CommonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);

			String campaignName = fc.utobj().generateTestData("campaignName");
			String camapignDescription = fc.utobj().generateTestData(campaignName);
			String accessibility = "Public";
			fc.utobj().sendKeys(driver, campaignCenterObj.campaignName, campaignName);
			fc.utobj().sendKeys(driver, campaignCenterObj.description, camapignDescription);
			fc.utobj().selectDropDown(driver, campaignCenterObj.campaignAccessibility, accessibility);
			// fc.utobj().selectDropDown(driver, pobj2.selectCategory,
			// categoryName);
			// fc.utobj().clickElement(driver, pobj2.statusDrivenTypeCampaign);
			fc.utobj().clickElement(driver, pobj2.startBtn);
			fc.utobj().switchFrameToDefault(driver);

			// Code Your Template
			String templateName = fc.utobj().generateTestData("templateName");
			String emailSubject = fc.utobj().generateTestData("emailSubject");
			String plainTextEditor = fc.utobj().generateTestData("plainTextEditor");

			fc.utobj().printTestStep("Code Your Own Template");

			fc.utobj().clickElement(driver, campaignCenterObj.codeYourOwn);
			fc.utobj().sendKeys(driver, campaignCenterObj.templateName, templateName);
			fc.utobj().sendKeys(driver, campaignCenterObj.mailSubject, emailSubject);

			fc.utobj().clickElement(driver, campaignCenterObj.plainTextEmailType);
			fc.utobj().sendKeys(driver, campaignCenterObj.textAreaPlainText, plainTextEditor);

			fc.utobj().clickElement(driver, campaignCenterObj.saveAndContinue);
			fc.utobj().clickElement(driver, campaignCenterObj.associateLater);
			fc.utobj().clickElement(driver, campaignCenterObj.finalizeAndLaunchCampaignBtn);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[contains(text(),'" + firstName + " " + lastName + "')]"));
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'Associate Campaign')]"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text(),'" + campaignName + "')]/ancestor::tr/td/input[@name='campaignradio']"));
			fc.utobj().clickElement(driver, pobj2.nextBtn);
			fc.utobj().clickElement(driver, pobj2.confirmBtn);

			// send email
			/*
			 * fc.utobj().clickElement(driver, pobj.sendEmailLnk);
			 * fc.utobj().sendKeys(driver, pobj.subject, "Test Email Subject" );
			 * String windowHandle = driver.getWindowHandle();
			 * driver.switchTo().frame(pobj.htmlFrame);
			 * fc.utobj().getElement(driver,pobj.mailText).clear();
			 * fc.utobj().sendKeys(driver,pobj.mailText,
			 * "This is test Email Body");
			 * driver.switchTo().window(windowHandle);
			 * fc.utobj().clickElement(driver,pobj.saveBtn);
			 */

			// adding Virtual Brochure template
			fsmod.emailCampaignTab(driver);
			// fc.utobj().clickElement(driver, pobj2.dashBoradLnk);
			// fc.utobj().clickElement(driver, pobj2.campaignLnk);

			fc.utobj().clickElement(driver, campaignCenterObj.createCampaign);
			// WebElement frame
			// =fc.CommonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);

			String campaignName1 = fc.utobj().generateTestData("campaignName");
			String camapignDescription1 = fc.utobj().generateTestData(campaignName1);
			fc.utobj().sendKeys(driver, campaignCenterObj.campaignName, campaignName1);
			fc.utobj().sendKeys(driver, campaignCenterObj.description, camapignDescription1);
			fc.utobj().selectDropDown(driver, campaignCenterObj.campaignAccessibility, accessibility);
			// fc.utobj().selectDropDown(driver, pobj2.selectCategory,
			// categoryName);
			// fc.utobj().clickElement(driver, pobj2.statusDrivenTypeCampaign);
			fc.utobj().clickElement(driver, campaignCenterObj.startBtn);
			fc.utobj().switchFrameToDefault(driver);

			// Code Your Template
			String templateName1 = fc.utobj().generateTestData("templateName");
			String emailSubject1 = fc.utobj().generateTestData("emailSubject");
			String plainTextEditor1 = fc.utobj().generateTestData("plainTextEditor");

			fc.utobj().printTestStep("Code Your Own Template");

			fc.utobj().clickElement(driver, campaignCenterObj.codeYourOwn);
			fc.utobj().sendKeys(driver, campaignCenterObj.templateName, templateName1);
			fc.utobj().sendKeys(driver, campaignCenterObj.mailSubject, emailSubject1);

			fc.utobj().clickElement(driver, campaignCenterObj.plainTextEmailType);
			fc.utobj().sendKeys(driver, campaignCenterObj.textAreaPlainText, plainTextEditor1);

			fc.utobj().clickElement(driver, campaignCenterObj.saveBtn);

			fsmod.leadManagement(driver);
			fc.utobj().printTestStep("send virtual brochure email");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[contains(text(),'" + firstName + " " + lastName + "')]"));
			fc.utobj().selectMoreActionMenu(driver, pobj.moreActionsLink, pobj.menu, "Send Virtual Brochure Email");
			fc.utobj().sendKeys(driver, pobj.subject, "Test Virtual Brochure Email Subject");
			String windowHandle1 = driver.getWindowHandle();
			driver.switchTo().frame(fc.utobj().getElement(driver, pobj.htmlFrame));
			fc.utobj().getElement(driver, pobj.mailText).clear();
			fc.utobj().sendKeys(driver, pobj.mailText,
					"This is test Virtual Brochure Email Body  $VIRTUAL_BROCHURE_WEB_PAGE$ $VIRTUAL_BROCHURE_USER_ID$ $VIRTUAL_BROCHURE_PASSWORD$");
			driver.switchTo().window(windowHandle1);
			fc.utobj().clickElement(driver, pobj.saveBtn);

			// fill qualification form
			fc.utobj().printTestStep("fill qualification form");
			FSLeadSummaryQualificationDetailsPage npobj = new FSLeadSummaryQualificationDetailsPage(driver);

			fc.utobj().selectDropDownByVisibleText(driver, npobj.currentNetLiquidWorth, "Over $5000000");
			fc.utobj().selectDropDownByVisibleText(driver, npobj.cashAvailableforInvestment, "Over $600000");
			fc.utobj().selectDropDownByVisibleText(driver, npobj.investmentTimeframe, "Under 1 Month");
			fc.utobj().selectDropDownByVisibleText(driver, npobj.employmentbackground, "Blue Chip professional");

			/*
			 * fc.utobj().clickElement(driver, npobj.gender_male);
			 * fc.utobj().sendKeys(driver, npobj.homeCity, "City");
			 * fc.utobj().selectDropDownByVisibleText(driver, npobj.homeCountry,
			 * "USA"); fc.utobj().selectDropDownByVisibleText(driver,
			 * npobj.homeState, "Alabama"); fc.utobj().sendKeys(driver,
			 * npobj.Email, "salesautomation@staffex.com");
			 * fc.utobj().clickElement(driver, npobj.homeOwnership_own);
			 * fc.utobj().clickElement(driver, npobj.maritalStatus_married);
			 * fc.utobj().sendKeys(driver, npobj.spouseName, "SpouseName");
			 * fc.utobj().sendKeys(driver, npobj.totalNetworth, "599999.00");
			 */
			fc.utobj().clickElement(driver, npobj.saveButton);
			fc.utobj().printTestStep("Send FDD and download the fdd");
			fc.utobj().printTestStep("Item 23 Receipt configure");
			fc.adminpage().item23ReceiptSummaryLnk(driver);
			AdminFDDManagementITEM23RECEIPTSummaryPage npobj1 = new AdminFDDManagementITEM23RECEIPTSummaryPage(driver);

			fc.utobj().clickElement(driver, npobj1.addItem23ReceiptBtn);
			AdminFDDManagementITEM23RECEIPTSummaryAddITEM23RECEIPTPage pobj6 = new AdminFDDManagementITEM23RECEIPTSummaryAddITEM23RECEIPTPage(
					driver);
			String item23Title = fc.utobj().generateTestData(dataSet.get("item23Title"));
			fc.utobj().sendKeys(driver, pobj6.templateTitle, item23Title);

			String content = "$BROKER_NAME$ \n$BROKER_AGENCY$ \n$FRANCHISE_SELLER_NAME$ \n$BUSINESS_NAME$ \n$ADDRESS$ \n$STATE$ \n $COUNTRY$ \n$PHONE$ \n$DATE$";
			fc.utobj().sendKeys(driver, pobj6.item23ReceiptTextArea, content);
			fc.utobj().sendKeys(driver, pobj6.businessName, "FranConnect Inc");
			fc.utobj().sendKeys(driver, pobj6.addressTxt, "B-5, Park Avenue");
			fc.utobj().selectDropDown(driver, pobj6.countryDrp, "USA");

			fc.utobj().selectDropDown(driver, pobj6.stateDrp, "Alabama");
			fc.utobj().sendKeys(driver, pobj6.phone, "9998887777");
			fc.utobj().clickElement(driver, pobj6.previewBtn);
			fc.utobj().clickElement(driver, pobj6.saveBtn);

			fc.adminpage().adminPage(driver);
			fc.utobj().clickLink(driver, "FDD Email Template summary");
			String mailTitle = fc.utobj().generateTestData("FDD Email Template Summary Title");
			String mailSubject = fc.utobj().generateTestData("FDD Email Template Summary");
			AdminFDDManagementFDDEmailTemplateSummaryAddTemplatePage pobj3 = new AdminFDDManagementFDDEmailTemplateSummaryAddTemplatePage(
					driver);
			fc.utobj().clickElement(driver, pobj3.addTemplateBtn);
			fc.utobj().clickElement(driver, pobj3.graphicalRadio);
			fc.utobj().sendKeys(driver, pobj3.mailTitle, mailTitle);
			fc.utobj().sendKeys(driver, pobj3.mailSubject, mailSubject);
			String windowHandle2 = driver.getWindowHandle();
			driver.switchTo().frame(fc.utobj().getElement(driver, pobj3.htmlFrame));
			fc.utobj().sendKeys(driver, pobj3.mailText, "Dear $FIRST_NAME$ ,"

					+ "We are pleased to send you a copy of our Franchise Disclosure Document (FDD) , Set forth below is a Username, Password and Website information for accessing and downloading our Franchise Disclosure Document."

					+ "The FDD is an important document and provides you with some basic information about our company, the franchise that we offer, and our franchise system Please carefully read the entire FDD (including all of the exhibits)."

					+ "The FDD is in PDF format. You will need a copy of Adobe Reader. You probably already have this on your computer, but in case you don't, You can download a free version of Adobe Acrobat Reader by going to www.adobe.com.)"

					+ "Username: $USERID$"

					+ "Password: $PASSWORD$"

					+ "Website: $URL_TO_DOWNLOAD_FDD$"

					+ "When you open the FDD, please click on the link on the first page. That page is the receipt, which just confirms that you have received the document."

					+ "Alternatively, you can receive the FDD (a) as an Adobe Portable Document Format (PDF) File on a CD ROM disc, (b) as a PDF attachment to an e-mail message, or (c) on paper as a two-sided hardcopy. If you prefer to have the FDD delivered in one of these other ways, please reply to this e-mail indicating which method you prefer."

					+ "If you choose to download the PDF or receive it via email, you will need internet access and a valid e-mail account, in addition to Adobe Acrobat Reader. You will also need access to a printer to print the Receipt exhibit, and also to print a paper copy for yourself."

					+ "The FDD is confidential, subject to the mutual confidentiality agreement you have signed, and contains proprietary information, including trade secrets. Neither the FDD nor any of the information contained in the FDD may be reproduced or disclosed to any person under any circumstances without our written permission."

					+ "$SENDER_NAME$");

			driver.switchTo().window(windowHandle2);

			fc.utobj().clickElement(driver, pobj3.submitBtn);
			fc.utobj().printTestStep("Fill the parameters required for Qualification Met");
			AdminFDDManagementFDDManagementPage pobj4 = new AdminFDDManagementFDDManagementPage(driver);
			String fddName = fc.utobj().generateTestData(dataSet.get("fddName"));
			String fddVersion = fc.utobj().generateTestData(dataSet.get("fddVersion"));
			fc.adminpage().fddManagementLnk(driver);
			fc.utobj().clickElement(driver, pobj4.uploadFDDbtn);
			fc.utobj().sendKeys(driver, pobj4.fddName, fddName);
			fc.utobj().sendKeys(driver, pobj4.fddVersion, fddVersion);
			fc.utobj().sendKeys(driver, pobj4.fddDate, "03/10/2021");
			fc.utobj().selectDropDownByPartialText(driver, pobj4.fddItem23ReceiptSales, item23Title);
			fc.utobj().doubleClickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='ufocAction_countryID']/option[contains(text(),'USA')]"));
			fc.utobj().selectDropDownByPartialText(driver, pobj4.fddItem23ReceiptInfoMgr, item23Title);
			// String divisionNAme=fc.utobj().getSelectedDropDownValue(driver,
			// fc.utobj().getElementByXpath(driver,".//*[@id='ufocAction_brandID']/option[2]")));
			// fc.utobj().selectDropDownByPartialText(driver, pobj4.fddDivision,
			// divisionNAme);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='countryID1_0']"));
			try {
				fc.utobj().acceptAlertBox(driver);
			} catch (Exception e) {
			}
			String fileName = fc.utobj().getFilePathFromTestData(dataSet.get("fileName"));
			fc.utobj().sendKeys(driver, pobj4.fddUploadFile, fileName);
			// fc.utobj().sendKeys(driver,pobj4.fddComments, "Comments");
			fc.utobj().clickElement(driver, pobj4.fddAddFdd);
			// add lead
			String defaultSeller = fc.utobj().generateTestData(dataSet.get("defaultSeller"));

			fc.utobj().printTestStep("Add a lead");

			// Map<String,String> leadName =
			// addLeadSummaryWithLeadNameOwnerName(driver,config,firstName,lastName,userName);
			// fc.utobj().clickElement(driver, pobj.moreActionsBtn);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[contains(text(),'" + firstName + " " + lastName + "')]"));
			fc.utobj().clickElement(driver, pobj.sendFddMailBtn);

			try {
				fc.utobj().selectDropDown(driver, pobj.fddEmailTemplate, mailTitle);
			} catch (Exception e) {
			}
			fc.utobj().sendKeys(driver, pobj.defaultSellerBtn, defaultSeller);
			// fc.utobj().clickElement(driver, pobj.fddAdditionalContact);
			fc.utobj().clickElement(driver, pobj.sendbtn);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			// driver.switchTo().frame(1);
			fc.utobj().clickElement(driver, pobj.cnfrmbtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, pobj.complianceTab);
			// fc.utobj().sendKeys(driver, pobj.documentAttachment, fileName);

			String fddDate = fc.utobj().getCurrentDateUSFormat();
			// String fddRecievedDate=fc.utobj().getFutureDateUSFormat(6);
			// String fddRequestedDate=fc.utobj().getFutureDateUSFormat(1);

			fc.utobj().sendKeys(driver, pobj.agreementSignedDate, fddDate);
			/*
			 * fc.utobj().sendKeys(driver,
			 * pobj.fddRecievedDate,fddRecievedDate);
			 * fc.utobj().clickElement(driver,pobj.francCommiteeApprovalYes);
			 * fc.utobj().sendKeys(driver,
			 * pobj.fddRequestedDate,fddRequestedDate);
			 * fc.utobj().sendKeys(driver, pobj.franFeeAmt,"1000");
			 * fc.utobj().clickElement(driver,pobj.francCommiteeApprovalYes);
			 */
			fc.utobj().clickElement(driver, pobj.saveBtn);

			/*
			 * fc.utobj().clickElement(driver, pobj.primaryInfo);
			 * fc.utobj().sendKeys(driver, pobj.currentNetWorth,"1600000");
			 * fc.utobj().sendKeys(driver,
			 * pobj.CashAvailableforInvestment,"700000");
			 * fc.utobj().clickElement(driver, pobj.saveBtn);
			 */
			fc.utobj().printTestStep("fill visit tab");
			fc.utobj().clickElement(driver, pobj.visitTab);
			fc.utobj().sendKeys(driver, pobj.scheduledDate, fddDate);
			fc.utobj().sendKeys(driver, pobj.visitorDate, fddDate);
			fc.utobj().sendKeys(driver, pobj.visitorName, "Visitor Name");
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().clickElement(driver, pobj.primaryInfo);
			fc.utobj().clickElement(driver, pobj.leadModify);
			fc.utobj().selectDropDownByPartialText(driver, pobj.leadSourceCatagory, "Referred By");
			fc.utobj().selectDropDownByPartialText(driver, pobj.leadSourceCatagory, "Friend");
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().clickElement(driver, pobj.heatMeter);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales", "sales_failed" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Sales_FDD_VerifyCancel_001", testCaseDescription = "Verify Cancel button of Fdd send functionality")
	void verifySalesFddCancel() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Go to Sales > FDD > Send FDD > Search > Select a lead > Send FDD");
			fc.sales().sales_common().fsModule(driver);
			Sales fsmod = new Sales();
			LeadSummaryUI pobj = new LeadSummaryUI(driver);
			String firstName = fc.utobj().generateTestData(dataSet.get("FirstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("LastName"));
			String userName = "FranConnect Administrator";
			addLeadSummaryWithLeadNameOwnerName(driver, firstName, lastName, userName);

			fsmod.fdd(driver);
			fc.utobj().clickElement(driver, pobj.sendFddMailBtn);
			fc.utobj().sendKeys(driver, pobj.firstName, firstName);
			fc.utobj().sendKeys(driver, pobj.lastName, lastName);
			fc.utobj().clickElement(driver, pobj.addBtn);
			// fc.utobj().sendKeys(driver, pobj.fddSearch, firstName+"
			// "+lastName);
			// fc.utobj().clickElement(driver,
			// fc.utobj().getElementByXpath(driver,".//*[contains(text(),'"+firstName+"
			// "+lastName+"')]")));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//input[@value='" + firstName + " " + lastName + "']/ancestor::td/input[@name='checkbox']"));
			fc.utobj().clickElement(driver, pobj.sendFddBtn);
			fc.utobj().printTestStep("Click on 'Cancel'");
			fc.utobj().clickElement(driver, pobj.fddcancelBtn);
			// fsmod.leadManagement(driver);
			boolean isLeadManagementPage = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text(),'" + firstName + " " + lastName + "')]");
			if (isLeadManagementPage == true) {
				fc.utobj().throwsException("Was not redirected at Lead management page");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesfail", "sales1" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-06", testCaseId = "TC_Sales_SendFDDToAdditionalContact_001", testCaseDescription = "Validate fdd login and download for Additional Contact")
	void verifySendFDDToAdditionalContact() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.adminpage().adminPage(driver);

			fc.utobj().clickLink(driver, "Configure Email sent Prior to FDD Email");
			AdminFDDmanagementConfigureEmailsentPriortoFDDEmailPage pobj = new AdminFDDmanagementConfigureEmailsentPriortoFDDEmailPage(
					driver);

			/*
			 * $NEXT_LINE$
			 */

			fc.utobj().clickElement(driver, pobj.sendEmailNotificationYes);
			fc.utobj().sendKeys(driver, pobj.mailSubject, "Shortly you will receive the FDD mail.");
			fc.utobj().sendKeys(driver, pobj.timeInterval, "0");
			String windowHandle = driver.getWindowHandle();
			driver.switchTo().frame(fc.utobj().getElement(driver, pobj.htmlFrame));
			// fc.utobj().getElement(driver,pobj.mailText).clear();

			String mailBody = "$FIRST_NAME_33_1$ $LAST_NAME_33_1$ $ADDRESS1_33_1$  $ADDRESS2_33_1$ $CITY_33_1$ $COUNTRY_33_1$ $STATE_PROVINCE_33_1$ $CENTER_NAME_1_1$"
					+ "$STREET_ADDRESS_1_1$ $CITY_1_1$ $STATE_PROVINCE_1_1$ $ZIP_POSTAL_CODE_1_1$ $EMAIL_1_1$ $FAX_1_1$ $PHONE_1_1$ $MOBILE_1_1$ $FIRST_NAME_1_2$ $LAST_NAME_1_2$"
					+ "$OWNERS_FIRST_NAME$ $OWNERS_LAST_NAME$ $OWNERS_TITLE$ $OWNERS_ADDRESS$ $OWNERS_CITY$ $OWNERS_STATE$  $OWNERS_ZIP$ $OWNERS_EMAIL$  $OWNERS_PHONE$ "
					+ "$OWNERS_PHONE_EXTENSION$ $OWNERS_MOBILE$ $OWNERS_FAX$ $OWNER_SIGNATURE$ $SENDER_NAME$ 111111";
			// mailBody = mailBody + fc.utobj().generateTestData(mailBody);

			fc.utobj().sendKeys(driver, pobj.mailText, mailBody);
			driver.switchTo().window(windowHandle);
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.adminpage().item23ReceiptSummaryLnk(driver);
			AdminFDDManagementITEM23RECEIPTSummaryPage npobj = new AdminFDDManagementITEM23RECEIPTSummaryPage(driver);

			fc.utobj().clickElement(driver, npobj.addItem23ReceiptBtn);
			AdminFDDManagementITEM23RECEIPTSummaryAddITEM23RECEIPTPage pobj2 = new AdminFDDManagementITEM23RECEIPTSummaryAddITEM23RECEIPTPage(
					driver);
			String item23Title = fc.utobj().generateTestData(dataSet.get("item23Title"));
			fc.utobj().sendKeys(driver, pobj2.templateTitle, item23Title);

			String content = "$BROKER_NAME$ \n$BROKER_AGENCY$ \n$FRANCHISE_SELLER_NAME$ \n$BUSINESS_NAME$ \n$ADDRESS$ \n$STATE$ \n $COUNTRY$ \n$PHONE$ \n$DATE$";
			fc.utobj().sendKeys(driver, pobj2.item23ReceiptTextArea, content);
			fc.utobj().sendKeys(driver, pobj2.businessName, "FranConnect Inc");
			fc.utobj().sendKeys(driver, pobj2.addressTxt, "B-5, Park Avenue");
			fc.utobj().selectDropDown(driver, pobj2.countryDrp, "USA");
			//
			fc.utobj().selectDropDown(driver, pobj2.stateDrp, "Alaska");
			fc.utobj().sendKeys(driver, pobj2.phone, "9998887777");
			fc.utobj().clickElement(driver, pobj2.previewBtn);
			fc.utobj().clickElement(driver, pobj2.saveBtn);

			fc.adminpage().logOnCredentialsDurationLnk(driver);
			AdminFDDManagementLogonCredentialsDurationPage nnpobj = new AdminFDDManagementLogonCredentialsDurationPage(
					driver);
			fc.utobj().selectDropDownByPartialText(driver, nnpobj.durationDrpDwn, "9 Days");
			fc.utobj().clickElement(driver, nnpobj.saveBtn);

			fc.adminpage().adminPage(driver);
			fc.utobj().clickLink(driver, "FDD Email Template summary");
			String mailTitle = fc.utobj().generateTestData("FDD Email Template Summary Title");
			String mailSubject = fc.utobj().generateTestData("FDD Email Template Summary");
			AdminFDDManagementFDDEmailTemplateSummaryAddTemplatePage pobj3 = new AdminFDDManagementFDDEmailTemplateSummaryAddTemplatePage(
					driver);
			fc.utobj().clickElement(driver, pobj3.addTemplateBtn);
			fc.utobj().sendKeys(driver, pobj3.mailTitle, mailTitle);
			fc.utobj().sendKeys(driver, pobj3.mailSubject, mailSubject + fc.utobj().generateRandomNumber());
			fc.utobj().clickElement(driver, pobj3.graphicalRadio);
			String windowHandle2 = driver.getWindowHandle();
			driver.switchTo().frame(fc.utobj().getElement(driver, pobj3.htmlFrame));
			fc.utobj().sendKeys(driver, pobj3.mailText, "Dear $FIRST_NAME$ ,"

					+ "We are pleased to send you a copy of our Franchise Disclosure Document (FDD) , Set forth below is a Username, Password and Website information for accessing and downloading our Franchise Disclosure Document."

					+ "The FDD is an important document and provides you with some basic information about our company, the franchise that we offer, and our franchise system Please carefully read the entire FDD (including all of the exhibits)."

					+ "The FDD is in PDF format. You will need a copy of Adobe Reader. You probably already have this on your computer, but in case you don't, You can download a free version of Adobe Acrobat Reader by going to www.adobe.com.)"

					+ "Username: $USERID$"

					+ "Password: $PASSWORD$"

					+ "Website: $URL_TO_DOWNLOAD_FDD$"

					+ "When you open the FDD, please click on the link on the first page. That page is the receipt, which just confirms that you have received the document."

					+ "Alternatively, you can receive the FDD (a) as an Adobe Portable Document Format (PDF) File on a CD ROM disc, (b) as a PDF attachment to an e-mail message, or (c) on paper as a two-sided hardcopy. If you prefer to have the FDD delivered in one of these other ways, please reply to this e-mail indicating which method you prefer."

					+ "If you choose to download the PDF or receive it via email, you will need internet access and a valid e-mail account, in addition to Adobe Acrobat Reader. You will also need access to a printer to print the Receipt exhibit, and also to print a paper copy for yourself."

					+ "The FDD is confidential, subject to the mutual confidentiality agreement you have signed, and contains proprietary information, including trade secrets. Neither the FDD nor any of the information contained in the FDD may be reproduced or disclosed to any person under any circumstances without our written permission."

					+ "$SENDER_NAME$");

			driver.switchTo().window(windowHandle2);

			fc.utobj().clickElement(driver, pobj3.submitBtn);

			AdminFDDManagementFDDManagementPage pobj4 = new AdminFDDManagementFDDManagementPage(driver);
			String fddName = fc.utobj().generateTestData(dataSet.get("fddName"));
			String fddVersion = fc.utobj().generateTestData(dataSet.get("fddVersion"));
			fc.adminpage().fddManagementLnk(driver);
			fc.utobj().clickElement(driver, pobj4.uploadFDDbtn);
			fc.utobj().sendKeys(driver, pobj4.fddName, fddName);
			fc.utobj().sendKeys(driver, pobj4.fddVersion, fddVersion);
			// fc.utobj().sendKeys(driver,pobj4.fddDate, "03/10/2021");
			fc.utobj().sendKeys(driver, pobj4.fddDate, "03/10/2021");
			fc.utobj().selectDropDownByPartialText(driver, pobj4.fddItem23ReceiptSales, item23Title);
			fc.utobj().doubleClickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='ufocAction_countryID']/option[contains(text(),'USA')]"));
			fc.utobj().selectDropDownByPartialText(driver, pobj4.fddItem23ReceiptInfoMgr, item23Title);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='countryID1_0']"));
			try {
				fc.utobj().acceptAlertBox(driver);
			} catch (Exception e) {
			}
			// add lead
			String fileName = fc.utobj().getFilePathFromTestData(dataSet.get("fileName"));
			fc.utobj().sendKeys(driver, pobj4.fddUploadFile, fileName);
			// fc.utobj().sendKeys(driver,pobj4.fddComments, "Comments");
			fc.utobj().clickElement(driver, pobj4.fddAddFdd);
			String firstName = fc.utobj().generateTestData(dataSet.get("FirstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("LastName"));
			String firstName1 = fc.utobj().generateTestData(dataSet.get("FirstName"));
			String lastName1 = fc.utobj().generateTestData(dataSet.get("LastName"));
			String defaultSeller = fc.utobj().generateTestData(dataSet.get("defaultSeller"));
			String userName = "FranConnect Administrator";

			String leadFullName = firstName + " " + lastName;
			String leadFullName1 = firstName1 + " " + lastName1;
			LeadSummaryUI pobj5 = new LeadSummaryUI(driver);
			Sales fsmod = new Sales();
			fc.utobj().printTestStep("Add a lead");

			// Map<String,String> leadName =
			// addLeadSummaryWithLeadNameOwnerName(driver,config,firstName,lastName,userName);

			// Map<String,String> dataSet = fc.utobj().readTestData("sales",
			// testCaseId);
			fsmod.leadManagement(driver);
			clickAddLeadLink(driver);
			String email = "salesautomation@staffex.com";

			FSLeadSummaryAddLeadPage pobj6 = new FSLeadSummaryAddLeadPage(driver);

			fc.utobj().sendKeys(driver, pobj6.firstName, firstName);
			fc.utobj().sendKeys(driver, pobj6.lastName, lastName);
			fc.utobj().selectDropDownByVisibleText(driver, pobj6.country, "USA");

			fc.utobj().selectDropDownByVisibleText(driver, pobj6.state, "Alabama");
			fc.utobj().sendKeys(driver, pobj6.emailID, email);
			fc.utobj().sendKeys(driver, pobj6.zip, "123456");
			fc.utobj().sendKeys(driver, pobj6.phone, "12345678");
			fc.utobj().sendKeys(driver, pobj6.address, "New Highway Road");
			fc.utobj().selectDropDownByVisibleText(driver, pobj6.leadOwnerID, userName);
			fc.utobj().selectDropDownByVisibleText(driver, pobj6.leadSource2ID, "Friends");

			fc.utobj().selectDropDownByVisibleText(driver, pobj6.leadSource3ID, "Friends");

			fc.utobj().clickElement(driver, pobj6.save);

			addLeadSummaryWithLeadNameOwnerName(driver, firstName1, lastName1, userName);

			fsmod.leadManagement(driver);

			try {
				// fc.utobj().actionImgOption(driver, leadFullName, " Associate
				// Co-Applicant ");
				fc.utobj().actionImgOption(driver, leadFullName, "Associate Co-Applicant");

			} catch (Exception e) {
				fc.utobj().clickElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'" + firstName + "')]"));
				fc.utobj().selectMoreActionMenu(driver, pobj5.moreActionsLink, pobj5.menu, "Associate Co-Applicant");
			}
			// fc.utobj().clickElement(driver,
			// fc.utobj().getElementByXpath(driver,".//*[@id='containsImage']//a[contains(text(),'"+leadFullName+"')]/ancestor::tr/td/input[@name='checkbox']")));
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj5.coApplicantSearch, leadFullName1);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='contentdivTopSearch']//span/u"));
			fc.utobj().selectDropDownByPartialText(driver, pobj5.coApplicantRelationshipIDDrp, "Relative");
			fc.utobj().clickElement(driver, pobj5.addCboxBtn);
			fc.utobj().clickElement(driver, pobj5.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[contains(text(),'" + firstName + " " + lastName + "')]"));
			// fc.utobj().clickElement(driver, pobj5.moreActionsBtn);
			fc.utobj().clickElement(driver, pobj5.sendFddMailBtn);

			fc.utobj().selectDropDownByPartialText(driver, pobj5.fddEmailTemplate, mailTitle);
			fc.utobj().sendKeys(driver, pobj5.defaultSellerBtn, defaultSeller);
			fc.utobj().clickElement(driver, pobj5.fddAdditionalContact);
			fc.utobj().clickElement(driver, pobj5.sendbtn);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			fc.utobj().clickElement(driver, pobj5.cnfrmbtn);
			fc.utobj().switchFrameToDefault(driver);
			FSLeadSummaryPrimaryInfoPageTest pobj1 = new FSLeadSummaryPrimaryInfoPageTest();
			driver = pobj1.gotoActivityHistorySection(driver);

			fc.utobj().clickElement(driver, driver
					.findElement(By.xpath(".//*[@id='printReady']//span[contains(text(),'" + mailSubject + "')]")));
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			boolean isLeadNamePresent = fc.utobj().assertPageSource(driver, leadFullName);
			boolean isSubjectPresent = fc.utobj().assertPageSource(driver, mailSubject);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//input[@value='Close']"));
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, pobj5.candidatePortalBtn);
			fc.utobj().clickElement(driver, pobj5.showleadDetailsBtn);
			String fddpassword = getCandidatePortal_FDD_Password(driver);

			String fddurl = config.get("buildUrl").concat("welcome.jsp");
			String signature = firstName;
			String signatureName = firstName;
			WebDriver driver_welcome = null;
			try {
				if (config.get("browserName").equalsIgnoreCase("chrome")) {

					System.setProperty("webdriver.chrome.driver",
							config.get("inputDirectory") + "\\exe\\chromedriver.exe");
					ChromeOptions options = new ChromeOptions();
					options.addArguments("disable-popup-blocking");
					DesiredCapabilities capabilities = DesiredCapabilities.chrome();
					capabilities.setCapability(ChromeOptions.CAPABILITY, options);
					LoggingPreferences logPrefs = new LoggingPreferences();
					logPrefs.enable(LogType.BROWSER, Level.ALL);
					capabilities.setCapability(CapabilityType.LOGGING_PREFS, logPrefs);
					driver_welcome = new ChromeDriver(options);

					driver_welcome.manage().window().maximize();
					driver_welcome.get(fddurl);

					fc.utobj().sendKeys(driver_welcome, fc.utobj().getElementByXpath(driver, ".//*[@id='Username']"),
							"salesautomation@staffex.com");
					fc.utobj().sendKeys(driver_welcome, fc.utobj().getElementByXpath(driver, ".//*[@id='Password']"),
							fddpassword);
					fc.utobj().clickElement(driver_welcome, fc.utobj().getElementByXpath(driver, ".//*[@id='ulogin']"));
					fc.utobj().sendKeys(driver_welcome, pobj5.candidatePortalSignature, "/" + signature + "/");
					fc.utobj().sendKeys(driver_welcome, pobj5.candidatePortalSignatureName, signatureName);
					fc.utobj().clickElement(driver, pobj5.submitRemarks);
					fc.utobj().clickElement(driver_welcome, pobj5.agreeChkBox);
					fc.utobj().clickElement(driver_welcome, pobj5.downloadFddBtn);

					ArrayList<String> tabs2 = new ArrayList<String>(driver_welcome.getWindowHandles());
					driver_welcome.switchTo().window(tabs2.get(1));
					fc.utobj().clickElement(driver_welcome, pobj5.clickHereDocumentlink);
					fc.utobj().sendKeys(driver_welcome, pobj5.acknowkledgeSignature, "/" + signature + "/");
					fc.utobj().sendKeys(driver_welcome, pobj5.acknowkledgeName, signatureName);
					fc.utobj().clickElement(driver_welcome, pobj5.acknowledgeSubmit);
					boolean isTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
							".//*[contains(text () ,'Thank you. Please print a copy of this receipt for your records.')]");
					if (isTextPresent == false) {
						fc.utobj().throwsException("Fdd login and download not verified ");
					}
					driver_welcome.close();
					driver_welcome.switchTo().window(tabs2.get(0));

					driver_welcome.quit();
				}
				boolean isRemarkPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
						".//*[contains(text () ,'" + mailSubject + "')]");
				if (isRemarkPresent == false) {
					fc.utobj().throwsException("At Activity History FDD Email not verfied");
				}

				String expectedSubject = mailSubject;
				String expectedMessageBody = "We are pleased to send you a copy of our Franchise Disclosure Document (FDD)";
				//
				String emailId = "salesautomation@staffex.com";
				Map<String, String> mailData = fc.utobj().readMailBox(expectedSubject, expectedMessageBody, emailId,
						"sdg@1a@Hfs");
				System.out.println(mailData);
				if (!mailData.get("mailBody")
						.contains("We are pleased to send you a copy of our Franchise Disclosure Document (FDD)")) {
					fc.utobj().throwsSkipException("was not able to verify Lead First Name in owner assignment email");
				}
				if (!mailData.get("subject").contains(expectedSubject)) {
					fc.utobj().throwsSkipException("was not able to verify lead Last Name in owner assignment email");
				}

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatch(driver_welcome, e, testCaseId);
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesank99full", "sales" , "TC_Sales_AddLeadWithFullInfo" }) // Fixed : Verified // Akshat
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Sales_AddLeadWithFullInfo", testCaseDescription = "Verify Add Lead With Full Info")
	void verifyAddLeadWithFullInfo() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			LeadSummaryUI pobj = new LeadSummaryUI(driver);
			Sales fsmod = new Sales();

			fc.utobj().printTestStep("confiure lead marketing code");
			String marketingCode = fc.utobj().generateTestData(dataSet.get("Lead Marketing Code"));
			fc.adminpage().adminPage(driver);

			fc.utobj().clickPartialLinkText(driver, "Marketing Code");
			fc.utobj().clickElement(driver, pobj.addLeadMarketingCode);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.leadMarketingCode, marketingCode);
			fc.utobj().clickElement(driver, pobj.addBtnMarketingCode);

			fc.utobj().clickElement(driver, pobj.cboxCloseBtn);
			driver.switchTo().defaultContent();

			fc.sales().sales_common().fsModule(driver);

			String firstName = fc.utobj().generateTestData(dataSet.get("FirstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("LastName"));
			String country = dataSet.get("country");
			String state = dataSet.get("state");
			String email = dataSet.get("email");
			String address = dataSet.get("address");
			String address2 = dataSet.get("address2");
			String leadSourceCategory = dataSet.get("leadSourceCategory");
			String leadSourceDetails = dataSet.get("leadSourceDetails");
			String zipCode = fc.utobj().generateRandomNumber6Digit();
			String userName = "FranConnect Administrator";
			fsmod.leadManagement(driver);

			fc.utobj().printTestStep("Add lead");
			clickAddLeadLink(driver);

			fc.utobj().printTestStep("Fill full lead info for all the tabs");
			FSLeadSummaryAddLeadPage leadAddPage = new FSLeadSummaryAddLeadPage(driver);

			fc.utobj().selectDropDown(driver, leadAddPage.salutation, "Mr.");
			fc.utobj().sendKeys(driver, leadAddPage.firstName, firstName);
			fc.utobj().sendKeys(driver, leadAddPage.lastName, lastName);
			fc.utobj().sendKeys(driver, leadAddPage.address, address);
			fc.utobj().sendKeys(driver, leadAddPage.address2, address2);
			fc.utobj().sendKeys(driver, leadAddPage.city, "Test city");
			fc.utobj().sendKeys(driver, leadAddPage.country, "USA");
			fc.utobj().sendKeys(driver, leadAddPage.state, "Alabama");
			fc.utobj().sendKeys(driver, leadAddPage.zip, zipCode);
			fc.utobj().sendKeys(driver, leadAddPage.county, "Bibb");
			fc.utobj().sendKeys(driver, leadAddPage.preferredModeofContact, "Email");
			fc.utobj().sendKeys(driver, leadAddPage.bestTimeToContact, "Morning");
			fc.utobj().sendKeys(driver, leadAddPage.phone, "8416825736");
			fc.utobj().sendKeys(driver, leadAddPage.phoneExt, "91");
			fc.utobj().sendKeys(driver, leadAddPage.homePhone, "9999999999");
			fc.utobj().sendKeys(driver, leadAddPage.homePhoneExt, "91");
			fc.utobj().sendKeys(driver, leadAddPage.fax, "1234567891");
			fc.utobj().sendKeys(driver, leadAddPage.mobile, "9123456789");
			fc.utobj().sendKeys(driver, leadAddPage.emailID, email);
			fc.utobj().sendKeys(driver, leadAddPage.companyName, "Company Name");
			fc.utobj().sendKeys(driver, leadAddPage.comments, "These are comments");
			fc.utobj().selectDropDownByVisibleText(driver, leadAddPage.leadOwnerID, userName);
			fc.utobj().selectDropDownByVisibleText(driver, leadAddPage.leadRatingID, "Hot");
			fc.utobj().selectDropDownByVisibleText(driver, leadAddPage.marketingCode, marketingCode);
			fc.utobj().selectDropDownByVisibleText(driver, leadAddPage.leadSource2ID, leadSourceCategory);
			fc.utobj().selectDropDownByVisibleText(driver, leadAddPage.leadSource3ID, leadSourceDetails);
			fc.utobj().sendKeys(driver, leadAddPage.currentNetWorth, "6000000");
			fc.utobj().sendKeys(driver, leadAddPage.cashAvailableForInvestment, "9000000");
			fc.utobj().sendKeys(driver, leadAddPage.investTimeframe, "2 Months");
			fc.utobj().sendKeys(driver, leadAddPage.background, "Background");
			fc.utobj().sendKeys(driver, leadAddPage.sourceOfFunding, "Source of Funding");
			String currentDate = fc.utobj().getCurrentDateUSFormat();
			String nextCallDate = fc.utobj().getFutureDateUSFormat(7);
			String forecastClosureDate = fc.utobj().getFutureDateUSFormat(50);
			fc.utobj().sendKeys(driver, leadAddPage.nextCallDate, nextCallDate);
			fc.utobj().sendKeys(driver, leadAddPage.noOfUnitReq, "1");

			boolean isPreferedLocationMaximized = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[@id='temppreferredCity1']");
			if (isPreferedLocationMaximized == false) {
				fc.utobj().clickElement(driver, leadAddPage.maximizePreferedLocation);
			}

			fc.utobj().sendKeys(driver, leadAddPage.temppreferredCity1, "Prefered City");
			fc.utobj().sendKeys(driver, leadAddPage.temppreferredCountry1, "USA");
			fc.utobj().sendKeys(driver, leadAddPage.temppreferredStateId1, "Alabama");
			fc.utobj().sendKeys(driver, leadAddPage.temppreferredCity2, "Prefered City2");
			fc.utobj().sendKeys(driver, leadAddPage.temppreferredCountry2, "USA");
			fc.utobj().sendKeys(driver, leadAddPage.temppreferredStateId2, "Alabama");
			fc.utobj().sendKeys(driver, leadAddPage.forecastClosureDate, forecastClosureDate);
			fc.utobj().sendKeys(driver, leadAddPage.probability, "90");
			fc.utobj().selectDropDownByVisibleText(driver, leadAddPage.forecastRating, "Most Likely");
			fc.utobj().sendKeys(driver, leadAddPage.forecastRevenue, "1500000");
			fc.utobj().clickElement(driver, leadAddPage.save);

			fc.utobj().clickElement(driver, pobj.personalProfile);
			FSLeadSummaryPersonalProfilePage personalProfile = new FSLeadSummaryPersonalProfilePage(driver);
			fc.utobj().clickElement(driver, personalProfile.gender);
			fc.utobj().sendKeys(driver, personalProfile.homeAddress, "Address");
			fc.utobj().sendKeys(driver, personalProfile.howLongAtAddress, "5");
			fc.utobj().sendKeys(driver, personalProfile.homeCity, "City");
			fc.utobj().selectDropDownByVisibleText(driver, personalProfile.homeCountry, "USA");
			String zipCode1 = fc.utobj().generateRandomNumber6Digit();
			fc.utobj().sendKeys(driver, personalProfile.homeZip, zipCode1);
			fc.utobj().sendKeys(driver, personalProfile.timeToCall, "Morning");
			fc.utobj().clickElement(driver, personalProfile.homeOwnership);
			fc.utobj().sendKeys(driver, personalProfile.email, email);
			fc.utobj().clickElement(driver, personalProfile.maritalStatus);
			// fc.utobj().sendKeys(driver, personalProfile.spouseName, "Spouse
			// Name");
			fc.utobj().sendKeys(driver, personalProfile.heardProformaFrom, "How did you hear about us");
			fc.utobj().sendKeys(driver, personalProfile.seekingOwnBusiness,
					"How long have you wanted to operate your own franchise");
			fc.utobj().clickElement(driver, personalProfile.fullTimeBusiness);
			fc.utobj().sendKeys(driver, personalProfile.otherInvestigation, "No");
			fc.utobj().sendKeys(driver, personalProfile.presentEmployer, "Present Emp;oyer");
			fc.utobj().sendKeys(driver, personalProfile.percentOwn, "60");
			fc.utobj().sendKeys(driver, personalProfile.title, "Title");
			fc.utobj().sendKeys(driver, personalProfile.dateStarted, currentDate);
			fc.utobj().sendKeys(driver, personalProfile.employerAddress, "Employer Address");
			fc.utobj().sendKeys(driver, personalProfile.employerCity, "Employer City");
			fc.utobj().selectDropDownByVisibleText(driver, personalProfile.employerCountry, "USA");
			fc.utobj().selectDropDownByVisibleText(driver, personalProfile.employerState, "Alabama");
			fc.utobj().sendKeys(driver, personalProfile.employerCity, "Employer City");
			fc.utobj().sendKeys(driver, personalProfile.businessPhone, "235646566666");
			fc.utobj().clickElement(driver, personalProfile.hourPerWeek);
			fc.utobj().sendKeys(driver, personalProfile.salary, "100,000.00");
			fc.utobj().sendKeys(driver, personalProfile.responsibility, "Brief description of your responsibilities");
			fc.utobj().clickElement(driver, personalProfile.selfEmployed);
			fc.utobj().clickElement(driver, personalProfile.limitProforma);
			fc.utobj().clickElement(driver, personalProfile.similarWork);
			fc.utobj().sendKeys(driver, personalProfile.financeProforma, " finance your franchise");
			fc.utobj().clickElement(driver, personalProfile.partner);
			fc.utobj().selectDropDownByVisibleText(driver, personalProfile.supportHowLong, "5");
			fc.utobj().sendKeys(driver, personalProfile.income, "5000000");
			fc.utobj().sendKeys(driver, personalProfile.income, "5000000");
			fc.utobj().sendKeys(driver, personalProfile.otherSalary, "400000");
			fc.utobj().sendKeys(driver, personalProfile.otherIncomeExplaination, "Other Income Explaination");
			fc.utobj().clickElement(driver, personalProfile.soleSource);
			fc.utobj().clickElement(driver, personalProfile.howSoon);
			fc.utobj().clickElement(driver, personalProfile.runYourself);
			fc.utobj().clickElement(driver, personalProfile.responsibleForOperation);
			fc.utobj().clickElement(driver, personalProfile.convictedForFelony);
			fc.utobj().clickElement(driver, personalProfile.liabilites);
			fc.utobj().clickElement(driver, personalProfile.bankruptcy);
			fc.utobj().clickElement(driver, personalProfile.lawsuit);
			fc.utobj().clickElement(driver, personalProfile.convicted);
			fc.utobj().sendKeys(driver, personalProfile.familyFeelings,
					"How does your spouse and family feel about your being in business for yourself");
			fc.utobj().sendKeys(driver, personalProfile.otherFacts, "Other Facts");
			fc.utobj().sendKeys(driver, personalProfile.personalPersonalityStyle, "Personal / Personality Style");
			fc.utobj().sendKeys(driver, personalProfile.backgroundOverview, "Lead's Background/Business Overview");
			fc.utobj().sendKeys(driver, personalProfile.goalDream, "Goals and Aspirations");
			fc.utobj().sendKeys(driver, personalProfile.concerns, "concerns");
			fc.utobj().sendKeys(driver, personalProfile.timing, "Reason For Time Frame Choice ");
			fc.utobj().sendKeys(driver, personalProfile.hotButtons,
					"Franchise Pros (Advantages to us in prospect's mind)");
			fc.utobj().sendKeys(driver, personalProfile.otherOptions, "Other Options");
			fc.utobj().clickElement(driver, personalProfile.saveBtn);

			fc.utobj().clickElement(driver, pobj.qualificationDetailsTab);

			FSLeadSummaryQualificationDetailsPage qualificationDetails = new FSLeadSummaryQualificationDetailsPage(
					driver);
			fc.utobj().sendKeys(driver, qualificationDetails.date, currentDate);
			fc.utobj().clickElement(driver, qualificationDetails.gender_male);
			fc.utobj().sendKeys(driver, qualificationDetails.presentAddress, "Address");
			fc.utobj().sendKeys(driver, qualificationDetails.howLong, "4");
			fc.utobj().sendKeys(driver, qualificationDetails.howLong, "4");
			fc.utobj().sendKeys(driver, qualificationDetails.zipCode, zipCode1);
			fc.utobj().sendKeys(driver, qualificationDetails.workPhone, "123466789");
			fc.utobj().sendKeys(driver, qualificationDetails.phoneExt, "91");
			fc.utobj().sendKeys(driver, qualificationDetails.phoneExt, "91");
			fc.utobj().clickElement(driver, qualificationDetails.usCitizen_yes);
			fc.utobj().sendKeys(driver, qualificationDetails.ssn, fc.utobj().generateTestData(zipCode));
			fc.utobj().sendKeys(driver, qualificationDetails.previousAddress, "Previous Address");
			fc.utobj().sendKeys(driver, qualificationDetails.city, "City");
			fc.utobj().selectDropDownByVisibleText(driver, qualificationDetails.country, "USA");
			fc.utobj().selectDropDownByVisibleText(driver, qualificationDetails.stateID, "Alabama");
			fc.utobj().clickElement(driver, qualificationDetails.spouseUsCitizen_yes);
			fc.utobj().sendKeys(driver, qualificationDetails.liablitiesDescription, "Description");
			fc.utobj().sendKeys(driver, qualificationDetails.assestsDescription, "Description");
			fc.utobj().sendKeys(driver, qualificationDetails.reAddress1, "Re Address");
			fc.utobj().sendKeys(driver, qualificationDetails.reDatePurchased1, currentDate);
			fc.utobj().sendKeys(driver, qualificationDetails.reOrigCost1, "50000");
			fc.utobj().sendKeys(driver, qualificationDetails.rePresentValue1, "60000");
			fc.utobj().sendKeys(driver, qualificationDetails.reMortgageBalance1, "50000");
			fc.utobj().sendKeys(driver, qualificationDetails.reAddress2, "Re Address2");
			fc.utobj().sendKeys(driver, qualificationDetails.reDatePurchased2, currentDate);
			fc.utobj().sendKeys(driver, qualificationDetails.reOrigCost2, "30000");
			fc.utobj().sendKeys(driver, qualificationDetails.rePresentValue2, "900000");
			fc.utobj().sendKeys(driver, qualificationDetails.reMortgageBalance2, "70000");
			fc.utobj().sendKeys(driver, qualificationDetails.reAddress3, " Re Address 3");
			fc.utobj().sendKeys(driver, qualificationDetails.reDatePurchased3, currentDate);
			fc.utobj().sendKeys(driver, qualificationDetails.reOrigCost3, "80000");
			fc.utobj().sendKeys(driver, qualificationDetails.rePresentValue3, "30000");
			fc.utobj().sendKeys(driver, qualificationDetails.reMortgageBalance3, "400000");
			fc.utobj().sendKeys(driver, qualificationDetails.otherAnnualSourceDescription, "600000");
			fc.utobj().sendKeys(driver, qualificationDetails.whenReadyIfApproved,
					"When would you be ready to invest in your franchise if you were approved");
			fc.utobj().sendKeys(driver, qualificationDetails.skillsExperience,
					"What skills/experience do you have that will help you be successful in this business");
			fc.utobj().sendKeys(driver, qualificationDetails.enableReachGoals,
					"Why do you think this franchise will enable you to reach your personal goals");
			fc.utobj().sendKeys(driver, qualificationDetails.responsibleForDailyOperations,
					"Who will be responsible for the daily operation of your store");
			fc.utobj().sendKeys(driver, qualificationDetails.cashAvailable, "4000000");
			fc.utobj().clickElement(driver, qualificationDetails.approvedForFinancing_yes);
			fc.utobj().sendKeys(driver, qualificationDetails.amountApproved, "4000000");
			fc.utobj().clickElement(driver, qualificationDetails.soleIncomeSource_no);
			fc.utobj().clickElement(driver, qualificationDetails.liablities_yes);
			fc.utobj().clickElement(driver, qualificationDetails.lawsuit_no);
			fc.utobj().clickElement(driver, qualificationDetails.convicted_no);
			fc.utobj().clickElement(driver, qualificationDetails.convictedOfFelony_yes);
			fc.utobj().sendKeys(driver, qualificationDetails.explainConviction, "If so, explain");
			fc.utobj().clickElement(driver, qualificationDetails.filedBankruptcy_yes);
			fc.utobj().sendKeys(driver, qualificationDetails.dateFiled, currentDate);
			fc.utobj().sendKeys(driver, qualificationDetails.dateDischarged, currentDate);
			fc.utobj().sendKeys(driver, qualificationDetails.locationPreference1, "Preference 1");
			fc.utobj().sendKeys(driver, qualificationDetails.locationPreference2, "Preference 2");
			fc.utobj().sendKeys(driver, qualificationDetails.locationPreference3, "Preference 3");
			fc.utobj().sendKeys(driver, qualificationDetails.businessQuestion1, "Buiseness Question1");
			fc.utobj().sendKeys(driver, qualificationDetails.businessQuestion2, "Buiseness Question2");
			fc.utobj().sendKeys(driver, qualificationDetails.businessQuestion3, "Buiseness Question3");
			fc.utobj().clickElement(driver, qualificationDetails.appCanApplicantbeCalled_yes);
			fc.utobj().selectDropDownByVisibleText(driver, qualificationDetails.currentNetLiquidWorth, "Over $5000000");
			fc.utobj().selectDropDownByVisibleText(driver, qualificationDetails.casshAvailableForInvestment,
					"Over $600000");
			fc.utobj().selectDropDownByVisibleText(driver, qualificationDetails.investmentTimeframe, "Under 1 Month");
			fc.utobj().selectDropDownByVisibleText(driver, qualificationDetails.employmentbackground,
					"Blue Chip professional");
			fc.utobj().clickElement(driver, qualificationDetails.backgroundCheckApproval_yes);
			// Qualification Checklists
			fc.utobj().clickElement(driver, qualificationDetails.saveBtn);

			fc.utobj().clickElement(driver, pobj.realEstate);
			FSLeadSummaryRealEstatePage realStatePage = new FSLeadSummaryRealEstatePage(driver);
			fc.utobj().sendKeys(driver, realStatePage.siteAddress1, "Site Street 1");
			fc.utobj().sendKeys(driver, realStatePage.siteAddress2, "Site Street 2");
			fc.utobj().sendKeys(driver, realStatePage.siteCity, "Site City");
			fc.utobj().selectDropDownByVisibleText(driver, realStatePage.siteCountry, "USA");
			fc.utobj().selectDropDownByVisibleText(driver, realStatePage.siteState, "Alabama");
			fc.utobj().sendKeys(driver, realStatePage.buildingSize, "5000");
			fc.utobj().sendKeys(driver, realStatePage.buildingDimentionsX, "100");
			fc.utobj().sendKeys(driver, realStatePage.buildingDimentionsY, "90");
			fc.utobj().sendKeys(driver, realStatePage.buildingDimentionsZ, "80");
			fc.utobj().sendKeys(driver, realStatePage.parkingSpaces, "100");
			fc.utobj().sendKeys(driver, realStatePage.dealType, "Deal Type");
			fc.utobj().sendKeys(driver, realStatePage.loiSent, currentDate);
			fc.utobj().sendKeys(driver, realStatePage.loiSigned, currentDate);
			fc.utobj().sendKeys(driver, realStatePage.approvalDate, currentDate);
			fc.utobj().sendKeys(driver, realStatePage.leaseCommencement, currentDate);
			String futureDate = fc.utobj().getFutureDateUSFormat(50);
			String futureDate1 = fc.utobj().getFutureDateUSFormat(10);

			fc.utobj().sendKeys(driver, realStatePage.leaseExpiration, futureDate);
			fc.utobj().sendKeys(driver, realStatePage.initialTerm, "5");
			fc.utobj().sendKeys(driver, realStatePage.optionTerm, "6");
			fc.utobj().clickElement(driver, realStatePage.purchaseOption);
			fc.utobj().sendKeys(driver, realStatePage.projectedOpeningDate, nextCallDate);
			fc.utobj().clickElement(driver, realStatePage.generalContractorSelector);
			fc.utobj().sendKeys(driver, realStatePage.nameGeneralContractor,
					fc.utobj().generateTestData("Name General Contractor"));
			fc.utobj().sendKeys(driver, realStatePage.addressGeneralContractor,
					fc.utobj().generateTestData("Address General Contractor"));
			fc.utobj().sendKeys(driver, realStatePage.addressGeneralContractor,
					fc.utobj().generateTestData("Address General Contractor"));
			fc.utobj().sendKeys(driver, realStatePage.permitApplied, nextCallDate);
			fc.utobj().sendKeys(driver, realStatePage.permitIssued, futureDate1);
			fc.utobj().sendKeys(driver, realStatePage.certificate, futureDate1);
			fc.utobj().sendKeys(driver, realStatePage.turnOverDate, futureDate1);
			fc.utobj().sendKeys(driver, realStatePage.grandOpeningDate, futureDate1);
			fc.utobj().clickElement(driver, realStatePage.saveBtn);

			fc.utobj().clickElement(driver, pobj.documentsTab);
			fc.utobj().sendKeys(driver, pobj.documentTitle, fc.utobj().generateTestData("Document Title"));
			String fileName = fc.utobj().getFilePathFromTestData(dataSet.get("fileName"));
			fc.utobj().sendKeys(driver, pobj.documentAttachment, fileName);
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().clickElement(driver, pobj.complianceTab);
			FSLeadSummaryCompliancePage compliacePage = new FSLeadSummaryCompliancePage(driver);
			String fddDate = fc.utobj().getCurrentDateUSFormat();
			String fddRecievedDate = fc.utobj().getFutureDateUSFormat(6);
			String fddRequestedDate = fc.utobj().getFutureDateUSFormat(1);

			fc.utobj().sendKeys(driver, compliacePage.fddDate, fddDate);
			fc.utobj().sendKeys(driver, compliacePage.recByFrancDate1, fddRecievedDate);
			fc.utobj().sendKeys(driver, compliacePage.bussDayExp10Date, fddDate);
			fc.utobj().sendKeys(driver, compliacePage.versionOfUfoc, "Version of FDD1");
			fc.utobj().sendKeys(driver, compliacePage.ipAddress, "192.168.9.44");
			fc.utobj().sendKeys(driver, compliacePage.browserType, "Browser Type");
			fc.utobj().sendKeys(driver, compliacePage.firstFrancPaymentDate, fddRecievedDate);
			fc.utobj().clickElement(driver, compliacePage.stateRegReq);
			fc.utobj().sendKeys(driver, compliacePage.secondFrancPaymentDate, fddRecievedDate);
			fc.utobj().clickElement(driver, compliacePage.stateReqAddendumProperlySign);
			fc.utobj().clickElement(driver, compliacePage.francCommiteeApprovalYes);
			fc.utobj().sendKeys(driver, compliacePage.faRequestedDate, fddRequestedDate);
			fc.utobj().sendKeys(driver, compliacePage.faReceivedDate, fddRecievedDate);
			fc.utobj().sendKeys(driver, compliacePage.francRecAgrDate, fddRecievedDate);
			fc.utobj().sendKeys(driver, compliacePage.bussDay5ExpDate, fddRecievedDate);
			fc.utobj().sendKeys(driver, compliacePage.francSignAgrDate, fddRecievedDate);
			fc.utobj().sendKeys(driver, compliacePage.bussDayRule10Check, fddRecievedDate);
			fc.utobj().sendKeys(driver, compliacePage.versionFrancAgr, "Version of Franchisee Agreement");
			fc.utobj().sendKeys(driver, compliacePage.franFeeAmt, "1000");
			fc.utobj().sendKeys(driver, compliacePage.franFeeDate, fddRecievedDate);
			fc.utobj().sendKeys(driver, compliacePage.areaFeeAmt, "1000");
			fc.utobj().sendKeys(driver, compliacePage.areaFeeDate, fddRecievedDate);
			fc.utobj().sendKeys(driver, compliacePage.adaExecutionDate, fddRecievedDate);
			fc.utobj().sendKeys(driver, compliacePage.faExecutionDate, fddRecievedDate);
			
			fc.utobj().clickElement(driver, compliacePage.contractRecSign);
			fc.utobj().clickElement(driver, compliacePage.leaseRiderProperlySign);
			fc.utobj().clickElement(driver, compliacePage.licAgrProperlySign);
			fc.utobj().clickElement(driver, compliacePage.promNotePropSign);
			fc.utobj().clickElement(driver, compliacePage.perCovenantAgrProperlySign);
			fc.utobj().clickElement(driver, compliacePage.ufocRecProperlySign);
			fc.utobj().clickElement(driver, compliacePage.guaranteeProperlySign);
			fc.utobj().clickElement(driver, compliacePage.otherDocProperlySign);
			fc.utobj().clickElement(driver, compliacePage.stateReqAddendumProperlySign);
			fc.utobj().clickElement(driver, compliacePage.handWrittenChanges);
			fc.utobj().clickElement(driver, compliacePage.otherAttendaProperlySign);
			fc.utobj().clickElement(driver, compliacePage.proofControlOverRealEstate);
			fc.utobj().clickElement(driver, compliacePage.saveBtn);

			fc.utobj().clickElement(driver, pobj.candidatePortalBtn);

			fc.utobj().sendKeys(driver, pobj.candidatePortalMessage, "Post Message");
			fc.utobj().clickElement(driver, pobj.postMessage);
			fc.utobj().clickElement(driver, pobj.visitTab);
			FSLeadSummaryVisitPage visitpage = new FSLeadSummaryVisitPage(driver);
			fc.utobj().sendKeys(driver, visitpage.visitSchedule, fddRecievedDate);
			fc.utobj().sendKeys(driver, visitpage.visitdate, fddRecievedDate);
			fc.utobj().clickElement(driver, visitpage.type);
			fc.utobj().clickElement(driver, visitpage.addToCalendar);
			fc.utobj().sendKeys(driver, visitpage.visitor1Name, fc.utobj().generateTestData("Visitor Name"));
			fc.utobj().sendKeys(driver, visitpage.relationship1, fc.utobj().generateTestData("Relationship"));
			fc.utobj().sendKeys(driver, visitpage.visitor2Name, fc.utobj().generateTestData("Visitor Name2"));
			fc.utobj().sendKeys(driver, visitpage.relationship2, fc.utobj().generateTestData("Relationship"));
			fc.utobj().sendKeys(driver, visitpage.visitor3Name, fc.utobj().generateTestData("Visitor Name3"));
			fc.utobj().sendKeys(driver, visitpage.relationship3, fc.utobj().generateTestData("Relationship3"));
			fc.utobj().sendKeys(driver, visitpage.agreedReimbursement, "60000");
			fc.utobj().sendKeys(driver, visitpage.actualReimbursement, "6000");
			fc.utobj().sendKeys(driver, visitpage.paymentSentDate, fddRecievedDate);
			fc.utobj().sendKeys(driver, visitpage.visitConfirmed, "Visit Confirmed By");
			fc.utobj().sendKeys(driver, visitpage.comments, "Comments");
			fc.utobj().printTestStep("Submit");
			fc.utobj().clickElement(driver, visitpage.saveBtn);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			// e.printStackTrace();
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales", "sales_failed" , "TC_Sales_RFCMail" }) // to be fixed - email not received ...
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-07-03", testCaseId = "TC_Sales_RFCMail", testCaseDescription = "Verify RFC Mail sent to the lead")
	void verifySalesRFCMail() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			LeadSummaryUI pobj1 = new LeadSummaryUI(driver);
			Sales fsmod = new Sales();

			fc.utobj().printTestStep("Go to Admin > Sales  >  Configure Web Form Email Content");
			fc.adminpage().adminPage(driver);

			// String emailId = "frantest2017@gmail.com";
			String emailId = "salesautomation@staffex.com";
			String mailBody = fc.utobj().generateTestData("RFC Mail Body");
			fc.utobj().clickLink(driver, "Configure Web Form Email Content");
			fc.utobj().clickElement(driver, pobj1.sendRFCMail);
			fc.utobj().sendKeys(driver, pobj1.emailFrom, emailId);
			String mailSubject = fc.utobj().generateTestData("RFC Email Subject");
			fc.utobj().sendKeys(driver, pobj1.emailSubject, mailSubject);
			fc.utobj().printTestStep("Send Web Form Email Notification");
			fc.utobj().printTestStep("Configure From Email, Subject, Email Content");
			fc.utobj().printTestStep("Select Email Fields");
			fc.utobj().printTestStep("Do you want to attach PDF yes");
			String windowHandle3 = driver.getWindowHandle();
			driver.switchTo().frame(pobj1.htmlFrame2);
			fc.utobj().sendKeys(driver, pobj1.mailText, mailBody);
			driver.switchTo().window(windowHandle3);
			fc.utobj().clickElement(driver, pobj1.emailFieldsDropDown);
			if (!fc.utobj().getElement(driver, pobj1.selectAllDrpDwn).isSelected()) {
				fc.utobj().clickElement(driver, pobj1.selectAllDrpDwn);
			}
			fc.utobj().clickElement(driver, pobj1.emailFieldsDropDown);
			fc.utobj().clickElement(driver, pobj1.pdfYesRadio);
			fc.utobj().clickElement(driver, pobj1.saveDoc); // configure button

			fc.utobj().printTestStep("Admin > Sales > Manage Web Form Generator");
			String fieldName = fc.utobj().generateTestData(dataSet.get("displayName"));
			AdminSalesManageWebFormGeneratorPageTest p1 = new AdminSalesManageWebFormGeneratorPageTest();
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Web Form Generator");

			AdminSales adsales = new AdminSales();
			adsales.manageWebFormGenerator(driver);

			AdminFranchiseSalesManageWebFormGeneratorPage pobj = new AdminFranchiseSalesManageWebFormGeneratorPage(
					driver);
			// Click on Create Form
			String formName = p1.addLeadFromWebForm(driver, fieldName);
			String firstName = fc.utobj().generateTestData(dataSet.get("FirstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("LastName"));
			String parentWindow = driver.getWindowHandle();
			fc.utobj().sendKeys(driver, pobj.searchMyForm, formName);
			fc.utobj().clickElement(driver, pobj.searchMyFormBtn);
			fc.utobj().actionImgOption(driver, formName, "Launch & Test");
			fc.utobj().sleep();
			String formTitle = "WebFrom Title";
			fc.utobj().printTestStep("Launch And Test Form");
			
			Set<String> allWindows = driver.getWindowHandles();
			
			for (String currentWindow : allWindows) {
				if (! (currentWindow.equalsIgnoreCase(parentWindow))) {
					fc.utobj().sleep();
					driver.switchTo().window(currentWindow);
					
					/*String titleTextCurrent = driver.getTitle();
					if (titleTextCurrent.contains(formTitle)) {*/
					
						// fill the form with data
						fc.utobj().sendKeys(driver, pobj.leadFirstName, firstName);
						fc.utobj().sendKeys(driver, pobj.leadLastName, lastName);
						fc.utobj().sendKeys(driver, pobj.emailID, "salesautomation@staffex.com");
						fc.utobj().clickElement(driver, pobj.submitBtn);
						fc.utobj().isTextDisplayed(driver,
								"Thank you for submitting your information, we will get back to you shortly.",
								"was not able to verify confirmation Msg");
						driver.close();
						
					/*} else {
						driver.close();
						driver.switchTo().window(parentWindow);*/
					}
					driver.switchTo().window(parentWindow);
				}
			// }

			fc.utobj().printTestStep("Open WebForm > Fill web form  > Submit");
			
			fc.commonMethods().getModules().clickSalesModule(driver); // akshat
			fsmod.search(driver);
			fc.utobj().printTestStep("Search for Lead Name added ");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByID(driver, "firstName"), firstName);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByID(driver, "lastName"), lastName);
			fc.utobj().clickElement(driver, fc.utobj().getElementByID(driver, "Submit"));

			String LeadName = firstName + " " + lastName;

			fc.utobj().printTestStep("Verify for the Lead Name added ");

			boolean isfieldNameText = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[@id='containsImage']/tbody/tr/td/span/a['" + LeadName + "']");
			if (isfieldNameText == false) {
				fc.utobj().throwsException("was not able to verify added Lead");
			}

			fc.utobj().printTestStep("Verifying RFC mail");
			String expectedSubject = mailSubject;
			String expectedMessageBody = mailBody;

			Map<String, String> mailData = fc.utobj().readMailBox(expectedSubject, expectedMessageBody, emailId,
					"sdg@1a@Hfs");
			System.out.println(mailData);
			if (!mailData.get("mailBody").contains(firstName)) {
				fc.utobj().throwsSkipException("was not able to verify Lead First Name in owner assignment email");
			}

			if (!mailData.get("mailBody").contains(lastName)) {
				fc.utobj().throwsSkipException("was not able to verify lead Last Name in owner assignment email");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			// e.printStackTrace();
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesank", "sales" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Sales_Sites_001", testCaseDescription = "Verify Add,Modify and delete of Sites")
	void verifySalesSite001() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			LeadSummaryUI pobj1 = new LeadSummaryUI(driver);
			Sales fsmod = new Sales();

			fc.utobj().printTestStep("Go to Sales > Sites > Add Sites ");
			fc.sales().sales_common().fsModule(driver);
			fsmod.sites(driver);
			FSSitesPage sitePage = new FSSitesPage(driver);
			String locationTitle = fc.utobj().generateTestData("Location Tilte");
			fc.utobj().clickElement(driver, sitePage.addSitesBtn);
			fc.utobj().sendKeys(driver, sitePage.locationTitle, locationTitle);
			fc.utobj().sendKeys(driver, sitePage.city, "City");
			fc.utobj().selectDropDownByVisibleText(driver, sitePage.countryID, "USA");
			fc.utobj().selectDropDownByVisibleText(driver, sitePage.stateID, "Alabama");
			fc.utobj().sendKeys(driver, sitePage.buildingSize, "1000");
			fc.utobj().sendKeys(driver, sitePage.parkingSpaces, "Yes");
			fc.utobj().clickElement(driver, sitePage.submitButton);

			boolean isSiteAdded = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + locationTitle + "')]");
			if (isSiteAdded == false) {
				fc.utobj().throwsException("Loaction not getting added in Sites ");
			}
			fc.utobj().printTestStep("Then Add a remarks");
			String remarkName = fc.utobj().generateTestData("Remarks");
			String remarkName1 = fc.utobj().generateTestData("Remarks");
			fc.utobj().clickElement(driver, sitePage.addRemarkBtn);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, sitePage.siteRemark, remarkName);
			fc.utobj().clickElement(driver, sitePage.addRemark);
			fc.utobj().clickElement(driver, sitePage.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Modify Remarks");
			fc.utobj().actionImgOption(driver, remarkName, "Modify");

			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().sendKeys(driver, sitePage.siteRemark, remarkName1);
			fc.utobj().clickElement(driver, sitePage.modifyBtn);

			fc.utobj().clickElement(driver, sitePage.closeBtn);
			fc.utobj().switchFrameToDefault(driver);
			boolean isRemarkPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + remarkName1 + "')]");
			if (isRemarkPresent == false) {
				fc.utobj().throwsException("Remarks not getting modified in FS sites");
			}
			fc.utobj().printTestStep("Then Delete remarks");
			fc.utobj().actionImgOption(driver, remarkName1, "Delete");
			fc.utobj().acceptAlertBox(driver);

			isRemarkPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + remarkName1 + "')]");
			if (isRemarkPresent == true) {
				fc.utobj().throwsException("Remarks not getting deleted in FS sites");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/*
	 * @Test(groups = { "salesank","sales"})
	 * 
	 * @TestCase(createdOn="2017-05-01",updatedOn="2018-06-14",testCaseId=
	 * "TC_Sales_Sites_001",testCaseDescription=
	 * "Validate Fdd Email in the activity history > Validate email in mail box >Validate fdd login and download "
	 * ) void verifySalesSites001() throws Exception { String testCaseId =
	 * fc.utobj().readTestCaseInfo(this.getClass().getName()+"."+new
	 * Object(){}.getClass().getEnclosingMethod().getName());
	 * 
	 * Map<String,String> config =
	 * fc.utobj().readConfigurationFile(FranconnectUtil.path);
	 * Map<String,String> dataSet = fc.utobj().readTestData("sales",
	 * testCaseId); WebDriver driver = fc.utobj().openDriver(config);
	 * 
	 * try{ driver = fc.loginpage().login(driver,config); FSLeadSummaryPage
	 * pobj1 = new FSLeadSummaryPage(driver); FsModulePageTest fsmod = new
	 * FsModulePageTest(driver);
	 * 
	 * fc.sales().sales_common().fsModule(fc.sales(), driver);
	 * fsmod.sites(driver); FSSitesPage sitePage=new FSSitesPage(driver); String
	 * locationTitle=fc.utobj().generateTestData("Location Tilte"); String
	 * locationTitle1=fc.utobj().generateTestData("Location Tilte");
	 * fc.utobj().clickElement(driver, sitePage.addSitesBtn);
	 * fc.utobj().sendKeys(driver, sitePage.locationTitle, locationTitle);
	 * fc.utobj().sendKeys(driver, sitePage.city, "City");
	 * fc.utobj().selectDropDownByVisibleText(driver, sitePage.countryID,
	 * "USA"); fc.utobj().selectDropDownByVisibleText(driver, sitePage.stateID,
	 * "Alabama"); fc.utobj().sendKeys(driver, sitePage.buildingSize, "1000");
	 * fc.utobj().sendKeys(driver, sitePage.parkingSpaces, "Yes");
	 * fc.utobj().clickElement(driver, sitePage.submitButton);
	 * 
	 * boolean isSiteAdded=fc.utobj().verifyCase(driver,
	 * ".//*[contains(text(),'"+locationTitle+"')]"); if(isSiteAdded==false) {
	 * fc.utobj().throwsException("Loaction not getting added in Sites "); }
	 * fsmod.sites(driver); fc.utobj().actionImgOption(driver, locationTitle,
	 * "Modify"); fc.utobj().sendKeys(driver, sitePage.locationTitle,
	 * locationTitle1); fc.utobj().clickElement(driver, sitePage.submitButton);
	 * boolean isSiteresent=fc.utobj().verifyCase(driver,
	 * ".//*[contains(text(),'"+locationTitle1+"')]"); if(isSiteresent==false) {
	 * fc.utobj().throwsException("Sales Sitw not getting modified"); }
	 * fsmod.sites(driver); fc.utobj().actionImgOption(driver, locationTitle1,
	 * "Delete"); fc.utobj().acceptAlertBox(driver);
	 * isSiteresent=fc.utobj().verifyCase(driver,
	 * ".//*[contains(text(),'"+locationTitle1+"')]"); if(isSiteresent==true) {
	 * fc.utobj().throwsException("Remarks not getting deleted in FS sites"); }
	 * 
	 * 
	 * fc.utobj().logoutAndQuitBrowser(driver, config, testCaseId);
	 * 
	 * } catch(Exception e){ fc.utobj().quitBrowserOnCatch(driver, config, e,
	 * testCaseId); } }
	 */

	@Test(groups = { "TC_Sales_SmartGroup_001", "sales" , "sales_failed" }) // fixed : verified // akshat
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Sales_SmartGroup_001", testCaseDescription = "Verify Smart Group functionality")
	void verifySmartGroup001() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			Sales fsmod = new Sales();
			fc.sales().sales_common().fsModule(driver);
			fsmod.leadManagement(driver);
			fc.utobj().printTestStep("Sales > Lead Summary > Add lead");
			LeadSummaryUI pobj = new LeadSummaryUI(driver);
			String firstName = fc.utobj().generateTestData(dataSet.get("FirstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("LastName"));
			String leadFullName = firstName + " " + lastName;
			String userName = "FranConnect Administrator";
			Map<String, String> leadName = addLeadSummaryWithLeadNameOwnerName(driver, firstName, lastName, userName);
			fsmod.groups(driver);
			fc.utobj().printTestStep(
					"Sales > Group > Create Group > Select Smart Group option > Select Name filter > Enter Name > Continue");
			FSGroupsPage groupsPage = new FSGroupsPage(driver);
			fc.utobj().clickElement(driver, groupsPage.addGroupsBtn);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);

			String groupName = fc.utobj().generateTestData("GroupName");
			fc.utobj().sendKeys(driver, groupsPage.groupName, groupName);
			fc.utobj().sendKeys(driver, groupsPage.groupDescription, groupName + " Description");
			fc.utobj().clickElement(driver, groupsPage.groupTypeSmart);
			fc.utobj().clickElement(driver, groupsPage.addBtn);
			fc.utobj().switchFrameToDefault(driver);
			
			fc.utobj().clickElement(driver, groupsPage.availableFields);
			fc.utobj().sendKeys(driver, groupsPage.selectCriteria_availableFields_Input, "First Name");
			fc.utobj().clickElement(driver, groupsPage.selectCriteria_availableFields_searchButton);
			fc.utobj().clickElement(driver, groupsPage.firstNameAvlbFields);
			
			fc.utobj().sendKeys(driver, groupsPage.selectCriteria_availableFields_Input, "Last Name");
			fc.utobj().clickElement(driver, groupsPage.selectCriteria_availableFields_searchButton);
			fc.utobj().clickElement(driver, groupsPage.lastNameAvlbFields);
			
			fc.utobj().sendKeys(driver, groupsPage.containsFirstName, firstName);
			fc.utobj().sendKeys(driver, groupsPage.containsLastName, lastName);
			fc.utobj().clickElement(driver, groupsPage.saveButton);
			fc.utobj().acceptAlertBox(driver);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//button[contains(text(),'Ok')]"));
			// fc.utobj().clickElement(driver, groupsPage.saveButton);
			
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'" + groupName
					+ "')]/ancestor::tr/td[@width='16%']/a[contains(text(),'1')]"));
			// fc.utobj().switchFrame(driver,
			// fc.CommonMethods().switch_frameClass_newLayoutcboxIframe(driver));
			boolean isLeadPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text(),'" + firstName + " " + lastName + "')]");
			if (isLeadPresent == false) {
				fc.utobj().throwsException("Lead not added in smart Group");
			}
			
			// akshat
			// as per new functionality - lead cannot be removed or associated from the Smart Group 
			/*// fc.utobj().switchFrameToDefault(driver);
			fc.utobj().printTestStep("Now click on the lead count > Remove the lead from the group");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'" + firstName
					+ " " + lastName + "')]/ancestor::tr/td/input[@name='checkbox']"));
			fc.utobj().clickElement(driver, groupsPage.removeFromGrpBtn);
			fc.utobj().acceptAlertBox(driver);
			isLeadPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text(),'" + firstName + " " + lastName + "')]");
			if (isLeadPresent == true) {
				fc.utobj().throwsException("Lead not added in smart Group");
			}*/

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			// e.printStackTrace();
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesank", "sales" , "TC_Sales_PreferredSiteBasedSearch_001" }) // fixed : verified // akshat
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Sales_PreferredSiteBasedSearch_001", testCaseDescription = "Verify Preferred Site Based Search")
	void verifyPreferredSiteBasedSearch() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			Sales fsmod = new Sales();

			// fc.sales().sales_common().fsModule(fc.sales(), driver);
			fc.utobj().printTestStep("Sales > Add lead with Preferred Locations");
			fsmod.leadManagement(driver);
			String firstName = fc.utobj().generateTestData("FirstName");
			String lastName = fc.utobj().generateTestData("FirstName");
			String country = "USA";
			String state = "Alabama";
			String emailId = "salesautomation@staffex.com";
			String leadOwner = "FranConnect Administrator";
			String leadSourceCategory = "Import";
			String leadSourceDetails = "None";
			String preferedCity1 = fc.utobj().generateTestData("Prefered City");
			String preferedCity2 = fc.utobj().generateTestData("Prefered City");

			FSLeadSummaryAddLeadPage pobj = new FSLeadSummaryAddLeadPage(driver);
			clickAddLeadLink(driver);
			fc.utobj().sendKeys(driver, pobj.firstName, firstName);
			fc.utobj().sendKeys(driver, pobj.lastName, lastName);
			fc.utobj().selectDropDownByVisibleText(driver, pobj.country, country);

			fc.utobj().selectDropDownByVisibleText(driver, pobj.state, state);
			fc.utobj().sendKeys(driver, pobj.emailID, emailId);
			fc.utobj().sendKeys(driver, pobj.zip, "123456");
			fc.utobj().sendKeys(driver, pobj.phone, "12345678");
			fc.utobj().sendKeys(driver, pobj.address, "New Highway Road");
			fc.utobj().selectDropDownByVisibleText(driver, pobj.leadOwnerID, leadOwner);
			fc.utobj().selectDropDownByVisibleText(driver, pobj.leadSource2ID, leadSourceCategory);

			fc.utobj().selectDropDownByVisibleText(driver, pobj.leadSource3ID, leadSourceDetails);
			boolean isPreferedLocationMaximized = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[@id='temppreferredCity1']");
			if (isPreferedLocationMaximized == false) {
				fc.utobj().clickElement(driver, pobj.maximizePreferedLocation);
			}

			fc.utobj().sendKeys(driver, pobj.temppreferredCity1, preferedCity1);
			fc.utobj().selectDropDownByVisibleText(driver, pobj.temppreferredCountry1, "USA");
			fc.utobj().selectDropDownByVisibleText(driver, pobj.temppreferredStateId1, "Alabama");
			fc.utobj().sendKeys(driver, pobj.temppreferredCity2, preferedCity2);
			fc.utobj().selectDropDownByVisibleText(driver, pobj.temppreferredCountry2, "USA");
			fc.utobj().selectDropDownByVisibleText(driver, pobj.temppreferredStateId2, "Alabama");

			fc.utobj().clickElement(driver, pobj.save);

			fc.utobj().printTestStep("Now go to sales > Search > Enter details of preferred lead search > Search");
			fsmod.search(driver);
			SearchUI searchPage = new SearchUI(driver);
			fc.utobj().sendKeys(driver, searchPage.preferredCity1, preferedCity1);
			
			fc.utobj().selectValFromMultiSelect(driver, searchPage.preferredCountry, "USA");
			
			/*fc.utobj().clickElement(driver, searchPage.preferredCountry1);
			fc.utobj().sendKeys(driver, searchPage.searchMultiple, "USA");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					(".//*[@id='selectItempreferredCountry1']/ancestor::label[contains(text(),'USA')]/input")));*/
			
			fc.utobj().clickElement(driver, searchPage.preferredStateId1);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='selectItempreferredStateId1']/ancestor::label[contains(text(),'Alabama')]/input"));
			// fc.utobj().moveToElement(driver,
			// fc.utobj().getElementByXpath(driver,".//span[contains(text(),'Preferred
			// Site Based Search')]")));
			fc.utobj().clickElement(driver, searchPage.locationbasedSearch);
			boolean isLeadPresent = fc.utobj().assertLinkPartialText(driver, firstName + " " + lastName);
			if (isLeadPresent == false) {
				fc.utobj().throwsException("Lead not searched by prefered location from search page");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			// e.printStackTrace();
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesfail", "sales" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-06", testCaseId = "FS_VisitTab_001", testCaseDescription = "Verify Visit tab functionality of lead")
	void verifyVisitTab() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Go to Sales");
			Sales fsmod = new Sales();

			LeadSummaryUI pobj = new LeadSummaryUI(driver);
			String firstName = fc.utobj().generateTestData(dataSet.get("FirstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("LastName"));
			String leadFullName = firstName + " " + lastName;
			String userName = "FranConnect Administrator";
			fc.utobj().printTestStep("Add a lead");
			Map<String, String> leadName = addLeadSummaryWithLeadNameOwnerName(driver, firstName, lastName, userName);
			String fddRecievedDate = fc.utobj().getCurrentDateUSFormat();
			fc.utobj().printTestStep("Go to Visit Tab and create Create Visit");
			fc.utobj().clickElement(driver, pobj.visitTab);
			FSLeadSummaryVisitPage visitpage = new FSLeadSummaryVisitPage(driver);
			fc.utobj().sendKeys(driver, visitpage.visitSchedule, fddRecievedDate);
			fc.utobj().sendKeys(driver, visitpage.visitdate, fddRecievedDate);
			fc.utobj().clickElement(driver, visitpage.type);
			fc.utobj().printTestStep("Check Add To Calendar check box > Select value in Assign To");
			fc.utobj().clickElement(driver, visitpage.addToCalendar);
			fc.utobj().sendKeys(driver, visitpage.visitor1Name, fc.utobj().generateTestData("Visitor Name"));
			fc.utobj().sendKeys(driver, visitpage.relationship1, fc.utobj().generateTestData("Relationship"));
			fc.utobj().sendKeys(driver, visitpage.visitor2Name, fc.utobj().generateTestData("Visitor Name2"));
			fc.utobj().sendKeys(driver, visitpage.relationship2, fc.utobj().generateTestData("Relationship"));
			fc.utobj().sendKeys(driver, visitpage.visitor3Name, fc.utobj().generateTestData("Visitor Name3"));
			fc.utobj().sendKeys(driver, visitpage.relationship3, fc.utobj().generateTestData("Relationship3"));
			fc.utobj().sendKeys(driver, visitpage.agreedReimbursement, "60000");
			fc.utobj().sendKeys(driver, visitpage.actualReimbursement, "6000");
			fc.utobj().sendKeys(driver, visitpage.paymentSentDate, fddRecievedDate);
			fc.utobj().sendKeys(driver, visitpage.visitConfirmed, "Visit Confirmed By");
			fc.utobj().sendKeys(driver, visitpage.comments, "Comments");
			fc.utobj().printTestStep("Save Visit Tab");
			fc.utobj().clickElement(driver, visitpage.saveBtn);

			fsmod.tasks(driver);
			fc.utobj().printTestStep("Validate if the Visit is coming in task tab");
			FSTasksPageTest fstask = new FSTasksPageTest();

			fstask.searchTask(driver, "Visit Schedule [" + leadFullName + "]");

			boolean isTaskCreated = fc.utobj().assertLinkPartialText(driver, leadFullName);
			if (isTaskCreated == false) {
				fc.utobj().throwsException("Task not created for added visit");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "FC_Sales_CandidatePortal_001", "sales" }) //
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-12", testCaseId = "FC_Sales_CandidatePortal_001", testCaseDescription = "Verify Candidate portal login functionality")
	void verifyCandidatePortal001() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			Sales fsmod = new Sales();

			fc.sales().sales_common().fsModule(driver);
			fsmod.leadManagement(driver);
			LeadSummaryUI pobj = new LeadSummaryUI(driver);
			String firstName = fc.utobj().generateTestData(dataSet.get("FirstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("LastName"));
			// String leadFullName = firstName + " " + lastName;
			String userName = "FranConnect Administrator";
			fc.utobj().printTestStep("Add a lead");
			Map<String, String> leadName = addLeadSummaryWithLeadNameOwnerName(driver, firstName, lastName, userName);
			fc.utobj().printTestStep("Go to  Candidate Portal tab > Take user name and password");
			fc.utobj().clickElement(driver, pobj.candidatePortalBtn);

			fc.utobj().clickElement(driver, pobj.showleadDetailsBtn);

			fc.utobj().printTestStep("open candidate portal jsp page");
			String buildUrl = config.get("buildUrl");
			String candidatePortalUserId = getCandidatePortal_FDD_UserName(driver);
			String fddpassword = getCandidatePortal_FDD_Password(driver);

			String fddurl = null;
			if ((buildUrl.charAt(buildUrl.length() - 1)) == '/') {
				fddurl = config.get("buildUrl").concat("candidate.jsp");
			} else {
				fddurl = config.get("buildUrl").concat("/candidate.jsp");
			}
			
			/*String signature = firstName;
			String signatureName = firstName;*/

			driver.navigate().to(fddurl);
			fc.utobj().printTestStep("enter user name and password >login");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//input[@name='userID']"),
					candidatePortalUserId);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//input[@name='password']"),
					fddpassword);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//input[@value='Login']"));

			try {
				fc.utobj().clickElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'My Dashboard')]"));
			} catch (Exception e) {
			}
			boolean isTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text () ,'Welcome " + firstName + ", to your personal franchise portal.')]");
			if (isTextPresent == false) {
				fc.utobj().throwsException("candidate potal login unsuccessfull");
			}
			
			driver = fc.loginpage().login(driver);
			
			fc.commonMethods().getModules().clickSalesModule(driver);
			// fc.sales().sales_common().fsModule(driver);
			fsmod.leadManagement(driver);
			
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[contains(text(),'" + firstName + " " + lastName + "')]"));
			fc.utobj().clickElement(driver, pobj.primaryInfo);
			FSLeadSummaryPrimaryInfoPageTest pobj1 = new FSLeadSummaryPrimaryInfoPageTest();
			driver = pobj1.gotoActivityHistorySection(driver);

			boolean isRemarkPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text () ,'" + firstName + " " + lastName + " logged into candidate portal')]");
			if (isRemarkPresent == false) {
				fc.utobj().throwsException("At Activity History logged into candidate portal remarks not created");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesjulyrt45", "sales12" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-12", testCaseId = "FC_Sales_CandidatePortal_002", testCaseDescription = "Verify Schedule Appointment functionality in Candidate Portal")
	void verifyCandidatePortal002() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		LeadTest lt = new LeadTest();
		Lead ld = new Lead();

		try {
			driver = fc.loginpage().login(driver);
			Sales fsmod = new Sales();

			Sales sales = fc.sales().sales_common().fsModule(driver);

			ld = fc.sales().sales_common().fillDefaultValue_LeadDetails(ld);
			lt.addLeadThroughWebServices(driver, ld);

			LeadSummaryUI pobj = new LeadSummaryUI(driver);
			fc.utobj().clickElement(driver, pobj.candidatePortalBtn);
			fc.utobj().printTestStep("Go to  Candidate Portal tab > Take user name and password");
			fc.utobj().printTestStep("open candidate portal jsp page");

			fc.utobj().clickElement(driver, pobj.showleadDetailsBtn);

			String buildUrl = config.get("buildUrl");
			String fddurl = null;
			String candidatePortalUserId = getCandidatePortal_FDD_UserName(driver);
			String fddpassword = getCandidatePortal_FDD_Password(driver);
			if ((buildUrl.charAt(buildUrl.length() - 1)) == '/') {
				fddurl = config.get("buildUrl").concat("candidate.jsp");
			} else {
				fddurl = config.get("buildUrl").concat("/candidate.jsp");
			}
			String appSubject = fc.utobj().generateTestData("Schedule Appointment Subject");
			driver.navigate().to(fddurl);
			fc.utobj().printTestStep("enter user name and password > login");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//input[@name='userID']"),
					candidatePortalUserId);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//input[@name='password']"),
					fddpassword);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//input[@value='Login']"));

			try {
				fc.utobj().clickElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'My Dashboard')]"));
			} catch (Exception e) {
			}
			fc.utobj().printTestStep("Click on Schedule Appointment");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'Schedule Appointment')]"));

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='subject']"), appSubject);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='description']"), appSubject);
			// fc.utobj().sendKeys(driver, pobjnew.scheduleAppDescription,
			// fc.utobj().generateTestData("Schedule Appointment Decription"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='Submit']"));

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//input[@value='Close']"));

			fc.utobj().switchFrameToDefault(driver);
			boolean appointmentScheduled = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + appSubject + "')]");
			if (appointmentScheduled == false) {
				fc.utobj().throwsException("Appointment scheduled not visible at candidate jsp page");
			}

			driver = fc.loginpage().login(driver);
			fc.sales().sales_common().fsModule(driver);
			fsmod.leadManagement(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[contains(text(),'" + ld.getLeadFullName() + "')]"));
			fc.utobj().clickElement(driver, pobj.primaryInfo);
			FSLeadSummaryPrimaryInfoPageTest pobj1 = new FSLeadSummaryPrimaryInfoPageTest();
			driver = pobj1.gotoActivityHistorySection(driver);

			boolean isRemarkPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text () ,'" + ld.getLeadFullName() + " logged into candidate portal')]");
			if (isRemarkPresent == false) {
				fc.utobj().throwsException("At Activity History logged into candidate portal remarks not created");
			}

			fc.utobj().switchFrameToDefault(driver);
			// fsmod.calendar(driver);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[@original-title='Manage Appointments/Meetings/Events']"));
			/*
			 * boolean isCalendorTaskPresent= fc.utobj().verifyCase(driver,
			 * ".//a[contains(text(),'"+appSubject+"')]");
			 */

			boolean isCalendorTaskPresent = fc.utobj().assertPageSource(driver, ld.getLeadFullName());

			/*
			 * boolean isCalendorTaskPresent= fc.utobj().verifyCase(driver,
			 * ".//*[contains(text(),'"+firstName+" "+lastName+
			 * "')]/ancestor::td/span[1]/a[contains(text(),'Schedule Appointment')]"
			 * ); if (isCalendorTaskPresent== false) {
			 * fc.utobj().throwsException(
			 * "Appointmet Scheduled not showing in calendor page"); }
			 */
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "FC_Sales_CandidatePortal_003", "sales" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-12", testCaseId = "FC_Sales_CandidatePortal_003", testCaseDescription = "Verify the email sent to owner on post a message and edit message")
	void verifyCandidatePortal003() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			Sales fsmod = new Sales();

			fc.sales().sales_common().fsModule(driver);
			LeadSummaryUI pobj = new LeadSummaryUI(driver);
			String firstName = fc.utobj().generateTestData(dataSet.get("FirstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("LastName"));
			AdminUsersManageCorporateUsersAddCorporateUserPageTest p4 = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "salesautomation@staffex.com";

			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			p4.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Add a lead");
			fc.sales().sales_common().fsModule(driver);
			addLeadSummaryWithLeadNameOwnerName(driver, firstName, lastName, corpUser.getuserFullName());
			fc.utobj().clickElement(driver, pobj.candidatePortalBtn);
			fc.utobj().printTestStep("Go to  Candidate Portal tab > Take user name and password");
			fc.utobj().printTestStep("open candidate portal jsp page");

			fc.utobj().clickElement(driver, pobj.showleadDetailsBtn);

			String candidatePortalUserId = getCandidatePortal_FDD_UserName(driver);
			String fddpassword = getCandidatePortal_FDD_Password(driver);
			String buildUrl = config.get("buildUrl");
			String fddurl = null;
			if ((buildUrl.charAt(buildUrl.length() - 1)) == '/') {
				fddurl = config.get("buildUrl").concat("candidate.jsp");
			} else {
				fddurl = config.get("buildUrl").concat("/candidate.jsp");
			}

			String message = fc.utobj().generateTestData("Message");
			String newmessage = fc.utobj().generateTestData("Message");
			fc.utobj().printTestStep("enter user name and password login");
			driver.navigate().to(fddurl);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//input[@name='userID']"),
					candidatePortalUserId);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//input[@name='password']"),
					fddpassword);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//input[@value='Login']"));

			try {
				fc.utobj().clickElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'My Dashboard')]"));
			} catch (Exception e) {
			}
			fc.utobj().printTestStep("Post a Message");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//textarea[@name='message']"), message);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//input[@value='Post Message']"));

			boolean isMessagePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + message + "')]");
			if (isMessagePresent == false) {
				fc.utobj().throwsException("Message not posted at candidate page");
			}
			fc.utobj().printTestStep("Edit Message");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[contains(text(),'" + message
					+ "')]/ancestor::tr/following-sibling::tr/td/span/a/u[contains(text(),'Edit')]"));
			fc.utobj().sendKeys(driver,
					fc.utobj().getElementByXpath(driver, ".//textarea[contains(text(),'" + message + "')]"),
					newmessage);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//input[@value='Comment']"));

			isMessagePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + newmessage + "')]");
			if (isMessagePresent == false) {
				fc.utobj().throwsException("Message not posted at candidate page");
			}
			driver = fc.loginpage().login(driver);
			fc.sales().sales_common().fsModule(driver);
			fsmod.leadManagement(driver);
			/*
			 * fc.utobj().sendKeys(driver, pobj.leadSearch, leadFullName);
			 * fc.utobj().clickElement(driver, pobj.leadSearchBtn);
			 */
			FSSearchPageTest p3 = new FSSearchPageTest();
			p3.searchByLeadName(driver, firstName, lastName);
			WebElement contactlink = driver
					.findElement(By.xpath(".//a[contains(text(),'" + firstName + " " + lastName + "')]"));

			try {
				fc.utobj().clickElement(driver, contactlink);
			} catch (Exception e) {
				contactlink.click();
			}

			fc.utobj().clickElement(driver, pobj.candidatePortalBtn);

			boolean isMessagePresentInTab = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text(),'" + newmessage + "')]");
			if (isMessagePresentInTab == false) {
				fc.utobj().throwsException("Message not posted at candidate page");
			}

			fc.utobj().printTestStep("Verifying new message post email");
			String expectedSubject = "New Post Email";
			// String emailId="salesautomation@staffex.com";
			String expectedMessageBody = firstName + " " + lastName;

			Map<String, String> mailData = fc.utobj().readMailBox(expectedSubject, expectedMessageBody, emailId,
					"sdg@1a@Hfs");
			System.out.println(mailData);
			if (!mailData.get("mailBody").contains(firstName)) {
				fc.utobj().throwsSkipException("was not able to verify Lead First Name in owner assignment email");
			}

			if (!mailData.get("mailBody").contains(lastName)) {
				fc.utobj().throwsSkipException("was not able to verify lead Last Name in owner assignment email");
			}
			if (!mailData.get("mailBody").contains(newmessage)) {
				fc.utobj().throwsSkipException("was not able to verify lead Message posted in owner assignment email");
			}
			if (!mailData.get("subject").contains(expectedSubject)) {
				fc.utobj().throwsSkipException("was not able to verify lead Last Name in owner assignment email");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales", "sales_failed" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-07-19", testCaseId = "FC_Sales_CandidatePortal_004", testCaseDescription = "Verify the post is getting added / deleted  from Candidate Portal jsp and Candidate Portal tab of the lead")
	void verifyCandidatePortal004() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.sales().sales_common().fsModule(driver);
			Sales fsmod = new Sales();

			LeadSummaryUI pobj = new LeadSummaryUI(driver);
			String firstName = fc.utobj().generateTestData(dataSet.get("FirstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("LastName"));
			String leadFullName = firstName + " " + lastName;
			String userName = "FranConnect Administrator";
			fc.utobj().printTestStep("Add a lead");
			Map<String, String> leadName = addLeadSummaryWithLeadNameOwnerName(driver, firstName, lastName, userName);
			fc.utobj().clickElement(driver, pobj.candidatePortalBtn);
			fc.utobj().printTestStep("Go to  Candidate Portal tab > Take user name and password");
			fc.utobj().printTestStep("open candidate portal jsp page");

			fc.utobj().clickElement(driver, pobj.showleadDetailsBtn);

			String candidatePortalUserId = getCandidatePortal_FDD_UserName(driver);
			String fddpassword = getCandidatePortal_FDD_Password(driver);

			String buildUrl = config.get("buildUrl");
			String fddurl = null;
			if ((buildUrl.charAt(buildUrl.length() - 1)) == '/') {
				fddurl = config.get("buildUrl").concat("candidate.jsp");
			} else {
				fddurl = config.get("buildUrl").concat("/candidate.jsp");
			}
			String message = fc.utobj().generateTestData("Message");
			String newmessage = fc.utobj().generateTestData("Message");

			driver.navigate().to(fddurl);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//input[@name='userID']"),
					candidatePortalUserId);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//input[@name='password']"),
					fddpassword);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//input[@value='Login']"));

			try {
				fc.utobj().clickElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'My Dashboard')]"));
			} catch (Exception e) {
			}
			fc.utobj().printTestStep("Post a Message");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//textarea[@name='message']"), message);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//input[@value='Post Message']"));
			boolean isMessagePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + message + "')]");
			if (isMessagePresent == false) {
				fc.utobj().throwsException("Message not posted at candidate page");
			}

			driver = fc.loginpage().login(driver);
			fc.sales().sales_common().fsModule(driver);
			fsmod.leadManagement(driver);
			FSSearchPageTest p3 = new FSSearchPageTest();
			p3.searchByLeadName(driver, firstName, lastName);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[contains(text(),'" + firstName + " " + lastName + "')]"));
			FSLeadSummaryPrimaryInfoPageTest pobj1 = new FSLeadSummaryPrimaryInfoPageTest();
			fc.utobj().clickElement(driver, pobj.candidatePortalBtn);
			boolean isMessagePresentInTab = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + message + "')]");
			if (isMessagePresentInTab == false) {
				fc.utobj().throwsException("Message not posted at candidate page");
			}
			driver.navigate().to(fddurl);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//input[@name='userID']"),
					candidatePortalUserId);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//input[@name='password']"),
					fddpassword);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//input[@value='Login']"));

			try {
				fc.utobj().clickElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'My Dashboard')]"));
			} catch (Exception e) {
			}
			fc.utobj().printTestStep("Delete this  Message");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[contains(text(),'" + message
					+ "')]/ancestor::tr/following-sibling::tr/td/span/a/u[contains(text(),'Delete')]"));
			fc.utobj().acceptAlertBox(driver);
			isMessagePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + message + "')]");
			if (isMessagePresent == true) {
				fc.utobj().throwsException("Message not posted at candidate page");
			}
			driver = fc.loginpage().login(driver);
			fc.sales().sales_common().fsModule(driver);
			fsmod.leadManagement(driver);
			p3.searchByLeadName(driver, firstName, lastName);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[contains(text(),'" + firstName + " " + lastName + "')]"));
			fc.utobj().clickElement(driver, pobj.candidatePortalBtn);
			isMessagePresentInTab = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + message + "')]");
			if (isMessagePresentInTab == true) {
				fc.utobj().throwsException("Message not posted at candidate page");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales" , "FC_Sales_CandidatePortal_005" }) // Verified // Akshat
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-12", testCaseId = "FC_Sales_CandidatePortal_005", testCaseDescription = "Verify the remarks is getting added in the Candidate Portal Jsp and Candidate Portal tab of the lead.")
	void verifyCandidatePortal005() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			Sales fsmod = new Sales();

			fc.sales().sales_common().fsModule(driver);
			LeadSummaryUI pobj = new LeadSummaryUI(driver);
			String firstName = fc.utobj().generateTestData(dataSet.get("FirstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("LastName"));
			String userName = "FranConnect Administrator";
			fc.utobj().printTestStep("Add a lead");
			Map<String, String> leadName = addLeadSummaryWithLeadNameOwnerName(driver, firstName, lastName, userName);
			fc.utobj().clickElement(driver, pobj.candidatePortalBtn);
			fc.utobj().printTestStep("Go to  Candidate Portal tab > Take user name and password");

			fc.utobj().clickElement(driver, pobj.showleadDetailsBtn);

			String candidatePortalUserId = getCandidatePortal_FDD_UserName(driver);
			String fddpassword = getCandidatePortal_FDD_Password(driver);
			fc.utobj().printTestStep("open candidate portal jsp page");
			String buildUrl = config.get("buildUrl");
			String fddurl = null;
			if ((buildUrl.charAt(buildUrl.length() - 1)) == '/') {
				fddurl = config.get("buildUrl").concat("candidate.jsp");
			} else {
				fddurl = config.get("buildUrl").concat("/candidate.jsp");
			}
			String remarks = fc.utobj().generateTestData("Remarks");

			driver.navigate().to(fddurl);
			fc.utobj().printTestStep("enter user name and password login");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//input[@name='userID']"),
					candidatePortalUserId);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//input[@name='password']"),
					fddpassword);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//input[@value='Login']"));

			try {
				fc.utobj().clickElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'My Dashboard')]"));
			} catch (Exception e) {
			}
			fc.utobj().printTestStep("Add Remarks and Complete");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[@title='Add Remarks']/ancestor::tr/td[1][not(contains(text(),'Locked'))]/following-sibling::td/a[@title='Add Remarks']"));

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='activityComments']"), remarks);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//input[@value='Add']"));

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//input[@value='Close']"));

			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[@title='Add Remarks']/ancestor::tr/td[1][not(contains(text(),'Locked'))]/following-sibling::td/a[@title='View Remarks']"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			boolean isreamrkAdded = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + remarks + "')]");
			if (isreamrkAdded == false) {
				fc.utobj().throwsException("Remarks not added at candidate jsp");
			}
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//input[@value='Close']"));
			fc.utobj().switchFrameToDefault(driver);

			driver = fc.loginpage().login(driver);
			fc.sales().sales_common().fsModule(driver);
			fsmod.leadManagement(driver);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[contains(text(),'" + firstName + " " + lastName + "')]"));
			fc.utobj().clickElement(driver, pobj.candidatePortalBtn);

			fc.utobj().clickElement(driver, pobj.veiwRemarksInitialCall);

			fc.commonMethods().switch_cboxIframe_frameId(driver);

			boolean isRemarkPresentInTab = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + remarks + "')]");
			if (isRemarkPresentInTab == false) {
				fc.utobj().throwsException("Message not posted at candidate page");
			}
			fc.utobj().clickElement(driver, pobj.cboxCloseBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salescheck", "sales", "sales_failed" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-12", testCaseId = "FC_Sales_CandidatePortal_006", testCaseDescription = "Verify the Post is displayed in the Candidate Portal Jsp of the lead")
	void verifyCandidatePortal006() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.sales().sales_common().fsModule(driver);
			LeadSummaryUI pobj = new LeadSummaryUI(driver);
			String firstName = fc.utobj().generateTestData(dataSet.get("FirstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("LastName"));
			String userName = "FranConnect Administrator";
			String message = fc.utobj().generateTestData("Message");
			fc.utobj().printTestStep("Add a lead");
			addLeadSummaryWithLeadNameOwnerName(driver, firstName, lastName, userName);
			fc.utobj().clickElement(driver, pobj.candidatePortalBtn);
			fc.utobj().printTestStep("Go to  Candidate Portal tab > Post a Message > Take user name and password");
			fc.utobj().printTestStep("open candidate portal jsp page");
			fc.utobj().sendKeys(driver, pobj.candidatePortalMessage, message);
			fc.utobj().clickElement(driver, pobj.postMessage);

			boolean isMessageAdded = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + message + "')]");
			if (isMessageAdded == false) {
				fc.utobj().throwsException("Remarks not added at candidate jsp");
			}
			fc.utobj().clickElement(driver, pobj.showleadDetailsBtn);

			String candidatePortalUserId = getCandidatePortal_FDD_UserName(driver);
			String fddpassword = getCandidatePortal_FDD_Password(driver);

			String buildUrl = config.get("buildUrl");
			String fddurl = null;
			if ((buildUrl.charAt(buildUrl.length() - 1)) == '/') {
				fddurl = config.get("buildUrl").concat("candidate.jsp");
			} else {
				fddurl = config.get("buildUrl").concat("/candidate.jsp");
			}
			driver.navigate().to(fddurl);
			fc.utobj().printTestStep("enter user name and password >login");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//input[@name='userID']"),
					candidatePortalUserId);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//input[@name='password']"),
					fddpassword);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//input[@value='Login']"));

			try {
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='cboxClose"));
			} catch (Exception e) {
			}
			boolean isreamrkAdded = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + message + "')]");
			if (isreamrkAdded == false) {
				fc.utobj().throwsException("Mesaage not posted at candidate jsp");
			}
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salecan", "sales" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "FC_Sales_CandidatePortal_007", testCaseDescription = " Candidate Portal tab > Quick Link > Send Email")
	void verifyCandidatePortal007() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.sales().sales_common().fsModule(driver);
			LeadSummaryUI pobj = new LeadSummaryUI(driver);
			String firstName = fc.utobj().generateTestData(dataSet.get("FirstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("LastName"));

			String userName = "FranConnect Administrator";
			fc.utobj().printTestStep("Add a lead");
			addLeadSummaryWithLeadNameOwnerName(driver, firstName, lastName, userName);
			fc.utobj().clickElement(driver, pobj.candidatePortalBtn);

			fc.utobj().printTestStep("Go to  Candidate Portal tab > Quick Link");
			fc.utobj().printTestStep("Send Emai > Fill Details > Add Keywords > Send Email");
			fc.utobj().clickElement(driver, pobj.sendEmailQuickLinks);

			String windowHandle2 = driver.getWindowHandle();
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String emailSubject = fc.utobj().generateTestData("Email Subject");
			fc.utobj().sendKeys(driver, pobj.emailSubject, emailSubject);
			driver.switchTo().frame(fc.utobj().getElement(driver, pobj.htmlFrame));
			String mailBody = "Dear $FIRST_NAME$ $LAST_NAME$  $FIRST_NAME_33_1$ $LAST_NAME_33_1$ $ADDRESS1_33_1$  $ADDRESS2_33_1$ $CITY_33_1$ $COUNTRY_33_1$ $STATE_PROVINCE_33_1$ $CENTER_NAME_1_1$"
					+ "$STREET_ADDRESS_1_1$ $CITY_1_1$ $STATE_PROVINCE_1_1$ $ZIP_POSTAL_CODE_1_1$ $EMAIL_1_1$ $FAX_1_1$ $PHONE_1_1$ $MOBILE_1_1$ $FIRST_NAME_1_2$ $LAST_NAME_1_2$"
					+ "$OWNERS_FIRST_NAME$ $OWNERS_LAST_NAME$ $OWNERS_TITLE$ $OWNERS_ADDRESS$ $OWNERS_CITY$ $OWNERS_STATE$  $OWNERS_ZIP$ $OWNERS_EMAIL$  $OWNERS_PHONE$ "
					+ "$OWNERS_PHONE_EXTENSION$ $OWNERS_MOBILE$ $OWNERS_FAX$ $OWNER_SIGNATURE$ $SENDER_NAME$ 111111";

			fc.utobj().sendKeys(driver, pobj.mailText, mailBody);
			driver.switchTo().window(windowHandle2);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.sendBtn);
			fc.utobj().clickElement(driver, pobj.closeRemarksBtn);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, pobj.primaryInfo);
			FSLeadSummaryPrimaryInfoPageTest pobj1 = new FSLeadSummaryPrimaryInfoPageTest();
			driver = pobj1.gotoActivityHistorySection(driver);
			boolean isRemarkPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text () ,'" + emailSubject + "')]");
			if (isRemarkPresent == false) {
				fc.utobj().throwsException("At Activity History Quick Links Email Remarks Not Created");
			}
			fc.utobj().printTestStep("Verifying new lead addition mail");
			String emailId = "salesautomation@staffex.com";
			String expectedSubject = emailSubject;
			String expectedMessageBody = firstName;

			Map<String, String> mailData = fc.utobj().readMailBox(expectedSubject, expectedMessageBody, emailId,
					"sdg@1a@Hfs");
			System.out.println(mailData);
			if (!mailData.get("mailBody").contains(firstName)) {
				fc.utobj().throwsSkipException("was not able to verify Lead First Name in owner assignment email");
			}
			if (!mailData.get("subject").contains(expectedSubject)) {
				fc.utobj().throwsSkipException("was not able to verify lead Last Name in owner assignment email");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales" , "FC_Sales_CandidatePortal_008"}) // fixed : verified // Akshat
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-15", testCaseId = "FC_Sales_CandidatePortal_008", testCaseDescription = "Verify logo and title in candidate portal of lead")
	void verifyCandidatePortal008() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			Sales fsmod = new Sales();
			fc.utobj().printTestStep("Go to Admin > Create Division");
			LeadSummaryUI pobj = new LeadSummaryUI(driver);
			fc.adminpage().adminPage(driver);

			fc.utobj().clickLink(driver, "Configure New Hierarchy Level");

			try {
				fc.utobj().clickElement(driver, pobj.configureNewHierarchyLevelYes);
			} catch (Exception e) {
			}
			fc.utobj().clickElement(driver, pobj.submitBtn1);

			fc.adminpage().adminPage(driver);

			String divisionName = fc.utobj().generateTestData("Division Name");
			fc.utobj().clickLink(driver, "Add Division");
			fc.utobj().sendKeys(driver, pobj.divisionName, divisionName);
			fc.utobj().clickElement(driver, pobj.submitBtn);

			try {
				fc.utobj().clickElement(driver, pobj.showAllButton);
			} catch (Exception e) {
			}
			fc.utobj().printTestStep("and then on Action Menu > Click Configure Candidate Portal Logo > Upload image ");
			fc.utobj().actionImgOption_WithoutTranslation(driver, divisionName,"Configure Division Logo");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			
			/*// click on 'Click Here'
			fc.utobj().clickElement(driver, fc.utobj().getElementByClassName(driver, "browseBtn"));
			
			// Akshat
			StringSelection ss = new StringSelection("C:\\Selenium_Test_Input\\testData\\document\\pictureFile.jpg");
			fc.utobj().sleep();
			Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);
						
			Robot robot = new Robot();
			robot.setAutoDelay(1000);
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_CONTROL);
			robot.setAutoDelay(2000);
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
			robot.setAutoDelay(1000);*/
			
			String fileName = fc.utobj().getFilePathFromTestData(dataSet.get("fileName"));
			System.out.println(fileName);
			
			/*Robot robot = new Robot();
			robot.*/
			
			// Does not work anymore due to changes in DOM using new API to Drop Logo Files
			fc.utobj().sendKeys(driver, fc.utobj().getElementByID(driver, "divisionLogo") , fileName);
			
			fc.utobj().clickElement(driver, pobj.saveCboxButton);
			fc.utobj().sleep();
			fc.utobj().switchFrameToDefault(driver);
			
			String userName = "FranConnect Administrator";
			String firstName = fc.utobj().generateTestData("FirstName");
			String lastName = fc.utobj().generateTestData("FirstName");
			String leadFullName = firstName + " " + lastName;
			String country = "USA";
			String state = "Alabama";
			String emailId = "salesautomation@staffex.com";
			String leadOwner = "FranConnect Administrator";
			String leadSourceCategory = "Import";
			String leadSourceDetails = "None";
			
			fc.sales().sales_common().fsModule(driver);
			String message = fc.utobj().generateTestData("Message");
			fc.utobj().printTestStep("Add a lead > Go to Candidate Portal tab > Take user name and password");
			// Map<String,String> leadName =
			// addLeadSummaryWithLeadNameOwnerName(driver,config,firstName,lastName,userName);
			fsmod.leadManagement(driver);

			clickAddLeadLink(driver);
			FSLeadSummaryAddLeadPage pobj1 = new FSLeadSummaryAddLeadPage(driver);
			fc.utobj().sendKeys(driver, pobj1.firstName, firstName);
			fc.utobj().sendKeys(driver, pobj1.lastName, lastName);
			fc.utobj().selectDropDownByVisibleText(driver, pobj1.country, country);

			fc.utobj().selectDropDownByVisibleText(driver, pobj1.state, state);
			fc.utobj().sendKeys(driver, pobj1.emailID, emailId);
			fc.utobj().sendKeys(driver, pobj1.zip, "123456");
			fc.utobj().sendKeys(driver, pobj1.phone, "12345678");
			fc.utobj().sendKeys(driver, pobj1.address, "New Highway Road");
			fc.utobj().selectDropDownByVisibleText(driver, pobj1.leadOwnerID, leadOwner);
			fc.utobj().selectDropDownByVisibleText(driver, pobj1.leadSource2ID, leadSourceCategory);

			fc.utobj().selectDropDownByVisibleText(driver, pobj1.leadSource3ID, leadSourceDetails);

			fc.utobj().selectDropDownByVisibleText(driver, pobj1.divisionDropDown, divisionName);
			fc.utobj().clickElement(driver, pobj1.save);

			fc.utobj().clickElement(driver, pobj.candidatePortalBtn);
			fc.utobj().clickElement(driver, pobj.showleadDetailsBtn);

			String candidatePortalUserId = getCandidatePortal_FDD_UserName(driver);
			String fddpassword = getCandidatePortal_FDD_Password(driver);

			// String
			// candidatePortalUserId=fc.utobj().getElementByXpath(driver,".//*[@id='printReady0']//td//strong[contains(text(),'Candidate
			// Portal
			// Password')]/ancestor::tr/td[2][@class='bText12']")).getText();
			// String
			// fddpassword=fc.utobj().getElementByXpath(driver,".//*[@id='printReady0']//td//strong[contains(text(),'Candidate
			// Portal
			// Password')]/ancestor::td/following-sibling::td/table/tbody/tr/td")).getText();
			
			fc.utobj().printTestStep("login into candidate portal of lead with this credential");
			String buildUrl = config.get("buildUrl");
			String fddurl = null;
			if ((buildUrl.charAt(buildUrl.length() - 1)) == '/') {
				fddurl = config.get("buildUrl").concat("candidate.jsp");
			} else {
				fddurl = config.get("buildUrl").concat("/candidate.jsp");
			}
			driver.navigate().to(fddurl);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//input[@name='userID']"),
					candidatePortalUserId);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//input[@name='password']"),
					fddpassword);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//input[@value='Login']"));

			try {
				fc.utobj().clickElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'My Dashboard')]"));
			} catch (Exception e) {

			}
			WebElement logofield = driver
					.findElement(By.xpath(".//ul[@class='multibrandLogo']/li/img[@title='" + divisionName + "']"));
			fc.utobj().moveToElement(driver, logofield);
			String logo = logofield.getAttribute("src").trim();
			System.out.println(logo);

			// Validation - Validate the Logo on Candidate Portal page 
			if(! (logo.contains("Apple_logo_black")))
			{
				fc.utobj().throwsException("Logo mismatch on External Candidate Portal");
			}
			
			/*if (!(logo.indexOf("Apple_logo_black") != -1)) {
				fc.utobj().throwsSkipException("was not able to verify mailbody/keywords");
			}*/
			
			boolean isTitlepresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text(),'" + divisionName + "')]");
			if (isTitlepresent == false) {
				fc.utobj().throwsException("Title not verfied at candidate.jsp");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesfail", "sales" , "FC_Sales_CandidatePortal_009" }) // Fixed : Verified // Akshat
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-07-19", testCaseId = "FC_Sales_CandidatePortal_009", testCaseDescription = "Verify the acceptable file format for logo in candiate portal")
	void verifyCandidatePortal009() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			Sales fsmod = new Sales();
			fc.utobj().printTestStep("Go to Admin > Create Division");
			LeadSummaryUI pobj = new LeadSummaryUI(driver);
			fc.adminpage().adminPage(driver);

			fc.utobj().clickLink(driver, "Configure New Hierarchy Level");

			fc.utobj().clickElement(driver, pobj.submitBtn1);

			fc.adminpage().adminPage(driver);

			String divisionName = fc.utobj().generateTestData("Division Name");
			fc.utobj().clickLink(driver, "Add Division");
			fc.utobj().sendKeys(driver, pobj.divisionName, divisionName);
			fc.utobj().clickElement(driver, pobj.submitBtn);

			try {
				fc.utobj().clickElement(driver, pobj.showAllButton);
			} catch (Exception e) {
			}
			fc.utobj().printTestStep("and then on Action Menu > Click Configure Candidate Portal Logo > Upload image");
			fc.utobj().actionImgOption(driver, divisionName, "Configure Division Logo");
			fc.utobj().printTestStep("Check for JPG; JPEG; PNG; GIF; ICO");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String fileName = fc.utobj().getFilePathFromTestData(dataSet.get("fileName"));
			fc.utobj().sendKeys(driver, pobj.divisionLogo, fileName);

			boolean isJpgFormat = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//div[contains(text(),'jpg')]");
			if (isJpgFormat == false) {
				fc.utobj().throwsException("jpg file type not uploaded properly");
			}

			String fileName1 = fc.utobj().getFilePathFromTestData(dataSet.get("fileName1"));
			fc.utobj().sendKeys(driver, pobj.divisionLogo, fileName1);

			boolean isPngFormat = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//div[contains(text(),'png')]");
			if (isPngFormat == false) {
				fc.utobj().throwsException("png file type not uploaded properly");
			}
			String fileName2 = fc.utobj().getFilePathFromTestData(dataSet.get("fileName2"));
			fc.utobj().sendKeys(driver, pobj.divisionLogo, fileName2);

			boolean isJpegFormat = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//div[contains(text(),'jpeg')]");
			if (isJpegFormat == false) {
				fc.utobj().throwsException("Jpeg file type not uploaded properly");
			}

			/*
			 * String fileName3=fc.utobj().getFilePathFromTestData(config,
			 * dataSet.get("fileName3")); fc.utobj().sendKeys(driver,
			 * pobj.divisionLogo, fileName3);
			 * 
			 * boolean isIcoFormat=fc.utobj().verifyCase(driver,
			 * ".//div[contains(text(),'ico')]"); if(isIcoFormat==false) {
			 * fc.utobj().throwsException("ico file type not getting uploaded");
			 * }
			 */

			String fileName4 = fc.utobj().getFilePathFromTestData(dataSet.get("fileName4"));
			fc.utobj().sendKeys(driver, pobj.divisionLogo, fileName4);

			boolean isGifFormat = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//div[contains(text(),'gif')]");
			if (isGifFormat == false) {
				fc.utobj().throwsException("gif file type not uploaded properly");
			}

			fc.utobj().clickElement(driver, pobj.saveCboxButton);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			// e.printStackTrace();
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales", "sales_failed" , "FC_Sales_CandidatePortal_010" }) // Fixed : Verified // Akshat
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-07-11", testCaseId = "FC_Sales_CandidatePortal_010", testCaseDescription = "Verify the logo for candiate portal is not accepting other than acceptable file format")
	void verifyCandidatePortal_10() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Go to Admin > Create Division");
			LeadSummaryUI pobj = new LeadSummaryUI(driver);
			fc.adminpage().adminPage(driver);

			fc.utobj().clickLink(driver, "Configure New Hierarchy Level");

			try {
				fc.utobj().clickElement(driver, pobj.configureNewHierarchyLevelYes);
			} catch (Exception e) {
			}
			fc.utobj().clickElement(driver, pobj.submitBtn1);

			fc.adminpage().adminPage(driver);

			String divisionName = fc.utobj().generateTestData("Division Name");
			fc.utobj().clickLink(driver, "Add Division");
			fc.utobj().sendKeys(driver, pobj.divisionName, divisionName);
			fc.utobj().clickElement(driver, pobj.submitBtn);

			try {
				fc.utobj().clickElement(driver, pobj.showAllButton);
			} catch (Exception e) {
			}
			fc.utobj().printTestStep("and then on Action Menu > Click Configure Candidate Portal Logo > Upload image");
			fc.utobj().printTestStep("Check for other than JPG; JPEG; PNG; GIF; ICO files");
			fc.utobj().actionImgOption(driver, divisionName, "Configure Division Logo");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			String fileName = fc.utobj().getFilePathFromTestData(dataSet.get("fileName"));
			fc.utobj().sendKeys(driver, pobj.divisionLogo, fileName);

			boolean isJpegFormat = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//div[contains(text(),'pdf");
			if (isJpegFormat == true) {
				fc.utobj().throwsException("Jpeg file type not uploaded properly");
			}
			fc.utobj().clickElement(driver, pobj.saveCboxButton);
			fc.utobj().acceptAlertBox(driver);
			fc.utobj().clickElement(driver, pobj.cboxCloseBtn1);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "FC_Sales_CandidatePortal_012", "sales" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-15", testCaseId = "FC_Sales_CandidatePortal_012", testCaseDescription = "Verify the Franchising Granting activity is present for the lead.")
	void verifyCandidatePortal012() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			Sales fsmod = new Sales();

			LeadSummaryUI pobj = new LeadSummaryUI(driver);

			String divisionName = fc.utobj().generateTestData("Division Name");
			String actitvityName = fc.utobj().generateTestData("ActitvityName");
			String Description = fc.utobj().generateTestData("Description");
			AdminDivisionAddDivisionPageTest divisionpage = new AdminDivisionAddDivisionPageTest();

			divisionpage.addDivision(driver, divisionName);

			fc.adminpage().adminPage(driver);
			fc.utobj().printTestStep("Manage Candidate Portal > Franchise Granting Activity > Add new activity");
			fc.utobj().clickLink(driver, "Manage Candidate Portal");
			fc.utobj().clickElement(driver, pobj.addNewActivityBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.activityName, actitvityName);
			fc.utobj().sendKeys(driver, pobj.activityDescription, Description);
			fc.utobj().sendKeys(driver, pobj.activityLink, actitvityName);
			fc.utobj().sendKeys(driver, pobj.documentTitle, "Document_Title" + fc.utobj().generateRandomNumber());
			String fileName = fc.utobj().getFilePathFromTestData(dataSet.get("fileName1"));
			fc.utobj().sendKeys(driver, pobj.documentName, fileName);
			fc.utobj().selectValFromMultiSelect(driver, pobj.activityDivision, divisionName);
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().printTestStep("modify and check if the division is appearing or not.");
			fc.utobj().actionImgOption(driver, actitvityName, "Modify");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			// String
			// divisionValue=fc.utobj().getAttributeValue(fc.utobj().getElementByXpath(driver,".//*[@id='ms-parentbrandDivisionID']/button/span")),
			// "value");

			boolean divisionValue = fc.utobj().assertPageSource(driver, divisionName);
			System.out.println(divisionName + " -----------" + divisionValue);

			if (divisionValue == false) {
				fc.utobj().throwsException("Activity Division value not present after modifying");
			}

			/*
			 * if (!(divisionName==divisionValue)) { fc.utobj().throwsException(
			 * "Activity Division value not present after modifying"); }
			 */
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().moveToElement(driver, fc.utobj().getElementByXpath(driver, ".//div[@class='header-logo']"));
			fc.sales().sales_common().fsModule(driver);

			fc.utobj().printTestStep("Add a lead with same division");
			String userName = "FranConnect Administrator";
			String firstName = fc.utobj().generateTestData("FirstName");
			String lastName = fc.utobj().generateTestData("FirstName");
			String leadFullName = firstName + " " + lastName;
			String country = "USA";
			String state = "Alabama";
			String emailId = "salesautomation@staffex.com";
			String leadOwner = "FranConnect Administrator";
			String leadSourceCategory = "Import";
			String leadSourceDetails = "None";

			fsmod.leadManagement(driver);

			clickAddLeadLink(driver);
			FSLeadSummaryAddLeadPage pobj1 = new FSLeadSummaryAddLeadPage(driver);
			fc.utobj().sendKeys(driver, pobj1.firstName, firstName);
			fc.utobj().sendKeys(driver, pobj1.lastName, lastName);
			fc.utobj().selectDropDownByVisibleText(driver, pobj1.country, country);

			fc.utobj().selectDropDownByVisibleText(driver, pobj1.state, state);
			fc.utobj().sendKeys(driver, pobj1.emailID, emailId);
			fc.utobj().sendKeys(driver, pobj1.zip, "123456");
			fc.utobj().sendKeys(driver, pobj1.phone, "12345678");
			fc.utobj().sendKeys(driver, pobj1.address, "New Highway Road");
			fc.utobj().selectDropDownByVisibleText(driver, pobj1.leadOwnerID, leadOwner);
			fc.utobj().selectDropDownByVisibleText(driver, pobj1.leadSource2ID, leadSourceCategory);

			fc.utobj().selectDropDownByVisibleText(driver, pobj1.leadSource3ID, leadSourceDetails);

			fc.utobj().selectDropDownByVisibleText(driver, pobj1.divisionDropDown, divisionName);
			fc.utobj().clickElement(driver, pobj1.save);

			fc.utobj().clickElement(driver, pobj.candidatePortalBtn);
			fc.utobj().printTestStep("Go to Candidate Portal > Take User name and password");
			fc.utobj().clickElement(driver, pobj.showleadDetailsBtn);

			String candidatePortalUserId = getCandidatePortal_FDD_UserName(driver);
			String fddpassword = getCandidatePortal_FDD_Password(driver);

			String buildUrl = config.get("buildUrl");
			String fddurl = null;
			if ((buildUrl.charAt(buildUrl.length() - 1)) == '/') {
				fddurl = config.get("buildUrl").concat("candidate.jsp");
			} else {
				fddurl = config.get("buildUrl").concat("/candidate.jsp");
			}

			fc.utobj().printTestStep("login into candidate portal ");
			driver.navigate().to(fddurl);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//input[@name='userID']"),
					candidatePortalUserId);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//input[@name='password']"),
					fddpassword);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//input[@value='Login']"));

			try {
				fc.utobj().clickElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'My Dashboard')]"));
			} catch (Exception e) {

			}
			// String
			// isActivity=fc.utobj().getElementByXpath(driver,".//tr[3]//table[@class='fc-todo-box-table'][1]//tr[1]/td[@class='pb
			// bText12'][1]")).getText();
			boolean isActivity1 = fc.utobj().assertLinkPartialText(driver, actitvityName);
			if (isActivity1 == false) {
				fc.utobj().throwsException("Activity not verfied at candidate portal");
			}
			fc.utobj().quitBrowser(driver, config, testCaseId);
			// fc.utobj().logoutAndQuitBrowser(driver, config, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales" , "FC_Sales_CandidatePortal_013" }) // Verified // Akshat
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-15", testCaseId = "FC_Sales_CandidatePortal_013", testCaseDescription = "Verify Franchise Granting Activity is present based on the division of the lead")
	void verifyCandidatePortal013() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			LeadSummaryUI pobj = new LeadSummaryUI(driver);
			fc.adminpage().adminPage(driver);

			fc.utobj().clickLink(driver, "Configure New Hierarchy Level");

			fc.utobj().clickElement(driver, pobj.submitBtn1);

			fc.adminpage().adminPage(driver);

			fc.utobj().printTestStep("Go to Admin > Add two Division");
			String actitvityName = fc.utobj().generateTestData("ActitvityName");
			String documentTitle = fc.utobj().generateTestData("Documenttitle");
			String divisionName = fc.utobj().generateTestData("Division Name");
			String divisionName1 = fc.utobj().generateTestData("Division Name");
			fc.utobj().clickLink(driver, "Add Division");
			fc.utobj().sendKeys(driver, pobj.divisionName, divisionName);
			fc.utobj().clickElement(driver, pobj.submitBtn);

			try {
				fc.utobj().clickElement(driver, pobj.showAllButton);
			} catch (Exception e) {
			}
			String Description = fc.utobj().generateTestData("ActitvityName Decription");

			fc.utobj().clickElement(driver, pobj.addDivisionBtn);
			// fc.utobj().clickLink(driver, "Add Division");
			fc.utobj().sendKeys(driver, pobj.divisionName, divisionName1);
			fc.utobj().clickElement(driver, pobj.submitBtn);

			try {
				fc.utobj().clickElement(driver, pobj.showAllButton);
			} catch (Exception e) {
			}

			fc.utobj().printTestStep(
					"Go to Admin > Sales > Manage Candidate Portal > Franchise Granting Activity > Add new activity ");
			fc.utobj().printTestStep("Selected one of the division and submit");

			fc.adminpage().adminPage(driver);
			fc.utobj().clickLink(driver, "Manage Candidate Portal");
			fc.utobj().clickElement(driver, pobj.addNewActivityBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.activityName, actitvityName);
			fc.utobj().sendKeys(driver, pobj.activityDescription, Description);
			fc.utobj().sendKeys(driver, pobj.activityLink, actitvityName);
			fc.utobj().sendKeys(driver, pobj.documentTitle, documentTitle);
			String fileName = fc.utobj().getFilePathFromTestData(dataSet.get("fileName"));
			fc.utobj().sendKeys(driver, pobj.documentName, fileName);
			fc.utobj().selectValFromMultiSelect(driver, pobj.activityDivision, divisionName);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			/*
			 * fc.utobj().actionImgOption(driver, actitvityName, "Modify");
			 * 
			 * String
			 * divisionValue=fc.utobj().getAttributeValue(driver.findElement(By.
			 * xpath(".//*[@id='ms-parentbrandDivisionID']/button/span")),
			 * "value"); if (!(divisionName==divisionValue)) {
			 * fc.utobj().throwsException(
			 * "Activity Division value not at after modidifying"); }
			 * fc.utobj().clickElement(driver, pobj.closeBtn);
			 */
			fc.utobj().switchFrameToDefault(driver);
			fc.sales().sales_common().fsModule(driver);

			fc.utobj().printTestStep("Add a lead with different division");
			String userName = "FranConnect Administrator";
			String firstName = fc.utobj().generateTestData("FirstName");
			String lastName = fc.utobj().generateTestData("FirstName");
			String leadFullName = firstName + " " + lastName;
			String country = "USA";
			String state = "Alabama";
			String emailId = "salesautomation@staffex.com";
			String leadOwner = "FranConnect Administrator";
			String leadSourceCategory = "Import";
			String leadSourceDetails = "None";

			Map<String, String> leadInfo = new HashMap<String, String>();
			leadInfo.put("firstName", firstName);
			leadInfo.put("lastName", lastName);
			leadInfo.put("country", country);
			leadInfo.put("state", state);
			leadInfo.put("email", emailId);
			leadInfo.put("leadOwner", leadOwner);
			leadInfo.put("leadSourceCategory", leadSourceCategory);
			leadInfo.put("leadSourceDetails", leadSourceDetails);
			leadInfo.put("division", divisionName1);
			addLead(driver, config, leadInfo);

			/*
			 * fsmod.leadManagement(driver);
			 * 
			 * clickAddLeadLink(driver); FSLeadSummaryAddLeadPage pobj1 = new
			 * FSLeadSummaryAddLeadPage(driver); fc.utobj().sendKeys(driver,
			 * pobj1.firstName, firstName); fc.utobj().sendKeys(driver,
			 * pobj1.lastName, lastName);
			 * fc.utobj().selectDropDownByVisibleText(driver, pobj1.country,
			 * country); fc.utobj().selectDropDownByVisibleText(driver,
			 * pobj1.state, state); fc.utobj().sendKeys(driver, pobj1.emailID,
			 * emailId); fc.utobj().sendKeys(driver, pobj1.zip, "123456");
			 * fc.utobj().sendKeys(driver, pobj1.phone, "12345678");
			 * fc.utobj().sendKeys(driver, pobj1.address, "New Highway Road");
			 * fc.utobj().selectDropDownByVisibleText(driver, pobj1.leadOwnerID,
			 * leadOwner); fc.utobj().selectDropDownByVisibleText(driver,
			 * pobj1.leadSource2ID, leadSourceCategory);
			 * fc.utobj().selectDropDownByVisibleText(driver,
			 * pobj1.leadSource3ID, leadSourceDetails);
			 * 
			 * fc.utobj().selectDropDownByVisibleText(driver,
			 * pobj1.divisionDropDown, divisionName1);
			 * fc.utobj().clickElement(driver, pobj1.save);
			 */

			fc.utobj().clickElement(driver, pobj.candidatePortalBtn);
			fc.utobj().printTestStep("Go to Candidate Portal > Take User name and password");
			fc.utobj().clickElement(driver, pobj.showleadDetailsBtn);

			String candidatePortalUserId = getCandidatePortal_FDD_UserName(driver);
			String fddpassword = getCandidatePortal_FDD_Password(driver);
			String buildUrl = config.get("buildUrl");
			String fddurl = null;
			if ((buildUrl.charAt(buildUrl.length() - 1)) == '/') {
				fddurl = config.get("buildUrl").concat("candidate.jsp");
			} else {
				fddurl = config.get("buildUrl").concat("/candidate.jsp");
			}

			fc.utobj().printTestStep("login into candidate portal ");
			driver.navigate().to(fddurl);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//input[@name='userID']"),
					candidatePortalUserId);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//input[@name='password']"),
					fddpassword);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//input[@value='Login']"));

			try {
				fc.utobj().clickElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'My Dashboard')]"));
			} catch (Exception e) {

			}
			boolean isActivity = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + divisionName + "')]");

			if (isActivity == true) {
				fc.utobj().throwsException("Activity not verfied at candidate portal");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales_failed", "sales" , "FC_Sales_CandidatePortal_014" }) // Fixed : Verified // Akshat
	@TestCase(createdOn = "2017-10-03", updatedOn = "2017-10-03", testCaseId = "FC_Sales_CandidatePortal_014", testCaseDescription = "Verify Candidate Portal logo after lead is merged.")
	void verifyCandidatePortal014() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			LeadSummaryUI pobj = new LeadSummaryUI(driver);
			AdminDivisionAddDivisionPageTest divisionpage = new AdminDivisionAddDivisionPageTest();

			String divisionName = fc.utobj().generateTestData(dataSet.get("divisionName"));
			String divisionName2 = fc.utobj().generateTestData(dataSet.get("divisionName2"));

			fc.adminpage().adminPage(driver);

			fc.utobj().clickLink(driver, "Configure New Hierarchy Level");

			fc.utobj().clickElement(driver, pobj.submitBtn1);

			fc.utobj().printTestStep("Add two brands > Configure logo for respective brand");
			divisionpage.addDivision(driver, divisionName);

			try {
				fc.utobj().clickElement(driver, pobj.showAllButton);
			} catch (Exception e) {
			}
			fc.utobj().actionImgOption(driver, divisionName, "Configure Division Logo");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String fileName = fc.utobj().getFilePathFromTestData(dataSet.get("fileName1"));
			System.out.println(fileName);
			fc.utobj().sendKeys(driver, pobj.divisionLogo, fileName);
			fc.utobj().sleep(); // akshat
			/*
			 * boolean isJpegFormat=fc.utobj().verifyCase(driver,
			 * ".//*[contains(text(),'Apple_logo_black");
			 * if(isJpegFormat==false) { //fc.utobj().throwsException(
			 * "Jpeg file type not uploaded properly"); }
			 */
			fc.utobj().clickElement(driver, pobj.saveCboxButton);

			divisionpage.addDivision(driver, divisionName2);

			try {
				fc.utobj().clickElement(driver, pobj.showAllButton);
			} catch (Exception e) {
			}
			fc.utobj().actionImgOption(driver, divisionName2, "Configure Division Logo");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String fileName2 = fc.utobj().getFilePathFromTestData(dataSet.get("fileName"));
			System.out.println(fileName2);
			fc.utobj().sendKeys(driver, pobj.divisionLogo, fileName2);
			fc.utobj().sleep(); // akshat

			boolean isPngFormat = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'png");
			if (isPngFormat == false) {
				// fc.utobj().throwsException("Jpeg file type not uploaded
				// properly");
			}
			fc.utobj().clickElement(driver, pobj.saveCboxButton);

			fc.utobj().printTestStep("Add two leads with different brands > Now merge the lead");
			fc.sales().sales_common().fsModule(driver);
			String userName = "FranConnect Administrator";
			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String firstName2 = fc.utobj().generateTestData(dataSet.get("newfirstName"));
			String lastName2 = fc.utobj().generateTestData(dataSet.get("newlastName"));

			String leadFullName = firstName + " " + lastName;
			String country = "USA";
			String state = "Alabama";
			String emailId = "salesautomation@staffex.com";
			String leadOwner = "FranConnect Administrator";
			String leadSourceCategory = "Import";
			String leadSourceDetails = "None";

			Map<String, String> leadInfo = new HashMap<String, String>();
			leadInfo.put("firstName", firstName);
			leadInfo.put("lastName", lastName);
			leadInfo.put("country", country);
			leadInfo.put("state", state);
			leadInfo.put("email", emailId);
			leadInfo.put("leadOwner", leadOwner);
			leadInfo.put("leadSourceCategory", leadSourceCategory);
			leadInfo.put("leadSourceDetails", leadSourceDetails);
			leadInfo.put("division", divisionName);
			addLead(driver, config, leadInfo);

			Map<String, String> leadInfo2 = new HashMap<String, String>();
			leadInfo2.put("firstName", firstName);
			leadInfo2.put("lastName", lastName);
			leadInfo2.put("country", country);
			leadInfo2.put("state", state);
			leadInfo2.put("email", emailId);
			leadInfo2.put("leadOwner", leadOwner);
			leadInfo2.put("leadSourceCategory", leadSourceCategory);
			leadInfo2.put("leadSourceDetails", leadSourceDetails);
			leadInfo2.put("division", divisionName2);
			addLead(driver, config, leadInfo2);

			FSSearchPageTest p3 = new FSSearchPageTest();
			p3.searchByLeadName(driver, firstName, lastName);
			fc.utobj().clickElement(driver, pobj.checkBoxAll);
			fc.utobj().printTestStep("Merge these two leads");

			fc.utobj().selectActionMenuItems(driver, "Merge Leads");
			FSLeadSummaryMergeLeadsPage pobj2 = new FSLeadSummaryMergeLeadsPage(driver);
			fc.utobj().clickElement(driver, pobj2.mergeBtn);

			fc.utobj().printTestStep("Take the user name and pwd and login into Candidate Portal");
			fc.utobj().clickElement(driver, pobj.candidatePortalBtn);
			fc.utobj().printTestStep("Go to Candidate Portal > Take User name and password");
			fc.utobj().clickElement(driver, pobj.showleadDetailsBtn);

			String candidatePortalUserId = getCandidatePortal_FDD_UserName(driver);
			String fddpassword = getCandidatePortal_FDD_Password(driver);

			String buildUrl = config.get("buildUrl");
			String fddurl = null;
			if ((buildUrl.charAt(buildUrl.length() - 1)) == '/') {
				fddurl = config.get("buildUrl").concat("candidate.jsp");
			} else {
				fddurl = config.get("buildUrl").concat("/candidate.jsp");
			}

			fc.utobj().printTestStep("login into candidate portal ");
			driver.navigate().to(fddurl);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//input[@name='userID']"),
					candidatePortalUserId);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//input[@name='password']"),
					fddpassword);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//input[@value='Login']"));

			try {
				fc.utobj().clickElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'My Dashboard')]"));
			} catch (Exception e) {

			}
			fc.utobj().printTestStep("Verify the logo of the brand of merged lead is appearing");
			boolean isActivity = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + divisionName2 + "')]");
			if (isActivity == false) {
				fc.utobj().throwsException("Activity not verfied at candidate portal");
			}

			WebElement logofield = driver
					.findElement(By.xpath(".//ul[@class='multibrandLogo']/li/img[@title='" + divisionName2 + "']"));
			fc.utobj().moveToElement(driver, logofield);
			String logo = logofield.getAttribute("src").trim();

			if (!(logo.indexOf("Apple_logo_black") != -1)) {
				fc.utobj().throwsSkipException("was not able to verify mailbody/keywords");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales" , "FC_Sales_CandidatePortal_015" }) // Fixed : Verified // Akshat
	@TestCase(createdOn = "2017-10-03", updatedOn = "2017-10-03", testCaseId = "FC_Sales_CandidatePortal_015", testCaseDescription = "Verify logo after lead is merged.")
	void verifyCandidatePortal015() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			LeadSummaryUI pobj = new LeadSummaryUI(driver);
			AdminDivisionAddDivisionPageTest divisionpage = new AdminDivisionAddDivisionPageTest();

			String divisionName = fc.utobj().generateTestData(dataSet.get("divisionName"));
			String divisionName2 = fc.utobj().generateTestData(dataSet.get("divisionName2"));

			fc.adminpage().adminPage(driver);

			fc.utobj().clickLink(driver, "Configure New Hierarchy Level");

			fc.utobj().clickElement(driver, pobj.submitBtn1);

			fc.utobj().printTestStep("Add two brands > Configure logo for one of the brand");
			divisionpage.addDivision(driver, divisionName);

			try {
				// fc.utobj().clickElement(driver, pobj.showAllButton);
			} catch (Exception e) {
			}
			fc.utobj().actionImgOption(driver, divisionName, "Configure Division Logo");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String fileName = fc.utobj().getFilePathFromTestData(dataSet.get("fileName"));
			fc.utobj().sendKeys(driver, pobj.divisionLogo, fileName);

			fc.utobj().clickElement(driver, pobj.saveCboxButton);

			divisionpage.addDivision(driver, divisionName2);

			fc.utobj().printTestStep(
					"Add two leads with different brands > Now merge the lead and select the lead > for which the brand does not have the logo");
			fc.sales().sales_common().fsModule(driver);
			String userName = "FranConnect Administrator";
			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String firstName2 = fc.utobj().generateTestData(dataSet.get("newfirstName"));
			String lastName2 = fc.utobj().generateTestData(dataSet.get("newlastName"));

			String leadFullName = firstName + " " + lastName;
			String country = "USA";
			String state = "Alabama";
			String state2 = "California";
			String emailId = "salesautomation@staffex.com";
			String leadOwner = "FranConnect Administrator";
			String leadSourceCategory = "Import";
			String leadSourceDetails = "None";

			Map<String, String> leadInfo = new HashMap<String, String>();
			leadInfo.put("firstName", firstName);
			leadInfo.put("lastName", lastName);
			leadInfo.put("country", country);
			leadInfo.put("state", state);
			leadInfo.put("email", emailId);
			leadInfo.put("leadOwner", leadOwner);
			leadInfo.put("leadSourceCategory", leadSourceCategory);
			leadInfo.put("leadSourceDetails", leadSourceDetails);
			leadInfo.put("division", divisionName);
			addLead(driver, config, leadInfo);

			Map<String, String> leadInfo2 = new HashMap<String, String>();
			leadInfo2.put("firstName", firstName);
			leadInfo2.put("lastName", lastName);
			leadInfo2.put("country", country);
			leadInfo2.put("state", state2);
			leadInfo2.put("email", emailId);
			leadInfo2.put("leadOwner", leadOwner);
			leadInfo2.put("leadSourceCategory", leadSourceCategory);
			leadInfo2.put("leadSourceDetails", leadSourceDetails);
			leadInfo2.put("division", divisionName2);
			
			addLead(driver, config, leadInfo2);

			FSSearchPageTest p3 = new FSSearchPageTest();
			p3.searchByLeadName(driver, firstName, lastName);
			fc.utobj().clickElement(driver, pobj.checkBoxAll);
			fc.utobj().printTestStep("Merge these two leads");

			fc.utobj().selectActionMenuItems(driver, "Merge Leads");
			FSLeadSummaryMergeLeadsPage pobj2 = new FSLeadSummaryMergeLeadsPage(driver);
			fc.utobj().clickElement(driver, pobj2.mergeBtn);

			fc.utobj().printTestStep("Take the user name and pwd and login into Candidate Portal");
			fc.utobj().clickElement(driver, pobj.candidatePortalBtn);
			fc.utobj().printTestStep("Go to Candidate Portal > Take User name and password");
			fc.utobj().clickElement(driver, pobj.showleadDetailsBtn);

			String candidatePortalUserId = getCandidatePortal_FDD_UserName(driver);
			String fddpassword = getCandidatePortal_FDD_Password(driver);

			String buildUrl = config.get("buildUrl");
			String fddurl = null;
			if ((buildUrl.charAt(buildUrl.length() - 1)) == '/') {
				fddurl = config.get("buildUrl").concat("candidate.jsp");
			} else {
				fddurl = config.get("buildUrl").concat("/candidate.jsp");
			}

			fc.utobj().printTestStep("login into candidate portal ");
			driver.navigate().to(fddurl);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//input[@name='userID']"),
					candidatePortalUserId);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//input[@name='password']"),
					fddpassword);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//input[@value='Login']"));

			try {
				fc.utobj().clickElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'My Dashboard')]"));
			} catch (Exception e) {

			}
			fc.utobj().printTestStep("Verify the logo of the brand of merged lead is appearing");
			// isActivity=fc.utobj().verifyCase(driver,
			// ".//*[contains(text(),'"+divisionName2+"')]");
			boolean isActivity = fc.utobj().assertPageSource(driver, divisionName2);
			if (isActivity == false) {
				fc.utobj().throwsException("Activity not verfied at candidate portal");
			}

			WebElement logofield =fc.utobj().getElementByXpath(driver, ".//ul[@class='multibrandLogo']/li/img[@title='" + divisionName2 + "']");
			fc.utobj().moveToElement(driver, logofield);
			String logo = logofield.getAttribute("src").trim();

			if (!(logo.indexOf("buildLogo") != -1)) {
				fc.utobj().throwsSkipException("was not able to verify mailbody/keywords");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesank", "sales", "sales_failed" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Sales_AddLead_MandatoryFields", testCaseDescription = "Validate Fdd Email in the activity history > Validate email in mail box >Validate fdd login and download ")
	void verifyAddLeadMandatoryFields() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			Sales fsmod = new Sales();
			fc.sales().sales_common().fsModule(driver);
			String firstName = fc.utobj().generateTestData(dataSet.get("FirstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("LastName"));
			String email = "salesautomation@franqa.com";
			String leadSourceCategory = "Import";
			String leadSourceDetails = "None";
			String userName = "FranConnect Administrator";
			fsmod.leadManagement(driver);
			fc.utobj().printTestStep("Add lead");
			try {
				clickAddLeadLink(driver);
			} catch (Exception e) {
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//a[@href='addLeadPrimaryInfoForm?addAnotherLead=true']"));
			}
			fc.utobj().printTestStep(
					"Try to fill all the tabs having mandatory fields with blank or no records and save.");
			FSLeadSummaryAddLeadPage leadAddPage = new FSLeadSummaryAddLeadPage(driver);

			fc.utobj().clickElement(driver, leadAddPage.save);
			fc.utobj().acceptAlertBox(driver);
			// fc.utobj().selectDropDown(driver, leadAddPage.salutation, "Mr.");
			fc.utobj().sendKeys(driver, leadAddPage.firstName, firstName);
			fc.utobj().clickElement(driver, leadAddPage.save);
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().sendKeys(driver, leadAddPage.lastName, lastName);
			fc.utobj().clickElement(driver, leadAddPage.save);
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().sendKeys(driver, leadAddPage.emailID, email);
			fc.utobj().clickElement(driver, leadAddPage.save);
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().selectDropDownByVisibleText(driver, leadAddPage.leadOwnerID, userName);
			fc.utobj().clickElement(driver, leadAddPage.save);
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().selectDropDownByVisibleText(driver, leadAddPage.leadSource2ID, leadSourceCategory);
			fc.utobj().clickElement(driver, leadAddPage.save);
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().selectDropDownByVisibleText(driver, leadAddPage.leadSource3ID, leadSourceDetails);
			fc.utobj().printTestStep("Fill only the mandatory fields and save");
			fc.utobj().clickElement(driver, leadAddPage.save);

			boolean isLeadAdded = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + firstName + "')]");
			if (isLeadAdded == false) {
				fc.utobj().throwsException("lead not added by filling only mandatory ");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void actionImgOption(WebDriver driver, String groupName, String option) throws Exception {
		fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
				".//*[contains(text () ,'" + groupName + "')]/../following-sibling::td//div[@class='list-name']"));

		WebElement element = fc.utobj().getElementByXpath(driver,
				".//*[contains(text () ,'" + groupName
						+ "')]/../following-sibling::td//div[@class='list-name']/following-sibling::ul/li/a[contains(text () ,'"
						+ option + "')]");
		fc.utobj().clickElement(driver, element);
	}

	public Map<String, String> addLeadLeadSummaryWithLeadName(WebDriver driver, Map<String, String> config,
			String firstName, String lastName) throws Exception {
		Sales fsmod = new Sales();
		fsmod.leadManagement(driver);
		clickAddLeadLink(driver);
		FSLeadSummaryPrimaryInfoPageTest p2 = new FSLeadSummaryPrimaryInfoPageTest();
		Map<String, String> leadName = p2.fillLeadBasicInfo_LeadName(driver, config, firstName, lastName);
		return leadName;
	}

	public void configureAllEmailContentLead(WebDriver driver) throws Exception {
		AdminSalesConfigureEmailContentPage pobj = new AdminSalesConfigureEmailContentPage(driver);
		fc.adminpage().openAdminSalesConfigureEmailContentSenttoLeadOwner(driver);

		if (!fc.utobj().getElement(driver, pobj.sendEmailnotificationYes).isSelected()) {
			fc.utobj().clickElement(driver, pobj.sendEmailnotificationYes);
		}
		fc.utobj().clickElement(driver, pobj.emailFields);
		if (!fc.utobj().getElement(driver, pobj.selectAll).isSelected()) {
			fc.utobj().clickElement(driver, pobj.selectAll);
		}
		fc.utobj().clickElement(driver, pobj.emailFields);
		fc.utobj().clickElement(driver, pobj.saveBtn);
		// fc.utobj().isTextDisplayed(driver, "All changes have been
		// successfully updated", "changes saved");
		/*
		 * if (!pobj.emailFields.getText().trim().equalsIgnoreCase(
		 * "All Selected")) { fc.utobj().throwsException(
		 * "was not able to configure Email Content For Leads"); }
		 */
	}
	/*
	 * public void addLead(WebDriver driver,Map<String,String>
	 * config,@Optional()String salutation,String firstName,String
	 * lastName,@Optional()String city,@Optional()String
	 * country,@Optional()String state,@Optional()String county,
	 * 
	 * @Optional()String email,@Optional()String zipCode,@Optional()String
	 * preferedModeOfContact,@Optional()String
	 * bestTimeToContact,@Optional()String address,@Optional()String
	 * address2,@Optional()String phone,@Optional()String fax,
	 * 
	 * @Optional()String phoneExtension,@Optional()String
	 * homePhone,@Optional()String homePhoneExtension,@Optional()String
	 * mobile,String leadOwner,String leadSourceCategory,String
	 * leadSourceDetails,@Optional()String campaignName) throws Exception {
	 */

	public void addLead(WebDriver driver, Map<String, String> config, Map<String, String> leadInfo) throws Exception {

		try {

			Sales fsmod = new Sales();
			fsmod.leadManagement(driver);

			clickAddLeadLink(driver);

			FSLeadSummaryAddLeadPage leadAddPage = new FSLeadSummaryAddLeadPage(driver);
			if (leadInfo.get("salutation") != null) {
				fc.utobj().selectDropDown(driver, leadAddPage.salutation, leadInfo.get("salutation"));
			}
			fc.utobj().sendKeys(driver, leadAddPage.firstName, leadInfo.get("firstName"));
			fc.utobj().sendKeys(driver, leadAddPage.lastName, leadInfo.get("lastName"));
			if (leadInfo.get("address") != null) {
				fc.utobj().sendKeys(driver, leadAddPage.address, leadInfo.get("address"));
			}
			if (leadInfo.get("address2") != null) {
				fc.utobj().sendKeys(driver, leadAddPage.address2, leadInfo.get("address2"));
			}
			if (leadInfo.get("city") != null) {
				fc.utobj().sendKeys(driver, leadAddPage.city, leadInfo.get("city"));
			}
			if (leadInfo.get("country") != null) {
				fc.utobj().sendKeys(driver, leadAddPage.country, leadInfo.get("country"));
			}
			if (leadInfo.get("state") != null) {
				fc.utobj().sendKeys(driver, leadAddPage.state, leadInfo.get("state"));
			}
			if (leadInfo.get("zipCode") != null) {
				fc.utobj().sendKeys(driver, leadAddPage.zip, leadInfo.get("zipCode"));
			}
			if (leadInfo.get("county") != null) {
				fc.utobj().sendKeys(driver, leadAddPage.county, leadInfo.get("county"));
			}
			// fc.utobj().sendKeys(driver, leadAddPage.preferredModeofContact,
			// leadInfo.get("preferedModeOfContact"));
			if (leadInfo.get("bestTimeToContact") != null) {
				fc.utobj().sendKeys(driver, leadAddPage.bestTimeToContact, leadInfo.get("bestTimeToContact"));
			}
			if (leadInfo.get("phone") != null) {
				fc.utobj().sendKeys(driver, leadAddPage.phone, leadInfo.get("phone"));
			}
			if (leadInfo.get("phoneExtension") != null) {
				fc.utobj().sendKeys(driver, leadAddPage.phoneExt, leadInfo.get("phoneExtension"));
			}
			if (leadInfo.get("homePhone") != null) {
				fc.utobj().sendKeys(driver, leadAddPage.homePhone, leadInfo.get("homePhone"));
			}
			if (leadInfo.get("homePhoneExtension") != null) {
				fc.utobj().sendKeys(driver, leadAddPage.homePhoneExt, leadInfo.get("homePhoneExtension"));
			}
			if (leadInfo.get("fax") != null) {
				fc.utobj().sendKeys(driver, leadAddPage.fax, leadInfo.get("fax"));
			}
			if (leadInfo.get("mobile") != null) {
				fc.utobj().sendKeys(driver, leadAddPage.mobile, leadInfo.get("mobile"));
			}
			fc.utobj().sendKeys(driver, leadAddPage.emailID, leadInfo.get("email"));
			// fc.utobj().sendKeys(driver, leadAddPage.companyName, "Company
			// Name");
			if (leadInfo.get("comments") != null) {
				fc.utobj().sendKeys(driver, leadAddPage.comments, leadInfo.get("comments"));
			}

			fc.utobj().selectDropDownByVisibleText(driver, leadAddPage.leadOwnerID, leadInfo.get("leadOwner"));
			
			fc.utobj().selectDropDownByVisibleText(driver, leadAddPage.leadSource2ID,
					leadInfo.get("leadSourceCategory").toString());

			fc.utobj().selectDropDownByVisibleText(driver, leadAddPage.leadSource3ID, leadInfo.get("leadSourceDetails").toString());
			
			if (leadInfo.get("division") != null) {
				fc.utobj().selectValFromMultiSelect(driver, fc.utobj().getElementByID(driver, "ms-parentbrandMapping_0brandID"), leadInfo.get("division").toString());
			}
			
			fc.utobj().clickElement(driver, leadAddPage.assignAutomaticCampaign);
			fc.utobj().clickElement(driver, leadAddPage.save);

		} catch (Exception e) {
 
		}
		// return leadName;
	}

	// Anukaran
	public Map<String, String> addLeadSummaryWithLeadNameOwnerName(WebDriver driver, String firstName, String lastName,
			String ownerName) throws Exception {
		Sales sales = new Sales();
		sales.leadManagement(driver);

		clickAddLeadLink(driver);

		FSLeadSummaryPrimaryInfoPageTest p2 = new FSLeadSummaryPrimaryInfoPageTest();
		Map<String, String> leadName = p2.fillLeadBasicInfo_OwnerNameBasic(driver, firstName, lastName, ownerName);
		return leadName;
	}

	@Test(groups = { "sales" , "TC_HomePage_LeadAgainstStatus_001" }) // Verified // Akshat
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-13", testCaseId = "TC_HomePage_LeadAgainstStatus_001", testCaseDescription = "Verify the lead count against the status of the lead")
	void verifyHomePageLeadCountAgainstStatus() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FSLeadSummaryAddLeadPage pobj = new FSLeadSummaryAddLeadPage(driver);
			// fc.sales().sales_common().fsModule(fc.sales(), driver);
			fc.adminpage().adminPage(driver);
			fc.utobj().printTestStep("Admin > Sales > Add New Status");
			AdminFranchiseSalesLeadStatusPageTest p1 = new AdminFranchiseSalesLeadStatusPageTest();
			String leadStatus = p1.addNewLeadStatus(driver);

			fc.sales().sales_common().fsModule(driver);
			Sales fsmod = new Sales();
			fsmod.leadManagement(driver);
			fc.utobj().clickElement(driver, pobj.addLeadLnk);
			String firstName = fc.utobj().generateTestData("FirstName");
			String lastName = fc.utobj().generateTestData("LastName");
			String country = "USA";
			String state = "Alabama";
			String emailId = "salesautomation@staffex.com";
			String leadOwner = "FranConnect Administrator";
			String leadSourceCategory = "Import";
			String leadSourceDetails = "None";
			fc.utobj().printTestStep("Add a lead with this status");
			fc.utobj().sendKeys(driver, pobj.firstName, firstName);
			fc.utobj().sendKeys(driver, pobj.lastName, lastName);
			fc.utobj().selectDropDownByVisibleText(driver, pobj.country, country);

			fc.utobj().selectDropDownByVisibleText(driver, pobj.state, state);
			fc.utobj().sendKeys(driver, pobj.emailID, emailId);
			fc.utobj().selectDropDownByVisibleText(driver, pobj.leadOwnerID, leadOwner);
			fc.utobj().selectDropDownByVisibleText(driver, pobj.leadSource2ID, leadSourceCategory);

			fc.utobj().selectDropDownByVisibleText(driver, pobj.leadSource3ID, leadSourceDetails);

			fc.utobj().clickElement(driver, pobj.save);
			fc.utobj().selectDropDownByVisibleText(driver, pobj.leadStatus, leadStatus);
			fc.utobj().clickElement(driver, pobj.chngLeadStatusBtn);
			fsmod.homePage(driver);
			FSHomePage homePage = new FSHomePage(driver);
			fc.utobj().selectDropDownByVisibleText(driver, homePage.salesFunnel, "Today");

			WebElement element = driver
					.findElement(By.xpath(".//td[contains(text(),'" + leadStatus + "')]/following-sibling::td[2]/a"));
			fc.utobj().moveToElement(driver, element);
			String leadCount = element.getText();
			if (!leadCount.equals("1")) {
				fc.utobj().throwsException("Lead count in sales funnel is not correct,against the configured status");
			}
			fc.utobj().clickElement(driver, driver
					.findElement(By.xpath(".//td[contains(text(),'" + leadStatus + "')]/following-sibling::td[2]/a")));

			boolean isLeadPresent = fc.utobj().assertLinkPartialText(driver, firstName + " " + lastName);
			if (isLeadPresent == false) {
				fc.utobj().throwsException("Lead not visible on clicking the lead count in sles funnel");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales", "salesJuly11" })
	@TestCase(createdOn = "2017-07-03", updatedOn = "2017-07-03", testCaseId = "TC_Sales_DivisionalUser", testCaseDescription = "Verify Divisional User is available for Lead Assignment, Search, Export and Report")
	void verifySalesDivisionalUser() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest divisionalUserPage = new AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest();
			LeadSummaryUI pobj = new LeadSummaryUI(driver);
			Sales fsmod = new Sales();
			SearchUI searchPage = new SearchUI(driver);
			FsExportPage exportPage = new FsExportPage(driver);

			String divisionName = fc.utobj().generateTestData(dataSet.get("divisionName"));
			String userName = fc.utobj().generateTestData(dataSet.get("userName"));
			String emailId = dataSet.get("emailId");

			fc.utobj().printTestStep("Go to Admin > Add Division");
			/*
			 * divisionpage.addDivision(driver, divisionName, config);
			 * fc.utobj().printTestStep(testCaseId, "Add Divisional User");
			 */
			divisionalUserPage.addDivisionalUser(driver, userName, divisionName, emailId);
			fc.utobj().printTestStep("Go to Admin > Sales >  Assign Lead Owners");
			fc.adminpage().adminPage(driver);

			AdminSales adsales = new AdminSales();
			adsales.assignLeadOwners(driver);
			fc.utobj().printTestStep("Verify the user is available in Default Owner drop down");
			boolean isLeadOwnerPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//select[@name='area1']/option[contains(text(),'" + userName + " " + userName + "')]");
			if (isLeadOwnerPresent == false) {
				fc.utobj().throwsException("User not present in Default Owner dropdown");
			}
			// fc.utobj().selectDropDown(driver,
			// pobj.defaultOwnerDrpDown,userName+" "+userName);
			fc.utobj().printTestStep("Click Continue");
			fc.utobj().clickElement(driver, pobj.continueButton);
			fc.utobj().printTestStep("Verify the user is available in Available Lead Owners");
			boolean availableLeadOwner = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//select[@id='allOwners']/option[contains(text(),'" + userName + " " + userName + "')]");
			if (availableLeadOwner == false) {
				fc.utobj().throwsException("User is  not available in Available Lead Owners");
			}
			// fc.utobj().clickElement(driver, pobj.updateLeadOwnerBtn);
			fc.utobj().printTestStep("Go to Search Tab");
			fc.sales().sales_common().fsModule(driver);
			fsmod.search(driver);
			fc.utobj().printTestStep("Check in the Lead Owner MultiSelect > Owner is present");
			fc.utobj().clickElement(driver, searchPage.leadOwnerDrp);
			fc.utobj().sendKeys(driver, searchPage.leadOwnerDrpInput, userName + " " + userName);
			boolean isOwnerNamePresent1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//label[contains(text(),'" + userName + " " + userName + "')]");
			if (isOwnerNamePresent1 == false) {
				fc.utobj().throwsException("User is  not available in Available Lead Owners");
			}
			fc.utobj().printTestStep("Go to Export Tab");
			fsmod.exportPage(driver);
			fc.utobj().printTestStep("Click Search Data");
			fc.utobj().clickElement(driver, pobj.searchData);
			fc.utobj().printTestStep("Check in the Lead Owner MultiSelect > Owner is present");
			fc.utobj().clickElement(driver, exportPage.leadOwner);
			fc.utobj().sendKeys(driver, exportPage.leadOwnerInput, userName + " " + userName);
			boolean isOwnerNamePresentExportPage = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//label[contains(text(),'" + userName + " " + userName + "')]");
			if (isOwnerNamePresentExportPage == false) {
				fc.utobj().throwsException("User is  not available at Lead Owner MultiSelect  Export Page");
			}
			fc.utobj().printTestStep("Go to Sales > Reports > Leads per Month Report");
			fsmod.reports(driver);
			fc.utobj().clickLink(driver, "Leads per Month Report");
			fc.utobj().printTestStep("Check in the View Leads Belonging To MultiSelect > Owner is present");
			fc.utobj().clickElement(driver, pobj.viewLeadsBelongingTo);
			fc.utobj().sendKeys(driver, pobj.viewLeadsBelongingToInput, userName + " " + userName);
			boolean isOwnerNamePresentReportsPage = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//label[contains(text(),'" + userName + " " + userName + "')]");
			if (isOwnerNamePresentReportsPage == false) {
				fc.utobj().throwsException(
						"User is  not available in 'View Leads Belonging To' MultiSelect at Lead per Month Report");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales" })
	@TestCase(createdOn = "2017-07-04", updatedOn = "2017-07-04", testCaseId = "TC_Sales_CorporateUser_Deactivate_001", testCaseDescription = "Verify Modify/Delete/Deactivate for all users")
	void verifyModifyDeleteDeactivateAllUsers() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			AdminUsersManageCorporateUsersAddCorporateUserPage corpUserPage = new AdminUsersManageCorporateUsersAddCorporateUserPage(
					driver);
			LeadSummaryUI pobj = new LeadSummaryUI(driver);
			FSLeadSummaryVisitPage visitpage = new FSLeadSummaryVisitPage(driver);
			FSBrokersPage brokerPage = new FSBrokersPage(driver);
			AdminFranchiseSalesBrokersAgencySummaryAddBrokersAgencyPage brokerAgencyObj = new AdminFranchiseSalesBrokersAgencySummaryAddBrokersAgencyPage(
					driver);
			FSBrokersSummaryPageTest brokerPageObj = new FSBrokersSummaryPageTest();
			FSLeadSummaryVisitPageTest visitPage = new FSLeadSummaryVisitPageTest();

			String userName = fc.utobj().generateTestData(dataSet.get("firstUserName"));
			String userName1 = fc.utobj().generateTestData(dataSet.get("secondUserName"));
			String password = dataSet.get("password");
			String emailId = dataSet.get("emailId");
			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String leadFullName = firstName + " " + lastName;
			String taskSubject = fc.utobj().generateTestData(dataSet.get("taskSubject"));
			String visitScheduledDate = fc.utobj().getCurrentDateUSFormat();
			String brokerFirstName = fc.utobj().generateTestData(dataSet.get("brokerFirstName"));
			// String brokerLastName =
			// fc.utobj().generateTestData(dataSet.get("brokerLastName"));
			String brokerAddress = fc.utobj().generateTestData(dataSet.get("brokerAddress"));
			String agencyName = fc.utobj().generateTestData(dataSet.get("agencyName"));

			fc.utobj().printTestStep("Add two Corporate User");

			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = corpTest.createDefaultUser(driver, corpUser);

			CorporateUser corpUser2 = new CorporateUser();
			corpUser2 = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser2);
			corpUser2.setEmail(emailId);
			corpUser2 = corpTest.createDefaultUser(driver, corpUser2);

			fc.sales().sales_common().fsModule(driver);
			fc.utobj().printTestStep("Add a lead and assign to this 1st user");
			addLeadSummaryWithLeadNameOwnerName(driver, firstName, lastName, corpUser.getuserFullName());
			// fc.utobj().sendKeys(driver, pobj.leadSearch, leadFullName);
			// fc.utobj().clickElement(driver, pobj.leadSearchBtn);
			FSSearchPageTest p3 = new FSSearchPageTest();
			p3.searchByLeadName(driver, firstName, lastName);
			fc.utobj().printTestStep("Log a task to this lead and assign to 1st user");
			fc.utobj().actionImgOption(driver, leadFullName, "Log a Task");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().printTestStep("Add Task Details");
			fc.utobj().clickElement(driver, pobj.assignTaskToLeadOwner);
			fc.utobj().sendKeys(driver, pobj.taskSubject, taskSubject);
			fc.utobj().clickElement(driver, pobj.addTaskBtn);

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'" + leadFullName + "')]"));

			fc.utobj().clickElement(driver, pobj.visitTab);
			fc.utobj().printTestStep("Fill Visit tab and assign task to 1st user");
			visitPage.addVisitWithAssignTo(driver, config, "Lead Owner");

			fc.utobj().printTestStep("Add a broker and log a task and assign to 1st user");
			brokerPageObj.addBrokers(driver, brokerFirstName);

			fc.utobj().sendKeys(driver, brokerPage.brokerNameSearch, brokerFirstName + " " + brokerFirstName);
			fc.utobj().clickElement(driver, brokerPage.searchBtn);
			fc.utobj().actionImgOption(driver, brokerFirstName + " " + brokerFirstName, "Log a Task");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().selectValFromMultiSelect(driver, brokerPage.brokerTaskOwner, corpUser.getuserFullName());
			fc.utobj().sendKeys(driver, pobj.taskSubject, taskSubject);
			fc.utobj().clickElement(driver, pobj.addTaskBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Now Deactivate 1st user");
			fc.adminpage().adminPage(driver);
			fc.adminpage().openCorporateUserPage(driver);
			fc.utobj().sendKeys(driver, corpUserPage.searchCorpUser, corpUser.getuserFullName());
			fc.utobj().clickElement(driver, corpUserPage.searchCorpUserBtn);
			fc.utobj().actionImgOption(driver, corpUser.getuserFullName(), "Deactivate");
			fc.utobj().printTestStep("Accept Validation message to deactivate 1st user");
			fc.utobj().acceptAlertBox(driver);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().printTestStep("Click on reassign the user");
			fc.utobj().clickElement(driver, corpUserPage.reassignBtn);
			fc.utobj()
					.printTestStep("Reassign to 2nd user and validate that the task has been reassigned to this user.");
			fc.utobj().selectDropDown(driver, corpUserPage.reassignToLeadDrp, corpUser2.getuserFullName());
			fc.utobj().selectDropDown(driver, corpUserPage.reassignToTaskDrp, corpUser2.getuserFullName());
			fc.utobj().clickElement(driver, corpUserPage.reassignToBtn);
			fc.utobj().clickElement(driver, pobj.closeCBoxBtn);

			fc.utobj().printTestStep("Also verify the user is present in deactivated user section");
			// fc.adminpage().openCorporateUserPage(driver);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, corpUserPage.deactivatedUsersTab);
			fc.utobj().sendKeys(driver, corpUserPage.searchCorpUser, corpUser.getuserFullName());
			fc.utobj().clickElement(driver, corpUserPage.searchCorpUserBtn);
			boolean isCorpUserDeactiavted = fc.utobj().assertLinkPartialText(driver, corpUser.getuserFullName());
			if (isCorpUserDeactiavted == false) {
				fc.utobj().throwsException("user is not present in deactivated user section");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales", "sales_failed" })
	@TestCase(createdOn = "2017-07-04", updatedOn = "2017-07-04", testCaseId = "TC_Sales_CorporateUser_Deactivate_002", testCaseDescription = "Verify the deactivation of the User without reassigning the task")
	void verifyUserDeactivationWithoutReassign() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			driver = fc.loginpage().login(driver);
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			AdminUsersManageCorporateUsersAddCorporateUserPage corpUserPage = new AdminUsersManageCorporateUsersAddCorporateUserPage(
					driver);
			LeadSummaryUI pobj = new LeadSummaryUI(driver);
			FSLeadSummaryVisitPage visitpage = new FSLeadSummaryVisitPage(driver);
			FSBrokersPage brokerPage = new FSBrokersPage(driver);
			Sales fsmod = new Sales();
			AdminFranchiseSalesBrokersAgencySummaryAddBrokersAgencyPage brokerAgencyObj = new AdminFranchiseSalesBrokersAgencySummaryAddBrokersAgencyPage(
					driver);
			FSBrokersSummaryPageTest brokerPageObj = new FSBrokersSummaryPageTest();
			FSLeadSummaryVisitPageTest visitPageObj = new FSLeadSummaryVisitPageTest();

			String userName = fc.utobj().generateTestData(dataSet.get("firstUserName"));
			String userName1 = fc.utobj().generateTestData(dataSet.get("secondUserName"));
			String password = dataSet.get("password");
			String emailId = dataSet.get("emailId");
			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String leadFullName = firstName + " " + lastName;
			String taskSubject = fc.utobj().generateTestData(dataSet.get("taskSubject"));
			String visitScheduledDate = fc.utobj().getCurrentDateUSFormat();
			String brokerFirstName = fc.utobj().generateTestData(dataSet.get("brokerFirstName"));
			// String brokerLastName =
			// fc.utobj().generateTestData(dataSet.get("brokerLastName"));
			String brokerAddress = fc.utobj().generateTestData(dataSet.get("brokerAddress"));
			String agencyName = fc.utobj().generateTestData(dataSet.get("agencyName"));

			fc.utobj().printTestStep("Add Corporate User");
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = corpTest.createDefaultUser(driver, corpUser);

			fc.utobj().printTestStep("Add a lead and assign to this user");

			fc.sales().sales_common().fsModule(driver);
			addLeadSummaryWithLeadNameOwnerName(driver, firstName, lastName, corpUser.getuserFullName());
			/*
			 * fc.utobj().sendKeys(driver, pobj.leadSearch, leadFullName);
			 * fc.utobj().clickElement(driver, pobj.leadSearchBtn);
			 */
			FSSearchPageTest p3 = new FSSearchPageTest();
			p3.searchByLeadName(driver, firstName, lastName);
			fc.utobj().printTestStep("Log a task to this lead and assign to user");
			fc.utobj().actionImgOption(driver, leadFullName, "Log a Task");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().printTestStep("Add Task Details");
			fc.utobj().clickElement(driver, pobj.assignTaskToLeadOwner);
			fc.utobj().sendKeys(driver, pobj.taskSubject, taskSubject);
			fc.utobj().clickElement(driver, pobj.addTaskBtn);

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'" + leadFullName + "')]"));

			fc.utobj().clickElement(driver, pobj.visitTab);
			fc.utobj().printTestStep("Fill Visit tab and assign task to this user");
			visitPageObj.addVisitWithAssignTo(driver, config, "Lead Owner");

			fc.utobj().printTestStep("Add a broker and log a task and assign to this user");
			brokerPageObj.addBrokers(driver, brokerFirstName);

			fc.utobj().sendKeys(driver, brokerPage.brokerNameSearch, brokerFirstName + " " + brokerFirstName);
			fc.utobj().clickElement(driver, brokerPage.searchBtn);
			fc.utobj().actionImgOption(driver, brokerFirstName + " " + brokerFirstName, "Log a Task");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().selectValFromMultiSelect(driver, brokerPage.brokerTaskOwner, corpUser.getuserFullName());
			fc.utobj().sendKeys(driver, pobj.taskSubject, taskSubject);
			fc.utobj().clickElement(driver, pobj.addTaskBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Now Deactivate this user");
			fc.adminpage().adminPage(driver);
			fc.adminpage().openCorporateUserPage(driver);
			fc.utobj().sendKeys(driver, corpUserPage.searchCorpUser, corpUser.getuserFullName());
			fc.utobj().clickElement(driver, corpUserPage.searchCorpUserBtn);
			fc.utobj().actionImgOption(driver, corpUser.getuserFullName(), "Deactivate");
			fc.utobj().printTestStep("Accept Validation message to deactivate 1st user");
			fc.utobj().acceptAlertBox(driver);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().printTestStep("Deactivate the user");
			fc.utobj().clickElement(driver, corpUserPage.deactivateBtn);
			fc.utobj().clickElement(driver, pobj.closeCBoxBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("The task should still be assigned to this user");
			fc.sales().sales_common().fsModule(driver);
			fsmod.tasks(driver);
			fc.utobj().clickElement(driver, pobj.showFilter);
			fc.utobj().sendKeys(driver, pobj.taskSubject, taskSubject);
			fc.utobj().clickElement(driver, pobj.searchFilterBtn);
			boolean isTaskAssociated = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + firstName + " " + lastName
							+ "')]/ancestor::td/following-sibling::td[contains(text(),'" + corpUser.getuserFullName()
							+ "')]");
			boolean isTaskAssociated1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + brokerFirstName + " " + brokerFirstName
							+ "')]/ancestor::td/following-sibling::td[contains(text(),'" + corpUser.getuserFullName()
							+ "')]");
			if (isTaskAssociated == false || isTaskAssociated1 == false) {
				fc.utobj().throwsException("Task associated with same user not verified");
			}
			fc.utobj().sendKeys(driver, pobj.taskSubject, "Visit Schedule [" + firstName + " " + lastName + "]");
			fc.utobj().clickElement(driver, pobj.searchFilterBtn);
			boolean isTaskAssociated2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'Visit Schedule [" + firstName + " " + lastName
							+ "]')]/ancestor::td/following-sibling::td[contains(text(),'" + corpUser.getuserFullName()
							+ "')]");
			if (isTaskAssociated2 == false) {
				fc.utobj().throwsException("Task associated with same user not verified");
			}

			fc.utobj().printTestStep("Also verify the user is present in deactivated user section");
			fc.adminpage().adminPage(driver);
			fc.adminpage().openCorporateUserPage(driver);
			fc.utobj().clickElement(driver, corpUserPage.deactivatedUsersTab);
			fc.utobj().sendKeys(driver, corpUserPage.searchCorpUser, corpUser.getuserFullName());
			fc.utobj().clickElement(driver, corpUserPage.searchCorpUserBtn);
			boolean isCorpUserDeactiavted = fc.utobj().assertLinkPartialText(driver, corpUser.getuserFullName());
			if (isCorpUserDeactiavted == false) {
				fc.utobj().throwsException("user is not present in deactivated user section");
			}

			fc.utobj().printTestStep("Go to Admin > Sales > Setup Sales Lead Owners");
			fc.adminpage().adminPage(driver);
			fc.adminpage().openSetupFranchiseSalesLeadOwnersPage(driver);
			fc.utobj().printTestStep("Click on radio button (Selected users as Sales Lead Owners)");
			fc.utobj().clickElement(driver, corpUserPage.usersAsSalesLeadOwners);
			fc.utobj().printTestStep("Verify the deactivated user is not coming in the list");
			boolean isUserPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//select[@name='selectedUsersRight']/option[contains(text(),'" + corpUser.getuserFullName()
							+ "')]");
			if (isUserPresent == true) {
				fc.utobj().throwsException("User present after deactivating in Setup Sales Lead Owners page ");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales", "sales_failed" , "TC_Sales_CorporateUser_Delete_001" }) // Verified : Akshat
	@TestCase(createdOn = "2017-07-04", updatedOn = "2017-07-04", testCaseId = "TC_Sales_CorporateUser_Delete_001", testCaseDescription = "Verify the Delete of the User and reassign the tasks")
	void verifyDeleteUsersReassignTask() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			AdminUsersManageCorporateUsersAddCorporateUserPage corpUserPage = new AdminUsersManageCorporateUsersAddCorporateUserPage(
					driver);
			FSLeadSummaryVisitPage visitpage = new FSLeadSummaryVisitPage(driver);
			FSBrokersPage brokerPage = new FSBrokersPage(driver);
			AdminFranchiseSalesBrokersAgencySummaryAddBrokersAgencyPage brokerAgencyObj = new AdminFranchiseSalesBrokersAgencySummaryAddBrokersAgencyPage(
					driver);
			FSBrokersSummaryPageTest brokerPageObj = new FSBrokersSummaryPageTest();
			FSLeadSummaryVisitPageTest visitPageObj = new FSLeadSummaryVisitPageTest();

			String userName = fc.utobj().generateTestData(dataSet.get("firstUserName"));
			String userName1 = fc.utobj().generateTestData(dataSet.get("secondUserName"));
			String password = dataSet.get("password");
			String emailId = dataSet.get("emailId");
			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String leadFullName = firstName + " " + lastName;
			String taskSubject = fc.utobj().generateTestData(dataSet.get("taskSubject"));
			String visitScheduledDate = fc.utobj().getCurrentDateUSFormat();
			String brokerFirstName = fc.utobj().generateTestData(dataSet.get("brokerFirstName"));
			String brokerAddress = fc.utobj().generateTestData(dataSet.get("brokerAddress"));
			String agencyName = fc.utobj().generateTestData(dataSet.get("agencyName"));

			fc.utobj().printTestStep("Add two Corporate User");

			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = corpTest.createDefaultUser(driver, corpUser);

			CorporateUser corpUser2 = new CorporateUser();
			corpUser2 = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser2);
			corpUser2.setEmail(emailId);
			corpUser2 = corpTest.createDefaultUser(driver, corpUser2);

			fc.utobj().printTestStep("Add a lead and assign to this 1st user");
			fc.sales().sales_common().fsModule(driver);
			addLeadSummaryWithLeadNameOwnerName(driver, firstName, lastName, corpUser.getuserFullName());

			FSSearchPageTest p3 = new FSSearchPageTest();
			p3.searchByLeadName(driver, firstName, lastName);

			fc.utobj().printTestStep("Log a task to this lead and assign to 1st user");
			fc.utobj().actionImgOption(driver, leadFullName, "Log a Task");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().printTestStep("Add Task Details");
			LeadSummaryUI pobj = new LeadSummaryUI(driver);
			fc.utobj().clickElement(driver, pobj.assignTaskToLeadOwner);
			fc.utobj().sendKeys(driver, pobj.taskSubject, taskSubject);
			fc.utobj().clickElement(driver, pobj.addTaskBtn);

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'" + leadFullName + "')]"));

			fc.utobj().clickElement(driver, pobj.visitTab);
			fc.utobj().printTestStep("Fill Visit tab and assign task to 1st user");
			visitPageObj.addVisitWithAssignTo(driver, config, "Lead Owner");

			fc.utobj().printTestStep("Add a broker and log a task and assign to 1st user");
			brokerPageObj.addBrokers(driver, brokerFirstName);

			fc.utobj().sendKeys(driver, brokerPage.brokerNameSearch, brokerFirstName + " " + brokerFirstName);
			fc.utobj().clickElement(driver, brokerPage.searchBtn);
			fc.utobj().actionImgOption(driver, brokerFirstName + " " + brokerFirstName, "Log a Task");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().selectValFromMultiSelect(driver, brokerPage.brokerTaskOwner, corpUser.getuserFullName());
			fc.utobj().sendKeys(driver, pobj.taskSubject, taskSubject);
			fc.utobj().clickElement(driver, pobj.addTaskBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Now delete 1st user");
			fc.adminpage().adminPage(driver);
			fc.adminpage().openCorporateUserPage(driver);
			fc.utobj().sendKeys(driver, corpUserPage.searchCorpUser, corpUser.getuserFullName());
			fc.utobj().clickElement(driver, corpUserPage.searchCorpUserBtn);
			fc.utobj().actionImgOption(driver, corpUser.getuserFullName(), "Delete");
			fc.utobj().printTestStep("Accept Validation message to Delete 1st user");
			fc.utobj().acceptAlertBox(driver);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().printTestStep("Click on reassign the user");
			fc.utobj().clickElement(driver, corpUserPage.reassignBtn);
			fc.utobj()
					.printTestStep("Reassign to 2nd user and validate that the task has been reassigned to this user.");
			fc.utobj().selectDropDown(driver, corpUserPage.reassignToLeadDrp, corpUser2.getuserFullName());
			fc.utobj().selectDropDown(driver, corpUserPage.reassignToTaskDrp, corpUser2.getuserFullName());
			fc.utobj().clickElement(driver, corpUserPage.reassignToBtn);
			fc.utobj().clickElement(driver, pobj.closeCBoxBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Also verify the user is not present in Active User Section");
			fc.utobj().clickElement(driver, corpUserPage.activeUsersTab);
			fc.utobj().sendKeys(driver, corpUserPage.searchCorpUser, corpUser.getuserFullName());
			fc.utobj().clickElement(driver, corpUserPage.searchCorpUserBtn);
			boolean isCorpUserDeactiavted = fc.utobj().assertLinkPartialText(driver, corpUser.getuserFullName());
			if (isCorpUserDeactiavted == true) {
				fc.utobj().throwsException("user is not present in deactivated user section");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales", "sales_failed" })
	@TestCase(createdOn = "2017-07-04", updatedOn = "2017-07-04", testCaseId = "TC_Sales_CorporateUser_Delete_002", testCaseDescription = "Verify the delete of the User without reassigning the task")
	void verifyUserDeleteWithoutReassigning() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			AdminUsersManageCorporateUsersAddCorporateUserPage corpUserPage = new AdminUsersManageCorporateUsersAddCorporateUserPage(
					driver);
			LeadSummaryUI pobj = new LeadSummaryUI(driver);
			FSLeadSummaryVisitPage visitpage = new FSLeadSummaryVisitPage(driver);
			FSBrokersPage brokerPage = new FSBrokersPage(driver);
			Sales sales = new Sales();
			sales.leadManagement(driver);
			AdminFranchiseSalesBrokersAgencySummaryAddBrokersAgencyPage brokerAgencyObj = new AdminFranchiseSalesBrokersAgencySummaryAddBrokersAgencyPage(
					driver);
			FSBrokersSummaryPageTest brokerPageObj = new FSBrokersSummaryPageTest();
			FSLeadSummaryVisitPageTest visitPageObj = new FSLeadSummaryVisitPageTest();

			String userName = fc.utobj().generateTestData(dataSet.get("firstUserName"));
			String userName1 = fc.utobj().generateTestData(dataSet.get("secondUserName"));
			String password = dataSet.get("password");
			String emailId = dataSet.get("emailId");
			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String leadFullName = firstName + " " + lastName;
			String taskSubject = fc.utobj().generateTestData(dataSet.get("taskSubject"));
			String visitScheduledDate = fc.utobj().getCurrentDateUSFormat();
			String brokerFirstName = fc.utobj().generateTestData(dataSet.get("brokerFirstName"));
			String brokerAddress = fc.utobj().generateTestData(dataSet.get("brokerAddress"));
			String agencyName = fc.utobj().generateTestData(dataSet.get("agencyName"));

			fc.utobj().printTestStep("Add Corporate User");
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = corpTest.createDefaultUser(driver, corpUser);

			fc.utobj().printTestStep("Add a lead and assign to this user");
			fc.sales().sales_common().fsModule(driver);
			addLeadSummaryWithLeadNameOwnerName(driver, firstName, lastName, corpUser.getuserFullName());
			/*
			 * fc.utobj().sendKeys(driver, pobj.leadSearch, leadFullName);
			 * fc.utobj().clickElement(driver, pobj.leadSearchBtn);
			 */
			FSSearchPageTest p3 = new FSSearchPageTest();
			p3.searchByLeadName(driver, firstName, lastName);

			fc.utobj().printTestStep("Log a task to this lead and assign to user");
			fc.utobj().actionImgOption(driver, leadFullName, "Log a Task");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().printTestStep("Add Task Details");
			fc.utobj().clickElement(driver, pobj.assignTaskToLeadOwner);
			fc.utobj().sendKeys(driver, pobj.taskSubject, taskSubject);
			fc.utobj().clickElement(driver, pobj.addTaskBtn);

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'" + leadFullName + "')]"));

			fc.utobj().clickElement(driver, pobj.visitTab);
			fc.utobj().printTestStep("Fill Visit tab and assign task to this user");
			visitPageObj.addVisitWithAssignTo(driver, config, "Lead Owner");

			fc.utobj().printTestStep("Add a broker and log a task and assign to this user");
			brokerPageObj.addBrokers(driver, brokerFirstName);

			fc.utobj().sendKeys(driver, brokerPage.brokerNameSearch, brokerFirstName + " " + brokerFirstName);
			fc.utobj().clickElement(driver, brokerPage.searchBtn);
			fc.utobj().actionImgOption(driver, brokerFirstName + " " + brokerFirstName, "Log a Task");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().selectValFromMultiSelect(driver, brokerPage.brokerTaskOwner, corpUser.getuserFullName());
			fc.utobj().sendKeys(driver, pobj.taskSubject, taskSubject);
			fc.utobj().clickElement(driver, pobj.addTaskBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Now delete 1st user");
			fc.adminpage().adminPage(driver);
			fc.adminpage().openCorporateUserPage(driver);
			fc.utobj().sendKeys(driver, corpUserPage.searchCorpUser, corpUser.getuserFullName());
			fc.utobj().clickElement(driver, corpUserPage.searchCorpUserBtn);
			fc.utobj().actionImgOption(driver, corpUser.getuserFullName(), "Delete");
			fc.utobj().printTestStep("Accept Validation message to Delete 1st user");
			fc.utobj().acceptAlertBox(driver);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().printTestStep("Delete the user");
			fc.utobj().clickElement(driver, corpUserPage.deleteBtn);
			fc.utobj().clickElement(driver, pobj.closeCBoxBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("The task should still be assigned to this user");
			fc.sales().sales_common().fsModule(driver);
			sales.tasks(driver);
			fc.utobj().clickElement(driver, pobj.showFilter);
			fc.utobj().sendKeys(driver, pobj.taskSubject, taskSubject);
			fc.utobj().clickElement(driver, pobj.searchFilterBtn);
			boolean isTaskAssociated = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + firstName + " " + lastName
							+ "')]/ancestor::td/following-sibling::td[contains(text(),'" + corpUser.getuserFullName()
							+ "')]");
			boolean isTaskAssociated1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + brokerFirstName + " " + brokerFirstName
							+ "')]/ancestor::td/following-sibling::td[contains(text(),'" + corpUser.getuserFullName()
							+ "')]");
			if (isTaskAssociated == false || isTaskAssociated1 == false) {
				fc.utobj().throwsException("Task associated with same user not verified");
			}
			fc.utobj().sendKeys(driver, pobj.taskSubject, "Visit Schedule [" + firstName + " " + lastName + "]");
			fc.utobj().clickElement(driver, pobj.searchFilterBtn);
			boolean isTaskAssociated2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'Visit Schedule [" + firstName + " " + lastName
							+ "]')]/ancestor::td/following-sibling::td[contains(text(),'" + corpUser.getuserFullName()
							+ "')]");
			if (isTaskAssociated2 == false) {
				fc.utobj().throwsException("Task associated with same user not verified");
			}

			fc.utobj().printTestStep("Also verify the user is not present in Active User Section");
			fc.adminpage().adminPage(driver);
			fc.adminpage().openCorporateUserPage(driver);
			fc.utobj().clickElement(driver, corpUserPage.activeUsersTab);
			fc.utobj().sendKeys(driver, corpUserPage.searchCorpUser, corpUser.getuserFullName());
			fc.utobj().clickElement(driver, corpUserPage.searchCorpUserBtn);
			boolean isCorpUserDeactiavted = fc.utobj().assertLinkPartialText(driver, corpUser.getuserFullName());
			if (isCorpUserDeactiavted == true) {
				fc.utobj().throwsException("user is not present in deactivated user section");
			}

			fc.utobj().printTestStep("Go to Admin > Sales > Setup Sales Lead Owners");
			fc.adminpage().adminPage(driver);
			fc.adminpage().openSetupFranchiseSalesLeadOwnersPage(driver);
			fc.utobj().printTestStep("Click on radio button (Selected users as Sales Lead Owners)");
			fc.utobj().clickElement(driver, corpUserPage.usersAsSalesLeadOwners);
			fc.utobj().printTestStep("Verify the Deleted user is not coming in the list");
			boolean isUserPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//select[@name='selectedUsersRight']/option[contains(text(),'" + corpUser.getuserFullName()
							+ "')]");
			if (isUserPresent == true) {
				fc.utobj().throwsException("User present after deactivating in Setup Sales Lead Owners page ");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales", "sales_failed" })
	@TestCase(createdOn = "2017-07-20", updatedOn = "2017-07-20", testCaseId = "TC_Sales_SourceCategory_004", testCaseDescription = "Verify the Lead Source Category name is mandatory")
	void verifyLeadSourceCatogoryNameMandatory() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		// Map<String,String> dataSet = fc.utobj().readTestData("sales",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			LeadSummaryUI pobj = new LeadSummaryUI(driver);
			fc.utobj().printTestStep("Go to  Admin > Sales > Lead Source Category");
			AdminSales adsales = new AdminSales();
			adsales.adminFranchiseSalesLeadSourceCategory(driver);
			fc.utobj().printTestStep(
					"Add Lead Source Category > without entering the value in the field > Click on submit");
			fc.utobj().clickElement(driver, pobj.addLeadSourceCarogoryBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.addLeadSourceCarogoryCboxBtn);
			fc.utobj().printTestStep("The validation message 'Please enter Source Category name.' should come.");
			try {
				fc.utobj().acceptAlertBox(driver);
			} catch (Exception e) {
				fc.utobj().throwsException("Alert Message 'Please enter Source Category name' not comming");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales", "sales_failed" })
	@TestCase(createdOn = "2017-07-20", updatedOn = "2017-07-20", testCaseId = "TC_Sales_SourceCategory_005", testCaseDescription = "Verify the duplicate Lead Source Category name is not getting added.")
	void verifyDuplicateLeadSourceCatogoryName() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			LeadSummaryUI pobj = new LeadSummaryUI(driver);

			String catogeryName = fc.utobj().generateTestData(dataSet.get("catogeryName"));
			fc.utobj().printTestStep("Go to Admin > Sales > Lead Source Category");
			AdminSales adsales = new AdminSales();
			adsales.adminFranchiseSalesLeadSourceCategory(driver);
			fc.utobj().printTestStep("Add Lead Source Category > Click on submit > The lead source should be added.");
			fc.utobj().clickElement(driver, pobj.addLeadSourceCarogoryBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.leadSourceName, catogeryName);
			fc.utobj().clickElement(driver, pobj.addLeadSourceCarogoryCboxBtn);
			fc.utobj().clickElement(driver, pobj.cboxCloseBtn);
			fc.utobj().switchFrameToDefault(driver);
			boolean isSourceCatogeryAdded = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text(),'" + catogeryName + "')]");
			if (isSourceCatogeryAdded == false) {
				fc.utobj().throwsException("Lead Source Catogery not added");
			}
			fc.utobj().printTestStep("Now add another category with same name ");
			fc.utobj().clickElement(driver, pobj.addLeadSourceCarogoryBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.leadSourceName, catogeryName);
			fc.utobj().clickElement(driver, pobj.addLeadSourceCarogoryCboxBtn);
			fc.utobj().printTestStep("The validation message should appear 'Lead Source Category already exists.'");
			try {
				fc.utobj().acceptAlertBox(driver);
			} catch (Exception e) {
				fc.utobj().throwsException("Duplicate Lead Source Catogery alert box not coming");
			}
			fc.utobj().clickElement(driver, pobj.cboxCloseBtn);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales", "salesJuly" , "TC_Sales_SourceCategory_006" }) // verified // akshat
	@TestCase(createdOn = "2017-07-21", updatedOn = "2017-07-21", testCaseId = "TC_Sales_SourceCategory_006", testCaseDescription = "Verify the Lead Source Category name is accepting the special character")
	void verifyLeadSourceCategoryWithSpecialCharactor() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			LeadSummaryUI pobj = new LeadSummaryUI(driver);
			String uniqueName = fc.utobj().generateTestData(dataSet.get("catogeryName"));
			String specialCharactors = fc.utobj().getMajorSpecialCharacter();
			String catogeryName = uniqueName + specialCharactors;
			fc.utobj().printTestStep("Go to  Admin > Sales > Lead Source Category");
			AdminSales adsales = new AdminSales();
			adsales.adminFranchiseSalesLeadSourceCategory(driver);
			fc.utobj().clickElement(driver, pobj.addLeadSourceCarogoryBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().printTestStep("Add Lead Source Category with special character > Click on submit > ");
			fc.utobj().sendKeys(driver, pobj.leadSourceName, catogeryName);
			fc.utobj().clickElement(driver, pobj.addLeadSourceCarogoryCboxBtn);
			fc.utobj().clickElement(driver, pobj.cboxCloseBtn);
			fc.utobj().switchFrameToDefault(driver);
			boolean isSourceCatogeryAdded = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text(),'" + uniqueName + "')]");
			boolean isSourceCatogeryAddedSpecilaChr = fc.utobj().assertPageSource(driver, catogeryName);
			fc.utobj().printTestStep("The lead source should be added.");
			if (isSourceCatogeryAdded == false && isSourceCatogeryAddedSpecilaChr) {
				fc.utobj().throwsException("Lead Source Catogery not added with special charactor");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales", "sales_failed","TC_Sales_SourceCategory_007" }) // verified : Akshat
	@TestCase(createdOn = "2017-07-21", updatedOn = "2017-07-21", testCaseId = "TC_Sales_SourceCategory_007", testCaseDescription = "Verify the duplicate Lead Source Category with special character name is not getting added.")
	void verifyDuplicateLeadSourceWithSpecialCharactor() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			LeadSummaryUI pobj = new LeadSummaryUI(driver);
			String uniqueName = fc.utobj().generateTestData(dataSet.get("catogeryName"));
			String specialCharactors = fc.utobj().getMajorSpecialCharacter();
			String catogeryName = uniqueName + specialCharactors;
			fc.utobj().printTestStep("Go to  Admin > Sales > Lead Source Category");
			AdminSales adsales = new AdminSales();
			adsales.adminFranchiseSalesLeadSourceCategory(driver);
			fc.utobj().clickElement(driver, pobj.addLeadSourceCarogoryBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().printTestStep(
					"Add Lead Source Category with special character > Click on submit > The lead source should be added.");
			fc.utobj().sendKeys(driver, pobj.leadSourceName, catogeryName);
			fc.utobj().clickElement(driver, pobj.addLeadSourceCarogoryCboxBtn);
			fc.utobj().clickElement(driver, pobj.cboxCloseBtn);
			fc.utobj().switchFrameToDefault(driver);
			boolean isSourceCatogeryAdded = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text(),'" + uniqueName + "')]");
			boolean isSourceCatogeryAddedSpecilaChr = fc.utobj().assertPageSource(driver, catogeryName);
			if (isSourceCatogeryAdded == false && isSourceCatogeryAddedSpecilaChr == false) {
				fc.utobj().throwsException("Lead Source Catogery not added with special charactor");
			}

			fc.utobj().clickElement(driver, pobj.addLeadSourceCarogoryBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().printTestStep("Now add another category with same name");
			fc.utobj().sendKeys(driver, pobj.leadSourceName, catogeryName);
			fc.utobj().clickElement(driver, pobj.addLeadSourceCarogoryCboxBtn);
			fc.utobj().printTestStep("The validation message should appear Lead Source Category already exists.");
			try {
				fc.utobj().acceptAlertBox(driver);
			} catch (Exception e) {
				fc.utobj().throwsException("Duplicate Lead Source Catogery not added with special charactor");
			}
			fc.utobj().clickElement(driver, pobj.cboxCloseBtn);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales", "salesJuly" })
	@TestCase(createdOn = "2017-07-21", updatedOn = "2017-07-21", testCaseId = "TC_Sales_SourceCategory_008", testCaseDescription = "Verify the Lead Source Category name is getting deleted with special character")
	void verify() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			LeadSummaryUI pobj = new LeadSummaryUI(driver);

			String uniqueName = fc.utobj().generateTestData(dataSet.get("catogeryName"));
			String specialCharactors = fc.utobj().getMajorSpecialCharacter();
			String catogeryName = uniqueName + specialCharactors;
			fc.utobj().printTestStep("Go to  Admin > Sales > Lead Source Category");
			AdminSales adsales = new AdminSales();
			adsales.adminFranchiseSalesLeadSourceCategory(driver);
			fc.utobj().printTestStep("Add Lead Source Category with special character > Click on submit > ");
			fc.utobj().clickElement(driver, pobj.addLeadSourceCarogoryBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.leadSourceName, catogeryName);
			fc.utobj().clickElement(driver, pobj.addLeadSourceCarogoryCboxBtn);
			fc.utobj().printTestStep("The lead source should be added.");
			fc.utobj().clickElement(driver, pobj.cboxCloseBtn);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().printTestStep(
					"Now click on delete > The validation message should come All source details corresponding to selected source Category will be deleted . Do you want to proceed ?");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					(".//td[contains(text(),'" + uniqueName + "')]/ancestor::tr/td/input[@name='checkb']")));
			fc.utobj().printTestStep("The lead source category should get deleted.");
			fc.utobj().clickElement(driver, pobj.deleteBtn);
			fc.utobj().acceptAlertBox(driver);
			boolean isSourceCatogeryAddedSpecilaChr = fc.utobj().assertPageSource(driver, catogeryName);
			if (isSourceCatogeryAddedSpecilaChr == true) {
				fc.utobj().throwsException("Lead Source Catogery not added with special charactor");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales", "salesJuly" })
	@TestCase(createdOn = "2017-07-21", updatedOn = "2017-07-21", testCaseId = "TC_Sales_SourceCategory_009", testCaseDescription = "Verify the Lead Source is getting modifiied without changing the name of the source")
	void verifyLeadSourceCatogoryModify() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			LeadSummaryUI pobj = new LeadSummaryUI(driver);

			String catogeryName = fc.utobj().generateTestData(dataSet.get("catogeryName"));

			AdminSales adsales = new AdminSales();
			adsales.adminFranchiseSalesLeadSourceCategory(driver);
			fc.utobj().printTestStep("Go to  Admin > Sales > Lead Source Category");
			fc.utobj().clickElement(driver, pobj.addLeadSourceCarogoryBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.leadSourceName, catogeryName);
			fc.utobj().clickElement(driver, pobj.addLeadSourceCarogoryCboxBtn);
			fc.utobj().clickElement(driver, pobj.cboxCloseBtn);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().printTestStep("Add Lead Source Category > Click on submit > The lead source should be added.");
			boolean isSourceCatogeryAdded = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text(),'" + catogeryName + "')]");
			if (isSourceCatogeryAdded == false) {
				fc.utobj().throwsException("Lead Source Catogery not added");
			}
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					(".//td[contains(text(),'" + catogeryName + "')]/ancestor::tr/td/input[@name='checkb']")));
			fc.utobj().printTestStep(
					"Now modify the source category > Without changing the name click on submit button.");
			fc.utobj().clickElement(driver, pobj.modifyBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.leadSourceName, catogeryName);
			fc.utobj().printTestStep("The lead source category should get submitted.");
			fc.utobj().clickElement(driver, pobj.ModifyBtn);
			fc.utobj().clickElement(driver, pobj.cboxCloseBtn);
			fc.utobj().switchFrameToDefault(driver);
			isSourceCatogeryAdded = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + catogeryName + "')]");
			if (isSourceCatogeryAdded == false) {
				fc.utobj().throwsException("Lead Source Catogery not added");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales", "sales_failed" , "TC_Sales_SourceCategory_010" }) // verified // akshat
	@TestCase(createdOn = "2017-07-21", updatedOn = "2017-07-21", testCaseId = "TC_Sales_SourceCategory_010", testCaseDescription = "Verify the Lead Source is getting modifiied without changing the name of the source")
	void verifyLeadSorceCategoryDetails() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			LeadSummaryUI pobj = new LeadSummaryUI(driver);

			String catogeryName = fc.utobj().generateTestData(dataSet.get("catogeryName"));
			String catogeryDetails = fc.utobj().generateTestData(dataSet.get("catogeryDetails"));

			fc.adminpage().adminPage(driver);

			AdminSales adsales = new AdminSales();
			adsales.adminFranchiseSalesLeadSourceCategory(driver);
			fc.utobj().printTestStep("Go to  Admin > Sales > Lead Source Category>Add");
			fc.utobj().clickElement(driver, pobj.addLeadSourceCarogoryBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.leadSourceName, catogeryName);
			fc.utobj().clickElement(driver, pobj.addLeadSourceCarogoryCboxBtn);
			fc.utobj().clickElement(driver, pobj.cboxCloseBtn);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj()
					.clickElement(driver,
							fc.utobj().getElementByXpath(driver,
									".//td[contains(text(),'" + catogeryName
											+ "')]/following-sibling::td/a[contains(text(),'"
											+ fc.utobj().translateString("Lead Source Details") + "')]"));
			fc.utobj().printTestStep("Click on Lead Source Details");
			fc.utobj().clickElement(driver, pobj.addLeadSourceDetailsBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().printTestStep("Add lead source details");
			fc.utobj().sendKeys(driver, pobj.leadSourceDetails, catogeryDetails);
			fc.utobj().clickElement(driver, pobj.addLeadSourceCarogoryCboxBtn);
			fc.utobj().clickElement(driver, pobj.cboxCloseBtn);
			fc.utobj().printTestStep("Verify the lead source details is getting added.");
			boolean isSourceDetailsVisible = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text(),'" + catogeryDetails + "')]");
			if (isSourceDetailsVisible == false) {
				fc.utobj().throwsException("Lead source details was not verified");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales", "salesJuly" })
	@TestCase(createdOn = "2017-07-21", updatedOn = "2017-07-21", testCaseId = "TC_Sales_SourceCategory_011", testCaseDescription = "Verify the Lead Source is getting modifiied without changing the name of the source")
	void verifyLeadSorceCategoryDetailsSpclChtr() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			LeadSummaryUI pobj = new LeadSummaryUI(driver);

			String catogeryName = fc.utobj().generateTestData(dataSet.get("catogeryName"));
			String uniqueName = fc.utobj().generateTestData(dataSet.get("catogeryDetails"));
			String specialCharactors = fc.utobj().getMajorSpecialCharacter();
			String catogeryDetails = uniqueName + specialCharactors;
			AdminSales adsales = new AdminSales();
			adsales.adminFranchiseSalesLeadSourceCategory(driver);
			fc.utobj().printTestStep("Go to  Admin > Sales > Lead Source Category>Add");
			fc.utobj().clickElement(driver, pobj.addLeadSourceCarogoryBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.leadSourceName, catogeryName);
			fc.utobj().clickElement(driver, pobj.addLeadSourceCarogoryCboxBtn);
			fc.utobj().clickElement(driver, pobj.cboxCloseBtn);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj()
					.clickElement(driver,
							fc.utobj().getElementByXpath(driver,
									".//td[contains(text(),'" + catogeryName
											+ "')]/following-sibling::td/a[contains(text(),'"
											+ fc.utobj().translateString("Lead Source Details") + "')]"));
			fc.utobj().printTestStep("Click on Lead Source Details");
			fc.utobj().clickElement(driver, pobj.addLeadSourceDetailsBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().printTestStep("Add lead source details with special character");
			fc.utobj().sendKeys(driver, pobj.leadSourceDetails, catogeryDetails);
			fc.utobj().clickElement(driver, pobj.addLeadSourceCarogoryCboxBtn);
			fc.utobj().clickElement(driver, pobj.cboxCloseBtn);
			boolean isSourceDetailsVisible = fc.utobj().assertPageSource(driver, catogeryDetails);
			if (isSourceDetailsVisible == false) {
				fc.utobj().throwsException("Lead source details with special Charactors was not verified");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales", "sales_failed_001" , "TC_Sales_SourceCategory_012" }) // Verified : Bug Logged
	@TestCase(createdOn = "2017-07-21", updatedOn = "2017-07-21", testCaseId = "TC_Sales_SourceCategory_012", testCaseDescription = "Verify the lead Source details with duplicate name is not getting added")
	void verifyDuplicateLeadSorceCategoryDetailsSpclChtr() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			LeadSummaryUI pobj = new LeadSummaryUI(driver);

			String catogeryName = fc.utobj().generateTestData(dataSet.get("catogeryName"));
			String uniqueName = fc.utobj().generateTestData(dataSet.get("catogeryDetails"));
			String specialCharactors = fc.utobj().getMajorSpecialCharacter();
			String catogeryDetails = uniqueName + specialCharactors;
			AdminSales adsales = new AdminSales();
			adsales.adminFranchiseSalesLeadSourceCategory(driver);
			fc.utobj().printTestStep("Go to  Admin > Sales > Lead Source Category>Add");
			fc.utobj().clickElement(driver, pobj.addLeadSourceCarogoryBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.leadSourceName, catogeryName);
			fc.utobj().clickElement(driver, pobj.addLeadSourceCarogoryCboxBtn);
			fc.utobj().clickElement(driver, pobj.cboxCloseBtn);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver,fc.utobj().getElementByXpath(driver,".//td[contains(text(),'" + catogeryName+ "')]/following-sibling::td/a[contains(text(),'"+ fc.utobj().translateString("Lead Source Details") + "')]"));
			fc.utobj().printTestStep("Click on Lead Source Details");
			fc.utobj().clickElement(driver, pobj.addLeadSourceDetailsBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().printTestStep("Add lead source details with special character");
			fc.utobj().sendKeys(driver, pobj.leadSourceDetails, catogeryDetails);
			fc.utobj().clickElement(driver, pobj.addLeadSourceCarogoryCboxBtn);
			fc.utobj().clickElement(driver, pobj.cboxCloseBtn);
			boolean isSourceDetailsVisible = fc.utobj().assertPageSource(driver, catogeryDetails);
			if (isSourceDetailsVisible == false) {
				fc.utobj().throwsException("Lead source details with special Charactors was not verified");
			}
			fc.utobj().printTestStep("Add another lead source details with same name");
			fc.utobj().clickElement(driver, pobj.addLeadSourceDetailsBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().printTestStep("Add lead source details with special character");
			fc.utobj().sendKeys(driver, pobj.leadSourceDetails, catogeryDetails);
			fc.utobj().clickElement(driver, pobj.addLeadSourceCarogoryCboxBtn);
			fc.utobj().printTestStep("An alert should come Lead Source Details already exists.");
			try {
				fc.utobj().acceptAlertBox(driver);
			} catch (Exception e) {
				fc.utobj().throwsException(
						"Duplicate Lead Source Catogery Details with special charactor is getting added");
			}
			fc.utobj().clickElement(driver, pobj.cboxCloseBtn);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			// e.printStackTrace();
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales", "sales_failed" })
	@TestCase(createdOn = "2017-07-21", updatedOn = "2017-07-21", testCaseId = "TC_Sales_SourceCategory_013", testCaseDescription = "Verify the lead Source details name is mandatory")
	void verifyLeadSorceCategoryDetailsBlank() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			LeadSummaryUI pobj = new LeadSummaryUI(driver);

			String catogeryName = fc.utobj().generateTestData(dataSet.get("catogeryName"));
			String uniqueName = fc.utobj().generateTestData(dataSet.get("catogeryDetails"));
			String specialCharactors = fc.utobj().getMajorSpecialCharacter();
			String catogeryDetails = uniqueName + specialCharactors;
			AdminSales adsales = new AdminSales();
			adsales.adminFranchiseSalesLeadSourceCategory(driver);
			fc.utobj().printTestStep("Go to  Admin > Sales > Lead Source Category>Add");
			fc.utobj().clickElement(driver, pobj.addLeadSourceCarogoryBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.leadSourceName, catogeryName);
			fc.utobj().clickElement(driver, pobj.addLeadSourceCarogoryCboxBtn);
			fc.utobj().clickElement(driver, pobj.cboxCloseBtn);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj()
					.clickElement(driver,
							fc.utobj().getElementByXpath(driver,
									".//td[contains(text(),'" + catogeryName
											+ "')]/following-sibling::td/a[contains(text(),'"
											+ fc.utobj().translateString("Lead Source Details") + "')]"));
			fc.utobj().printTestStep("Click on Lead Source Details");
			fc.utobj().clickElement(driver, pobj.addLeadSourceDetailsBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().printTestStep("Add lead source details with special character");
			fc.utobj().clickElement(driver, pobj.addLeadSourceCarogoryCboxBtn);
			fc.utobj().printTestStep("Validation message should come");
			try {
				fc.utobj().acceptAlertBox(driver);
			} catch (Exception e) {
				fc.utobj().throwsException(
						"Duplicate Lead Source Catogery Details with special charactor is getting added");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales", "sales_failed" })
	@TestCase(createdOn = "2017-07-28", updatedOn = "2017-07-28", testCaseId = "TC_Sales_SourceCategory_015", testCaseDescription = "Verify the lead source category and details is not getting deleted when lead is associated with source")
	void verifyLeadsourceCategoryDelete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			LeadSummaryUI pobj = new LeadSummaryUI(driver);
			AddLeadFromAllSources addlead = new AddLeadFromAllSources();
			FSSearchPageTest p3 = new FSSearchPageTest();
			Sales fsmod = new Sales();

			String catogeryName = fc.utobj().generateTestData(dataSet.get("catogeryName"));
			String catogeryDetails = fc.utobj().generateTestData(dataSet.get("catogeryDetails"));
			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			dataSet.put("firstName", firstName);
			dataSet.put("lastName", lastName);

			AdminSales adsales = new AdminSales();
			adsales.adminFranchiseSalesLeadSourceCategory(driver);
			fc.utobj().printTestStep("Go to  Admin > Sales > Lead Source Category");
			fc.utobj().printTestStep("Add lead source category and details");
			fc.utobj().clickElement(driver, pobj.addLeadSourceCarogoryBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.leadSourceName, catogeryName);
			fc.utobj().clickElement(driver, pobj.addLeadSourceCarogoryCboxBtn);
			fc.utobj().clickElement(driver, pobj.cboxCloseBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj()
					.clickElement(driver,
							fc.utobj().getElementByXpath(driver,
									".//td[contains(text(),'" + catogeryName
											+ "')]/following-sibling::td/a[contains(text(),'"
											+ fc.utobj().translateString("Lead Source Details") + "')]"));
			fc.utobj().clickElement(driver, pobj.addLeadSourceDetailsBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.leadSourceDetails, catogeryDetails);
			fc.utobj().clickElement(driver, pobj.addLeadSourceCarogoryCboxBtn);
			fc.utobj().clickElement(driver, pobj.cboxCloseBtn);

			dataSet.put("leadSource2ID", catogeryName);
			dataSet.put("leadSource3ID", catogeryDetails);
			fc.utobj().printTestStep("Add a lead with same lead source and details");
			fc.sales().sales_common().fsModule(driver);
			fsmod.leadManagement(driver);
			FSLeadSummaryPageTest leadSummaryobj = new FSLeadSummaryPageTest();

			leadSummaryobj.clickAddLeadLink(driver);
			addlead.fillFormDataWithDataset(driver, testCaseId, dataSet);
			adsales.adminFranchiseSalesLeadSourceCategory(driver);
			fc.utobj().printTestStep(
					"Try to delete the Source Category > The alert message should come 'You cannot delete value marked with *.' ");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					(".//td[contains(text(),'" + catogeryName + "')]/ancestor::tr/td/input[@name='checkb']")));
			fc.utobj().clickElement(driver, pobj.deleteBtn);
			try {
				fc.utobj().acceptAlertBox(driver);
			} catch (Exception e) {
				fc.utobj().throwsException("Source Category marked with * getting deleted (No alert box comming) ");
			}
			fc.utobj().printTestStep("Then try to delete the source details");
			fc.utobj()
					.clickElement(driver,
							fc.utobj().getElementByXpath(driver,
									".//td[contains(text(),'" + catogeryName
											+ "')]/following-sibling::td/a[contains(text(),'"
											+ fc.utobj().translateString("Lead Source Details") + "')]"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//td[contains(text(),'" + catogeryDetails + "')]/ancestor::tr/td/input[@name='checkb']"));
			fc.utobj().clickElement(driver, pobj.deleteBtn);
			fc.utobj().printTestStep("The alert message should come 'You cannot delete value marked with *.'");
			try {
				fc.utobj().acceptAlertBox(driver);
			} catch (Exception e) {
				fc.utobj().throwsException(
						"Source Category Details marked with * getting deleted (No alert box comming) ");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales" , "TC_SalesVerifyExistingLeadOwnercombo_001" }) // ok
	@TestCase(createdOn = "2017-08-24", updatedOn = "2017-08-24", testCaseId = "TC_SalesVerifyExistingLeadOwnercombo_001", testCaseDescription = "Verify lead type as Existing owner")
	void verifyExistingOwnerType() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			LeadSummaryUI pobj = new LeadSummaryUI(driver);
			AddLeadFromAllSources addlead = new AddLeadFromAllSources();
			FSSearchPageTest p3 = new FSSearchPageTest();
			AdminFranchiseLocationAddFranchiseLocationPageTest franchiseLocation = new AdminFranchiseLocationAddFranchiseLocationPageTest();

			String userName = fc.utobj().generateTestData(dataSet.get("userName"));
			String franchiseId = fc.utobj().generateTestData(dataSet.get("franchiseId"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			String userFullName = userName + " " + userName;
			String password = "T0n1ght1";
			String userNameF = fc.utobj().generateTestData(dataSet.get("userNameF"));
			String firstNameF = fc.utobj().generateTestData(dataSet.get("firstNameF"));
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String roleName = "Default Franchise Role";
			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String country = dataSet.get("country");
			String state = dataSet.get("state");
			// String leadOwner=dataSet.get("leadOwner");
			String leadSourceCategory = dataSet.get("leadSourceCategory");
			String leadSourceDetails = dataSet.get("leadSourceDetails");
			String emailId = "salesautomation@staffex.com";

			fc.utobj().printTestStep("Admin > Area / Region > Add Area / Region > Add Region");
			fc.utobj().printTestStep(
					"Admin > Franchise Location > Add Franchise Location (With created Region) and Add Owner Details > Add Franchise User");
			franchiseLocation.addFranchiseLocation_AllWithFName(driver, franchiseId, regionName, storeType, firstNameF);
			AdminUsersManageManageFranchiseUsersPageTest addFranUser = new AdminUsersManageManageFranchiseUsersPageTest();
			addFranUser.addFranchiseUser(driver, userNameF, password, franchiseId, roleName, emailId);

			fc.utobj().printTestStep(
					" Admin > Hidden Links > Link Lead With Existing Lead / Owner > Link Lead With Existing Lead / Owner > Yes");
			fc.adminpage().adminHiddenLinksLinkLeadWithExistingLeadorOwner(driver);
			fc.utobj().clickElement(driver, pobj.hiddenlinkslinkLeadYes);
			fc.utobj().clickElement(driver, pobj.submitBtn1);
			fc.utobj().printTestStep(
					" Admin > Hidden Links > Link Lead With Existing Lead / Owner > Link Lead With Existing Lead / Owner > Yes");
			Map<String, String> leadInfo = new HashMap<String, String>();
			leadInfo.put("firstName", firstName);
			leadInfo.put("lastName", lastName);
			leadInfo.put("country", country);
			leadInfo.put("state", state);
			leadInfo.put("email", emailId);
			leadInfo.put("existingOwner", firstNameF + " " + firstNameF);
			leadInfo.put("leadSourceCategory", leadSourceCategory);
			leadInfo.put("leadSourceDetails", leadSourceDetails);
			leadInfo.put("leadType", "Existing Owner");
			leadInfo.put("leadOwner", "FranConnect Administrator");
			addlead.addLeadFromSystem(driver, leadInfo);

			String existingOwner=leadInfo.get("existingOwner").concat(" (").concat(franchiseId).concat(")");
			boolean isExistingOwnerPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text(),'Owner(s)  :')]/following-sibling::td[contains(text(),'"+existingOwner+"')] ");
			if (isExistingOwnerPresent == false) {
				fc.utobj().throwsException("Existing owner not verified after adding lead");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			// e.printStackTrace();
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales", "salesjulyrt45" , "TC_SalesVerifyExistingLeadOwnercombo_002" }) // Fixed : Verified // Akshat
	@TestCase(createdOn = "2017-08-24", updatedOn = "2017-08-24", testCaseId = "TC_SalesVerifyExistingLeadOwnercombo_002", testCaseDescription = "Verify lead type as Existing Lead")
	void verifyExistingLeadType() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			LeadSummaryUI pobj = new LeadSummaryUI(driver);
			AddLeadFromAllSources addlead = new AddLeadFromAllSources();
			FSSearchPageTest p3 = new FSSearchPageTest();
			AdminFranchiseLocationAddFranchiseLocationPageTest franchiseLocation = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			FSLeadSummaryAddLeadPage npobj = new FSLeadSummaryAddLeadPage(driver);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String firstName1 = fc.utobj().generateTestData(dataSet.get("FirstNamenew"));
			String lastName1 = fc.utobj().generateTestData(dataSet.get("LastNamenew"));
			String country = dataSet.get("country");
			String state = dataSet.get("state");
			String leadSourceCategory = dataSet.get("leadSourceCategory");
			String leadSourceDetails = dataSet.get("leadSourceDetails");
			String emailId = "salesautomation@staffex.com";

			fc.utobj().printTestStep(
					" Admin > Hidden Links > Link Lead With Existing Lead / Owner > Link Lead With Existing Lead / Owner > Yes");
			fc.adminpage().adminHiddenLinksLinkLeadWithExistingLeadorOwner(driver);
			fc.utobj().clickElement(driver, pobj.hiddenlinkslinkLeadYes);
			fc.utobj().clickElement(driver, pobj.submitBtn1);

			fc.utobj().printTestStep("Sales > Lead Summary > Add Lead");
			Map<String, String> leadInfo = new HashMap<String, String>();
			leadInfo.put("firstName", firstName);
			leadInfo.put("lastName", lastName);
			leadInfo.put("country", country);
			leadInfo.put("state", state);
			leadInfo.put("email", emailId);
			// leadInfo.put("existingOwner", firstNameF+" "+firstNameF);
			leadInfo.put("leadSourceCategory", leadSourceCategory);
			leadInfo.put("leadSourceDetails", leadSourceDetails);
			// leadInfo.put("leadType", "Existing Lead");
			leadInfo.put("leadOwner", "FranConnect Administrator");
			addlead.addLeadFromSystem(driver, leadInfo);

			fc.utobj().printTestStep(
					"Sales > Lead Summary > Add Lead > Existing Lead > Enter the added lead name > Now add mandatory lead details > Save > Verify the lead is added with existing lead details also");
			fc.utobj().printTestStep("Sales > Lead Summary > Add Lead");
			Map<String, String> secondLeadInfo = new HashMap<String, String>();
			secondLeadInfo.put("firstName", firstName1);
			secondLeadInfo.put("lastName", lastName1);
			secondLeadInfo.put("country", country);
			secondLeadInfo.put("state", state);
			secondLeadInfo.put("email", emailId);
			secondLeadInfo.put("existingOwner", leadInfo.get("firstName") + " " + leadInfo.get("lastName"));
			secondLeadInfo.put("leadSourceCategory", leadSourceCategory);
			secondLeadInfo.put("leadSourceDetails", leadSourceDetails);
			secondLeadInfo.put("leadType", "Existing Lead");
			secondLeadInfo.put("leadOwner", "FranConnect Administrator");
			// addlead.addLeadFromSystem(driver, config, secondLeadInfo);

			// add second lead
			Sales fsmod = new Sales();
			fsmod.leadManagement(driver);

			clickAddLeadLink(driver);
			fc.utobj().selectDropDown(driver, npobj.ownerType, secondLeadInfo.get("leadType"));
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='autoComplete']"),
					leadInfo.get("firstName") + " " + leadInfo.get("lastName"));
			
			fc.utobj().sleep();

			try {
				System.out.println("Try Block");
				fc.utobj().moveToElementThroughAction(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='fc-id-1']/li[1]/div[1]")); // akshat
				fc.utobj().clickElement(driver,fc.utobj().getElementByXpath(driver, ".//*[@id='fc-id-1']/li[1]/div[1]"));
			} catch (Exception e) {
				System.out.println("Catch Block");
				fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='autoComplete']"),
						leadInfo.get("firstName") + " " + leadInfo.get("lastName"));
				fc.utobj().moveToElementThroughAction(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='fc-id-1']/li[1]/div[1]")); // akshat
				fc.utobj().clickElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fc-id-1']/li[1]/div[1]"));
			}
			fc.utobj().sendKeys(driver, npobj.firstName, firstName1);
			fc.utobj().sendKeys(driver, npobj.lastName, lastName1);
			fc.utobj().selectDropDownByVisibleText(driver, npobj.country, country);

			fc.utobj().selectDropDownByVisibleText(driver, npobj.state, state);
			fc.utobj().sendKeys(driver, npobj.emailID, emailId);
			fc.utobj().sendKeys(driver, npobj.zip, "123456");
			fc.utobj().sendKeys(driver, npobj.phone, "12345678");
			fc.utobj().sendKeys(driver, npobj.address, "New Highway Road");
			fc.utobj().selectDropDownByVisibleText(driver, npobj.leadOwnerID, secondLeadInfo.get("leadOwner"));
			fc.utobj().selectDropDownByVisibleText(driver, npobj.leadSource2ID, leadSourceCategory);

			fc.utobj().selectDropDownByVisibleText(driver, npobj.leadSource3ID, leadSourceDetails);

			fc.utobj().clickElement(driver, npobj.save);

			String oldLeadname = leadInfo.get("firstName").toString() + " " + leadInfo.get("lastName").toString();
			String newLeadname = secondLeadInfo.get("firstName").toString() + " "
					+ secondLeadInfo.get("lastName").toString();
			String name = firstName + " " + lastName + ", " + firstName1 + " " + lastName1;
			String name1 = firstName1 + " " + lastName1 + ", " + firstName + " " + lastName;

			boolean isExistingLeadPresent = fc.utobj().assertPageSource(driver, name);
			boolean isExistingLeadPresentOr = fc.utobj().assertPageSource(driver, name1);
			if (isExistingLeadPresent == false && isExistingLeadPresentOr == false) {
				fc.utobj().throwsException("Existing Lead not verified after adding lead");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			e.printStackTrace();
			// fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales", "TC_SalesVerifyExistingLeadOwnercombo_003" }) // Passed
	@TestCase(createdOn = "2017-08-30", updatedOn = "2017-08-30", testCaseId = "TC_SalesVerifyExistingLeadOwnercombo_003", testCaseDescription = "Verify Existing Owner DropDown at Location Owner Tab be adding lead")
	void existingLeadOwnercombo003() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FSLeadSummaryAddLeadPage pobj = new FSLeadSummaryAddLeadPage(driver);
			AddLeadFromAllSources addlead = new AddLeadFromAllSources();
			FSSearchPageTest p3 = new FSSearchPageTest();
			AdminFranchiseLocationAddFranchiseLocationPageTest franchiseLocation = new AdminFranchiseLocationAddFranchiseLocationPageTest();

			String userName = fc.utobj().generateTestData(dataSet.get("userName"));
			String franchiseId = fc.utobj().generateTestData(dataSet.get("franchiseId"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			String userFullName = userName + " " + userName;
			String password = "T0n1ght1";
			String userNameF = fc.utobj().generateTestData(dataSet.get("userNameF"));
			String firstNameF = fc.utobj().generateTestData(dataSet.get("firstNameF"));
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String roleName = "Default Franchise Role";
			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String country = dataSet.get("country");
			String state = dataSet.get("state");

			String newUserNameF = fc.utobj().generateTestData(dataSet.get("newUserNameFus"));
			String newUserName = fc.utobj().generateTestData(dataSet.get("newuserName"));
			String newFranchiseId = fc.utobj().generateTestData(dataSet.get("newfranchiseId"));
			String newFirstNameF = fc.utobj().generateTestData(dataSet.get("newfirstNameF"));
			String newregionName = fc.utobj().generateTestData(dataSet.get("newregionName"));
			String newstoreType = fc.utobj().generateTestData(dataSet.get("newstoreType"));

			// String leadOwner=dataSet.get("leadOwner");
			String leadSourceCategory = dataSet.get("leadSourceCategory");
			String leadSourceDetails = dataSet.get("leadSourceDetails");
			String emailId = "salesautomation@staffex.com";

			fc.utobj()
					.printTestStep("Admin > Franchise Location > Manage Franchise Locations > Add Franchise Location");
			franchiseLocation.addFranchiseLocation_AllWithFName(driver, franchiseId, regionName, storeType, firstNameF);
			AdminUsersManageManageFranchiseUsersPageTest addFranUser = new AdminUsersManageManageFranchiseUsersPageTest();
			addFranUser.addFranchiseUser(driver, userNameF, password, franchiseId, roleName, emailId);

			fc.utobj().printTestStep(" Go to Info Mgr > Franchisees > Owner > Add Owner With Full Details");
			fc.infomgr().infomgr_common().InfoMgrFranchisees(driver);

			InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();
			FranchiseesCommonMethods franchisees = new FranchiseesCommonMethods();
			franchisees.searchFranchiseAndClick(driver, franchiseId);

			// Modify Center Info
			String infotestCaseId = "TC_InFoMgr_Franchisees_CenterInfo_Modify";
			Map<String, String> dsCenterInfo = fc.utobj().readTestData("infomgr", infotestCaseId);

			fc.utobj().sendKeys(driver, centerInfoPage.txtCenterName, dsCenterInfo.get("centerName"));
			fc.utobj().sendKeys(driver, centerInfoPage.txtLicenseNo, dsCenterInfo.get("licenseNumber"));
			fc.utobj().sendKeys(driver, centerInfoPage.txtReportPeriodStartDate,
					fc.utobj().getFutureDateUSFormat(10));
			fc.utobj().sendKeys(driver, centerInfoPage.txtStoreOpeningDate,
					fc.utobj().getFutureDateUSFormat(10));
			fc.utobj().sendKeys(driver, centerInfoPage.txtStreetAddress, dsCenterInfo.get("streetAddress"));
			fc.utobj().sendKeys(driver, centerInfoPage.txtAddress2, dsCenterInfo.get("address2"));
			fc.utobj().sendKeys(driver, centerInfoPage.txtCity, dsCenterInfo.get("city"));
			fc.utobj().selectDropDownByVisibleText(driver, centerInfoPage.drpCountry, dsCenterInfo.get("country"));
			//
			fc.utobj().sendKeys(driver, centerInfoPage.txtZipCode, dsCenterInfo.get("zip"));
			fc.utobj().selectDropDownByVisibleText(driver, centerInfoPage.drpState, dsCenterInfo.get("state"));
			fc.utobj().sendKeys(driver, centerInfoPage.txtPhone, dsCenterInfo.get("phone"));
			fc.utobj().sendKeys(driver, centerInfoPage.txtExtension, dsCenterInfo.get("phoneExtension"));
			fc.utobj().sendKeys(driver, centerInfoPage.txtStoreFax, dsCenterInfo.get("fax"));
			fc.utobj().sendKeys(driver, centerInfoPage.txtStoreMobile, dsCenterInfo.get("mobile"));
			fc.utobj().sendKeys(driver, centerInfoPage.txtStoreEmail, dsCenterInfo.get("email"));
			fc.utobj().selectDropDownByVisibleText(driver, centerInfoPage.drpContactSalutation,
					dsCenterInfo.get("salutation"));
			fc.utobj().sendKeys(driver, centerInfoPage.txtContactFirstName, dsCenterInfo.get("contactFirstName"));
			fc.utobj().sendKeys(driver, centerInfoPage.txtContactLastName, dsCenterInfo.get("contactLastName"));
			fc.utobj().sendKeys(driver, centerInfoPage.txtContactPhone, dsCenterInfo.get("contactPhone"));
			fc.utobj().sendKeys(driver, centerInfoPage.txtContactExtension, dsCenterInfo.get("contactPhoneExtension"));
			fc.utobj().sendKeys(driver, centerInfoPage.txtContactEmail, dsCenterInfo.get("contactEmail"));
			fc.utobj().sendKeys(driver, centerInfoPage.txtContactFax, dsCenterInfo.get("contactFax"));
			fc.utobj().sendKeys(driver, centerInfoPage.txtContactMobile, dsCenterInfo.get("contactMobile"));
			fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
			// for
			// Custom
			// Manadatory
			// Fields
			// Of
			// Client
			fc.utobj().clickElement(driver, centerInfoPage.btnSumbit);

			// franchisees.modifyCenterInfoWithDataMap(driver, config,
			// dsCenterInfo);

			fc.utobj().printTestStep(
					" Admin > Franchise Location > Manage Franchise Locations > Add Another Franchise Location");
			franchiseLocation.addFranchiseLocation_AllWithFName(driver, newFranchiseId, newregionName, newstoreType,
					newFirstNameF);
			addFranUser.addFranchiseUser(driver, newUserNameF, password, newFranchiseId, roleName, emailId);

			fc.utobj().printTestStep(" Go to Info Mgr > Franchisees > Owner > Owner Details > Actions > Add Lead");
			fc.infomgr().infomgr_common().InfoMgrFranchisees(driver);
			franchisees.searchFranchise(driver, newFranchiseId);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'" + newFranchiseId + "')]"));
			fc.utobj().clickElement(driver, centerInfoPage.tabOwners);
			try {
				fc.utobj().actionImgOption(driver, newFirstNameF + " " + newFirstNameF, "Add Lead");
			} catch (Exception e) {
				// fc.utobj().actionImgOption(driver, newFirstNameF+"
				// "+newFirstNameF , "Add Lead");
			}
			String ownerName = "FranConnect Administrator";
			fc.utobj().sendKeys(driver, pobj.selectOwner, firstNameF + " " + firstNameF);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					(".//div[@tabindex='-1' and contains(text(),'" + firstNameF + " " + firstNameF + "')]")));

			/*
			 * fc.utobj().sendKeys(driver, pobj.firstName, firstName);
			 * fc.utobj().sendKeys(driver, pobj.lastName, lastName);
			 */
			fc.utobj().selectDropDownByVisibleText(driver, pobj.country, country);

			fc.utobj().selectDropDownByVisibleText(driver, pobj.state, state);
			fc.utobj().sendKeys(driver, pobj.emailID, emailId);
			/*
			 * fc.utobj().sendKeys(driver, pobj.zip, "123456");
			 * fc.utobj().sendKeys(driver, pobj.phone, "12345678");
			 * fc.utobj().sendKeys(driver, pobj.address, "New Highway Road");
			 */
			fc.utobj().selectDropDownByVisibleText(driver, pobj.leadOwnerID, ownerName);
			fc.utobj().selectDropDownByVisibleText(driver, pobj.leadSource2ID, leadSourceCategory);

			fc.utobj().selectDropDownByVisibleText(driver, pobj.leadSource3ID, leadSourceDetails);
			fc.utobj().clickElement(driver, pobj.save);

			boolean isFirstNamePopulated = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text(),'" + firstNameF + "')]");
			// boolean isLastNamePopulated= fc.utobj().verifyCase(driver,
			// ".//td[contains(text(),'Last
			// Name')]/following-sibling::td[contains(text(),'"+firstNameF+"')][1]");
			if (isFirstNamePopulated == false) {
				fc.utobj().throwsException("Added lead First name and last name not verified");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales", "sales_failed" , "TC_SalesVerifyExistingLeadOwnercombo_004" }) // 
	@TestCase(createdOn = "2017-08-3", updatedOn = "2017-08-30", testCaseId = "TC_SalesVerifyExistingLeadOwnercombo_004", testCaseDescription = "Verify the multiple Existing Lead can be selected at lead modify page")
	void verifyLeadOwnercombo_004() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FSLeadSummaryAddLeadPage pobj = new FSLeadSummaryAddLeadPage(driver);
			AddLeadFromAllSources addlead = new AddLeadFromAllSources();
			FSSearchPageTest p3 = new FSSearchPageTest();
			AdminFranchiseLocationAddFranchiseLocationPageTest franchiseLocation = new AdminFranchiseLocationAddFranchiseLocationPageTest();

			String userName = fc.utobj().generateTestData(dataSet.get("userName"));
			String franchiseId = fc.utobj().generateTestData(dataSet.get("franchiseId"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			String userFullName = userName + " " + userName;
			String password = "T0n1ght1";
			String userNameF = fc.utobj().generateTestData(dataSet.get("userNameF"));
			String firstNameF = fc.utobj().generateTestData(dataSet.get("firstNameF"));
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String roleName = "Default Franchise Role";
			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String country = dataSet.get("country");
			String state = dataSet.get("state");
			String firstName2 = fc.utobj().generateTestData(dataSet.get("firstNamenew"));
			String lastName2 = fc.utobj().generateTestData(dataSet.get("lastNamenew"));
			String firstName3 = fc.utobj().generateTestData(dataSet.get("firstNamethird"));
			String lastName3 = fc.utobj().generateTestData(dataSet.get("lastNamethird"));

			// String leadOwner=dataSet.get("leadOwner");
			String leadSourceCategory = dataSet.get("leadSourceCategory");
			String leadSourceDetails = dataSet.get("leadSourceDetails");
			String emailId = "salesautomation@staffex.com";

			fc.utobj().printTestStep("Admin > Hidden Links > Link Lead With Existing Lead / Owner > Set Yes");
			fc.adminpage().adminPage(driver);
			fc.adminpage().openLinkLeadWithExistingLeadorOwner(driver);
			if (!pobj.linkLeadYes.isSelected()) {
				fc.utobj().clickElement(driver, pobj.linkLeadYes);
			}
			fc.utobj().clickElement(driver, pobj.save);

			fc.utobj().printTestStep(" Sales > Lead Summary > Add 2 Leads.");
			Map<String, String> leadInfo = new HashMap<String, String>();
			leadInfo.put("firstName", firstName);
			leadInfo.put("lastName", lastName);
			leadInfo.put("country", country);
			leadInfo.put("state", state);
			leadInfo.put("email", emailId);
			leadInfo.put("leadSourceCategory", leadSourceCategory);
			leadInfo.put("leadSourceDetails", leadSourceDetails);
			leadInfo.put("leadOwner", "FranConnect Administrator");
			addlead.addLeadFromSystem(driver, leadInfo);

			Map<String, String> leadInfo2 = new HashMap<String, String>();
			leadInfo2.put("firstName", firstName2);
			leadInfo2.put("lastName", lastName2);
			leadInfo2.put("country", country);
			leadInfo2.put("state", state);
			leadInfo2.put("email", emailId);
			leadInfo2.put("leadSourceCategory", leadSourceCategory);
			leadInfo2.put("leadSourceDetails", leadSourceDetails);
			leadInfo2.put("leadOwner", "FranConnect Administrator");
			addlead.addLeadFromSystem(driver, leadInfo2);

			fc.utobj().printTestStep(
					" Sales > Lead Summary > Add another Lead > Save and modify > Select Lead Type > Existing lead > select one lead and then another > Verify both the leads are getting associated with this lead.");
			Sales fsmod = new Sales();
			fsmod.leadManagement(driver);

			clickAddLeadLink(driver);

			fc.utobj().sendKeys(driver, pobj.firstName, firstName3);
			fc.utobj().sendKeys(driver, pobj.lastName, lastName3);
			fc.utobj().selectDropDownByVisibleText(driver, pobj.country, country);

			fc.utobj().selectDropDownByVisibleText(driver, pobj.state, state);
			fc.utobj().sendKeys(driver, pobj.emailID, emailId);
			fc.utobj().sendKeys(driver, pobj.zip, "123456");
			fc.utobj().sendKeys(driver, pobj.phone, "12345678");
			fc.utobj().sendKeys(driver, pobj.address, "New Highway Road");
			fc.utobj().selectDropDownByVisibleText(driver, pobj.leadOwnerID, "FranConnect Administrator");
			fc.utobj().selectDropDownByVisibleText(driver, pobj.leadSource2ID, leadSourceCategory);

			fc.utobj().selectDropDownByVisibleText(driver, pobj.leadSource3ID, leadSourceDetails);

			fc.utobj().clickElement(driver, pobj.save);

			fc.utobj().clickElement(driver, pobj.modifyLead);
			fc.utobj().selectDropDown(driver, pobj.ownerType, "Existing Lead");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='autoComplete']"),
					leadInfo.get("firstName") + " " + leadInfo.get("lastName"));
			
			fc.utobj().sleep();

			try {
				fc.utobj().clickElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fc-id-1']/li[1]/div[1]"));
			} catch (Exception e) {
				fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='autoComplete']"),
						leadInfo.get("firstName") + " " + leadInfo.get("lastName"));

				fc.utobj().clickElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fc-id-1']/li[1]/div[1]"));
			}
			fc.utobj().clickElement(driver, pobj.save);
			String name = firstName3 + " " + lastName3 + ", " + firstName + " " + lastName;
			String name1 = firstName + " " + lastName + ", " + firstName3 + " " + lastName3;
			boolean isExistingLeadPresent = fc.utobj().assertPageSource(driver, name);
			boolean isExistingLeadPresentOr = fc.utobj().assertPageSource(driver, name1);
			if (isExistingLeadPresent == false && isExistingLeadPresentOr == false) {
				fc.utobj().throwsException("Existing Lead not verified on associating first lead adding lead");
			}

			fc.utobj().clickElement(driver, pobj.modifyLead);
			// fc.utobj().selectDropDown(driver, pobj.ownerType, "Existing
			// Lead");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='autoComplete']"),
					leadInfo2.get("firstName") + " " + leadInfo2.get("lastName"));

			try {
				fc.utobj().clickElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fc-id-1']/li[1]/div[1]"));
			} catch (Exception e) {
				fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='autoComplete']"),
						leadInfo2.get("firstName") + " " + leadInfo2.get("lastName"));

				fc.utobj().clickElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fc-id-1']/li[1]/div[1]"));
			}
			fc.utobj().clickElement(driver, pobj.save);
			String name2 = firstName3 + " " + lastName3 + ", " + firstName + " " + lastName + ", " + firstName2 + " "
					+ lastName2;
			name = firstName + " " + lastName + ", " + firstName2 + " " + lastName2 + ", " + firstName3 + " "
					+ lastName3;
			name1 = firstName2 + " " + lastName2 + ", " + firstName + " " + lastName + ", " + firstName3 + " "
					+ lastName3;

			boolean isExistingLeadPresentthird = fc.utobj().assertPageSource(driver, name2);
			isExistingLeadPresent = fc.utobj().assertPageSource(driver, name);
			isExistingLeadPresentOr = fc.utobj().assertPageSource(driver, name1);
			if (isExistingLeadPresentthird == false && isExistingLeadPresent == false
					&& isExistingLeadPresentOr == false) {
				fc.utobj().throwsException("Existing Lead not verified on associating second lead ");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales", "TC_SalesVerifyExistingLeadOwnercombo_005" , "sales_failed" }) // Verified // akshat
	@TestCase(createdOn = "2017-08-30", updatedOn = "2017-08-30", testCaseId = "TC_SalesVerifyExistingLeadOwnercombo_005", testCaseDescription = "Verify the archived lead is not getting searched in existing lead text box.")
	void verifyLeadOwnercombo_005() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FSLeadSummaryAddLeadPage pobj = new FSLeadSummaryAddLeadPage(driver);
			LeadSummaryUI npobj = new LeadSummaryUI(driver);
			AddLeadFromAllSources addlead = new AddLeadFromAllSources();
			FSSearchPageTest p3 = new FSSearchPageTest();
			AdminFranchiseLocationAddFranchiseLocationPageTest franchiseLocation = new AdminFranchiseLocationAddFranchiseLocationPageTest();

			String userName = fc.utobj().generateTestData(dataSet.get("userName"));
			String franchiseId = fc.utobj().generateTestData(dataSet.get("franchiseId"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			String userFullName = userName + " " + userName;
			String password = "T0n1ght1";
			String userNameF = fc.utobj().generateTestData(dataSet.get("userNameF"));
			String firstNameF = fc.utobj().generateTestData(dataSet.get("firstNameF"));
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String roleName = "Default Franchise Role";
			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String country = dataSet.get("country");
			String state = dataSet.get("state");
			String firstName2 = fc.utobj().generateTestData(dataSet.get("firstNamenew"));
			String lastName2 = fc.utobj().generateTestData(dataSet.get("lastNamenew"));

			// String leadOwner=dataSet.get("leadOwner");
			String leadSourceCategory = dataSet.get("leadSourceCategory");
			String leadSourceDetails = dataSet.get("leadSourceDetails");
			String emailId = "salesautomation@staffex.com";

			fc.utobj().printTestStep("Admin > Hidden Links > Link Lead With Existing Lead / Owner > Set Yes");
			fc.adminpage().adminPage(driver);
			fc.adminpage().openLinkLeadWithExistingLeadorOwner(driver);
			if (!pobj.linkLeadYes.isSelected()) {
				fc.utobj().clickElement(driver, pobj.linkLeadYes);
			}
			fc.utobj().clickElement(driver, pobj.save);

			fc.utobj().printTestStep(" Sales > Lead Summary > Add 2 Leads.");
			Map<String, String> leadInfo = new HashMap<String, String>();
			leadInfo.put("firstName", firstName);
			leadInfo.put("lastName", lastName);
			leadInfo.put("country", country);
			leadInfo.put("state", state);
			leadInfo.put("email", emailId);
			leadInfo.put("leadSourceCategory", leadSourceCategory);
			leadInfo.put("leadSourceDetails", leadSourceDetails);
			leadInfo.put("leadOwner", "FranConnect Administrator");
			addlead.addLeadFromSystem(driver, leadInfo);

			fc.utobj().printTestStep("Archive this lead");
			archiveLead(driver, firstName, lastName);

			Map<String, String> leadInfo2 = new HashMap<String, String>();
			leadInfo2.put("firstName", firstName2);
			leadInfo2.put("lastName", lastName2);
			leadInfo2.put("country", country);
			leadInfo2.put("state", state);
			leadInfo2.put("email", emailId);
			leadInfo2.put("leadSourceCategory", leadSourceCategory);
			// leadInfo2.put("leadType", "Existing Lead");
			// leadInfo2.put("existingOwner", leadInfo.get("firstName")+"
			// "+leadInfo.get("lastName"));
			leadInfo2.put("leadSourceDetails", leadSourceDetails);
			leadInfo2.put("leadOwner", "FranConnect Administrator");
			addlead.addLeadFromSystem(driver, leadInfo2);

			fc.utobj().clickElement(driver, pobj.modifyLead);
			fc.utobj().selectDropDown(driver, pobj.ownerType, "Existing Lead");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='autoComplete']"),
					leadInfo2.get("firstName") + " " + leadInfo2.get("lastName"));

			try {
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='fc-id-1']/li[1]/div"));
				fc.utobj().throwsException("Archived lead searchable for existind lead type");
			} catch (Exception e) {

			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales", "TC_SalesVerifyExistingLeadOwnercombo_006" }) // verified // akshat
	@TestCase(createdOn = "2017-08-30", updatedOn = "2017-08-30", testCaseId = "TC_SalesVerifyExistingLeadOwnercombo_006", testCaseDescription = "Verify the Deleted lead is not getting searched in existing lead text box.")
	void verifyLeadOwnercombo_006() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FSLeadSummaryAddLeadPage pobj = new FSLeadSummaryAddLeadPage(driver);
			LeadSummaryUI npobj = new LeadSummaryUI(driver);
			AddLeadFromAllSources addlead = new AddLeadFromAllSources();
			FSSearchPageTest p3 = new FSSearchPageTest();
			AdminFranchiseLocationAddFranchiseLocationPageTest franchiseLocation = new AdminFranchiseLocationAddFranchiseLocationPageTest();

			String userName = fc.utobj().generateTestData(dataSet.get("userName"));
			String franchiseId = fc.utobj().generateTestData(dataSet.get("franchiseId"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			String userFullName = userName + " " + userName;
			String password = "T0n1ght1";
			String userNameF = fc.utobj().generateTestData(dataSet.get("userNameF"));
			String firstNameF = fc.utobj().generateTestData(dataSet.get("firstNameF"));
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String roleName = "Default Franchise Role";
			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String country = dataSet.get("country");
			String state = dataSet.get("state");
			String firstName2 = fc.utobj().generateTestData(dataSet.get("firstNamenew"));
			String lastName2 = fc.utobj().generateTestData(dataSet.get("lastNamenew"));

			// String leadOwner=dataSet.get("leadOwner");
			String leadSourceCategory = dataSet.get("leadSourceCategory");
			String leadSourceDetails = dataSet.get("leadSourceDetails");
			String emailId = "salesautomation@staffex.com";

			fc.utobj().printTestStep("Admin > Hidden Links > Link Lead With Existing Lead / Owner > Set Yes");
			fc.adminpage().adminPage(driver);
			fc.adminpage().openLinkLeadWithExistingLeadorOwner(driver);
			if (!pobj.linkLeadYes.isSelected()) {
				fc.utobj().clickElement(driver, pobj.linkLeadYes);
			}
			fc.utobj().clickElement(driver, pobj.save);

			fc.utobj().printTestStep(" Sales > Lead Summary > Add Lead.");
			Map<String, String> leadInfo = new HashMap<String, String>();
			leadInfo.put("firstName", firstName);
			leadInfo.put("lastName", lastName);
			leadInfo.put("country", country);
			leadInfo.put("state", state);
			leadInfo.put("email", emailId);
			leadInfo.put("leadSourceCategory", leadSourceCategory);
			leadInfo.put("leadSourceDetails", leadSourceDetails);
			leadInfo.put("leadOwner", "FranConnect Administrator");
			addlead.addLeadFromSystem(driver, leadInfo);

			fc.utobj().printTestStep("Delete this lead");
			p3.searchByLeadName(driver, firstName, lastName);
			fc.utobj().clickElement(driver, npobj.checkBoxAll);
			fc.utobj().selectActionMenuItems(driver, "Delete");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//input[@name='Ok']"));
			// archiveLead(driver, firstName, lastName);

			Map<String, String> leadInfo2 = new HashMap<String, String>();
			leadInfo2.put("firstName", firstName2);
			leadInfo2.put("lastName", lastName2);
			leadInfo2.put("country", country);
			leadInfo2.put("state", state);
			leadInfo2.put("email", emailId);
			leadInfo2.put("leadSourceCategory", leadSourceCategory);
			// leadInfo2.put("existingOwner", leadInfo.get("firstName")+"
			// "+leadInfo.get("lastName"));
			leadInfo2.put("leadSourceDetails", leadSourceDetails);
			leadInfo2.put("leadOwner", "FranConnect Administrator");
			addlead.addLeadFromSystem(driver, leadInfo2);

			fc.utobj().clickElement(driver, pobj.modifyLead);
			fc.utobj().selectDropDown(driver, pobj.ownerType, "Existing Lead");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='autoComplete']"),
					leadInfo.get("firstName") + " " + leadInfo.get("lastName"));

			try {
				fc.utobj().clickElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fc-id-1']/li[1]/div[1]"));
				fc.utobj().throwsException("Deleted lead searchable for existind lead type");
			} catch (Exception e) {

			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales", "saleschk0045ss" , "TC_Sales_AdvancedSearch_CallDetails_001" }) // Fixed : Verified // Akshat
	@TestCase(createdOn = "2017-10-06", updatedOn = "2017-10-06", testCaseId = "TC_Sales_AdvancedSearch_CallDetails_001", testCaseDescription = " Verify the advanced search working for call details")
	void verifyAdvanceSearchCallDetails() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			LeadSummaryUI pobj = new LeadSummaryUI(driver);
			Sales fsmod = new Sales();

			String firstName = fc.utobj().generateTestData(dataSet.get("FirstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("LastName"));
			String callSubject = fc.utobj().generateTestData(dataSet.get("CallSubject"));
			String userName = "FranConnect Administrator";

			fc.utobj().printTestStep("Sales > Add Lead > log a call");
			Map<String, String> leadName = addLeadSummaryWithLeadNameOwnerName(driver, firstName, lastName, userName);
			fc.utobj().printTestStep("Now perform log a call");
			// log a call
			fc.utobj().clickElement(driver, pobj.LogACallLnk);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.callSubject, callSubject);
			fc.utobj().clickElement(driver, pobj.addCallBtn);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//input[@value='No']"));

			fsmod.search(driver);
			fc.utobj().printTestStep(" Go to Advanced Search");
			fc.utobj().clickElement(driver, pobj.advancedSearch);
			fc.utobj().printTestStep("Check the checkbox for call details");
			fc.utobj().clickElement(driver, pobj.advSearchActivityHistoryCalls);
			fc.utobj().clickElement(driver, pobj.searchData);
			fc.utobj().printTestStep(" Enter the search parameter and Search");
			fc.utobj().sendKeys(driver, pobj.searchCallSubject, callSubject);
			fc.utobj().clickElement(driver, pobj.veiwDataBtn);

			fc.utobj().printTestStep(" The lead should be searched.");
			boolean isLeadSearched = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text () , '" + firstName + " " + lastName + "')]");
			if (isLeadSearched == false) {
				fc.utobj().throwsException("Lead not getting searched from advanced search log a call");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			// e.printStackTrace();
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales_old", "salesSolrLeads1" })
	@TestCase(createdOn = "2017-11-09", updatedOn = "2017-11-09", testCaseId = "TC_Solr_Search_Corp_Leads_001", testCaseDescription = " Verify the Solr search working for Lead added by corporate user")
	void verifySolrSearchCorporateLead() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			LeadSummaryUI pobj = new LeadSummaryUI(driver);
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			SearchUI search_page = new SearchUI(driver);

			String firstNamePre = fc.utobj().generateTestData(dataSet.get("firstName"));
			String firstName1 = firstNamePre + dataSet.get("firstName1");
			String firstName2 = firstNamePre + dataSet.get("firstName2");
			String firstName3 = firstNamePre + dataSet.get("firstName3");
			String firstName4 = firstNamePre + dataSet.get("firstName4");
			String firstName5 = firstNamePre + dataSet.get("firstName5");
			String firstName6 = firstNamePre + dataSet.get("firstName6");
			String firstName7 = firstNamePre + dataSet.get("firstName7");
			String firstName8 = firstNamePre + dataSet.get("firstName8");

			String lastName = dataSet.get("lastName");
			/*
			 * String lastName1 = lastNamePre+dataSet.get("lastName1"); String
			 * lastName2 = lastNamePre+dataSet.get("lastName2"); String
			 * lastName3 = lastNamePre+dataSet.get("lastName3"); String
			 * lastName4 = lastNamePre+dataSet.get("lastName4"); String
			 * lastName5 = lastNamePre+dataSet.get("lastName5"); String
			 * lastName6 = lastNamePre+dataSet.get("lastName6"); String
			 * lastName7 = lastNamePre+dataSet.get("lastName7"); String
			 * lastName8 = lastNamePre+dataSet.get("lastName8");
			 */
			String userName = fc.utobj().generateTestData(dataSet.get("userName"));
			String userNameCorp = userName;
			String password = "T0n1ght1";

			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "salesautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = corpTest.createDefaultUser(driver, corpUser);

			fc.utobj().printTestStep("Login with Corporate User");
			fc.home_page().logout(driver);
			fc.loginpage().loginWithParameter(driver, userNameCorp, "T0n1ght1");

			fc.sales().sales_common().fsModule(driver);
			fc.utobj().printTestStep("Add 8 leads");

			addLeadSummaryWithLeadNameOwnerName(driver, firstName1, lastName, userName);
			addLeadSummaryWithLeadNameOwnerName(driver, firstName2, lastName, userName);
			addLeadSummaryWithLeadNameOwnerName(driver, firstName3, lastName, userName);
			addLeadSummaryWithLeadNameOwnerName(driver, firstName4, lastName, userName);
			addLeadSummaryWithLeadNameOwnerName(driver, firstName5, lastName, userName);
			addLeadSummaryWithLeadNameOwnerName(driver, firstName6, lastName, userName);
			addLeadSummaryWithLeadNameOwnerName(driver, firstName7, lastName, userName);
			addLeadSummaryWithLeadNameOwnerName(driver, firstName8, lastName, userName);

			boolean isSearchTrue = false;
			boolean isVeiwAll = false;
			for (int i = 0; i < 3; i++) {
				if (isSearchTrue == false) {
					fc.utobj().sendKeys(driver, search_page.topSearchField, firstNamePre);

					if (i == 2) {

					}
					fc.utobj().clickEnterOnElement(driver, search_page.topSearchField);

					fc.utobj().moveToElement(driver, driver
							.findElement(By.xpath(".//span/*[contains(text(),'" + firstName1 + lastName + "')]")));
					isSearchTrue = true;
				}
			}
			if (isSearchTrue == false) {
				fc.utobj().throwsException("was not able to search Lead through Top Search");
			}
			// fc.utobj().moveToElement(driver,
			// fc.utobj().getElementByXpath(driver,".//span/*[contains(text(),'"+brokerName2+"')]")));
			// fc.utobj().moveToElement(driver,
			// fc.utobj().getElementByXpath(driver,".//span/*[contains(text(),'"+brokerName3+"')]")));
			// fc.utobj().moveToElement(driver,
			// fc.utobj().getElementByXpath(driver,".//span/*[contains(text(),'"+brokerName8+"')]")));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[@ng-click='viewAll()']"));

			isVeiwAll = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + firstName1 + lastName + "')]");
			isVeiwAll = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + firstName2 + lastName + "')]");
			isVeiwAll = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + firstName3 + lastName + "')]");
			isVeiwAll = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + firstName4 + lastName + "')]");
			isVeiwAll = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + firstName5 + lastName + "')]");
			isVeiwAll = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + firstName6 + lastName + "')]");
			isVeiwAll = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + firstName7 + lastName + "')]");
			isVeiwAll = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + firstName8 + lastName + "')]");
			if (isVeiwAll == false) {
				fc.utobj().throwsException("All elements not visible on clicking view all");
			}
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'" + firstName8 + lastName + "')]"));
			// fc.utobj().switchFrame(driver,
			// fc.CommonMethods().switch_frameClass_newLayoutcboxIframe(driver));
			boolean isBrokerVerified = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text(),'" + firstName8 + lastName + "')]");
			if (isBrokerVerified == false) {
				fc.utobj().throwsException("Leads not verified on click at search");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales_old", "salesSolrRLeads1" })
	@TestCase(createdOn = "2017-11-09", updatedOn = "2017-11-09", testCaseId = "TC_Solr_Search_Regional_Leads_001", testCaseDescription = " Verify the Solr search working for Lead added by corporate user")
	void verifySolrSearchRegionalLead() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			LeadSummaryUI pobj = new LeadSummaryUI(driver);
			SearchUI search_page = new SearchUI(driver);
			AddLeadFromAllSources addLead = new AddLeadFromAllSources();

			String firstNamePre = fc.utobj().generateTestData(dataSet.get("firstName"));
			String firstName1 = firstNamePre + dataSet.get("firstName1");
			String firstName2 = firstNamePre + dataSet.get("firstName2");
			String firstName3 = firstNamePre + dataSet.get("firstName3");
			String firstName4 = firstNamePre + dataSet.get("firstName4");
			String firstName5 = firstNamePre + dataSet.get("firstName5");
			String firstName6 = firstNamePre + dataSet.get("firstName6");
			String firstName7 = firstNamePre + dataSet.get("firstName7");
			String firstName8 = firstNamePre + dataSet.get("firstName8");

			String lastName = dataSet.get("lastName");
			String userName = fc.utobj().generateTestData(dataSet.get("userName"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			String userNameCorp = userName;
			String password = "T0n1ght1";

			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "salesautomation@staffex.com";
			AdminUsersManageRegionalUsersAddRegionalUserPageTest RegUser = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
			RegUser.addRegionalUser(driver, userName, regionName, emailId);

			fc.utobj().printTestStep("Login with Corporate User");
			fc.home_page().logout(driver);
			fc.loginpage().loginWithParameter(driver, userNameCorp, "T0n1ght1");

			fc.sales().sales_common().fsModule(driver);
			fc.utobj().printTestStep("Add 8 leads");

			fc.utobj().printTestStep(" Add a lead with this region and user should be different.");
			Map<String, String> leadInfo = new HashMap<String, String>();
			leadInfo.put("firstName", firstName1);
			leadInfo.put("lastName", lastName);
			leadInfo.put("country", "USA");
			leadInfo.put("state", "Alabama");
			leadInfo.put("email", emailId);
			leadInfo.put("leadOwner", userName);
			leadInfo.put("leadSourceCategory", "Friends");
			leadInfo.put("leadSourceDetails", "Friends");
			addLead.addLeadFromSystem(driver, leadInfo);

			leadInfo.put("firstName", firstName2);
			addLead.addLeadFromSystem(driver, leadInfo);

			leadInfo.put("firstName", firstName3);
			addLead.addLeadFromSystem(driver, leadInfo);

			leadInfo.put("firstName", firstName4);
			addLead.addLeadFromSystem(driver, leadInfo);

			leadInfo.put("firstName", firstName5);
			addLead.addLeadFromSystem(driver, leadInfo);

			leadInfo.put("firstName", firstName6);
			addLead.addLeadFromSystem(driver, leadInfo);

			leadInfo.put("firstName", firstName7);
			addLead.addLeadFromSystem(driver, leadInfo);

			leadInfo.put("firstName", firstName8);
			addLead.addLeadFromSystem(driver, leadInfo);

			addLeadSummaryWithLeadNameOwnerName(driver, firstName1, lastName, userName);
			addLeadSummaryWithLeadNameOwnerName(driver, firstName2, lastName, userName);
			addLeadSummaryWithLeadNameOwnerName(driver, firstName3, lastName, userName);
			addLeadSummaryWithLeadNameOwnerName(driver, firstName4, lastName, userName);
			addLeadSummaryWithLeadNameOwnerName(driver, firstName5, lastName, userName);
			addLeadSummaryWithLeadNameOwnerName(driver, firstName6, lastName, userName);
			addLeadSummaryWithLeadNameOwnerName(driver, firstName7, lastName, userName);
			addLeadSummaryWithLeadNameOwnerName(driver, firstName8, lastName, userName);

			boolean isSearchTrue = false;
			boolean isVeiwAll = false;
			for (int i = 0; i < 3; i++) {
				if (isSearchTrue == false) {
					fc.utobj().sendKeys(driver, search_page.topSearchField, firstNamePre);

					if (i == 2) {

					}
					fc.utobj().clickEnterOnElement(driver, search_page.topSearchField);

					fc.utobj().moveToElement(driver, driver.findElement(
							By.xpath(".//span/*[contains(text(),'" + firstName8 + " " + lastName + "')]")));
					isSearchTrue = true;
				}
			}
			if (isSearchTrue == false) {
				fc.utobj().throwsException("was not able to search Lead through Top Search");
			}
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[@ng-click='viewAll()']"));

			isVeiwAll = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + firstName1 + " " + lastName + "')]");
			isVeiwAll = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + firstName2 + " " + lastName + "')]");
			isVeiwAll = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + firstName3 + " " + lastName + "')]");
			isVeiwAll = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + firstName4 + " " + lastName + "')]");
			isVeiwAll = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + firstName5 + " " + lastName + "')]");
			isVeiwAll = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + firstName6 + " " + lastName + "')]");
			isVeiwAll = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + firstName7 + " " + lastName + "')]");
			isVeiwAll = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + firstName8 + " " + lastName + "')]");
			if (isVeiwAll == false) {
				fc.utobj().throwsException("All elements not visible on clicking view all");
			}
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'" + firstName8 + lastName + "')]"));
			// fc.utobj().switchFrame(driver,
			// fc.CommonMethods().switch_frameClass_newLayoutcboxIframe(driver));
			boolean isBrokerVerified = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text(),'" + firstName8 + lastName + "')]");
			if (isBrokerVerified == false) {
				fc.utobj().throwsException("Leads not verified on click at search");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales_old", "salesSolrDLeads1" })
	@TestCase(createdOn = "2017-11-14", updatedOn = "2017-11-14", testCaseId = "TC_Solr_Search_Divisional_Leads_001", testCaseDescription = " Verify the Solr search working for Lead added by corporate user")
	void verifySolrSearchDivisionalLead() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			LeadSummaryUI pobj = new LeadSummaryUI(driver);
			SearchUI search_page = new SearchUI(driver);
			AddLeadFromAllSources addLead = new AddLeadFromAllSources();

			String firstNamePre = fc.utobj().generateTestData(dataSet.get("firstName"));
			String firstName1 = firstNamePre + dataSet.get("firstName1");
			String firstName2 = firstNamePre + dataSet.get("firstName2");
			String firstName3 = firstNamePre + dataSet.get("firstName3");
			String firstName4 = firstNamePre + dataSet.get("firstName4");
			String firstName5 = firstNamePre + dataSet.get("firstName5");
			String firstName6 = firstNamePre + dataSet.get("firstName6");
			String firstName7 = firstNamePre + dataSet.get("firstName7");
			String firstName8 = firstNamePre + dataSet.get("firstName8");

			String lastName = dataSet.get("lastName");
			String userName = fc.utobj().generateTestData(dataSet.get("userName"));
			String divisionName = fc.utobj().generateTestData(dataSet.get("divisionName"));
			String userNameCorp = userName;
			String password = "T0n1ght1";

			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "salesautomation@staffex.com";
			/*
			 * AdminUsersManageRegionalUsersAddRegionalUserPageTest RegUser=new
			 * AdminUsersManageRegionalUsersAddRegionalUserPageTest();
			 * RegUser.addRegionalUser(driver, userName, regionName, config,
			 * emailId);
			 */
			AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest divUser = new AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest();
			divUser.addDivisionalUser(driver, userName, divisionName, emailId);

			fc.utobj().printTestStep("Login with Corporate User");
			fc.home_page().logout(driver);
			fc.loginpage().loginWithParameter(driver, userNameCorp, "T0n1ght1");
			fc.sales().sales_common().fsModule(driver);
			fc.utobj().printTestStep("Add 8 leads");

			fc.utobj().printTestStep(" Add a lead with this region and user should be different.");
			Map<String, String> leadInfo = new HashMap<String, String>();
			leadInfo.put("firstName", firstName1);
			leadInfo.put("lastName", lastName);
			leadInfo.put("country", "USA");
			leadInfo.put("state", "Alabama");
			leadInfo.put("email", emailId);
			leadInfo.put("leadOwner", userName);
			leadInfo.put("leadSourceCategory", "Friends");
			leadInfo.put("leadSourceDetails", "Friends");
			addLead.addLeadFromSystem(driver, leadInfo);

			leadInfo.put("firstName", firstName2);
			addLead.addLeadFromSystem(driver, leadInfo);

			leadInfo.put("firstName", firstName3);
			addLead.addLeadFromSystem(driver, leadInfo);

			leadInfo.put("firstName", firstName4);
			addLead.addLeadFromSystem(driver, leadInfo);

			leadInfo.put("firstName", firstName5);
			addLead.addLeadFromSystem(driver, leadInfo);

			leadInfo.put("firstName", firstName6);
			addLead.addLeadFromSystem(driver, leadInfo);

			leadInfo.put("firstName", firstName7);
			addLead.addLeadFromSystem(driver, leadInfo);

			leadInfo.put("firstName", firstName8);
			addLead.addLeadFromSystem(driver, leadInfo);

			addLeadSummaryWithLeadNameOwnerName(driver, firstName1, lastName, userName);
			addLeadSummaryWithLeadNameOwnerName(driver, firstName2, lastName, userName);
			addLeadSummaryWithLeadNameOwnerName(driver, firstName3, lastName, userName);
			addLeadSummaryWithLeadNameOwnerName(driver, firstName4, lastName, userName);
			addLeadSummaryWithLeadNameOwnerName(driver, firstName5, lastName, userName);
			addLeadSummaryWithLeadNameOwnerName(driver, firstName6, lastName, userName);
			addLeadSummaryWithLeadNameOwnerName(driver, firstName7, lastName, userName);
			addLeadSummaryWithLeadNameOwnerName(driver, firstName8, lastName, userName);

			boolean isSearchTrue = false;
			boolean isVeiwAll = false;
			for (int i = 0; i < 3; i++) {
				if (isSearchTrue == false) {
					fc.utobj().sendKeys(driver, search_page.topSearchField, firstNamePre);

					if (i == 2) {

					}
					fc.utobj().clickEnterOnElement(driver, search_page.topSearchField);

					fc.utobj().moveToElement(driver, driver.findElement(
							By.xpath(".//span/*[contains(text(),'" + firstName8 + " " + lastName + "')]")));
					isSearchTrue = true;
				}
			}
			if (isSearchTrue == false) {
				fc.utobj().throwsException("was not able to search Lead through Top Search");
			}
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[@ng-click='viewAll()']"));

			isVeiwAll = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + firstName1 + " " + lastName + "')]");
			isVeiwAll = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + firstName2 + " " + lastName + "')]");
			isVeiwAll = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + firstName3 + " " + lastName + "')]");
			isVeiwAll = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + firstName4 + " " + lastName + "')]");
			isVeiwAll = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + firstName5 + " " + lastName + "')]");
			isVeiwAll = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + firstName6 + " " + lastName + "')]");
			isVeiwAll = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + firstName7 + " " + lastName + "')]");
			isVeiwAll = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + firstName8 + " " + lastName + "')]");
			if (isVeiwAll == false) {
				fc.utobj().throwsException("All elements not visible on clicking view all");
			}
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'" + firstName8 + lastName + "')]"));
			// fc.utobj().switchFrame(driver,
			// fc.CommonMethods().switch_frameClass_newLayoutcboxIframe(driver));
			boolean isBrokerVerified = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text(),'" + firstName8 + lastName + "')]");
			if (isBrokerVerified == false) {
				fc.utobj().throwsException("Leads not verified on click at search");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void archiveLead(WebDriver driver, String firstName, String lastName) throws Exception {
		// FSLeadSummaryAddLeadPage pobj = new FSLeadSummaryAddLeadPage(driver);
		try {
			LeadSummaryUI pobj = new LeadSummaryUI(driver);
			FSSearchPageTest p3 = new FSSearchPageTest();
			p3.searchByLeadName(driver, firstName, lastName);
			fc.utobj().clickElement(driver, pobj.checkBoxAll);

			fc.utobj().selectActionMenuItems(driver, "Archive Leads");
			fc.utobj().acceptAlertBox(driver);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.notesArchive, "Test Leads Archived!");
			fc.utobj().clickElement(driver, pobj.notesSubmitBtn);
			fc.utobj().clickElement(driver, pobj.cboxCloseBtn);

		} catch (Exception e) {
			fc.utobj().throwsException("Lead not Archived");
		}
	}

	private String getCandidatePortal_FDD_UserName(WebDriver driver) throws Exception {
		String candidatePortalUserId = "";
		for (int x = 0; x < 30; x++) {
			if (candidatePortalUserId.isEmpty()) {
				candidatePortalUserId = fc.utobj().getElementByXpath(driver, ".//*[@id='printReady0']//td[contains(text(),'Candidate Portal Password')]/ancestor::tr/td[2][@class='bText12']").getText();
			} else {
				return candidatePortalUserId;
			}
		}
		return candidatePortalUserId;

	}

	private String getCandidatePortal_FDD_Password(WebDriver driver) throws Exception {
		String fddpassword = "";
		for (int x = 0; x < 30; x++) {
			if (fddpassword.isEmpty()) {
				// fddpassword = fc.utobj().getElementByXpath(driver,".//*[@id='printReady0']//td[contains(text(),'Candidate Portal Password')]/ancestor::td/following-sibling::td/table/tbody/tr/td").getText();
				fddpassword = fc.utobj().getElementByXpath(driver,".//*[@id='printReady0']//td[contains(text(),'Candidate Portal Password')]/following-sibling::td//td").getText();
			} else {
				return fddpassword;
			}
		}
		return fddpassword;
	}

}