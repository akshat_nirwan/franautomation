package com.builds.test.fs;

import java.util.Map;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.fs.FSLeadSummaryPersonalProfilePage;
import com.builds.utilities.FranconnectUtil;

public class FSLeadSummaryPersonalProfilePageTest {
	FranconnectUtil fc = new FranconnectUtil();

	// Anukaran starts
	public void fillPersonalProfileTab(WebDriver driver, Map<String, String> config) throws Exception {

		FSLeadSummaryPersonalProfilePage pobj = new FSLeadSummaryPersonalProfilePage(driver);

		fc.utobj().clickElement(driver, pobj.gender);
		fc.utobj().sendKeys(driver, pobj.homeAddress, "My home address");
		fc.utobj().sendKeys(driver, pobj.homeCity, "Home City");
		fc.utobj().clickElement(driver, pobj.maritalStatus);
		fc.utobj().sendKeys(driver, pobj.presentEmployer, "Present Employer");
		fc.utobj().sendKeys(driver, pobj.employmentTitle, "employment Title");
		fc.utobj().clickElement(driver, pobj.partner);
		fc.utobj().clickElement(driver, pobj.convictedForFelony);
		fc.utobj().sendKeys(driver, pobj.otherFacts, "Other Facts");
		fc.utobj().sendKeys(driver, pobj.otherComments, "Other Comments");
		fc.utobj().clickElement(driver, pobj.saveBtn);

	}
}
