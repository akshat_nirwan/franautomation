package com.builds.test.fs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminFDDManagementFDDEmailTemplateSummaryAddTemplatePageTest;
import com.builds.test.admin.AdminFDDmanagementConfigureEmailsentPriortoFDDEmailPageTest;
import com.builds.test.salesTest.Sales_Common_New;
import com.builds.uimaps.fs.FSLeadSummaryAddLeadPage;
import com.builds.uimaps.fs.FSLeadSummaryPrimaryInfoPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class FSLeadSummaryPrimaryInfoPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	// Log a Task Through Link
	// Not Verified

	@Test(groups = { "sales222222" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FS_PrimaryInfo_AddTask_001", testCaseDescription = "Verify Log A Task from primary info log a task link")
	private void logATaskLeadPrimaryInfoLnk() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String subject = fc.utobj().generateTestData(dataSet.get("subject"));

		try {
			driver = fc.loginpage().login(driver);

			fc.sales().sales_common().fsModule(driver);
			Sales fs = new Sales();
			fs.leadManagement(driver);
			FSLeadSummaryPageTest fsPage = new FSLeadSummaryPageTest();
			fsPage.clickAddLeadLink(driver);
			fc.utobj().printTestStep("Add a lead and Go to Primary Info");

			FSLeadSummaryPrimaryInfoPageTest pInfoPage = new FSLeadSummaryPrimaryInfoPageTest();
			Map<String, String> leadInfo = pInfoPage.fillLeadBasicInfo(driver);
			Reporter.log("Lead Added with lead name : " + leadInfo.get("firstName") + " " + leadInfo.get("lastName"));

			fc.utobj().clickPartialLinkText(driver, "Log a Task");
			fc.utobj().printTestStep("Log A Task");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='radioOwner' and @value='0']"));
			fc.utobj().selectDropDownByVisibleText(driver, fc.utobj().getElementByID(driver, "status"), "Not Started");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByID(driver, "subject"), subject);
			fc.utobj().clickElement(driver, fc.utobj().getElementByName(driver, "add"));
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().switchFrameById(driver, "leadOpenActivitesSummary");

			boolean isTaskAdded = fc.utobj().assertPageSource(driver, subject);

			if (isTaskAdded == false) {
				fc.utobj().throwsException("Task Not Added Successfully");
			}
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales","TC_FS_PrimaryInfo_ModifyLead_001" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FS_PrimaryInfo_ModifyLead_001", testCaseDescription = "Verify Modification of lead from Primary Info")
	public void modifyLeadPrimaryInfoLnk() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		// String testCaseId = "TC_FS_PrimaryInfo_ModifyLead_001";

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.sales().sales_common().fsModule(driver);
			Sales fs = new Sales();
			fs.leadManagement(driver);
			FSLeadSummaryPageTest fsPage = new FSLeadSummaryPageTest();
			fc.utobj().printTestStep("Add a lead and Go to Primary Info");
			fsPage.clickAddLeadLink(driver);
			FSLeadSummaryPrimaryInfoPageTest pInfoPage = new FSLeadSummaryPrimaryInfoPageTest();
			Map<String, String> leadInfo = pInfoPage.fillLeadBasicInfo(driver);
			Reporter.log("Lead Added with lead name : " + leadInfo.get("firstName") + " " + leadInfo.get("lastName"));
			fc.utobj().printTestStep("Modify Lead Info");
			fc.utobj().clickElement(driver, driver
					.findElement(By.xpath(".//*[@id='siteMainTable']/tbody//td[1]/a[contains(text(),'Modify')]")));
			
			Sales_Common_New sales_Common_New = new Sales_Common_New();
			sales_Common_New.minimizeNotificationFooter_ifMaximised(driver);
			
			fc.utobj().clickElement(driver, fc.utobj().getElementByID(driver, "Submit"));

			boolean isLeadSave = fc.utobj().assertPageSource(driver, leadInfo.get("firstName"));

			if (isLeadSave == false) {
				fc.utobj().throwsException("Unable to Modify Lead");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales" , "TC_FS_MoveToInfoMgr_001" }) // passed
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FS_MoveToInfoMgr_001", testCaseDescription = "Verify A Lead is moved to InfoMgr")
	public void moveToInfoMgrLeadPrimaryInfoLnk() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.sales().sales_common().fsModule(driver);
			Sales fs = new Sales();
			fc.utobj().printTestStep("Add a lead and Go to Primary Info");
			fs.leadManagement(driver);
			FSLeadSummaryPageTest fsPage = new FSLeadSummaryPageTest();
			fsPage.clickAddLeadLink(driver);
			FSLeadSummaryPrimaryInfoPageTest pInfoPage = new FSLeadSummaryPrimaryInfoPageTest();
			Map<String, String> leadInfo = pInfoPage.fillLeadBasicInfo(driver);
			Reporter.log("Lead Added with lead name : " + leadInfo.get("firstName") + " " + leadInfo.get("lastName"));
			fc.utobj().printTestStep("Move to Info Mgr");

			try {
				fc.utobj().clickElement(driver, fc.utobj().getElementByName(driver, "MoveToFIM"));
			} catch (Exception e) {
				fc.utobj().throwsException("Move to Info Mgr button not available");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales" , "TC_FS_AddAnotherLead_001" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FS_AddAnotherLead_001", testCaseDescription = "Verify add another lead from primary info")
	public void addAnotherLeadPrimaryInfoLnk() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.sales().sales_common().fsModule(driver);
			Sales fs = new Sales();
			fs.leadManagement(driver);
			FSLeadSummaryPageTest fsPage = new FSLeadSummaryPageTest();
			fsPage.clickAddLeadLink(driver);
			FSLeadSummaryPrimaryInfoPageTest pInfoPage = new FSLeadSummaryPrimaryInfoPageTest();
			Map<String, String> leadInfo = pInfoPage.fillLeadBasicInfo(driver);
			Reporter.log("Lead Added with lead name : " + leadInfo.get("firstName") + " " + leadInfo.get("lastName"));

			fc.utobj().printTestStep("Add a lead and Go to Primary Info");

			fc.utobj().clickElement(driver, fc.utobj().getElementByName(driver, "addLead"));

			fc.utobj().printTestStep("Add Another lead");

			pInfoPage.fillLeadBasicInfo(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales", "TC_FS_PrimaryInfo_AddTask_002" }) // new ui fixed
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FS_PrimaryInfo_AddTask_002", testCaseDescription = "Verify Log A Task from primary info Contact History Section")
	public void logATaskLeadContactHistoryLnk() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String subject = fc.utobj().generateTestData(dataSet.get("subject"));

		try {
			driver = fc.loginpage().login(driver);

			fc.sales().sales_common().fsModule(driver);
			Sales fs = new Sales();
			fs.leadManagement(driver);
			FSLeadSummaryPageTest fsPage = new FSLeadSummaryPageTest();
			fsPage.clickAddLeadLink(driver);
			fc.utobj().printTestStep("Add a lead and Go to Primary Info");

			FSLeadSummaryPrimaryInfoPageTest pInfoPage = new FSLeadSummaryPrimaryInfoPageTest();
			Map<String, String> leadInfo = pInfoPage.fillLeadBasicInfo(driver);
			Reporter.log("Lead Added with lead name : " + leadInfo.get("firstName") + " " + leadInfo.get("lastName"));
			fc.utobj().switchFrameById(driver, "leadOpenActivitesSummary");
			fc.utobj().clickElementByJS(driver, fc.utobj().getElementByPartialLinkText(driver, "Log a Task")); // new ui update
			fc.utobj().printTestStep("Log A Task");
			fc.utobj().switchFrameToDefault(driver);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='radioOwner' and @value='0']"));
			fc.utobj().selectDropDownByVisibleText(driver, fc.utobj().getElementByID(driver, "status"), "Not Started");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByID(driver, "subject"), subject);
			fc.utobj().clickElement(driver, fc.utobj().getElementByName(driver, "add"));
			fc.utobj().switchFrameById(driver, "leadOpenActivitesSummary");

			boolean isTaskAdded = fc.utobj().assertPageSource(driver, subject);

			if (isTaskAdded == false) {
				fc.utobj().throwsException("Task Not Added Successfully");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void processTaskInOpenTaskPrimaryInfo(WebDriver driver, String taskSubject) throws Exception {

		FSLeadSummaryPrimaryInfoPage pobj = new FSLeadSummaryPrimaryInfoPage(driver);
		fc.utobj().moveToElement(driver,
				fc.utobj().getElementByXpath(driver, ".//*[contains(text(),'Contact History')]"));
		try {
			// fc.utobj().switchFrame(driver, pobj.openTaskIFrame);
			// driver.switchTo().frame("leadOpenActivitesSummary");
			fc.utobj().switchFrameById(driver, "leadOpenActivitesSummary");
		} catch (Exception e) {

		}
		WebElement e2 = fc.utobj().getElementByXpath(driver, ".//*[contains(text(),'" + taskSubject
				+ "')]/ancestor::tr[@class='bText12']//div[@id='menuBar']/layer");
		String layerId = e2.getAttribute("id");
		String option = layerId.replace("Actions_dynamicmenu", "");
		option = option.replace("Bar", "");
		WebElement e1 = fc.utobj().getElementByXpath(driver, ".//*[contains(text(),'" + taskSubject
				+ "')]/ancestor::tr[@class='bText12']//div[@id='menuBar']/layer/a/img");
		fc.utobj().clickElement(driver, e1);
		driver.findElement(
				By.xpath(".//*[@id='Actions_dynamicmenu" + option + "Menu']/span[contains(text(),'Process')]")).click();
		driver = fc.utobj().switchFrameToDefault(driver);

		fc.commonMethods().switch_cboxIframe_frameId(driver);
		boolean isFrameOpened = fc.utobj().assertPageSource(driver, "Change Status");
		if (isFrameOpened == false) {
			fc.utobj().throwsException("Unable to Open Task");
		}
		fc.utobj().selectDropDown(driver, pobj.taskStatusDrp, "Completed");
		fc.utobj().clickElement(driver, pobj.processBtn);
		driver = fc.utobj().switchFrameToDefault(driver);
		fc.utobj().moveToElement(driver,
				fc.utobj().getElementByXpath(driver, ".//*[contains(text(),'Contact History')]"));

		// fc.utobj().switchFrame(driver, pobj.activityHistoryIFrame);
		// driver.switchTo().frame("leadLogCallSummary");
		fc.utobj().switchFrameById(driver, "leadLogCallSummary");

		WebElement e = fc.utobj().getElementByXpath(driver, ".//*[contains(text(),'" + taskSubject
				+ "')]/ancestor::tr[@class='bText12']/td[contains(text(),'Task (Completed)')]");
		fc.utobj().moveToElement(driver, e);
		boolean isTaskCompleted = e.isDisplayed();

		if (isTaskCompleted == false) {
			fc.utobj().throwsException("Not able to Process - Complete the Task! ");
		}
	}

	// Log a Call Through Link

	@Test(groups = { "sales123" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FS_LeadSummary_PrimaryInfo_004", testCaseDescription = "Verify Log a call funcationality from Primary Info")
	public void logACallLeadPrimaryInfoLnk() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String callSubject = fc.utobj().generateTestData(dataSet.get("callSubject"));

		try {
			driver = fc.loginpage().login(driver);

			fc.sales().sales_common().fsModule(driver);
			Sales fs = new Sales();
			fs.leadManagement(driver);
			FSLeadSummaryPageTest fsPage = new FSLeadSummaryPageTest();
			fsPage.clickAddLeadLink(driver);
			FSLeadSummaryPrimaryInfoPageTest pInfoPage = new FSLeadSummaryPrimaryInfoPageTest();
			Map<String, String> leadInfo = pInfoPage.fillLeadBasicInfo(driver);
			Reporter.log("Lead Added with lead name : " + leadInfo.get("firstName") + " " + leadInfo.get("lastName"));
			fc.utobj().printTestStep("Add a lead and Go to Primary Info");

			fc.utobj().printTestStep("Log A Call");
			FSLeadSummaryPrimaryInfoPage pobj = new FSLeadSummaryPrimaryInfoPage(driver);

			fc.utobj().clickElement(driver, pobj.logACallLnk);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.taskSubject, callSubject);

			String hhTime = fc.utobj().getSelectedDropDownValue(driver, pobj.hhTime);
			String mmTime = fc.utobj().getSelectedDropDownValue(driver, pobj.mmTime);
			mmTime = mmTime.replace("Min", "").trim();
			String ampmTime = fc.utobj().getSelectedDropDownValue(driver, pobj.ampmTime);

			fc.utobj().clickElement(driver, pobj.addCallBtn);

			fc.utobj().clickElement(driver, pobj.callConfirmScheduleTaskNoBtn);

			boolean isCallPresent = fc.utobj().assertLinkPartialText(driver, callSubject);

			if (isCallPresent == false) {
				fc.utobj().throwsException("Call is Not Added!");
			}

			String time = hhTime.concat(":").concat(mmTime).concat(" " + ampmTime);

			fc.utobj().clickPartialLinkText(driver, callSubject);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			boolean isTimePresent = fc.utobj().assertPageSource(driver, time);

			if (isTimePresent == false) {
				fc.utobj().throwsException("Call was added but timing mismatch");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales113" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_02_FS_LeadSummary_PrimaryInfo_004", testCaseDescription = "Verify Log a call funcationality from Primary Info - Activity History")
	public void logACallLeadActivityHistoryLnk() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String callSubject = fc.utobj().generateTestData(dataSet.get("callSubject"));

		try {
			driver = fc.loginpage().login(driver);

			fc.sales().sales_common().fsModule(driver);
			Sales fs = new Sales();
			fs.leadManagement(driver);
			FSLeadSummaryPageTest fsPage = new FSLeadSummaryPageTest();
			fsPage.clickAddLeadLink(driver);
			FSLeadSummaryPrimaryInfoPageTest pInfoPage = new FSLeadSummaryPrimaryInfoPageTest();
			Map<String, String> leadInfo = pInfoPage.fillLeadBasicInfo(driver);
			Reporter.log("Lead Added with lead name : " + leadInfo.get("firstName") + " " + leadInfo.get("lastName"));
			fc.utobj().printTestStep("Add a lead and Go to Primary Info");

			fc.utobj().printTestStep("Log A Call");
			FSLeadSummaryPrimaryInfoPage pobj = new FSLeadSummaryPrimaryInfoPage(driver);

			fc.utobj().switchFrameById(driver, "leadLogCallSummary");
			fc.utobj().clickElement(driver, pobj.logACallLnk);
			fc.utobj().switchFrameToDefault(driver);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.taskSubject, callSubject);

			String hhTime = fc.utobj().getSelectedDropDownValue(driver, pobj.hhTime);
			String mmTime = fc.utobj().getSelectedDropDownValue(driver, pobj.mmTime);
			mmTime = mmTime.replace("Min", "").trim();
			String ampmTime = fc.utobj().getSelectedDropDownValue(driver, pobj.ampmTime);

			fc.utobj().clickElement(driver, pobj.addCallBtn);

			fc.utobj().clickElement(driver, pobj.callConfirmScheduleTaskNoBtn);

			boolean isCallPresent = fc.utobj().assertLinkPartialText(driver, callSubject);

			if (isCallPresent == false) {
				fc.utobj().throwsException("Call is Not Added!");
			}

			String time = hhTime.concat(":").concat(mmTime).concat(" " + ampmTime);

			fc.utobj().clickPartialLinkText(driver, callSubject);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			boolean isTimePresent = fc.utobj().assertPageSource(driver, time);

			if (isTimePresent == false) {
				fc.utobj().throwsException("Call was added but timing mismatch");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	// Add Remarks Through Add Remarks Link

	@Test(groups = { "sales", "TC_FS_PrimaryInfo_AddLeadRemarks_001" }) // passed
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-06", testCaseId = "TC_FS_PrimaryInfo_AddLeadRemarks_001", testCaseDescription = "Verify Add Lead Remarks")
	public void addRemarksLeadPrimaryInfoLnk() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.sales().sales_common().fsModule(driver);
			Sales fs = new Sales();
			fs.leadManagement(driver);
			FSLeadSummaryPageTest fsPage = new FSLeadSummaryPageTest();
			fsPage.clickAddLeadLink(driver);
			fc.utobj().printTestStep("Add a lead and Go to Primary Info");

			FSLeadSummaryPrimaryInfoPageTest pInfoPage = new FSLeadSummaryPrimaryInfoPageTest();
			Map<String, String> leadInfo = pInfoPage.fillLeadBasicInfo(driver);
			Reporter.log("Lead Added with lead name : " + leadInfo.get("firstName") + " " + leadInfo.get("lastName"));

			fc.utobj().printTestStep("Add Remarks");

			fc.utobj().clickPartialLinkText(driver, "Add Remarks");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			String remarks = fc.utobj().generateTestData("Test Remarks");

			fc.utobj().sendKeys(driver, fc.utobj().getElementByID(driver, "remarks"), remarks);
			fc.utobj().printTestStep("Submit");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//button[@name='Submit']"));

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//input[@value='Close' and @onclick='javascript:closeWin()']"));
			gotoActivityHistorySection(driver);

			fc.utobj().printTestStep("Validate in Activity History if the remarks are added");
			boolean isAdded = verifySubjectCommentActivityHistory(driver, remarks);

			if (isAdded == false) {
				fc.utobj().throwsException("Remarks not added!");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}

	@Test(groups = { "sales"  , "sales_failedTC_FS_PrimaryInfo_AddLeadRemarks_002"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FS_PrimaryInfo_AddLeadRemarks_002", testCaseDescription = "Verify Add Lead Remarks of Activity History")
	public void addRemarksActivityHistoryLnk() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.sales().sales_common().fsModule(driver);
			Sales fs = new Sales();
			fs.leadManagement(driver);
			FSLeadSummaryPageTest fsPage = new FSLeadSummaryPageTest();
			fsPage.clickAddLeadLink(driver);
			fc.utobj().printTestStep("Add a lead and Go to Primary Info");

			FSLeadSummaryPrimaryInfoPageTest pInfoPage = new FSLeadSummaryPrimaryInfoPageTest();
			Map<String, String> leadInfo = pInfoPage.fillLeadBasicInfo(driver);
			Reporter.log("Lead Added with lead name : " + leadInfo.get("firstName") + " " + leadInfo.get("lastName"));

			fc.utobj().printTestStep("Activity History - Add Remarks");

			fc.utobj().switchFrameById(driver, "leadLogCallSummary");
			fc.utobj().clickPartialLinkText(driver, "Add Remarks");
			fc.utobj().switchFrameToDefault(driver);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			String remarks = fc.utobj().generateTestData("Test Remarks");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByID(driver, "remarks"), remarks);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//button[@name='Submit']"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//input[@value='Close' and @onclick='javascript:closeWin()']"));
			gotoActivityHistorySection(driver);
			boolean isAdded = verifySubjectCommentActivityHistory(driver, remarks);

			if (isAdded == false) {
				fc.utobj().throwsException("Remarks not added!");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FS_LeadSummary_PrimaryInfo_016", testCaseDescription = "Verify Killed lead functionality from more Action menu")
	public void killLeadMoreActions() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String killedReason = dataSet.get("killedReason");
		String leadStatus = dataSet.get("leadStatus");

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Add Lead Killed Reason");
			AdminFranchiseSalesLeadKilledReasonPageTest p2 = new AdminFranchiseSalesLeadKilledReasonPageTest();
			killedReason = p2.addLeadKilledReason_All(driver, killedReason, leadStatus);
			fc.utobj().printTestStep("Add Lead killed Reason");
			fc.sales().sales_common().fsModule(driver);
			Sales fs = new Sales();
			fs.leadManagement(driver);
			FSLeadSummaryPageTest fsPage = new FSLeadSummaryPageTest();
			fsPage.clickAddLeadLink(driver);
			FSLeadSummaryPrimaryInfoPageTest pInfoPage = new FSLeadSummaryPrimaryInfoPageTest();
			Map<String, String> leadInfo = pInfoPage.fillLeadBasicInfo(driver);
			Reporter.log("Lead Added with lead name : " + leadInfo.get("firstName") + " " + leadInfo.get("lastName"));

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FS_LeadSummary_PrimaryInfo_017", testCaseDescription = "Verify Delete Lead More Actions")
	public void deleteLeadMoreActions() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Sales > Lead Management Page");
			fc.sales().sales_common().fsModule(driver);
			Sales fs = new Sales();
			fs.leadManagement(driver);
			FSLeadSummaryPageTest fsPage = new FSLeadSummaryPageTest();
			fsPage.clickAddLeadLink(driver);
			FSLeadSummaryPrimaryInfoPageTest pInfoPage = new FSLeadSummaryPrimaryInfoPageTest();
			Map<String, String> leadInfo = pInfoPage.fillLeadBasicInfo(driver);
			Reporter.log("Lead Added with lead name : " + leadInfo.get("firstName") + " " + leadInfo.get("lastName"));
			fc.utobj().printTestStep("Add a lead and Go to Primary Info");
			fc.utobj().printTestStep("Delete");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/*
	 * Change Owner Through Button TC_FS_LeadSummary_PrimaryInfo_021
	 */

	@Test(groups = { "sales" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FS_LeadSummary_PrimaryInfo_021", testCaseDescription = "Verify Change Owner through Change Owner Button in Primary Info")
	public void changeOwnerThroughBtn() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			// AdminUsersManageCorporateUsersAddCorporateUserPageTest p2 = new
			// AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			// String leadOwner1 = p2.addCorporateUser(driver,userName);

			fc.utobj().printTestStep("Navigate To Sales > Lead Management Page");
			fc.sales().sales_common().fsModule(driver);
			Sales fs = new Sales();
			fs.leadManagement(driver);
			FSLeadSummaryPageTest fsPage = new FSLeadSummaryPageTest();
			fsPage.clickAddLeadLink(driver);
			FSLeadSummaryPrimaryInfoPageTest pInfoPage = new FSLeadSummaryPrimaryInfoPageTest();
			Map<String, String> leadInfo = pInfoPage.fillLeadBasicInfo(driver);
			Reporter.log("Lead Added with lead name : " + leadInfo.get("firstName") + " " + leadInfo.get("lastName"));
			fc.utobj().printTestStep("Add a lead and Go to Primary Info");
			fc.utobj().printTestStep("Change Owner through button");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales","sales_failed" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FS_LeadSummary_PrimaryInfo_028", testCaseDescription = "Change Lead Status through Change Status Button")
	private void changeStatusBtn() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String leadName = dataSet.get("leadName");
		String leadStatus = dataSet.get("leadStatus");

		try {

			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Add Lead Status");
			AdminFranchiseSalesLeadStatusPageTest p2 = new AdminFranchiseSalesLeadStatusPageTest();
			leadStatus = p2.addLeadStatus(driver, leadStatus);

			fc.sales().sales_common().fsModule(driver);
			Sales fs = new Sales();
			fs.leadManagement(driver);
			FSLeadSummaryPageTest fsPage = new FSLeadSummaryPageTest();
			fsPage.clickAddLeadLink(driver);
			FSLeadSummaryPrimaryInfoPageTest pInfoPage = new FSLeadSummaryPrimaryInfoPageTest();
			Map<String, String> leadInfo = pInfoPage.fillLeadBasicInfo(driver);
			Reporter.log("Lead Added with lead name : " + leadInfo.get("firstName") + " " + leadInfo.get("lastName"));
			fc.utobj().printTestStep("Add a lead and Go to Primary Info");
			fc.utobj().printTestStep("Change Status");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	// @Test(groups = { "abc"})
	// @TestCase(createdOn="2017-05-01",updatedOn="2018-06-14",testCaseId =
	// "TC_FS_PrimaryInfo_AddTask_001",testCaseDescription="Verify Log A Task
	// from primary info log a task link")
	private void sendFDDEmailMoreActions_All() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Reporter.log(
				"FSFDDFunctionality : Sales Module > Add Prior to FDD Emai > Add FDD Template > Add FDD > Add lead > Send Fdd.\n");
		Reporter.log("###################################################################");
		// String testCaseId = "FSFDDFunctionality";

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String emailSubject = dataSet.get("emailSubject");
		String timeInterval = dataSet.get("timeInterval");
		String emailContent = dataSet.get("emailContent");
		String mailTitle = dataSet.get("mailTitle");
		String mailSubject = dataSet.get("mailSubject");

		try {
			driver = fc.loginpage().login(driver);
			AdminFDDmanagementConfigureEmailsentPriortoFDDEmailPageTest p1 = new AdminFDDmanagementConfigureEmailsentPriortoFDDEmailPageTest();

			p1.configureEmailSentPriortoFDDEmail(driver, emailSubject, timeInterval, emailContent);

			AdminFDDManagementFDDEmailTemplateSummaryAddTemplatePageTest p2 = new AdminFDDManagementFDDEmailTemplateSummaryAddTemplatePageTest();
			mailTitle = p2.configureFDDEmailTemplateSummary(driver, mailTitle, mailSubject);
			// AdminFDDManagementFDDManagementPageTest p3 = new
			// AdminFDDManagementFDDManagementPageTest();
			// p3.uploadFDD_All(driver,fddName,item23Title,fileName);

			fc.sales().sales_common().fsModule(driver);
			Sales fs = new Sales();
			fs.leadManagement(driver);
			FSLeadSummaryPageTest fsPage = new FSLeadSummaryPageTest();
			fsPage.clickAddLeadLink(driver);
			FSLeadSummaryPrimaryInfoPageTest pInfoPage = new FSLeadSummaryPrimaryInfoPageTest();
			Map<String, String> leadInfo = pInfoPage.fillLeadBasicInfo(driver);
			Reporter.log("Lead Added with lead name : " + leadInfo.get("firstName") + " " + leadInfo.get("lastName"));

			FSLeadSummaryPrimaryInfoSendFDDPageTest p5 = new FSLeadSummaryPrimaryInfoSendFDDPageTest();
			p5.sendFDDWithTemplate(driver, mailTitle);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void moveToFIMBtn(WebDriver driver) throws Exception {

		FSLeadSummaryPrimaryInfoPage pobj = new FSLeadSummaryPrimaryInfoPage(driver);

		fc.utobj().clickElement(driver, pobj.moveToFIMBtn);
	}

	public void moveToFOBtn(WebDriver driver) throws Exception {

		FSLeadSummaryPrimaryInfoPage pobj = new FSLeadSummaryPrimaryInfoPage(driver);

		fc.utobj().clickElement(driver, pobj.moveToFOBtn);
	}

	public void moveToFIMMoreActions(WebDriver driver) throws Exception {

		FSLeadSummaryPrimaryInfoPage pobj = new FSLeadSummaryPrimaryInfoPage(driver);
		fc.utobj().selectMoreActionMenu(driver, pobj.moreActionsLink, pobj.menu, "Move To FIM");
	}

	public void moveToFOMoreActions(WebDriver driver) throws Exception {

		FSLeadSummaryPrimaryInfoPage pobj = new FSLeadSummaryPrimaryInfoPage(driver);
		fc.utobj().selectMoreActionMenu(driver, pobj.moreActionsLink, pobj.menu, "Move To Franchise Opener");

	}

	// Fully Verified on 20 sep 2016

	public void clickModifyLink(WebDriver driver) throws Exception {
		Reporter.log("Modifying Lead");

		FSLeadSummaryPrimaryInfoPage pobj = new FSLeadSummaryPrimaryInfoPage(driver);
		try {
			fc.utobj().clickElement(driver, pobj.modifyLnk);
		} catch (Exception e) {
			fc.utobj().throwsException("Modify Link Not Present!");
		}
	}

	public WebDriver gotoTasksSection(WebDriver driver) throws Exception {

		FSLeadSummaryPrimaryInfoPage fs = new FSLeadSummaryPrimaryInfoPage(driver);
		fc.utobj().moveToElement(driver, fc.utobj().getElement(driver, fs.contactHistorySection));

		fc.utobj().switchFrameById(driver, "leadOpenActivitesSummary");
		return driver;
	}

	public WebDriver gotoActivityHistorySection(WebDriver driver) throws Exception {

		Reporter.log("Go To Activity History Section");
		try {

			driver.switchTo().defaultContent();
		} catch (Exception E) {

		}

		FSLeadSummaryPrimaryInfoPage fs = new FSLeadSummaryPrimaryInfoPage(driver);
		fc.utobj().moveToElement(driver, fc.utobj().getElement(driver, fs.contactHistorySection));
		fc.utobj().switchFrameById(driver, "leadLogCallSummary");
		return driver;
	}

	public WebDriver gotoAcitityTimelineSection(WebDriver driver) throws Exception {

		FSLeadSummaryPrimaryInfoPage fs = new FSLeadSummaryPrimaryInfoPage(driver);
		fc.utobj().moveToElement(driver, fc.utobj().getElement(driver, fs.activityTimeLineSection));
		return driver;
	}

	public WebDriver gotoSMSHistorySection(WebDriver driver) throws Exception {

		FSLeadSummaryPrimaryInfoPage fs = new FSLeadSummaryPrimaryInfoPage(driver);
		fc.utobj().moveToElement(driver, fc.utobj().getElement(driver, fs.contactHistorySection));

		fc.utobj().switchFrameById(driver, "leadLogCallSummary");
		return driver;
	}

	public WebDriver gotoMessageHistorySection(WebDriver driver) throws Exception {

		FSLeadSummaryPrimaryInfoPage fs = new FSLeadSummaryPrimaryInfoPage(driver);
		fc.utobj().moveToElement(driver, fc.utobj().getElement(driver, fs.contactHistorySection));

		fc.utobj().switchFrameById(driver, "fsExternalMail");
		return driver;

	}

	public void verifyTaskInfo(WebDriver driver, String subject, List<String> taskInfo) throws Exception {
		FSLeadSummaryPrimaryInfoPage pobj = new FSLeadSummaryPrimaryInfoPage(driver);

		gotoTasksSection(driver);

		boolean isTaskAdded = fc.utobj().assertPageSource(driver, subject);

		if (isTaskAdded == false) {
			fc.utobj().throwsException("Task Not Visible in Task Section / Task not added");
		}

		fc.utobj().clickLink(driver, subject);

		fc.commonMethods().switch_cboxIframe_frameId(driver);

		boolean isTaskInfoPresent = fc.utobj().assertPageSourceWithMultipleRecords(driver, taskInfo);

		if (isTaskInfoPresent == false) {
			fc.utobj().throwsException("Task info not found in task details page.");
		}

		fc.utobj().clickElement(driver, pobj.closeBtnCbox);
	}

	public void completeTaskInfo(WebDriver driver, String subject, List<String> taskInfo) throws Exception {
		FSLeadSummaryPrimaryInfoPage pobj = new FSLeadSummaryPrimaryInfoPage(driver);

		gotoTasksSection(driver);

		boolean isTaskAdded = fc.utobj().assertPageSource(driver, subject);

		if (isTaskAdded == false) {
			fc.utobj().throwsException("Task Not Visible in Task Section / Task not added");
		}

		fc.utobj().clickLink(driver, subject);

		fc.commonMethods().switch_cboxIframe_frameId(driver);

		boolean isTaskInfoPresent = fc.utobj().assertPageSourceWithMultipleRecords(driver, taskInfo);

		if (isTaskInfoPresent == false) {
			fc.utobj().throwsException("Task info not found in task details page.");
		}

		fc.utobj().clickElement(driver, pobj.taskCompleteLnk);
		fc.utobj().clickElement(driver, pobj.processBtn);

		fc.utobj().clickElement(driver, pobj.closeBtnCbox);
	}

	// Method fully verified on 20 sept 2016
	public Map<String, String> fillLeadFullInfo(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("Filling Lead Info");

		String testCaseId = "TC_FullData_Lead_001";
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);

		String salutation = dataSet.get("Salutation");
		String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
		String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
		String address1 = dataSet.get("address1");
		String address2 = dataSet.get("address2");
		String city = dataSet.get("city");
		String country = dataSet.get("country");
		String state = dataSet.get("state");
		String zip = dataSet.get("zip");
		String county = dataSet.get("county");
		String preferredModeOfContact = dataSet.get("preferredModeOfContact");
		String bestTimeToContact = dataSet.get("bestTimeToContact");
		String workPhone = dataSet.get("workPhone");
		String workPhoneExtension = dataSet.get("workPhoneExtension");
		String homePhone = dataSet.get("homePhone");
		String homePhoneExtension = dataSet.get("homePhoneExtension");
		String fax = dataSet.get("fax");
		String mobile = dataSet.get("mobile");
		String email = dataSet.get("email");
		String companyName = dataSet.get("companyName");
		String comments = dataSet.get("comments");
		String leadOwner = dataSet.get("leadOwner");
		String leadRating = dataSet.get("leadRating");
		String leadSourceCategory = dataSet.get("leadSourceCategory");
		String leadSourceDetails = dataSet.get("leadSourceDetails");
		String otherLeadSources = dataSet.get("otherLeadSources");
		String currentNetWorth = dataSet.get("currentNetWorth");
		String cashAvailableForInvestment = dataSet.get("cashAvailableForInvestment");
		String investmentTimeframe = dataSet.get("investmentTimeframe");
		String Background = dataSet.get("Background");
		String SourceOfInvestment = dataSet.get("SourceOfInvestment");
		String noOfLocation = dataSet.get("noOfLocation");
		String preferredCity1 = dataSet.get("preferredCity1");
		String preferredCountry1 = dataSet.get("preferredCountry1");
		String preferredState1 = dataSet.get("preferredState1");
		String preferredCity2 = dataSet.get("preferredCity2");
		String preferredCountry2 = dataSet.get("preferredCountry2");
		String preferredState2 = dataSet.get("preferredState2");
		String probability = dataSet.get("probability");
		String rating = dataSet.get("rating");
		String forecastRevenue = dataSet.get("forecastRevenue");
		String campaignName = dataSet.get("campaignName");

		List<String> listItems = new ArrayList<String>();
		listItems.add(firstName);
		listItems.add(lastName);
		listItems.add(country);
		listItems.add(state);
		listItems.add(zip);
		listItems.add(leadOwner);
		listItems.add(leadRating);
		listItems.add(leadSourceCategory);
		listItems.add(leadSourceDetails);
		listItems.add(companyName);
		listItems.add(email);
		listItems.add(county);
		listItems.add(city);
		listItems.add(Background);
		listItems.add(SourceOfInvestment);
		listItems.add(rating);
		listItems.add(probability);
		listItems.add(forecastRevenue);

		FSLeadSummaryAddLeadPage pobj = new FSLeadSummaryAddLeadPage(driver);
		try {
			fc.utobj().selectDropDownByVisibleText(driver, pobj.salutation, salutation);
			fc.utobj().sendKeys(driver, pobj.firstName, firstName);
			fc.utobj().sendKeys(driver, pobj.lastName, lastName);
			fc.utobj().sendKeys(driver, pobj.address, address1);
			fc.utobj().sendKeys(driver, pobj.address2, address2);
			fc.utobj().sendKeys(driver, pobj.city, city);
			fc.utobj().selectDropDownByVisibleText(driver, pobj.country, country);

			fc.utobj().selectDropDownByVisibleText(driver, pobj.state, state);

			fc.utobj().selectDropDownByVisibleText(driver, pobj.county, county);
			fc.utobj().sendKeys(driver, pobj.zip, zip);
			fc.utobj().selectDropDownByVisibleText(driver, pobj.preferredModeofContact, preferredModeOfContact);
			fc.utobj().sendKeys(driver, pobj.bestTimeToContact, bestTimeToContact);
			fc.utobj().sendKeys(driver, pobj.phone, workPhone);
			fc.utobj().sendKeys(driver, pobj.phoneExt, workPhoneExtension);
			fc.utobj().sendKeys(driver, pobj.homePhone, homePhone);
			fc.utobj().sendKeys(driver, pobj.homePhoneExt, homePhoneExtension);
			fc.utobj().sendKeys(driver, pobj.fax, fax);
			fc.utobj().sendKeys(driver, pobj.mobile, mobile);
			fc.utobj().sendKeys(driver, pobj.emailID, email);
			fc.utobj().sendKeys(driver, pobj.companyName, companyName);
			fc.utobj().sendKeys(driver, pobj.comments, comments);
			fc.utobj().selectDropDownByVisibleText(driver, pobj.leadOwnerID, leadOwner);
			fc.utobj().selectDropDownByVisibleText(driver, pobj.leadRatingID, leadRating);
			// fc.utobj().sendKeys(driver, pobj.marketingCode, );
			fc.utobj().selectDropDownByVisibleText(driver, pobj.leadSource2ID, leadSourceCategory);

			fc.utobj().selectDropDownByVisibleText(driver, pobj.leadSource3ID, leadSourceDetails);

			fc.utobj().sendKeys(driver, pobj.otherLeadSourceDetail, otherLeadSources);
			fc.utobj().sendKeys(driver, pobj.currentNetWorth, currentNetWorth);
			fc.utobj().sendKeys(driver, pobj.cashAvailableForInvestment, cashAvailableForInvestment);
			fc.utobj().sendKeys(driver, pobj.investTimeframe, investmentTimeframe);
			fc.utobj().sendKeys(driver, pobj.background, Background);
			fc.utobj().sendKeys(driver, pobj.sourceOfFunding, SourceOfInvestment);
			fc.utobj().sendKeys(driver, pobj.nextCallDate, fc.utobj().getCurrentDateUSFormat());
			fc.utobj().sendKeys(driver, pobj.noOfUnitReq, noOfLocation);
			fc.utobj().clickElement(driver, pobj.locationImage);

			fc.utobj().sendKeys(driver, pobj.temppreferredCity1, preferredCity1);
			fc.utobj().selectDropDownByVisibleText(driver, pobj.temppreferredCountry1, preferredCountry1);

			fc.utobj().selectDropDownByVisibleText(driver, pobj.temppreferredStateId1, preferredState1);
			fc.utobj().sendKeys(driver, pobj.temppreferredCity2, preferredCity2);
			fc.utobj().selectDropDownByVisibleText(driver, pobj.temppreferredCountry2, preferredCountry2);

			fc.utobj().selectDropDownByVisibleText(driver, pobj.temppreferredStateId2, preferredState2);
			fc.utobj().sendKeys(driver, pobj.forecastClosureDate, fc.utobj().getCurrentDateUSFormat());
			fc.utobj().sendKeys(driver, pobj.probability, probability);
			fc.utobj().selectDropDownByVisibleText(driver, pobj.forecastRating, rating);
			fc.utobj().sendKeys(driver, pobj.forecastRevenue, forecastRevenue);
			fc.utobj().selectDropDownByVisibleText(driver, pobj.campaignID, campaignName);
			fc.utobj().clickElement(driver, pobj.save);
		} catch (Exception e) {
			fc.utobj().throwsException(e.toString());
		}

		boolean isLeadInfoFound = fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);

		if (isLeadInfoFound == false) {
			fc.utobj().throwsException("Either Lead Not Added or lead info missing in lead!");
		}

		// fc.utobj().switchFrameById(driver, "leadLogCallSummary");

		driver.switchTo().frame("leadLogCallSummary");
		boolean isRemarkAdded = fc.utobj().assertPageSource(driver, "Lead added through FranConnect Application");

		if (isRemarkAdded == false) {
			fc.utobj().throwsException("Lead Added but remark is missing from lead info");
		}

		driver.switchTo().defaultContent();

		FSLeadSummaryPrimaryInfoPageTest flpipt = new FSLeadSummaryPrimaryInfoPageTest();
		flpipt.clickModifyLink(driver);

		List<String> listItems1 = new ArrayList<String>();
		listItems1.add(workPhone);
		listItems1.add(workPhoneExtension);
		listItems1.add(homePhone);
		listItems1.add(homePhoneExtension);
		listItems1.add(cashAvailableForInvestment);
		listItems1.add(firstName);
		listItems1.add(lastName);
		listItems1.add(zip);
		listItems1.add(companyName);
		listItems1.add(email);
		listItems1.add(city);
		listItems1.add(Background);
		listItems1.add(SourceOfInvestment);

		boolean isTextPresent = fc.utobj().assertMultipleInputBoxValue(driver, listItems1);

		if (isTextPresent == false) {
			fc.utobj().throwsException("One or all the values not found on Modify Window.");
		}

		Map<String, String> leadInfo = new HashMap<String, String>();

		leadInfo.put("firstName", firstName);
		leadInfo.put("lastName", lastName);

		return leadInfo;
	}

	// Method fully verified on 20 sept 2016
	public Map<String, String> fillLeadBasicInfo(WebDriver driver) throws Exception {
		Reporter.log("Filling Lead Info");

		String testCaseId = "TC_BasicData_Lead_001";
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);

		String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
		String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
		String country = dataSet.get("country");
		String state = dataSet.get("state");
		String email = dataSet.get("email");
		String leadOwner = dataSet.get("leadOwner");
		String leadSourceCategory = dataSet.get("leadSourceCategory");
		String leadSourceDetails = dataSet.get("leadSourceDetails");
		String campaignName = dataSet.get("campaignName");

		FSLeadSummaryAddLeadPage pobj = new FSLeadSummaryAddLeadPage(driver);
		try {
			fc.utobj().sendKeys(driver, pobj.firstName, firstName);
			fc.utobj().sendKeys(driver, pobj.lastName, lastName);
			fc.utobj().selectDropDownByVisibleText(driver, pobj.country, country);

			fc.utobj().selectDropDownByVisibleText(driver, pobj.state, state);
			fc.utobj().sendKeys(driver, pobj.city, "Testcity");
			fc.utobj().sendKeys(driver, pobj.phone, "1245789632");
			fc.utobj().sendKeys(driver, pobj.emailID, email);
			fc.utobj().selectDropDownByVisibleText(driver, pobj.leadOwnerID, leadOwner);
			fc.utobj().selectDropDownByVisibleText(driver, pobj.leadSource2ID, leadSourceCategory);

			fc.utobj().selectDropDownByVisibleText(driver, pobj.leadSource3ID, leadSourceDetails);
			fc.utobj().selectDropDownByVisibleText(driver, pobj.campaignID, campaignName);
			fc.utobj().clickElement(driver, pobj.save);
			// unexpected alert open: {Alert text : Lead's email is already subscribed for below subscription types
			if(fc.utobj().isAlertPresent(driver))
			{
				fc.utobj().acceptAlertBox(driver);
			}
		} catch (Exception e) {
			fc.utobj().throwsException("Mandatory fields are not available at Lead's Primary Info Page");
		}

		List<String> listItems = new ArrayList<String>();
		listItems.add(firstName);
		listItems.add(lastName);
		listItems.add(country);
		listItems.add(state);
		listItems.add(leadOwner);
		listItems.add(leadSourceCategory);
		listItems.add(leadSourceDetails);
		listItems.add(email);

		boolean isLeadInfoFound = fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);
		if (isLeadInfoFound == false) {
			fc.utobj().printBugStatus("Either Lead Not Added or lead info missing in lead!");
		}

		Map<String, String> leadInfo = new HashMap<String, String>();
		leadInfo.put("firstName", firstName);
		leadInfo.put("lastName", lastName);

		return leadInfo;
	}

	public Map<String, String> fillLeadBasicInfo_LeadName(WebDriver driver, Map<String, String> config,
			String firstName, String lastName) throws Exception {
		Reporter.log("Filling Lead Info");

		String testCaseId = "TC_BasicData_Lead_ExcludeLeadName_001";
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);

		String country = dataSet.get("country");
		String state = dataSet.get("state");
		String email = dataSet.get("email");
		String leadOwner = dataSet.get("leadOwner");
		String leadSourceCategory = dataSet.get("leadSourceCategory");
		String leadSourceDetails = dataSet.get("leadSourceDetails");
		String campaignName = dataSet.get("campaignName");

		FSLeadSummaryAddLeadPage pobj = new FSLeadSummaryAddLeadPage(driver);
		try {
			fc.utobj().sendKeys(driver, pobj.firstName, firstName);
			fc.utobj().sendKeys(driver, pobj.lastName, lastName);
			fc.utobj().selectDropDownByVisibleText(driver, pobj.country, country);

			fc.utobj().selectDropDownByVisibleText(driver, pobj.state, state);
			fc.utobj().sendKeys(driver, pobj.emailID, email);
			fc.utobj().selectDropDownByVisibleText(driver, pobj.leadOwnerID, leadOwner);
			fc.utobj().selectDropDownByVisibleText(driver, pobj.leadSource2ID, leadSourceCategory);

			fc.utobj().selectDropDownByVisibleText(driver, pobj.leadSource3ID, leadSourceDetails);

			try {

				fc.utobj().selectDropDownByVisibleText(driver, pobj.campaignID, campaignName);
			} catch (Exception e) {
				fc.utobj().printBugStatus("Campaign drop down not available");
			}

			fc.utobj().clickElement(driver, pobj.save);
		} catch (Exception e) {
			fc.utobj().throwsException("Mandatory fields are not available at Lead's Primary Info Page");
		}

		List<String> listItems = new ArrayList<String>();
		listItems.add(firstName);
		listItems.add(lastName);
		listItems.add(country);
		listItems.add(state);
		listItems.add(leadOwner);
		listItems.add(leadSourceCategory);
		listItems.add(leadSourceDetails);
		listItems.add(email);

		boolean isLeadInfoFound = fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);
		if (isLeadInfoFound == false) {
			fc.utobj().printBugStatus("Either Lead Not Added or lead info missing in lead!");
		}

		Map<String, String> leadInfo = new HashMap<String, String>();
		leadInfo.put("firstName", firstName);
		leadInfo.put("lastName", lastName);

		return leadInfo;
	}

	public void gotoPrimaryInfoTab(WebDriver driver) throws Exception {
		FSLeadSummaryPrimaryInfoPage pobj = new FSLeadSummaryPrimaryInfoPage(driver);

		fc.utobj().clickElement(driver, pobj.primaryInfoTab);
	}

	public void gotoQualificationDetailsTab(WebDriver driver) throws Exception {
		FSLeadSummaryPrimaryInfoPage pobj = new FSLeadSummaryPrimaryInfoPage(driver);

		fc.utobj().clickElement(driver, pobj.qualificationDetailsTab);
	}

	public void gotoPersonalProfileTab(WebDriver driver) throws Exception {
		FSLeadSummaryPrimaryInfoPage pobj = new FSLeadSummaryPrimaryInfoPage(driver);

		fc.utobj().clickElement(driver, pobj.personalProfileTab);
	}

	public boolean verifySubjectOpenTasks(WebDriver driver, Map<String, String> taskInfo) throws Exception {

		String assignTo = taskInfo.get("assignTo");
		String status = taskInfo.get("status");
		String subject = taskInfo.get("subject");
		String taskType = taskInfo.get("taskType");
		String comments = taskInfo.get("comments");

		List<String> taskDetails = new ArrayList<String>();
		taskDetails.add(assignTo);
		taskDetails.add(status);
		taskDetails.add(subject);
		taskDetails.add(taskType);
		taskDetails.add(comments);

		Reporter.log("Verifying Subject of Task : " + taskInfo.get("subject"));
		driver = gotoTasksSection(driver);

		boolean isRemarksAdded = fc.utobj().assertPageSource(driver, taskInfo.get("subject"));
		if (isRemarksAdded == false) {
			fc.utobj().printBugStatus("Activity History Not Added : " + taskInfo.get("subject"));
		}
		fc.utobj().clickLink(driver, taskInfo.get("subject"));
		driver.switchTo().defaultContent();

		boolean isTrue = fc.utobj().assertPageSourceWithMultipleRecords(driver, taskDetails);

		if (isTrue == false) {
			fc.utobj().throwsException("Task details mismatching in the Task Details Color Box");
		}

		return isRemarksAdded;
	}

	public boolean verifySubjectCommentActivityHistory(WebDriver driver, String subjectComments) throws Exception {
		Reporter.log("Verifying Comment / Activity History for status : " + subjectComments);
		driver = gotoActivityHistorySection(driver);

		boolean isRemarksAdded = fc.utobj().assertPageSource(driver, subjectComments);
		if (isRemarksAdded == false) {
			fc.utobj().printBugStatus("Activity History Not Added : " + subjectComments);
		}
		driver.switchTo().defaultContent();
		return isRemarksAdded;
	}

	public boolean verifyActivityTimeline(WebDriver driver, String activity) throws Exception {

		Reporter.log("Verifying Activity Timeline for status : " + activity);
		driver = gotoAcitityTimelineSection(driver);

		List<WebElement> activities = driver
				.findElements(By.xpath(".//*[@id='activityTline']//span[contains(text(),'" + activity + "')]"));
		boolean isActivityPresent = false;

		for (int x = 0; x < activities.size(); x++) {
			if (activity.equalsIgnoreCase(activities.get(x).getText().trim())) {
				isActivityPresent = true;
			}
		}

		if (isActivityPresent == false) {
			fc.utobj().printBugStatus("Activity History Not Added in Activity Timeline : " + activity);
		}
		return isActivityPresent;
	}

	public void clickNext(WebDriver driver) throws Exception {
		FSLeadSummaryPrimaryInfoPage pobj = new FSLeadSummaryPrimaryInfoPage(driver);

		fc.utobj().clickElement(driver, pobj.nextLnk);
	}

	public WebDriver clickDetailedHistoryFromInsideFrame(WebDriver driver) throws Exception {

		FSLeadSummaryPrimaryInfoPage pobj = new FSLeadSummaryPrimaryInfoPage(driver);
		gotoActivityHistorySection(driver);
		fc.utobj().clickElementByJS(driver, pobj.detailedHistory);
		driver.switchTo().defaultContent();
		fc.commonMethods().switch_cboxIframe_frameId(driver);
		return driver;
	}

	public WebDriver clickStatusChangeHistory_DetailedHistory(WebDriver driver) throws Exception {
		driver = clickDetailedHistoryFromInsideFrame(driver);

		fc.utobj().clickLink(driver, "Status Change History");
		return driver;
	}

	public WebDriver clickOwnerChangeHistory_DetailedHistory(WebDriver driver) throws Exception {
		driver = clickDetailedHistoryFromInsideFrame(driver);

		fc.utobj().clickLink(driver, "Owner Change History");
		return driver;
	}

	public String getOwnerName(WebDriver driver) throws Exception {

		FSLeadSummaryPrimaryInfoPage pobj = new FSLeadSummaryPrimaryInfoPage(driver);
		String ownerName = fc.utobj().getElement(driver, pobj.leadOwnerName).getText();
		return ownerName;
	}

	/* Harish Dwivedi */

	public Map<String, String> fillLeadBasicInfo_LeadNameBasic(WebDriver driver, String firstName, String lastName)
			throws Exception {
		Reporter.log("Filling Lead Info");

		String testCaseId = "TC_BasicData_Lead_ExcludeLeadNameFormGenerator_001";
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);

		String country = dataSet.get("country");
		String state = dataSet.get("state");
		String email = dataSet.get("email");
		String leadOwner = dataSet.get("leadOwner");
		String leadSourceCategory = dataSet.get("leadSourceCategory");
		String leadSourceDetails = dataSet.get("leadSourceDetails");
		String campaignName = dataSet.get("campaignName");

		FSLeadSummaryAddLeadPage pobj = new FSLeadSummaryAddLeadPage(driver);
		try {
			fc.utobj().sendKeys(driver, pobj.firstName, firstName);
			fc.utobj().sendKeys(driver, pobj.lastName, lastName);
			fc.utobj().selectDropDownByVisibleText(driver, pobj.country, country);

			fc.utobj().selectDropDownByVisibleText(driver, pobj.state, state);
			fc.utobj().sendKeys(driver, pobj.emailID, email);
			/*
			 * fc.utobj().sendKeys(driver, pobj.zip, "123456");
			 * fc.utobj().sendKeys(driver, pobj.phone, "12345678");
			 */
			fc.utobj().selectDropDownByVisibleText(driver, pobj.leadOwnerID, leadOwner);
			fc.utobj().selectDropDownByVisibleText(driver, pobj.leadSource2ID, leadSourceCategory);

			fc.utobj().selectDropDownByVisibleText(driver, pobj.leadSource3ID, leadSourceDetails);

			try {

				fc.utobj().selectDropDownByVisibleText(driver, pobj.campaignID, campaignName);
			} catch (Exception e) {
				fc.utobj().printBugStatus("Campaign drop down not available");
			}

			fc.utobj().clickElement(driver, pobj.save);
		} catch (Exception e) {
			fc.utobj().throwsException("Mandatory fields are not available at Lead's Primary Info Page");
		}

		List<String> listItems = new ArrayList<String>();
		listItems.add(firstName);
		listItems.add(lastName);
		listItems.add(country);
		listItems.add(state);
		listItems.add(leadOwner);
		listItems.add(leadSourceCategory);
		listItems.add(leadSourceDetails);
		listItems.add(email);

		boolean isLeadInfoFound = fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);
		if (isLeadInfoFound == false) {
			fc.utobj().printBugStatus("Either Lead Not Added or lead info missing in lead!");
		}

		Map<String, String> leadInfo = new HashMap<String, String>();
		leadInfo.put("firstName", firstName);
		leadInfo.put("lastName", lastName);

		return leadInfo;
	}

	/* Anuakran Mishra */

	public Map<String, String> fillLeadBasicInfo_OwnerNameBasic(WebDriver driver, String firstName, String lastName,
			String leadOwner) throws Exception {
		Reporter.log("Filling Lead Info");

		String testCaseId = "TC_BasicData_Lead_ExcludeLeadNameFormGenerator_001";
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);

		String country = dataSet.get("country");
		String state = dataSet.get("state");
		String leadSourceCategory = dataSet.get("leadSourceCategory");
		String leadSourceDetails = dataSet.get("leadSourceDetails");
		String i18n = FranconnectUtil.config.get("i18Files");
		
		if (i18n != null && !i18n.isEmpty()) {
			country = fc.utobj().translateString(country);
			state = fc.utobj().translateString(state);
			leadSourceCategory = fc.utobj().translateString(leadSourceCategory);
			leadSourceDetails = fc.utobj().translateString(leadSourceDetails);
		}
		
		String email = "salesautomation@staffex.com";
		// String leadOwner = dataSet.get("leadOwner");
		String campaignName = dataSet.get("campaignName");

		FSLeadSummaryAddLeadPage pobj = new FSLeadSummaryAddLeadPage(driver);
		try {
			fc.utobj().sendKeys(driver, pobj.firstName, firstName);
			fc.utobj().sendKeys(driver, pobj.lastName, lastName);
			fc.utobj().selectDropDownByVisibleText(driver, pobj.country, country);

			fc.utobj().selectDropDownByVisibleText(driver, pobj.state, state);
			fc.utobj().sendKeys(driver, pobj.city, "TestCity");
			fc.utobj().sendKeys(driver, pobj.phone, "9214578963");
			fc.utobj().sendKeys(driver, pobj.emailID, email);
			fc.utobj().sendKeys(driver, pobj.zip, "123456");
			fc.utobj().sendKeys(driver, pobj.phone, "12345678");
			fc.utobj().sendKeys(driver, pobj.address, "New Highway Road");
			fc.utobj().selectDropDownByVisibleText(driver, pobj.leadOwnerID, leadOwner);
			fc.utobj().selectDropDownByVisibleText(driver, pobj.leadSource2ID, leadSourceCategory);

			try {
				fc.utobj().selectDropDownByVisibleText(driver, pobj.leadSource3ID, leadSourceDetails);
			} catch (Exception e) {
				fc.utobj().selectDropDownByVisibleText(driver, pobj.leadSource3ID, "Business Alliance");
			}

			try {

				fc.utobj().selectDropDownByVisibleText(driver, pobj.campaignID, campaignName);
			} catch (Exception e) {
				fc.utobj().printBugStatus("Campaign drop down not available");
			}

			fc.utobj().clickElement(driver, pobj.save);
		} catch (Exception e) {
			fc.utobj().throwsException("Mandatory fields are not available at Lead's Primary Info Page");
		}

		List<String> listItems = new ArrayList<String>();
		listItems.add(firstName);
		listItems.add(lastName);
		listItems.add(country);
		listItems.add(state);
		listItems.add(leadOwner);
		listItems.add(leadSourceCategory);
		listItems.add(leadSourceDetails);
		listItems.add(email);

		boolean isLeadInfoFound = fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);
		if (isLeadInfoFound == false) {
			fc.utobj().printBugStatus("Either Lead Not Added or lead info missing in lead!");
		}

		Map<String, String> leadInfo = new HashMap<String, String>();
		leadInfo.put("firstName", firstName);
		leadInfo.put("lastName", lastName);

		return leadInfo;
	}

}