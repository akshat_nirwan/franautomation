package com.builds.test.fs;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.fs.FSLeadSummaryPrimaryInfoSendFDDPage;
import com.builds.utilities.FranconnectUtil;

public class FSLeadSummaryPrimaryInfoSendFDDPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	public void sendFDD(WebDriver driver) throws Exception {

		FSLeadSummaryPrimaryInfoSendFDDPage pobj = new FSLeadSummaryPrimaryInfoSendFDDPage(driver);
		fc.utobj().clickElement(driver, pobj.sendBtn);
	}

	public void sendFDDWithTemplate(WebDriver driver, String templateName) throws Exception {

		FSLeadSummaryPrimaryInfoSendFDDPage pobj = new FSLeadSummaryPrimaryInfoSendFDDPage(driver);
		fc.utobj().selectDropDown(driver, pobj.fddEmailTemplatesDrp, templateName);
		fc.utobj().clickElement(driver, pobj.sendBtn);
	}

}
