package com.builds.test.fs;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.Reporter;

import com.builds.uimaps.fs.FSLeadSummaryQualificationDetailsPage;
import com.builds.utilities.FranconnectUtil;

public class FSLeadSummaryQualificationDetailsPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	public void fillFullQualificationDetailsTab(WebDriver driver, Map<String, String> config) throws Exception {

		List<String> listInfo = new ArrayList<String>();
		listInfo.addAll(fillQualificationDetails_PersonalInformation(driver, config));
		listInfo.addAll(fillQualificationDetails_AssetsLiabilities(driver, config));
		listInfo.addAll(fillQualificationDetails_RealEstateOwned(driver, config));
		fillQualificationDetails_AnnualSourcesOfIncome(driver, config);
		fillQualificationDetails_TotalContingentLiabilities(driver, config);
		listInfo.addAll(fillQualificationDetails_SpecificData(driver, config));
		listInfo.addAll(fillQualificationDetails_InternalAnalysisofApplicant(driver, config));
		saveTab(driver);
		modifyQualificationDetails(driver);
		validateModifyPage(driver, listInfo);
		FSLeadSummaryPrimaryInfoPageTest fs = new FSLeadSummaryPrimaryInfoPageTest();
		fs.gotoPrimaryInfoTab(driver);
		fs.verifySubjectCommentActivityHistory(driver, "Lead Qualification Details Added");
		fs.verifyActivityTimeline(driver, "Lead Qualification Details Added");
		fs.gotoQualificationDetailsTab(driver);
		deleteQualificationDetails(driver);
		fs.gotoPrimaryInfoTab(driver);
		fs.verifySubjectCommentActivityHistory(driver, "Lead Qualification Details Deleted");
		fs.verifyActivityTimeline(driver, "Lead Qualification Details Deleted");
	}

	public void saveTab(WebDriver driver) throws Exception {
		FSLeadSummaryQualificationDetailsPage pobj = new FSLeadSummaryQualificationDetailsPage(driver);

		fc.utobj().clickElement(driver, pobj.saveBtn);
	}

	public List<String> fillQualificationDetails_PersonalInformation(WebDriver driver, Map<String, String> config)
			throws Exception {

		Reporter.log("Filling Lead Qualification Details > Personal Information");

		String testCaseId = "TC_LeadQualificationDetails_PersonalInformation_001";
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);

		FSLeadSummaryQualificationDetailsPage pobj = new FSLeadSummaryQualificationDetailsPage(driver);

		fc.utobj().sendKeys(driver, pobj.date, dataSet.get("Date"));
		String name = fc.utobj().generateTestData(dataSet.get("Name"));
		fc.utobj().sendKeys(driver, pobj.firstName, name);

		if (dataSet.get("Gender").equalsIgnoreCase("Male")) {
			fc.utobj().clickElement(driver, pobj.gender_male);
		} else if (dataSet.get("Gender").equalsIgnoreCase("Female")) {
			fc.utobj().clickElement(driver, pobj.gender_female);
		}
		fc.utobj().sendKeys(driver, pobj.presentAddress, dataSet.get("PresentAddress"));
		fc.utobj().sendKeys(driver, pobj.howLong, dataSet.get("Howmanyyearsatthisaddress"));
		fc.utobj().sendKeys(driver, pobj.city, dataSet.get("City"));
		fc.utobj().selectDropDownByVisibleText(driver, pobj.country, dataSet.get("Country"));
		fc.utobj().selectDropDownByVisibleText(driver, pobj.stateID, dataSet.get("StateProvince"));

		fc.utobj().sendKeys(driver, pobj.zipCode, dataSet.get("ZipPostalCode"));
		fc.utobj().sendKeys(driver, pobj.workPhone, dataSet.get("WorkPhone"));
		fc.utobj().sendKeys(driver, pobj.phoneExt, dataSet.get("WorkPhoneExtension"));
		fc.utobj().sendKeys(driver, pobj.homePhone, dataSet.get("HomePhone"));
		fc.utobj().sendKeys(driver, pobj.homePhoneExt, dataSet.get("HomePhoneExtension"));
		fc.utobj().sendKeys(driver, pobj.emailID, dataSet.get("Email"));

		if (dataSet.get("USCitizen").equalsIgnoreCase("Yes")) {
			fc.utobj().clickElement(driver, pobj.usCitizen_yes);
		} else if (dataSet.get("USCitizen").equalsIgnoreCase("No")) {
			fc.utobj().clickElement(driver, pobj.usCitizen_No);
		}

		fc.utobj().sendKeys(driver, pobj.ssn, dataSet.get("SocialSecurity"));
		fc.utobj().sendKeys(driver, pobj.previousAddress, dataSet.get("PreviousAddress"));
		fc.utobj().sendKeys(driver, pobj.previousCity, dataSet.get("PreviousCity"));
		fc.utobj().selectDropDownByVisibleText(driver, pobj.previousCountry, dataSet.get("PreviousCountry"));
		fc.utobj().selectDropDownByVisibleText(driver, pobj.previousStateID, dataSet.get("PreviousStateProvince"));
		fc.utobj().sendKeys(driver, pobj.previousZipCode, dataSet.get("PreviousZipPostalCode"));
		fc.utobj().selectDropDownByVisibleText(driver, pobj.birthMonth, dataSet.get("BirthMonth"));
		fc.utobj().selectDropDownByVisibleText(driver, pobj.birthDate, dataSet.get("BirthDate"));
		fc.utobj().selectDropDownByVisibleText(driver, pobj.bestTimeToCall, dataSet.get("BestTimeToCall"));

		if (dataSet.get("HomeOwnership").equalsIgnoreCase("Own")) {
			fc.utobj().clickElement(driver, pobj.homeOwnership_own);
		} else if (dataSet.get("HomeOwnership").equalsIgnoreCase("Renting")) {
			fc.utobj().clickElement(driver, pobj.homeOwnership_renting);
		}

		if (dataSet.get("MaritalStatus").equalsIgnoreCase("Married")) {
			fc.utobj().clickElement(driver, pobj.maritalStatus_married);
		} else if (dataSet.get("MaritalStatus").equalsIgnoreCase("Single")) {
			fc.utobj().clickElement(driver, pobj.maritalStatus_single);
		}

		fc.utobj().sendKeys(driver, pobj.spouseName, dataSet.get("SpouseName"));
		fc.utobj().sendKeys(driver, pobj.spouseSsn, dataSet.get("SpouseSocialSecurity"));

		if (dataSet.get("SpouseUSCitizen").equalsIgnoreCase("Yes")) {
			fc.utobj().clickElement(driver, pobj.spouseUsCitizen_yes);
		} else if (dataSet.get("SpouseUSCitizen").equalsIgnoreCase("No")) {
			fc.utobj().clickElement(driver, pobj.spouseUsCitizen_no);
		}
		fc.utobj().selectDropDownByVisibleText(driver, pobj.spouseBirthMonth, dataSet.get("SpouseBirthMonth"));
		fc.utobj().selectDropDownByVisibleText(driver, pobj.spouseBirthDate, dataSet.get("SpouseBirthDate"));

		List<String> listInfo = new ArrayList<String>();
		listInfo.add(dataSet.get("Date"));
		listInfo.add(dataSet.get("City"));
		listInfo.add(name);
		return listInfo;
	}

	public List<String> fillQualificationDetails_AssetsLiabilities(WebDriver driver, Map<String, String> config)
			throws Exception {

		Reporter.log("Filling Lead Qualification Details > Assets & Liabilities");

		String testCaseId = "TC_LeadQualificationDetails_AssetsLiabilities_002";
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);

		FSLeadSummaryQualificationDetailsPage pobj = new FSLeadSummaryQualificationDetailsPage(driver);

		// Assets
		fc.utobj().sendKeys(driver, pobj.cashOnHand, dataSet.get("CashonHandinBanks"));
		fc.utobj().sendKeys(driver, pobj.mortgages, dataSet.get("Mortgages"));
		fc.utobj().sendKeys(driver, pobj.marketableSecurities, dataSet.get("MarketableSecurities"));
		fc.utobj().sendKeys(driver, pobj.accountsPayable, dataSet.get("AccountsPayable"));
		fc.utobj().sendKeys(driver, pobj.accountsReceivable, dataSet.get("AccountsNotesReceivable"));
		fc.utobj().sendKeys(driver, pobj.notesPayable, dataSet.get("NotesPayable"));
		fc.utobj().sendKeys(driver, pobj.retirementPlans, dataSet.get("RetirementPlans"));
		fc.utobj().sendKeys(driver, pobj.loansOnLifeInsurance, dataSet.get("LoansOnLifeInsurance"));
		fc.utobj().sendKeys(driver, pobj.realEstate, dataSet.get("RealEstate"));
		fc.utobj().sendKeys(driver, pobj.creditCardBalance, dataSet.get("CreditCardstotalbalance"));
		fc.utobj().sendKeys(driver, pobj.personalProperty, dataSet.get("PersonalProperty"));
		fc.utobj().sendKeys(driver, pobj.unpaidTaxes, dataSet.get("UnpaidTaxes"));
		fc.utobj().sendKeys(driver, pobj.businessHoldings, dataSet.get("BusinessHoldings"));
		fc.utobj().sendKeys(driver, pobj.lifeInsurance, dataSet.get("LifeInsurance"));
		fc.utobj().sendKeys(driver, pobj.otherAssets, dataSet.get("OtherAssets"));
		fc.utobj().sendKeys(driver, pobj.otherLiabilities, dataSet.get("OtherLiabilities"));

		fc.utobj().sendKeys(driver, pobj.assestsDescription, dataSet.get("assetsDescription"));
		fc.utobj().sendKeys(driver, pobj.liablitiesDescription, dataSet.get("liabilitiesDescription"));

		String expectedTotalAssets = dataSet.get("TotalAssets");
		String totalAssets = fc.utobj().getTextTxtFld(driver, pobj.totalAssets);
		String expectedTotalLiabilities = dataSet.get("TotalLiabilities");
		String totalLiabilities = fc.utobj().getTextTxtFld(driver, pobj.totalLiabilities);
		String expectedTotalNetworth = dataSet.get("TotalNetWorth");
		String totalNetworth = fc.utobj().getTextTxtFld(driver, pobj.totalNetworth);

		System.out.println(expectedTotalAssets);
		System.out.println(totalAssets);
		System.out.println(expectedTotalLiabilities);
		System.out.println(totalLiabilities);
		System.out.println(expectedTotalNetworth);
		System.out.println(totalNetworth);

		if (!totalAssets.equalsIgnoreCase(expectedTotalAssets)) {
			fc.utobj().printBugStatus("Total Assets did not match");
		}

		if (!totalLiabilities.equalsIgnoreCase(expectedTotalLiabilities)) {
			fc.utobj().printBugStatus("Total Liabilities did not match");
		}

		if (!totalNetworth.equalsIgnoreCase(expectedTotalNetworth)) {
			fc.utobj().printBugStatus("Total net worth did not match");
		}

		List<String> listInfo = new ArrayList<String>();
		listInfo.add(dataSet.get("assetsDescription"));
		listInfo.add(dataSet.get("liabilitiesDescription"));
		return listInfo;

	}

	public List<String> fillQualificationDetails_RealEstateOwned(WebDriver driver, Map<String, String> config)
			throws Exception {

		Reporter.log("Filling Lead Qualification Details > Real Estate Owned");

		String testCaseId = "TC_LeadQualificationDetails_RealEstateOwned_003";
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);

		FSLeadSummaryQualificationDetailsPage pobj = new FSLeadSummaryQualificationDetailsPage(driver);

		// Real estate

		fc.utobj().sendKeys(driver, pobj.reAddress1, dataSet.get("RealEstateAddress1"));
		fc.utobj().sendKeys(driver, pobj.reDatePurchased1, dataSet.get("RealEstateDatePurchased1"));
		fc.utobj().sendKeys(driver, pobj.reOrigCost1, dataSet.get("RealEstateOriginalCost1"));
		fc.utobj().sendKeys(driver, pobj.rePresentValue1, dataSet.get("RealEstatePresentValue1"));
		fc.utobj().sendKeys(driver, pobj.reMortgageBalance1, dataSet.get("RealEstateMortgageBalance1"));

		fc.utobj().sendKeys(driver, pobj.reAddress2, dataSet.get("RealEstateAddress2"));
		fc.utobj().sendKeys(driver, pobj.reDatePurchased2, dataSet.get("RealEstateDatePurchased2"));
		fc.utobj().sendKeys(driver, pobj.reOrigCost2, dataSet.get("RealEstateOriginalCost2"));
		fc.utobj().sendKeys(driver, pobj.rePresentValue2, dataSet.get("RealEstatePresentValue2"));
		fc.utobj().sendKeys(driver, pobj.reMortgageBalance2, dataSet.get("RealEstateMortgageBalance2"));

		fc.utobj().sendKeys(driver, pobj.reAddress3, dataSet.get("RealEstateAddress3"));
		fc.utobj().sendKeys(driver, pobj.reDatePurchased3, dataSet.get("RealEstateDatePurchased3"));
		fc.utobj().sendKeys(driver, pobj.reOrigCost3, dataSet.get("RealEstateOriginalCost3"));
		fc.utobj().sendKeys(driver, pobj.rePresentValue3, dataSet.get("RealEstatePresentValue3"));
		fc.utobj().sendKeys(driver, pobj.reMortgageBalance3, dataSet.get("RealEstateMortgageBalance3"));

		List<String> listInfo = new ArrayList<String>();
		listInfo.add(dataSet.get("RealEstateAddress1"));
		listInfo.add(dataSet.get("RealEstateDatePurchased1"));
		listInfo.add(dataSet.get("RealEstateAddress2"));
		listInfo.add(dataSet.get("RealEstateDatePurchased2"));
		listInfo.add(dataSet.get("RealEstateAddress3"));
		listInfo.add(dataSet.get("RealEstateDatePurchased3"));

		return listInfo;

	}

	public void fillQualificationDetails_AnnualSourcesOfIncome(WebDriver driver, Map<String, String> config)
			throws Exception {

		Reporter.log("Filling Lead Qualification Details > Annual Sources Of Income");

		String testCaseId = "TC_LeadQualificationDetails_AnnualSourcesOfIncome_004";
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);

		FSLeadSummaryQualificationDetailsPage pobj = new FSLeadSummaryQualificationDetailsPage(driver);
		// Annual Sources Of Income

		fc.utobj().sendKeys(driver, pobj.annualSalary, dataSet.get("Salary"));
		fc.utobj().sendKeys(driver, pobj.annualInvestment, dataSet.get("Investment"));
		fc.utobj().sendKeys(driver, pobj.annualReIncome, dataSet.get("RealEstateIncome"));
		fc.utobj().sendKeys(driver, pobj.otherAnnualSource, dataSet.get("Other"));
		fc.utobj().sendKeys(driver, pobj.otherAnnualSourceDescription, dataSet.get("Description"));

		String expectedTotal = dataSet.get("Total");

		fc.utobj().clickElement(driver, pobj.annualSourceTotal);

		String actualTotal = fc.utobj().getTextTxtFld(driver, pobj.annualSourceTotal);

		if (!expectedTotal.equalsIgnoreCase(actualTotal)) {
			fc.utobj().printBugStatus("Total - Annual Sources Of Income did not match");
		}
	}

	public void fillQualificationDetails_TotalContingentLiabilities(WebDriver driver, Map<String, String> config)
			throws Exception {

		Reporter.log("Filling Lead Qualification Details > Total Contingent Liabilities");

		String testCaseId = "TC_LeadQualificationDetails_TotalContingentLiabilities_005";
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);

		FSLeadSummaryQualificationDetailsPage pobj = new FSLeadSummaryQualificationDetailsPage(driver);

		// Total Contingent Liabilities

		fc.utobj().sendKeys(driver, pobj.loanCoSign, dataSet.get("LoanCosignature"));
		fc.utobj().sendKeys(driver, pobj.legalJudgement, dataSet.get("LegalJudgement"));
		fc.utobj().sendKeys(driver, pobj.incomeTaxes, dataSet.get("IncomeTaxes"));
		fc.utobj().sendKeys(driver, pobj.otherSpecialDebt, dataSet.get("OtherSpecialDebt"));

		String total = dataSet.get("Total");

		fc.utobj().clickElement(driver, pobj.totalContingent);

		String actualTotal = fc.utobj().getTextTxtFld(driver, pobj.totalContingent);

		if (!actualTotal.equalsIgnoreCase(total)) {
			fc.utobj().printBugStatus("Total - Contingent Liabilities did not match");
		}
	}

	public List<String> fillQualificationDetails_SpecificData(WebDriver driver, Map<String, String> config)
			throws Exception {

		Reporter.log("Filling Lead Qualification Details > Specific Data");

		String testCaseId = "TC_LeadQualificationDetails_SpecificData_006";
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);

		FSLeadSummaryQualificationDetailsPage pobj = new FSLeadSummaryQualificationDetailsPage(driver);

		// Specific Data

		fc.utobj().sendKeys(driver, pobj.whenReadyIfApproved, dataSet.get("Whenwouldyoubereadytoinvest"));
		fc.utobj().sendKeys(driver, pobj.skillsExperience, dataSet.get("Whatskillsexperiencedoyouhave"));
		fc.utobj().sendKeys(driver, pobj.enableReachGoals, dataSet.get("Whydoyouthink"));
		fc.utobj().sendKeys(driver, pobj.responsibleForDailyOperations, dataSet.get("Whowillberesponsible"));
		fc.utobj().sendKeys(driver, pobj.cashAvailable, dataSet.get("Amountofcashavailableforinvestment"));

		if (dataSet.get("Haveyoubeenapprovedforfinancing").equalsIgnoreCase("yes")) {
			fc.utobj().clickElement(driver, pobj.approvedForFinancing_yes);
		} else if (dataSet.get("Haveyoubeenapprovedforfinancing").equalsIgnoreCase("no")) {
			fc.utobj().clickElement(driver, pobj.approvedForFinancing_no);
		}

		fc.utobj().sendKeys(driver, pobj.amountApproved, dataSet.get("AmountApproved"));

		if (dataSet.get("Wouldthisbusinessbeyoursoleincomesource").equalsIgnoreCase("yes")) {
			fc.utobj().clickElement(driver, pobj.soleIncomeSource_yes);
		} else if (dataSet.get("Wouldthisbusinessbeyoursoleincomesource").equalsIgnoreCase("no")) {
			fc.utobj().clickElement(driver, pobj.soleIncomeSource_no);
		}

		if (dataSet.get("Doyouhaveanycontingentliabilities").equalsIgnoreCase("yes")) {
			fc.utobj().clickElement(driver, pobj.liablities_yes);
		} else if (dataSet.get("Doyouhaveanycontingentliabilities").equalsIgnoreCase("no")) {
			fc.utobj().clickElement(driver, pobj.liablities_no);
		}

		if (dataSet.get("Areyounoworhaveyoueverbeenpartytoanylawsuit").equalsIgnoreCase("yes")) {
			fc.utobj().clickElement(driver, pobj.lawsuit_yes);
		} else if (dataSet.get("Areyounoworhaveyoueverbeenpartytoanylawsuit").equalsIgnoreCase("no")) {
			fc.utobj().clickElement(driver, pobj.lawsuit_no);
		}

		if (dataSet.get("Haveyoueverbeenconvictedofanyoffense").equalsIgnoreCase("yes")) {
			fc.utobj().clickElement(driver, pobj.convicted_yes);
		} else if (dataSet.get("Haveyoueverbeenconvictedofanyoffense").equalsIgnoreCase("no")) {
			fc.utobj().clickElement(driver, pobj.convicted_no);
		}

		if (dataSet.get("Haveyoueverbeenconvictedofafelony").equalsIgnoreCase("yes")) {
			fc.utobj().clickElement(driver, pobj.convictedOfFelony_yes);
		} else if (dataSet.get("Haveyoueverbeenconvictedofafelony").equalsIgnoreCase("no")) {
			fc.utobj().clickElement(driver, pobj.convictedOfFelony_no);
		}

		fc.utobj().sendKeys(driver, pobj.explainConviction, dataSet.get("Ifsoexplain"));

		if (dataSet.get("Haveyoueverfiledforbankruptcy").equalsIgnoreCase("yes")) {
			fc.utobj().clickElement(driver, pobj.filedBankruptcy_yes);
		} else if (dataSet.get("Haveyoueverfiledforbankruptcy").equalsIgnoreCase("no")) {
			fc.utobj().clickElement(driver, pobj.filedBankruptcy_no);
		}

		fc.utobj().sendKeys(driver, pobj.dateFiled, dataSet.get("DateFiled"));
		fc.utobj().sendKeys(driver, pobj.dateDischarged, dataSet.get("DateDischarged"));

		fc.utobj().sendKeys(driver, pobj.locationPreference1, dataSet.get("Preference1"));
		fc.utobj().sendKeys(driver, pobj.locationPreference2, dataSet.get("Preference2"));
		fc.utobj().sendKeys(driver, pobj.locationPreference3, dataSet.get("Preference3"));

		fc.utobj().sendKeys(driver, pobj.businessQuestion1, dataSet.get("Question1"));
		fc.utobj().sendKeys(driver, pobj.businessQuestion2, dataSet.get("Question2"));
		fc.utobj().sendKeys(driver, pobj.businessQuestion3, dataSet.get("Question3"));

		List<String> listInfo = new ArrayList<String>();
		listInfo.add(dataSet.get("Whenwouldyoubereadytoinvest"));
		listInfo.add(dataSet.get("Preference1"));
		listInfo.add(dataSet.get("Preference2"));
		listInfo.add(dataSet.get("Preference3"));
		listInfo.add(dataSet.get("Question1"));
		listInfo.add(dataSet.get("Question2"));
		listInfo.add(dataSet.get("Question3"));
		return listInfo;

	}

	public List<String> fillQualificationDetails_InternalAnalysisofApplicant(WebDriver driver,
			Map<String, String> config) throws Exception {
		Reporter.log("Filling Lead Qualification Details > Specific Data");

		String testCaseId = "TC_LeadQualificationDetails_InternalAnalysisofApplicant_007";
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);

		FSLeadSummaryQualificationDetailsPage pobj = new FSLeadSummaryQualificationDetailsPage(driver);

		fc.utobj().sendKeys(driver, pobj.appFirstName, dataSet.get("FirstName"));
		fc.utobj().sendKeys(driver, pobj.appLastName, dataSet.get("LastName"));
		fc.utobj().sendKeys(driver, pobj.appAddress, dataSet.get("Address"));
		fc.utobj().sendKeys(driver, pobj.appCity, dataSet.get("City"));
		fc.utobj().sendKeys(driver, pobj.appPhone, dataSet.get("WorkPhone"));
		fc.utobj().selectDropDownByVisibleText(driver, pobj.appCountry, dataSet.get("Country"));

		fc.utobj().selectDropDownByVisibleText(driver, pobj.appState, dataSet.get("StateProvince"));
		fc.utobj().sendKeys(driver, pobj.appPhoneExt2, dataSet.get("WorkPhoneExtension"));

		if (dataSet.get("CanApplicantbeCalledatOffice").equalsIgnoreCase("yes")) {
			fc.utobj().clickElement(driver, pobj.appCanApplicantbeCalled_yes);
		} else if (dataSet.get("CanApplicantbeCalledatOffice").equalsIgnoreCase("no")) {
			fc.utobj().clickElement(driver, pobj.appCanApplicantbeCalled_no);
		}

		fc.utobj().selectDropDownByVisibleText(driver, pobj.currentNetWorth, dataSet.get("CurrentNetWorth"));
		fc.utobj().selectDropDownByVisibleText(driver, pobj.cashAvailableforInvestment,
				dataSet.get("CashAvailableforInvestment"));
		fc.utobj().selectDropDownByVisibleText(driver, pobj.investTimeframe, dataSet.get("InvestmentTimeframe"));
		fc.utobj().selectDropDownByVisibleText(driver, pobj.employmentbackground, dataSet.get("EmploymentBackground"));

		if (dataSet.get("BackgroundCheckApproval").equalsIgnoreCase("yes")) {
			fc.utobj().clickElement(driver, pobj.backgroundCheckApproval_yes);
		} else if (dataSet.get("BackgroundCheckApproval").equalsIgnoreCase("no")) {
			fc.utobj().clickElement(driver, pobj.backgroundCheckApproval_no);
		}

		fc.utobj().sendKeys(driver, pobj.LendableNetWorth_Value, dataSet.get("LendableNetWorth_Value"));

		if (dataSet.get("LendableNetWorth_Completed").equalsIgnoreCase("yes")) {
			fc.utobj().clickElement(driver, pobj.LendableNetWorth_Completed_yes);
		} else if (dataSet.get("LendableNetWorth_Completed").equalsIgnoreCase("no")) {
			fc.utobj().clickElement(driver, pobj.LendableNetWorth_Completed_No);
		}

		fc.utobj().sendKeys(driver, pobj.LendableNetWorth_CompletionDate,
				dataSet.get("LendableNetWorth_CompletionDate"));

		fc.utobj().sendKeys(driver, pobj.LendableNetWorth_associatedDocument,
				fc.utobj().getFilePathFromTestData(dataSet.get("LendableNetWorth_associatedDocument")));
		fc.utobj().sendKeys(driver, pobj.LendableNetWorth_VerifiedBy, dataSet.get("LendableNetWorth_VerifiedBy"));
		fc.utobj().sendKeys(driver, pobj.LendableNetWorth_Date, dataSet.get("LendableNetWorth_Date"));

		fc.utobj().sendKeys(driver, pobj.BackgroundandCriminalCheck_Value,
				dataSet.get("BackgroundandCriminalCheck_Value"));

		if (dataSet.get("BackgroundandCriminalCheck_Completed").equalsIgnoreCase("yes")) {
			fc.utobj().clickElement(driver, pobj.BackgroundandCriminalCheck_Completed_yes);
		} else if (dataSet.get("BackgroundandCriminalCheck_Completed").equalsIgnoreCase("no")) {
			fc.utobj().clickElement(driver, pobj.BackgroundandCriminalCheck_Completed_yes);
		}

		fc.utobj().sendKeys(driver, pobj.BackgroundandCriminalCheck_CompletionDate,
				dataSet.get("BackgroundandCriminalCheck_CompletionDate"));

		fc.utobj().sendKeys(driver, pobj.BackgroundandCriminalCheck_associatedDocument,
				fc.utobj().getFilePathFromTestData(dataSet.get("BackgroundandCriminalCheck_associatedDocument")));
		fc.utobj().sendKeys(driver, pobj.BackgroundandCriminalCheck_VerifiedBy,
				dataSet.get("BackgroundandCriminalCheck_VerifiedBy"));
		fc.utobj().sendKeys(driver, pobj.BackgroundandCriminalCheck_Date,
				dataSet.get("BackgroundandCriminalCheck_Date"));

		// Credit Check
		fc.utobj().sendKeys(driver, pobj.CreditCheck_Value, dataSet.get("CreditCheck_Value"));

		if (dataSet.get("CreditCheck_Completed").equalsIgnoreCase("yes")) {
			fc.utobj().clickElement(driver, pobj.CreditCheck_Completed_yes);
		} else if (dataSet.get("CreditCheck_Completed").equalsIgnoreCase("no")) {
			fc.utobj().clickElement(driver, pobj.CreditCheck_Completed_No);
		}

		fc.utobj().sendKeys(driver, pobj.CreditCheck_CompletionDate,
				dataSet.get("CreditCheck_CompletionDate"));
		fc.utobj().sendKeys(driver, pobj.CreditCheck_associatedDocument,
				fc.utobj().getFilePathFromTestData(dataSet.get("CreditCheck_associatedDocument")));
		fc.utobj().sendKeys(driver, pobj.CreditCheck_VerifiedBy, dataSet.get("CreditCheck_VerifiedBy"));
		fc.utobj().sendKeys(driver, pobj.CreditCheck_Date, dataSet.get("CreditCheck_Date"));

		// Territory Approved
		fc.utobj().sendKeys(driver, pobj.TerritoryApproved_Value, dataSet.get("TerritoryApproved_Value"));

		if (dataSet.get("TerritoryApproved_Completed").equalsIgnoreCase("yes")) {
			fc.utobj().clickElement(driver, pobj.TerritoryApproved_Completed_yes);
		} else if (dataSet.get("TerritoryApproved_Completed").equalsIgnoreCase("no")) {
			fc.utobj().clickElement(driver, pobj.TerritoryApproved_Completed_No);
		}

		fc.utobj().sendKeys(driver, pobj.TerritoryApproved_CompletionDate,
				dataSet.get("TerritoryApproved_CompletionDate"));

		fc.utobj().sendKeys(driver, pobj.TerritoryApproved_associatedDocument,
				fc.utobj().getFilePathFromTestData(dataSet.get("TerritoryApproved_associatedDocument")));
		fc.utobj().sendKeys(driver, pobj.TerritoryApproved_VerifiedBy, dataSet.get("TerritoryApproved_VerifiedBy"));
		fc.utobj().sendKeys(driver, pobj.TerritoryApproved_Date, dataSet.get("TerritoryApproved_Date"));

		// Franchise Agreement on File
		fc.utobj().sendKeys(driver, pobj.FranchiseAgreementonFile_Value, dataSet.get("FranchiseAgreementonFile_Value"));

		if (dataSet.get("FranchiseAgreementonFile_Completed").equalsIgnoreCase("yes")) {
			fc.utobj().clickElement(driver, pobj.FranchiseAgreementonFile_Completed_yes);
		} else if (dataSet.get("FranchiseAgreementonFile_Completed").equalsIgnoreCase("no")) {
			fc.utobj().clickElement(driver, pobj.FranchiseAgreementonFile_Completed_No);
		}

		fc.utobj().sendKeys(driver, pobj.FranchiseAgreementonFile_CompletionDate,
				dataSet.get("FranchiseAgreementonFile_CompletionDate"));

		fc.utobj().sendKeys(driver, pobj.FranchiseAgreementonFile_associatedDocument,
				fc.utobj().getFilePathFromTestData(dataSet.get("FranchiseAgreementonFile_associatedDocument")));
		fc.utobj().sendKeys(driver, pobj.FranchiseAgreementonFile_VerifiedBy,
				dataSet.get("FranchiseAgreementonFile_VerifiedBy"));
		fc.utobj().sendKeys(driver, pobj.FranchiseAgreementonFile_Date,
				dataSet.get("FranchiseAgreementonFile_Date"));

		// FDD Receipt on File
		fc.utobj().sendKeys(driver, pobj.FDDReceiptonFile_Value, dataSet.get("FDDReceiptonFile_Value"));

		if (dataSet.get("FDDReceiptonFile_Completed").equalsIgnoreCase("yes")) {
			fc.utobj().clickElement(driver, pobj.FDDReceiptonFile_Completed_yes);
		} else if (dataSet.get("FDDReceiptonFile_Completed").equalsIgnoreCase("no")) {
			fc.utobj().clickElement(driver, pobj.FDDReceiptonFile_CompletionDate);
		}

		fc.utobj().sendKeys(driver, pobj.FDDReceiptonFile_CompletionDate,
				dataSet.get("FDDReceiptonFile_CompletionDate"));

		fc.utobj().sendKeys(driver, pobj.FDDReceiptonFile_associatedDocument,
				fc.utobj().getFilePathFromTestData(dataSet.get("FDDReceiptonFile_associatedDocument")));
		fc.utobj().sendKeys(driver, pobj.FDDReceiptonFile_VerifiedBy, dataSet.get("FDDReceiptonFile_VerifiedBy"));
		fc.utobj().sendKeys(driver, pobj.FDDReceiptonFile_Date, dataSet.get("FDDReceiptonFile_Date"));

		List<String> listInfo = new ArrayList<String>();
		listInfo.add(dataSet.get("FDDReceiptonFile_Value"));
		listInfo.add(dataSet.get("FranchiseAgreementonFile_Value"));
		listInfo.add(dataSet.get("TerritoryApproved_Value"));
		listInfo.add(dataSet.get("CreditCheck_Value"));
		listInfo.add(dataSet.get("LendableNetWorth_Value"));
		return listInfo;
	}

	public void deleteQualificationDetails(WebDriver driver) throws Exception {
		FSLeadSummaryQualificationDetailsPage pobj = new FSLeadSummaryQualificationDetailsPage(driver);

		try {
			fc.utobj().clickElement(driver, pobj.delete);
			fc.utobj().acceptAlertBox(driver);
			fc.utobj().moveToElement(driver, fc.utobj().getElement(driver, pobj.saveBtn));
		} catch (Exception e) {
			fc.utobj().printBugStatus("Error in deleting qualification details tab");
		}
	}

	public void modifyQualificationDetails(WebDriver driver) throws Exception {

		try {
			FSLeadSummaryQualificationDetailsPage pobj = new FSLeadSummaryQualificationDetailsPage(driver);
			fc.utobj().clickElement(driver, pobj.modify);
		} catch (Exception e) {
			fc.utobj().printBugStatus("Error in modifying qualification details tab");
		}
	}

	public void validateModifyPage(WebDriver driver, List<String> listItems) throws Exception {

		boolean isValueFound = fc.utobj().assertMultipleInputBoxValue(driver, listItems);

		if (isValueFound == false) {
			fc.utobj().printBugStatus("Values missing. ");
		}

	}
}
