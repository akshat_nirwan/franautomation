package com.builds.test.fs;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.fs.FSLeadSummarySendEmailPage;
import com.builds.utilities.FranconnectUtil;

public class FSLeadSummarySendEmailPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	public String sendEmail(WebDriver driver) throws Exception {

		String mailSubject = "Test Email Subject ";
		mailSubject = mailSubject + fc.utobj().generateTestData(mailSubject);
		String mailBody = "Test Email By FranConnect System ";
		mailBody = mailBody + fc.utobj().generateTestData(mailBody);
		FSLeadSummarySendEmailPage pobj = new FSLeadSummarySendEmailPage(driver);
		fc.utobj().sendKeys(driver, pobj.subject, mailSubject);
		String windowHandle = driver.getWindowHandle();
		driver.switchTo().frame(pobj.htmlFrame);
		fc.utobj().sendKeys(driver, pobj.mailText, mailBody);
		driver.switchTo().window(windowHandle);
		fc.utobj().clickElement(driver, pobj.sendBtn);
		System.out.println(mailSubject);
		return mailSubject;
	}
}
