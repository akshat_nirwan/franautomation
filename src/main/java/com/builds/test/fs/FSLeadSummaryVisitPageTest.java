package com.builds.test.fs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.Reporter;

import com.builds.uimaps.fs.FSLeadSummaryVisitPage;
import com.builds.utilities.FranconnectUtil;

public class FSLeadSummaryVisitPageTest {
	FranconnectUtil fc = new FranconnectUtil();
	// To be verified only

	// Anukaran
	public void addVisitWithAssignTo(WebDriver driver, Map<String, String> config, String assignTo) throws Exception {
		String testCaseId = "TC_FS_Visit_FullDetails_001";
		Reporter.log("Filling Visit Details");

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);

		FSLeadSummaryVisitPage pobj = new FSLeadSummaryVisitPage(driver);

		String visitSchedule = fc.utobj().getCurrentDateUSFormat();
		String visitDate = fc.utobj().getCurrentDateUSFormat();
		String type = dataSet.get("type");
		String name1 = fc.utobj().generateTestData(dataSet.get("visitor1Name"));
		String relationship1 = dataSet.get("relationship1");
		String name2 = fc.utobj().generateTestData(dataSet.get("name2"));
		String relationship2 = dataSet.get("relationship2");
		String name3 = fc.utobj().generateTestData(dataSet.get("name3"));
		String relationship3 = dataSet.get("relationship3");
		String agreedReimbursement = dataSet.get("agreedReimbursement");
		String actualReimbursement = dataSet.get("actualReimbursement");
		String paymentSentDate = fc.utobj().getCurrentDateUSFormat();
		String visitConfirmedBy = dataSet.get("visitConfirmedBy");
		String comments = dataSet.get("comments");

		fc.utobj().sendKeys(driver, pobj.visitSchedule, visitSchedule);
		fc.utobj().sendKeys(driver, pobj.visitdate, visitDate);
		if (type.equalsIgnoreCase("Individual")) {
			fc.utobj().clickElement(driver, pobj.typeGroup);
		} else if (type.equalsIgnoreCase("Individual")) {
			fc.utobj().clickElement(driver, pobj.typeIndividual);
		}
		fc.utobj().clickElement(driver, pobj.addToCalendar);

		if (assignTo.equalsIgnoreCase("Lead Owner")) {
			fc.utobj().clickElement(driver, pobj.assignToLeadOwner);
		} else if (assignTo.equalsIgnoreCase("Logged in User")) {
			fc.utobj().clickElement(driver, pobj.assignToLoggedInUser);
			fc.utobj().clickElement(driver, pobj.userListBtn);
			fc.utobj().sendKeys(driver, pobj.searchUser, assignTo);
			fc.utobj().clickElement(driver, pobj.selectAll);
		} else {
			fc.utobj().clickElement(driver, pobj.assignToOtherUser);
			fc.utobj().selectValFromMultiSelect(driver, pobj.assignToOtherUserName, assignTo);
		}

		fc.utobj().sendKeys(driver, pobj.visitor1Name, name1);
		fc.utobj().sendKeys(driver, pobj.relationship1, relationship1);
		fc.utobj().sendKeys(driver, pobj.visitor2Name, name2);
		fc.utobj().sendKeys(driver, pobj.relationship2, relationship2);
		fc.utobj().sendKeys(driver, pobj.visitor1Name, name3);
		fc.utobj().sendKeys(driver, pobj.relationship3, relationship3);

		fc.utobj().sendKeys(driver, pobj.agreedReimbursement, agreedReimbursement);
		fc.utobj().sendKeys(driver, pobj.actualReimbursement, actualReimbursement);
		fc.utobj().sendKeys(driver, pobj.paymentSentDate, paymentSentDate);
		fc.utobj().sendKeys(driver, pobj.visitConfirmed, visitConfirmedBy);
		fc.utobj().sendKeys(driver, pobj.comments, comments);

		fc.utobj().clickElement(driver, pobj.saveBtn);

	}

	public Map<String, String> addVisitWithFullInfo(WebDriver driver, Map<String, String> config) throws Exception {
		String testCaseId = "TC_FS_Visit_FullDetails_001";
		Reporter.log("Filling Visit Details");

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);

		FSLeadSummaryVisitPage pobj = new FSLeadSummaryVisitPage(driver);

		String visitSchedule = dataSet.get("visitSchedule");
		String visitDate = dataSet.get("visitDate");
		String type = dataSet.get("type");
		String addToCalendar = dataSet.get("addToCalendar");
		String assignTo = dataSet.get("assignTo");
		String name1 = fc.utobj().generateTestData(dataSet.get("name1"));
		String relationship1 = dataSet.get("relationship1");
		String name2 = fc.utobj().generateTestData(dataSet.get("name2"));
		String relationship2 = dataSet.get("relationship2");
		String name3 = fc.utobj().generateTestData(dataSet.get("name3"));
		String relationship3 = dataSet.get("relationship3");
		String agreedReimbursement = dataSet.get("agreedReimbursement");
		String actualReimbursement = dataSet.get("actualReimbursement");
		String paymentSentDate = dataSet.get("paymentSentDate");
		String visitConfirmedBy = dataSet.get("visitConfirmedBy");
		String comments = dataSet.get("comments");

		fc.utobj().sendKeys(driver, pobj.visitSchedule, visitSchedule);
		fc.utobj().sendKeys(driver, pobj.visitdate, visitDate);
		fc.utobj().selectDropDownByVisibleText(driver, pobj.type, type);
		fc.utobj().check(pobj.addToCalendar, addToCalendar);

		if (assignTo.equalsIgnoreCase("Lead Owner")) {
			fc.utobj().clickElement(driver, pobj.assignToLeadOwner);
		} else if (assignTo.equalsIgnoreCase("Logged in User")) {
			fc.utobj().clickElement(driver, pobj.assignToLoggedInUser);
			fc.utobj().clickElement(driver, pobj.userListBtn);
			fc.utobj().sendKeys(driver, pobj.searchUser, assignTo);
			fc.utobj().clickElement(driver, pobj.selectAll);
		} else {
			fc.utobj().clickElement(driver, pobj.assignToOtherUser);
		}

		fc.utobj().sendKeys(driver, pobj.visitor1Name, name1);
		fc.utobj().sendKeys(driver, pobj.relationship1, relationship1);
		fc.utobj().sendKeys(driver, pobj.visitor2Name, name2);
		fc.utobj().sendKeys(driver, pobj.relationship2, relationship2);
		fc.utobj().sendKeys(driver, pobj.visitor1Name, name3);
		fc.utobj().sendKeys(driver, pobj.relationship3, relationship3);

		fc.utobj().sendKeys(driver, pobj.agreedReimbursement, agreedReimbursement);
		fc.utobj().sendKeys(driver, pobj.actualReimbursement, actualReimbursement);
		fc.utobj().sendKeys(driver, pobj.paymentSentDate, paymentSentDate);
		fc.utobj().sendKeys(driver, pobj.visitConfirmed, visitConfirmedBy);
		fc.utobj().sendKeys(driver, pobj.comments, comments);

		fc.utobj().clickElement(driver, pobj.saveBtn);

		List<String> listItems = new ArrayList<String>();
		listItems.add(visitSchedule);
		listItems.add(visitDate);
		listItems.add(type);

		listItems.add(name1);
		listItems.add(relationship1);
		listItems.add(name2);
		listItems.add(relationship2);
		listItems.add(name3);
		listItems.add(relationship3);

		listItems.add(agreedReimbursement);
		listItems.add(actualReimbursement);
		listItems.add(paymentSentDate);
		listItems.add(visitConfirmedBy);
		listItems.add(comments);

		boolean visitAdded = fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);
		if (visitAdded == false) {
			Reporter.log("Either Visit not Saved / Few or All the information of visit is not displayed");
		}

		modifyVisit(driver);

		List<String> listItems2 = new ArrayList<String>();
		listItems2.add(visitSchedule);
		listItems2.add(visitDate);

		listItems2.add(name1);
		listItems2.add(relationship1);
		listItems2.add(name2);
		listItems2.add(relationship2);
		listItems2.add(name3);
		listItems2.add(relationship3);

		listItems2.add(agreedReimbursement);
		listItems2.add(actualReimbursement);
		listItems2.add(paymentSentDate);
		listItems2.add(visitConfirmedBy);
		listItems2.add(comments);

		boolean isTextPresent = fc.utobj().assertMultipleInputBoxValue(driver, listItems2);
		if (isTextPresent == false) {
			Reporter.log("Few or All of the information is missing in modify page!");
		}

		fc.utobj().clickElement(driver, pobj.saveBtn);

		deleteVisit(driver);
		visitSchedule = fc.utobj().getTextTxtFld(driver, pobj.visitdate);

		if (visitSchedule.length() > 0) {
			Reporter.log("Field Value still available after Delete / Or Unable to delete the Visit Tab");
		}

		Map<String, String> visitInfo = new HashMap<String, String>();

		return visitInfo;

	}

	private void modifyVisit(WebDriver driver) throws Exception {

		Reporter.log("Modifying Visit");
		FSLeadSummaryVisitPage visitPage = new FSLeadSummaryVisitPage(driver);

		fc.utobj().clickElement(driver, visitPage.modifyVisit);

	}

	private void deleteVisit(WebDriver driver) throws Exception {

		Reporter.log("Deleting Visit");
		FSLeadSummaryVisitPage visitPage = new FSLeadSummaryVisitPage(driver);

		fc.utobj().clickElement(driver, visitPage.deleteVisit);

	}
}
