package com.builds.test.fs;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.fs.FSLeadTabPage;
import com.builds.utilities.FranconnectUtil;

class FSLeadTabs {
	FranconnectUtil fc = new FranconnectUtil();

	public void gotoPrimaryInfo1(WebDriver driver) throws Exception {

		FSLeadTabPage pobj = new FSLeadTabPage(driver);
		fc.utobj().clickElement(driver, pobj.primaryInfo);
	}

	public void gotoCandidatePortal1(WebDriver driver) throws Exception {

		FSLeadTabPage pobj = new FSLeadTabPage(driver);
		fc.utobj().clickElement(driver, pobj.candiatePortal);
	}

	public void gotoCoApplicant1(WebDriver driver) throws Exception {

		FSLeadTabPage pobj = new FSLeadTabPage(driver);
		fc.utobj().clickElement(driver, pobj.coapplicant);
	}

	public void gotoCompliance1(WebDriver driver) throws Exception {

		FSLeadTabPage pobj = new FSLeadTabPage(driver);
		fc.utobj().clickElement(driver, pobj.compliance);
	}

	public void gotoDocuments1(WebDriver driver) throws Exception {

		FSLeadTabPage pobj = new FSLeadTabPage(driver);
		fc.utobj().clickElement(driver, pobj.documents);
	}

	public void gotoPersonalProfile1(WebDriver driver) throws Exception {

		FSLeadTabPage pobj = new FSLeadTabPage(driver);
		fc.utobj().clickElement(driver, pobj.personalProfile);
	}

	public void gotoQualificationDetails1(WebDriver driver) throws Exception {

		FSLeadTabPage pobj = new FSLeadTabPage(driver);
		fc.utobj().clickElement(driver, pobj.qualficationDetails);
	}

	public void gotoRealEstate1(WebDriver driver) throws Exception {

		FSLeadTabPage pobj = new FSLeadTabPage(driver);
		fc.utobj().clickElement(driver, pobj.realEstate);
	}

	public void gotoVirtualBrochure1(WebDriver driver) throws Exception {

		FSLeadTabPage pobj = new FSLeadTabPage(driver);
		fc.utobj().clickElement(driver, pobj.virtualBrochure);
	}

	public void gotoVisit1(WebDriver driver) throws Exception {

		FSLeadTabPage pobj = new FSLeadTabPage(driver);
		fc.utobj().clickElement(driver, pobj.visit);
	}
}
