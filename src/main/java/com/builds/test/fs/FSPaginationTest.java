package com.builds.test.fs;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import org.xml.sax.SAXException;

import com.builds.uimaps.infomgr.InfoMgrPagination;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class FSPaginationTest {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "sales" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Sales_Paging_Report_Filter_LeadSummary", testCaseDescription = "This test case will verify LeadSummary Paging all the summary of Sales Module ", reference = {
			"" })
	public void VerifySalesSummaryPaging_LeadSummary() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			driver = fc.loginpage().login(driver);
			fc.commonMethods().getModules().clickSalesModule(driver);
			fc.utobj().printTestStep("verify Paging for Lead Summary ");
			showPagingSalesModule(driver, "LeadSummary", 20);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Sales_Paging_Report_Filter_Tasks", testCaseDescription = "This test case will verify Tasks Paging all the summary of Sales Module ", reference = {
			"" })
	public void VerifySalesSummaryPaging_Tasks() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.commonMethods().getModules().clickSalesModule(driver);
			fc.utobj().printTestStep("verify Paging for Task Summary ");
			showPagingSalesModule(driver, "Tasks", 20);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Sales_Paging_Report_Filter_BrokersSummary", testCaseDescription = "This test case will verify BrokersSummary Paging all the summary of Sales Module ", reference = {
			"" })
	public void VerifySalesSummaryPaging_BrokersSummary() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			driver = fc.loginpage().login(driver);
			fc.commonMethods().getModules().clickSalesModule(driver);
			fc.utobj().printTestStep("verify Paging for Brokers Summary ");
			showPagingSalesModule(driver, "BrokersSummary", 20);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Sales_Paging_Report_Filter_Sites", testCaseDescription = "This test case will verify Sites Paging all the summary of Sales Module ", reference = {
			"" })
	public void VerifySalesSummaryPaging_Sites() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			driver = fc.loginpage().login(driver);
			fc.commonMethods().getModules().clickSalesModule(driver);
			fc.utobj().printTestStep("verify Paging for Sites ");
			showPagingSalesModule(driver, "Sites", 20);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Sales_Paging_Report_Filter_FDD", testCaseDescription = "This test case will verify FDD Paging all the summary of Sales Module ", reference = {
			"" })
	public void VerifySalesSummaryPaging_FDD() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			driver = fc.loginpage().login(driver);
			fc.commonMethods().getModules().clickSalesModule(driver);
			fc.utobj().printTestStep("verify Paging for FDD ");
			showPagingSalesModule(driver, "FDD", 20);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Sales_Paging_Report_Filter_Groups", testCaseDescription = "This test case will verify Groups Paging all the summary of Sales Module ", reference = {
			"" })
	public void VerifySalesSummaryPaging_Groups() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			driver = fc.loginpage().login(driver);
			fc.commonMethods().getModules().clickSalesModule(driver);
			fc.utobj().printTestStep("verify Paging for Groups Summary ");
			showPagingSalesModule(driver, "Groups", 20);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Sales_Paging_Report_Filter_MailMerge", testCaseDescription = "This test case will verify MailMerge Paging all the summary of Sales Module ", reference = {
			"" })
	public void VerifySalesSummaryPaging_MailMerge() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			driver = fc.loginpage().login(driver);
			fc.commonMethods().getModules().clickSalesModule(driver);
			fc.utobj().printTestStep("verify Paging for Mail Merge ");
			showPagingSalesModule(driver, "MailMerge", 20);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales" , "TC_Sales_Paging_Report_Filter_Templates" , "sales_failed" }) // Verified // Akshat

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Sales_Paging_Report_Filter_Templates", testCaseDescription = "This test case will verify Templates Paging all the summary of Sales Module ", reference = {
			"" })
	public void VerifySalesSummaryPaging_Templates() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			driver = fc.loginpage().login(driver);
			fc.commonMethods().getModules().clickSalesModule(driver);
			fc.utobj().printTestStep("verify Paging for Templates Summary ");
			showPagingSalesModule(driver, "Templates", 20);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			// e.printStackTrace();
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Sales_Paging_Report_Filter_Campaigns", testCaseDescription = "This test case will verify Campaigns Paging all the summary of Sales Module ", reference = {
			"" })
	public void VerifySalesSummaryPaging_Campaigns() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			driver = fc.loginpage().login(driver);
			fc.commonMethods().getModules().clickSalesModule(driver);
			fc.utobj().printTestStep("verify Paging for Campaigns Summary ");
			showPagingSalesModule(driver, "Campaigns", 20);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Sales_Paging_Report_Workflows", testCaseDescription = "This test case will verify Workflows Paging all the summary of Sales Module ", reference = {
			"" })
	public void VerifySalesSummaryPaging_Workflows() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			driver = fc.loginpage().login(driver);

			// New UI Paging
			fc.commonMethods().getModules().clickSalesModule(driver);
			fc.utobj().printTestStep("verify Paging for Workflows Summary ");
			showPagingSalesModule(driver, "Workflows", 20);

			// End Lead Summary paging

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void showPagingSalesModule(WebDriver driver, String subModuleName, int viewPerPage)
			throws ParserConfigurationException, SAXException, IOException, InterruptedException, Exception

	{
		String testCaseId = "TC_Sales_Paging_Report_Filter";
		InfoMgrPagination InfoMgrPagination = new InfoMgrPagination(driver);

		if (subModuleName == null)
			subModuleName = "LeadSummary";
		List<WebElement> records = null;
		Sales fsmod = new Sales();
		fc.utobj().printTestStep("Search " + subModuleName + " and click");

		if (subModuleName != null && "LeadSummary".equals(subModuleName)) {
			fsmod.leadManagement(driver);
			try {
				records = driver.findElements(
						By.xpath(".//*[@id='fsLeadTable']/tbody/tr/td/form/table/tbody/tr/td/table/tbody/tr/td/table"));
				//
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (subModuleName != null && "Tasks".equals(subModuleName)) {
			fsmod.tasks(driver);
			try {
				records = driver.findElements(
						By.xpath(".//*[@id='siteMainTable']/tbody/tr/td/table/tbody/tr[6]/td/table/tbody/tr[1]/td"));
				//
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (subModuleName != null && "BrokersSummary".equals(subModuleName)) {
			fsmod.brokers(driver);
			try {
				records = driver.findElements(
						By.xpath(".//*[@id='siteMainTable']/tbody/tr/td/form[11]/table/tbody/tr[3]/td/table/tbody"));
				//
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (subModuleName != null && "Sites".equals(subModuleName)) {

			try {
				fsmod.sites(driver);
				records = driver.findElements(By.xpath(".//*[@id='siteMainTable']/tbody/tr/td/table[3]"));
				//
			} catch (Exception e) {
				// e.printStackTrace();
				fc.utobj().printTestStep(subModuleName + " Not clickable or opreational.");
			}
		} else if (subModuleName != null && "FDD".equals(subModuleName)) {
			fsmod.fdd(driver);
			try {
				records = driver.findElements(
						By.xpath(".//*[@id='siteMainTable']/tbody/tr/td/table/tbody/tr[5]/td[2]/table/tbody"));
				//
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (subModuleName != null && "MailMerge".equals(subModuleName)) {
			fsmod.mailMerge(driver);
			try {
				records = driver.findElements(By.xpath(
						".//*[@id='siteMainTable']/tbody/tr/td/table[2]/tbody/tr[1]/td[2]/div/table/tbody/tr/td/table/tbody"));
				//
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (subModuleName != null && ("Groups".equals(subModuleName) || "Templates".equals(subModuleName)
				|| "Workflows".equals(subModuleName))) {

			if ("Groups".equals(subModuleName))
				fsmod.groups(driver);
			else if ("Templates".equals(subModuleName)) {
				fsmod.emailCampaignTab(driver);
				fc.utobj().printTestStep("Navigate to Manage Templates");
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[@class='bottom-fixed-btn']//*[contains(text () ,'Manage Templates')]"));

			} else if ("Campaigns".equals(subModuleName)) {
				fsmod.emailCampaignTab(driver);
				fc.utobj().printTestStep("Navigate to Manage Campaigns");
				fc.utobj().clickElement(driver, driver.findElement(
						By.xpath(".//*[@class='bottom-fixed-btn']//*[contains(text () ,'Manage Campaigns')]")));

			} else if ("Workflows".equals(subModuleName)) {
				fsmod.emailCampaignTab(driver);
				fc.utobj().printTestStep("Navigate to Manage Workflows");
				try {
					fc.utobj().clickElement(driver, driver.findElement(
							By.xpath(".//*[@class='bottom-fixed-btn']//*[contains(text () ,'Manage Workflows')]")));
				} catch (Exception e) {
					fc.utobj().printTestStep("You don't have any active workflows");
				}

			}

			try {
				records = driver.findElements(By.xpath(".//*[@id='siteMainTable']/tbody/tr/td/div"));
				//
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		String header;
		int totalRecordCount = 0;
		int totalrecordPerPage = 0;
		try {
			header = fc.utobj().getElementByXpath(driver, ".//*[@id='pageid']").getText();
			if (subModuleName != null && ("Groups".equals(subModuleName) || "Templates".equals(subModuleName)
					|| "Workflows".equals(subModuleName))) {
				header = fc.utobj().getElementByXpath(driver, ".//*[@class='caption white-space']").getText();
			}

			String options[] = header.split(" ");
			if (options != null) {
				totalRecordCount = Integer.valueOf(options[5]);
				totalrecordPerPage = Integer.valueOf(options[3]);
			}
			fc.utobj().printTestStep(subModuleName + " ToTal Record " + totalRecordCount);

		} catch (Exception E) {
			fc.utobj().printTestStep("Pagination not found in " + subModuleName);
			// System.out.println("Pagination not found");
			totalRecordCount = 0;
		}

		if (totalRecordCount > 0 && totalRecordCount > viewPerPage) {
			fc.utobj().printTestStep("Pagination exists in " + subModuleName);

			int clickCount = totalRecordCount / viewPerPage;
			String optionsShoud[] = new String[clickCount + 1];
			;
			int inLastRerord = totalRecordCount - (viewPerPage * clickCount);
			for (int cnt = 0; cnt <= clickCount; cnt++) {

				optionsShoud[cnt] = viewPerPage + "";
				if (cnt == clickCount) {
					optionsShoud[cnt] = inLastRerord + "";
				}

			}

			int foundRecordCount = 0;
			int counter = 1;
			try {
				if (optionsShoud != null && optionsShoud.length > 0) {
					for (int i = 0; i < (optionsShoud.length); i++) {
						// System.out.println("===Val=="+lstOptions.get(i)
						// +"==New=="+lstOptions.get(i).getText()
						// +"===clickCount=="+clickCount+"optionsShoud=="+optionsShoud[i]);
						if (i != 0) {

							if (i == 1 && i != 0) {

								if (subModuleName != null && ("Groups".equals(subModuleName)
										|| "Templates".equals(subModuleName) || "Campaigns".equals(subModuleName)
										|| "Workflows".equals(subModuleName))) {
									fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
											".//*[@class='count']/a[" + (i) + "]"));
									counter = i;
								} else {
									fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
											".//*[@id='pageid']/a[" + (i) + "]/u"));
									counter = i;
								}
							} else if (i > 1 && i != 0) {
								if (subModuleName != null && ("Groups".equals(subModuleName)
										|| "Templates".equals(subModuleName) || "Campaigns".equals(subModuleName)
										|| "Workflows".equals(subModuleName))) {
									fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
											".//*[@class='count']/a[" + (i + 1) + "]"));
									counter = i + 1;
								} else {
									fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
											".//*[@id='pageid']/a[" + (i + 1) + "]/u"));
									counter = i + 1;
								}
							}
							foundRecordCount = pagingRecordsSales(driver, records, subModuleName).size();
							if (foundRecordCount > 0) {

								if (foundRecordCount == Integer.parseInt(optionsShoud[i])) {
									// System.out.println("found record
									// "+foundRecordCount+"= Test Case Pass for
									// the page "+(i));
									fc.utobj().printTestStep("" + foundRecordCount + " records for page = " + counter);
								} else {

									if (foundRecordCount <= Integer.parseInt(optionsShoud[i])) {
										// System.out.println("found record
										// "+foundRecordCount+"= Test Case Pass
										// for the Last page "+(i));
										fc.utobj().printTestStep(
												"" + foundRecordCount + " records for page = " + counter);
									} else {
										fc.utobj().throwsException("found record " + foundRecordCount
												+ "= Test Case Fail for the page Almost Last Page " + counter);
									}
								}
							} else {
								fc.utobj().throwsException(
										"" + foundRecordCount + " records for page " + counter + " Test case fails");
							}
							// Check for paging after sort function
							if (subModuleName != null && !"Groups".equals(subModuleName)
									&& !"Templates".equals(subModuleName) && !"Campaigns".equals(subModuleName)
									&& !"Workflows".equals(subModuleName)) {
								fc.utobj().clickElement(driver, driver
										.findElement(By.xpath(".//table[@class='summaryTblex']/tbody/tr[1]/td[2]/a")));
								foundRecordCount = pagingRecordsSales(driver, records, subModuleName).size();
								if (foundRecordCount > 0) {

									if (foundRecordCount == Integer.parseInt(optionsShoud[i])) {
										// System.out.println("found record
										// "+foundRecordCount+"= Test Case Pass
										// for the page "+(i));
										fc.utobj().printTestStep(
												"After Sort record " + foundRecordCount + " for the page " + counter);
									} else {

										if (foundRecordCount <= Integer.parseInt(optionsShoud[i])) {
											// System.out.println("found record
											// "+foundRecordCount+"= Test Case
											// Pass for the Last page "+(i));
											fc.utobj().printTestStep("After Sort record " + foundRecordCount
													+ " for the page " + counter);
										} else {
											fc.utobj().throwsException("After Sort record " + foundRecordCount
													+ "= Test Case Fail for the page Almost Last Page " + counter);
										}

									}
								} else {
									fc.utobj().throwsException("" + foundRecordCount + " records for page " + counter
											+ " Test case fails on click sort");
								}
								//
							}
						}
					}
				}

			} catch (Exception e) {
				e.printStackTrace();
			}

			try {
				//// fc.utobj().printTestStep(testCaseId, "Reset our page to the
				//// page 1 to check the results Per Page verification.");

				if (subModuleName != null && ("Groups".equals(subModuleName) || "Templates".equals(subModuleName)
						|| "Campaigns".equals(subModuleName) || "Workflows".equals(subModuleName))) {
					fc.utobj().clickElement(driver,
							fc.utobj().getElementByXpath(driver, ".//*[@class='print-active-name']/a[1]"));
					fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
							".//*[@id='siteMainTable']//*[@class='full-width']/div/div[3]//*[@class='dropdown-list direction']/li[3]/a[1]"));

				} else {
					fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='pageid']/a[1]/u"));
					fc.utobj().selectDropDown(driver, fc.utobj().getElementByID(driver, "resultsPerPage"), "100");
				}

				foundRecordCount = pagingRecordsSales(driver, records, subModuleName).size();
				if (foundRecordCount > 0) {

				}
				if (foundRecordCount <= 100) {
					// System.out.println("found record in Result Per page all
					// "+foundRecordCount+" test case passed at View Per Page");
					fc.utobj().printTestStep("View Per Page " + foundRecordCount + " records.");

				} else {
					if (foundRecordCount > 100) {
						fc.utobj().throwsException("View Per Page " + foundRecordCount + " records.");
					}
				}
				// Check for paging after click on Sort function

				if (subModuleName != null && ("Groups".equals(subModuleName) || "Templates".equals(subModuleName)
						|| "Campaigns".equals(subModuleName) || "Workflows".equals(subModuleName))) {
					fc.utobj().clickElement(driver, driver.findElement(
							By.xpath(".//table[@class='tbl-style tbl-hover tbl-sticky-header']/thead/tr/th[2]/a")));
				} else {
					fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
							".//table[@class='summaryTblex']/tbody/tr[1]/td[2]/a"));
				}
				foundRecordCount = pagingRecordsSales(driver, records, subModuleName).size();
				if (foundRecordCount > 0) {

				}
				if (foundRecordCount <= 100) {
					// System.out.println("found record in Result Per page all
					// "+foundRecordCount+" test case passed at View Per Page");
					fc.utobj().printTestStep("After Sort found record in Result Per page  " + foundRecordCount
							+ " test case passed at View Per Page");

				} else {
					if (foundRecordCount > 100) {
						fc.utobj().throwsException("View Per Page " + foundRecordCount + " records.");
					}
				}

			} catch (Exception E) {
				// System.out.println("resultsPerPage Field Not Found!");
				fc.utobj().printTestStep("View Per Page  Field Not Found! in " + subModuleName);

				// totalRecordCount = 0;
			}

			if (subModuleName != null && "FDD".equals(subModuleName)) {
				try {
					fc.utobj().clickLink(driver, "Show All");
					// System.out.println("totalRecordCount===in show
					// all"+totalRecordCount);
					fc.utobj().printTestStep("Total in show all " + subModuleName + " = " + totalRecordCount);
					foundRecordCount = pagingRecordsSales(driver, records, subModuleName).size();
					if (foundRecordCount > 0) {

					}
					if (totalRecordCount == foundRecordCount) {
						// System.out.println("found record in show all
						// "+foundRecordCount+" test case passed");
						fc.utobj().printTestStep("found record in show all " + foundRecordCount + " test case passed");
					} else {
						if (foundRecordCount > 0) {
							if (totalRecordCount != foundRecordCount) {
								fc.utobj().throwsException(
										"Show All " + foundRecordCount + " records not working correct.");
							}
						}
					}
					// System.out.println("found record "+foundRecordCount+" in
					// "+lstOptions.get(i).getText()+" th Page");

				} catch (Exception E) {
					// System.out.println("Show All Field Not Found!");
					fc.utobj().printTestStep("Show All field not found!");
					// totalRecordCount = 0;
				}
			}

		} else {
			// System.out.println("Pagination Not Exists");
			fc.utobj().printTestStep("Pagination not exists in " + subModuleName);
		}

		// sortedList
		try {
			boolean isSorttable = sortedList(driver, records, subModuleName);
			if (isSorttable == true) {
				fc.utobj().printTestStep("Sorting function working for  " + subModuleName);
			} else {
				fc.utobj().printTestStep("Sorting function not Working for  " + subModuleName + " OR not applicable.");
			}

		} catch (Exception e) {
			// e.printStackTrace();
			fc.utobj().printTestStep("Eception in Sorting function which Working for  " + subModuleName);
		}

		// End sortedList

		fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
	}

	public boolean sortedList(WebDriver driver, List<WebElement> records, String subModuleName) {

		boolean isSorttable = false;
		try {
			records = pagingRecordsSales(driver, records, subModuleName);
			ArrayList<String> obtainedList = new ArrayList<>();
			ArrayList<String> sortedList = new ArrayList<>();
			List<WebElement> elementList = records; // driver.findElements(By.xpath(YourLocator));
			for (WebElement we : elementList) {
				obtainedList.add(we.getText());
			}

			if (subModuleName != null && !"Groups".equals(subModuleName) && !"Templates".equals(subModuleName)
					&& !"Campaigns".equals(subModuleName) && !"Workflows".equals(subModuleName)) {
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//table[@class='summaryTblex']/tbody/tr[1]/td[2]//a[not(*)]"));
				records = pagingRecordsSales(driver, records, subModuleName);
				for (WebElement we : records) {
					sortedList.add(we.getText());
				}
			}
			try {
				if (sortedList != null) {
					for (int i = 0; i < sortedList.size(); i++) {
						if (sortedList.get(0) != obtainedList.get(0)) {
							isSorttable = true;
							break;
						}
					}
				}

			} catch (Exception e) {
				isSorttable = false;

			}

		} catch (Exception e) {
			isSorttable = false;
			// e.printStackTrace();
		}
		return isSorttable;
	}

	public List<WebElement> pagingRecordsSales(WebDriver driver, List<WebElement> records, String subModuleName) {

		if (subModuleName == null)
			subModuleName = "LeadSummary";
		List<WebElement> listofElements = null;
		int rc = 0;
		if (subModuleName != null && "LeadSummary".equals(subModuleName)) {
			listofElements = driver.findElements(By.xpath(".//*[@id='containsImage']/tbody/tr/td/span/a"));
		} else if (subModuleName != null && "Tasks".equals(subModuleName)) {
			listofElements = driver.findElements(
					By.xpath(".//table[@class='summaryTblex']/tbody/tr/td[@class='botBorder colPadding'][2]/a"));
		} else if (subModuleName != null && "BrokersSummary".equals(subModuleName)) {
			listofElements = driver.findElements(
					By.xpath(".//table[@class='summaryTblex']/tbody/tr/td[@class='botBorder colPadding'][2]/a"));
		} else if (subModuleName != null && "Sites".equals(subModuleName)) {
			listofElements = driver.findElements(By.xpath(
					".//*[@id='siteMainTable']/tbody/tr/td/table[3]/tbody/tr[1]/td/form/table/tbody/tr[2]/td/table/tbody/tr/td[@class='botBorder colPadding']/a"));
		} else if (subModuleName != null && "FDD".equals(subModuleName)) {
			listofElements = driver.findElements(By.xpath(
					".//*[@id='siteMainTable']/tbody/tr/td/table/tbody/tr[5]/td[2]/table/tbody/tr/td/table[1]/tbody/tr/td[1]/a"));
		} else if (subModuleName != null && "MailMerge".equals(subModuleName)) {
			listofElements = driver.findElements(By.xpath(
					".//*[@id='siteMainTable']/tbody/tr/td/table[2]/tbody/tr[1]/td[2]/div/table/tbody/tr/td/table/tbody/tr/td[@class='botBorder colPadding']/a"));
		} else if (subModuleName != null && ("Groups".equals(subModuleName))) {
			listofElements = driver
					.findElements(By.xpath(".//div[@class='full-width']/table/tbody/tr/td[@class='ellipsis'][2]"));
		} else if (subModuleName != null && ("Templates".equals(subModuleName))) {
			listofElements = driver.findElements(
					By.xpath(".//div[@class='full-width']/div[2]/table/tbody/tr/td[@class='ellipsis'][1]/a"));
		} else if (subModuleName != null && ("Campaigns".equals(subModuleName))) {
			listofElements = driver.findElements(
					By.xpath(".//div[@class='full-width']/div[2]/table/tbody/tr/td[@class='ellipsis'][2]/a"));
		} else if (subModuleName != null && ("Workflows".equals(subModuleName))) {
			listofElements = driver.findElements(
					By.xpath(".//div[@class='full-width']/div[2]/table/tbody/tr/td[@class='ellipsis'][1]/a"));
		}

		rc = listofElements.size();
		return listofElements;

	}
}
