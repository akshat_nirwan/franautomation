package com.builds.test.fs;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.uimaps.campaigncentercrm.CRMCampaignCenterCampaignsPage;
import com.builds.uimaps.fs.FSBrokersPage;
import com.builds.uimaps.fs.FSCampaignCenterPage;
import com.builds.uimaps.fs.FSGroupsPage;
import com.builds.uimaps.fs.FSLeadSummaryAddLeadPage;
import com.builds.uimaps.fs.LeadSummaryUI;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class FSQuickLinksPageTest {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "sales1" })
	@TestCase(createdOn = "2017-10-06", updatedOn = "2017-10-06", testCaseId = "TC_Sales_QuickLinks_001", testCaseDescription = "Verify the Add Broker through Quick link")
	private void checkQuickLinkPosition() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("sales",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		LeadSummaryUI pobj = new LeadSummaryUI(driver);
		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Admin > Configuration > Configure Quick Links");
			fc.adminpage().adminPage(driver);
			fc.adminpage().adminConfigureQuickLinks(driver);
			fc.utobj().printTestStep("Sales > Change the sequence of menu.");
			fc.utobj().dragAndDrop(driver, fc.utobj().getElementByXpath(driver, ".//td[contains(text(),'Add Site')]"),
					fc.utobj().getElementByXpath(driver, ".//td[contains(text(),'Add Broker')]"));
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.sales().sales_common().fsModule(driver);
			fc.utobj().clickElement(driver, pobj.showQucikLinkBtn);
			boolean isPositionChanged = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[@id='Sales']/ancestor::tr/following-sibling::tr[1]/td/a[contains(text(),'Add Broker')]");
			if (isPositionChanged == false) {
				fc.utobj().throwsException("Quick link position not changed");
			}
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}

	@Test(groups = { "sales" })
	@TestCase(createdOn = "2017-10-06", updatedOn = "2017-10-06", testCaseId = "TC_Sales_QuickLinks_002", testCaseDescription = "Verify the sequence of quick link is getting changed as per the configuration.")
	private void addBrokerQuickLink() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		FSBrokersPage pobj = new FSBrokersPage(driver);
		try {
			driver = fc.loginpage().login(driver);

			String brokerName = fc.utobj().generateTestData(dataSet.get("brokerName"));
			String agencyName = "None";

			fc.utobj().printTestStep("Quick link > Add Broker");
			fc.sales().sales_common().fsModule(driver);
			brokerName = addBrokerQuickLink(driver, brokerName, agencyName);
			fc.utobj().sendKeys(driver, pobj.brokerNameSearch, brokerName);
			fc.utobj().clickElement(driver, pobj.searchBtn);
			boolean isBrokerAdded = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + brokerName + "')]");
			if (isBrokerAdded == false) {
				fc.utobj().throwsException("Broker not getting added through quick link");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}

	@Test(groups = { "sales", "sales_failed" , "TC_Sales_QuickLinks_003" }) // 
	@TestCase(createdOn = "2017-10-10", updatedOn = "2017-10-10", testCaseId = "TC_Sales_QuickLinks_003", testCaseDescription = "Verify the Add Campaign through Quick link.")
	private void addCampaignQuickLink() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		FSBrokersPage pobj = new FSBrokersPage(driver);
		try {
			driver = fc.loginpage().login(driver);

			AddLeadFromAllSources addLead = new AddLeadFromAllSources();
			LeadSummaryUI pobj2 = new LeadSummaryUI(driver);
			FSCampaignCenterPage campaignCenterObj = new FSCampaignCenterPage(driver);
			CRMCampaignCenterCampaignsPage camObj = new CRMCampaignCenterCampaignsPage(driver);
			FSGroupsPage groupsPage = new FSGroupsPage(driver);
			Sales fsMod = new Sales();
			FSSearchPageTest p3 = new FSSearchPageTest();

			String campaignName = fc.utobj().generateTestData(dataSet.get("campaignName"));
			String emailId = "salesautomation@staffex.com";
			String camapignDescription = fc.utobj().generateTestData(dataSet.get("camapignDescription"));
			String accessibility = dataSet.get("accessibility");
			String templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String emailSubject = fc.utobj().generateTestData(dataSet.get("emailSubject"));

			fc.utobj().printTestStep("Add a lead with different division");
			String firstName = fc.utobj().generateTestData(dataSet.get("FirstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("LastName"));
			String country = "USA";
			String state = "Alabama";
			String leadOwner = "FranConnect Administrator";
			String leadSourceCategory = "Import";
			String leadSourceDetails = "None";

			Map<String, String> leadInfo = new HashMap<String, String>();
			leadInfo.put("firstName", firstName);
			leadInfo.put("lastName", lastName);
			leadInfo.put("country", country);
			leadInfo.put("state", state);
			leadInfo.put("email", emailId);
			leadInfo.put("leadOwner", leadOwner);
			leadInfo.put("leadSourceCategory", leadSourceCategory);
			leadInfo.put("leadSourceDetails", leadSourceDetails);
			addLead.addLeadFromSystem(driver, leadInfo);

			fc.utobj().printTestStep(" Go to Quick link > Add Campaign");
			try {
				fc.utobj().clickElement(driver, pobj2.notificationBarShow);
			} catch (Exception e) {
			}
			fc.utobj().clickElement(driver, pobj2.showQucikLinkBtn);
			fc.utobj().clickElement(driver, pobj.addCampaignQuickLinkBtn);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);

			fc.utobj().sendKeys(driver, campaignCenterObj.campaignName, campaignName);
			fc.utobj().sendKeys(driver, campaignCenterObj.description, camapignDescription);
			fc.utobj().selectDropDown(driver, campaignCenterObj.campaignAccessibility, accessibility);
			// fc.utobj().selectDropDown(driver, pobj2.selectCategory,
			// categoryName);
			// fc.utobj().clickElement(driver, pobj2.statusDrivenTypeCampaign);
			fc.utobj().clickElement(driver, camObj.startBtn);
			fc.utobj().switchFrameToDefault(driver);

			// Code Your Template
			String plainTextEditor = fc.utobj().generateTestData("plainTextEditor");
			fc.utobj().clickElement(driver, campaignCenterObj.codeYourOwn);
			fc.utobj().sendKeys(driver, campaignCenterObj.templateNameTitle, templateName);
			fc.utobj().sendKeys(driver, campaignCenterObj.emailSubject, emailSubject);

			fc.utobj().clickElement(driver, campaignCenterObj.CodeOwnplainTextEmailType);
			fc.utobj().sendKeys(driver, campaignCenterObj.textAreaPlainText, plainTextEditor);

			fc.utobj().clickElement(driver, campaignCenterObj.saveAndContinue);
			fc.utobj().clickElement(driver, campaignCenterObj.fiterRecipients);

			fc.utobj().clickElement(driver, groupsPage.availableFields);
			fc.utobj().sendKeys(driver, groupsPage.selectCriteria_availableFields_Input, "First Name");
			fc.utobj().clickElement(driver, groupsPage.selectCriteria_availableFields_searchButton);
			fc.utobj().clickElement(driver, groupsPage.firstNameAvlbFields);
			
			fc.utobj().sendKeys(driver, groupsPage.selectCriteria_availableFields_Input, "Last Name");
			fc.utobj().clickElement(driver, groupsPage.selectCriteria_availableFields_searchButton);
			fc.utobj().clickElement(driver, groupsPage.lastNameAvlbFields);
			
			fc.utobj().sendKeys(driver, groupsPage.containsFirstName, firstName);
			fc.utobj().sendKeys(driver, groupsPage.containsLastName, lastName);

			fc.utobj().clickElement(driver, groupsPage.applyFilter);
			fc.utobj().clickElement(driver, groupsPage.continueButton);
			fc.utobj().clickElement(driver, campaignCenterObj.LaunchCampaignBtn);

			boolean isSchelduled = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//span[contains(text(),'has been scheduled successfully')]");
			boolean isCampaignNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//span[contains(text(),'" + campaignName + "')]");

			if (isSchelduled == false || isCampaignNamePresent == false) {
				fc.utobj().throwsException("Congratulation message not verified");
			}

			fc.utobj().clickElement(driver, campaignCenterObj.veiwCampaings);

			boolean isCampaignAdded = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + campaignName + "')]");
			if (isCampaignAdded == false) {
				fc.utobj().throwsException("Campaign not verified at veiw campaign page");
			}

			fsMod.leadManagement(driver);
			p3.searchByLeadName(driver, firstName, lastName);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[contains(text(),'" + firstName + " " + lastName + "')]"));
			boolean isCampaignAssociated = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[@id='for_bar']//td[contains(text(),'Promotional Campaign  : ')]/following-sibling::td[contains(text(),'"
							+ campaignName + "')]");
			if (isCampaignAssociated == false) {
				fc.utobj().throwsException("Campaign association with lead not verified");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			// e.printStackTrace();
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}

	@Test(groups = { "sales","sales_failed"})
	@TestCase(createdOn = "2017-10-10", updatedOn = "2017-10-10", testCaseId = "TC_Sales_QuickLinks_004", testCaseDescription = "Verify the Add Lead")
	private void addleadQuickLink() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		FSBrokersPage pobj = new FSBrokersPage(driver);
		try {
			driver = fc.loginpage().login(driver);

			LeadSummaryUI pobj2 = new LeadSummaryUI(driver);

			String firstName = fc.utobj().generateTestData(dataSet.get("FirstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("LastName"));
			String leadState = "Alabama";
			String leadCountry = "USA";
			String leadSourceCategory = "Import";
			String leadSourceDetails = "None";
			String emailId = "salesautomation@staffex.com";
			String leadOwner = "FranConnect Administrator";

			fc.utobj().printTestStep("Click on Quick Link > Add lead");
			try {
				fc.utobj().clickElement(driver, pobj2.notificationBarShow);
			} catch (Exception e) {
			}
			fc.utobj().clickElement(driver, pobj2.showQucikLinkBtn);
			fc.utobj().clickElement(driver, pobj2.addLeadQuickLinkBtn);

			FSLeadSummaryAddLeadPage leadAddPage = new FSLeadSummaryAddLeadPage(driver);

			fc.utobj().sendKeys(driver, leadAddPage.firstName, firstName);
			fc.utobj().sendKeys(driver, leadAddPage.lastName, lastName);
			fc.utobj().sendKeys(driver, leadAddPage.country, leadCountry);
			fc.utobj().sendKeys(driver, leadAddPage.state, leadState);
			fc.utobj().sendKeys(driver, leadAddPage.emailID, emailId);
			fc.utobj().selectDropDownByVisibleText(driver, leadAddPage.leadOwnerID, leadOwner);
			fc.utobj().selectDropDownByVisibleText(driver, leadAddPage.leadSource2ID, leadSourceCategory);
			fc.utobj().selectDropDownByVisibleText(driver, leadAddPage.leadSource3ID, leadSourceDetails);
			fc.utobj().clickElement(driver, leadAddPage.save);

			boolean verifyFirstName = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text(),'First Name')]/following-sibling::td[contains(text(),'" + firstName + "')]");
			if (verifyFirstName == false) {
				fc.utobj().throwsSkipException("Lead first name Not verified ");
			}

			boolean verifylastName = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text(),'Last Name')]/following-sibling::td[contains(text(),'" + lastName + "')]");
			if (verifylastName == false) {
				fc.utobj().throwsSkipException("Lead first name Not verified ");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}

	@Test(groups = { "sales", "salessqiuckAddGrop" })
	@TestCase(createdOn = "2017-10-10", updatedOn = "2017-10-10", testCaseId = "TC_Sales_QuickLinks_005", testCaseDescription = "Verify the Add Group")
	private void addGroupQuickLink() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			LeadSummaryUI pobj2 = new LeadSummaryUI(driver);
			FSGroupsPage pobj = new FSGroupsPage(driver);
			FSGroupsPageTest groupPage = new FSGroupsPageTest();

			String groupName = fc.utobj().generateTestData(dataSet.get("Groupname"));

			fc.utobj().printTestStep("Click on Quick Link > Add Gorup");
			try {
				fc.utobj().clickElement(driver, pobj2.notificationBarShow);
			} catch (Exception e) {
			}
			fc.utobj().clickElement(driver, pobj2.showQucikLinkBtn);
			fc.utobj().clickElement(driver, pobj2.addGroupQuickLinkBtn);

			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);

			fc.utobj().sendKeys(driver, pobj.groupName, groupName);
			fc.utobj().sendKeys(driver, pobj.groupDescription, groupName + " Description");
			fc.utobj().clickElement(driver, pobj.addBtn);

			groupPage.searchGroup(driver, groupName);
			fc.utobj().assertSingleLinkText(driver, groupName);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}

	public String addBrokerQuickLink(WebDriver driver, String brokerName, String agencyName) throws Exception {
		FSBrokersPage pobj = new FSBrokersPage(driver);
		LeadSummaryUI pobj2 = new LeadSummaryUI(driver);
		try {
			fc.utobj().clickElement(driver, pobj2.notificationBarShow);
		} catch (Exception e) {
		}
		fc.utobj().clickElement(driver, pobj2.showQucikLinkBtn);
		fc.utobj().clickElement(driver, pobj.addBrokersQuickLinkBtn);
		try {
			fc.utobj().selectDropDownByIndex(driver, pobj.brokerType, 1);
		} catch (Exception e) {
		}
		fc.utobj().sendKeys(driver, pobj.firstName, brokerName);
		fc.utobj().sendKeys(driver, pobj.lastName, brokerName);
		fc.utobj().sendKeys(driver, pobj.emailID, "salesautoamtion@franqa.com");
		fc.utobj().selectDropDown(driver, pobj.brokerAgencyId, agencyName);
		fc.utobj().clickElement(driver, pobj.submitBtn);
		return brokerName + " " + brokerName;
	}

}
