package com.builds.test.fs;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Optional;

import com.builds.uimaps.fs.SearchUI;
import com.builds.utilities.FranconnectUtil;

public class FSSearchPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	public boolean searchTabByAdavaceSearch(WebDriver driver, String tabName) throws Exception {
		Sales p1 = new Sales();
		p1.search(driver);
		SearchUI pobj = new SearchUI(driver);
		fc.utobj().clickElement(driver, pobj.advancesearch);
		// fc.utobj().clickElement(driver,
		// fc.utobj().getElementByXpath(driver,".//td[contains(text(),'"+tabName+"')]/input")));
		// fc.utobj().clickElement(driver, pobj.searchData);
		return fc.utobj().assertPageSource(driver, tabName);
	}

	public void checkAdavaceSearchSubTab(WebDriver driver, String tabName) throws Exception {
		Sales p1 = new Sales();
		p1.search(driver);
		SearchUI pobj = new SearchUI(driver);
		fc.utobj().clickElement(driver, pobj.advancesearch);
		fc.utobj().clickElement(driver,
				fc.utobj().getElementByXpath(driver, ".//td[contains(text(),'" + tabName + "')]/input"));
		fc.utobj().clickElement(driver, pobj.searchData);
		// return fc.utobj().assertPageSource(driver, tabName);
	}

	public boolean searchTabByExport(WebDriver driver, String tabName) throws Exception {
		Sales p1 = new Sales();
		p1.exportPage(driver);
		return fc.utobj().assertPageSource(driver, tabName);
	}

	public void searchByLeadName(WebDriver driver, String firstName, String lastName) throws Exception {
		Sales p1 = new Sales();

		p1.search(driver);
		SearchUI pobj = new SearchUI(driver);
		fc.utobj().sendKeys(driver, pobj.firstName, firstName);
		fc.utobj().sendKeys(driver, pobj.lastName, lastName);
		fc.utobj().clickElement(driver, pobj.search);
	}

	public void searchByGroupName(WebDriver driver, String groupName) throws Exception {
		Sales p1 = new Sales();

		p1.search(driver);
		SearchUI pobj = new SearchUI(driver);
		fc.utobj().selectValFromMultiSelect(driver, pobj.searchGrpDrpDwn, groupName);
		fc.utobj().clickElement(driver, pobj.search);
	}

	public void searchAdvanceByGroupName(WebDriver driver, String groupName) throws Exception {

		SearchUI pobj = new SearchUI(driver);
		fc.utobj().selectValFromMultiSelectWithoutReset(driver, pobj.advSearchGrpDrpDwn, groupName);
		fc.utobj().clickElement(driver, pobj.viewData);
	}

	public void archivedLeadSearchByName(WebDriver driver, @Optional String firstName, @Optional() String lastName)
			throws Exception {

		Sales p1 = new Sales();
		p1.search(driver);
		SearchUI pobj = new SearchUI(driver);
		fc.utobj().clickElement(driver, pobj.archivedleadRadio);

		fc.utobj().sendKeys(driver, pobj.firstName, firstName);
		fc.utobj().sendKeys(driver, pobj.lastName, lastName);
		fc.utobj().clickElement(driver, pobj.search);
	}

	public void primaryInfoBasedSearch(WebDriver driver, Map<String, String> leadInfo) throws Exception {
		SearchUI pobj = new SearchUI(driver);

		fc.utobj().sendKeys(driver, pobj.requestDateFrom, leadInfo.get(""));
		fc.utobj().sendKeys(driver, pobj.requestDateTo, leadInfo.get(""));
		fc.utobj().sendKeys(driver, pobj.firstName, leadInfo.get(""));
		fc.utobj().sendKeys(driver, pobj.lastName, leadInfo.get(""));
		fc.utobj().sendKeys(driver, pobj.address, leadInfo.get(""));
		fc.utobj().sendKeys(driver, pobj.city, leadInfo.get(""));
		fc.utobj().selectValFromMultiSelect(driver, pobj.countryListBox, leadInfo.get(""));
		fc.utobj().sendKeys(driver, pobj.zip, leadInfo.get(""));
		fc.utobj().selectValFromMultiSelect(driver, pobj.countryListBox, leadInfo.get(""));
		fc.utobj().selectValFromMultiSelect(driver, pobj.countryListBox, leadInfo.get(""));
		fc.utobj().sendKeys(driver, pobj.phone, leadInfo.get(""));
		fc.utobj().sendKeys(driver, pobj.emailID, leadInfo.get(""));
		fc.utobj().selectValFromMultiSelect(driver, pobj.countryListBox, leadInfo.get(""));
		fc.utobj().selectValFromMultiSelect(driver, pobj.countryListBox, leadInfo.get(""));
		fc.utobj().sendKeys(driver, pobj.statusChangeDateFrom, leadInfo.get(""));
		fc.utobj().sendKeys(driver, pobj.statusChangeDateTo, leadInfo.get(""));
		fc.utobj().selectValFromMultiSelect(driver, pobj.countryListBox, leadInfo.get(""));
		fc.utobj().selectValFromMultiSelect(driver, pobj.countryListBox, leadInfo.get(""));
		fc.utobj().selectValFromMultiSelect(driver, pobj.countryListBox, leadInfo.get(""));
		fc.utobj().selectValFromMultiSelect(driver, pobj.countryListBox, leadInfo.get(""));
		fc.utobj().selectValFromMultiSelect(driver, pobj.countryListBox, leadInfo.get(""));
		fc.utobj().sendKeys(driver, pobj.nextCallDateFrom, leadInfo.get(""));
		fc.utobj().sendKeys(driver, pobj.nextCallDateTo, leadInfo.get(""));

		fc.utobj().clickElement(driver, pobj.search);
	}

}
