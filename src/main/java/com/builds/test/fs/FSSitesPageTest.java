package com.builds.test.fs;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.common.CorporateUser;
import com.builds.uimaps.fs.FSSitesPage;
import com.builds.uimaps.fs.SearchUI;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class FSSitesPageTest {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "sales" , "TC_FC_Sites_001" }) // Fixed : Verified // Akshat
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FC_Sites_001", testCaseDescription = "Add Sites in Site Tab of Sales Module")
	public void addSites() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String siteName = fc.utobj().generateTestData(dataSet.get("siteName"));

		try {
			driver = fc.loginpage().login(driver);
			FSSitesPage pobj = new FSSitesPage(driver);
			Sales p1 = new Sales();
			fc.commonMethods().getModules().clickSalesModule(driver);
			p1.sites(driver);
			fc.utobj().printTestStep("Add a Site");
			fc.utobj().clickElement(driver, pobj.addSitesBtn);
			fc.utobj().sendKeys(driver, pobj.locationTitle, siteName);
			fc.utobj().sendKeys(driver, pobj.city, "test");
			fc.utobj().selectDropDown(driver, pobj.stateID, 2);
			fc.utobj().clickElement(driver, pobj.submitButton);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addSites(WebDriver driver, String siteName) throws Exception {
		String country = "USA";
		String city = "NYC";
		String state = "New York";

		FSSitesPage pobj = new FSSitesPage(driver);
		Sales p1 = new Sales();
		p1.sites(driver);
		fc.utobj().clickElement(driver, pobj.addSitesBtn);
		fc.utobj().sendKeys(driver, pobj.locationTitle, siteName);
		fc.utobj().selectDropDown(driver, pobj.countryID, country);
		fc.utobj().sendKeys(driver, pobj.city, city);
		fc.utobj().selectDropDown(driver, pobj.stateID, state);
		fc.utobj().clickElement(driver, pobj.submitButton);

		/*
		 * List<String> listItems = null; listItems.add(siteName);
		 * listItems.add(country); listItems.add(city); listItems.add(state);
		 * 
		 * boolean isSiteSaved =
		 * fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);
		 */
		p1.sites(driver);

		return siteName;
	}

	@Test(groups = { "abc" })
	public void viewSites() throws Exception {
		Reporter.log("TC_FC_Sites_002 : Sales Module > Add / View Sites.\n");
		Reporter.log("###################################################################");
		String testCaseId = "TC_FC_Sites_002";

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String siteName = fc.utobj().generateTestData(dataSet.get("siteName"));

		try {
			driver = fc.loginpage().login(driver);
			siteName = addSites(driver, siteName);
			fc.utobj().inPageSource(driver, siteName);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "commonModule", "add", "Add Lead", "fssanity1" })
	public String modifySites() throws Exception {
		Reporter.log("TC_FC_Sites_003 : Sales Module > Add Sites > Modify Sites\n");
		Reporter.log("###################################################################");
		String testCaseId = "TC_FC_Sites_003";

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String siteName = dataSet.get("siteName");

		try {
			driver = fc.loginpage().login(driver);
			siteName = addSites(driver, siteName);
			FSSitesPage pobj = new FSSitesPage(driver);
			fc.utobj().clickLink(driver, "Sites");
			fc.utobj().selectDropDownByVisibleText(driver, pobj.locationTypeSearch, "New Available");
			fc.utobj().clickElement(driver, pobj.searchBtn);

			List<WebElement> list = pobj.siteListing;
			fc.utobj().actionImgOption(driver, siteName, "Modify");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String updatedName = pobj.locationTitle.getAttribute("value") + fc.utobj().generateRandomChar();
			fc.utobj().sendKeys(driver, pobj.locationTitle, updatedName);
			fc.utobj().clickElement(driver, pobj.submitButton);
			fc.utobj().clickElement(driver, pobj.okBtn);
			fc.utobj().assertNoSingleLinkText(driver, updatedName);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
		return siteName;
	}

	@Test(groups = { "abc" })
	public void associatesiteWithLead() throws Exception {
		Reporter.log("TC_FC_Sites_005 : Sales Module > Add Sites > Add lead > Associate Site > Remove Site \n");
		Reporter.log("###################################################################");
		String testCaseId = "TC_FC_Sites_005";

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String siteName1 = fc.utobj().generateTestData(dataSet.get("siteName"));
		String siteName2 = fc.utobj().generateTestData(dataSet.get("siteName"));

		try {
			driver = fc.loginpage().login(driver);
			siteName1 = addSites(driver, siteName1);
			siteName2 = addSites(driver, siteName2);

			fc.utobj().clickLink(driver, "Sites");

			try {
				fc.utobj().clickLink(driver, "Show All");
			} catch (Exception e) {

			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales","sales_failed" , "TC_Sales_Sites_002" }) // Verified : OK // Akshat
	@TestCase(createdOn = "2017-10-07", updatedOn = "2017-10-07", testCaseId = "TC_Sales_Sites_002", testCaseDescription = "Verify Lock option should come with site & its functionality.")
	public void checkLockOptionSites() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FSSitesPage pobj = new FSSitesPage(driver);
			Sales p1 = new Sales();
			String siteName = fc.utobj().generateTestData(dataSet.get("siteName"));

			// p1.sites(driver);
			fc.adminpage().adminConfigureSiteClearance(driver);
			fc.utobj().clickElement(driver, pobj.enableSiteClearanceYes);
			fc.utobj().clickElement(driver, pobj.submitButton);

			fc.utobj().printTestStep("Sales > Sites > Add Sites > Action menu > Click on lock > Site gets locked");

			fc.sales().sales_common().fsModule(driver);
			siteName = addSites(driver, siteName);
			fc.utobj().actionImgOption(driver, siteName, "Lock this Site");
			fc.utobj().acceptAlertBox(driver);
			String src = driver
					.findElement(By.xpath(".//a[contains(text(),'" + siteName + "')]/following-sibling::img"))
					.getAttribute("src");
			if (!src.contains("goldLock.gif")) {
				fc.utobj().throwsException("Lock icon not visible");
			}
			fc.utobj().printTestStep("Check that the 'Unlock this Site' option is coming now.");
			try {
				fc.utobj().actionImgOption(driver, siteName, "Unlock this Site");
				fc.utobj().dismissAlertBox(driver);
			} catch (Exception e) {
				fc.utobj().throwsException("'Unlock this Site' option not comming in side action");
			}
			fc.utobj().printTestStep(
					" And option to Modify, Archive, Associate Lead / Store, Lock this Site are not coming");

			try {
				fc.utobj().actionImgOption(driver, siteName, "Modify");
				fc.utobj().sendKeys(driver, pobj.locationTitle, siteName);
				fc.utobj().throwsException("Modify option still comming after locking the site in action menu");
			} catch (Exception e) {
			}
			try {
				fc.utobj().actionImgOption(driver, siteName, "Archive");
				fc.utobj().acceptAlertBox(driver);
				fc.utobj().throwsException("Archive option still comming after locking the site in action menu");
			} catch (Exception e) {
			}
			try {
				fc.utobj().actionImgOption(driver, siteName, "Associate Lead / Store");
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().throwsException(
						"'Associate Lead / Store' option still comming after locking the site in action menu");
			} catch (Exception e) {
			}
			try {
				fc.utobj().actionImgOption(driver, siteName, "Lock this Site");
				fc.utobj().acceptAlertBox(driver);
				fc.utobj()
						.throwsException("'Lock this Site' option still comming after locking the site in action menu");
			} catch (Exception e) {
			}
			fc.utobj().printTestStep(
					"Add another User > Login with this user > Check with other user > Only Add Remarks option should come for this user.");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "salesautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = corpTest.createDefaultUser(driver, corpUser);
			fc.home_page().logout(driver);
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());
			fc.sales().sales_common().fsModule(driver);
			p1.sites(driver);
			fc.utobj().actionImgOption(driver, siteName, "Add Remark");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String remarks = fc.utobj().generateTestData("Test Remarks");
			fc.utobj().sendKeys(driver, pobj.siteRemarkadd, remarks);
			fc.utobj().clickElement(driver, pobj.addRemark);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().printTestStep("Modified remarks should be displayed");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[contains(text(),'" + siteName + "')]"));
			boolean isRemarkAdded = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + remarks + "')]");
			if (isRemarkAdded == false) {
				fc.utobj().throwsException("Reamrk Not shown");
			}
			fc.utobj().printTestStep(" Delete > Remarks > Remarks should be deleted.");
			fc.utobj().actionImgOption(driver, remarks, "Delete");
			fc.utobj().acceptAlertBox(driver);
			boolean isRemarkDeleted = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + remarks + "')]");
			if (isRemarkDeleted == true) {
				fc.utobj().throwsException("Remark Not deleted");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			// e.printStackTrace();
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales","sales_failed" })
	@TestCase(createdOn = "2017-11-11", updatedOn = "2017-11-11", testCaseId = "TC_Solr_Search_Sites_003", testCaseDescription = "Verify Lock option should come with site & its functionality.")
	public void checkSolrSearchforSites() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FSSitesPage pobj = new FSSitesPage(driver);
			SearchUI search_page = new SearchUI(driver);

			String sitePre = fc.utobj().generateTestData(dataSet.get("siteName"));
			String siteName1 = sitePre + dataSet.get("siteName1");
			String siteName2 = sitePre + dataSet.get("siteName2");
			String siteName3 = sitePre + dataSet.get("siteName3");
			String siteName4 = sitePre + dataSet.get("siteName4");
			String siteName5 = sitePre + dataSet.get("siteName5");
			String siteName6 = sitePre + dataSet.get("siteName6");
			String siteName7 = sitePre + dataSet.get("siteName7");
			String siteName8 = sitePre + dataSet.get("siteName8");
			// p1.sites(driver);
			fc.adminpage().adminConfigureSiteClearance(driver);
			fc.utobj().clickElement(driver, pobj.enableSiteClearanceYes);
			fc.utobj().clickElement(driver, pobj.submitButton);

			fc.utobj().printTestStep("Sales > Sites > Add Sites > Action menu > Click on lock > Site gets locked");
			fc.sales().sales_common().fsModule(driver);
			siteName1 = addSites(driver, siteName1);
			siteName2 = addSites(driver, siteName2);
			siteName3 = addSites(driver, siteName3);
			siteName4 = addSites(driver, siteName4);
			siteName5 = addSites(driver, siteName5);
			siteName6 = addSites(driver, siteName6);
			siteName7 = addSites(driver, siteName7);
			siteName8 = addSites(driver, siteName8);

			boolean isSearchTrue = false;
			boolean isVeiwAll = false;
			for (int i = 0; i < 3; i++) {
				if (isSearchTrue == false) {
					fc.utobj().sendKeys(driver, search_page.topSearchField, sitePre);

					if (i == 2) {

					}
					fc.utobj().clickEnterOnElement(driver, search_page.topSearchField);

					try {
						fc.utobj().moveToElement(driver, fc.utobj().getElementByXpath(driver,
								".//span/*[contains(text(),'" + siteName8 + "')]"));
						isSearchTrue = true;
					} catch (Exception e) {
					}
				}
			}
			if (isSearchTrue == false) {
				fc.utobj().throwsException("was not able to search Lead through Top Search");
			}
			// fc.utobj().moveToElement(driver,
			// fc.utobj().getElementByXpath(driver,".//span/*[contains(text(),'"+brokerName2+"')]")));
			// fc.utobj().moveToElement(driver,
			// fc.utobj().getElementByXpath(driver,".//span/*[contains(text(),'"+brokerName3+"')]")));
			// fc.utobj().moveToElement(driver,
			// fc.utobj().getElementByXpath(driver,".//span/*[contains(text(),'"+brokerName8+"')]")));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[@ng-click='viewAll()']"));

			isVeiwAll = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + siteName1 + "')]");
			isVeiwAll = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + siteName2 + "')]");
			isVeiwAll = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + siteName3 + "')]");
			isVeiwAll = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + siteName4 + "')]");
			isVeiwAll = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + siteName5 + "')]");
			isVeiwAll = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + siteName6 + "')]");
			isVeiwAll = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + siteName7 + "')]");
			isVeiwAll = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + siteName8 + "')]");
			if (isVeiwAll == false) {
				fc.utobj().throwsException("All elements not visible on clicking view all");
			}
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'" + siteName1 + "')]"));
			boolean isBrokerVerified = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + siteName1 + "')]");
			if (isBrokerVerified == false) {
				fc.utobj().throwsException("Site not verified on click at search");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

}
