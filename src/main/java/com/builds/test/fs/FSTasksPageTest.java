package com.builds.test.fs;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.uimaps.fs.FSTasksPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class FSTasksPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "sales", "sales","sales_failed" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FS_Tasks_001", testCaseDescription = "Add Task for a lead from Task Tab")
	public void addTask() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		String taskName = dataSet.get("taskName");
		String firstName = fc.utobj().generateTestData("lead");
		String lastName = fc.utobj().generateTestData("task");

		try {
			driver = fc.loginpage().login(driver);
			FSLeadSummaryPageTest lead = new FSLeadSummaryPageTest();
			fc.utobj().printTestStep("Add a lead");
			Map<String, String> leadInfo = lead.addLeadLeadSummaryWithLeadName(driver, config, firstName, lastName);
			fc.utobj().printTestStep("Log a Task for this lead");
			taskName = addTask(driver, taskName,
					leadInfo.get("firstName").concat(" ").concat(leadInfo.get("lastName")));
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addTask(WebDriver driver, String taskName, String leadName) throws Exception {

		// leadName = p1.addLeadLeadSummaryWithBasicInfo(driver,leadName);
		Sales p2 = new Sales();
		p2.tasks(driver);
		// cboxIframe
		taskName = taskName + fc.utobj().generateRandomNumber();
		FSTasksPage pobj = new FSTasksPage(driver);
		String windowHandle = driver.getWindowHandle();
		try {
			fc.utobj().clickElement(driver, pobj.addTaskLnk);

			fc.commonMethods().switch_cboxIframe_frameId(driver);

		} catch (Exception e) {
			fc.utobj().clickElement(driver, pobj.addTaskLnk);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
		}
		fc.utobj().sendKeys(driver, pobj.leadSearchName, leadName + " " + leadName);
		fc.utobj().clickElement(driver, pobj.searchBtn);

		try {
			fc.utobj().clickElement(driver, pobj.checkBox);
		} catch (Exception e) {
			fc.utobj().clickElement(driver, pobj.checkBoxCboxiframeAll);
		}

		fc.utobj().clickElement(driver, pobj.createTaskBtn);

		fc.utobj().sendKeys(driver, pobj.taskSubject, taskName);
		fc.utobj().clickElement(driver, pobj.addTaskBtn);
		driver.switchTo().window(windowHandle);

		boolean isShowFilter = false;

		try {
			fc.utobj().getElement(driver, pobj.hideFilterLnk).isDisplayed();
			fc.utobj().sendKeys(driver, pobj.taskSubject, taskName);
			isShowFilter = true;
		} catch (Exception e) {
			fc.utobj().clickElement(driver, pobj.showFilterLnk);
			fc.utobj().sendKeys(driver, pobj.taskSubject, taskName);
			isShowFilter = true;
		}

		if (isShowFilter = false) {
			try {
				fc.utobj().clickElement(driver, pobj.showFilterLnk);
				fc.utobj().sendKeys(driver, pobj.taskSubject, taskName);
			} catch (Exception e) {

			}
		}

		// fc.utobj().selectValFromMultiSelect(driver, pobj.taskStatusDrp,
		// "Select All");
		// fc.utobj().selectValFromMultiSelect(driver, pobj.viewDrp, "All");
		fc.utobj().clickElement(driver, pobj.searchBtn);
		fc.utobj().assertSingleLinkText(driver, taskName);
		return taskName;
	}

	@Test(groups = { "sales","sales_failed" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Modify Task From Action Menu", testCaseId = "TC_FS_Tasks_002")
	public void modifyTaskActionIcon() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		String taskName = dataSet.get("taskName");
		String leadName = dataSet.get("leadName");
		leadName = fc.utobj().generateTestData(leadName);

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Add a lead");
			FSLeadSummaryPageTest lead = new FSLeadSummaryPageTest();
			Map<String, String> leadInfo = lead.addLeadLeadSummaryWithLeadName(driver, config, leadName, leadName);
			fc.utobj().printTestStep("Log a Task");
			taskName = addTask(driver, taskName, leadInfo.get("firstName"));

			FSTasksPage pobj = new FSTasksPage(driver);
			String windowHandle = driver.getWindowHandle();
			fc.utobj().printTestStep("Modify Task");
			fc.utobj().singleActionIcon(driver, "Modify");

			fc.commonMethods().switch_cboxIframe_frameId(driver);

			String updatedName = taskName + fc.utobj().generateRandomChar();
			fc.utobj().sendKeys(driver, pobj.taskSubject, updatedName);
			fc.utobj().clickElement(driver, pobj.addTaskBtn);

			driver.switchTo().window(windowHandle);

			boolean isShowFilter = false;

			try {
				fc.utobj().getElement(driver, pobj.hideFilterLnk).isDisplayed();
				fc.utobj().sendKeys(driver, pobj.taskSubject, taskName);
				isShowFilter = true;
			} catch (Exception e) {
				fc.utobj().clickElement(driver, pobj.showFilterLnk);
				fc.utobj().sendKeys(driver, pobj.taskSubject, taskName);
				isShowFilter = true;
			}

			if (isShowFilter = false) {
				try {
					fc.utobj().clickElement(driver, pobj.showFilterLnk);
					fc.utobj().sendKeys(driver, pobj.taskSubject, taskName);
				} catch (Exception e) {

				}
			}

			fc.utobj().clickElement(driver, pobj.searchBtn);

			fc.utobj().assertSingleLinkText(driver, updatedName);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales","sales_failed" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Delete Task From Action Menu", testCaseId = "TC_FS_Tasks_003")
	public void deleteTaskActionIcon() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		String taskName = dataSet.get("taskName");
		String leadName = dataSet.get("leadName");
		leadName = fc.utobj().generateTestData(leadName);

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Add a lead");
			FSLeadSummaryPageTest lead = new FSLeadSummaryPageTest();
			Map<String, String> leadInfo = lead.addLeadLeadSummaryWithLeadName(driver, config, leadName, leadName);
			fc.utobj().printTestStep("Log a Task");
			taskName = addTask(driver, taskName, leadInfo.get("firstName"));

			FSTasksPage pobj = new FSTasksPage(driver);
			fc.utobj().printTestStep("Delete a Task");
			fc.utobj().singleActionIcon(driver, "Delete");
			fc.utobj().acceptAlertBox(driver);

			boolean isShowFilter = false;

			try {
				fc.utobj().getElement(driver, pobj.hideFilterLnk).isDisplayed();
				fc.utobj().sendKeys(driver, pobj.taskSubject, taskName);
				isShowFilter = true;
			} catch (Exception e) {
				fc.utobj().clickElement(driver, pobj.showFilterLnk);
				fc.utobj().sendKeys(driver, pobj.taskSubject, taskName);
				isShowFilter = true;
			}

			if (isShowFilter = false) {
				try {
					fc.utobj().clickElement(driver, pobj.showFilterLnk);
					fc.utobj().sendKeys(driver, pobj.taskSubject, taskName);
				} catch (Exception e) {

				}
			}
			fc.utobj().clickElement(driver, pobj.searchBtn);

			// fc.utobj().assertSingleLinkText(driver,taskName);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales" ,"TC_FS_Tasks_004" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FS_Tasks_004", testCaseDescription = "Process Task from Action Menu")
	public void processTaskActionIcon() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		String taskName = dataSet.get("taskName");
		String leadName = dataSet.get("leadName");
		leadName = fc.utobj().generateTestData(leadName);

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Add a lead");
			FSLeadSummaryPageTest lead = new FSLeadSummaryPageTest();
			Map<String, String> leadInfo = lead.addLeadLeadSummaryWithLeadName(driver, config, leadName, leadName);
			fc.utobj().printTestStep("Log a Task");
			taskName = addTask(driver, taskName, leadInfo.get("firstName"));

			FSTasksPage pobj = new FSTasksPage(driver);
			fc.utobj().printTestStep("Process status of a Task");
			// fc.utobj().singleActionIcon(driver,"Process");
			fc.utobj().actionImgOption(driver, taskName, "Process");
			String windowHandle = driver.getWindowHandle();
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().selectDropDownByVisibleText(driver, pobj.statusDropDown, "Completed");
			fc.utobj().clickElement(driver, pobj.processBtn);
			driver.switchTo().window(windowHandle);

			boolean isShowFilter = false;

			try {
				fc.utobj().getElement(driver, pobj.hideFilterLnk).isDisplayed();
				fc.utobj().sendKeys(driver, pobj.taskSubject, taskName);
				isShowFilter = true;
			} catch (Exception e) {
				fc.utobj().clickElement(driver, pobj.showFilterLnk);
				fc.utobj().sendKeys(driver, pobj.taskSubject, taskName);
				isShowFilter = true;
			}

			if (isShowFilter = false) {
				try {
					fc.utobj().clickElement(driver, pobj.showFilterLnk);
					fc.utobj().sendKeys(driver, pobj.taskSubject, taskName);
				} catch (Exception e) {

				}
			}

			fc.utobj().clickElement(driver, pobj.searchBtn);

			fc.utobj().assertSingleLinkText(driver, taskName);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales","sales_failed" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FS_Tasks_005", testCaseDescription = "Change Status of Task from Action Menu")
	public void changeStatusActionMenu() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		String taskName = dataSet.get("taskName");
		String leadName = dataSet.get("leadName");
		leadName = fc.utobj().generateTestData(leadName);

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Add a lead");
			FSLeadSummaryPageTest lead = new FSLeadSummaryPageTest();
			Map<String, String> leadInfo = lead.addLeadLeadSummaryWithLeadName(driver, config, leadName, leadName);
			fc.utobj().printTestStep("Log a Task");
			taskName = addTask(driver, taskName, leadInfo.get("firstName"));
			FSTasksPage pobj = new FSTasksPage(driver);

			fc.utobj().clickElement(driver, pobj.checkBoxAll1);
			fc.utobj().printTestStep("Change status of a Task");
			String windowHandle = driver.getWindowHandle();
			fc.utobj().selectActionMenuItems(driver, "Change Status");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().selectDropDownByVisibleText(driver, pobj.statusDropDown, "Completed");
			fc.utobj().clickElement(driver, pobj.statusChangeCbox);
			driver.switchTo().window(windowHandle);

			boolean isShowFilter = false;

			try {
				fc.utobj().getElement(driver, pobj.hideFilterLnk).isDisplayed();
				fc.utobj().sendKeys(driver, pobj.taskSubject, taskName);
				isShowFilter = true;
			} catch (Exception e) {
				fc.utobj().clickElement(driver, pobj.showFilterLnk);
				fc.utobj().sendKeys(driver, pobj.taskSubject, taskName);
				isShowFilter = true;
			}

			if (isShowFilter = false) {
				try {
					fc.utobj().clickElement(driver, pobj.showFilterLnk);
					fc.utobj().sendKeys(driver, pobj.taskSubject, taskName);
				} catch (Exception e) {

				}
			}
			fc.utobj().clickElement(driver, pobj.searchBtn);

			fc.utobj().assertSingleLinkText(driver, taskName);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales","sales_failed" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FS_Tasks_006", testCaseDescription = "Delete Task from Task Tab")
	public void deleteTaskActionMenu() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		String taskName = dataSet.get("taskName");
		String leadName = dataSet.get("leadName");
		leadName = fc.utobj().generateTestData(leadName);

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Add a lead");
			FSLeadSummaryPageTest lead = new FSLeadSummaryPageTest();
			Map<String, String> leadInfo = lead.addLeadLeadSummaryWithLeadName(driver, config, leadName, leadName);
			fc.utobj().printTestStep("Log a Task");
			taskName = addTask(driver, taskName, leadInfo.get("firstName"));

			FSTasksPage pobj = new FSTasksPage(driver);

			fc.utobj().clickElement(driver, pobj.checkBoxAll1);
			fc.utobj().selectActionMenuItems(driver, "Delete");
			fc.utobj().printTestStep("Delete a Task");
			fc.utobj().acceptAlertBox(driver);
			try {
				fc.utobj().getElement(driver, pobj.hideFilterLnk).isDisplayed();
				fc.utobj().sendKeys(driver, pobj.taskSubject, taskName);
			} catch (Exception e) {
				fc.utobj().clickElement(driver, pobj.showFilterLnk);
				fc.utobj().sendKeys(driver, pobj.taskSubject, taskName);
			}

			fc.utobj().clickElement(driver, pobj.searchBtn);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales","sales_failed" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FS_Tasks_007", testCaseDescription = "Change status of a Task from Task Tab")
	public void changeStatusBtn() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		String taskName = dataSet.get("taskName");
		String leadName = dataSet.get("leadName");
		leadName = fc.utobj().generateTestData(leadName);

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Add a lead");
			FSLeadSummaryPageTest lead = new FSLeadSummaryPageTest();
			Map<String, String> leadInfo = lead.addLeadLeadSummaryWithLeadName(driver, config, leadName, leadName);
			fc.utobj().printTestStep("Log a Task");
			taskName = addTask(driver, taskName, leadInfo.get("firstName"));

			FSTasksPage pobj = new FSTasksPage(driver);
			String windowHandle = driver.getWindowHandle();
			fc.utobj().clickElement(driver, pobj.checkBoxAll1);
			fc.utobj().printTestStep("Change status of a Task");
			fc.utobj().clickElement(driver, pobj.changeStatusBottomBtn);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().selectDropDownByVisibleText(driver, pobj.statusDropDown, "Completed");
			fc.utobj().clickElement(driver, pobj.statusChangeCbox);
			driver.switchTo().window(windowHandle);

			try {
				fc.utobj().getElement(driver, pobj.hideFilterLnk).isDisplayed();
				fc.utobj().sendKeys(driver, pobj.taskSubject, taskName);
			} catch (Exception e) {
				fc.utobj().clickElement(driver, pobj.showFilterLnk);
				fc.utobj().sendKeys(driver, pobj.taskSubject, taskName);
			}

			fc.utobj().clickElement(driver, pobj.searchBtn);

			fc.utobj().assertSingleLinkText(driver, taskName);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales","sales_failed" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FS_Tasks_008", testCaseDescription = "Delete a Task from Task Tab")
	public void deleteTaskBtn() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		String taskName = dataSet.get("taskName");
		String leadName = dataSet.get("leadName");
		leadName = fc.utobj().generateTestData(leadName);

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Add a lead");
			FSLeadSummaryPageTest lead = new FSLeadSummaryPageTest();
			Map<String, String> leadInfo = lead.addLeadLeadSummaryWithLeadName(driver, config, leadName, leadName);
			fc.utobj().printTestStep("Log a Task");
			taskName = addTask(driver, taskName, leadInfo.get("firstName"));

			FSTasksPage pobj = new FSTasksPage(driver);

			fc.utobj().clickElement(driver, pobj.checkBoxAll1);
			fc.utobj().printTestStep("Delete a Task");
			fc.utobj().clickElement(driver, pobj.deleteBottomBtn);
			fc.utobj().acceptAlertBox(driver);
			try {
				fc.utobj().getElement(driver, pobj.hideFilterLnk).isDisplayed();
				fc.utobj().sendKeys(driver, pobj.taskSubject, taskName);
			} catch (Exception e) {
				fc.utobj().clickElement(driver, pobj.showFilterLnk);
				fc.utobj().sendKeys(driver, pobj.taskSubject, taskName);
			}
			fc.utobj().clickElement(driver, pobj.searchBtn);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void searchTask(WebDriver driver, String taskName) throws Exception {
		FSTasksPage pobj = new FSTasksPage(driver);

		boolean isShowFilter = false;

		try {
			fc.utobj().getElement(driver, pobj.hideFilterLnk).isDisplayed();
			fc.utobj().sendKeys(driver, pobj.taskSubject, taskName);
			isShowFilter = true;
		} catch (Exception e) {
			fc.utobj().clickElement(driver, pobj.showFilterLnk);
			fc.utobj().sendKeys(driver, pobj.taskSubject, taskName);
			isShowFilter = true;
		}

		if (isShowFilter = false) {
			try {
				fc.utobj().clickElement(driver, pobj.showFilterLnk);
				fc.utobj().sendKeys(driver, pobj.taskSubject, taskName);
			} catch (Exception e) {

			}
		}

		fc.utobj().clickElement(driver, pobj.searchBtn);

	}
}
