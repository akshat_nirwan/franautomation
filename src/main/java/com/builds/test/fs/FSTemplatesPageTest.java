package com.builds.test.fs;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.uimaps.fs.FSCampaignCenterPage;
import com.builds.uimaps.fs.FSTemplatePage;
import com.builds.uimaps.fs.SearchUI;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class FSTemplatesPageTest {

	FranconnectUtil fc = new FranconnectUtil();

	public void createBasicTemplate(WebDriver driver, String templateName, String emailSubject, String accessibility,
			String emailType, String emailContent) throws Exception {

		Sales fsMod = new Sales();
		fsMod.emailCampaignTab(driver);
		FSTemplatePage pobj = new FSTemplatePage(driver);
		FSCampaignCenterPage campaignCenterPage = new FSCampaignCenterPage(driver);
		try {
			fc.utobj().clickElement(driver, campaignCenterPage.managetTemplateBtn);
			fc.utobj().clickElement(driver, campaignCenterPage.createTemplateBtn);

		} catch (Exception e) {
			fc.utobj().clickElement(driver, campaignCenterPage.createTemplateTab);
		}

		fc.utobj().clickElement(driver, pobj.codeYourOwnLink);

		fc.utobj().sendKeys(driver, pobj.templateName, templateName);
		fc.utobj().sendKeys(driver, pobj.emailSubject, emailSubject);

		if (accessibility.equalsIgnoreCase("Public")) {
			fc.utobj().clickElement(driver, pobj.publicTemplate);
		} else if (accessibility.equalsIgnoreCase("Private")) {
			fc.utobj().clickElement(driver, pobj.privateTemplate);
		}

		if (emailType.equalsIgnoreCase("plainText")) {
			fc.utobj().clickElement(driver, pobj.plainText);
			fc.utobj().sendKeys(driver, pobj.plainTextEditor, emailContent);
		} else if (emailType.equalsIgnoreCase("graphicalText")) {
			fc.utobj().clickElement(driver, pobj.graphicalText);
			fc.utobj().switchFrameById(driver, "ta_ifr");
			fc.utobj().sendKeys(driver, pobj.graphicalContent, emailContent);
			fc.utobj().switchFrameToDefault(driver);
		} else if (emailType.equalsIgnoreCase("uploadHTML")) {
			fc.utobj().clickElement(driver, pobj.uploadHTML);
			// pending work
		}

		fc.utobj().clickElement(driver, pobj.saveBtn);

		// driver.quit();
	}

	@Test(groups = { "TC_Solr_Search_Template_002", "sales" }) // ok
	@TestCase(createdOn = "2017-11-02", updatedOn = "2017-11-02", testCaseId = "TC_Solr_Search_Template_002", testCaseDescription = "Validate Fdd Email in the activity history > Validate email in mail box >Validate fdd login and download ")
	private void checkSolrSearchforSites() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Sales > Campaigns> Create template");
			fc.sales().sales_common().fsModule(driver);

			SearchUI search_page = new SearchUI(driver);

			String templatePre = fc.utobj().generateTestData(dataSet.get("template"));
			String template1 = templatePre + dataSet.get("template1");
			String template2 = templatePre + dataSet.get("template2");
			String template3 = templatePre + dataSet.get("template3");
			String template4 = templatePre + dataSet.get("template4");
			String template5 = templatePre + dataSet.get("template5");
			String template6 = templatePre + dataSet.get("template6");
			String template7 = templatePre + dataSet.get("template7");
			String template8 = templatePre + dataSet.get("template8");

			String folderName = dataSet.get("folderName");
			String emailSubject = dataSet.get("emailSubject");
			String accessibility = dataSet.get("accessibility");
			String emailType = dataSet.get("emailType");
			String emailContent = dataSet.get("emailContent");

			fc.utobj().printTestStep("Sales > Sites > Add Sites > Action menu > Click on lock > Site gets locked");
			fc.sales().sales_common().fsModule(driver);
			createBasicTemplate(driver, template1, emailSubject, accessibility, emailType, emailContent);
			createBasicTemplate(driver, template2, emailSubject, accessibility, emailType, emailContent);
			createBasicTemplate(driver, template3, emailSubject, accessibility, emailType, emailContent);
			createBasicTemplate(driver, template4, emailSubject, accessibility, emailType, emailContent);
			createBasicTemplate(driver, template5, emailSubject, accessibility, emailType, emailContent);
			createBasicTemplate(driver, template6, emailSubject, accessibility, emailType, emailContent);
			createBasicTemplate(driver, template7, emailSubject, accessibility, emailType, emailContent);
			createBasicTemplate(driver, template8, emailSubject, accessibility, emailType, emailContent);

			boolean isSearchTrue = false;
			boolean isVeiwAll = false;
			for (int i = 0; i < 3; i++) {
				if (isSearchTrue == false) {
					fc.utobj().sendKeys(driver, search_page.topSearchField, templatePre);

					if (i == 2) {

					}
					fc.utobj().clickEnterOnElement(driver, search_page.topSearchField);

					fc.utobj().moveToElement(driver,
							fc.utobj().getElementByXpath(driver, ".//span/*[contains(text(),'" + template8 + "')]"));
					isSearchTrue = true;
				}
			}
			if (isSearchTrue == false) {
				fc.utobj().throwsException("was not able to search Lead through Top Search");
			}
			// fc.utobj().moveToElement(driver,
			// fc.utobj().getElementByXpath(driver,".//span/*[contains(text(),'"+brokerName2+"')]")));
			// fc.utobj().moveToElement(driver,
			// fc.utobj().getElementByXpath(driver,".//span/*[contains(text(),'"+brokerName3+"')]")));
			// fc.utobj().moveToElement(driver,
			// fc.utobj().getElementByXpath(driver,".//span/*[contains(text(),'"+brokerName8+"')]")));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[@ng-click='viewAll()']"));

			isVeiwAll = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + template1 + "')]");
			isVeiwAll = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + template2 + "')]");
			isVeiwAll = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + template3 + "')]");
			isVeiwAll = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + template4 + "')]");
			isVeiwAll = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + template5 + "')]");
			isVeiwAll = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + template6 + "')]");
			isVeiwAll = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + template7 + "')]");
			isVeiwAll = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + template8 + "')]");
			if (isVeiwAll == false) {
				fc.utobj().throwsException("All elements not visible on clicking view all");
			}
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'" + template1 + "')]"));
			boolean isBrokerVerified = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + template1 + "')]");
			if (isBrokerVerified == false) {
				fc.utobj().throwsException("Worflow not verified on click at search");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

}
