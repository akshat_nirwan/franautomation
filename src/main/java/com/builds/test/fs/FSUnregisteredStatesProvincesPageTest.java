package com.builds.test.fs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.builds.test.crm.CRMCampaignCenterPageTest;
import com.builds.test.fs.RestAPI.FsRestServicesAction;
import com.builds.uimaps.fs.FSUnregisteredStatesProvincesPage;
import com.builds.uimaps.fs.LeadSummaryUI;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class FSUnregisteredStatesProvincesPageTest {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "sales", "salesJuly10" , "TC_Sales_UnregisteredStates_001" }) // Fixed : Verified // Akshat
	@TestCase(createdOn = "2017-07-04", updatedOn = "2017-07-17", testCaseId = "TC_Sales_UnregisteredStates_001", testCaseDescription = "Verify the count of Active countries are available at Unregistered States Countries")
	private void verifyCountofUnregisteredCountries() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Go to Admin > Configuration > Configure Countries");
			fc.adminpage().adminPage(driver);
			fc.adminpage().openAdminConfigureCountries(driver);

			List<String> domesticIntnlountryList = new ArrayList<String>();
			List<String> inActiveCountriesList = new ArrayList<String>();
			List<String> allActivecountryList = new ArrayList<String>();
			String countryName = null;
			fc.utobj().printTestStep("List Countries in Inactive Countries,");
			fc.utobj().printTestStep("Countries falling in International Area / Region and ");
			fc.utobj().printTestStep("Countries falling in Domestic Area / Region");

			List<WebElement> domesticIntrnlCountriesDrpDwn = driver.findElements(
					By.xpath(".//select[@name='internationalActiveCountries' or @name='activeCountries']/option"));
			List<WebElement> inactiveCountriesDrpDwn = driver
					.findElements(By.xpath(".//select[@name='inActiveCountries']/option"));

			int size = domesticIntrnlCountriesDrpDwn.size();
			for (int i = 0; i < size; i++) {
				WebElement webElement = domesticIntrnlCountriesDrpDwn.get(i);
				countryName = webElement.getText();
				domesticIntnlountryList.add(countryName);
			}

			for (int i = 0; i < inactiveCountriesDrpDwn.size(); i++) {
				WebElement webElement2 = inactiveCountriesDrpDwn.get(i);
				countryName = webElement2.getText();
				inActiveCountriesList.add(countryName);
			}
			fc.utobj().printTestStep("Go to Admin > Sales  >  Unregistered States / Provinces");
			fc.adminpage().adminPage(driver);
			fc.adminpage().openDefineUnregisteredStatesProvincesPage(driver);
			
			// Try if already some Unregistered State/Province is made --> find and hit modify button in that case
			try {
				fc.utobj().clickElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'Modify')]"));
			} catch (Exception e) {
			}
			
			List<WebElement> unregisteredCountriesList = driver
					.findElements(By.xpath(".//*[@id='ms-parentcountryCombo']/div/ul/li/label"));
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='ms-parentcountryCombo']/button"));
			
			for (int i = 0; i < unregisteredCountriesList.size(); i++) {
				fc.utobj().moveToElement(driver, unregisteredCountriesList.get(i)); // akshat
				WebElement webElement1 = unregisteredCountriesList.get(i);
				String unregisteredcountryName = webElement1.getText();
				allActivecountryList.add(unregisteredcountryName);
			}

			fc.utobj().printTestStep(
					"The countries falling in Domestic and International region is present in the Countries multiselect box");
		
			/*int i =1 ; 
			for(String str : allActivecountryList)
			{
				System.out.println("---> "+ ""+i+"" + str);
				i++;
			}*/
			
			if (allActivecountryList.containsAll(domesticIntnlountryList) == false) {
				fc.utobj().throwsException(
						"The countries falling in Domestic and International region having mismatch in the Countries multiselect box");
			}
			fc.utobj().printTestStep("Inactive Countries should not come in the Counties multiselect box");
			inActiveCountriesList.retainAll(allActivecountryList);
			if (!inActiveCountriesList.isEmpty()) {
				fc.utobj().throwsException(
						"The countries falling in Domestic and International region having mismatch in the Countries multiselect box");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			e.getStackTrace();
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales1", "salesRestAPI", "FCRestAPI" })
	@TestCase(createdOn = "2017-07-04", updatedOn = "2017-07-17", testCaseId = "TC_Sales_UnregisteredStates_002", testCaseDescription = "Verify the count of Active countries are available at Unregistered States Countries")
	private void verifyUserDeleteWithoutReassigning() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			FSEmailCampaignsPageTest campaignPageTest = new FSEmailCampaignsPageTest();
			AdminFranchiseSalesLeadStatusPageTest p1 = new AdminFranchiseSalesLeadStatusPageTest();
			FSUnregisteredStatesProvincesPage pobj = new FSUnregisteredStatesProvincesPage(driver);
			AddLeadFromAllSources addLeadFromAllSourecesObj = new AddLeadFromAllSources();
			Sales fsmod = new Sales();
			FsRestServicesAction restObj = new FsRestServicesAction();
			LeadSummaryUI LeadSummaryObj = new LeadSummaryUI(driver);
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			FSSearchPageTest p3 = new FSSearchPageTest();

			String campaignName = fc.utobj().generateTestData(dataSet.get("campaignName"));
			String newCampaignName = fc.utobj().generateTestData(dataSet.get("campaignName"));
			String camapignDescription = fc.utobj().generateTestData(dataSet.get("camapignDescription"));
			String accessibility = dataSet.get("accessibility");
			String templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String newtemplateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String emailSubject = fc.utobj().generateTestData(dataSet.get("emailSubject"));
			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String firstNameWebForm = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastNameWebForm = fc.utobj().generateTestData(dataSet.get("lastName"));
			Map<String, String> UniqueKey_PrimaryInfo = new HashMap<String, String>();
			String formName = fc.utobj().generateTestData(dataSet.get("formName"));
			dataSet.put("firstName", firstName);
			dataSet.put("lastName", lastName);
			String country = dataSet.get("country");
			String state = dataSet.get("stateID");
			String emailId = dataSet.get("emailID");
			String leadOwner = dataSet.get("leadOwnerID");
			String leadSourceCategory = dataSet.get("leadSource2ID");
			String leadSourceDetails = dataSet.get("leadSource3ID");

			fc.utobj().printTestStep("Go to Admin > Sales > Lead Status > Add lead status");
			String leadStatus = p1.addNewLeadStatus(driver);

			fc.utobj().printTestStep("Sales > Campaign Center > Create Campaign");
			campaignName = campaignPageTest.createEmailCampaignCodeYourOwnTemp(driver, campaignName,
					camapignDescription, accessibility, templateName, emailSubject);
			newCampaignName = campaignPageTest.createEmailCampaignCodeYourOwnTemp(driver, newCampaignName,
					camapignDescription, accessibility, newtemplateName, emailSubject);

			fc.utobj().printTestStep(
					"Go to Admin > Sales  >  Unregistered States / Provinces > Congure Any Country and State > Configure Status and Campaign > Submit.");
			fc.adminpage().adminPage(driver);
			fc.adminpage().openDefineUnregisteredStatesProvincesPage(driver);
			try {
				fc.utobj().clickElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'Modify')]"));
			} catch (Exception e) {
			}
			fc.utobj().selectValFromMultiSelect(driver, pobj.campaignDownDwn, "USA");
			if (!fc.utobj().getElement(driver, pobj.secondStateChkBox).isSelected()) {
				fc.utobj().clickElement(driver, pobj.secondStateChkBox);
			}
			fc.utobj().selectDropDown(driver, pobj.campaignDrpDwn, campaignName);
			fc.utobj().selectDropDown(driver, pobj.statusDrpDwn, leadStatus);
			fc.utobj().clickElement(driver, pobj.updateBtn);

			fc.utobj().printTestStep(
					"Add a lead from all system, webform, parsing, restapi and import check the configured campaign and status is getting assigned to these lead.");

			fc.utobj().printTestStep("Adding lead from System");
			fc.sales().sales_common().fsModule(driver);
			fsmod.leadManagement(driver);
			FSLeadSummaryPageTest leadSummaryobj = new FSLeadSummaryPageTest();

			leadSummaryobj.clickAddLeadLink(driver);
			addLeadFromAllSourecesObj.fillFormDataWithDataset(driver, testCaseId, dataSet);
			boolean isCampaignAssociated = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text(),'" + campaignName + "')]");
			if (isCampaignAssociated == false) {
				fc.utobj().throwsException("Campaign trigered with Unregistered States not associated");
			}
			p3.searchByLeadName(driver, firstName, lastName);
			fc.utobj().printTestStep(
					"Go to lead summary and check the icon for unregistered lead is coming against the lead.");

			boolean isToolTipPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					" .//*[contains(text(),'" + firstName + " " + lastName + "')]/following-sibling::img[@id='Image']");
			if (isToolTipPresent == false) {
				fc.utobj().throwsException("Icon for unregistered State not present at Lead Summary page");
			}
			fc.utobj().printTestStep(
					"Add another campaign and try to associate with the lead > The campaign should not get associated with the lead.");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[contains(text(),'" + firstName + " " + lastName + "')]"));
			fc.utobj().clickElement(driver, LeadSummaryObj.associateCampaign);
			campaignCenterPage.searchCampaignByFilter(driver, newCampaignName);
			fc.utobj().clickElement(driver, LeadSummaryObj.associateCampaignIcon);
			fc.utobj().clickElement(driver, LeadSummaryObj.confirmBtn);
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Adding lead from Web Form");
			Map<String, String> leadInfo = new HashMap<String, String>();
			leadInfo.put("firstName", firstNameWebForm);
			leadInfo.put("lastName", lastNameWebForm);
			leadInfo.put("country", country);
			leadInfo.put("state", state);
			leadInfo.put("email", emailId);
			leadInfo.put("leadOwner", leadOwner);
			leadInfo.put("leadSourceCategory", leadSourceCategory);
			leadInfo.put("leadSourceDetails", leadSourceDetails);
			addLeadFromAllSourecesObj.addLeadFromWebForm(driver, formName, leadInfo, "Confirmation Message");

			p3.searchByLeadName(driver, firstNameWebForm, lastNameWebForm);
			fc.utobj().printTestStep(
					"Go to lead summary and check the icon for unregistered lead is coming against the lead added from Web Form.");
			boolean isToolTipPresentWebForm = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//img[@id='Image']");
			if (isToolTipPresentWebForm == false) {
				fc.utobj().throwsException("Icon for unregistered State not present at Lead Summary page");
			}

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[contains(text(),'" + firstNameWebForm + " " + lastNameWebForm + "')]"));
			boolean isCampaignAssociatedWebForm = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text(),'" + campaignName + "')]");
			if (isCampaignAssociatedWebForm == false) {
				fc.utobj().throwsException("Campaign trigered with Unregistered States not associated through webform");
			}
			boolean isStatusAssociatedWebForm = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text(),'" + leadStatus + "')]");
			if (isStatusAssociatedWebForm == false) {
				fc.utobj().throwsException("Status trigered with Unregistered States not associated through webform");
			}

			fc.utobj().printTestStep("Adding lead from Rest API");
			restObj.CreatePrimaryInfo(driver, dataSet, testCaseId, UniqueKey_PrimaryInfo);
			driver.navigate().to(FranconnectUtil.config.get("buildUrl"));
			fc.loginpage().login(driver);
			fc.sales().sales_common().fsModule(driver);
			p3.searchByLeadName(driver, UniqueKey_PrimaryInfo.get("firstName"), UniqueKey_PrimaryInfo.get("lastName"));
			fc.utobj().printTestStep(
					"Go to lead summary and check the icon for unregistered lead is coming against the lead added from Web Form.");
			boolean isToolTipPresentRestApi = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//img[@id='Image']");
			if (isToolTipPresentRestApi == false) {
				fc.utobj().throwsException("Icon for unregistered State not present at Lead Summary page");
			}
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[contains(text(),'"
					+ UniqueKey_PrimaryInfo.get("firstName") + " " + UniqueKey_PrimaryInfo.get("lastName") + "')]"));
			boolean isCampaignAssociatedRestApi = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text(),'" + campaignName + "')]");
			if (isCampaignAssociatedRestApi == false) {
				fc.utobj()
						.throwsException("Campaign trigered with Unregistered States not associated through Rest Api");
			}
			boolean isStatusAssociatedRestApi = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text(),'" + leadStatus + "')]");
			if (isStatusAssociatedRestApi == false) {
				fc.utobj().throwsException("Status trigered with Unregistered States not associated through Rest Api");
			}

			fc.utobj().printTestStep(
					"Go to Admin > Sales  >  Unregistered States / Provinces > Modify the unregistered states and submit.");
			fc.adminpage().adminPage(driver);
			fc.adminpage().openDefineUnregisteredStatesProvincesPage(driver);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'Modify')]"));
			fc.utobj().selectValFromMultiSelect(driver, pobj.campaignDownDwn, "USA");
			fc.utobj().selectDropDown(driver, pobj.statusDrpDwn, "New Lead");
			fc.utobj().clickElement(driver, pobj.updateBtn);
			boolean isModified = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text(),'New Lead')]");
			if (isModified == false) {
				fc.utobj().throwsException("Unregistered States / Provinces modified succesfully");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
}
