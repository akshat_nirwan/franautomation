package com.builds.test.fs;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.uimaps.fs.FSCampaignCenterPage;
import com.builds.uimaps.fs.SearchUI;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class FSWorflowsPageTest {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "salesank12", "sales","sales_failed" , "sales_failed_TC_Sales_CreateWorkFlow_001" }) // Fixed : Verified // Akshat 
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Sales_CreateWorkFlow_001", testCaseDescription = "Validate Fdd Email in the activity history > Validate email in mail box >Validate fdd login and download ")
	private void verifyCreateWorkflow() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Sales > Workflows > Create Workflow > Select Type > Standard");
			fc.sales().sales_common().fsModule(driver);
			Sales fsmod = new Sales();
			FSWorkflowsPage workflowsPage = new FSWorkflowsPage(driver);

			// Adding Campaign
			FSCampaignCenterPage campaignCenterPage = new FSCampaignCenterPage(driver);
			fsmod.emailCampaignTab(driver);

			String campaignName = fc.utobj().generateTestData("campaignName");
			String camapignDescription = fc.utobj().generateTestData(campaignName);
			String accessibility = "Public";
			// fc.utobj().clickElement(driver,
			// campaignCenterPage.createCampaign);
			try {
				fc.utobj().clickElement(driver, campaignCenterPage.manageCampaignBtn);
				fc.utobj().clickElement(driver, campaignCenterPage.btnCreateCampaign);
			} catch (Exception e) {
				fc.utobj().clickElement(driver, campaignCenterPage.createCampaignBtn);
			}
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			fc.utobj().sendKeys(driver, campaignCenterPage.campaignName, campaignName);
			fc.utobj().sendKeys(driver, campaignCenterPage.description, camapignDescription);
			// fc.utobj().selectDropDown(driver,
			// campaignCenterPage.campaignAccessibility, accessibility);
			fc.utobj().clickElement(driver, campaignCenterPage.startBtn);

			// Code Your Template
			String templateName = fc.utobj().generateTestData("templateName");
			String emailSubject = fc.utobj().generateTestData("emailSubject");
			String plainTextEditor = fc.utobj().generateTestData("plainTextEditor");

			fc.utobj().clickElement(driver, campaignCenterPage.codeYourOwn);
			fc.utobj().sendKeys(driver, campaignCenterPage.templateNameTitle, templateName);
			fc.utobj().sendKeys(driver, campaignCenterPage.emailSubject, emailSubject);

			fc.utobj().clickElement(driver, campaignCenterPage.CodeOwnplainTextEmailType);
			fc.utobj().sendKeys(driver, campaignCenterPage.textAreaPlainText, plainTextEditor);

			fc.utobj().clickElement(driver, campaignCenterPage.saveAndContinue);
			fc.utobj().clickElement(driver, campaignCenterPage.codeOwnAssociateLater);
			// fc.utobj().clickElement(driver,fc.utobj().getElementByXpath(driver,".//td[contains(text(),'"+groupName+"')]/ancestor::tr/td/input[@class='checkbox']")));
			// fc.utobj().clickElement(driver, campaignCenterPage.continueBtn);
			fc.utobj().clickElement(driver, campaignCenterPage.LaunchCampaignBtn);

			fc.utobj().printTestStep("Configure TRIGGER");
			fc.utobj().printTestStep("Configure CONDITION");
			fsmod.workflows(driver);
			String workflowname = fc.utobj().generateTestData("WrorkflowName");
			fc.utobj().clickElement(driver, workflowsPage.createWorkflowBtn);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			fc.utobj().sendKeys(driver, workflowsPage.workFlowName, workflowname);
			// fc.utobj().clickElement(driver, workflowsPage.eventBased);
			fc.utobj().clickElement(driver, workflowsPage.nextBtn);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, workflowsPage.whenNextBtn);
			fc.utobj().clickElement(driver, workflowsPage.ifNextBtn);
			fc.utobj().clickElement(driver, workflowsPage.chooseAction);
			fc.utobj().clickElement(driver, workflowsPage.sendCampaignAction);

			driver.switchTo().defaultContent();
			fc.utobj().selectDropDown(driver, workflowsPage.selectActionDrpDwn, campaignName);
			fc.utobj().clickElement(driver, workflowsPage.associateAndSave_SendEmailCampaign_iFrame);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().printTestStep("Configure CONDITION");
			fc.utobj().printTestStep("Configure ACTION > Configure All the Action");
			fc.utobj().clickElement(driver, workflowsPage.chooseAction);
			fc.utobj().clickElement(driver, workflowsPage.changeStatusAction);

			driver.switchTo().defaultContent();
			fc.utobj().selectDropDownByPartialText(driver, workflowsPage.selectActionDrpDwn, "New Lead");
			fc.utobj().clickElement(driver, workflowsPage.associateAndSave_SendEmailCampaign_iFrame);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, workflowsPage.chooseAction);
			fc.utobj().clickElement(driver, workflowsPage.sendEmailAction);

			driver.switchTo().defaultContent();
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			String mailSubject = fc.utobj().generateTestData("Test Email Subject");
			fc.utobj().sendKeys(driver, workflowsPage.sendMailSubject, mailSubject);
			// String windowHandle = driver.getWindowHandle();
			driver.switchTo().frame(workflowsPage.htmlFrame);
			fc.utobj().sendKeys(driver, workflowsPage.mailText, "Dear $FIRST_NAME$  mail body,");
			// driver.switchTo().window(windowHandle);
			fc.utobj().switchToParentFrame(driver);
			fc.utobj().clickElement(driver, workflowsPage.associsteAndSave);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, workflowsPage.chooseAction);
			fc.utobj().clickElement(driver, workflowsPage.createTask);

			driver.switchTo().defaultContent();
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			fc.utobj().sendKeys(driver, workflowsPage.sendMailSubject, fc.utobj().generateTestData("Task Subject"));
			fc.utobj().sendKeys(driver, workflowsPage.taskdesc, fc.utobj().generateTestData("Task Desc"));
			fc.utobj().selectDropDownByPartialText(driver, workflowsPage.taskType, "Default");
			// fc.utobj().selectDropDownByPartialText(driver,
			// workflowsPage.taskType, "Default");
			fc.utobj().clickElement(driver, workflowsPage.timeleassTask);
			// fc.utobj().selectDropDownByPartialText(driver,
			// workflowsPage.selectActionDrpDwn, "New Lead");
			fc.utobj().clickElement(driver, workflowsPage.createBtn);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, workflowsPage.create);

			boolean isworkFlowCreated = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + workflowname + "')]");
			if (isworkFlowCreated == false) {
				fc.utobj().throwsException("WorkFlow Creation not veried");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			// System.out.println(e.getMessage());
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesank12aad", "sales","sales_failed" , "TC_Solr_Search_Workflow_002"}) // Fixed : Verified // Akshat
	@TestCase(createdOn = "2017-11-02", updatedOn = "2017-11-02", testCaseId = "TC_Solr_Search_Workflow_002", testCaseDescription = "Validate Fdd Email in the activity history > Validate email in mail box >Validate fdd login and download ")
	private void checkSolrSearchforSites() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Sales > Workflows > Create Workflow > Select Type > Standard");
			fc.sales().sales_common().fsModule(driver);

			SearchUI search_page = new SearchUI(driver);

			String workflowPre = fc.utobj().generateTestData(dataSet.get("workflow"));
			String workflow1 = workflowPre + dataSet.get("workflow1");
			String workflow2 = workflowPre + dataSet.get("workflow2");
			String workflow3 = workflowPre + dataSet.get("workflow3");
			String workflow4 = workflowPre + dataSet.get("workflow4");
			String workflow5 = workflowPre + dataSet.get("workflow5");
			String workflow6 = workflowPre + dataSet.get("workflow6");
			String workflow7 = workflowPre + dataSet.get("workflow7");
			String workflow8 = workflowPre + dataSet.get("workflow8");

			fc.utobj().printTestStep("Sales > Sites > Add Sites > Action menu > Click on lock > Site gets locked");
			fc.sales().sales_common().fsModule(driver);
			workflow1 = addStandardWorkflowForAllLeadAdded(driver, workflow1, "Send Email", "Email Subject");
			workflow2 = addStandardWorkflowForAllLeadAdded(driver, workflow2, "Send Email", "Email Subject");
			workflow3 = addStandardWorkflowForAllLeadAdded(driver, workflow3, "Send Email", "Email Subject");
			workflow4 = addStandardWorkflowForAllLeadAdded(driver, workflow4, "Send Email", "Email Subject");
			workflow5 = addStandardWorkflowForAllLeadAdded(driver, workflow5, "Send Email", "Email Subject");
			workflow6 = addStandardWorkflowForAllLeadAdded(driver, workflow6, "Send Email", "Email Subject");
			workflow7 = addStandardWorkflowForAllLeadAdded(driver, workflow7, "Send Email", "Email Subject");
			workflow8 = addStandardWorkflowForAllLeadAdded(driver, workflow8, "Send Email", "Email Subject");

			boolean isSearchTrue = false;
			boolean isVeiwAll = false;
			for (int i = 0; i < 3; i++) {
				if (isSearchTrue == false) {
					fc.utobj().sendKeys(driver, search_page.topSearchField, workflowPre);

					if (i == 2) {

					}
					fc.utobj().clickEnterOnElement(driver, search_page.topSearchField);
					

					fc.utobj().moveToElement(driver,
							fc.utobj().getElementByXpath(driver, ".//span/*[contains(text(),'" + workflow8 + "')]"));
					isSearchTrue = true;
				}
			}
			if (isSearchTrue == false) {
				fc.utobj().throwsException("was not able to search Lead through Top Search");
			}
			// fc.utobj().moveToElement(driver,
			// fc.utobj().getElementByXpath(driver,".//span/*[contains(text(),'"+brokerName2+"')]")));
			// fc.utobj().moveToElement(driver,
			// fc.utobj().getElementByXpath(driver,".//span/*[contains(text(),'"+brokerName3+"')]")));
			// fc.utobj().moveToElement(driver,
			// fc.utobj().getElementByXpath(driver,".//span/*[contains(text(),'"+brokerName8+"')]")));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[@ng-click='viewAll()']"));

			isVeiwAll = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + workflow1 + "')]");
			isVeiwAll = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + workflow2 + "')]");
			isVeiwAll = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + workflow3 + "')]");
			isVeiwAll = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + workflow4 + "')]");
			isVeiwAll = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + workflow5 + "')]");
			isVeiwAll = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + workflow6 + "')]");
			isVeiwAll = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + workflow7 + "')]");
			isVeiwAll = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + workflow8 + "')]");
			if (isVeiwAll == false) {
				fc.utobj().throwsException("All elements not visible on clicking view all");
			}
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'" + workflow1 + "')]"));
			boolean isBrokerVerified = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + workflow1 + "')]");
			if (isBrokerVerified == false) {
				fc.utobj().throwsException("Worflow not verified on click at search");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addStandardWorkflowForAllLeadAdded(WebDriver driver, String workflowName, String workflowAction,
			String actionInput) throws Exception {

		// adsales.fsModule(fc.sales(), driver);
		Sales fsmod = new Sales();
		FSWorkflowsPage workflowsPage = new FSWorkflowsPage(driver);
		FSCampaignCenterPage campaignCenterPage = new FSCampaignCenterPage(driver);

		fsmod.workflows(driver);
		fc.utobj().clickElement(driver, workflowsPage.createWorkflowBtn);
		fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
		fc.utobj().sendKeys(driver, workflowsPage.workFlowName, workflowName);
		// fc.utobj().clickElement(driver, workflowsPage.eventBased);
		fc.utobj().clickElement(driver, workflowsPage.nextBtn);
		fc.utobj().switchFrameToDefault(driver);
		fc.utobj().clickElement(driver, workflowsPage.whenNextBtn);
		fc.utobj().clickElement(driver, workflowsPage.ifNextBtn);
		fc.utobj().clickElement(driver, workflowsPage.chooseAction);

		// Adding Campaign
		if ("Send Email Campaign".equalsIgnoreCase(workflowAction)) {
			String campaignName = actionInput;
			String camapignDescription = fc.utobj().generateTestData(actionInput);
			String accessibility = "Public";

			fsmod.emailCampaignTab(driver);

			try {
				fc.utobj().clickElement(driver, campaignCenterPage.manageCampaignBtn);
				fc.utobj().clickElement(driver, campaignCenterPage.btnCreateCampaign);
			} catch (Exception e) {
				fc.utobj().clickElement(driver, campaignCenterPage.createCampaignBtn);
			}
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			fc.utobj().sendKeys(driver, campaignCenterPage.campaignName, campaignName);
			fc.utobj().sendKeys(driver, campaignCenterPage.description, camapignDescription);
			// fc.utobj().selectDropDown(driver,
			// campaignCenterPage.campaignAccessibility, accessibility);
			fc.utobj().clickElement(driver, campaignCenterPage.startBtn);

			// Code Your Template
			String templateName = fc.utobj().generateTestData("templateName");
			String emailSubject = fc.utobj().generateTestData("emailSubject");
			String plainTextEditor = fc.utobj().generateTestData("plainTextEditor");

			fc.utobj().clickElement(driver, campaignCenterPage.codeYourOwn);
			fc.utobj().sendKeys(driver, campaignCenterPage.templateNameTitle, templateName);
			fc.utobj().sendKeys(driver, campaignCenterPage.emailSubject, emailSubject);

			fc.utobj().clickElement(driver, campaignCenterPage.CodeOwnplainTextEmailType);
			fc.utobj().sendKeys(driver, campaignCenterPage.textAreaPlainText, plainTextEditor);

			fc.utobj().clickElement(driver, campaignCenterPage.saveAndContinue);
			fc.utobj().clickElement(driver, campaignCenterPage.codeOwnAssociateLater);
			// fc.utobj().clickElement(driver,fc.utobj().getElementByXpath(driver,".//td[contains(text(),'"+groupName+"')]/ancestor::tr/td/input[@class='checkbox']")));
			// fc.utobj().clickElement(driver, campaignCenterPage.continueBtn);
			fc.utobj().clickElement(driver, campaignCenterPage.LaunchCampaignBtn);

			fc.utobj().clickElement(driver, workflowsPage.sendCampaignAction);

			driver.switchTo().defaultContent();
			fc.utobj().selectDropDown(driver, workflowsPage.selectActionDrpDwn, campaignName);
			fc.utobj().clickElement(driver, workflowsPage.associsteAndSave);
			fc.utobj().switchFrameToDefault(driver);

		}

		else if ("Change Status".equalsIgnoreCase(workflowAction)) {
			fc.utobj().clickElement(driver, workflowsPage.changeStatusAction);

			driver.switchTo().defaultContent();
			fc.utobj().selectDropDownByPartialText(driver, workflowsPage.selectActionDrpDwn, actionInput);
			fc.utobj().clickElement(driver, workflowsPage.associsteAndSave);
			fc.utobj().switchFrameToDefault(driver);

		}

		else if ("Send Email".equalsIgnoreCase(workflowAction)) {
			fc.utobj().clickElement(driver, workflowsPage.sendEmailAction);

			driver.switchTo().defaultContent();
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			String mailSubject = actionInput;
			fc.utobj().sendKeys(driver, workflowsPage.sendMailSubject, mailSubject);
			// String windowHandle = driver.getWindowHandle();
			driver.switchTo().frame(workflowsPage.htmlFrame);
			fc.utobj().sendKeys(driver, workflowsPage.mailText, "Dear $FIRST_NAME$  mail body,");
			fc.utobj().switchToParentFrame(driver);                                                 // akshat
			// driver.switchTo().window(windowHandle);
			fc.utobj().clickElement(driver, workflowsPage.associsteAndSave);
			fc.utobj().switchFrameToDefault(driver);

		}

		else if ("Create Task".equalsIgnoreCase(workflowAction)) {
			String taskName = actionInput;
			fc.utobj().clickElement(driver, workflowsPage.createTask);

			driver.switchTo().defaultContent();
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			fc.utobj().sendKeys(driver, workflowsPage.sendMailSubject, taskName);
			fc.utobj().sendKeys(driver, workflowsPage.taskdesc, fc.utobj().generateTestData("Task Desc"));
			fc.utobj().selectDropDownByPartialText(driver, workflowsPage.taskType, "Default");
			// fc.utobj().selectDropDownByPartialText(driver,
			// workflowsPage.taskType, "Default");
			fc.utobj().clickElement(driver, workflowsPage.timeleassTask);
			// fc.utobj().selectDropDownByPartialText(driver,
			// workflowsPage.selectActionDrpDwn, "New Lead");
			fc.utobj().clickElement(driver, workflowsPage.createBtn);
			fc.utobj().switchFrameToDefault(driver);
		}

		fc.utobj().clickElement(driver, workflowsPage.create);

		return workflowName;
	}

}
