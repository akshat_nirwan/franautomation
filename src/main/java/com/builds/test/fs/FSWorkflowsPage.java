package com.builds.test.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FSWorkflowsPage {

	WebDriver driver;

	@FindBy(xpath = ".//button[.='Create Workflow']")
	public WebElement createWorkflowBtn;

	@FindBy(xpath = ".//*[@id='workFlowName']")
	public WebElement workFlowName;

	@FindBy(xpath = ".//*[@id='addWorkFlowForm']//label[@for='workFlowType3']")
	public WebElement eventBased;

	@FindBy(xpath = ".//button[.='Next']")
	public WebElement nextBtn;

	@FindBy(xpath = ".//*[@id='when-next']")
	public WebElement whenNextBtn;

	@FindBy(xpath = ".//*[@id='if-next']")
	public WebElement ifNextBtn;

	@FindBy(xpath = ".//*[@id='action-button']/div[@class='list-name']")
	public WebElement chooseAction;

	@FindBy(xpath = ".//*[@id='li2']")
	public WebElement sendCampaignAction;

	@FindBy(xpath = ".//*[@id='li1']")
	public WebElement changeStatusAction;

	@FindBy(xpath = ".//*[@id='li3']")
	public WebElement sendEmailAction;

	@FindBy(xpath = ".//*[@id='li4']")
	public WebElement createTask;

	@FindBy(xpath = ".//*[@id='actionType']")
	public WebElement selectActionDrpDwn;

	@FindBy(xpath = ".//*[@id='asave' and @class='btn-style save-dialog close-dialog']")
	public WebElement associsteAndSave;
	
	@FindBy(xpath = ".//*[@id='asave' and @class='btn-style save-dialog ']")
	public WebElement associateAndSave_SendEmailCampaign_iFrame;

	@FindBy(xpath = ".//*[@id='aasave']")
	public WebElement create;

	@FindBy(xpath = ".//*[@id='subject']")
	public WebElement sendMailSubject;

	@FindBy(xpath = ".//*[@id='taskDesc']")
	public WebElement taskdesc;

	@FindBy(xpath = ".//*[@id='taskType1']")
	public WebElement taskType;

	@FindBy(xpath = ".//iframe[@id='ta_ifr']")
	public WebElement htmlFrame;

	@FindBy(xpath = "html/body")
	public WebElement mailText;

	@FindBy(xpath = ".//button[.='Create']")
	public WebElement createBtn;

	@FindBy(xpath = ".//label[@for='timelessTaskId']")
	public WebElement timeleassTask;

	public FSWorkflowsPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
}
