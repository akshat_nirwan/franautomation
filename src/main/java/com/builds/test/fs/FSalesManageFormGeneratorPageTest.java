package com.builds.test.fs;

import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.builds.uimaps.fs.AdminFranchiseSalesManageFormGeneratorPage;
import com.builds.uimaps.fs.FSLeadSummaryAddLeadPage;
import com.builds.uimaps.fs.FSLeadSummaryMergeLeadsPage;
import com.builds.uimaps.fs.FSLeadSummaryPrimaryInfoPage;
import com.builds.uimaps.fs.LeadSummaryUI;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class FSalesManageFormGeneratorPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	/* Harish Dwivedi TC_FS_FormGenerator_001_002_003_004 */

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FS_FormGenerator_001", testCaseDescription = "Verify in lead info > the tab is added and we are able to submit the tab with field info")
	private void verifyCustomTabLeadPrimaryInfo() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			String tabName = dataSet.get("tabDisplayName");
			String sectionName = dataSet.get("sectionValue");
			String fieldName = dataSet.get("displayName");
			String docTitle = dataSet.get("docTitle");

			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");

			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);

			fc.utobj().printTestStep("Add a Tab in Manage Form Generator ");
			AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();
			tabName = p1.addTabName(driver, tabName);

			boolean itabNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () , '" + tabName + "')]");
			if (itabNamePresent == false) {
				fc.utobj().throwsException("was not able to verify added Tab in Manage Form Generator");
			}
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'" + tabName + "')]"));

			fc.utobj().printTestStep("Add a Section in Manage Form Generator ");
			sectionName = p1.addSectionName(driver, sectionName);
			boolean issectionNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + sectionName + "')]");
			if (issectionNamePresent == false) {
				fc.utobj().throwsException("was not able to verify added sectionName in Manage Form Generator");
			}

			fc.utobj().printTestStep("Add Field Name in Manage Form Generator ");
			fieldName = p1.addFieldName(driver, sectionName, fieldName);
			boolean isFieldNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + fieldName + "')]");
			if (isFieldNamePresent == false) {
				fc.utobj().throwsException("was not able to verify added fieldName in Manage Form Generator");
			}

			fc.utobj().printTestStep("Add Document with Subject in Manage Form Generator ");
			docTitle = p1.addDocFieldName(driver, sectionName, fieldName, docTitle);

			boolean isdocTitlePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , '" + docTitle + "')]");
			if (isdocTitlePresent == false) {
				fc.utobj().throwsException("was not able to verify added Document in Manage Form Generator");
			}

			fc.utobj().printTestStep("Navigate to sales module Lead summary ");
			fc.sales().sales_common().fsModule(driver);
			Sales fs = new Sales();
			fs.leadManagement(driver);
			FSLeadSummaryPageTest fsPage = new FSLeadSummaryPageTest();
			fsPage.clickAddLeadLink(driver);
			fc.utobj().printTestStep("Add a lead and Go to Primary Info");

			FSLeadSummaryPrimaryInfoPageTest pInfoPage = new FSLeadSummaryPrimaryInfoPageTest();
			Map<String, String> leadInfo = pInfoPage.fillLeadBasicInfo(driver);
			Reporter.log("Lead Added with lead name : " + leadInfo.get("firstName") + " " + leadInfo.get("lastName"));

			// Test lead
			// fc.utobj().printTestStep(testCaseId, "Click On Lad details to go
			// Primary Info");
			// fc.utobj().clickElement(driver,fc.utobj().getElementByXpath(driver,".//a[contains(text(),'Lead
			// Testerotwr')]")));

			// tabName="NewTab1x06175190";
			// fieldName="Field1r06175212".toLowerCase();
			// docTitle ="Documentd06175268";
			String fieldNameAction = fieldName;
			fieldName = fieldName.toLowerCase();

			fc.utobj().printTestStep("Navigate to newly added Tab at Primary Info");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, "//*[@class='verTab']//*[text()='" + tabName + "']"));

			fc.utobj().sendKeys(driver,
					fc.utobj().getElementByXpath(driver, "//input[contains(@name, '_" + fieldName + "')]"), fieldName);
			fc.utobj().sendKeys(driver,
					fc.utobj().getElementByXpath(driver, "//input[contains(@name,'documentTitle')]"), docTitle);
			String fileName = fc.utobj().getFilePathFromTestData(dataSet.get("fileName"));
			fc.utobj().sendKeys(driver,
					fc.utobj().getElementByXpath(driver, "//input[contains(@name,'fsDocumentAttachment')]"), fileName);

			AdminFranchiseSalesManageFormGeneratorPage newp = new AdminFranchiseSalesManageFormGeneratorPage(driver);

			fc.utobj().clickElement(driver, newp.addDocumentSalePrimaryInfoBtn);

			fc.utobj().printTestStep("Verify The Added Field");
			boolean isFieldPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , '" + fieldName + "')]");
			if (isFieldPresent == false) {
				fc.utobj().throwsException("was not able to verify added Field");
			}

			fc.utobj().printTestStep("Verify The Added Document Title");
			boolean isTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , '" + docTitle + "')]");
			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to verify Document Title");
			}

			// again call to admin

			// String fieldNameAction = fieldName; //"Field1r06175212" ;
			// tabName="NewTab1x06175190";
			// fieldName="Field1r06175212".toLowerCase();
			// docTitle ="Documentd06175268";
			String fieldNameModify = fieldNameAction + "Modify";
			// String fieldNameAction = "Field1r06175212" ;//fieldName;//;

			AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);
			fc.utobj().printTestStep("Again Go to Admin > Sales > Manage Form Generator");
			adsales.manageFormGenerator(driver);
			fc.utobj().clickElement(driver, pobj.continueBtn);
			fc.utobj().printTestStep("Click on Tab newly added Tab");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'" + tabName + "')]"));

			actionImgOptionForFormBuilder(driver, fieldNameAction, "Modify");
			// String fieldNameModify = fieldNameAction+"Modify";
			String windowHandle = driver.getWindowHandle();
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.fieldDisplayName, fieldNameModify);
			fc.utobj().clickElement(driver, pobj.submitFieldCBoxButton);
			driver.switchTo().window(windowHandle);

			boolean isFielModifydNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + fieldNameModify + "')]");
			if (isFielModifydNamePresent == false) {
				fc.utobj().throwsException("was not able to verify modify fieldName in Manage Form Generator");
			}

			fc.utobj().printTestStep("Again Navigate to sales module Lead summary ");

			fc.sales().sales_common().fsModule(driver);
			fs = new Sales();
			fs.search(driver);

			fc.utobj().sendKeys(driver, fc.utobj().getElementByID(driver, "firstName"), "" + leadInfo.get("firstName"));
			fc.utobj().sendKeys(driver, fc.utobj().getElementByID(driver, "lastName"), "" + leadInfo.get("lastName"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByID(driver, "Submit"));
			// String firstName ="Test07144267";
			// String lastName ="Test07144267";

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='containsImage']/tbody/tr/td/span/a"));

			fc.utobj().printTestStep("Navigate to previsously added Tab at Primary Info");

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, "//*[@class='verTab']//*[text()='" + tabName + "']"));

			fc.utobj().printTestStep("Verify The Modified Field in Primary Info");
			boolean isModifiedNewFieldPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + fieldNameModify + "')]");
			if (isModifiedNewFieldPresent == false) {
				fc.utobj().throwsException("was not able to verify modifed Field in Primary Info");
			}

			fc.utobj().printTestStep("Again Go to Admin > Sales > Manage Form Generator");
			adsales.manageFormGenerator(driver);
			fc.utobj().clickElement(driver, pobj.continueBtn);
			fc.utobj().printTestStep("Click on Tab newly added Tab");

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'" + tabName + "')]"));

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='siteMainTable']/tbody/tr/td/form[2]/table/tbody/tr[11]/td[2]/table/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr[2]/td[2]/table/tbody/tr[1]/td/table/tbody/tr[2]/td[6]/a/img"));

			fc.utobj().printTestStep("Again Navigate to sales module Lead summary ");
			fc.sales().sales_common().fsModule(driver);
			fs = new Sales();
			fs.search(driver);

			fc.utobj().sendKeys(driver, fc.utobj().getElementByID(driver, "firstName"), "" + leadInfo.get("firstName"));
			fc.utobj().sendKeys(driver, fc.utobj().getElementByID(driver, "lastName"), "" + leadInfo.get("lastName"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByID(driver, "Submit"));

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='containsImage']/tbody/tr/td/span/a"));

			fc.utobj().printTestStep("Navigate to previsously added Tab at Primary Info");

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, "//*[@class='verTab']//*[text()='" + tabName + "']"));

			fc.utobj().printTestStep("Verify The Deactivated Field in Primary Info");
			boolean isDeactivatedPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + fieldNameModify + "')]");
			if (isDeactivatedPresent == false) {

			} else {
				fc.utobj().throwsException("deactivated Field also coming in Primary Info");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/* Harish Dwivedi TC_FS_FormGenerator_005 */

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FS_FormGenerator_005", testCaseDescription = "Verify in lead info > the tab is added and deleted info from primary info")
	private void verifyCustomTabDeletionLeadPrimaryInfo() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			String tabName = dataSet.get("tabDisplayName");
			String sectionName = dataSet.get("sectionValue");
			String fieldName = dataSet.get("displayName");
			// String docTitle = dataSet.get("docTitle");

			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");

			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);

			fc.utobj().printTestStep("Add a Tab in Manage Form Generator ");
			AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();
			tabName = p1.addTabName(driver, tabName);

			boolean itabNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () , '" + tabName + "')]");
			if (itabNamePresent == false) {
				fc.utobj().throwsException("was not able to verify added Tab in Manage Form Generator");
			}
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'" + tabName + "')]"));

			fc.utobj().printTestStep("Add a Section in Manage Form Generator ");
			sectionName = p1.addSectionName(driver, sectionName);
			boolean issectionNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + sectionName + "')]");
			if (issectionNamePresent == false) {
				fc.utobj().throwsException("was not able to verify added sectionName in Manage Form Generator");
			}

			fc.utobj().printTestStep("Add Field Name in Manage Form Generator ");
			fieldName = p1.addFieldName(driver, sectionName, fieldName);
			boolean isFieldNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + fieldName + "')]");
			if (isFieldNamePresent == false) {
				fc.utobj().throwsException("was not able to verify added fieldName in Manage Form Generator");
			}

			// fc.utobj().printTestStep(testCaseId, "Add Document with Subject
			// in Manage Form Generator ");
			// docTitle =
			// p1.addDocFieldName(driver,sectionName,fieldName,docTitle);

			// boolean isdocTitlePresent=fc.utobj().verifyCase(driver,
			// ".//td[contains(text () , '"+docTitle+"')]");
			// if (isdocTitlePresent==false) {
			// fc.utobj().throwsException("was not able to verify added Document
			// in Manage Form Generator");
			// }

			fc.utobj().printTestStep("Navigate to sales module Lead summary ");
			fc.sales().sales_common().fsModule(driver);
			Sales fs = new Sales();
			fs.leadManagement(driver);
			FSLeadSummaryPageTest fsPage = new FSLeadSummaryPageTest();
			fsPage.clickAddLeadLink(driver);
			fc.utobj().printTestStep("Add a lead and Go to Primary Info");

			FSLeadSummaryPrimaryInfoPageTest pInfoPage = new FSLeadSummaryPrimaryInfoPageTest();
			Map<String, String> leadInfo = pInfoPage.fillLeadBasicInfo(driver);
			Reporter.log("Lead Added with lead name : " + leadInfo.get("firstName") + " " + leadInfo.get("lastName"));

			String fieldNameAction = fieldName;
			fieldName = fieldName.toLowerCase();

			fc.utobj().printTestStep("Navigate to newly added Tab at Primary Info");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, "//*[@class='verTab']//*[text()='" + tabName + "']"));

			fc.utobj().sendKeys(driver,
					fc.utobj().getElementByXpath(driver, "//input[contains(@name, '_" + fieldName + "')]"), fieldName);
			// fc.utobj().sendKeys(driver,fc.utobj().getElementByXpath(driver,"//input[contains(@name,'documentTitle')]")),docTitle);
			// String fileName=fc.utobj().getFilePathFromTestData(config,
			// dataSet.get("fileName"));
			// fc.utobj().sendKeys(driver,fc.utobj().getElementByXpath(driver,"//input[contains(@name,'fsDocumentAttachment')]")),fileName);

			AdminFranchiseSalesManageFormGeneratorPage newp = new AdminFranchiseSalesManageFormGeneratorPage(driver);

			fc.utobj().clickElement(driver, newp.addDocumentSalePrimaryInfoBtn);

			fc.utobj().printTestStep("Verify The Added Field");
			boolean isFieldPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , '" + fieldName + "')]");
			if (isFieldPresent == false) {
				fc.utobj().throwsException("was not able to verify added Field");
			}

			// fc.utobj().printTestStep(testCaseId, "Verify The Added Document
			// Title");
			// boolean isTextPresent=fc.utobj().verifyCase(driver,
			// ".//td[contains(text () , '"+docTitle+"')]");
			// if (isTextPresent==false) {
			// fc.utobj().throwsException("was not able to verify Document
			// Title");
			// }

			// again call to admin

			// String fieldNameModify = fieldNameAction+"Modify";
			// String fieldNameAction = "Field1r06175212" ;//fieldName;//;

			AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);
			fc.utobj().printTestStep("Again Go to Admin > Sales > Manage Form Generator");
			adsales.manageFormGenerator(driver);
			fc.utobj().clickElement(driver, pobj.continueBtn);
			fc.utobj().printTestStep("Click on Tab newly added Tab");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'" + tabName + "')]"));

			boolean isdelatble = actionImgOptionForFormBuilderDeleteOption(driver, fieldNameAction, "Delete");

			if (isdelatble == false) {
				fc.utobj().printTestStep("Verfified that no delete option avabile in Admin form generator");
			} else {
				// fc.utobj().throwsException("was not able to verify delete
				// option fieldName in Manage Form Generator");
			}

			// actionImgOptionForFormBuilder(driver, fieldNameAction, "Modify");
			// String fieldNameModify = fieldNameAction+"Modify";
			// String windowHandle = driver.getWindowHandle();
			// fc.commonMethods().switch_cboxIframe_frameId(driver);
			// fc.utobj().sendKeys(driver,pobj.fieldDisplayName,fieldNameModify);
			// fc.utobj().clickElement(driver,pobj.submitFieldCBoxButton);
			// driver.switchTo().window(windowHandle);

			// boolean isFielModifydNamePresent=fc.utobj().verifyCase(driver,
			// ".//td[contains(text () , '"+fieldNameModify+"')]");
			// if (isFielModifydNamePresent==false) {
			// fc.utobj().throwsException("was not able to verify modify
			// fieldName in Manage Form Generator");
			// }

			fc.utobj().printTestStep("Again Navigate to sales module Lead summary ");
			fc.sales().sales_common().fsModule(driver);
			fs = new Sales();
			fs.search(driver);

			fc.utobj().sendKeys(driver, fc.utobj().getElementByID(driver, "firstName"), "" + leadInfo.get("firstName"));
			fc.utobj().sendKeys(driver, fc.utobj().getElementByID(driver, "lastName"), "" + leadInfo.get("lastName"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByID(driver, "Submit"));
			// String firstName ="Test07144267";
			// String lastName ="Test07144267";

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='containsImage']/tbody/tr/td/span/a"));

			fc.utobj().printTestStep("Navigate to previsously added Tab at Primary Info");

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, "//*[@class='verTab']//*[text()='" + tabName + "']"));

			fc.utobj().printTestStep("Navigate to delete info at Primary Info");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='moduleCustomTab']/tbody/tr/td[2]/table[2]/tbody/tr[1]/td/table/tbody/tr/td/table/tbody/tr/td[2]/span/a[2]/span"));
			fc.utobj().acceptAlertBox(driver);
			// Need verify here also

			fc.utobj().printTestStep("Again Go to Admin > Sales > Manage Form Generator");
			adsales.manageFormGenerator(driver);
			fc.utobj().clickElement(driver, pobj.continueBtn);
			fc.utobj().printTestStep("Click on Tab newly added Tab");

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'" + tabName + "')]"));

			// fc.utobj().clickElement(driver,fc.utobj().getElementByXpath(driver,".//*[@id='siteMainTable']/tbody/tr/td/form[2]/table/tbody/tr[11]/td[2]/table/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr[2]/td[2]/table/tbody/tr[1]/td/table/tbody/tr[2]/td[6]/a/img")));

			actionImgOptionForFormBuilder(driver, fieldNameAction, "Delete");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Again Navigate to sales module Lead summary ");
			fc.sales().sales_common().fsModule(driver);
			fs = new Sales();
			fs.search(driver);

			fc.utobj().sendKeys(driver, fc.utobj().getElementByID(driver, "firstName"), "" + leadInfo.get("firstName"));
			fc.utobj().sendKeys(driver, fc.utobj().getElementByID(driver, "lastName"), "" + leadInfo.get("lastName"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByID(driver, "Submit"));

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='containsImage']/tbody/tr/td/span/a"));

			fc.utobj().printTestStep("Navigate to previsously added Tab at Primary Info");

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, "//*[@class='verTab']//*[text()='" + tabName + "']"));

			fc.utobj().printTestStep("Verify The Deleted Field in Primary Info");

			boolean isMessagePage = fc.utobj().assertPageSource(driver, "No Form Fields found");
			if (isMessagePage == false) {
				fc.utobj().throwsException("Deleted Fields also coming in Primary Info");
			}

			fc.utobj().printTestStep("Again Go to Admin > Sales > Manage Form Generator");
			adsales.manageFormGenerator(driver);
			fc.utobj().clickElement(driver, pobj.continueBtn);
			fc.utobj().printTestStep("Click on Tab newly added Tab");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'" + tabName + "')]"));

			fc.utobj().printTestStep("Again Go to Delete Sectin in Manage Form Generator");
			fc.utobj().clickElement(driver, pobj.deleteSectionBtn);
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Again Go to Admin > Sales > Manage Form Generator");
			adsales.manageFormGenerator(driver);
			fc.utobj().clickElement(driver, pobj.continueBtn);

			fc.utobj().printTestStep("Click on Delete action for newly added Tab");
			actionImgOptionForFormBuilder(driver, tabName, "Delete");
			fc.utobj().acceptAlertBox(driver);

			itabNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () , '" + tabName + "')]");
			if (itabNamePresent == false) {
				fc.utobj().printTestStep("deleted tab now removed from admin");
			} else {
				fc.utobj().throwsException("was not able to verify deleted Tab in Manage Form Generator");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/* Harish Dwivedi TC_FS_FormGenerator_006 */

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FS_FormGenerator_006", testCaseDescription = "Verify in lead info > All Fields added in tab with field info")
	private void verifyCustomTabLeadPrimaryInfoAllTypeField() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			String tabName = dataSet.get("tabDisplayName");
			String sectionName = dataSet.get("sectionValue");
			String fieldName = dataSet.get("displayName");
			// String docTitle = dataSet.get("docTitle");

			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");

			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);

			fc.utobj().printTestStep("Add a Tab in Manage Form Generator ");
			AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();
			tabName = p1.addTabName(driver, tabName);

			boolean itabNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () , '" + tabName + "')]");
			if (itabNamePresent == false) {
				fc.utobj().throwsException("was not able to verify added Tab in Manage Form Generator");
			}
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'" + tabName + "')]"));

			fc.utobj().printTestStep("Add a Section in Manage Form Generator ");
			sectionName = p1.addSectionName(driver, sectionName);
			boolean issectionNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + sectionName + "')]");
			if (issectionNamePresent == false) {
				fc.utobj().throwsException("was not able to verify added sectionName in Manage Form Generator");
			}

			fc.utobj().printTestStep("Add Field Name in Manage Form Generator ");

			String fieldNameText = fieldName + "Text";
			fieldNameText = p1.addFieldNameAllType(driver, sectionName, fieldNameText, "Text");

			String fieldNameTextArea = fieldName + "TextArea";
			fieldNameTextArea = p1.addFieldNameAllType(driver, sectionName, fieldNameTextArea, "Text Area");

			String fieldNameDate = fieldName + "Date";
			fieldNameDate = p1.addFieldNameAllType(driver, sectionName, fieldNameDate, "Date");

			String fieldNameDropdown = fieldName + "Dropdown";
			fieldNameDropdown = p1.addFieldNameAllType(driver, sectionName, fieldNameDropdown, "Drop-down");

			String fieldNameRadio = fieldName + "Radio";
			fieldNameRadio = p1.addFieldNameAllType(driver, sectionName, fieldNameRadio, "Radio");

			String fieldNameNumeric = fieldName + "Numeric";
			fieldNameNumeric = p1.addFieldNameAllType(driver, sectionName, fieldNameNumeric, "Numeric");

			String fieldNameCheckbox = fieldName + "Checkbox";
			fieldNameCheckbox = p1.addFieldNameAllType(driver, sectionName, fieldNameCheckbox, "Checkbox");

			String fieldNameMultiSelectDropdown = fieldName + "MultiSelectDropdown";
			fieldNameMultiSelectDropdown = p1.addFieldNameAllType(driver, sectionName, fieldNameMultiSelectDropdown,
					"Multi Select Drop-down");

			String fieldNameDocument = fieldName + "Document";
			fieldNameDocument = p1.addFieldNameAllType(driver, sectionName, fieldNameDocument, "Document");

			boolean isFieldNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + fieldNameText + "')]");
			if (isFieldNamePresent == false) {
				fc.utobj().throwsException("was not able to verify added fieldName in Manage Form Generator");
			}

			fc.utobj().printTestStep("Navigate to sales module Lead summary ");
			fc.sales().sales_common().fsModule(driver);
			Sales fs = new Sales();
			fs.leadManagement(driver);
			FSLeadSummaryPageTest fsPage = new FSLeadSummaryPageTest();
			fsPage.clickAddLeadLink(driver);
			fc.utobj().printTestStep("Add a lead and Go to Primary Info");

			FSLeadSummaryPrimaryInfoPageTest pInfoPage = new FSLeadSummaryPrimaryInfoPageTest();
			Map<String, String> leadInfo = pInfoPage.fillLeadBasicInfo(driver);
			Reporter.log("Lead Added with lead name : " + leadInfo.get("firstName") + " " + leadInfo.get("lastName"));

			// Test lead
			// fc.utobj().printTestStep(testCaseId, "Click On Lad details to go
			// Primary Info");
			// fc.utobj().clickElement(driver,fc.utobj().getElementByXpath(driver,".//a[contains(text(),'Lead
			// Testerotwr')]")));

			// tabName="NewTab1x06175190";
			// fieldName="Field1r06175212".toLowerCase();
			// docTitle ="Documentd06175268";
			// Test Envoiumrnet
			// tabName ="Sociald08190584";
			// fc.utobj().printTestStep(testCaseId, "Navigate to sales module
			// Lead summary ");
			// fc.sales().sales_common().fsModule(fc.sales(), driver);
			// fs = new FsModulePageTest(driver);
			// fs.leadManagement(driver);
			// fc.utobj().printTestStep(testCaseId, "Click On Lad details to go
			// Primary Info");
			// fc.utobj().clickElement(driver,fc.utobj().getElementByXpath(driver,".//a[contains(text(),'"+leadInfo.get("firstName")+"
			// "+leadInfo.get("firstName")+"')]")));
			fieldNameText = fieldNameText.toLowerCase();
			fieldNameTextArea = fieldNameTextArea.toLowerCase();
			fieldNameDate = fieldNameDate.toLowerCase();
			fieldNameDropdown = fieldNameDropdown.toLowerCase();
			fieldNameRadio = fieldNameRadio.toLowerCase();
			fieldNameNumeric = fieldNameNumeric.toLowerCase();
			fieldNameCheckbox = fieldNameCheckbox.toLowerCase();
			fieldNameMultiSelectDropdown = fieldNameMultiSelectDropdown.toLowerCase();
			fieldNameDocument = fieldNameDocument.toLowerCase();

			String fieldNameAction = fieldNameText;
			fieldNameText = fieldNameText.toLowerCase();

			fc.utobj().printTestStep("Navigate to newly added Tab at Primary Info");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, "//*[@class='verTab']//*[text()='" + tabName + "']"));

			fc.utobj().sendKeys(driver,
					fc.utobj().getElementByXpath(driver, "//input[contains(@name, '_" + fieldNameText + "')]"),
					fieldNameText); // Work
			fc.utobj().sendKeys(driver,
					fc.utobj().getElementByXpath(driver, "//*[contains(@id, '_" + fieldNameTextArea + "')]"),
					fieldNameTextArea); // work
			fc.utobj().sendKeys(driver,
					fc.utobj().getElementByXpath(driver, "//input[contains(@name, '_" + fieldNameDate + "')]"),
					"02/08/2018"); // work
			fc.utobj().selectDropDownByVisibleText(driver,
					fc.utobj().getElementByXpath(driver, "//*[contains(@name, '_" + fieldNameDropdown + "')]"),
					"DropValue1"); // work
			List<WebElement> rdBtn_Field = driver
					.findElements(By.xpath("//*[contains(@name, '_" + fieldNameRadio + "')]")); // work
			fc.utobj().clickRadioButton(driver, rdBtn_Field, "1"); // work
			fc.utobj().sendKeys(driver,
					fc.utobj().getElementByXpath(driver, "//input[contains(@name, '_" + fieldNameNumeric + "')]"),
					"100"); // work
			List<WebElement> checkbox = driver
					.findElements(By.xpath("//*[contains(@name, '_" + fieldNameCheckbox + "')]"));
			((WebElement) checkbox.get(0)).click();
			WebElement element = driver
					.findElement(By.xpath(".//div[contains(@id , '" + fieldNameMultiSelectDropdown + "')]"));
			fc.utobj().setToDefault(driver, element);
			WebElement elementInput = element.findElement(By.xpath("./div/div/input"));
			fc.utobj().selectValFromMultiSelect(driver, element, elementInput, "Multi1");

			AdminFranchiseSalesManageFormGeneratorPage newp = new AdminFranchiseSalesManageFormGeneratorPage(driver);
			String fileName = fc.utobj().getFilePathFromTestData(dataSet.get("fileName")); // "C:\\images.jpg"
																							// ;//
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, "//input[@type='file']"), fileName);

			fc.utobj().clickElement(driver, newp.addDocumentSalePrimaryInfoBtn);

			fc.utobj().printTestStep("Verify The Added Field");
			boolean isfieldNameText = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + fieldNameText + "')]");
			if (isfieldNameText == false) {
				fc.utobj().throwsException("was not able to verify Text Field");
			}
			boolean isfieldNameTextArea = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + fieldNameTextArea + "')]");
			if (isfieldNameTextArea == false) {
				fc.utobj().throwsException("was not able to verify TextArea Field");
			}
			boolean isfieldNameDate = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , '02/08/2018')]");
			if (isfieldNameDate == false) {
				fc.utobj().throwsException("was not able to verify Date Field");
			}
			boolean isfieldNameDropdown = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , 'DropValue1')]");
			if (isfieldNameDropdown == false) {
				fc.utobj().throwsException("was not able to verify DropDown Field");
			}

			boolean isfieldNameRadio = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , 'Radio1')]");
			if (isfieldNameRadio == false) {
				fc.utobj().throwsException("was not able to verify Radio Field");
			}
			boolean isfieldNameNumeric = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , '100')]");
			if (isfieldNameNumeric == false) {
				fc.utobj().throwsException("was not able to verify numric Field");
			}
			boolean isfieldNameCheckbox = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , 'Checkbox1')]");
			if (isfieldNameCheckbox == false) {
				fc.utobj().throwsException("was not able to verify checkBox Field");
			}
			boolean isfieldNameMultiSelectDropdown = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , 'Multi1')]");
			if (isfieldNameMultiSelectDropdown == false) {
				fc.utobj().throwsException("was not able to verify Multi Select Field");
			}
			boolean isfileName = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () , '.jpg')]");
			if (isfileName == false) {
				// fc.utobj().throwsException("was not able to verify File Type
				// Field");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/* Harish Dwivedi */

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FS_FormGenerator_008", testCaseDescription = "Verify Add fields from Form Generator in Web Form")
	private void verifyCustomFieldWebform() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			String tabName = dataSet.get("tabDisplayName");
			String sectionName = dataSet.get("sectionValue");
			// String fieldName = dataSet.get("displayName");
			// String docTitle = dataSet.get("docTitle");
			String fieldName = fc.utobj().generateTestData(dataSet.get("displayName"));
			AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");

			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);

			AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);
			fc.utobj().clickElement(driver, pobj.continueBtn);

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, "//*[@id='tabdetails']//*[text()='Primary Info']"));

			fc.utobj().clickElement(driver, pobj.PrimaryInfoAddNewFied);
			fieldName = p1.addFieldNamePrimaryInfo(driver, sectionName, fieldName);

			boolean isFieldNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + fieldName + "')]");
			if (isFieldNamePresent == false) {
				fc.utobj().throwsException("was not able to verify added fieldName in Manage Form Generator");
			}

			fc.utobj().printTestStep("Navigate to Manage Web Form Generator");
			adsales.manageWebFormGenerator(driver);
			String newFormUrl = p1.addLeadFromWebForm(driver, fieldName);
			String LeadName = newFormUrl.substring(newFormUrl.lastIndexOf("/"), newFormUrl.length());
			// String formName ="wb10140831";
			// String fieldName="Default10140612u10140649";
			// adsales.manageWebFormGenerator( driver);
			// fc.utobj().printTestStep(testCaseId, "Click to newly a Form in
			// Manage web Generator ");
			// boolean isformName=fc.utobj().verifyCase(driver,
			// ".//*[@id='openModify'][contains(text () , '"+formName+"')]");
			// if (isformName==false) {
			// fc.utobj().throwsException("was not able to verify added
			// fieldName in Manage Form Generator");
			// }
			// fc.utobj().clickElement(driver,fc.utobj().getElementByXpath(driver,".//*[@id='openModify'][contains(text
			// () , '"+formName+"')]")));
			// fc.utobj().clickElement(driver,
			// fc.utobj().getElementByID(driver,"detailsNextBtn")));
			// boolean isformNameavaible=fc.utobj().verifyCase(driver,
			// ".//*[@id='field_label'][contains(text () , '"+fieldName+"')]");
			// if (isformNameavaible==false) {
			// fc.utobj().throwsException("was not able to verify added
			// fieldName in Manage Form Generator");
			// }

			// String
			// newFormUrl="http://192.168.9.57:8080/fc96/extforms/wb10140831";

			// Make New Instances of Window or driver

			if (FranconnectUtil.config.get("browserName").equalsIgnoreCase("Chrome")) {

				System.setProperty("webdriver.chrome.driver",
						FranconnectUtil.config.get("inputDirectory") + "\\exe\\chromedriver.exe");
				ChromeOptions options = new ChromeOptions();
				options.addArguments("disable-popup-blocking");
				DesiredCapabilities capabilities = DesiredCapabilities.chrome();
				capabilities.setCapability(ChromeOptions.CAPABILITY, options);
				// Browser Logs
				LoggingPreferences logPrefs = new LoggingPreferences();
				logPrefs.enable(LogType.BROWSER, Level.ALL);
				capabilities.setCapability(CapabilityType.LOGGING_PREFS, logPrefs);
				WebDriver driver1 = new ChromeDriver(capabilities);

				driver1.get(newFormUrl);

				// verify
				fc.utobj().sendKeys(driver1, driver1.findElement(By.xpath(".//*[@id='firstName']")), "Test" + LeadName);
				fc.utobj().sendKeys(driver1, driver1.findElement(By.xpath(".//*[@id='lastName']")), "Test" + LeadName);
				fc.utobj().sendKeys(driver1, driver1.findElement(By.xpath(".//*[@id='emailID']")), "Test@test.com");
				fc.utobj().sendKeys(driver1,
						driver1.findElement(By.xpath(".//input[contains(@name, '" + fieldName.toLowerCase() + "')]")),
						"" + "ValuesPreset");
				fc.utobj().clickElement(driver1, driver1.findElement(By.xpath(".//*[@id='submitButton']")));

				boolean isSubmitted = fc.utobj().assertPageSource(driver1,
						"Thank you for submitting your information, we will get back to you shortly.");

				if (isSubmitted == false) {
					fc.utobj().throwsException("Unable to submit lead");
				}

				driver1.quit();

			} else if (FranconnectUtil.config.get("browserName").equalsIgnoreCase("firefox")) {

			}

			Sales fs = new Sales();
			fc.utobj().printTestStep("Again Navigate to sales module Lead summary ");
			fc.sales().sales_common().fsModule(driver);
			fs = new Sales();
			fs.search(driver);

			fc.utobj().sendKeys(driver, fc.utobj().getElementByID(driver, "firstName"), "Test" + LeadName);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByID(driver, "lastName"), "Test" + LeadName);
			fc.utobj().clickElement(driver, fc.utobj().getElementByID(driver, "Submit"));

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='containsImage']/tbody/tr/td/span/a"));

			boolean isfieldNameText = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , 'ValuesPreset')]");
			if (isfieldNameText == false) {
				fc.utobj().throwsException("was not able to verify added Text Field");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/* Harish Dwivedi TC_FS_FormGenerator_009 */

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FS_FormGenerator_009", testCaseDescription = "Verify that mandatory fields are not getting saved with blank info in new added Tab")
	private void verifyCustomFieldMandatoryLeadPrimaryInfo() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			String tabName = dataSet.get("tabDisplayName");
			String sectionName = dataSet.get("sectionValue");
			String fieldName = dataSet.get("displayName");
			// String docTitle = dataSet.get("docTitle");

			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");

			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);

			fc.utobj().printTestStep("Add a Tab in Manage Form Generator ");
			AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();
			tabName = p1.addTabName(driver, tabName);

			boolean itabNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () , '" + tabName + "')]");
			if (itabNamePresent == false) {
				fc.utobj().throwsException("was not able to verify added Tab in Manage Form Generator");
			}
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'" + tabName + "')]"));

			fc.utobj().printTestStep("Add a Section in Manage Form Generator ");
			sectionName = p1.addSectionName(driver, sectionName);
			boolean issectionNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + sectionName + "')]");
			if (issectionNamePresent == false) {
				fc.utobj().throwsException("was not able to verify added sectionName in Manage Form Generator");
			}

			fc.utobj().printTestStep("Add Field Name in Manage Form Generator ");
			fieldName = p1.addFieldNameMandatory(driver, sectionName, fieldName);
			boolean isFieldNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + fieldName + "')]");
			if (isFieldNamePresent == false) {
				fc.utobj().throwsException("was not able to verify added fieldName in Manage Form Generator");
			}

			AdminFranchiseSalesManageFormGeneratorPage newp = new AdminFranchiseSalesManageFormGeneratorPage(driver);
			fc.utobj().printTestStep("Click on mandatory link in Manage Form Generator ");
			fc.utobj().clickElement(driver, newp.mandatoryButton);

			fc.utobj().printTestStep("Navigate to sales module Lead summary ");
			fc.sales().sales_common().fsModule(driver);
			Sales fs = new Sales();
			fs.leadManagement(driver);
			FSLeadSummaryPageTest fsPage = new FSLeadSummaryPageTest();
			fsPage.clickAddLeadLink(driver);
			fc.utobj().printTestStep("Add a lead and Go to Primary Info");

			FSLeadSummaryPrimaryInfoPageTest pInfoPage = new FSLeadSummaryPrimaryInfoPageTest();
			Map<String, String> leadInfo = pInfoPage.fillLeadBasicInfo(driver);
			Reporter.log("Lead Added with lead name : " + leadInfo.get("firstName") + " " + leadInfo.get("lastName"));

			fieldName = fieldName.toLowerCase();

			fc.utobj().printTestStep("Navigate to newly added Tab at Primary Info");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, "//*[@class='verTab']//*[text()='" + tabName + "']"));

			fc.utobj().printTestStep("Verify The blank Field");
			fc.utobj().sendKeys(driver,
					fc.utobj().getElementByXpath(driver, "//input[contains(@name, '_" + fieldName + "')]"), "");
			fc.utobj().clickElement(driver, newp.tabLeadLabelsubmit);

			try {
				fc.utobj().acceptAlertBox(driver);
			} catch (Exception e) {
				fc.utobj().throwsException("was able to submit value blank info");
			}

			fc.utobj().printTestStep("Verify The submit with Field value");
			fc.utobj().sendKeys(driver,
					fc.utobj().getElementByXpath(driver, "//input[contains(@name, '_" + fieldName + "')]"), fieldName);

			fc.utobj().clickElement(driver, newp.tabLeadLabelsubmit);

			fc.utobj().printTestStep("Verify The Added Field");
			boolean isFieldPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , '" + fieldName + "')]");
			if (isFieldPresent == false) {
				fc.utobj().throwsException("was not able to verify added Field");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/* Harish Dwivedi TC_FS_FormGenerator_010 */

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FS_FormGenerator_010", testCaseDescription = "Verify that mandatory fields are not getting saved with blank info in new added Tab")
	private void verifyCustomFieldinarchiveRestoreLeadPrimaryInfo() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			String tabName = dataSet.get("tabDisplayName");
			String sectionName = dataSet.get("sectionValue");
			String fieldName = dataSet.get("displayName");
			// String docTitle = dataSet.get("docTitle");

			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");

			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);

			fc.utobj().printTestStep("Add a Tab in Manage Form Generator ");
			AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();
			tabName = p1.addTabName(driver, tabName);

			boolean itabNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () , '" + tabName + "')]");
			if (itabNamePresent == false) {
				fc.utobj().throwsException("was not able to verify added Tab in Manage Form Generator");
			}
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'" + tabName + "')]"));

			fc.utobj().printTestStep("Add a Section in Manage Form Generator ");
			sectionName = p1.addSectionName(driver, sectionName);
			boolean issectionNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + sectionName + "')]");
			if (issectionNamePresent == false) {
				fc.utobj().throwsException("was not able to verify added sectionName in Manage Form Generator");
			}

			fc.utobj().printTestStep("Add Field Name in Manage Form Generator ");
			fieldName = p1.addFieldNameMandatory(driver, sectionName, fieldName);
			boolean isFieldNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + fieldName + "')]");
			if (isFieldNamePresent == false) {
				fc.utobj().throwsException("was not able to verify added fieldName in Manage Form Generator");
			}

			AdminFranchiseSalesManageFormGeneratorPage newp = new AdminFranchiseSalesManageFormGeneratorPage(driver);
			fc.utobj().printTestStep("Click on mandatory link in Manage Form Generator ");
			// fc.utobj().clickElement(driver,newp.mandatoryButton);

			fc.utobj().printTestStep("Navigate to sales module Lead summary ");
			fc.sales().sales_common().fsModule(driver);
			Sales fs = new Sales();
			fs.leadManagement(driver);
			FSLeadSummaryPageTest fsPage = new FSLeadSummaryPageTest();
			fsPage.clickAddLeadLink(driver);
			fc.utobj().printTestStep("Add a lead and Go to Primary Info");

			FSLeadSummaryPrimaryInfoPageTest pInfoPage = new FSLeadSummaryPrimaryInfoPageTest();
			Map<String, String> leadInfo = pInfoPage.fillLeadBasicInfo(driver);
			Reporter.log("Lead Added with lead name : " + leadInfo.get("firstName") + " " + leadInfo.get("lastName"));

			fieldName = fieldName.toLowerCase();

			fc.utobj().printTestStep("Navigate to newly added Tab at Primary Info");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, "//*[@class='verTab']//*[text()='" + tabName + "']"));

			fc.utobj().printTestStep("Verify The submit with Field value");
			fc.utobj().sendKeys(driver,
					fc.utobj().getElementByXpath(driver, "//input[contains(@name, '_" + fieldName + "')]"), fieldName);
			fc.utobj().clickElement(driver, newp.tabLeadLabelsubmit);

			fc.utobj().printTestStep("Verify The Added Field");
			boolean isFieldPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , '" + fieldName + "')]");
			if (isFieldPresent == false) {
				fc.utobj().throwsException("was not able to verify added Field");
			}

			fc.utobj().printTestStep("Navigate to sales search Lead summary ");
			fc.sales().sales_common().fsModule(driver);
			fs = new Sales();
			fs.search(driver);

			fc.utobj().sendKeys(driver, fc.utobj().getElementByID(driver, "firstName"), "" + leadInfo.get("firstName"));
			fc.utobj().sendKeys(driver, fc.utobj().getElementByID(driver, "lastName"), "" + leadInfo.get("lastName"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByID(driver, "Submit"));
			// String firstName ="Test07144267";
			// String lastName ="Test07144267";

			fc.utobj().printTestStep("Click to Archive Lead Lead summary ");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//input[@name='checkBox']"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='fsLeadTable']/tbody/tr/td[2]/form/table[2]/tbody/tr[6]/td/table/tbody/tr/td/input[contains (@name,'archive')]"));
			fc.utobj().acceptAlertBox(driver);
			String windowHandle = driver.getWindowHandle();
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, newp.notesArchive, "Leads Archived!");
			fc.utobj().clickElement(driver, newp.tabLeadLabelsubmit);
			fc.utobj().clickElement(driver, newp.closeBtn);
			driver.switchTo().window(windowHandle);

			fc.utobj().printTestStep("Navigate to sales archive search Lead summary ");
			fc.sales().sales_common().fsModule(driver);
			fs = new Sales();
			fs.search(driver);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='siteMainTable']/tbody/tr/td/table/tbody/tr[6]/td/table/tbody/tr/td[2]/input[2]"));
			fc.utobj().sendKeys(driver, fc.utobj().getElementByID(driver, "firstName"), "" + leadInfo.get("firstName"));
			fc.utobj().sendKeys(driver, fc.utobj().getElementByID(driver, "lastName"), "" + leadInfo.get("lastName"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByID(driver, "Submit"));

			String leadName = leadInfo.get("firstName") + " " + leadInfo.get("lastName");
			boolean iLeadNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () , '" + leadName + "')]");
			if (iLeadNamePresent == false) {
				fc.utobj().throwsException("was not able to verify lead in Archieve Lead Summary");
			}
			fc.utobj().printTestStep("Click to Active Lead from summary ");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//input[@name='checkBox']"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='siteMainTable']/tbody/tr/td/table/tbody/tr[6]/td[2]/table/tbody/tr/td/input[contains (@name,'active')]"));
			fc.utobj().acceptAlertBox(driver);
			windowHandle = driver.getWindowHandle();
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, newp.notesArchive, "Leads Un Archived!");
			fc.utobj().clickElement(driver, newp.tabLeadLabelsubmit);
			fc.utobj().clickElement(driver, newp.closeBtn);
			driver.switchTo().window(windowHandle);

			fc.utobj().printTestStep("Navigate to sales search Lead summary ");
			fc.sales().sales_common().fsModule(driver);
			fs = new Sales();
			fs.search(driver);

			fc.utobj().sendKeys(driver, fc.utobj().getElementByID(driver, "firstName"), "" + leadInfo.get("firstName"));
			fc.utobj().sendKeys(driver, fc.utobj().getElementByID(driver, "lastName"), "" + leadInfo.get("lastName"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByID(driver, "Submit"));

			leadName = leadInfo.get("firstName") + " " + leadInfo.get("lastName");
			iLeadNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () , '" + leadName + "')]");
			if (iLeadNamePresent == false) {
				fc.utobj().throwsException("was not able to verify lead in active Lead Summary");
			}

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'" + leadName + "')]"));
			fc.utobj().printTestStep("Navigate to newly added Tab at Primary Info");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, "//*[@class='verTab']//*[text()='" + tabName + "']"));
			fc.utobj().printTestStep("Verify The Added Field");
			isFieldPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , '" + fieldName + "')]");
			if (isFieldPresent == false) {
				fc.utobj().throwsException("was not able to verify added tab Field");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/* Harish Dwivedi TC_FS_FormGenerator_011 */

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FS_FormGenerator_011", testCaseDescription = "Verify that the information is not getting lost in merged lead")
	private void verifyCustomFieldinMergeLeadPrimaryInfo() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		int recordCount = 0;

		try {
			driver = fc.loginpage().login(driver);

			String tabName = dataSet.get("tabDisplayName");
			String sectionName = dataSet.get("sectionValue");
			String fieldName = dataSet.get("displayName");
			// String docTitle = dataSet.get("docTitle");
			String firstName = dataSet.get("firstName");
			String lastName = dataSet.get("lastName");

			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");

			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);

			fc.utobj().printTestStep("Add a Tab in Manage Form Generator ");
			AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();
			tabName = p1.addTabName(driver, tabName);

			boolean itabNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () , '" + tabName + "')]");
			if (itabNamePresent == false) {
				fc.utobj().throwsException("was not able to verify added Tab in Manage Form Generator");
			}
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'" + tabName + "')]"));

			fc.utobj().printTestStep("Add a Section in Manage Form Generator ");
			sectionName = p1.addSectionName(driver, sectionName);
			boolean issectionNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + sectionName + "')]");
			if (issectionNamePresent == false) {
				fc.utobj().throwsException("was not able to verify added sectionName in Manage Form Generator");
			}

			fc.utobj().printTestStep("Add Field Name in Manage Form Generator ");
			fieldName = p1.addFieldNameMandatory(driver, sectionName, fieldName);
			boolean isFieldNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + fieldName + "')]");
			if (isFieldNamePresent == false) {
				fc.utobj().throwsException("was not able to verify added fieldName in Manage Form Generator");
			}

			AdminFranchiseSalesManageFormGeneratorPage newp = new AdminFranchiseSalesManageFormGeneratorPage(driver);
			fc.utobj().printTestStep("Click on mandatory link in Manage Form Generator ");
			// fc.utobj().clickElement(driver,newp.mandatoryButton);

			String randomChar = fc.utobj().generateRandomChar();

			String lastNameUnique = lastName + randomChar;
			String leadNameSearch = lastNameUnique;

			String leadName = firstName + " " + lastNameUnique;

			fc.utobj().printTestStep("Navigate to sales module Lead summary ");
			// fc.sales().sales_common().fsModule(fc.sales(), driver);
			Sales fs = new Sales();
			// fs.leadManagement(driver);
			// FSLeadSummaryPageTest fsPage = new FSLeadSummaryPageTest();
			// fsPage.clickAddLeadLink(driver);
			fc.utobj().printTestStep("Add two leads");

			Map<String, String> leadName1 = addLeadLeadSummaryWithLeadName(driver, firstName, lastNameUnique);
			Map<String, String> leadName2 = addLeadLeadSummaryWithLeadName(driver, firstName, lastNameUnique);

			// FSLeadSummaryPrimaryInfoPageTest pInfoPage = new
			// FSLeadSummaryPrimaryInfoPageTest();
			// Map<String, String> leadInfo =
			// pInfoPage.fillLeadBasicInfo(driver,config);
			// Reporter.log("Lead Added with lead name :
			// "+leadInfo.get("firstName")+" "+leadInfo.get("lastName"));

			fieldName = fieldName.toLowerCase();

			fc.utobj().printTestStep("Navigate to newly added Tab at Primary Info");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, "//*[@class='verTab']//*[text()='" + tabName + "']"));

			fc.utobj().printTestStep("Verify The submit with Field value");
			fc.utobj().sendKeys(driver,
					fc.utobj().getElementByXpath(driver, "//input[contains(@name, '_" + fieldName + "')]"), fieldName);
			fc.utobj().clickElement(driver, newp.tabLeadLabelsubmit);

			fc.utobj().printTestStep("Verify The Added Field");
			boolean isFieldPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , '" + fieldName + "')]");
			if (isFieldPresent == false) {
				fc.utobj().throwsException("was not able to verify added Field");
			}

			// fs.leadManagement(driver);
			// fsPage.clickAddLeadLink(driver);
			// Map<String, String> leadInfo2 =
			// pInfoPage.fillLeadBasicInfo(driver,config);
			// Reporter.log("Lead Added with lead name :
			// "+leadInfo2.get("firstName")+" "+leadInfo2.get("lastName"));

			FSSearchPageTest p3 = new FSSearchPageTest();
			p3.searchByLeadName(driver, firstName, leadNameSearch);
			fc.utobj().clickElement(driver, newp.checkBoxAll);
			fc.utobj().printTestStep("Merge these two leads");

			fc.utobj().selectActionMenuItems(driver, "Merge Leads");
			FSLeadSummaryMergeLeadsPage pobj2 = new FSLeadSummaryMergeLeadsPage(driver);
			fc.utobj().clickElement(driver, pobj2.mergeBtn);

			fc.utobj().printTestStep("Navigate to newly added Tab at Primary Info");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, "//*[@class='verTab']//*[text()='" + tabName + "']"));
			isFieldPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , '" + fieldName + "')]");
			if (isFieldPresent == false) {
				fc.utobj().throwsException("was not able to verify added Field");
			}
			fc.utobj().assertPageSource(driver, "Lead added through merging of two leads");
			p3.searchByLeadName(driver, firstName, lastNameUnique);
			fc.utobj().printTestStep("Search if the leads are merged or not");

			recordCount = fc.utobj().findSingleLinkPartialText(driver, lastNameUnique);
			if (recordCount > 1) {
				fc.utobj().throwsException("More than one lead found after merge - record mismatch!");
			} else if (recordCount < 1) {
				fc.utobj().throwsException("No leads found after merge - record mismatch!");
			}
			boolean isLeadMerged = fc.utobj().assertLinkPartialText(driver, lastNameUnique);
			if (isLeadMerged == false) {
				fc.utobj().throwsException("Leads not found in archived section!");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/* Harish Dwivedi TC_FS_FormGenerator_007 */

	/* Harish Dwivedi */

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FS_FormGenerator_007", testCaseDescription = "Verify Add Remarks, Task and Call with added details from Form Generator Lead Primary Info.")
	private void verifyCustomFieldTaskCallRemark() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			String tabName = dataSet.get("tabDisplayName");
			String sectionName = dataSet.get("sectionValue");
			String fieldName = dataSet.get("displayName");
			// String docTitle = dataSet.get("docTitle");
			// String
			// fieldName=fc.utobj().generateTestData(dataSet.get("displayName"));
			AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");

			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);

			AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);
			fc.utobj().clickElement(driver, pobj.continueBtn);

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, "//*[@id='tabdetails']//*[text()='Remark']"));

			fc.utobj().clickElement(driver, pobj.PrimaryInfoAddNewFied);
			String fieldNameRemark = p1.addFieldNamePrimaryInfo(driver, sectionName, fieldName);

			boolean isFieldNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + fieldNameRemark + "')]");
			if (isFieldNamePresent == false) {
				fc.utobj().throwsException("was not able to verify added fieldName in Manage Form Generator");
			}

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='siteMainTable']//*[text()='Manage Form Generator']"));

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, "//*[@id='tabdetails']//*[text()='Task']"));
			fc.utobj().clickElement(driver, pobj.PrimaryInfoAddNewFied);
			String fieldNameTask = p1.addFieldNamePrimaryInfo(driver, sectionName, fieldName);

			isFieldNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , '" + fieldNameTask + "')]");
			if (isFieldNamePresent == false) {
				fc.utobj().throwsException("was not able to verify added fieldName in Manage Form Generator");
			}

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='siteMainTable']//*[text()='Manage Form Generator']"));

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, "//*[@id='tabdetails']//*[text()='Call']"));
			fc.utobj().clickElement(driver, pobj.PrimaryInfoAddNewFied);
			String fieldNameCall = p1.addFieldNamePrimaryInfo(driver, sectionName, fieldName);

			isFieldNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , '" + fieldNameCall + "')]");
			if (isFieldNamePresent == false) {
				fc.utobj().throwsException("was not able to verify added fieldName in Manage Form Generator");
			}

			// Now add lead

			fc.utobj().printTestStep("Navigate to sales module Lead summary ");
			fc.sales().sales_common().fsModule(driver);
			Sales fs = new Sales();
			fs.leadManagement(driver);
			FSLeadSummaryPageTest fsPage = new FSLeadSummaryPageTest();
			fsPage.clickAddLeadLink(driver);
			fc.utobj().printTestStep("Add a lead and Go to Primary Info");

			FSLeadSummaryPrimaryInfoPageTest pInfoPage = new FSLeadSummaryPrimaryInfoPageTest();
			Map<String, String> leadInfo = pInfoPage.fillLeadBasicInfo(driver);
			Reporter.log("Lead Added with lead name : " + leadInfo.get("firstName") + " " + leadInfo.get("lastName"));

			fc.utobj().printTestStep("Log A Call");
			fc.utobj().clickElement(driver, pobj.logACallLinks);
			String windowHandle = driver.getWindowHandle();
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String taskSubject = "Call Subject ";
			taskSubject = fc.utobj().generateTestData(taskSubject);
			fc.utobj().sendKeys(driver, pobj.callSubject, taskSubject);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver,
					"//input[contains(@name, '_" + fieldName.toLowerCase() + "')]"), fieldName);
			fc.utobj().clickElement(driver, pobj.submitBtn);
			fc.utobj().clickElement(driver, pobj.callConfirmScheduleTaskNoBtn);
			driver.switchTo().window(windowHandle);

			fc.utobj().clickPartialLinkText(driver, taskSubject);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().printTestStep("Verify A Call Detailed History Frame");

			boolean isCallSubjectPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'" + taskSubject + "')]");
			if (isCallSubjectPresent == false) {
				fc.utobj().throwsException("was not able to verify Call Subject");
			}
			isFieldNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , '" + fieldName + "')]");
			if (isFieldNamePresent == false) {
				fc.utobj().throwsException("was not able to verify added fieldName in Manage Form Generator");
			}

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='dataTbl']//input[@type='button']"));

			fc.utobj().printTestStep("Add Remarks");

			fc.utobj().clickPartialLinkText(driver, "Add Remarks");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			String remarks = fc.utobj().generateTestData("Test Remarks");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByID(driver, "remarks"), remarks);
			fc.utobj()
					.sendKeys(driver,
							fc.utobj().getElementByXpath(driver,
									"//input[contains(@name, '_" + fieldNameRemark.toLowerCase() + "')]"),
							fieldNameRemark);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//input[@value='Submit']"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//input[@value='Close']"));
			gotoActivityHistorySection(driver);
			boolean isAdded = verifySubjectCommentActivityHistory(driver, remarks);

			if (isAdded == false) {
				fc.utobj().throwsException("Remarks not added!");
			}

			fc.utobj().switchFrameById(driver, "leadLogCallSummary");

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='printReady']//*[contains(text(),'" + remarks + "')]"));
			driver = fc.utobj().switchFrameToDefault(driver);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			isCallSubjectPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () ,'" + remarks + "')]");
			if (isCallSubjectPresent == false) {
				fc.utobj().throwsException("was not able to verify Remark");
			}
			isFieldNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , '" + fieldName + "')]");
			if (isFieldNamePresent == false) {
				fc.utobj().throwsException("was not able to verify added fieldName in Lead Primary infor");
			}

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@value='Close']"));
			driver = fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify the Log a Task");
			fc.utobj().clickPartialLinkText(driver, "Log a Task");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			taskSubject = fc.utobj().generateTestData(taskSubject);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='radioOwner' and @value='0']"));
			fc.utobj().selectDropDownByVisibleText(driver, fc.utobj().getElementByID(driver, "status"), "Not Started");
			fc.utobj().sendKeys(driver, pobj.taskSubject, taskSubject);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='schduleTime1' and @value='0']"));
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver,
					"//input[contains(@name, '_" + fieldNameTask.toLowerCase() + "')]"), fieldNameTask);
			fc.utobj().clickElement(driver, pobj.addTaskBtn);
			driver = fc.utobj().switchFrameToDefault(driver);
			fc.utobj().moveToElement(driver, fc.utobj().getElement(driver, pobj.contactHistorySection));
			fc.utobj().switchFrameById(driver, "leadOpenActivitesSummary");

			boolean isTaskAdded = fc.utobj().assertPageSource(driver, taskSubject);

			if (isTaskAdded == false) {
				fc.utobj().throwsException("Task Not Added Successfully");
			}

			fc.utobj().clickElement(driver, driver
					.findElement(By.xpath(".//*[@id='printReadyOpen']//*[contains(text(),'" + taskSubject + "')]")));
			driver = fc.utobj().switchFrameToDefault(driver);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			isFieldNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , '" + fieldNameTask + "')]");
			if (isFieldNamePresent == false) {
				fc.utobj().throwsException("was not able to verify added fieldName in Task Detail");
			}
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@value='Close']"));
			driver = fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/* Harish Dwivedi TC_FS_FormGenerator_012 */

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FS_FormGenerator_012", testCaseDescription = "Verify in lead info > Sync Fields added in tab with field info")
	private void verifySyncFieldPrimaryInfowithCustomTabFiled() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			String tabName = dataSet.get("tabDisplayName");
			String sectionName = dataSet.get("sectionValue");
			String fieldName = dataSet.get("displayName");
			// String docTitle = dataSet.get("docTitle");

			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");

			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);

			fc.utobj().printTestStep("Add a Tab in Manage Form Generator ");
			AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();
			tabName = p1.addTabNameSync(driver, tabName);

			boolean itabNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () , '" + tabName + "')]");
			if (itabNamePresent == false) {
				fc.utobj().throwsException("was not able to verify added Tab in Manage Form Generator");
			}
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'" + tabName + "')]"));

			fc.utobj().printTestStep("Add a Section in Manage Form Generator ");
			sectionName = p1.addSectionName(driver, sectionName);
			boolean issectionNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + sectionName + "')]");
			if (issectionNamePresent == false) {
				fc.utobj().throwsException("was not able to verify added sectionName in Manage Form Generator");
			}

			fc.utobj().printTestStep("Add Field Name in Manage Form Generator ");

			String fieldNameText = fieldName + "Sync";
			fieldNameText = p1.addFieldSynchType(driver, sectionName, fieldNameText, "Sync");

			boolean isFieldNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + fieldNameText + "')]");
			if (isFieldNamePresent == false) {
				fc.utobj().throwsException("was not able to verify added fieldName in Manage Form Generator");
			}

			fc.utobj().printTestStep("Navigate to sales module Lead summary ");
			fc.sales().sales_common().fsModule(driver);
			Sales fs = new Sales();
			fs.leadManagement(driver);
			FSLeadSummaryPageTest fsPage = new FSLeadSummaryPageTest();
			fsPage.clickAddLeadLink(driver);
			fc.utobj().printTestStep("Add a lead and Go to Primary Info");

			FSLeadSummaryPrimaryInfoPageTest pInfoPage = new FSLeadSummaryPrimaryInfoPageTest();
			Map<String, String> leadInfo = pInfoPage.fillLeadBasicInfo(driver);
			Reporter.log("Lead Added with lead name : " + leadInfo.get("firstName") + " " + leadInfo.get("lastName"));

			fieldNameText = fieldNameText.toLowerCase();

			String fieldNameAction = fieldNameText;
			fieldNameText = fieldNameText.toLowerCase();

			fc.utobj().printTestStep("Navigate to newly added Tab at Primary Info");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, "//*[@class='verTab']//*[text()='" + tabName + "']"));

			fc.utobj().sendKeys(driver,
					fc.utobj().getElementByXpath(driver, "//input[contains(@name, '_" + fieldNameText + "')]"),
					fieldNameText); // Work

			AdminFranchiseSalesManageFormGeneratorPage newp = new AdminFranchiseSalesManageFormGeneratorPage(driver);

			fc.utobj().clickElement(driver, newp.addDocumentSalePrimaryInfoBtn);

			fc.utobj().printTestStep("Verify The Added Field");
			boolean isfieldNameText = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + fieldNameText + "')]");
			if (isfieldNameText == false) {
				fc.utobj().throwsException("was not able to verify Text Field");
			}

			fc.utobj().printTestStep("Navigate to Tab at Personal Profile");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, "//*[@class='verTab']//*[text()='Personal Profile']"));

			fc.utobj().printTestStep("Verify The Added Field in Personal Profile with City Field ");
			isfieldNameText = fc.utobj().verifyElementOnVisible_ByXpath(driver, "//input[@id='homeCity'][@value='" + fieldNameText + "']");
			if (isfieldNameText == false) {
				fc.utobj().throwsException("was not able to verify Sync Field Personal Profile ");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public WebDriver gotoActivityHistorySection(WebDriver driver) throws Exception {

		Reporter.log("Go To Activity History Section");
		try {

			driver.switchTo().defaultContent();
		} catch (Exception E) {

		}

		FSLeadSummaryPrimaryInfoPage fs = new FSLeadSummaryPrimaryInfoPage(driver);
		fc.utobj().moveToElement(driver, fc.utobj().getElement(driver, fs.contactHistorySection));
		fc.utobj().switchFrameById(driver, "leadLogCallSummary");
		return driver;
	}

	public boolean verifySubjectCommentActivityHistory(WebDriver driver, String subjectComments) throws Exception {
		Reporter.log("Verifying Comment / Activity History for status : " + subjectComments);
		driver = gotoActivityHistorySection(driver);

		boolean isRemarksAdded = fc.utobj().assertPageSource(driver, subjectComments);
		if (isRemarksAdded == false) {
			fc.utobj().printBugStatus("Activity History Not Added : " + subjectComments);
		}
		driver.switchTo().defaultContent();
		return isRemarksAdded;
	}

	public boolean verifySubjectCommentActivityHistoryAddesField(WebDriver driver, String fieldNameRemark)
			throws Exception {
		Reporter.log("Verifying Comment / Activity History for status : " + fieldNameRemark);
		driver = gotoActivityHistorySection(driver);

		boolean isRemarksAdded = fc.utobj().assertPageSource(driver, fieldNameRemark);
		if (isRemarksAdded == false) {
			fc.utobj().printBugStatus("Activity History Not Added : " + fieldNameRemark);
		}
		driver.switchTo().defaultContent();
		return isRemarksAdded;
	}

	public void multiSelectListBox(WebDriver driver, WebElement multiSelectbox, String searchTxt) throws Exception {

		fc.utobj().clickElement(driver, multiSelectbox);
		WebElement newElement = multiSelectbox.findElement(By.xpath("./div/div/input[@class='searchInputMultiple']"));
		fc.utobj().sendKeys(driver, newElement, searchTxt);

		WebElement checkboxElement = multiSelectbox.findElement(By.xpath(".//input[@id='selectAll']"));
		checkboxElement.click();
		fc.utobj().clickElement(driver, multiSelectbox);

	}

	public void actionImgOptionForFormBuilder(WebDriver driver, String fieldName, String option) throws Exception {

		fc.utobj().moveToElement(driver, driver.findElement(
				By.xpath(".//*[contains(text () ,'" + fieldName + "')]/ancestor::tr/td/div[@id='menuBar']/layer")));
		String alterText = driver
				.findElement(By
						.xpath(".//*[contains(text () ,'" + fieldName + "')]/ancestor::tr/td/div[@id='menuBar']/layer"))
				.getAttribute("id").trim();
		alterText = alterText.replace("Actions_dynamicmenu", "");
		alterText = alterText.replace("Bar", "");

		fc.utobj().clickElement(driver, driver.findElement(By
				.xpath(".//*[contains(text () ,'" + fieldName + "')]/ancestor::tr/td/div[@id='menuBar']/layer/a/img")));
		fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
				".//div[@id='Actions_dynamicmenu" + alterText + "Menu']/span[contains(text () , '" + option + "')]"));
	}

	public boolean actionImgOptionForFormBuilderDeleteOption(WebDriver driver, String fieldName, String option)
			throws Exception {

		boolean isDeletebale = false;

		fc.utobj().moveToElement(driver, driver.findElement(
				By.xpath(".//*[contains(text () ,'" + fieldName + "')]/ancestor::tr/td/div[@id='menuBar']/layer")));
		String alterText = driver
				.findElement(By
						.xpath(".//*[contains(text () ,'" + fieldName + "')]/ancestor::tr/td/div[@id='menuBar']/layer"))
				.getAttribute("id").trim();
		alterText = alterText.replace("Actions_dynamicmenu", "");
		alterText = alterText.replace("Bar", "");

		fc.utobj().clickElement(driver, driver.findElement(By
				.xpath(".//*[contains(text () ,'" + fieldName + "')]/ancestor::tr/td/div[@id='menuBar']/layer/a/img")));
		// fc.utobj().clickElement(driver,
		// fc.utobj().getElementByXpath(driver,".//div[@id='Actions_dynamicmenu"+alterText+"Menu']/span[contains(text
		// () , '"+option+"')]")));

		isDeletebale = fc.utobj().verifyElementOnVisible_ByXpath(driver,
				".//div[@id='Actions_dynamicmenu" + alterText + "Menu']/span[contains(text () , '" + option + "')]");
		if (isDeletebale) {
			isDeletebale = true;
		} else {
			isDeletebale = false;
		}

		return isDeletebale;

	}

	public Map<String, String> addLeadLeadSummaryWithLeadName(WebDriver driver, String firstName, String lastName)
			throws Exception {
		LeadSummaryUI pobj = new LeadSummaryUI(driver);
		Sales fsmod = new Sales();
		fsmod.leadManagement(driver);
		clickAddLeadLink(driver);
		FSLeadSummaryPrimaryInfoPageTest p2 = new FSLeadSummaryPrimaryInfoPageTest();
		Map<String, String> leadName = p2.fillLeadBasicInfo_LeadNameBasic(driver, firstName, lastName);
		return leadName;
	}

	public void clickAddLeadLink(WebDriver driver) throws Exception {
		LeadSummaryUI pobj = new LeadSummaryUI(driver);

		try {
			fc.utobj().clickElement(driver, pobj.addLeadLnkAlternate);
		} catch (Exception e) {
			fc.utobj().clickElement(driver, pobj.addLeadLnk);
		}
		WebElement leadNameTxtBox = null;

		FSLeadSummaryAddLeadPage pobj2 = new FSLeadSummaryAddLeadPage(driver);

		try {
			leadNameTxtBox = pobj2.firstName;
			if (leadNameTxtBox.isDisplayed() == false) {
				fc.utobj().throwsException("Add Lead Page Not Opened!");
			}
		} catch (Exception e) {

		}
	}
}