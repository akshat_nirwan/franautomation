package com.builds.test.fs;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.admin.AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest;
import com.builds.test.common.CorporateUser;
import com.builds.uimaps.fs.AdminFranchiseSalesManageWebFormGeneratorPage;
import com.builds.uimaps.fs.FSCampaignCenterPage;
import com.builds.uimaps.fs.FSLeadSummaryAddLeadPage;
import com.builds.uimaps.fs.FSLeadSummaryPrimaryInfoPage;
import com.builds.uimaps.fs.LeadSummaryUI;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class FSalesManageWebFormGeneratorPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	/* Harish Dwivedi TC_FS_WebForm_001 */

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FS_WebForm_001", testCaseDescription = "Verify View Electronic Details after details filled Web From Generator in Web Form")
	private void verifyElectronicDetailsfromWebform() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			// String fieldName = dataSet.get("displayName");
			// String docTitle = dataSet.get("docTitle");
			String fieldName = fc.utobj().generateTestData(dataSet.get("displayName"));
			AdminSalesManageWebFormGeneratorPageTest p1 = new AdminSalesManageWebFormGeneratorPageTest();
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Web Form Generator");

			AdminSales adsales = new AdminSales();
			adsales.manageWebFormGenerator(driver);

			AdminFranchiseSalesManageWebFormGeneratorPage pobj = new AdminFranchiseSalesManageWebFormGeneratorPage(
					driver);
			// Click on Create Form
			String formName = p1.addLeadFromWebForm(driver, fieldName);
			String parentWindow = driver.getWindowHandle();
			fc.utobj().sendKeys(driver, pobj.searchMyForm, formName);
			fc.utobj().clickElement(driver, pobj.searchMyFormBtn);
			fc.utobj().actionImgOption(driver, formName, "Launch & Test");

			String formTitle = "WebFrom Title";
			fc.utobj().printTestStep("Launc And Test Form");
			Set<String> allWindows2 = driver.getWindowHandles();
			for (String currentWindow : allWindows2) {
				//
				if (!currentWindow.equalsIgnoreCase(parentWindow)) {
					driver.switchTo().window(currentWindow);
					//
					String titleTextCurrent = driver.getTitle();
					if (titleTextCurrent.equalsIgnoreCase(formTitle)) {
						// fill the form with data
						//

						fc.utobj().sendKeys(driver, pobj.leadFirstName, formName);
						fc.utobj().sendKeys(driver, pobj.leadLastName, formName);

						fc.utobj().sendKeys(driver, pobj.emailID, "harish.dwivedi@franconnect.net");
						fc.utobj().clickElement(driver, pobj.submitBtn);

						fc.utobj().isTextDisplayed(driver,
								"Thank you for submitting your information, we will get back to you shortly.",
								"was not able to verify confirmation Msg");

						driver.close();
					} else {
						driver.close();
						driver.switchTo().window(parentWindow);
					}
					driver.switchTo().window(parentWindow);
				}
				//
			}

			fc.utobj().printTestStep("Again Navigate to sales module Lead summary ");
			Sales fs = new Sales();
			fc.sales().sales_common().fsModule(driver);
			fs = new Sales();
			fs.search(driver);
			fc.utobj().printTestStep("Search for Lead Name added ");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByID(driver, "firstName"), formName);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByID(driver, "lastName"), formName);
			fc.utobj().clickElement(driver, fc.utobj().getElementByID(driver, "Submit"));

			String LeadName = formName + " " + formName;

			fc.utobj().printTestStep("Verify for the Lead Name added ");

			boolean isfieldNameText = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[@id='containsImage']/tbody/tr/td/span/a['" + LeadName + "']");
			if (isfieldNameText == false) {
				fc.utobj().throwsException("was not able to verify added Lead");
			}

			fc.utobj().printTestStep("Verify for the View Electronic Details ");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='containsImage']/tbody/tr/td/span/a"));

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='siteMainTable']//a[text()='View Electronic Details']"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String browserName = (String) config.get("browserName");
			if ("chrome".equals(browserName.toLowerCase()))
				browserName = "Google Chrome";
			boolean isBroswerPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + browserName + "')]");
			if (isBroswerPresent == false) {
				fc.utobj().throwsException("was not able to verify browserName Field");
			}
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//input[@value='Close']"));

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/* Harish Dwivedi TC_FS_WebForm_002 */

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FS_WebForm_002", testCaseDescription = "Verify Modify details filled Web From Generator in Web Form")
	private void verifyModifydSalesWebform() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			// String fieldName = dataSet.get("displayName");
			// String docTitle = dataSet.get("docTitle");
			String fieldName = fc.utobj().generateTestData(dataSet.get("displayName"));
			AdminSalesManageWebFormGeneratorPageTest p1 = new AdminSalesManageWebFormGeneratorPageTest();
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Web Form Generator");

			AdminSales adsales = new AdminSales();
			adsales.manageWebFormGenerator(driver);

			AdminFranchiseSalesManageWebFormGeneratorPage pobj = new AdminFranchiseSalesManageWebFormGeneratorPage(
					driver);
			// Click on Create Form
			String formName = p1.addLeadFromWebForm(driver, fieldName);
			fc.utobj().sendKeys(driver, pobj.searchMyForm, formName);
			fc.utobj().clickElement(driver, pobj.searchMyFormBtn);
			fc.utobj().printTestStep("Modify Form");
			fc.utobj().actionImgOption(driver, formName, "Modify");

			String newfromname = "ModifiedForm" + fc.utobj().generateRandomNumber();

			fc.utobj().sendKeys(driver, pobj.formName, newfromname);
			fc.utobj().clickElement(driver, pobj.saveNextBtn);
			fc.utobj().clickElement(driver, pobj.saveAndNextBtnDesign);
			fc.utobj().clickElement(driver, pobj.afterSubmissionMsg);

			fc.utobj().clickElement(driver, pobj.finishBtn);
			if (!pobj.hostURL.isSelected()) {
				fc.utobj().clickElement(driver, pobj.hostURL);
			}
			fc.utobj().clickElement(driver, pobj.okBtn);
			fc.utobj().printTestStep("Search modfied Form");
			fc.utobj().sendKeys(driver, pobj.searchMyForm, newfromname);
			fc.utobj().clickElement(driver, pobj.searchMyFormBtn);

			fc.utobj().printTestStep("Verify Modify Form");

			boolean isformexits = fc.utobj().verifyElementOnVisible_ByXpath(driver, " .//*[contains(text(),'" + formName + "')]");
			if (isformexits == false) {
				// fc.utobj().throwsException("was not able to verify
				// browserName Field");
			} else {
				fc.utobj().throwsException("was not able to verify browserName Field");

			}

			fc.utobj().printTestStep("Search Form");
			fc.utobj().sendKeys(driver, pobj.searchMyForm, newfromname);
			fc.utobj().clickElement(driver, pobj.searchMyFormBtn);

			fc.utobj().printTestStep("Deactivate Form");
			fc.utobj().actionImgOption(driver, newfromname, "Deactivate");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().sendKeys(driver, pobj.searchMyForm, newfromname);
			fc.utobj().clickElement(driver, pobj.searchMyFormBtn);

			fc.utobj().printTestStep("Verify The DeActivate Form");
			fc.utobj().moveToElement(driver, fc.utobj().getElementByXpath(driver,".//*[contains(text () ,'" + newfromname + "')]/ancestor::tr/td/div[@id='menuBar']/layer"));
			String alterText = driver
					.findElement(By.xpath(
							".//*[contains(text () ,'" + newfromname + "')]/ancestor::tr/td/div[@id='menuBar']/layer"))
					.getAttribute("id").trim();
			alterText = alterText.replace("Actions_dynamicmenu", "");
			alterText = alterText.replace("Bar", "");

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[contains(text () ,'" + newfromname + "')]/ancestor::tr/td/div[@id='menuBar']/layer/a/img"));

			boolean formStatus = false;
			List<WebElement> listElements = driver
					.findElements(By.xpath(".//div[@id='Actions_dynamicmenu" + alterText + "Menu']/span"));

			for (int i = 0; i < listElements.size(); i++) {

				if (listElements.get(i).getText().trim().equalsIgnoreCase("Activate")) {
					formStatus = true;
					break;
				}
			}

			if (formStatus == false) {
				fc.utobj().throwsException("was not able to deactivate Form");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/* Harish Dwivedi TC_FS_WebForm_005 */

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FS_WebForm_005", testCaseDescription = "Verify that the form is getting copied and customized details filled in Web Form Generator.")
	private void verifyCopyCustomizeSalesWebform() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			// String fieldName = dataSet.get("displayName");
			// String docTitle = dataSet.get("docTitle");
			String fieldName = fc.utobj().generateTestData(dataSet.get("displayName"));
			AdminSalesManageWebFormGeneratorPageTest p1 = new AdminSalesManageWebFormGeneratorPageTest();
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Web Form Generator");

			AdminSales adsales = new AdminSales();
			adsales.manageWebFormGenerator(driver);

			AdminFranchiseSalesManageWebFormGeneratorPage pobj = new AdminFranchiseSalesManageWebFormGeneratorPage(
					driver);
			// Click on Create Form
			String formName = p1.addLeadFromWebForm(driver, fieldName);
			fc.utobj().sendKeys(driver, pobj.searchMyForm, formName);
			fc.utobj().clickElement(driver, pobj.searchMyFormBtn);
			fc.utobj().printTestStep("Copy and Customize Form");
			fc.utobj().actionImgOption(driver, formName, "Copy and Customize");

			String newfromname = "CopyCustomize" + fc.utobj().generateRandomNumber();

			fc.utobj().sendKeys(driver, pobj.formName, newfromname);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByID(driver, "formUrl"), newfromname);
			fc.utobj().clickElement(driver, pobj.saveNextBtn);
			fc.utobj().clickElement(driver, pobj.saveAndNextBtnDesign);
			fc.utobj().clickElement(driver, pobj.afterSubmissionMsg);

			fc.utobj().clickElement(driver, pobj.finishBtn);
			if (!pobj.hostURL.isSelected()) {
				fc.utobj().clickElement(driver, pobj.hostURL);
			}
			fc.utobj().clickElement(driver, pobj.okBtn);
			fc.utobj().printTestStep("Search Copy and Customize For");
			fc.utobj().sendKeys(driver, pobj.searchMyForm, newfromname);
			fc.utobj().clickElement(driver, pobj.searchMyFormBtn);

			fc.utobj().printTestStep("Verify Copy and Customize Form");

			boolean isformexits = fc.utobj().verifyElementOnVisible_ByXpath(driver, " .//*[contains(text(),'" + newfromname + "')]");
			if (isformexits == false) {
				fc.utobj().throwsException("was not able to Verify Copy and Customize For");
			}

			// fc.utobj().printTestStep(testCaseId, "Search Form");
			// fc.utobj().sendKeys(driver, pobj.searchMyForm, newfromname);
			// fc.utobj().clickElement(driver, pobj.searchMyFormBtn);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/* Harish Dwivedi TC_FS_WebForm_006 */

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FS_WebForm_006", testCaseDescription = "Verify WebForm With multiple tabs in lead that the lead is submitted with all the info.")
	private void verifyMultiStepWebform() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			String tabName = dataSet.get("tabDisplayName");
			String sectionName = dataSet.get("sectionValue");
			// String fieldName = dataSet.get("displayName");
			// String docTitle = dataSet.get("docTitle");
			AdminSalesManageWebFormGeneratorPageTest p1 = new AdminSalesManageWebFormGeneratorPageTest();
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Web Form Generator");

			AdminSales adsales = new AdminSales();
			adsales.manageWebFormGenerator(driver);

			AdminFranchiseSalesManageWebFormGeneratorPage pobj = new AdminFranchiseSalesManageWebFormGeneratorPage(
					driver);
			// Click on Create Form
			// String formName = p1.addLeadFromWebForm(driver, fieldName);
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			String formFormat = dataSet.get("formFormat");
			formFormat = "Multi Page";
			String header = fc.utobj().generateTestData(dataSet.get("header"));
			String textLabel = fc.utobj().generateTestData(dataSet.get("textLabel"));
			String fieldLabel = fc.utobj().generateTestData(dataSet.get("fieldLabel"));
			String footer = fc.utobj().generateTestData(dataSet.get("footer"));
			String submissionType = dataSet.get("submissionType");
			String formName = fc.utobj().generateTestData(dataSet.get("formName"));
			// String
			// formTitle=fc.utobj().generateTestData(dataSet.get("formTitle"));
			String formTitle = "WebFrom Title";
			description = fc.utobj().generateTestData(dataSet.get("description"));
			String formUrl = fc.utobj().generateTestData(dataSet.get("formUrl"));
			String afterSubmission = dataSet.get("afterSubmission");
			String redirectedUrl = "";

			p1.createNewFormContactType(driver, formName, formTitle, description, formFormat, submissionType, formUrl,
					sectionName, tabName, header, textLabel, fieldLabel, footer, afterSubmission, redirectedUrl,
					config);

			String parentWindow = driver.getWindowHandle();
			fc.utobj().sendKeys(driver, pobj.searchMyForm, formName);
			fc.utobj().clickElement(driver, pobj.searchMyFormBtn);
			fc.utobj().actionImgOption(driver, formName, "Launch & Test");

			fc.utobj().printTestStep("Launc And Test Form");
			Set<String> allWindows2 = driver.getWindowHandles();
			for (String currentWindow : allWindows2) {
				//
				if (!currentWindow.equalsIgnoreCase(parentWindow)) {
					driver.switchTo().window(currentWindow);
					//
					String titleTextCurrent = driver.getTitle();
					if (titleTextCurrent.equalsIgnoreCase(formTitle)) {
						// fill the form with data
						//

						fc.utobj().sendKeys(driver, pobj.leadFirstName, formName);
						fc.utobj().sendKeys(driver, pobj.leadLastName, formName);

						fc.utobj().sendKeys(driver, pobj.emailID, "harish.dwivedi@franconnect.net");
						fc.utobj().clickElement(driver, pobj.nextBtn);
						fc.utobj().clickElement(driver, pobj.submitBtn);

						fc.utobj().isTextDisplayed(driver,
								"Thank you for submitting your information, we will get back to you shortly.",
								"was not able to verify confirmation Msg");

						driver.close();
					} else {
						driver.close();
						driver.switchTo().window(parentWindow);
					}
					driver.switchTo().window(parentWindow);
				}
				//
			}

			fc.utobj().printTestStep("Again Navigate to sales module Lead summary ");
			Sales fs = new Sales();
			fc.sales().sales_common().fsModule(driver);
			fs = new Sales();
			fs.search(driver);
			fc.utobj().printTestStep("Search for Lead Name added ");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByID(driver, "firstName"), formName);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByID(driver, "lastName"), formName);
			fc.utobj().clickElement(driver, fc.utobj().getElementByID(driver, "Submit"));

			String LeadName = formName + " " + formName;

			fc.utobj().printTestStep("Verify for the Lead Name added ");

			boolean isfieldNameText = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[@id='containsImage']/tbody/tr/td/span/a['" + LeadName + "']");
			if (isfieldNameText == false) {
				fc.utobj().throwsException("was not able to verify added Lead");
			}

			fc.utobj().printTestStep("Verify for the View Electronic Details ");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='containsImage']/tbody/tr/td/span/a"));

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/* Harish Dwivedi TC_FS_WebForm_004 */

	@Test(groups = "webformCopyUrlteharish")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Copy Url , from Wen Form with Format Single Page Web Form Generator", testCaseId = "TC_FS_WebForm_004")
	public void copyUrlverifyfsWebform() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			// String fieldName = dataSet.get("displayName");
			// String docTitle = dataSet.get("docTitle");
			String fieldName = fc.utobj().generateTestData(dataSet.get("displayName"));
			AdminSalesManageWebFormGeneratorPageTest p1 = new AdminSalesManageWebFormGeneratorPageTest();
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Web Form Generator");

			AdminSales adsales = new AdminSales();
			adsales.manageWebFormGenerator(driver);

			AdminFranchiseSalesManageWebFormGeneratorPage pobj = new AdminFranchiseSalesManageWebFormGeneratorPage(
					driver);
			// Click on Create Form
			String formName = p1.addLeadFromWebForm(driver, fieldName);
			fc.utobj().sendKeys(driver, pobj.searchMyForm, formName);
			fc.utobj().clickElement(driver, pobj.searchMyFormBtn);

			fc.utobj().actionImgOption(driver, formName, "Copy URL");

			fc.utobj().printTestStep("Copy Url");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			if (!pobj.hostURL.isSelected()) {
				fc.utobj().clickElement(driver, pobj.hostURL);
			}
			String targetUrl = fc.utobj().getElementByXpath(driver, ".//*[@id='hostCodeBox']/a").getText().trim();
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//input[@value='Close']"));
			fc.utobj().switchFrameToDefault(driver);

			// driver.navigate().to(targetUrl);
			fc.utobj().printTestStep("Add Lead with Copy Url");
			if (config.get("browserName").equalsIgnoreCase("Chrome")) {

				System.setProperty("webdriver.chrome.driver", config.get("inputDirectory") + "\\exe\\chromedriver.exe");
				ChromeOptions options = new ChromeOptions();
				options.addArguments("disable-popup-blocking");
				DesiredCapabilities capabilities = DesiredCapabilities.chrome();
				capabilities.setCapability(ChromeOptions.CAPABILITY, options);
				// Browser Logs
				LoggingPreferences logPrefs = new LoggingPreferences();
				logPrefs.enable(LogType.BROWSER, Level.ALL);
				capabilities.setCapability(CapabilityType.LOGGING_PREFS, logPrefs);
				WebDriver driver1 = new ChromeDriver(capabilities);

				driver1.get(targetUrl);

				// verify
				fc.utobj().sendKeys(driver1, driver1.findElement(By.xpath(".//*[@id='firstName']")), fieldName);
				fc.utobj().sendKeys(driver1, driver1.findElement(By.xpath(".//*[@id='lastName']")), fieldName);
				fc.utobj().sendKeys(driver1, driver1.findElement(By.xpath(".//*[@id='emailID']")), "Test@test.com");
				fc.utobj().clickElement(driver1, driver1.findElement(By.xpath(".//*[@id='submitButton']")));

				boolean isSubmitted = fc.utobj().assertPageSource(driver1,
						"Thank you for submitting your information, we will get back to you shortly.");

				if (isSubmitted == false) {
					fc.utobj().throwsException("Unable to submit lead");
				}

				driver1.quit();

			} else if (config.get("browserName").equalsIgnoreCase("firefox")) {

			}

			String leadName = fieldName + " " + fieldName;

			Sales fs = new Sales();
			fc.utobj().printTestStep("Again Navigate to sales module Lead summary ");
			fc.sales().sales_common().fsModule(driver);
			fs = new Sales();
			fs.search(driver);

			fc.utobj().sendKeys(driver, fc.utobj().getElementByID(driver, "firstName"), fieldName);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByID(driver, "lastName"), fieldName);
			fc.utobj().clickElement(driver, fc.utobj().getElementByID(driver, "Submit"));

			fc.utobj().printTestStep("Verify for the Lead Details ");
			boolean iLeadNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[@id='containsImage']/tbody/tr/td/span/a['" + leadName + "']");
			if (iLeadNamePresent == false) {
				fc.utobj().throwsException("was not able to verify lead in  Lead Summary");
			}

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='containsImage']/tbody/tr/td/span/a"));

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales", "sales" })
	@TestCase(createdOn = "2017-07-24", updatedOn = "2017-07-24", testCaseId = "TC_Sales_SourceCategory_014", testCaseDescription = "Verify Lead Source Category and Details at webform")
	private void verifyLeadSorceCategoryDetailsBlank() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			LeadSummaryUI pobj = new LeadSummaryUI(driver);
			AddLeadFromAllSources addlead = new AddLeadFromAllSources();
			FSSearchPageTest p3 = new FSSearchPageTest();
			String catogeryName = fc.utobj().generateTestData(dataSet.get("catogeryName"));
			String uniqueName = fc.utobj().generateTestData(dataSet.get("catogeryDetails"));
			String formName = fc.utobj().generateTestData(dataSet.get("formName"));
			String firstNameWebForm = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastNameWebForm = fc.utobj().generateTestData(dataSet.get("lastName"));
			String country = dataSet.get("country");
			String state = dataSet.get("stateID");
			String emailId = dataSet.get("emailID");
			String leadOwner = dataSet.get("leadOwnerID");
			// String leadSourceCategory=dataSet.get("leadSource2ID");
			// String leadSourceDetails=dataSet.get("leadSource3ID");
			// String specialCharactors=fc.utobj().getMajorSpecialCharacter();
			String catogeryDetails = uniqueName;
			fc.adminpage().adminPage(driver);

			AdminSales adsales = new AdminSales();
			adsales.adminFranchiseSalesLeadSourceCategory(driver);
			fc.utobj().printTestStep("Go to  Admin > Sales > Lead Source Category");
			fc.utobj().printTestStep("Add lead source category and details");
			fc.utobj().clickElement(driver, pobj.addLeadSourceCarogoryBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.leadSourceName, catogeryName);
			fc.utobj().clickElement(driver, pobj.addLeadSourceCarogoryCboxBtn);
			fc.utobj().clickElement(driver, pobj.cboxCloseBtn);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//td[contains(text(),'"
					+ catogeryName + "')]/following-sibling::td/a[contains(text(),'Lead Source Details')]"));
			fc.utobj().clickElement(driver, pobj.addLeadSourceDetailsBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.leadSourceDetails, catogeryDetails);
			fc.utobj().clickElement(driver, pobj.addLeadSourceCarogoryCboxBtn);
			fc.utobj().clickElement(driver, pobj.cboxCloseBtn);

			fc.utobj().printTestStep("Go to Admin > Sales > Manage Web Form Generator > Add Web form > "
					+ "Select lead source category and details from the Available fields section > Save and Next");
			fc.utobj().printTestStep("Adding lead from Web Form");
			Map<String, String> leadInfo = new HashMap<String, String>();
			leadInfo.put("firstName", firstNameWebForm);
			leadInfo.put("lastName", lastNameWebForm);
			leadInfo.put("country", country);
			leadInfo.put("state", state);
			leadInfo.put("email", emailId);
			leadInfo.put("leadOwner", leadOwner);
			leadInfo.put("leadSourceCategory", catogeryName);
			leadInfo.put("leadSourceDetails", catogeryDetails);
			fc.utobj().printTestStep(
					"Now on this page > Admin > Sales > Manage Web Form Generator  > Settings > Check the added source category and detail is available");
			fc.utobj().printTestStep("Click Finish");
			fc.utobj().printTestStep("Click launch and test against the added webform");
			fc.utobj().printTestStep("Check on the webform the added lead source category and detail is visible");
			fc.utobj().printTestStep("Add a lead with same source");
			addlead.addLeadFromWebForm(driver, formName, leadInfo, "Confirmation Message");

			fc.utobj().printTestStep("Check that the lead is added with same source and details.");

			fc.sales().sales_common().fsModule(driver);
			p3.searchByLeadName(driver, firstNameWebForm, lastNameWebForm);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,(".//*[contains(text(),'" + firstNameWebForm + " " + lastNameWebForm + "')]")));
			boolean isCategoryPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text(),'" + catogeryName + "')]");
			boolean isCategoryDetailsPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text(),'" + catogeryDetails + "')]");
			if (isCategoryPresent == false && isCategoryDetailsPresent == false) {
				fc.utobj().throwsException(
						"Lead Catagory or category details not verified not verified in lead added from webform");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales","sales_failed"  , "TC_Sales_WebForm_002"}) // fixed : verified // akshat
	@TestCase(createdOn = "2017-07-28", updatedOn = "2017-07-28", testCaseId = "TC_Sales_WebForm_002", testCaseDescription = "Verify the webform with default value in the page")
	private void verifyDeafultvalueWebform() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			AdminFranchiseSalesLeadStatusPageTest p1 = new AdminFranchiseSalesLeadStatusPageTest();
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			FSEmailCampaignsPageTest campaignPageTest = new FSEmailCampaignsPageTest();
			LeadSummaryUI pobj = new LeadSummaryUI(driver);
			AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest divisionalUserPage = new AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest();
			AddLeadFromAllSources addlead = new AddLeadFromAllSources();
			// FsModulePageTest fsmod=new FsModulePageTest();
			FSSearchPageTest p3 = new FSSearchPageTest();

			String userName = fc.utobj().generateTestData(dataSet.get("userName"));
			String userNameCorp = userName;
			String campaignName = fc.utobj().generateTestData(dataSet.get("campaignName"));
			String emailId = "salesautomation@staffex.com";
			String camapignDescription = fc.utobj().generateTestData(dataSet.get("camapignDescription"));
			String accessibility = dataSet.get("accessibility");
			String templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String emailSubject = fc.utobj().generateTestData(dataSet.get("emailSubject"));
			String catogeryName = fc.utobj().generateTestData(dataSet.get("catogeryName"));
			String catogeryDetails = fc.utobj().generateTestData(dataSet.get("catogeryDetails"));
			String divisionName = fc.utobj().generateTestData(dataSet.get("divisionName"));
			String divUserName = fc.utobj().generateTestData(dataSet.get("divUserName"));
			String formName = fc.utobj().generateTestData(dataSet.get("formName"));
			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));

			fc.utobj().printTestStep("Add Lead Status");
			String leadStatus = p1.addNewLeadStatus(driver);
			fc.utobj().printTestStep("Add Lead Owner");
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = corpTest.createDefaultUser(driver, corpUser);
			fc.utobj().printTestStep("Add Campaign");
			campaignName = campaignPageTest.createEmailCampaignCodeYourOwnTemp(driver, campaignName,
					camapignDescription, accessibility, templateName, emailSubject);

			fc.adminpage().adminPage(driver);

			AdminSales adsales = new AdminSales();
			adsales.adminFranchiseSalesLeadSourceCategory(driver);
			fc.utobj().printTestStep("Add lead source category and details");
			fc.utobj().clickElement(driver, pobj.addLeadSourceCarogoryBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.leadSourceName, catogeryName);
			fc.utobj().clickElement(driver, pobj.addLeadSourceCarogoryCboxBtn);
			fc.utobj().clickElement(driver, pobj.cboxCloseBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//td[contains(text(),'"
					+ catogeryName + "')]/following-sibling::td/a[contains(text(),'Lead Source Details')]"));
			fc.utobj().clickElement(driver, pobj.addLeadSourceDetailsBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.leadSourceDetails, catogeryDetails);
			fc.utobj().clickElement(driver, pobj.addLeadSourceCarogoryCboxBtn);
			fc.utobj().clickElement(driver, pobj.cboxCloseBtn);

			fc.utobj().printTestStep("Add Division");
			divisionalUserPage.addDivisionalUser(driver, divUserName, divisionName, emailId);

			fc.utobj().printTestStep("Admin > Sales > Manage Web Form Generator ");
			Map<String, String> leadInfo = new HashMap<String, String>();
			leadInfo.put("firstName", firstName);
			leadInfo.put("lastName", lastName);
			leadInfo.put("email", emailId);
			leadInfo.put("leadOwner", corpUser.getuserFullName());
			leadInfo.put("leadSourceCategory", catogeryName);
			leadInfo.put("leadSourceDetails", catogeryDetails);
			leadInfo.put("campaignName", campaignName);
			leadInfo.put("divisionName", divisionName);
			leadInfo.put("leadStatus", leadStatus);
			fc.utobj().printTestStep("Enter form info");
			fc.utobj().printTestStep("Click on Save & Next");
			fc.utobj().printTestStep("Fill created values for 'Default values for Fields'");
			fc.utobj().printTestStep("Configure After Submission 'Confirmation Message'");
			fc.utobj()
					.printTestStep("Enter the text" + "Click Finish" + "Get the URL"
							+ "Launch Web Form > Enter the details > Submit"
							+ "Check the confirmation message is shown as configured");
			
			addlead.addLeadFromWebForm(driver, formName, leadInfo, "Confirmation Message");
			fc.utobj().sleep();
			
			fc.utobj().printTestStep("Check that the lead is added in the build");
			fc.sales().sales_common().fsModule(driver);
			p3.searchByLeadName(driver, firstName, lastName);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[contains(text(),'" + firstName + " " + lastName + "')]"));
			boolean isLeadFirstName = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + firstName + "')]");
			boolean isLeadLastName = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + lastName + "')]");
		//	boolean isLeadCampaignName = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + campaignName + "')]"); // Promotional campaign won't be shown in "Manage Web Form Generator > Settings" // Only status driven
			boolean isLeadCategoryName = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + catogeryName + "')]");
			boolean isLeadcatogeryDetails = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text(),'" + catogeryDetails + "')]");
			boolean isLeadDivisionName = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + divisionName + "')]");
			boolean isLeadOwnerName = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//tr[@class='bText12']//table[@class='summaryTbl bText12']/tbody/tr[8]/td[contains(text(),'" + corpUser.getuserFullName() + "')]");
			boolean isLeadStatus = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//option[contains(text(),'" + leadStatus + "')");
			isLeadStatus = fc.utobj().assertPageSource(driver, leadStatus);

			if (isLeadFirstName == false) {
				fc.utobj().throwsException("Lead First Name not verfied");
			}
			if (isLeadLastName == false) {
				fc.utobj().throwsException("Lead last Name not verfied");
			}
			/*if (isLeadCampaignName == false) {
				fc.utobj().throwsException("Lead Campaign Name not verfied");
			}*/ // Promotional campaign won't be shown in "Manage Web Form Generator > Settings" // Only status driven
			if (isLeadCategoryName == false) {
				fc.utobj().throwsException("Lead Category Name not verfied");
			}
			if (isLeadcatogeryDetails == false) {
				fc.utobj().throwsException("Lead Category Details Name not verfied");
			}
			if (isLeadDivisionName == false) {
				fc.utobj().throwsException("Lead Division Name not verfied");
			}
			if (isLeadOwnerName == false) {
				fc.utobj().throwsException("Lead Owner Name not verfied");
			}
			if (isLeadStatus == false) {
				fc.utobj().throwsException("Lead Status Name not verfied");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales", "TC_Sales_WebForm_003" }) // fixed : verified // akshat
	@TestCase(createdOn = "2017-07-31", updatedOn = "2017-07-31", testCaseId = "TC_Sales_WebForm_003", testCaseDescription = "Verify the redirection of the webform")
	private void verifyRedirectionWebForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			String emailId = "salesautomation@staffex.com";
			String formName = fc.utobj().generateTestData(dataSet.get("formName"));
			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));

			AddLeadFromAllSources addlead = new AddLeadFromAllSources();
			FSSearchPageTest p3 = new FSSearchPageTest();
			FSLeadSummaryPrimaryInfoPageTest pobj1 = new FSLeadSummaryPrimaryInfoPageTest();

			fc.utobj().printTestStep("Admin > Sales > Manage Web Form Generator ");
			Map<String, String> leadInfo = new HashMap<String, String>();
			leadInfo.put("firstName", firstName);
			leadInfo.put("lastName", lastName);
			leadInfo.put("email", emailId);
			leadInfo.put("leadOwner", "FranConnect Administrator");
			leadInfo.put("leadStatus", "New Lead");
			fc.utobj().printTestStep("Enter form info");
			fc.utobj().printTestStep("Click on Save & Next");
			fc.utobj().printTestStep("Fill created values for 'Default values for Fields'");
			fc.utobj().printTestStep("Configure After Submission 'Redirect URL'");
			fc.utobj()
					.printTestStep("Enter the URL : http://www.franconnect.com" + "Click Finish" + "Get the URL"
							+ "Launch Web Form > Enter the details > Submit"
							+ "Check the confirmation message is shown as configured");
			addlead.addLeadFromWebForm(driver, formName, leadInfo, "Redirect Url");

			fc.sales().sales_common().fsModule(driver);
			p3.searchByLeadName(driver, firstName, lastName);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[contains(text(),'" + firstName + " " + lastName + "')]"));
			boolean isLeadFirstName = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + firstName + "')]");
			boolean isLeadLastName = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + lastName + "')]");
			if (isLeadFirstName == false && isLeadLastName == false) {
				fc.utobj().throwsException("Lead first name or last name not verified");
			}
			driver = pobj1.gotoActivityHistorySection(driver);

			boolean isRemarkPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text () ,'Lead added through WebFrom Title.')]");
			if (isRemarkPresent == false) {
				fc.utobj().throwsException(
						"At Activity History 'Lead added through Lead Added through webform name.' not verfied");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales", "salesJulyForm004" })
	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseId = "TC_Sales_WebForm_004", testCaseDescription = "Verify the keywords for webform is getting generated")
	private void verifyWebFormKeyword() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			String formName = fc.utobj().generateTestData(dataSet.get("formName"));
			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String emailId = "salesautomation@staffex.com";
			String templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String emailSubject = fc.utobj().generateTestData(dataSet.get("emailSubject"));

			FSCampaignCenterPage campaignCenterPage = new FSCampaignCenterPage(driver);
			AddLeadFromAllSources addlead = new AddLeadFromAllSources();
			Sales fsMod = new Sales();

			fc.utobj().printTestStep("Admin > Sales > Manage Web Form Generator ");
			Map<String, String> leadInfo = new HashMap<String, String>();
			leadInfo.put("firstName", firstName);
			leadInfo.put("lastName", lastName);
			leadInfo.put("email", emailId);
			leadInfo.put("leadOwner", "FranConnect Administrator");
			leadInfo.put("leadStatus", "New Lead");
			fc.utobj().printTestStep("Enter form info");
			fc.utobj().printTestStep("Click on Save & Next");
			fc.utobj().printTestStep("Fill created values for 'Default values for Fields'");
			fc.utobj().printTestStep("Configure After Submission 'Redirect URL'");
			fc.utobj()
					.printTestStep("Enter the URL : http://www.franconnect.com" + "Click Finish" + "Get the URL"
							+ "Launch Web Form > Enter the details > Submit"
							+ "Check the confirmation message is shown as configured");
			addlead.addLeadFromWebForm(driver, formName, leadInfo, "Confirmation Message");

			fc.sales().sales_common().fsModule(driver);
			fsMod.emailCampaignTab(driver);
			fc.utobj().printTestStep("Add Template");
			try {
				fc.utobj().clickElement(driver, campaignCenterPage.managetTemplateBtn);
				fc.utobj().clickElement(driver, campaignCenterPage.createTemplateBtn);

			} catch (Exception e) {
				fc.utobj().clickElement(driver, campaignCenterPage.createTemplateTab);
			}
			fc.utobj().clickElement(driver, campaignCenterPage.codeYourOwn);

			fc.utobj().sendKeys(driver, campaignCenterPage.templateNameTitle, templateName);
			fc.utobj().sendKeys(driver, campaignCenterPage.emailSubject, emailSubject);

			fc.utobj().clickElement(driver, campaignCenterPage.CodeOwnplainTextEmailType);
			fc.utobj().clickElement(driver, campaignCenterPage.keywordsBtn);
			try {
				fc.utobj().selectDropDownByPartialText(driver, campaignCenterPage.keywordsDropDown, formName);
			} catch (Exception e) {
				fc.utobj().throwsException("Check in the Available Keywords - the created form keyword not verified");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public WebDriver gotoActivityHistorySection(WebDriver driver) throws Exception {

		Reporter.log("Go To Activity History Section");
		try {

			driver.switchTo().defaultContent();
		} catch (Exception E) {

		}

		FSLeadSummaryPrimaryInfoPage fs = new FSLeadSummaryPrimaryInfoPage(driver);
		fc.utobj().moveToElement(driver, fc.utobj().getElement(driver, fs.contactHistorySection));
		fc.utobj().switchFrameById(driver, "leadLogCallSummary");
		return driver;
	}

	public boolean verifySubjectCommentActivityHistory(WebDriver driver, String subjectComments) throws Exception {
		Reporter.log("Verifying Comment / Activity History for status : " + subjectComments);
		driver = gotoActivityHistorySection(driver);

		boolean isRemarksAdded = fc.utobj().assertPageSource(driver, subjectComments);
		if (isRemarksAdded == false) {
			fc.utobj().printBugStatus("Activity History Not Added : " + subjectComments);
		}
		driver.switchTo().defaultContent();
		return isRemarksAdded;
	}

	public boolean verifySubjectCommentActivityHistoryAddesField(WebDriver driver, String fieldNameRemark)
			throws Exception {
		Reporter.log("Verifying Comment / Activity History for status : " + fieldNameRemark);
		driver = gotoActivityHistorySection(driver);

		boolean isRemarksAdded = fc.utobj().assertPageSource(driver, fieldNameRemark);
		if (isRemarksAdded == false) {
			fc.utobj().printBugStatus("Activity History Not Added : " + fieldNameRemark);
		}
		driver.switchTo().defaultContent();
		return isRemarksAdded;
	}

	public void multiSelectListBox(WebDriver driver, WebElement multiSelectbox, String searchTxt) throws Exception {

		fc.utobj().clickElement(driver, multiSelectbox);
		WebElement newElement = multiSelectbox.findElement(By.xpath("./div/div/input[@class='searchInputMultiple']"));
		fc.utobj().sendKeys(driver, newElement, searchTxt);

		WebElement checkboxElement = multiSelectbox.findElement(By.xpath(".//input[@id='selectAll']"));
		checkboxElement.click();
		fc.utobj().clickElement(driver, multiSelectbox);

	}

	public void actionImgOptionForFormBuilder(WebDriver driver, String fieldName, String option) throws Exception {

		fc.utobj().moveToElement(driver, driver.findElement(
				By.xpath(".//*[contains(text () ,'" + fieldName + "')]/ancestor::tr/td/div[@id='menuBar']/layer")));
		String alterText = driver
				.findElement(By
						.xpath(".//*[contains(text () ,'" + fieldName + "')]/ancestor::tr/td/div[@id='menuBar']/layer"))
				.getAttribute("id").trim();
		alterText = alterText.replace("Actions_dynamicmenu", "");
		alterText = alterText.replace("Bar", "");

		fc.utobj().clickElement(driver, driver.findElement(By
				.xpath(".//*[contains(text () ,'" + fieldName + "')]/ancestor::tr/td/div[@id='menuBar']/layer/a/img")));
		fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
				".//div[@id='Actions_dynamicmenu" + alterText + "Menu']/span[contains(text () , '" + option + "')]"));
	}

	public boolean actionImgOptionForFormBuilderDeleteOption(WebDriver driver, String fieldName, String option)
			throws Exception {

		boolean isDeletebale = false;

		fc.utobj().moveToElement(driver, driver.findElement(
				By.xpath(".//*[contains(text () ,'" + fieldName + "')]/ancestor::tr/td/div[@id='menuBar']/layer")));
		String alterText = driver
				.findElement(By
						.xpath(".//*[contains(text () ,'" + fieldName + "')]/ancestor::tr/td/div[@id='menuBar']/layer"))
				.getAttribute("id").trim();
		alterText = alterText.replace("Actions_dynamicmenu", "");
		alterText = alterText.replace("Bar", "");

		fc.utobj().clickElement(driver, driver.findElement(By
				.xpath(".//*[contains(text () ,'" + fieldName + "')]/ancestor::tr/td/div[@id='menuBar']/layer/a/img")));
		// fc.utobj().clickElement(driver,
		// fc.utobj().getElementByXpath(driver,".//div[@id='Actions_dynamicmenu"+alterText+"Menu']/span[contains(text
		// () , '"+option+"')]")));

		isDeletebale = fc.utobj().verifyElementOnVisible_ByXpath(driver,
				".//div[@id='Actions_dynamicmenu" + alterText + "Menu']/span[contains(text () , '" + option + "')]");
		if (isDeletebale) {
			isDeletebale = true;
		} else {
			isDeletebale = false;
		}

		return isDeletebale;

	}

	public Map<String, String> addLeadLeadSummaryWithLeadName(WebDriver driver, Map<String, String> config,
			String firstName, String lastName) throws Exception {
		Sales fsmod = new Sales();
		fsmod.leadManagement(driver);
		clickAddLeadLink(driver);
		FSLeadSummaryPrimaryInfoPageTest p2 = new FSLeadSummaryPrimaryInfoPageTest();
		Map<String, String> leadName = p2.fillLeadBasicInfo_LeadNameBasic(driver, firstName, lastName);
		return leadName;
	}

	public void clickAddLeadLink(WebDriver driver) throws Exception {
		LeadSummaryUI pobj = new LeadSummaryUI(driver);

		try {
			fc.utobj().clickElement(driver, pobj.addLeadLnkAlternate);
		} catch (Exception e) {
			fc.utobj().clickElement(driver, pobj.addLeadLnk);
		}
		WebElement leadNameTxtBox = null;

		FSLeadSummaryAddLeadPage pobj2 = new FSLeadSummaryAddLeadPage(driver);

		try {
			leadNameTxtBox = pobj2.firstName;
			if (leadNameTxtBox.isDisplayed() == false) {
				fc.utobj().throwsException("Add Lead Page Not Opened!");
			}
		} catch (Exception e) {

		}
	}
}