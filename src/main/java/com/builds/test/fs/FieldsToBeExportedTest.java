package com.builds.test.fs;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.fs.FieldstobeExportedUI;
import com.builds.utilities.FranconnectUtil;

class FieldsToBeExportedTest {

	public void selectFieldstoBeExported(WebDriver driver, FieldsToBeExported fields) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		FieldstobeExportedUI ui = new FieldstobeExportedUI(driver);

		fc.utobj().printTestStep("Select Fields to be exported");

		if (fields.getRequestDate() != null) {
			fc.utobj().check(ui.requestDate, fields.getRequestDate());
		}

		if (fields.getFirstName() != null) {
			fc.utobj().check(ui.firstName, fields.getFirstName());
		}

		if (fields.getLastName() != null) {
			fc.utobj().check(ui.lastName, fields.getLastName());
		}

		if (fields.getAddress() != null) {
			fc.utobj().check(ui.address, fields.getAddress());
		}

		if (fields.getAddress2() != null) {
			fc.utobj().check(ui.address2, fields.getAddress2());
		}

		if (fields.getCity() != null) {
			fc.utobj().check(ui.city, fields.getCity());
		}

		if (fields.getCountry() != null) {
			fc.utobj().check(ui.country, fields.getCountry());
		}

		if (fields.getStateID() != null) {
			fc.utobj().check(ui.stateID, fields.getStateID());
		}

		if (fields.getEmailID() != null) {
			fc.utobj().check(ui.emailID, fields.getEmailID());
		}

	}
}
