package com.builds.test.fs;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;

import com.builds.uimaps.fs.FSLeadSummaryAddLeadPage;
import com.builds.utilities.FranconnectUtil;

public class FranchiseeSalesCommonMethods {

	FranconnectUtil fc = new FranconnectUtil();

	public SalesLeadFilter addSaleLeadForFilter(WebDriver driver, Map<String, String> config, boolean isBasicnfoOnly,
			Map<String, String> customDataSet) throws Exception {
		Reporter.log("********************* Add Sales Lead for filter********************* \n");

		String testCaseId = "TC_FC_QA_Sale_Lead_Primary";
		SalesLeadFilter franchiseesale = new SalesLeadFilter();
		WebDriverWait wait = new WebDriverWait(driver, 60);

		Map<String, String> dataSet = new HashMap<String, String>();
		Map<String, String> newdataSetTestCase = new HashMap<String, String>();
		newdataSetTestCase = fc.utobj().readTestData("sales", testCaseId);

		if (customDataSet != null && customDataSet.size() > 0) {
			for (String key : newdataSetTestCase.keySet()) {
				// System.out.println("Key = " + key + " - " +
				// dataSet.get(key));
				if (customDataSet.containsKey(key)) {
					dataSet.put(key, customDataSet.get(key));
				} else {
					dataSet.put(key, newdataSetTestCase.get(key));
				}
			}
		} else {
			for (String key : newdataSetTestCase.keySet()) {
				dataSet.put(key, newdataSetTestCase.get(key));
			}

		}

		if (isBasicnfoOnly) {
			franchiseesale.setfirstName(dataSet.get("firstName"));
			franchiseesale.setlastName(dataSet.get("lastName"));
			franchiseesale.setcountry(dataSet.get("country"));
			franchiseesale.setstateID(dataSet.get("stateID"));
			franchiseesale.setcity(dataSet.get("city"));
			franchiseesale.setphone(dataSet.get("phone"));
			franchiseesale.setmobile(dataSet.get("mobile"));
			franchiseesale.setemailID(dataSet.get("emailID"));
			franchiseesale.setleadOwnerID(dataSet.get("leadOwnerID"));
			franchiseesale.setleadSource2ID(dataSet.get("leadSource2ID"));
			franchiseesale.setleadSource3ID(dataSet.get("leadSource3ID"));
			franchiseesale.setautomatic(dataSet.get("automatic"));
			franchiseesale.setassignAutomaticCampaign(dataSet.get("assignAutomaticCampaign"));

		} else {

			franchiseesale.setsalutation(dataSet.get("salutation"));
			franchiseesale.setfirstName(dataSet.get("firstName"));
			franchiseesale.setlastName(dataSet.get("lastName"));
			franchiseesale.setaddress(dataSet.get("address"));
			franchiseesale.setaddress2(dataSet.get("address2"));
			franchiseesale.setcity(dataSet.get("city"));
			franchiseesale.setcountry(dataSet.get("country"));
			franchiseesale.setstateID(dataSet.get("stateID"));
			franchiseesale.setzip(dataSet.get("zip"));
			franchiseesale.setcountyID(dataSet.get("countyID"));
			franchiseesale.setprimaryPhoneToCall(dataSet.get("primaryPhoneToCall"));
			franchiseesale.setbestTimeToContact(dataSet.get("bestTimeToContact"));
			franchiseesale.setphone(dataSet.get("phone"));
			franchiseesale.setphoneExt(dataSet.get("phoneExt"));
			franchiseesale.sethomePhone(dataSet.get("homePhone"));
			franchiseesale.sethomePhoneExt(dataSet.get("homePhoneExt"));
			franchiseesale.setfax(dataSet.get("fax"));
			franchiseesale.setmobile(dataSet.get("mobile"));
			franchiseesale.setemailID(dataSet.get("emailID"));
			franchiseesale.setcompanyName(dataSet.get("companyName"));
			franchiseesale.setcomments(dataSet.get("comments"));
			franchiseesale.setsendAutomaticMail(dataSet.get("sendAutomaticMail"));
			franchiseesale.setassignUser(dataSet.get("assignUser"));
			franchiseesale.setleadOwnerID(dataSet.get("leadOwnerID"));
			franchiseesale.setautomatic(dataSet.get("automatic"));
			franchiseesale.setleadRatingID(dataSet.get("leadRatingID"));
			franchiseesale.setmarketingCodeId(dataSet.get("marketingCodeId"));
			franchiseesale.setleadSource2ID(dataSet.get("leadSource2ID"));
			franchiseesale.setleadSource3ID(dataSet.get("leadSource3ID"));
			franchiseesale.setotherLeadSourceDetail(dataSet.get("otherLeadSourceDetail"));
			franchiseesale.setbrokerID(dataSet.get("brokerID"));
			franchiseesale.setliquidCapitalMin(dataSet.get("liquidCapitalMin"));
			franchiseesale.setliquidCapitalMax(dataSet.get("liquidCapitalMax"));
			franchiseesale.setinvestTimeframe(dataSet.get("investTimeframe"));
			franchiseesale.setbackground(dataSet.get("background"));
			franchiseesale.setsourceOfFunding(dataSet.get("sourceOfFunding"));
			franchiseesale.setnextCallDate(dataSet.get("nextCallDate"));
			franchiseesale.setnoOfUnitReq(dataSet.get("noOfUnitReq"));
			franchiseesale.setstatusChangeDate(dataSet.get("statusChangeDate"));
			franchiseesale.setnoOfFieldSold(dataSet.get("noOfFieldSold"));
			franchiseesale.setdateOfOpen(dataSet.get("dateOfOpen"));
			franchiseesale.setforecastClosureDate(dataSet.get("forecastClosureDate"));
			franchiseesale.setprobability(dataSet.get("probability"));
			franchiseesale.setforecastRating(dataSet.get("forecastRating"));
			franchiseesale.setforecastRevenue(dataSet.get("forecastRevenue"));
			franchiseesale.setassignAutomaticCampaign(dataSet.get("assignAutomaticCampaign"));
			franchiseesale.setcampaignID(dataSet.get("campaignID"));
			franchiseesale.setbrandMapping_0brandID(dataSet.get("brandMapping_0brandID"));

		}

		if (fc.utobj().validate(testCaseId) == true) {
			try {

				fc.sales().sales_common().fsModule(driver);
				Sales fs = new Sales();
				fs.leadManagement(driver);
				FSLeadSummaryPageTest fsPage = new FSLeadSummaryPageTest();
				fsPage.clickAddLeadLink(driver);
				fc.utobj().printTestStep("Add a lead and Go to Primary Info");

				FSLeadSummaryAddLeadPage leadAddPage = new FSLeadSummaryAddLeadPage(driver);

				if (isBasicnfoOnly) {
					fc.utobj().sendKeys(driver, leadAddPage.firstName, dataSet.get("firstName"));
					fc.utobj().sendKeys(driver, leadAddPage.lastName, dataSet.get("lastName"));
					fc.utobj().sendKeys(driver, leadAddPage.city, dataSet.get("city"));
					fc.utobj().selectDropDownByVisibleText(driver, leadAddPage.country, dataSet.get("country"));
					fc.utobj().selectDropDownByVisibleText(driver, leadAddPage.state, dataSet.get("stateID"));
					fc.utobj().sendKeys(driver, leadAddPage.phone, dataSet.get("phone"));
					fc.utobj().sendKeys(driver, leadAddPage.mobile, dataSet.get("mobile"));
					fc.utobj().sendKeys(driver, leadAddPage.emailID, dataSet.get("emailID"));
					fc.utobj().selectDropDownByPartialText(driver, leadAddPage.leadOwnerID, dataSet.get("leadOwnerID"));
					fc.utobj().selectDropDownByVisibleText(driver, leadAddPage.leadSource2ID,
							dataSet.get("leadSource2ID").toString());
					fc.utobj().selectDropDownByVisibleText(driver, leadAddPage.leadSource3ID,
							dataSet.get("leadSource3ID").toString());
					if (customDataSet != null && customDataSet.get("automatic") != null) {
						fc.utobj().clickElement(driver, leadAddPage.automatic);
					}
					if (customDataSet != null && customDataSet.get("assignAutomaticCampaign") != null) {
						fc.utobj().clickElement(driver, leadAddPage.assignAutomaticCampaign1);
					}

				} else {
					fc.utobj().selectDropDown(driver, leadAddPage.salutation, dataSet.get("salutation"));
					fc.utobj().sendKeys(driver, leadAddPage.firstName, dataSet.get("firstName"));
					fc.utobj().sendKeys(driver, leadAddPage.lastName, dataSet.get("lastName"));
					fc.utobj().sendKeys(driver, leadAddPage.address, dataSet.get("address"));
					fc.utobj().sendKeys(driver, leadAddPage.address2, dataSet.get("address2"));
					fc.utobj().sendKeys(driver, leadAddPage.city, dataSet.get("city"));
					fc.utobj().selectDropDownByVisibleText(driver, leadAddPage.country, dataSet.get("country"));
					fc.utobj().selectDropDownByVisibleText(driver, leadAddPage.state, dataSet.get("stateID"));
					fc.utobj().sendKeys(driver, leadAddPage.zip, dataSet.get("zip"));
					fc.utobj().sendKeys(driver, leadAddPage.county, dataSet.get("countyID"));
					fc.utobj().selectDropDownByPartialText(driver, leadAddPage.preferredModeofContact,
							dataSet.get("primaryPhoneToCall"));
					fc.utobj().sendKeys(driver, leadAddPage.bestTimeToContact, dataSet.get("bestTimeToContact"));
					fc.utobj().sendKeys(driver, leadAddPage.phone, dataSet.get("phone"));
					fc.utobj().sendKeys(driver, leadAddPage.phoneExt, dataSet.get("phoneExt"));
					fc.utobj().sendKeys(driver, leadAddPage.homePhone, dataSet.get("homePhone"));
					fc.utobj().sendKeys(driver, leadAddPage.homePhoneExt, dataSet.get("homePhoneExt"));
					fc.utobj().sendKeys(driver, leadAddPage.fax, dataSet.get("fax"));
					fc.utobj().sendKeys(driver, leadAddPage.mobile, dataSet.get("mobile"));
					fc.utobj().sendKeys(driver, leadAddPage.emailID, dataSet.get("emailID"));
					fc.utobj().sendKeys(driver, leadAddPage.companyName, dataSet.get("companyName"));
					fc.utobj().sendKeys(driver, leadAddPage.comments, dataSet.get("comments"));
					fc.utobj().selectDropDownByPartialText(driver, leadAddPage.leadOwnerID, dataSet.get("leadOwnerID"));
					if (customDataSet != null && customDataSet.get("automatic") != null) {
						fc.utobj().clickElement(driver, leadAddPage.automatic);
					}
					fc.utobj().selectDropDownByVisibleText(driver, leadAddPage.leadRatingID,
							dataSet.get("leadRatingID"));
					fc.utobj().selectDropDownByVisibleText(driver, leadAddPage.marketingCode,
							dataSet.get("marketingCodeId"));
					fc.utobj().selectDropDownByVisibleText(driver, leadAddPage.leadSource2ID,
							dataSet.get("leadSource2ID").toString());
					fc.utobj().selectDropDownByVisibleText(driver, leadAddPage.leadSource3ID,
							dataSet.get("leadSource3ID").toString());
					fc.utobj().sendKeys(driver, leadAddPage.currentNetWorth, dataSet.get("liquidCapitalMin"));
					fc.utobj().sendKeys(driver, leadAddPage.cashAvailableForInves, dataSet.get("liquidCapitalMax"));
					fc.utobj().sendKeys(driver, leadAddPage.investmentTimeframe, dataSet.get("investTimeframe"));
					fc.utobj().sendKeys(driver, leadAddPage.sourceOfInvestment, dataSet.get("sourceOfFunding"));
					fc.utobj().sendKeys(driver, leadAddPage.background, dataSet.get("background"));
					fc.utobj().sendKeys(driver, leadAddPage.nextCallDate, dataSet.get("nextCallDate"));
					fc.utobj().sendKeys(driver, leadAddPage.noOfUnitReq, dataSet.get("noOfUnitReq"));
					fc.utobj().sendKeys(driver, leadAddPage.forecastClosureDate,
							dataSet.get("forecastClosureDate"));
					fc.utobj().sendKeys(driver, leadAddPage.probability, dataSet.get("probability"));
					fc.utobj().selectDropDownByVisibleText(driver, leadAddPage.forecastRating,
							dataSet.get("forecastRating").toString());
					fc.utobj().sendKeys(driver, leadAddPage.forecastRevenue, dataSet.get("forecastRevenue").toString());
					if (customDataSet != null && customDataSet.get("assignAutomaticCampaign") != null) {
						fc.utobj().clickElement(driver, leadAddPage.assignAutomaticCampaign1);
					}
					try {
						boolean isBrandOpen = fc.utobj().getElementByID(driver, "brandMapping_0brandID").isDisplayed();
						if (isBrandOpen) {
							fc.utobj().selectDropDownByVisibleText(driver, leadAddPage.divisionDropDown,
									dataSet.get("brandMapping_0brandID").toString());
						}
					} catch (Exception e) {
					}

				} // End full data

				fc.utobj().clickElement(driver, leadAddPage.save);

				wait = new WebDriverWait(driver, 45);
				boolean leadadded = fc.utobj().getElementByXpath(driver, ".//span[@class='text_b' and text()='"
						+ dataSet.get("firstName") + " " + dataSet.get("lastName") + "']").isDisplayed();

				if (leadadded) {
					fc.utobj().printTestStep("Lead added sucessfuly!");
				} else {
					fc.utobj().throwsException("Lead addtion not Succesful may be xml issue.");
				}

			} catch (Exception e) {
				fc.utobj().throwsException("Lead addtion not Succesful " + e.getMessage());
				// fc.utobj().quitBrowserOnCatchBasicMethod(driver, config, e,
				// testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in adding Lead , please refer screenshot!");
		}
		return franchiseesale;
	}

}
