package com.builds.test.fs;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.builds.uimaps.fs.GroupsUI;
import com.builds.utilities.FranconnectUtil;

class GroupTest {

	public void clickCreateGroupBtn(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		GroupsUI gui = new GroupsUI(driver);
		fc.utobj().printTestStep("Click Create Group Button");
		fc.utobj().clickElement(driver, gui.createGroupBtn);
		fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
	}

	public void clickSaveBtn(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		GroupsUI gui = new GroupsUI(driver);
		fc.utobj().printTestStep("Click Save Button");
		fc.utobj().clickElement(driver, gui.Save_Button_ByValue);
	}

	public void clickCancelBtn(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		GroupsUI gui = new GroupsUI(driver);
		fc.utobj().printTestStep("Click Cancel Button");
		fc.utobj().clickElement(driver, gui.Cancel_Button_ByValue);
	}

	public void acceptAlertAfterSavingFilters(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		fc.utobj().printTestStep("Accept Confirmation for filters");
		fc.utobj().acceptAlertBox(driver);
	}

	public void addGroupDetails_AndCreate(WebDriver driver, Group group) throws Exception {
		clickCreateGroupBtn(driver);
		fillGroupDetails(driver, group);
		clickCreateBtn(driver);
	}

	public void fillGroupDetails(WebDriver driver, Group group) throws Exception {

		GroupsUI gui = new GroupsUI(driver);
		FranconnectUtil fc = new FranconnectUtil();
		fc.utobj().printTestStep("Fill Group Details");

		if (group.getName() != null) {
			fc.utobj().printTestStep("Enter Group Name");
			fc.utobj().sendKeys(driver, gui.groupName, group.getName());
		}
		if (group.getDescription() != null) {
			fc.utobj().printTestStep("Enter Group Description");
			fc.utobj().sendKeys(driver, gui.groupDescription, group.getDescription());
		}
		if (group.getVisibility() != null) {
			fc.utobj().printTestStep("Select Visibility : " + group.getVisibility());
			fc.utobj().selectDropDown(driver, gui.visibility, group.getVisibility());
		}
		if (group.getGroupType() != null) {
			boolean isSmartGroup = gui.groupChoice_Input_ById.isSelected();
			if (isSmartGroup == false && group.getGroupType().equalsIgnoreCase("Smart")) {
				fc.utobj().printTestStep("Select Group Type : Smart");
				gui.smartGroup.click();
			}
			if (isSmartGroup == true && group.getGroupType().equalsIgnoreCase("Regular")) {
				fc.utobj().printTestStep("Select Group Type : Regular");
				gui.smartGroup.click();
			}
		}
	}

	public void openSearchFilter(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		try {
			GroupsUI gui = new GroupsUI(driver);
			if (fc.utobj().getElement(driver, gui.Search_Button_ByID).isDisplayed() == false) {
				fc.utobj().printTestStep("Open Search Filter");
				fc.utobj().clickElement(driver, gui.ico_Filter_XPATH_ByDataRole);
			}
		} catch (Exception e) {
			fc.utobj().throwsException("Not able to open the search Filter");
		}
	}

	public void closeSearchFilter(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		try {
			GroupsUI gui = new GroupsUI(driver);
			if (fc.utobj().getElement(driver, gui.Search_Button_ByID).isDisplayed() == true) {
				fc.utobj().printTestStep("Close Search Filter");
				fc.utobj().clickElement(driver, gui.ico_Filter_XPATH_ByDataRole);
			}
		} catch (Exception e) {
			fc.utobj().throwsException("Not able to close the search Filter");
		}
	}

	public void salesGroupsSearch(WebDriver driver, Group group) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		GroupsUI gui = new GroupsUI(driver);

		openSearchFilter(driver);

		if (group.getName() != null) {
			fc.utobj().printTestStep("Enter Group Name in Search Box");
			fc.utobj().sendKeys(driver, gui.searchTemplate_Input_ByID, group.getName());
		}

		if (group.getCreationDate() != null) {
			//
		}

		if (group.getVisibility() != null) {
			fc.utobj().selectRadioValueInDropDown(driver, gui.dropdown_Div_ById, gui.textField_dropdown_Div_ById,
					group.getVisibility());
		}

		clickSearch(driver);

	}

	public void clickCreateBtn(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		GroupsUI gui = new GroupsUI(driver);
		fc.utobj().printTestStep("Click Create Button on Add Group page");
		fc.utobj().clickElement(driver, gui.Create_Button_ByXPATH);
	}

	public void clickSearch(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		GroupsUI gui = new GroupsUI(driver);
		fc.utobj().printTestStep("Click Search");
		fc.utobj().clickElement(driver, gui.Search_Button_ByID);

	}

	public void modifyRegularGroup(WebDriver driver, Group group) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		GroupsUI gui = new GroupsUI(driver);

		fc.utobj().printTestStep("Modify Group : " + group.getName());
		click_Ico_ThreeDots_ForGroup(driver, group);
		fc.utobj().clickElement(driver,
				fc.utobj().getElementByXpath(driver, gui.selectMenuFromRightActionDiv("Modify")));
		fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);

	}

	private void click_Ico_ThreeDots_ForGroup(WebDriver driver, Group group) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		fc.utobj().printTestStep("Click on Top Right Action");
		GroupsUI gui = new GroupsUI(driver);
		fc.utobj().clickElement(driver,
				fc.utobj().getElementByXpath(driver, gui.clickGroup_ico_ThreeDots(group.getName())));
	}

	public void clickAssociateWithLead(WebDriver driver, Group group) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		GroupsUI gui = new GroupsUI(driver);

		fc.utobj().printTestStep("Associate Group with lead");
		click_Ico_ThreeDots_ForGroup(driver, group);
		fc.utobj().clickElement(driver,
				fc.utobj().getElementByXpath(driver, gui.selectMenuFromRightActionDiv("Associate with Leads")));
		fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);

	}

	public void fillAndSearchLeadInfoInGroupFilterPage(WebDriver driver, Lead lead) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		GroupsUI gui = new GroupsUI(driver);

		fc.utobj().printTestStep("Fill the lead info to search");

		if (lead.getFirstName() != null) {
			fc.utobj().sendKeys(driver, gui.firstName, lead.getFirstName());
		}

		if (lead.getLastName() != null) {
			fc.utobj().sendKeys(driver, gui.lastName, lead.getLastName());
		}

		if (lead.getEmail() != null) {
			fc.utobj().sendKeys(driver, gui.emailID, lead.getEmail());
		}

		fc.utobj().clickElement(driver, gui.Search_Input_ByValue);

		fc.utobj().clickElement(driver, gui.yes_Input_ByValue);
		fc.utobj().clickElement(driver, gui.Close_Input_ByValue);

	}

	public void clickLeadCountForGroup(WebDriver driver, Group group) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		GroupsUI gui = new GroupsUI(driver);

		String position = "5";

		fc.utobj().printTestStep("Click on the lead count");
		fc.utobj().clickElement(driver,
				fc.utobj().getElementByXpath(driver, gui.getXPathOfLeadCount(group.getName(), position)));

	}

	public int getRowCountOfGroup(WebDriver driver) {
		FranconnectUtil fc = new FranconnectUtil();
		GroupsUI gui = new GroupsUI(driver);

		List<WebElement> list = driver.findElements(By.xpath(gui.getRowXPathOfGroup()));

		if (list != null && !list.isEmpty()) {
			return list.size() - 1;
		} else {
			return 0;
		}
	}

	public void setViewPerPage(WebDriver driver, String count) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		GroupsUI gui = new GroupsUI(driver);
		fc.utobj().printTestStep("Click View Per Page");
		fc.utobj().clickElement(driver, gui.clickViewPerPage);

		fc.utobj().clickElement(driver,
				fc.utobj().getElementByXpath(driver, gui.getXPathOfViewPerPageCount("+count+")));
	}

	public void clickGroupName(WebDriver driver, Group group) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		GroupsUI gui = new GroupsUI(driver);

		fc.utobj().printTestStep("Click on group name");
		fc.utobj().clickLink(driver, group.getName());
		fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
	}

	public Group validateGroupInfoInFrame(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		GroupsUI gui = new GroupsUI(driver);

		Group group = new Group();

		// List<WebElement> groupInfo =
		// driver.findElements(By.xpath(".//div[@class='grid-row']"));

		// closeGroupDetailsFrame(driver);

		return group;
	}

	public void closeGroupDetailsFrame(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		GroupsUI gui = new GroupsUI(driver);

		fc.utobj().printTestStep("Close the Group Details frame.");
		fc.utobj().clickElement(driver, gui.Close_Button_ByValue);
	}

	public void archiveGroup(WebDriver driver, Group group) throws Exception {

		FranconnectUtil fc = new FranconnectUtil();
		GroupsUI gui = new GroupsUI(driver);

		fc.utobj().printTestStep("Archive Group : " + group.getName());
		click_Ico_ThreeDots_ForGroup(driver, group);
		fc.utobj().clickElement(driver,
				fc.utobj().getElementByXpath(driver, gui.selectMenuFromRightActionDiv("Archive")));
		fc.utobj().acceptAlertBox(driver);
	}

	public void archiveGroup_Batch_Top(WebDriver driver, Group group) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		GroupsUI gui = new GroupsUI(driver);

		fc.utobj().printTestStep("Click on Top Action for Batch Selection");
		fc.utobj().clickElement(driver, gui.top_Action_Batch_ByXpath);
		fc.utobj().clickElement(driver,
				fc.utobj().getElementByXpath(driver, gui.selectMenuFromTopActionDiv("Archive")));
		fc.utobj().acceptAlertBox(driver);
	}

	public void archiveGroup_Batch_Bottom(WebDriver driver, Group group) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		GroupsUI gui = new GroupsUI(driver);

		fc.utobj().printTestStep("Archive Group through bottom button : " + group.getName());
		fc.utobj().clickElement(driver, gui.Archive_Button_ByValue);
		fc.utobj().acceptAlertBox(driver);
	}

	public void deleteGroup(WebDriver driver, Group group) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		GroupsUI gui = new GroupsUI(driver);

		fc.utobj().printTestStep("Delete Group : " + group.getName());
		click_Ico_ThreeDots_ForGroup(driver, group);
		fc.utobj().clickElement(driver,
				fc.utobj().getElementByXpath(driver, gui.selectMenuFromRightActionDiv("Delete")));
		fc.utobj().acceptAlertBox(driver);
	}

	public void deleteGroup_Batch_Top(WebDriver driver, Group group) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		GroupsUI gui = new GroupsUI(driver);

		fc.utobj().printTestStep("Click on Top Action for Batch Selection");
		fc.utobj().clickElement(driver, gui.top_Action_Batch_ByXpath);
		fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, gui.selectMenuFromTopActionDiv("Delete")));
		fc.utobj().acceptAlertBox(driver);

	}

	public void deleteGroup_Batch_Bottom(WebDriver driver, Group group) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		GroupsUI gui = new GroupsUI(driver);

		fc.utobj().printTestStep("Delete Group through bottom button : " + group.getName());
		fc.utobj().clickElement(driver, gui.Delete_Button_ByValue);
		fc.utobj().acceptAlertBox(driver);

	}

	public void goToArchiveGroupTab(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		GroupsUI gui = new GroupsUI(driver);

		fc.utobj().printTestStep("Go to Archived Groups");
		fc.utobj().clickElement(driver, gui.archivedGroupsTab);
	}

	public void goToGroupTab(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		GroupsUI gui = new GroupsUI(driver);

		fc.utobj().printTestStep("Go to Groups");
		fc.utobj().clickElement(driver, gui.groupsTab);
	}

	public void restoreGroup(WebDriver driver, Group group) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		GroupsUI gui = new GroupsUI(driver);

		fc.utobj().printTestStep("Restore Group : " + group.getName());
		click_Ico_ThreeDots_ForGroup(driver, group);
		fc.utobj().clickElement(driver,
				fc.utobj().getElementByXpath(driver, gui.selectMenuFromRightActionDiv("Restore")));
		fc.utobj().acceptAlertBox(driver);

	}

	public void checkGroupName(WebDriver driver, Group group) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		GroupsUI gui = new GroupsUI(driver);

		fc.utobj().clickElement(driver,
				fc.utobj().getElementByXpath(driver, gui.checkboxAgainstGroup(group.getName())));
	}

	public void clickModifyButton(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		GroupsUI gui = new GroupsUI(driver);

		fc.utobj().clickElement(driver, gui.modify_Button_ByValue);
	}

	public void clickFiltersForAvailableFields(WebDriver driver, String SubModuleName, String fieldName)
			throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		GroupsUI gui = new GroupsUI(driver);

		fc.utobj().clickElement(driver, gui.AvailableFields_Button_ByClass);
		fc.utobj().clickElement(driver,
				fc.utobj().getElementByXpath(driver, gui.clickFieldNameInFilter(SubModuleName, fieldName)));
		fc.utobj().clickElement(driver, gui.AvailableFields_Button_ByClass);

	}

	public void selectFieldsValueInFilters(WebDriver driver, String SubModuleName, String fieldName, String fieldType,
			String position, String fieldValue) throws Exception {

		FranconnectUtil fc = new FranconnectUtil();
		GroupsUI gui = new GroupsUI(driver);

		if (fieldType.equalsIgnoreCase("select")) {
			fc.utobj().selectDropDownByVisibleText(driver,
					gui.getFieldXpathInFilters(SubModuleName + " > " + fieldName, fieldType, position), fieldValue);
		}

		if (fieldType.equalsIgnoreCase("input")) {
			fc.utobj().sendKeys(driver,
					driver.findElement(By
							.xpath(gui.getFieldXpathInFilters(SubModuleName + " > " + fieldName, fieldType, position))),
					fieldValue);
		}
	}

	public void clickDeleteFilter(WebDriver driver, String SubModuleName, String fieldName) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		GroupsUI gui = new GroupsUI(driver);

		fc.utobj().printTestStep("Delete filter : " + SubModuleName + " > " + fieldName);
		fc.utobj().clickElement(driver, gui.getdeleteFieldXpath(SubModuleName + " > " + fieldName));
	}
}
