package com.builds.test.fs;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.fs.LaunchCampaignUI;
import com.builds.utilities.FranconnectUtil;

public class LaunchCampaignTest {

	public void clickLaunchCampaign_Button(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		LaunchCampaignUI ui = new LaunchCampaignUI(driver);

		fc.utobj().printTestStep("Click Launch Campaign");
		fc.utobj().clickElement(driver, ui.launchCampaign);

	}

}
