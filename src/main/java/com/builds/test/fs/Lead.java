package com.builds.test.fs;

class Lead {

	private String SelectLeadType;
	private String ExistingLead;
	private String ExistingOwner;
	private String Salutation;
	private String FirstName;
	private String LastName;
	private String Address1;
	private String Address2;
	private String City;
	private String Country;
	private String StateProvince;
	private String ZipPostalCode;
	private String County;
	private String PreferredModeofContact;
	private String BestTimeToContact;
	private String WorkPhone;
	private String WorkPhoneExtension;
	private String HomePhone;
	private String HomePhoneExtension;
	private String Fax;
	private String Mobile;
	private String Email;
	private String CompanyName;
	private String Comment;
	private String LeadStatus;
	private String LeadOwner;
	private String BasedonAssignmentRules;
	private String LeadRating;
	private String MarketingCode;
	private String LeadSourceCategory;
	private String LeadSourceDetails;
	private String OtherLeadSources;
	private String CurrentNetWorth;
	private String CashAvailableforInvestment;
	private String InvestmentTimeframe;
	private String Background;
	private String SourceOfInvestment;
	private String NextCallDate;
	private String NoOfUnitsLocationsRequested;
	private String FranchiseAwarded;
	private String Division;
	private String PreferredCity1;
	private String PreferredCountry1;
	private String PreferredStateProvince1;
	private String PreferredCity2;
	private String PreferredCountry2;
	private String PreferredStateProvince2;
	private String NewAvailableSites;
	private String ExistingSites;
	private String ResaleSites;
	private String ForecastClosureDate;
	private String Probability;
	private String ForecastRating;
	private String ForecastRevenue;
	private String CampaignName;
	private String sendAutomaticMail;

	public String getSendAutomaticMail() {
		return sendAutomaticMail;
	}

	public void setSendAutomaticMail(String sendAutomaticMail) {
		this.sendAutomaticMail = sendAutomaticMail;
	}

	public String getSelectLeadType() {
		return SelectLeadType;
	}

	public void setSelectLeadType(String selectLeadType) {
		SelectLeadType = selectLeadType;
	}

	public String getExistingLead() {
		return ExistingLead;
	}

	public void setExistingLead(String existingLead) {
		ExistingLead = existingLead;
	}

	public String getExistingOwner() {
		return ExistingOwner;
	}

	public void setExistingOwner(String existingOwner) {
		ExistingOwner = existingOwner;
	}

	public String getSalutation() {
		return Salutation;
	}

	public void setSalutation(String salutation) {
		Salutation = salutation;
	}

	public String getFirstName() {
		return FirstName;
	}

	public void setFirstName(String firstName) {
		FirstName = firstName;
	}

	public String getLastName() {
		return LastName;
	}

	public void setLastName(String lastName) {
		LastName = lastName;
	}

	public String getAddress1() {
		return Address1;
	}

	public void setAddress1(String address1) {
		Address1 = address1;
	}

	public String getAddress2() {
		return Address2;
	}

	public void setAddress2(String address2) {
		Address2 = address2;
	}

	public String getCity() {
		return City;
	}

	public void setCity(String city) {
		City = city;
	}

	public String getCountry() {
		return Country;
	}

	public void setCountry(String country) {
		Country = country;
	}

	public String getStateProvince() {
		return StateProvince;
	}

	public void setStateProvince(String stateProvince) {
		StateProvince = stateProvince;
	}

	public String getZipPostalCode() {
		return ZipPostalCode;
	}

	public void setZipPostalCode(String zipPostalCode) {
		ZipPostalCode = zipPostalCode;
	}

	public String getCounty() {
		return County;
	}

	public void setCounty(String county) {
		County = county;
	}

	public String getPreferredModeofContact() {
		return PreferredModeofContact;
	}

	public void setPreferredModeofContact(String preferredModeofContact) {
		PreferredModeofContact = preferredModeofContact;
	}

	public String getBestTimeToContact() {
		return BestTimeToContact;
	}

	public void setBestTimeToContact(String bestTimeToContact) {
		BestTimeToContact = bestTimeToContact;
	}

	public String getWorkPhone() {
		return WorkPhone;
	}

	public void setWorkPhone(String workPhone) {
		WorkPhone = workPhone;
	}

	public String getWorkPhoneExtension() {
		return WorkPhoneExtension;
	}

	public void setWorkPhoneExtension(String workPhoneExtension) {
		WorkPhoneExtension = workPhoneExtension;
	}

	public String getHomePhone() {
		return HomePhone;
	}

	public void setHomePhone(String homePhone) {
		HomePhone = homePhone;
	}

	public String getHomePhoneExtension() {
		return HomePhoneExtension;
	}

	public void setHomePhoneExtension(String homePhoneExtension) {
		HomePhoneExtension = homePhoneExtension;
	}

	public String getFax() {
		return Fax;
	}

	public void setFax(String fax) {
		Fax = fax;
	}

	public String getMobile() {
		return Mobile;
	}

	public void setMobile(String mobile) {
		Mobile = mobile;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		Email = email;
	}

	public String getCompanyName() {
		return CompanyName;
	}

	public void setCompanyName(String companyName) {
		CompanyName = companyName;
	}

	public String getComment() {
		return Comment;
	}

	public void setComment(String comment) {
		Comment = comment;
	}

	public String getLeadStatus() {
		return LeadStatus;
	}

	public void setLeadStatus(String leadStatus) {
		LeadStatus = leadStatus;
	}

	public String getLeadOwner() {
		return LeadOwner;
	}

	public void setLeadOwner(String leadOwner) {
		LeadOwner = leadOwner;
	}

	public String getBasedonAssignmentRules() {
		return BasedonAssignmentRules;
	}

	public void setBasedonAssignmentRules(String basedonAssignmentRules) {
		BasedonAssignmentRules = basedonAssignmentRules;
	}

	public String getLeadRating() {
		return LeadRating;
	}

	public void setLeadRating(String leadRating) {
		LeadRating = leadRating;
	}

	public String getMarketingCode() {
		return MarketingCode;
	}

	public void setMarketingCode(String marketingCode) {
		MarketingCode = marketingCode;
	}

	public String getLeadSourceCategory() {
		return LeadSourceCategory;
	}

	public void setLeadSourceCategory(String leadSourceCategory) {
		LeadSourceCategory = leadSourceCategory;
	}

	public String getLeadSourceDetails() {
		return LeadSourceDetails;
	}

	public void setLeadSourceDetails(String leadSourceDetails) {
		LeadSourceDetails = leadSourceDetails;
	}

	public String getOtherLeadSources() {
		return OtherLeadSources;
	}

	public void setOtherLeadSources(String otherLeadSources) {
		OtherLeadSources = otherLeadSources;
	}

	public String getCurrentNetWorth() {
		return CurrentNetWorth;
	}

	public void setCurrentNetWorth(String currentNetWorth) {
		CurrentNetWorth = currentNetWorth;
	}

	public String getCashAvailableforInvestment() {
		return CashAvailableforInvestment;
	}

	public void setCashAvailableforInvestment(String cashAvailableforInvestment) {
		CashAvailableforInvestment = cashAvailableforInvestment;
	}

	public String getInvestmentTimeframe() {
		return InvestmentTimeframe;
	}

	public void setInvestmentTimeframe(String investmentTimeframe) {
		InvestmentTimeframe = investmentTimeframe;
	}

	public String getBackground() {
		return Background;
	}

	public void setBackground(String background) {
		Background = background;
	}

	public String getSourceOfInvestment() {
		return SourceOfInvestment;
	}

	public void setSourceOfInvestment(String sourceOfInvestment) {
		SourceOfInvestment = sourceOfInvestment;
	}

	public String getNextCallDate() {
		return NextCallDate;
	}

	public void setNextCallDate(String nextCallDate) {
		NextCallDate = nextCallDate;
	}

	public String getNoOfUnitsLocationsRequested() {
		return NoOfUnitsLocationsRequested;
	}

	public void setNoOfUnitsLocationsRequested(String noOfUnitsLocationsRequested) {
		NoOfUnitsLocationsRequested = noOfUnitsLocationsRequested;
	}

	public String getFranchiseAwarded() {
		return FranchiseAwarded;
	}

	public void setFranchiseAwarded(String franchiseAwarded) {
		FranchiseAwarded = franchiseAwarded;
	}

	public String getDivision() {
		return Division;
	}

	public void setDivision(String division) {
		Division = division;
	}

	public String getPreferredCity1() {
		return PreferredCity1;
	}

	public void setPreferredCity1(String preferredCity1) {
		PreferredCity1 = preferredCity1;
	}

	public String getPreferredCountry1() {
		return PreferredCountry1;
	}

	public void setPreferredCountry1(String preferredCountry1) {
		PreferredCountry1 = preferredCountry1;
	}

	public String getPreferredStateProvince1() {
		return PreferredStateProvince1;
	}

	public void setPreferredStateProvince1(String preferredStateProvince1) {
		PreferredStateProvince1 = preferredStateProvince1;
	}

	public String getPreferredCity2() {
		return PreferredCity2;
	}

	public void setPreferredCity2(String preferredCity2) {
		PreferredCity2 = preferredCity2;
	}

	public String getPreferredCountry2() {
		return PreferredCountry2;
	}

	public void setPreferredCountry2(String preferredCountry2) {
		PreferredCountry2 = preferredCountry2;
	}

	public String getPreferredStateProvince2() {
		return PreferredStateProvince2;
	}

	public void setPreferredStateProvince2(String preferredStateProvince2) {
		PreferredStateProvince2 = preferredStateProvince2;
	}

	public String getNewAvailableSites() {
		return NewAvailableSites;
	}

	public void setNewAvailableSites(String newAvailableSites) {
		NewAvailableSites = newAvailableSites;
	}

	public String getExistingSites() {
		return ExistingSites;
	}

	public void setExistingSites(String existingSites) {
		ExistingSites = existingSites;
	}

	public String getResaleSites() {
		return ResaleSites;
	}

	public void setResaleSites(String resaleSites) {
		ResaleSites = resaleSites;
	}

	public String getForecastClosureDate() {
		return ForecastClosureDate;
	}

	public void setForecastClosureDate(String forecastClosureDate) {
		ForecastClosureDate = forecastClosureDate;
	}

	public String getProbability() {
		return Probability;
	}

	public void setProbability(String probability) {
		Probability = probability;
	}

	public String getForecastRating() {
		return ForecastRating;
	}

	public void setForecastRating(String forecastRating) {
		ForecastRating = forecastRating;
	}

	public String getForecastRevenue() {
		return ForecastRevenue;
	}

	public void setForecastRevenue(String forecastRevenue) {
		ForecastRevenue = forecastRevenue;
	}

	public String getCampaignName() {
		return CampaignName;
	}

	public void setCampaignName(String campaignName) {
		CampaignName = campaignName;
	}

	public String getLeadFullName() {
		String firstName = getFirstName();
		String lastName = getLastName();

		if (firstName == null) {
			firstName = "";
		}
		if (lastName == null) {
			lastName = "";
		}
		return (firstName + " " + lastName).trim();
	}

}
