package com.builds.test.fs;

public class LeadAsCoApplicant extends Lead {

	private String CoApplicantRelationship;

	public String getCoApplicantRelationship() {
		return CoApplicantRelationship;
	}

	public void setCoApplicantRelationship(String coApplicantRelationship) {
		CoApplicantRelationship = coApplicantRelationship;
	}

}
