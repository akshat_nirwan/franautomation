package com.builds.test.fs;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.uimaps.fs.LeadUI;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class LeadManagementTemporary {

	@Test(groups = { "addLead1221" })
	@TestCase(createdOn = "2017-01-02", updatedOn = "2017-01-02", testCaseId = "Sales_AddLead_001", testCaseDescription = "Add a lead with full info")
	private void addPrivateGroup() throws Exception {

		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String firstName = fc.utobj().generateTestData("Test");
		String lastName = fc.utobj().generateTestData("Test");
		String Address1 = fc.utobj().generateTestData("Address1");

		try {
			driver = fc.loginpage().login(driver);
			fc.sales().sales_common().fsModule(driver);
			Sales fsmod = new Sales();
			fsmod.leadManagement(driver);
			FSLeadSummaryPageTest fs = new FSLeadSummaryPageTest();
			fs.clickAddLeadLink(driver);

			LeadUI ldu = new LeadUI(driver);
			LeadAsCoApplicant ldc = new LeadAsCoApplicant();

			// In Between

			Lead ld = new Lead();
			ld.setFirstName(firstName);
			ld.setLastName(lastName);

			ld.setSalutation("Mr.");
			ld.setAddress1(Address1);
			ld.setAddress2("address2");
			ld.setCity("City");
			ld.setCountry("USA");
			ld.setStateProvince("Arizona");
			ld.setZipPostalCode("201301");
			ld.setCounty("Apache");
			ld.setPreferredModeofContact("Fax");
			ld.setBestTimeToContact("9");
			ld.setWorkPhone("1234658");
			ld.setWorkPhoneExtension("54654");
			ld.setHomePhoneExtension("546544");
			ld.setHomePhone("5674654165");
			ld.setMobile("2465215");
			ld.setEmail("frantest2017@gmail.com");
			ld.setCompanyName("companyName");
			ld.setFax("546565");
			ld.setComment("comment");
			// ld.setLeadStatus("");
			ld.setLeadOwner("FranConnect Administrator");
			ld.setLeadRating("Hot");
			ld.setMarketingCode("123");
			ld.setLeadSourceCategory("Friends");
			ld.setLeadSourceDetails("Friends");
			ld.setCurrentNetWorth("652656");
			ld.setCashAvailableforInvestment("2000000");
			ld.setInvestmentTimeframe("123");
			ld.setBackground("background");
			ld.setSourceOfInvestment("sourceOfInvestment");
			ld.setNextCallDate("01/25/2018");
			ld.setNoOfUnitsLocationsRequested("5000");
			// ld.setFranchiseAwarded(franchiseAwarded);
			ld.setDivision("div1");
			ld.setPreferredCity1("preferredCity1");
			ld.setPreferredCity2("preferredCity2");
			ld.setPreferredCountry1("USA");
			ld.setPreferredCountry2("India");
			ld.setPreferredStateProvince1("Arizona");
			ld.setPreferredStateProvince2("Goa");
			ld.setNewAvailableSites("City , Alaska ( Newavailablesite1 )");
			ld.setExistingSites("Jhgyjg , California ( Existingsite )");
			ld.setResaleSites("Dfgfd , Arizona ( Resalesite )");
			ld.setForecastClosureDate("01/31/2018");
			ld.setForecastRevenue("600000");
			ld.setProbability("50");
			ld.setForecastRating("Upside");
			ld.setCampaignName("link test Camp");

			ldc.setCoApplicantRelationship("Spouse");

			LeadTest ldp = new LeadTest();
			ldp.addLeadAndClickSave(driver, ld);

			fc.utobj().clickElement(driver, ldu.Save);

			fc.utobj().clickElement(driver, ldu.CoApplicantsTab);
			fc.utobj().clickElement(driver, ldu.AddCoApplicant);
			fc.utobj().clickElement(driver, ldu.ContactWithAdditionOfLead);
			fc.utobj().clickElement(driver, ldu.ChooseNewLead);

			ldp.addCoApplicant(driver, ld, ldc);
			fc.utobj().clickElement(driver, ldu.Save);

			fc.utobj().clickElement(driver, ldu.Modify);
			ldp.modifyLead(driver, ld);

			System.out.println(ld.getFirstName());
			System.out.println(ld.getLastName());
			System.out.println(ld.getCity());

			// In between
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

}
