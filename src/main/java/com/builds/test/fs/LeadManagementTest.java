package com.builds.test.fs;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.builds.uimaps.fs.LeadManagementUI;
import com.builds.utilities.FranconnectUtil;

public class LeadManagementTest {
	private FranconnectUtil fc = new FranconnectUtil();
	private WebDriver driver;

	public LeadManagementTest(WebDriver driver) {
		this.driver = driver;
	}

	public void click_Lead_OnLeadManagementPage(Lead lead) throws Exception {
		fc.utobj().printTestStep("Click on Lead Name to open the Lead details page.");
		LeadManagementUI ui = new LeadManagementUI(driver);
		// fc.utobj().clickElement(driver, ui.getXpathOfTheLead(lead.getLeadFullName()));
		fc.utobj().clickLink(driver, lead.getLeadFullName());
	}

	public void addLeadAndSave(Lead ld) throws Exception {
		LeadTest ldp = new LeadTest();
		clickAddLead(driver);
		ldp.addLeadAndClickSave(driver, ld);
	}

	public void clickAddLead(WebDriver driver) throws Exception {
		LeadManagementUI ui = new LeadManagementUI(driver);
		fc.utobj().printTestStep("Click Add Lead");
		fc.utobj().clickElement(driver, ui.addLead);
	}

	public void removeFromGroup(WebDriver driver) throws Exception {
		LeadManagementUI ui = new LeadManagementUI(driver);
		fc.utobj().printTestStep("Remove from group");

		fc.utobj().clickElement(driver, ui.removeFromGroup);
		fc.utobj().acceptAlertBox(driver);
	}

	public void clickCheckBoxForLead(Lead ld) throws Exception {
		LeadManagementUI ui = new LeadManagementUI(driver);
		fc.utobj().printTestStep("Select the lead.");
		fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
				ui.getXpathOfTheLeadCheckBox(ld.getFirstName() + " " + ld.getLastName())));
	}

	public void clickCheckBox_All(WebDriver driver) throws Exception {
		LeadManagementUI ui = new LeadManagementUI(driver);
		fc.utobj().printTestStep("Select all the lead.");
		fc.utobj().clickElement(driver, ui.checkAll);
	}

	public void clickRightAction_AndClick_DetailedHistory(String leadName) throws Exception {
		clickRightAction_AndClick_selectActionsFromRightActionMenu(leadName, "Detailed History");
		fc.commonMethods().switch_cboxIframe_frameId(driver);
	}

	public void clickRightAction_AndClick_SendEmail(String leadName) throws Exception {
		clickRightAction_AndClick_selectActionsFromRightActionMenu(leadName, "Send Email");
		fc.commonMethods().switch_cboxIframe_frameId(driver);
	}

	public void clickRightAction_AndClick_LogACall(String leadName) throws Exception {
		clickRightAction_AndClick_selectActionsFromRightActionMenu(leadName, "Log a Call");
		fc.commonMethods().switch_cboxIframe_frameId(driver);
	}

	public void clickRightAction_AndClick_LogATask(String leadName) throws Exception {
		clickRightAction_AndClick_selectActionsFromRightActionMenu(leadName, "Log a Task");
		fc.commonMethods().switch_cboxIframe_frameId(driver);
	}

	public void clickRightAction_AndClick_AddRemarks(String leadName) throws Exception {
		clickRightAction_AndClick_selectActionsFromRightActionMenu(leadName, "Add Remarks");
		fc.commonMethods().switch_cboxIframe_frameId(driver);
	}

	public void clickRightAction_AndClick_Delete(String leadName) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		clickRightAction_AndClick_selectActionsFromRightActionMenu(leadName, "Delete");
		fc.commonMethods().switch_cboxIframe_frameId(driver);
		fc.commonMethods().Click_OK_Input_ByValue(driver);
	}

	public void clickRightAction_AndClick_MovetoInfoMgr(String leadName) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		clickRightAction_AndClick_selectActionsFromRightActionMenu(leadName, "Move to Info Mgr");
		fc.commonMethods().switch_cboxIframe_frameId(driver);
	}

	public void clickRightAction_AndClick_MovetoOpener(String leadName) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		clickRightAction_AndClick_selectActionsFromRightActionMenu(leadName, "Move to Opener");
		fc.commonMethods().switch_cboxIframe_frameId(driver);
	}

	public void clickRightAction_AndClick_OptOutfromEmailList(String leadName) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		clickRightAction_AndClick_selectActionsFromRightActionMenu(leadName, "Opt Out from Email List");
		fc.commonMethods().switch_cboxIframe_frameId(driver);
	}

	public void clickRightAction_AndClick_AssociateCoApplicant(String leadName) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		clickRightAction_AndClick_selectActionsFromRightActionMenu(leadName, "Associate Co-Applicant");
		fc.commonMethods().switch_cboxIframe_frameId(driver);
	}

	private void clickRightAction_AndClick_selectActionsFromRightActionMenu(String leadName, String actionName)
			throws Exception {
		clickRightAction(leadName);
		selectActionsFromRightActionMenu(actionName);
	}

	private void selectActionsFromRightActionMenu(String actionName) throws Exception {
		LeadManagementUI ui = new LeadManagementUI(driver);

		fc.utobj().printTestStep("Select : " + actionName + ", from Right Action Menu");
		List<WebElement> actionMenu = ui.actionsInRigthActionMenu_ByXpath;

		for (WebElement menu : actionMenu) {
			if (actionName.equalsIgnoreCase(menu.getText().trim())) {
				fc.utobj().clickElement(driver, menu);
				return;
			}
		}
	}

	private void clickRightAction(String leadName) throws Exception {
		LeadManagementUI ui = new LeadManagementUI(driver);
		fc.utobj().printTestStep("Click Right Actions");
		fc.utobj().clickElement(driver, ui.getXpathOfTheLeads_RigthActionIcon(leadName));

	}

	private void clickLeftAction(WebDriver driver) throws Exception {
		LeadManagementUI ui = new LeadManagementUI(driver);
		fc.utobj().printTestStep("Click Left Actions");
		fc.utobj().clickElement(driver, ui.ActionsMenu_Left_Input_ByValue);

	}

	public void clickLeftActionsMenu_Click_Delete_ClickOK(WebDriver driver) throws Exception {
		selectActionsFromLeftActionMenu("Delete");
		fc.commonMethods().switch_cboxIframe_frameId(driver);
		fc.commonMethods().Click_OK_Input_ByValue(driver);
	}

	public void clickLeftActionsMenu_Click_ChangeStatus() throws Exception {
		selectActionsFromLeftActionMenu("Change Status");
		fc.commonMethods().switch_cboxIframe_frameId(driver);
	}

	public void clickLeftActionsMenu_Click_logACall() throws Exception {
		selectActionsFromLeftActionMenu("Log a Call");
		fc.commonMethods().switch_cboxIframe_frameId(driver);
	}

	public void clickLeftActionsMenu_Click_SendEmail() throws Exception {
		selectActionsFromLeftActionMenu("Send Email");
		//fc.commonMethods().switch_cboxIframe_frameId(driver);
	}

	public void clickLeftActionsMenu_Click_logATask(WebDriver driver) throws Exception {
		selectActionsFromLeftActionMenu("Log a Task");
		fc.commonMethods().switch_cboxIframe_frameId(driver);
	}

	public void clickLeftActionsMenu_Click_Archive(String notes) throws Exception {
		selectActionsFromLeftActionMenu("Archive Leads");

		fc.utobj().acceptAlertBox(driver);

		ArchiveWindowTest arvhiveTest = new ArchiveWindowTest();
		arvhiveTest.addNotes_ClickArchive(driver, notes);
	}

	public void clickLeftActionsMenu_Click_Add_To_Group_SwitchToiFrame(WebDriver driver) throws Exception {
		selectActionsFromLeftActionMenu("Add To Group");
		fc.commonMethods().switch_cboxIframe_frameId(driver);
	}

	public void clickLeftActionsMenu_Click_ChangeOwner(WebDriver driver) throws Exception {
		selectActionsFromLeftActionMenu("Change Owner");
		fc.commonMethods().switch_cboxIframe_frameId(driver);
	}

	private void checkGroupNameOnGroupWindow(Group group) throws Exception {
		LeadManagementUI ui = new LeadManagementUI(driver);
		fc.utobj().printTestStep("Check the Group Name");
		fc.utobj().clickElement(driver, ui.getXpathOfCheckBoxOfGroup(group.getName()));
	}

	private void clickAddToGroupButtonOnGroupWindow(WebDriver driver) throws Exception {
		LeadManagementUI ui = new LeadManagementUI(driver);
		fc.utobj().printTestStep("Click Add To Group Button");
		fc.utobj().clickElement(driver, ui.addToGroup_Button);
		fc.utobj().printTestStep("Click OK on confirmation");
		fc.utobj().clickElement(driver, ui.ok_Input_ByValue);
		fc.utobj().switchFrameToDefault(driver);
	}

	public void checkGroupName_clickAddToGroupButtonOnGroupWindow(Group group) throws Exception {
		checkGroupNameOnGroupWindow(group);
		clickAddToGroupButtonOnGroupWindow(driver);
	}

	private void selectActionsFromLeftActionMenu(String actionName) throws Exception {
		LeadManagementUI ui = new LeadManagementUI(driver);

		clickLeftAction(driver);

		fc.utobj().printTestStep("Select : " + actionName + ", from Left Action Menu");
		List<WebElement> actionMenu = ui.actionsIn_LeftActionMenu_ByXpath;

		for (WebElement menu : actionMenu) {
			if (actionName.equalsIgnoreCase(menu.getText().trim())) {
				fc.utobj().clickElement(driver, menu);
				break;
			}
		}
	}

	public void clickChangeStatus_BottomButton(WebDriver driver) throws Exception {
		LeadManagementUI ui = new LeadManagementUI(driver);

		fc.utobj().printTestStep("Click Change Status");
		fc.utobj().clickElement(driver, ui.changeStatus);
		fc.commonMethods().switch_cboxIframe_frameId(driver);
	}

	public void clickChangeOwner_BottomButton(WebDriver driver) throws Exception {
		LeadManagementUI ui = new LeadManagementUI(driver);

		fc.utobj().printTestStep("Click Change Owner");
		fc.utobj().clickElement(driver, ui.changeOwner);
	}

	public void clickSendEmail_BottomButton(WebDriver driver) throws Exception {
		LeadManagementUI ui = new LeadManagementUI(driver);

		fc.utobj().printTestStep("Click Send Email");
		fc.utobj().clickElement(driver, ui.sendMail);
	}

	public void clickAddToGroup_BottomButton(WebDriver driver) throws Exception {
		LeadManagementUI ui = new LeadManagementUI(driver);

		fc.utobj().printTestStep("Click Add to Group");
		fc.utobj().clickElement(driver, ui.addToGroup);
		fc.commonMethods().switch_cboxIframe_frameId(driver);
	}

	public void clickAssociateWithCampaign_BottomButton(WebDriver driver) throws Exception {
		LeadManagementUI ui = new LeadManagementUI(driver);

		fc.utobj().printTestStep("Click Associate With Campaign");
		fc.utobj().clickElement(driver, ui.associateCampaign);
	}

	public void clickMailMerge_BottomButton(WebDriver driver) throws Exception {
		LeadManagementUI ui = new LeadManagementUI(driver);

		fc.utobj().printTestStep("Click Mail Merge");
		fc.utobj().clickElement(driver, ui.mailMerge);
	}

	public void clickMergeLeads_BottomButton(WebDriver driver) throws Exception {
		LeadManagementUI ui = new LeadManagementUI(driver);

		fc.utobj().printTestStep("Click Merge Leads");
		fc.utobj().clickElement(driver, ui.mergeLeads);
	}

	public void clickArchiveLeads_BottomButton(WebDriver driver) throws Exception {
		LeadManagementUI ui = new LeadManagementUI(driver);

		fc.utobj().printTestStep("Click Archive Leads");
		fc.utobj().clickElement(driver, ui.archiveLeads);
	}

	public void clickDelete_BottomButton_ClickOK(WebDriver driver) throws Exception {
		LeadManagementUI ui = new LeadManagementUI(driver);
		fc.utobj().printTestStep("Click Delete");
		fc.utobj().clickElement(driver, ui.delete);
		fc.commonMethods().switch_cboxIframe_frameId(driver);
		fc.commonMethods().Click_OK_Input_ByValue(driver);
	}

	public void clickPrint_BottomButton(WebDriver driver) throws Exception {
		LeadManagementUI ui = new LeadManagementUI(driver);

		fc.utobj().printTestStep("Click Print");
		fc.utobj().clickElement(driver, ui.Print);
	}
}
