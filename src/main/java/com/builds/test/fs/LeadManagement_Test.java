package com.builds.test.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.builds.uimaps.fs.LeadSummaryUI;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class LeadManagement_Test {

	@Test(groups = { "sales", "sales_restructured", "sales_failed" , "Sales_LeadManagement_SendEmail_001" }) // working fine now
	@TestCase(createdOn = "2018-02-01", updatedOn = "2018-02-01", testCaseId = "Sales_LeadManagement_SendEmail_001", testCaseDescription = "Verify Send Email (Left Action Menu) in batch functionality \nfrom lead summary page")
	private void Sales_Lead_LeadManagement_SendEmail_001() throws Exception {

		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String basicFirstName = fc.utobj().generateRandomChar().toLowerCase();
		String basicLastName = fc.utobj().generateRandomChar().toLowerCase();

		LeadManagementTest lm = new LeadManagementTest(driver);
		Lead ld = new Lead();
		Lead ld2 = new Lead();
		Sales sales = new Sales();
		SearchTest searchTest = new SearchTest();
		Search search = new Search();
		PrimaryInfo pinfo = new PrimaryInfo();
		LeadTest lt = new LeadTest();
		Email email = new Email();
		Sales_Common common = new Sales_Common();

		try {
			driver = fc.loginpage().login(driver);
			ld = fc.sales().sales_common().fillDefaultValue_LeadDetails(ld2);
			ld.setEmail("salesautomation@staffex.com");
			ld.setFirstName(ld.getFirstName() + basicFirstName);
			ld.setLastName(ld.getLastName() + basicLastName);

			fc.sales().sales_common().fsModule(driver);
			sales.leadManagement(driver);

			lm.clickAddLead(driver);
			lt.addLeadAndClickSave(driver, ld);

			sales.search(driver);
			search.setFirstName(basicFirstName);
			search.setLastName(basicLastName);
			searchTest.fill_And_Search_PrimaryInfoBasedSearch(driver, search);

			lm.clickCheckBox_All(driver);
			lm.clickLeftActionsMenu_Click_SendEmail();

			// Send Email method is due
			email.setSubject("Email Subject");
			email.setCC("cc@abc.com");
			email.setBody("Email Body");
			SendEmailTest emailTest = new SendEmailTest();
			emailTest.fillEmailDetails_AndClickSendEmail_Bottom_Button(driver, email);
			
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[contains(text(),'"+ld.getLeadFullName()+"')]/parent::*[contains(@onmouseover,'showLeadDetails')] | .//*[contains(text(),'"+ld.getLeadFullName()+"')][contains(@onmouseover,'showLeadDetails')]"));
			// fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, ld.getLeadFullName()));

			pinfo.verify_TextUnder_ActivityHistory(driver, email.getSubject());
			pinfo.verify_RemarksAndCallHistory_In_DetailedHistory(driver, email.getSubject());

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales", "sales_restructured"  , "sales_failed" , "Sales_LeadManagement_ArchiveInBatch_001" }) // fixed : verified // Akshat
	@TestCase(createdOn = "2018-02-01", updatedOn = "2018-02-01", testCaseId = "Sales_LeadManagement_ArchiveInBatch_001", testCaseDescription = "Verify Lead Archive (Left Action Menu) in batch functionality \nfrom lead summary page")
	private void Sales_Lead_LeadManagement_ArchiveInBatch_001() throws Exception {

		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String basicFirstName = fc.utobj().generateRandomChar().toLowerCase();
		String basicLastName = fc.utobj().generateRandomChar().toLowerCase();

		LeadManagementTest lm = new LeadManagementTest(driver);
		LeadTest lt = new LeadTest();
		Lead ld = new Lead();
		Lead ld2 = new Lead();
		Sales sales = new Sales();
		SearchTest searchTest = new SearchTest();
		Search search = new Search();
		PrimaryInfo pinfo = new PrimaryInfo();

		try {
			driver = fc.loginpage().login(driver);
			ld = fc.sales().sales_common().fillDefaultValue_LeadDetails(ld);
			ld2 = fc.sales().sales_common().fillDefaultValue_LeadDetails(ld2);

			ld.setFirstName(ld.getFirstName() + basicFirstName);
			ld.setLastName(ld.getLastName() + basicLastName);

			ld2.setFirstName(ld2.getFirstName() + basicFirstName);
			ld2.setLastName(ld2.getLastName() + basicLastName);

			lt.addLeadThroughWebServices(driver, ld);
			lt.addLeadThroughWebServices(driver, ld2);

			fc.sales().sales_common().fsModule(driver);
			sales.search(driver);
			search.setFirstName(basicFirstName);
			search.setLastName(basicLastName);
			searchTest.fill_And_Search_PrimaryInfoBasedSearch(driver, search);

			lm.clickCheckBox_All(driver);
			lm.clickLeftActionsMenu_Click_Archive(fc.utobj().generateTestData("Archive Notes"));

			sales.search(driver);
			search.setFirstName(basicFirstName);
			search.setLastName(basicLastName);
			searchTest.fill_And_Search_PrimaryInfoBasedSearch_ArchivedLeads(driver, search);

			if (fc.utobj().assertPageSource(driver, ld.getLeadFullName()) == false) {
				fc.utobj().throwsException("Lead is not available in Archive leads section");
			}
			if (fc.utobj().assertPageSource(driver, ld2.getLeadFullName()) == false) {
				fc.utobj().throwsException("Lead is not available in Archive leads section");
			}

			lm.click_Lead_OnLeadManagementPage(ld);
			pinfo.verify_TextUnder_ActivityHistory(driver, "Lead Archived");
			pinfo.clickNextOrPrev(driver);
			pinfo.verify_TextUnder_ActivityHistory(driver, "Lead Archived");

			sales.search(driver);
			search.setFirstName(basicFirstName);
			search.setLastName(basicLastName);
			searchTest.fill_And_Search_PrimaryInfoBasedSearch(driver, search);

			if (fc.utobj().assertPageSource(driver, ld.getLeadFullName()) == true) {
				fc.utobj().throwsException("Lead still available in Active Lead Section");
			}
			if (fc.utobj().assertPageSource(driver, ld2.getLeadFullName()) == true) {
				fc.utobj().throwsException("Lead still available in Active Lead Section");
			}

			// TODO : Restore Leads code to be done here

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			e.printStackTrace();
			//fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales", "sales_restructured" })
	@TestCase(createdOn = "2018-02-01", updatedOn = "2018-02-01", testCaseId = "Sales_Lead_LeadManagement_DeleteInBatch_001", testCaseDescription = "Verify Deletion of Lead (Left Action Menu) in batch functionality \nfrom lead summary page")
	private void Sales_Lead_LeadManagement_DeleteInBatch_001() throws Exception {

		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String basicFirstName = fc.utobj().generateRandomChar().toLowerCase();
		String basicLastName = fc.utobj().generateRandomChar().toLowerCase();
		LeadTest lt = new LeadTest();
		Lead ld = new Lead();
		Lead ld2 = new Lead();
		Sales sales = new Sales();
		Call call = new Call();
		SearchTest searchTest = new SearchTest();
		Search search = new Search();
		LeadManagementTest lm = new LeadManagementTest(driver);

		try {
			driver = fc.loginpage().login(driver);
			ld = fc.sales().sales_common().fillDefaultValue_LeadDetails(ld);
			ld2 = fc.sales().sales_common().fillDefaultValue_LeadDetails(ld2);

			ld.setFirstName(ld.getFirstName() + basicFirstName);
			ld.setLastName(ld.getLastName() + basicLastName);

			ld2.setFirstName(ld2.getFirstName() + basicFirstName);
			ld2.setLastName(ld2.getLastName() + basicLastName);

			lt.addLeadThroughWebServices(driver, ld);
			lt.addLeadThroughWebServices(driver, ld2);

			fc.sales().sales_common().fsModule(driver);
			sales.search(driver);
			search.setFirstName(basicFirstName);
			search.setLastName(basicLastName);
			searchTest.fill_And_Search_PrimaryInfoBasedSearch(driver, search);

			call = fc.sales().sales_common().fillDefaultValue_CallDetails(call);

			lm.clickCheckBox_All(driver);
			lm.clickLeftActionsMenu_Click_Delete_ClickOK(driver);

			sales.search(driver);
			searchTest.fill_And_Search_PrimaryInfoBasedSearch(driver, search);

			if (fc.utobj().assertPageSource(driver, ld.getLeadFullName()) == true) {
				fc.utobj().throwsException("Lead available even after deletion.");
			}
			if (fc.utobj().assertPageSource(driver, ld2.getLeadFullName()) == true) {
				fc.utobj().throwsException("Lead available even after deletion.");
			}

			// TODO : Verify the deleted lead in deleted logs

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales", "Sales_Lead_LeadManagement_DeleteInBatch_002" })
	@TestCase(createdOn = "2018-02-06", updatedOn = "2018-02-06", testCaseId = "Sales_Lead_LeadManagement_DeleteInBatch_002", testCaseDescription = "Verify Deletion of Lead (Bottom button) in batch functionality \nfrom lead summary page")
	private void Sales_Lead_LeadManagement_DeleteInBatch_002() throws Exception {

		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String basicFirstName = fc.utobj().generateRandomChar().toLowerCase();
		String basicLastName = fc.utobj().generateRandomChar().toLowerCase();

		LeadManagementTest lm = new LeadManagementTest(driver);
		Lead ld = new Lead();
		Lead ld2 = new Lead();
		LeadTest lt = new LeadTest();
		try {
			driver = fc.loginpage().login(driver);
			ld = fc.sales().sales_common().fillDefaultValue_LeadDetails(ld);
			ld2 = fc.sales().sales_common().fillDefaultValue_LeadDetails(ld2);

			ld.setFirstName(ld.getFirstName() + basicFirstName);
			ld.setLastName(ld.getLastName() + basicLastName);

			ld2.setFirstName(ld.getFirstName() + basicFirstName);
			ld2.setLastName(ld2.getLastName() + basicLastName);

			fc.sales().sales_common().fsModule(driver);
			Sales sales = new Sales();
			sales.leadManagement(driver);

			lt.addLeadThroughWebServices(driver, ld);
			lt.addLeadThroughWebServices(driver, ld2);

			fc.sales().sales_common().fsModule(driver);

			fc.sales().sales_common().topSearch_EnterText_clickTopSearchImage(driver, ld.getFirstName());

			lm.clickCheckBox_All(driver);
			lm.clickDelete_BottomButton_ClickOK(driver);

			fc.sales().sales_common().topSearch_EnterText_clickTopSearchImage(driver, ld.getFirstName());

			if (fc.utobj().assertPageSource(driver, ld.getLeadFullName()) == true) {
				fc.utobj().throwsException("Lead available even after deletion.");
			}
			if (fc.utobj().assertPageSource(driver, ld2.getLeadFullName()) == true) {
				fc.utobj().throwsException("Lead available even after deletion.");
			}

			// TODO : Verify the deleted lead in deleted logs

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales", "sales_restructured" , "sales_failed" , "Sales_LeadManagement_LogACallInBatch_001" }) //  Fixed : Verified // Akshat 
	@TestCase(createdOn = "2018-01-31", updatedOn = "2018-01-31", testCaseId = "Sales_LeadManagement_LogACallInBatch_001", testCaseDescription = "Verify log a call in batch functionality from lead summary page.")
	private void Sales_Lead_LeadManagement_LogACallInBatch_001() throws Exception {

		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String basicFirstName = fc.utobj().generateRandomChar().toLowerCase();
		String basicLastName = fc.utobj().generateRandomChar().toLowerCase();

		LeadTest lt = new LeadTest();
		Lead ld = new Lead();
		Lead ld2 = new Lead();
		Sales sales = new Sales();
		Call call = new Call();
		SearchTest searchTest = new SearchTest();
		PrimaryInfo pinfo = new PrimaryInfo();
		Search search = new Search();
		CallTest callTest = new CallTest();
		LeadManagementTest lm = new LeadManagementTest(driver);

		try {
			driver = fc.loginpage().login(driver);
			ld = fc.sales().sales_common().fillDefaultValue_LeadDetails(ld);
			ld2 = fc.sales().sales_common().fillDefaultValue_LeadDetails(ld2);

			ld.setFirstName(ld.getFirstName() + basicFirstName);
			ld.setLastName(ld.getLastName() + basicLastName);

			ld2.setFirstName(ld2.getFirstName() + basicFirstName);
			ld2.setLastName(ld2.getLastName() + basicLastName);

			lt.addLeadThroughWebServices(driver, ld);
			lt.addLeadThroughWebServices(driver, ld2);

			fc.sales().sales_common().fsModule(driver);
			sales.search(driver);
			search.setFirstName(basicFirstName);
			search.setLastName(basicLastName);
			searchTest.fill_And_Search_PrimaryInfoBasedSearch(driver, search);

			call = fc.sales().sales_common().fillDefaultValue_CallDetails(call);

			lm.clickCheckBox_All(driver);
			lm.clickLeftActionsMenu_Click_logACall();
			callTest.fill_And_Add_Call_Details_ScheduleTask_No(driver, call);

			lm.click_Lead_OnLeadManagementPage(ld);
			pinfo.verifyCallInfo_ActivityHistory(driver, call);
			pinfo.clickNextOrPrev(driver);
			pinfo.verifyCallInfo_ActivityHistory(driver, call);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales", "sales_restructured", "sales_failed" , "Sales_LeadManagement_LogATaskInBatch_001" }) // Verified : Akshat 
	@TestCase(createdOn = "2018-01-31", updatedOn = "2018-01-31", testCaseId = "Sales_LeadManagement_LogATaskInBatch_001", testCaseDescription = "Verify log a Task in batch functionality from lead summary page.")
	private void Sales_Lead_LeadManagement_LogATaskInBatch_001() throws Exception {

		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String basicFirstName = fc.utobj().generateRandomChar().toLowerCase();
		String basicLastName = fc.utobj().generateRandomChar().toLowerCase();

		LeadTest lt = new LeadTest();
		Lead ld = new Lead();
		Lead ld2 = new Lead();
		Sales sales = new Sales();
		Task task = new Task();
		SearchTest searchTest = new SearchTest();
		PrimaryInfo pinfo = new PrimaryInfo();
		Search search = new Search();
		TaskTest taskTest = new TaskTest();
		LeadManagementTest lm = new LeadManagementTest(driver);

		try {
			driver = fc.loginpage().login(driver);
			ld = fc.sales().sales_common().fillDefaultValue_LeadDetails(ld);
			ld2 = fc.sales().sales_common().fillDefaultValue_LeadDetails(ld2);

			ld.setFirstName(ld.getFirstName() + basicFirstName);
			ld.setLastName(ld.getLastName() + basicLastName);

			ld2.setFirstName(ld2.getFirstName() + basicFirstName);
			ld2.setLastName(ld2.getLastName() + basicLastName);

			fc.sales().sales_common().fsModule(driver);

			lt.addLeadThroughWebServices(driver, ld);
			lt.addLeadThroughWebServices(driver, ld2);

			fc.sales().sales_common().fsModule(driver);
			sales.search(driver);
			search.setFirstName(basicFirstName);
			search.setLastName(basicLastName);
			searchTest.fill_And_Search_PrimaryInfoBasedSearch(driver, search);

			task = fc.sales().sales_common().fillDefaultValue_TaskDetails(task);

			lm.clickCheckBox_All(driver);
			lm.clickLeftActionsMenu_Click_logATask(driver);
			taskTest.fillTaskAndClickCreate(driver, task);

			lm.click_Lead_OnLeadManagementPage(ld);
			pinfo.verifyTaskInfo_ActivityHistory(driver, task);
			pinfo.clickNextOrPrev(driver);
			pinfo.verifyTaskInfo_ActivityHistory(driver, task);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales", "sales_restructured","sales_failed" , "Sales_LeadManagement_AddToGroupInBatch_001" }) // Fixed : Verified // Akshat
	@TestCase(createdOn = "2018-02-01", updatedOn = "2018-02-01", testCaseId = "Sales_LeadManagement_AddToGroupInBatch_001", testCaseDescription = "Verify Add To Group(Left Actions) in batch functionality from lead summary page.")
	private void Sales_Lead_LeadManagement_AddToGroupInBatch_001() throws Exception {

		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String basicFirstName = fc.utobj().generateRandomChar().toLowerCase();
		String basicLastName = fc.utobj().generateRandomChar().toLowerCase();

		LeadTest lt = new LeadTest();
		Lead ld = new Lead();
		Lead ld2 = new Lead();
		Sales sales = new Sales();
		SearchTest searchTest = new SearchTest();
		Search search = new Search();
		PrimaryInfo pinfo = new PrimaryInfo();
		GroupTest groupTest = new GroupTest();
		Group group = new Group();
		LeadManagementTest lm = new LeadManagementTest(driver);

		try {
			driver = fc.loginpage().login(driver);

			fc.commonMethods().getModules().clickSalesModule(driver);
			sales.groups(driver);
			fc.sales().sales_common().fillDefaultValue_RegularGroupDetails(group);
			groupTest.addGroupDetails_AndCreate(driver, group);

			ld = fc.sales().sales_common().fillDefaultValue_LeadDetails(ld);
			ld2 = fc.sales().sales_common().fillDefaultValue_LeadDetails(ld2);

			ld.setFirstName(ld.getFirstName() + basicFirstName);
			ld.setLastName(ld.getLastName() + basicLastName);

			ld2.setFirstName(ld2.getFirstName() + basicFirstName);
			ld2.setLastName(ld2.getLastName() + basicLastName);

			lt.addLeadThroughWebServices(driver, ld);
			lt.addLeadThroughWebServices(driver, ld2);

			fc.sales().sales_common().fsModule(driver);
			sales.search(driver);
			search.setFirstName(basicFirstName);
			search.setLastName(basicLastName);
			searchTest.fill_And_Search_PrimaryInfoBasedSearch(driver, search);

			lm.clickCheckBox_All(driver);
			lm.clickLeftActionsMenu_Click_Add_To_Group_SwitchToiFrame(driver);
			lm.checkGroupName_clickAddToGroupButtonOnGroupWindow(group);

			lm.click_Lead_OnLeadManagementPage(ld);

			if (!fc.utobj().assertPageSource(driver, group.getName())) {
				fc.utobj().throwsException("Group name is not associated with the lead.");
			}
			pinfo.clickNextOrPrev(driver);

			if (!fc.utobj().assertPageSource(driver, group.getName())) {
				fc.utobj().throwsException("Group name is not associated with the lead.");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales", "Sales_LeadManagement_LeadAddtoGroupInBatch_001" }) // Fixed : Verified // Akshat
	@TestCase(createdOn = "2018-02-01", updatedOn = "2018-02-20", testCaseId = "Sales_LeadManagement_LeadAddtoGroupInBatch_001", testCaseDescription = "Verify Leads Add to Group in batch.")
	private void Sales_LeadManagement_LeadAddtoGroupInBatch_001() throws Exception {

		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String basicFirstName = fc.utobj().generateRandomChar().toLowerCase();
		String basicLastName = fc.utobj().generateRandomChar().toLowerCase();

		LeadTest lt = new LeadTest();
		Lead ld = new Lead();
		Lead ld2 = new Lead();
		Sales sales = new Sales();
		SearchTest searchTest = new SearchTest();
		Search search = new Search();
		PrimaryInfo pinfo = new PrimaryInfo();
		GroupTest groupTest = new GroupTest();
		Group group = new Group();
		LeadManagementTest lm = new LeadManagementTest(driver);

		try {
			driver = fc.loginpage().login(driver);
			
			fc.commonMethods().getModules().clickSalesModule(driver);
			sales.groups(driver);
			fc.sales().sales_common().fillDefaultValue_RegularGroupDetails(group);
			groupTest.addGroupDetails_AndCreate(driver, group);

			ld = fc.sales().sales_common().fillDefaultValue_LeadDetails(ld);
			ld2 = fc.sales().sales_common().fillDefaultValue_LeadDetails(ld2);

			ld.setFirstName(ld.getFirstName() + basicFirstName);
			ld.setLastName(ld.getLastName() + basicLastName);

			ld2.setFirstName(ld2.getFirstName() + basicFirstName);
			ld2.setLastName(ld2.getLastName() + basicLastName);

			lt.addLeadThroughWebServices(driver, ld);
			lt.addLeadThroughWebServices(driver, ld2);

			fc.sales().sales_common().fsModule(driver);
			sales.search(driver);
			search.setFirstName(basicFirstName);
			search.setLastName(basicLastName);
			searchTest.fill_And_Search_PrimaryInfoBasedSearch(driver, search);

			lm.clickCheckBox_All(driver);
			lm.clickLeftActionsMenu_Click_Add_To_Group_SwitchToiFrame(driver);
			WebElement checkBoxAgainstGroupName = fc.utobj().getElementByXpath(driver, ".//td[contains(text(),'" +group.getName()+ "')]/preceding-sibling::td/input[@type='checkbox']");
			fc.utobj().clickElement(driver, checkBoxAgainstGroupName);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//input[@type='Submit' and @value='Add To Group']"));
			fc.utobj().sleep();
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//input[@type='button' and @value='Ok']"));
			
			fc.utobj().switchFrameToDefault(driver);
			
			lm.click_Lead_OnLeadManagementPage(ld);

			if (!fc.utobj().assertPageSource(driver, group.getName())) {
				fc.utobj().throwsException("Group name is not associated with the lead.");
			}
			pinfo.clickNextOrPrev(driver);

			if (!fc.utobj().assertPageSource(driver, group.getName())) {
				fc.utobj().throwsException("Group name is not associated with the lead.");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales", "Sales_LeadManagement_AddToGroupInBatch_002" }) // Fixed : Verified // Akshat
	@TestCase(createdOn = "2018-02-01", updatedOn = "2018-02-01", testCaseId = "Sales_LeadManagement_AddToGroupInBatch_002", testCaseDescription = "Verify Add To Group(Bottom button) in batch functionality from lead summary page.")
	private void Sales_LeadManagement_AddToGroupInBatch_002() throws Exception {

		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String basicFirstName = fc.utobj().generateRandomChar().toLowerCase();
		String basicLastName = fc.utobj().generateRandomChar().toLowerCase();

		LeadTest lt = new LeadTest();
		Lead ld = new Lead();
		Lead ld2 = new Lead();
		Sales sales = new Sales();
		SearchTest searchTest = new SearchTest();
		Search search = new Search();
		PrimaryInfo pinfo = new PrimaryInfo();
		GroupTest groupTest = new GroupTest();
		Group group = new Group();
		LeadManagementTest lm = new LeadManagementTest(driver);

		try {
			driver = fc.loginpage().login(driver);

			fc.commonMethods().getModules().clickSalesModule(driver);
			sales.groups(driver);
			fc.sales().sales_common().fillDefaultValue_RegularGroupDetails(group);
			groupTest.addGroupDetails_AndCreate(driver, group);

			ld = fc.sales().sales_common().fillDefaultValue_LeadDetails(ld);
			ld2 = fc.sales().sales_common().fillDefaultValue_LeadDetails(ld2);

			ld.setFirstName(ld.getFirstName() + basicFirstName);
			ld.setLastName(ld.getLastName() + basicLastName);

			ld2.setFirstName(ld2.getFirstName() + basicFirstName);
			ld2.setLastName(ld2.getLastName() + basicLastName);

			fc.sales().sales_common().fsModule(driver);

			lt.addLeadThroughWebServices(driver, ld);
			lt.addLeadThroughWebServices(driver, ld2);

			fc.sales().sales_common().fsModule(driver);
			sales.search(driver);
			search.setFirstName(basicFirstName);
			search.setLastName(basicLastName);
			searchTest.fill_And_Search_PrimaryInfoBasedSearch(driver, search);

			lm.clickCheckBox_All(driver);
			lm.clickAddToGroup_BottomButton(driver);
			lm.checkGroupName_clickAddToGroupButtonOnGroupWindow(group);

			lm.click_Lead_OnLeadManagementPage(ld);

			if (!fc.utobj().assertPageSource(driver, group.getName())) {
				fc.utobj().throwsException("Group name is not associated with the lead.");
			}
			pinfo.clickNextOrPrev(driver);

			if (!fc.utobj().assertPageSource(driver, group.getName())) {
				fc.utobj().throwsException("Group name is not associated with the lead.");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales", "sales_restructured" , "sales_failed" , "Sales_LeadManagement_ChangeStatusInBatch_001" }) // Fixed : Verified // Akshat 
	@TestCase(createdOn = "2018-02-07", updatedOn = "2018-02-07", testCaseId = "Sales_LeadManagement_ChangeStatusInBatch_001", testCaseDescription = "Verify Change Status(Left Actions) in batch functionality from lead summary page.")
	private void Sales_Lead_LeadManagement_ChangeStatusInBatch_001() throws Exception {

		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String basicFirstName = fc.utobj().generateRandomChar().toLowerCase();
		String basicLastName = fc.utobj().generateRandomChar().toLowerCase();

		LeadManagementTest lm = new LeadManagementTest(driver);
		Lead ld = new Lead();
		Lead ld2 = new Lead();
		LeadTest lt = new LeadTest();
		Sales sales = new Sales();
		SearchTest searchTest = new SearchTest();
		Search search = new Search();
		PrimaryInfo pinfo = new PrimaryInfo();
		Status status = new Status();
		StatusTest statusTest = new StatusTest();
		String statusToBeChanged = "Closed Lead";

		try {
			driver = fc.loginpage().login(driver);

			ld = fc.sales().sales_common().fillDefaultValue_LeadDetails(ld);
			ld2 = fc.sales().sales_common().fillDefaultValue_LeadDetails(ld2);

			ld.setFirstName(ld.getFirstName() + basicFirstName);
			ld.setLastName(ld.getLastName() + basicLastName);

			ld2.setFirstName(ld2.getFirstName() + basicFirstName);
			ld2.setLastName(ld2.getLastName() + basicLastName);

			lt.addLeadThroughWebServices(driver, ld);
			lt.addLeadThroughWebServices(driver, ld2);

			fc.sales().sales_common().fsModule(driver);
			sales.leadManagement(driver);
			sales.search(driver);
			search.setFirstName(basicFirstName);
			search.setLastName(basicLastName);
			searchTest.fill_And_Search_PrimaryInfoBasedSearch(driver, search);

			status = fc.sales().sales_common().fillDefaultValue_LeadStatus(driver, status, statusToBeChanged);

			lm.clickCheckBox_All(driver);
			lm.clickLeftActionsMenu_Click_ChangeStatus();
			statusTest.changeStatus_EnterRemarks_clickChange_Button_ClickClose(driver, ld, status);

			lm.click_Lead_OnLeadManagementPage(ld);
			if (statusToBeChanged.equalsIgnoreCase(pinfo.getCurrentLeadStatusFromDropDown(driver)) == false) {
				fc.utobj().throwsException("Lead Status not changed");
			}
			pinfo.verify_TextUnder_ActivityHistory(driver, status.getRemarks());
			pinfo.verify_RemarksAndCallHistory_In_DetailedHistory(driver, status.getRemarks());
			pinfo.verify_StatusChangeHistory_In_DetailedHistory(driver, status);

			pinfo.clickNextOrPrev(driver);

			if (statusToBeChanged.equalsIgnoreCase(pinfo.getCurrentLeadStatusFromDropDown(driver)) == false) {
				fc.utobj().throwsException("Lead Status not changed");
			}
			pinfo.verify_TextUnder_ActivityHistory(driver, status.getRemarks());
			pinfo.verify_RemarksAndCallHistory_In_DetailedHistory(driver, status.getRemarks());
			pinfo.verify_StatusChangeHistory_In_DetailedHistory(driver, status);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales", "Sales_LeadManagement_ChangeStatusInBatch_002" }) // Fixed : Verified // Akshat
	@TestCase(createdOn = "2018-02-07", updatedOn = "2018-02-07", testCaseId = "Sales_LeadManagement_ChangeStatusInBatch_002", testCaseDescription = "Verify Change Status(Bottom button) in batch functionality from lead summary page.")
	private void Sales_Lead_LeadManagement_ChangeStatusInBatch_002() throws Exception {

		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String basicFirstName = fc.utobj().generateRandomChar().toLowerCase();
		String basicLastName = fc.utobj().generateRandomChar().toLowerCase();

		LeadTest lt = new LeadTest();
		Lead ld = new Lead();
		Lead ld2 = new Lead();
		Sales sales = new Sales();
		SearchTest searchTest = new SearchTest();
		Search search = new Search();
		PrimaryInfo pinfo = new PrimaryInfo();
		Status status = new Status();
		StatusTest statusTest = new StatusTest();
		String statusToBeChanged = "Closed Lead";
		LeadManagementTest lm = new LeadManagementTest(driver);

		try {
			driver = fc.loginpage().login(driver);

			ld = fc.sales().sales_common().fillDefaultValue_LeadDetails(ld);
			ld2 = fc.sales().sales_common().fillDefaultValue_LeadDetails(ld2);

			ld.setFirstName(ld.getFirstName() + basicFirstName);
			ld.setLastName(ld.getLastName() + basicLastName);

			ld2.setFirstName(ld2.getFirstName() + basicFirstName);
			ld2.setLastName(ld2.getLastName() + basicLastName);

			lt.addLeadThroughWebServices(driver, ld);
			lt.addLeadThroughWebServices(driver, ld2);

			fc.sales().sales_common().fsModule(driver);
			sales.search(driver);
			search.setFirstName(basicFirstName);
			search.setLastName(basicLastName);
			searchTest.fill_And_Search_PrimaryInfoBasedSearch(driver, search);

			status = fc.sales().sales_common().fillDefaultValue_LeadStatus(driver, status, statusToBeChanged);

			lm.clickCheckBox_All(driver);
			lm.clickChangeStatus_BottomButton(driver);
			statusTest.changeStatus_EnterRemarks_clickChange_Button_ClickClose(driver, ld, status);
			
			fc.utobj().switchFrameToDefault(driver);
			
			lm.click_Lead_OnLeadManagementPage(ld);
			if (statusToBeChanged.equalsIgnoreCase(pinfo.getCurrentLeadStatusFromDropDown(driver)) == false) {
				fc.utobj().throwsException("Lead Status not changed");
			}
			pinfo.verify_TextUnder_ActivityHistory(driver, status.getRemarks());
			pinfo.verify_RemarksAndCallHistory_In_DetailedHistory(driver, status.getRemarks());
			pinfo.verify_StatusChangeHistory_In_DetailedHistory(driver, status);

			pinfo.clickNextOrPrev(driver);

			if (statusToBeChanged.equalsIgnoreCase(pinfo.getCurrentLeadStatusFromDropDown(driver)) == false) {
				fc.utobj().throwsException("Lead Status not changed");
			}
			pinfo.verify_TextUnder_ActivityHistory(driver, status.getRemarks());
			pinfo.verify_RemarksAndCallHistory_In_DetailedHistory(driver, status.getRemarks());
			pinfo.verify_StatusChangeHistory_In_DetailedHistory(driver, status);

			sales.search(driver);
			search.setFirstName(ld.getFirstName());
			search.setLastName(ld.getLastName());
			searchTest.fill_And_Search_PrimaryInfoBasedSearch(driver, search);
			lm.clickRightAction_AndClick_DetailedHistory(ld.getLeadFullName());
			pinfo.verifyTextInDetailedHistoryWindow(driver, status.getRemarks());

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales", "sales_restructured" , "sales_failed" ,  "Sales_LeadManagement_LogATask_FromAction_001" }) // Verified : Akshat
	@TestCase(createdOn = "2018-01-31", updatedOn = "2018-01-31", testCaseId = "Sales_LeadManagement_LogATask_FromAction_001", testCaseDescription = "Verify log a Task from Lead summary page - Right Action")
	private void Sales_Lead_LeadManagement_LogATask_FromAction_001() throws Exception {

		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		LeadTest lt = new LeadTest();
		Lead ld = new Lead();
		Sales sales = new Sales();
		Task task = new Task();
		SearchTest searchTest = new SearchTest();
		PrimaryInfo pinfo = new PrimaryInfo();
		Search search = new Search();
		TaskTest taskTest = new TaskTest();
		LeadManagementTest lm = new LeadManagementTest(driver);

		try {
			driver = fc.loginpage().login(driver);
			ld = fc.sales().sales_common().fillDefaultValue_LeadDetails(ld);

			lt.addLeadThroughWebServices(driver, ld);

			fc.sales().sales_common().fsModule(driver);
			sales.leadManagement(driver);
			sales.search(driver);
			search.setFirstName(ld.getFirstName());
			search.setLastName(ld.getLastName());
			searchTest.fill_And_Search_PrimaryInfoBasedSearch(driver, search);

			task = fc.sales().sales_common().fillDefaultValue_TaskDetails(task);

			lm.clickRightAction_AndClick_LogATask(ld.getLeadFullName());
			taskTest.fillTaskAndClickCreate(driver, task);

			lm.click_Lead_OnLeadManagementPage(ld);
			pinfo.verifyTaskInfo_ActivityHistory(driver, task);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales", "sales_restructured" , "Sales_LeadManagement_LogACall_FromAction_001" }) // Verified // Akshat
	@TestCase(createdOn = "2018-01-31", updatedOn = "2018-01-31", testCaseId = "Sales_LeadManagement_LogACall_FromAction_001", testCaseDescription = "Verify log a call from Lead summary page - Right Action")
	private void Sales_Lead_LeadManagement_LogACall_FromAction_001() throws Exception {

		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		LeadTest lt = new LeadTest();
		Lead ld = new Lead();
		Sales sales = new Sales();
		Call call = new Call();
		SearchTest searchTest = new SearchTest();
		PrimaryInfo pinfo = new PrimaryInfo();
		Search search = new Search();
		CallTest callTest = new CallTest();
		LeadManagementTest lm = new LeadManagementTest(driver);

		try {
			driver = fc.loginpage().login(driver);
			ld = fc.sales().sales_common().fillDefaultValue_LeadDetails(ld);

			lt.addLeadThroughWebServices(driver, ld);

			fc.sales().sales_common().fsModule(driver);
			sales.leadManagement(driver);

			sales.search(driver);
			search.setFirstName(ld.getFirstName());
			search.setLastName(ld.getLastName());
			searchTest.fill_And_Search_PrimaryInfoBasedSearch(driver, search);

			call = fc.sales().sales_common().fillDefaultValue_CallDetails(call);

			lm.clickRightAction_AndClick_LogACall(ld.getLeadFullName());
			callTest.fill_And_Add_Call_Details_ScheduleTask_No(driver, call);

			lm.click_Lead_OnLeadManagementPage(ld);
			pinfo.verifyCallInfo_ActivityHistory(driver, call);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales", "sales_restructured","sales_failed" , "Sales_LeadManagement_AddRemarks_FromAction_001" }) // Fixed : Verified // Akshat
	@TestCase(createdOn = "2018-01-31", updatedOn = "2018-01-31", testCaseId = "Sales_LeadManagement_AddRemarks_FromAction_001", testCaseDescription = "Verify log a call from Lead summary page - Right Action")
	private void Sales_Lead_LeadManagement_AddRemarks_FromAction_001() throws Exception {

		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		LeadTest lt = new LeadTest();
		Lead ld = new Lead();
		Sales sales = new Sales();
		SearchTest searchTest = new SearchTest();
		PrimaryInfo pinfo = new PrimaryInfo();
		Search search = new Search();
		Remark remark = new Remark();
		RemarkTest remarkTest = new RemarkTest();
		LeadManagementTest lm = new LeadManagementTest(driver);

		try {
			driver = fc.loginpage().login(driver);
			ld = fc.sales().sales_common().fillDefaultValue_LeadDetails(ld);

			lt.addLeadThroughWebServices(driver, ld);

			fc.sales().sales_common().fsModule(driver);
			sales.leadManagement(driver);
			sales.search(driver);
			search.setFirstName(ld.getFirstName());
			search.setLastName(ld.getLastName());
			searchTest.fill_And_Search_PrimaryInfoBasedSearch(driver, search);

			remark = fc.sales().sales_common().fillDefaultValue_Remark(remark);

			lm.clickRightAction_AndClick_AddRemarks(ld.getLeadFullName());
			remarkTest.fill_Remarks_And_Submit(driver, remark);

			lm.click_Lead_OnLeadManagementPage(ld);
			pinfo.verify_TextUnder_ActivityHistory(driver, remark.getRemark());

			pinfo.verify_RemarksAndCallHistory_In_DetailedHistory(driver, remark.getRemark());

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales" })
	@TestCase(createdOn = "2018-02-05", updatedOn = "2018-02-05", testCaseId = "Sales_Lead_LeadManagement_DeleteLead_FromAction_001", testCaseDescription = "Verify lead deletion from Lead summary page - Right Action")
	private void Sales_Lead_LeadManagement_DeleteLead_FromAction_001() throws Exception {

		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		Lead ld = new Lead();
		Sales sales = new Sales();
		SearchTest searchTest = new SearchTest();
		Search search = new Search();
		LeadManagementTest lm = new LeadManagementTest(driver);
		LeadTest lt = new LeadTest();

		try {
			driver = fc.loginpage().login(driver);
			ld = fc.sales().sales_common().fillDefaultValue_LeadDetails(ld);

			lt.addLeadThroughWebServices(driver, ld);

			fc.sales().sales_common().fsModule(driver);
			sales.leadManagement(driver);
			sales.search(driver);
			search.setFirstName(ld.getFirstName());
			search.setLastName(ld.getLastName());
			searchTest.fill_And_Search_PrimaryInfoBasedSearch(driver, search);
			lm.clickRightAction_AndClick_Delete(ld.getLeadFullName());

			sales.search(driver);
			searchTest.fill_And_Search_PrimaryInfoBasedSearch(driver, search);

			if (fc.utobj().assertPageSource(driver, ld.getLeadFullName()) == true) {
				fc.utobj().throwsException("Lead still present after deletion (On lead summary page)");
			}

			// TODO : Check in deleted logs

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

}
