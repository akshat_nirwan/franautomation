package com.builds.test.fs;

public class LeadOwner {

	private String leadOwner;

	public String getLeadOwner() {
		return leadOwner;
	}

	public void setLeadOwner(String leadOwner) {
		this.leadOwner = leadOwner;
	}

}
