package com.builds.test.fs;

import org.openqa.selenium.WebDriver;

import com.builds.test.common.WebService;
import com.builds.uimaps.fs.LeadUI;
import com.builds.utilities.FranconnectUtil;

class LeadTest {

	Lead addLeadAndClickSave(WebDriver driver, Lead ld) throws Exception {
		LeadUI ui = new LeadUI(driver);
		FranconnectUtil fc = new FranconnectUtil();

		fillLeadInfo(driver, ld);

		fc.utobj().printTestStep("Click Save");
		fc.utobj().clickElement(driver, ui.Save);

		return ld;
	}

	Lead fillLeadInfo(WebDriver driver, Lead ld) throws Exception {

		LeadUI ui = new LeadUI(driver);
		FranconnectUtil fc = new FranconnectUtil();

		fc.utobj().printTestStep("Fill Lead Info");

		if (ld.getSelectLeadType() != null) {
			fc.utobj().selectDropDown(driver, ui.SelectLeadType, ld.getSelectLeadType());
		}

		if (ld.getExistingLead() != null) {
			fc.utobj().sendKeys(driver, ui.ExistingLead, ld.getExistingLead());
		}

		if (ld.getExistingOwner() != null) {
			fc.utobj().sendKeys(driver, ui.ExistingOwner, ld.getExistingOwner());
		}

		if (ld.getSalutation() != null) {
			fc.utobj().selectDropDownByValue(driver, ui.Salutation, ld.getSalutation());
		}

		if (ld.getFirstName() != null) {
			fc.utobj().sendKeys(driver, ui.firstName, ld.getFirstName());
		}

		if (ld.getLastName() != null) {
			fc.utobj().sendKeys(driver, ui.lastName, ld.getLastName());
		}

		if (ld.getAddress1() != null) {
			fc.utobj().sendKeys(driver, ui.address1, ld.getAddress1());
		}

		if (ld.getAddress2() != null) {
			fc.utobj().sendKeys(driver, ui.address2, ld.getAddress2());
		}

		if (ld.getCity() != null) {
			fc.utobj().sendKeys(driver, ui.city, ld.getCity());
		}

		if (ld.getCountry() != null) {
			fc.utobj().selectDropDown(driver, ui.country, ld.getCountry());
		}

		if (ld.getStateProvince() != null) {
			fc.utobj().selectDropDown(driver, ui.stateID, ld.getStateProvince());
		}

		if (ld.getZipPostalCode() != null) {
			fc.utobj().sendKeys(driver, ui.zip, ld.getZipPostalCode());
		}

		if (ld.getCounty() != null) {
			fc.utobj().selectDropDown(driver, ui.countyID, ld.getCounty());
		}

		if (ld.getPreferredModeofContact() != null) {
			fc.utobj().selectDropDown(driver, ui.PreferredModeofContact, ld.getPreferredModeofContact());
		}

		if (ld.getBestTimeToContact() != null) {
			fc.utobj().sendKeys(driver, ui.bestTimeToContact, ld.getBestTimeToContact());
		}

		if (ld.getWorkPhone() != null) {
			fc.utobj().sendKeys(driver, ui.workPhone, ld.getWorkPhone());
		}

		if (ld.getWorkPhoneExtension() != null) {
			fc.utobj().sendKeys(driver, ui.workPhoneExt, ld.getWorkPhoneExtension());
		}

		if (ld.getHomePhone() != null) {
			fc.utobj().sendKeys(driver, ui.homePhone, ld.getHomePhone());
		}

		if (ld.getHomePhoneExtension() != null) {
			fc.utobj().sendKeys(driver, ui.homePhoneExt, ld.getHomePhoneExtension());
		}

		if (ld.getFax() != null) {
			fc.utobj().sendKeys(driver, ui.fax, ld.getFax());
		}

		if (ld.getMobile() != null) {
			fc.utobj().sendKeys(driver, ui.mobile, ld.getMobile());
		}

		if (ld.getEmail() != null) {
			fc.utobj().sendKeys(driver, ui.emailID, ld.getEmail());
		}

		if (ld.getCompanyName() != null) {
			fc.utobj().sendKeys(driver, ui.companyName, ld.getCompanyName());
		}

		if (ld.getComment() != null) {
			fc.utobj().sendKeys(driver, ui.comments, ld.getComment());
		}

		if (ld.getLeadOwner() != null) {
			fc.utobj().selectDropDown(driver, ui.leadOwnerID, ld.getLeadOwner());
		}

		if (ld.getBasedonAssignmentRules() != null) {
			if (ld.getBasedonAssignmentRules().equalsIgnoreCase("yes")) {
				fc.utobj().clickElement(driver, ui.basedonAssignmentRules);
			}
		}

		if (ld.getLeadRating() != null) {
			fc.utobj().selectDropDown(driver, ui.leadRatingID, ld.getLeadRating());
		}

		if (ld.getMarketingCode() != null) {
			fc.utobj().selectDropDown(driver, ui.marketingCodeId, ld.getMarketingCode());
		}

		if (ld.getLeadSourceCategory() != null) {
			fc.utobj().selectDropDown(driver, ui.leadSource2ID_Category, ld.getLeadSourceCategory());
		}

		if (ld.getLeadSourceDetails() != null) {
			fc.utobj().selectDropDown(driver, ui.leadSource3ID_Details, ld.getLeadSourceDetails());
		}

		if (ld.getOtherLeadSources() != null) {
			fc.utobj().sendKeys(driver, ui.otherLeadSourceDetail, ld.getOtherLeadSources());
		}

		if (ld.getCurrentNetWorth() != null) {
			fc.utobj().sendKeys(driver, ui.CurrentNetWorth, ld.getCurrentNetWorth());
		}

		if (ld.getCashAvailableforInvestment() != null) {
			fc.utobj().sendKeys(driver, ui.CashAvailableforInvestment, ld.getCashAvailableforInvestment());
		}

		if (ld.getInvestmentTimeframe() != null) {
			fc.utobj().sendKeys(driver, ui.investTimeframe, ld.getInvestmentTimeframe());
		}

		if (ld.getBackground() != null) {
			fc.utobj().sendKeys(driver, ui.background, ld.getBackground());
		}

		if (ld.getSourceOfInvestment() != null) {
			fc.utobj().sendKeys(driver, ui.sourceOfFunding, ld.getSourceOfInvestment());
		}

		if (ld.getNextCallDate() != null) {
			fc.utobj().sendKeys(driver, ui.nextCallDate, ld.getNextCallDate());
		}

		if (ld.getNoOfUnitsLocationsRequested() != null) {
			fc.utobj().sendKeys(driver, ui.noOfUnitReq, ld.getNoOfUnitsLocationsRequested());
		}

		if (ld.getDivision() != null) {
			fc.utobj().selectDropDown(driver, ui.brandMapping_0brandID, ld.getDivision());
		}

		if (ld.getPreferredCity1() != null) {
			fc.utobj().sendKeys(driver, ui.temppreferredCity1, ld.getPreferredCity1());
		}

		if (ld.getPreferredCountry1() != null) {
			fc.utobj().selectDropDown(driver, ui.temppreferredCountry1, ld.getPreferredCountry1());
		}

		if (ld.getPreferredStateProvince1() != null) {
			fc.utobj().selectDropDown(driver, ui.temppreferredStateId1, ld.getPreferredStateProvince1());
		}

		if (ld.getPreferredCity2() != null) {
			fc.utobj().sendKeys(driver, ui.temppreferredCity2, ld.getPreferredCity2());
		}

		if (ld.getPreferredCountry2() != null) {
			fc.utobj().selectDropDown(driver, ui.temppreferredCountry2, ld.getPreferredCountry2());
		}

		if (ld.getPreferredStateProvince2() != null) {
			fc.utobj().selectDropDown(driver, ui.temppreferredStateId2, ld.getPreferredStateProvince2());
		}

		if (ld.getNewAvailableSites() != null) {
			fc.utobj().selectDropDown(driver, ui.AvailableSites, ld.getNewAvailableSites());
		}

		if (ld.getExistingSites() != null) {
			fc.utobj().selectDropDown(driver, ui.ExistingSites, ld.getExistingSites());
		}

		if (ld.getResaleSites() != null) {
			fc.utobj().selectDropDown(driver, ui.ResaleSites, ld.getResaleSites());
		}

		if (ld.getForecastClosureDate() != null) {
			fc.utobj().sendKeys(driver, ui.forecastClosureDate, ld.getForecastClosureDate());
		}

		if (ld.getProbability() != null) {
			fc.utobj().sendKeys(driver, ui.probability, ld.getProbability());
		}

		if (ld.getForecastRating() != null) {
			fc.utobj().sendKeys(driver, ui.forecastRating, ld.getForecastRating());
		}

		if (ld.getForecastRevenue() != null) {
			fc.utobj().sendKeys(driver, ui.forecastRevenue, ld.getForecastRevenue());
		}

		if (ld.getCampaignName() != null) {
			fc.utobj().printTestStep("Select Campaign : " + ld.getCampaignName());
			if ("Based on Workflow Assignment Rules".equalsIgnoreCase(ld.getCampaignName())) {
				fc.utobj().clickElement(driver, ui.BasedonWorkflowAssignmentRules);
			} else {
				fc.utobj().clickElement(driver, ui.campaignName);
				fc.utobj().selectDropDown(driver, ui.campaignID, ld.getCampaignName());
			}
		}

		return ld;
	}

	Lead addCoApplicant(WebDriver driver, Lead ldi, LeadAsCoApplicant ldc) throws Exception {

		LeadUI ui = new LeadUI(driver);
		FranconnectUtil fc = new FranconnectUtil();

		if (ldi.getSalutation() != null) {
			fc.utobj().selectDropDownByValue(driver, ui.Salutation, ldi.getSalutation());
		}

		if (ldi.getFirstName() != null) {
			fc.utobj().sendKeys(driver, ui.firstName, ldi.getFirstName());
		}

		if (ldi.getLastName() != null) {
			fc.utobj().sendKeys(driver, ui.lastName, ldi.getLastName());
		}

		if (ldc.getCoApplicantRelationship() != null) {
			fc.utobj().selectDropDown(driver, ui.CoApplicantRelationship, ldc.getCoApplicantRelationship());
		}

		if (ldi.getAddress1() != null) {
			fc.utobj().sendKeys(driver, ui.address1, ldi.getAddress1());
		}

		if (ldi.getAddress2() != null) {
			fc.utobj().sendKeys(driver, ui.address2, ldi.getAddress2());
		}

		if (ldi.getCity() != null) {
			fc.utobj().sendKeys(driver, ui.city, ldi.getCity());
		}

		if (ldi.getCountry() != null) {
			fc.utobj().selectDropDown(driver, ui.country, ldi.getCountry());
		}

		if (ldi.getStateProvince() != null) {
			fc.utobj().selectDropDown(driver, ui.stateID, ldi.getStateProvince());
		}

		if (ldi.getZipPostalCode() != null) {
			fc.utobj().sendKeys(driver, ui.zip, ldi.getZipPostalCode());
		}

		if (ldi.getCounty() != null) {
			fc.utobj().selectDropDown(driver, ui.countyID, ldi.getCounty());
		}

		if (ldi.getPreferredModeofContact() != null) {
			fc.utobj().selectDropDown(driver, ui.PreferredModeofContact, ldi.getPreferredModeofContact());
		}

		if (ldi.getBestTimeToContact() != null) {
			fc.utobj().sendKeys(driver, ui.bestTimeToContact, ldi.getBestTimeToContact());
		}

		if (ldi.getWorkPhone() != null) {
			fc.utobj().sendKeys(driver, ui.workPhone, ldi.getWorkPhone());
		}

		if (ldi.getWorkPhoneExtension() != null) {
			fc.utobj().sendKeys(driver, ui.workPhoneExt, ldi.getWorkPhoneExtension());
		}

		if (ldi.getHomePhone() != null) {
			fc.utobj().sendKeys(driver, ui.homePhone, ldi.getHomePhone());
		}

		if (ldi.getHomePhoneExtension() != null) {
			fc.utobj().sendKeys(driver, ui.homePhoneExt, ldi.getHomePhoneExtension());
		}

		if (ldi.getFax() != null) {
			fc.utobj().sendKeys(driver, ui.fax, ldi.getFax());
		}

		if (ldi.getMobile() != null) {
			fc.utobj().sendKeys(driver, ui.mobile, ldi.getMobile());
		}

		if (ldi.getEmail() != null) {
			fc.utobj().sendKeys(driver, ui.emailID, ldi.getEmail());
		}

		if (ldi.getCompanyName() != null) {
			fc.utobj().sendKeys(driver, ui.companyName, ldi.getCompanyName());
		}

		if (ldi.getComment() != null) {
			fc.utobj().sendKeys(driver, ui.comments, ldi.getComment());
		}

		if (ldi.getLeadOwner() != null) {
			fc.utobj().selectDropDown(driver, ui.leadOwnerID, ldi.getLeadOwner());
		}

		if (ldi.getBasedonAssignmentRules() != null) {
			if (ldi.getBasedonAssignmentRules().equalsIgnoreCase("yes")) {
				fc.utobj().clickElement(driver, ui.basedonAssignmentRules);
			}
		}

		if (ldi.getLeadRating() != null) {
			fc.utobj().selectDropDown(driver, ui.leadRatingID, ldi.getLeadRating());
		}

		if (ldi.getMarketingCode() != null) {
			fc.utobj().selectDropDown(driver, ui.marketingCodeId, ldi.getMarketingCode());
		}

		if (ldi.getLeadSourceCategory() != null) {
			fc.utobj().selectDropDown(driver, ui.leadSource2ID_Category, ldi.getLeadSourceCategory());
		}

		if (ldi.getLeadSourceDetails() != null) {
			fc.utobj().selectDropDown(driver, ui.leadSource3ID_Details, ldi.getLeadSourceDetails());
		}

		if (ldi.getOtherLeadSources() != null) {
			fc.utobj().sendKeys(driver, ui.otherLeadSourceDetail, ldi.getOtherLeadSources());
		}

		if (ldi.getCurrentNetWorth() != null) {
			fc.utobj().sendKeys(driver, ui.CurrentNetWorth, ldi.getCurrentNetWorth());
		}

		if (ldi.getCashAvailableforInvestment() != null) {
			fc.utobj().sendKeys(driver, ui.CashAvailableforInvestment, ldi.getCashAvailableforInvestment());
		}

		if (ldi.getInvestmentTimeframe() != null) {
			fc.utobj().sendKeys(driver, ui.investTimeframe, ldi.getInvestmentTimeframe());
		}

		if (ldi.getBackground() != null) {
			fc.utobj().sendKeys(driver, ui.background, ldi.getBackground());
		}

		if (ldi.getSourceOfInvestment() != null) {
			fc.utobj().sendKeys(driver, ui.sourceOfFunding, ldi.getSourceOfInvestment());
		}

		if (ldi.getNextCallDate() != null) {
			fc.utobj().sendKeys(driver, ui.nextCallDate, ldi.getNextCallDate());
		}

		if (ldi.getNoOfUnitsLocationsRequested() != null) {
			fc.utobj().sendKeys(driver, ui.noOfUnitReq, ldi.getNoOfUnitsLocationsRequested());
		}

		if (ldi.getDivision() != null) {
			fc.utobj().selectDropDown(driver, ui.brandMapping_0brandID, ldi.getDivision());
		}

		if (ldi.getPreferredCity1() != null) {
			fc.utobj().sendKeys(driver, ui.temppreferredCity1, ldi.getPreferredCity1());
		}

		if (ldi.getPreferredCountry1() != null) {
			fc.utobj().selectDropDown(driver, ui.temppreferredCountry1, ldi.getPreferredCountry1());
		}

		if (ldi.getPreferredStateProvince1() != null) {
			fc.utobj().selectDropDown(driver, ui.temppreferredStateId1, ldi.getPreferredStateProvince1());
		}

		if (ldi.getPreferredCity2() != null) {
			fc.utobj().sendKeys(driver, ui.temppreferredCity2, ldi.getPreferredCity2());
		}

		if (ldi.getPreferredCountry2() != null) {
			fc.utobj().selectDropDown(driver, ui.temppreferredCountry2, ldi.getPreferredCountry2());
		}

		if (ldi.getPreferredStateProvince2() != null) {
			fc.utobj().selectDropDown(driver, ui.temppreferredStateId2, ldi.getPreferredStateProvince2());
		}

		if (ldi.getNewAvailableSites() != null) {
			fc.utobj().selectDropDown(driver, ui.AvailableSites, ldi.getNewAvailableSites());
		}

		if (ldi.getExistingSites() != null) {
			fc.utobj().selectDropDown(driver, ui.ExistingSites, ldi.getExistingSites());
		}

		if (ldi.getResaleSites() != null) {
			fc.utobj().selectDropDown(driver, ui.ResaleSites, ldi.getResaleSites());
		}

		if (ldi.getForecastClosureDate() != null) {
			fc.utobj().sendKeys(driver, ui.forecastClosureDate, ldi.getForecastClosureDate());
		}

		if (ldi.getProbability() != null) {
			fc.utobj().sendKeys(driver, ui.probability, ldi.getProbability());
		}

		if (ldi.getForecastRating() != null) {
			fc.utobj().sendKeys(driver, ui.forecastRating, ldi.getForecastRating());
		}

		if (ldi.getForecastRevenue() != null) {
			fc.utobj().sendKeys(driver, ui.forecastRevenue, ldi.getForecastRevenue());
		}

		if (ldi.getCampaignName() != null) {
			fc.utobj().printTestStep("Select Campaign : " + ldi.getCampaignName());
			if ("Based on Workflow Assignment Rules".equalsIgnoreCase(ldi.getCampaignName())) {
				fc.utobj().clickElement(driver, ui.BasedonWorkflowAssignmentRules);
			} else {
				fc.utobj().clickElement(driver, ui.campaignName);
				fc.utobj().selectDropDown(driver, ui.campaignID, ldi.getCampaignName());
			}
		}

		return ldi;
	}

	Lead modifyLead(WebDriver driver, Lead ldi) throws Exception {
		LeadUI ui = new LeadUI(driver);

		Lead lead = new Lead();

		if (ui.firstName.getAttribute("value") != null) {
			lead.setFirstName(ui.firstName.getAttribute("value"));
		}

		if (ui.lastName.getAttribute("value") != null) {
			lead.setLastName(ui.lastName.getAttribute("value"));
		}

		if (ui.address1.getAttribute("value") != null) {
			lead.setAddress1(ui.address1.getAttribute("value"));
		}

		if (ui.address2.getAttribute("value") != null) {
			lead.setAddress2(ui.address2.getAttribute("null"));
		}

		if (ui.city.getAttribute("value") != null) {
			lead.setCity(ui.city.getAttribute("value"));
		}

		if (ui.country.getAttribute("value") != null) {
			lead.setCountry(ui.country.getAttribute("value"));
		}

		if (ui.stateID.getAttribute("value") != null) {
			lead.setStateProvince(ui.stateID.getAttribute("value"));
		}

		if (ui.zip.getAttribute("value") != null) {
			lead.setZipPostalCode(ui.zip.getAttribute("value"));
		}

		if (ui.countyID.getAttribute("value") != null) {
			lead.setCounty(ui.countyID.getAttribute("value"));
		}

		if (ui.PreferredModeofContact.getAttribute("value") != null) {
			lead.setPreferredModeofContact(ui.PreferredModeofContact.getAttribute("value"));
		}

		if (ui.bestTimeToContact.getAttribute("value") != null) {
			lead.setBestTimeToContact(ui.bestTimeToContact.getAttribute("value"));
		}

		if (ui.workPhone.getAttribute("value") != null) {
			lead.setHomePhone(ui.workPhone.getAttribute("value"));
		}

		if (ui.workPhoneExt.getAttribute("value") != null) {
			lead.setWorkPhoneExtension(ui.workPhoneExt.getAttribute("value"));
		}

		if (ui.homePhone.getAttribute("value") != null) {
			lead.setHomePhone(ui.homePhone.getAttribute("value"));
		}

		if (ui.homePhoneExt.getAttribute("value") != null) {
			lead.setHomePhoneExtension(ui.homePhoneExt.getAttribute("null"));
		}

		if (ui.fax.getAttribute("value") != null) {
			lead.setFax(ui.fax.getAttribute("value"));
		}

		if (ui.mobile.getAttribute("value") != null) {
			lead.setMobile(ui.mobile.getAttribute("value"));
		}

		if (ui.emailID.getAttribute("value") != null) {
			lead.setEmail(ui.emailID.getAttribute("value"));
		}

		if (ui.companyName.getAttribute("value") != null) {
			lead.setCompanyName(ui.companyName.getAttribute("value"));
		}

		if (ui.comments.getAttribute("value") != null) {
			lead.setComment(ui.comments.getAttribute("value"));
		}

		if (ui.leadOwnerID.getAttribute("value") != null) {
			lead.setLeadOwner(ui.leadOwnerID.getAttribute("value"));
		}

		if (ui.leadRatingID.getAttribute("value") != null) {
			lead.setLeadRating(ui.leadRatingID.getAttribute("value"));
		}

		if (ui.marketingCodeId.getAttribute("value") != null) {
			lead.setMarketingCode(ui.marketingCodeId.getAttribute("value"));
		}

		if (ui.leadSource2ID_Category.getAttribute("value") != null) {
			lead.setLeadSourceCategory(ui.leadSource2ID_Category.getAttribute("value"));
		}

		if (ui.leadSource3ID_Details.getAttribute("value") != null) {
			lead.setLeadSourceDetails(ui.leadSource3ID_Details.getAttribute("value"));
		}

		if (ui.otherLeadSourceDetail.getAttribute("value") != null) {
			lead.setOtherLeadSources(ui.otherLeadSourceDetail.getAttribute("value"));
		}

		if (ui.CurrentNetWorth.getAttribute("value") != null) {
			lead.setCurrentNetWorth(ui.CurrentNetWorth.getAttribute("value"));
		}

		if (ui.CashAvailableforInvestment.getAttribute("value") != null) {
			lead.setCashAvailableforInvestment(ui.CashAvailableforInvestment.getAttribute("value"));
		}

		if (ui.investTimeframe.getAttribute("value") != null) {
			lead.setInvestmentTimeframe(ui.investTimeframe.getAttribute("value"));
		}

		if (ui.background.getAttribute("value") != null) {
			lead.setBackground(ui.background.getAttribute("value"));
		}

		if (ui.sourceOfFunding.getAttribute("value") != null) {
			lead.setSourceOfInvestment(ui.sourceOfFunding.getAttribute("value"));
		}

		if (ui.nextCallDate.getAttribute("value") != null) {
			lead.setNextCallDate(ui.nextCallDate.getAttribute("value"));
		}

		if (ui.noOfUnitReq.getAttribute("value") != null) {
			lead.setNoOfUnitsLocationsRequested(ui.noOfUnitReq.getAttribute("value"));
		}

		if (ui.brandMapping_0brandID.getAttribute("value") != null) {
			lead.setDivision(ui.brandMapping_0brandID.getAttribute("value"));
		}

		if (ui.temppreferredCity1.getAttribute("value") != null) {
			lead.setPreferredCity1(ui.temppreferredCity1.getAttribute("value"));
		}

		if (ui.temppreferredCountry1.getAttribute("value") != null) {
			lead.setPreferredCountry1(ui.temppreferredCountry1.getAttribute("value"));
		}

		if (ui.temppreferredStateId1.getAttribute("value") != null) {
			lead.setPreferredStateProvince1(ui.temppreferredStateId1.getAttribute("value"));
		}

		if (ui.temppreferredCity2.getAttribute("value") != null) {
			lead.setPreferredCity2(ui.temppreferredCity2.getAttribute("value"));
		}

		if (ui.temppreferredCity2.getAttribute("value") != null) {
			lead.setPreferredCity2(ui.temppreferredCity2.getAttribute("value"));
		}

		if (ui.temppreferredCountry2.getAttribute("value") != null) {
			lead.setPreferredCountry2(ui.temppreferredCountry2.getAttribute("value"));
		}

		if (ui.temppreferredStateId2.getAttribute("value") != null) {
			lead.setPreferredStateProvince2(ui.temppreferredStateId2.getAttribute("value"));
		}

		if (ui.AvailableSites.getAttribute("value") != null) {
			lead.setNewAvailableSites(ui.AvailableSites.getAttribute("value"));
		}

		if (ui.ExistingSites.getAttribute("value") != null) {
			lead.setExistingSites(ui.ExistingSites.getAttribute("value"));
		}

		if (ui.ResaleSites.getAttribute("value") != null) {
			lead.setResaleSites(ui.ResaleSites.getAttribute("value"));
		}

		if (ui.forecastClosureDate.getAttribute("value") != null) {
			lead.setForecastClosureDate(ui.forecastClosureDate.getAttribute("value"));
		}

		if (ui.probability.getAttribute("value") != null) {
			lead.setProbability(ui.probability.getAttribute("value"));
		}

		if (ui.forecastRating.getAttribute("value") != null) {
			lead.setForecastRating(ui.forecastRating.getAttribute("value"));
		}

		if (ui.forecastRevenue.getAttribute("value") != null) {
			lead.setForecastRevenue(ui.forecastRevenue.getAttribute("value"));
		}

		if (ui.campaignName.getAttribute("value") != null) {
			lead.setCampaignName(ui.campaignName.getAttribute("value"));
		}

		return lead;
	}

	void addLeadThroughWebServices(WebDriver driver, Lead ld) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		fc.utobj().printTestStep("Add a Lead Through WebServices");

		String module = "fs";
		String subModule = "lead";
		String isISODate = "no";

		String xmlString = "<fcRequest>" + "<fsLead>" + "<salutation>" + ld.getSalutation() + "</salutation>"
				+ "<firstName>" + ld.getFirstName() + "</firstName>" + "<lastName>" + ld.getLastName() + "</lastName>"
				+ "<address>" + ld.getAddress1() + "</address>" + "<address2>" + ld.getAddress2() + "</address2>"
				+ "<city>" + ld.getCity() + "</city>" + "<country>" + ld.getCountry() + "</country>" + "<stateID>"
				+ ld.getStateProvince() + "</stateID>" + "<sendAutomaticMail>" + ld.getSendAutomaticMail()
				+ "</sendAutomaticMail>" + "<zip>" + ld.getZipPostalCode() + "</zip>" + "<countyID>" + ld.getCounty()
				+ "</countyID>" + "<primaryPhoneToCall>" + ld.getPreferredModeofContact() + "</primaryPhoneToCall>"
				+ "<bestTimeToContact>" + ld.getBestTimeToContact() + "</bestTimeToContact>" + "<phone>"
				+ ld.getWorkPhone() + "</phone>" + "<phoneExt>" + ld.getWorkPhoneExtension() + "</phoneExt>"
				+ "<homePhone>" + ld.getHomePhone() + "</homePhone>" + "<homePhoneExt>" + ld.getHomePhoneExtension()
				+ "</homePhoneExt>" + "<fax>" + ld.getFax() + "</fax>" + "<mobile>" + ld.getMobile() + "</mobile>"
				+ "<emailID>" + ld.getEmail() + "</emailID>" + "<companyName>" + ld.getCompanyName() + "</companyName>"
				+ "<comments>" + ld.getComment() + "</comments>" + "<basedOnAssignmentRule>"
				+ ld.getBasedonAssignmentRules() + "</basedOnAssignmentRule>" + "<leadOwnerID>" + ld.getLeadOwner()
				+ "</leadOwnerID>" + "<leadOwnerReferenceId>" + "</leadOwnerReferenceId>" + "<leadRatingID>"
				+ ld.getLeadRating() + "</leadRatingID>" + "<marketingCodeId>" + ld.getMarketingCode()
				+ "</marketingCodeId>" + "<leadSource2ID>" + ld.getLeadSourceCategory() + "</leadSource2ID>"
				+ "<leadSource3ID>" + ld.getLeadSourceDetails() + "</leadSource3ID>" + "<otherLeadSourceDetail>"
				+ ld.getOtherLeadSources() + "</otherLeadSourceDetail>" + "<liquidCapitalMin>"
				+ ld.getCashAvailableforInvestment() + "</liquidCapitalMin>" + "<liquidCapitalMax>"
				+ ld.getCurrentNetWorth() + "</liquidCapitalMax>" + "<investTimeframe>" + ld.getInvestmentTimeframe()
				+ "</investTimeframe>" + "<background>" + ld.getBackground() + "</background>" + "<sourceOfFunding>"
				+ ld.getSourceOfInvestment() + "</sourceOfFunding>" + "<nextCallDate>" + ld.getNextCallDate()
				+ "</nextCallDate>" + "<divisionReferenceId>" + ld.getDivision() + "</divisionReferenceId>"
				+ "<division>" + ld.getDivision() + "</division>" + "<noOfUnitReq>"
				+ ld.getNoOfUnitsLocationsRequested() + "</noOfUnitReq>" + "<locationId1>" + ld.getNewAvailableSites()
				+ "</locationId1>" + "<locationId1b>" + ld.getExistingSites() + "</locationId1b>" + "<locationId2>"
				+ ld.getResaleSites() + "</locationId2>" + "<preferredCity1>" + ld.getPreferredCity1()
				+ "</preferredCity1>" + "<preferredCountry1>" + ld.getPreferredCountry1() + "</preferredCountry1>"
				+ "<preferredStateId1>" + ld.getPreferredStateProvince1() + "</preferredStateId1>" + "<preferredCity2>"
				+ ld.getPreferredCity2() + "</preferredCity2>" + "<preferredCountry2>" + ld.getPreferredCountry2()
				+ "</preferredCountry2>" + "<preferredStateId2>" + ld.getPreferredStateProvince2()
				+ "</preferredStateId2>" + "<forecastClosureDate>" + ld.getForecastClosureDate()
				+ "</forecastClosureDate>" + "<probability>" + ld.getProbability() + "</probability>"
				+ "<forecastRating>" + ld.getForecastRating() + "</forecastRating>" + "<forecastRevenue>"
				+ ld.getForecastRevenue() + "</forecastRevenue>" + "<basedOnWorkflowAssignmentRule>"
				+ "</basedOnWorkflowAssignmentRule>" + "<campaignID>" + ld.getCampaignName() + "</campaignID>"
				+ "</fsLead>" + "</fcRequest>";

		WebService rest = new WebService();
		System.out.println(rest.create(module, subModule, xmlString, isISODate));

	}

}
