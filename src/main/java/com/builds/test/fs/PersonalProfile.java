package com.builds.test.fs;

class PersonalProfile {

	// Personal Information
	private String firstName;
	private String lastName;
	private String gender;
	private String homeAddress;
	private String howLongAtAddress;
	private String homeCity;
	private String homeCountry;
	private String homeStateProvince;
	private String homeZipPostalCode;
	private String birthMonth;
	private String birthDate;
	private String homePhone;
	private String homePhoneExt;
	private String timeToCall;
	private String homeOwnership;
	private String email;
	// private String maritalStatus;
	private String maritalStatus;
	private String spouseName;
	private String heardProformaFrom;
	private String seekingOwnBusiness;
	private String fullTimeBusiness;
	private String otherInvestigation;

	// Employment
	private String presentEmployer;
	private String percentOwn;
	private String title;
	private String dateStarted;
	private String employerAddress;
	private String employerCity;
	private String employerCountry;
	private String employerState;
	private String employerZip;
	private String businessPhone;
	private String callAtWork;
	private String hourPerWeek;
	private String salary;
	private String responsibility;
	private String selfEmployed;
	private String limitProforma;
	private String similarWork;
	private String financeProforma;
	private String partner;
	private String supportHowLong;
	private String income;
	private String otherSalary;
	private String otherIncomeExplaination;
	private String soleSource;
	private String howSoon;
	private String runYourself;
	private String responsibleForOperation;
	private String convictedForFelony;
	private String liabilites;
	private String bankruptcy;
	private String lawsuit;
	private String convicted;
	private String familyFeelings;
	private String otherFacts;
	private String personalPersonalityStyle;
	private String backgroundOverview;
	private String goalDream;
	private String concerns;
	private String timing;
	private String hotButtons;
	private String otherOptions;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getHomeAddress() {
		return homeAddress;
	}

	public void setHomeAddress(String homeAddress) {
		this.homeAddress = homeAddress;
	}

	public String getHowLongAtAddress() {
		return howLongAtAddress;
	}

	public void setHowLongAtAddress(String howLongAtAddress) {
		this.howLongAtAddress = howLongAtAddress;
	}

	public String getHomeCity() {
		return homeCity;
	}

	public void setHomeCity(String homeCity) {
		this.homeCity = homeCity;
	}

	public String getHomeCountry() {
		return homeCountry;
	}

	public void setHomeCountry(String homeCountry) {
		this.homeCountry = homeCountry;
	}

	public String getHomeStateProvince() {
		return homeStateProvince;
	}

	public void setHomeStateProvince(String homeStateProvince) {
		this.homeStateProvince = homeStateProvince;
	}

	public String getHomeZipPostalCode() {
		return homeZipPostalCode;
	}

	public void setHomeZipPostalCode(String homeZipPostalCode) {
		this.homeZipPostalCode = homeZipPostalCode;
	}

	public String getBirthMonth() {
		return birthMonth;
	}

	public void setBirthMonth(String birthMonth) {
		this.birthMonth = birthMonth;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	public String getHomePhone() {
		return homePhone;
	}

	public void setHomePhone(String homePhone) {
		this.homePhone = homePhone;
	}

	public String getHomePhoneExt() {
		return homePhoneExt;
	}

	public void setHomePhoneExt(String homePhoneExt) {
		this.homePhoneExt = homePhoneExt;
	}

	public String getTimeToCall() {
		return timeToCall;
	}

	public void setTimeToCall(String timeToCall) {
		this.timeToCall = timeToCall;
	}

	public String getHomeOwnership() {
		return homeOwnership;
	}

	public void setHomeOwnership(String homeOwnership) {
		this.homeOwnership = homeOwnership;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMaritalStatus() {
		return maritalStatus;
	}

	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public String getSpouseName() {
		return spouseName;
	}

	public void setSpouseName(String spouseName) {
		this.spouseName = spouseName;
	}

	public String getHeardProformaFrom() {
		return heardProformaFrom;
	}

	public void setHeardProformaFrom(String heardProformaFrom) {
		this.heardProformaFrom = heardProformaFrom;
	}

	public String getSeekingOwnBusiness() {
		return seekingOwnBusiness;
	}

	public void setSeekingOwnBusiness(String seekingOwnBusiness) {
		this.seekingOwnBusiness = seekingOwnBusiness;
	}

	public String getFullTimeBusiness() {
		return fullTimeBusiness;
	}

	public void setFullTimeBusiness(String fullTimeBusiness) {
		this.fullTimeBusiness = fullTimeBusiness;
	}

	public String getOtherInvestigation() {
		return otherInvestigation;
	}

	public void setOtherInvestigation(String otherInvestigation) {
		this.otherInvestigation = otherInvestigation;
	}

	public String getPresentEmployer() {
		return presentEmployer;
	}

	public void setPresentEmployer(String presentEmployer) {
		this.presentEmployer = presentEmployer;
	}

	public String getPercentOwn() {
		return percentOwn;
	}

	public void setPercentOwn(String percentOwn) {
		this.percentOwn = percentOwn;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDateStarted() {
		return dateStarted;
	}

	public void setDateStarted(String dateStarted) {
		this.dateStarted = dateStarted;
	}

	public String getEmployerAddress() {
		return employerAddress;
	}

	public void setEmployerAddress(String employerAddress) {
		this.employerAddress = employerAddress;
	}

	public String getEmployerCity() {
		return employerCity;
	}

	public void setEmployerCity(String employerCity) {
		this.employerCity = employerCity;
	}

	public String getEmployerCountry() {
		return employerCountry;
	}

	public void setEmployerCountry(String employerCountry) {
		this.employerCountry = employerCountry;
	}

	public String getEmployerState() {
		return employerState;
	}

	public void setEmployerState(String employerState) {
		this.employerState = employerState;
	}

	public String getEmployerZip() {
		return employerZip;
	}

	public void setEmployerZip(String employerZip) {
		this.employerZip = employerZip;
	}

	public String getBusinessPhone() {
		return businessPhone;
	}

	public void setBusinessPhone(String businessPhone) {
		this.businessPhone = businessPhone;
	}

	public String getCallAtWork() {
		return callAtWork;
	}

	public void setCallAtWork(String callAtWork) {
		this.callAtWork = callAtWork;
	}

	public String getHourPerWeek() {
		return hourPerWeek;
	}

	public void setHourPerWeek(String hourPerWeek) {
		this.hourPerWeek = hourPerWeek;
	}

	public String getSalary() {
		return salary;
	}

	public void setSalary(String salary) {
		this.salary = salary;
	}

	public String getResponsibility() {
		return responsibility;
	}

	public void setResponsibility(String responsibility) {
		this.responsibility = responsibility;
	}

	public String getSelfEmployed() {
		return selfEmployed;
	}

	public void setSelfEmployed(String selfEmployed) {
		this.selfEmployed = selfEmployed;
	}

	public String getLimitProforma() {
		return limitProforma;
	}

	public void setLimitProforma(String limitProforma) {
		this.limitProforma = limitProforma;
	}

	public String getSimilarWork() {
		return similarWork;
	}

	public void setSimilarWork(String similarWork) {
		this.similarWork = similarWork;
	}

	public String getFinanceProforma() {
		return financeProforma;
	}

	public void setFinanceProforma(String financeProforma) {
		this.financeProforma = financeProforma;
	}

	public String getPartner() {
		return partner;
	}

	public void setPartner(String partner) {
		this.partner = partner;
	}

	public String getSupportHowLong() {
		return supportHowLong;
	}

	public void setSupportHowLong(String supportHowLong) {
		this.supportHowLong = supportHowLong;
	}

	public String getIncome() {
		return income;
	}

	public void setIncome(String income) {
		this.income = income;
	}

	public String getOtherSalary() {
		return otherSalary;
	}

	public void setOtherSalary(String otherSalary) {
		this.otherSalary = otherSalary;
	}

	public String getOtherIncomeExplaination() {
		return otherIncomeExplaination;
	}

	public void setOtherIncomeExplaination(String otherIncomeExplaination) {
		this.otherIncomeExplaination = otherIncomeExplaination;
	}

	public String getSoleSource() {
		return soleSource;
	}

	public void setSoleSource(String soleSource) {
		this.soleSource = soleSource;
	}

	public String getHowSoon() {
		return howSoon;
	}

	public void setHowSoon(String howSoon) {
		this.howSoon = howSoon;
	}

	public String getRunYourself() {
		return runYourself;
	}

	public void setRunYourself(String runYourself) {
		this.runYourself = runYourself;
	}

	public String getResponsibleForOperation() {
		return responsibleForOperation;
	}

	public void setResponsibleForOperation(String responsibleForOperation) {
		this.responsibleForOperation = responsibleForOperation;
	}

	public String getConvictedForFelony() {
		return convictedForFelony;
	}

	public void setConvictedForFelony(String convictedForFelony) {
		this.convictedForFelony = convictedForFelony;
	}

	public String getLiabilites() {
		return liabilites;
	}

	public void setLiabilites(String liabilites) {
		this.liabilites = liabilites;
	}

	public String getBankruptcy() {
		return bankruptcy;
	}

	public void setBankruptcy(String bankruptcy) {
		this.bankruptcy = bankruptcy;
	}

	public String getLawsuit() {
		return lawsuit;
	}

	public void setLawsuit(String lawsuit) {
		this.lawsuit = lawsuit;
	}

	public String getConvicted() {
		return convicted;
	}

	public void setConvicted(String convicted) {
		this.convicted = convicted;
	}

	public String getFamilyFeelings() {
		return familyFeelings;
	}

	public void setFamilyFeelings(String familyFeelings) {
		this.familyFeelings = familyFeelings;
	}

	public String getOtherFacts() {
		return otherFacts;
	}

	public void setOtherFacts(String otherFacts) {
		this.otherFacts = otherFacts;
	}

	public String getPersonalPersonalityStyle() {
		return personalPersonalityStyle;
	}

	public void setPersonalPersonalityStyle(String personalPersonalityStyle) {
		this.personalPersonalityStyle = personalPersonalityStyle;
	}

	public String getBackgroundOverview() {
		return backgroundOverview;
	}

	public void setBackgroundOverview(String backgroundOverview) {
		this.backgroundOverview = backgroundOverview;
	}

	public String getGoalDream() {
		return goalDream;
	}

	public void setGoalDream(String goalDream) {
		this.goalDream = goalDream;
	}

	public String getConcerns() {
		return concerns;
	}

	public void setConcerns(String concerns) {
		this.concerns = concerns;
	}

	public String getTiming() {
		return timing;
	}

	public void setTiming(String timing) {
		this.timing = timing;
	}

	public String getHotButtons() {
		return hotButtons;
	}

	public void setHotButtons(String hotButtons) {
		this.hotButtons = hotButtons;
	}

	public String getOtherOptions() {
		return otherOptions;
	}

	public void setOtherOptions(String otherOptions) {
		this.otherOptions = otherOptions;
	}

}
