package com.builds.test.fs;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.fs.PersonalProfileUI;
import com.builds.utilities.FranconnectUtil;

public class PersonalProfileTest {

	public void fillAndSubmitPersonalProfile(WebDriver driver, PersonalProfile pp) throws Exception {
		PersonalProfileUI ppui = new PersonalProfileUI(driver);
		FranconnectUtil fc = new FranconnectUtil();

		fc.utobj().printTestStep("Fill Personal Profile Info");

		// Personal Information
		if (pp.getFirstName() != null) {
			fc.utobj().sendKeys(driver, ppui.firstName, pp.getFirstName());
		}

		if (pp.getLastName() != null) {
			fc.utobj().sendKeys(driver, ppui.lastName, pp.getLastName());
		}

		if (pp.getGender() != null) {
			if (pp.getGender().equalsIgnoreCase("Male")) {
				fc.utobj().clickElement(driver, ppui.genderMale);
			} else if (pp.getGender().equalsIgnoreCase("Female")) {
				fc.utobj().clickElement(driver, ppui.genderFemale);
			}
		}

		if (pp.getHomeAddress() != null) {
			fc.utobj().sendKeys(driver, ppui.homeAddress, pp.getHomeAddress());
		}

		if (pp.getHowLongAtAddress() != null) {
			fc.utobj().sendKeys(driver, ppui.howLongAtAddress, pp.getHowLongAtAddress());
		}

		if (pp.getHomeCity() != null) {
			fc.utobj().sendKeys(driver, ppui.homeCity, pp.getHomeCity());
		}

		if (pp.getHomeCountry() != null) {
			fc.utobj().selectDropDownByVisibleText(driver, ppui.homeCountry, pp.getHomeCountry());
		}

		if (pp.getHomeStateProvince() != null) {
			fc.utobj().selectDropDownByVisibleText(driver, ppui.homeState, pp.getHomeStateProvince());
		}

		if (pp.getHomeZipPostalCode() != null) {
			fc.utobj().sendKeys(driver, ppui.homeZip, pp.getHomeZipPostalCode());
		}

		if (pp.getBirthMonth() != null) {
			fc.utobj().selectDropDownByVisibleText(driver, ppui.birthMonth, pp.getBirthMonth());
		}

		if (pp.getBirthDate() != null) {
			fc.utobj().selectDropDownByVisibleText(driver, ppui.birthDate, pp.getBirthDate());
		}

		if (pp.getHomePhone() != null) {
			fc.utobj().sendKeys(driver, ppui.homePhone, pp.getHomePhone());
		}

		if (pp.getHomePhoneExt() != null) {
			fc.utobj().sendKeys(driver, ppui.homePhoneExt, pp.getHomePhoneExt());
		}

		if (pp.getTimeToCall() != null) {
			fc.utobj().sendKeys(driver, ppui.timeToCall, pp.getTimeToCall());
		}

		if (pp.getHomeOwnership() != null) {
			if (pp.getHomeOwnership().equalsIgnoreCase("Own")) {
				fc.utobj().clickElement(driver, ppui.homeOwnershipOwn);
			} else if (pp.getHomeOwnership().equalsIgnoreCase("Renting")) {
				fc.utobj().clickElement(driver, ppui.homeOwnershipRenting);
			}
		}

		if (pp.getEmail() != null) {
			fc.utobj().sendKeys(driver, ppui.email, pp.getEmail());
		}

		// private String maritalStatus;

		if (pp.getMaritalStatus() != null) {
			if (pp.getMaritalStatus().equalsIgnoreCase("Single")) {
				fc.utobj().clickElement(driver, ppui.maritalStatusSingle);
			} else if (pp.getMaritalStatus().equalsIgnoreCase("Married")) {
				fc.utobj().clickElement(driver, ppui.maritalStatusMarried);
			}
		}

		if (pp.getSpouseName() != null) {
			fc.utobj().sendKeys(driver, ppui.spouseName, pp.getSpouseName());
		}

		if (pp.getHeardProformaFrom() != null) {
			fc.utobj().sendKeys(driver, ppui.heardProformaFrom, pp.getHeardProformaFrom());
		}

		if (pp.getSeekingOwnBusiness() != null) {
			fc.utobj().sendKeys(driver, ppui.seekingOwnBusiness, pp.getSeekingOwnBusiness());
		}

		if (pp.getFullTimeBusiness() != null) {
			fc.utobj().sendKeys(driver, ppui.fullTimeBusiness, pp.getFullTimeBusiness());
		}

		if (pp.getOtherInvestigation() != null) {
			fc.utobj().sendKeys(driver, ppui.otherInvestigation, pp.getOtherInvestigation());
		}

		// Employment

		if (pp.getPresentEmployer() != null) {
			fc.utobj().sendKeys(driver, ppui.presentEmployer, pp.getPresentEmployer());
		}

		if (pp.getPercentOwn() != null) {
			fc.utobj().sendKeys(driver, ppui.percentOwn, pp.getPercentOwn());
		}

		if (pp.getTitle() != null) {
			fc.utobj().sendKeys(driver, ppui.title, pp.getTitle());
		}

		if (pp.getDateStarted() != null) {
			fc.utobj().sendKeys(driver, ppui.dateStarted, pp.getDateStarted());
		}

		if (pp.getEmployerAddress() != null) {
			fc.utobj().sendKeys(driver, ppui.employerAddress, pp.getEmployerAddress());
		}

		if (pp.getEmployerCity() != null) {
			fc.utobj().sendKeys(driver, ppui.employerCity, pp.getEmployerCity());
		}

		if (pp.getEmployerCountry() != null) {
			fc.utobj().selectDropDownByVisibleText(driver, ppui.employerCountry, pp.getEmployerCountry());
		}

		if (pp.getEmployerState() != null) {
			fc.utobj().selectDropDownByVisibleText(driver, ppui.employerState, pp.getEmployerState());
		}

		if (pp.getEmployerZip() != null) {
			fc.utobj().sendKeys(driver, ppui.employerZip, pp.getEmployerZip());
		}

		if (pp.getBusinessPhone() != null) {
			fc.utobj().sendKeys(driver, ppui.businessPhone, pp.getBusinessPhone());
		}

		if (pp.getCallAtWork() != null) {
			fc.utobj().sendKeys(driver, ppui.callAtWork, pp.getCallAtWork());
		}

		if (pp.getHourPerWeek() != null) {
			fc.utobj().selectDropDownByVisibleText(driver, ppui.hourPerWeek, pp.getHourPerWeek());
		}

		if (pp.getSalary() != null) {
			fc.utobj().sendKeys(driver, ppui.salary, pp.getSalary());
		}

		if (pp.getResponsibility() != null) {
			fc.utobj().sendKeys(driver, ppui.responsibility, pp.getResponsibility());
		}

		if (pp.getSelfEmployed() != null) {
			if (pp.getSelfEmployed().equalsIgnoreCase("Yes")) {
				fc.utobj().clickElement(driver, ppui.selfEmployedYes);
			} else if (pp.getSelfEmployed().equalsIgnoreCase("No")) {
				fc.utobj().clickElement(driver, ppui.selfEmployedNo);
			}
		}

		if (pp.getLimitProforma() != null) {
			if (pp.getLimitProforma().equalsIgnoreCase("Yes")) {
				fc.utobj().clickElement(driver, ppui.limitProformaYes);
			} else if (pp.getLimitProforma().equalsIgnoreCase("No")) {
				fc.utobj().clickElement(driver, ppui.limitProformaNo);
			}
		}

		if (pp.getSimilarWork() != null) {
			if (pp.getSimilarWork().equalsIgnoreCase("Yes")) {
				fc.utobj().clickElement(driver, ppui.similarWorkYes);
			} else if (pp.getSimilarWork().equalsIgnoreCase("No")) {
				fc.utobj().clickElement(driver, ppui.similarWorkNo);
			}
		}

		if (pp.getFinanceProforma() != null) {
			fc.utobj().sendKeys(driver, ppui.financeProforma, pp.getFinanceProforma());
		}

		if (pp.getPartner() != null) {
			if (pp.getPartner().equalsIgnoreCase("Yes")) {
				fc.utobj().clickElement(driver, ppui.partnerYes);
			} else if (pp.getPartner().equalsIgnoreCase("No")) {
				fc.utobj().clickElement(driver, ppui.partnerNo);
			}
		}

		if (pp.getSupportHowLong() != null) {
			fc.utobj().selectDropDownByVisibleText(driver, ppui.supportHowLong, pp.getSupportHowLong());
		}

		if (pp.getIncome() != null) {
			fc.utobj().sendKeys(driver, ppui.income, pp.getIncome());
		}

		if (pp.getOtherSalary() != null) {
			fc.utobj().sendKeys(driver, ppui.otherSalary, pp.getOtherSalary());
		}

		if (pp.getOtherIncomeExplaination() != null) {
			fc.utobj().sendKeys(driver, ppui.otherIncomeExplaination, pp.getOtherIncomeExplaination());
		}

		if (pp.getSoleSource() != null) {
			if (pp.getSoleSource().equalsIgnoreCase("Yes")) {
				fc.utobj().clickElement(driver, ppui.soleSourceYes);
			} else if (pp.getSoleSource().equalsIgnoreCase("No")) {
				fc.utobj().clickElement(driver, ppui.soleSourceNo);
			}
		}

		if (pp.getHowSoon() != null) {
			fc.utobj().sendKeys(driver, ppui.howSoon, pp.getHowSoon());
		}

		if (pp.getRunYourself() != null) {
			if (pp.getRunYourself().equalsIgnoreCase("Yes")) {
				fc.utobj().clickElement(driver, ppui.runYourselfYes);
			} else if (pp.getRunYourself().equalsIgnoreCase("No")) {
				fc.utobj().clickElement(driver, ppui.runYourselfNo);
			}
		}

		if (pp.getResponsibleForOperation() != null) {
			fc.utobj().sendKeys(driver, ppui.responsibleForOperation, pp.getResponsibleForOperation());
		}

		if (pp.getConvictedForFelony() != null) {
			if (pp.getConvictedForFelony().equalsIgnoreCase("Yes")) {
				fc.utobj().clickElement(driver, ppui.convictedForFelonyYes);
			} else if (pp.getConvictedForFelony().equalsIgnoreCase("No")) {
				fc.utobj().clickElement(driver, ppui.convictedForFelonyNo);
			}
		}

		if (pp.getLiabilites() != null) {
			if (pp.getLiabilites().equalsIgnoreCase("Yes")) {
				fc.utobj().clickElement(driver, ppui.liabilitesYes);
			} else if (pp.getLiabilites().equalsIgnoreCase("No")) {
				fc.utobj().clickElement(driver, ppui.liabilitesNo);
			}
		}

		if (pp.getBankruptcy() != null) {
			if (pp.getBankruptcy().equalsIgnoreCase("Yes")) {
				fc.utobj().clickElement(driver, ppui.bankruptcyYes);
			} else if (pp.getBankruptcy().equalsIgnoreCase("No")) {
				fc.utobj().clickElement(driver, ppui.bankruptcyNo);
			}
		}

		if (pp.getLawsuit() != null) {
			if (pp.getLawsuit().equalsIgnoreCase("Yes")) {
				fc.utobj().clickElement(driver, ppui.lawsuitYes);
			} else if (pp.getLawsuit().equalsIgnoreCase("No")) {
				fc.utobj().clickElement(driver, ppui.lawsuitNo);
			}
		}

		if (pp.getConvicted() != null) {
			if (pp.getConvicted().equalsIgnoreCase("Yes")) {
				fc.utobj().clickElement(driver, ppui.convictedYes);
			} else if (pp.getConvicted().equalsIgnoreCase("No")) {
				fc.utobj().clickElement(driver, ppui.convictedNo);
			}
		}

		if (pp.getFamilyFeelings() != null) {
			fc.utobj().sendKeys(driver, ppui.familyFeelings, pp.getFamilyFeelings());
		}

		if (pp.getOtherFacts() != null) {
			fc.utobj().sendKeys(driver, ppui.otherFacts, pp.getOtherFacts());
		}

		if (pp.getPersonalPersonalityStyle() != null) {
			fc.utobj().sendKeys(driver, ppui.personalPersonalityStyle, pp.getPersonalPersonalityStyle());
		}

		if (pp.getBackgroundOverview() != null) {
			fc.utobj().sendKeys(driver, ppui.backgroundOverview, pp.getBackgroundOverview());
		}

		if (pp.getGoalDream() != null) {
			fc.utobj().sendKeys(driver, ppui.goalDream, pp.getGoalDream());
		}

		if (pp.getConcerns() != null) {
			fc.utobj().sendKeys(driver, ppui.concerns, pp.getConcerns());
		}

		if (pp.getTiming() != null) {
			fc.utobj().sendKeys(driver, ppui.timing, pp.getTiming());
		}

		if (pp.getHotButtons() != null) {
			fc.utobj().sendKeys(driver, ppui.hotButtons, pp.getHotButtons());
		}

		if (pp.getOtherOptions() != null) {
			fc.utobj().sendKeys(driver, ppui.otherOptions, pp.getOtherOptions());
		}

		fc.utobj().clickElement(driver, ppui.saveBtn);

	}

}
