package com.builds.test.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.builds.uimaps.fs.LeadUI;
import com.builds.uimaps.fs.PrimaryInfoUI;
import com.builds.utilities.FranconnectUtil;

class PrimaryInfo {

	public String getCurrentLeadStatusFromDropDown(WebDriver driver) throws Exception {
		PrimaryInfoUI ui = new PrimaryInfoUI(driver);
		FranconnectUtil fc = new FranconnectUtil();

		return fc.utobj().getSelectedDropDownValue(driver, ui.LeadStatus_Select).trim();
	}

	public void clickSave(WebDriver driver, Lead ld) throws Exception {
		LeadUI ui = new LeadUI(driver);
		FranconnectUtil fc = new FranconnectUtil();

		fc.utobj().printTestStep("Click Save");
		fc.utobj().clickElement(driver, ui.Save);
	}

	public void click_AddRemarks_TopLink(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		PrimaryInfoUI ui = new PrimaryInfoUI(driver);

		fc.utobj().printTestStep("Click Add Remarks - Top Link");
		fc.utobj().clickElement(driver, ui.AddRemarks_Link);
		fc.commonMethods().switch_cboxIframe_frameId(driver);
	}

	public void click_Modify_TopLink(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		PrimaryInfoUI ui = new PrimaryInfoUI(driver);

		fc.utobj().printTestStep("Click Modify - Top Link");
		fc.utobj().clickElement(driver, ui.modify_Link);
	}

	public void click_Log_A_Call_TopLink(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		PrimaryInfoUI ui = new PrimaryInfoUI(driver);

		fc.utobj().printTestStep("Click Log A Call - Top Link");
		fc.utobj().clickElement(driver, ui.LogaCall_Link);
		fc.commonMethods().switch_cboxIframe_frameId(driver);
	}

	public void click_Log_A_Task_TopLink(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		PrimaryInfoUI ui = new PrimaryInfoUI(driver);

		fc.utobj().printTestStep("Click Log A Task - Top Link");
		fc.utobj().clickElement(driver, ui.LogaTask_Link);
		fc.commonMethods().switch_cboxIframe_frameId(driver);
	}

	public void click_AddRemarks_ActivityHistory(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		PrimaryInfoUI ui = new PrimaryInfoUI(driver);

		fc.utobj().printTestStep("Click Add Remarks - Activity History");
		switchToActivityHistoryFrame(driver);
		fc.utobj().clickElement(driver, ui.AddRemarks_Link_ActivityHistory);
		fc.utobj().switchFrameToDefault(driver);
		fc.commonMethods().switch_cboxIframe_frameId(driver);
	}

	public void click_DetailedHistory_ActivityHistory(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		PrimaryInfoUI ui = new PrimaryInfoUI(driver);

		fc.utobj().printTestStep("Click Detailed History");
		switchToActivityHistoryFrame(driver);
		fc.utobj().clickElementByJS(driver, ui.DetailedHistory_Link_ActivityHistory);
		fc.utobj().switchFrameToDefault(driver);
		fc.commonMethods().switch_cboxIframe_frameId(driver);
	}

	public void verify_RemarksAndCallHistory_In_DetailedHistory(WebDriver driver, String subject) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		click_DetailedHistory_ActivityHistory(driver);
		clickRemarksandCallHistory_DetailedHistory(driver);

		fc.utobj().printTestStep("Verify that Remark is available in Remarks And Call History");
		verifyTextInDetailedHistoryWindow(driver, subject);
	}

	public void verify_ActivityHistory_In_DetailedHistory(WebDriver driver, String subject) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		click_DetailedHistory_ActivityHistory(driver);
		clickActivityHistory_DetailedHistory(driver);

		fc.utobj().printTestStep("Verify that Activity is available in Activity History");
		verifyTextInDetailedHistoryWindow(driver, subject);
	}

	public void verifyTextInDetailedHistoryWindow(WebDriver driver, String text) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();

		if (fc.utobj().assertPageSource(driver, text) == false) {
			fc.utobj().throwsException("Remarks history not found.");
		}
		fc.utobj().switchFrameToDefault(driver);
		fc.sales().sales_common().closeColorBox(driver);
		fc.utobj().switchFrameToDefault(driver);
	}

	public void verify_StatusChangeHistory_In_DetailedHistory(WebDriver driver, Status status) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		click_DetailedHistory_ActivityHistory(driver);
		clickStatusChangeHistory_DetailedHistory(driver);

		fc.utobj().printTestStep("Verify the Status change history.");
		verifyTextInDetailedHistoryWindow(driver, status.getLeadStatus());
	}

	public void logACall_TopLink(WebDriver driver, Call call) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		fc.utobj().printTestStep("Verify log a Call");

		// TODO

	}

	public void logATask_TopLink(WebDriver driver, Task task) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		fc.utobj().printTestStep("Verify log a Task");

		// TODO

	}

	public void ViewElectronicDetails_TopLink(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		fc.utobj().printTestStep("Verify View Electronic Details");

		// TODO This code is incomplete
	}

	private void switchToActivityHistoryFrame(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		PrimaryInfoUI ui = new PrimaryInfoUI(driver);
		fc.utobj().switchFrameById(driver, ui.leadLogCallSummary_IFrame_ById);

	}

	public void verify_TextUnder_ActivityHistory(WebDriver driver, String text) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();

		fc.utobj().printTestStep("Verify the Subject/Remarks is present");

		switchToActivityHistoryFrame(driver);

		if (!fc.utobj().assertPageSource(driver, text)) {
			fc.utobj().throwsException("Remarks not found in the Activity History.");
		}
		fc.utobj().switchFrameToDefault(driver);
	}

	public void verifyCallInfo_ActivityHistory(WebDriver driver, Call call) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();

		switchToActivityHistoryFrame(driver);
		fc.utobj().printTestStep("Verify Call is present in Activity History.");
		if (fc.utobj().assertPageSource(driver, call.getSubject()) == false) {
			fc.utobj().throwsException("Call not present in Activity History");
		}

		// TODO: This method is incomplete

		fc.utobj().switchFrameToDefault(driver);

	}

	public void verifyTaskInfo_ActivityHistory(WebDriver driver, Task task) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		PrimaryInfoUI ui = new PrimaryInfoUI(driver);

		fc.utobj().printTestStep("Verify Task in Contact History > Open Task");
		fc.utobj().switchFrameById(driver, ui.leadOpenActivitesSummary_IFrame_ById);

		// TODO: This method is incomplete

		if (!fc.utobj().assertPageSource(driver, task.getSubject())) {
			fc.utobj().throwsException("Task not found in Open Task Section");
		}

		fc.utobj().switchFrameToDefault(driver);

	}

	public void modifyCallInfo_ActivityHistory(WebDriver driver, Call call) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		PrimaryInfoUI ui = new PrimaryInfoUI(driver);

		switchToActivityHistoryFrame(driver);
		fc.utobj().printTestStep("Modify Call in Activity History.");

		fc.utobj().clickElement(driver, call.getSubject());
		fc.utobj().clickElement(driver, ui.getXPathOfActivityHistoryRemarksActions(""));

		// TODO This method is incomplete.

		fc.utobj().switchFrameToDefault(driver);

	}

	public void clickPrev(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		PrimaryInfoUI ui = new PrimaryInfoUI(driver);

		fc.utobj().printTestStep("Click Prev link");
		fc.utobj().clickElement(driver, ui.prev_Xpath);
	}

	public void clickNext(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		PrimaryInfoUI ui = new PrimaryInfoUI(driver);

		fc.utobj().printTestStep("Click Next link");
		fc.utobj().clickElement(driver, ui.next_Xpath);
	}

	public void clickNextOrPrev(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		PrimaryInfoUI ui = new PrimaryInfoUI(driver);

		if (fc.utobj().isElementPresent(driver, ui.next_Xpath) == true) {
			fc.utobj().printTestStep("Click Next Link");
			fc.utobj().isElementPresent(driver, ui.next_Xpath);
		} else if (fc.utobj().isElementPresent(driver, ui.prev_Xpath) == true) {
			fc.utobj().printTestStep("Click Prev Link");
			fc.utobj().isElementPresent(driver, ui.prev_Xpath);
		}
	}

	private void clickActivityTimeline(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		PrimaryInfoUI ui = new PrimaryInfoUI(driver);

		fc.utobj().printTestStep("Click Activity Timeline");
		fc.utobj().clickElement(driver, ui.ActivityTimeline);
	}

	private void clickHeatMeter(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		PrimaryInfoUI ui = new PrimaryInfoUI(driver);

		fc.utobj().printTestStep("Click Heat Meter");
		fc.utobj().clickElement(driver, ui.HeatMeter);
	}

	public void verifyUnderActivityTimeline(WebDriver driver, String comments) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		PrimaryInfoUI ui = new PrimaryInfoUI(driver);

		clickActivityTimeline(driver);

		fc.utobj().printTestStep("Verify Comments/Remarks under Activity Timeline : " + comments);
		boolean isTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ui.checkValueUnderRightMeter(comments));

		if (isTextPresent == false) {
			fc.utobj().throwsException("Comments/Remarks not found under Activity Timeline");
		}
	}

	public void verifyUnderHeatMeter(WebDriver driver, String comments) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		PrimaryInfoUI ui = new PrimaryInfoUI(driver);

		clickHeatMeter(driver);

		fc.utobj().printTestStep("Verify Comments/Remarks under Right Meter");

		fc.utobj().moveToElement(driver, ui.checkValueUnderRightMeter(comments));
	}

	public void clickCommentsUnderActivityTimeline(WebDriver driver, String comments) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		PrimaryInfoUI ui = new PrimaryInfoUI(driver);

		clickActivityTimeline(driver);

		fc.utobj().printTestStep("Click Comments/Remarks under Right Meter");

		fc.utobj().clickElement(driver, ui.checkValueUnderRightMeter(comments));
	}

	public void verifyCommentsUnderHeatMeter(WebDriver driver, String comments) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		PrimaryInfoUI ui = new PrimaryInfoUI(driver);

		clickHeatMeter(driver);

		fc.utobj().printTestStep("Click Comments/Remarks under Right Meter");

		fc.utobj().clickElement(driver, ui.checkValueUnderRightMeter(comments));
	}

	public void clickMoreActionRightMenu(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		PrimaryInfoUI ui = new PrimaryInfoUI(driver);

		fc.utobj().printTestStep("Click on More Action Menu");

		fc.utobj().clickElement(driver, ui.moreAction_RigthMenu_Xpath);
	}

	public void clickOnPrimaryInfoTab(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		PrimaryInfoUI ui = new PrimaryInfoUI(driver);

		fc.utobj().printTestStep("Click on Primary Info tab");
		fc.utobj().clickElement(driver, ui.PrimaryInfo_tab_link);
	}

	public void clickOnCo_ApplicantTab(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		PrimaryInfoUI ui = new PrimaryInfoUI(driver);

		fc.utobj().printTestStep("Click on Co_Applicant tab");
		fc.utobj().clickElement(driver, ui.CoApplicants_tab_link);
	}

	public void clickOnComplianceTab(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		PrimaryInfoUI ui = new PrimaryInfoUI(driver);

		fc.utobj().printTestStep("Click on Compliance tab");
		fc.utobj().clickElement(driver, ui.Compliance_tab_link);
	}

	public void clickOnDocumentsTab(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		PrimaryInfoUI ui = new PrimaryInfoUI(driver);

		fc.utobj().printTestStep("Click on Documents tab");
		fc.utobj().clickElement(driver, ui.Documents_tab_link);

	}

	public void clickOnPersonalProfileTab(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		PrimaryInfoUI ui = new PrimaryInfoUI(driver);

		fc.utobj().printTestStep("Click on Personal Profile tab");
		fc.utobj().clickElement(driver, ui.PersonalProfile_tab_link);

	}

	public void clickOnQualificationDetailsTab(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		PrimaryInfoUI ui = new PrimaryInfoUI(driver);

		fc.utobj().printTestStep("Click on Qualification Details tab");
		fc.utobj().clickElement(driver, ui.QualificationDetails_tab_link);

	}

	public void clickOnRealEstateTab(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		PrimaryInfoUI ui = new PrimaryInfoUI(driver);

		fc.utobj().printTestStep("Click on Real Estate tab");
		fc.utobj().clickElement(driver, ui.RealEstate_tab_link);

	}

	public void clickOnVirtualBrochureTab(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		PrimaryInfoUI ui = new PrimaryInfoUI(driver);

		fc.utobj().printTestStep("Click on Virtual Brochure tab");
		fc.utobj().clickElement(driver, ui.VirtualBrochure_tab_link);

	}

	public void clickOnVisitTab(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		PrimaryInfoUI ui = new PrimaryInfoUI(driver);

		fc.utobj().printTestStep("Click on Visit tab");
		fc.utobj().clickElement(driver, ui.Visit_tab_link);

	}

	public void clickOnbQualTab(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		PrimaryInfoUI ui = new PrimaryInfoUI(driver);

		fc.utobj().printTestStep("Click on bQual tab");
		fc.utobj().clickElement(driver, ui.bQual_tab_link);

	}

	public void clickOnProvenMatchAssessmentTab(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		PrimaryInfoUI ui = new PrimaryInfoUI(driver);

		fc.utobj().printTestStep("Click on Proven Match Assessment tab");
		fc.utobj().clickElement(driver, ui.ProvenMatchAssessment_tab_link);

	}

	public void clickProvenMatchAssessment_AndClose(WebDriver driver, String successMessage) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		PrimaryInfoUI ui = new PrimaryInfoUI(driver);

		fc.utobj().printTestStep("Click on Proven Match Assessment tab");
		fc.utobj().clickElement(driver, ui.provenMatchLink);
		fc.commonMethods().switch_cboxIframe_frameId(driver);

		if (!fc.utobj().assertPageSource(driver, successMessage)) {
			fc.utobj().throwsException("Proven Match Invite not successful.");
		}
		fc.commonMethods().Click_Close_Input_ByValue(driver);

		fc.utobj().switchFrameToDefault(driver);

		if (fc.utobj().isElementPresent(driver, ui.provenMatchLink) == true) {
			fc.utobj().throwsException("Proven Match Invite Link is still present");
		}
	}

	private void selectStatusInDropDown_OnPrimaryInfoViewPage(WebDriver driver, String status) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		PrimaryInfoUI ui = new PrimaryInfoUI(driver);

		fc.utobj().printTestStep("Select New Status In Status Drop Down");
		fc.utobj().clickElement(driver, ui.LeadStatus_Select);
	}

	public void changeStatusInDropDownOnPrimaryInfoViewPage(WebDriver driver, String status) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		PrimaryInfoUI ui = new PrimaryInfoUI(driver);

		selectStatusInDropDown_OnPrimaryInfoViewPage(driver, status);
		fc.utobj().printTestStep("Click Change Status Button on View Page");
		fc.utobj().clickElement(driver, ui.changeStatus_Button);
	}

	public void clickRemarksandCallHistory_DetailedHistory(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		PrimaryInfoUI ui = new PrimaryInfoUI(driver);

		fc.utobj().printTestStep("Click Remarks and Call History");
		fc.utobj().clickElement(driver, ui.RemarksandCallHistory_link);

	}

	public void clickStatusChangeHistory_DetailedHistory(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		PrimaryInfoUI ui = new PrimaryInfoUI(driver);
		fc.utobj().printTestStep("Click Status Change History");
		fc.utobj().clickElement(driver, ui.StatusChangeHistory_link);
	}

	public void clickOwnerChangeHistory_DetailedHistory(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();

		fc.utobj().printTestStep("Click Remarks and Call History");
	}

	public void clickTasks_DetailedHistory(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();

		fc.utobj().printTestStep("Click Remarks and Call History");
	}

	public void clickActivityHistory_DetailedHistory(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();

		fc.utobj().printTestStep("Click Remarks and Call History");
	}

	public void clickMessageHistory_DetailedHistory(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();

		fc.utobj().printTestStep("Click Remarks and Call History");
	}

	public void SMSHistory_DetailedHistory(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();

		fc.utobj().printTestStep("Click Remarks and Call History");
	}

	public void click_RightActionMenu_Then_ChangeOwner(WebDriver driver) throws Exception {
		clickMoreActions_SelectOption(driver, "Change Owner");
	}

	public void click_RightActionMenu_Then_SendFDDEmail(WebDriver driver) throws Exception {
		clickMoreActions_SelectOption(driver, "Send FDD Email");
	}

	public void click_RightActionMenu_Then_SendVirtualBrochureEmail(WebDriver driver) throws Exception {
		clickMoreActions_SelectOption(driver, "Send Virtual Brochure Email");
	}

	public void click_RightActionMenu_Then_AddToGroup(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		clickMoreActions_SelectOption(driver, "Add To Group");
		fc.commonMethods().switch_cboxIframe_frameId(driver);
	}

	public void click_RightActionMenu_Then_MailMerge(WebDriver driver) throws Exception {
		clickMoreActions_SelectOption(driver, "Mail Merge");
	}

	public void click_RightActionMenu_Then_MoveToInfoMgr(WebDriver driver) throws Exception {
		clickMoreActions_SelectOption(driver, "Move To Info Mgr");
	}

	public void click_RightActionMenu_Then_MoveToOpener(WebDriver driver) throws Exception {
		clickMoreActions_SelectOption(driver, "Move To Opener");
	}

	public void click_RightActionMenu_Then_AddAnotherLead(WebDriver driver) throws Exception {
		clickMoreActions_SelectOption(driver, "Add Another Lead");
	}

	public void click_RightActionMenu_Then_AssociateWithCampaign(WebDriver driver) throws Exception {
		clickMoreActions_SelectOption(driver, "Associate With Campaign");
	}

	public void click_RightActionMenu_Then_ViewDetailedHistory(WebDriver driver) throws Exception {
		clickMoreActions_SelectOption(driver, "View Detailed History");
	}

	public void click_RightActionMenu_Then_ViewinMap(WebDriver driver) throws Exception {
		clickMoreActions_SelectOption(driver, "View in Map");
	}

	public void click_RightActionMenu_Then_KillLead(WebDriver driver) throws Exception {
		clickMoreActions_SelectOption(driver, "Kill Lead");
	}

	public void click_RightActionMenu_Then_DeleteLead(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		clickMoreActions_SelectOption(driver, "Delete Lead");
		fc.commonMethods().switch_cboxIframe_frameId(driver);
		fc.commonMethods().Click_OK_Input_ByValue(driver);

	}

	public void click_RightActionMenu_Then_OptOutfromEmailList(WebDriver driver) throws Exception {
		clickMoreActions_SelectOption(driver, "Opt Out from Email List");
	}

	public void click_RightActionMenu_Then_AssociateCoApplicant(WebDriver driver) throws Exception {
		clickMoreActions_SelectOption(driver, "Associate Co-Applicant");
	}

	public void click_RightActionMenu_Then_ViewTabsonTop(WebDriver driver) throws Exception {
		clickMoreActions_SelectOption(driver, "View Tabs on Top");
	}

	private void clickMoreActions_SelectOption(WebDriver driver, String action) throws Exception {

		clickMoreActionRightMenu(driver);

		FranconnectUtil fc = new FranconnectUtil();
		PrimaryInfoUI ui = new PrimaryInfoUI(driver);
		fc.utobj().printTestStep("Click on More-Actions : " + action);

		for (WebElement el : ui.list_RightActionsMenu) {
			if (action.trim().equalsIgnoreCase(el.getText().trim())) {
				el.click();
				break;
			}
		}
	}

	public void checkGroupName_clickAddToGroupButtonOnGroupWindow(WebDriver driver, Group group) throws Exception {
		LeadManagementTest lm = new LeadManagementTest(driver);
		lm.checkGroupName_clickAddToGroupButtonOnGroupWindow(group);
	}

}
