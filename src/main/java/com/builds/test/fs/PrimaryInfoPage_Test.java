package com.builds.test.fs;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

class PrimaryInfoPage_Test {

	@Test(groups = { "sales","sales_failed" , "Sales_PrimaryInfo_VerifyAllTabs" , "Add_Lead_2018"}) // Fixed : Verified // Akshat 
	@TestCase(createdOn = "2018-01-24", updatedOn = "2018-01-24", testCaseId = "Sales_PrimaryInfo_VerifyAllTabs", testCaseDescription = "Verify submit all tabs")
	private void Sales_PrimaryInfo_VerifyAllTabs() throws Exception {

		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		PrimaryInfo pinfo = new PrimaryInfo();
		Sales sales = new Sales();
		LeadManagementTest lm = new LeadManagementTest(driver);
		Lead ld2 = new Lead();
		Lead ld = new Lead();
		VisitTest visitTest = new VisitTest();
		Visit visit = new Visit();
		RealEstateTest realEstateTest = new RealEstateTest();
		RealEstate realEstate = new RealEstate();
		QualificationDetailsTest qualificationDetailsTest = new QualificationDetailsTest();
		QualificationDetails qualificationDetails = new QualificationDetails();
		CoApplicantsTest coApplicantTest = new CoApplicantsTest();
		CoApplicantWithAdditionLead coApplicant = new CoApplicantWithAdditionLead();
		CoApplicantWithoutSeperateLead additionalContact = new CoApplicantWithoutSeperateLead();
		Compliance compliance = new Compliance();
		ComplianceTest complianceTest = new ComplianceTest();
		PersonalProfile personalProfile = new PersonalProfile();
		PersonalProfileTest personalProfileTest = new PersonalProfileTest();
		Documents documents = new Documents();
		DocumentsTest documentsTest = new DocumentsTest();

		Broker brokers = new Broker();
		BrokersTest brokersTest = new BrokersTest();

		try {

			driver = fc.loginpage().login(driver);
			fc.sales().sales_common().fsModule(driver);
			sales.leadManagement(driver);
			lm.addLeadAndSave(fc.sales().sales_common().fillDefaultValue_LeadDetails(ld2));

			sales.leadManagement(driver);
			lm.addLeadAndSave(fc.sales().sales_common().fillDefaultValue_LeadDetails(ld));

			pinfo.clickOnVisitTab(driver);
			fc.sales().sales_common().fillDefaultValue_Visit_FullInfo(visit);
			visitTest.fillAndSubmitVisit(driver, visit);
			visitTest.validateVisit(driver, visit);

			pinfo.clickOnRealEstateTab(driver);
			fc.sales().sales_common().fillDefaultValue_RealEstate_FullInfo(realEstate);
			realEstateTest.fillAndSubmitRealEstate(driver, realEstate);
			realEstateTest.validateRealEstate(driver, realEstate);

			pinfo.clickOnQualificationDetailsTab(driver);
			fc.sales().sales_common().fillDefaultValue_QualificationDetails_FullInfo(qualificationDetails);
			qualificationDetailsTest.fillAndSubmitQualificationDetails(driver, qualificationDetails);
			// TODO : Qualification Details validation
			pinfo.clickOnPrimaryInfoTab(driver);
			pinfo.verify_TextUnder_ActivityHistory(driver, "Lead Qualification Details Added");

			pinfo.clickOnCo_ApplicantTab(driver);

			fc.sales().sales_common().fillDefaultValue_Coapplicant_AdditionalContactWithoutAddingSeparateLead_FullInfo(
					additionalContact);
			coApplicantTest.fillAndSubmitCoApplicantsWithoutSeperateLead(driver, additionalContact);
			coApplicantTest.verifyCoapplicantWithoutSeparateLead(driver, additionalContact);

			/*
			 * fc.sales().sales_common().
			 * fillDefaultValue_Coapplicant_AdditionalContactWithAdditionOf_ExistingLead
			 * (coApplicant);
			 * coApplicant.setCoApplicantName(ld2.getLeadFullName());
			 * coApplicantTest.fillAndSubmitCoApplicantsWithAddition(driver,
			 * coApplicant);
			 * coApplicantTest.verifyCoapplicantWithAddition(driver,
			 * coApplicant);
			 * 
			 * fc.sales().sales_common().
			 * fillDefaultValue_Coapplicant_AdditionalContactWithAdditionOf_NewLead
			 * (coApplicant);
			 * coApplicantTest.fillAndSubmitCoApplicantsWithAddition(driver,
			 * coApplicant);
			 * coApplicantTest.verifyCoapplicantWithAddition(driver,
			 * coApplicant);
			 */

			pinfo.clickOnComplianceTab(driver);
			fc.sales().sales_common().fillDefaultValue_Compliance_FullInfo(compliance);
			complianceTest.fillAndSubmitComplianceInfo(driver, compliance);
			complianceTest.validateComplianceInfo(driver, compliance);

			pinfo.clickOnPersonalProfileTab(driver);
			fc.sales().sales_common().fillDefaultValue_PersonalProfile_FullInfo(personalProfile);
			personalProfileTest.fillAndSubmitPersonalProfile(driver, personalProfile);
			// TODO : Personal Profile tab validation
			pinfo.clickOnPrimaryInfoTab(driver);
			pinfo.verify_TextUnder_ActivityHistory(driver, "Lead Personal Profile Added");

			pinfo.clickOnDocumentsTab(driver);
			documents.setNumberOfDocuments(4);
			documents.setDocumentTitle("document");
			documents.setUploadDocument(fc.utobj().getFilePathFromTestData("financeDoc.pdf"));
			// documents.setUploadDocument("0.pdf");
			documentsTest.fillDocuments(driver, documents);
			documentsTest.validateDocuments(driver, documents);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales_restructured" })
	@TestCase(createdOn = "2018-01-24", updatedOn = "2018-01-24", testCaseId = "Sales_PrimaryInfo_ProvenMatchInvite_TopLink", testCaseDescription = "Verify Proven Match from Top Link on Primary Info Page.")
	private void Sales_PrimaryInfo_ProvenMatchInvite_TopLink() throws Exception {

		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		PrimaryInfo pinfo = new PrimaryInfo();
		Sales sales = new Sales();
		LeadManagementTest lm = new LeadManagementTest(driver);
		Lead ld = new Lead();
		ConfigureProvenMatchIntegrationTest pm = new ConfigureProvenMatchIntegrationTest();
		ConfigureProvenMatchIntegrationDetailsTest pmDetails = new ConfigureProvenMatchIntegrationDetailsTest();
		AdminSales adminsales = new AdminSales();

		try {

			driver = fc.loginpage().login(driver);

			adminsales.adminConfigureProvenMatchIntegration(driver);
			pm.configureProvenMatchIntegration_Yes_clickSave(driver);

			adminsales.adminConfigureProvenMatchIntegrationDetails(driver);
			pmDetails.configureProvenMatchIntegrationDetails_clickSave(driver, "100059");

			fc.sales().sales_common().fsModule(driver);
			sales.leadManagement(driver);
			lm.addLeadAndSave(fc.sales().sales_common().fillDefaultValue_LeadDetails(ld));
			pinfo.clickProvenMatchAssessment_AndClose(driver,
					"Lead successfully pushed for Proven Match Assessment. Invite email has been sent to the lead to take the assessment.");
			pinfo.verify_TextUnder_ActivityHistory(driver,
					"Lead successfully pushed for Proven Match Assessment. Invite email has been sent to the lead to take the assessment.");
			pinfo.clickOnProvenMatchAssessmentTab(driver);

			// TODO : Proven Match Tab verification is pending.

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales_restructured" })
	@TestCase(createdOn = "2018-01-24", updatedOn = "2018-01-24", testCaseId = "Sales_PrimaryInfo_SendToNathanProfiler_TopLink", testCaseDescription = "Verify Send To Nathan Profiler from Top Link on Primary Info Page.")
	private void Sales_PrimaryInfo_SendToNathanProfiler_TopLink() throws Exception {

		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		Sales sales = new Sales();
		LeadManagementTest lm = new LeadManagementTest(driver);
		Lead ld = new Lead();
		AdminSales adminsales = new AdminSales();

		try {

			driver = fc.loginpage().login(driver);

			adminsales.adminConfigureNathanProfilerIntegration(driver);

			// TODO : Configuration Pending

			// TODO : Test Pending

			fc.sales().sales_common().fsModule(driver);
			sales.leadManagement(driver);
			lm.addLeadAndSave(fc.sales().sales_common().fillDefaultValue_LeadDetails(ld));

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales_restructured" })
	@TestCase(createdOn = "2018-01-24", updatedOn = "2018-01-24", testCaseId = "Sales_PrimaryInfo_Modify_TopLink", testCaseDescription = "Verify Modify Lead from Top Link on Primary Info Page.")
	private void Sales_PrimaryInfo_Modify_TopLink() throws Exception {

		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		Sales sales = new Sales();
		LeadManagementTest lm = new LeadManagementTest(driver);
		Lead ld = new Lead();

		try {

			driver = fc.loginpage().login(driver);
			fc.sales().sales_common().fsModule(driver);
			sales.leadManagement(driver);
			lm.addLeadAndSave(fc.sales().sales_common().fillDefaultValue_LeadDetails(ld));

			// TODO: Modify method pending

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales_restructured" })
	@TestCase(createdOn = "2018-01-24", updatedOn = "2018-01-24", testCaseId = "Sales_PrimaryInfo_SendEmail_TopLink", testCaseDescription = "Verify Send Email from Top Link on Primary Info Page.")
	private void Sales_PrimaryInfo_SendEmail_TopLink() throws Exception {

		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		Sales sales = new Sales();
		LeadManagementTest lm = new LeadManagementTest(driver);
		Lead ld = new Lead();

		try {

			driver = fc.loginpage().login(driver);
			fc.sales().sales_common().fsModule(driver);
			sales.leadManagement(driver);
			lm.addLeadAndSave(fc.sales().sales_common().fillDefaultValue_LeadDetails(ld));

			// TODO : Mail part pending

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales_restructured", "sales" })
	@TestCase(createdOn = "2018-01-24", updatedOn = "2018-01-24", testCaseId = "Sales_PrimaryInfo_AddRemarks", testCaseDescription = "Verify Add Remarks from Top Link on Primary Info Page.")
	private void Sales_PrimaryInfo_AddRemarks() throws Exception {

		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		PrimaryInfo pinfo = new PrimaryInfo();
		Sales sales = new Sales();
		LeadManagementTest lm = new LeadManagementTest(driver);
		Remark remark = new Remark();
		Remark remark2 = new Remark();
		RemarkTest remarktest = new RemarkTest();
		Lead ld = new Lead();

		try {

			driver = fc.loginpage().login(driver);
			fc.sales().sales_common().fsModule(driver);
			sales.leadManagement(driver);
			lm.addLeadAndSave(fc.sales().sales_common().fillDefaultValue_LeadDetails(ld));
			remark = fc.sales().sales_common().fillDefaultValue_Remark(remark);
			remark2 = fc.sales().sales_common().fillDefaultValue_Remark(remark2);

			pinfo.click_AddRemarks_TopLink(driver);
			remarktest.fill_Remarks_And_Submit(driver, remark);
			pinfo.verify_TextUnder_ActivityHistory(driver, remark.getRemark());
			pinfo.verify_RemarksAndCallHistory_In_DetailedHistory(driver, remark.getRemark());

			pinfo.click_AddRemarks_ActivityHistory(driver);
			remarktest.fill_Remarks_And_Submit(driver, remark2);
			pinfo.verify_TextUnder_ActivityHistory(driver, remark2.getRemark());
			pinfo.verify_RemarksAndCallHistory_In_DetailedHistory(driver, remark2.getRemark());

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales_restructured", "sales" })
	@TestCase(createdOn = "2018-01-24", updatedOn = "2018-01-24", testCaseId = "Sales_PrimaryInfo_AddLeadBasicVerification", testCaseDescription = "Verify Basic Details After adding a lead.")
	private void Sales_PrimaryInfo_AddLeadBasicVerification() throws Exception {

		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		PrimaryInfo pinfo = new PrimaryInfo();
		Sales sales = new Sales();
		LeadManagementTest lm = new LeadManagementTest(driver);
		Lead ld = new Lead();
		Lead ld2 = new Lead();
		LeadTest lt = new LeadTest();

		try {

			driver = fc.loginpage().login(driver);
			fc.sales().sales_common().fsModule(driver);
			sales.leadManagement(driver);
			lm.addLeadAndSave(fc.sales().sales_common().fillDefaultValue_LeadDetails(ld));
			pinfo.verifyUnderActivityTimeline(driver, "Lead added through FranConnect Application");
			pinfo.verify_TextUnder_ActivityHistory(driver, "Lead added through FranConnect Application");

			pinfo.verify_RemarksAndCallHistory_In_DetailedHistory(driver, "Lead added through FranConnect Application");

			pinfo.click_Modify_TopLink(driver);

			// TODO : Change as per modify lead new method

			ld2.setEmail(fc.utobj().generateTestData("e") + "@gmail.com");
			lt.fillLeadInfo(driver, ld2);
			pinfo.clickSave(driver, ld2);

			pinfo.verify_TextUnder_ActivityHistory(driver, "Email has been changed from " + ld.getEmail() + " to "
					+ ld2.getEmail() + " through FranConnect Application.");

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales_restructured", "sales" })
	@TestCase(createdOn = "2018-01-29", updatedOn = "2018-01-29", testCaseId = "Sales_PrimaryInfo_AddLead_VerifyEmail_IsMandatory_001", testCaseDescription = "Verify lead is not getting added without email,\nwhen Campaign is selected for : Based on Workflow Assignment Rules")
	private void Sales_PrimaryInfo_AddLead_VerifyEmail_IsMandatory_001() throws Exception {

		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		Sales sales = new Sales();
		LeadManagementTest lm = new LeadManagementTest(driver);
		Lead ld = new Lead();

		try {

			driver = fc.loginpage().login(driver);
			fc.sales().sales_common().fsModule(driver);
			sales.leadManagement(driver);

			ld = fc.sales().sales_common().fillDefaultValue_LeadDetails(ld);
			ld.setEmail("");
			ld.setCampaignName("Based on Workflow Assignment Rules");
			lm.addLeadAndSave(ld);

			if (!"Please enter the valid Email if you want to include this lead in mailing Campaign."
					.equalsIgnoreCase(fc.utobj().acceptAlertBox(driver))) {
				fc.utobj().throwsException(
						"Email should be mandatory when selecting 'Based on Workflow Assignment Rules'");
			}

			// TODO : Change as per modify lead new method

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales_restructured", "sales", "sales_failed" , "Sales_PrimaryInfo_AddLead_VerifyEmail_IsMandatory_002" }) //
	@TestCase(createdOn = "2018-01-29", updatedOn = "2018-01-29", testCaseId = "Sales_PrimaryInfo_AddLead_VerifyEmail_IsMandatory_002", testCaseDescription = "Verify email is mandatory when lead is added and\nCampaign is Selected at the time of lead addition")
	private void Sales_PrimaryInfo_AddLead_VerifyEmail_IsMandatory_002() throws Exception {

		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		Sales sales = new Sales();
		LeadManagementTest lm = new LeadManagementTest(driver);
		Lead ld = new Lead();
		CampaignCenterTest campCenTest = new CampaignCenterTest();
		CampaignTest campTest = new CampaignTest();
		Campaign campaign = new Campaign();
		CreateTemplateTest createTemplate = new CreateTemplateTest();
		SelectRecipientsTest recipient = new SelectRecipientsTest();
		LaunchCampaignTest launchCampaign = new LaunchCampaignTest();

		try {

			driver = fc.loginpage().login(driver);
			fc.sales().sales_common().fsModule(driver);

			sales.emailCampaignTab(driver);
			campCenTest.clickCreateAndCreateCampaign_Link(driver);

			campTest.fillCampaignDetailsAndClickStart(driver,fc.sales().sales_common().fillDefaultValue_Campaign(campaign));
			createTemplate.clickContinueButton(driver);
			recipient.clickAssociateLater(driver);
			launchCampaign.clickLaunchCampaign_Button(driver);

			sales.leadManagement(driver);
			ld = fc.sales().sales_common().fillDefaultValue_LeadDetails(ld);
			ld.setEmail("");
			ld.setCampaignName(campaign.getCampaignName());
			lm.addLeadAndSave(ld);

			if (! (fc.utobj().acceptAlertBox(driver).toLowerCase().contains("Please enter the valid Email if you want to include this lead in mailing Campaign".toLowerCase())) ) 
			{
				fc.utobj().throwsException("Email should be mandatory when selecting 'Campaign Name'");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales_restructured" })
	@TestCase(createdOn = "2018-01-25", updatedOn = "2018-01-25", testCaseId = "Sales_PrimaryInfo_LogACall_TopLink", testCaseDescription = "Verify Log A Call at Primary Info - Top link")
	private void Sales_PrimaryInfo_LogACall_TopLink() throws Exception {

		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		PrimaryInfo pinfo = new PrimaryInfo();
		Sales sales = new Sales();
		LeadManagementTest lm = new LeadManagementTest(driver);
		Lead ld = new Lead();
		Call call = new Call();
		CallTest calltest = new CallTest();

		try {

			driver = fc.loginpage().login(driver);
			fc.sales().sales_common().fsModule(driver);
			sales.leadManagement(driver);
			lm.addLeadAndSave(fc.sales().sales_common().fillDefaultValue_LeadDetails(ld));

			pinfo.click_Log_A_Call_TopLink(driver);
			calltest.fill_And_Add_Call_Details_ScheduleTask_No(driver,
					fc.sales().sales_common().fillDefaultValue_CallDetails(call));

			pinfo.verifyUnderActivityTimeline(driver, call.getSubject());
			pinfo.verify_TextUnder_ActivityHistory(driver, call.getSubject());

			// TODO : Verify CAll details after modify.

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales_restructured", "sales","sales_failed" })
	@TestCase(createdOn = "2018-01-25", updatedOn = "2018-01-25", testCaseId = "Sales_PrimaryInfo_LogATask_TopLink", testCaseDescription = "Verify Log A Task at Primary Info - Top link")
	private void Sales_PrimaryInfo_LogATask_TopLink() throws Exception {

		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		PrimaryInfo pinfo = new PrimaryInfo();
		Sales sales = new Sales();
		LeadManagementTest lm = new LeadManagementTest(driver);
		Lead ld = new Lead();
		Task task = new Task();
		TaskTest tasktest = new TaskTest();

		try {

			driver = fc.loginpage().login(driver);
			fc.sales().sales_common().fsModule(driver);
			sales.leadManagement(driver);
			lm.addLeadAndSave(fc.sales().sales_common().fillDefaultValue_LeadDetails(ld));

			pinfo.click_Log_A_Task_TopLink(driver);
			tasktest.fillTaskAndClickCreate(driver, fc.sales().sales_common().fillDefaultValue_TaskDetails(task));

			pinfo.verifyTaskInfo_ActivityHistory(driver, task);

			// TODO : Verify Task details after modify.

			// TODO : Verify the email sent to the lead owner and notification

			// TODO : Verify task is Added to calendar

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales_restructured", "sales" })
	@TestCase(createdOn = "2018-01-25", updatedOn = "2018-01-25", testCaseId = "Sales_PrimaryInfo_LogATask_TimeLess_TopLink", testCaseDescription = "Verify Log A Task (Timeless) at Primary Info - Top link")
	private void Sales_PrimaryInfo_LogATask_TimeLess_TopLink() throws Exception {

		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		PrimaryInfo pinfo = new PrimaryInfo();
		Sales sales = new Sales();
		LeadManagementTest lm = new LeadManagementTest(driver);
		Lead ld = new Lead();
		Task task = new Task();
		TaskTest tasktest = new TaskTest();

		try {

			driver = fc.loginpage().login(driver);
			fc.sales().sales_common().fsModule(driver);
			sales.leadManagement(driver);
			lm.addLeadAndSave(fc.sales().sales_common().fillDefaultValue_LeadDetails(ld));

			pinfo.click_Log_A_Task_TopLink(driver);

			task = fc.sales().sales_common().fillDefaultValue_TaskDetails(task);
			task.setTimelessTask("yes");
			tasktest.fillTaskAndClickCreate(driver, task);

			pinfo.verifyTaskInfo_ActivityHistory(driver, task);

			// TODO : Verify Task details after modify.

			// TODO : Verify the email sent to the lead owner and notification

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales_restructured" })
	@TestCase(createdOn = "2018-01-25", updatedOn = "2018-01-25", testCaseId = "Sales_PrimaryInfo_ChangeOwner_MoreAction", testCaseDescription = "Verify Change Owner from More Action")
	private void Sales_PrimaryInfo_ChangeOwner_MoreAction() throws Exception {

		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		Sales sales = new Sales();
		LeadManagementTest lm = new LeadManagementTest(driver);
		Lead ld = new Lead();

		try {

			driver = fc.loginpage().login(driver);
			fc.sales().sales_common().fsModule(driver);
			sales.leadManagement(driver);
			lm.addLeadAndSave(fc.sales().sales_common().fillDefaultValue_LeadDetails(ld));

			// TODO : Create Owner

			// TODO : Verify the email sent to the lead owner and notification

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales_restructured" })
	@TestCase(createdOn = "2018-01-25", updatedOn = "2018-01-25", testCaseId = "Sales_PrimaryInfo_SendSMSOptInRequest_MoreAction", testCaseDescription = "Verify Send SMS OptIn Request from More Action")
	private void Sales_PrimaryInfo_SendSMSOptInRequest_MoreAction() throws Exception {

		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		Sales sales = new Sales();
		LeadManagementTest lm = new LeadManagementTest(driver);
		Lead ld = new Lead();

		try {

			driver = fc.loginpage().login(driver);
			fc.sales().sales_common().fsModule(driver);
			sales.leadManagement(driver);
			lm.addLeadAndSave(fc.sales().sales_common().fillDefaultValue_LeadDetails(ld));

			// TODO : Verify Task details after modify.

			// TODO : Verify the email sent to the lead owner and notification

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales_restructured" })
	@TestCase(createdOn = "2018-01-25", updatedOn = "2018-01-25", testCaseId = "Sales_PrimaryInfo_SendFDDEmail_MoreAction", testCaseDescription = "Verify Send FDD Email from More Action")
	private void Sales_PrimaryInfo_SendFDDEmail_MoreAction() throws Exception {

		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		Sales sales = new Sales();
		LeadManagementTest lm = new LeadManagementTest(driver);
		Lead ld = new Lead();

		try {

			driver = fc.loginpage().login(driver);
			fc.sales().sales_common().fsModule(driver);
			sales.leadManagement(driver);
			lm.addLeadAndSave(fc.sales().sales_common().fillDefaultValue_LeadDetails(ld));

			// TODO : Verify Task details after modify.

			// TODO : Verify the email sent to the lead owner and notification

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales_restructured" })
	@TestCase(createdOn = "2018-01-25", updatedOn = "2018-01-25", testCaseId = "Sales_PrimaryInfo_SendVirtualBrochureEmail_MoreAction", testCaseDescription = "Verify Send Virtual Brochure Email from More Action")
	private void Sales_PrimaryInfo_SendVirtualBrochureEmail_MoreAction() throws Exception {

		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		Sales sales = new Sales();
		LeadManagementTest lm = new LeadManagementTest(driver);
		Lead ld = new Lead();

		try {

			driver = fc.loginpage().login(driver);
			fc.sales().sales_common().fsModule(driver);
			sales.leadManagement(driver);
			lm.addLeadAndSave(fc.sales().sales_common().fillDefaultValue_LeadDetails(ld));

			// TODO : Verify Task details after modify.

			// TODO : Verify the email sent to the lead owner and notification

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales", "sales_restructured" })
	@TestCase(createdOn = "2018-01-25", updatedOn = "2018-01-25", testCaseId = "Sales_PrimaryInfo_AddToGroup_MoreAction", testCaseDescription = "Verify Add To Group from More Action")
	private void Sales_PrimaryInfo_AddToGroup_MoreAction() throws Exception {

		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		Sales sales = new Sales();
		LeadManagementTest lm = new LeadManagementTest(driver);
		Lead ld = new Lead();
		PrimaryInfo pinfo = new PrimaryInfo();
		Group group = new Group();
		GroupTest groupTest = new GroupTest();

		try {

			driver = fc.loginpage().login(driver);
			fc.sales().sales_common().fsModule(driver);

			sales.groups(driver);
			group = fc.sales().sales_common().fillDefaultValue_RegularGroupDetails(group);
			groupTest.addGroupDetails_AndCreate(driver, group);

			sales.leadManagement(driver);
			lm.addLeadAndSave(fc.sales().sales_common().fillDefaultValue_LeadDetails(ld));
			pinfo.click_RightActionMenu_Then_AddToGroup(driver);
			pinfo.checkGroupName_clickAddToGroupButtonOnGroupWindow(driver, group);
			if (fc.utobj().assertPageSource(driver, group.getName()) == false) {
				fc.utobj().throwsException("Group not associated to the lead.");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales_restructured" })
	@TestCase(createdOn = "2018-01-25", updatedOn = "2018-01-25", testCaseId = "Sales_PrimaryInfo_MailMerge_MoreAction", testCaseDescription = "Verify Mail Merge from More Action")
	private void Sales_PrimaryInfo_MailMerge_MoreAction() throws Exception {

		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		Sales sales = new Sales();
		LeadManagementTest lm = new LeadManagementTest(driver);
		Lead ld = new Lead();

		try {

			driver = fc.loginpage().login(driver);
			fc.sales().sales_common().fsModule(driver);
			sales.leadManagement(driver);
			lm.addLeadAndSave(fc.sales().sales_common().fillDefaultValue_LeadDetails(ld));

			// TODO : Verify Task details after modify.

			// TODO : Verify the email sent to the lead owner and notification

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales_restructured" })
	@TestCase(createdOn = "2018-01-25", updatedOn = "2018-01-25", testCaseId = "Sales_PrimaryInfo_MoveToInfoMgr_MoreAction", testCaseDescription = "Verify Move To Info Mgr from More Action")
	private void Sales_PrimaryInfo_MoveToInfoMgr_MoreAction() throws Exception {

		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		Sales sales = new Sales();
		LeadManagementTest lm = new LeadManagementTest(driver);
		Lead ld = new Lead();

		try {

			driver = fc.loginpage().login(driver);
			fc.sales().sales_common().fsModule(driver);
			sales.leadManagement(driver);
			lm.addLeadAndSave(fc.sales().sales_common().fillDefaultValue_LeadDetails(ld));

			// TODO : Verify Task details after modify.

			// TODO : Verify the email sent to the lead owner and notification

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales_restructured" })
	@TestCase(createdOn = "2018-01-25", updatedOn = "2018-01-25", testCaseId = "Sales_PrimaryInfo_MoveToOpener_MoreAction", testCaseDescription = "Verify Move To Opener from More Action")
	private void Sales_PrimaryInfo_MoveToOpener_MoreAction() throws Exception {

		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		Sales sales = new Sales();
		LeadManagementTest lm = new LeadManagementTest(driver);
		Lead ld = new Lead();

		try {

			driver = fc.loginpage().login(driver);
			fc.sales().sales_common().fsModule(driver);
			sales.leadManagement(driver);
			lm.addLeadAndSave(fc.sales().sales_common().fillDefaultValue_LeadDetails(ld));

			// TODO : Verify Task details after modify.

			// TODO : Verify the email sent to the lead owner and notification

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales_restructured" })
	@TestCase(createdOn = "2018-01-25", updatedOn = "2018-01-25", testCaseId = "Sales_PrimaryInfo_AddAnotherLead_MoreAction", testCaseDescription = "Verify Add Another Lead from More Action")
	private void Sales_PrimaryInfo_AddAnotherLead_MoreAction() throws Exception {

		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		Sales sales = new Sales();
		LeadManagementTest lm = new LeadManagementTest(driver);
		Lead ld = new Lead();

		try {

			driver = fc.loginpage().login(driver);
			fc.sales().sales_common().fsModule(driver);
			sales.leadManagement(driver);
			lm.addLeadAndSave(fc.sales().sales_common().fillDefaultValue_LeadDetails(ld));

			// TODO : Verify Task details after modify.

			// TODO : Verify the email sent to the lead owner and notification

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales_restructured" })
	@TestCase(createdOn = "2018-01-25", updatedOn = "2018-01-25", testCaseId = "Sales_PrimaryInfo_AssociateWithCampaign_MoreAction", testCaseDescription = "Verify Associate With Campaign from More Action")
	private void Sales_PrimaryInfo_AssociateWithCampaign_MoreAction() throws Exception {

		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		Sales sales = new Sales();
		LeadManagementTest lm = new LeadManagementTest(driver);
		Lead ld = new Lead();

		try {

			driver = fc.loginpage().login(driver);
			fc.sales().sales_common().fsModule(driver);
			sales.leadManagement(driver);
			lm.addLeadAndSave(fc.sales().sales_common().fillDefaultValue_LeadDetails(ld));

			// TODO : Verify Task details after modify.

			// TODO : Verify the email sent to the lead owner and notification

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales_restructured" })
	@TestCase(createdOn = "2018-01-25", updatedOn = "2018-01-25", testCaseId = "Sales_PrimaryInfo_ViewDetailedHistory_MoreAction", testCaseDescription = "Verify View Detailed History from More Action")
	private void Sales_PrimaryInfo_ViewDetailedHistory_MoreAction() throws Exception {

		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		Sales sales = new Sales();
		LeadManagementTest lm = new LeadManagementTest(driver);
		Lead ld = new Lead();

		try {

			driver = fc.loginpage().login(driver);
			fc.sales().sales_common().fsModule(driver);
			sales.leadManagement(driver);
			lm.addLeadAndSave(fc.sales().sales_common().fillDefaultValue_LeadDetails(ld));

			// TODO : Verify Task details after modify.

			// TODO : Verify the email sent to the lead owner and notification

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales_restructured" })
	@TestCase(createdOn = "2018-01-25", updatedOn = "2018-01-25", testCaseId = "Sales_PrimaryInfo_ViewInMap_MoreAction", testCaseDescription = "Verify View In Map from More Action")
	private void Sales_PrimaryInfo_ViewInMap_MoreAction() throws Exception {

		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		Sales sales = new Sales();
		LeadManagementTest lm = new LeadManagementTest(driver);
		Lead ld = new Lead();

		try {

			driver = fc.loginpage().login(driver);
			fc.sales().sales_common().fsModule(driver);
			sales.leadManagement(driver);
			lm.addLeadAndSave(fc.sales().sales_common().fillDefaultValue_LeadDetails(ld));

			// TODO : Verify Task details after modify.

			// TODO : Verify the email sent to the lead owner and notification

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales_restructured" })
	@TestCase(createdOn = "2018-01-25", updatedOn = "2018-01-25", testCaseId = "Sales_PrimaryInfo_KillLead_MoreAction", testCaseDescription = "Verify Kill Lead from More Action")
	private void Sales_PrimaryInfo_KillLead_MoreAction() throws Exception {

		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		Sales sales = new Sales();
		LeadManagementTest lm = new LeadManagementTest(driver);
		Lead ld = new Lead();

		try {

			driver = fc.loginpage().login(driver);
			fc.sales().sales_common().fsModule(driver);
			sales.leadManagement(driver);
			lm.addLeadAndSave(fc.sales().sales_common().fillDefaultValue_LeadDetails(ld));

			// TODO : Verify Task details after modify.

			// TODO : Verify the email sent to the lead owner and notification

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales", "sales_restructured" })
	@TestCase(createdOn = "2018-01-25", updatedOn = "2018-01-25", testCaseId = "Sales_PrimaryInfo_DeleteLead_MoreAction", testCaseDescription = "Verify Delete Lead from More Action")
	private void Sales_PrimaryInfo_DeleteLead_MoreAction() throws Exception {

		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		Sales sales = new Sales();
		LeadManagementTest lm = new LeadManagementTest(driver);
		Lead ld = new Lead();
		PrimaryInfo pinfo = new PrimaryInfo();
		SearchTest searchTest = new SearchTest();
		Search search = new Search();

		try {

			driver = fc.loginpage().login(driver);
			fc.sales().sales_common().fsModule(driver);
			sales.leadManagement(driver);
			lm.addLeadAndSave(fc.sales().sales_common().fillDefaultValue_LeadDetails(ld));
			pinfo.click_RightActionMenu_Then_DeleteLead(driver);

			sales.search(driver);
			search.setFirstName(ld.getFirstName());
			search.setLastName(ld.getLastName());
			searchTest.fill_And_Search_PrimaryInfoBasedSearch(driver, search);

			if (fc.utobj().assertPageSource(driver, ld.getLeadFullName()) == true) {
				fc.utobj().throwsException("Lead still present after deletion (On lead summary page)");
			}

			// TODO : Check in deleted logs

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales_restructured" })
	@TestCase(createdOn = "2018-01-25", updatedOn = "2018-01-25", testCaseId = "Sales_PrimaryInfo_OptOutFromEmailList_MoreAction", testCaseDescription = "Verify OptOut From Email List from More Action")
	private void Sales_PrimaryInfo_OptOutFromEmailList_MoreAction() throws Exception {

		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		Sales sales = new Sales();
		LeadManagementTest lm = new LeadManagementTest(driver);
		Lead ld = new Lead();

		try {

			driver = fc.loginpage().login(driver);
			fc.sales().sales_common().fsModule(driver);
			sales.leadManagement(driver);
			lm.addLeadAndSave(fc.sales().sales_common().fillDefaultValue_LeadDetails(ld));

			// TODO : Verify Task details after modify.

			// TODO : Verify the email sent to the lead owner and notification

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales_restructured" })
	@TestCase(createdOn = "2018-01-25", updatedOn = "2018-01-25", testCaseId = "Sales_PrimaryInfo_AssociateCo_Applicant_MoreAction", testCaseDescription = "Verify Associate Co-Applicant from More Action")
	private void Sales_PrimaryInfo_AssociateCo_Applicant_MoreAction() throws Exception {

		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		Sales sales = new Sales();
		LeadManagementTest lm = new LeadManagementTest(driver);
		Lead ld = new Lead();

		try {

			driver = fc.loginpage().login(driver);
			fc.sales().sales_common().fsModule(driver);
			sales.leadManagement(driver);
			lm.addLeadAndSave(fc.sales().sales_common().fillDefaultValue_LeadDetails(ld));

			// TODO : Verify Task details after modify.

			// TODO : Verify the email sent to the lead owner and notification

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales_restructured" })
	@TestCase(createdOn = "2018-01-25", updatedOn = "2018-01-25", testCaseId = "Sales_PrimaryInfo_ViewTabsOnTop_MoreAction", testCaseDescription = "Verify View Tabs On Tops from More Action")
	private void Sales_PrimaryInfo_ViewTabsOnTop_MoreAction() throws Exception {

		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		Sales sales = new Sales();
		LeadManagementTest lm = new LeadManagementTest(driver);
		Lead ld = new Lead();

		try {

			driver = fc.loginpage().login(driver);
			fc.sales().sales_common().fsModule(driver);
			sales.leadManagement(driver);
			lm.addLeadAndSave(fc.sales().sales_common().fillDefaultValue_LeadDetails(ld));

			// TODO : Verify Task details after modify.

			// TODO : Verify the email sent to the lead owner and notification

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales_restructured" })
	@TestCase(createdOn = "2018-01-25", updatedOn = "2018-01-25", testCaseId = "Sales_PrimaryInfo_ChangeStatus_ThroughButton", testCaseDescription = "Verify Change Status Through Button")
	private void Sales_PrimaryInfo_ChangeStatus_ThroughButton() throws Exception {

		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		Sales sales = new Sales();
		LeadManagementTest lm = new LeadManagementTest(driver);
		Lead ld = new Lead();
		PrimaryInfo pinfo = new PrimaryInfo();
		String changedLeadStatus = "Closed Lead";

		try {

			driver = fc.loginpage().login(driver);
			fc.sales().sales_common().fsModule(driver);
			sales.leadManagement(driver);
			lm.addLeadAndSave(fc.sales().sales_common().fillDefaultValue_LeadDetails(ld));
			pinfo.changeStatusInDropDownOnPrimaryInfoViewPage(driver, changedLeadStatus);
			pinfo.verify_TextUnder_ActivityHistory(driver, "Lead Status has been changed to " + changedLeadStatus);
			pinfo.click_DetailedHistory_ActivityHistory(driver);

			// TODO : Check Detailed History

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales_restructured" })
	@TestCase(createdOn = "2018-01-25", updatedOn = "2018-01-25", testCaseId = "Sales_PrimaryInfo_ChangeOwner_ThroughButton", testCaseDescription = "Verify Change Owner Through Button")
	private void Sales_PrimaryInfo_ChangeOwner_ThroughButton() throws Exception {

		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		Sales sales = new Sales();
		LeadManagementTest lm = new LeadManagementTest(driver);
		Lead ld = new Lead();

		try {

			driver = fc.loginpage().login(driver);
			fc.sales().sales_common().fsModule(driver);
			sales.leadManagement(driver);
			lm.addLeadAndSave(fc.sales().sales_common().fillDefaultValue_LeadDetails(ld));

			// TODO : Verify Task details after modify.

			// TODO : Verify the email sent to the lead owner and notification

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales_restructured" })
	@TestCase(createdOn = "2018-01-25", updatedOn = "2018-01-25", testCaseId = "Sales_PrimaryInfo_SendFDDEmail_ThroughButton", testCaseDescription = "Verify Send FDD Email Through Button")
	private void Sales_PrimaryInfo_SendFDDEmail_ThroughButton() throws Exception {

		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		Sales sales = new Sales();
		LeadManagementTest lm = new LeadManagementTest(driver);
		Lead ld = new Lead();

		try {

			driver = fc.loginpage().login(driver);
			fc.sales().sales_common().fsModule(driver);
			sales.leadManagement(driver);
			lm.addLeadAndSave(fc.sales().sales_common().fillDefaultValue_LeadDetails(ld));

			// TODO : Verify Task details after modify.

			// TODO : Verify the email sent to the lead owner and notification

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales_restructured" })
	@TestCase(createdOn = "2018-01-25", updatedOn = "2018-01-25", testCaseId = "Sales_PrimaryInfo_AddToGroup_ThroughButton", testCaseDescription = "Verify Add To Group Through Button")
	private void Sales_PrimaryInfo_AddToGroup_ThroughButton() throws Exception {

		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		Sales sales = new Sales();
		LeadManagementTest lm = new LeadManagementTest(driver);
		Lead ld = new Lead();

		try {

			driver = fc.loginpage().login(driver);
			fc.sales().sales_common().fsModule(driver);
			sales.leadManagement(driver);
			lm.addLeadAndSave(fc.sales().sales_common().fillDefaultValue_LeadDetails(ld));

			// TODO : Verify Task details after modify.

			// TODO : Verify the email sent to the lead owner and notification

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales_restructured" })
	@TestCase(createdOn = "2018-01-25", updatedOn = "2018-01-25", testCaseId = "Sales_PrimaryInfo_MailMerge_ThroughButton", testCaseDescription = "Verify Mail Merge Through Button")
	private void Sales_PrimaryInfo_MailMerge_ThroughButton() throws Exception {

		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		Sales sales = new Sales();
		LeadManagementTest lm = new LeadManagementTest(driver);
		Lead ld = new Lead();

		try {

			driver = fc.loginpage().login(driver);
			fc.sales().sales_common().fsModule(driver);
			sales.leadManagement(driver);
			lm.addLeadAndSave(fc.sales().sales_common().fillDefaultValue_LeadDetails(ld));

			// TODO : Verify Task details after modify.

			// TODO : Verify the email sent to the lead owner and notification

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales_restructured" })
	@TestCase(createdOn = "2018-01-25", updatedOn = "2018-01-25", testCaseId = "Sales_PrimaryInfo_MoveToInfoMgr_ThroughButton", testCaseDescription = "Verify Move To Info Mgr Through Button")
	private void Sales_PrimaryInfo_MoveToInfoMgr_ThroughButton() throws Exception {

		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		Sales sales = new Sales();
		LeadManagementTest lm = new LeadManagementTest(driver);
		Lead ld = new Lead();

		try {

			driver = fc.loginpage().login(driver);
			fc.sales().sales_common().fsModule(driver);
			sales.leadManagement(driver);
			lm.addLeadAndSave(fc.sales().sales_common().fillDefaultValue_LeadDetails(ld));

			// TODO : Verify Task details after modify.

			// TODO : Verify the email sent to the lead owner and notification

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales_restructured" })
	@TestCase(createdOn = "2018-01-25", updatedOn = "2018-01-25", testCaseId = "Sales_PrimaryInfo_MoveToOpener_ThroughButton", testCaseDescription = "Verify Move To Opener Through Button")
	private void Sales_PrimaryInfo_MoveToOpener_ThroughButton() throws Exception {

		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		Sales sales = new Sales();
		LeadManagementTest lm = new LeadManagementTest(driver);
		Lead ld = new Lead();

		try {

			driver = fc.loginpage().login(driver);
			fc.sales().sales_common().fsModule(driver);
			sales.leadManagement(driver);
			lm.addLeadAndSave(fc.sales().sales_common().fillDefaultValue_LeadDetails(ld));

			// TODO : Verify Task details after modify.

			// TODO : Verify the email sent to the lead owner and notification

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales_restructured" })
	@TestCase(createdOn = "2018-01-25", updatedOn = "2018-01-25", testCaseId = "Sales_PrimaryInfo_AddLead_ThroughButton", testCaseDescription = "Verify Add Lead Through Button")
	private void Sales_PrimaryInfo_AddLead_ThroughButton() throws Exception {

		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		Sales sales = new Sales();
		LeadManagementTest lm = new LeadManagementTest(driver);
		Lead ld = new Lead();

		try {

			driver = fc.loginpage().login(driver);
			fc.sales().sales_common().fsModule(driver);
			sales.leadManagement(driver);
			lm.addLeadAndSave(fc.sales().sales_common().fillDefaultValue_LeadDetails(ld));

			// TODO : Verify Task details after modify.

			// TODO : Verify the email sent to the lead owner and notification

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales_restructured" })
	@TestCase(createdOn = "2018-01-25", updatedOn = "2018-01-25", testCaseId = "Sales_PrimaryInfo_Print_ThroughButton", testCaseDescription = "Verify Print Through Button")
	private void Sales_PrimaryInfo_Print_ThroughButton() throws Exception {

		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		Sales sales = new Sales();
		LeadManagementTest lm = new LeadManagementTest(driver);
		Lead ld = new Lead();

		try {

			driver = fc.loginpage().login(driver);
			fc.sales().sales_common().fsModule(driver);
			sales.leadManagement(driver);
			lm.addLeadAndSave(fc.sales().sales_common().fillDefaultValue_LeadDetails(ld));

			// TODO : Verify Task details after modify.

			// TODO : Verify the email sent to the lead owner and notification

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

}
