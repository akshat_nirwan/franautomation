package com.builds.test.fs;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.fs.QualificationDetailsUI;
import com.builds.utilities.FranconnectUtil;

public class QualificationDetailsTest {

	public void fillAndSubmitQualificationDetails(WebDriver driver, QualificationDetails qd) throws Exception {
		QualificationDetailsUI qdui = new QualificationDetailsUI(driver);
		FranconnectUtil fc = new FranconnectUtil();

		fc.utobj().printTestStep("Fill Qualification Details Info");

		if (qd.getDate() != null) {
			fc.utobj().sendKeys(driver, qdui.Date, qd.getDate());
		}

		if (qd.getName() != null) {
			fc.utobj().sendKeys(driver, qdui.Name, qd.getName());
		}

		if (qd.getGender() != null) {
			if (qd.getGender().equalsIgnoreCase("Male")) {
				fc.utobj().clickElement(driver, qdui.GenderMale);
			} else if (qd.getGender().equalsIgnoreCase("Female")) {
				fc.utobj().clickElement(driver, qdui.GenderFemale);
			}
		}

		if (qd.getPresentAddress() != null) {
			fc.utobj().sendKeys(driver, qdui.PresentAddress, qd.getPresentAddress());
		}

		if (qd.getYearsAtThisAddress() != null) {
			fc.utobj().sendKeys(driver, qdui.HowManyYearsAtThisAddress, qd.getYearsAtThisAddress());
		}

		if (qd.getCity() != null) {
			fc.utobj().sendKeys(driver, qdui.City, qd.getCity());
		}

		if (qd.getCountry() != null) {
			fc.utobj().selectDropDownByVisibleText(driver, qdui.Country, qd.getCountry());
		}

		if (qd.getStateProvince() != null) {
			fc.utobj().selectDropDownByVisibleText(driver, qdui.StateProvince, qd.getStateProvince());
		}

		if (qd.getZipPostalCode() != null) {
			fc.utobj().sendKeys(driver, qdui.ZipPostalCode, qd.getZipPostalCode());
		}

		if (qd.getWorkPhone() != null) {
			fc.utobj().sendKeys(driver, qdui.WorkPhone, qd.getWorkPhone());
		}

		if (qd.getWorkPhoneExt() != null) {
			fc.utobj().sendKeys(driver, qdui.WorkPhoneExt, qd.getWorkPhoneExt());
		}

		if (qd.getHomePhone() != null) {
			fc.utobj().sendKeys(driver, qdui.HomePhone, qd.getHomePhone());
		}

		if (qd.getHomePhoneExt() != null) {
			fc.utobj().sendKeys(driver, qdui.HomePhoneExt, qd.getHomePhoneExt());
		}

		if (qd.getEmail() != null) {
			fc.utobj().sendKeys(driver, qdui.Email, qd.getEmail());
		}

		if (qd.getUsCitizen() != null) {
			if (qd.getUsCitizen().equalsIgnoreCase("Yes")) {
				fc.utobj().clickElement(driver, qdui.USCitizenYes);
			} else if (qd.getUsCitizen().equalsIgnoreCase("No"))
				;
			{
				fc.utobj().clickElement(driver, qdui.USCitizenNo);
			}
		}

		if (qd.getSocialSecurity() != null) {
			fc.utobj().sendKeys(driver, qdui.SocialSecurity, qd.getSocialSecurity());
		}

		if (qd.getPreviousAddress() != null) {
			fc.utobj().sendKeys(driver, qdui.PreviousAddress, qd.getPreviousAddress());
		}

		if (qd.getPreviousCity() != null) {
			fc.utobj().sendKeys(driver, qdui.PreviousCity, qd.getPreviousCity());
		}

		if (qd.getPreviousCountry() != null) {
			fc.utobj().selectDropDownByVisibleText(driver, qdui.PreviousCountry, qd.getPreviousCountry());
		}

		if (qd.getPreviousStateProvince() != null) {
			fc.utobj().selectDropDownByVisibleText(driver, qdui.PreviousStateProvince, qd.getPreviousStateProvince());
		}

		if (qd.getPreviousZipPostalCode() != null) {
			fc.utobj().sendKeys(driver, qdui.ZipPostalCode, qd.getPreviousZipPostalCode());
		}

		if (qd.getBirthMonth() != null) {
			fc.utobj().selectDropDownByVisibleText(driver, qdui.BirthMonth, qd.getBirthMonth());
		}

		if (qd.getBirthDate() != null) {
			fc.utobj().selectDropDownByVisibleText(driver, qdui.BirthDate, qd.getBirthDate());
		}

		if (qd.getBestTimeToCall() != null) {
			fc.utobj().selectDropDownByVisibleText(driver, qdui.BestTimeToCall, qd.getBestTimeToCall());
		}

		if (qd.getHomeOwnership() != null) {
			if (qd.getHomeOwnership().equalsIgnoreCase("Own")) {
				fc.utobj().clickElement(driver, qdui.HomeOwnershipOwn);
			} else if (qd.getHomeOwnership().equalsIgnoreCase("Renting")) {
				fc.utobj().clickElement(driver, qdui.HomeOwnershipRenting);
			}
		}

		if (qd.getMaritalStatus() != null) {
			if (qd.getMaritalStatus().equalsIgnoreCase("Single")) {
				fc.utobj().clickElement(driver, qdui.MaritalStatusSingle);

			} else if (qd.getMaritalStatus().equalsIgnoreCase("Married")) {
				fc.utobj().clickElement(driver, qdui.MaritalStatusMarried);
				if (qd.getSpouseName() != null) {
					fc.utobj().sendKeys(driver, qdui.SpouseName, qd.getSpouseName());
				}

				if (qd.getSpouseSocialSecurity() != null) {
					fc.utobj().sendKeys(driver, qdui.SpouseSocialSecurity, qd.getSpouseSocialSecurity());
				}

				if (qd.getSpouseUSCitizen() != null) {
					if (qd.getSpouseUSCitizen().equalsIgnoreCase("Yes")) {
						fc.utobj().clickElement(driver, qdui.SpouseUSCitizenYes);
					} else if (qd.getSpouseUSCitizen().equalsIgnoreCase("No")) {
						fc.utobj().clickElement(driver, qdui.SpouseUSCitizenNo);
					}
				}

				if (qd.getSpouseBirthMonth() != null) {
					fc.utobj().selectDropDownByVisibleText(driver, qdui.SpouseBirthMonth, qd.getSpouseBirthMonth());// (driver,
																													// qdui.SpouseBirthMonth,
																													// qd.getSpouseBirthMonth());
				}

				if (qd.getSpouseBirthDate() != null) {
					fc.utobj().selectDropDownByVisibleText(driver, qdui.SpouseBirthDate, qd.getSpouseBirthDate());
				}

			}
		}

		// Assets & Liabilities

		if (qd.getCashOnHandInBanks() != null) {
			fc.utobj().sendKeys(driver, qdui.CashOnHandAndBanks, qd.getCashOnHandInBanks());
		}

		if (qd.getMortgages() != null) {
			fc.utobj().sendKeys(driver, qdui.Mortgages, qd.getMortgages());
		}

		if (qd.getMarketableSecurities() != null) {
			fc.utobj().sendKeys(driver, qdui.MarketableSecurities, qd.getMarketableSecurities());
		}

		if (qd.getAccountsPayable() != null) {
			fc.utobj().sendKeys(driver, qdui.AccountsPayable, qd.getAccountsPayable());
		}

		if (qd.getAccountsNotesReceivable() != null) {
			fc.utobj().sendKeys(driver, qdui.AccountsNotesReceivable, qd.getAccountsNotesReceivable());
		}

		if (qd.getNotesPayable() != null) {
			fc.utobj().sendKeys(driver, qdui.NotesPayable, qd.getNotesPayable());
		}

		if (qd.getRetirementPlans() != null) {
			fc.utobj().sendKeys(driver, qdui.RetirementPlans, qd.getRetirementPlans());
		}

		if (qd.getLoansOnLifeInsurance() != null) {
			fc.utobj().sendKeys(driver, qdui.LoansOnLifeInsurance, qd.getLoansOnLifeInsurance());
		}

		if (qd.getRealEstate() != null) {
			fc.utobj().sendKeys(driver, qdui.RealEstate, qd.getRealEstate());
		}

		if (qd.getCreditCardsTotalBalance() != null) {
			fc.utobj().sendKeys(driver, qdui.CreditCardsTotalBalance, qd.getCreditCardsTotalBalance());
		}

		if (qd.getPersonalProperty() != null) {
			fc.utobj().sendKeys(driver, qdui.PersonalProperty, qd.getPersonalProperty());
		}

		if (qd.getUnpaidTaxes() != null) {
			fc.utobj().sendKeys(driver, qdui.UnpaidTaxes, qd.getUnpaidTaxes());
		}

		if (qd.getBusinessHoldings() != null) {
			fc.utobj().sendKeys(driver, qdui.BusinessHoldings, qd.getBusinessHoldings());
		}

		if (qd.getLifeInsurance() != null) {
			fc.utobj().sendKeys(driver, qdui.LifeInsurance, qd.getLifeInsurance());
		}

		if (qd.getOtherAssets() != null) {
			fc.utobj().sendKeys(driver, qdui.OtherAssets, qd.getOtherAssets());
		}

		if (qd.getOtherLiabilities() != null) {
			fc.utobj().sendKeys(driver, qdui.OtherLiabilities, qd.getOtherLiabilities());
		}

		if (qd.getDescriptionLiabilities() != null) {
			fc.utobj().sendKeys(driver, qdui.LiabilitiesDescription, qd.getDescriptionLiabilities());
		}

		if (qd.getDescriptionAssests() != null) {
			fc.utobj().sendKeys(driver, qdui.AssetsDescription, qd.getDescriptionAssests());
		}

		if (qd.getTotalAssets() != null) {
			fc.utobj().sendKeys(driver, qdui.TotalAssets, qd.getTotalAssets());
		}

		if (qd.getTotalLiabilities() != null) {
			fc.utobj().sendKeys(driver, qdui.TotalLiabilities, qd.getTotalLiabilities());
		}

		if (qd.getTotalNetWorth() != null) {
			fc.utobj().sendKeys(driver, qdui.TotalNetWorth, qd.getTotalNetWorth());
		}

		// Real Estate Owned

		if (qd.getRealEstateAddress1() != null) {
			fc.utobj().sendKeys(driver, qdui.RealEstateAddress1, qd.getRealEstateAddress1());
		}

		if (qd.getRealEstateDatePurchased1() != null) {
			fc.utobj().sendKeys(driver, qdui.RealEstateDatePurchased1, qd.getRealEstateDatePurchased1());
		}

		if (qd.getRealEstateOriginalCost1() != null) {
			fc.utobj().sendKeys(driver, qdui.RealEstateOriginalCost1, qd.getRealEstateOriginalCost1());
		}

		if (qd.getRealEstatePresentValue1() != null) {
			fc.utobj().sendKeys(driver, qdui.RealEstatePresentValue1, qd.getRealEstatePresentValue1());
		}

		if (qd.getRealEstateMortgageBalance1() != null) {
			fc.utobj().sendKeys(driver, qdui.RealEstateMortgageBalance1, qd.getRealEstateMortgageBalance1());
		}

		if (qd.getRealEstateAddress2() != null) {
			fc.utobj().sendKeys(driver, qdui.RealEstateAddress2, qd.getRealEstateAddress2());
		}

		if (qd.getRealEstateDatePurchased2() != null) {
			fc.utobj().sendKeys(driver, qdui.RealEstateDatePurchased2, qd.getRealEstateDatePurchased2());
		}

		if (qd.getRealEstateOriginalCost2() != null) {
			fc.utobj().sendKeys(driver, qdui.RealEstateOriginalCost2, qd.getRealEstateOriginalCost2());
		}

		if (qd.getRealEstatePresentValue2() != null) {
			fc.utobj().sendKeys(driver, qdui.RealEstatePresentValue2, qd.getRealEstatePresentValue2());
		}

		if (qd.getRealEstateMortgageBalance2() != null) {
			fc.utobj().sendKeys(driver, qdui.RealEstateMortgageBalance2, qd.getRealEstateMortgageBalance2());
		}

		if (qd.getRealEstateAddress3() != null) {
			fc.utobj().sendKeys(driver, qdui.RealEstateAddress3, qd.getRealEstateAddress3());
		}

		if (qd.getRealEstateDatePurchased3() != null) {
			fc.utobj().sendKeys(driver, qdui.RealEstateDatePurchased3, qd.getRealEstateDatePurchased3());
		}

		if (qd.getRealEstateOriginalCost3() != null) {
			fc.utobj().sendKeys(driver, qdui.RealEstateOriginalCost3, qd.getRealEstateOriginalCost3());
		}

		if (qd.getRealEstatePresentValue3() != null) {
			fc.utobj().sendKeys(driver, qdui.RealEstatePresentValue3, qd.getRealEstatePresentValue3());
		}

		if (qd.getRealEstateMortgageBalance3() != null) {
			fc.utobj().sendKeys(driver, qdui.RealEstateMortgageBalance3, qd.getRealEstateMortgageBalance3());
		}

		// Annual Sources Of Income

		if (qd.getSalary() != null) {
			fc.utobj().sendKeys(driver, qdui.Salary, qd.getSalary());
		}

		if (qd.getInvestment() != null) {
			fc.utobj().sendKeys(driver, qdui.Investment, qd.getInvestment());
		}

		if (qd.getRealEstateIncome() != null) {
			fc.utobj().sendKeys(driver, qdui.RealEstateIncome, qd.getRealEstateIncome());
		}

		if (qd.getOther() != null) {
			fc.utobj().sendKeys(driver, qdui.Other, qd.getOther());
		}

		if (qd.getDescription() != null) {
			fc.utobj().sendKeys(driver, qdui.Description, qd.getDescription());
		}

		if (qd.getTotalAnnualSource() != null) {
			fc.utobj().sendKeys(driver, qdui.TotalAnnualSource, qd.getTotalAnnualSource());
		}

		// Total Contingent Liabilities

		if (qd.getLoanCoSignature() != null) {
			fc.utobj().sendKeys(driver, qdui.LoanCoSignature, qd.getLoanCoSignature());
		}

		if (qd.getLegalJudgement() != null) {
			fc.utobj().sendKeys(driver, qdui.LegalJudgement, qd.getLegalJudgement());
		}

		if (qd.getIncomeTaxes() != null) {
			fc.utobj().sendKeys(driver, qdui.IncomeTaxes, qd.getIncomeTaxes());
		}

		if (qd.getOtherSpecialDebt() != null) {
			fc.utobj().sendKeys(driver, qdui.OtherSpecialDebt, qd.getOtherSpecialDebt());
		}

		if (qd.getTotalContingent() != null) {
			fc.utobj().sendKeys(driver, qdui.TotalContingent, qd.getTotalContingent());
		}

		// Specific Data

		if (qd.getWhenReadyIfApproved() != null) {
			fc.utobj().sendKeys(driver, qdui.WhenReadyIfApproved, qd.getWhenReadyIfApproved());
		}

		if (qd.getSkillsExperience() != null) {
			fc.utobj().sendKeys(driver, qdui.SkillsExperience, qd.getSkillsExperience());
		}

		if (qd.getEnableReachGoals() != null) {
			fc.utobj().sendKeys(driver, qdui.EnableReachGoals, qd.getEnableReachGoals());
		}

		if (qd.getResponsibleForDailyOperations() != null) {
			fc.utobj().sendKeys(driver, qdui.ResponsibleForDailyOperations, qd.getResponsibleForDailyOperations());
		}

		if (qd.getCashAvailableForInvestment() != null) {
			fc.utobj().sendKeys(driver, qdui.CashAvailableForInvestment, qd.getCashAvailableForInvestment());
		}

		if (qd.getApprovedForFinancing() != null) {
			if (qd.getApprovedForFinancing().equalsIgnoreCase("Yes")) {
				fc.utobj().clickElement(driver, qdui.ApprovedForFinancingYes);
			} else if (qd.getApprovedForFinancing().equalsIgnoreCase("No")) {
				fc.utobj().clickElement(driver, qdui.ApprovedForFinancingNo);
			}
		}

		if (qd.getAmountApprovedForFinance() != null) {
			fc.utobj().sendKeys(driver, qdui.AmountApprovedForFinance, qd.getAmountApprovedForFinance());
		}

		if (qd.getSoleIncomeSource() != null) {
			if (qd.getSoleIncomeSource().equalsIgnoreCase("Yes")) {
				fc.utobj().clickElement(driver, qdui.SoleIncomeSourceYes);
			} else if (qd.getSoleIncomeSource().equalsIgnoreCase("No")) {
				fc.utobj().clickElement(driver, qdui.SoleIncomeSourceNo);
			}
		}

		if (qd.getContingentliabilites() != null) {
			if (qd.getContingentliabilites().equalsIgnoreCase("Yes")) {
				fc.utobj().clickElement(driver, qdui.ContingentLiabilitesYes);
			} else if (qd.getContingentliabilites().equalsIgnoreCase("No")) {
				fc.utobj().clickElement(driver, qdui.ContingentLiabilitesNo);
			}
		}

		if (qd.getLawsuit() != null) {
			if (qd.getLawsuit().equalsIgnoreCase("Yes")) {
				fc.utobj().clickElement(driver, qdui.LawsuitYes);
			} else if (qd.getLawsuit().equalsIgnoreCase("No")) {
				fc.utobj().clickElement(driver, qdui.LawsuitNo);
			}
		}

		if (qd.getConvicted() != null) {
			if (qd.getConvicted().equalsIgnoreCase("Yes")) {
				fc.utobj().clickElement(driver, qdui.ConvictedYes);
			} else if (qd.getConvicted().equalsIgnoreCase("No")) {
				fc.utobj().clickElement(driver, qdui.ConvictedNo);
			}
		}

		if (qd.getConvictedOfFelony() != null) {
			if (qd.getConvictedOfFelony().equalsIgnoreCase("Yes")) {
				fc.utobj().clickElement(driver, qdui.ConvictedOfFelonyYes);
				if (qd.getExplainConviction() != null) {
					fc.utobj().sendKeys(driver, qdui.ExplainConviction, qd.getExplainConviction());
				}
			} else if (qd.getConvictedOfFelony().equalsIgnoreCase("No")) {
				fc.utobj().clickElement(driver, qdui.ConvictedOfFelonyNo);
			}
		}

		if (qd.getFiledBankruptcy() != null) {
			if (qd.getFiledBankruptcy().equalsIgnoreCase("Yes")) {
				fc.utobj().clickElement(driver, qdui.FiledBankruptcyYes);
				if (qd.getDateFiledBankruptcy() != null) {
					fc.utobj().sendKeys(driver, qdui.DateFieldBankruptcy, qd.getDateFiledBankruptcy());
				}

				if (qd.getDateDischargedBankruptcy() != null) {
					fc.utobj().sendKeys(driver, qdui.DateDischargedField, qd.getDateDischargedBankruptcy());
				}
			} else if (qd.getFiledBankruptcy().equalsIgnoreCase("No")) {
				fc.utobj().clickElement(driver, qdui.FiledBankruptcyNo);
			}
		}

		if (qd.getLocationPreference1() != null) {
			fc.utobj().sendKeys(driver, qdui.AreaLocationPreferences1, qd.getLocationPreference1());
		}

		if (qd.getLocationPreference2() != null) {
			fc.utobj().sendKeys(driver, qdui.AreaLocationPreferences2, qd.getLocationPreference2());
		}

		if (qd.getLocationPreference3() != null) {
			fc.utobj().sendKeys(driver, qdui.AreaLocationPreferences3, qd.getLocationPreference3());
		}

		if (qd.getBusinessQuestion1() != null) {
			fc.utobj().sendKeys(driver, qdui.BusinessQuestion1, qd.getBusinessQuestion1());
		}

		if (qd.getBusinessQuestion2() != null) {
			fc.utobj().sendKeys(driver, qdui.BusinessQuestion2, qd.getBusinessQuestion2());
		}

		if (qd.getBusinessQuestion3() != null) {
			fc.utobj().sendKeys(driver, qdui.BusinessQuestion3, qd.getBusinessQuestion3());
		}

		// Internal Analysis of Applicant

		if (qd.getFirstNameApplicant() != null) {
			fc.utobj().sendKeys(driver, qdui.FinFirstName, qd.getFirstNameApplicant());
		}

		if (qd.getLastNameApplicant() != null) {
			fc.utobj().sendKeys(driver, qdui.FinLastName, qd.getLastNameApplicant());
		}

		if (qd.getAddressApplicant() != null) {
			fc.utobj().sendKeys(driver, qdui.FinAddress, qd.getAddressApplicant());
		}

		if (qd.getCityApplicant() != null) {
			fc.utobj().sendKeys(driver, qdui.FinCity, qd.getCityApplicant());
		}

		if (qd.getWorkPhoneApplicant() != null) {
			fc.utobj().sendKeys(driver, qdui.FinWorkPhone, qd.getWorkPhoneApplicant());
		}

		if (qd.getWorkPhoneExtApplicant() != null) {
			fc.utobj().sendKeys(driver, qdui.FinWorkPhoneExt, qd.getWorkPhoneExtApplicant());
		}

		if (qd.getCountryApplicant() != null) {
			fc.utobj().selectDropDownByVisibleText(driver, qdui.FinCountry, qd.getCountryApplicant());
		}

		if (qd.getStateProvinceApplicant() != null) {
			fc.utobj().selectDropDownByVisibleText(driver, qdui.FinStateProvince, qd.getStateProvinceApplicant());
		}

		if (qd.getCalledAtOffice() != null) {
			if (qd.getCalledAtOffice().equalsIgnoreCase("Yes")) {
				fc.utobj().clickElement(driver, qdui.CalledOfficeYes);
			} else if (qd.getCalledAtOffice().equalsIgnoreCase("No")) {
				fc.utobj().clickElement(driver, qdui.CalledOfficeNo);
			}
		}

		// Heat Index Components

		if (qd.getCurrentNetWorth_liquidCapitalMin() != null) {
			fc.utobj().selectDropDownByVisibleText(driver, qdui.HeatIndexCurrentNetWorth,
					qd.getCurrentNetWorth_liquidCapitalMin());
		}

		if (qd.getCashAvailableForInvestment_liquidCapitalMax() != null) {
			fc.utobj().selectDropDownByVisibleText(driver, qdui.HeatIndexCashAvailableForInvestment,
					qd.getCashAvailableForInvestment_liquidCapitalMax());
		}

		if (qd.getInvestmentTimeframe() != null) {
			fc.utobj().selectDropDownByVisibleText(driver, qdui.InvestTimeframe, qd.getInvestmentTimeframe());
		}

		if (qd.getEmploymentBackground() != null) {
			fc.utobj().selectDropDownByVisibleText(driver, qdui.EmploymentBackground, qd.getEmploymentBackground());
		}

		if (qd.getBackgroundCheckApproval() != null) {
			if (qd.getBackgroundCheckApproval().equalsIgnoreCase("Yes")) {
				fc.utobj().clickElement(driver, qdui.BackgroundCheckApprovalYes);
			} else if (qd.getBackgroundCheckApproval().equalsIgnoreCase("No")) {
				fc.utobj().clickElement(driver, qdui.BackgroundCheckApprovalNo);
			}
		}

		// Qualification Checklists

		if (qd.getLendableNetWorth_value() != null) {
			fc.utobj().sendKeys(driver, qdui.LendableNetWorth_Value, qd.getLendableNetWorth_value());
		}

		if (qd.getLendableNetWorth_completed() != null) {
			if (qd.getLendableNetWorth_completed().equalsIgnoreCase("Yes")) {
				fc.utobj().clickElement(driver, qdui.LendableNetWorth_Completed_yes);
			} else if (qd.getLendableNetWorth_completed().equalsIgnoreCase("No")) {
				fc.utobj().clickElement(driver, qdui.LendableNetWorth_Completed_No);
			}
		}

		if (qd.getLendableNetWorth_completionDate() != null) {
			fc.utobj().sendKeys(driver, qdui.LendableNetWorth_CompletionDate, qd.getLendableNetWorth_completionDate());
		}

		if (qd.getLendableNetWorth_associatedDocument() != null) {
			fc.utobj().sendKeys(driver, qdui.LendableNetWorth_associatedDocument,
					qd.getLendableNetWorth_associatedDocument());
		}

		if (qd.getLendableNetWorth_verifiedBy() != null) {
			fc.utobj().sendKeys(driver, qdui.LendableNetWorth_VerifiedBy, qd.getLendableNetWorth_verifiedBy());
		}

		if (qd.getLendableNetWorth_date() != null) {
			fc.utobj().sendKeys(driver, qdui.LendableNetWorth_Date, qd.getLendableNetWorth_date());
		}

		//

		if (qd.getBackgroundandCriminalCheck_value() != null) {
			fc.utobj().sendKeys(driver, qdui.BackgroundandCriminalCheck_Value,
					qd.getBackgroundandCriminalCheck_value());
		}

		if (qd.getBackgroundandCriminalCheck_completed() != null) {
			if (qd.getBackgroundandCriminalCheck_completed().equalsIgnoreCase("Yes")) {
				fc.utobj().clickElement(driver, qdui.BackgroundandCriminalCheck_Completed_yes);
			} else if (qd.getBackgroundandCriminalCheck_completed().equalsIgnoreCase("No")) {
				fc.utobj().clickElement(driver, qdui.BackgroundandCriminalCheck_Completed_No);
			}
		}

		if (qd.getBackgroundandCriminalCheck_CompletionDate() != null) {
			fc.utobj().sendKeys(driver, qdui.BackgroundandCriminalCheck_CompletionDate,
					qd.getBackgroundandCriminalCheck_CompletionDate());
		}

		if (qd.getBackgroundandCriminalCheck_associatedDocument() != null) {
			fc.utobj().sendKeys(driver, qdui.BackgroundandCriminalCheck_associatedDocument,
					qd.getBackgroundandCriminalCheck_associatedDocument());
		}

		if (qd.getBackgroundandCriminalCheck_VerifiedBy() != null) {
			fc.utobj().sendKeys(driver, qdui.BackgroundandCriminalCheck_VerifiedBy,
					qd.getBackgroundandCriminalCheck_VerifiedBy());
		}

		if (qd.getBackgroundandCriminalCheck_Date() != null) {
			fc.utobj().sendKeys(driver, qdui.BackgroundandCriminalCheck_Date, qd.getBackgroundandCriminalCheck_Date());
		}

		//

		if (qd.getCreditCheck_value() != null) {
			fc.utobj().sendKeys(driver, qdui.CreditCheck_Value, qd.getCreditCheck_value());
		}

		if (qd.getCreditCheck_completed() != null) {
			if (qd.getCreditCheck_completed().equalsIgnoreCase("Yes")) {
				fc.utobj().clickElement(driver, qdui.CreditCheck_Completed_yes);
			} else if (qd.getCreditCheck_completed().equalsIgnoreCase("No")) {
				fc.utobj().clickElement(driver, qdui.CreditCheck_Completed_No);
			}
		}

		if (qd.getCreditCheck_CompletionDate() != null) {
			fc.utobj().sendKeys(driver, qdui.CreditCheck_CompletionDate, qd.getCreditCheck_CompletionDate());
		}

		if (qd.getCreditCheck_associatedDocument() != null) {
			fc.utobj().sendKeys(driver, qdui.CreditCheck_associatedDocument, qd.getCreditCheck_associatedDocument());
		}

		if (qd.getCreditCheck_VerifiedBy() != null) {
			fc.utobj().sendKeys(driver, qdui.CreditCheck_VerifiedBy, qd.getCreditCheck_VerifiedBy());
		}

		if (qd.getCreditCheck_Date() != null) {
			fc.utobj().sendKeys(driver, qdui.CreditCheck_Date, qd.getCreditCheck_Date());
		}

		//

		if (qd.getTerritoryApproved_value() != null) {
			fc.utobj().sendKeys(driver, qdui.TerritoryApproved_Value, qd.getTerritoryApproved_value());
		}

		if (qd.getTerritoryApproved_completed() != null) {
			if (qd.getTerritoryApproved_completed().equalsIgnoreCase("Yes")) {
				fc.utobj().clickElement(driver, qdui.TerritoryApproved_Completed_yes);
			} else if (qd.getTerritoryApproved_completed().equalsIgnoreCase("No")) {
				fc.utobj().clickElement(driver, qdui.TerritoryApproved_Completed_No);
			}
		}

		if (qd.getTerritoryApproved_CompletionDate() != null) {
			fc.utobj().sendKeys(driver, qdui.TerritoryApproved_CompletionDate,
					qd.getTerritoryApproved_CompletionDate());
		}

		if (qd.getTerritoryApproved_associatedDocument() != null) {
			fc.utobj().sendKeys(driver, qdui.TerritoryApproved_associatedDocument,
					qd.getTerritoryApproved_associatedDocument());
		}

		if (qd.getTerritoryApproved_VerifiedBy() != null) {
			fc.utobj().sendKeys(driver, qdui.TerritoryApproved_VerifiedBy, qd.getTerritoryApproved_VerifiedBy());
		}

		if (qd.getTerritoryApproved_Date() != null) {
			fc.utobj().sendKeys(driver, qdui.TerritoryApproved_Date, qd.getTerritoryApproved_Date());
		}

		//

		if (qd.getFranchiseAgreementonFile_value() != null) {
			fc.utobj().sendKeys(driver, qdui.FranchiseAgreementonFile_Value, qd.getFranchiseAgreementonFile_value());
		}

		if (qd.getFranchiseAgreementonFile_completed() != null) {
			if (qd.getFranchiseAgreementonFile_completed().equalsIgnoreCase("Yes")) {
				fc.utobj().clickElement(driver, qdui.FranchiseAgreementonFile_Completed_yes);
			} else if (qd.getFranchiseAgreementonFile_completed().equalsIgnoreCase("No")) {
				fc.utobj().clickElement(driver, qdui.FranchiseAgreementonFile_Completed_No);
			}
		}

		if (qd.getFranchiseAgreementonFile_CompletionDate() != null) {
			fc.utobj().sendKeys(driver, qdui.FranchiseAgreementonFile_CompletionDate,
					qd.getFranchiseAgreementonFile_CompletionDate());
		}

		if (qd.getFranchiseAgreementonFile_associatedDocument() != null) {
			fc.utobj().sendKeys(driver, qdui.FranchiseAgreementonFile_associatedDocument,
					qd.getFranchiseAgreementonFile_associatedDocument());
		}

		if (qd.getFranchiseAgreementonFile_VerifiedBy() != null) {
			fc.utobj().sendKeys(driver, qdui.FranchiseAgreementonFile_VerifiedBy,
					qd.getFranchiseAgreementonFile_VerifiedBy());
		}

		if (qd.getFranchiseAgreementonFile_Date() != null) {
			fc.utobj().sendKeys(driver, qdui.FranchiseAgreementonFile_Date, qd.getFranchiseAgreementonFile_Date());
		}

		//

		if (qd.getFddReceiptonFile_value() != null) {
			fc.utobj().sendKeys(driver, qdui.FDDReceiptonFile_Value, qd.getFddReceiptonFile_value());
		}

		if (qd.getFddReceiptonFile_completed() != null) {
			if (qd.getFddReceiptonFile_completed().equalsIgnoreCase("Yes")) {
				fc.utobj().clickElement(driver, qdui.FDDReceiptonFile_Completed_yes);
			} else if (qd.getFddReceiptonFile_completed().equalsIgnoreCase("No")) {
				fc.utobj().clickElement(driver, qdui.FDDReceiptonFile_Completed_No);
			}
		}

		if (qd.getFddReceiptonFile_CompletionDate() != null) {
			fc.utobj().sendKeys(driver, qdui.FDDReceiptonFile_CompletionDate, qd.getFddReceiptonFile_CompletionDate());
		}

		if (qd.getFddReceiptonFile_associatedDocument() != null) {
			fc.utobj().sendKeys(driver, qdui.FDDReceiptonFile_associatedDocument,
					qd.getFddReceiptonFile_associatedDocument());
		}

		if (qd.getFddReceiptonFile_VerifiedBy() != null) {
			fc.utobj().sendKeys(driver, qdui.FDDReceiptonFile_VerifiedBy, qd.getFddReceiptonFile_VerifiedBy());
		}

		if (qd.getFddReceiptonFile_Date() != null) {
			fc.utobj().sendKeys(driver, qdui.FDDReceiptonFile_Date, qd.getFddReceiptonFile_Date());
		}

		//

		if (qd.getOpener_value() != null) {
			fc.utobj().sendKeys(driver, qdui.Opener_Value, qd.getOpener_value());
		}

		if (qd.getOpener_completed() != null) {
			if (qd.getOpener_completed().equalsIgnoreCase("Yes")) {
				fc.utobj().clickElement(driver, qdui.Opener_Completed_yes);
			} else if (qd.getOpener_completed().equalsIgnoreCase("No")) {
				fc.utobj().clickElement(driver, qdui.Opener_Completed_No);
			}
		}

		if (qd.getOpener_CompletionDate() != null) {
			fc.utobj().sendKeys(driver, qdui.Opener_CompletionDate, qd.getOpener_CompletionDate());
		}

		if (qd.getOpener_associatedDocument() != null) {
			fc.utobj().sendKeys(driver, qdui.Opener_associatedDocument, qd.getOpener_associatedDocument());
		}

		if (qd.getOpener_VerifiedBy() != null) {
			fc.utobj().sendKeys(driver, qdui.Opener_VerifiedBy, qd.getOpener_VerifiedBy());
		}

		if (qd.getOpener_Date() != null) {
			fc.utobj().sendKeys(driver, qdui.Opener_Date, qd.getOpener_Date());
		}

		fc.utobj().clickElement(driver, qdui.save);

	}

}
