package com.builds.test.fs;

class RealEstate {

	private String siteAddress1;
	private String siteAddress2;
	private String siteCity;
	private String siteCountry;
	private String siteStateProvince;
	private String buildingSize;
	private String buildingLengthX;
	private String buildingBreadthY;
	private String buildingHeightZ;
	private String parkingSpaces;
	private String dealType;
	private String loiSent;
	private String loiSigned;
	private String approvalDate;
	private String leaseCommencement;
	private String leaseExpiration;
	private String initialTerm;
	private String optionTerm;
	private String purchaseOption;
	private String projectedOpeningDate;
	private String generalContractorSelected;
	private String nameGeneralContractor;
	private String addressGeneralContractor;
	private String permitAppliedFor;
	private String permitIssuedDate;
	private String certificateOfOccupancy;
	private String turnoverDate;
	private String ExpectedOpeningDate;

	public String getSiteAddress1() {
		return siteAddress1;
	}

	public void setSiteAddress1(String siteAddress1) {
		this.siteAddress1 = siteAddress1;
	}

	public String getSiteAddress2() {
		return siteAddress2;
	}

	public void setSiteAddress2(String siteAddress2) {
		this.siteAddress2 = siteAddress2;
	}

	public String getSiteCity() {
		return siteCity;
	}

	public void setSiteCity(String siteCity) {
		this.siteCity = siteCity;
	}

	public String getSiteCountry() {
		return siteCountry;
	}

	public void setSiteCountry(String siteCountry) {
		this.siteCountry = siteCountry;
	}

	public String getSiteStateProvince() {
		return siteStateProvince;
	}

	public void setSiteStateProvince(String siteStateProvince) {
		this.siteStateProvince = siteStateProvince;
	}

	public String getBuildingSize() {
		return buildingSize;
	}

	public void setBuildingSize(String buildingSize) {
		this.buildingSize = buildingSize;
	}

	public String getBuildingLengthX() {
		return buildingLengthX;
	}

	public void setBuildingLengthX(String buildingLengthX) {
		this.buildingLengthX = buildingLengthX;
	}

	public String getBuildingBreadthY() {
		return buildingBreadthY;
	}

	public void setBuildingBreadthY(String buildingBreadthY) {
		this.buildingBreadthY = buildingBreadthY;
	}

	public String getBuildingHeightZ() {
		return buildingHeightZ;
	}

	public void setBuildingHeightZ(String buildingHeightZ) {
		this.buildingHeightZ = buildingHeightZ;
	}

	public String getParkingSpaces() {
		return parkingSpaces;
	}

	public void setParkingSpaces(String parkingSpaces) {
		this.parkingSpaces = parkingSpaces;
	}

	public String getDealType() {
		return dealType;
	}

	public void setDealType(String dealType) {
		this.dealType = dealType;
	}

	public String getLoiSent() {
		return loiSent;
	}

	public void setLoiSent(String loiSent) {
		this.loiSent = loiSent;
	}

	public String getLoiSigned() {
		return loiSigned;
	}

	public void setLoiSigned(String loiSigned) {
		this.loiSigned = loiSigned;
	}

	public String getApprovalDate() {
		return approvalDate;
	}

	public void setApprovalDate(String approvalDate) {
		this.approvalDate = approvalDate;
	}

	public String getLeaseCommencement() {
		return leaseCommencement;
	}

	public void setLeaseCommencement(String leaseCommencement) {
		this.leaseCommencement = leaseCommencement;
	}

	public String getLeaseExpiration() {
		return leaseExpiration;
	}

	public void setLeaseExpiration(String leaseExpiration) {
		this.leaseExpiration = leaseExpiration;
	}

	public String getInitialTerm() {
		return initialTerm;
	}

	public void setInitialTerm(String initialTerm) {
		this.initialTerm = initialTerm;
	}

	public String getOptionTerm() {
		return optionTerm;
	}

	public void setOptionTerm(String optionTerm) {
		this.optionTerm = optionTerm;
	}

	public String getPurchaseOption() {
		return purchaseOption;
	}

	public void setPurchaseOption(String purchaseOption) {
		this.purchaseOption = purchaseOption;
	}

	public String getProjectedOpeningDate() {
		return projectedOpeningDate;
	}

	public void setProjectedOpeningDate(String projectedOpeningDate) {
		this.projectedOpeningDate = projectedOpeningDate;
	}

	public String getGeneralContractorSelected() {
		return generalContractorSelected;
	}

	public void setGeneralContractorSelected(String generalContractorSelected) {
		this.generalContractorSelected = generalContractorSelected;
	}

	public String getNameGeneralContractor() {
		return nameGeneralContractor;
	}

	public void setNameGeneralContractor(String nameGeneralContractor) {
		this.nameGeneralContractor = nameGeneralContractor;
	}

	public String getAddressGeneralContractor() {
		return addressGeneralContractor;
	}

	public void setAddressGeneralContractor(String addressGeneralContractor) {
		this.addressGeneralContractor = addressGeneralContractor;
	}

	public String getPermitAppliedFor() {
		return permitAppliedFor;
	}

	public void setPermitAppliedFor(String permitAppliedFor) {
		this.permitAppliedFor = permitAppliedFor;
	}

	public String getPermitIssuedDate() {
		return permitIssuedDate;
	}

	public void setPermitIssuedDate(String permitIssuedDate) {
		this.permitIssuedDate = permitIssuedDate;
	}

	public String getCertificateOfOccupancy() {
		return certificateOfOccupancy;
	}

	public void setCertificateOfOccupancy(String certificateOfOccupancy) {
		this.certificateOfOccupancy = certificateOfOccupancy;
	}

	public String getTurnoverDate() {
		return turnoverDate;
	}

	public void setTurnoverDate(String turnoverDate) {
		this.turnoverDate = turnoverDate;
	}

	public String getExpectedOpeningDate() {
		return ExpectedOpeningDate;
	}

	public void setExpectedOpeningDate(String expectedOpeningDate) {
		ExpectedOpeningDate = expectedOpeningDate;
	}

}
