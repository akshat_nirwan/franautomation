package com.builds.test.fs;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.fs.RealEstateUI;
import com.builds.utilities.FranconnectUtil;

class RealEstateTest {

	void fillAndSubmitRealEstate(WebDriver driver, RealEstate re) throws Exception {
		RealEstateUI reui = new RealEstateUI(driver);
		FranconnectUtil fc = new FranconnectUtil();

		fc.utobj().printTestStep("Fill Real Estate Info");

		if (re.getSiteAddress1() != null) {
			fc.utobj().sendKeys(driver, reui.siteAddress1, re.getSiteAddress1());
		}

		if (re.getSiteAddress2() != null) {
			fc.utobj().sendKeys(driver, reui.siteAddress2, re.getSiteAddress2());
		}

		if (re.getSiteCity() != null) {
			fc.utobj().sendKeys(driver, reui.siteCity, re.getSiteCity());
		}

		if (re.getSiteCountry() != null) {
			fc.utobj().selectDropDownByVisibleText(driver, reui.siteCountry, re.getSiteCountry());
		}

		if (re.getSiteStateProvince() != null) {
			fc.utobj().selectDropDownByVisibleText(driver, reui.siteState, re.getSiteStateProvince());
		}

		if (re.getBuildingSize() != null) {
			fc.utobj().sendKeys(driver, reui.buildingSize, re.getBuildingSize());
		}

		if (re.getBuildingLengthX() != null) {
			fc.utobj().sendKeys(driver, reui.buildingDimentionsX, re.getBuildingLengthX());
		}

		if (re.getBuildingBreadthY() != null) {
			fc.utobj().sendKeys(driver, reui.buildingDimentionsY, re.getBuildingBreadthY());
		}

		if (re.getBuildingHeightZ() != null) {
			fc.utobj().sendKeys(driver, reui.buildingDimentionsZ, re.getBuildingHeightZ());
		}

		if (re.getParkingSpaces() != null) {
			fc.utobj().sendKeys(driver, reui.parkingSpaces, re.getParkingSpaces());
		}

		if (re.getDealType() != null) {
			fc.utobj().sendKeys(driver, reui.dealType, re.getDealType());
		}

		if (re.getLoiSent() != null) {
			fc.utobj().sendKeys(driver, reui.loiSent, re.getLoiSent());
		}

		if (re.getLoiSigned() != null) {
			fc.utobj().sendKeys(driver, reui.loiSigned, re.getLoiSigned());
		}

		if (re.getApprovalDate() != null) {
			fc.utobj().sendKeys(driver, reui.approvalDate, re.getApprovalDate());
		}

		if (re.getLeaseCommencement() != null) {
			fc.utobj().sendKeys(driver, reui.leaseCommencement, re.getLeaseCommencement());
		}

		if (re.getLeaseExpiration() != null) {
			fc.utobj().sendKeys(driver, reui.leaseExpiration, re.getLeaseExpiration());
		}

		if (re.getInitialTerm() != null) {
			fc.utobj().sendKeys(driver, reui.initialTerm, re.getInitialTerm());
		}

		if (re.getOptionTerm() != null) {
			fc.utobj().sendKeys(driver, reui.optionTerm, re.getOptionTerm());
		}

		if (re.getPurchaseOption() != null) {
			if (re.getPurchaseOption().equalsIgnoreCase("Yes")) {
				fc.utobj().clickElement(driver, reui.purchaseOptionYes);
			} else if (re.getPurchaseOption().equalsIgnoreCase("No")) {
				fc.utobj().clickElement(driver, reui.purchaseOptionNo);
			}
		}

		if (re.getProjectedOpeningDate() != null) {
			fc.utobj().sendKeys(driver, reui.projectedOpeningDate, re.getProjectedOpeningDate());
		}

		if (re.getGeneralContractorSelected() != null) {
			if (re.getGeneralContractorSelected().equalsIgnoreCase("Yes")) {
				fc.utobj().clickElement(driver, reui.generalContractorSelectedYes);
				if (re.getNameGeneralContractor() != null) {
					fc.utobj().sendKeys(driver, reui.nameGeneralContractor, re.getNameGeneralContractor());
				}

				if (re.getAddressGeneralContractor() != null) {
					fc.utobj().sendKeys(driver, reui.addressGeneralContractor, re.getAddressGeneralContractor());
				}
			} else if (re.getGeneralContractorSelected().equalsIgnoreCase("No")) {
				fc.utobj().clickElement(driver, reui.generalContractorSelectedNo);
			}
		}

		if (re.getPermitAppliedFor() != null) {
			fc.utobj().sendKeys(driver, reui.permitApplied, re.getPermitAppliedFor());
		}

		if (re.getPermitIssuedDate() != null) {
			fc.utobj().sendKeys(driver, reui.permitIssued, re.getPermitIssuedDate());
		}

		if (re.getCertificateOfOccupancy() != null) {
			fc.utobj().sendKeys(driver, reui.certificate, re.getCertificateOfOccupancy());

		}

		if (re.getTurnoverDate() != null) {
			fc.utobj().sendKeys(driver, reui.turnOverDate, re.getTurnoverDate());
		}

		if (re.getExpectedOpeningDate() != null) {
			fc.utobj().sendKeys(driver, reui.grandOpeningDate, re.getExpectedOpeningDate());
		}

		fc.utobj().clickElement(driver, reui.saveBtn);

	}

	void validateRealEstate(WebDriver driver, RealEstate re) throws Exception {
		RealEstateUI reui = new RealEstateUI(driver);
		FranconnectUtil fc = new FranconnectUtil();

		fc.utobj().printTestStep("Validate Real Estate Info");

		if (!fc.utobj().isElementPresent(driver, reui.modifyRealEstateBtn)) {
			fc.utobj().throwsException("Modify button not present on Real Estate tab page");
		}
		if (!fc.utobj().isElementPresent(driver, reui.printRealEstateBtn)) {
			fc.utobj().throwsException("Print button not present on Real Estate tab page");
		}
	}
}
