package com.builds.test.fs;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.fs.RemarkUI;
import com.builds.utilities.FranconnectUtil;

class RemarkTest {
	FranconnectUtil fc = new FranconnectUtil();

	public void fill_Remarks_And_Submit(WebDriver driver, Remark remark) throws Exception {
		RemarkUI ui = new RemarkUI(driver);

		fc.utobj().printTestStep("Fill Remarks");

		if (remark.getRemark() != null) {
			fc.utobj().sendKeys(driver, ui.remarks, remark.getRemark());
		}

		fc.commonMethods().Click_Submit_Button_ByValue(driver);
		fc.commonMethods().Click_Close_Input_ByValue(driver);
		fc.utobj().switchFrameToDefault(driver);
	}
}
