package com.builds.test.fs.RestAPI;

import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Reporter;

import com.builds.utilities.FranconnectUtil;
import com.ibm.icu.text.SimpleDateFormat;

public class BaseRestUtil {
	FranconnectUtil fc = new FranconnectUtil();

	// Change Date format to MM/dd/yyyy from yyyy-MM-dd
	protected String changeFormat(String date) {
		Date date1;
		String mdy = null;
		try {
			date1 = new SimpleDateFormat("yyyy-MM-dd").parse(date);
			SimpleDateFormat sdm = new SimpleDateFormat("MM/dd/yyyy");
			mdy = sdm.format(date1);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return mdy;
	}

	// public static Map<String, String> ReferenceStatus_keys = new
	// HashMap<String, String>();
	protected boolean ActivityPerform(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			Map<String, String> UniqueKey, String Module, String SubModule, String Operation, String Divtag) {
		String secureToken = GetSecureToken(driver, dataSet);
		String encToken = GetEcryptedToken(driver, dataSet, secureToken);
		String Query = GetQuery(driver, dataSet, encToken, null, Module, SubModule, Operation);
		String ResultantQuery = GetResultantQuery_R(Query, dataSet, UniqueKey);
		boolean CreateSubModule = CRUDByQuery(driver, dataSet, config, encToken, ResultantQuery, Module, SubModule,
				Operation, Divtag, DataServicePage.ReferenceStatus_keys);
		if (CreateSubModule) {
			System.out.println(" Contract Signing Sub Module created Successfully for Info Manager");
			return true;
		} else {
			System.out.println(" Contract Signing Sub Module Has not been created for Info Manager");
		}
		System.out.println("End of your Program");
		return false;
	}

	protected boolean ActivityPerform(WebDriver driver, Map<String, String> dataSet, Map<String, String> UniqueKey,
			String Module, String SubModule, String Operation, String Divtag, String testcaseid) throws Exception {

		driver.navigate().to(FranconnectUtil.config.get("buildUrl") + DataServicePage.restURL);
		;
		String secureToken = GetSecureToken(driver, dataSet);
		;
		String encToken = GetEcryptedToken(driver, dataSet, secureToken);
		;
		String Query = GetQuery(driver, dataSet, encToken, null, Module, SubModule, Operation);
		;
		String ResultantQuery = GetResultantQuery_R(Query, dataSet, UniqueKey);
		;
		boolean CreateSubModule = CRUDByQuery(driver, dataSet, encToken, ResultantQuery, Module, SubModule, Operation,
				Divtag, DataServicePage.ReferenceStatus_keys, testcaseid);
		;
		if (CreateSubModule) {
			System.out.println(" successful");
			return true;
		} else {
			System.out.println(" Problem with rest api " + testcaseid);
		}
		System.out.println("End of your Program");
		return false;

	}

	public boolean ActivityPerformDuplicate(WebDriver driver, Map<String, String> dataSet,
			Map<String, String> UniqueKey, String Module, String SubModule, String Operation, String Divtag,
			String testcaseid) throws Exception {

		driver.navigate().to(FranconnectUtil.config.get("buildUrl") + DataServicePage.restURL);
		;
		String secureToken = GetSecureToken(driver, dataSet);
		;
		String encToken = GetEcryptedToken(driver, dataSet, secureToken);
		;
		String Query = GetQuery(driver, dataSet, encToken, null, Module, SubModule, Operation);
		;
		String ResultantQuery = GetResultantQuery_R(Query, dataSet, UniqueKey);
		;
		boolean CreateSubModule = CRUDByQuery(driver, dataSet, encToken, ResultantQuery, Module, SubModule, Operation,
				Divtag, DataServicePage.ReferenceStatus_keys, testcaseid);
		;
		if (CreateSubModule) {
			System.out.println(" successful");
			return true;
		} else {
			System.out.println(" Problem with rest api " + testcaseid);
		}
		System.out.println("End of your Program");
		return false;

	}

	public boolean CRUDByQuery(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String ecryptedToken, String resultantQuery, String Module, String SubModule, String Operation,
			String Divtag, Map<String, String> ReferenceStatus) {
		// TODO Auto-generated method stub createDiv
		try {
			Select DropDownServiceType = new Select(fc.utobj().getElementByID(driver, "serviceType"));
			DropDownServiceType.selectByVisibleText(Operation);

			fc.utobj().sendKeys(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='" + Divtag + "']//*[@id='key']"), ecryptedToken);
			Select CModule = new Select(
					fc.utobj().getElementByXpath(driver, "//*[@id='" + Divtag + "']//*[@name='module']"));
			CModule.selectByVisibleText(Module);
			Select CSubModule = new Select(
					fc.utobj().getElementByXpath(driver, "//*[@id='" + Divtag + "']//*[@name='subModule']"));
			CSubModule.selectByVisibleText(SubModule);
			if (Divtag.equals("retrieveDiv")) {
				fc.utobj().sendKeys(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='" + Divtag + "']//*[@id='filterXML']"),
						resultantQuery);
			} else {
				fc.utobj().sendKeys(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='" + Divtag + "']//*[@id='xmlString']"),
						resultantQuery);
			}
			clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='" + Divtag + "']//*[@name='Submit']"));

			driver.switchTo().frame("resultIfr");

			String FinalResult = fc.utobj().getElementByTagName(driver, "responseStatus").getText();

			if (!FinalResult.equals("Error"))
				ReferenceStatus.put(SubModule.toString(), driver.findElement(By.tagName("referenceId")).getText());
			else
				ReferenceStatus.put(SubModule.toString(), "");
			driver.switchTo().defaultContent();
			if (FinalResult.equals("Success") || FinalResult.equals("Warning"))
				return true;
			else
				return false;

		} catch (Exception ex) {
			driver.switchTo().defaultContent();
			ex.printStackTrace();
			return false;
		}
	}

	public boolean CRUDByQuery(WebDriver driver, Map<String, String> dataSet, String ecryptedToken,
			String resultantQuery, String Module, String SubModule, String Operation, String Divtag,
			Map<String, String> ReferenceStatus, String testcaseid) {
		// TODO Auto-generated method stub createDiv
		try {
			Select DropDownServiceType = new Select(fc.utobj().getElementByID(driver, "serviceType"));
			DropDownServiceType.selectByVisibleText(Operation);

			fc.utobj().sendKeys(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='" + Divtag + "']//*[@id='key']"), ecryptedToken);
			fc.utobj().sleep(1500);
			Select CModule = new Select(
					fc.utobj().getElementByXpath(driver, "//*[@id='" + Divtag + "']//*[@name='module']"));
			CModule.selectByVisibleText(Module);
			fc.utobj().sleep(1500);
			Select CSubModule = new Select(
					fc.utobj().getElementByXpath(driver, "//*[@id='" + Divtag + "']//*[@name='subModule']"));
			CSubModule.selectByVisibleText(SubModule);
			fc.utobj().sleep(1500);
			if (Divtag.equals("retrieveDiv")) {
				fc.utobj().sendKeys(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='" + Divtag + "']//*[@id='filterXML']"),
						resultantQuery);
			} else {
				fc.utobj().sendKeys(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='" + Divtag + "']//*[@id='xmlString']"),
						resultantQuery);
			}
			fc.utobj().sleep(1500);
			clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='" + Divtag + "']//*[@name='Submit']"));

			driver.switchTo().frame("resultIfr");

			String FinalResult = fc.utobj().getElementByTagName(driver, "responseStatus").getText();
			if (!FinalResult.equals("Error"))
				try {
					ReferenceStatus.put(testcaseid + SubModule.toString(),
							fc.utobj().getElementByTagName(driver, "referenceId").getText());
				} catch (Exception ex) {

				}
			else
				ReferenceStatus.put(SubModule.toString(), "");
			driver.switchTo().defaultContent();
			if (FinalResult.equals("Success") || FinalResult.equals("Warning"))
				return true;
			else
				return false;

		} catch (Exception ex) {
			driver.switchTo().defaultContent();
			ex.printStackTrace();
			return false;
		}
	}

	public String GetResultantQuery_R(String query, Map<String, String> dataSet, Map<String, String> UniqueKey) {
		try {
			String format = query;
			HashMap<String, String> map = new HashMap<String, String>();
			Pattern p = Pattern.compile("<([^\\s>/]+)");
			Matcher m = p.matcher(format);
			int i = 0;
			while (m.find()) {
				String tag = m.group(1);
				map.put(String.valueOf(i), tag);
				i++;
			}
			int j = 2;
			while (i > 0) {

				// System.out.println(map.get(String.valueOf(j)) +
				// ">>>>>>>>>>>>>>>>>>>>>>"+
				// dataSet.get(map.get(String.valueOf(j))));
				if (dataSet.get(map.get(String.valueOf(j))) != null
						|| UniqueKey.get(map.get(String.valueOf(j))) != null) {
					// if (map.get(String.valueOf(j)).equals("userID"))
					if (UniqueKey.get(map.get(String.valueOf(j))) != null) {
						format = format.replaceAll(
								"(?<=<" + map.get(String.valueOf(j)) + ">).*?(?=</" + map.get(String.valueOf(j)) + ">)",
								UniqueKey.get(map.get(String.valueOf(j))));
						// System.out.println(format);
					} else {
						format = format.replaceAll(
								"(?<=<" + map.get(String.valueOf(j)) + ">).*?(?=</" + map.get(String.valueOf(j)) + ">)",
								dataSet.get(map.get(String.valueOf(j))));
					}
				} else {
					format = format.replaceAll(
							"(?<=<" + map.get(String.valueOf(j)) + ">).*?(?=</" + map.get(String.valueOf(j)) + ">)",
							"");
				}
				i--;
				j++;
			}

			// System.out.println(format);
			return format;
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}

	public String GetQuery(WebDriver driver, Map<String, String> dataSet, String ecryptedToken, String roleType,
			String module, String subModule, String operation) {
		// TODO Auto-generated method stub
		try {
			Select DropDownServiceType = new Select(fc.utobj().getElementByID(driver, "serviceType"));
			DropDownServiceType.selectByVisibleText("Query");

			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='queryDiv']//*[@id='key']"),
					ecryptedToken);
			fc.utobj().sleep(1500);
			Select Module = new Select(fc.utobj().getElementByXpath(driver, "//*[@id='queryDiv']//*[@name='module']"));
			Module.selectByVisibleText(module);
			fc.utobj().sleep(1500);
			Select SubModule = new Select(
					fc.utobj().getElementByXpath(driver, "//*[@id='queryDiv']//*[@name='subModule']"));
			SubModule.selectByVisibleText(subModule);
			fc.utobj().sleep(1500);
			Select Operation = new Select(
					fc.utobj().getElementByXpath(driver, "//*[@id='queryDiv']//*[@name='operation']"));
			Operation.selectByVisibleText(operation);
			fc.utobj().sleep(1500);
			if (roleType != null) {
				Select roleTypeForQuery = new Select(
						fc.utobj().getElementByXpath(driver, "//*[@id='queryDiv']//*[@name='roleTypeForQuery']"));
				roleTypeForQuery.selectByVisibleText(roleType);
			}
			clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='queryDiv']//*[@id='Submit']"));

			driver.switchTo().frame("resultIfr");
			String responseData = fc.utobj().getElementByTagName(driver, "responseData").getText();
			String str1 = driver.getPageSource();
			// System.out.println(str1);
			int firstOpenP = str1.indexOf("<fcRequest");
			int lastOpenP = str1.lastIndexOf("fcRequest>");
			String format = str1.substring(firstOpenP, lastOpenP + 10);
			driver.switchTo().defaultContent();
			// System.out.println(format);
			return format;
		} catch (Exception ex) {
			driver.switchTo().defaultContent();
			ex.printStackTrace();
		}
		return null;
	}

	public String GetEcryptedToken(WebDriver driver, Map<String, String> dataSet, String SecureToken) {
		// TODO Auto-generated method stub
		try {
			Select DropDownServiceType = new Select(fc.utobj().getElementByID(driver, "serviceType"));
			DropDownServiceType.selectByVisibleText("Get Encrypted Token");

			DataServicePage de = new DataServicePage(driver);
			fc.utobj().sendKeys(driver, de.SecureToken, SecureToken);
			fc.utobj().sleep(1500);
			fc.utobj().sendKeys(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='getEncTokenDiv']//*[@id='clientCode']"),
					dataSet.get("clientcode"));
			fc.utobj().sleep(1500);
			clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='getEncTokenDiv']//*[@name='Submit']"));
			fc.utobj().sleep(1500);
			driver.switchTo().frame("resultIfr");
			String enctext = fc.utobj().getElementByXpath(driver, ".//html/body").getText();
			// System.out.println(enctext);
			int index = enctext.indexOf(":");
			String str = enctext.substring(index + 2, enctext.length());
			driver.switchTo().defaultContent();
			// System.out.println(str);
			return str;
		} catch (Exception ex) {
			driver.switchTo().defaultContent();
		}
		return null;
	}

	public String GetSecureToken(WebDriver driver, Map<String, String> dataSet) {
		try {
			// driver = gotoDataServicePage(driver, config);
			DataServicePage de = new DataServicePage(driver);

			Select DropDownServiceType = new Select(fc.utobj().getElementByID(driver, "serviceType"));
			DropDownServiceType.selectByVisibleText("Login");

			fc.utobj().sendKeys(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='loginDiv']//*[@id='clientCode']"),
					dataSet.get("clientcode"));
			fc.utobj().sleep(1500);
			Select DropDownResponseType = new Select(fc.utobj().getElementByID(driver, "responseType"));
			DropDownResponseType.selectByVisibleText("XML");
			fc.utobj().sleep(1500);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='loginDiv']//*[@id='Submit']"));

			driver.switchTo().frame("resultIfr");
			fc.utobj().sleep(1500);
			String text = fc.utobj().getElementByTagName(driver, "secureToken").getText();
			driver.switchTo().defaultContent();
			// System.out.println(text);
			return text;
		} catch (Exception e) {
			driver.switchTo().defaultContent();
			e.printStackTrace();
			return null;
		}

	}

	public void clickElement(WebDriver driver, WebElement element) throws Exception {

		fc.utobj().moveToElement(driver, element);
		try {

			element.click();
			// analyzeLog(driver);
		} catch (Exception e) {
			throwsException("Element not clickable : " + element);
		}
	}

	public String throwsException(String exceptionMsg) throws Exception {
		Reporter.log("");
		throw new Exception(exceptionMsg);
	}

}
