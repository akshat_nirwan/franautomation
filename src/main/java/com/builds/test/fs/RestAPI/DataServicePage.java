package com.builds.test.fs.RestAPI;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class DataServicePage {

	// @FindBy(xpath=".//*[@id='serviceType']/option[contains(@value,'login')
	// and text()='Login']")
	@FindBy(xpath = ".//*[@id='serviceType']/option[1]")
	public WebElement ServiceTypeLogin;

	@FindBy(xpath = ".//*[@id='serviceType']")
	public WebElement ServiceType;

	@FindBy(xpath = ".//*[@id='serviceType']/option[contains(@value,'query') and text()='Query']")
	public WebElement ServiceTypeQuery;

	@FindBy(xpath = ".//*[@id='serviceType']/option[contains(@value,'submodule') and text()='Sub Modules']")
	public WebElement ServiceTypeSubModules;

	@FindBy(xpath = ".//*[@id='serviceType']/option[contains(@value,'create') and text()='Create']")
	public WebElement ServiceTypeCreate;

	@FindBy(xpath = ".//*[@id='serviceType']/option[contains(@value,'update') and text()='Update']")
	public WebElement ServiceTypeUpdate;

	@FindBy(xpath = ".//*[@id='serviceType']/option[contains(@value,'retrieve') and text()='Retrieve']")
	public WebElement ServiceTypeRetrieve;

	@FindBy(xpath = ".//*[@id='serviceType']/option[contains(@value,'delete') and text()='Delete']")
	public WebElement ServiceTypeLog;

	@FindBy(xpath = ".//*[@id='serviceType']/option[contains(@value,'log') and text()='Log']")
	public WebElement ServiceTypeLogout;

	@FindBy(xpath = ".//*[@id='serviceType']/option[contains(@value,'getEncToken') and text()='Get Encrypted Token']")
	public WebElement ServiceTypeGetEncryptedToken;

	@FindBy(xpath = ".//*[@id='serviceType']/option[contains(@value,'userlogin') and text()='User Login']")
	public WebElement ServiceTypeUserLogin;

	@FindBy(id = "clientCode")
	public WebElement ClientCode;

	@FindBy(xpath = ".//*[@id='responseType']/option[contains(@value,'xml') and text()='XML']")
	public WebElement ResponseTypexml;

	@FindBy(xpath = ".//*[@id='responseType']/option[contains(@value,'json') and text()='JSON']")
	public WebElement ResponseTypejson;

	@FindBy(id = "Submit")
	public WebElement Submit;

	@FindBy(id = "secureToken")
	public WebElement SecureToken;

	public static final String PrimaryInfo = "Primary Info";
	public static final String AccountInfo = "Account Info";
	public static final String OpportunityInformation = "Opportunity Information";
	public static final String Create = "Create";
	public static final String Update = "Update";
	public static final String Retrieve = "Retrieve";
	public static final String Delete = "Delete";
	public static final String CoApplicants = "Co-Applicants";
	public static final String PersonalProfile = "Personal Profile";
	public static final String QualificationDetails = "Qualification Details";
	public static final String Compliance = "Compliance";
	public static final String RealEstate = "Real Estate";
	public static final String Visit = "Visit";
	public static final String InfoMgr = "Info Mgr";
	public static final String CRM = "CM";
	public static final String FS = "FS";
	public static final String LeadInfo = "Lead Info";
	public static final String LeadCall = "Lead - Call";
	public static final String LeadEmail = "Lead Email";
	public static final String LeadRemarks = "Lead - Remarks";
	public static final String LeadTask = "Lead - Task";
	public static final String Agreement = "Agreement";
	public static final String Call = "Call";
	public static final String CenterInfo = "Center Info";
	public static final String CustomerComplaints = "Customer Complaints";
	public static final String ContractSigning = "Contract Signing";
	public static final String Employees = "Employees";
	public static final String EntityDetails = "Entity Details";
	public static final String Events = "Events";
	public static final String Financial = "Financial";
	public static final String Franchisee = "Franchisee";
	public static final String Guarantors = "Guarantors";
	public static final String Insurance = "Insurance";
	public static final String LegalViolation = "Legal Violation";
	public static final String Lenders = "Lenders";
	public static final String ExternalMail = "External Mail";
	public static final String Marketing = "Marketing";
	public static final String MysteryReview = "Mystery Review";
	public static final String OutlookEmails = "Outlook Emails";
	public static final String Owners = "Owners";
	public static final String QAHistory = "QA History";
	public static final String Remark = "Remark";
	public static final String Renewal = "Renewal";
	public static final String Task = "Task";
	public static final String Territory = "Territory";
	public static final String Training = "Training";
	public static final String queryDiv = "queryDiv";
	public static final String retrieveDiv = "retrieveDiv";
	public static final String updateDiv = "updateDiv";
	public static final String deleteDiv = "deleteDiv";
	public static final String createDiv = "createDiv";
	public static final String Admin = "Admin";
	public static final String User = "User";
	public static final String exepath = "C:\\Selenium_Test_Input\\exe\\geckodriver.exe";
	public static Map<String, String> ReferenceStatus_keys = new HashMap<String, String>();

	public static String emails = "Emails";
	public static final String restURL = "dataservices/dataServicesTest.jsp";
	public static final String buildURL = "http://cluster.franconnect.net/franconnect/dataservices/dataServicesTest.jsp";

	public DataServicePage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
