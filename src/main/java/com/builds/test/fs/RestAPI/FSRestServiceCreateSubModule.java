package com.builds.test.fs.RestAPI;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import org.xml.sax.SAXException;

import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class FSRestServiceCreateSubModule {

	FranconnectUtil fc = new FranconnectUtil();
	FsRestService_CRUDSubModule fcServices = new FsRestService_CRUDSubModule();
	FsRestServicesAction fcService = new FsRestServicesAction();

	@Test(groups = { "salesRestAPI", "FCRestAPI" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-07-18", testCaseDescription = "Verify The Create Primary info In FS module with Rest API", testCaseId = "TC_18_Sales_C_PrimaryInfoWithAction")
	public void CreatePrimaryInfo() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("salesRestAPI", testCaseId);
		Map<String, String> UniqueKey_PrimaryInfo = new HashMap<String, String>();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			fc.utobj().printTestStep("Navigating to FS > RestAPI > Adding Franchisee");
			boolean createPrimaryInfo = fcService.CreatePrimaryInfo(driver, dataSet, testCaseId, UniqueKey_PrimaryInfo);
			boolean ActionCreated = false;
			if (createPrimaryInfo) {
				fc.utobj().printTestStep("Navigating to FS > View Page > Verifying Franchisee");
				UniqueKey_PrimaryInfo.remove("referenceId");
				UniqueKey_PrimaryInfo.remove("parentReferenceId");
				ActionCreated = fcService.ValidatePrimaryInfo(driver, config, UniqueKey_PrimaryInfo);
			} else {
				fc.utobj().throwsException("Primary info is not created in Rest API with testcaseid " + testCaseId);
			}
			if (!ActionCreated)
				fc.utobj().throwsException("Primary info is not on View Page with testcaseid " + testCaseId);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception ex) {
			fc.utobj().quitBrowserOnCatch(driver, ex, testCaseId);
		}
	}

	@Test(groups = { "salesRestAPI", "FCRestAPI" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-07-18", testCaseDescription = "Verify The Create Primary info With Co-Applicant In FS module with Rest API", testCaseId = "TC_SalesRestAPIPrimaryInfoWithCoApplicant")
	public void CreatePrimaryInfoWithCoApplicantSubModule()
			throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("salesRestAPI", testCaseId);
		Map<String, String> UniqueKey_PrimaryInfo = new HashMap<String, String>();
		Map<String, String> UniqueKey_CoApplicant = new HashMap<String, String>();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			fc.utobj().printTestStep("Navigating to FS > RestAPI > Adding Franchisee");
			boolean createPrimaryInfo = fcService.CreatePrimaryInfo(driver, dataSet, testCaseId, UniqueKey_PrimaryInfo);
			fc.utobj().printTestStep("Navigating to FS > RestAPI > Adding Co-Applicant");
			boolean createCoApplicant = fcService.CreateCoApplicant(driver, dataSet, config, testCaseId,
					UniqueKey_CoApplicant);
			boolean ActionCreated = false;
			boolean CoApplicant = false;
			if (createPrimaryInfo && createCoApplicant) {
				fc.utobj().printTestStep("Navigating to FS > View Page > Verifying Franchisee");
				UniqueKey_PrimaryInfo.remove("referenceId");
				UniqueKey_PrimaryInfo.remove("parentReferenceId");
				ActionCreated = fcService.ValidatePrimaryInfo(driver, config, UniqueKey_PrimaryInfo);
			} else {
				fc.utobj().throwsException(
						"Primary info or Co-Applicant is not created in Rest API with testcaseid " + testCaseId);
			}
			if (!ActionCreated) {
				fc.utobj().throwsException("Primary info is not on View Page with testcaseid " + testCaseId);
			} else {
				fc.utobj().printTestStep("Navigating to FS > View Page > Verifying CoApplicant");
				CoApplicant = fcService.ValidateCoApplicant(driver, config, UniqueKey_CoApplicant);
				if (!CoApplicant)
					fc.utobj().throwsException("CoApplicant not found on View page " + testCaseId);
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception ex) {
			fc.utobj().quitBrowserOnCatch(driver, ex, testCaseId);
		}
	}
	//// =============================================================================Call
	//// Module========================================================================

	@Test(groups = { "salesRestAPI", "FCRestAPI" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-07-18", testCaseDescription = "Verify The Create Primary info With Call In FS module with Rest API", testCaseId = "TC_SalesRestAPIPrimaryInfoWithCall")
	public void CreatePrimaryInfoWithCallSubModule()
			throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("salesRestAPI", testCaseId);
		Map<String, String> UniqueKey_PrimaryInfo = new HashMap<String, String>();
		Map<String, String> UniqueKey_Call = new HashMap<String, String>();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			fc.utobj().printTestStep("Navigating to FS > RestAPI > Adding Franchisee");
			boolean createPrimaryInfo = fcService.CreatePrimaryInfo(driver, dataSet, testCaseId, UniqueKey_PrimaryInfo);
			fc.utobj().printTestStep("Navigating to FS > RestAPI > Adding Call");
			boolean ActionCreated = fcService.CreateCall(driver, dataSet, config, testCaseId, UniqueKey_Call);
			if (createPrimaryInfo) {
				fc.utobj().printTestStep("Navigating to FS > View Page > Verifying Franchisee");
				UniqueKey_PrimaryInfo.remove("referenceId");
				UniqueKey_PrimaryInfo.remove("parentReferenceId");
				ActionCreated = fcService.ValidatePrimaryInfo(driver, config, UniqueKey_PrimaryInfo);
			} else {
				fc.utobj().throwsException("Primary info is not created in Rest API with testcaseid " + testCaseId);
			}
			if (!ActionCreated) {
				fc.utobj().throwsException("Call Action is not on View Page with testcaseid " + testCaseId);
			} else {
				fc.utobj().printTestStep("Navigating to FS > View Page > Verifying Call");
				fcService.ValidateCall(driver, config, UniqueKey_Call, UniqueKey_PrimaryInfo);
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception ex) {
			fc.utobj().quitBrowserOnCatch(driver, ex, testCaseId);
		}
	}
	//// =============================================================================Compliance
	//// Module========================================================================

	@Test(groups = { "salesRestAPI", "FCRestAPI" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-07-18", testCaseDescription = "Verify The Create Primary info with Compilance In FS module with Rest API", testCaseId = "TC_SalesRestAPIPrimaryInfoWithCompliance")
	public void CreatePrimaryInfoWithComplianceSubModule()
			throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("salesRestAPI", testCaseId);
		Map<String, String> UniqueKey_PrimaryInfo = new HashMap<String, String>();
		Map<String, String> UniqueKey_Compliance = new HashMap<String, String>();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			fc.utobj().printTestStep("Navigating to FS > RestAPI > Adding Franchisee");
			boolean createPrimaryInfo = fcService.CreatePrimaryInfo(driver, dataSet, testCaseId, UniqueKey_PrimaryInfo);
			fc.utobj().printTestStep("Navigating to FS > RestAPI > Adding Compliance");
			boolean ActionCreated = fcService.CreateCompliance(driver, dataSet, config, testCaseId,
					UniqueKey_Compliance);
			if (createPrimaryInfo) {
				fc.utobj().printTestStep("Navigating to FS > View Page > Verifying Franchisee");
				UniqueKey_PrimaryInfo.remove("referenceId");
				UniqueKey_PrimaryInfo.remove("parentReferenceId");
				ActionCreated = fcService.ValidatePrimaryInfo(driver, config, UniqueKey_PrimaryInfo);
			} else {
				fc.utobj().throwsException("Primary info is not created in Rest API with testcaseid " + testCaseId);
			}
			if (!ActionCreated) {
				fc.utobj().throwsException("Call Action is not on View Page with testcaseid " + testCaseId);
			} else {
				fc.utobj().printTestStep("Navigating to FS > View Page > Verifying Compilance");
				fcService.ValidateCompliance(driver, config, UniqueKey_Compliance, UniqueKey_PrimaryInfo);
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception ex) {
			fc.utobj().quitBrowserOnCatch(driver, ex, testCaseId);
		}
	}
	//// =============================================================================Email
	//// Module========================================================================

	@Test(groups = { "salesRestAPI", "FCRestAPI" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-07-18", testCaseDescription = "Verify The Create Primary info In FS module with Rest API", testCaseId = "TC_SalesRestAPIPrimaryInfoWithEmail")
	public void CreatePrimaryInfoWithEmailsSubModules()
			throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("salesRestAPI", testCaseId);
		Map<String, String> UniqueKey_PrimaryInfo = new HashMap<String, String>();
		Map<String, String> UniqueKey_Emails = new HashMap<String, String>();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			fc.utobj().printTestStep("Navigating to FS > RestAPI > Adding Franchisee");
			boolean createPrimaryInfo = fcService.CreatePrimaryInfo(driver, dataSet, testCaseId, UniqueKey_PrimaryInfo);
			fc.utobj().printTestStep("Navigating to FS > RestAPI > Adding Email for Franchisee");
			boolean ActionCreated = fcService.CreateEmail(driver, dataSet, config, testCaseId, UniqueKey_Emails);
			if (createPrimaryInfo) {
				fc.utobj().printTestStep("Navigating to FS > View Page > Verifying Franchisee");
				UniqueKey_PrimaryInfo.remove("referenceId");
				UniqueKey_PrimaryInfo.remove("parentReferenceId");
				ActionCreated = fcService.ValidatePrimaryInfo(driver, config, UniqueKey_PrimaryInfo);
			} else {
				fc.utobj().throwsException("Primary info is not created in Rest API with testcaseid " + testCaseId);
			}
			if (!ActionCreated) {
				fc.utobj().throwsException("Call Action is not on View Page with testcaseid " + testCaseId);
			} else {
				fc.utobj().printTestStep("Navigating to FS > View Page > Verifying Email");
				// fcService.ValidateEmail(driver, config,
				// UniqueKey_Emails,UniqueKey_PrimaryInfo);
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception ex) {
			fc.utobj().quitBrowserOnCatch(driver, ex, testCaseId);
		}
	}
	//// =============================================================================Personal
	//// Profile
	//// Module========================================================================//

	@Test(groups = { "salesRestAPI", "FCRestAPI", "test" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-07-18", testCaseDescription = "Verify The Create Primary info With PersonalProfile In FS module with Rest API", testCaseId = "TC_SalesRestAPIPrimaryInfoWithPersonalProfile")
	public void CreatePrimaryInfoWithPersonalProfileSubModule()
			throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("salesRestAPI", testCaseId);
		Map<String, String> UniqueKey_PrimaryInfo = new HashMap<String, String>();
		Map<String, String> UniqueKey_PersonalProfile = new HashMap<String, String>();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			fc.utobj().printTestStep("Navigating to FS > RestAPI > Adding Franchisee");
			boolean createPrimaryInfo = fcService.CreatePrimaryInfo(driver, dataSet, testCaseId, UniqueKey_PrimaryInfo);
			fc.utobj().printTestStep("Navigating to FS > RestAPI > Adding PersoanlProfile for Franchisee");
			boolean ActionCreated = fcService.CreatePersonalProfile(driver, dataSet, config, testCaseId,
					UniqueKey_PersonalProfile); // CreateEmail( driver, dataSet,
												// config,
												// testCaseId,UniqueKey_Emails);
			if (createPrimaryInfo) {
				fc.utobj().printTestStep("Navigating to FS > View Page > Verifying Franchisee");
				UniqueKey_PrimaryInfo.remove("referenceId");
				UniqueKey_PrimaryInfo.remove("parentReferenceId");
				ActionCreated = fcService.ValidatePrimaryInfo(driver, config, UniqueKey_PrimaryInfo);
			} else {
				fc.utobj().throwsException("Primary info is not created in Rest API with testcaseid " + testCaseId);
			}
			if (!ActionCreated) {
				fc.utobj().throwsException("PersonalProfile Action is not on View Page with testcaseid " + testCaseId);
			} else {
				fc.utobj().printTestStep("Navigating to FS > View Page > Verifying PersonalProfile");
				fcService.ValidatePersonalProfile(driver, config, UniqueKey_PersonalProfile, UniqueKey_PrimaryInfo);//
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception ex) {
			fc.utobj().quitBrowserOnCatch(driver, ex, testCaseId);
		}
	}

	//// =============================================================================Qualification
	//// Detail
	//// Module========================================================================//

	@Test(groups = { "salesRestAPI", "FCRestAPI" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-07-18", testCaseDescription = "Verify The Create Primary info In FS module with Rest API", testCaseId = "TC_SalesRestAPIPrimaryInfoWithQualificationDetails")
	public void CreatePrimaryInfoWithQualificationDetailsSubModule()
			throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("salesRestAPI", testCaseId);
		Map<String, String> UniqueKey_PrimaryInfo = new HashMap<String, String>();
		Map<String, String> UniqueKey_QualificationDetails = new HashMap<String, String>();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			fc.utobj().printTestStep("Navigating to FS > RestAPI > Adding Franchisee");
			boolean createPrimaryInfo = fcService.CreatePrimaryInfo(driver, dataSet, testCaseId, UniqueKey_PrimaryInfo);
			fc.utobj().printTestStep("Navigating to FS > RestAPI > Adding Qualification Details for Franchisee");
			boolean ActionCreated = fcService.CreateQualificationDetail(driver, dataSet, config, testCaseId,
					UniqueKey_QualificationDetails);
			if (createPrimaryInfo) {
				fc.utobj().printTestStep("Navigating to FS > View Page > Verifying Franchisee");
				UniqueKey_PrimaryInfo.remove("referenceId");
				UniqueKey_PrimaryInfo.remove("parentReferenceId");
				ActionCreated = fcService.ValidatePrimaryInfo(driver, config, UniqueKey_PrimaryInfo);
			} else {
				fc.utobj().throwsException("Primary info is not created in Rest API with testcaseid " + testCaseId);
			}
			if (!ActionCreated) {
				fc.utobj().throwsException(
						"Qualification Details Action is not on View Page with testcaseid " + testCaseId);
			} else {
				fc.utobj().printTestStep("Navigating to FS > View Page > Verifying QualificationDetails");
				fcService.ValidateQualificationDetails(driver, config, UniqueKey_QualificationDetails,
						UniqueKey_PrimaryInfo);//
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception ex) {
			fc.utobj().quitBrowserOnCatch(driver, ex, testCaseId);
		}
	}

	// =============================================================================Real
	// Estate
	// Module========================================================================//

	@Test(groups = { "salesRestAPI", "FCRestAPI" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-07-18", testCaseDescription = "Verify The Create Primary info In FS module with Rest API", testCaseId = "TC_SalesRestAPIPrimaryInfoWithRealEstate")
	public void CreatePrimaryInfoWithRealEstateSubModule()
			throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("salesRestAPI", testCaseId);
		Map<String, String> UniqueKey_RealEstate = new HashMap<String, String>();
		Map<String, String> UniqueKey_PrimaryInfo = new HashMap<String, String>();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			fc.utobj().printTestStep("Navigating to FS > RestAPI > Adding Franchisee");
			boolean createPrimaryInfo = fcService.CreatePrimaryInfo(driver, dataSet, testCaseId, UniqueKey_PrimaryInfo);
			fc.utobj().printTestStep("Navigating to FS > RestAPI > Adding Real Estate for Franchisee");
			boolean ActionCreated = fcService.CreateRealEstate(driver, dataSet, config, testCaseId,
					UniqueKey_RealEstate);
			if (createPrimaryInfo) {
				fc.utobj().printTestStep("Navigating to FS > View Page > Verifying Franchisee");
				UniqueKey_PrimaryInfo.remove("referenceId");
				UniqueKey_PrimaryInfo.remove("parentReferenceId");
				ActionCreated = fcService.ValidatePrimaryInfo(driver, config, UniqueKey_PrimaryInfo);
			} else {
				fc.utobj().throwsException("Primary info is not created in Rest API with testcaseid " + testCaseId);
			}
			if (!ActionCreated) {
				fc.utobj().throwsException("RealEstate Action is not on View Page with testcaseid " + testCaseId);
			} else {
				fc.utobj().printTestStep("Navigating to FS > View Page > Verifying Real Estate");
				fcService.ValidateRealEstate(driver, config, UniqueKey_RealEstate, UniqueKey_PrimaryInfo);//
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception ex) {
			fc.utobj().quitBrowserOnCatch(driver, ex, testCaseId);
		}
	}
	// =============================================================================RemarkModule========================================================================//

	@Test(groups = { "salesRestAPI", "FCRestAPI" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-07-18", testCaseDescription = "Verify The Create Primary info In FS module with Rest API", testCaseId = "TC_SalesRestAPIPrimaryInfoWithRemark")
	public void CreatePrimaryInfoWithRemarkSubModule()
			throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("salesRestAPI", testCaseId);
		Map<String, String> UniqueKey_PrimaryInfo = new HashMap<String, String>();
		Map<String, String> UniqueKey_Remarks = new HashMap<String, String>();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			fc.utobj().printTestStep("Navigating to FS > RestAPI > Adding Franchisee");
			boolean createPrimaryInfo = fcService.CreatePrimaryInfo(driver, dataSet, testCaseId, UniqueKey_PrimaryInfo);
			fc.utobj().printTestStep("Navigating to FS > RestAPI > Adding Remark for Franchisee");
			boolean ActionCreated = fcService.CreateRemarks(driver, dataSet, config, testCaseId, UniqueKey_Remarks);
			if (createPrimaryInfo) {
				fc.utobj().printTestStep("Navigating to FS > View Page > Verifying Franchisee");
				UniqueKey_PrimaryInfo.remove("referenceId");
				UniqueKey_PrimaryInfo.remove("parentReferenceId");
				ActionCreated = fcService.ValidatePrimaryInfo(driver, config, UniqueKey_PrimaryInfo);
			} else {
				fc.utobj().throwsException("Primary info is not created in Rest API with testcaseid " + testCaseId);
			}
			if (!ActionCreated) {
				fc.utobj().throwsException("Remarks Action is not on View Page with testcaseid " + testCaseId);
			} else {
				fc.utobj().printTestStep("Navigating to FS > View Page > Verifying Remarks");
				fcService.ValidateRemarks(driver, config, UniqueKey_Remarks, UniqueKey_PrimaryInfo);//
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception ex) {
			fc.utobj().quitBrowserOnCatch(driver, ex, testCaseId);
		}
	}
	// =============================================================================Task
	// Module========================================================================//

	@Test(groups = { "salesRestAPI", "FCRestAPI", "test" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-07-18", testCaseDescription = "Verify The Create Primary info In FS module with Rest API", testCaseId = "TC_SalesRestAPIPrimaryInfoWithTask")
	public void CreatePrimaryInfoWithTaskSubModule()
			throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("salesRestAPI", testCaseId);
		Map<String, String> UniqueKey_PrimaryInfo = new HashMap<String, String>();
		Map<String, String> UniqueKey_Tasks = new HashMap<String, String>();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			fc.utobj().printTestStep("Navigating to FS > RestAPI > Adding Franchisee");
			boolean createPrimaryInfo = fcService.CreatePrimaryInfo(driver, dataSet, testCaseId, UniqueKey_PrimaryInfo);
			fc.utobj().printTestStep("Navigating to FS > RestAPI > Adding Tasks for Franchisee");
			boolean ActionCreated = fcService.CreateTasks(driver, dataSet, config, testCaseId, UniqueKey_Tasks);
			if (createPrimaryInfo) {
				fc.utobj().printTestStep("Navigating to FS > View Page > Verifying Franchisee");
				UniqueKey_PrimaryInfo.remove("referenceId");
				UniqueKey_PrimaryInfo.remove("parentReferenceId");
				ActionCreated = fcService.ValidatePrimaryInfo(driver, config, UniqueKey_PrimaryInfo);
			} else {
				fc.utobj().throwsException("Primary info is not created in Rest API with testcaseid " + testCaseId);
			}
			if (!ActionCreated) {
				fc.utobj().throwsException("Tasks Action is not on View Page with testcaseid " + testCaseId);
			} else {
				fc.utobj().printTestStep("Navigating to FS > View Page > Verifying Tasks");
				fcService.ValidateTasks(driver, config, UniqueKey_Tasks, UniqueKey_PrimaryInfo);//
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception ex) {
			fc.utobj().quitBrowserOnCatch(driver, ex, testCaseId);
		}
	}
	// =============================================================================Visit
	// Module========================================================================//

	@Test(groups = { "salesRestAPI", "FCRestAPI", "test" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-07-18", testCaseDescription = "Verify The Create Primary info In FS module with Rest API", testCaseId = "TC_SalesRestAPIPrimaryInfoWithVisit")
	public void CreatePrimaryInfoWithVisitSubModule()
			throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("salesRestAPI", testCaseId);
		Map<String, String> UniqueKey_PrimaryInfo = new HashMap<String, String>();
		Map<String, String> UniqueKey_Visits = new HashMap<String, String>();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			fc.utobj().printTestStep("Navigating to FS > RestAPI > Adding Franchisee");
			boolean createPrimaryInfo = fcService.CreatePrimaryInfo(driver, dataSet, testCaseId, UniqueKey_PrimaryInfo);
			fc.utobj().printTestStep("Navigating to FS > RestAPI > Adding Remark for Franchisee");
			boolean ActionCreated = fcService.CreateVisit(driver, dataSet, config, testCaseId, UniqueKey_Visits);
			if (createPrimaryInfo) {
				fc.utobj().printTestStep("Navigating to FS > View Page > Verifying Franchisee");
				UniqueKey_PrimaryInfo.remove("referenceId");
				UniqueKey_PrimaryInfo.remove("parentReferenceId");
				ActionCreated = fcService.ValidatePrimaryInfo(driver, config, UniqueKey_PrimaryInfo);
			} else {
				fc.utobj().throwsException("Primary info is not created in Rest API with testcaseid " + testCaseId);
			}
			if (!ActionCreated) {
				fc.utobj().throwsException("Visits Action is not on View Page with testcaseid " + testCaseId);
			} else {
				fc.utobj().printTestStep("Navigating to FS > View Page > Verifying Visits");
				fcService.ValidateVisits(driver, config, UniqueKey_Visits, UniqueKey_PrimaryInfo);//
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception ex) {
			fc.utobj().quitBrowserOnCatch(driver, ex, testCaseId);
		}
	}

}
