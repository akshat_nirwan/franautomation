package com.builds.test.fs.RestAPI;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import org.xml.sax.SAXException;

import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class FSRestServiceDeleteSubModule {

	FranconnectUtil fc = new FranconnectUtil();
	FsRestService_CRUDSubModule fcServices = new FsRestService_CRUDSubModule();
	FsRestServicesAction fcService = new FsRestServicesAction();

	@Test(groups = { "salesRestAPI", "FCRestAPI" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-07-18", testCaseDescription = "Verify Deleted Primary info In FS module withRest API", testCaseId = "TC_18_Sales_Update_PrimaryInfoWithActionDelete")
	public void DeletePrimaryInfo() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("salesRestAPI", testCaseId);
		Map<String, String> UniqueKey_PrimaryInfo = new HashMap<String, String>();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			fc.utobj().printTestStep("Navigating to FS > RestAPI > Adding Franchisee");
			boolean createPrimaryInfo = fcService.CreatePrimaryInfo(driver, dataSet, testCaseId, UniqueKey_PrimaryInfo);
			fc.utobj().printTestStep("Navigating to FS > RestAPI > Deleting Franchisee");
			boolean updatePrimaryInfo = fcService.deletePrimaryInfo(driver, dataSet, config, testCaseId,
					UniqueKey_PrimaryInfo);
			boolean ActionCreated = false;
			if (createPrimaryInfo && updatePrimaryInfo) {
				fc.utobj().printTestStep("Navigating to FS > View Page > Verifying Franchisee");
				UniqueKey_PrimaryInfo.remove("referenceId");
				UniqueKey_PrimaryInfo.remove("parentReferenceId");
				ActionCreated = fcService.ValidatePrimaryInfo(driver, config, UniqueKey_PrimaryInfo);
			} else {
				fc.utobj().throwsException("Primary info is not created in Rest API with testcaseid " + testCaseId);
			}
			if (ActionCreated) {
				fc.utobj().throwsException("Primary info is found on View Page with testcaseid " + testCaseId);
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception ex) {
			fc.utobj().quitBrowserOnCatch(driver, ex, testCaseId);
		}
	}

	@Test(groups = { "salesRestAPI", "FCRestAPI" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-07-18", testCaseDescription = "Verify Primary info With Deleted Co-Applicant In FS module with Rest API", testCaseId = "TC_SalesRestAPIPrimaryInfoWithCoApplicant_Delete")
	public void DeleteSubModuleofPrimaryInfoWithCoApplicantSubModule()
			throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("salesRestAPI", testCaseId);
		Map<String, String> UniqueKey_PrimaryInfo = new HashMap<String, String>();
		Map<String, String> UniqueKey_CoApplicant = new HashMap<String, String>();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			fc.utobj().printTestStep("Navigating to FS > RestAPI > Adding Franchisee");
			boolean createPrimaryInfo = fcService.CreatePrimaryInfo(driver, dataSet, testCaseId, UniqueKey_PrimaryInfo);
			fc.utobj().printTestStep("Navigating to FS > RestAPI > Adding Co-Applicant");
			fcService.CreateCoApplicant(driver, dataSet, config, testCaseId, UniqueKey_CoApplicant);
			fc.utobj().printTestStep("Navigating to FS > RestAPI > Deleting Co-Applicant");
			fcService.DeleteCoApplicant(driver, dataSet, config, testCaseId, UniqueKey_CoApplicant);
			boolean ActionCreated = false;
			boolean CoApplicant = false;
			if (createPrimaryInfo) {
				fc.utobj().printTestStep("Navigating to FS > View Page > Verifying Franchisee");
				UniqueKey_PrimaryInfo.remove("referenceId");
				UniqueKey_PrimaryInfo.remove("parentReferenceId");
				ActionCreated = fcService.ValidatePrimaryInfo(driver, config, UniqueKey_PrimaryInfo);
			} else {
				fc.utobj().throwsException(
						"Primary info or Co-Applicant is not created in Rest API with testcaseid " + testCaseId);
			}
			if (!ActionCreated) {
				fc.utobj().throwsException("Primary info is not on View Page with testcaseid " + testCaseId);
			} else {
				fc.utobj().printTestStep("Navigating to FS > View Page > Verifying CoApplicant");
				CoApplicant = fcService.ValidateCoApplicant(driver, config, UniqueKey_CoApplicant);
				if (CoApplicant)
					fc.utobj().throwsException("CoApplicant found on View page " + testCaseId);
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception ex) {
			fc.utobj().quitBrowserOnCatch(driver, ex, testCaseId);
		}
	}
	//// =============================================================================Call
	//// Module========================================================================

	@Test(groups = { "salesRestAPI", "FCRestAPI", "test" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-07-18", testCaseDescription = "Verify Primary info With Deleted Call In FS module with Rest API", testCaseId = "TC_SalesRestAPIPrimaryInfoWithCall_Delete")
	public void DeleteSubModuleofPrimaryInfoWithCallSubModule()
			throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("salesRestAPI", testCaseId);
		Map<String, String> UniqueKey_PrimaryInfo = new HashMap<String, String>();
		Map<String, String> UniqueKey_Call = new HashMap<String, String>();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			fc.utobj().printTestStep("Navigating to FS > RestAPI > Adding Franchisee");
			boolean createPrimaryInfo = fcService.CreatePrimaryInfo(driver, dataSet, testCaseId, UniqueKey_PrimaryInfo);
			fc.utobj().printTestStep("Navigating to FS > RestAPI > Adding Call");
			boolean ActionCreated = fcService.CreateCall(driver, dataSet, config, testCaseId, UniqueKey_Call);
			boolean validatecall = false;
			fc.utobj().printTestStep("Navigating to FS > RestAPI > Deleting Call");
			ActionCreated = fcService.DeleteCall(driver, dataSet, config, testCaseId, UniqueKey_Call);
			if (createPrimaryInfo) {
				fc.utobj().printTestStep("Navigating to FS > View Page > Verifying Franchisee");
				UniqueKey_PrimaryInfo.remove("referenceId");
				UniqueKey_PrimaryInfo.remove("parentReferenceId");
				ActionCreated = fcService.ValidatePrimaryInfo(driver, config, UniqueKey_PrimaryInfo);
			} else {
				fc.utobj().throwsException("Primary info is not created in Rest API with testcaseid " + testCaseId);
			}
			if (!ActionCreated) {
				fc.utobj().throwsException("Call Action is not on View Page with testcaseid " + testCaseId);
			} else {
				fc.utobj().printTestStep("Navigating to FS > View Page > Verifying Call");
				validatecall = fcService.ValidateCall(driver, config, UniqueKey_Call, UniqueKey_PrimaryInfo);
			}
			if (validatecall) {
				fc.utobj().throwsException("Call details found on view page");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception ex) {
			fc.utobj().quitBrowserOnCatch(driver, ex, testCaseId);
		}
	}
	//// =============================================================================Compliance
	//// Module========================================================================

	@Test(groups = { "salesRestAPI", "FCRestAPI" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-07-18", testCaseDescription = "Verify Primary info With Deleted Compilance In FS module with Rest API", testCaseId = "TC_SalesRestAPIPrimaryInfoWithCompliance_Delete")
	public void DeleteSubModuleofPrimaryInfoWithComplianceSubModule()
			throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("salesRestAPI", testCaseId);
		Map<String, String> UniqueKey_PrimaryInfo = new HashMap<String, String>();
		Map<String, String> UniqueKey_Compliance = new HashMap<String, String>();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			fc.utobj().printTestStep("Navigating to FS > RestAPI > Adding Franchisee");
			boolean createPrimaryInfo = fcService.CreatePrimaryInfo(driver, dataSet, testCaseId, UniqueKey_PrimaryInfo);
			fc.utobj().printTestStep("Navigating to FS > RestAPI > Adding Compliance");
			boolean ActionCreated = fcService.CreateCompliance(driver, dataSet, config, testCaseId,
					UniqueKey_Compliance);
			fc.utobj().printTestStep("Navigating to FS > RestAPI > Deleting Compliance");
			ActionCreated = fcService.DeleteCompliance(driver, dataSet, config, testCaseId, UniqueKey_Compliance);
			if (createPrimaryInfo) {
				fc.utobj().printTestStep("Navigating to FS > View Page > Verifying Franchisee");
				UniqueKey_PrimaryInfo.remove("referenceId");
				UniqueKey_PrimaryInfo.remove("parentReferenceId");
				ActionCreated = fcService.ValidatePrimaryInfo(driver, config, UniqueKey_PrimaryInfo);
			} else {
				fc.utobj().throwsException("Primary info is not created in Rest API with testcaseid " + testCaseId);
			}
			if (!ActionCreated) {
				fc.utobj().throwsException("Call Action is not on View Page with testcaseid " + testCaseId);
			} else {
				fc.utobj().printTestStep("Navigating to FS > View Page > Verifying Compilance");
				ActionCreated = fcService.ValidateCompliance(driver, config, UniqueKey_Compliance,
						UniqueKey_PrimaryInfo);
			}
			if (ActionCreated) {
				fc.utobj().throwsException("Compilance found on view page ");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception ex) {
			fc.utobj().quitBrowserOnCatch(driver, ex, testCaseId);
		}
	}
	//// =============================================================================Email
	//// Module========================================================================
	// email module updation is not supported by the functionality
	//// =============================================================================Personal
	//// Profile
	//// Module========================================================================//

	@Test(groups = { "salestest123", "salesRestAPI", "test" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-07-18", testCaseDescription = "Verify Primary info With Deleted PersonalProfile In FS module with Rest API", testCaseId = "TC_SalesRestAPIPrimaryInfoWithPersonalProfile_Delete")
	public void DeleteSubModuleofPrimaryInfoWithPersonalProfileSubModule()
			throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("salesRestAPI", testCaseId);
		Map<String, String> UniqueKey_PrimaryInfo = new HashMap<String, String>();
		Map<String, String> UniqueKey_PersonalProfile = new HashMap<String, String>();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			fc.utobj().printTestStep("Navigating to FS > RestAPI > Adding Franchisee");
			boolean createPrimaryInfo = fcService.CreatePrimaryInfo(driver, dataSet, testCaseId, UniqueKey_PrimaryInfo);
			fc.utobj().printTestStep("Navigating to FS > RestAPI > Adding PersoanlProfile for Franchisee");
			boolean ActionCreated = fcService.CreatePersonalProfile(driver, dataSet, config, testCaseId,
					UniqueKey_PersonalProfile); // CreateEmail( driver, dataSet,
												// config,
												// testCaseId,UniqueKey_Emails);
			fc.utobj().printTestStep("Navigating to FS > RestAPI > Deleting PersoanlProfile for Franchisee");
			ActionCreated = fcService.DeletePersonalProfile(driver, dataSet, config, testCaseId,
					UniqueKey_PersonalProfile); // CreateEmail( driver, dataSet,
												// config,
												// testCaseId,UniqueKey_Emails);
			if (createPrimaryInfo) {
				fc.utobj().printTestStep("Navigating to FS > View Page > Verifying Franchisee");
				UniqueKey_PrimaryInfo.remove("referenceId");
				UniqueKey_PrimaryInfo.remove("parentReferenceId");
				ActionCreated = fcService.ValidatePrimaryInfo(driver, config, UniqueKey_PrimaryInfo);
			} else {
				fc.utobj().throwsException("Primary info is not created in Rest API with testcaseid " + testCaseId);
			}
			if (!ActionCreated) {
				fc.utobj().throwsException("PersonalProfile Action is not on View Page with testcaseid " + testCaseId);
			} else {
				fc.utobj().printTestStep("Navigating to FS > View Page > Verifying PersonalProfile");
				ActionCreated = fcService.ValidatePersonalProfile(driver, config, UniqueKey_PersonalProfile,
						UniqueKey_PrimaryInfo);//
			}
			if (ActionCreated) {
				fc.utobj().throwsException("Personal Profile found on view page ");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception ex) {
			fc.utobj().quitBrowserOnCatch(driver, ex, testCaseId);
		}
	}

	//// =============================================================================Qualification
	//// Detail
	//// Module========================================================================//

	@Test(groups = { "salesRestAPI", "FCRestAPI", "test" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-07-18", testCaseDescription = "Verify Primary info With Deleted Qualification Detail In FS module with Rest API", testCaseId = "TC_SalesRestAPIPrimaryInfoWithQualificationDetails_Delete")
	public void DeleteSubModuleofPrimaryInfoWithQualificationDetailsSubModule()
			throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("salesRestAPI", testCaseId);
		Map<String, String> UniqueKey_PrimaryInfo = new HashMap<String, String>();
		Map<String, String> UniqueKey_QualificationDetails = new HashMap<String, String>();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			fc.utobj().printTestStep("Navigating to FS > RestAPI > Adding Franchisee");
			boolean createPrimaryInfo = fcService.CreatePrimaryInfo(driver, dataSet, testCaseId, UniqueKey_PrimaryInfo);
			fc.utobj().printTestStep("Navigating to FS > RestAPI > Adding Qualification Details for Franchisee");
			boolean ActionCreated = fcService.CreateQualificationDetail(driver, dataSet, config, testCaseId,
					UniqueKey_QualificationDetails);
			fc.utobj().printTestStep("Navigating to FS > RestAPI > Updating Qualification Details for Franchisee");
			ActionCreated = fcService.DeleteQualificationDetails(driver, dataSet, config, testCaseId,
					UniqueKey_QualificationDetails);
			if (createPrimaryInfo) {
				fc.utobj().printTestStep("Navigating to FS > View Page > Verifying Franchisee");
				UniqueKey_PrimaryInfo.remove("referenceId");
				UniqueKey_PrimaryInfo.remove("parentReferenceId");
				ActionCreated = fcService.ValidatePrimaryInfo(driver, config, UniqueKey_PrimaryInfo);
			} else {
				fc.utobj().throwsException("Primary info is not created in Rest API with testcaseid " + testCaseId);
			}
			if (!ActionCreated) {
				fc.utobj().throwsException(
						"Qualification Details Action is not on View Page with testcaseid " + testCaseId);
			} else {
				fc.utobj().printTestStep("Navigating to FS > View Page > Verifying QualificationDetails");
				ActionCreated = fcService.ValidateQualificationDetails(driver, config, UniqueKey_QualificationDetails,
						UniqueKey_PrimaryInfo);//
			}
			if (ActionCreated) {
				fc.utobj().throwsException("Qualification Details found on view page ");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception ex) {
			fc.utobj().quitBrowserOnCatch(driver, ex, testCaseId);
		}
	}

	// =============================================================================Real
	// Estate
	// Module========================================================================//

	@Test(groups = { "salesRestAPI", "FCRestAPI", "test" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-07-18", testCaseDescription = "Verify Primary info With Deleted Real Estate In FS module with Rest API", testCaseId = "TC_SalesRestAPIPrimaryInfoWithRealEstate_Delete")
	public void DeleteSubModuleofPrimaryInfoWithRealEstateSubModule()
			throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("salesRestAPI", testCaseId);
		Map<String, String> UniqueKey_RealEstate = new HashMap<String, String>();
		Map<String, String> UniqueKey_PrimaryInfo = new HashMap<String, String>();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			fc.utobj().printTestStep("Navigating to FS > RestAPI > Adding Franchisee");
			boolean createPrimaryInfo = fcService.CreatePrimaryInfo(driver, dataSet, testCaseId, UniqueKey_PrimaryInfo);
			fc.utobj().printTestStep("Navigating to FS > RestAPI > Adding Real Estate for Franchisee");
			boolean ActionCreated = fcService.CreateRealEstate(driver, dataSet, config, testCaseId,
					UniqueKey_RealEstate);
			fc.utobj().printTestStep("Navigating to FS > RestAPI > Updating Real Estate for Franchisee");
			ActionCreated = fcService.DeleteRealEstate(driver, dataSet, config, testCaseId, UniqueKey_RealEstate);
			if (createPrimaryInfo) {
				fc.utobj().printTestStep("Navigating to FS > View Page > Verifying Franchisee");
				UniqueKey_PrimaryInfo.remove("referenceId");
				UniqueKey_PrimaryInfo.remove("parentReferenceId");
				ActionCreated = fcService.ValidatePrimaryInfo(driver, config, UniqueKey_PrimaryInfo);
			} else {
				fc.utobj().throwsException("Primary info is not created in Rest API with testcaseid " + testCaseId);
			}
			if (!ActionCreated) {
				fc.utobj().throwsException("RealEstate Action is not on View Page with testcaseid " + testCaseId);
			} else {
				fc.utobj().printTestStep("Navigating to FS > View Page > Verifying Real Estate");
				ActionCreated = fcService.ValidateRealEstate(driver, config, UniqueKey_RealEstate,
						UniqueKey_PrimaryInfo);//
			}
			if (ActionCreated) {
				fc.utobj().throwsException("Real Estate found on view page ");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception ex) {
			fc.utobj().quitBrowserOnCatch(driver, ex, testCaseId);
		}
	}
	// =============================================================================RemarkModule========================================================================//

	@Test(groups = { "salesRest" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-07-18", testCaseDescription = "Verify Primary info With Deleted Remark In FS module with Rest API", testCaseId = "TC_SalesRestAPIPrimaryInfoWithRemark_Delete")
	public void DeleteSubModuleofPrimaryInfoWithRemarkSubModule()
			throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("salesRestAPI", testCaseId);
		Map<String, String> UniqueKey_PrimaryInfo = new HashMap<String, String>();
		Map<String, String> UniqueKey_Remarks = new HashMap<String, String>();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			fc.utobj().printTestStep("Navigating to FS > RestAPI > Adding Franchisee");
			boolean createPrimaryInfo = fcService.CreatePrimaryInfo(driver, dataSet, testCaseId, UniqueKey_PrimaryInfo);
			fc.utobj().printTestStep("Navigating to FS > RestAPI > Adding Remark for Franchisee");
			boolean ActionCreated = fcService.CreateRemarks(driver, dataSet, config, testCaseId, UniqueKey_Remarks);
			fc.utobj().printTestStep("Navigating to FS > RestAPI > Updating Remark for Franchisee");
			// ActionCreated =fcService.DeleteRemarks( driver, dataSet, config,
			// testCaseId, UniqueKey_Remarks);
			if (createPrimaryInfo) {
				fc.utobj().printTestStep("Navigating to FS > View Page > Verifying Franchisee");
				UniqueKey_PrimaryInfo.remove("referenceId");
				UniqueKey_PrimaryInfo.remove("parentReferenceId");
				ActionCreated = fcService.ValidatePrimaryInfo(driver, config, UniqueKey_PrimaryInfo);
			} else {
				fc.utobj().throwsException("Primary info is not created in Rest API with testcaseid " + testCaseId);
			}
			if (!ActionCreated) {
				fc.utobj().throwsException("Remarks Action is not on View Page with testcaseid " + testCaseId);
			} else {
				fc.utobj().printTestStep("Navigating to FS > View Page > Verifying Remarks");
				ActionCreated = fcService.ValidateRemarks(driver, config, UniqueKey_Remarks, UniqueKey_PrimaryInfo);//
			}
			if (!ActionCreated) {
				fc.utobj().throwsException("Remark found on view page ");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception ex) {
			fc.utobj().quitBrowserOnCatch(driver, ex, testCaseId);
		}
	}
	// =============================================================================Task
	// Module========================================================================//

	@Test(groups = { "salesRestAPI", "FCRestAPI" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-07-18", testCaseDescription = "Verify Primary info With Deleted Task In FS module with Rest API", testCaseId = "TC_SalesRestAPIPrimaryInfoWithTask_Delete")
	public void DeleteSubModuleofPrimaryInfoWithTaskSubModule()
			throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("salesRestAPI", testCaseId);
		Map<String, String> UniqueKey_PrimaryInfo = new HashMap<String, String>();
		Map<String, String> UniqueKey_Tasks = new HashMap<String, String>();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			fc.utobj().printTestStep("Navigating to FS > RestAPI > Adding Franchisee");
			boolean createPrimaryInfo = fcService.CreatePrimaryInfo(driver, dataSet, testCaseId, UniqueKey_PrimaryInfo);
			fc.utobj().printTestStep("Navigating to FS > RestAPI > Adding Tasks for Franchisee");
			boolean ActionCreated = fcService.CreateTasks(driver, dataSet, config, testCaseId, UniqueKey_Tasks);
			fc.utobj().printTestStep("Navigating to FS > RestAPI > Updating Tasks for Franchisee");
			ActionCreated = fcService.DeleteTasks(driver, dataSet, config, testCaseId, UniqueKey_Tasks);
			if (createPrimaryInfo) {
				fc.utobj().printTestStep("Navigating to FS > View Page > Verifying Franchisee");
				UniqueKey_PrimaryInfo.remove("referenceId");
				UniqueKey_PrimaryInfo.remove("parentReferenceId");
				ActionCreated = fcService.ValidatePrimaryInfo(driver, config, UniqueKey_PrimaryInfo);
			} else {
				fc.utobj().throwsException("Primary info is not created in Rest API with testcaseid " + testCaseId);
			}
			if (!ActionCreated) {
				fc.utobj().throwsException("Tasks Action is not on View Page with testcaseid " + testCaseId);
			} else {
				fc.utobj().printTestStep("Navigating to FS > View Page > Verifying Tasks");
				ActionCreated = fcService.ValidateTasks(driver, config, UniqueKey_Tasks, UniqueKey_PrimaryInfo);//
			}
			if (ActionCreated) {
				fc.utobj().throwsException("Task found on view page ");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception ex) {
			fc.utobj().quitBrowserOnCatch(driver, ex, testCaseId);
		}
	}
	// =============================================================================Visit
	// Module========================================================================//

	@Test(groups = { "salesRestAPI", "FCRestAPI", "test" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-07-18", testCaseDescription = "Verify Primary info With Deleted Visit In FS module with Rest API", testCaseId = "TC_SalesRestAPIPrimaryInfoWithVisit_Delete")
	public void DeleteSubModuleofPrimaryInfoWithVisitSubModule()
			throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("salesRestAPI", testCaseId);
		Map<String, String> UniqueKey_PrimaryInfo = new HashMap<String, String>();
		Map<String, String> UniqueKey_Visits = new HashMap<String, String>();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			fc.utobj().printTestStep("Navigating to FS > RestAPI > Adding Franchisee");
			boolean createPrimaryInfo = fcService.CreatePrimaryInfo(driver, dataSet, testCaseId, UniqueKey_PrimaryInfo);
			fc.utobj().printTestStep("Navigating to FS > RestAPI > Adding VIsit for Franchisee");
			boolean ActionCreated = fcService.CreateVisit(driver, dataSet, config, testCaseId, UniqueKey_Visits);
			fc.utobj().printTestStep("Navigating to FS > RestAPI > Updating Visit for Franchisee");
			ActionCreated = fcService.DeleteVisit(driver, dataSet, config, testCaseId, UniqueKey_Visits);
			if (createPrimaryInfo) {
				fc.utobj().printTestStep("Navigating to FS > View Page > Verifying Franchisee");
				UniqueKey_PrimaryInfo.remove("referenceId");
				UniqueKey_PrimaryInfo.remove("parentReferenceId");
				ActionCreated = fcService.ValidatePrimaryInfo(driver, config, UniqueKey_PrimaryInfo);
			} else {
				fc.utobj().throwsException("Primary info is not created in Rest API with testcaseid " + testCaseId);
			}
			if (!ActionCreated) {
				fc.utobj().throwsException("Visits Action is not on View Page with testcaseid " + testCaseId);
			} else {
				fc.utobj().printTestStep("Navigating to FS > View Page > Verifying Visits");
				ActionCreated = fcService.ValidateVisits(driver, config, UniqueKey_Visits, UniqueKey_PrimaryInfo);//
			}
			if (ActionCreated) {
				fc.utobj().throwsException("Visit found on view page ");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception ex) {
			fc.utobj().quitBrowserOnCatch(driver, ex, testCaseId);
		}
	}

}
