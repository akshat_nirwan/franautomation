package com.builds.test.fs.RestAPI;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.WebDriver;

import com.builds.test.crm.RestAPI.UserCreation;
import com.builds.test.fs.FSSearchPageTest;
import com.builds.test.fs.Sales;
import com.builds.uimaps.fs.FsExportPage;
import com.builds.utilities.FranconnectUtil;

public class FsRestService_CRUDSubModule extends BaseRestUtil {

	UserCreation uc = new UserCreation();
	FranconnectUtil fc = new FranconnectUtil();

	public FsRestService_CRUDSubModule() {
	}

	public boolean Validate(WebDriver driver, Map<String, String> config, List<String> listVI, String subModule,
			Map<String, String> uniqueKey, String subject) throws Exception {
		try {
			boolean status = false;
			if (subModule.equals("Call")) {
				fc.loginpage().login(driver);
				FSSearchPageTest fsst = new FSSearchPageTest();
				fsst.searchByLeadName(driver, uniqueKey.get("firstName"), uniqueKey.get("lastName"));
				fc.utobj().clickPartialLinkText(driver, uniqueKey.get("firstName"));
				fc.utobj().clickLink(driver, "Primary Info");
				fc.utobj().switchFrameById(driver, "leadLogCallSummary");// leadOpenActivitesSummary
				status = fc.utobj().assertPageSourceWithMultipleRecords(driver, listVI);
				fc.utobj().switchFrameToDefault(driver);
				if (!status) {
					fc.utobj().switchFrameToDefault(driver);
					fc.utobj().clickPartialLinkText(driver, subject);
					fc.commonMethods().switch_cboxIframe_frameId(driver);
					status = fc.utobj().assertPageSourceWithMultipleRecords(driver, listVI);
				}

				return status;
			} else if (subModule.equals("Task")) {
				fc.loginpage().login(driver);
				FSSearchPageTest fsst = new FSSearchPageTest();
				fsst.searchByLeadName(driver, uniqueKey.get("firstName"), uniqueKey.get("lastName"));
				fc.utobj().clickPartialLinkText(driver, uniqueKey.get("firstName"));
				fc.utobj().clickLink(driver, "Primary Info");
				fc.utobj().switchFrameById(driver, "leadOpenActivitesSummary");//
				status = fc.utobj().assertPageSourceWithMultipleRecords(driver, listVI);
				if (!status) {
					fc.utobj().clickPartialLinkText(driver, subject);
					fc.commonMethods().switch_cboxIframe_frameId(driver);
					status = fc.utobj().assertPageSourceWithMultipleRecords(driver, listVI);
				}
				return status;
			} else if (subModule.equals("Remark")) {
				fc.loginpage().login(driver);
				FSSearchPageTest fsst = new FSSearchPageTest();
				fsst.searchByLeadName(driver, uniqueKey.get("firstName"), uniqueKey.get("lastName"));
				fc.utobj().clickPartialLinkText(driver, uniqueKey.get("firstName"));
				fc.utobj().clickLink(driver, "Primary Info");
				fc.utobj().switchFrameById(driver, "leadLogCallSummary");// leadOpenActivitesSummary
				status = fc.utobj().assertPageSourceWithMultipleRecords(driver, listVI);
				fc.utobj().switchFrameToDefault(driver);
				if (!status) {
					fc.utobj().switchFrameToDefault(driver);
					try {
						String xpath = ".//*[@id='activityTline']/table/tbody/tr[2]/td/table/tbody/tr[2]/*/span[2]";
						String value = fc.utobj().getElementByXpath(driver, xpath).getText();
						if (value.equals(subject)) {
							return true;
						}
					} catch (Exception e) {
						throwsException("Remark in not present at View page ");
					}

				}

				return status;
			} else {
				fc.utobj().clickLink(driver, subModule);
				status = fc.utobj().assertPageSourceWithMultipleRecords(driver, listVI);
				return status;
			}
		} catch (Exception ex) {
			fc.utobj().quitBrowserOnCatch(driver, ex, "validate failed for " + subModule);
			return false;
		}

	}

	public boolean ValidatePrimaryInfo(WebDriver driver, Map<String, String> config, Map<String, String> uniqueKey) {
		try {
			fc.loginpage().login(driver);
			FSSearchPageTest fsst = new FSSearchPageTest();
			fsst.searchByLeadName(driver, uniqueKey.get("firstName"), uniqueKey.get("lastName"));
			fc.utobj().clickPartialLinkText(driver, uniqueKey.get("firstName"));
			uniqueKey.remove("sendAutomaticMail");
			List<String> ListPI = new ArrayList<String>(uniqueKey.values());
			boolean status = fc.utobj().assertPageSourceWithMultipleRecords(driver, ListPI);
			return status;

		} catch (Exception ex) {
		}
		return false;
	}

	/*
	 * protected boolean AcitityPerform(WebDriver driver, Map<String, String>
	 * dataSet, Map<String, String> config, Map<String, String> UniqueKey,
	 * String Module, String SubModule, String Operation, String Divtag) {
	 * String secureToken = uc.GetSecureToken(driver, dataSet, config); String
	 * encToken = "pHNsc6x5mBksJDfo";// TRP.GetEcryptedToken(driver, dataSet,
	 * config, secureToken); String Query = uc.GetQuery(driver, dataSet, config,
	 * encToken, null, Module, SubModule, Operation); String ResultantQuery =
	 * uc.GetResultantQuery_R(Query, dataSet, UniqueKey); boolean
	 * CreateSubModule = uc.CRUDByQuery(driver, dataSet, config, encToken,
	 * ResultantQuery, Module, SubModule, Operation, Divtag,
	 * ReferenceStatus_keys); if (CreateSubModule) { System.out.println(
	 * " Contract Signing Sub Module created Successfully for Info Manager");
	 * return true; } else { System.out.println(
	 * " Contract Signing Sub Module Has not been created for	 Info Manager");
	 * } System.out.println("End of your Program"); return false; }
	 */

	/*
	 * private String getLeadParentID(WebDriver driver, Map<String, String>
	 * config, Map<String, String> uniqueKey) { try {
	 * fc.loginpage().login(driver, config); FSSearchPageTest fsst = new
	 * FSSearchPageTest(); fsst.searchByLeadName(driver,
	 * uniqueKey.get("firstName"), uniqueKey.get("lastName"));
	 * fc.utobj().clickPartialLinkText(driver, uniqueKey.get("firstName"));
	 * String temp =
	 * fc.utobj().getElementByXpath(driver,".//input[@name='leadId']")).
	 * getAttribute( "value"); System.out.println(">>>>>>>>>>>>>>>>>>>    " +
	 * temp); return temp; } catch (Exception ex) { ex.printStackTrace(); }
	 * return null; }
	 */

	public boolean ValidateForExportCall(WebDriver driver, Map<String, String> config, List<String> listVI,
			String subModule, Map<String, String> uniqueKey, String subject) throws Exception {
		try {
			boolean status = false;
			fc.loginpage().login(driver);

			fc.sales().sales_common().fsModule(driver);
			Sales fmpt = new Sales();
			FsExportPage pobj = new FsExportPage(driver);
			fmpt.exportPage(driver);
			fc.utobj().clickElement(driver, pobj.chkbxActHistoryCall);
			fc.utobj().clickElement(driver, pobj.srchDataBtn);
			fc.utobj().sendKeys(driver, pobj.fnametxt, uniqueKey.get("firstName"));
			fc.utobj().sendKeys(driver, pobj.lnametxt, uniqueKey.get("lastName"));
			fc.utobj().sendKeys(driver, pobj.datefrom, uniqueKey.get("calldate"));
			fc.utobj().sendKeys(driver, pobj.dateTo, uniqueKey.get("calldate"));
			fc.utobj().clickElement(driver, pobj.viewdata);
			status = fc.utobj().assertPageSourceWithMultipleRecords(driver, listVI);
			return status;

		} catch (Exception ex) {
			fc.utobj().quitBrowserOnCatch(driver, ex, "validate failed for " + subModule);
			return false;
		}

		// TODO Auto-generated method stub

	}

	public boolean ValidateForExportRemarks(WebDriver driver, Map<String, String> config, List<String> listVI,
			String subModule, Map<String, String> uniqueKey, String subject) throws Exception {
		try {
			boolean status = false;
			fc.loginpage().login(driver);
			fc.sales().sales_common().fsModule(driver);
			Sales fmpt = new Sales();
			FsExportPage pobj = new FsExportPage(driver);
			fmpt.exportPage(driver);
			fc.utobj().clickElement(driver, pobj.chkbxActHistoryRemarks);
			fc.utobj().clickElement(driver, pobj.srchDataBtn);
			fc.utobj().sendKeys(driver, pobj.fnametxt, uniqueKey.get("firstName"));
			fc.utobj().sendKeys(driver, pobj.lnametxt, uniqueKey.get("lastName"));
			fc.utobj().sendKeys(driver, pobj.remark, uniqueKey.get("remark"));
			fc.utobj().clickElement(driver, pobj.viewdata);
			status = fc.utobj().assertPageSourceWithMultipleRecords(driver, listVI);
			return status;

		} catch (Exception ex) {
			fc.utobj().quitBrowserOnCatch(driver, ex, "validate failed for " + subModule);
		}
		return false;
	}

	public boolean ValidateForExportTasks(WebDriver driver, Map<String, String> config, List<String> listVI,
			String subModule, Map<String, String> uniqueKey, String subject) throws Exception {
		try {
			boolean status = false;
			fc.loginpage().login(driver);
			fc.sales().sales_common().fsModule(driver);
			Sales fmpt = new Sales();
			FsExportPage pobj = new FsExportPage(driver);
			fmpt.exportPage(driver);
			fc.utobj().clickElement(driver, pobj.chkbxActHistoryTasks);
			fc.utobj().clickElement(driver, pobj.srchDataBtn);
			fc.utobj().sendKeys(driver, pobj.fnametxt, uniqueKey.get("firstName"));
			fc.utobj().sendKeys(driver, pobj.lnametxt, uniqueKey.get("lastName"));
			fc.utobj().sendKeys(driver, pobj.taskdatefrom, uniqueKey.get("calldate"));
			fc.utobj().sendKeys(driver, pobj.ExportTaskSubject, uniqueKey.get("subjecttask"));
			// fc.utobj().sendKeys(driver, pobj.taskdateto,
			// uniqueKey.get("calldate"));
			fc.utobj().clickElement(driver, pobj.viewdata);
			status = fc.utobj().assertPageSourceWithMultipleRecords(driver, listVI);
			return status;

		} catch (Exception ex) {
			fc.utobj().quitBrowserOnCatch(driver, ex, "validate failed for " + subModule);
			return false;
		}

		// TODO Auto-generated method stub

	}

}
