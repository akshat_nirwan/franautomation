package com.builds.test.fs.RestAPI;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.openqa.selenium.WebDriver;
import org.xml.sax.SAXException;

import com.builds.test.fs.FSSearchPageTest;
import com.builds.utilities.FranconnectUtil;

public class FsRestServicesAction extends BaseRestUtil {

	FranconnectUtil fc = new FranconnectUtil();
	BaseRestUtil bRU = new BaseRestUtil();
	FsRestService_CRUDSubModule fcServices = new FsRestService_CRUDSubModule();

	public boolean DeletePrimaryInfo(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey_PrimaryInfo)
			throws IOException, Exception, ParserConfigurationException, SAXException {
		try {
			Map<String, String> TempMap = new HashMap<String, String>();
			;
			TempMap.put("referenceId",
					DataServicePage.ReferenceStatus_keys.get(testCaseId + DataServicePage.PrimaryInfo));
			driver.navigate().to(config.get("buildUrl") + DataServicePage.restURL);
			boolean DeletePrimaryInfo = ActivityPerform(driver, dataSet, TempMap, DataServicePage.FS,
					DataServicePage.PrimaryInfo, DataServicePage.Delete, DataServicePage.deleteDiv, testCaseId);
			if (DeletePrimaryInfo) {
				boolean PrimaryInfo = fcServices.ValidatePrimaryInfo(driver, config, UniqueKey_PrimaryInfo);
				return PrimaryInfo;
			} else {
				fc.utobj().throwsException("Delete FS Primary Info not Done on Rest API" + testCaseId);
			}
		} catch (Exception ex) {
			return false;
		}
		return false;
	}

	public boolean deletePrimaryInfo(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey_PrimaryInfo)
			throws IOException, Exception, ParserConfigurationException, SAXException {
		boolean DeletePrimaryInfo = false;
		try {
			Map<String, String> TempMap = new HashMap<String, String>();
			;
			TempMap.put("referenceId",
					DataServicePage.ReferenceStatus_keys.get(testCaseId + DataServicePage.PrimaryInfo));
			driver.navigate().to(config.get("buildUrl") + DataServicePage.restURL);
			DeletePrimaryInfo = ActivityPerform(driver, dataSet, TempMap, DataServicePage.FS,
					DataServicePage.PrimaryInfo, DataServicePage.Delete, DataServicePage.deleteDiv, testCaseId);
		} catch (Exception ex) {
			return false;
		}
		return DeletePrimaryInfo;
	}

	public boolean CreatePrimaryInfo(WebDriver driver, Map<String, String> dataSet, String testCaseId,
			Map<String, String> UniqueKey_PrimaryInfo)
			throws IOException, Exception, ParserConfigurationException, SAXException {
		boolean primaryinfostatus = false;
		try {
			driver.navigate().to(FranconnectUtil.config.get("buildUrl") + DataServicePage.restURL);
			UniqueKey_PrimaryInfo.put("firstName", dataSet.get("firstname") + fc.utobj().generateRandomNumber());
			UniqueKey_PrimaryInfo.put("lastName", dataSet.get("lastname") + fc.utobj().generateRandomNumber());
			UniqueKey_PrimaryInfo.put("basedOnAssignmentRule", "No");
			UniqueKey_PrimaryInfo.put("leadSource2ID", "Advertisement");
			UniqueKey_PrimaryInfo.put("leadSource3ID", "Magazine");
			UniqueKey_PrimaryInfo.put("nextCallDate", fc.utobj().generatefutureDatewithformat("yyyy-MM-dd", 45));
			UniqueKey_PrimaryInfo.put("sendAutomaticMail", "Yes");
			UniqueKey_PrimaryInfo.put("forecastClosureDate", fc.utobj().generatefutureDatewithformat("yyyy-MM-dd", 50));
			UniqueKey_PrimaryInfo.put("basedOnWorkflowAssignmentRule", "No");
			UniqueKey_PrimaryInfo.put("emailID", "test@franconnect.net");
			UniqueKey_PrimaryInfo.put("division", "All");
			primaryinfostatus = ActivityPerform(driver, dataSet, UniqueKey_PrimaryInfo, DataServicePage.FS,
					DataServicePage.PrimaryInfo, DataServicePage.Create, DataServicePage.createDiv, testCaseId);
			if (primaryinfostatus) {
				System.out.println("Primary Info Created Successfully");
				return primaryinfostatus;
			} else {
				fc.utobj().throwsException("FS Primary Info not created on RestApi" + testCaseId);
				return primaryinfostatus;
			}
		} catch (Exception ex) {
			fc.utobj().quitBrowserOnCatch(driver, ex, testCaseId);
		}
		return primaryinfostatus;
	}

	public boolean UpdatePrimaryInfo(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey_PrimaryInfo)
			throws IOException, Exception, ParserConfigurationException, SAXException {
		try {
			driver.navigate().to(config.get("buildUrl") + DataServicePage.restURL);
			;
			UniqueKey_PrimaryInfo.put("referenceId",
					DataServicePage.ReferenceStatus_keys.get(testCaseId + "Primary Info"));
			UniqueKey_PrimaryInfo.put("firstName", dataSet.get("firstname") + fc.utobj().generateRandomNumber());
			UniqueKey_PrimaryInfo.put("lastName", dataSet.get("lastname") + fc.utobj().generateRandomNumber());
			UniqueKey_PrimaryInfo.put("basedOnAssignmentRule", "No");
			UniqueKey_PrimaryInfo.put("leadSource2ID", "Advertisement");
			UniqueKey_PrimaryInfo.put("leadSource3ID", "Magazine");
			UniqueKey_PrimaryInfo.put("nextCallDate", fc.utobj().generatefutureDatewithformat("yyyy-MM-dd", 45));
			UniqueKey_PrimaryInfo.put("sendAutomaticMail", "Yes");
			UniqueKey_PrimaryInfo.put("forecastClosureDate", fc.utobj().generatefutureDatewithformat("yyyy-MM-dd", 50));
			UniqueKey_PrimaryInfo.put("basedOnWorkflowAssignmentRule", "No");
			UniqueKey_PrimaryInfo.put("emailID", "test@franconnect.net");
			UniqueKey_PrimaryInfo.put("division", "All");
			boolean primaryinfostatus = ActivityPerform(driver, dataSet, UniqueKey_PrimaryInfo, DataServicePage.FS,
					DataServicePage.PrimaryInfo, DataServicePage.Update, DataServicePage.updateDiv, testCaseId);
			if (primaryinfostatus) {
				System.out.println("Primary Info Created Successfully");
			} else {
				fc.utobj().throwsException("FS Primary Info not created on RestApi" + testCaseId);
			}
			boolean PrimaryInfo = true;// fcServices.ValidatePrimaryInfo(driver,
			// config, UniqueKey_PrimaryInfo);
			if (PrimaryInfo) {
				System.out.println("Primary Info Created Successfully");
				return true;
			} else {
				fc.utobj().throwsException("FS Primary Info not found on ViewPage" + testCaseId);
			}

		} catch (Exception ex) {

			fc.utobj().quitBrowserOnCatch(driver, ex, testCaseId);

		}
		return false;

	}

	public boolean RetrievePrimaryInfo(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey_PrimaryInfo)
			throws IOException, Exception, ParserConfigurationException, SAXException {
		Map<String, String> TempMap = new HashMap<String, String>();
		String LeadId = DataServicePage.ReferenceStatus_keys.get(testCaseId + "Primary Info");
		TempMap.put("referenceId", LeadId);
		boolean RetrievePrimaryInfo = ActivityPerform(driver, dataSet, TempMap, "FS", "Primary Info", "Retrieve",
				"retrieveDiv", testCaseId);
		if (!RetrievePrimaryInfo) {
			fc.utobj().throwsException("Primary Info not Retrieved for " + testCaseId);
		}
		return RetrievePrimaryInfo;
	}

	public boolean ValidatePrimaryInfo(WebDriver driver, Map<String, String> config, Map<String, String> uniqueKey) {
		try {
			fc.loginpage().login(driver);
			FSSearchPageTest fsst = new FSSearchPageTest();
			fsst.searchByLeadName(driver, uniqueKey.get("firstName"), uniqueKey.get("lastName"));
			fc.utobj().clickPartialLinkText(driver, uniqueKey.get("firstName"));
			uniqueKey.remove("sendAutomaticMail");
			uniqueKey.remove("_centerOpeningDate964782486");
			uniqueKey.remove("division");
			uniqueKey.remove("forecastClosureDate");
			uniqueKey.put("nextCallDate", bRU.changeFormat(uniqueKey.get("nextCallDate")));
			List<String> ListPI = new ArrayList<String>(uniqueKey.values());
			boolean status = fc.utobj().assertPageSourceWithMultipleRecords(driver, ListPI);
			return status;

		} catch (Exception ex) {
		}
		return false;
	}

	public boolean CreateCoApplicant(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey_CoApplicant) throws Exception {
		String LeadParentID = DataServicePage.ReferenceStatus_keys.get(testCaseId + "Primary Info");
		UniqueKey_CoApplicant.put("parentReferenceId", LeadParentID);
		UniqueKey_CoApplicant.put("coApplicantRelationshipID", "Relative");
		UniqueKey_CoApplicant.put("firstName", dataSet.get("firstname") + fc.utobj().generateRandomNumber());
		UniqueKey_CoApplicant.put("lastName", dataSet.get("lastname") + fc.utobj().generateRandomNumber());
		UniqueKey_CoApplicant.put("emailID", "test@franconnect.net");
		boolean CoApplicantStauts = ActivityPerform(driver, dataSet, UniqueKey_CoApplicant, DataServicePage.FS,
				DataServicePage.CoApplicants, DataServicePage.Create, DataServicePage.createDiv, testCaseId);
		if (!CoApplicantStauts) {
			fc.utobj().throwsException("Co-Applicant hasn't been Created in Fs Module " + testCaseId);
		}
		return CoApplicantStauts;
	}

	public boolean RetrieveCoApplicant(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey_CoApplicant) throws Exception {
		Map<String, String> TempMap = new HashMap<String, String>();
		String LeadId = DataServicePage.ReferenceStatus_keys.get(testCaseId + "Primary Info");
		TempMap.put("referenceId", LeadId);
		boolean RetrieveCoApplicants = ActivityPerform(driver, dataSet, TempMap, "FS", "Co-Applicants", "Retrieve",
				"retrieveDiv", testCaseId);
		if (!RetrieveCoApplicants) {
			fc.utobj().throwsException("Primary Info not Retrieved for " + testCaseId);
		} else {
			System.out.println("CoApplicants not Present for this Reference Id");
		}
		return RetrieveCoApplicants;
	}

	public boolean RetrievePersonalProfile(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> Uniquekey) throws Exception {
		Map<String, String> TempMap = new HashMap<String, String>();
		String LeadId = DataServicePage.ReferenceStatus_keys.get(testCaseId + "Primary Info");
		TempMap.put("referenceId", LeadId);
		boolean RetrievePersonalProfile = ActivityPerform(driver, dataSet, TempMap, "FS", "Personal Profile",
				"Retrieve", "retrieveDiv", testCaseId);
		if (!RetrievePersonalProfile) {
			fc.utobj().throwsException("Primary Info not Retrieved for " + testCaseId);
		} else {
			System.out.println("PersonalProfile not Present for this Reference Id");
		}
		return RetrievePersonalProfile;
	}

	public boolean RetrieveCompliance(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> Uniquekey) throws Exception {
		Map<String, String> TempMap = new HashMap<String, String>();
		String LeadId = DataServicePage.ReferenceStatus_keys.get(testCaseId + "Primary Info");
		TempMap.put("referenceId", LeadId);
		boolean RetrieveCompliance = ActivityPerform(driver, dataSet, TempMap, "FS", "Compliance", "Retrieve",
				"retrieveDiv", testCaseId);
		if (!RetrieveCompliance) {
			fc.utobj().throwsException("Primary Info not Retrieved for " + testCaseId);
		} else {
			System.out.println("Compliance not Present for this Reference Id");
		}
		return RetrieveCompliance;
	}

	public boolean RetrieveRealEstate(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> Uniquekey) throws Exception {
		Map<String, String> TempMap = new HashMap<String, String>();
		String LeadId = DataServicePage.ReferenceStatus_keys.get(testCaseId + "Primary Info");
		TempMap.put("referenceId", LeadId);
		boolean RetrieveRealEstate = ActivityPerform(driver, dataSet, TempMap, "FS", "Real Estate", "Retrieve",
				"retrieveDiv", testCaseId);
		if (!RetrieveRealEstate) {
			fc.utobj().throwsException("Primary Info not Retrieved for " + testCaseId);
		} else {
			System.out.println("Real Estate   not Present for this Reference Id");
		}
		return RetrieveRealEstate;
	}

	public boolean RetrieveVisit(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> Uniquekey) throws Exception {
		Map<String, String> TempMap = new HashMap<String, String>();
		String LeadId = DataServicePage.ReferenceStatus_keys.get(testCaseId + "Primary Info");
		TempMap.put("referenceId", LeadId);
		boolean RetrieveVisit = ActivityPerform(driver, dataSet, TempMap, "FS", "Visit", "Retrieve", "retrieveDiv",
				testCaseId);
		if (!RetrieveVisit) {
			fc.utobj().throwsException("Primary Info not Retrieved for " + testCaseId);
		} else {
			System.out.println("Visit not Present for this Reference Id");
		}
		return RetrieveVisit;
	}

	public boolean RetrieveQualificationDetails(WebDriver driver, Map<String, String> dataSet,
			Map<String, String> config, String testCaseId, Map<String, String> Uniquekey) throws Exception {
		Map<String, String> TempMap = new HashMap<String, String>();
		String LeadId = DataServicePage.ReferenceStatus_keys.get(testCaseId + "Primary Info");
		TempMap.put("referenceId", LeadId);
		boolean RetrieveQualificationDetails = ActivityPerform(driver, dataSet, TempMap, "FS", "Qualification Details",
				"Retrieve", "retrieveDiv", testCaseId);
		if (!RetrieveQualificationDetails) {
			fc.utobj().throwsException("Primary Info not Retrieved for " + testCaseId);
		} else {
			System.out.println("Visit not Present for this Reference Id");
		}
		return RetrieveQualificationDetails;
	}

	public boolean RetrieveCall(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> Uniquekey) throws Exception {
		Map<String, String> TempMap = new HashMap<String, String>();
		String LeadId = DataServicePage.ReferenceStatus_keys.get(testCaseId + "Primary Info");
		TempMap.put("referenceId", LeadId);
		boolean RetrieveCall = ActivityPerform(driver, dataSet, TempMap, "FS", "Call", "Retrieve", "retrieveDiv",
				testCaseId);
		if (!RetrieveCall) {
			fc.utobj().throwsException("Primary Info not Retrieved for " + testCaseId);
		} else {
			System.out.println("Visit not Present for this Reference Id");
		}
		return RetrieveCall;
	}

	public boolean RetrieveRemark(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> Uniquekey) throws Exception {
		Map<String, String> TempMap = new HashMap<String, String>();
		String LeadId = DataServicePage.ReferenceStatus_keys.get(testCaseId + "Primary Info");
		TempMap.put("referenceId", LeadId);
		boolean RetrieveRemark = ActivityPerform(driver, dataSet, TempMap, "FS", "Remark", "Retrieve", "retrieveDiv",
				testCaseId);
		if (!RetrieveRemark) {
			fc.utobj().throwsException("Primary Info not Retrieved for " + testCaseId);
		} else {
			System.out.println("Visit not Present for this Reference Id");
		}
		return RetrieveRemark;
	}

	public boolean RetrieveTasks(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> Uniquekey) throws Exception {
		Map<String, String> TempMap = new HashMap<String, String>();
		String LeadId = DataServicePage.ReferenceStatus_keys.get(testCaseId + "Primary Info");
		TempMap.put("referenceId", LeadId);
		boolean RetrieveTask = ActivityPerform(driver, dataSet, TempMap, "FS", "Task", "Retrieve", "retrieveDiv",
				testCaseId);
		if (!RetrieveTask) {
			fc.utobj().throwsException("Primary Info not Retrieved for " + testCaseId);
		} else {
			System.out.println("Visit not Present for this Reference Id");
		}
		return RetrieveTask;
	}

	public boolean UpdateCoApplicant(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey_CoApplicant) throws Exception {
		boolean UpdateCoApplicant;
		String coApplicantLeadId = DataServicePage.ReferenceStatus_keys.get(testCaseId + "Co-Applicants");
		UniqueKey_CoApplicant.put("referenceId", coApplicantLeadId);
		UniqueKey_CoApplicant.put("coApplicantRelationshipID", "Relative");
		UniqueKey_CoApplicant.put("firstName", dataSet.get("firstname") + fc.utobj().generateRandomNumber());
		UniqueKey_CoApplicant.put("lastName", dataSet.get("lastname") + fc.utobj().generateRandomNumber());
		UniqueKey_CoApplicant.put("emailID", "test@franconnect.net");
		UpdateCoApplicant = ActivityPerform(driver, dataSet, UniqueKey_CoApplicant, DataServicePage.FS,
				DataServicePage.CoApplicants, DataServicePage.Update, DataServicePage.updateDiv, testCaseId);
		if (!UpdateCoApplicant)
			fc.utobj().throwsException("Co Applicant can't be Updated in Fs Module " + testCaseId);
		return UpdateCoApplicant;
	}

	public boolean DeleteCoApplicant(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey_CoApplicant) throws Exception {
		Map<String, String> TempMap = new HashMap<String, String>();
		TempMap.put("referenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Co-Applicants"));
		UniqueKey_CoApplicant.put("referenceId",
				DataServicePage.ReferenceStatus_keys.get(testCaseId + "Co-Applicants"));
		boolean DeleteCoApplicants = ActivityPerform(driver, dataSet, TempMap, "FS", "Co-Applicants", "Delete",
				"deleteDiv", testCaseId);
		if (!DeleteCoApplicants) {
			fc.utobj().throwsException("CoApplicant not deleted in restApi " + testCaseId);
		}
		return DeleteCoApplicants;
	}

	public boolean DeleteCall(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey_Call) throws Exception {
		Map<String, String> TempMap = new HashMap<String, String>();
		TempMap.put("referenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Call"));
		UniqueKey_Call.put("referenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Call"));
		boolean DeleteCall = ActivityPerform(driver, dataSet, TempMap, "FS", "Call", "Delete", "deleteDiv", testCaseId);
		if (!DeleteCall) {
			fc.utobj().throwsException("CoApplicant not deleted in restApi " + testCaseId);
		}
		return DeleteCall;
	}

	public boolean DeleteCompliance(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey_Compliance) throws Exception {
		Map<String, String> TempMap = new HashMap<String, String>();
		TempMap.put("referenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Compliance"));
		UniqueKey_Compliance.put("referenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Compliance"));
		boolean DeleteCompliance = ActivityPerform(driver, dataSet, TempMap, "FS", "Compliance", "Delete", "deleteDiv",
				testCaseId);
		if (!DeleteCompliance) {
			fc.utobj().throwsException("CoApplicant not deleted in restApi " + testCaseId);
		}
		return DeleteCompliance;
	}

	public boolean DeletePersonalProfile(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey_PersonalProfile) throws Exception {
		Map<String, String> TempMap = new HashMap<String, String>();
		TempMap.put("referenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Personal Profile"));
		UniqueKey_PersonalProfile.put("referenceId",
				DataServicePage.ReferenceStatus_keys.get(testCaseId + "Personal Profile"));
		boolean RetrievePersonalProfile = ActivityPerform(driver, dataSet, TempMap, "FS", "Personal Profile", "Delete",
				"deleteDiv", testCaseId);
		if (!RetrievePersonalProfile) {
			fc.utobj().throwsException("PersonalProfile not deleted in restApi " + testCaseId);
		}
		return RetrievePersonalProfile;
	}

	public boolean DeleteQualificationDetails(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey) throws Exception {
		Map<String, String> TempMap = new HashMap<String, String>();
		TempMap.put("referenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Qualification Details"));
		boolean QualificationDetails = ActivityPerform(driver, dataSet, TempMap, "FS", "Qualification Details",
				"Delete", "deleteDiv", testCaseId);
		if (!QualificationDetails) {
			fc.utobj().throwsException("QualificationDetails not deleted in restApi " + testCaseId);
		}
		return QualificationDetails;
	}

	public boolean DeleteRealEstate(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey) throws Exception {
		Map<String, String> TempMap = new HashMap<String, String>();
		TempMap.put("referenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Real Estate"));
		UniqueKey.put("referenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Real Estate"));
		boolean RealEstate = ActivityPerform(driver, dataSet, TempMap, "FS", "Real Estate", "Delete", "deleteDiv",
				testCaseId);
		if (!RealEstate) {
			fc.utobj().throwsException("Real Estate not deleted in restApi " + testCaseId);
		}
		return RealEstate;
	}

	public boolean DeleteRemarks(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey) throws Exception {
		Map<String, String> TempMap = new HashMap<String, String>();
		TempMap.put("referenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Remark"));
		UniqueKey.put("referenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Remark"));
		boolean RealEstate = ActivityPerform(driver, dataSet, TempMap, "FS", "Remark", "Delete", "deleteDiv",
				testCaseId);
		if (!RealEstate) {
			fc.utobj().throwsException("Remark not deleted in restApi " + testCaseId);
		}
		return RealEstate;
	}

	public boolean DeleteVisit(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey) throws Exception {
		Map<String, String> TempMap = new HashMap<String, String>();
		TempMap.put("referenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Visit"));
		UniqueKey.put("referenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Visit"));
		boolean DeleteVisit = ActivityPerform(driver, dataSet, TempMap, "FS", "Visit", "Delete", "deleteDiv",
				testCaseId);
		if (!DeleteVisit) {
			fc.utobj().throwsException("Visit not deleted in restApi " + testCaseId);
		}
		return DeleteVisit;
	}

	public boolean ValidateCoApplicant(WebDriver driver, Map<String, String> config,
			Map<String, String> UniqueKey_CoApplicant) throws Exception {
		UniqueKey_CoApplicant.remove("referenceId");
		UniqueKey_CoApplicant.remove("parentReferenceId");
		List<String> listCA = new ArrayList<String>(UniqueKey_CoApplicant.values());
		fc.utobj().clickLink(driver, DataServicePage.CoApplicants);
		boolean status = fc.utobj().assertPageSourceWithMultipleRecords(driver, listCA);
		return status;

	}

	public boolean CreateCall(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey_Call) throws Exception {
		UniqueKey_Call.put("parentReferenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Primary Info"));
		UniqueKey_Call.put("subject", "Call" + fc.utobj().generateRandomNumber());
		String date = fc.utobj().generateCurrentDatewithformat("yyyy-MM-dd");
		UniqueKey_Call.put("date", date);
		UniqueKey_Call.put("callStatus", "Contacted");
		UniqueKey_Call.put("callType", "Inbound Call");
		UniqueKey_Call.put("timeAdded", "10:15Z");
		UniqueKey_Call.put("comments", "Hello this is calll test");
		driver.navigate().to(config.get("buildUrl") + DataServicePage.restURL);
		boolean CallStauts = ActivityPerform(driver, dataSet, UniqueKey_Call, DataServicePage.FS, DataServicePage.Call,
				DataServicePage.Create, DataServicePage.createDiv, testCaseId);
		return CallStauts;
	}

	public boolean UpdateCall(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey_Call) throws Exception {
		String CallLeadId = DataServicePage.ReferenceStatus_keys.get(testCaseId + DataServicePage.Call);
		UniqueKey_Call.put("referenceId", CallLeadId);
		UniqueKey_Call.put("subject", "Call" + fc.utobj().generateRandomNumber());
		UniqueKey_Call.put("date", fc.utobj().generateCurrentDatewithformat("yyyy-MM-dd"));
		UniqueKey_Call.put("callStatus", "Contacted");
		UniqueKey_Call.put("callType", "Inbound Call");
		UniqueKey_Call.put("timeAdded", "10:15Z");
		boolean CallStauts = ActivityPerform(driver, dataSet, UniqueKey_Call, DataServicePage.FS, DataServicePage.Call,
				DataServicePage.Update, DataServicePage.updateDiv, testCaseId);
		return CallStauts;
	}

	public boolean ValidateCall(WebDriver driver, Map<String, String> config, Map<String, String> UniqueKey_Call,
			Map<String, String> UniqueKey_PrimaryInfo) throws Exception {
		UniqueKey_Call.remove("referenceId");
		UniqueKey_Call.remove("parentReferenceId");
		UniqueKey_Call.remove("timeAdded");
		UniqueKey_PrimaryInfo.put("calldate", UniqueKey_Call.get("date"));
		List<String> listCAll = new ArrayList<String>(UniqueKey_Call.values());
		boolean FoundExportCall = fcServices.ValidateForExportCall(driver, config, listCAll, DataServicePage.Call,
				UniqueKey_PrimaryInfo, UniqueKey_Call.get("subject"));
		return FoundExportCall;
	}

	public boolean ValidateCompliance(WebDriver driver, Map<String, String> config,
			Map<String, String> uniqueKey_Compliance, Map<String, String> UniqueKey_PrimaryInfo) throws Exception {
		uniqueKey_Compliance.remove("referenceId");
		uniqueKey_Compliance.remove("parentReferenceId");
		List<String> listCA = new ArrayList<String>(uniqueKey_Compliance.values());
		fc.utobj().clickLink(driver, DataServicePage.Compliance);
		boolean status = fc.utobj().assertPageSourceWithMultipleRecords(driver, listCA);
		return status;

	}

	public boolean ValidatePersonalProfile(WebDriver driver, Map<String, String> config,
			Map<String, String> UniqueKey_PersonalProfile, Map<String, String> UniqueKey_PrimaryInfo) throws Exception {
		UniqueKey_PersonalProfile.remove("referenceId");
		UniqueKey_PersonalProfile.remove("parentReferenceId");
		List<String> listCA = new ArrayList<String>(UniqueKey_PersonalProfile.values());
		fc.utobj().clickLink(driver, DataServicePage.PersonalProfile);
		boolean status = fc.utobj().assertPageSourceWithMultipleRecords(driver, listCA);
		return status;

	}

	public boolean CreateCompliance(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey_Compliance) throws Exception {
		String LeadParentID = DataServicePage.ReferenceStatus_keys.get(testCaseId + "Primary Info");
		UniqueKey_Compliance.put("parentReferenceId", LeadParentID);
		UniqueKey_Compliance.put("stateRegReq", "No");
		UniqueKey_Compliance.put("stateAddendumReq", "No");
		UniqueKey_Compliance.put("francCommiteeApproval", "No");
		UniqueKey_Compliance.put("contractRecSign", "No");
		UniqueKey_Compliance.put("leaseRiderProperlySign", "Yes");
		UniqueKey_Compliance.put("ipAddress", "192.168.9.102");
		UniqueKey_Compliance.put("licAgrProperlySign", "Yes");
		UniqueKey_Compliance.put("promNotePropSign", "Yes");
		UniqueKey_Compliance.put("perCovenantAgrProperlySign", "Yes");
		UniqueKey_Compliance.put("ufocRecProperlySign", "Yes");
		UniqueKey_Compliance.put("guaranteeProperlySign", "Yes");
		UniqueKey_Compliance.put("otherDocProperlySign", "Yes");
		UniqueKey_Compliance.put("stateReqAddendumProperlySign", "Yes");
		UniqueKey_Compliance.put("handWrittenChanges", "Yes");
		UniqueKey_Compliance.put("otherAttendaProperlySign", "Yes");
		UniqueKey_Compliance.put("proofControlOverRealEstate", "Yes");
		boolean ComplianceStatus = ActivityPerform(driver, dataSet, UniqueKey_Compliance, DataServicePage.FS,
				DataServicePage.Compliance, DataServicePage.Create, DataServicePage.createDiv, testCaseId);
		return ComplianceStatus;

	}

	public boolean UpdateCompliance(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey_Compliance) throws Exception {
		String leadComplianceID = DataServicePage.ReferenceStatus_keys.get(testCaseId + "Compliance");
		UniqueKey_Compliance.put("referenceId", leadComplianceID);
		UniqueKey_Compliance.put("stateRegReq", "No");
		UniqueKey_Compliance.put("stateAddendumReq", "No");
		UniqueKey_Compliance.put("francCommiteeApproval", "No");
		UniqueKey_Compliance.put("contractRecSign", "No");
		UniqueKey_Compliance.put("leaseRiderProperlySign", "Yes");
		UniqueKey_Compliance.put("ipAddress", "192.168.9.12");
		UniqueKey_Compliance.put("licAgrProperlySign", "Yes");
		UniqueKey_Compliance.put("promNotePropSign", "Yes");
		UniqueKey_Compliance.put("perCovenantAgrProperlySign", "Yes");
		UniqueKey_Compliance.put("ufocRecProperlySign", "Yes");
		UniqueKey_Compliance.put("guaranteeProperlySign", "Yes");
		UniqueKey_Compliance.put("otherDocProperlySign", "Yes");
		UniqueKey_Compliance.put("stateReqAddendumProperlySign", "Yes");
		UniqueKey_Compliance.put("handWrittenChanges", "Yes");
		UniqueKey_Compliance.put("otherAttendaProperlySign", "Yes");
		UniqueKey_Compliance.put("proofControlOverRealEstate", "Yes");
		boolean UpdateCompliance = ActivityPerform(driver, dataSet, UniqueKey_Compliance, DataServicePage.FS,
				DataServicePage.Compliance, DataServicePage.Update, DataServicePage.updateDiv, testCaseId);
		if (!UpdateCompliance)
			fc.utobj().throwsException("Compliance can't be Updated in Fs Module " + testCaseId);
		return UpdateCompliance;

	}

	public boolean CreateRealEstate(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey_RealEstate) throws Exception {
		String LeadParentID = DataServicePage.ReferenceStatus_keys.get(testCaseId + "Primary Info");
		UniqueKey_RealEstate.put("generalContractorSelector", "Yes");
		UniqueKey_RealEstate.put("parentReferenceId", LeadParentID);
		UniqueKey_RealEstate.put("siteAddress2", "2/10 park avenue south city ");
		boolean RealEstateStatus = ActivityPerform(driver, dataSet, UniqueKey_RealEstate, DataServicePage.FS,
				DataServicePage.RealEstate, DataServicePage.Create, DataServicePage.createDiv, testCaseId);
		return RealEstateStatus;

	}

	public boolean UpdateRealEstate(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey_RealEstate) throws Exception {
		UniqueKey_RealEstate.put("siteAddress1", "Address");
		String realEstateID = DataServicePage.ReferenceStatus_keys.get(testCaseId + "Real Estate");
		UniqueKey_RealEstate.put("siteAddress2", "2/10 park avenue south city updated ");
		UniqueKey_RealEstate.put("referenceId", realEstateID);
		boolean UpdateRealEstate = ActivityPerform(driver, dataSet, UniqueKey_RealEstate, DataServicePage.FS,
				DataServicePage.RealEstate, DataServicePage.Update, DataServicePage.updateDiv, testCaseId);
		if (!UpdateRealEstate)
			fc.utobj().throwsException("Real Estate can't be Updated in Fs Module " + testCaseId);
		return UpdateRealEstate;

	}

	public boolean CreateVisit(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey_Visit) throws Exception {
		String LeadParentID = DataServicePage.ReferenceStatus_keys.get(testCaseId + "Primary Info");
		UniqueKey_Visit.put("calenderVisit", "N");
		UniqueKey_Visit.put("parentReferenceId", LeadParentID);
		UniqueKey_Visit.put("visitor1Name", dataSet.get("firstname") + fc.utobj().generateRandomNumber());
		String currentdate = fc.utobj().generateCurrentDatewithformat("yyyy-MM-dd");
		UniqueKey_Visit.put("duedate", currentdate);
		boolean VisitStatus = ActivityPerform(driver, dataSet, UniqueKey_Visit, DataServicePage.FS,
				DataServicePage.Visit, DataServicePage.Create, DataServicePage.createDiv, testCaseId);
		return VisitStatus;

	}

	public boolean UpdateVisit(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey_Visit) throws Exception {
		UniqueKey_Visit.put("calenderVisit", "N");
		UniqueKey_Visit.put("referenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Visit"));
		UniqueKey_Visit.put("visitor1Name", dataSet.get("firstname") + fc.utobj().generateRandomNumber());
		UniqueKey_Visit.put("duedate", fc.utobj().generateCurrentDatewithformat("yyyy-MM-dd"));
		String VisitID = DataServicePage.ReferenceStatus_keys.get(testCaseId + "Visit");
		UniqueKey_Visit.put("parentReferenceId", VisitID);
		boolean UpdateVisit = ActivityPerform(driver, dataSet, UniqueKey_Visit, DataServicePage.FS,
				DataServicePage.Visit, DataServicePage.Update, DataServicePage.updateDiv, testCaseId);
		if (!UpdateVisit)
			fc.utobj().throwsException("Visit can't be Updated in Fs Module " + testCaseId);
		return UpdateVisit;

	}

	public boolean CreateQualificationDetail(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey_QualificationDetail) throws Exception {
		String LeadParentID = DataServicePage.ReferenceStatus_keys.get(testCaseId + "Primary Info");
		UniqueKey_QualificationDetail.put("parentReferenceId", LeadParentID);
		UniqueKey_QualificationDetail.put("firstName", dataSet.get("firstname") + fc.utobj().generateRandomNumber());
		UniqueKey_QualificationDetail.put("gender", "Male");
		UniqueKey_QualificationDetail.put("date", fc.utobj().generateCurrentDatewithformat("yyyy-MM-dd"));
		UniqueKey_QualificationDetail.put("city", "NYC");
		UniqueKey_QualificationDetail.put("stateID", "New York");
		UniqueKey_QualificationDetail.put("usCitizen", "Yes");
		UniqueKey_QualificationDetail.put("homeOwnership", "Own");
		UniqueKey_QualificationDetail.put("maritalStatus", "Single");
		UniqueKey_QualificationDetail.put("spouseUsCitizen", "Yes");
		UniqueKey_QualificationDetail.put("city", "NYC");
		boolean QualificationDetailStatus = ActivityPerform(driver, dataSet, UniqueKey_QualificationDetail,
				DataServicePage.FS, DataServicePage.QualificationDetails, DataServicePage.Create,
				DataServicePage.createDiv, testCaseId);
		return QualificationDetailStatus;

	}

	public boolean UpdateQualificationDetail(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey_QualificationDetail) throws Exception {
		String leadQualificationID = DataServicePage.ReferenceStatus_keys.get(testCaseId + "Qualification Details");
		UniqueKey_QualificationDetail.put("referenceId", leadQualificationID);
		UniqueKey_QualificationDetail.put("firstName", dataSet.get("firstname") + fc.utobj().generateRandomNumber());
		UniqueKey_QualificationDetail.put("gender", "Male");
		UniqueKey_QualificationDetail.put("date", fc.utobj().generateCurrentDatewithformat("yyyy-MM-dd"));
		UniqueKey_QualificationDetail.put("city", "NYC");
		UniqueKey_QualificationDetail.put("stateID", "New York");
		UniqueKey_QualificationDetail.put("usCitizen", "Yes");
		UniqueKey_QualificationDetail.put("homeOwnership", "Own");
		UniqueKey_QualificationDetail.put("maritalStatus", "Single");
		UniqueKey_QualificationDetail.put("spouseUsCitizen", "Yes");
		UniqueKey_QualificationDetail.put("city", "NYC");
		UniqueKey_QualificationDetail.put("city", "NYC");

		boolean UpdateQualificationDetail = ActivityPerform(driver, dataSet, UniqueKey_QualificationDetail,
				DataServicePage.FS, DataServicePage.QualificationDetails, DataServicePage.Update,
				DataServicePage.updateDiv, testCaseId);
		if (!UpdateQualificationDetail)
			fc.utobj().throwsException("Qualification Details can't be Updated in Fs Module " + testCaseId);
		return UpdateQualificationDetail;

	}

	public boolean CreatePersonalProfile(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey_PersonalProfile) throws Exception {
		String LeadParentID = DataServicePage.ReferenceStatus_keys.get(testCaseId + "Primary Info");
		UniqueKey_PersonalProfile.put("parentReferenceId", LeadParentID);
		UniqueKey_PersonalProfile.put("homeOwnership", "Own");
		UniqueKey_PersonalProfile.put("maritalStatus", "Single");
		UniqueKey_PersonalProfile.put("fullTimeBusiness", "Yes");
		UniqueKey_PersonalProfile.put("callAtWork", "No");
		UniqueKey_PersonalProfile.put("limitProforma", "No");
		UniqueKey_PersonalProfile.put("partner", "No");
		UniqueKey_PersonalProfile.put("liabilites", "No");
		UniqueKey_PersonalProfile.put("convictedForFelony", "No");
		UniqueKey_PersonalProfile.put("runYourself", "No");
		UniqueKey_PersonalProfile.put("soleSource", "Yes");
		UniqueKey_PersonalProfile.put("gender", "Male");
		UniqueKey_PersonalProfile.put("homeCity", "Delhi");
		UniqueKey_PersonalProfile.put("bankruptcy", "No");
		UniqueKey_PersonalProfile.put("lawsuit", "No");
		UniqueKey_PersonalProfile.put("convicted", "No");
		boolean PersonalProfileStauts = ActivityPerform(driver, dataSet, UniqueKey_PersonalProfile, DataServicePage.FS,
				DataServicePage.PersonalProfile, DataServicePage.Create, DataServicePage.createDiv, testCaseId);
		return PersonalProfileStauts;

	}

	public boolean UpdatePersonalProfile(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey_PersonalProfile) throws Exception {
		String personalProfileID = DataServicePage.ReferenceStatus_keys.get(testCaseId + "Personal Profile");
		UniqueKey_PersonalProfile.put("referenceId", personalProfileID);
		UniqueKey_PersonalProfile.put("homeOwnership", "Own");
		UniqueKey_PersonalProfile.put("maritalStatus", "Single");
		UniqueKey_PersonalProfile.put("fullTimeBusiness", "Yes");
		UniqueKey_PersonalProfile.put("callAtWork", "No");
		UniqueKey_PersonalProfile.put("limitProforma", "No");
		UniqueKey_PersonalProfile.put("partner", "No");
		UniqueKey_PersonalProfile.put("liabilites", "No");
		UniqueKey_PersonalProfile.put("convictedForFelony", "No");
		UniqueKey_PersonalProfile.put("runYourself", "No");
		UniqueKey_PersonalProfile.put("gender", "Male");
		UniqueKey_PersonalProfile.put("homeCity", "UpdatedDelhi");
		UniqueKey_PersonalProfile.put("soleSource", "Yes");
		UniqueKey_PersonalProfile.put("bankruptcy", "No");
		UniqueKey_PersonalProfile.put("lawsuit", "No");
		UniqueKey_PersonalProfile.put("convicted", "No");
		boolean UpdatePersonalProfile = ActivityPerform(driver, dataSet, UniqueKey_PersonalProfile, DataServicePage.FS,
				DataServicePage.PersonalProfile, DataServicePage.Update, DataServicePage.updateDiv, testCaseId);
		if (!UpdatePersonalProfile)
			fc.utobj().throwsException("PersonalProfile can't be Updated in Fs Module " + testCaseId);
		return UpdatePersonalProfile;

	}

	public boolean CreateEmail(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> uniqueKey_Emails) throws Exception {
		String LeadParentID = DataServicePage.ReferenceStatus_keys.get(testCaseId + "Primary Info");
		uniqueKey_Emails.put("parentReferenceId", LeadParentID);
		uniqueKey_Emails.put("cc", "ravi.pal@franconnect.com");
		uniqueKey_Emails.put("subject", "Test Mail for Sales Primary info ");
		uniqueKey_Emails.put("mailText", "mail is send by RestAPI");
		uniqueKey_Emails.put("mailFrom", "automation@franconnect.com");
		boolean PersonalProfileStauts = ActivityPerform(driver, dataSet, uniqueKey_Emails, DataServicePage.FS,
				DataServicePage.emails, DataServicePage.Create, DataServicePage.createDiv, testCaseId);
		return PersonalProfileStauts;
	}

	public boolean ValidateQualificationDetails(WebDriver driver, Map<String, String> config,
			Map<String, String> uniqueKey_QualificationDetails, Map<String, String> uniqueKey_PrimaryInfo)
			throws Exception {
		uniqueKey_QualificationDetails.remove("referenceId");
		uniqueKey_QualificationDetails.remove("parentReferenceId");
		List<String> listCA = new ArrayList<String>(uniqueKey_QualificationDetails.values());
		fc.utobj().clickLink(driver, DataServicePage.QualificationDetails);
		boolean status = fc.utobj().assertPageSourceWithMultipleRecords(driver, listCA);
		return status;

	}

	public boolean ValidateRealEstate(WebDriver driver, Map<String, String> config,
			Map<String, String> uniqueKey_RealEstate, Map<String, String> uniqueKey_PrimaryInfo) throws Exception {
		uniqueKey_RealEstate.remove("referenceId");
		uniqueKey_RealEstate.remove("parentReferenceId");
		List<String> listCA = new ArrayList<String>(uniqueKey_RealEstate.values());
		fc.utobj().clickLink(driver, DataServicePage.RealEstate);
		boolean status = fc.utobj().assertPageSourceWithMultipleRecords(driver, listCA);
		return status;

	}

	public boolean ValidateRemarks(WebDriver driver, Map<String, String> config, Map<String, String> uniqueKey_Remarks,
			Map<String, String> UniqueKey_PrimaryInfo) throws Exception {
		uniqueKey_Remarks.remove("referenceId");
		uniqueKey_Remarks.remove("parentReferenceId");
		UniqueKey_PrimaryInfo.put("remark", uniqueKey_Remarks.get("remarks"));
		List<String> listCAll = new ArrayList<String>(uniqueKey_Remarks.values());
		boolean FoundExportRemark = fcServices.ValidateForExportRemarks(driver, config, listCAll,
				DataServicePage.Remark, UniqueKey_PrimaryInfo, uniqueKey_Remarks.get("subject"));
		return FoundExportRemark;
	}

	public boolean ValidateTasks(WebDriver driver, Map<String, String> config, Map<String, String> uniqueKey_Tasks,
			Map<String, String> UniqueKey_PrimaryInfo) throws Exception {
		uniqueKey_Tasks.remove("referenceId");
		uniqueKey_Tasks.remove("parentReferenceId");
		UniqueKey_PrimaryInfo.put("calldate", changeFormat(uniqueKey_Tasks.get("date")));
		UniqueKey_PrimaryInfo.put("subjecttask", uniqueKey_Tasks.get("subject"));
		List<String> listCAll = new ArrayList<String>(uniqueKey_Tasks.values());
		boolean FoundExportRemark = fcServices.ValidateForExportTasks(driver, config, listCAll, DataServicePage.Task,
				UniqueKey_PrimaryInfo, uniqueKey_Tasks.get("subject"));
		return FoundExportRemark;
	}

	public boolean ValidateVisits(WebDriver driver, Map<String, String> config, Map<String, String> uniqueKey_Visits,
			Map<String, String> uniqueKey_PrimaryInfo) throws Exception {
		uniqueKey_Visits.remove("referenceId");
		uniqueKey_Visits.remove("parentReferenceId");
		List<String> listCA = new ArrayList<String>(uniqueKey_Visits.values());
		fc.utobj().clickLink(driver, DataServicePage.Visit);
		boolean status = fc.utobj().assertPageSourceWithMultipleRecords(driver, listCA);
		return status;
	}

	public boolean CreateRemarks(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey_Remarks) throws Exception {

		UniqueKey_Remarks.put("parentReferenceId",
				DataServicePage.ReferenceStatus_keys.get(testCaseId + "Primary Info"));
		UniqueKey_Remarks.put("remarks", "Performing the Test Operation");
		driver.navigate().to(config.get("buildUrl") + DataServicePage.restURL);
		boolean RemarksStauts = ActivityPerform(driver, dataSet, UniqueKey_Remarks, DataServicePage.FS,
				DataServicePage.Remark, DataServicePage.Create, DataServicePage.createDiv, testCaseId);
		return RemarksStauts;
	}

	public boolean UpdateRemarks(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey_Remarks) throws Exception {
		String RemarksID = DataServicePage.ReferenceStatus_keys.get(testCaseId + "Remark");
		UniqueKey_Remarks.put("parentReferenceId",
				DataServicePage.ReferenceStatus_keys.get(testCaseId + "Primary Info"));
		UniqueKey_Remarks.put("remarks", "Performing updation in the Test Operation");
		UniqueKey_Remarks.put("referenceId", RemarksID);
		driver.navigate().to(config.get("buildUrl") + DataServicePage.restURL);
		boolean RemarksStauts = ActivityPerform(driver, dataSet, UniqueKey_Remarks, DataServicePage.FS,
				DataServicePage.Remark, DataServicePage.Update, DataServicePage.updateDiv, testCaseId);

		return RemarksStauts;
	}

	public boolean CreateTasks(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey_Task) throws Exception {

		UniqueKey_Task.put("parentReferenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Primary Info"));
		UniqueKey_Task.put("assignmentTo", "OWNER");
		UniqueKey_Task.put("status", "Work In Progress");
		UniqueKey_Task.put("taskType", "Default");
		UniqueKey_Task.put("subject", "Test" + fc.utobj().generateRandomNumber());
		String date = fc.utobj().generatefutureDatewithformat("yyyy-MM-dd", 3);
		UniqueKey_Task.put("date", date);
		UniqueKey_Task.put("timelessTask", "Y");
		driver.navigate().to(config.get("buildUrl") + DataServicePage.restURL);
		boolean TaskStauts = ActivityPerform(driver, dataSet, UniqueKey_Task, DataServicePage.FS, DataServicePage.Task,
				DataServicePage.Create, DataServicePage.createDiv, testCaseId);
		return TaskStauts;
	}

	public boolean UpdateTasks(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey_Task) throws Exception {
		String TaskID = DataServicePage.ReferenceStatus_keys.get(testCaseId + "Task");
		UniqueKey_Task.put("parentReferenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Primary Info"));
		UniqueKey_Task.put("assignmentTo", "OWNER");
		UniqueKey_Task.put("status", "Work In Progress");
		UniqueKey_Task.put("referenceId", TaskID);
		UniqueKey_Task.put("taskType", "Default");
		UniqueKey_Task.put("subject", "Test" + fc.utobj().generateRandomNumber());
		String date = fc.utobj().generateCurrentDatewithformat("yyyy-MM-dd");
		UniqueKey_Task.put("date", date);
		UniqueKey_Task.put("timelessTask", "Y");
		driver.navigate().to(config.get("buildUrl") + DataServicePage.restURL);
		boolean TaskStauts = ActivityPerform(driver, dataSet, UniqueKey_Task, DataServicePage.FS, DataServicePage.Task,
				DataServicePage.Update, DataServicePage.updateDiv, testCaseId);
		return TaskStauts;
	}

	public boolean DeleteTasks(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey_Task) throws Exception {
		Map<String, String> TempMap = new HashMap<String, String>();
		TempMap.put("referenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Task"));
		UniqueKey_Task.put("referenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Task"));
		boolean DeleteTask = ActivityPerform(driver, dataSet, TempMap, "FS", "Task", "Delete", "deleteDiv", testCaseId);
		if (!DeleteTask) {
			fc.utobj().throwsException("Task not deleted in restApi " + testCaseId);
		}
		return DeleteTask;
	}

}
