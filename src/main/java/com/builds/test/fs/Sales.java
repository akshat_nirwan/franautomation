package com.builds.test.fs;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.fs.SalesUI;
import com.builds.utilities.FranconnectUtil;

public class Sales {

	public void homePage(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		SalesUI pobj = new SalesUI(driver);

		fc.utobj().clickElement(driver, pobj.salesHomePageLnk);
	}

	public void leadManagement(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		SalesUI pobj = new SalesUI(driver);

		fc.utobj().printTestStep("Go to Lead Management");

		try
		{
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			fc.utobj().clickElement(driver, pobj.leadManagementLnk);
		}catch(Exception e)
		{
			fc.commonMethods().getModules().clickSalesModule(driver);
			fc.utobj().clickElement(driver, pobj.leadManagementLnk);
		}finally {
			
			driver.manage().timeouts().implicitlyWait(45, TimeUnit.SECONDS);
		}
		
	}

	// Anukaran
	public void workflows(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		SalesUI pobj = new SalesUI(driver);

		fc.utobj().clickElement(driver, pobj.Workflows);
	}

	public void groups(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		fc.utobj().printTestStep("Click on Groups Tab");
		SalesUI pobj = new SalesUI(driver);

		try {
			fc.utobj().clickElement(driver, pobj.groupsPageLnk);
		} catch (Exception e) {
			System.out.println("Unable to Click on Module");
		}
	}

	public void search(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		SalesUI pobj = new SalesUI(driver);

		fc.utobj().clickElementByJS(driver, pobj.search);
	}

	public void emailCampaignTab(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		SalesUI pobj = new SalesUI(driver);

		fc.utobj().clickElement(driver, pobj.campaign);
	}

	public void fdd(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		SalesUI pobj = new SalesUI(driver);

		fc.utobj().clickElement(driver, pobj.FDD);
	}

	public void tasks(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		SalesUI pobj = new SalesUI(driver);

		fc.utobj().clickElement(driver, pobj.Tasks);
	}

	public void calendar(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		SalesUI pobj = new SalesUI(driver);

		fc.utobj().clickElement(driver, pobj.Calendar);
	}

	public void importPage(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		SalesUI pobj = new SalesUI(driver);

		fc.utobj().clickElement(driver, pobj.Import);
	}

	public void exportPage(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		SalesUI pobj = new SalesUI(driver);

		fc.utobj().clickElement(driver, pobj.Export);
	}

	public void mailMerge(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		SalesUI pobj = new SalesUI(driver);

		fc.utobj().clickElement(driver, pobj.MailMerge);
	}

	public void sites(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		SalesUI pobj = new SalesUI(driver);

		fc.utobj().clickElement(driver, pobj.Sites);
	}

	public void brokers(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		SalesUI pobj = new SalesUI(driver);

		fc.utobj().clickElement(driver, pobj.Brokers);
	}

	public void reports(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		SalesUI pobj = new SalesUI(driver);

		fc.utobj().clickElement(driver, pobj.Reports);
	}

	public void dashboard(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		SalesUI pobj = new SalesUI(driver);

		fc.utobj().clickElement(driver, pobj.Dashboard);
	}

	public void archivedEmailCampaignTab(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		SalesUI pobj = new SalesUI(driver);

		emailCampaignTab(driver);
		fc.utobj().clickElement(driver, pobj.fsArchivedCampaignSummaryLnk);
	}

	public void emailTemplateTab(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		SalesUI pobj = new SalesUI(driver);

		emailCampaignTab(driver);
		fc.utobj().clickElement(driver, pobj.fstemplateSummaryLnk);
	}

	public void infomercialsTab(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		SalesUI pobj = new SalesUI(driver);

		emailCampaignTab(driver);
		fc.utobj().clickElement(driver, pobj.fsInfomercialsLnk);
	}

	public void campaignTriggerTab(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		SalesUI pobj = new SalesUI(driver);

		emailCampaignTab(driver);
		fc.utobj().clickElement(driver, pobj.fsCampaignTriggersLnk);
	}

	public void templateFolderTab(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		SalesUI pobj = new SalesUI(driver);

		emailCampaignTab(driver);
		fc.utobj().clickElement(driver, pobj.templateFolderLnk);
	}

	public void mySignatureTab(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		SalesUI pobj = new SalesUI(driver);

		emailCampaignTab(driver);
		fc.utobj().clickElement(driver, pobj.modifySignatureLnk);
	}
}
