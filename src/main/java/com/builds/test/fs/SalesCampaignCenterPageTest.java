package com.builds.test.fs;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.fs.SalesCampaignCenterPage;
import com.builds.utilities.FranconnectUtil;

class SalesCampaignCenterPageTest {

	FranconnectUtil fc = new FranconnectUtil();

	public void clickDashboard_left() {

	}

	public void clickCampaigns_left() {

	}

	public void clickTemplates_left() {

	}

	public void clickRecipientsGroups_left() {

	}

	public void clickWorkflows_left() {

	}

	public void clickCreate(WebDriver driver) throws Exception {
		SalesCampaignCenterPage pobj = new SalesCampaignCenterPage(driver);
		fc.utobj().clickElement(driver, pobj.createBtn);
	}

	public void createCampaign_right() {

	}

	public void createTemplate_right(WebDriver driver) throws Exception {

	}

	public void createRecipientsGroups_right() {

	}

	public void createWorkflow_right() {

	}

	public void createCampaign_bottom() {

	}

	public void manageTemplates_bottom() {

	}

	public void createGroup_bottom() {

	}

	public void manageWorkflows_bottom() {

	}

}
