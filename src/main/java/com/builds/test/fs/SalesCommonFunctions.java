package com.builds.test.fs;

import java.util.HashSet;
import java.util.Set;

import org.openqa.selenium.WebDriver;
import org.testng.Reporter;

import com.builds.uimaps.fs.FSGroupsPage;
import com.builds.utilities.FranconnectUtil;

class SalesCommonFunctions {

	public void salesGroupsSearch(WebDriver driver, String groupName, String creationDate, String groupType)
			throws Exception {
		FranconnectUtil fc = new FranconnectUtil();

		FSGroupsPage groupPage = new FSGroupsPage(driver);

		fc.utobj().clickElement(driver, groupPage.openSearchFilters);

		if (groupName != null) {
			fc.utobj().sendKeys(driver, groupPage.searchTxt, groupName);
		}

		if (creationDate != null) {
			fc.utobj().clickElement(driver, groupPage.creationDateInput);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//ul[@id='matchType1UL']//span[contains(text(),'Is Between')]"));
		}

		if (groupType != null) {
			fc.utobj().clickElement(driver, groupPage.groupTypeDiv);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//span[contains(text(),'" + groupType + "')]"));
		}

	}

	public void VerifyPrintPreview(WebDriver driver, String textTobeVerified) throws Exception {
		Set<String> windows = new HashSet<String>();
		windows = driver.getWindowHandles();
		FranconnectUtil fc = new FranconnectUtil();

		boolean isTextDisplayed = false;

		String currentWindow = driver.getWindowHandle();
		for (String window : windows) {
			if (!window.equals(currentWindow)) {

				driver.switchTo().window(window);
				if (driver.getCurrentUrl().contains("printcheck")) {
					isTextDisplayed = fc.utobj().assertPageSource(driver, textTobeVerified);
				}
			}
		}
		driver.close();

		if (isTextDisplayed) {
			Reporter.log(textTobeVerified + "  is displayed on the print preview page. !!! ");
		} else {
			fc.utobj().throwsException(textTobeVerified + "  is not displayed on the print preview page. !!! ");
		}

		driver.switchTo().window(currentWindow);
	}

}
