package com.builds.test.fs;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

class SalesGroupsPage_Test {

	@Test(groups = { "runsales13", "sales_group" })
	@TestCase(createdOn = "2018-02-06", updatedOn = "2018-02-06", testCaseId = "Sales_Regular_Groups_AdvancedSearch_Export", testCaseDescription = "Verify the lead associated with group is getting searched on basis of group at search / advanced search and export.")
	private void Sales_Regular_Groups_AdvancedSearch_Export() throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Sales sales = new Sales();
		GroupTest groupTest = new GroupTest();
		Group group = new Group();
		LeadManagementTest lm = new LeadManagementTest(driver);
		Lead ld = new Lead();

		try {
			driver = fc.loginpage().login(driver);
			fc.sales().sales_common().fsModule(driver);

			group = fc.sales().sales_common().fillDefaultValue_RegularGroupDetails(group);
			groupTest.addGroupDetails_AndCreate(driver, group);

			ld = fc.sales().sales_common().fillDefaultValue_LeadDetails(ld);
			sales.leadManagement(driver);
			lm.addLeadAndSave(ld);

			// TODO : Process remaining

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales", "sales_group" , "Sales_Regular_Groups_Public_Archive_001" }) // Verified : Akshat
	@TestCase(createdOn = "2018-01-17", updatedOn = "2018-01-17", testCaseId = "Sales_Regular_Groups_Public_Archive_001", testCaseDescription = "Add, Archive and Restore - Regular Public Group")

	private void Sales_Regular_Groups_Archive_001() throws Exception {

		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Sales sales = new Sales();
		Group group = new Group();
		GroupTest gdp = new GroupTest();

		try {
			driver = fc.loginpage().login(driver);
			fc.sales().sales_common().fsModule(driver);

			sales.groups(driver);

			group = fc.sales().sales_common().fillDefaultValue_RegularGroupDetails(group);

			gdp.addGroupDetails_AndCreate(driver, group);

			/*
			 * Verify group archive from action
			 */

			gdp.salesGroupsSearch(driver, group);
			fc.utobj().assertSingleLinkText(driver, group.getName());
			gdp.archiveGroup(driver, group);
			gdp.salesGroupsSearch(driver, group);
			fc.utobj().assertLinkTextNotPresent(driver, group.getName());
			gdp.goToArchiveGroupTab(driver);
			gdp.salesGroupsSearch(driver, group);
			fc.utobj().assertSingleLinkText(driver, group.getName());
			gdp.restoreGroup(driver, group);
			gdp.salesGroupsSearch(driver, group);
			fc.utobj().assertLinkTextNotPresent(driver, group.getName());
			gdp.goToGroupTab(driver);
			gdp.salesGroupsSearch(driver, group);
			fc.utobj().assertSingleLinkText(driver, group.getName());

			/*
			 * Verify group archive from batch bottom button
			 */

			gdp.checkGroupName(driver, group);
			gdp.archiveGroup_Batch_Bottom(driver, group);
			gdp.goToArchiveGroupTab(driver);
			gdp.salesGroupsSearch(driver, group);
			fc.utobj().assertSingleLinkText(driver, group.getName());
			gdp.restoreGroup(driver, group);
			gdp.salesGroupsSearch(driver, group);
			fc.utobj().assertLinkTextNotPresent(driver, group.getName());
			gdp.goToGroupTab(driver);
			gdp.salesGroupsSearch(driver, group);
			fc.utobj().assertSingleLinkText(driver, group.getName());

			/*
			 * Verify group archive from batch top button
			 */

			gdp.checkGroupName(driver, group);
			gdp.archiveGroup_Batch_Top(driver, group);
			gdp.goToArchiveGroupTab(driver);
			gdp.salesGroupsSearch(driver, group);
			fc.utobj().assertSingleLinkText(driver, group.getName());
			gdp.restoreGroup(driver, group);
			gdp.salesGroupsSearch(driver, group);
			fc.utobj().assertLinkTextNotPresent(driver, group.getName());
			gdp.goToGroupTab(driver);
			gdp.salesGroupsSearch(driver, group);
			fc.utobj().assertSingleLinkText(driver, group.getName());

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales", "sales_group" , "Sales_Regular_Groups_Private_Archive_002" }) // Verified : Akshat
	@TestCase(createdOn = "2018-01-18", updatedOn = "2018-01-18", testCaseId = "Sales_Regular_Groups_Private_Archive_002", testCaseDescription = "Add, Archive and Restore - Regular Private Group")
	private void Sales_Smart_Groups_Archive_002() throws Exception {

		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.sales().sales_common().fsModule(driver);

			Sales sales = new Sales();
			sales.groups(driver);

			Group group = new Group();
			group = fc.sales().sales_common().fillDefaultValue_RegularGroupDetails(group);
			group.setVisibility("Private");

			GroupTest gdp = new GroupTest();
			gdp.addGroupDetails_AndCreate(driver, group);

			/*
			 * Verify group archive from action
			 */

			gdp.salesGroupsSearch(driver, group);
			fc.utobj().assertSingleLinkText(driver, group.getName());
			gdp.archiveGroup(driver, group);
			gdp.salesGroupsSearch(driver, group);
			fc.utobj().assertLinkTextNotPresent(driver, group.getName());
			gdp.goToArchiveGroupTab(driver);
			gdp.salesGroupsSearch(driver, group);
			fc.utobj().assertSingleLinkText(driver, group.getName());
			gdp.restoreGroup(driver, group);
			gdp.salesGroupsSearch(driver, group);
			fc.utobj().assertLinkTextNotPresent(driver, group.getName());
			gdp.goToGroupTab(driver);
			gdp.salesGroupsSearch(driver, group);
			fc.utobj().assertSingleLinkText(driver, group.getName());

			/*
			 * Verify group archive from batch bottom button
			 */

			gdp.checkGroupName(driver, group);
			gdp.archiveGroup_Batch_Bottom(driver, group);
			gdp.goToArchiveGroupTab(driver);
			gdp.salesGroupsSearch(driver, group);
			fc.utobj().assertSingleLinkText(driver, group.getName());
			gdp.restoreGroup(driver, group);
			gdp.salesGroupsSearch(driver, group);
			fc.utobj().assertLinkTextNotPresent(driver, group.getName());
			gdp.goToGroupTab(driver);
			gdp.salesGroupsSearch(driver, group);
			fc.utobj().assertSingleLinkText(driver, group.getName());

			/*
			 * Verify group archive from batch top button
			 */

			gdp.checkGroupName(driver, group);
			gdp.archiveGroup_Batch_Top(driver, group);
			gdp.goToArchiveGroupTab(driver);
			gdp.salesGroupsSearch(driver, group);
			fc.utobj().assertSingleLinkText(driver, group.getName());
			gdp.restoreGroup(driver, group);
			gdp.salesGroupsSearch(driver, group);
			fc.utobj().assertLinkTextNotPresent(driver, group.getName());
			gdp.goToGroupTab(driver);
			gdp.salesGroupsSearch(driver, group);
			fc.utobj().assertSingleLinkText(driver, group.getName());

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales", "sales_group","sales_failed" })
	@TestCase(createdOn = "2018-01-18", updatedOn = "2018-01-18", testCaseId = "Sales_Regular_Groups_Public_Modify_003", testCaseDescription = "Add and Modify - Regular Public Group")
	private void Sales_Regular_Groups_Public_Modify_003() throws Exception {

		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String name = fc.utobj().generateTestData("Test");
		String description = fc.utobj().generateTestData("Description");

		try {
			driver = fc.loginpage().login(driver);
			fc.sales().sales_common().fsModule(driver);

			Sales sales = new Sales();
			sales.groups(driver);

			Group group = new Group();
			group = fc.sales().sales_common().fillDefaultValue_RegularGroupDetails(group);

			GroupTest gdp = new GroupTest();
			gdp.addGroupDetails_AndCreate(driver, group);

			/*
			 * Verify group archive from action
			 */

			gdp.salesGroupsSearch(driver, group);
			fc.utobj().assertSingleLinkText(driver, group.getName());

			gdp.modifyRegularGroup(driver, group);

			group.setName("Modified_" + name);
			group.setDescription("Modified_" + description);

			gdp.fillGroupDetails(driver, group);
			gdp.clickModifyButton(driver);

			gdp.salesGroupsSearch(driver, group);
			fc.utobj().assertSingleLinkText(driver, group.getName());

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales", "sales_group","sales_failed" })
	@TestCase(createdOn = "2018-01-18", updatedOn = "2018-01-18", testCaseId = "Sales_Regular_Groups_Public_Delete_004", testCaseDescription = "Add and Delete - Regular Public Group")
	private void Sales_Regular_Groups_Public_Delete_004() throws Exception {

		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Sales sales = new Sales();
		Group group = new Group();
		GroupTest gdp = new GroupTest();

		try {
			driver = fc.loginpage().login(driver);
			fc.sales().sales_common().fsModule(driver);

			sales.groups(driver);

			group = fc.sales().sales_common().fillDefaultValue_RegularGroupDetails(group);

			gdp.addGroupDetails_AndCreate(driver, group);

			/*
			 * Verify group delete from action
			 */

			gdp.salesGroupsSearch(driver, group);
			fc.utobj().assertSingleLinkText(driver, group.getName());
			gdp.deleteGroup(driver, group);
			gdp.salesGroupsSearch(driver, group);
			fc.utobj().assertLinkTextNotPresent(driver, group.getName());

			/*
			 * Verify group delete from batch bottom button
			 */

			gdp = new GroupTest();
			gdp.addGroupDetails_AndCreate(driver, group);

			gdp.salesGroupsSearch(driver, group);
			gdp.checkGroupName(driver, group);
			gdp.deleteGroup_Batch_Bottom(driver, group);
			gdp.salesGroupsSearch(driver, group);
			fc.utobj().assertLinkTextNotPresent(driver, group.getName());

			/*
			 * Verify group delete from batch top button
			 */

			gdp = new GroupTest();
			gdp.addGroupDetails_AndCreate(driver, group);

			gdp.checkGroupName(driver, group);
			gdp.deleteGroup_Batch_Top(driver, group);
			gdp.salesGroupsSearch(driver, group);
			fc.utobj().assertLinkTextNotPresent(driver, group.getName());

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales", "sales_group","sales_failed" })
	@TestCase(createdOn = "2018-01-19", updatedOn = "2018-01-19", testCaseId = "Sales_Regular_Groups_AddLeadAndRemovefromGroup_005", testCaseDescription = "Add Regular Public Group - Associate and Remove lead from group")

	private void Sales_Regular_Groups_AddLeadAndRemovefromGroup_005() throws Exception {

		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Group group = new Group();
		Sales sales = new Sales();
		LeadTest ldp = new LeadTest();
		Lead ld = new Lead();
		LeadManagementTest lm = new LeadManagementTest(driver);
		GroupTest gdp = new GroupTest();

		try {
			driver = fc.loginpage().login(driver);
			fc.sales().sales_common().fsModule(driver);

			sales.leadManagement(driver);

			lm.clickAddLead(driver);

			ld = fc.sales().sales_common().fillDefaultValue_LeadDetails(ld);

			ldp.addLeadAndClickSave(driver, ld);

			sales.groups(driver);

			group = fc.sales().sales_common().fillDefaultValue_RegularGroupDetails(group);

			gdp.addGroupDetails_AndCreate(driver, group);

			gdp.salesGroupsSearch(driver, group);
			fc.utobj().assertSingleLinkText(driver, group.getName());

			gdp.clickAssociateWithLead(driver, group);

			gdp.fillAndSearchLeadInfoInGroupFilterPage(driver, ld);

			gdp.salesGroupsSearch(driver, group);
			fc.utobj().assertSingleLinkText(driver, group.getName());

			gdp.clickLeadCountForGroup(driver, group);
			fc.utobj().assertSingleLinkText(driver, ld.getLeadFullName());

			lm.clickCheckBoxForLead(ld);
			lm.removeFromGroup(driver);
			fc.utobj().assertLinkTextNotPresent(driver, ld.getLeadFullName());

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales", "sales_group","sales_failed" })
	@TestCase(createdOn = "2018-01-19", updatedOn = "2018-01-19", testCaseId = "Sales_Regular_Groups_VerifyGroupCount_006", testCaseDescription = "Add Regular Public Group and verify group count")

	private void Sales_Regular_Groups_VerifyGroupCount_006() throws Exception {

		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Sales sales = new Sales();
		Group group = new Group();
		GroupTest gdp = new GroupTest();

		try {
			driver = fc.loginpage().login(driver);
			fc.sales().sales_common().fsModule(driver);

			sales.groups(driver);
			group = fc.sales().sales_common().fillDefaultValue_RegularGroupDetails(group);
			gdp.addGroupDetails_AndCreate(driver, group);

			gdp.salesGroupsSearch(driver, group);
			fc.utobj().assertSingleLinkText(driver, group.getName());

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales", "sales_group","sales_failed" })
	@TestCase(createdOn = "2018-01-19", updatedOn = "2018-01-19", testCaseId = "Sales_Regular_SpecialCharacterGroups_007", testCaseDescription = "Add Regular Public Group with special character")
	private void Sales_Regular_SpecialCharacterGroups_007() throws Exception {

		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.sales().sales_common().fsModule(driver);

			Sales sales = new Sales();
			sales.groups(driver);

			Group group = new Group();
			group = fc.sales().sales_common().fillDefaultValue_RegularGroupDetails(group);

			GroupTest gdp = new GroupTest();
			gdp.addGroupDetails_AndCreate(driver, group);

			gdp.salesGroupsSearch(driver, group);
			fc.utobj().assertSingleLinkText(driver, group.getName());

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales", "sales_group" , "Sales_Smart_Groups_Public_Archive_001" }) // fixed : verified // akshat
	@TestCase(createdOn = "2018-01-18", updatedOn = "2018-01-18", testCaseId = "Sales_Smart_Groups_Public_Archive_001", testCaseDescription = "Add, Archive and Restore - Smart Public Group")
	private void Sales_Smart_Groups_Public_Archive_001() throws Exception {

		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.sales().sales_common().fsModule(driver);

			Sales sales = new Sales();
			sales.groups(driver);

			Group group = new Group();
			group = fc.sales().sales_common().fillDefaultValue_SmartGroupDetails(group);

			GroupTest gdp = new GroupTest();

			gdp.addGroupDetails_AndCreate(driver, group);

			gdp.clickFiltersForAvailableFields(driver, "Co-Applicants", "First Name");
			gdp.selectFieldsValueInFilters(driver, "Co-Applicants", "First Name", "select", "1", "Equals");
			gdp.selectFieldsValueInFilters(driver, "Co-Applicants", "First Name", "input", "1", "Test Record");

			gdp.clickSaveBtn(driver);
			gdp.acceptAlertAfterSavingFilters(driver);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//button[contains(text(),'Ok')]"));

			// gdp.clickSaveBtn(driver);

			/*
			 * Verify group archive from action
			 */

			gdp.salesGroupsSearch(driver, group);
			fc.utobj().assertSingleLinkText(driver, group.getName());
			gdp.archiveGroup(driver, group);
			gdp.salesGroupsSearch(driver, group);
			fc.utobj().assertLinkTextNotPresent(driver, group.getName());
			gdp.goToArchiveGroupTab(driver);
			gdp.salesGroupsSearch(driver, group);
			fc.utobj().assertSingleLinkText(driver, group.getName());
			gdp.restoreGroup(driver, group);
			gdp.salesGroupsSearch(driver, group);
			fc.utobj().assertLinkTextNotPresent(driver, group.getName());
			gdp.goToGroupTab(driver);
			gdp.salesGroupsSearch(driver, group);
			fc.utobj().assertSingleLinkText(driver, group.getName());

			/*
			 * Verify group archive from batch bottom button
			 */

			gdp.checkGroupName(driver, group);
			gdp.archiveGroup_Batch_Bottom(driver, group);
			gdp.goToArchiveGroupTab(driver);
			gdp.salesGroupsSearch(driver, group);
			fc.utobj().assertSingleLinkText(driver, group.getName());
			gdp.restoreGroup(driver, group);
			gdp.salesGroupsSearch(driver, group);
			fc.utobj().assertLinkTextNotPresent(driver, group.getName());
			gdp.goToGroupTab(driver);
			gdp.salesGroupsSearch(driver, group);
			fc.utobj().assertSingleLinkText(driver, group.getName());

			/*
			 * Verify group archive from batch top button
			 */

			gdp.checkGroupName(driver, group);
			gdp.archiveGroup_Batch_Top(driver, group);
			gdp.goToArchiveGroupTab(driver);
			gdp.salesGroupsSearch(driver, group);
			fc.utobj().assertSingleLinkText(driver, group.getName());
			gdp.restoreGroup(driver, group);
			gdp.salesGroupsSearch(driver, group);
			fc.utobj().assertLinkTextNotPresent(driver, group.getName());
			gdp.goToGroupTab(driver);
			gdp.salesGroupsSearch(driver, group);
			fc.utobj().assertSingleLinkText(driver, group.getName());

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales", "sales_group" , "Sales_Smart_Groups_Private_Archive_002" }) // fixed : verified // akshat
	@TestCase(createdOn = "2018-01-18", updatedOn = "2018-01-18", testCaseId = "Sales_Smart_Groups_Private_Archive_002", testCaseDescription = "Add, Archive and Restore - Smart Private Group")
	private void Sales_Smart_Groups_Private_Archive_002() throws Exception {

		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.sales().sales_common().fsModule(driver);

			Sales sales = new Sales();
			sales.groups(driver);

			Group group = new Group();
			group = fc.sales().sales_common().fillDefaultValue_SmartGroupDetails(group);
			group.setVisibility("Private");

			GroupTest gdp = new GroupTest();

			gdp.addGroupDetails_AndCreate(driver, group);

			gdp.clickFiltersForAvailableFields(driver, "Co-Applicants", "First Name");
			gdp.selectFieldsValueInFilters(driver, "Co-Applicants", "First Name", "select", "1", "Equals");
			gdp.selectFieldsValueInFilters(driver, "Co-Applicants", "First Name", "input", "1", "Test Record");

			gdp.clickSaveBtn(driver);
			gdp.acceptAlertAfterSavingFilters(driver);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//button[contains(text(),'Ok')]"));
			// gdp.clickSaveBtn(driver);

			// System.out.println("1");
			
			/*
			 * Verify group archive from action
			 */

			gdp.salesGroupsSearch(driver, group);
			fc.utobj().assertSingleLinkText(driver, group.getName());
			gdp.archiveGroup(driver, group);
			gdp.salesGroupsSearch(driver, group);
			fc.utobj().assertLinkTextNotPresent(driver, group.getName());
			gdp.goToArchiveGroupTab(driver);
			gdp.salesGroupsSearch(driver, group);
			fc.utobj().assertSingleLinkText(driver, group.getName());
			gdp.restoreGroup(driver, group);
			gdp.salesGroupsSearch(driver, group);
			fc.utobj().assertLinkTextNotPresent(driver, group.getName());
			gdp.goToGroupTab(driver);
			gdp.salesGroupsSearch(driver, group);
			fc.utobj().assertSingleLinkText(driver, group.getName());

			/*
			 * Verify group archive from batch bottom button
			 */
			
			// System.out.println("2");

			gdp.checkGroupName(driver, group);
			gdp.archiveGroup_Batch_Bottom(driver, group);
			gdp.goToArchiveGroupTab(driver);
			gdp.salesGroupsSearch(driver, group);
			fc.utobj().assertSingleLinkText(driver, group.getName());
			gdp.restoreGroup(driver, group);
			gdp.salesGroupsSearch(driver, group);
			fc.utobj().assertLinkTextNotPresent(driver, group.getName());
			gdp.goToGroupTab(driver);
			gdp.salesGroupsSearch(driver, group);
			fc.utobj().assertSingleLinkText(driver, group.getName());

			/*
			 * Verify group archive from batch top button
			 */

			gdp.checkGroupName(driver, group);
			gdp.archiveGroup_Batch_Top(driver, group);
			gdp.goToArchiveGroupTab(driver);
			gdp.salesGroupsSearch(driver, group);
			fc.utobj().assertSingleLinkText(driver, group.getName());
			gdp.restoreGroup(driver, group);
			gdp.salesGroupsSearch(driver, group);
			fc.utobj().assertLinkTextNotPresent(driver, group.getName());
			gdp.goToGroupTab(driver);
			gdp.salesGroupsSearch(driver, group);
			fc.utobj().assertSingleLinkText(driver, group.getName());

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales", "sales_group","sales_failed" })
	@TestCase(createdOn = "2018-01-18", updatedOn = "2018-01-18", testCaseId = "Sales_Smart_Groups_Public_CreateAndCancel_003", testCaseDescription = "Add and then cancel the creation of - Smart Public Group")
	private void Sales_Smart_Groups_Public_CreateAndCancel_003() throws Exception {

		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.sales().sales_common().fsModule(driver);

			Sales sales = new Sales();
			sales.groups(driver);

			Group group = new Group();
			group = fc.sales().sales_common().fillDefaultValue_SmartGroupDetails(group);

			GroupTest gdp = new GroupTest();

			gdp.addGroupDetails_AndCreate(driver, group);

			gdp.clickFiltersForAvailableFields(driver, "Co-Applicants", "First Name");
			gdp.selectFieldsValueInFilters(driver, "Co-Applicants", "First Name", "select", "1", "Equals");
			gdp.selectFieldsValueInFilters(driver, "Co-Applicants", "First Name", "input", "1", "Test Record");

			gdp.clickDeleteFilter(driver, "Co-Applicants", "First Name");

			gdp.clickCancelBtn(driver);

			gdp.salesGroupsSearch(driver, group);
			fc.utobj().assertSingleLinkText(driver, group.getName());

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

}
