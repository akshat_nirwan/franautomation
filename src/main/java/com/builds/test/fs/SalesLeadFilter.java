package com.builds.test.fs;

class SalesLeadFilter {

	private String salutation;
	private String firstName;
	private String lastName;
	private String address;
	private String address2;
	private String city;
	private String country;
	private String stateID;
	private String zip;
	private String countyID;
	private String primaryPhoneToCall;
	private String bestTimeToContact;
	private String phone;
	private String phoneExt;
	private String homePhone;
	private String homePhoneExt;
	private String fax;
	private String mobile;
	private String emailID;
	private String companyName;
	private String comments;
	private String sendAutomaticMail;
	private String assignUser;
	private String leadOwnerID;
	private String automatic;
	private String leadRatingID;
	private String marketingCodeId;
	private String leadSource2ID;
	private String leadSource3ID;
	private String otherLeadSourceDetail;
	private String brokerID;
	private String liquidCapitalMin;
	private String liquidCapitalMax;
	private String investTimeframe;
	private String background;
	private String sourceOfFunding;
	private String nextCallDate;
	private String noOfUnitReq;
	private String statusChangeDate;
	private String noOfFieldSold;
	private String dateOfOpen;
	private String forecastClosureDate;
	private String probability;
	private String forecastRating;
	private String forecastRevenue;
	private String assignAutomaticCampaign;
	private String campaignID;
	private String brandMapping_0brandID;

	public String getsalutation() {
		return salutation;
	}

	public String getfirstName() {
		return firstName;
	}

	public String getlastName() {
		return lastName;
	}

	public String getaddress() {
		return address;
	}

	public String getaddress2() {
		return address2;
	}

	public String getcity() {
		return city;
	}

	public String getcountry() {
		return country;
	}

	public String getstateID() {
		return stateID;
	}

	public String getzip() {
		return zip;
	}

	public String getcountyID() {
		return countyID;
	}

	public String getprimaryPhoneToCall() {
		return primaryPhoneToCall;
	}

	public String getbestTimeToContact() {
		return bestTimeToContact;
	}

	public String getphone() {
		return phone;
	}

	public String getphoneExt() {
		return phoneExt;
	}

	public String gethomePhone() {
		return homePhone;
	}

	public String gethomePhoneExt() {
		return homePhoneExt;
	}

	public String getfax() {
		return fax;
	}

	public String getmobile() {
		return mobile;
	}

	public String getemailID() {
		return emailID;
	}

	public String getcompanyName() {
		return companyName;
	}

	public String getcomments() {
		return comments;
	}

	public String getsendAutomaticMail() {
		return sendAutomaticMail;
	}

	public String getassignUser() {
		return assignUser;
	}

	public String getleadOwnerID() {
		return leadOwnerID;
	}

	public String getautomatic() {
		return automatic;
	}

	public String getleadRatingID() {
		return leadRatingID;
	}

	public String getmarketingCodeId() {
		return marketingCodeId;
	}

	public String getleadSource2ID() {
		return leadSource2ID;
	}

	public String getleadSource3ID() {
		return leadSource3ID;
	}

	public String getotherLeadSourceDetail() {
		return otherLeadSourceDetail;
	}

	public String getbrokerID() {
		return brokerID;
	}

	public String getliquidCapitalMin() {
		return liquidCapitalMin;
	}

	public String getliquidCapitalMax() {
		return liquidCapitalMax;
	}

	public String getinvestTimeframe() {
		return investTimeframe;
	}

	public String getbackground() {
		return background;
	}

	public String getsourceOfFunding() {
		return sourceOfFunding;
	}

	public String getnextCallDate() {
		return nextCallDate;
	}

	public String getnoOfUnitReq() {
		return noOfUnitReq;
	}

	public String getstatusChangeDate() {
		return statusChangeDate;
	}

	public String getnoOfFieldSold() {
		return noOfFieldSold;
	}

	public String getdateOfOpen() {
		return dateOfOpen;
	}

	public String getforecastClosureDate() {
		return forecastClosureDate;
	}

	public String getprobability() {
		return probability;
	}

	public String getforecastRating() {
		return forecastRating;
	}

	public String getforecastRevenue() {
		return forecastRevenue;
	}

	public String getassignAutomaticCampaign() {
		return assignAutomaticCampaign;
	}

	public String getcampaignID() {
		return campaignID;
	}

	public String getbrandMapping_0brandID() {
		return brandMapping_0brandID;
	}

	public void setsalutation(String salutation) {
		this.salutation = salutation;
	}

	public void setfirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setlastName(String lastName) {
		this.lastName = lastName;
	}

	public void setaddress(String address) {
		this.address = address;
	}

	public void setaddress2(String address2) {
		this.address2 = address2;
	}

	public void setcity(String city) {
		this.city = city;
	}

	public void setcountry(String country) {
		this.country = country;
	}

	public void setstateID(String stateID) {
		this.stateID = stateID;
	}

	public void setzip(String zip) {
		this.zip = zip;
	}

	public void setcountyID(String countyID) {
		this.countyID = countyID;
	}

	public void setprimaryPhoneToCall(String primaryPhoneToCall) {
		this.primaryPhoneToCall = primaryPhoneToCall;
	}

	public void setbestTimeToContact(String bestTimeToContact) {
		this.bestTimeToContact = bestTimeToContact;
	}

	public void setphone(String phone) {
		this.phone = phone;
	}

	public void setphoneExt(String phoneExt) {
		this.phoneExt = phoneExt;
	}

	public void sethomePhone(String homePhone) {
		this.homePhone = homePhone;
	}

	public void sethomePhoneExt(String homePhoneExt) {
		this.homePhoneExt = homePhoneExt;
	}

	public void setfax(String fax) {
		this.fax = fax;
	}

	public void setmobile(String mobile) {
		this.mobile = mobile;
	}

	public void setemailID(String emailID) {
		this.emailID = emailID;
	}

	public void setcompanyName(String companyName) {
		this.companyName = companyName;
	}

	public void setcomments(String comments) {
		this.comments = comments;
	}

	public void setsendAutomaticMail(String sendAutomaticMail) {
		this.sendAutomaticMail = sendAutomaticMail;
	}

	public void setassignUser(String assignUser) {
		this.assignUser = assignUser;
	}

	public void setleadOwnerID(String leadOwnerID) {
		this.leadOwnerID = leadOwnerID;
	}

	public void setautomatic(String automatic) {
		this.automatic = automatic;
	}

	public void setleadRatingID(String leadRatingID) {
		this.leadRatingID = leadRatingID;
	}

	public void setmarketingCodeId(String marketingCodeId) {
		this.marketingCodeId = marketingCodeId;
	}

	public void setleadSource2ID(String leadSource2ID) {
		this.leadSource2ID = leadSource2ID;
	}

	public void setleadSource3ID(String leadSource3ID) {
		this.leadSource3ID = leadSource3ID;
	}

	public void setotherLeadSourceDetail(String otherLeadSourceDetail) {
		this.otherLeadSourceDetail = otherLeadSourceDetail;
	}

	public void setbrokerID(String brokerID) {
		this.brokerID = brokerID;
	}

	public void setliquidCapitalMin(String liquidCapitalMin) {
		this.liquidCapitalMin = liquidCapitalMin;
	}

	public void setliquidCapitalMax(String liquidCapitalMax) {
		this.liquidCapitalMax = liquidCapitalMax;
	}

	public void setinvestTimeframe(String investTimeframe) {
		this.investTimeframe = investTimeframe;
	}

	public void setbackground(String background) {
		this.background = background;
	}

	public void setsourceOfFunding(String sourceOfFunding) {
		this.sourceOfFunding = sourceOfFunding;
	}

	public void setnextCallDate(String nextCallDate) {
		this.nextCallDate = nextCallDate;
	}

	public void setnoOfUnitReq(String noOfUnitReq) {
		this.noOfUnitReq = noOfUnitReq;
	}

	public void setstatusChangeDate(String statusChangeDate) {
		this.statusChangeDate = statusChangeDate;
	}

	public void setnoOfFieldSold(String noOfFieldSold) {
		this.noOfFieldSold = noOfFieldSold;
	}

	public void setdateOfOpen(String dateOfOpen) {
		this.dateOfOpen = dateOfOpen;
	}

	public void setforecastClosureDate(String forecastClosureDate) {
		this.forecastClosureDate = forecastClosureDate;
	}

	public void setprobability(String probability) {
		this.probability = probability;
	}

	public void setforecastRating(String forecastRating) {
		this.forecastRating = forecastRating;
	}

	public void setforecastRevenue(String forecastRevenue) {
		this.forecastRevenue = forecastRevenue;
	}

	public void setassignAutomaticCampaign(String assignAutomaticCampaign) {
		this.assignAutomaticCampaign = assignAutomaticCampaign;
	}

	public void setcampaignID(String campaignID) {
		this.campaignID = campaignID;
	}

	public void setbrandMapping_0brandID(String brandMapping_0brandID) {
		this.brandMapping_0brandID = brandMapping_0brandID;
	}

}
