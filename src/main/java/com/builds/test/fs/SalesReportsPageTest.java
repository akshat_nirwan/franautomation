package com.builds.test.fs;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.test.support.SupportReportsPageTest;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

class SalesReportsPageTest extends FranconnectUtil {

	FranconnectUtil fc = new FranconnectUtil();
	SupportReportsPageTest rpReport = new SupportReportsPageTest();

	@Test(groups = { "salesReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click Over Leads per Month Report", testCaseId = "TC_Sales_Report_01")
	private void salesReports01() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("sales",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Sales > Reports Page > Leads per Month Report");
			Sales fsmod = new Sales();
			fsmod.reports(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'Leads per Month Report')]"));
			rpReport.setSelectAllMultiDropDown(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click over Leads per Sales Territory Report", testCaseId = "TC_Sales_Report_02")
	private void salesReports02() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("sales",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Sales > Reports Page > Leads per Sales Territory Report");
			Sales fsmod = new Sales();
			fsmod.reports(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'Leads per Sales Territory Report')]"));
			rpReport.setSelectAllMultiDropDown(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click over Lead Status Report", testCaseId = "TC_Sales_Report_03")
	private void salesReports03() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("sales",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Sales > Reports Page > Lead Status Report");
			Sales fsmod = new Sales();
			fsmod.reports(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'Lead Status Report')]"));
			rpReport.setSelectAllMultiDropDown(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click over Lead Time Lines Report", testCaseId = "TC_Sales_Report_04")
	private void salesReports04() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("sales",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Sales > Reports Page > Lead Time Lines Report");
			Sales fsmod = new Sales();
			fsmod.reports(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'Lead Time Lines Report')]"));
			rpReport.setSelectAllMultiDropDown(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click over Lead Status Pipeline Report", testCaseId = "TC_Sales_Report_05")
	private void salesReports05() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("sales",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Sales > Reports Page > Lead Status Pipeline Report");
			Sales fsmod = new Sales();
			fsmod.reports(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'Lead Status Pipeline Report')]"));
			rpReport.setSelectAllMultiDropDown(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click over Lead Status Change Report", testCaseId = "TC_Sales_Report_06")
	private void salesReports06() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("sales",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Sales > Reports Page > Lead Status Change Report");
			Sales fsmod = new Sales();
			fsmod.reports(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'Lead Status Change Report')]"));
			rpReport.setSelectAllMultiDropDown(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click over Leads by Owner and Status Report", testCaseId = "TC_Sales_Report_07")
	private void salesReports07() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("sales",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Sales > Reports Page > Leads by Owner and Status Report");
			Sales fsmod = new Sales();
			fsmod.reports(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'Leads by Owner and Status Report')]"));
			rpReport.setSelectAllMultiDropDown(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click over Sales Forecasting Report", testCaseId = "TC_Sales_Report_08")
	private void salesReports08() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("sales",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Sales > Reports Page > Sales Forecasting Report");
			Sales fsmod = new Sales();
			fsmod.reports(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'Sales Forecasting Report')]"));
			rpReport.setSelectAllMultiDropDown(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click over Sales Owner Performance Analysis Report", testCaseId = "TC_Sales_Report_09")
	private void salesReports09() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("sales",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Sales > Reports Page > Sales Owner Performance Analysis Report");
			Sales fsmod = new Sales();
			fsmod.reports(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'Sales Owner Performance Analysis Report')]"));
			rpReport.setSelectAllMultiDropDown(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click over Master Tracking By Lead Owner", testCaseId = "TC_Sales_Report_10")
	private void salesReports10() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("sales",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Sales > Reports Page > Master Tracking By Lead Owner");
			Sales fsmod = new Sales();
			fsmod.reports(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'Master Tracking By Lead Owner')]"));
			rpReport.setSelectAllMultiDropDown(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click Over Closings per Month Report", testCaseId = "TC_Sales_Report_11")
	private void salesReports11() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("sales",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Sales > Reports Page > Closings per Month Report");
			Sales fsmod = new Sales();
			fsmod.reports(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'Closings per Month Report')]"));
			rpReport.setSelectAllMultiDropDown(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click over Closings by Owner Report", testCaseId = "TC_Sales_Report_12")
	private void salesReports12() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("sales",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Sales > Reports Page > Closings by Owner Report");
			Sales fsmod = new Sales();
			fsmod.reports(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'Closings by Owner Report')]"));
			rpReport.setSelectAllMultiDropDown(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click over Closings by Source Report", testCaseId = "TC_Sales_Report_13")
	private void salesReports13() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("sales",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Sales > Reports Page > Closings by Source Report");
			Sales fsmod = new Sales();
			fsmod.reports(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'Closings by Source Report')]"));
			rpReport.setSelectAllMultiDropDown(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click over Lead Conversion by Brokers Report", testCaseId = "TC_Sales_Report_14")
	private void salesReports14() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("sales",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Sales > Reports Page > Lead Conversion by Brokers Report");
			Sales fsmod = new Sales();
			fsmod.reports(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'Lead Conversion by Brokers Report')]"));
			rpReport.setSelectAllMultiDropDown(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click over Marketing Costs ROI Analysis Report", testCaseId = "TC_Sales_Report_15")
	private void salesReports15() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("sales",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Sales > Reports Page > Marketing Costs ROI Analysis Report");
			Sales fsmod = new Sales();
			fsmod.reports(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'Marketing Costs ROI Analysis Report')]"));
			rpReport.setSelectAllMultiDropDown(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click over # of Locations sold per Lead Report", testCaseId = "TC_Sales_Report_16")
	private void salesReports16() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("sales",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Sales > Reports Page > # of Locations sold per Lead Report");
			Sales fsmod = new Sales();
			fsmod.reports(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'of Locations sold per Lead Report')]"));
			rpReport.setSelectAllMultiDropDown(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click over Lead Source Report", testCaseId = "TC_Sales_Report_17")
	private void salesReports17() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("sales",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Sales > Reports Page > Lead Source Report");
			Sales fsmod = new Sales();
			fsmod.reports(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'Lead Source Report')]"));
			rpReport.setSelectAllMultiDropDown(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click over Master Tracking Report", testCaseId = "TC_Sales_Report_18")
	private void salesReports18() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("sales",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Sales > Reports Page > Master Tracking Report");
			Sales fsmod = new Sales();
			fsmod.reports(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'Master Tracking Report')]"));
			rpReport.setSelectAllMultiDropDown(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click over ", testCaseId = "TC_Sales_Report_19")
	private void salesReports19() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("sales",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep(
					"Navigate To Sales > Reports Page > Master Tracking - Performance comparison Report");
			Sales fsmod = new Sales();
			fsmod.reports(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'Master Tracking - Performance comparison Report')]"));
			rpReport.setSelectAllMultiDropDown(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click over Lead Heat Index Report", testCaseId = "TC_Sales_Report_20")
	private void salesReports20() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("sales",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Sales > Reports Page > Lead Heat Index Report");
			Sales fsmod = new Sales();
			fsmod.reports(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'Lead Heat Index Report')]"));
			rpReport.setSelectAllMultiDropDown(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click over Discovery Day Visit Report", testCaseId = "TC_Sales_Report_21")
	private void salesReports21() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("sales",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Sales > Reports Page > Discovery Day Visit Report");
			Sales fsmod = new Sales();
			fsmod.reports(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'Discovery Day Visit Report')]"));
			rpReport.setSelectAllMultiDropDown(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click over Email Status Report", testCaseId = "TC_Sales_Report_22")
	private void salesReports22() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("sales",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Sales > Reports Page > Email Status Report");
			Sales fsmod = new Sales();
			fsmod.reports(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'Email Status Report')]"));
			rpReport.setSelectAllMultiDropDown(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click over Track Inactive(without any activity) Leads Report", testCaseId = "TC_Sales_Report_23")
	private void salesReports23() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("sales",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep(
					"Navigate To Sales > Reports Page > Track Inactive(without any activity) Leads Report");
			Sales fsmod = new Sales();
			fsmod.reports(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,(".//a[contains(text () ,'Track Inactive(without any activity) Leads Report')]")));
			rpReport.setSelectAllMultiDropDown(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click over Call Activity Report", testCaseId = "TC_Sales_Report_24")
	private void salesReports24() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("sales",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Sales > Reports Page > Call Activity Report");
			Sales fsmod = new Sales();
			fsmod.reports(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'Call Activity Report')]"));
			rpReport.setSelectAllMultiDropDown(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click over Lead Contacted Details Report", testCaseId = "TC_Sales_Report_25")
	private void salesReports25() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("sales",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Sales > Reports Page > Lead Contacted Details Report");
			Sales fsmod = new Sales();
			fsmod.reports(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'Lead Contacted Details Report')]"));
			rpReport.setSelectAllMultiDropDown(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click over Website Activity Report", testCaseId = "TC_Sales_Report_26")
	private void salesReports26() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("sales",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Sales > Reports Page > Website Activity Report");
			Sales fsmod = new Sales();
			fsmod.reports(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'Website Activity Report')]"));
			rpReport.setSelectAllMultiDropDown(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click over Email Subscription Report", testCaseId = "TC_Sales_Report_27")
	private void salesReports27() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("sales",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Sales > Reports Page > Email Subscription Report");
			Sales fsmod = new Sales();
			fsmod.reports(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'Email Subscription Report')]"));
			rpReport.setSelectAllMultiDropDown(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click over Virtual Brochure Visited by Section Report", testCaseId = "TC_Sales_Report_28")
	private void salesReports28() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("sales",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Sales > Reports Page > Virtual Brochure Visited by Section Report");
			Sales fsmod = new Sales();
			fsmod.reports(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'Virtual Brochure Visited by Section Report')]"));
			rpReport.setSelectAllMultiDropDown(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click over Visitor to Lead Count Tracking Report", testCaseId = "TC_Sales_Report_29")
	private void salesReports29() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("sales",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Sales > Reports Page > Visitor to Lead Count Tracking Report");
			Sales fsmod = new Sales();
			fsmod.reports(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'Visitor to Lead Count Tracking Report')]"));
			rpReport.setSelectAllMultiDropDown(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click over Candidate Portal Report", testCaseId = "TC_Sales_Report_30")
	private void salesReports30() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("sales",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Sales > Reports Page > Candidate Portal Report");
			Sales fsmod = new Sales();
			fsmod.reports(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'Candidate Portal Report')]"));
			rpReport.setSelectAllMultiDropDown(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click over Virtual Brochure Pipeline Report", testCaseId = "TC_Sales_Report_31")
	private void salesReports31() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("sales",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Sales > Reports Page > Virtual Brochure Pipeline Report");
			Sales fsmod = new Sales();
			fsmod.reports(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'Virtual Brochure Pipeline Report')]"));
			rpReport.setSelectAllMultiDropDown(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click over Virtual Brochure Activity Report", testCaseId = "TC_Sales_Report_32")
	private void salesReports32() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("sales",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Sales > Reports Page > Virtual Brochure Activity Report");
			Sales fsmod = new Sales();
			fsmod.reports(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'Virtual Brochure Activity Report')]"));
			rpReport.setSelectAllMultiDropDown(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click over Section and Page View Report", testCaseId = "TC_Sales_Report_33")
	private void salesReports33() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("sales",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Sales > Reports Page > Section and Page View Report");
			Sales fsmod = new Sales();
			fsmod.reports(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'Section and Page View Report')]"));
			rpReport.setSelectAllMultiDropDown(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click over Question and Answers Report", testCaseId = "TC_Sales_Report_34")
	private void salesReports34() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("sales",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Sales > Reports Page > Question and Answers Report");
			Sales fsmod = new Sales();
			fsmod.reports(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'Question and Answers Report')]"));
			rpReport.setSelectAllMultiDropDown(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click over Visitor Summary Report", testCaseId = "TC_Sales_Report_35")
	private void salesReports35() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("sales",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Sales > Reports Page > Visitor Summary Report");
			Sales fsmod = new Sales();
			fsmod.reports(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'Visitor Summary Report')]"));
			rpReport.setSelectAllMultiDropDown(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click over Lead Summary Report", testCaseId = "TC_Sales_Report_36")
	private void salesReports36() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("sales",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Sales > Reports Page > Lead Summary Report");
			Sales fsmod = new Sales();
			fsmod.reports(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'Lead Summary Report')]"));
			rpReport.setSelectAllMultiDropDown(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click over Candidate Portal Activity Report", testCaseId = "TC_Sales_Report_37")
	private void salesReports37() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("sales",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Sales > Reports Page > Candidate Portal Activity Report");
			Sales fsmod = new Sales();
			fsmod.reports(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'Candidate Portal Activity Report')]"));
			rpReport.setSelectAllMultiDropDown(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click over Application Activity Report", testCaseId = "TC_Sales_Report_38")
	private void salesReports38() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("sales",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Sales > Reports Page > Application Activity Report");
			Sales fsmod = new Sales();
			fsmod.reports(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'Application Activity Report')]"));
			rpReport.setSelectAllMultiDropDown(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click over Killed Lead Count by Source Report", testCaseId = "TC_Sales_Report_39")
	private void salesReports39() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("sales",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Sales > Reports Page > Killed Lead Count by Source Report");
			Sales fsmod = new Sales();
			fsmod.reports(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'Killed Lead Count by Source Report')]"));
			rpReport.setSelectAllMultiDropDown(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click Over Killed Lead Analysis Report", testCaseId = "TC_Sales_Report_40")
	private void salesReports40() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("sales",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Sales > Reports Page > Killed Lead Analysis Report");
			Sales fsmod = new Sales();
			fsmod.reports(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'Killed Lead Analysis Report')]"));
			rpReport.setSelectAllMultiDropDown(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click over Lead Unsubscribed Report", testCaseId = "TC_Sales_Report_41")
	private void salesReports41() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("sales",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Sales > Reports Page > Lead Unsubscribed Report");
			Sales fsmod = new Sales();
			fsmod.reports(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'Lead Unsubscribed Report')]"));
			rpReport.setSelectAllMultiDropDown(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click over Campaign Unsubscribed Report", testCaseId = "TC_Sales_Report_42")
	private void salesReports42() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("sales",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Sales > Reports Page > Campaign Unsubscribed Report");
			Sales fsmod = new Sales();
			fsmod.reports(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'Campaign Unsubscribed Report')]"));
			rpReport.setSelectAllMultiDropDown(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click over Number Of Emails Sent Report", testCaseId = "TC_Sales_Report_43")
	private void salesReports43() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("sales",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Sales > Reports Page > Number Of Emails Sent Report");
			Sales fsmod = new Sales();
			fsmod.reports(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'Number Of Emails Sent Report')]"));
			rpReport.setSelectAllMultiDropDown(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click over Click Through Rate -Number Of Clickthroughs / Number Of Emails Opened Report", testCaseId = "TC_Sales_Report_44")
	private void salesReports44() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("sales",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep(
					"Navigate To Sales > Reports Page > Click Through Rate -Number Of Clickthroughs / Number Of Emails Opened Report");
			Sales fsmod = new Sales();
			fsmod.reports(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'Click Through Rate -Number Of Clickthroughs / Number Of Emails Opened Report')]"));
			rpReport.setSelectAllMultiDropDown(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click over ", testCaseId = "TC_Sales_Report_45")
	private void salesReports45() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("sales",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Sales > Reports Page > Number Of Current Subscribers Report");
			Sales fsmod = new Sales();
			fsmod.reports(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'Number Of Current Subscribers Report')]"));
			rpReport.setSelectAllMultiDropDown(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click over ", testCaseId = "TC_Sales_Report_46")
	private void salesReports46() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("sales",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Sales > Reports Page > FDD Tracking Report");
			Sales fsmod = new Sales();
			fsmod.reports(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'FDD Tracking Report')]"));
			rpReport.setSelectAllMultiDropDown(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click over Prospect Tracking Report", testCaseId = "TC_Sales_Report_47")
	private void salesReports47() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("sales",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Sales > Reports Page > Prospect Tracking Report");
			Sales fsmod = new Sales();
			fsmod.reports(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'Prospect Tracking Report')]"));
			rpReport.setSelectAllMultiDropDown(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click over New Report", testCaseId = "TC_Sales_Report_48")
	private void salesReports48() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("sales",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Sales > Reports Page > New Report");
			Sales fsmod = new Sales();
			fsmod.reports(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[contains(text () , 'New Report')]"));
			rpReport.setSelectAllMultiDropDown(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
}
