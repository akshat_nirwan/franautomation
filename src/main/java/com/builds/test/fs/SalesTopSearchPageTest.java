package com.builds.test.fs;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminConfigurationConfigureCallStatusPageTest;
import com.builds.test.admin.AdminConfigurationConfigureCommunicationTypePageTest;
import com.builds.uimaps.crm.CRMContactsPage;
import com.builds.uimaps.crm.CRMLeadsPage;
import com.builds.uimaps.fs.LeadSummaryUI;
import com.builds.uimaps.fs.SearchUI;
import com.builds.uimaps.thehub.AdminTheHubLibraryPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

class SalesTopSearchPageTest {

	/*
	 * 1. Add Lead And Verify It After search through Top Search 2. Parallel
	 * This process with 100 threads
	 * 
	 */

	public long ajaxTimeAvg = 0;

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "salesTopSearch" }, invocationCount = 4, threadPoolSize = 4)
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-21", testCaseId = "TC_Lead_Search", testCaseDescription = "Verify The Added Lead Search By Top Search")
	private void addLeadVerify() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		// Map<String,String> dataSet = fc.utobj().readTestData("sales",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			SearchUI search_page = new SearchUI(driver);
			fc.utobj().printTestStep("Navigate To Sales > Lead Summary");
			AddLeadFromAllSources lead_summary = new AddLeadFromAllSources();

			String emailId = "salesautomation@staffex.com";

			String firstName = fc.utobj().generateTestData("Fname");
			String lastName = fc.utobj().generateTestData("Lname");
			String country = "USA";
			String state = "Alabama";
			String leadOwner = "FranConnect Administrator";
			String leadSourceCategory = "Advertisement";
			String leadSourceDetails = "Magazine";

			fc.utobj().printTestStep("Add a Lead for this user");
			Map<String, String> leadInfo = new HashMap<String, String>();

			leadInfo.put("firstName", firstName);
			leadInfo.put("lastName", lastName);
			leadInfo.put("country", country);
			leadInfo.put("state", state);
			leadInfo.put("email", emailId);
			leadInfo.put("leadOwner", leadOwner);
			leadInfo.put("leadSourceCategory", leadSourceCategory);
			leadInfo.put("leadSourceDetails", leadSourceDetails);
			lead_summary.addLeadFromSystem(driver, leadInfo);

			fc.utobj().printTestStep("Serch Lead");
			String leadName = leadInfo.get("firstName") + " " + leadInfo.get("lastName");

			try {
				fc.utobj().sendKeys(driver, search_page.topSearchField, leadName);
			} catch (Exception e) {
				fc.utobj().throwsException("Not able to find Top Search");
			}

			fc.utobj().clickEnterOnElement(driver, search_page.topSearchField);

			try {
				fc.utobj().clickElement(driver, search_page.moduleClick);
			} catch (Exception e) {
				fc.utobj().throwsException("Not able to find Sales Module In Search");
			}

			try {
				fc.utobj().clickElement(driver,
						fc.utobj().getElementByXpath(driver, ".//custom[contains(text () , '" + leadName + "')]"));
			} catch (Exception e) {
				fc.utobj().throwsException("Not able to find Lead At Top Search Result");
			}

			try {
				fc.utobj().clickElement(driver, search_page.viewDetails);
			} catch (Exception e) {
				fc.utobj().throwsException("Not able to find View Details Link");
			}

			boolean isLnamePresent = fc.utobj().assertPageSource(driver, lastName);

			if (isLnamePresent == false) {
				fc.utobj().throwsException("Not able to search Lead By Top Search");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/*
	 * 2. Lead Search By Top Search And Verify
	 */

	@Test(groups = { "salesTopSearch" }, invocationCount = 5000, threadPoolSize = 100)
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-21", testCaseId = "TC_Lead_Search_01", testCaseDescription = "Verify The Added Lead Search By Top Search")
	private void countTimeAvgForAjax() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		// Map<String,String> dataSet = fc.utobj().readTestData("sales",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			SearchUI search_page = new SearchUI(driver);
			fc.utobj().printTestStep("Navigate To Sales > Lead Summary");
			AddLeadFromAllSources lead_summary = new AddLeadFromAllSources();

			String emailId = "salesautomation@staffex.com";

			String firstName = fc.utobj().generateTestData("Fname");
			String lastName = fc.utobj().generateTestData("Lname");
			String country = "USA";
			String state = "Alabama";
			String leadOwner = "FranConnect Administrator";
			String leadSourceCategory = "Advertisement";
			String leadSourceDetails = "Magazine";

			fc.utobj().printTestStep("Add a Lead for this user");
			Map<String, String> leadInfo = new HashMap<String, String>();

			leadInfo.put("firstName", firstName);
			leadInfo.put("lastName", lastName);
			leadInfo.put("country", country);
			leadInfo.put("state", state);
			leadInfo.put("email", emailId);
			leadInfo.put("leadOwner", leadOwner);
			leadInfo.put("leadSourceCategory", leadSourceCategory);
			leadInfo.put("leadSourceDetails", leadSourceDetails);
			leadInfo.put("city", "Test City");
			leadInfo.put("phone", "1245789632");
			lead_summary.addLeadFromSystem(driver, leadInfo);

			String leadName = leadInfo.get("firstName") + " " + leadInfo.get("lastName");

			fc.utobj().printTestStep("Verifying Lead at primary Info");
			boolean isLeadNotPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text(),'" + firstName + "')]");
			if (isLeadNotPresent == false) {
				Reporter.log("Lead is not visible at Primary Info Also");
			}
			fc.utobj().printTestStep("Serch Lead");

			//
			Reporter.log(">>>>LEAD NAME<<<<< : " + leadName);
			long startTime = 0;
			long endTime = 0;
			long gapTime = 0;
			boolean isTextPresent = false;

			startTime = System.currentTimeMillis();
			Reporter.log("Start Time : " + startTime);

			boolean isSearchTrue = false;
			for (int i = 0; i < 3; i++) {
				if (isSearchTrue == false) {
					fc.utobj().sendKeys(driver, search_page.topSearchField, leadName);

					if (i == 2) {

					}
					fc.utobj().clickEnterOnElement(driver, search_page.topSearchField);

					try {
						// fc.utobj().getElement(driver,
						// search_page.moduleClick).click();
						fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
								".//custom[contains(text () , '" + leadName + "')]"));
						isTextPresent = true;
						isSearchTrue = true;
					} catch (Exception e) {
						Reporter.log(e.getMessage());
						System.out.println(e.getMessage());
					}
				}
			}

			if (isTextPresent) {
				endTime = System.currentTimeMillis();
				Reporter.log("End Time : " + endTime);
				gapTime = endTime - startTime;
				Reporter.log("Gap Time : " + gapTime);
			} else {
				fc.utobj().throwsException("Was not able to search Lead In Top Search");
			}

			ajaxTimeAvg = ajaxTimeAvg + gapTime;
			Reporter.log("Ajax Avg Time In Seconds: " + ajaxTimeAvg / 1000);

			// fc.utobj().logoutAndQuitBrowser(driver, config, testCaseId);
		} catch (Exception e) {
			// fc.utobj().quitBrowserOnCatch(driver, config, e, testCaseId);
		} finally {
			driver.quit();
		}
	}

	/*
	 * 1. Add Lead CRM And Search At Top Search Solar Search
	 */

	@Test(groups = { "crmTopSearch" }, invocationCount = 1, threadPoolSize = 1)
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-21", testCaseId = "TC_Lead_Search_CRM_01", testCaseDescription = "Verify The Added Lead Search By Top Search")
	private void addLeadCRM() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		// Map<String,String> dataSet =
		// fc.utobj().readTestData("sales",testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);
			SearchUI search_page = new SearchUI(driver);

			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");
			fc.crm().crm_common().CRMLeadsLnk(driver);

			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.leadLnk);

			String firstName = fc.utobj().generateTestData("Fname");
			fc.utobj().sendKeys(driver, pobj.leadFirstName, firstName);
			String lastName = fc.utobj().generateTestData("Lname");
			fc.utobj().sendKeys(driver, pobj.leadLastName, lastName);

			fc.utobj().selectDropDown(driver, pobj.primaryContactMethodSelect, "Email");
			if (!fc.utobj().getElement(driver, pobj.assignToCorporate).isSelected()) {
				fc.utobj().clickElement(driver, pobj.assignToCorporate);
			}
			fc.utobj().selectDropDown(driver, pobj.selectCorporateUser, "FranConnect Administrator");
			fc.utobj().selectDropDown(driver, pobj.country, "USA");
			fc.utobj().selectDropDown(driver, pobj.state, "Alabama");
			String email = "crmautomation@staffex.com";
			fc.utobj().sendKeys(driver, pobj.emailIds, email);
			fc.utobj().clickElement(driver, pobj.saveBtn);

			String leadName = firstName + " " + lastName;

			fc.utobj().printTestStep("Verifying Lead at primary Info");
			boolean isLeadNotPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text(),'" + firstName + "')]");
			if (isLeadNotPresent == false) {
				Reporter.log("Lead is not visible at Primary Info Also");
			}
			fc.utobj().printTestStep("Serch Lead");

			Reporter.log(">>>>LEAD NAME<<<<< : " + leadName);
			long startTime = 0;
			long endTime = 0;
			long gapTime = 0;
			boolean isTextPresent = false;

			startTime = System.currentTimeMillis();
			Reporter.log("Start Time : " + startTime);

			boolean isSearchTrue = false;
			for (int i = 0; i < 3; i++) {
				if (isSearchTrue == false) {
					fc.utobj().sendKeys(driver, search_page.topSearchField, leadName);

					if (i == 2) {

					}
					fc.utobj().clickEnterOnElement(driver, search_page.topSearchField);

					try {
						// fc.utobj().getElement(driver,
						// search_page.moduleClick).click();
						fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
								".//custom[contains(text () , '" + leadName + "')]"));
						isTextPresent = true;
						isSearchTrue = true;
					} catch (Exception e) {
						Reporter.log(e.getMessage());
						System.out.println(e.getMessage());
					}
				}
			}

			if (isTextPresent) {
				endTime = System.currentTimeMillis();
				Reporter.log("End Time : " + endTime);
				gapTime = endTime - startTime;
				Reporter.log("Gap Time : " + gapTime);
			} else {
				fc.utobj().throwsException("Was not able to search Lead In Top Search");
			}

			ajaxTimeAvg = ajaxTimeAvg + gapTime;
			Reporter.log("Ajax Avg Time In Seconds: " + ajaxTimeAvg / 1000);

		} catch (Exception e) {
			fc.utobj().captureScreenShot(driver, testCaseId);
		} finally {
			driver.quit();
		}
	}

	/*
	 * 2. Add Contact CRM And Search At Top Search Solar Search
	 */

	@Test(groups = { "crmTopSearch" }, invocationCount = 1, threadPoolSize = 1)
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-21", testCaseId = "TC_Contact_Search_CRM_02", testCaseDescription = "Verify The Added Contact Search By Top Search")
	private void addContactCRM() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		// Map<String,String> dataSet =
		// fc.utobj().readTestData("sales",testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);
			SearchUI search_page = new SearchUI(driver);

			fc.utobj().printTestStep("Naviagte To CRM > Contacts > Contacts Summary");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().printTestStep("Add Contacs");
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			fc.utobj().selectDropDown(driver, pobj.contactType, "Contacts");

			String firstName = fc.utobj().generateTestData("Fname");
			String lastName = fc.utobj().generateTestData("Lname");

			fc.utobj().sendKeys(driver, pobj.contactFirstName, firstName);
			fc.utobj().sendKeys(driver, pobj.contactLastName, lastName);
			fc.utobj().selectDropDown(driver, pobj.country, "USA");
			fc.utobj().selectDropDown(driver, pobj.state, "Alabama");
			fc.utobj().selectDropDown(driver, pobj.primaryContactMethodSelect, "Email");

			fc.utobj().sendKeys(driver, pobj.emailIds, "crmautomation@staffex.com");

			if (!fc.utobj().getElement(driver, pobj.assignToCorporate).isSelected()) {
				fc.utobj().clickElement(driver, pobj.assignToCorporate);
			}

			fc.utobj().selectDropDown(driver, pobj.selectCorporateUserContact, "FranConnect Administrator");
			fc.utobj().clickElement(driver, pobj.saveBtn);

			String contactName = firstName + " " + lastName;

			fc.utobj().printTestStep("Verifying Lead at primary Info");
			boolean isLeadNotPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text(),'" + firstName + "')]");
			if (isLeadNotPresent == false) {
				Reporter.log("Contact is not visible at Primary Info Also");
			}
			fc.utobj().printTestStep("Serch Lead");

			Reporter.log(">>>>Contact NAME<<<<< : " + contactName);
			long startTime = 0;
			long endTime = 0;
			long gapTime = 0;
			boolean isTextPresent = false;

			startTime = System.currentTimeMillis();
			Reporter.log("Start Time : " + startTime);

			boolean isSearchTrue = false;
			for (int i = 0; i < 3; i++) {
				if (isSearchTrue == false) {
					fc.utobj().sendKeys(driver, search_page.topSearchField, contactName);

					if (i == 2) {

					}
					fc.utobj().clickEnterOnElement(driver, search_page.topSearchField);

					try {
						// fc.utobj().getElement(driver,search_page.moduleClick).click();
						fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
								".//custom[contains(text () , '" + contactName + "')]"));
						isTextPresent = true;
						isSearchTrue = true;
					} catch (Exception e) {
						Reporter.log(e.getMessage());
						System.out.println(e.getMessage());
					}
				}
			}

			if (isTextPresent) {
				endTime = System.currentTimeMillis();
				Reporter.log("End Time : " + endTime);
				gapTime = endTime - startTime;
				Reporter.log("Gap Time : " + gapTime);
			} else {
				fc.utobj().throwsException("Was not able to search Contact In Top Search");
			}

			ajaxTimeAvg = ajaxTimeAvg + gapTime;
			Reporter.log("Ajax Avg Time In Seconds: " + ajaxTimeAvg / 1000);

		} catch (Exception e) {
			fc.utobj().captureScreenShot(driver, testCaseId);
		} finally {
			driver.quit();
		}
	}

	/*
	 * Search The Hub Document
	 */

	@Test(groups = { "thehubTopSearch" }, invocationCount = 1, threadPoolSize = 1)
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-21", testCaseId = "TC_Document_Search_Thehub_01", testCaseDescription = "Verify The Added Document Search By Top Search")
	private void searchTheHubDocument() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		// Map<String,String> dataSet =
		// fc.utobj().readTestData("sales",testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminTheHubLibraryPage pobj = new AdminTheHubLibraryPage(driver);
			SearchUI search_page = new SearchUI(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder With Document");
			fc.hub().hub_common().adminTheHubLibraryPage(driver);
			fc.utobj().clickElement(driver, pobj.addFolder);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String folderName = fc.utobj().generateTestData("Foldername");
			fc.utobj().sendKeys(driver, pobj.folderName, folderName);

			if (!pobj.summaryForamte.get(0).isSelected()) {
				fc.utobj().clickElement(driver, pobj.summaryForamte.get(0));
			}
			String folderSummary = fc.utobj().generateTestData("foldersummary");
			fc.utobj().sendKeys(driver, pobj.folderSummary, folderSummary);
			if (!pobj.roleBase.get(0).isSelected()) {
				fc.utobj().clickElement(driver, pobj.roleBase.get(0));
			}
			if (!pobj.addDocumentFolder.get(0).isSelected()) {
				fc.utobj().clickElement(driver, pobj.addDocumentFolder.get(0));
			}

			String documentTitle = fc.utobj().generateTestData("Doctitle");
			fc.utobj().sendKeys(driver, pobj.documentTitle1, documentTitle);
			String briefSummary = fc.utobj().generateTestData("summary");
			fc.utobj().sendKeys(driver, pobj.briefSummary1, briefSummary);
			String currentDate = fc.utobj().getCurrentDateUSFormat();
			fc.utobj().sendKeys(driver, pobj.startDisplayDate1, currentDate);

			if (!pobj.documentType1.get(0).isSelected()) {
				fc.utobj().clickElement(driver, pobj.documentType1.get(0));
			}
			String file = fc.utobj().getFilePathFromTestData("taskFile.pdf");
			fc.utobj().moveToElement(driver, pobj.selectFile1);
			pobj.selectFile1.sendKeys(file);
			if (pobj.thumbnailImage1.get(1).isSelected()) {
				fc.utobj().clickElement(driver, pobj.thumbnailImage1.get(1));
			}
			fc.utobj().clickElement(driver, pobj.addBtn1);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			;
			fc.utobj().switchFrameToDefault(driver);

			Reporter.log(">>>>Document Title<<<<< : " + documentTitle);
			long startTime = 0;
			long endTime = 0;
			long gapTime = 0;
			boolean isTextPresent = false;

			startTime = System.currentTimeMillis();
			Reporter.log("Start Time : " + startTime);

			boolean isSearchTrue = false;
			for (int i = 0; i < 3; i++) {
				if (isSearchTrue == false) {
					fc.utobj().sendKeys(driver, search_page.topSearchField, documentTitle);

					if (i == 2) {

					}
					fc.utobj().clickEnterOnElement(driver, search_page.topSearchField);

					try {
						// fc.utobj().getElement(driver,search_page.moduleClick).click();
						// fc.utobj().clickElement(driver,fc.utobj().getElementByXpath(driver,".//custom[contains(text
						// () , '" + documentTitle + "')]")));
						fc.utobj().getElementByXpath(driver, ".//custom[contains(text () , '" + documentTitle + "')]")
								.isDisplayed();
						isTextPresent = true;
						isSearchTrue = true;
					} catch (Exception e) {
						Reporter.log(e.getMessage());
						System.out.println(e.getMessage());
					}
				}
			}

			if (isTextPresent) {
				endTime = System.currentTimeMillis();
				Reporter.log("End Time : " + endTime);
				gapTime = endTime - startTime;
				Reporter.log("Gap Time : " + gapTime);
			} else {
				fc.utobj().throwsException("Was not able to search Document In Top Search");
			}

			ajaxTimeAvg = ajaxTimeAvg + gapTime;
			Reporter.log("Ajax Avg Time In Seconds: " + ajaxTimeAvg / 1000);

		} catch (Exception e) {
			fc.utobj().captureScreenShot(driver, testCaseId);
		} finally {
			driver.quit();
		}
	}

	/*
	 * Create a Suite where a lead is added, modified, a task is added and
	 * modified, a call is added and modified.
	 * 
	 * 
	 */

	@Test(groups = { "AddModify" }, invocationCount = 200, threadPoolSize = 10)
	@TestCase(createdOn = "2017-09-28", updatedOn = "2017-09-28", testCaseDescription = "Add And Modify Lead , Task and Call", testCaseId = "TC_Verify_Lead_Task_Call")
	private void addLeadModify() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("thehub",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			LeadSummaryUI summary_page = new LeadSummaryUI(driver);
			SearchUI search_page = new SearchUI(driver);
			fc.utobj().printTestStep("Navigate To Sales > Lead Summary");
			AddLeadFromAllSources lead_summary = new AddLeadFromAllSources();

			String emailId = "salesautomation@staffex.com";
			String firstName = fc.utobj().generateTestData("Fname");
			String lastName = fc.utobj().generateTestData("Lname");
			String country = "USA";
			String state = "Alabama";
			String leadOwner = "FranConnect Administrator";
			String leadSourceCategory = "Advertisement";
			String leadSourceDetails = "Magazine";

			fc.utobj().printTestStep("Add a Lead for this user");
			Map<String, String> leadInfo = new HashMap<String, String>();

			leadInfo.put("firstName", firstName);
			leadInfo.put("lastName", lastName);
			leadInfo.put("country", country);
			leadInfo.put("state", state);
			leadInfo.put("email", emailId);
			leadInfo.put("leadOwner", leadOwner);
			leadInfo.put("leadSourceCategory", leadSourceCategory);
			leadInfo.put("leadSourceDetails", leadSourceDetails);
			leadInfo.put("city", "TestCity");
			leadInfo.put("phone", "1245789632");

			fc.utobj().printTestStep("Add Lead");
			lead_summary.addLeadFromSystem(driver, leadInfo);

			String leadName = firstName + " " + lastName;

			fc.utobj().printTestStep("Search Lead");
			FSSearchPageTest p3 = new FSSearchPageTest();
			p3.searchByLeadName(driver, firstName, lastName);

			fc.utobj().printTestStep("Modify Lead");
			fc.utobj().actionImgOption(driver, leadName, "Modify");

			String mFirstName = fc.utobj().generateTestData("Testname");

			fc.utobj().sendKeys(driver, summary_page.firstName, mFirstName);
			fc.utobj().clickElement(driver, summary_page.saveBtn);
			boolean isLeadFirstNamePresent = fc.utobj().assertPageSource(driver, mFirstName);

			if (isLeadFirstNamePresent == false) {
				fc.utobj().throwsException("Not able to Modify The Added Lead");
			}

			// Search Lead By Top Search

			String modifiedName = mFirstName + " " + lastName;

			boolean isSearchTrue = false;
			for (int i = 0; i < 3; i++) {
				if (isSearchTrue == false) {
					fc.utobj().sendKeys(driver, search_page.topSearchField, modifiedName);

					if (i == 2) {

					}
					fc.utobj().clickEnterOnElement(driver, search_page.topSearchField);

					try {
						isSearchTrue = fc.utobj().isElementPresent(
								driver.findElements(By.xpath(".//custom[contains(text () , '" + modifiedName + "')]")));
					} catch (Exception e) {
						Reporter.log(e.getMessage());
						System.out.println(e.getMessage());
					}
				}
			}

			if (isSearchTrue) {
				Reporter.log("Lead Search Working Properly");
			} else {
				Reporter.log("Search Not Working");
				fc.utobj().throwsException("Not able to search Lead By Top Search");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "AddModify" }, invocationCount = 200, threadPoolSize = 10)
	@TestCase(createdOn = "2017-09-28", updatedOn = "2017-09-28", testCaseId = "TC_Add_Modify_Call", testCaseDescription = "Verify Log a Call through Action Menu And Modify")
	private void logACallActionIcon() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("sales",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String callStatus = "Contacted";
		String communicationType = "Outbound Call";

		try {
			driver = fc.loginpage().login(driver);
			FSLeadSummaryPageTest lead_Summary = new FSLeadSummaryPageTest();
			AdminConfigurationConfigureCallStatusPageTest p1 = new AdminConfigurationConfigureCallStatusPageTest();
			fc.utobj().printTestStep("Add Call Status");
			callStatus = p1.addCallStatus(driver, callStatus);

			AdminConfigurationConfigureCommunicationTypePageTest p2 = new AdminConfigurationConfigureCommunicationTypePageTest();
			communicationType = p2.addCommunicationType(driver, communicationType);
			Sales fs = new Sales();
			fs.leadManagement(driver);
			lead_Summary.clickAddLeadLink(driver);
			FSLeadSummaryPrimaryInfoPageTest p3 = new FSLeadSummaryPrimaryInfoPageTest();
			fc.utobj().printTestStep("Add a lead");

			Map<String, String> leadInfo = p3.fillLeadBasicInfo(driver);
			String firstName = leadInfo.get("firstName");
			String lastName = leadInfo.get("lastName");
			FSSearchPageTest p4 = new FSSearchPageTest();
			p4.searchByLeadName(driver, firstName, lastName);
			LeadSummaryUI pobj = new LeadSummaryUI(driver);

			fc.utobj().printTestStep("Log a Call");
			fc.utobj().actionImgOption(driver, firstName + " " + lastName, "Log a Call");
			String callSubject = "Call Subject";
			callSubject = fc.utobj().generateTestData(callSubject);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.callSubject, callSubject);
			fc.utobj().selectDropDown(driver, pobj.callStatus, callStatus);
			fc.utobj().selectDropDown(driver, pobj.communicationType, communicationType);
			fc.utobj().selectDropDown(driver, pobj.callTimeHr, "10");
			fc.utobj().selectDropDown(driver, pobj.callTimeMin, "15 Min");
			try {
				fc.utobj().selectDropDown(driver, pobj.callTimeAPM, "AM");
			} catch (Exception e) {
				// TODO: handle exception
			}
			fc.utobj().clickElement(driver, pobj.addCallBtn);

			fc.utobj().clickElement(driver, pobj.callConfirmScheduleTaskNoBtn);
			fc.utobj().switchFrameToDefault(driver);

			p4.searchByLeadName(driver, firstName, lastName);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text(),'" + firstName + " " + lastName + "')]"));

			fc.utobj().switchFrameById(driver, "leadLogCallSummary");
			boolean isLeadCallSubjectPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text(),'" + callSubject + "')]");
			fc.utobj().switchFrameToDefault(driver);

			if (isLeadCallSubjectPresent == false) {
				fc.utobj().throwsException("Call Not Added");
			}

			fc.utobj().printTestStep("Modify The Added Call");
			fc.utobj().switchFrameById(driver, "leadLogCallSummary");
			fc.utobj().actionImgOption(driver, callSubject, "Modify");
			String MCallSubject = fc.utobj().generateTestData("ModifyCall");
			fc.utobj().switchFrameToDefault(driver);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.callSubject, MCallSubject);
			fc.utobj().clickElement(driver, pobj.addCallBtn);

			fc.utobj().clickElement(driver, pobj.callConfirmScheduleTaskNoBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().switchFrameById(driver, "leadLogCallSummary");
			boolean modifyCall = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + MCallSubject + "')]");
			fc.utobj().switchFrameToDefault(driver);

			if (modifyCall == false) {
				fc.utobj().throwsException("Call Not Modified");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "AddModify" }, invocationCount = 200, threadPoolSize = 10)
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-13", testCaseId = "TC_Added_Task_Modify", testCaseDescription = "Verify Log A task And Modify")
	private void verifySalesLogATask() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("sales",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FSLeadSummaryPageTest lead_Summary = new FSLeadSummaryPageTest();
			String firstName = fc.utobj().generateTestData("Fname");
			String lastName = fc.utobj().generateTestData("Lname");

			String userName = "FranConnect Administrator";
			String leadFullName = firstName + " " + lastName;
			Sales fsmod = new Sales();
			LeadSummaryUI pobj = new LeadSummaryUI(driver);
			fc.utobj().printTestStep("Add a lead");

			lead_Summary.addLeadSummaryWithLeadNameOwnerName(driver, firstName, lastName, userName);
			fc.utobj().printTestStep("Go to summary page");
			fsmod.leadManagement(driver);
			fc.utobj().printTestStep("Search the lead");
			FSSearchPageTest p3 = new FSSearchPageTest();
			p3.searchByLeadName(driver, firstName, lastName);
			fc.utobj().printTestStep("Right Action Icon > Click on Log A task");
			fc.utobj().actionImgOption(driver, leadFullName, "Log a Task");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String taskSubject = "Task Subject";
			taskSubject = fc.utobj().generateTestData(taskSubject);
			fc.utobj().printTestStep("Add Task Details");
			fc.utobj().sendKeys(driver, pobj.taskSubject, taskSubject);
			fc.utobj().clickElement(driver, pobj.addTaskBtn);

			fc.utobj().switchFrameToDefault(driver);

			p3.searchByLeadName(driver, firstName, lastName);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'" + leadFullName + "')]"));
			fc.utobj().switchFrameById(driver, "leadOpenActivitesSummary");

			boolean isTaskAdded = fc.utobj().assertPageSource(driver, taskSubject);
			if (isTaskAdded == false) {
				fc.utobj().throwsException("Task Not Added Successfully");
			}
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Modify Logged Task");
			fc.utobj().switchFrameById(driver, "leadOpenActivitesSummary");
			fc.utobj().actionImgOption(driver, taskSubject, "Modify");
			fc.utobj().switchFrameToDefault(driver);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String MTaskSubject = fc.utobj().generateTestData("Modify Task");
			fc.utobj().sendKeys(driver, pobj.taskSubject, MTaskSubject);
			fc.utobj().clickElement(driver, pobj.addTaskBtn);

			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().switchFrameById(driver, "leadOpenActivitesSummary");
			boolean isModified = fc.utobj().assertPageSource(driver, MTaskSubject);
			if (isModified == false) {
				fc.utobj().throwsException("Task Not Modifed Successfully");
			}
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
}
