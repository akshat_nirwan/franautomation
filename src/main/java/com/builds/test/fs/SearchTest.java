package com.builds.test.fs;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.fs.SearchUI;
import com.builds.utilities.FranconnectUtil;

class SearchTest {

	public void fill_And_Search_PrimaryInfoBasedSearch(WebDriver driver, Search search) throws Exception {
		fillDetailsInPrimaryInfoBasedSearch(driver, search);
		clickSearchPrimaryInfoBasedSearch(driver, search);
	}

	public void fill_And_Search_PrimaryInfoBasedSearch_ArchivedLeads(WebDriver driver, Search search) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		SearchUI ui = new SearchUI(driver);
		fc.utobj().printTestStep("Click on Archive lead Option");
		fc.utobj().clickElement(driver, ui.archivedleadRadio);

		fillDetailsInPrimaryInfoBasedSearch(driver, search);
		clickSearchPrimaryInfoBasedSearch(driver, search);
	}

	private void clickSearchPrimaryInfoBasedSearch(WebDriver driver, Lead ld) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		SearchUI ui = new SearchUI(driver);

		fc.utobj().printTestStep("Click on Search Button");
		fc.utobj().clickElement(driver, ui.search);
	}

	private void fillDetailsInPrimaryInfoBasedSearch(WebDriver driver, Search search) throws Exception {
		// inquiry date
		FranconnectUtil fc = new FranconnectUtil();
		SearchUI ui = new SearchUI(driver);

		if (search.getFirstName() != null) {
			fc.utobj().sendKeys(driver, ui.firstName, search.getFirstName());
		}

		if (search.getLastName() != null) {
			fc.utobj().sendKeys(driver, ui.lastName, search.getLastName());
		}
	}
}
