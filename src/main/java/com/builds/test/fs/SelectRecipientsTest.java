package com.builds.test.fs;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.fs.SelectRecipientsUI;
import com.builds.utilities.FranconnectUtil;

class SelectRecipientsTest {

	public void clickContinue(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		SelectRecipientsUI ui = new SelectRecipientsUI(driver);

		fc.utobj().printTestStep("Click Continue Button");
		fc.utobj().clickElement(driver, ui.continueButton);

	}

	public void clickAssociateLater(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		SelectRecipientsUI ui = new SelectRecipientsUI(driver);

		fc.utobj().printTestStep("Click Associate Later Button");
		fc.utobj().clickElement(driver, ui.associateLaterButton);

	}

	public String acceptAlertForNoRecipients(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		SelectRecipientsUI ui = new SelectRecipientsUI(driver);

		fc.utobj().printTestStep("Alert Accept");
		return (fc.utobj().acceptAlertBox(driver));

	}
}
