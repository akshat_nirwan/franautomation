package com.builds.test.fs;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.fs.SendEmailUI;
import com.builds.utilities.FranconnectUtil;

 public class SendEmailTest {
	FranconnectUtil fc = new FranconnectUtil();

	Email fillEmailDetails_AndClickSendEmail_Top_Button(WebDriver driver, Email email) throws Exception {
		filEmailDetails(driver, email);
		clickSend_Top_Button(driver, email);
		return email;
	}

	public Email fillEmailDetails_AndClickSendEmail_Bottom_Button(WebDriver driver, Email email) throws Exception {
		filEmailDetails(driver, email);
		clickSend_Bottom_Button(driver, email);
		return email;
	}

	Email filEmailDetails(WebDriver driver, Email email) throws Exception {
		SendEmailUI ui = new SendEmailUI(driver);
		fc.utobj().printTestStep("Fill Email Details");

		if (email.getSubject() != null) {
			fc.utobj().sendKeys(driver, ui.subject, email.getSubject());
		}

		if (email.getBody() != null) {
			fc.utobj().switchFrameById(driver, ui.frame_forbody);
			fc.utobj().sendKeys(driver, ui.body, email.getBody());
			fc.utobj().switchFrameToDefault(driver);
		}

		return email;
	}

	Email clickSend_Top_Button(WebDriver driver, Email email) throws Exception {
		SendEmailUI ui = new SendEmailUI(driver);
		fc.utobj().printTestStep("Click on Send Email Top Button");
		fc.utobj().clickElement(driver, ui.sendEmail_Top_Button);
		return email;
	}

	Email clickSend_Bottom_Button(WebDriver driver, Email email) throws Exception {
		SendEmailUI ui = new SendEmailUI(driver);
		fc.utobj().printTestStep("Click on Send Email Bottom Button");
		fc.utobj().clickElement(driver, ui.sendEmail_Bottom_Button);
		return email;
	}
}
