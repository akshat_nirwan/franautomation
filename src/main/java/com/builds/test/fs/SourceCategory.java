package com.builds.test.fs;

class SourceCategory {

	private String sourceCategory;
	private String IncludeinWebPage;

	public String getSourceCategory() {
		return sourceCategory;
	}

	public void setSourceCategory(String sourceCategory) {
		this.sourceCategory = sourceCategory;
	}

	public String getIncludeinWebPage() {
		return IncludeinWebPage;
	}

	public void setIncludeinWebPage(String includeinWebPage) {
		IncludeinWebPage = includeinWebPage;
	}

}
