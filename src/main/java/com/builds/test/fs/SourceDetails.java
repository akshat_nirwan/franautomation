package com.builds.test.fs;

class SourceDetails {

	private String sourceDetails;
	private String IncludeinWebPage;

	public String getSourceDetails() {
		return sourceDetails;
	}

	public void setSourceDetails(String sourceDetails) {
		this.sourceDetails = sourceDetails;
	}

	public String getIncludeinWebPage() {
		return IncludeinWebPage;
	}

	public void setIncludeinWebPage(String includeinWebPage) {
		IncludeinWebPage = includeinWebPage;
	}

}
