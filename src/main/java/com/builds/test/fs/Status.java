package com.builds.test.fs;

class Status {

	private String leadStatus;
	private String remarks;
	private String FranchiseAwardedDate;
	private String noOfLocation;
	private String DatetoOpen;

	public String getFranchiseAwardedDate() {
		return FranchiseAwardedDate;
	}

	public void setFranchiseAwardedDate(String franchiseAwardedDate) {
		FranchiseAwardedDate = franchiseAwardedDate;
	}

	public String getNoOfLocation() {
		return noOfLocation;
	}

	public void setNoOfLocation(String noOfLocation) {
		this.noOfLocation = noOfLocation;
	}

	public String getDatetoOpen() {
		return DatetoOpen;
	}

	public void setDatetoOpen(String datetoOpen) {
		DatetoOpen = datetoOpen;
	}

	public String getLeadStatus() {
		return leadStatus;
	}

	public void setLeadStatus(String leadStatus) {
		this.leadStatus = leadStatus;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
}
