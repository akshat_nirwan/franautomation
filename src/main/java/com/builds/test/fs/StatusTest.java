package com.builds.test.fs;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.fs.StatusUI;
import com.builds.utilities.FranconnectUtil;

class StatusTest {

	private void changeStatus_EnterRemarks(WebDriver driver, Status status) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		StatusUI ui = new StatusUI(driver);

		if (status.getLeadStatus() != null) {
			fc.utobj().printTestStep("Select Lead Status");
			fc.utobj().selectDropDown(driver, ui.leadStatusID_Select_ByID, status.getLeadStatus());
		}

		if (status.getFranchiseAwardedDate() != null) {
			fc.utobj().sendKeys(driver, ui.FranchiseAwardedDate, status.getFranchiseAwardedDate());
		}

		if (status.getNoOfLocation() != null) {
			fc.utobj().sendKeys(driver, ui.noOflocations, status.getNoOfLocation());
		}
		if (status.getDatetoOpen() != null) {
			fc.utobj().sendKeys(driver, ui.dateToOpen, status.getDatetoOpen());
		}

		if (status.getRemarks() != null) {
			fc.utobj().printTestStep("Enter Lead Status change Remarks");
			fc.utobj().sendKeys(driver, ui.remarks_TextArea_ByID, status.getRemarks());
		}
	}

	private void clickChange_Button_ClickClose(WebDriver driver, Lead ld, Status status) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		StatusUI ui = new StatusUI(driver);

		fc.utobj().printTestStep("Click Change");
		fc.utobj().clickElement(driver, ui.change_status_button);
		fc.commonMethods().Click_Close_Input_ByValue(driver);
		ld.setLeadStatus(status.getLeadStatus());
	}

	public void changeStatus_EnterRemarks_clickChange_Button_ClickClose(WebDriver driver, Lead ld, Status status)
			throws Exception {
		changeStatus_EnterRemarks(driver, status);
		clickChange_Button_ClickClose(driver, ld, status);
	}
}
