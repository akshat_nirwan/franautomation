package com.builds.test.fs;

public class Task {

	private String assignTo;
	private String Status;
	private String taskType;
	private String Subject;
	private String TimelessTask;
	private String Priority;
	private String startDate;
	private String AddToCalendar;
	private String startTimeHH;
	private String startTimeMM;
	private String startTimeAMPM;
	private String endTimeHH;
	private String endTimeMM;
	private String endTimeAMPM;
	private String taskDescription;
	private String setReminder;
	private String TaskEmailReminder;
	private String ReminderDate;
	private String ReminderTimeHH;
	private String ReminderTimeMM;
	private String ReminderTimeAMPM;

	public String getAssignTo() {
		return assignTo;
	}

	public void setAssignTo(String assignTo) {
		this.assignTo = assignTo;
	}

	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public String getTaskType() {
		return taskType;
	}

	public void setTaskType(String taskType) {
		this.taskType = taskType;
	}

	public String getSubject() {
		return Subject;
	}

	public void setSubject(String subject) {
		Subject = subject;
	}

	public String getTimelessTask() {
		return TimelessTask;
	}

	public void setTimelessTask(String timelessTask) {
		TimelessTask = timelessTask;
	}

	public String getPriority() {
		return Priority;
	}

	public void setPriority(String priority) {
		Priority = priority;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getAddToCalendar() {
		return AddToCalendar;
	}

	public void setAddToCalendar(String addToCalendar) {
		AddToCalendar = addToCalendar;
	}

	public String getStartTimeHH() {
		return startTimeHH;
	}

	public void setStartTimeHH(String startTimeHH) {
		this.startTimeHH = startTimeHH;
	}

	public String getStartTimeMM() {
		return startTimeMM;
	}

	public void setStartTimeMM(String startTimeMM) {
		this.startTimeMM = startTimeMM;
	}

	public String getStartTimeAMPM() {
		return startTimeAMPM;
	}

	public void setStartTimeAMPM(String startTimeAMPM) {
		this.startTimeAMPM = startTimeAMPM;
	}

	public String getEndTimeHH() {
		return endTimeHH;
	}

	public void setEndTimeHH(String endTimeHH) {
		this.endTimeHH = endTimeHH;
	}

	public String getEndTimeMM() {
		return endTimeMM;
	}

	public void setEndTimeMM(String endTimeMM) {
		this.endTimeMM = endTimeMM;
	}

	public String getEndTimeAMPM() {
		return endTimeAMPM;
	}

	public void setEndTimeAMPM(String endTimeAMPM) {
		this.endTimeAMPM = endTimeAMPM;
	}

	public String getTaskDescription() {
		return taskDescription;
	}

	public void setTaskDescription(String taskDescription) {
		this.taskDescription = taskDescription;
	}

	public String getSetReminder() {
		return setReminder;
	}

	public void setSetReminder(String setReminder) {
		this.setReminder = setReminder;
	}

	public String getTaskEmailReminder() {
		return TaskEmailReminder;
	}

	public void setTaskEmailReminder(String taskEmailReminder) {
		TaskEmailReminder = taskEmailReminder;
	}

	public String getReminderDate() {
		return ReminderDate;
	}

	public void setReminderDate(String reminderDate) {
		ReminderDate = reminderDate;
	}

	public String getReminderTimeHH() {
		return ReminderTimeHH;
	}

	public void setReminderTimeHH(String reminderTimeHH) {
		ReminderTimeHH = reminderTimeHH;
	}

	public String getReminderTimeMM() {
		return ReminderTimeMM;
	}

	public void setReminderTimeMM(String reminderTimeMM) {
		ReminderTimeMM = reminderTimeMM;
	}

	public String getReminderTimeAMPM() {
		return ReminderTimeAMPM;
	}

	public void setReminderTimeAMPM(String reminderTimeAMPM) {
		ReminderTimeAMPM = reminderTimeAMPM;
	}

}
