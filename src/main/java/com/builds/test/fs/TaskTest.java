package com.builds.test.fs;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.fs.TaskWindowUI;
import com.builds.utilities.FranconnectUtil;

 public class TaskTest {

	public void fillTaskAndClickCreate(WebDriver driver, Task task) throws Exception {
		fillTask(driver, task);
		clickCreate(driver);
	}

	public void fillTask(WebDriver driver, Task task) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		TaskWindowUI ui = new TaskWindowUI(driver);

		fc.utobj().printTestStep("Fill Task Details");

		if (task.getAssignTo() != null) {
			if ("Lead Owner".equalsIgnoreCase(task.getAssignTo())) {
				fc.utobj().clickElement(driver, ui.leadOwnerRadio);
			} else {
				fc.utobj().clickElement(driver, ui.OtherUserRadio);
				// TODO Write code for multi select
			}
		}

		if (task.getStatus() != null) {
			fc.utobj().selectDropDown(driver, ui.Status_Select, task.getStatus());
		}

		if (task.getTaskType() != null) {
			fc.utobj().selectDropDown(driver, ui.TaskType_Select, task.getTaskType());
		}

		if (task.getSubject() != null) {
			fc.utobj().sendKeys(driver, ui.Subject, task.getSubject());
		}

		if (task.getTimelessTask() != null) {
			if ("yes".equalsIgnoreCase(task.getTimelessTask())) {
				fc.utobj().check(ui.TimelessTaskId_CheckBox, "yes");
			}
			if ("no".equalsIgnoreCase(task.getTimelessTask())) {
				fc.utobj().check(ui.TimelessTaskId_CheckBox, "no");
			}
		}

		if ("no".equalsIgnoreCase(task.getTimelessTask())) {
			if (task.getAddToCalendar() != null) {
				if ("yes".equalsIgnoreCase(task.getAddToCalendar())) {
					fc.utobj().check(ui.calendarTaskCheckBox, "yes");
				}
				if ("no".equalsIgnoreCase(task.getAddToCalendar())) {
					fc.utobj().check(ui.calendarTaskCheckBox, "no");
				}
			}

			if (task.getStartTimeHH() != null) {
				fc.utobj().selectDropDown(driver, ui.startTimeHH, task.getStartTimeHH());
			}

			if (task.getStartTimeMM() != null) {
				fc.utobj().selectDropDown(driver, ui.startTimeMM, task.getStartTimeMM());
			}

			if (task.getStartTimeAMPM() != null) {
				fc.utobj().selectDropDown(driver, ui.startTimeAMPM, task.getStartTimeAMPM());
			}

			if (task.getEndTimeHH() != null) {
				fc.utobj().selectDropDown(driver, ui.endTimeHH, task.getEndTimeHH());
			}

			if (task.getEndTimeMM() != null) {
				fc.utobj().selectDropDown(driver, ui.endTimeMM, task.getEndTimeMM());
			}

			if (task.getEndTimeAMPM() != null) {
				fc.utobj().selectDropDown(driver, ui.endTimeAMPM, task.getEndTimeAMPM());
			}

			if (task.getSetReminder() != null) {
				if ("No Reminder".equalsIgnoreCase(task.getSetReminder())) {
					fc.utobj().clickElement(driver, ui.noReminderRadio);
				} else if ("15 Minutes Prior".equalsIgnoreCase(task.getSetReminder())) {
					fc.utobj().clickElement(driver, ui.FifteenMinutesPriorRadio);
				} else if ("Set Time Yourself".equalsIgnoreCase(task.getSetReminder())) {
					fc.utobj().clickElement(driver, ui.SetTimeYourselfRadio);
				}
			}

			if (task.getTaskEmailReminder() != null) {
				if ("yes".equalsIgnoreCase(task.getTaskEmailReminder())) {
					fc.utobj().check(ui.taskEmailReminder_checkbox, "yes");
				}
				if ("no".equalsIgnoreCase(task.getTaskEmailReminder())) {
					fc.utobj().check(ui.taskEmailReminder_checkbox, "no");
				}
			}

			if (task.getReminderDate() != null) {
				fc.utobj().sendKeys(driver, ui.reminderDate, task.getReminderDate());
			}

			if (task.getReminderTimeHH() != null) {
				fc.utobj().selectDropDown(driver, ui.rTimeHH, task.getReminderTimeHH());
			}

			if (task.getReminderTimeMM() != null) {
				fc.utobj().selectDropDown(driver, ui.rTimeMM, task.getReminderTimeMM());
			}

			if (task.getReminderTimeAMPM() != null) {
				fc.utobj().selectDropDown(driver, ui.rTimeAMPM, task.getReminderTimeAMPM());
			}

		}

		if (task.getPriority() != null) {
			fc.utobj().selectDropDown(driver, ui.Priority_Select, task.getPriority());
		}

		if (task.getStartDate() != null) {
			fc.utobj().sendKeys(driver, ui.startDate_Date, task.getStartDate());
		}

		if (task.getTaskDescription() != null) {
			fc.utobj().sendKeys(driver, ui.taskDescription, task.getTaskDescription());
		}

	}

	public void clickCreate(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		TaskWindowUI ui = new TaskWindowUI(driver);

		fc.utobj().printTestStep("Click Create Button");
		fc.utobj().clickElement(driver, ui.Create_Input_ByValue);
	}
}
