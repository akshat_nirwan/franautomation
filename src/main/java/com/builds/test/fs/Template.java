package com.builds.test.fs;

class Template {

	private String TemplateName;
	private String EmailType;
	private String FolderName;
	private String Attachment;
	private String SetAsEmailType;
	private String EmailSubject;
	private String Accessibility;
	private String body;

	public String getTemplateName() {
		return TemplateName;
	}

	public void setTemplateName(String templateName) {
		TemplateName = templateName;
	}

	public String getEmailType() {
		return EmailType;
	}

	public void setEmailType(String emailType) {
		EmailType = emailType;
	}

	public String getFolderName() {
		return FolderName;
	}

	public void setFolderName(String folderName) {
		FolderName = folderName;
	}

	public String getAttachment() {
		return Attachment;
	}

	public void setAttachment(String attachment) {
		Attachment = attachment;
	}

	public String getSetAsEmailType() {
		return SetAsEmailType;
	}

	public void setSetAsEmailType(String setAsEmailType) {
		SetAsEmailType = setAsEmailType;
	}

	public String getEmailSubject() {
		return EmailSubject;
	}

	public void setEmailSubject(String emailSubject) {
		EmailSubject = emailSubject;
	}

	public String getAccessibility() {
		return Accessibility;
	}

	public void setAccessibility(String accessibility) {
		Accessibility = accessibility;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}
}
