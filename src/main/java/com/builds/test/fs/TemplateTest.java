package com.builds.test.fs;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.fs.TemplateUI;
import com.builds.utilities.FranconnectUtil;

class TemplateTest {
	FranconnectUtil fc = new FranconnectUtil();

	public void fillTemplateDetails_clickSaveButton(WebDriver driver, Template template) throws Exception {
		fillTemplateDetails(driver, template);
		clickSaveButton(driver);
	}

	public void fillTemplateDetails(WebDriver driver, Template template) throws Exception {
		TemplateUI ui = new TemplateUI(driver);

		fc.utobj().printTestStep("Fill Template Details");

		if (template.getTemplateName() != null) {
			fc.utobj().sendKeys(driver, ui.mailTitle, template.getTemplateName());
		}

		if (template.getEmailSubject() != null) {
			fc.utobj().sendKeys(driver, ui.mailSubject, template.getEmailSubject());
		}

		if (template.getAccessibility() != null) {
			if (template.getAccessibility().equalsIgnoreCase("Public")) {
				fc.utobj().clickElement(driver, ui.public_Accessibility);
			} else {
				fc.utobj().clickElement(driver, ui.private_Accessibility);
			}
		}

		if (template.getEmailType() != null) {
			if (template.getEmailType().equalsIgnoreCase("Graphical")) {
				fc.utobj().clickElement(driver, ui.graphicalTemplate);
			} else if (template.getEmailType().contains("Plain")) {
				fc.utobj().clickElement(driver, ui.graphicalTemplate);
			} else if (template.getEmailType().toLowerCase().contains("html")
					|| template.getEmailType().toLowerCase().contains("zip")) {
				fc.utobj().clickElement(driver, ui.uploadHTML_ZIPTemplate);
			}
		}

		if (template.getFolderName() != null) {
			fc.utobj().sendKeys(driver, ui.mailSubject, template.getFolderName());
		}

		if (template.getBody() != null) {
			fc.utobj().switchFrame(driver, ui.emailBody_Frame_ByID);
			fc.utobj().sendKeys(driver, ui.emailBody, template.getBody());

		}

		if (template.getSetAsEmailType() != null) {
			fc.utobj().selectDropDown(driver, ui.SetAsEmailType_Select, template.getSetAsEmailType());
		}
	}

	public void clickSaveButton(WebDriver driver) throws Exception {
		fc.commonMethods().Click_Save_Button_ByValue(driver);
	}

}
