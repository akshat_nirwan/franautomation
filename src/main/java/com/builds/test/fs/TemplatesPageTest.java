package com.builds.test.fs;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.fs.TemplatePageUI;
import com.builds.utilities.FranconnectUtil;

class TemplatesPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	public void createTemplate_topRight_Button(WebDriver driver) throws Exception {
		TemplatePageUI ui = new TemplatePageUI(driver);
		fc.utobj().printTestStep("Click Create Template Button (Top Right)");
		fc.utobj().clickElement(driver, ui.createTemplateButton);
	}

}
