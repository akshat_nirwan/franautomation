package com.builds.test.fs;

class Visit {

	private String visitSchedule;
	private String visitdate;
	private String type;
	// private String addToCalendar;
	private String assignTo;

	private String visitor1Name;
	private String relationship1;
	private String visitor2Name;
	private String relationship2;
	private String visitor3Name;
	private String relationship3;

	private String agreedReimbursement;
	private String actualReimbursement;
	private String paymentSentDate;
	private String visitConfirmed;
	private String comments;

	public String getVisitSchedule() {
		return visitSchedule;
	}

	public void setVisitSchedule(String visitSchedule) {
		this.visitSchedule = visitSchedule;
	}

	public String getVisitdate() {
		return visitdate;
	}

	public void setVisitdate(String visitdate) {
		this.visitdate = visitdate;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getAssignTo() {
		return assignTo;
	}

	public void setAssignTo(String assignTo) {
		this.assignTo = assignTo;
	}

	public String getVisitor1Name() {
		return visitor1Name;
	}

	public void setVisitor1Name(String visitor1Name) {
		this.visitor1Name = visitor1Name;
	}

	public String getRelationship1() {
		return relationship1;
	}

	public void setRelationship1(String relationship1) {
		this.relationship1 = relationship1;
	}

	public String getVisitor2Name() {
		return visitor2Name;
	}

	public void setVisitor2Name(String visitor2Name) {
		this.visitor2Name = visitor2Name;
	}

	public String getRelationship2() {
		return relationship2;
	}

	public void setRelationship2(String relationship2) {
		this.relationship2 = relationship2;
	}

	public String getVisitor3Name() {
		return visitor3Name;
	}

	public void setVisitor3Name(String visitor3Name) {
		this.visitor3Name = visitor3Name;
	}

	public String getRelationship3() {
		return relationship3;
	}

	public void setRelationship3(String relationship3) {
		this.relationship3 = relationship3;
	}

	public String getAgreedReimbursement() {
		return agreedReimbursement;
	}

	public void setAgreedReimbursement(String agreedReimbursement) {
		this.agreedReimbursement = agreedReimbursement;
	}

	public String getActualReimbursement() {
		return actualReimbursement;
	}

	public void setActualReimbursement(String actualReimbursement) {
		this.actualReimbursement = actualReimbursement;
	}

	public String getPaymentSentDate() {
		return paymentSentDate;
	}

	public void setPaymentSentDate(String paymentSentDate) {
		this.paymentSentDate = paymentSentDate;
	}

	public String getVisitConfirmed() {
		return visitConfirmed;
	}

	public void setVisitConfirmed(String visitConfirmed) {
		this.visitConfirmed = visitConfirmed;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

}
