package com.builds.test.fs;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.fs.VisitUI;
import com.builds.utilities.FranconnectUtil;

class VisitTest {

	void fillAndSubmitVisit(WebDriver driver, Visit vi) throws Exception {
		VisitUI vui = new VisitUI(driver);
		FranconnectUtil fc = new FranconnectUtil();

		fc.utobj().printTestStep("Fill Visit Info");

		if (vi.getVisitSchedule() != null) {
			fc.utobj().sendKeys(driver, vui.visitSchedule, vi.getVisitSchedule());
		}

		if (vi.getVisitdate() != null) {
			fc.utobj().sendKeys(driver, vui.visitdate, vi.getVisitdate());
		}

		if (vi.getType() != null) {
			if (vi.getType().equalsIgnoreCase("Individual")) {
				fc.utobj().clickElement(driver, vui.typeIndividual);
			} else if (vi.getType().equalsIgnoreCase("Group")) {
				fc.utobj().clickElement(driver, vui.typeGroup);
			}
		}

		fc.utobj().clickElement(driver, vui.addToCalendar);

		if (vi.getAssignTo() != null) {
			if (vi.getAssignTo().equalsIgnoreCase("Lead Owner")) {
				fc.utobj().clickElement(driver, vui.assignToLeadOwner);
			} else if (vi.getAssignTo() == "Logged in User") {
				fc.utobj().clickElement(driver, vui.assignToLoggedInUser);
			} else {
				if (vi.getAssignTo().equalsIgnoreCase("Select All")) {
					fc.utobj().clickElement(driver, vui.assignToOtherUser);
					fc.utobj().clickElement(driver, vui.OtherUser_Div_ByXpath);
					fc.utobj().clickElement(driver, vui.selectAll);
				} else {
					fc.utobj().clickElement(driver, vui.assignToOtherUser);
					fc.utobj().clickElement(driver, vui.OtherUser_Div_ByXpath);
					fc.utobj().sendKeys(driver, vui.searchUser, vi.getAssignTo());
					fc.utobj().clickElement(driver, vui.assignToXyzUser);
				}
			}
		}

		if (vi.getVisitor1Name() != null) {
			fc.utobj().sendKeys(driver, vui.visitor1Name, vi.getVisitor1Name());
		}

		if (vi.getRelationship1() != null) {
			fc.utobj().sendKeys(driver, vui.relationship1, vi.getRelationship1());
		}

		if (vi.getVisitor2Name() != null) {
			fc.utobj().sendKeys(driver, vui.visitor2Name, vi.getVisitor2Name());
		}

		if (vi.getRelationship2() != null) {
			fc.utobj().sendKeys(driver, vui.relationship2, vi.getRelationship2());
		}

		if (vi.getVisitor3Name() != null) {
			fc.utobj().sendKeys(driver, vui.visitor3Name, vi.getVisitor3Name());
		}

		if (vi.getRelationship3() != null) {
			fc.utobj().sendKeys(driver, vui.relationship3, vi.getRelationship3());
		}

		if (vi.getAgreedReimbursement() != null) {
			fc.utobj().sendKeys(driver, vui.agreedReimbursement, vi.getAgreedReimbursement());
		}

		if (vi.getActualReimbursement() != null) {
			fc.utobj().sendKeys(driver, vui.actualReimbursement, vi.getActualReimbursement());
		}

		if (vi.getPaymentSentDate() != null) {
			fc.utobj().sendKeys(driver, vui.paymentSentDate, vi.getPaymentSentDate());
		}

		if (vi.getVisitConfirmed() != null) {
			fc.utobj().sendKeys(driver, vui.visitConfirmed, vi.getVisitConfirmed());
		}

		if (vi.getComments() != null) {
			fc.utobj().sendKeys(driver, vui.comments, vi.getComments());
		}

		fc.utobj().clickElement(driver, vui.saveVisitBtn);

	}

	void validateVisit(WebDriver driver, Visit vi) throws Exception {
		VisitUI vui = new VisitUI(driver);
		FranconnectUtil fc = new FranconnectUtil();

		fc.utobj().printTestStep("Validate Visit data successfully submission");

		if (!fc.utobj().isElementPresent(driver, vui.modifyVisitBtn)) {
			fc.utobj().throwsException("Modify button not present on visit tab page");
		}
		if (!fc.utobj().isElementPresent(driver, vui.printVisitBtn)) {
			fc.utobj().throwsException("Print button not present on visit tab page");
		}

	}

}
