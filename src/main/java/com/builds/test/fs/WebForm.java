package com.builds.test.fs;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.builds.utilities.FranconnectUtil;

class WebForm {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "addLeadWebForm1" })

	public void leadSubmit() throws Exception {

		Reporter.log("TC_Lead : Create CorporateUser.\n");
		Reporter.log("###################################################################");
		String testCaseId = "TC_Lead";

		WebDriver driver = new FirefoxDriver();

		for (int x = 1; x <= 24; x++) {

			testCaseId = "TC_Lead".concat("_");
			testCaseId = testCaseId + x;
			Map<String, String> dataSet = fc.utobj().readTestData(testCaseId);

			System.out.println("???????????????" + testCaseId);

			String un = dataSet.get("uniqueCode");
			String firstName = dataSet.get("firstName");
			System.out.println(firstName);
			String lastName = dataSet.get("lastName");
			System.out.println(lastName);
			String emailID = dataSet.get("emailID");
			System.out.println(emailID);
			String country = dataSet.get("country");
			System.out.println(country);
			String stateID = dataSet.get("stateID");
			System.out.println(stateID);
			String zip = dataSet.get("zip");
			System.out.println(zip);
			String leadSource2ID = dataSet.get("leadSource2ID");
			System.out.println(leadSource2ID);
			String leadSource3ID = dataSet.get("leadSource3ID");
			System.out.println(leadSource3ID);

			String county = dataSet.get("county");
			System.out.println(county);

			String division = dataSet.get("division");
			System.out.println(division);

			if (division.equalsIgnoreCase("Division_1")) {
				driver.get("http://franconnect.test.net/franconnect/extforms/division-1");
			} else if (division.equalsIgnoreCase("Division_2")) {
				driver.get("http://franconnect.test.net/franconnect/extforms/division-2");
			} else {
				driver.get("http://franconnect.test.net/franconnect/extforms/division-none");
			}

			fc.utobj().getElementByName(driver, "firstName").sendKeys(firstName);
			fc.utobj().getElementByName(driver, "lastName").sendKeys(lastName + "" + un);
			fc.utobj().getElementByName(driver, "emailID").sendKeys(emailID);

			fc.utobj().selectDropDownByVisibleTextTrimed(driver, fc.utobj().getElementByName(driver, "country"),
					country);
			fc.utobj().selectDropDownByVisibleTextTrimed(driver, fc.utobj().getElementByName(driver, "stateID"),
					stateID);
			fc.utobj().getElementByName(driver, "zip").sendKeys(zip);
			fc.utobj().selectDropDownByVisibleTextTrimed(driver, fc.utobj().getElementByName(driver, "leadSource2ID"),
					leadSource2ID);

			fc.utobj().selectDropDownByVisibleTextTrimed(driver, fc.utobj().getElementByName(driver, "leadSource3ID"),
					leadSource3ID);
			fc.utobj().clickElement(driver, fc.utobj().getElementByID(driver, "submitButton"));

		}
		driver.quit();
	}
}
