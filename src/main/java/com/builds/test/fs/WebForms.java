package com.builds.test.fs;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

class WebForms {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "sales12334343" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FS_addLead_LeadPage_001", testCaseDescription = "Add WebForm for sales module and verify the lead is getting added")
	private void addLeadFromWebForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminSales adsales = new AdminSales();
			adsales.manageWebFormGenerator(driver);
			fc.utobj().printTestStep("Create a webform in admin sales");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//input[@value='Create New Form']"));
			String formName = "wb" + fc.utobj().generateRandomNumber();
			fc.utobj().sendKeys(driver, fc.utobj().getElementByID(driver, "formName"), formName);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByID(driver, "formDisplayTitle"), "WebFrom Title");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByID(driver, "formUrl"), formName);
			fc.utobj().clickElement(driver, fc.utobj().getElementByID(driver, "detailsNextBtn"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByID(driver, "designNextBtn"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByID(driver, "settingsNextBtn"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByID(driver, "hostURL"));
			String newFormUrl = fc.utobj().getText(driver, fc.utobj().getElementByID(driver, "hostCodeBox"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByID(driver, "confirmationNextBtn"));
			fc.utobj().printTestStep("Open Created web form");
			driver.get(newFormUrl);
			fc.utobj().printTestStep("Fill the form");
			String firstName = "test" + fc.utobj().generateRandomNumber();
			String lastName = "test" + fc.utobj().generateRandomNumber();
			fc.utobj().sendKeys(driver, fc.utobj().getElementByID(driver, "firstName"), firstName);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByID(driver, "lastName"), lastName);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByID(driver, "emailID"),
					"salesautomation@staffex.com");
			fc.utobj().printTestStep("Submit");
			fc.utobj().clickElement(driver, fc.utobj().getElementByID(driver, "submitButton"));

			boolean isSubmitted = fc.utobj().assertPageSource(driver,
					"Thank you for submitting your information, we will get back to you shortly.");

			if (isSubmitted == false) {
				fc.utobj().throwsException("Unable to submit lead");
			}

			driver = fc.loginpage().login(driver);

			Sales fs = new Sales();
			fc.utobj().printTestStep("Search for the added lead");
			fs.search(driver);
			FSSearchPageTest p3 = new FSSearchPageTest();
			p3.searchByLeadName(driver, firstName, lastName);

			boolean isLeadPresent = fc.utobj().assertPageSource(driver, firstName);
			if (isLeadPresent == false) {
				fc.utobj().throwsException("Lead not added from leadPage.jsp");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
}
