package com.builds.test.fs.testravi;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

public class franconnectListener implements ITestListener {
	StringBuffer htmlPage = new StringBuffer();
	FileWriter fileWriter = null;
	BufferedWriter bufferedWriter = null;
	File file = new File("D:\\Reports_Franconnect.html");

	@Override

	public void onStart(ITestContext arg0) {

		if (file.exists()) {
			file.delete();
			file = new File("D:\\Reports_Franconnect.html");
		}

		try {
			fileWriter = new FileWriter(file);
			bufferedWriter = new BufferedWriter(fileWriter);

			htmlPage.append("<!DOCTYPE html><html><head>" + "<link href='fran.css' type='text/css' rel='stylesheet' />"
					+ "</head><body><div class=\"topvalue\">"
					+ "<div ><b><h3><center><u>Customized TestCases Report</u></center></h3><div></b><ul></ul></div>"
					+ "</body></html>");

			bufferedWriter.write(htmlPage.toString());

			bufferedWriter.flush();
			fileWriter.flush();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				System.out.println("hello");
				bufferedWriter.close();
				fileWriter.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	@Override

	public void onTestStart(ITestResult arg0) {

		BufferedReader In = null;
		if (file.exists()) {
			try {
				In = new BufferedReader(new FileReader(file));
				StringBuilder fileLines = new StringBuilder();
				String temp = null;
				while ((temp = In.readLine()) != null) {
					fileLines.append(temp);
				}
				String finstr = fileLines.toString();
				String onteststartstr = "<li><span >" + arg0.getName() + "</span></li></ul></div></body></html>";

				finstr = finstr.replace("</ul></div></body></html>", onteststartstr);
				In.close();
				FileWriter fw = new FileWriter(file);
				BufferedWriter out = new BufferedWriter(fw);
				out.write(finstr);
				out.flush();
				out.close();

			} catch (IOException e) {
				e.printStackTrace();
			}

		}

		// htmlPage.append("<ul><li>" + arg0.getName() +
		// "</li></ul></body></html>");

	}

	@Override

	public void onTestSuccess(ITestResult arg0) {

		System.out.println("Test Pass->" + arg0.getName());

	}

	@Override

	public void onTestFailure(ITestResult arg0) {

		System.out.println("Test Failed->" + arg0.getName());

	}

	@Override

	public void onTestSkipped(ITestResult arg0) {

		System.out.println("Test Skipped->" + arg0.getName());

	}

	@Override

	public void onFinish(ITestContext arg0) {

		System.out.println("END Of Execution(TEST)->" + arg0.getName());

	}

	@Override

	public void onTestFailedButWithinSuccessPercentage(ITestResult arg0) {

		// TODO Auto-generated method stub

	}

}
