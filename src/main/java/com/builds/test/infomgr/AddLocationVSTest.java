package com.builds.test.infomgr;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.uimaps.admin.AdminAreaRegionAddAreaRegionPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AddLocationVSTest {
	
	FranconnectUtil fc= new FranconnectUtil();
	
	@Test(groups = { "InfoMgr2" })
	@TestCase(createdOn = "2018-04-27", updatedOn = "2018-04-27", testCaseId = "TC_AddAdminLocaion", testCaseDescription = "Verify Add Location From Admin")
	private void addLocationAdmin() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("infomgr2", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminAreaRegionAddAreaRegionPage pobj = new AdminAreaRegionAddAreaRegionPage(driver);
			System.out.println("Login Done");
			fc.adminpage().adminAreaRegionAddAreaRegionPage(driver);
			fc.adminpage().openAddNewAreaRegion(driver);
						
			
			
			
			
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
}


		

