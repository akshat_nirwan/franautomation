package com.builds.test.infomgr;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminAreaRegionAddAreaRegionPageTest;
import com.builds.test.admin.LocationData;
import com.builds.test.common.Location;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminAddLocationPageTest {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "infomgr","IM1" })
	@TestCase(createdOn = "2018-05-03", updatedOn = "2018-05-07", testCaseId = "TC_IM_AddAdminLocation_01", testCaseDescription = "Verify add location from admin with owner type as Individual with a New Owner")
	private void addLocationAdmin01() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());


		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Location location = new Location();
		LocationData ld = new LocationData();

		try {
			driver = fc.loginpage().login(driver);
			AdminAreaRegionAddAreaRegionPageTest region_page = new AdminAreaRegionAddAreaRegionPageTest();
			String region_name = fc.utobj().generateTestData("R1");
			region_page.addAreaRegion(driver, region_name);

			location = fc.commonMethods().getDefaultDataFor_Location(location);
			fc.adminpage().adminAddNewFranchiseLocationLnk(driver);
			location.setAreaRegion(region_name);
			ld.filllocationInfo(driver, location).submit(driver);
			fc.adminpage().manageFranchiseLocationLnk(driver).SearchFranchise(driver, location).VerifyLocationExists(driver, location);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr","IM1" })
	@TestCase(createdOn = "2018-05-03", updatedOn = "2018-05-07", testCaseId = "TC_IM_AddAdminLocation_02", testCaseDescription = "Verify add location from admin with Owner type as individual and Existing Owner and also create a MUID" + 
			"")
	private void addLocationAdmin02() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());


		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Location location = new Location();
		LocationData ld = new LocationData();

		try {
			driver = fc.loginpage().login(driver);
			AdminAreaRegionAddAreaRegionPageTest region_page = new AdminAreaRegionAddAreaRegionPageTest();
			String region_name = fc.utobj().generateTestData("R1");
			region_page.addAreaRegion(driver, region_name);

			location = fc.commonMethods().getDefaultDataFor_Location(location);
			// location.setOwnerTypeIndividual(null);

			fc.adminpage().adminAddNewFranchiseLocationLnk(driver);
			location.setAreaRegion(region_name);
			ld.filllocationInfo(driver, location).submit(driver);
			fc.adminpage().manageFranchiseLocationLnk(driver).SearchFranchise(driver, location)
					.VerifyLocationExists(driver, location);

			// Add Second location with existing Owner/muid
			fc.adminpage().adminAddNewFranchiseLocationLnk(driver);
			location.setAreaRegion(region_name);
			location.setSelectOwners(location.getOwnerFirstName() + " " + location.getOwnerLastName());
			String existingownerNme = location.getSelectOwners();
			location.setNewOwner(null);
			System.out.println(existingownerNme);
			location.setFranchiseID(location.getFranchiseID() + "_MUID");
			ld.filllocationInfo(driver, location).submit(driver);
			fc.adminpage().manageFranchiseLocationLnk(driver).SearchFranchise(driver, location).VerifyLocationExists(driver, location);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr","IM1" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "TC_IM_AddAdminLocation_03", testCaseDescription = "Verify add location  from admin with owner type as Entity and Entity as New Entity with a New Owner")
	private void addLocationAdmin03() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());


		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Location location = new Location();
		LocationData ld = new LocationData();

		try {
			driver = fc.loginpage().login(driver);
			AdminAreaRegionAddAreaRegionPageTest region_page = new AdminAreaRegionAddAreaRegionPageTest();
			String region_name = fc.utobj().generateTestData("R1");
			region_page.addAreaRegion(driver, region_name);

			location = fc.commonMethods().getDefaultDataFor_Location(location);
			fc.adminpage().adminAddNewFranchiseLocationLnk(driver);
			location.setAreaRegion(region_name);
			location.setOwnerTypeIndividual(null);

			ld.filllocationInfo(driver, location).submit(driver);
			fc.adminpage().manageFranchiseLocationLnk(driver).SearchFranchise(driver, location).VerifyLocationExists(driver, location);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr","IM1" })
	@TestCase(createdOn = "2018-05-11", updatedOn = "2018-05-11", testCaseId = "TC_IM_AddAdminLocation_04", testCaseDescription = "Verify add location from admin with owner type as Entity and Entity as New Entity and Existing Owner and also create a MUID")
	private void addLocationAdmin04() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());


		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Location location = new Location();
		LocationData ld = new LocationData();

		try {
			driver = fc.loginpage().login(driver);
			AdminAreaRegionAddAreaRegionPageTest region_page = new AdminAreaRegionAddAreaRegionPageTest();
			String region_name = fc.utobj().generateTestData("R1");
			region_page.addAreaRegion(driver, region_name);

			location = fc.commonMethods().getDefaultDataFor_Location(location);
			fc.adminpage().adminAddNewFranchiseLocationLnk(driver);
			location.setAreaRegion(region_name);
			location.setOwnerTypeIndividual(null);

			ld.filllocationInfo(driver, location).submit(driver);
			fc.adminpage().manageFranchiseLocationLnk(driver).SearchFranchise(driver, location).VerifyLocationExists(driver, location);
			
			

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	@Test(groups = { "infomgr","IM1" })
	@TestCase(createdOn = "2018-05-15", updatedOn = "2018-05-15", testCaseId = "TC_IM_AddAdminLocation_05", testCaseDescription = "To verify add location from admin with owner type as Entity and Entity as Existing Entity with a New Owner")
	private void addLocationAdmin05() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());


		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Location location = new Location();
		LocationData ld = new LocationData();

		try {
			driver = fc.loginpage().login(driver);
			AdminAreaRegionAddAreaRegionPageTest region_page = new AdminAreaRegionAddAreaRegionPageTest();
			String region_name = fc.utobj().generateTestData("R1");
			region_page.addAreaRegion(driver, region_name);

			location = fc.commonMethods().getDefaultDataFor_Location(location);
			fc.adminpage().adminAddNewFranchiseLocationLnk(driver);
			location.setAreaRegion(region_name);
			location.setOwnerTypeIndividual(null);

			ld.filllocationInfo(driver, location).submit(driver);
			fc.adminpage().manageFranchiseLocationLnk(driver).SearchFranchise(driver, location).VerifyLocationExists(driver, location);
			
			// Add Second location with existing user/muid
			fc.adminpage().adminAddNewFranchiseLocationLnk(driver);
			location.setAreaRegion(region_name);
			location.setFranchiseID(location.getFranchiseID() + "_MUID");
			location.setNewEntity(null);
			
			location.setExistingEntityName(location.getEntityName());
			location.setExistingEntityName(location.getExistingEntityName());
			ld.filllocationInfo(driver, location).submit(driver);
			fc.adminpage().manageFranchiseLocationLnk(driver).SearchFranchise(driver, location).VerifyLocationExists(driver, location);
			
			

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
}
	@Test(groups = { "infomgr","IM1" })
	@TestCase(createdOn = "2018-05-16", updatedOn = "2018-05-16", testCaseId = "TC_IM_AddAdminLocation_06", testCaseDescription = "To verify add location from admin with owner type as Entity and Entity as Existing Entity and Existing Owner and also create a MUID" + 
			"")
	private void addLocationAdmin06() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());


		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Location location = new Location();
		LocationData ld = new LocationData();

		try {
			driver = fc.loginpage().login(driver);
			AdminAreaRegionAddAreaRegionPageTest region_page = new AdminAreaRegionAddAreaRegionPageTest();
			String region_name = fc.utobj().generateTestData("R1");
			region_page.addAreaRegion(driver, region_name);

			location = fc.commonMethods().getDefaultDataFor_Location(location);
			fc.adminpage().adminAddNewFranchiseLocationLnk(driver);
			location.setAreaRegion(region_name);
			location.setOwnerTypeIndividual(null);

			ld.filllocationInfo(driver, location).submit(driver);
			fc.adminpage().manageFranchiseLocationLnk(driver).SearchFranchise(driver, location).VerifyLocationExists(driver, location);
			
			// Add Second location with existing user/muid
			fc.adminpage().adminAddNewFranchiseLocationLnk(driver);
			location.setAreaRegion(region_name);
			location.setSelectOwners(location.getOwnerFirstName() + " " + location.getOwnerLastName());
			String existingownerNme = location.getSelectOwners();
			location.setNewOwner(null);
			System.out.println(existingownerNme);
			
			location.setFranchiseID(location.getFranchiseID() + "_MUID");
			location.setNewEntity(null);
			
			location.setExistingEntityName(location.getEntityName());
			location.setExistingEntityName(location.getExistingEntityName());
			ld.filllocationInfo(driver, location).submit(driver);
			fc.adminpage().manageFranchiseLocationLnk(driver).SearchFranchise(driver, location).VerifyLocationExists(driver, location);
			
			

			//fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
}
	@Test(groups = { "infomgr","IM1" })
	@TestCase(createdOn = "2018-05-16", updatedOn = "2018-05-16", testCaseId = "TC_IM_AddAdminLocation_07", testCaseDescription = "To verify location modification from Admin" + 
			"")
	private void addLocationAdmin07() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());


		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Location location = new Location();
		LocationData ld = new LocationData();

		try {
			driver = fc.loginpage().login(driver);
			AdminAreaRegionAddAreaRegionPageTest region_page = new AdminAreaRegionAddAreaRegionPageTest();
			String region_name = fc.utobj().generateTestData("R1");
			region_page.addAreaRegion(driver, region_name);

			location = fc.commonMethods().getDefaultDataFor_Location(location);
			fc.adminpage().adminAddNewFranchiseLocationLnk(driver);
			location.setAreaRegion(region_name);
			location.setOwnerTypeIndividual(null);

			ld.filllocationInfo(driver, location).submit(driver);
			fc.adminpage().manageFranchiseLocationLnk(driver).SearchFranchise(driver, location).VerifyLocationExists(driver, location);
			
			fc.utobj().actionImgOption(driver, location.getFranchiseID(), "Modify");
			location.setFranchiseID(location.getFranchiseID() + "_M");
			location.setCenterName(location.getCenterName() + "_M");
					
			location.setLicenseNo(location.getLicenseNo()+ "_M");
			location.setCorporateLocation("No");
			location.setOpeningDate(fc.utobj().getFutureDateUSFormat(1));
			location.setStreetAddress(location.getStreetAddress()+"_M");
			location.setAddress2(location.getAddress2()+"_M");
			location.setCity(location.getCity()+ "_M");
			location.setCountry("India");
			location.setState("Delhi");
			location.setZipCode(location.getZipCode()+"9");
			location.setPhone(location.getPhone()+"9");
			location.setPhoneExtension(location.getPhoneExtension()+"9");
			location.setFax(location.getFax()+"9");
			location.setMobile(location.getMobile()+"9");
			location.setEmail("vimal.sharma@franconnect.com");
			
			//location.setEmail(fc.utobj().generateRandomChar()+"@franconnect.com");
			
			location.setWebsite("http://www.franconnect.com");
			location.setTitle("Dr.");
			location.setFirstName(location.getFirstName()+"_M");
			location.setLastName(location.getLastName()+"_M");
			location.setCenterPhone(location.getCenterPhone()+"9");
			location.setCenterPhoneExtesion(location.getCenterPhoneExtesion()+"9");
			location.setCenterEmail("fcprelive@gmail.com");
			location.setCenterFax(location.getCenterFax()+"9");
			location.setCenterMobile(location.getCenterMobile()+"9");
			location.setEntityType("Limited Partnership");
			location.setEntityName(location.getEntityName()+"_M");	
			//location.setTaxpayerID(location.getTaxpayerID()+"_M");
			location.setCountryofFormationResidency("India");
			location.setStateofFormationResidency("Goa");
			ld.Modifylocationinfo(driver, location).submit(driver);
			fc.adminpage().manageFranchiseLocationLnk(driver).SearchFranchise(driver, location).VerifyLocationExists(driver, location);
			fc.utobj().printTestStep("Location Exists with Modified Franchisee ID");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);


		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
}
	@Test(groups = { "infomgr","IM1" })
	@TestCase(createdOn = "2018-05-017", updatedOn = "2018-05-17", testCaseId = "TC_IM_AddAdminLocation_08", testCaseDescription = "Create New Entity from Modify Location Page")
	private void addLocationAdmin08() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());


		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Location location = new Location();
		LocationData ld = new LocationData();

		try {
			driver = fc.loginpage().login(driver);
			AdminAreaRegionAddAreaRegionPageTest region_page = new AdminAreaRegionAddAreaRegionPageTest();
			String region_name = fc.utobj().generateTestData("R1");
			region_page.addAreaRegion(driver, region_name);

			location = fc.commonMethods().getDefaultDataFor_Location(location);
			fc.adminpage().adminAddNewFranchiseLocationLnk(driver);
					
			location.setAreaRegion(region_name);
			ld.filllocationInfo(driver, location).submit(driver);
			fc.adminpage().manageFranchiseLocationLnk(driver).SearchFranchise(driver, location).VerifyLocationExists(driver, location);
			
			fc.utobj().actionImgOption(driver, location.getFranchiseID(), "Modify");
			Location location2 = new Location();
			location2.setOwnerTypeIndividual(null);
			location2.setOwnerTypeIndividualEntity("Entity");
			location2.setNewEntity("New Entity");
			location2.setEntityType("General Partnership");
			location2.setEntityName(fc.utobj().generateTestData("FIM_Entity"));	
			location2.setTaxpayerID(fc.utobj().generateTestData("FIM_Taxpayer"));
			location2.setCountryofFormationResidency("USA");
			location2.setStateofFormationResidency("Florida");
			ld.ModifylocationAndAddEntity(driver, location2).submit(driver);
			fc.adminpage().manageFranchiseLocationLnk(driver).SearchEntity(driver, location2).VerifyEntityExists(driver, location2);
								
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

}
	@Test(groups = { "infomgr","IM1" })
	@TestCase(createdOn = "2018-05-21", updatedOn = "2018-05-21", testCaseId = "TC_AddAdminLocaion_09", testCaseDescription = "Delete Franchisee Location")
	private void addLocationAdmin09() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Location location = new Location();
		LocationData ld = new LocationData();

		try {
			driver = fc.loginpage().login(driver);
			AdminAreaRegionAddAreaRegionPageTest region_page = new AdminAreaRegionAddAreaRegionPageTest();
			String region_name = fc.utobj().generateTestData("R1");
			region_page.addAreaRegion(driver, region_name);
			location = fc.commonMethods().getDefaultDataFor_Location(location);
			fc.adminpage().adminAddNewFranchiseLocationLnk(driver);
			location.setAreaRegion(region_name);
			ld.filllocationInfo(driver, location).submit(driver);
			fc.adminpage().manageFranchiseLocationLnk(driver).SearchFranchise(driver, location).VerifyLocationExists(driver, location);
			fc.adminpage().openDeleteFranchiseeLocation(driver).SearchFranchise(driver, location);
			fc.utobj().actionImgOption(driver, location.getFranchiseID(),"Delete");
			fc.utobj().acceptAlertBox(driver);	
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.commonMethods().Click_Close_Input_ByValue(driver);
			fc.adminpage().openDeleteFranchiseeLocation(driver).SearchDeletedFranchiseByRegion(driver, location).VerifyDeletedLocation(driver, location);
			fc.utobj().printTestStep("Deleted Location not found. Success!!!");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

}

	@Test(groups = { "infomgr","IM1" })
	@TestCase(createdOn = "2018-05-21", updatedOn = "2018-05-21", testCaseId = "TC_AddAdminLocaion_10_REST_API", testCaseDescription = "Delete Franchisee Location")
	private void addLocationAdmin10() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());


		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Location location = new Location();
		LocationData ld = new LocationData();

		try {
			driver = fc.loginpage().login(driver);
			AdminAreaRegionAddAreaRegionPageTest region_page = new AdminAreaRegionAddAreaRegionPageTest();
			String region_name = fc.utobj().generateTestData("R1");
			region_page.addAreaRegion(driver, region_name);
			location = fc.commonMethods().getDefaultDataFor_Location(location);
			fc.adminpage().adminAddNewFranchiseLocationLnk(driver);
			location.setAreaRegion(region_name);
			ld.filllocationInfo(driver, location).submit(driver);
			fc.adminpage().manageFranchiseLocationLnk(driver).SearchFranchise(driver, location).VerifyLocationExists(driver, location);
			fc.adminpage().openDeleteFranchiseeLocation(driver).SearchFranchise(driver, location);
			fc.utobj().actionImgOption(driver, location.getFranchiseID(),"Delete");
			fc.utobj().acceptAlertBox(driver);	
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.commonMethods().Click_Close_Input_ByValue(driver);
			fc.adminpage().openDeleteFranchiseeLocation(driver).SearchDeletedFranchiseByRegion(driver, location).VerifyDeletedLocation(driver, location);
			fc.utobj().printTestStep("Deleted Location not found. Success!!!");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

}

}
