package com.builds.test.infomgr;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;

import com.builds.test.admin.AdminAreaRegionAddAreaRegionPageTest;
import com.builds.test.admin.AdminConfigurationConfigureStoreTypePageTest;
import com.builds.test.admin.AdminDivisionAddDivisionPageTest;
import com.builds.test.admin.AdminFDDManagementFDDEmailTemplateSummaryAddTemplatePageTest;
import com.builds.test.admin.AdminFDDManagementITEM23RECEIPTSummaryPageTest;
import com.builds.test.admin.AdminHiddenLinksConfigureNewHierarchyLevelPageTest;
import com.builds.test.admin.AdminPageTest;
import com.builds.test.admin.AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest;
import com.builds.test.admin.LocationData;
import com.builds.test.common.Location;
import com.builds.uimaps.admin.AdminAreaRegionAddAreaRegionPage;
import com.builds.uimaps.admin.AdminAreaRegionManageAreaRegionPage;
import com.builds.uimaps.admin.AdminConfigurationConfigureStoreTypePage;
import com.builds.uimaps.admin.AdminConfigurationPIIConfigurationPage;
import com.builds.uimaps.admin.AdminDivisionAddDivisionPage;
import com.builds.uimaps.admin.AdminDivisionManageDivisionPage;
import com.builds.uimaps.admin.AdminFDDManagementFDDEmailTemplateSummaryPage;
import com.builds.uimaps.admin.AdminFDDManagementFDDManagementPage;
import com.builds.uimaps.admin.AdminFDDManagementFDDManagementUploadModifyFDDPage;
import com.builds.uimaps.admin.AdminFranchiseLocationAddFranchiseLocationPage;
import com.builds.uimaps.fin.AdminFinanceAgreementVersionsPage;
import com.builds.uimaps.infomgr.AdminConfigurartionConfigureAreaRegionLabelPage;
import com.builds.uimaps.infomgr.AdminInfoMgrConfigureInfoMgrTabsPage;
import com.builds.uimaps.infomgr.AdminInfoMgrManageFormGeneratorAddTabPage;
import com.builds.uimaps.infomgr.AdminInfoMgrManageFormGeneratorConfigureTabularViewPage;
import com.builds.uimaps.infomgr.AdminInfoMgrManageFormGeneratorModifyFieldsPositionPage;
import com.builds.uimaps.infomgr.AdminInfoMgrManageFormGeneratorPage;
import com.builds.uimaps.infomgr.AdminInfoMgrManageFormGeneratorTabDetailsPage;
import com.builds.uimaps.infomgr.AdminInfoMgrManageWebFormGeneratorPage;
import com.builds.uimaps.infomgr.AdminInfoMgrTriggersAndAuditingPage;
import com.builds.uimaps.infomgr.AgreementUI;
import com.builds.uimaps.infomgr.InfoMgrCorporateLocationsPage;
import com.builds.uimaps.infomgr.InfoMgrCustomTabPage;
import com.builds.uimaps.infomgr.InfoMgrFranchiseesPage;
import com.builds.utilities.DateManipulator;
import com.builds.utilities.FranconnectUtil;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class AdminInfoMgrCommonMethods {

	FranconnectUtil fc = new FranconnectUtil();
	AdminPageTest adminpagelinks = new AdminPageTest();
	InfoMgr_Common imc = new InfoMgr_Common();
	AdminDivisionAddDivisionPageTest division = new AdminDivisionAddDivisionPageTest();

	public String addNewTab(WebDriver driver, String Component) throws Exception {
		Reporter.log("**** Add New Tab **** \n");

		String testCaseId = "TC_InfoMgr_Admin_ManagetFormGenerator_AddTab";

		fc.infomgr().infomgr_common().AdminFIMManageFormGenerator(driver);

		NavigateToManageFormGeneratorTabs(driver, "Franchisee");

		AdminInfoMgrManageFormGeneratorPage objAdminManageFormGen = new AdminInfoMgrManageFormGeneratorPage(driver);

		String newTabName = "";

		// CLick on Add New Tab after selecting the component tab
		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().clickLink(driver, Component);
				fc.utobj().clickElement(driver, objAdminManageFormGen.btnAddNewTab);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				AdminInfoMgrManageFormGeneratorAddTabPage objAddNewTabPage = new AdminInfoMgrManageFormGeneratorAddTabPage(driver);
				newTabName = fc.utobj().generateTestData("Custom Tab");
				fc.utobj().sendKeys(driver, objAddNewTabPage.txtDisplayName, newTabName);
				fc.utobj().selectDropDownByPartialText(driver, objAddNewTabPage.drpSubModuleName, Component);
				fc.utobj().clickElement(driver, objAddNewTabPage.addMoreFalse);
				fc.utobj().clickElement(driver, objAddNewTabPage.btnSubmit);
				fc.utobj().switchFrameToDefault(driver);

				// verify that tab is added
				boolean tabAddedFlag = fc.utobj().assertLinkText(driver, newTabName);
				if (tabAddedFlag) {
					Reporter.log("Tab is added successfully");
				} else {
					fc.utobj().throwsException("Error - Tab couldn't be added. ");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsException("Error in adding custom tab , please refer screenshot!");
		}
		return newTabName;
	}

	public String addAgreement(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("********************* Add Agreement ********************* \n");

		String testCaseId = "TC_Admin_InfoMgr_AddAgreement";
		Map<String, String> dataSet = fc.utobj().readTestData("infomgr", testCaseId);

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));

		AdminFinanceAgreementVersionsPage pobj = new AdminFinanceAgreementVersionsPage(driver);
		fc.finance().finance_common().adminFinAgreementVersionsPage(driver);
		fc.utobj().sleep();
		fc.utobj().clickElement(driver, pobj.AgreemenmtVersionBtn);
		fc.utobj().clickElement(driver, pobj.AddAgreement);

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().sendKeys(driver, pobj.AgreementName, agrmntVrsnName);
				fc.utobj().sendKeys(driver, pobj.AgreementStartDate, fc.utobj().getFutureDateUSFormat(3));
				fc.utobj().sendKeys(driver, pobj.AgreementEndDate, fc.utobj().getFutureDateUSFormat(100));
				try {
					fc.utobj().selectDropDown(driver, pobj.AgreementFrequency, "Monthly");
				} catch (Exception e1) {
					try {
						fc.utobj().selectDropDownByVisibleText(driver, pobj.AgreementFrequency, "Weekly");
					} catch (Exception e2) {
						try {
							fc.utobj().selectDropDownByVisibleText(driver, pobj.AgreementFrequency, "Daily");
						} catch (Exception e3) {

						}
					}
				}
				fc.utobj().sendKeys(driver, pobj.RoyaltyValue, dataSet.get("royaltyPercentage"));
				fc.utobj().clickElement(driver, pobj.AdvType);
				fc.utobj().sendKeys(driver, pobj.AdvValue, dataSet.get("advertisementPercentage"));
				fc.utobj().sendKeys(driver, pobj.RoyaltyAreaFranchiseValue, dataSet.get("RoyaltyAreaFranchise"));
				fc.utobj().sendKeys(driver, pobj.AdvAreaFranchiseValue, dataSet.get("AdvAreaFranchise"));
				fc.utobj().clickElement(driver, pobj.SubmitBtn);

				boolean isTextPresent = fc.utobj().assertLinkText(driver, agrmntVrsnName);
				if (isTextPresent == false) {
					fc.utobj().selectDropDownByValue(driver, pobj.selectViewPerPage, "500");
					{
						boolean isTextPresent1 = fc.utobj().assertLinkText(driver, agrmntVrsnName);
						if (isTextPresent1 == false) {
							fc.utobj().throwsException("Agreement Version is Not added!");
						}
					}
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception ex) {
				fc.utobj().throwsException("Agreement couldn't be added on page");
			}
		} else {
			fc.utobj().throwsException("Error in adding owner , please refer screenshot!");
		}
		return agrmntVrsnName;
	}

	

	public ArrayList<Map<String,String>>  addSectionAndFieldsToTab(WebDriver driver, String Component, String linkText ,String SectionName) throws Exception {
		Reporter.log("********************* Add  Section and Fields ********************* \n");

		String testCaseId = "TC_InfoMgr_Admin_ManagetFormGenerator_Add_Section_and_Fields";

		fc.infomgr().infomgr_common().AdminFIMManageFormGenerator(driver);
		NavigateToManageFormGeneratorTabs(driver, Component);
		AdminInfoMgrManageFormGeneratorPage objAdminManageFormGen = new AdminInfoMgrManageFormGeneratorPage(driver);

		String fieldText = fc.utobj().generateTestData("text");
		String fieldTextPhone = fc.utobj().generateTestData("textphone");
		String fieldTextEmail = fc.utobj().generateTestData("textemail");
		String fieldTextURL = fc.utobj().generateTestData("texturl");
		String fieldTextArea = fc.utobj().generateTestData("textarea");
		String fieldDate = fc.utobj().generateTestData("date");
		String fieldDropDown = fc.utobj().generateTestData("dropdown");
		String fieldCountry= fc.utobj().generateTestData("country");
		String fieldCountryState= fc.utobj().generateTestData("countrystate");
		String fieldRadio = fc.utobj().generateTestData("radio");
		String fieldRadioVer = fc.utobj().generateTestData("radiover");	
		String fieldDouble = fc.utobj().generateTestData("double");
		String fieldPercentage = fc.utobj().generateTestData("percentage");
		String fieldCheckBox = fc.utobj().generateTestData("chkbox");
		String fieldCheckBoxVer = fc.utobj().generateTestData("chkboxver");
        String state=fc.utobj().generateTestData("state");
		Map<String,String> add = new HashMap<String,String>();
		
		Map<String,String> modify = new HashMap<String,String>();
		
		String testCaseId1 = "TC_InfoMgr_AddCustomTabData";
		Map<String, String> cust1 = fc.utobj().readTestDatawithsqllite("info", testCaseId1, "Franchisee");
		String testCaseId2 = "TC_InfoMgr_ModifyCustomTabData";
		Map<String, String> cust2 = fc.utobj().readTestDatawithsqllite("info", testCaseId2, "Franchisee");
		ArrayList<Map<String,String>> list = new ArrayList<Map<String,String>>();
		
		if (fc.utobj().validate(testCaseId) == true) {
			
			
			try {
				fc.utobj().clickLink(driver, linkText);

				AdminInfoMgrManageFormGeneratorTabDetailsPage tabDetailsPage = objAdminManageFormGen.getAddTabDetailsPage();
			     fc.utobj().clickElement(driver, tabDetailsPage.btnAddSection);

				fc.commonMethods().switch_cboxIframe_frameId(driver);
						
				fc.utobj().sendKeys(driver, tabDetailsPage.txtSectionName, SectionName);	
				if(SectionName.contains("TAB")){
					fc.utobj().clickElement(driver, tabDetailsPage.radioTabular);
				}
				fc.utobj().clickElement(driver, tabDetailsPage.btnAddSectionName);			
				
			
			    // Adding Field date
				fc.utobj().switchFrameToDefault(driver);			
				Add_field(driver, SectionName);
				fc.commonMethods().switch_cboxIframe_frameId(driver);		
				fc.utobj().sendKeys(driver, tabDetailsPage.txtFieldName, fieldDate);
				fc.utobj().selectDropDownByValue(driver, tabDetailsPage.drpFieldType, "Date");
				fc.utobj().clickElement(driver, tabDetailsPage.btnSumbitField);
		     	add.put(fieldDate,cust1.get("Date"));
		     	modify.put(fieldDate,cust2.get("Date"));
		     
				// Adding field text None
		     	fc.utobj().switchFrameToDefault(driver);	     	
		     	Add_field(driver, SectionName);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().sendKeys(driver, tabDetailsPage.txtFieldName, fieldText);
				fc.utobj().selectDropDownByValue(driver, tabDetailsPage.fldValidationType, "None");
				fc.utobj().clickElement(driver, tabDetailsPage.btnSumbitField);		
				add.put(fieldText,cust1.get("Text"));
				modify.put(fieldText,cust2.get("Text"));
				
				// Adding field text Phone
				fc.utobj().switchFrameToDefault(driver);
				Add_field(driver, SectionName);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().sendKeys(driver, tabDetailsPage.txtFieldName, fieldTextPhone);
				fc.utobj().selectDropDownByValue(driver, tabDetailsPage.fldValidationType, "Phone");
				fc.utobj().clickElement(driver, tabDetailsPage.btnSumbitField);
				add.put(fieldTextPhone,cust1.get("Phone"));
				modify.put(fieldTextPhone,cust2.get("Phone"));
				if(!(SectionName.contains("TAB"))){
		 	    // Adding field text Email
				fc.utobj().switchFrameToDefault(driver);
				
				Add_field(driver, SectionName);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().sendKeys(driver, tabDetailsPage.txtFieldName, fieldTextEmail);
				fc.utobj().selectDropDownByValue(driver, tabDetailsPage.fldValidationType, "Email");
				fc.utobj().clickElement(driver, tabDetailsPage.btnSumbitField);
			    add.put(fieldTextEmail,cust1.get("Email"));
			    modify.put(fieldTextEmail,cust2.get("Email"));
				}
				
				// Adding field text URL
				fc.utobj().switchFrameToDefault(driver);
				Add_field(driver, SectionName);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().sendKeys(driver, tabDetailsPage.txtFieldName, fieldTextURL);
				fc.utobj().selectDropDownByValue(driver, tabDetailsPage.fldValidationType, "Url");
				fc.utobj().clickElement(driver, tabDetailsPage.btnSumbitField);
                add.put(fieldTextURL,cust1.get("Url"));
                modify.put(fieldTextURL,cust2.get("Url"));
                
				// Adding Field Text Area
				fc.utobj().switchFrameToDefault(driver);
				Add_field(driver, SectionName);
				fc.commonMethods().switch_cboxIframe_frameId(driver);	
				fc.utobj().sendKeys(driver, tabDetailsPage.txtFieldName, fieldTextArea);
				fc.utobj().selectDropDownByValue(driver, tabDetailsPage.drpFieldType, "text");
				fc.utobj().clickElement(driver, tabDetailsPage.btnSumbitField);
			add.put(fieldTextArea,cust1.get("TextArea"));
			modify.put(fieldTextArea,cust2.get("TextArea"));
				
				
				// Adding Field Drop down Country
				fc.utobj().switchFrameToDefault(driver);
				Add_field(driver, SectionName);
				fc.commonMethods().switch_cboxIframe_frameId(driver);			
				fc.utobj().sendKeys(driver, tabDetailsPage.txtFieldName, fieldCountry);
				fc.utobj().selectDropDownByValue(driver, tabDetailsPage.drpFieldType, "combo");		
				fc.utobj().clickElement(driver, tabDetailsPage.dropDownOption2);			   
				fc.utobj().clickElement(driver, tabDetailsPage.btnSumbitField);
                  add.put(fieldCountry,cust1.get("Country"));
                  modify.put(fieldCountry,cust2.get("Country"));
                  
				// Adding Field Drop down Country and State
				fc.utobj().switchFrameToDefault(driver);
				Add_field(driver, SectionName);
				fc.commonMethods().switch_cboxIframe_frameId(driver);			
				fc.utobj().sendKeys(driver, tabDetailsPage.txtFieldName, fieldCountryState);
				fc.utobj().selectDropDownByValue(driver, tabDetailsPage.drpFieldType, "combo");	
				fc.utobj().clickElement(driver, tabDetailsPage.dropDownOption3);
				fc.utobj().sendKeys(driver, tabDetailsPage.state, state);
				fc.utobj().clickElement(driver, tabDetailsPage.btnSumbitField);
				add.put(fieldCountryState,cust1.get("CountryState"));
				modify.put(fieldCountryState,cust2.get("CountryState"));
				//add.put(state,cust1.get("State"));
				//modify.put(state,cust2.get("State"));
				/*
				// Adding Field Drop down
				fc.utobj().switchFrameToDefault(driver);
				Add_field(driver, SectionName);
				fc.commonMethods().switch_cboxIframe_frameId(driver);			
				fc.utobj().sendKeys(driver, tabDetailsPage.txtFieldName, fieldDropDown);
				fc.utobj().selectDropDownByValue(driver, tabDetailsPage.drpFieldType, "combo");
				fc.utobj().sendKeys(driver, tabDetailsPage.option1, "option1");
				fc.utobj().clickElement(driver, tabDetailsPage.addOption);
			    fc.utobj().sendKeys(driver, tabDetailsPage.option2, "option2");
				fc.utobj().clickElement(driver, tabDetailsPage.btnSumbitField);			
				add.put(fieldDropDown,cust1.get("DropDownOption"));
				modify.put(fieldDropDown,cust2.get("DropDownOption"));
				
				// Adding Field Radio Horizontal
				fc.utobj().switchFrameToDefault(driver);
				Add_field(driver, SectionName);
				fc.commonMethods().switch_cboxIframe_frameId(driver);			
				fc.utobj().sendKeys(driver, tabDetailsPage.txtFieldName, fieldRadio);
				fc.utobj().selectDropDownByValue(driver, tabDetailsPage.drpFieldType, "radio");
				fc.utobj().sendKeys(driver, tabDetailsPage.option1, "RadioOption1");
				fc.utobj().clickElement(driver, tabDetailsPage.addOption);
			    fc.utobj().sendKeys(driver, tabDetailsPage.option2, "RadioOption2");
				fc.utobj().clickElement(driver, tabDetailsPage.btnSumbitField);
				add.put(fieldRadio,cust1.get("RadioOption"));
				modify.put(fieldRadio, cust2.get("RadioOption"));
				
				// Adding Field Radio Vertical
				fc.utobj().switchFrameToDefault(driver);
				Add_field(driver, SectionName);
				fc.commonMethods().switch_cboxIframe_frameId(driver);			
				fc.utobj().sendKeys(driver, tabDetailsPage.txtFieldName, fieldRadioVer);
				fc.utobj().selectDropDownByValue(driver, tabDetailsPage.drpFieldType, "radio");
				fc.utobj().clickElement(driver, tabDetailsPage.optionViewHor);
				fc.utobj().sendKeys(driver, tabDetailsPage.option1, "RadioOption1");
				fc.utobj().clickElement(driver, tabDetailsPage.addOption);
			    fc.utobj().sendKeys(driver, tabDetailsPage.option2, "RadioOption2");
				fc.utobj().clickElement(driver, tabDetailsPage.btnSumbitField);
				add.put(fieldRadioVer,cust1.get("RadioOption"));
				modify.put(fieldRadioVer, cust2.get("RadioOption"));*/
				
				// Adding Field Numeric Currency
				fc.utobj().switchFrameToDefault(driver);
				Add_field(driver, SectionName);
				fc.commonMethods().switch_cboxIframe_frameId(driver);			
				fc.utobj().sendKeys(driver, tabDetailsPage.txtFieldName, fieldDouble);
				fc.utobj().selectDropDownByValue(driver, tabDetailsPage.drpFieldType, "numeric");
				fc.utobj().selectDropDownByValue(driver, tabDetailsPage.fldValidationType1, "Double");
				fc.utobj().clickElement(driver, tabDetailsPage.btnSumbitField);
	    		add.put(fieldDouble,cust1.get("Double"));
		    	modify.put(fieldDouble, cust2.get("Double"));
		    	
				// Adding Field Numeric Percentage
				fc.utobj().switchFrameToDefault(driver);
				Add_field(driver, SectionName);
				fc.commonMethods().switch_cboxIframe_frameId(driver);			
				fc.utobj().sendKeys(driver, tabDetailsPage.txtFieldName, fieldPercentage);
				fc.utobj().selectDropDownByValue(driver, tabDetailsPage.drpFieldType, "numeric");
				fc.utobj().selectDropDownByValue(driver, tabDetailsPage.fldValidationType1, "Percentage");
				fc.utobj().clickElement(driver, tabDetailsPage.btnSumbitField);
			     add.put(fieldPercentage,cust1.get("Percentage"));
			     modify.put(fieldPercentage, cust2.get("Percentage"));
			     
				/*// Adding Field CheckBox Horizontal
				fc.utobj().switchFrameToDefault(driver);
				Add_field(driver, SectionName);
				fc.commonMethods().switch_cboxIframe_frameId(driver);			
				fc.utobj().sendKeys(driver, tabDetailsPage.txtFieldName, fieldCheckBox);
				fc.utobj().selectDropDownByValue(driver, tabDetailsPage.drpFieldType, "checkbox");
				fc.utobj().sendKeys(driver, tabDetailsPage.option1, "ChkBoxOption1");
				fc.utobj().clickElement(driver, tabDetailsPage.addOption);
			    fc.utobj().sendKeys(driver, tabDetailsPage.option2, "ChkBoxOption2");
				fc.utobj().clickElement(driver, tabDetailsPage.btnSumbitField);
				add.put(fieldCheckBox,cust1.get("ChkBoxOption"));
				modify.put(fieldCheckBox, cust2.get("ChkBoxOption"));
				
				// Adding Field CheckBox Vertical
				fc.utobj().switchFrameToDefault(driver);
				Add_field(driver, SectionName);
				fc.commonMethods().switch_cboxIframe_frameId(driver);			
				fc.utobj().sendKeys(driver, tabDetailsPage.txtFieldName, fieldCheckBoxVer);
				fc.utobj().selectDropDownByValue(driver, tabDetailsPage.drpFieldType, "checkbox");
				fc.utobj().clickElement(driver, tabDetailsPage.optviewtype);
				fc.utobj().sendKeys(driver, tabDetailsPage.option1, "ChkBoxOption1");
				fc.utobj().clickElement(driver, tabDetailsPage.addOption);
			    fc.utobj().sendKeys(driver, tabDetailsPage.option2, "ChkBoxOption2");
				fc.utobj().clickElement(driver, tabDetailsPage.btnSumbitField);
				add.put(fieldCheckBoxVer,cust1.get("ChkBoxOption"));
				modify.put(fieldCheckBoxVer, cust2.get("ChkBoxOption"));*/
				
			/*	// Adding Field Document
				String fieldDocument = fc.utobj().generateTestData("document");
				fc.utobj().switchFrameToDefault(driver);
				Add_field(driver, SectionName);
				fc.commonMethods().switch_cboxIframe_frameId(driver);			
				fc.utobj().sendKeys(driver, tabDetailsPage.txtFieldName, fieldDocument);
				fc.utobj().selectDropDownByValue(driver, tabDetailsPage.drpFieldType, "file");
				
				fc.utobj().clickElement(driver, tabDetailsPage.btnSumbitField);
				fc.utobj().switchFrameToDefault(driver);
				add.put(fieldDocument,cust1.get("Document"));
				modify.put(fieldDocument, cust2.get("Document"));
				
					// Adding Field Multi select Drop down
				fc.utobj().switchFrameToDefault(driver);
				Add_field(driver, SectionName);
				fc.commonMethods().switch_cboxIframe_frameId(driver);			
				fc.utobj().sendKeys(driver, tabDetailsPage.txtFieldName, fieldMultiSelectDropDown);
				fc.utobj().selectDropDownByValue(driver, tabDetailsPage.drpFieldType, "multiselect");
				fc.utobj().sendKeys(driver, tabDetailsPage.option1, "option1");
				fc.utobj().clickElement(driver, tabDetailsPage.addOption);
			    fc.utobj().sendKeys(driver, tabDetailsPage.option2, "option2");
				fc.utobj().clickElement(driver, tabDetailsPage.btnSumbitField);
		     	add.put(fieldMultiSelectDropDown,cust11.get("MultiSelectOption"));
				modify.put(fieldMultiSelectDropDown, cust2.get("MultiSelectOption"));
				
				// Adding Field Numeric Integer
				String fieldInteger = fc.utobj().generateTestData("integer");
				fc.utobj().switchFrameToDefault(driver);
				Add_field(driver, SectionName);
				fc.commonMethods().switch_cboxIframe_frameId(driver);			
				fc.utobj().sendKeys(driver, tabDetailsPage.txtFieldName, fieldInteger);
				fc.utobj().selectDropDownByValue(driver, tabDetailsPage.drpFieldType, "numeric");
				fc.utobj().selectDropDownByValue(driver, tabDetailsPage.fldValidationType1, "Integer");
				fc.utobj().clickElement(driver, tabDetailsPage.btnSumbitField);
				add.put(fieldInteger,cust1.get("Integer"));
				modify.put(fieldInteger, cust2.get("Integer"));*/
			
				
				list.add(add);
				list.add(modify);
			} catch (Exception e) {
			
			
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsException("Error in adding section and fields , please refer screenshot!");
			
		}
		return list;
	}

	public void enableAuditHistoryAndTriggercustomtab(WebDriver driver, String newTabName, InfoMgr_Common cmn, String corporateUserName, Map<String,String> field,String triggerEmail) throws Exception {

		Reporter.log("********************* Enable Audit History and Trigger for Custom Fields ********************* \n");
		fc.infomgr().infomgr_common().AdminFIMManageFormGenerator(driver);	
		AdminInfoMgrTriggersAndAuditingPage tau = new AdminInfoMgrTriggersAndAuditingPage(driver);
	
		try {
			cmn.adminFimTriggersAndAuditing(driver);
			fc.utobj().selectActionMenuItems(driver, "Modify");
			fc.utobj().clickElement(driver, tau.auditForm);
			WebElement enableAuditChkBox = driver.findElement(By.xpath(".//td[contains(text(),'" + newTabName + "')]//following::td[1]/input"));
			WebElement enableTriggerChkBox = driver.findElement(By.xpath(".//td[contains(text(),'" + newTabName + "')]//following::td[2]/input"));
			WebElement selectUser = driver.findElement(By.xpath(".//td[contains(text(),'" + newTabName + "')]//following::td[3]//div"));
			WebElement email = driver.findElement(By.xpath(".//td[contains(text(),'" + newTabName + "')]//following::td[4]//textarea"));
			WebElement selectAction = driver.findElement(By.xpath(".//td[contains(text(),'" + newTabName + "')]//following::td[5]//select"));

			
			fc.utobj().check(enableAuditChkBox, "yes");
			fc.utobj().check(enableTriggerChkBox, "yes");
			fc.utobj().selectValFromMultiSelectwithoutSelectAll(driver, selectUser, corporateUserName);
			fc.utobj().sendKeys(driver, email, triggerEmail);
			fc.utobj().selectDropDown(driver, selectAction, "Add / Modify");
			fc.utobj().clickElement(driver, tau.submit);
			WebElement setting = driver.findElement(By.xpath(".//td[contains(text(),'" + newTabName + "')]/following::td[10]//a"));
			fc.utobj().clickElement(driver, setting);
			fc.utobj().actionImgOption(driver, newTabName, "Field Level Configuration");
			
			fc.utobj().clickElement(driver, tau.enableAudit);
			fc.utobj().clickElement(driver, tau.selectAllTrigger);
			fc.utobj().selectValFromMultiSelectwithoutSelectAll(driver, tau.trigerDropDown, corporateUserName);
			fc.utobj().clickElement(driver, tau.tickUser);
			fc.utobj().sendKeys(driver, tau.email, triggerEmail);
			fc.utobj().clickElement(driver, tau.tickRecipient);
			//
			for (Map.Entry<String, String> entry : field.entrySet())
			{
				String name =entry.getKey();
				String value =entry.getValue();
				if(name.contains("date") || name.contains("document")|| name.contains("country")|| name.contains("state")){
					
					fc.utobj().selectDropDownByValue(driver, driver.findElement(By.xpath(".//td[contains(text(),'"+name+"')]//following-sibling::td//select[contains(@onchange ,'configureEvent')]")), "#MOD#");
				}
				else if(name.contains("integer") ||name.contains("percent")){
					fc.utobj().selectDropDownByValue(driver, driver.findElement(By.xpath(".//td[contains(text(),'"+name+"')]//following-sibling::td//select[contains(@onchange ,'configureEvent')]")), "#LTH#");
					fc.utobj().sendKeys(driver, driver.findElement(By.xpath(".//td[contains(text(),'"+name+"')]//following-sibling::td//select[contains(@onchange ,'configureEvent')]/following-sibling::input")), "1200000");
				}
				else if(name.contains("double")){
					fc.utobj().selectDropDownByValue(driver, driver.findElement(By.xpath(".//td[contains(text(),'"+name+"')]//following-sibling::td//select[contains(@onchange ,'configureEvent')]")), "#GTH#");
					fc.utobj().sendKeys(driver, driver.findElement(By.xpath(".//td[contains(text(),'"+name+"')]//following-sibling::td//select[contains(@onchange ,'configureEvent')]/following-sibling::input")), "1");
				}
				else if(name.contains("drop") ||name.contains("multi")){
					fc.utobj().selectDropDownByValue(driver, driver.findElement(By.xpath(".//td[contains(text(),'"+name+"')]//following-sibling::td//select[contains(@onchange ,'configureEvent')]")), "#MOD#");
				
				   // fc.utobj().selectDropDownByValue(driver, driver.findElement(By.xpath(".//td[contains(text(),'"+name+"')]//following-sibling::td//select[contains(@onchange ,'configureEvent')]/following-sibling::select")), value);
				}
				else{
					fc.utobj().selectDropDownByValue(driver, driver.findElement(By.xpath(".//td[contains(text(),'"+name+"')]//following-sibling::td//select[contains(@onchange ,'configureEvent')]")), "#EQA#");
				
			    fc.utobj().sendKeys(driver, driver.findElement(By.xpath(".//td[contains(text(),'"+name+"')]//following-sibling::td//select[contains(@onchange ,'configureEvent')]/following-sibling::input")), value);
				}
					
			}
			fc.utobj().clickElement(driver, tau.submit);
			
  cmn.configureFormLevelEmail(driver);
  cmn.configureFieldLevelEmail(driver);
		}

		catch (Exception e) {
			
			fc.utobj().throwsException("Error in enabling audit and trigger history, please refer screenshot!");
		}
	}

	public ArrayList<String> addFields(WebDriver driver, String Component, String linkText) throws Exception {
		Reporter.log("********************* Add  Fields ********************* \n");

		String testCaseId = "TC_InFo_Mgr_Admin_ManagetFormGenerator_Add_Fields";

		ArrayList<String> lstFields = new ArrayList<String>();

		fc.infomgr().infomgr_common().AdminFIMManageFormGenerator(driver);
		NavigateToManageFormGeneratorTabs(driver, Component);
		AdminInfoMgrManageFormGeneratorPage objAdminManageFormGen = new AdminInfoMgrManageFormGeneratorPage(driver);

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().clickLink(driver, linkText);

				AdminInfoMgrManageFormGeneratorTabDetailsPage tabDetailsPage = objAdminManageFormGen.getAddTabDetailsPage();
				lstFields.add(fc.utobj().generateTestData("field1"));
				lstFields.add(fc.utobj().generateTestData("field2"));

				fc.utobj().clickElement(driver, tabDetailsPage.btnAddField);

				// Adding field 1
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().sendKeys(driver, tabDetailsPage.txtFieldName, lstFields.get(0));
				fc.utobj().clickElement(driver, tabDetailsPage.btnSumbitField);

				// Adding Field2
				fc.utobj().switchFrameToDefault(driver);
				fc.utobj().clickElement(driver, tabDetailsPage.btnAddField);

				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().sendKeys(driver, tabDetailsPage.txtFieldName, lstFields.get(1));
				fc.utobj().clickElement(driver, tabDetailsPage.btnSumbitField);
				fc.utobj().switchFrameToDefault(driver);

				boolean field1Flag = fc.utobj().assertPageSource(driver, lstFields.get(0));
				boolean field2Flag = fc.utobj().assertPageSource(driver, lstFields.get(1));
				if (field1Flag && field2Flag) {
					Reporter.log("Fields were added to the form successfully");
				} else {
					fc.utobj().throwsException("Error - Fields couldn't be added ");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsException("Error in adding fields , please refer screenshot!");
		}

		return lstFields;
	}

	public String addDivisionalUser(WebDriver driver, String userName, String password, String divisionName) throws Exception {
		Reporter.log("********************* Add  Divisional User ********************* \n");

		String testCaseId = "TC_InFoMgr_Admin_Add_Divisional_User";

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				userName = fc.utobj().generateTestData(userName);
				AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest objAddDivisionalUser = new AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest();

				String emailId = "infomgrautomation@staffex.com";

				objAddDivisionalUser.addDivisionalUser(driver, userName, password, divisionName, emailId);
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsException("Error in adding divisional user , please refer screenshot!");
		}
		return userName;
	}

	public void configureTabularViewInfoMgrFranchiseesForms(WebDriver driver, String formName, ArrayList<String> lstFields) throws Exception {
		Reporter.log("********************* Configure Tabular View in Info Mgr Forms ********************* \n");

		String testCaseId = "TC_InFoMgr_Admin_ManagetFormGenerator_Configure_Tabular_View_For_Forms";

		fc.infomgr().infomgr_common().AdminFIMManageFormGenerator(driver);
		NavigateToManageFormGeneratorTabs(driver, "Franchisee");

		AdminInfoMgrManageFormGeneratorPage objAdminManageFormGen = new AdminInfoMgrManageFormGeneratorPage(driver);

		AdminInfoMgrManageFormGeneratorConfigureTabularViewPage objConfigureTabularView = objAdminManageFormGen.getConfigureTabularViewPage();

		if (fc.utobj().validate(testCaseId) == true) {
			try {

				fc.utobj().selectDropDownByVisibleText(driver, objAdminManageFormGen.drpFormNames, formName);
				fc.utobj().clickElement(driver, objAdminManageFormGen.btnConfgTabularView);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				// First remove all the values from the selected fields list box
				int countFileds = fc.utobj().listBoxSelectAllValues(objConfigureTabularView.lstSelectedFields);
				if (countFileds > 0) {
					fc.utobj().clickElement(driver, objConfigureTabularView.imgRemoveFields);
				}

				// Select the values as passed in the parameter
				fc.utobj().listBoxSelectValues(lstFields, objConfigureTabularView.lstFieldsTabularView);
				fc.utobj().clickElement(driver, objConfigureTabularView.imgAddFields);

				fc.utobj().clickElement(driver, objConfigureTabularView.btnSave);
				fc.utobj().switchFrameToDefault(driver);
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsException("Error in configuring tabular view , please refer screenshot!");
		}
	}

	public void disableFieldsInFormGenerator(WebDriver driver, String linkText, String fieldName) throws Exception {
		Reporter.log("********************* Disable fields in Manage Form Generator ********************* \n");
		// Navigate to Admin > Info Mgr > Manage Form Generator

		String testCaseId = "TC_InFoMgr_Admin_ManagetFormGenerator_Disable_Fields_For_Forms";

		fc.infomgr().infomgr_common().AdminFIMManageFormGenerator(driver);
		NavigateToManageFormGeneratorTabs(driver, "Franchisee");

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().clickLink(driver, linkText);
				Thread.sleep(2000);

				// Get the src property of the image button
				WebElement element = fc.utobj().getElementByXpath(driver, ".//*[@id='siteMainTable']//td[contains(text(), '" + fieldName + "')]/ancestor::tr[1]/td[6]/a/img");
				String src = fc.utobj().getAttributeValue(element, "src");
				if (!src.contains("deactive.png")) {
					fc.utobj().clickElement(driver, element);
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsException("Error in disabling fields in manage form generator , please refer screenshot!");
		}
	}

	public String uploadFDDWithDivision(WebDriver driver, String fileName, String divisionName) throws Exception {
		String testCaseId = "TC_InFoMgr_Admin_FDDManagement_Upload_FDD_With_Division";

		Reporter.log("********************************** Adding Item Receipt ******************");

		AdminFDDManagementITEM23RECEIPTSummaryPageTest objItem23Receipt = new AdminFDDManagementITEM23RECEIPTSummaryPageTest();
		String item23Receipt = objItem23Receipt.addITEM23RECEIPTS_DS(driver, "item23Receipt");

		Reporter.log("****************************** Upload FDD With Division ********************************");

		String fddName = fc.utobj().generateTestData("FDD1");
		fc.infomgr().infomgr_common().adminFDDManagement(driver);

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				AdminFDDManagementFDDManagementPage pobj = new AdminFDDManagementFDDManagementPage(driver);
				fc.utobj().clickElement(driver, pobj.uploadFDDBtn);

				AdminFDDManagementFDDManagementUploadModifyFDDPage pobj2 = new AdminFDDManagementFDDManagementUploadModifyFDDPage(driver);
				fc.utobj().sendKeys(driver, pobj2.fddNameTxt, fddName);
				fc.utobj().sendKeys(driver, pobj2.fddVersionTxt, "Version1");
				fc.utobj().sendKeys(driver, pobj2.fddExpiryTxt, DateManipulator.futureDate());
				fc.utobj().selectDropDownByVisibleText(driver, pobj2.drpDivisionBrand, divisionName);
				fc.utobj().selectDropDownByVisibleText(driver, pobj2.fddItemReceiptFSDrp, item23Receipt);
				fc.utobj().selectDropDownByVisibleText(driver, pobj2.countryDrp, "USA");
				fc.utobj().selectDropDownByVisibleText(driver, pobj2.fddItemReceiptFIMDrp, item23Receipt);
				fc.utobj().clickElement(driver, pobj2.selectAll);
				try {
					fc.utobj().acceptAlertBox(driver);
				} catch (Exception e) {

				}
				fc.utobj().pdfFileUpload(pobj2.fddFileUploadBtn, fileName);
				fc.utobj().sendKeys(driver, pobj2.commentsTxtArea, "");
				fc.utobj().clickElement(driver, pobj2.addFDDBtn);
				fc.utobj().sendKeys(driver, pobj.fddNameTxt, fddName);
				fc.utobj().clickElement(driver, pobj.searchBtn);

				boolean boolFddUploadFlag = fc.utobj().assertLinkText(driver, fddName);
				if (boolFddUploadFlag == true) {
					Reporter.log("FDD Upload Sucessfull !!!");
				} else {
					fc.utobj().throwsException("FDD Upload Failed !!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsException("Error in uploading FDD with division , please refer screenshot!");
		}
		return fddName;
	}

	public String uploadFDDWithoutDivision(WebDriver driver, String fileName) throws Exception {
		String testCaseId = "TC_InFoMgr_Admin_FDDManagement_Upload_FDD_Without_Division";

		Reporter.log("********************************** Adding Item Receipt ******************");

		AdminFDDManagementITEM23RECEIPTSummaryPageTest objItem23Receipt = new AdminFDDManagementITEM23RECEIPTSummaryPageTest();
		String item23Receipt = objItem23Receipt.addITEM23RECEIPTS_DS(driver, "item23Receipt");

		Reporter.log("********************* Upload FDD Without Division  ********************* \n");

		fc.infomgr().infomgr_common().adminFDDManagement(driver);
		String fddName = fc.utobj().generateTestData("FDD1");
		if (fc.utobj().validate(testCaseId) == true) {
			try {
				AdminFDDManagementFDDManagementPage pobj = new AdminFDDManagementFDDManagementPage(driver);
				fc.utobj().clickElement(driver, pobj.uploadFDDBtn);
				AdminFDDManagementFDDManagementUploadModifyFDDPage pobj2 = new AdminFDDManagementFDDManagementUploadModifyFDDPage(driver);
				fc.utobj().sendKeys(driver, pobj2.fddNameTxt, fddName);
				fc.utobj().sendKeys(driver, pobj2.fddVersionTxt, "Version1");
				fc.utobj().sendKeys(driver, pobj2.fddExpiryTxt, DateManipulator.futureDate());
				// fc.utobj().clickElement(driver,pobj2.calCloseButton); //
				// Element not clickable
				fc.utobj().selectDropDownByVisibleText(driver, pobj2.fddItemReceiptFSDrp, item23Receipt);
				fc.utobj().selectDropDownByVisibleText(driver, pobj2.countryDrp, "USA");
				fc.utobj().selectDropDownByVisibleText(driver, pobj2.fddItemReceiptFIMDrp, item23Receipt);
				fc.utobj().clickElement(driver, pobj2.selectAll);
				try {
					fc.utobj().acceptAlertBox(driver);
				} catch (Exception e) {
				}
				fc.utobj().pdfFileUpload(pobj2.fddFileUploadBtn, fileName);
				fc.utobj().sendKeys(driver, pobj2.commentsTxtArea, "");
				fc.utobj().clickElement(driver, pobj2.addFDDBtn);
				fc.utobj().sendKeys(driver, pobj.fddNameTxt, fddName);
				fc.utobj().clickElement(driver, pobj.searchBtn);
				boolean boolFddUploadFlag = fc.utobj().assertLinkText(driver, fddName);
				if (boolFddUploadFlag == true) {
					Reporter.log("FDD Upload Sucessfull !!!");
				} else {
					fc.utobj().throwsException("FDD Upload Failed !!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}

		} else {
			fc.utobj().throwsException("Error in uploading FDD without division , please refer screenshot!");
		}
		return fddName;
	}

	public String addAreaRegion(WebDriver driver) throws Exception {

		Reporter.log("********************* Add  Area Region ********************* \n");

		String testCaseId = "TC_InFoMgr_Admin_AreaRegion_AddAreaRegion";
		String regionName = fc.utobj().generateTestData("TestRegion");

		configureAreaRegionLabels(driver);
		    
		
		
		

		fc.adminpage().adminAreaRegionAddAreaRegionPage(driver);
		AdminAreaRegionAddAreaRegionPage pobj = new AdminAreaRegionAddAreaRegionPage(driver);

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().selectDropDown(driver, pobj.category, "Domestic");
				fc.utobj().sendKeys(driver, pobj.aregRegionName, regionName);
				fc.utobj().selectDropDown(driver, pobj.groupBy, "Zip Codes");
				fc.utobj().clickElement(driver, pobj.all);
				fc.utobj().clickElement(driver, pobj.commaSeparated);
				String zipCode = fc.utobj().generateRandomNumber();
				fc.utobj().sendKeys(driver, pobj.ziplist, zipCode);
				fc.utobj().clickElement(driver, pobj.Submit);
				try {
					fc.utobj().dismissAlertBox(driver);
				} catch (Exception e) {
				}

				fc.utobj().sleep();
				pobj = new AdminAreaRegionAddAreaRegionPage(driver);
				try {
					fc.utobj().clickElement(driver, pobj.nextBtn);
				} catch (Exception ex) {
					fc.utobj().clickElement(driver, pobj.proceedBtn);
					fc.utobj().clickElement(driver, pobj.nextBtn);

				}

				// Verify that region creation
				AdminAreaRegionManageAreaRegionPage objManageRegion = new AdminAreaRegionManageAreaRegionPage(driver);
				try {
					fc.utobj().clickElement(driver, objManageRegion.lnkShowAll);
				} catch (Exception ex) {

				}

				boolean regionAddFlag = fc.utobj().assertPageSource(driver, regionName);
				if (regionAddFlag) {
					Reporter.log("Region/Area Creation Successfull");
				} else {
					fc.utobj().throwsException("Region/Area Creation Failed");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsException("Error in adding area region , please refer screenshot!");
		}
		return regionName;
	}

	public String addStoreType(WebDriver driver) throws Exception {
		Reporter.log("********************* Add  Store Type ********************* \n");

		String testCaseId = "TC_InFoMgr_Admin_Add_Store_Type";

		String storeType = fc.utobj().generateTestData("StoreType1");

		fc.adminpage().adminConfigurationConfigureStoreType(driver);
		AdminConfigurationConfigureStoreTypePage pobj = new AdminConfigurationConfigureStoreTypePage(driver);

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().clickElement(driver, pobj.addStoreType);
				fc.utobj().sleep();
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().sendKeys(driver, pobj.storeType, storeType);
				fc.utobj().clickElement(driver, pobj.addBtn);
				fc.utobj().clickElement(driver, pobj.closeBtn);
				fc.utobj().sleep();
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsException("Error in adding store type , please refer screenshot!");
		}
		return storeType;
	}

	public String addDivisionName(WebDriver driver) throws Exception {

		String testCaseId = "TC_InFoMgr_Admin_DivisionManagement_division_add";

		Reporter.log("********************* Configuring Division ********************* \n");
		fc.utobj().printTestStep("Division Configuration Start");
		AdminHiddenLinksConfigureNewHierarchyLevelPageTest hierarchyPage = new AdminHiddenLinksConfigureNewHierarchyLevelPageTest();
		hierarchyPage.ConfigureNewHierarchyLevel(driver);
		fc.utobj().printTestStep("Division Configuration Ends");

		Reporter.log("********************* Add  Division ********************* \n");

		AdminDivisionAddDivisionPage pobj = new AdminDivisionAddDivisionPage(driver);
		fc.adminpage().adminDivisionAddDivision(driver);
		;
		String divisionName = fc.utobj().generateTestData("Division 01");

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().sendKeys(driver, pobj.DivisionName, divisionName);
				fc.utobj().clickElement(driver, pobj.submit);
				// Verify that division has been added on page
				try {
					AdminDivisionManageDivisionPage objManagerDivision = new AdminDivisionManageDivisionPage(driver);
					fc.utobj().clickElement(driver, objManagerDivision.lnkShowAll);
				} catch (Exception ex) {
				}
				boolean boolDivName = fc.utobj().assertPageSource(driver, divisionName);
				if (boolDivName == true) {
					Reporter.log("Division / Brand creation successfull !!!");
				} else {
					fc.utobj().throwsException("Division / Brand creation failed !!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsException("Error in creating division , please refer screenshot!");
		}
		return divisionName;
	}

	public String addFranchiseLocation(WebDriver driver) throws Exception {

		Reporter.log("********************* Add  Franchisee Location ********************* \n");

		String testCaseId = "TC_InFoMgr_Admin_FranchiseeLocation_AddLocation_DefaultValues";

		String franchiseId = fc.utobj().generateTestData("TestfranchiseId");

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				// Area Region
				String regionName = addAreaRegion(driver);

				String storeType = addStoreType(driver);

				fc.adminpage().adminAddNewFranchiseLocationLnk(driver);

				AdminFranchiseLocationAddFranchiseLocationPage pobj = new AdminFranchiseLocationAddFranchiseLocationPage(driver);

				fc.utobj().sendKeys(driver, pobj.franchiseeID, franchiseId);
				fc.utobj().sendKeys(driver, pobj.centerName, "0000");

				fc.utobj().selectDropDownByVisibleText(driver, pobj.areaRegion, regionName);

				fc.utobj().selectDropDownByVisibleText(driver, pobj.storeTypeId, storeType);
				fc.utobj().sendKeys(driver, pobj.openingDate, fc.utobj().getFutureDateUSFormat(30));
				fc.utobj().sendKeys(driver, pobj.city, "test city");

				fc.utobj().selectDropDown(driver, pobj.countryID, "USA");
				fc.utobj().selectDropDown(driver, pobj.stateID, "Alaska");
				fc.utobj().sendKeys(driver, pobj.storePhone, "0001");
				fc.utobj().sendKeys(driver, pobj.emailID, "Automationtest@gmail.com");
				fc.utobj().sendKeys(driver, pobj.ownerFirstName, fc.utobj().generateTestData("firstName"));
				fc.utobj().sendKeys(driver, pobj.ownerLastName, "lastName");
				fc.utobj().clickElement(driver, pobj.submit);

				// Verify that Franchise Location is added

				fc.infomgr().infomgr_common().InfoMgrFranchisees(driver);

				InfoMgrFranchiseesPage objFranPage = new InfoMgrFranchiseesPage(driver);
				try {
					fc.utobj().clickElement(driver, objFranPage.lnkShowAll);
				} catch (Exception ex) {

				}

				boolean boolAddFranFlag = fc.utobj().assertPageSource(driver, franchiseId);
				if (boolAddFranFlag == true) {
					Reporter.log("Franchise Location addition successfull !!! ");
				} else {
					fc.utobj().throwsException("Franchise Location addition failed !!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsException("Error in adding location , please refer screenshot!");
		}
		return franchiseId;
	}

	public String addFranchiseLocationWithRegion(WebDriver driver, String regionName) throws Exception {

		Reporter.log("********************* Add  Franchisee Location ********************* \n");

		String testCaseId = "TC_InFoMgr_Admin_FranchiseeLocation_AddLocation_DefaultValues";

		String franchiseId = fc.utobj().generateTestData("TestfranchiseId");

		if (fc.utobj().validate(testCaseId) == true) {
			try {

				// Store Type
				AdminConfigurationConfigureStoreTypePageTest addStoreType = new AdminConfigurationConfigureStoreTypePageTest();
				String storeType = fc.utobj().generateTestData("TestStore");
				addStoreType.addStoreType(driver, storeType);

				fc.adminpage().adminAddNewFranchiseLocationLnk(driver);
				AdminFranchiseLocationAddFranchiseLocationPage pobj = new AdminFranchiseLocationAddFranchiseLocationPage(driver);
				fc.utobj().sendKeys(driver, pobj.franchiseeID, franchiseId);
				fc.utobj().sendKeys(driver, pobj.centerName, "0000");
				;
				fc.utobj().selectDropDownByVisibleText(driver, pobj.areaRegion, regionName);
				;
				fc.utobj().selectDropDownByVisibleText(driver, pobj.storeTypeId, storeType);
				fc.utobj().sendKeys(driver, pobj.openingDate, fc.utobj().getFutureDateUSFormat(30));
				fc.utobj().sendKeys(driver, pobj.city, "test city");
				;
				fc.utobj().selectDropDownByVisibleText(driver, pobj.stateID, "Alaska");
				fc.utobj().sendKeys(driver, pobj.storePhone, "0001");
				fc.utobj().sendKeys(driver, pobj.ownerFirstName, fc.utobj().generateTestData("firstName"));
				fc.utobj().sendKeys(driver, pobj.ownerLastName, "lastName");
				fc.utobj().clickElement(driver, pobj.submit);

				// Verify that Franchise Location is added

				fc.infomgr().infomgr_common().InfoMgrFranchisees(driver);

				InfoMgrFranchiseesPage objFranPage = new InfoMgrFranchiseesPage(driver);
				try {
					fc.utobj().clickElement(driver, objFranPage.lnkShowAll);
				} catch (Exception ex) {

				}

				boolean boolAddFranFlag = fc.utobj().assertPageSource(driver, franchiseId);
				if (boolAddFranFlag == true) {
					Reporter.log("Franchise Location addition successfull !!! ");
				} else {
					fc.utobj().throwsException("Franchise Location addition failed !!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsException("Error in adding location , please refer screenshot!");
		}
		return franchiseId;
	}

	public String addFranchiseLocation(WebDriver driver, String franchiseID) throws Exception {

		Reporter.log("********************* Add  Franchisee Location with FranchiseID as Parameter ********************* \n");

		String testCaseId = "TC_InFoMgr_Admin_FranchiseeLocation_AddLocation_FranchiseeId_AsParam";
		String franchiseId = fc.utobj().generateTestData(franchiseID);

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				// Area Region
				AdminAreaRegionAddAreaRegionPageTest addRegion = new AdminAreaRegionAddAreaRegionPageTest();
				String regionName = fc.utobj().generateTestData("TestRegion");
				addRegion.addAreaRegion(driver, regionName);

				// Store Type
				AdminConfigurationConfigureStoreTypePageTest addStoreType = new AdminConfigurationConfigureStoreTypePageTest();
				String storeType = fc.utobj().generateTestData("TestStore");
				addStoreType.addStoreType(driver, storeType);

				fc.adminpage().adminAddNewFranchiseLocationLnk(driver);
				AdminFranchiseLocationAddFranchiseLocationPage pobj = new AdminFranchiseLocationAddFranchiseLocationPage(driver);
				fc.utobj().sendKeys(driver, pobj.franchiseeID, franchiseId);
				fc.utobj().sendKeys(driver, pobj.centerName, "0000");
				;
				fc.utobj().selectDropDownByVisibleText(driver, pobj.areaRegion, regionName);
				;
				fc.utobj().selectDropDownByVisibleText(driver, pobj.storeTypeId, storeType);

				fc.utobj().sendKeys(driver, pobj.openingDate, fc.utobj().getFutureDateUSFormat(10));

				fc.utobj().sendKeys(driver, pobj.city, "test city");
				;
				fc.utobj().selectDropDownByVisibleText(driver, pobj.stateID, "Alaska");

				fc.utobj().sendKeys(driver, pobj.storePhone, "0001");
				fc.utobj().sendKeys(driver, pobj.ownerFirstName, fc.utobj().generateTestData("firstName"));
				fc.utobj().sendKeys(driver, pobj.ownerLastName, "lastName");
				fc.utobj().clickElement(driver, pobj.submit);

				// Verify that Franchise LOcation is added

				fc.infomgr().infomgr_common().InfoMgrFranchisees(driver);

				InfoMgrFranchiseesPage objFranPage = new InfoMgrFranchiseesPage(driver);
				try {
					fc.utobj().clickElement(driver, objFranPage.lnkShowAll);
				} catch (Exception ex) {

				}
				boolean boolAddFranFlag = fc.utobj().assertPageSource(driver, franchiseId);
				if (boolAddFranFlag == true) {
					Reporter.log("Franchise Location addition successfull !!! ");
				} else {
					fc.utobj().throwsException("Franchise Location addition failed !!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsException("Error in adding location , please refer screenshot!");
		}
		return franchiseId;
	}

	public String addFranchiseLocationWithDivision(WebDriver driver, String divisionName) throws Exception {

		Reporter.log("************************ Add Franchisee Location With Division************");

		String testCaseId = "TC_InFoMgr_Admin_FranchiseeLocation_AddLocation_Division_AsParam";
		String franchiseId = fc.utobj().generateTestData("TestfranchiseId");

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				// Area Region
				AdminAreaRegionAddAreaRegionPageTest addRegion = new AdminAreaRegionAddAreaRegionPageTest();
				String regionName = fc.utobj().generateTestData("TestRegion");
				addRegion.addAreaRegion(driver, regionName);

				// Store Type
				AdminConfigurationConfigureStoreTypePageTest addStoreType = new AdminConfigurationConfigureStoreTypePageTest();
				String storeType = fc.utobj().generateTestData("TestStore");
				addStoreType.addStoreType(driver, storeType);

				// Add Franchise Location
				fc.adminpage().adminAddNewFranchiseLocationLnk(driver);
				AdminFranchiseLocationAddFranchiseLocationPage pobj = new AdminFranchiseLocationAddFranchiseLocationPage(driver);
				fc.utobj().sendKeys(driver, pobj.franchiseeID, franchiseId);
				fc.utobj().sendKeys(driver, pobj.centerName, "0000");
				fc.utobj().selectDropDownByVisibleText(driver, pobj.drpDivisionBrand, divisionName);
				;
				;
				fc.utobj().selectDropDownByVisibleText(driver, pobj.areaRegion, regionName);
				;
				fc.utobj().selectDropDownByVisibleText(driver, pobj.storeTypeId, storeType);

				fc.utobj().sendKeys(driver, pobj.openingDate, fc.utobj().getFutureDateUSFormat(10));

				fc.utobj().sendKeys(driver, pobj.city, "test city");
				;
				fc.utobj().selectDropDownByVisibleText(driver, pobj.stateID, "Alaska");

				fc.utobj().sendKeys(driver, pobj.storePhone, "0001");
				fc.utobj().sendKeys(driver, pobj.ownerFirstName, fc.utobj().generateTestData("firstName"));
				fc.utobj().sendKeys(driver, pobj.ownerLastName, "lastName");
				fc.utobj().clickElement(driver, pobj.submit);

				// Verify that Franchise LOcation is added

				fc.infomgr().infomgr_common().InfoMgrFranchisees(driver);

				InfoMgrFranchiseesPage objFranPage = new InfoMgrFranchiseesPage(driver);
				try {
					fc.utobj().clickElement(driver, objFranPage.lnkShowAll);
				} catch (Exception ex) {

				}
				boolean boolAddFranFlag = fc.utobj().assertPageSource(driver, franchiseId);
				if (boolAddFranFlag == true) {
					Reporter.log("Franchise Location addition successfull !!! ");
				} else {
					fc.utobj().throwsException("Franchise Location addition failed !!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsException("Error in adding location , please refer screenshot!");
		}

		return franchiseId;
	}

	public String addFranchiseLocationWithDivision(WebDriver driver, String divisionName, String franchiseID) throws Exception {

		Reporter.log("**************************** Add Franchisee Location *************");

		String testCaseId = "TC_InFoMgr_Admin_FranchiseeLocation_AddLocation_Division_And_FranchiseID_AsParam";
		String franchiseId = "";

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				// Area Region
				AdminAreaRegionAddAreaRegionPageTest addRegion = new AdminAreaRegionAddAreaRegionPageTest();
				String regionName = fc.utobj().generateTestData("TestRegion");
				addRegion.addAreaRegion(driver, regionName);

				// Store Type
				AdminConfigurationConfigureStoreTypePageTest addStoreType = new AdminConfigurationConfigureStoreTypePageTest();
				String storeType = fc.utobj().generateTestData("TestStore");
				addStoreType.addStoreType(driver, storeType);

				// Add Franchise Location

				fc.adminpage().adminAddNewFranchiseLocationLnk(driver);
				AdminFranchiseLocationAddFranchiseLocationPage pobj = new AdminFranchiseLocationAddFranchiseLocationPage(driver);
				franchiseId = fc.utobj().generateTestData(franchiseID);
				fc.utobj().sendKeys(driver, pobj.franchiseeID, franchiseId);
				fc.utobj().sendKeys(driver, pobj.centerName, "0000");
				fc.utobj().selectDropDownByVisibleText(driver, pobj.drpDivisionBrand, divisionName);
				;
				;
				fc.utobj().selectDropDownByVisibleText(driver, pobj.areaRegion, regionName);
				;
				fc.utobj().selectDropDownByVisibleText(driver, pobj.storeTypeId, storeType);

				fc.utobj().sendKeys(driver, pobj.openingDate, fc.utobj().getFutureDateUSFormat(10));
				fc.utobj().sendKeys(driver, pobj.city, "test city");
				;
				fc.utobj().selectDropDownByVisibleText(driver, pobj.stateID, "Alaska");

				fc.utobj().sendKeys(driver, pobj.storePhone, "0001");
				fc.utobj().sendKeys(driver, pobj.ownerFirstName, fc.utobj().generateTestData("firstName"));
				fc.utobj().sendKeys(driver, pobj.ownerLastName, "lastName");
				fc.utobj().clickElement(driver, pobj.submit);

				// Verify that Franchise LOcation is added

				fc.infomgr().infomgr_common().InfoMgrFranchisees(driver);

				InfoMgrFranchiseesPage objFranPage = new InfoMgrFranchiseesPage(driver);
				try {
					fc.utobj().clickElement(driver, objFranPage.lnkShowAll);
				} catch (Exception ex) {

				}

				boolean boolAddFranFlag = fc.utobj().assertPageSource(driver, franchiseId);
				if (boolAddFranFlag == true) {
					Reporter.log("Franchise Location addition successfull !!! ");
				} else {
					fc.utobj().throwsException("Franchise Location addition failed !!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsException("Error in adding location , please refer screenshot!");
		}
		return franchiseId;
	}

	public String addFranchiseLocationWithDivisionRegion(WebDriver driver, String divisionName, String regionName) throws Exception {

		Reporter.log("********************* Add Franchisee Location with Division and Region**************");

		String testCaseId = "TC_InFoMgr_Admin_FranchiseeLocation_AddLocation_Division_And_RegionName_AsParam";
		String franchiseId = "";

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				// Store Type
				AdminConfigurationConfigureStoreTypePageTest addStoreType = new AdminConfigurationConfigureStoreTypePageTest();
				String storeType = fc.utobj().generateTestData("TestStore");
				addStoreType.addStoreType(driver, storeType);

				// Add Franchise Location

				fc.adminpage().adminAddNewFranchiseLocationLnk(driver);
				AdminFranchiseLocationAddFranchiseLocationPage pobj = new AdminFranchiseLocationAddFranchiseLocationPage(driver);
				franchiseId = fc.utobj().generateTestData("TestfranchiseId");
				fc.utobj().sendKeys(driver, pobj.franchiseeID, franchiseId);
				fc.utobj().sendKeys(driver, pobj.centerName, "0000");
				fc.utobj().selectDropDownByVisibleText(driver, pobj.drpDivisionBrand, divisionName);
				;
				;
				fc.utobj().selectDropDownByVisibleText(driver, pobj.areaRegion, regionName);
				;
				fc.utobj().selectDropDownByVisibleText(driver, pobj.storeTypeId, storeType);
				fc.utobj().sendKeys(driver, pobj.openingDate, fc.utobj().getFutureDateUSFormat(10));
				fc.utobj().sendKeys(driver, pobj.city, "test city");
				;
				fc.utobj().selectDropDownByVisibleText(driver, pobj.stateID, "Alaska");

				fc.utobj().sendKeys(driver, pobj.storePhone, "0001");
				fc.utobj().sendKeys(driver, pobj.ownerFirstName, fc.utobj().generateTestData("firstName"));
				fc.utobj().sendKeys(driver, pobj.ownerLastName, "lastName");
				fc.utobj().clickElement(driver, pobj.submit);

				// Verify that Franchise LOcation is added

				fc.infomgr().infomgr_common().InfoMgrFranchisees(driver);

				InfoMgrFranchiseesPage objFranPage = new InfoMgrFranchiseesPage(driver);
				try {
					fc.utobj().clickElement(driver, objFranPage.lnkShowAll);
				} catch (Exception ex) {

				}

				boolean boolAddFranFlag = fc.utobj().assertPageSource(driver, franchiseId);
				if (boolAddFranFlag == true) {
					Reporter.log("Franchise Location addition successfull !!! ");
				} else {
					fc.utobj().throwsException("Franchise Location addition failed !!!");
				}

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsException("Error in adding location , please refer screenshot!");
		}
		return franchiseId;
	}

	public String addFranchiseLocationByDivisionalUser(WebDriver driver, String divisionName, String regionName, String storeType) throws Exception {

		Reporter.log("******************************** Add Franchisee Location *********************");

		String testCaseId = "TC_InFoMgr_Admin_FranchiseeLocation_AddLocation_By_Divisional_User";
		String franchiseId = "";

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.adminpage().adminAddNewFranchiseLocationLnk(driver);
				AdminFranchiseLocationAddFranchiseLocationPage pobj = new AdminFranchiseLocationAddFranchiseLocationPage(driver);
				franchiseId = fc.utobj().generateTestData("TestfranchiseId");
				fc.utobj().sendKeys(driver, pobj.franchiseeID, franchiseId);
				fc.utobj().sendKeys(driver, pobj.centerName, "0000");
				fc.utobj().selectDropDownByVisibleText(driver, pobj.drpDivisionBrand, divisionName);
				;
				;
				fc.utobj().selectDropDownByVisibleText(driver, pobj.areaRegion, regionName);
				;
				fc.utobj().selectDropDownByVisibleText(driver, pobj.storeTypeId, storeType);
				fc.utobj().sendKeys(driver, pobj.openingDate, fc.utobj().getFutureDateUSFormat(10));
				fc.utobj().sendKeys(driver, pobj.city, "test city");
				;
				fc.utobj().selectDropDownByVisibleText(driver, pobj.stateID, "Alaska");

				fc.utobj().sendKeys(driver, pobj.storePhone, "0001");
				fc.utobj().sendKeys(driver, pobj.ownerFirstName, fc.utobj().generateTestData("firstName"));
				fc.utobj().sendKeys(driver, pobj.ownerLastName, "lastName");
				fc.utobj().clickElement(driver, pobj.submit);

				// Verify that Franchise LOcation is added

				fc.infomgr().infomgr_common().InfoMgrFranchisees(driver);

				InfoMgrFranchiseesPage objFranPage = new InfoMgrFranchiseesPage(driver);
				try {
					fc.utobj().clickElement(driver, objFranPage.lnkShowAll);
				} catch (Exception ex) {

				}

				boolean boolAddFranFlag = fc.utobj().assertPageSource(driver, franchiseId);
				if (boolAddFranFlag == true) {
					Reporter.log("Franchise Location creation successfull !!! ");
				} else {
					fc.utobj().throwsException("Franchise Location creation failed !!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsException("Error in adding location , please refer screenshot!");
		}
		return franchiseId;
	}

	public String createFDDTemplate(WebDriver driver) throws Exception {

		Reporter.log("********************************* Email Template **********************");

		String testCaseId = "TC_InFoMgr_Admin_FDDManagement_FDDTemplate_Create";
		String fddEmailTemplate = "";
		if (fc.utobj().validate(testCaseId) == true) {
			try {
				AdminFDDManagementFDDEmailTemplateSummaryAddTemplatePageTest objEmailTemplate = new AdminFDDManagementFDDEmailTemplateSummaryAddTemplatePageTest();
				fddEmailTemplate = objEmailTemplate.configureFDDEmailTemplateSummary(driver, "New FDD Template", "New Subject");
				AdminFDDManagementFDDEmailTemplateSummaryPage objFDDTempSumPage = new AdminFDDManagementFDDEmailTemplateSummaryPage(driver);
				try {
					fc.utobj().clickElement(driver, objFDDTempSumPage.lnkShowAll);
				} catch (Exception ex) {
					// Reporter.log("Show All link not found");
				}
				boolean boolFDDTemplate = fc.utobj().assertPageSource(driver, fddEmailTemplate);

				if (boolFDDTemplate == true) {
					Reporter.log("FDD Template Creation Successfull !!!");
				} else {
					fc.utobj().throwsException("FDD Template Creation Failed !!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsException("Error in creating FDD Template , please refer screenshot!");
		}
		return fddEmailTemplate;
	}

	public void NavigateToManageFormGeneratorTabs(WebDriver driver, String tabText) throws Exception {
		Reporter.log("*************** Navigating to Manage Form Generator page ******");
		AdminInfoMgrManageFormGeneratorPage manageFormGeneratorPage = new AdminInfoMgrManageFormGeneratorPage(driver);
		fc.utobj().clickElement(driver, manageFormGeneratorPage.btnContinue);
		fc.utobj().clickPartialLinkText(driver, tabText);
	}

	public void VerifyAllFormsPreview(WebDriver driver) throws Exception {
		Reporter.log("*********** Verifying All forms preview ******************");

		String testCaseId = "TC_InFoMgr_Admin_ManageFormGenerator_Forms_Preview";

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				AdminInfoMgrManageFormGeneratorPage manageFormGeneratorPage = new AdminInfoMgrManageFormGeneratorPage(driver);
				Select drpFormNames = new Select(manageFormGeneratorPage.drpFormNames);
				List<WebElement> lstForms = drpFormNames.getOptions();
				String formName = "";
				String currentWindow = driver.getWindowHandle();

				for (int i = 0; i < lstForms.size(); i++) {
					lstForms = drpFormNames.getOptions();
					if (!(lstForms.get(i).getText().trim().equals("All"))) {
						formName = lstForms.get(i).getText();
						lstForms.get(i).click();
						fc.utobj().sleep();
						try {
							fc.utobj().clickLink(driver, "Preview Form");
						} catch (Exception e) {
							continue;
						}
						Set<String> windows = driver.getWindowHandles();
						for (String window : windows) {
							if (window != currentWindow) {
								driver.switchTo().window(window);
								break;
							}
						}
						// Verify that Print preview form contains form name

						boolean isFormNameDisplayed = fc.utobj().assertPageSource(driver, formName);
						if (isFormNameDisplayed) {
							driver.switchTo().window(currentWindow);
						} else {
							fc.utobj().throwsException("Preview form couldn't load properly. Test case fails!!!");
						}
					}
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsException("Error in verifying preview , please refer screenshot!");
		}
	}

	public void VerifyPreviewAfterModifyFieldsPosition(WebDriver driver, String formName) throws Exception {
		Reporter.log("*********** Verifying forms preview after Modify Fields Position ******************");

		String testCaseId = "TC_InFoMgr_Admin_ManageFormGenerator_Modify_Fields_Position_And_Preview";
		String currentWindow = "";

		if (fc.utobj().validate(testCaseId) == true) {
			try {

				AdminInfoMgrManageFormGeneratorPage manageFormGeneratorPage = new AdminInfoMgrManageFormGeneratorPage(driver);

				fc.utobj().selectDropDown(driver, manageFormGeneratorPage.drpFormNames, formName);

				fc.utobj().clickLink(driver, "Modify Fields Position");

				AdminInfoMgrManageFormGeneratorModifyFieldsPositionPage objModifyFieldsPosition = new AdminInfoMgrManageFormGeneratorPage(driver).getModifyFieldsPositionPage();

				// Drag and Drop the fields

				fc.utobj().dragAndDrop(driver, objModifyFieldsPosition.tabAgreement, objModifyFieldsPosition.tabDateExecuted);

				currentWindow = driver.getWindowHandle();

				// Click on Preview button
				fc.utobj().clickElement(driver, objModifyFieldsPosition.btnPreview);

				Set<String> lstWindows = driver.getWindowHandles();

				for (String window : lstWindows) {
					if (!window.equals(currentWindow)) {
						driver.switchTo().window(window);
						break;
					}
				}

				boolean isFormNameDisplayed = fc.utobj().assertPageSource(driver, formName);
				if (isFormNameDisplayed) {
					driver.close();
					driver.switchTo().window(currentWindow);
					Reporter.log("Preview form is displayed properly ");
				} else {
					fc.utobj().throwsException("Preview form couldn't load properly. Test case fails!!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsException("Error in verifying preview , please refer screenshot!");
		}
	}

	/*
	 * public String addCorporateUser(WebDriver driver , Map<String , String>
	 * config) throws ParserConfigurationException, SAXException, IOException,
	 * Exception { Reporter.log(
	 * "************************* Add Corporate User *************");
	 * 
	 * String testCaseId = "TC_InFoMgr_Admin_Users_Add_CorporateUser"; String
	 * corporateUser =fc.utobj().generateTestData("Test_CorpUser1");
	 * 
	 * if(fc.utobj().validate(config,testCaseId)==true) { try{
	 * AdminUsersManageCorporateUsersAddCorporateUserPageTest adminUserPage =
	 * new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
	 * 
	 * String emailId="infomgrautomation@staffex.com";
	 * 
	 * adminUserPage.createDefaultUser(driver , corpUser);
	 * fc.utobj().printTestResultExcel(config, testCaseId, null, null,"Passed",
	 * null, null); } catch(Exception e) {
	 * fc.utobj().quitBrowserOnCatchBasicMethod(driver, config, e, testCaseId);
	 * }
	 * 
	 * } else{ fc.utobj().throwsException(
	 * "Error in adding corporate user , please refer screenshot!"); } return
	 * corporateUser; }
	 */

	public String addCorporateLocation(WebDriver driver, Map<String, String> config) throws Exception {

		Reporter.log("********************* Add  Corporate Location ********************* \n");

		String testCaseId = "TC_InFoMgr_Admin_CorporateLocation_DefaultValues";

		String franchiseId = fc.utobj().generateTestData("TestCorpLocation");

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				String regionName = addAreaRegion(driver);

				String storeType = addStoreType(driver);

				fc.adminpage().adminAddNewFranchiseLocationLnk(driver);
				AdminFranchiseLocationAddFranchiseLocationPage pobj = new AdminFranchiseLocationAddFranchiseLocationPage(driver);
				fc.utobj().sendKeys(driver, pobj.franchiseeID, franchiseId);
				fc.utobj().sendKeys(driver, pobj.centerName, "0000");
				;
				fc.utobj().selectDropDownByVisibleText(driver, pobj.areaRegion, regionName);
				;
				fc.utobj().selectDropDownByVisibleText(driver, pobj.storeTypeId, storeType);
				fc.utobj().selectDropDownByVisibleText(driver, pobj.status, "Yes");
				fc.utobj().sendKeys(driver, pobj.openingDate, fc.utobj().getFutureDateUSFormat(30));
				fc.utobj().sendKeys(driver, pobj.city, "test city");
				fc.utobj().selectDropDownByVisibleText(driver, pobj.countryID, "USA");
				fc.utobj().selectDropDownByVisibleText(driver, pobj.stateID, "Alaska");
				fc.utobj().sendKeys(driver, pobj.emailID, "Automationtest@gmail.com");
				fc.utobj().sendKeys(driver, pobj.storePhone, "0001");
				fc.utobj().sendKeys(driver, pobj.ownerFirstName, fc.utobj().generateTestData("firstName"));
				fc.utobj().sendKeys(driver, pobj.ownerLastName, "lastName");
				fc.utobj().clickElement(driver, pobj.submit);

				// Verify that Franchise Location is added

				fc.infomgr().infomgr_common().InfoMgrCorporateLocations(driver);

				InfoMgrCorporateLocationsPage objFranPage = new InfoMgrCorporateLocationsPage(driver);
				try {
					fc.utobj().clickElement(driver, objFranPage.lnkShowAll);
				} catch (Exception ex) {

				}

				boolean boolAddFranFlag = fc.utobj().assertPageSource(driver, franchiseId);
				if (boolAddFranFlag == true) {
					Reporter.log("Corporate Location addition successfull !!! ");
				} else {
					fc.utobj().throwsException("Corporate Location addition failed !!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsException("Error in adding location , please refer screenshot!");
		}
		return franchiseId;
	}

	// Method is pending
	public void configureAllFieldsForFranchisees(WebDriver driver, Map<String, String> config) throws Exception {
		String formsStr = "Agreement Center Info Contract Signing Customer Complaints Default and Termination Employees Entity Details  Events  Financial   Guarantors Insurance   Legal Violation   Lenders  Marketing  Mystery Review  Owners  Real Estate  Renewal  Territory  Training  Transfer  call  Remark  Task ";

		String testCaseId = "TC_InFoMgr_Admin_ConfigureFields_For_Franchisees_Forms";

		AdminInfoMgrManageFormGeneratorPage manageFormGeneratorPage = new AdminInfoMgrManageFormGeneratorPage(driver);

		fc.infomgr().infomgr_common().AdminFIMManageFormGenerator(driver);

		NavigateToManageFormGeneratorTabs(driver, "Franchisee");

		if (fc.utobj().configurationTestCaseToExecute(testCaseId)) {
			try {

				List<WebElement> lstOptions = new Select(manageFormGeneratorPage.drpFormNames).getOptions();
				for (int i = 0; i < lstOptions.size(); i++) {
					lstOptions = new Select(manageFormGeneratorPage.drpFormNames).getOptions();

					if (formsStr.contains(lstOptions.get(i).getText())) {
						fc.utobj().clickElement(driver, lstOptions.get(i));
						fc.utobj().sleep();

						// Activate country and state - Begin
						List<WebElement> elementsState = manageFormGeneratorPage.lnkDeactiveStates;
						if (elementsState.size() > 0) {
							List<WebElement> elementsCountry = manageFormGeneratorPage.lnkDeactiveCountries;
							if (elementsCountry.size() > 0) {
								for (WebElement element : elementsCountry) {
									fc.utobj().clickElement(driver, element);
								}
							}
							for (WebElement element : elementsState) {
								fc.utobj().clickElement(driver, element);
							}
						}
						// Activate country and state - End

						List<WebElement> activeLinks = manageFormGeneratorPage.lnkAllActive;
						int size = activeLinks.size();

						for (int j = 0; j < size; j++) {

							fc.utobj().clickElement(driver, activeLinks.get(0));

						}
					}
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		}
	}

	public void configureAllFieldsForRegional(WebDriver driver, Map<String, String> config) throws Exception {
		String formsStr = "Agreement Area Info Area Owner Contract Signing Entity Details Events Financial Guarantors Insurance Legal Violation Lenders Marketing Mystery Review Other Addresses QA History Real Estate Renewal Territory Training Call Remark Task";
		String testCaseId = "TC_InFoMgr_Admin_ConfigureFields_For_Regional_Forms";

		AdminInfoMgrManageFormGeneratorPage manageFormGeneratorPage = new AdminInfoMgrManageFormGeneratorPage(driver);

		fc.infomgr().infomgr_common().AdminFIMManageFormGenerator(driver);

		NavigateToManageFormGeneratorTabs(driver, "Regional");

		if (fc.utobj().configurationTestCaseToExecute(testCaseId)) {
			try {

				List<WebElement> lstOptions = new Select(manageFormGeneratorPage.drpFormNames).getOptions();
				for (int i = 0; i < lstOptions.size(); i++) {
					lstOptions = new Select(manageFormGeneratorPage.drpFormNames).getOptions();

					if (formsStr.contains(lstOptions.get(i).getText())) {
						fc.utobj().clickElement(driver, lstOptions.get(i));
						fc.utobj().sleep();

						// Activate country and state - Begin
						List<WebElement> elementsState = manageFormGeneratorPage.lnkDeactiveStates;
						if (elementsState.size() > 0) {
							List<WebElement> elementsCountry = manageFormGeneratorPage.lnkDeactiveCountries;
							if (elementsCountry.size() > 0) {
								for (WebElement element : elementsCountry) {
									fc.utobj().clickElement(driver, element);
								}
							}
							for (WebElement element : elementsState) {
								fc.utobj().clickElement(driver, element);
							}
						}
						// Activate country and state - End
						List<WebElement> activeLinks = manageFormGeneratorPage.lnkAllActive;
						int size = activeLinks.size();

						for (int j = 0; j < size; j++) {

							fc.utobj().clickElement(driver, activeLinks.get(0));

						}
					}
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		}
	}

	public void configureAllFieldsForMultiUnit(WebDriver driver, Map<String, String> config) throws Exception {
		String formsStr = "Agreement Contract Signing Events Financial Guarantors Insurance Legal Violation Lenders Marketing Mystery Review QA History Real Estate Renewal Territory Call Remark Task";
		String testCaseId = "TC_InFoMgr_Admin_ConfigureFields_For_MultiUnit_Forms";

		AdminInfoMgrManageFormGeneratorPage manageFormGeneratorPage = new AdminInfoMgrManageFormGeneratorPage(driver);

		fc.infomgr().infomgr_common().AdminFIMManageFormGenerator(driver);

		NavigateToManageFormGeneratorTabs(driver, "Multi-Unit / Entity");

		if (fc.utobj().configurationTestCaseToExecute(testCaseId)) {
			try {

				List<WebElement> lstOptions = new Select(manageFormGeneratorPage.drpFormNames).getOptions();
				for (int i = 0; i < lstOptions.size(); i++) {
					lstOptions = new Select(manageFormGeneratorPage.drpFormNames).getOptions();

					if (formsStr.contains(lstOptions.get(i).getText())) {
						fc.utobj().clickElement(driver, lstOptions.get(i));
						fc.utobj().sleep();

						// Activate country and state - Begin
						List<WebElement> elementsState = manageFormGeneratorPage.lnkDeactiveStates;
						if (elementsState.size() > 0) {
							List<WebElement> elementsCountry = manageFormGeneratorPage.lnkDeactiveCountries;
							if (elementsCountry.size() > 0) {
								for (WebElement element : elementsCountry) {
									fc.utobj().clickElement(driver, element);
								}
							}
							for (WebElement element : elementsState) {
								fc.utobj().clickElement(driver, element);
							}
						}
						// Activate country and state - End

						List<WebElement> activeLinks = manageFormGeneratorPage.lnkAllActive;
						int size = activeLinks.size();

						for (int j = 0; j < size; j++) {

							fc.utobj().clickElement(driver, activeLinks.get(0));

						}
					}
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		}
	}

	private void configureInfoMgrTabs(WebDriver driver, Map<String, String> config) throws Exception {
		String testCaseId = "TC_InFoMgr_Admin_ConfigureTabs";

		fc.infomgr().infomgr_common().adminConfigureInfoMgrTabs(driver);

		AdminInfoMgrConfigureInfoMgrTabsPage configureInfoMgrTabsPage = new AdminInfoMgrConfigureInfoMgrTabsPage(driver);

		if (fc.utobj().configurationTestCaseToExecute(testCaseId)) {
			try {
				fc.utobj().check(configureInfoMgrTabsPage.chkSelectAll);
				fc.utobj().clickElement(driver, configureInfoMgrTabsPage.btnSave);
				driver.switchTo().alert().accept();
				fc.utobj().switchFrameToDefault(driver);
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		}
	}

	public void configureAreaRegionLabels(WebDriver driver) throws Exception {
		String testCaseId = "TC_InFoMgr_Admin_AreaRegionLabel";

		fc.infomgr().infomgr_common().adminConfigureAreaRegionLabel(driver);

		AdminConfigurartionConfigureAreaRegionLabelPage configureAreaRegionPage = new AdminConfigurartionConfigureAreaRegionLabelPage(driver);

		if (fc.utobj().configurationTestCaseToExecute(testCaseId)) {
			try {
				fc.utobj().clickElement(driver, configureAreaRegionPage.btnModify);
				fc.utobj().sendKeys(driver, configureAreaRegionPage.txtAreaRegion, "Area / Region");
				fc.utobj().clickElement(driver, configureAreaRegionPage.btnSave);
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		}
	}

	public void configureTabularViewInfoMgrFranchiseesAllForms(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("********************* Configure Tabular View in Info Mgr Franchisees Forms ********************* \n");

		String formsStr = "Customer Complaints Default and termination Events Guarantors Insurance Legal Violation Lenders Mystry review Renewal Training Transfer";
		String testCaseId = "TC_InFoMgr_Admin_ManagetFormGenerator_Franchisees_Configure_Tabular_View_For_All_Forms";

		fc.infomgr().infomgr_common().AdminFIMManageFormGenerator(driver);
		NavigateToManageFormGeneratorTabs(driver, "Franchisee");

		AdminInfoMgrManageFormGeneratorPage objAdminManageFormGen = new AdminInfoMgrManageFormGeneratorPage(driver);

		AdminInfoMgrManageFormGeneratorConfigureTabularViewPage objConfigureTabularView = objAdminManageFormGen.getConfigureTabularViewPage();

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				// Remove tabular view from the forms listed in formsStr
				List<WebElement> lstOptions = new Select(objAdminManageFormGen.drpFormNames).getOptions();
				for (int i = 0; i < lstOptions.size(); i++) {
					lstOptions = new Select(objAdminManageFormGen.drpFormNames).getOptions();
					if (formsStr.contains(lstOptions.get(i).getText())) {
						fc.utobj().clickElement(driver, lstOptions.get(i));
						fc.utobj().sleep();
						fc.utobj().clickElement(driver, objAdminManageFormGen.btnConfgTabularView);
						fc.commonMethods().switch_cboxIframe_frameId(driver);
						int countFileds = fc.utobj().listBoxSelectAllValues(objConfigureTabularView.lstSelectedFields);
						if (countFileds > 0) {
							fc.utobj().clickElement(driver, objConfigureTabularView.imgRemoveFields);
						}
						fc.utobj().clickElement(driver, objConfigureTabularView.btnSave);
						fc.utobj().switchFrameToDefault(driver);
					}
				}

				// Configure tabular view for employees
				ArrayList<String> lstFields = new ArrayList<String>();
				lstFields.add("First Name");
				lstFields.add("Last Name");
				lstFields.add("City");
				lstFields.add("Country");
				lstFields.add("Fax");

				fc.utobj().selectDropDownByVisibleText(driver, objAdminManageFormGen.drpFormNames, "Employees");
				fc.utobj().clickElement(driver, objAdminManageFormGen.btnConfgTabularView);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				int countFileds = fc.utobj().listBoxSelectAllValues(objConfigureTabularView.lstSelectedFields);
				if (countFileds > 0) {
					fc.utobj().clickElement(driver, objConfigureTabularView.imgRemoveFields);
				}

				// Select the values as passed in the parameter
				fc.utobj().listBoxSelectValues(lstFields, objConfigureTabularView.lstFieldsTabularView);
				fc.utobj().clickElement(driver, objConfigureTabularView.imgAddFields);
				fc.utobj().clickElement(driver, objConfigureTabularView.btnSave);
				fc.utobj().switchFrameToDefault(driver);
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsException("Error in configuring tabular view for franchisees forms , please refer screenshot!");
		}
	}

	public void configureTabularViewInfoMgrRegionalAllForms(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("********************* Configure Tabular View in Info Mgr > Regional Forms ********************* \n");

		String formsStr = "Events Guarantors Insurance Legal Violation Lenders Mystry review Other Addresses Renewal training";
		String testCaseId = "TC_InFoMgr_Admin_ManagetFormGenerator_Regional_Configure_Tabular_View_For_All_Forms";

		fc.infomgr().infomgr_common().AdminFIMManageFormGenerator(driver);
		NavigateToManageFormGeneratorTabs(driver, "Regional");

		AdminInfoMgrManageFormGeneratorPage objAdminManageFormGen = new AdminInfoMgrManageFormGeneratorPage(driver);

		AdminInfoMgrManageFormGeneratorConfigureTabularViewPage objConfigureTabularView = objAdminManageFormGen.getConfigureTabularViewPage();

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				// Remove tabular view from the forms listed in formsStr
				List<WebElement> lstOptions = new Select(objAdminManageFormGen.drpFormNames).getOptions();
				for (int i = 0; i < lstOptions.size(); i++) {
					lstOptions = new Select(objAdminManageFormGen.drpFormNames).getOptions();
					if (formsStr.contains(lstOptions.get(i).getText())) {
						fc.utobj().clickElement(driver, lstOptions.get(i));
						fc.utobj().sleep();
						fc.utobj().clickElement(driver, objAdminManageFormGen.btnConfgTabularView);
						fc.commonMethods().switch_cboxIframe_frameId(driver);
						int countFileds = fc.utobj().listBoxSelectAllValues(objConfigureTabularView.lstSelectedFields);
						if (countFileds > 0) {
							fc.utobj().clickElement(driver, objConfigureTabularView.imgRemoveFields);
						}
						fc.utobj().clickElement(driver, objConfigureTabularView.btnSave);
						fc.utobj().switchFrameToDefault(driver);
					}
				}

				// Configure tabular view for Area Owner
				ArrayList<String> lstFieldsAreaOwner = new ArrayList<String>();
				lstFieldsAreaOwner.add("First Name");
				lstFieldsAreaOwner.add("Last Name");
				lstFieldsAreaOwner.add("Street Address");
				fc.utobj().selectDropDownByVisibleText(driver, objAdminManageFormGen.drpFormNames, "Area Owner");
				fc.utobj().clickElement(driver, objAdminManageFormGen.btnConfgTabularView);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				int countFileds = fc.utobj().listBoxSelectAllValues(objConfigureTabularView.lstSelectedFields);
				if (countFileds > 0) {
					fc.utobj().clickElement(driver, objConfigureTabularView.imgRemoveFields);
				}

				// Select the values as passed in the parameter
				fc.utobj().listBoxSelectValues(lstFieldsAreaOwner, objConfigureTabularView.lstFieldsTabularView);
				fc.utobj().clickElement(driver, objConfigureTabularView.imgAddFields);
				fc.utobj().clickElement(driver, objConfigureTabularView.btnSave);
				fc.utobj().switchFrameToDefault(driver);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsException("Error in configuring tabular view for Regional forms , please refer screenshot!");
		}
	}

	public void configureTabularViewInfoMgrMultiUnitAllForms(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("********************* Configure Tabular View in Info Mgr > Multi Unit Forms ********************* \n");

		String formsStr = "Events Guarantors Insurance Legal Violation Lenders Mystry review Renewal training";
		String testCaseId = "TC_InFoMgr_Admin_ManagetFormGenerator_MultiUnit_Configure_Tabular_View_For_All_Forms";

		fc.infomgr().infomgr_common().AdminFIMManageFormGenerator(driver);
		NavigateToManageFormGeneratorTabs(driver, "Multi-Unit / Entity");

		AdminInfoMgrManageFormGeneratorPage objAdminManageFormGen = new AdminInfoMgrManageFormGeneratorPage(driver);

		AdminInfoMgrManageFormGeneratorConfigureTabularViewPage objConfigureTabularView = objAdminManageFormGen.getConfigureTabularViewPage();

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				// Remove tabular view from the forms listed in formsStr
				List<WebElement> lstOptions = new Select(objAdminManageFormGen.drpFormNames).getOptions();
				for (int i = 0; i < lstOptions.size(); i++) {
					lstOptions = new Select(objAdminManageFormGen.drpFormNames).getOptions();
					if (formsStr.contains(lstOptions.get(i).getText())) {
						fc.utobj().clickElement(driver, lstOptions.get(i));
						fc.utobj().sleep();
						fc.utobj().clickElement(driver, objAdminManageFormGen.btnConfgTabularView);
						fc.commonMethods().switch_cboxIframe_frameId(driver);
						int countFileds = fc.utobj().listBoxSelectAllValues(objConfigureTabularView.lstSelectedFields);
						if (countFileds > 0) {
							fc.utobj().clickElement(driver, objConfigureTabularView.imgRemoveFields);
						}
						fc.utobj().clickElement(driver, objConfigureTabularView.btnSave);
						fc.utobj().switchFrameToDefault(driver);
					}
				}

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsException("Error in configuring tabular view for Regional forms , please refer screenshot!");
		}
	}

	public void configureInfoMgr(WebDriver driver, Map<String, String> config) throws Exception {
		String testCaseId = "TC_Configure_InFoMgr";
		if (fc.utobj().configurationTestCaseToExecute(testCaseId) == true) {
			try {
				configureAllFieldsForFranchisees(driver, config);

				configureAllFieldsForRegional(driver, config);

				configureAllFieldsForMultiUnit(driver, config);

				configureAreaRegionLabels(driver);

				configureInfoMgrTabs(driver, config);

				configureTabularViewInfoMgrFranchiseesAllForms(driver, config);

				configureTabularViewInfoMgrRegionalAllForms(driver, config);

				configureTabularViewInfoMgrMultiUnitAllForms(driver, config);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsException("Error in configuring infoMgr , please refer screenshot!");
		}
	}

	public InfoMgrFranchiseeFilter addFranchiseLocationForFilter(WebDriver driver, boolean isCorporateLocation) throws Exception {
		Reporter.log("********************* Add  Franchisee Location for filter********************* \n");

		String testCaseId = "TC_InFoMgr_Admin_FranchiseeLocation_AddLocation_For_Filter";
		InfoMgrFranchiseeFilter franchisee = new InfoMgrFranchiseeFilter();
		WebDriverWait wait = new WebDriverWait(driver, 60);

		String regionName = addAreaRegion(driver);

		String divisionName = addDivisionName(driver);

		String storeType = addStoreType(driver);

		franchisee.setStatusID("Active");
		franchisee.setStoreStatus("Operational");
		franchisee.setCenterName(fc.utobj().generateTestData("centerName"));
		franchisee.setStoreTypeID(storeType);
		franchisee.setFranchiseeID(fc.utobj().generateTestData("franchiseID"));
		franchisee.setDivisionID(divisionName);
		franchisee.setPhone(fc.utobj().generateRandomNumber());
		franchisee.setOwnerType("Managing Owner");
		franchisee.setOwnerFirstName(fc.utobj().generateTestData("Owner First Name"));
		franchisee.setOwnerLastName(fc.utobj().generateTestData("Owner Last Name"));
		franchisee.setAreaRegionName(regionName);
		franchisee.setCountry("USA");
		franchisee.setState("Alabama");
		franchisee.setCity("Demo City");
		franchisee.setZipCode("123456");
		franchisee.setEmailID("test@mail.com");
		franchisee.setContactFirstName(fc.utobj().generateTestData("contact FirstName"));
		franchisee.setContactLastName(fc.utobj().generateTestData("contactLastName"));

		if (fc.utobj().validate(testCaseId) == true) {
			try {

				fc.adminpage().adminAddNewFranchiseLocationLnk(driver);

				AdminFranchiseLocationAddFranchiseLocationPage pobj = new AdminFranchiseLocationAddFranchiseLocationPage(driver);

				fc.utobj().sendKeys(driver, pobj.franchiseeID, franchisee.getFranchiseeID());
				fc.utobj().sendKeys(driver, pobj.centerName, franchisee.getCenterName());

				fc.utobj().selectDropDownByVisibleText(driver, pobj.areaRegion, franchisee.getAreaRegionName());

				fc.utobj().selectDropDownByVisibleText(driver, pobj.drpDivisionBrand, franchisee.getDivisionID());

				fc.utobj().selectDropDownByVisibleText(driver, pobj.storeTypeId, franchisee.getStoreTypeID());

				fc.utobj().sendKeys(driver, pobj.openingDate, fc.utobj().getFutureDateUSFormat(30));
				fc.utobj().sendKeys(driver, pobj.city, franchisee.getCity());

				fc.utobj().selectDropDownByVisibleText(driver, pobj.stateID, franchisee.getState());

				fc.utobj().sendKeys(driver, pobj.storePhone, franchisee.getPhone());
				fc.utobj().sendKeys(driver, pobj.storeEmail, franchisee.getEmailID());
				fc.utobj().sendKeys(driver, pobj.zipcode, franchisee.getZipCode());
				fc.utobj().sendKeys(driver, pobj.contactFirstName, franchisee.getContactFirstName());
				fc.utobj().sendKeys(driver, pobj.contactLastName, franchisee.getContactLastName());
				fc.utobj().sendKeys(driver, pobj.emailID, franchisee.getEmailID());// Email
																					// ID
																					// for
																					// mail
				fc.utobj().sendKeys(driver, pobj.ownerFirstName, franchisee.getOwnerFirstName());
				fc.utobj().sendKeys(driver, pobj.ownerLastName, franchisee.getOwnerLastName());
				if (isCorporateLocation) {
					fc.utobj().selectDropDownByVisibleText(driver, pobj.status, "Yes");
				}

				fc.utobj().clickElement(driver, pobj.submit);

				if (isCorporateLocation) {
					fc.infomgr().infomgr_common().InfoMgrCorporateLocations(driver);
				} else {
					fc.infomgr().infomgr_common().InfoMgrFranchisees(driver);

				}

				InfoMgrFranchiseesPage objFranPage = new InfoMgrFranchiseesPage(driver);
				try {
					fc.utobj().clickElement(driver, objFranPage.lnkShowAll);
				} catch (Exception ex) {

				}

				boolean boolAddFranFlag = fc.utobj().assertPageSource(driver, franchisee.getFranchiseeID());
				if (boolAddFranFlag == true) {
					Reporter.log("Franchise Location addition successfull !!! ");
				} else {
					fc.utobj().throwsException("Franchise Location addition failed !!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsException("Error in adding location , please refer screenshot!");
		}
		return franchisee;
	}

	public Map<String, String> configure_Franchisees_AddToCenterInfo_AllForms(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("***************************************** Add Fields to Center Info *****************");
		String formsStr = "Agreement Contract Signing Customer Complaints Default and Termination Employees Entity Details  Events  Financial   Guarantors Insurance   Legal Violation   Lenders  Marketing  Mystery Review  Owners  Real Estate  Renewal  Territory  Training  Transfer";

		String testCaseId = "TC_Admin_InfoMgr_ManageFormGenerator_Franchisees_Add_To_Center_Info";

		AdminInfoMgrManageFormGeneratorPage manageFormGeneratorPage = new AdminInfoMgrManageFormGeneratorPage(driver);

		Map<String, String> dsFormsFileds = fc.utobj().readTestData("infomgr", testCaseId);

		fc.infomgr().infomgr_common().AdminFIMManageFormGenerator(driver);

		NavigateToManageFormGeneratorTabs(driver, fc.utobj().translateString("Franchisee"));

		if (fc.utobj().configurationTestCaseToExecute(testCaseId)) {
			try {

				List<WebElement> lstOptions = new Select(manageFormGeneratorPage.drpFormNames).getOptions();
				for (int i = 0; i < lstOptions.size(); i++) {
					lstOptions = new Select(manageFormGeneratorPage.drpFormNames).getOptions();
					String formName = lstOptions.get(i).getText();
					if (formsStr.contains(lstOptions.get(i).getText())) {
						fc.utobj().clickElement(driver, lstOptions.get(i));
						fc.utobj().sleep();

						List<WebElement> element = driver.findElements(By.xpath(".//*[@id='siteMainTable']//td[contains(text(), '" + dsFormsFileds.get(formName) + "')]/ancestor::tr[1]//img[@title='Click to add to Center Info']"));
						if (element.size() > 0)
							fc.utobj().clickElement(driver, element.get(0));
					}
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsException("Error in add to center info , please refer screenshot!");
		}
		return dsFormsFileds;
	}

	public Map<String, String> ManageFormGenerator_Add_Section_Fields_To_Forms(WebDriver driver, Map<String, String> config, String tabName, String formName, String sectionNamePrefix) throws Exception {
		Reporter.log("***************************************** Add Fields to form " + formName + " *****************");

		String testCaseId = "TC_Admin_InfoMgr_ManageFormGenerator_Franchisees_Add_Fields";

		WebDriverWait wait = new WebDriverWait(driver, 60);
		AdminInfoMgrManageFormGeneratorTabDetailsPage tabDetailsPage = new AdminInfoMgrManageFormGeneratorTabDetailsPage(driver);

		fc.infomgr().infomgr_common().AdminFIMManageFormGenerator(driver);
		NavigateToManageFormGeneratorTabs(driver, tabName);

		Map<String, String> lstFields = new HashMap<String, String>();
		String SectionName = sectionNamePrefix + fc.utobj().generateTestData("Section1");
		lstFields.put("txtField", sectionNamePrefix + fc.utobj().generateTestData("txtField") + fc.utobj().generateRandomNumber());
		lstFields.put("txtAreaField", sectionNamePrefix + fc.utobj().generateTestData("txtAreaField") + fc.utobj().generateRandomNumber());
		lstFields.put("dateField", sectionNamePrefix + fc.utobj().generateTestData("dateField") + fc.utobj().generateRandomNumber());
		lstFields.put("dropdownField", sectionNamePrefix + fc.utobj().generateTestData("dropdownField") + fc.utobj().generateRandomNumber());
		lstFields.put("drpOption1", sectionNamePrefix + fc.utobj().generateTestData("drpOption1") + fc.utobj().generateRandomNumber());
		lstFields.put("drpOption2", sectionNamePrefix + fc.utobj().generateTestData("drpOption2") + fc.utobj().generateRandomNumber());
		lstFields.put("rdoField", sectionNamePrefix + fc.utobj().generateTestData("rdoField") + fc.utobj().generateRandomNumber());
		lstFields.put("rdoOption1", sectionNamePrefix + fc.utobj().generateTestData("rdoOption1") + fc.utobj().generateRandomNumber());
		lstFields.put("rdoOption2", sectionNamePrefix + fc.utobj().generateTestData("rdoOption2") + fc.utobj().generateRandomNumber());
		lstFields.put("chkboxField", sectionNamePrefix + fc.utobj().generateTestData("chkboxField") + fc.utobj().generateRandomNumber());
		lstFields.put("chkboxOption1", sectionNamePrefix + fc.utobj().generateTestData("chkboxOption1") + fc.utobj().generateRandomNumber());
		lstFields.put("chkboxOption2", sectionNamePrefix + fc.utobj().generateTestData("chkboxOption2") + fc.utobj().generateRandomNumber());
		lstFields.put("mulSelectDrpField", sectionNamePrefix + fc.utobj().generateTestData("mulSelectDrpField") + fc.utobj().generateRandomNumber());
		lstFields.put("mulSelectdrpOption1", sectionNamePrefix + fc.utobj().generateTestData("mulSelectDrpOption1") + fc.utobj().generateRandomNumber());
		lstFields.put("mulSelectdrpOption2", sectionNamePrefix + fc.utobj().generateTestData("mulSelectDrpOption2") + fc.utobj().generateRandomNumber());
		lstFields.put("numericField", sectionNamePrefix + fc.utobj().generateRandomNumber());
		lstFields.put("documentField", sectionNamePrefix + fc.utobj().generateTestData("documentField") + fc.utobj().generateRandomNumber());
		lstFields.put("sectionName", SectionName);

		if (fc.utobj().validate(testCaseId)) {
			try {
				fc.utobj().clickLink(driver, formName);

				fc.utobj().clickElement(driver, tabDetailsPage.btnAddSection);

				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().sendKeys(driver, tabDetailsPage.txtSectionName, SectionName);
				fc.utobj().clickElement(driver, tabDetailsPage.btnAddSectionName);
				fc.utobj().switchFrameToDefault(driver);

				WebElement btnAddField = fc.utobj().getElementByXpath(driver, ".//*[@id='siteMainTable']//*[text()='" + SectionName + "']//following::a[text()='Add New Field']");
				fc.utobj().clickElement(driver, btnAddField);

				Reporter.log("**************************Add Text field  and add to center info***********");
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().sendKeys(driver, tabDetailsPage.txtFieldName, lstFields.get("txtField"));
				fc.utobj().selectDropDownByVisibleText(driver, tabDetailsPage.drpFieldType, "Text");
				fc.utobj().clickElement(driver, tabDetailsPage.btnSumbitField);
				fc.utobj().switchFrameToDefault(driver);
				btnAddField = fc.utobj().getElementByXpath(driver, ".//*[@id='siteMainTable']//*[text()='" + SectionName + "']//following::a[text()='Add New Field']");
				fc.utobj().clickElement(driver, btnAddField);

				Reporter.log("**************************Add Text Area field ***********");
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().sendKeys(driver, tabDetailsPage.txtFieldName, lstFields.get("txtAreaField"));
				fc.utobj().selectDropDownByVisibleText(driver, tabDetailsPage.drpFieldType, "Text Area");
				fc.utobj().clickElement(driver, tabDetailsPage.btnSumbitField);
				fc.utobj().switchFrameToDefault(driver);
				btnAddField = fc.utobj().getElementByXpath(driver, ".//*[@id='siteMainTable']//*[text()='" + SectionName + "']//following::a[text()='Add New Field']");
				fc.utobj().clickElement(driver, btnAddField);

				Reporter.log("**************************Add Date field ***********");
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().sendKeys(driver, tabDetailsPage.txtFieldName, lstFields.get("dateField"));
				fc.utobj().selectDropDownByVisibleText(driver, tabDetailsPage.drpFieldType, "Date");
				fc.utobj().clickElement(driver, tabDetailsPage.btnSumbitField);
				fc.utobj().switchFrameToDefault(driver);
				btnAddField = fc.utobj().getElementByXpath(driver, ".//*[@id='siteMainTable']//*[text()='" + SectionName + "']//following::a[text()='Add New Field']");
				fc.utobj().clickElement(driver, btnAddField);

				Reporter.log("**************************Add drop down field ***********");
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().sendKeys(driver, tabDetailsPage.txtFieldName, lstFields.get("dropdownField"));
				fc.utobj().selectDropDownByVisibleText(driver, tabDetailsPage.drpFieldType, "Drop-down");
				fc.utobj().sendKeys(driver, tabDetailsPage.option1, lstFields.get("drpOption1"));
				fc.utobj().clickLink(driver, "Add Option");
				fc.utobj().sendKeys(driver, tabDetailsPage.option2, lstFields.get("drpOption2"));
				fc.utobj().clickElement(driver, tabDetailsPage.btnSumbitField);
				fc.utobj().switchFrameToDefault(driver);
				btnAddField = fc.utobj().getElementByXpath(driver, ".//*[@id='siteMainTable']//*[text()='" + SectionName + "']//following::a[text()='Add New Field']");
				fc.utobj().clickElement(driver, btnAddField);

				Reporter.log("**************************Add radio field ***********");
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().sendKeys(driver, tabDetailsPage.txtFieldName, lstFields.get("rdoField"));
				fc.utobj().selectDropDownByVisibleText(driver, tabDetailsPage.drpFieldType, "Radio");
				fc.utobj().sendKeys(driver, tabDetailsPage.option1, lstFields.get("rdoOption1"));
				fc.utobj().clickLink(driver, "Add Option");
				fc.utobj().sendKeys(driver, tabDetailsPage.option2, lstFields.get("rdoOption2"));
				fc.utobj().clickElement(driver, tabDetailsPage.btnSumbitField);
				fc.utobj().switchFrameToDefault(driver);
				btnAddField = fc.utobj().getElementByXpath(driver, ".//*[@id='siteMainTable']//*[text()='" + SectionName + "']//following::a[text()='Add New Field']");
				fc.utobj().clickElement(driver, btnAddField);

				Reporter.log("**************************Add Numeric field ***********");
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().sendKeys(driver, tabDetailsPage.txtFieldName, lstFields.get("numericField"));
				fc.utobj().selectDropDownByVisibleText(driver, tabDetailsPage.drpFieldType, "Numeric");
				fc.utobj().clickElement(driver, tabDetailsPage.btnSumbitField);
				fc.utobj().switchFrameToDefault(driver);
				btnAddField = fc.utobj().getElementByXpath(driver, ".//*[@id='siteMainTable']//*[text()='" + SectionName + "']//following::a[text()='Add New Field']");
				fc.utobj().clickElement(driver, btnAddField);

				Reporter.log("**************************Add checkbox field ***********");
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().sendKeys(driver, tabDetailsPage.txtFieldName, lstFields.get("chkboxField"));
				fc.utobj().selectDropDownByVisibleText(driver, tabDetailsPage.drpFieldType, "Checkbox");
				fc.utobj().sendKeys(driver, tabDetailsPage.option1, lstFields.get("chkboxOption1"));
				fc.utobj().clickLink(driver, "Add Option");
				fc.utobj().sendKeys(driver, tabDetailsPage.option2, lstFields.get("chkboxOption2"));
				fc.utobj().clickElement(driver, tabDetailsPage.btnSumbitField);
				fc.utobj().switchFrameToDefault(driver);
				btnAddField = fc.utobj().getElementByXpath(driver, ".//*[@id='siteMainTable']//*[text()='" + SectionName + "']//following::a[text()='Add New Field']");
				fc.utobj().clickElement(driver, btnAddField);

				Reporter.log("************************** Add multi select drop down field ***********");
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().sendKeys(driver, tabDetailsPage.txtFieldName, lstFields.get("mulSelectDrpField"));
				fc.utobj().selectDropDownByVisibleText(driver, tabDetailsPage.drpFieldType, "Multi Select Drop-down");
				fc.utobj().sendKeys(driver, tabDetailsPage.option1, lstFields.get("mulSelectdrpOption1"));
				fc.utobj().clickLink(driver, "Add Option");
				fc.utobj().sendKeys(driver, tabDetailsPage.option2, lstFields.get("mulSelectdrpOption2"));
				fc.utobj().clickElement(driver, tabDetailsPage.btnSumbitField);
				fc.utobj().switchFrameToDefault(driver);
				btnAddField = fc.utobj().getElementByXpath(driver, ".//*[@id='siteMainTable']//*[text()='" + SectionName + "']//following::a[text()='Add New Field']");
				fc.utobj().clickElement(driver, btnAddField);

				Reporter.log("**************************Add document field ***********");
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().sendKeys(driver, tabDetailsPage.txtFieldName, lstFields.get("documentField"));
				fc.utobj().selectDropDownByVisibleText(driver, tabDetailsPage.drpFieldType, "Document");
				fc.utobj().clickElement(driver, tabDetailsPage.btnSumbitField);
				fc.utobj().switchFrameToDefault(driver);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsException("Error in adding fields and section , please refer screenshot!");
		}
		return lstFields;
	}

	public void deleteSectionandFields(WebDriver driver, Map<String, String> config, String tabName, String formName, Map<String, String> dsFields) throws Exception {
		Reporter.log("***************************************** Delete Fields and Section *****************");

		String testCaseId = "TC_Admin_InfoMgr_ManageFormGenerator_Section_Field_Delete";

		fc.infomgr().infomgr_common().AdminFIMManageFormGenerator(driver);

		NavigateToManageFormGeneratorTabs(driver, tabName);

		if (fc.utobj().validate(testCaseId)) {
			try {
				fc.utobj().clickLink(driver, formName);

				for (String key : dsFields.keySet()) {
					if (!(key.contains("sectionName") || key.contains("Option"))) {
						fc.utobj().actionImgOption(driver, dsFields.get(key), "Delete");
						driver.switchTo().alert().accept();
						driver.switchTo().defaultContent();
					}
				}

				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='siteMainTable']//*[text()='" + dsFields.get("sectionName") + "']//following::a[text()='Delete Section']"));
				driver.switchTo().alert().accept();
				driver.switchTo().defaultContent();

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsException("Error in deleting fields and section , please refer screenshot!");
		}

	}

	public String addWebFormFromManageWebFormGenerator(WebDriver driver, Map<String, String> config, Map<String, String> dsCustomFields) throws Exception {
		Reporter.log("***************************************** Add Fields to Web Form *****************");

		String testCaseId = "TC_Admin_InfoMgr_ManageWebFormGenerator_Add_Web_Form";

		fc.infomgr().infomgr_common().AdminInfoManageWebFormGenerator(driver);

		AdminInfoMgrManageWebFormGeneratorPage manageWebFormGeneratorPage = new AdminInfoMgrManageWebFormGeneratorPage(driver);
		WebDriverWait wait = new WebDriverWait(driver, 60);

		String formName = fc.utobj().generateTestData("form1");
		String newFormUrl = "";
		if (fc.utobj().validate(testCaseId)) {
			try {

				fc.utobj().clickElement(driver, manageWebFormGeneratorPage.btnCreateNewForm);
				fc.utobj().sendKeys(driver, manageWebFormGeneratorPage.txtFormName, formName);
				fc.utobj().sendKeys(driver, manageWebFormGeneratorPage.txtDisplayTitle, "form title1");
				fc.utobj().sendKeys(driver, manageWebFormGeneratorPage.txtFormDesc, "form desc1");
				fc.utobj().sendKeys(driver, manageWebFormGeneratorPage.txtFormURL, formName);
				fc.utobj().clickElement(driver, manageWebFormGeneratorPage.btnSaveandText);

				fc.utobj().clickLink(driver, "Center Info");
				Actions actions = new Actions(driver);

				for (String key : dsCustomFields.keySet()) {
					if (!(key.contains("Option") || key.contains("section"))) {
						fc.utobj().sendKeys(driver, manageWebFormGeneratorPage.txtCenterInfoSearchBox, dsCustomFields.get(key));
						try {
							WebElement element1 = fc.utobj().getElementByLinkText(driver, dsCustomFields.get(key));
							actions.moveByOffset(0, 0);
							actions.clickAndHold(element1).moveToElement(fc.utobj().getElement(driver, manageWebFormGeneratorPage.mainSection)).release().build().perform();
							actions.dragAndDrop(element1, fc.utobj().getElement(driver, manageWebFormGeneratorPage.mainSection)).build().perform();
							// fc.utobj().dragAndDropElement(driver,
							// manageWebFormGeneratorPage.mainSection,
							// fc.utobj().getElementByXpath(driver,".//ul[contains(@id
							// ,
							// '"+dsCustomFields.get(key)+"')]/li//p[.='Drop
							// here.']")));
							fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + dsCustomFields.get(key) + "']"), manageWebFormGeneratorPage.defaultDropHere);
						} catch (Exception ex) {
							Reporter.log(ex.getMessage());
						}
					}
				}

				fc.utobj().clickElement(driver, manageWebFormGeneratorPage.saveAndNextBtnDesign);
				fc.utobj().clickElement(driver, fc.utobj().getElementByID(driver, "settingsNextBtn"));
				fc.utobj().clickElement(driver, fc.utobj().getElementByID(driver, "hostURL"));
				newFormUrl = fc.utobj().getText(driver, fc.utobj().getElementByID(driver, "hostCodeBox"));
				fc.utobj().clickElement(driver, fc.utobj().getElementByID(driver, "confirmationNextBtn"));

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsException("Error in creating a web form , please refer screenshot!");
		}
		return newFormUrl;
	}

	// Harish Dwivedi Info Mgr Form Generator

	public Map<String, String> ManageFormGenerator_Add_Section_Fields_To_FormsCustomTab(WebDriver driver, Map<String, String> config, String tabName, String formName, String sectionNamePrefix) throws Exception {
		Reporter.log("***************************************** Add Fields to form " + formName + " *****************");

		String testCaseId = "TC_Admin_InfoMgr_ManageFormGenerator_Franchisees_Add_Fields";

		WebDriverWait wait = new WebDriverWait(driver, 60);
		AdminInfoMgrManageFormGeneratorTabDetailsPage tabDetailsPage = new AdminInfoMgrManageFormGeneratorTabDetailsPage(driver);

		fc.infomgr().infomgr_common().AdminFIMManageFormGenerator(driver);
		NavigateToManageFormGeneratorTabs(driver, tabName);

		Map<String, String> lstFields = new HashMap<String, String>();
		String SectionName = sectionNamePrefix + fc.utobj().generateTestData("Section1");
		lstFields.put("txtField", sectionNamePrefix + fc.utobj().generateTestData("txtField") + fc.utobj().generateRandomNumber());
		lstFields.put("txtAreaField", sectionNamePrefix + fc.utobj().generateTestData("txtAreaField") + fc.utobj().generateRandomNumber());
		lstFields.put("dateField", sectionNamePrefix + fc.utobj().generateTestData("dateField") + fc.utobj().generateRandomNumber());
		lstFields.put("dropdownField", sectionNamePrefix + fc.utobj().generateTestData("dropdownField") + fc.utobj().generateRandomNumber());
		lstFields.put("drpOption1", sectionNamePrefix + fc.utobj().generateTestData("drpOption1") + fc.utobj().generateRandomNumber());
		lstFields.put("drpOption2", sectionNamePrefix + fc.utobj().generateTestData("drpOption2") + fc.utobj().generateRandomNumber());
		lstFields.put("rdoField", sectionNamePrefix + fc.utobj().generateTestData("rdoField") + fc.utobj().generateRandomNumber());
		lstFields.put("rdoOption1", sectionNamePrefix + fc.utobj().generateTestData("rdoOption1") + fc.utobj().generateRandomNumber());
		lstFields.put("rdoOption2", sectionNamePrefix + fc.utobj().generateTestData("rdoOption2") + fc.utobj().generateRandomNumber());
		lstFields.put("chkboxField", sectionNamePrefix + fc.utobj().generateTestData("chkboxField") + fc.utobj().generateRandomNumber());
		lstFields.put("chkboxOption1", sectionNamePrefix + fc.utobj().generateTestData("chkboxOption1") + fc.utobj().generateRandomNumber());
		lstFields.put("chkboxOption2", sectionNamePrefix + fc.utobj().generateTestData("chkboxOption2") + fc.utobj().generateRandomNumber());
		lstFields.put("mulSelectDrpField", sectionNamePrefix + fc.utobj().generateTestData("mulSelectDrpField") + fc.utobj().generateRandomNumber());
		lstFields.put("mulSelectdrpOption1", sectionNamePrefix + fc.utobj().generateTestData("mulSelectDrpOption1") + fc.utobj().generateRandomNumber());
		lstFields.put("mulSelectdrpOption2", sectionNamePrefix + fc.utobj().generateTestData("mulSelectDrpOption2") + fc.utobj().generateRandomNumber());
		lstFields.put("numericField", sectionNamePrefix + fc.utobj().generateRandomNumber());
		lstFields.put("documentField", sectionNamePrefix + fc.utobj().generateTestData("documentField") + fc.utobj().generateRandomNumber());
		lstFields.put("sectionName", SectionName);

		if (fc.utobj().validate(testCaseId)) {
			try {
				fc.utobj().clickLink(driver, formName);

				fc.utobj().clickElement(driver, tabDetailsPage.btnAddSection);

				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().sendKeys(driver, tabDetailsPage.txtSectionName, SectionName);
				fc.utobj().clickElement(driver, tabDetailsPage.btnAddSectionName);
				fc.utobj().switchFrameToDefault(driver);

				WebElement btnAddField = fc.utobj().getElementByXpath(driver, ".//*[@id='siteMainTable']//*[text()='" + SectionName + "']//following::a[text()='Add New Field']");
				fc.utobj().clickElement(driver, btnAddField);

				Reporter.log("**************************Add Text field  and add to center info***********");
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().sendKeys(driver, tabDetailsPage.txtFieldName, lstFields.get("txtField"));
				fc.utobj().selectDropDownByVisibleText(driver, tabDetailsPage.drpFieldType, "Text");
				fc.utobj().clickElement(driver, tabDetailsPage.btnSumbitField);
				fc.utobj().switchFrameToDefault(driver);
				btnAddField = fc.utobj().getElementByXpath(driver, ".//*[@id='siteMainTable']//*[text()='" + SectionName + "']//following::a[text()='Add New Field']");
				fc.utobj().clickElement(driver, btnAddField);

				Reporter.log("**************************Add Text Area field ***********");
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().sendKeys(driver, tabDetailsPage.txtFieldName, lstFields.get("txtAreaField"));
				fc.utobj().selectDropDownByVisibleText(driver, tabDetailsPage.drpFieldType, "Text Area");
				fc.utobj().clickElement(driver, tabDetailsPage.btnSumbitField);
				fc.utobj().switchFrameToDefault(driver);
				btnAddField = fc.utobj().getElementByXpath(driver, ".//*[@id='siteMainTable']//*[text()='" + SectionName + "']//following::a[text()='Add New Field']");
				fc.utobj().clickElement(driver, btnAddField);

				Reporter.log("**************************Add Date field ***********");
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().sendKeys(driver, tabDetailsPage.txtFieldName, lstFields.get("dateField"));
				fc.utobj().selectDropDownByVisibleText(driver, tabDetailsPage.drpFieldType, "Date");
				fc.utobj().clickElement(driver, tabDetailsPage.btnSumbitField);
				fc.utobj().switchFrameToDefault(driver);
				btnAddField = fc.utobj().getElementByXpath(driver, ".//*[@id='siteMainTable']//*[text()='" + SectionName + "']//following::a[text()='Add New Field']");
				fc.utobj().clickElement(driver, btnAddField);

				Reporter.log("**************************Add drop down field ***********");
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().sendKeys(driver, tabDetailsPage.txtFieldName, lstFields.get("dropdownField"));
				fc.utobj().selectDropDownByVisibleText(driver, tabDetailsPage.drpFieldType, "Drop-down");
				fc.utobj().sendKeys(driver, tabDetailsPage.option1, lstFields.get("drpOption1"));
				fc.utobj().clickLink(driver, "Add Option");
				fc.utobj().sendKeys(driver, tabDetailsPage.option2, lstFields.get("drpOption2"));
				fc.utobj().clickElement(driver, tabDetailsPage.btnSumbitField);
				fc.utobj().switchFrameToDefault(driver);
				btnAddField = fc.utobj().getElementByXpath(driver, ".//*[@id='siteMainTable']//*[text()='" + SectionName + "']//following::a[text()='Add New Field']");
				fc.utobj().clickElement(driver, btnAddField);

				Reporter.log("**************************Add radio field ***********");
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().sendKeys(driver, tabDetailsPage.txtFieldName, lstFields.get("rdoField"));
				fc.utobj().selectDropDownByVisibleText(driver, tabDetailsPage.drpFieldType, "Radio");
				fc.utobj().sendKeys(driver, tabDetailsPage.option1, lstFields.get("rdoOption1"));
				fc.utobj().clickLink(driver, "Add Option");
				fc.utobj().sendKeys(driver, tabDetailsPage.option2, lstFields.get("rdoOption2"));
				fc.utobj().clickElement(driver, tabDetailsPage.btnSumbitField);
				fc.utobj().switchFrameToDefault(driver);
				btnAddField = fc.utobj().getElementByXpath(driver, ".//*[@id='siteMainTable']//*[text()='" + SectionName + "']//following::a[text()='Add New Field']");
				fc.utobj().clickElement(driver, btnAddField);

				Reporter.log("**************************Add Numeric field ***********");
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().sendKeys(driver, tabDetailsPage.txtFieldName, lstFields.get("numericField"));
				fc.utobj().selectDropDownByVisibleText(driver, tabDetailsPage.drpFieldType, "Numeric");
				fc.utobj().clickElement(driver, tabDetailsPage.btnSumbitField);
				fc.utobj().switchFrameToDefault(driver);
				btnAddField = fc.utobj().getElementByXpath(driver, ".//*[@id='siteMainTable']//*[text()='" + SectionName + "']//following::a[text()='Add New Field']");
				fc.utobj().clickElement(driver, btnAddField);

				Reporter.log("**************************Add checkbox field ***********");
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().sendKeys(driver, tabDetailsPage.txtFieldName, lstFields.get("chkboxField"));
				fc.utobj().selectDropDownByVisibleText(driver, tabDetailsPage.drpFieldType, "Checkbox");
				fc.utobj().sendKeys(driver, tabDetailsPage.option1, lstFields.get("chkboxOption1"));
				fc.utobj().clickLink(driver, "Add Option");
				fc.utobj().sendKeys(driver, tabDetailsPage.option2, lstFields.get("chkboxOption2"));
				fc.utobj().clickElement(driver, tabDetailsPage.btnSumbitField);
				fc.utobj().switchFrameToDefault(driver);
				btnAddField = fc.utobj().getElementByXpath(driver, ".//*[@id='siteMainTable']//*[text()='" + SectionName + "']//following::a[text()='Add New Field']");
				fc.utobj().clickElement(driver, btnAddField);

				Reporter.log("************************** Add multi select drop down field ***********");
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().sendKeys(driver, tabDetailsPage.txtFieldName, lstFields.get("mulSelectDrpField"));
				fc.utobj().selectDropDownByVisibleText(driver, tabDetailsPage.drpFieldType, "Multi Select Drop-down");
				fc.utobj().sendKeys(driver, tabDetailsPage.option1, lstFields.get("mulSelectdrpOption1"));
				fc.utobj().clickLink(driver, "Add Option");
				fc.utobj().sendKeys(driver, tabDetailsPage.option2, lstFields.get("mulSelectdrpOption2"));
				fc.utobj().clickElement(driver, tabDetailsPage.btnSumbitField);
				fc.utobj().switchFrameToDefault(driver);
				btnAddField = fc.utobj().getElementByXpath(driver, ".//*[@id='siteMainTable']//*[text()='" + SectionName + "']//following::a[text()='Add New Field']");
				fc.utobj().clickElement(driver, btnAddField);

				Reporter.log("**************************Add document field ***********");
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().sendKeys(driver, tabDetailsPage.txtFieldName, lstFields.get("documentField"));
				fc.utobj().selectDropDownByVisibleText(driver, tabDetailsPage.drpFieldType, "Document");
				fc.utobj().clickElement(driver, tabDetailsPage.btnSumbitField);
				fc.utobj().switchFrameToDefault(driver);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsException("Error in adding fields and section , please refer screenshot!");
		}
		return lstFields;
	}

	public Location addFranchiseLocationForInfoMgr(WebDriver driver, boolean isCorporateLocation) throws Exception {
		Reporter.log("********************* Add Franchisee Location for InfoMgr********************* \n");

		Location location = new Location();
		LocationData ld = new LocationData();
		try {
			AdminAreaRegionAddAreaRegionPageTest region_page = new AdminAreaRegionAddAreaRegionPageTest();
			String region_name = fc.utobj().generateTestData("R1");
			region_page.addAreaRegion(driver, region_name);
			if (isCorporateLocation) {
				location.setCorporateLocation("Yes");
			}
			location = fc.commonMethods().getDefaultDataFor_Location(location);
			fc.adminpage().adminAddNewFranchiseLocationLnk(driver);
			location.setAreaRegion(region_name);
			ld.filllocationInfo(driver, location).submit(driver);
			fc.adminpage().manageFranchiseLocationLnk(driver).SearchFranchise(driver, location).VerifyLocationExists(driver, location);
		} catch (Exception e) {
			fc.utobj().printTestStep("Unable to Create Location for Info Mgr");
		}

		return location;
	}

	public Location addFranchiseLocationForInfoMgrWithDivision(WebDriver driver, boolean isCorporateLocation) throws Exception {
		Reporter.log("********************* Add Franchisee Location for InfoMgr********************* \n");

		Location location = new Location();
		LocationData ld = new LocationData();
		try {
			AdminAreaRegionAddAreaRegionPageTest region_page = new AdminAreaRegionAddAreaRegionPageTest();
			String region_name = fc.utobj().generateTestData("R1");
			region_page.addAreaRegion(driver, region_name);
			String DivisionID = division.addDivisionName(driver, fc.utobj().generateTestData("D1"));
			if (isCorporateLocation) {
				location.setCorporateLocation("Yes");
			}
			location = fc.commonMethods().getDefaultDataFor_Location(location);
			fc.adminpage().adminAddNewFranchiseLocationLnk(driver);
			location.setAreaRegion(region_name);
			location.setBrands(DivisionID);
			ld.filllocationInfo(driver, location).submit(driver);
			fc.adminpage().manageFranchiseLocationLnk(driver).SearchFranchise(driver, location).VerifyLocationExists(driver, location);
		} catch (Exception e) {
			fc.utobj().printTestStep("Unable to Create Location for Info Mgr");
		}

		return location;
	}

	public String addFranchiseLocationForInfoMgrName(WebDriver driver, boolean isCorporateLocation) throws Exception {
		Reporter.log("********************* Add Franchisee Location for InfoMgr********************* \n");

		Location location = new Location();
		LocationData ld = new LocationData();
		try {
			AdminAreaRegionAddAreaRegionPageTest region_page = new AdminAreaRegionAddAreaRegionPageTest();
			String region_name = fc.utobj().generateTestData("R1");
			region_page.addAreaRegion(driver, region_name);
			if (isCorporateLocation) {
				location.setCorporateLocation("Yes");
			}
			location = fc.commonMethods().getDefaultDataFor_Location(location);
			fc.adminpage().adminAddNewFranchiseLocationLnk(driver);
			location.setAreaRegion(region_name);
			ld.filllocationInfo(driver, location).submit(driver);
			fc.adminpage().manageFranchiseLocationLnk(driver).SearchFranchise(driver, location).VerifyLocationExists(driver, location);
			fc.utobj().printTestStep("Franchisee Location Created Successfully :" + location.getFranchiseID());
		} catch (Exception e) {
			fc.utobj().throwsException("Unable to Create Location for Info Mgr");
		}

		return location.getFranchiseID();
	}

	public String addInDevelopmentFranchiseLocationForInfoMgrName(WebDriver driver, boolean isCorporateLocation) throws Exception {
		Reporter.log("********************* Add Franchisee Location for InfoMgr********************* \n");

		Location location = new Location();
		LocationData ld = new LocationData();
		try {
			AdminAreaRegionAddAreaRegionPageTest region_page = new AdminAreaRegionAddAreaRegionPageTest();
			String region_name = fc.utobj().generateTestData("R1");
			region_page.addAreaRegion(driver, region_name);
			imc.InfoMgrInDevelopment(driver);
			fc.utobj().clickLink(driver, "Add In Development Location");
			if (isCorporateLocation) {
				location.setCorporateLocation("Yes");
			}
			location = fc.commonMethods().getDefaultDataFor_Location(location);
			location.setAreaRegion(region_name);
			ld.filllocationInfo(driver, location).submit(driver);
		} catch (Exception e) {
			fc.utobj().throwsException("Unable to Create In Development Location for Info Mgr");

		}

		return location.getFranchiseID();
	}

	public  void EnablePIIunderAdmin(WebDriver driver) throws Exception {
		fc.adminpage().adminConfigurationPIIConfiguration(driver);
		AdminConfigurationPIIConfigurationPage pobj = new AdminConfigurationPIIConfigurationPage(driver);
		if (!fc.utobj().getElement(driver, pobj.passwordFunctionalityOn).isSelected()) {
			fc.utobj().clickElement(driver, pobj.passwordFunctionalityOn);
		}
		fc.utobj().clickElement(driver, pobj.passwordFunctionalityOn);
		fc.utobj().sendKeys(driver, pobj.oldPassword, "piisupervisior");
		fc.utobj().sendKeys(driver, pobj.newpassword, "piisupervisior");
		fc.utobj().sendKeys(driver, pobj.confirmPasswordText, "piisupervisior");
		fc.utobj().clickElement(driver, pobj.saveBtn);

	}

	public void SetQATabIntregrationNo(WebDriver driver) throws Exception {
		adminpagelinks.qaTabIntegrationLnk(driver);
		fc.utobj().clickElement(driver, ".//*[@value='No']");
		fc.utobj().clickElement(driver, ".//*[@value='Submit']");

	}

	public void SetQATabIntregrationYes(WebDriver driver) throws Exception {
		adminpagelinks.qaTabIntegrationLnk(driver);
		fc.utobj().clickElement(driver, ".//*[@type='radio' and @value='Yes']");
		fc.utobj().clickElement(driver, ".//*[@value='Submit']");
	}
	
	public WebElement Add_field(WebDriver driver , String section) throws Exception
	{     
		fc.utobj().printTestStep("Click on the add field button");
		
		//WebElement field = driver.findElement(By.xpath("//td[contains(text(),'"+section+"')]/following-sibling::td//a[contains(text(),'Add New Field')]"));
		 //fc.utobj().waitForElement(driver, field);
		// WebElement field1 = driver.findElement(By.xpath("//td[contains(text(),'"+section+"')]/following-sibling::td//a[contains(text(),'Add New Field')]"));
		//WebElement field=fc.utobj().getElementByXpath(driver, "//td[contains(text(),'"+section+"')]/following-sibling::td//a[contains(text(),'Add New Field')]");
		WebElement field1=fc.utobj().getElementByXpath(driver, "//td[contains(text(),'"+section+"')]/following-sibling::td//a[contains(text(),'Add New Field')]");
		JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", field1);
		return field1;
	}
	/*public WebElement Add_Section(WebDriver driver , String tabName) throws Exception
	{
		fc.utobj().printTestStep("Click on the add field button");
		WebElement field = driver.findElement(By.xpath("//td[contains(text(),'"+tabName+"')]/following-sibling::td//a[contains(text(),'Add Section')]"));
		
		return field;
	}*/
}
