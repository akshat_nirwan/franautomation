package com.builds.test.infomgr;

import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.builds.test.fs.AdminFranchiseSalesManageFormGeneratorPageTest;
import com.builds.uimaps.fs.AdminFranchiseSalesManageFormGeneratorPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminInfoMgrManageFormGeneratorPageTest {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "InfoFormGenerate" })

	// BugID - 69606
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Admin_InfoMgr_ManageFormGenerator_Page_Verify", testCaseDescription = "This test case is used to verify Manage Form Generator Page", reference = {
			"69606" })
	public void VerifyAdminFIMManageFormGeneratorPage() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigating to Admin > Info Mgr > Manage Form Generator Page ");

			fc.infomgr().infomgr_common().AdminFIMManageFormGenerator(driver);

			boolean isErrorPage = fc.utobj().assertPageSource(driver,
					"Oops! Looks like the page you are requesting is unavailable");

			boolean isManageFormGeneratorContent = fc.utobj().assertPageSource(driver, "Form Generator Instructions")
					&& fc.utobj().assertPageSource(driver, "Document Management");

			if (!isErrorPage && isManageFormGeneratorContent) {

				Reporter.log("There is no error page and page loads successfully. Test Passes !!!");
			} else {
				fc.utobj().throwsException("Manage form generator doesn't load properly.. Test Failes !!!");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "InfoFormGenerate" })

	// BugID - 72180
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Admin_InfoMgr_ManageFormGenerator_MultiUnit_PreviewForm", testCaseDescription = "This test case is used to verify the print preview of Multi Unit Forms. ", reference = {
			"72180" })
	public void VerifyMultiUnitPreviewAllForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		AdminInfoMgrCommonMethods adminCommonMethods = fc.adminpage().adminInfoMgr();

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigating to Admin > Info Mgr > Manage Form Generator Page ");

			fc.infomgr().infomgr_common().AdminFIMManageFormGenerator(driver);

			fc.utobj().printTestStep("Clicking on the Multi-Unit / Entity tab .");
			adminCommonMethods.NavigateToManageFormGeneratorTabs(driver, "Multi-Unit / Entity");

			fc.utobj().printTestStep("Verify all the forms Print Preview.");
			adminCommonMethods.VerifyAllFormsPreview(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "InfoFormGenerate" })

	// BugID - 72180
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Admin_InfoMgr_ManageFormGenerator_Regional_PreviewForm", testCaseDescription = "This test case will verify Print preview of all the forms of Regional tab", reference = {
			"72180" })
	public void VerifyRegionalPreviewAllForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		AdminInfoMgrCommonMethods adminCommonMethods = fc.adminpage().adminInfoMgr();

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			// Navigate to Admin > Info Mgr > Manage Form Generator > Multi-Unit
			// / Entity
			fc.utobj().printTestStep("Navigating to Admin > Info Mgr > Manage Form Generator Page ");

			fc.infomgr().infomgr_common().AdminFIMManageFormGenerator(driver);

			fc.utobj().printTestStep("Clicking on 'Regional Tab' ");

			adminCommonMethods.NavigateToManageFormGeneratorTabs(driver, "Regional");

			fc.utobj().printTestStep("Verify all the forms Print Preview.");

			adminCommonMethods.VerifyAllFormsPreview(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "InfoFormGenerate" })

	// BugID - 72366
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Admin_InfoMgr_ManageFormGenerator_Franchisee_Agreement", testCaseDescription = "This test case will verify the print preview after modifying the field positions.", reference = {
			"72366" })
	public void VerifyFormModifyFieldsPositionPreview() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		AdminInfoMgrCommonMethods adminCommonMethods = fc.adminpage().adminInfoMgr();

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigating to Admin > Info Mgr > Manage Form Generator Page ");

			fc.infomgr().infomgr_common().AdminFIMManageFormGenerator(driver);

			fc.utobj().printTestStep("Clicking on Franchisee tab.");

			adminCommonMethods.NavigateToManageFormGeneratorTabs(driver, "Franchisee");

			fc.utobj().printTestStep(
					"Verifying the print preview of Agreement form after modifying the fields position.");

			adminCommonMethods.VerifyPreviewAfterModifyFieldsPosition(driver, "Agreement");

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/*
	 * @Parameters({ "tabName" })
	 * 
	 * @Test(groups = { "commonModule", "add", "Lead Status"}) public String
	 * addTab(@Optional()String tabName) throws Exception { String testCaseId =
	 * "";
	 * 
	 * WebDriver driver = fc.loginpage().login();
	 * 
	 * Map<String,String> dataSet = fc.utobj().readTestData("infomgr",
	 * testCaseId);
	 * 
	 * AdminSales adsales = new AdminSales(); adsales.manageFormGenerator(
	 * driver);
	 * 
	 * AdminFranchiseSalesManageFormGeneratorPage pobj = new
	 * AdminFranchiseSalesManageFormGeneratorPage(driver);
	 * fc.utobj().clickElement(driver,pobj.continueBtn);
	 * fc.utobj().clickElement(driver,pobj.addNewTabBtn);
	 * fc.commonMethods().switch_cboxIframe_frameId(driver); tabName =
	 * fc.utobj().generateTestData(tabName);
	 * fc.utobj().sendKeys(driver,pobj.displayName,tabName);
	 * fc.utobj().selectDropDownByPartialText(driver, pobj.submoduleNameDrp,
	 * "Franchisee"); fc.utobj().clickElement(driver,pobj.submitBtn);
	 * fc.utobj().assertSingleLinkText(driver,tabName); return tabName; }
	 */

	@Parameters({ "tabName" })
	@Test(groups = { "commonModule", "add", "Lead Status" })
	public String addSection(@Optional() String tabName) throws Exception {
		String testCaseId = "";

		WebDriver driver = fc.loginpage().login();

		Map<String, String> dataSet = fc.utobj().readTestData("infomgr", testCaseId);
		AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();
		tabName = p1.addTab(tabName);
		fc.utobj().getElementByLinkText(driver, tabName).click();
		String sectionName = "Section " + fc.utobj().generateRandomNumber();
		AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);
		String windowHandle = driver.getWindowHandle();
		fc.utobj().clickElement(driver, pobj.addSectionBtn);
		fc.commonMethods().switch_cboxIframe_frameId(driver);

		fc.utobj().sendKeys(driver, pobj.sectionName, sectionName);
		fc.utobj().clickElement(driver, pobj.submitSectionCboxBtn);
		driver.switchTo().window(windowHandle);
		return sectionName;
	}

	@Parameters({ "tabName" })
	@Test(groups = { "commonModule", "add", "Lead Status" })
	public String addNewField(@Optional() String tabName) throws Exception {
		String testCaseId = "";

		WebDriver driver = fc.loginpage().login();

		AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();
		String sectionName = p1.addSection(tabName);
		String fieldName = "Test Field " + fc.utobj().generateRandomNumber();
		AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);

		String windowHandle = driver.getWindowHandle();
		fc.utobj().clickElement(driver, pobj.addNewFieldBtn);
		fc.commonMethods().switch_cboxIframe_frameId(driver);

		fc.utobj().sendKeys(driver, pobj.fieldDisplayName, fieldName);
		fc.utobj().clickElement(driver, pobj.submitFieldCBoxButton);
		driver.switchTo().window(windowHandle);
		return fieldName;
	}

	@Parameters({ "tabName" })
	@Test(groups = { "commonModule", "add", "Lead Status" })
	public String addDocumentField(@Optional() String tabName) throws Exception {
		String testCaseId = "";

		WebDriver driver = fc.loginpage().login();

		AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();
		String sectionName = p1.addSection(tabName);
		String docName = "Test Field " + fc.utobj().generateRandomNumber();
		AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);

		String windowHandle = driver.getWindowHandle();
		fc.utobj().clickElement(driver, pobj.addDocumentBtn);
		fc.commonMethods().switch_cboxIframe_frameId(driver);

		fc.utobj().sendKeys(driver, pobj.documentTitle, docName);
		fc.utobj().sendKeys(driver, pobj.documentLabel, docName);
		fc.utobj().clickElement(driver, pobj.addDocumentCboxBtn);
		driver.switchTo().window(windowHandle);
		return docName;
	}

	public String addTabName(WebDriver driver, String tabName) throws Exception {

		AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);
		fc.utobj().clickElement(driver, pobj.continueBtn);
		fc.utobj().clickElement(driver, pobj.addNewTabBtn);
		fc.commonMethods().switch_cboxIframe_frameId(driver);
		tabName = fc.utobj().generateTestData(tabName);
		fc.utobj().sendKeys(driver, pobj.displayName, tabName);
		fc.utobj().selectDropDownByPartialText(driver, pobj.submoduleNameDrp, "Franchisee");
		fc.utobj().clickElement(driver, pobj.submitBtn);
		fc.utobj().assertSingleLinkText(driver, tabName);
		return tabName;
	}

	public String addTabNameSync(WebDriver driver, String tabName) throws Exception {

		AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);
		fc.utobj().clickElement(driver, pobj.continueBtn);
		fc.utobj().clickElement(driver, pobj.addNewTabBtn);
		fc.commonMethods().switch_cboxIframe_frameId(driver);
		tabName = fc.utobj().generateTestData(tabName);
		fc.utobj().sendKeys(driver, pobj.displayName, tabName);
		fc.utobj().selectDropDownByPartialText(driver, pobj.submoduleNameDrp, "Franchisee");
		fc.utobj().clickElement(driver,
				fc.utobj().getElementByXpath(driver, ".//*[@name='addMore' and @value='false']"));
		fc.utobj().clickElement(driver, pobj.submitBtn);
		fc.utobj().assertSingleLinkText(driver, tabName);
		return tabName;
	}

	public String addSectionName(WebDriver driver, String sectionName) throws Exception {

		AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);
		String windowHandle = driver.getWindowHandle();
		sectionName = fc.utobj().generateTestData(sectionName);
		fc.utobj().clickElement(driver, pobj.addSectionBtn);
		fc.commonMethods().switch_cboxIframe_frameId(driver);

		fc.utobj().sendKeys(driver, pobj.sectionName, sectionName);
		fc.utobj().clickElement(driver, pobj.submitSectionCboxBtn);
		driver.switchTo().window(windowHandle);
		return sectionName;
	}

	public String addFieldName(WebDriver driver, String sectionName, String fieldName) throws Exception {

		AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);

		fieldName = fc.utobj().generateTestData(fieldName);
		String windowHandle = driver.getWindowHandle();
		fc.utobj().clickElement(driver, pobj.addNewFieldBtn);
		fc.commonMethods().switch_cboxIframe_frameId(driver);

		fc.utobj().sendKeys(driver, pobj.fieldDisplayName, fieldName);
		fc.utobj().clickElement(driver, pobj.submitFieldCBoxButton);
		driver.switchTo().window(windowHandle);
		return fieldName;

	}

	public String addFieldNameMandatory(WebDriver driver, String sectionName, String fieldName) throws Exception {

		AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);

		fieldName = fc.utobj().generateTestData(fieldName);
		String windowHandle = driver.getWindowHandle();
		fc.utobj().clickElement(driver, pobj.addNewFieldBtn);
		fc.commonMethods().switch_cboxIframe_frameId(driver);

		fc.utobj().sendKeys(driver, pobj.fieldDisplayName, fieldName);
		fc.utobj().clickElement(driver, pobj.submitFieldCBoxButton);
		driver.switchTo().window(windowHandle);
		return fieldName;

	}

	public String addFieldNameAllType(WebDriver driver, String sectionName, String fieldName, String type)
			throws Exception {

		AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);

		fieldName = fc.utobj().generateTestData(fieldName);

		if (type == null || "".equals(type) || "null".equals(type)) {
			type = "Text";
		}

		String windowHandle = driver.getWindowHandle();
		if ("Text".equals(type))
			fc.utobj().clickElement(driver, pobj.addNewFieldBtn);
		else
			fc.utobj().clickElement(driver, pobj.addFiledtoBtnmultiple);
		fc.commonMethods().switch_cboxIframe_frameId(driver);

		fc.utobj().sendKeys(driver, pobj.fieldDisplayName, fieldName);
		fc.utobj().selectDropDownByVisibleText(driver, pobj.dbColumnType, type);

		// if("Drop-down".equals(type) || "Radio".equals(type) ||
		// "Checkbox".equals(type) || "Multi Select Drop-down".equals(type))
		// fc.utobj().sendKeys(driver,pobj.optionNameTemp,"A");
		if ("Drop-down".equals(type))
			fc.utobj().sendKeys(driver, pobj.optionNameTemp, "DropValue1");
		if ("Radio".equals(type))
			fc.utobj().sendKeys(driver, pobj.optionNameTemp, "Radio1");
		if ("Checkbox".equals(type))
			fc.utobj().sendKeys(driver, pobj.optionNameTemp, "Checkbox1");
		if ("Multi Select Drop-down".equals(type))
			fc.utobj().sendKeys(driver, pobj.optionNameTemp, "Multi1");

		fc.utobj().clickElement(driver, pobj.submitFieldCBoxButton);
		driver.switchTo().window(windowHandle);
		return fieldName;

	}

	public String addFieldSynchType(WebDriver driver, String sectionName, String fieldName, String type)
			throws Exception {

		AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);

		fieldName = fc.utobj().generateTestData(fieldName);

		if (type == null || "".equals(type) || "null".equals(type)) {
			type = "Text";
		}

		String windowHandle = driver.getWindowHandle();
		fc.utobj().clickElement(driver, pobj.addNewFieldBtn);
		fc.commonMethods().switch_cboxIframe_frameId(driver);

		fc.utobj().sendKeys(driver, pobj.fieldDisplayName, fieldName);
		fc.utobj().selectDropDownByVisibleText(driver, pobj.dbColumnType, type);

		fc.utobj().clickElement(driver,
				fc.utobj().getElementByXpath(driver, ".//*[@id='ms-parentfieldNamesForTabNew']/button"));
		fc.utobj().sendKeys(driver, pobj.searchBoxSynch, "City");
		// fc.utobj().clickElement(driver,
		// fc.utobj().getElementByXpath(driver,".//*[@id='ms-parentfieldNamesForTabNew']/div/ul/li/label[contains(text
		// () ,'Primary Info')]/input")));
		// fc.utobj().moveToElement(driver, pobj.PrimaryInfoSectionSearch);
		fc.utobj().clickElement(driver, driver.findElement(
				By.xpath(".//*[@id='ms-parentfieldNamesForTabNew']/div/ul/li/label[contains(text () ,'City')]/input")));
		fc.utobj().clickElement(driver,
				fc.utobj().getElementByXpath(driver, ".//*[@id='ms-parentfieldNamesForTabNew']/button"));

		fc.utobj().clickElement(driver, pobj.submitFieldCBoxButton);
		driver.switchTo().window(windowHandle);
		return fieldName;

	}

	public String addDocFieldName(WebDriver driver, String sectionName, String fieldName, String docName)
			throws Exception {

		docName = fc.utobj().generateTestData(docName);
		AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);

		String windowHandle = driver.getWindowHandle();
		fc.utobj().clickElement(driver, pobj.addDocumentSaleBtn);
		fc.commonMethods().switch_cboxIframe_frameId(driver);

		fc.utobj().sendKeys(driver, pobj.documentTitle, docName);
		fc.utobj().sendKeys(driver, pobj.documentLabel, docName);
		fc.utobj().clickElement(driver, pobj.addDocumentCboxBtn);
		driver.switchTo().window(windowHandle);
		return docName;

	}

	public String addLeadFromWebForm(WebDriver driver, String fieldName) throws Exception {

		AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);
		// fc.sales().sales_common().manageWebFormGenerator( driver);
		fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//input[@value='Create New Form']"));
		String formName = "wb" + fc.utobj().generateRandomNumber();
		fc.utobj().sendKeys(driver, fc.utobj().getElementByID(driver, "formName"), formName);
		fc.utobj().sendKeys(driver, fc.utobj().getElementByID(driver, "formDisplayTitle"), "WebFrom Title");
		fc.utobj().sendKeys(driver, fc.utobj().getElementByID(driver, "formUrl"), formName);
		fc.utobj().clickElement(driver, fc.utobj().getElementByID(driver, "detailsNextBtn"));

		fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'Center Info')]"));
		fc.utobj().sendKeys(driver, pobj.searchField, fieldName);

		fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + fieldName + "']"),
				pobj.defaultDropHere);

		fc.utobj().clickElement(driver, pobj.saveAndNextBtnDesign);
		// fc.utobj().clickElement(driver,
		// fc.utobj().getElementByID(driver,"designNextBtn")));

		fc.utobj().clickElement(driver, fc.utobj().getElementByID(driver, "settingsNextBtn"));
		fc.utobj().clickElement(driver, fc.utobj().getElementByID(driver, "hostURL"));
		String newFormUrl = fc.utobj().getText(driver, fc.utobj().getElementByID(driver, "hostCodeBox"));
		fc.utobj().clickElement(driver, fc.utobj().getElementByID(driver, "confirmationNextBtn"));

		// driver.get(newFormUrl);

		return newFormUrl;

	}

	public String addFieldNamePrimaryInfo(WebDriver driver, String sectionName, String fieldName) throws Exception {

		AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);

		fieldName = fc.utobj().generateTestData(fieldName);
		String windowHandle = driver.getWindowHandle();
		// fc.utobj().clickElement(driver,pobj.addNewFieldBtn);
		fc.commonMethods().switch_cboxIframe_frameId(driver);

		fc.utobj().sendKeys(driver, pobj.fieldDisplayName, fieldName);
		fc.utobj().clickElement(driver, pobj.submitFieldCBoxButton);
		driver.switchTo().window(windowHandle);
		return fieldName;

	}

}
