package com.builds.test.infomgr;

public class Agreement {

	private String ApprovedDate;
	private String DateExecuted;
	private String EffectiveDate;
	private String TermExpirationDate;
	private String ClosingDate;
	private String StateAddendum;
	private String OtherAddendum;
	private String RightsofFirstRefusalYes;
	private String RightsofFirstRefusalNo;
	private String ProtectedTerritory;
	private String Salesperson;
	private String PreviousLicenseNumber;
	private String RelatedCenter;
	private String DateCenterSold;
	private String RequiredOpeningDate;
	private String Comments;
	private String InitialTerm;
	private String RenewalTermFirst;
	private String RenewalDueDateFirst;
	private String RenewalFeeFirst;
	private String RenewalFeePaidFirstDate;
	private String RenewalTermSecond;
	private String RenewalDueDateSecond;
	private String RenewalFeeSecond;
	private String RenewalFeePaidSecondDate;
	private String RenewalTermThird;
	private String RenewalDueDateThird;
	private String RenewalFeeThird;
	private String RenewalFeePaidThirdDate;
	private String ContactTitle;
	private String FirstName;
	private String LastName;
	private String StreetAddress;
	private String City;
	private String Country;
	private String Zipcode;
	private String State;
	private String PhoneNumbers;
	private String PhoneExt;
	private String FaxNumbers;
	private String MobileNumbers;
	private String Email;
	private String Description1;
	private String Amount1;
	private String AmountTerm1;
	private String Interest1;
	private String Comment1;
	private String Description2;
	private String Amount2;
	private String AmountTerm2;
	private String Interest2;
	private String Comment2;
	private String Description3;
	private String Amount3;
	private String AmountTerm3;
	private String Interest3;
	private String Comment3;
		
	
	public String getApprovedDate() {
		return ApprovedDate;
	}
	public void setApprovedDate(String approvedDate) {
		ApprovedDate = approvedDate;
	}
	public String getDateExecuted() {
		return DateExecuted;
	}
	public void setDateExecuted(String dateExecuted) {
		DateExecuted = dateExecuted;
	}
	public String getEffectiveDate() {
		return EffectiveDate;
	}
	public void setEffectiveDate(String effectiveDate) {
		EffectiveDate = effectiveDate;
	}
	public String getTermExpirationDate() {
		return TermExpirationDate;
	}
	public void setTermExpirationDate(String termExpirationDate) {
		TermExpirationDate = termExpirationDate;
	}
	public String getClosingDate() {
		return ClosingDate;
	}
	public void setClosingDate(String closingDate) {
		ClosingDate = closingDate;
	}
	public String getStateAddendum() {
		return StateAddendum;
	}
	public void setStateAddendum(String stateAddendum) {
		StateAddendum = stateAddendum;
	}
	public String getOtherAddendum() {
		return OtherAddendum;
	}
	public void setOtherAddendum(String otherAddendum) {
		OtherAddendum = otherAddendum;
	}
	public String getRightsofFirstRefusalYes() {
		return RightsofFirstRefusalYes;
	}
	public void setRightsofFirstRefusalYes(String rightsofFirstRefusalYes) {
		RightsofFirstRefusalYes = rightsofFirstRefusalYes;
	}
	public String getRightsofFirstRefusalNo() {
		return RightsofFirstRefusalNo;
	}
	public void setRightsofFirstRefusalNo(String rightsofFirstRefusalNo) {
		RightsofFirstRefusalNo = rightsofFirstRefusalNo;
	}
	public String getProtectedTerritory() {
		return ProtectedTerritory;
	}
	public void setProtectedTerritory(String protectedTerritory) {
		ProtectedTerritory = protectedTerritory;
	}
	public String getSalesperson() {
		return Salesperson;
	}
	public void setSalesperson(String salesperson) {
		Salesperson = salesperson;
	}
	public String getPreviousLicenseNumber() {
		return PreviousLicenseNumber;
	}
	public void setPreviousLicenseNumber(String previousLicenseNumber) {
		PreviousLicenseNumber = previousLicenseNumber;
	}
	public String getRelatedCenter() {
		return RelatedCenter;
	}
	public void setRelatedCenter(String relatedCenter) {
		RelatedCenter = relatedCenter;
	}
	public String getDateCenterSold() {
		return DateCenterSold;
	}
	public void setDateCenterSold(String dateCenterSold) {
		DateCenterSold = dateCenterSold;
	}
	public String getRequiredOpeningDate() {
		return RequiredOpeningDate;
	}
	public void setRequiredOpeningDate(String requiredOpeningDate) {
		RequiredOpeningDate = requiredOpeningDate;
	}
	public String getComments() {
		return Comments;
	}
	public void setComments(String comments) {
		Comments = comments;
	}
	public String getInitialTerm() {
		return InitialTerm;
	}
	public void setInitialTerm(String initialTerm) {
		InitialTerm = initialTerm;
	}
	public String getRenewalTermFirst() {
		return RenewalTermFirst;
	}
	public void setRenewalTermFirst(String renewalTermFirst) {
		RenewalTermFirst = renewalTermFirst;
	}
	public String getRenewalDueDateFirst() {
		return RenewalDueDateFirst;
	}
	public void setRenewalDueDateFirst(String renewalDueDateFirst) {
		RenewalDueDateFirst = renewalDueDateFirst;
	}
	public String getRenewalFeeFirst() {
		return RenewalFeeFirst;
	}
	public void setRenewalFeeFirst(String renewalFeeFirst) {
		RenewalFeeFirst = renewalFeeFirst;
	}
	public String getRenewalFeePaidFirstDate() {
		return RenewalFeePaidFirstDate;
	}
	public void setRenewalFeePaidFirstDate(String renewalFeePaidFirstDate) {
		RenewalFeePaidFirstDate = renewalFeePaidFirstDate;
	}
	public String getRenewalTermSecond() {
		return RenewalTermSecond;
	}
	public void setRenewalTermSecond(String renewalTermSecond) {
		RenewalTermSecond = renewalTermSecond;
	}
	public String getRenewalDueDateSecond() {
		return RenewalDueDateSecond;
	}
	public void setRenewalDueDateSecond(String renewalDueDateSecond) {
		RenewalDueDateSecond = renewalDueDateSecond;
	}
	public String getRenewalFeeSecond() {
		return RenewalFeeSecond;
	}
	public void setRenewalFeeSecond(String renewalFeeSecond) {
		RenewalFeeSecond = renewalFeeSecond;
	}
	public String getRenewalFeePaidSecondDate() {
		return RenewalFeePaidSecondDate;
	}
	public void setRenewalFeePaidSecondDate(String renewalFeePaidSecondDate) {
		RenewalFeePaidSecondDate = renewalFeePaidSecondDate;
	}
	public String getRenewalTermThird() {
		return RenewalTermThird;
	}
	public void setRenewalTermThird(String renewalTermThird) {
		RenewalTermThird = renewalTermThird;
	}
	public String getRenewalDueDateThird() {
		return RenewalDueDateThird;
	}
	public void setRenewalDueDateThird(String renewalDueDateThird) {
		RenewalDueDateThird = renewalDueDateThird;
	}
	public String getRenewalFeeThird() {
		return RenewalFeeThird;
	}
	public void setRenewalFeeThird(String renewalFeeThird) {
		RenewalFeeThird = renewalFeeThird;
	}
	public String getRenewalFeePaidThirdDate() {
		return RenewalFeePaidThirdDate;
	}
	public void setRenewalFeePaidThirdDate(String renewalFeePaidThirdDate) {
		RenewalFeePaidThirdDate = renewalFeePaidThirdDate;
	}
	public String getContactTitle() {
		return ContactTitle;
	}
	public void setContactTitle(String contactTitle) {
		ContactTitle = contactTitle;
	}
	public String getFirstName() {
		return FirstName;
	}
	public void setFirstName(String firstName) {
		FirstName = firstName;
	}
	public String getLastName() {
		return LastName;
	}
	public void setLastName(String lastName) {
		LastName = lastName;
	}
	public String getStreetAddress() {
		return StreetAddress;
	}
	public void setStreetAddress(String streetAddress) {
		StreetAddress = streetAddress;
	}
	public String getCity() {
		return City;
	}
	public void setCity(String city) {
		City = city;
	}
	public String getCountry() {
		return Country;
	}
	public void setCountry(String country) {
		Country = country;
	}
	public String getZipcode() {
		return Zipcode;
	}
	public void setZipcode(String zipcode) {
		Zipcode = zipcode;
	}
	public String getState() {
		return State;
	}
	public void setState(String state) {
		State = state;
	}
	public String getPhoneNumbers() {
		return PhoneNumbers;
	}
	public void setPhoneNumbers(String phoneNumbers) {
		PhoneNumbers = phoneNumbers;
	}
	public String getPhoneExt() {
		return PhoneExt;
	}
	public void setPhoneExt(String phoneExt) {
		PhoneExt = phoneExt;
	}
	public String getFaxNumbers() {
		return FaxNumbers;
	}
	public void setFaxNumbers(String faxNumbers) {
		FaxNumbers = faxNumbers;
	}
	public String getMobileNumbers() {
		return MobileNumbers;
	}
	public void setMobileNumbers(String mobileNumbers) {
		MobileNumbers = mobileNumbers;
	}
	public String getEmail() {
		return Email;
	}
	public void setEmail(String email) {
		Email = email;
	}
	public String getDescription1() {
		return Description1;
	}
	public void setDescription1(String description1) {
		Description1 = description1;
	}
	public String getAmount1() {
		return Amount1;
	}
	public void setAmount1(String amount1) {
		Amount1 = amount1;
	}
	public String getAmountTerm1() {
		return AmountTerm1;
	}
	public void setAmountTerm1(String amountTerm1) {
		AmountTerm1 = amountTerm1;
	}
	public String getInterest1() {
		return Interest1;
	}
	public void setInterest1(String interest1) {
		Interest1 = interest1;
	}
	public String getComment1() {
		return Comment1;
	}
	public void setComment1(String comment1) {
		Comment1 = comment1;
	}
	public String getDescription2() {
		return Description2;
	}
	public void setDescription2(String description2) {
		Description2 = description2;
	}
	public String getAmount2() {
		return Amount2;
	}
	public void setAmount2(String amount2) {
		Amount2 = amount2;
	}
	public String getAmountTerm2() {
		return AmountTerm2;
	}
	public void setAmountTerm2(String amountTerm2) {
		AmountTerm2 = amountTerm2;
	}
	public String getInterest2() {
		return Interest2;
	}
	public void setInterest2(String interest2) {
		Interest2 = interest2;
	}
	public String getComment2() {
		return Comment2;
	}
	public void setComment2(String comment2) {
		Comment2 = comment2;
	}
	public String getDescription3() {
		return Description3;
	}
	public void setDescription3(String description3) {
		Description3 = description3;
	}
	public String getAmount3() {
		return Amount3;
	}
	public void setAmount3(String amount3) {
		Amount3 = amount3;
	}
	
	public String getAmountTerm3() {
		return AmountTerm3;
	}
	public void setAmountTerm3(String amountTerm3) {
		AmountTerm3 = amountTerm3;
	}
	public String getInterest3() {
		return Interest3;
	}
	public void setInterest3(String interest3) {
		Interest3 = interest3;
	}
	public String getComment3() {
		return Comment3;
	}
	public void setComment3(String comment3) {
		Comment3 = comment3;
	}
	
	
}
