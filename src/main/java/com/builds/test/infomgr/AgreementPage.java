package com.builds.test.infomgr;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.infomgr.AgreementUI;
import com.builds.utilities.FranconnectUtil;

public class AgreementPage {

	public AgreementPage FillAgreementTabData(WebDriver driver, Agreement Ag)throws Exception  {
			
		AgreementUI ui= new AgreementUI(driver);
		FranconnectUtil fc= new FranconnectUtil();
		
		fc.utobj().printTestStep("Fill Franchisee Agreement Tab Information");
		
		if (Ag.getApprovedDate()!=null) {
			fc.utobj().sendKeys(driver, ui.ApprovedDate, Ag.getApprovedDate());
		}
			
		if(Ag.getDateExecuted()!=null) {
			fc.utobj().sendKeys(driver, ui.DateExecuted, Ag.getDateExecuted());
		}
		
		if(Ag.getEffectiveDate()!=null) {
			fc.utobj().sendKeys(driver, ui.EffectiveDate, Ag.getEffectiveDate());
		}
		
		if(Ag.getTermExpirationDate()!=null) {
			fc.utobj().sendKeys(driver, ui.TermExpirationDate, Ag.getTermExpirationDate());
		}
		if(Ag.getClosingDate()!=null) {
			fc.utobj().sendKeys(driver, ui.ClosingDate, Ag.getClosingDate());
		}
		
		if(Ag.getStateAddendum()!=null) {
			fc.utobj().sendKeys(driver, ui.StateAddendum, Ag.getStateAddendum());
		}
		
		if(Ag.getOtherAddendum()!=null) {
			fc.utobj().sendKeys(driver, ui.OtherAddendum, Ag.getOtherAddendum());
		}
		
		if(Ag.getRightsofFirstRefusalYes()!=null) {
			fc.utobj().clickElement(driver, ui.RightsofFirstRefusalYes);
		}
		
		/*if(Ag.getRightsofFirstRefusalNo()!=null) {
			fc.utobj().clickElement(driver, ui.RightsofFirstRefusalNo);
		}*/
		
		if(Ag.getProtectedTerritory()!=null) {
			fc.utobj().sendKeys(driver, ui.ProtectedTerritory, Ag.getProtectedTerritory());
		}
		
		if(Ag.getSalesperson()!=null) {
			fc.utobj().sendKeys(driver, ui.Salesperson, Ag.getSalesperson());
		}
		
		if(Ag.getPreviousLicenseNumber()!=null) {
			fc.utobj().sendKeys(driver, ui.PreviousLicenseNumber, Ag.getPreviousLicenseNumber());
		}
		
		if(Ag.getRelatedCenter()!=null) {
			fc.utobj().sendKeys(driver, ui.RelatedCenter, Ag.getRelatedCenter());
		}
		
		if(Ag.getRequiredOpeningDate()!=null) {
			fc.utobj().sendKeys(driver, ui.RequiredOpeningDate, Ag.getRequiredOpeningDate());
		}
		if(Ag.getComments()!=null) {
			fc.utobj().sendKeys(driver, ui.Comments, Ag.getComments());
		}
		
		if(Ag.getInitialTerm()!=null) {
			fc.utobj().sendKeys(driver, ui.InitialTerm, Ag.getInitialTerm());
		}
		
		//RenewalFirst
		
		if(Ag.getRenewalTermFirst()!=null) {
			fc.utobj().sendKeys(driver, ui.RenewalTermFirst, Ag.getRenewalTermFirst());
		}
		if(Ag.getRenewalDueDateFirst()!=null) {
			fc.utobj().sendKeys(driver, ui.RenewalDueDateFirst, Ag.getRenewalDueDateFirst());
		}
		if(Ag.getRenewalFeeFirst()!=null) {
			fc.utobj().sendKeys(driver, ui.RenewalFeeFirst, Ag.getRenewalFeeFirst());
		}
		if(Ag.getRenewalFeePaidFirstDate()!=null) {
			fc.utobj().sendKeys(driver, ui.RenewalFeePaidFirstDate, Ag.getRenewalFeePaidFirstDate());
		}
		
		//RenewalSecond
		
		if(Ag.getRenewalTermSecond()!=null) {
			fc.utobj().sendKeys(driver, ui.RenewalTermSecond, Ag.getRenewalTermSecond());
		}
		if(Ag.getRenewalDueDateSecond()!=null) {
			fc.utobj().sendKeys(driver, ui.RenewalDueDateSecond, Ag.getRenewalDueDateSecond());
		}
		if(Ag.getRenewalFeeSecond()!=null) {
			fc.utobj().sendKeys(driver, ui.RenewalFeeSecond, Ag.getRenewalFeeSecond());
		}
		if(Ag.getRenewalFeePaidSecondDate()!=null) {
			fc.utobj().sendKeys(driver, ui.RenewalFeePaidSecondDate, Ag.getRenewalFeePaidSecondDate());
		}
		
		//RenewalThird
		
		if(Ag.getRenewalTermThird()!=null) {
			fc.utobj().sendKeys(driver, ui.RenewalTermThird, Ag.getRenewalTermThird());
		}
		if(Ag.getRenewalDueDateThird()!=null) {
			fc.utobj().sendKeys(driver, ui.RenewalDueDateThird, Ag.getRenewalDueDateThird());
		}
		if(Ag.getRenewalFeeThird()!=null) {
			fc.utobj().sendKeys(driver, ui.RenewalFeeThird, Ag.getRenewalFeeThird());
		}
		if(Ag.getRenewalFeePaidThirdDate()!=null) {
			fc.utobj().sendKeys(driver, ui.RenewalFeePaidThirdDate, Ag.getRenewalFeePaidThirdDate());
		}
		
		//Agreement Contact Details	
		
		if(Ag.getContactTitle()!=null) {
			fc.utobj().selectDropDown(driver, ui.ContactTitle, Ag.getContactTitle());
		}
		
		if(Ag.getFirstName()!=null) {
			fc.utobj().sendKeys(driver, ui.FirstName, Ag.getFirstName());
		}
		if(Ag.getLastName()!=null) {
			fc.utobj().sendKeys(driver, ui.LastName, Ag.getLastName());
		}
		if(Ag.getStreetAddress()!=null) {
			fc.utobj().sendKeys(driver, ui.StreetAddress, Ag.getStreetAddress());
		}
		if(Ag.getCity()!=null) {
			fc.utobj().sendKeys(driver, ui.City, Ag.getCity());
		}
		if(Ag.getCountry()!=null) {
			fc.utobj().selectDropDown(driver, ui.Country, Ag.getCountry());
		}
		if(Ag.getState()!=null) {
			fc.utobj().selectDropDown(driver, ui.State, Ag.getState());
		}
		if(Ag.getZipcode()!=null) {
			fc.utobj().sendKeys(driver, ui.Zipcode, Ag.getZipcode());
		}
		if(Ag.getPhoneNumbers()!=null) {
			fc.utobj().sendKeys(driver, ui.PhoneNumbers, Ag.getPhoneNumbers());
		}
		if(Ag.getPhoneExt()!=null) {
			fc.utobj().sendKeys(driver, ui.PhoneExt, Ag.getPhoneExt());
		}
		if(Ag.getFaxNumbers()!=null) {
			fc.utobj().sendKeys(driver, ui.FaxNumbers, Ag.getFaxNumbers());
		}
		if(Ag.getMobileNumbers()!=null) {
			fc.utobj().sendKeys(driver, ui.MobileNumbers, Ag.getMobileNumbers());
		}
		if(Ag.getEmail()!=null) {
			fc.utobj().sendKeys(driver, ui.Email, Ag.getEmail());
		}
		//Financial Note Section
		
		if(Ag.getDescription1()!=null) {
			fc.utobj().sendKeys(driver, ui.Description1, Ag.getDescription1());
		}
		if(Ag.getAmount1()!=null) {
			fc.utobj().sendKeys(driver, ui.Amount1, Ag.getAmount1());
		}
		if(Ag.getAmountTerm1()!=null) {
			fc.utobj().sendKeys(driver, ui.AmountTerm1, Ag.getAmountTerm1());
		}
		if(Ag.getInterest1()!=null) {
			fc.utobj().sendKeys(driver, ui.Interest1, Ag.getInterest1());
		}
		if(Ag.getComment1()!=null) {
			fc.utobj().sendKeys(driver, ui.Comment1, Ag.getComment1());
		}
				
		if(Ag.getDescription2()!=null) {
			fc.utobj().sendKeys(driver, ui.Description2, Ag.getDescription2());
		}
		if(Ag.getAmount2()!=null) {
			fc.utobj().sendKeys(driver, ui.Amount2, Ag.getAmount2());
		}
		if(Ag.getAmountTerm2()!=null) {
			fc.utobj().sendKeys(driver, ui.AmountTerm2, Ag.getAmountTerm2());
		}
		if(Ag.getInterest2()!=null) {
			fc.utobj().sendKeys(driver, ui.Interest2, Ag.getInterest2());
		}
		if(Ag.getComment2()!=null) {
			fc.utobj().sendKeys(driver, ui.Comment2, Ag.getComment2());
		}
		
		if(Ag.getDescription3()!=null) {
			fc.utobj().sendKeys(driver, ui.Description3, Ag.getDescription3());
		}
		if(Ag.getAmount3()!=null) {
			fc.utobj().sendKeys(driver, ui.Amount3, Ag.getAmount3());
		}
		if(Ag.getAmountTerm3()!=null) {
			fc.utobj().sendKeys(driver, ui.AmountTerm3, Ag.getAmountTerm3());
		}
		if(Ag.getInterest3()!=null) {
			fc.utobj().sendKeys(driver, ui.Interest3, Ag.getInterest3());
		}
		if(Ag.getComment3()!=null) {
			fc.utobj().sendKeys(driver, ui.Comment3, Ag.getComment3());
		}
		
	
	return this;
}
	public void submit(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();

		AgreementUI ui = new AgreementUI(driver);

		fc.utobj().printTestStep("Click Submit");

		fc.utobj().clickElement(driver, ui.Submit);

	}

	public void Cancel(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();

		AgreementUI ui = new AgreementUI(driver);

		fc.utobj().printTestStep("Click Cancel");

		fc.utobj().clickElement(driver, ui.Cancel);

	}
}


