package com.builds.test.infomgr;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;

import com.builds.uimaps.infomgr.InfoMgrCampaignCenterAddCampaignPage;
import com.builds.uimaps.infomgr.InfoMgrCampaignCenterEmailCampaignsPage;
import com.builds.uimaps.infomgr.InfoMgrCampaignCenterEmailTemplatesPage;
import com.builds.uimaps.infomgr.InfoMgrCampaignCenterPage;
import com.builds.utilities.FranconnectUtil;

public class CampaignCenterCommonMethods {

	FranconnectUtil fc = new FranconnectUtil();

	public String createCampaign(WebDriver driver) throws Exception {
		Reporter.log("********************** Create a Campaign ***********************");

		String testCaseId = "TC_InfoMgr_CampaignCenter_CreateCampaign";
		Map<String, String> dsCampaignCenter = fc.utobj().readTestData("infomgr", testCaseId);
		String campaignName = fc.utobj().generateTestData(dsCampaignCenter.get("campaignName"));

		InfoMgrCampaignCenterPage campaignCenterPage = new InfoMgrCampaignCenterPage(driver);
		InfoMgrCampaignCenterAddCampaignPage addCampaignPage = new InfoMgrCampaignCenterAddCampaignPage(driver);

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().clickElement(driver, campaignCenterPage.btnCreateCampaign);
				fc.utobj().sendKeys(driver, addCampaignPage.txtCampaignName, campaignName);
				fc.utobj().sendKeys(driver, addCampaignPage.txtCampaignDescription,
						dsCampaignCenter.get("campaignDesc"));
				fc.utobj().clickElement(driver, addCampaignPage.rdoPromotionalCampaign);
				fc.utobj().clickElement(driver, addCampaignPage.rdoNewTemplate);
				fc.utobj().sendKeys(driver, addCampaignPage.txtMailSubject, dsCampaignCenter.get("mailSubject"));
				fc.utobj().clickElement(driver, addCampaignPage.rdoText);
				WebDriverWait wait = new WebDriverWait(driver, 60);
				fc.utobj().sendKeys(driver, addCampaignPage.txtMailContent, dsCampaignCenter.get("mailContent"));
				fc.utobj().clickRadioButton(driver, addCampaignPage.rdoFromHeaderEmail,
						dsCampaignCenter.get("fromEmail"));
				fc.utobj().clickRadioButton(driver, addCampaignPage.rdoFromHeaderEmail,
						dsCampaignCenter.get("replyEmail"));
				fc.utobj().sendKeys(driver, addCampaignPage.txtEndDate, fc.utobj().getFutureDateUSFormat(20));
				fc.utobj().clickElement(driver, addCampaignPage.btnCreate);

				if (fc.utobj().assertLinkText(driver, "Show All")) {
					fc.utobj().clickLink(driver, "Show All");
				}
				boolean isCampaignOnPage = fc.utobj().assertLinkText(driver, campaignName);
				if (isCampaignOnPage) {
					Reporter.log("Campaign created successfully !!!");
				} else {
					fc.utobj().throwsException("Error in creating campaign !!!");
				}

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in creating campaign , please refer screenshot!");
		}
		return campaignName;
	}

	public String createGraphicalCampaign(WebDriver driver) throws Exception {
		Reporter.log("********************** Create a Campaign ***********************");

		String testCaseId = "TC_InfoMgr_CampaignCenter_AddandAssociateGraphicalCampaign";
		Map<String, String> dsCampaignCenter = fc.utobj().readTestData("infomgr", testCaseId);
		String campaignName = fc.utobj().generateTestData(dsCampaignCenter.get("campaignName"));

		InfoMgrCampaignCenterPage campaignCenterPage = new InfoMgrCampaignCenterPage(driver);
		InfoMgrCampaignCenterAddCampaignPage addCampaignPage = new InfoMgrCampaignCenterAddCampaignPage(driver);

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().clickElement(driver, campaignCenterPage.btnCreateCampaign);
				fc.utobj().sendKeys(driver, addCampaignPage.txtCampaignName, campaignName);
				fc.utobj().sendKeys(driver, addCampaignPage.txtCampaignDescription,
						dsCampaignCenter.get("campaignDesc"));
				fc.utobj().clickElement(driver, addCampaignPage.rdoPromotionalCampaign);
				fc.utobj().clickElement(driver, addCampaignPage.rdoNewTemplate);
				fc.utobj().sendKeys(driver, addCampaignPage.txtMailSubject, dsCampaignCenter.get("mailSubject"));
				fc.utobj().switchFrameById(driver, "emailContent_ifr");
				fc.utobj().clickElement(driver, addCampaignPage.Textbody);
				WebDriverWait wait = new WebDriverWait(driver, 30);
				fc.utobj().sendKeys(driver, addCampaignPage.Textbody, dsCampaignCenter.get("mailContent"));
				fc.utobj().switchFrameToDefault(driver);
				fc.utobj().clickElement(driver, addCampaignPage.Attachment);
				fc.utobj().switchFrameById(driver, "cboxIframe");
				fc.utobj().sendKeys(driver, addCampaignPage.ChooseFile,
						"C:\\Selenium_Test_Input\\testData\\document\\financeDoc.pdf");
				fc.utobj().clickElement(driver, addCampaignPage.attach);
				fc.utobj().clickElement(driver, addCampaignPage.Done);
				fc.utobj().switchFrameToDefault(driver);

				fc.utobj().clickRadioButton(driver, addCampaignPage.rdoFromHeaderEmail,
						dsCampaignCenter.get("fromEmail"));
				fc.utobj().clickRadioButton(driver, addCampaignPage.rdoFromHeaderEmail,
						dsCampaignCenter.get("replyEmail"));
				fc.utobj().sendKeys(driver, addCampaignPage.txtEndDate, fc.utobj().getFutureDateUSFormat(20));
				fc.utobj().clickElement(driver, addCampaignPage.btnCreate);

				if (fc.utobj().assertLinkText(driver, "Show All")) {
					fc.utobj().clickLink(driver, "Show All");
				}
				boolean isCampaignOnPage = fc.utobj().assertLinkText(driver, campaignName);
				if (isCampaignOnPage) {
					Reporter.log("Campaign created successfully !!!");
				} else {
					fc.utobj().throwsException("Error in creating campaign !!!");
				}

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in creating campaign , please refer screenshot!");
		}
		return campaignName;
	}

	public String createGraphicalRegularCampaign(WebDriver driver) throws Exception {
		Reporter.log("********************** Create a Campaign ***********************");

		String testCaseId = "TC_InfoMgr_CampaignCenter_AddandAssociateGraphicalCampaign";
		Map<String, String> dsCampaignCenter = fc.utobj().readTestData("infomgr", testCaseId);
		String campaignName = fc.utobj().generateTestData(dsCampaignCenter.get("campaignName"));

		InfoMgrCampaignCenterPage campaignCenterPage = new InfoMgrCampaignCenterPage(driver);
		InfoMgrCampaignCenterAddCampaignPage addCampaignPage = new InfoMgrCampaignCenterAddCampaignPage(driver);

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().clickElement(driver, campaignCenterPage.btnCreateCampaign);
				fc.utobj().sendKeys(driver, addCampaignPage.txtCampaignName, campaignName);
				fc.utobj().sendKeys(driver, addCampaignPage.txtCampaignDescription,
						dsCampaignCenter.get("campaignDesc"));
				fc.utobj().clickElement(driver, addCampaignPage.rdoRegularCampaign);
				fc.utobj().clickElement(driver, addCampaignPage.rdoNewTemplate);
				fc.utobj().sendKeys(driver, addCampaignPage.txtMailSubject, dsCampaignCenter.get("mailSubject"));
				fc.utobj().switchFrameById(driver, "emailContent_ifr");
				fc.utobj().clickElement(driver, addCampaignPage.Textbody);
				WebDriverWait wait = new WebDriverWait(driver, 30);
				fc.utobj().sendKeys(driver, addCampaignPage.Textbody, dsCampaignCenter.get("mailContent"));
				fc.utobj().switchFrameToDefault(driver);
				fc.utobj().clickElement(driver, addCampaignPage.Attachment);
				fc.utobj().switchFrameById(driver, "cboxIframe");
				fc.utobj().sendKeys(driver, addCampaignPage.ChooseFile,
						"C:\\Selenium_Test_Input\\testData\\document\\financeDoc.pdf");
				fc.utobj().clickElement(driver, addCampaignPage.attach);
				fc.utobj().clickElement(driver, addCampaignPage.Done);
				fc.utobj().switchFrameToDefault(driver);

				fc.utobj().clickRadioButton(driver, addCampaignPage.rdoFromHeaderEmail,
						dsCampaignCenter.get("fromEmail"));
				fc.utobj().clickRadioButton(driver, addCampaignPage.rdoFromHeaderEmail,
						dsCampaignCenter.get("replyEmail"));
				// fc.utobj().sendKeys(driver, addCampaignPage.txtEndDate,
				// fc.utobj().getFutureDateUSFormat(20));
				fc.utobj().clickElement(driver, addCampaignPage.btnCreate);

				if (fc.utobj().assertLinkText(driver, "Show All")) {
					fc.utobj().clickLink(driver, "Show All");
				}
				boolean isCampaignOnPage = fc.utobj().assertLinkText(driver, campaignName);
				if (isCampaignOnPage) {
					Reporter.log("Campaign created successfully !!!");
				} else {
					fc.utobj().throwsException("Error in creating campaign !!!");
				}

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in creating campaign , please refer screenshot!");
		}
		return campaignName;
	}

	public String createHTMLCampaign(WebDriver driver) throws Exception {
		Reporter.log("********************** Create a Campaign ***********************");

		String testCaseId = "TC_InfoMgr_CampaignCenter_AddandAssociate_HTMLCampaign";
		Map<String, String> dsCampaignCenter = fc.utobj().readTestData("infomgr", testCaseId);
		String campaignName = fc.utobj().generateTestData(dsCampaignCenter.get("campaignName"));

		InfoMgrCampaignCenterPage campaignCenterPage = new InfoMgrCampaignCenterPage(driver);
		InfoMgrCampaignCenterAddCampaignPage addCampaignPage = new InfoMgrCampaignCenterAddCampaignPage(driver);

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().clickElement(driver, campaignCenterPage.btnCreateCampaign);
				fc.utobj().sendKeys(driver, addCampaignPage.txtCampaignName, campaignName);
				fc.utobj().sendKeys(driver, addCampaignPage.txtCampaignDescription,
						dsCampaignCenter.get("campaignDesc"));
				fc.utobj().clickElement(driver, addCampaignPage.rdoPromotionalCampaign);
				fc.utobj().clickElement(driver, addCampaignPage.rdoNewTemplate);
				fc.utobj().sendKeys(driver, addCampaignPage.txtMailSubject, dsCampaignCenter.get("mailSubject"));
				fc.utobj().clickElement(driver, addCampaignPage.rdoHTML);
				fc.utobj().clickElement(driver, addCampaignPage.Keywordslink);
				fc.utobj().switchFrameById(driver, "cboxIframe");
				WebElement keyword = fc.utobj().getElementByXpath(driver, ".//*[contains(text(),'Keywords List')]");
				String text = keyword.getText();
				if (text.contains("Keywords List")) {
					System.out.println("Verified");
				} else {
					System.out.println("Keywords List not found");
				}
				fc.utobj().clickElement(driver, addCampaignPage.Closebtn);
				fc.utobj().switchFrameToDefault(driver);
				fc.utobj().sendKeys(driver, addCampaignPage.ChooseHTMLFile,
						"C:\\Selenium_Test_Input\\testData\\document\\test.zip");
				fc.utobj().clickRadioButton(driver, addCampaignPage.rdoFromHeaderEmail,
						dsCampaignCenter.get("fromEmail"));
				fc.utobj().clickRadioButton(driver, addCampaignPage.rdoFromHeaderEmail,
						dsCampaignCenter.get("replyEmail"));
				fc.utobj().sendKeys(driver, addCampaignPage.txtEndDate, fc.utobj().getFutureDateUSFormat(20));
				fc.utobj().clickElement(driver, addCampaignPage.btnCreate);
				fc.utobj().clickElement(driver, addCampaignPage.UploadHTMLFile);

				if (fc.utobj().assertLinkText(driver, "Show All")) {
					fc.utobj().clickLink(driver, "Show All");
				}
				boolean isCampaignOnPage = fc.utobj().assertLinkText(driver, campaignName);
				if (isCampaignOnPage) {
					Reporter.log("Campaign created successfully !!!");
				} else {
					fc.utobj().throwsException("Error in creating campaign !!!");
				}

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in creating campaign , please refer screenshot!");
		}
		return campaignName;
	}

	public void filterLocation(WebDriver driver, String franchiseID) throws Exception {
		Reporter.log("********************** Filter location ***********************");
		String testCaseId = "TC_InfoMgr_CampaignCenter_Page_FilterLocation";
		WebDriverWait wait = new WebDriverWait(driver, 60);

		InfoMgrCampaignCenterEmailCampaignsPage emailCampaignPage = new InfoMgrCampaignCenterEmailCampaignsPage(driver);

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().clickElement(driver, emailCampaignPage.locationByOtherInformationLnk);
				fc.utobj().sendKeys(driver, emailCampaignPage.txtFranchiseID, franchiseID);
				fc.utobj().clickElement(driver, emailCampaignPage.btnSearch);
				WebElement chkFranchisee = driver
						.findElement(By.xpath("(.//*[@id='locationdiv']//input[@type='checkbox'])[2]"));
				fc.utobj().clickElement(driver, chkFranchisee);
				fc.utobj().clickElement(driver, emailCampaignPage.nextBtn);
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in filtering the location , please refer screenshot!");
		}

	}

	public void associateWithCampaign(WebDriver driver, String campaignTitle) throws Exception {
		Reporter.log("********************** Associate location with campaign ***********************");

		String testCaseId = "TC_InfoMgr_CampaignCenter_Campaign_AssociateWithLocation";
		WebDriverWait wait = new WebDriverWait(driver, 60);

		InfoMgrCampaignCenterEmailCampaignsPage emailCampaignPage = new InfoMgrCampaignCenterEmailCampaignsPage(driver);

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().clickElement(driver, emailCampaignPage.btnConfirm);
				if (fc.utobj().assertLinkText(driver, "Show All")) {
					fc.utobj().clickLink(driver, "Show All");
				}
				WebElement lnkFrachisee = fc.utobj().getElementByXpath(driver, ".//*[@id='printReady']//*[text()='"
						+ campaignTitle + "']//following::tr//tr[2]//a[contains(text() , '1')]");

				if (fc.utobj().getText(driver, lnkFrachisee).contains("1")) {
					Reporter.log("Campaign has been associated with franchise location");
				} else {
					fc.utobj().throwsException("Some problem occured while associating the location with campaign.");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj()
					.throwsSkipException("Error in associating the campaign with location , please refer screenshot!");
		}
	}

	public void chooseCampaign(WebDriver driver, String campaignTitle) throws Exception {
		Reporter.log("********************** Associate with location ***********************");

		String testCaseId = "TC_InfoMgr_CampaignCenter_Choose_Campaign";
		InfoMgrCampaignCenterEmailCampaignsPage emailCampaignPage = new InfoMgrCampaignCenterEmailCampaignsPage(driver);

		WebDriverWait wait = new WebDriverWait(driver, 60);

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				WebElement rdoCampaignID = fc.utobj().getElementByXpath(driver, "(.//*[@id='printReady']//a[text()='"
						+ campaignTitle + "']//ancestor::table[3]//tr[1]/td)[1]//input[@type='radio']");
				fc.utobj().clickElement(driver, rdoCampaignID);
				fc.utobj().clickElement(driver, emailCampaignPage.btnSelectandContinue);
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in choosing the campaign , please refer screenshot!");
		}
	}

	public String addGraphicalEmailTemplate(WebDriver driver) throws Exception {
		Reporter.log("********************** Create a graphical template ***********************");

		String testCaseId = "TC_InfoMgr_CampaignCenter_CreateCampaign_With_Existing_Graphical_HTML_Plain_Templates";
		Map<String, String> dsEmailTemplate = fc.utobj().readTestData("infomgr", testCaseId);
		String templateTitle = fc.utobj().generateTestData(dsEmailTemplate.get("templateTitle"));

		InfoMgrCampaignCenterEmailTemplatesPage emailTEmplate = new InfoMgrCampaignCenterEmailTemplatesPage(driver);

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().clickElement(driver, emailTEmplate.addTemplateBtn);
				fc.utobj().sendKeys(driver, emailTEmplate.titleTxt, templateTitle);
				fc.utobj().sendKeys(driver, emailTEmplate.mailSubject, dsEmailTemplate.get("templateSubject"));
				fc.utobj().switchFrameById(driver, "ta_ifr");
				fc.utobj().sendKeys(driver, emailTEmplate.mailTextbody, dsEmailTemplate.get("templateText"));
				fc.utobj().switchFrameToDefault(driver);

				fc.utobj().clickElement(driver, emailTEmplate.attachment);
				fc.utobj().switchFrameById(driver, "cboxIframe");
				fc.utobj().sendKeys(driver, emailTEmplate.choosefile,
						"C:\\Selenium_Test_Input\\testData\\document\\financeDoc.pdf");
				fc.utobj().clickElement(driver, emailTEmplate.attach);
				fc.utobj().clickElement(driver, emailTEmplate.done);
				fc.utobj().switchFrameToDefault(driver);

				fc.utobj().clickElement(driver, emailTEmplate.addBtn);

				if (fc.utobj().assertLinkText(driver, "Show All")) {
					fc.utobj().clickLink(driver, "Show All");
				}

				boolean isTemplateTitle = fc.utobj().assertLinkText(driver, templateTitle);
				if (isTemplateTitle) {
					Reporter.log("Email Template created successfully !!!");
				} else {
					fc.utobj().throwsException("Error in creating email template !!!");
				}

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in creating email template , please refer screenshot!");
		}
		return templateTitle;

	}

	public String addHTMLEmailTemplate(WebDriver driver) throws Exception {
		Reporter.log("********************** Create a graphical template ***********************");

		String testCaseId = "TC_InfoMgr_CampaignCenter_CreateCampaign_With_Existing_Graphical_HTML_Plain_Templates";
		Map<String, String> dsEmailTemplate = fc.utobj().readTestData("infomgr", testCaseId);
		String templateTitle = fc.utobj().generateTestData(dsEmailTemplate.get("templateTitle"));

		InfoMgrCampaignCenterEmailTemplatesPage emailTEmplate = new InfoMgrCampaignCenterEmailTemplatesPage(driver);

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().clickElement(driver, emailTEmplate.addTemplateBtn);
				fc.utobj().sendKeys(driver, emailTEmplate.titleTxt, templateTitle);
				fc.utobj().sendKeys(driver, emailTEmplate.mailSubject, dsEmailTemplate.get("templateSubject"));
				fc.utobj().clickElement(driver, emailTEmplate.htmlrdo);
				fc.utobj().sendKeys(driver, emailTEmplate.choosefilehtml,
						"C:\\Selenium_Test_Input\\testData\\document\\test.zip");
				fc.utobj().clickElement(driver, emailTEmplate.addhtml);
				fc.utobj().clickElement(driver, emailTEmplate.uploadhtml);

				if (fc.utobj().assertLinkText(driver, "Show All")) {
					fc.utobj().clickLink(driver, "Show All");
				}

				boolean isTemplateTitle = fc.utobj().assertLinkText(driver, templateTitle);
				if (isTemplateTitle) {
					Reporter.log("Email Template created successfully !!!");
				} else {
					fc.utobj().throwsException("Error in creating email template !!!");
				}

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in creating email template , please refer screenshot!");
		}
		return templateTitle;
	}

	public String addTextEmailTemplate(WebDriver driver) throws Exception {
		Reporter.log("********************** Create a text template ***********************");

		String testCaseId = "TC_InfoMgr_CampaignCenter_EmailTemplate_Text_Add";
		Map<String, String> dsEmailTemplate = fc.utobj().readTestData("infomgr", testCaseId);
		String templateTitle = fc.utobj().generateTestData(dsEmailTemplate.get("templateTitle"));

		InfoMgrCampaignCenterEmailTemplatesPage emailTEmplate = new InfoMgrCampaignCenterEmailTemplatesPage(driver);

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().clickElement(driver, emailTEmplate.addTemplateBtn);
				fc.utobj().sendKeys(driver, emailTEmplate.titleTxt, templateTitle);
				fc.utobj().sendKeys(driver, emailTEmplate.mailSubject, dsEmailTemplate.get("templateSubject"));
				fc.utobj().clickRadioButton(driver, emailTEmplate.rdoTypeEmailTemplate,
						dsEmailTemplate.get("typeTemplate"));
				fc.utobj().sendKeys(driver, emailTEmplate.txtContent, dsEmailTemplate.get("templateText"));
				fc.utobj().clickElement(driver, emailTEmplate.addBtn);

				if (fc.utobj().assertLinkText(driver, "Show All")) {
					fc.utobj().clickLink(driver, "Show All");
				}

				boolean isTemplateTitle = fc.utobj().assertLinkText(driver, templateTitle);
				if (isTemplateTitle) {
					Reporter.log("Email Template created successfully !!!");
				} else {
					fc.utobj().throwsException("Error in creating email template !!!");
				}

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in creating email template , please refer screenshot!");
		}
		return templateTitle;
	}

	public String createCampaignWithExistingTemplate(WebDriver driver, ArrayList<String> lstEmailTemplates)
			throws Exception {
		Reporter.log("********************** Create a Campaign with exiting templates ***********************");

		String testCaseId = "TC_InfoMgr_CampaignCenter_CreateCampaign_Existing_Template";
		Map<String, String> dsCampaignCenter = fc.utobj().readTestData("infomgr", testCaseId);
		String campaignName = fc.utobj().generateTestData(dsCampaignCenter.get("campaignName"));
		WebDriverWait wait = new WebDriverWait(driver, 60);

		InfoMgrCampaignCenterPage campaignCenterPage = new InfoMgrCampaignCenterPage(driver);
		InfoMgrCampaignCenterAddCampaignPage addCampaignPage = new InfoMgrCampaignCenterAddCampaignPage(driver);

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().clickElement(driver, campaignCenterPage.btnCreateCampaign);
				fc.utobj().sendKeys(driver, addCampaignPage.txtCampaignName, campaignName);
				fc.utobj().sendKeys(driver, addCampaignPage.txtCampaignDescription,
						dsCampaignCenter.get("campaignDesc"));
				fc.utobj().clickElement(driver, addCampaignPage.rdoPromotionalCampaign);
				fc.utobj().clickElement(driver, addCampaignPage.rdoExistingTemplate);
				fc.utobj().clickElement(driver, addCampaignPage.lnkAssociateTemplate);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				// fc.utobj().sendKeys(driver, addCampaignPage.txtSearch, );

				WebElement chkboxTemplate1 = fc.utobj().getElementByXpath(driver,
						".//*[@id='templateTable']//*[text()='" + lstEmailTemplates.get(0)
								+ "']/preceding::input[@type='checkbox'][1]");
				WebElement chkboxTemplate2 = fc.utobj().getElementByXpath(driver,
						".//*[@id='templateTable']//*[text()='" + lstEmailTemplates.get(1)
								+ "']/preceding::input[@type='checkbox'][1]");

				fc.utobj().clickElement(driver, chkboxTemplate1);
				fc.utobj().clickElement(driver, chkboxTemplate2);
				fc.utobj().clickElement(driver, addCampaignPage.btnAdd);

				fc.utobj().switchFrameToDefault(driver);

				fc.utobj().clickRadioButton(driver, addCampaignPage.rdoFromHeaderEmail,
						dsCampaignCenter.get("fromEmail"));
				fc.utobj().clickRadioButton(driver, addCampaignPage.rdoFromHeaderEmail,
						dsCampaignCenter.get("replyEmail"));
				fc.utobj().sendKeys(driver, addCampaignPage.txtEndDate, fc.utobj().getFutureDateUSFormat(20));
				fc.utobj().clickElement(driver, addCampaignPage.btnCreate);

				if (fc.utobj().assertLinkText(driver, "Show All")) {
					fc.utobj().clickLink(driver, "Show All");
				}
				boolean isCampaignOnPage = fc.utobj().assertLinkText(driver, campaignName);
				if (isCampaignOnPage) {
					Reporter.log("Campaign created successfully !!!");
				} else {
					fc.utobj().throwsException("Error in creating campaign !!!");
				}

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in creating campaign , please refer screenshot!");
		}
		return campaignName;
	}

	public String createCampaignWithallExistingTemplate(WebDriver driver, ArrayList<String> lstEmailTemplates)
			throws Exception {
		Reporter.log("********************** Create a Campaign with exiting templates ***********************");

		String testCaseId = "TC_InfoMgr_CampaignCenter_CreateCampaign_Existing_Template";
		Map<String, String> dsCampaignCenter = fc.utobj().readTestData("infomgr", testCaseId);
		String campaignName = fc.utobj().generateTestData(dsCampaignCenter.get("campaignName"));
		WebDriverWait wait = new WebDriverWait(driver, 60);

		InfoMgrCampaignCenterPage campaignCenterPage = new InfoMgrCampaignCenterPage(driver);
		InfoMgrCampaignCenterAddCampaignPage addCampaignPage = new InfoMgrCampaignCenterAddCampaignPage(driver);

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().clickElement(driver, campaignCenterPage.btnCreateCampaign);
				fc.utobj().sendKeys(driver, addCampaignPage.txtCampaignName, campaignName);
				fc.utobj().sendKeys(driver, addCampaignPage.txtCampaignDescription,
						dsCampaignCenter.get("campaignDesc"));
				fc.utobj().clickElement(driver, addCampaignPage.rdoPromotionalCampaign);
				fc.utobj().clickElement(driver, addCampaignPage.rdoExistingTemplate);
				fc.utobj().clickElement(driver, addCampaignPage.lnkAssociateTemplate);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				// fc.utobj().sendKeys(driver, addCampaignPage.txtSearch, );

				WebElement chkboxTemplate1 = fc.utobj().getElementByXpath(driver,
						".//*[@id='templateTable']//*[text()='" + lstEmailTemplates.get(0)
								+ "']/preceding::input[@type='checkbox'][1]");
				WebElement chkboxTemplate2 = fc.utobj().getElementByXpath(driver,
						".//*[@id='templateTable']//*[text()='" + lstEmailTemplates.get(1)
								+ "']/preceding::input[@type='checkbox'][1]");
				WebElement chkboxTemplate3 = fc.utobj().getElementByXpath(driver,
						".//*[@id='templateTable']//*[text()='" + lstEmailTemplates.get(2)
								+ "']/preceding::input[@type='checkbox'][1]");

				fc.utobj().clickElement(driver, chkboxTemplate1);
				fc.utobj().clickElement(driver, chkboxTemplate2);
				fc.utobj().clickElement(driver, chkboxTemplate3);
				fc.utobj().clickElement(driver, addCampaignPage.btnAdd);

				fc.utobj().switchFrameToDefault(driver);

				fc.utobj().clickRadioButton(driver, addCampaignPage.rdoFromHeaderEmail,
						dsCampaignCenter.get("fromEmail"));
				fc.utobj().clickRadioButton(driver, addCampaignPage.rdoFromHeaderEmail,
						dsCampaignCenter.get("replyEmail"));
				fc.utobj().sendKeys(driver, addCampaignPage.txtEndDate, fc.utobj().getFutureDateUSFormat(20));
				fc.utobj().clickElement(driver, addCampaignPage.btnCreate);

				if (fc.utobj().assertLinkText(driver, "Show All")) {
					fc.utobj().clickLink(driver, "Show All");
				}
				boolean isCampaignOnPage = fc.utobj().assertLinkText(driver, campaignName);
				if (isCampaignOnPage) {
					Reporter.log("Campaign created successfully !!!");
				} else {
					fc.utobj().throwsException("Error in creating campaign !!!");
				}

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in creating campaign , please refer screenshot!");
		}
		return campaignName;
	}

	public void CampaignCenterPageShowAll(WebDriver driver) throws Exception {
		gotoCampaignCenterDashboard(driver);
		if (fc.utobj().assertLinkText(driver, "Show All")) {
			fc.utobj().clickLink(driver, "Show All");
		}
	}

	public void gotoCampaignCenterDashboard(WebDriver driver) throws Exception {
		fc.infomgr().infomgr_common().InfoMgrCampaignCenter(driver);
		InfoMgrCampaignCenterPage campaignCenterPage = new InfoMgrCampaignCenterPage(driver);
		fc.utobj().clickElement(driver, campaignCenterPage.lnkCampaignDashboard);
	}

	public void gotoCampaignCenterEmailTemplate(WebDriver driver) throws Exception {
		fc.infomgr().infomgr_common().InfoMgrCampaignCenter(driver);
		InfoMgrCampaignCenterPage campaignCenterPage = new InfoMgrCampaignCenterPage(driver);
		fc.utobj().clickElement(driver, campaignCenterPage.lnkEmailTemplates);
	}

	public void checkLocation(WebDriver driver, String location) throws Exception {
		String xpath = ".//*[@id='templateTable']//a[text()='" + location + "']/preceding::input[@type='checkbox'][1]";
		WebElement checkBox = fc.utobj().getElementByXpath(driver, xpath);
		fc.utobj().clickElement(driver, checkBox);
	}

	public List<String> CopyAndTestEmailTemplate(WebDriver driver) throws Exception {
		Reporter.log("********************** Create a text template ***********************");

		String testCaseId = "TC_InfoMgr_CampaignCenter_Center_Copy_Test_EmailTemplate";
		Map<String, String> dsEmailTemplate = fc.utobj().readTestData("infomgr", testCaseId);
		String templateTitle = fc.utobj().generateTestData(dsEmailTemplate.get("templateTitle"));
		String copyTemplate = fc.utobj().generateTestData(dsEmailTemplate.get("CopyTemplateTitle"));
		String subject = fc.utobj().generateTestData(dsEmailTemplate.get("copytemplateSubject"));
		List<String> list = new ArrayList<String>();
		InfoMgrCampaignCenterEmailTemplatesPage emailTEmplate = new InfoMgrCampaignCenterEmailTemplatesPage(driver);

		if (fc.utobj().validate(testCaseId) == true) {
			try {

				fc.utobj().clickElement(driver, emailTEmplate.addTemplateBtn);
				fc.utobj().sendKeys(driver, emailTEmplate.titleTxt, templateTitle);
				fc.utobj().sendKeys(driver, emailTEmplate.mailSubject, dsEmailTemplate.get("templateSubject"));
				fc.utobj().clickElement(driver, emailTEmplate.textrdo);
				fc.utobj().sendKeys(driver, emailTEmplate.txtContent, dsEmailTemplate.get("templateText"));
				fc.utobj().clickElement(driver, emailTEmplate.addBtn);

				fc.utobj().clickElement(driver, emailTEmplate.actionButton);
				fc.utobj().clickElement(driver, emailTEmplate.copy);

				String templateText = dsEmailTemplate.get("copytemplateText");
				fc.utobj().sendKeys(driver, emailTEmplate.Title, copyTemplate);
				fc.utobj().sendKeys(driver, emailTEmplate.Subject, subject);
				fc.utobj().sendKeys(driver, emailTEmplate.txtContent, templateText);
				fc.utobj().clickElement(driver, emailTEmplate.attachment);
				fc.utobj().switchFrameById(driver, "cboxIframe");
				fc.utobj().sendKeys(driver, emailTEmplate.choosefile,
						"C:\\Selenium_Test_Input\\testData\\document\\financeDoc.pdf");
				fc.utobj().clickElement(driver, emailTEmplate.attach);
				fc.utobj().clickElement(driver, emailTEmplate.done);
				fc.utobj().switchFrameToDefault(driver);
				fc.utobj().clickElement(driver, emailTEmplate.addhtml);
				fc.utobj().clickElement(driver, emailTEmplate.actionButton);
				fc.utobj().clickElement(driver, emailTEmplate.testTemplate);
				fc.utobj().switchFrameById(driver, "cboxIframe");
				fc.utobj().sendKeys(driver, emailTEmplate.email, "infomgrautomation@staffex.com");
				fc.utobj().clickElement(driver, emailTEmplate.testButton);
				fc.utobj().clickElement(driver, emailTEmplate.okbtn);
				fc.utobj().switchFrameToDefault(driver);

				if (fc.utobj().assertLinkText(driver, "Show All")) {
					fc.utobj().clickLink(driver, "Show All");
				}

				boolean isTemplateTitle = fc.utobj().assertLinkText(driver, copyTemplate);
				if (isTemplateTitle) {
					Reporter.log("Email Template created successfully !!!");
				} else {
					fc.utobj().throwsException("Error in creating email template !!!");
				}

				list.add(copyTemplate);
				list.add(subject);
				list.add(templateText);
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in creating email template , please refer screenshot!");
		}
		return list;
	}

	public List<String> AddAndModifyEmailTemplate(WebDriver driver) throws Exception {
		Reporter.log("********************** Create a text template ***********************");

		String testCaseId = "TC_InfoMgr_CampaignCenter_Add_Modify_EmailTemplate";
		Map<String, String> dsEmailTemplate = fc.utobj().readTestData("infomgr", testCaseId);
		String templateTitle = fc.utobj().generateTestData(dsEmailTemplate.get("templateTitle"));
		String modifyTemplateTitle = fc.utobj().generateTestData(dsEmailTemplate.get("modifyTemplateTitle"));
		String subject = fc.utobj().generateTestData(dsEmailTemplate.get("modifytemplateSubject"));
		List<String> list = new ArrayList<String>();
		InfoMgrCampaignCenterEmailTemplatesPage emailTEmplate = new InfoMgrCampaignCenterEmailTemplatesPage(driver);

		if (fc.utobj().validate(testCaseId) == true) {
			try {

				fc.utobj().clickElement(driver, emailTEmplate.addTemplateBtn);
				fc.utobj().sendKeys(driver, emailTEmplate.titleTxt, templateTitle);
				fc.utobj().sendKeys(driver, emailTEmplate.mailSubject, dsEmailTemplate.get("templateSubject"));
				fc.utobj().clickElement(driver, emailTEmplate.textrdo);
				fc.utobj().sendKeys(driver, emailTEmplate.txtContent, dsEmailTemplate.get("plaintemplateText"));
				fc.utobj().clickElement(driver, emailTEmplate.addBtn);

				fc.utobj().clickElement(driver, emailTEmplate.actionButton);
				fc.utobj().clickElement(driver, emailTEmplate.modify);

				String templateText = dsEmailTemplate.get("templateText");
				fc.utobj().sendKeys(driver, emailTEmplate.Title, modifyTemplateTitle);
				fc.utobj().sendKeys(driver, emailTEmplate.Subject, subject);
				fc.utobj().sendKeys(driver, emailTEmplate.txtContent, templateText);
				fc.utobj().clickElement(driver, emailTEmplate.attachment);
				fc.utobj().switchFrameById(driver, "cboxIframe");
				fc.utobj().sendKeys(driver, emailTEmplate.choosefile,
						"C:\\Selenium_Test_Input\\testData\\document\\financeDoc.pdf");
				fc.utobj().clickElement(driver, emailTEmplate.attach);
				fc.utobj().clickElement(driver, emailTEmplate.done);
				fc.utobj().switchFrameToDefault(driver);
				fc.utobj().clickElement(driver, emailTEmplate.modifybtn);
				fc.utobj().clickElement(driver, emailTEmplate.actionButton);
				fc.utobj().clickElement(driver, emailTEmplate.testTemplate);
				fc.utobj().switchFrameById(driver, "cboxIframe");
				fc.utobj().sendKeys(driver, emailTEmplate.email, "infomgrautomation@staffex.com");
				fc.utobj().clickElement(driver, emailTEmplate.testButton);
				fc.utobj().clickElement(driver, emailTEmplate.okbtn);
				fc.utobj().switchFrameToDefault(driver);

				if (fc.utobj().assertLinkText(driver, "Show All")) {
					fc.utobj().clickLink(driver, "Show All");
				}

				boolean isTemplateTitle = fc.utobj().assertLinkText(driver, modifyTemplateTitle);
				if (isTemplateTitle) {
					Reporter.log("Email Template created successfully !!!");
				} else {
					fc.utobj().throwsException("Error in creating email template !!!");
				}

				list.add(modifyTemplateTitle);
				list.add(subject);
				list.add(templateText);
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in creating email template , please refer screenshot!");
		}
		return list;
	}

	public List<String> AddCopyDeleteTestEmailTemplate(WebDriver driver) throws Exception {
		Reporter.log("********************** Create a text template ***********************");

		String testCaseId = "TC_InfoMgr_CampaignCenter_Center_Copy_Test_EmailTemplate";
		Map<String, String> dsEmailTemplate = fc.utobj().readTestData("infomgr", testCaseId);
		String templateTitle = fc.utobj().generateTestData(dsEmailTemplate.get("templateTitle"));
		String copyTemplate = fc.utobj().generateTestData(dsEmailTemplate.get("CopyTemplateTitle"));
		String subject = fc.utobj().generateTestData(dsEmailTemplate.get("copytemplateSubject"));
		List<String> list = new ArrayList<String>();
		InfoMgrCampaignCenterEmailTemplatesPage emailTEmplate = new InfoMgrCampaignCenterEmailTemplatesPage(driver);

		if (fc.utobj().validate(testCaseId) == true) {
			try {

				fc.utobj().clickElement(driver, emailTEmplate.addTemplateBtn);
				fc.utobj().sendKeys(driver, emailTEmplate.titleTxt, templateTitle);
				fc.utobj().sendKeys(driver, emailTEmplate.mailSubject, dsEmailTemplate.get("templateSubject"));
				fc.utobj().clickElement(driver, emailTEmplate.textrdo);
				fc.utobj().sendKeys(driver, emailTEmplate.txtContent, dsEmailTemplate.get("templateText"));
				fc.utobj().clickElement(driver, emailTEmplate.addBtn);

				fc.utobj().clickElement(driver, emailTEmplate.actionButton);
				fc.utobj().clickElement(driver, emailTEmplate.copy);

				String templateText = dsEmailTemplate.get("copytemplateText");
				fc.utobj().sendKeys(driver, emailTEmplate.Title, copyTemplate);
				fc.utobj().sendKeys(driver, emailTEmplate.Subject, subject);
				fc.utobj().sendKeys(driver, emailTEmplate.txtContent, templateText);
				fc.utobj().clickElement(driver, emailTEmplate.attachment);
				fc.utobj().switchFrameById(driver, "cboxIframe");
				fc.utobj().sendKeys(driver, emailTEmplate.choosefile,
						"C:\\Selenium_Test_Input\\testData\\document\\financeDoc.pdf");
				fc.utobj().clickElement(driver, emailTEmplate.attach);
				fc.utobj().clickElement(driver, emailTEmplate.done);
				fc.utobj().switchFrameToDefault(driver);
				fc.utobj().clickElement(driver, emailTEmplate.addhtml);
				fc.utobj().clickElement(driver, emailTEmplate.actionButton);
				fc.utobj().clickElement(driver, emailTEmplate.delete);
				fc.utobj().acceptAlertBox(driver);

				if (fc.utobj().assertLinkText(driver, "Show All")) {
					fc.utobj().clickLink(driver, "Show All");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in creating email template , please refer screenshot!");
		}
		return list;
	}

	/*
	 * public void openInfoMgrCampaignCenterLnk(WebDriver driver) throws
	 * Exception {
	 * 
	 * 
	 * fc.nav().fimModule(driver); InfoMgrModulePage pobj = new
	 * InfoMgrModulePage(driver); Thread.sleep(2500);
	 * fc.utobj().clickElement(driver,pobj.campaignCenterTab); }
	 * 
	 * public void openInfoMgrSearch(WebDriver driver) throws Exception {
	 * 
	 * 
	 * fc.nav().fimModule(driver); InfoMgrModulePage pobj = new
	 * InfoMgrModulePage(driver); Thread.sleep(2500);
	 * fc.utobj().clickElement(driver,pobj.searchTab); }
	 * 
	 * 
	 * 
	 * public void openInfoMgrAddressesTab(WebDriver driver) throws Exception {
	 * 
	 * 
	 * InfoMgrModulePage pobj = new InfoMgrModulePage(driver); try{
	 * fc.utobj().clickLink(driver, "Addresses"); }catch(Exception e){
	 * fc.utobj().moveToElement(driver, pobj.moreBtn);
	 * fc.utobj().clickLink(driver, "Addresses"); } }
	 * 
	 * public void openInfoMgrAgreementTab(WebDriver driver) throws Exception {
	 * 
	 * InfoMgrModulePage pobj = new InfoMgrModulePage(driver); try{
	 * fc.utobj().clickLink(driver, "Agreement"); }catch(Exception e){
	 * fc.utobj().moveToElement(driver, pobj.moreBtn);
	 * fc.utobj().clickLink(driver, "Agreement"); } }
	 * 
	 * public void openInfoMgrCenterInfoTab(WebDriver driver) throws Exception {
	 * 
	 * InfoMgrModulePage pobj = new InfoMgrModulePage(driver); try{
	 * fc.utobj().clickLink(driver, "Center Info"); }catch(Exception e){
	 * fc.utobj().moveToElement(driver, pobj.moreBtn);
	 * fc.utobj().clickLink(driver, "Center Info"); } }
	 * 
	 * public void openInfoMgrContactHistoryTab(WebDriver driver) throws
	 * Exception {
	 * 
	 * InfoMgrModulePage pobj = new InfoMgrModulePage(driver); try{
	 * fc.utobj().clickLink(driver, "Contact History"); }catch(Exception e){
	 * fc.utobj().moveToElement(driver, pobj.moreBtn);
	 * fc.utobj().clickLink(driver, "Contact History"); } }
	 * 
	 * public void openInfoMgrContractSigningTab(WebDriver driver) throws
	 * Exception {
	 * 
	 * InfoMgrModulePage pobj = new InfoMgrModulePage(driver); try{
	 * fc.utobj().clickLink(driver, "Contract Signing"); }catch(Exception e){
	 * fc.utobj().moveToElement(driver, pobj.moreBtn);
	 * fc.utobj().clickLink(driver, "Contract Signing"); } }
	 * 
	 * public void openInfoMgrCustomerComplaintsTab(WebDriver driver) throws
	 * Exception {
	 * 
	 * InfoMgrModulePage pobj = new InfoMgrModulePage(driver); try{
	 * fc.utobj().clickLink(driver, "Customer Complaints"); }catch(Exception e){
	 * fc.utobj().moveToElement(driver, pobj.moreBtn);
	 * fc.utobj().clickLink(driver, "Customer Complaints"); } }
	 * 
	 * public void openInfoMgrDefaultandTerminationTab(WebDriver driver) throws
	 * Exception {
	 * 
	 * InfoMgrModulePage pobj = new InfoMgrModulePage(driver); try{
	 * fc.utobj().clickLink(driver, "Default and Termination"); }catch(Exception
	 * e){ fc.utobj().moveToElement(driver, pobj.moreBtn);
	 * fc.utobj().clickLink(driver, "Default and Termination"); } }
	 * 
	 * public void openInfoMgrDocumentsTab(WebDriver driver) throws Exception {
	 * 
	 * InfoMgrModulePage pobj = new InfoMgrModulePage(driver); try{
	 * fc.utobj().clickLink(driver, "Documents"); }catch(Exception e){
	 * fc.utobj().moveToElement(driver, pobj.moreBtn);
	 * fc.utobj().clickLink(driver, "Documents"); } }
	 * 
	 * public void openInfoMgrEmployeesTab(WebDriver driver) throws Exception {
	 * 
	 * InfoMgrModulePage pobj = new InfoMgrModulePage(driver); try{
	 * fc.utobj().clickLink(driver, "Employees"); }catch(Exception e){
	 * fc.utobj().moveToElement(driver, pobj.moreBtn);
	 * fc.utobj().clickLink(driver, "Employees"); } }
	 * 
	 * public void openInfoMgrEntityDetailTab(WebDriver driver) throws Exception
	 * {
	 * 
	 * InfoMgrModulePage pobj = new InfoMgrModulePage(driver); try{
	 * fc.utobj().clickLink(driver, "Entity Detail"); }catch(Exception e){
	 * fc.utobj().moveToElement(driver, pobj.moreBtn);
	 * fc.utobj().clickLink(driver, "Entity Detail"); } }
	 * 
	 * public void openInfoMgrEventsTab(WebDriver driver) throws Exception {
	 * 
	 * InfoMgrModulePage pobj = new InfoMgrModulePage(driver); try{
	 * fc.utobj().clickLink(driver, "Events"); }catch(Exception e){
	 * fc.utobj().moveToElement(driver, pobj.moreBtn);
	 * fc.utobj().clickLink(driver, "Events"); } }
	 * 
	 * public void openInfoMgrFinancialTab(WebDriver driver) throws Exception {
	 * 
	 * InfoMgrModulePage pobj = new InfoMgrModulePage(driver); try{
	 * fc.utobj().clickLink(driver, "Financial"); }catch(Exception e){
	 * fc.utobj().moveToElement(driver, pobj.moreBtn);
	 * fc.utobj().clickLink(driver, "Financial"); } }
	 * 
	 * public void openInfoMgrGuarantorsTab(WebDriver driver) throws Exception {
	 * 
	 * InfoMgrModulePage pobj = new InfoMgrModulePage(driver); try{
	 * fc.utobj().clickLink(driver, "Guarantors"); }catch(Exception e){
	 * fc.utobj().moveToElement(driver, pobj.moreBtn);
	 * fc.utobj().clickLink(driver, "Guarantors"); } }
	 * 
	 * public void openInfoMgrInsuranceTab(WebDriver driver) throws Exception {
	 * 
	 * InfoMgrModulePage pobj = new InfoMgrModulePage(driver); try{
	 * fc.utobj().clickLink(driver, "Insurance"); }catch(Exception e){
	 * fc.utobj().moveToElement(driver, pobj.moreBtn);
	 * fc.utobj().clickLink(driver, "Insurance"); } }
	 * 
	 * public void openInfoMgrLegalViolationTab(WebDriver driver) throws
	 * Exception {
	 * 
	 * InfoMgrModulePage pobj = new InfoMgrModulePage(driver); try{
	 * fc.utobj().clickLink(driver, "Legal Violation"); }catch(Exception e){
	 * fc.utobj().moveToElement(driver, pobj.moreBtn);
	 * fc.utobj().clickLink(driver, "Legal Violation"); } }
	 * 
	 * public void openInfoMgrLendersTab(WebDriver driver) throws Exception {
	 * 
	 * InfoMgrModulePage pobj = new InfoMgrModulePage(driver); try{
	 * fc.utobj().clickLink(driver, "Lenders"); }catch(Exception e){
	 * fc.utobj().moveToElement(driver, pobj.moreBtn);
	 * fc.utobj().clickLink(driver, "Lenders"); } }
	 * 
	 * public void openInfoMgrMarketingTab(WebDriver driver) throws Exception {
	 * 
	 * InfoMgrModulePage pobj = new InfoMgrModulePage(driver); try{
	 * fc.utobj().clickLink(driver, "Marketing"); }catch(Exception e){
	 * fc.utobj().moveToElement(driver, pobj.moreBtn);
	 * fc.utobj().clickLink(driver, "Marketing"); } }
	 * 
	 * public void openInfoMgrMysteryReviewTab(WebDriver driver) throws
	 * Exception {
	 * 
	 * InfoMgrModulePage pobj = new InfoMgrModulePage(driver); try{
	 * fc.utobj().clickLink(driver, "Mystery Review"); }catch(Exception e){
	 * fc.utobj().moveToElement(driver, pobj.moreBtn);
	 * fc.utobj().clickLink(driver, "Mystery Review"); } }
	 * 
	 * public void openInfoMgrOwnersTab(WebDriver driver) throws Exception {
	 * 
	 * InfoMgrModulePage pobj = new InfoMgrModulePage(driver); try{
	 * fc.utobj().clickLink(driver, "Owners"); }catch(Exception e){
	 * fc.utobj().moveToElement(driver, pobj.moreBtn);
	 * fc.utobj().clickLink(driver, "Owners"); } }
	 * 
	 * public void openInfoMgrPicturesTab(WebDriver driver) throws Exception {
	 * 
	 * InfoMgrModulePage pobj = new InfoMgrModulePage(driver); try{
	 * fc.utobj().clickLink(driver, "Pictures"); }catch(Exception e){
	 * fc.utobj().moveToElement(driver, pobj.moreBtn);
	 * fc.utobj().clickLink(driver, "Pictures"); } }
	 * 
	 * public void openInfoMgrPreviousFranchiseesTab(WebDriver driver) throws
	 * Exception {
	 * 
	 * InfoMgrModulePage pobj = new InfoMgrModulePage(driver); try{
	 * fc.utobj().clickLink(driver, "Previous Franchisees"); }catch(Exception
	 * e){ fc.utobj().moveToElement(driver, pobj.moreBtn);
	 * fc.utobj().clickLink(driver, "Previous Franchisees"); } }
	 * 
	 * public void openInfoMgrQAHistoryTab(WebDriver driver) throws Exception {
	 * 
	 * InfoMgrModulePage pobj = new InfoMgrModulePage(driver); try{
	 * fc.utobj().clickLink(driver, "QA History"); }catch(Exception e){
	 * fc.utobj().moveToElement(driver, pobj.moreBtn);
	 * fc.utobj().clickLink(driver, "QA History"); } } public void
	 * openInfoMgrRealEstateTab(WebDriver driver) throws Exception {
	 * 
	 * InfoMgrModulePage pobj = new InfoMgrModulePage(driver); try{
	 * fc.utobj().clickLink(driver, "Real Estate"); }catch(Exception e){
	 * fc.utobj().moveToElement(driver, pobj.moreBtn);
	 * fc.utobj().clickLink(driver, "Real Estate"); } } public void
	 * openInfoMgrRenewalTab(WebDriver driver) throws Exception {
	 * 
	 * InfoMgrModulePage pobj = new InfoMgrModulePage(driver); try{
	 * fc.utobj().clickLink(driver, "Renewal"); }catch(Exception e){
	 * fc.utobj().moveToElement(driver, pobj.moreBtn);
	 * fc.utobj().clickLink(driver, "Renewal"); } } public void
	 * openInfoMgrTerritoryTab(WebDriver driver) throws Exception {
	 * 
	 * InfoMgrModulePage pobj = new InfoMgrModulePage(driver); try{
	 * fc.utobj().clickLink(driver, "Territory"); }catch(Exception e){
	 * fc.utobj().moveToElement(driver, pobj.moreBtn);
	 * fc.utobj().clickLink(driver, "Territory"); } } public void
	 * openInfoMgrTrainingTab(WebDriver driver) throws Exception {
	 * 
	 * InfoMgrModulePage pobj = new InfoMgrModulePage(driver); try{
	 * fc.utobj().clickLink(driver, "Training"); }catch(Exception e){
	 * fc.utobj().moveToElement(driver, pobj.moreBtn);
	 * fc.utobj().clickLink(driver, "Training"); } } public void
	 * openInfoMgrTransferTab(WebDriver driver) throws Exception {
	 * 
	 * InfoMgrModulePage pobj = new InfoMgrModulePage(driver); try{
	 * fc.utobj().clickLink(driver, "Transfer"); }catch(Exception e){
	 * fc.utobj().moveToElement(driver, pobj.moreBtn);
	 * fc.utobj().clickLink(driver, "Transfer"); } } public void
	 * openInfoMgrUsersTab(WebDriver driver) throws Exception {
	 * 
	 * InfoMgrModulePage pobj = new InfoMgrModulePage(driver); try{
	 * fc.utobj().clickLink(driver, "Users"); }catch(Exception e){
	 * fc.utobj().moveToElement(driver, pobj.moreBtn);
	 * fc.utobj().clickLink(driver, "Users"); } } public void
	 * openInfoMgrEmailSummaryTab(WebDriver driver) throws Exception {
	 * 
	 * InfoMgrModulePage pobj = new InfoMgrModulePage(driver); try{
	 * fc.utobj().clickLink(driver, "Email Summary"); }catch(Exception e){
	 * fc.utobj().moveToElement(driver, pobj.moreBtn);
	 * fc.utobj().clickLink(driver, "Email Summary"); } }
	 * 
	 * public void clickCreateCampaign(WebDriver driver) throws Exception {
	 * 
	 * openInfoMgrCampaignCenterLnk(driver); InfoMgrCampaignCenterPage pobj2 =
	 * new InfoMgrCampaignCenterPage(driver);
	 * fc.utobj().clickElement(driver,pobj2.createCampaignBtn); }
	 * 
	 * 
	 * public void clickCampaignDashboard(WebDriver driver) throws Exception {
	 * 
	 * 
	 * openInfoMgrCampaignCenterLnk(driver); InfoMgrCampaignCenterPage pobj =
	 * new InfoMgrCampaignCenterPage(driver);
	 * fc.utobj().clickElement(driver,pobj.campainDashboardBtn); }
	 */
}
