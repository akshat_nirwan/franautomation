package com.builds.test.infomgr;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.time.DateUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;

import com.builds.test.fieldops.AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest;
import com.builds.test.fieldops.AdminFieldOpsManageVisitFormPageTest;
import com.builds.test.fieldops.AdminFieldOpsQuestionLibraryPageTest;
import com.builds.uimaps.fieldops.FieldOpsVisitsPage;
import com.builds.uimaps.infomgr.InfoMgrAddToGroupsPge;
import com.builds.uimaps.infomgr.InfoMgrAddressBook;
import com.builds.uimaps.infomgr.InfoMgrAddressesPage;
import com.builds.uimaps.infomgr.InfoMgrAgreementPage;
import com.builds.uimaps.infomgr.InfoMgrCenterInfoPage;
import com.builds.uimaps.infomgr.InfoMgrContactHistoryPage;
import com.builds.uimaps.infomgr.InfoMgrContractSigningPage;
import com.builds.uimaps.infomgr.InfoMgrCorporateLocationsPage;
import com.builds.uimaps.infomgr.InfoMgrCustomTabPage;
import com.builds.uimaps.infomgr.InfoMgrCustomerComplaintsPage;
import com.builds.uimaps.infomgr.InfoMgrDefaultAndTerminationPage;
import com.builds.uimaps.infomgr.InfoMgrDocumentsPage;
import com.builds.uimaps.infomgr.InfoMgrEmployeesPage;
import com.builds.uimaps.infomgr.InfoMgrEntityDetailsPage;
import com.builds.uimaps.infomgr.InfoMgrEventsPage;
import com.builds.uimaps.infomgr.InfoMgrFinancialsPage;
import com.builds.uimaps.infomgr.InfoMgrFranchiseeFilterPage;
import com.builds.uimaps.infomgr.InfoMgrFranchiseesPage;
import com.builds.uimaps.infomgr.InfoMgrGroupsPage;
import com.builds.uimaps.infomgr.InfoMgrGuarantorsPage;
import com.builds.uimaps.infomgr.InfoMgrInsurancePage;
import com.builds.uimaps.infomgr.InfoMgrLegalViolationPage;
import com.builds.uimaps.infomgr.InfoMgrLendersPage;
import com.builds.uimaps.infomgr.InfoMgrLogACallPage;
import com.builds.uimaps.infomgr.InfoMgrLogATaskPage;
import com.builds.uimaps.infomgr.InfoMgrMarketingPage;
import com.builds.uimaps.infomgr.InfoMgrMystryReviewPage;
import com.builds.uimaps.infomgr.InfoMgrOwnersPage;
import com.builds.uimaps.infomgr.InfoMgrPicturesPage;
import com.builds.uimaps.infomgr.InfoMgrQAHistoryPage;
import com.builds.uimaps.infomgr.InfoMgrRealStatePage;
import com.builds.uimaps.infomgr.InfoMgrSendEmailPage;
import com.builds.uimaps.infomgr.InfoMgrSendMessagePage;
import com.builds.uimaps.infomgr.InfoMgrTerritoryPage;
import com.builds.uimaps.infomgr.InfoMgrTrainingPage;
import com.builds.uimaps.infomgr.InfoMgrTransferPage;
import com.builds.uimaps.infomgr.InfoMgrUserPage;
import com.builds.utilities.FranconnectUtil;

public class CorporateLocationsCommonMethods {
	public static String Renewal = "Renewal ";
	FranconnectUtil fc = new FranconnectUtil();

	// ******************************************************* Add Data
	// *******************************************************************

	public Map<String, String> addOwner(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("**** Adding Franchisee Owner ****");

		String testCaseID = "TC_InFoMgr_CorporateLocations_Add_Owner";

		Map<String, String> lstOwnerDetails = new HashMap<>(); // for storing
																// owner details

		InfoMgrOwnersPage objAddOwnersPage = new InfoMgrCorporateLocationsPage(driver).getOwnersPage();

		fc.utobj().clickElement(driver, objAddOwnersPage.lnkAddOwner);

		lstOwnerDetails.put("FirstName", fc.utobj().generateTestData("owner1"));
		lstOwnerDetails.put("LastName", fc.utobj().generateTestData("lastName"));
		lstOwnerDetails.put("Email", "testEmail@yahoo.com");
		lstOwnerDetails.put("Country", "USA");
		lstOwnerDetails.put("State", "Alabama");

		if (fc.utobj().validate(testCaseID) == true) {
			try {
				fc.utobj().sendKeys(driver, objAddOwnersPage.txtFirstName, lstOwnerDetails.get("FirstName"));
				fc.utobj().sendKeys(driver, objAddOwnersPage.txtLastName, lstOwnerDetails.get("LastName"));
				fc.utobj().sendKeys(driver, objAddOwnersPage.txtEmail, lstOwnerDetails.get("Email"));
				fc.utobj().selectDropDownByVisibleText(driver, objAddOwnersPage.drpHomeCountry, lstOwnerDetails.get("Country"));
				fc.utobj().selectDropDownByVisibleText(driver, objAddOwnersPage.drpHomeState, lstOwnerDetails.get("State"));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, objAddOwnersPage.btnAdd);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseID);
			}
		} else {
			fc.utobj().throwsSkipException("Error in adding owner , please refer screenshot!");
		}

		boolean ownerAddedFlag = fc.utobj().assertPageSource(driver, lstOwnerDetails.get("FirstName"));
		if (ownerAddedFlag) {
			Reporter.log("Owner added successfully !!!");
		} else {
			fc.utobj().throwsException("Error in adding owner !!!");
		}

		return lstOwnerDetails;
	}

	public String addExistingOwner(WebDriver driver, Map<String, String> config, String owner) throws Exception {
		Reporter.log("******************** Adding Existing Owner ************************* ");

		String testCaseId = "TC_InFoMgr_CorporateLocations_Add_Existing_Owner";

		WebDriverWait wait = new WebDriverWait(driver, 4);

		String multiUnitName = fc.utobj().generateTestData("Multi Unit 01");

		InfoMgrOwnersPage ownersPage = new InfoMgrCorporateLocationsPage(driver).getOwnersPage();

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().clickElement(driver, ownersPage.tabOwners);

				fc.utobj().clickElement(driver, ownersPage.lnkAddOwner);

				fc.utobj().clickRadioButton(driver, ownersPage.rdoExistingOwner, "existing");

				// Wait for existing owner section to load
				fc.utobj().selectValFromMultiSelect(driver, ownersPage.drpMultiCheckboxOwnerID, owner);

				fc.utobj().sendKeys(driver, ownersPage.txtMuidName, multiUnitName);
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, ownersPage.btnAdd);
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in adding existing owner , please refer screenshot!");
		}

		return multiUnitName;
	}

	public String addRemark(WebDriver driver, Map<String, String> config, Object remarksObj) throws Exception {
		Reporter.log("******************* Adding Remarks ********************** \n");

		String testCaseID = "TC_InFo_Mgr_CorporateLocations_Add_Remarks";

		fc.commonMethods().switch_cboxIframe_frameId(driver);
		String remark = fc.utobj().generateTestData("Test Remarks");

		if (fc.utobj().validate(testCaseID) == true) {
			try {
				if (remarksObj instanceof InfoMgrContactHistoryPage) {
					InfoMgrContactHistoryPage contactHistory = (InfoMgrContactHistoryPage) remarksObj;
					fc.utobj().sendKeys(driver, contactHistory.txtRemarks, remark);
					fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																					// for
																					// Custom
																					// Manadatory
																					// Fields
																					// Of
																					// Client
					fc.utobj().clickElement(driver, contactHistory.btnSubmit);
					fc.utobj().clickElement(driver, contactHistory.btnClose);
				} else if (remarksObj instanceof InfoMgrOwnersPage) {
					InfoMgrOwnersPage ownersPage = (InfoMgrOwnersPage) remarksObj;
					fc.utobj().sendKeys(driver, ownersPage.txtRemarks, remark);
					fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																					// for
																					// Custom
																					// Manadatory
																					// Fields
																					// Of
																					// Client
					fc.utobj().clickElement(driver, ownersPage.btnSubmit);
					fc.utobj().clickElement(driver, ownersPage.btnClose);
				}
				Thread.sleep(2000);

				boolean isRemarkOnPage = fc.utobj().assertPageSource(driver, remark);
				if (isRemarkOnPage) {
					Reporter.log("Remarks have been added successfully !!! ");
				} else {
					fc.utobj().throwsException("Some problem occured while adding remarks !!!!");
				}

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseID);
			}
		} else {
			fc.utobj().throwsSkipException("Error in adding remarks , please refer screenshot!");
		}

		return remark;
	}

	public void addTraining(WebDriver driver, Map<String, String> config, InfoMgrTrainingPage objAddTrainingPage) throws Exception {
		Reporter.log("********************** Add Training ****************** \n");

		String testCaseID = "TC_InfoMgr_CorporateLocations_Training_Add";

		Map<String, String> dsTraining = fc.utobj().readTestData("infomgr", testCaseID);

		// Click on training link
		fc.utobj().clickElement(driver, objAddTrainingPage.lnkTraining);

		// Check if the page has already training listed , if yes then click on
		// "Add More" button

		String tableText = fc.utobj().getText(driver, objAddTrainingPage.tblTrainings);
		if (tableText.contains("Additional Trainings")) {
			fc.utobj().clickElement(driver, objAddTrainingPage.btnAddMore);
		}
		String trainingProgram = fc.utobj().generateTestData(dsTraining.get("trainingProgram"));

		if (fc.utobj().validate(testCaseID) == true) {
			try {

				fc.utobj().sendKeys(driver, objAddTrainingPage.txtTrainingProgName, trainingProgram);
				fc.utobj().selectDropDownByPartialText(driver, objAddTrainingPage.drpTrainingType, dsTraining.get("trainingType"));
				fc.utobj().sendKeys(driver, objAddTrainingPage.txtAttendee, dsTraining.get("attendee"));
				fc.utobj().sendKeys(driver, objAddTrainingPage.txtCompletionDate, fc.utobj().getFutureDateUSFormat(10));
				fc.utobj().sendKeys(driver, objAddTrainingPage.txtGrade, dsTraining.get("grade"));
				fc.utobj().sendKeys(driver, objAddTrainingPage.txtInstructor, dsTraining.get("instructor"));
				fc.utobj().sendKeys(driver, objAddTrainingPage.txtLocation, dsTraining.get("location"));
				fc.utobj().sendKeys(driver, objAddTrainingPage.txtAttendeeTitle, dsTraining.get("attendeeTitle"));
				fc.utobj().sendKeys(driver, objAddTrainingPage.txtScore, dsTraining.get("score"));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, objAddTrainingPage.btnSubmit);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseID);
			}
		} else {
			fc.utobj().throwsSkipException("Error in adding training , please refer screenshot!");
		}

		// Verify that training has been added successfully

		String txtTraining = fc.utobj().getText(driver, objAddTrainingPage.tblTrainings);
		if (txtTraining.contains(trainingProgram)) {
			Reporter.log("Training created successfully");
		} else {
			fc.utobj().throwsException("Error - Training didn't got created. Test Case failes !!!");
		}
	}

	public String sendTextMessage(WebDriver driver, Map<String, String> config, boolean isFRomContactHistoryPage, String corporateUser) throws Exception {
		Reporter.log("******************* Sending Text Message ********************** \n");

		InfoMgrSendMessagePage messagePage = new InfoMgrCorporateLocationsPage(driver).getSendMessagePage();

		String testCaseId = "TC_InFoMgr_CorporateLocations_Send_Text_Message";

		Map<String, String> dsMessage = fc.utobj().readTestData("infomgr", testCaseId);

		String txtSubject = fc.utobj().generateTestData(dsMessage.get("subject"));

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().clickLink(driver, "Address book");
				SelectToEmail(driver, corporateUser);
				fc.utobj().switchFrameToDefault(driver);
				fc.utobj().sendKeys(driver, messagePage.txtSubject, txtSubject);
				fc.utobj().clickRadioButton(driver, messagePage.rdoTxtMsg, dsMessage.get("messageType"));
				fc.utobj().sleep();
				fc.utobj().sendKeys(driver, messagePage.txtMessageText, dsMessage.get("textMessage"));
				fc.utobj().clickElement(driver, messagePage.btnSend);
				Thread.sleep(2000);

				boolean isSubjectOnPage = fc.utobj().assertPageSource(driver, txtSubject);
				if (isSubjectOnPage) {
					Reporter.log("Message have been sent successfully !!! ");
					fc.utobj().clickElement(driver, messagePage.btnOk);
				} else {
					fc.utobj().throwsException("Some problem occured while sending message !!!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in sending message, please refer screenshot!");
		}
		return txtSubject;
	}

	public String sendTextEmail(WebDriver driver, Map<String, String> config, boolean isFromContactHistoryPage) throws Exception {
		Reporter.log("******************* Sending Text Email ********************** \n");

		InfoMgrSendEmailPage emailPage = new InfoMgrCorporateLocationsPage(driver).getSendMailPage();

		String testCaseId = "TC_InFoMgr_CorporateLocations_Send_Text_Email";

		Map<String, String> dsEmail = fc.utobj().readTestData("infomgr", testCaseId);

		String txtSubject = fc.utobj().generateTestData(dsEmail.get("subject"));

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().sendKeys(driver, emailPage.txtSubject, txtSubject);
				fc.utobj().clickRadioButton(driver, emailPage.rdoTxtMail, dsEmail.get("emailType"));
				fc.utobj().sleep();
				fc.utobj().sendKeys(driver, emailPage.mailMessage, dsEmail.get("textMessage"));
				fc.utobj().clickElement(driver, emailPage.btnSend);
				Thread.sleep(2000);

				if (isFromContactHistoryPage) {
					boolean isSubjectOnPage = fc.utobj().assertPageSource(driver, txtSubject);
					if (isSubjectOnPage) {
						Reporter.log("Email have been sent successfully !!! ");
					} else {
						fc.utobj().throwsException("Some problem occured while sending mail !!!!");
					}
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in sending text mail, please refer screenshot!");
		}
		return txtSubject;
	}

	public String sendHTMLEmail(WebDriver driver, Map<String, String> config, boolean isFromContactHistoryPage) throws Exception {
		Reporter.log("******************* Sending HTML Email ********************** \n");

		InfoMgrSendEmailPage emailPage = new InfoMgrCorporateLocationsPage(driver).getSendMailPage();

		String testCaseId = "TC_InFoMgr_CorporateLocations_Send_HTML_Email";

		Map<String, String> dsEmail = fc.utobj().readTestData("infomgr", testCaseId);

		String txtSubject = fc.utobj().generateTestData(dsEmail.get("subject"));

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().sendKeys(driver, emailPage.txtSubject, txtSubject);
				fc.utobj().clickRadioButton(driver, emailPage.rdoTxtMail, dsEmail.get("emailType"));
				fc.utobj().sendKeys(driver, emailPage.mailMessage, dsEmail.get("textMessage"));
				fc.utobj().clickElement(driver, emailPage.btnSend);
				Thread.sleep(2000);

				if (isFromContactHistoryPage) {
					boolean isSubjectOnPage = fc.utobj().assertPageSource(driver, txtSubject);
					if (isSubjectOnPage) {
						Reporter.log("Mail have been sent successfully !!! ");
					} else {
						fc.utobj().throwsException("Some problem occured while sending mail !!!!");
					}
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in sending HTML mail, please refer screenshot!");
		}
		return txtSubject;
	}

	public String sendHtmlMessage(WebDriver driver, Map<String, String> config, boolean isFromContactHistoryPage) throws Exception {
		Reporter.log("******************* Sending HTML Message ********************** \n");

		InfoMgrSendMessagePage messagePage = new InfoMgrCorporateLocationsPage(driver).getSendMessagePage();

		String testCaseId = "TC_InFoMgr_CorporateLocations_Send_HTML_Message";

		Map<String, String> dsMessage = fc.utobj().readTestData("infomgr", testCaseId);

		String txtSubject = fc.utobj().generateTestData(dsMessage.get("subject"));

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().sendKeys(driver, messagePage.txtToEmail, dsMessage.get("toEmail"));
				fc.utobj().sendKeys(driver, messagePage.txtSubject, txtSubject);
				fc.utobj().clickRadioButton(driver, messagePage.rdoTxtMsg, dsMessage.get("messageType"));
				fc.utobj().clickElement(driver, messagePage.btnSend);
				Thread.sleep(2000);
				if (isFromContactHistoryPage) {
					boolean isSubjectOnPage = fc.utobj().assertPageSource(driver, txtSubject);
					if (isSubjectOnPage) {
						Reporter.log("HTML Message have been sent successfully !!! ");
						fc.utobj().clickElement(driver, messagePage.btnOk);
					} else {
						fc.utobj().throwsException("Some problem occured while sending HTML message !!!!");
					}
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in sending HTML message, please refer screenshot!");
		}
		return txtSubject;
	}

	public String transfer(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("********************** Transfer Franchise Location ****************** \n");

		String testCaseId = "TC_InFoMgr_CorporateLocations_Transfer_NewFranchisee";

		Map<String, String> dsTransferDetails = fc.utobj().readTestData("infomgr", testCaseId);

		// Check if the page has already training listed , if yes then click on
		// "Add More" button
		InfoMgrTransferPage objTransferPage = new InfoMgrCorporateLocationsPage(driver).getTransferPage();

		String newFranchiseeID = fc.utobj().generateTestData(dsTransferDetails.get("franchiseID"));

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().clickRadioButton(driver, objTransferPage.rdoNewCenterNo, dsTransferDetails.get("transferType"));
				fc.utobj().sendKeys(driver, objTransferPage.txtCenterNo, newFranchiseeID);
				fc.utobj().sendKeys(driver, objTransferPage.txtFirstName, dsTransferDetails.get("firstName"));
				fc.utobj().sendKeys(driver, objTransferPage.txtLastName, dsTransferDetails.get("lastName"));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, objTransferPage.btnTransferComplete);

				corporateLocationsPageShowAll(driver);

				if (fc.utobj().assertPageSource(driver, newFranchiseeID)) {
					Reporter.log("Franchise location has been transfered successfully");
				} else {
					fc.utobj().throwsException("Franchise location couldn't  be transfered successfully !!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in adding training , please refer screenshot!");
		}
		return newFranchiseeID;
	}

	public void addTraining(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("********************** Add Training ****************** \n");

		String testCaseId = "TC_InFoMgr_CorporateLocations_Training_Add";

		Map<String, String> dsTraining = fc.utobj().readTestData("infomgr", testCaseId);

		// Check if the page has already training listed , if yes then click on
		// "Add More" button
		InfoMgrTrainingPage objAddTrainingPage = new InfoMgrCorporateLocationsPage(driver).getTrainingPage();
		String tableText = fc.utobj().getText(driver, objAddTrainingPage.tblTrainings);

		if (tableText.contains("Additional Trainings")) {
			fc.utobj().clickElement(driver, objAddTrainingPage.btnAddMore);
		}

		String trainingProgram = fc.utobj().generateTestData(dsTraining.get("trainingProgram"));

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().sendKeys(driver, objAddTrainingPage.txtTrainingProgName, trainingProgram);
				fc.utobj().selectDropDownByPartialText(driver, objAddTrainingPage.drpTrainingType, dsTraining.get("trainingType"));
				fc.utobj().sendKeys(driver, objAddTrainingPage.txtAttendee, dsTraining.get("attendee"));
				fc.utobj().sendKeys(driver, objAddTrainingPage.txtCompletionDate, fc.utobj().getFutureDateUSFormat(10));
				fc.utobj().sendKeys(driver, objAddTrainingPage.txtGrade, dsTraining.get("grade"));
				fc.utobj().sendKeys(driver, objAddTrainingPage.txtInstructor, dsTraining.get("instructor"));
				fc.utobj().sendKeys(driver, objAddTrainingPage.txtLocation, dsTraining.get("location"));
				fc.utobj().sendKeys(driver, objAddTrainingPage.txtAttendeeTitle, dsTraining.get("attendeeTitle"));
				fc.utobj().sendKeys(driver, objAddTrainingPage.txtScore, dsTraining.get("score"));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, objAddTrainingPage.btnSubmit);
				String txtTraining = fc.utobj().getText(driver, objAddTrainingPage.tblTrainings);
				if (txtTraining.contains(trainingProgram)) {
					Reporter.log("Training created successfully");
				} else {
					fc.utobj().throwsException("Error - Training didn't got created. Test Case failes !!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in adding training , please refer screenshot!");
		}
	}

	public WebDriver addAgreement(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("********************** Add Agreement ****************** \n");

		String testCaseId = "TC_InFoMgr_CorporateLocations_Agreement_Add";

		Map<String, String> dsAgreement = fc.utobj().readTestData("infomgr", testCaseId);

		InfoMgrAgreementPage infoMgrAgreementPage = new InfoMgrCorporateLocationsPage(driver).getAgreementPage();
		fc.utobj().clickElement(driver, infoMgrAgreementPage.lnkAgreement);

		String openDateVal = fc.utobj().getText(driver, infoMgrAgreementPage.lblOpeningDate);
		SimpleDateFormat sdfDate = new SimpleDateFormat("MM/dd/yyyy");
		Date closingDate = new Date(openDateVal);
		closingDate = DateUtils.addDays(closingDate, 10);
		String closingDateStr = sdfDate.format(closingDate);

		String comments = fc.utobj().generateTestData(dsAgreement.get("comments"));

		if (fc.utobj().validate(testCaseId) == true) {
			try {

				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtApprovedDate, fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtDateExecuted, fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtEffectiveDate, fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtExpirationDate, closingDateStr);
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtClosingDate, closingDateStr);

				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtStateAddendum, dsAgreement.get("stateAddendum"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtOtherAddendum, dsAgreement.get("otherAddendum"));
				fc.utobj().clickRadioButton(driver, infoMgrAgreementPage.radioRightsOfFirstRefusal, dsAgreement.get("rightsofFirstRefusal"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtProtectedTerritory, dsAgreement.get("protectedTerritory"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtSalesperson, dsAgreement.get("salesPerson"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtPreviousLicenseNumber, dsAgreement.get("previousLicenseNumber"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRelatedCenter, dsAgreement.get("relatedCenter"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRequiredOpeningDate, fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtStoreSoldDate, fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtComments, comments);
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtInitialTerm, dsAgreement.get("initialTerm"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRenewalTermFirst, dsAgreement.get("termFirst"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRenewalDueDateFirst, fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRenewalFeeFirst, dsAgreement.get("feeFirst"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRenewalFeePaidFirstDate, fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRenewalTermSecond, dsAgreement.get("termSecond"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRenewalDueDateSecond, fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRenewalFeeSecond, dsAgreement.get("feeSecond"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRenewalFeePaidSecondDate, fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRenewalTermThird, dsAgreement.get("termThird"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRenewalDueDateThird, fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRenewalFeeThird, dsAgreement.get("feeThird"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRenewalFeePaidThirdDate, fc.utobj().getFutureDateUSFormat(-10));
				// Contact Details
				fc.utobj().selectDropDownByVisibleText(driver, infoMgrAgreementPage.drpContactTitle, dsAgreement.get("title"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtFirstName, dsAgreement.get("firstName"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtLastName, dsAgreement.get("lastName"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtStreetAddress, dsAgreement.get("streetAddress"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtCity, dsAgreement.get("city"));
				fc.utobj().selectDropDownByVisibleText(driver, infoMgrAgreementPage.drpCountry, dsAgreement.get("country"));
				fc.utobj().sleep();
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtZipcode, dsAgreement.get("zip"));
				fc.utobj().selectDropDownByVisibleText(driver, infoMgrAgreementPage.drpState, dsAgreement.get("state"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtPhoneNumbers, dsAgreement.get("phone"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtExtn, dsAgreement.get("phoneExtension"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtFaxNumbers, dsAgreement.get("fax"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtMobileNumbers, dsAgreement.get("mobile"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtMailIds, dsAgreement.get("email"));

				// Financial Notes
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtDescription1, dsAgreement.get("description1"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtAmount1, dsAgreement.get("amount1"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtAmountTerm1, dsAgreement.get("amountTerm1"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtInterest1, dsAgreement.get("interest1"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtComment1, dsAgreement.get("comments1"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtDescription2, dsAgreement.get("description2"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtAmount2, dsAgreement.get("amount2"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtAmountTerm2, dsAgreement.get("amountTerm2"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtInterest2, dsAgreement.get("interest2"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtComment2, dsAgreement.get("comments2"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtDescription3, dsAgreement.get("description3"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtAmount3, dsAgreement.get("amount3"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtAmountTerm3, dsAgreement.get("amountTerm3"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtInterest3, dsAgreement.get("interest3"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtComment3, dsAgreement.get("comments3"));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, infoMgrAgreementPage.btnsubmit);

				if (fc.utobj().assertPageSource(driver, comments)) {
					Reporter.log("Agreement has been added successfully");
				} else {
					fc.utobj().throwsException("Agreement couldn't be added . Test couldn't continue !!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in adding agreement , please refer screenshot!");
		}
		return driver;
	}

	public Map<String, String> addAgreementAndGetFieldLabels(WebDriver driver, Map<String, String> config, String area) throws Exception {
		Reporter.log("********************** Add Agreement ****************** \n");
		String testCaseId = "TC_InFoMgr_CorporateLocations_Agreement_Add";
		Map<String, String> dsAgreement = fc.utobj().readTestDatawithsqllite("info", testCaseId, area);
		InfoMgrAgreementPage infoMgrAgreementPage = new InfoMgrCorporateLocationsPage(driver).getAgreementPage();
		InfoMgr_Common cmn = new InfoMgr_Common();
		fc.utobj().clickElement(driver, infoMgrAgreementPage.lnkAgreement);
		String openDateVal = fc.utobj().getText(driver, infoMgrAgreementPage.lblOpeningDate);
		SimpleDateFormat sdfDate = new SimpleDateFormat("MM/dd/yyyy");
		Date closingDate = new Date(openDateVal);
		closingDate = DateUtils.addDays(closingDate, 10);
		String closingDateStr = sdfDate.format(closingDate);
		Map<String, String> map = new HashMap<String, String>();
		String comments = fc.utobj().generateTestData(dsAgreement.get("comments"));
		String futuredateUs = fc.utobj().getFutureDateUSFormat(-10);

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				map.put("Opening Date", openDateVal);

				map.put(cmn.getFieldLabel(infoMgrAgreementPage.labelApprovedDate), futuredateUs);
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtApprovedDate, futuredateUs);
				map.put(cmn.getFieldLabel(infoMgrAgreementPage.labelDateExecuted), futuredateUs);
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtDateExecuted, futuredateUs);
				map.put(cmn.getFieldLabel(infoMgrAgreementPage.labelEffectiveDate), futuredateUs);
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtEffectiveDate, futuredateUs);

				map.put(cmn.getFieldLabel(infoMgrAgreementPage.labelExpirationDate), closingDateStr);
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtExpirationDate, closingDateStr);

				map.put(cmn.getFieldLabel(infoMgrAgreementPage.labelClosingDate), closingDateStr);
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtClosingDate, closingDateStr);

				map.put("State / Province Addendum", dsAgreement.get("stateAddendum"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtStateAddendum, dsAgreement.get("stateAddendum"));

				map.put("Other Addendum", dsAgreement.get("otherAddendum"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtOtherAddendum, dsAgreement.get("otherAddendum"));

				map.put(cmn.getFieldLabel(infoMgrAgreementPage.labelRightsOfFirstRefusal), dsAgreement.get("rightsofFirstRefusal"));
				fc.utobj().clickRadioButton(driver, infoMgrAgreementPage.radioRightsOfFirstRefusal, dsAgreement.get("rightsofFirstRefusal"));

				map.put(cmn.getFieldLabel(infoMgrAgreementPage.labelProtectedTerritory), dsAgreement.get("protectedTerritory"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtProtectedTerritory, dsAgreement.get("protectedTerritory"));
				map.put(cmn.getFieldLabel(infoMgrAgreementPage.labelSalesperson), dsAgreement.get("salesPerson"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtSalesperson, dsAgreement.get("salesPerson"));

				map.put(cmn.getFieldLabel(infoMgrAgreementPage.labelPreviousLicenseNumber), dsAgreement.get("previousLicenseNumber"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtPreviousLicenseNumber, dsAgreement.get("previousLicenseNumber"));

				map.put(cmn.getFieldLabel(infoMgrAgreementPage.labelRelatedCenter), dsAgreement.get("relatedCenter"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRelatedCenter, dsAgreement.get("relatedCenter"));

				map.put(cmn.getFieldLabel(infoMgrAgreementPage.labelRequiredOpeningDate), futuredateUs);
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRequiredOpeningDate, futuredateUs);

				map.put(cmn.getFieldLabel(infoMgrAgreementPage.labelStoreSoldDate), futuredateUs);
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtStoreSoldDate, futuredateUs);
				
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtComments, comments);

				map.put(cmn.getFieldLabel(infoMgrAgreementPage.labelInitialTerm), dsAgreement.get("initialTerm"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtInitialTerm, dsAgreement.get("initialTerm"));

				map.put(Renewal + cmn.getFieldLabel(infoMgrAgreementPage.labelRenewalTermFirst), dsAgreement.get("termFirst"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRenewalTermFirst, dsAgreement.get("termFirst"));

				map.put(Renewal + cmn.getFieldLabel(infoMgrAgreementPage.labelRenewalDueDateFirst), futuredateUs);
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRenewalDueDateFirst, futuredateUs);
				map.put(Renewal + cmn.getFieldLabel(infoMgrAgreementPage.labelRenewalFeeFirst), dsAgreement.get("feeFirst"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRenewalFeeFirst, dsAgreement.get("feeFirst"));
				map.put(Renewal + cmn.getFieldLabel(infoMgrAgreementPage.labelRenewalFeePaidFirstDate) + " Date", futuredateUs);
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRenewalFeePaidFirstDate, futuredateUs);
				map.put(Renewal + cmn.getFieldLabel(infoMgrAgreementPage.labelRenewalTermSecond), dsAgreement.get("termSecond"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRenewalTermSecond, dsAgreement.get("termSecond"));
				map.put(Renewal + cmn.getFieldLabel(infoMgrAgreementPage.labelRenewalDueDateSecond), futuredateUs);
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRenewalDueDateSecond, futuredateUs);
				map.put(Renewal + cmn.getFieldLabel(infoMgrAgreementPage.labelRenewalFeeSecond), dsAgreement.get("feeSecond"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRenewalFeeSecond, dsAgreement.get("feeSecond"));
				map.put(Renewal + cmn.getFieldLabel(infoMgrAgreementPage.labelRenewalFeePaidSecondDate) + " Date", futuredateUs);
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRenewalFeePaidSecondDate, futuredateUs);
				map.put(Renewal + cmn.getFieldLabel(infoMgrAgreementPage.labelRenewalTermThird), dsAgreement.get("termThird"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRenewalTermThird, dsAgreement.get("termThird"));
				map.put(Renewal + cmn.getFieldLabel(infoMgrAgreementPage.labelRenewalDueDateThird), futuredateUs);
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRenewalDueDateThird, futuredateUs);
				map.put(Renewal + cmn.getFieldLabel(infoMgrAgreementPage.labelRenewalFeeThird), dsAgreement.get("feeThird"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRenewalFeeThird, dsAgreement.get("feeThird"));
				map.put(Renewal + cmn.getFieldLabel(infoMgrAgreementPage.labelRenewalFeePaidThirdDate) + " Date", futuredateUs);
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRenewalFeePaidThirdDate, futuredateUs);
				// Contact Details
				map.put("Contact " + cmn.getFieldLabel(infoMgrAgreementPage.labeldrpContactTitle), dsAgreement.get("title"));
				fc.utobj().selectDropDownByVisibleText(driver, infoMgrAgreementPage.drpContactTitle, dsAgreement.get("title"));
				map.put(cmn.getFieldLabel(infoMgrAgreementPage.labelFirstName), dsAgreement.get("firstName"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtFirstName, dsAgreement.get("firstName"));
				map.put(cmn.getFieldLabel(infoMgrAgreementPage.labelLastName), dsAgreement.get("lastName"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtLastName, dsAgreement.get("lastName"));

				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtStreetAddress, dsAgreement.get("streetAddress"));

				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtCity, dsAgreement.get("city"));

				fc.utobj().selectDropDownByVisibleText(driver, infoMgrAgreementPage.drpCountry, dsAgreement.get("country"));

				fc.utobj().sleep();

				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtZipcode, dsAgreement.get("zip"));

				fc.utobj().selectDropDownByVisibleText(driver, infoMgrAgreementPage.drpState, dsAgreement.get("state"));

				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtPhoneNumbers, dsAgreement.get("phone"));

				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtExtn, dsAgreement.get("phoneExtension"));

				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtFaxNumbers, dsAgreement.get("fax"));

				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtMobileNumbers, dsAgreement.get("mobile"));

				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtMailIds, dsAgreement.get("email"));

				// Financial Notes
				map.put("Description 1", dsAgreement.get("description1"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtDescription1, dsAgreement.get("description1"));
				map.put("Amount 1", dsAgreement.get("amount1"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtAmount1, dsAgreement.get("amount1"));
				map.put("Amount Term 1", dsAgreement.get("amountTerm1"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtAmountTerm1, dsAgreement.get("amountTerm1"));
				map.put("Interest 1", dsAgreement.get("interest1"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtInterest1, dsAgreement.get("interest1"));
				map.put("Comments 1", dsAgreement.get("comments1"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtComment1, dsAgreement.get("comments1"));
				map.put("Description 2", dsAgreement.get("description2"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtDescription2, dsAgreement.get("description2"));
				map.put("Amount 2", dsAgreement.get("amount2"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtAmount2, dsAgreement.get("amount2"));
				map.put("Amount Term 2", dsAgreement.get("amountTerm2"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtAmountTerm2, dsAgreement.get("amountTerm2"));
				map.put("Interest 2", dsAgreement.get("interest2"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtInterest2, dsAgreement.get("interest2"));
				map.put("Comments 2", dsAgreement.get("comments2"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtComment2, dsAgreement.get("comments2"));
				map.put("Description 3", dsAgreement.get("description3"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtDescription3, dsAgreement.get("description3"));
				map.put("Amount 3", dsAgreement.get("amount3"));
				;
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtAmount3, dsAgreement.get("amount3"));
				map.put("Amount Term 3", dsAgreement.get("amountTerm3"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtAmountTerm3, dsAgreement.get("amountTerm3"));
				map.put("Interest 3", dsAgreement.get("interest3"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtInterest3, dsAgreement.get("interest3"));
				map.put("Comments 3", dsAgreement.get("comments3"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtComment3, dsAgreement.get("comments3"));

				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");

				fc.utobj().clickElement(driver, infoMgrAgreementPage.btnsubmit);

				if (fc.utobj().assertPageSource(driver, comments)) {
					Reporter.log("Agreement has been added successfully");
				} else {
					fc.utobj().throwsException("Agreement couldn't be added . Test couldn't continue !!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in adding agreement , please refer screenshot!");
		}
		return map;
	}

	public Map<String, String> ModifyAgreementAndGetFieldLabels(WebDriver driver, Map<String, String> config, String area) throws Exception {
		Reporter.log("********************** Modify Agreement ****************** \n");

		String testCaseId = "TC_InFoMgr_CorporateLocations_Agreement_Modify";
		Map<String, String> dsAgreement = fc.utobj().readTestDatawithsqllite("info", testCaseId, area);
		InfoMgrAgreementPage infoMgrAgreementPage = new InfoMgrCorporateLocationsPage(driver).getAgreementPage();
		InfoMgr_Common cmn = new InfoMgr_Common();

		fc.utobj().clickElement(driver, infoMgrAgreementPage.lnkAgreement);
		fc.utobj().clickLink(driver, "Modify");
		String openDateVal = fc.utobj().getText(driver, infoMgrAgreementPage.lblOpeningDate);
		SimpleDateFormat sdfDate = new SimpleDateFormat("MM/dd/yyyy");
		Date closingDate = new Date(openDateVal);
		closingDate = DateUtils.addDays(closingDate, 11);
		String closingDateStr = sdfDate.format(closingDate);
		Map<String, String> map = new HashMap<String, String>();
		String comments = fc.utobj().generateTestData(dsAgreement.get("comments"));
		String futuredateUs = fc.utobj().getFutureDateUSFormat(-11);

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				map.put("Opening Date", openDateVal);

				map.put(cmn.getFieldLabel(infoMgrAgreementPage.labelApprovedDate), futuredateUs);
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtApprovedDate, futuredateUs);
				map.put(cmn.getFieldLabel(infoMgrAgreementPage.labelDateExecuted), futuredateUs);
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtDateExecuted, futuredateUs);
				map.put(cmn.getFieldLabel(infoMgrAgreementPage.labelEffectiveDate), futuredateUs);

				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtEffectiveDate, futuredateUs);

				map.put(cmn.getFieldLabel(infoMgrAgreementPage.labelExpirationDate), closingDateStr);
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtExpirationDate, closingDateStr);

				map.put(cmn.getFieldLabel(infoMgrAgreementPage.labelClosingDate), closingDateStr);
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtClosingDate, closingDateStr);

				map.put("State / Province Addendum", dsAgreement.get("stateAddendum"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtStateAddendum, dsAgreement.get("stateAddendum"));

				map.put("Other Addendum", dsAgreement.get("otherAddendum"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtOtherAddendum, dsAgreement.get("otherAddendum"));

				map.put(cmn.getFieldLabel(infoMgrAgreementPage.labelRightsOfFirstRefusal), dsAgreement.get("rightsofFirstRefusal"));
				fc.utobj().clickRadioButton(driver, infoMgrAgreementPage.radioRightsOfFirstRefusal, dsAgreement.get("rightsofFirstRefusal"));

				map.put(cmn.getFieldLabel(infoMgrAgreementPage.labelProtectedTerritory), dsAgreement.get("protectedTerritory"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtProtectedTerritory, dsAgreement.get("protectedTerritory"));
				map.put(cmn.getFieldLabel(infoMgrAgreementPage.labelSalesperson), dsAgreement.get("salesPerson"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtSalesperson, dsAgreement.get("salesPerson"));

				map.put(cmn.getFieldLabel(infoMgrAgreementPage.labelPreviousLicenseNumber), dsAgreement.get("previousLicenseNumber"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtPreviousLicenseNumber, dsAgreement.get("previousLicenseNumber"));

				map.put(cmn.getFieldLabel(infoMgrAgreementPage.labelRelatedCenter), dsAgreement.get("relatedCenter"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRelatedCenter, dsAgreement.get("relatedCenter"));

				map.put(cmn.getFieldLabel(infoMgrAgreementPage.labelRequiredOpeningDate), futuredateUs);
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRequiredOpeningDate, futuredateUs);

				map.put(cmn.getFieldLabel(infoMgrAgreementPage.labelStoreSoldDate), futuredateUs);
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtStoreSoldDate, futuredateUs);
				map.put(cmn.getFieldLabel(infoMgrAgreementPage.labelComments), comments);
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtComments, comments);

				map.put(cmn.getFieldLabel(infoMgrAgreementPage.labelInitialTerm), dsAgreement.get("initialTerm"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtInitialTerm, dsAgreement.get("initialTerm"));

				map.put(Renewal + cmn.getFieldLabel(infoMgrAgreementPage.labelRenewalTermFirst), dsAgreement.get("termFirst"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRenewalTermFirst, dsAgreement.get("termFirst"));

				map.put(Renewal + cmn.getFieldLabel(infoMgrAgreementPage.labelRenewalDueDateFirst), futuredateUs);
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRenewalDueDateFirst, futuredateUs);
				map.put(Renewal + cmn.getFieldLabel(infoMgrAgreementPage.labelRenewalFeeFirst), dsAgreement.get("feeFirst"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRenewalFeeFirst, dsAgreement.get("feeFirst"));
				map.put(Renewal + cmn.getFieldLabel(infoMgrAgreementPage.labelRenewalFeePaidFirstDate) + " Date", futuredateUs);
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRenewalFeePaidFirstDate, futuredateUs);
				map.put(Renewal + cmn.getFieldLabel(infoMgrAgreementPage.labelRenewalTermSecond), dsAgreement.get("termSecond"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRenewalTermSecond, dsAgreement.get("termSecond"));
				map.put(Renewal + cmn.getFieldLabel(infoMgrAgreementPage.labelRenewalDueDateSecond), futuredateUs);
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRenewalDueDateSecond, futuredateUs);
				map.put(Renewal + cmn.getFieldLabel(infoMgrAgreementPage.labelRenewalFeeSecond), dsAgreement.get("feeSecond"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRenewalFeeSecond, dsAgreement.get("feeSecond"));
				map.put(Renewal + cmn.getFieldLabel(infoMgrAgreementPage.labelRenewalFeePaidSecondDate) + " Date", futuredateUs);
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRenewalFeePaidSecondDate, futuredateUs);
				map.put(Renewal + cmn.getFieldLabel(infoMgrAgreementPage.labelRenewalTermThird), dsAgreement.get("termThird"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRenewalTermThird, dsAgreement.get("termThird"));
				map.put(Renewal + cmn.getFieldLabel(infoMgrAgreementPage.labelRenewalDueDateThird), futuredateUs);
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRenewalDueDateThird, futuredateUs);
				map.put(Renewal + cmn.getFieldLabel(infoMgrAgreementPage.labelRenewalFeeThird), dsAgreement.get("feeThird"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRenewalFeeThird, dsAgreement.get("feeThird"));
				map.put(Renewal + cmn.getFieldLabel(infoMgrAgreementPage.labelRenewalFeePaidThirdDate) + " Date", futuredateUs);
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRenewalFeePaidThirdDate, futuredateUs);
				// Contact Details
				map.put("Contact " + cmn.getFieldLabel(infoMgrAgreementPage.labeldrpContactTitle), dsAgreement.get("title"));
				fc.utobj().selectDropDownByVisibleText(driver, infoMgrAgreementPage.drpContactTitle, dsAgreement.get("title"));
				map.put(cmn.getFieldLabel(infoMgrAgreementPage.labelFirstName), dsAgreement.get("firstName"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtFirstName, dsAgreement.get("firstName"));
				map.put(cmn.getFieldLabel(infoMgrAgreementPage.labelLastName), dsAgreement.get("lastName"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtLastName, dsAgreement.get("lastName"));

				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtStreetAddress, dsAgreement.get("streetAddress"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtCity, dsAgreement.get("city"));
				fc.utobj().selectDropDownByVisibleText(driver, infoMgrAgreementPage.drpCountry, dsAgreement.get("country"));
				fc.utobj().sleep();
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtZipcode, dsAgreement.get("zip"));
				fc.utobj().selectDropDownByVisibleText(driver, infoMgrAgreementPage.drpState, dsAgreement.get("state"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtPhoneNumbers, dsAgreement.get("phone"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtExtn, dsAgreement.get("phoneExtension"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtFaxNumbers, dsAgreement.get("fax"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtMobileNumbers, dsAgreement.get("mobile"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtMailIds, dsAgreement.get("email"));

				// Financial Notes
				map.put("Description 1", dsAgreement.get("description1"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtDescription1, dsAgreement.get("description1"));
				map.put("Amount 1", dsAgreement.get("amount1"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtAmount1, dsAgreement.get("amount1"));
				map.put("Amount Term 1", dsAgreement.get("amountTerm1"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtAmountTerm1, dsAgreement.get("amountTerm1"));
				map.put("Interest 1", dsAgreement.get("interest1"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtInterest1, dsAgreement.get("interest1"));
				map.put("Comments 1", dsAgreement.get("comments1"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtComment1, dsAgreement.get("comments1"));
				map.put("Description 2", dsAgreement.get("description2"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtDescription2, dsAgreement.get("description2"));
				map.put("Amount 2", dsAgreement.get("amount2"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtAmount2, dsAgreement.get("amount2"));
				map.put("Amount Term 2", dsAgreement.get("amountTerm2"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtAmountTerm2, dsAgreement.get("amountTerm2"));
				map.put("Interest 2", dsAgreement.get("interest2"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtInterest2, dsAgreement.get("interest2"));
				map.put("Comments 2", dsAgreement.get("comments2"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtComment2, dsAgreement.get("comments2"));
				map.put("Description 3", dsAgreement.get("description3"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtDescription3, dsAgreement.get("description3"));
				map.put("Amount 3", dsAgreement.get("amount3"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtAmount3, dsAgreement.get("amount3"));
				map.put("Amount Term 3", dsAgreement.get("amountTerm3"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtAmountTerm3, dsAgreement.get("amountTerm3"));
				map.put("Interest 3", dsAgreement.get("interest3"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtInterest3, dsAgreement.get("interest3"));
				map.put("Comments 3", dsAgreement.get("comments3"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtComment3, dsAgreement.get("comments3"));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");
				fc.utobj().clickElement(driver, infoMgrAgreementPage.btnsubmit);
				if (fc.utobj().assertPageSource(driver, comments)) {
					Reporter.log("Agreement has been Modified successfully");
				} else {
					fc.utobj().throwsException("Agreement couldn't be modified . Test couldn't continue !!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in modifying agreement , please refer screenshot!");
		}
		return map;
	}

	public WebElement addButton(WebDriver driver, String section) {
		WebElement addButton = driver.findElement(By.xpath("//td[contains(text(),'" + section + "')]/following::a[contains(text(),'Add')]"));
		return addButton;
	}

	public WebElement settings(WebDriver driver, String section) {
		WebElement sett = driver.findElement(By.xpath("//*[contains(text(),'" + section + "')]/following::a[contains(@onmouseover,'')]//img[contains(@src,'edit.gif')]"));
		return sett;
	}

	public void addCustomTabData(WebDriver driver, Map<String, String> config, String area, Map<String, String> map, String newTab, String secType, String section, String action) throws Exception {
		Reporter.log("********************** " + action + "Custom Tab Data ****************** \n");
		String testCaseId = "";
		InfoMgrCustomTabPage ctab = new InfoMgrFranchiseesPage(driver).getCustomTabPage();

		fc.utobj().clickLink(driver, newTab);
		try {
			if (action.equalsIgnoreCase("add") && secType.equalsIgnoreCase("tabular")) {

				fc.utobj().clickElement(driver, ctab.addButton);
				fc.commonMethods().switch_cboxIframe_frameId(driver);

			} else {

				if (action.equalsIgnoreCase("modify") && secType.equalsIgnoreCase("regular")) {
					fc.utobj().clickLink(driver, "Modify");

				} else if (action.equalsIgnoreCase("modify") && secType.equalsIgnoreCase("tabular")) {
					fc.utobj().actionImgOption(driver, ctab.date.getText(), "Modify");
					fc.commonMethods().switch_cboxIframe_frameId(driver);
				}
			}
		} catch (Exception e) {
			fc.utobj().throwsSkipException("Error in " + action + " custom tab , please refer screenshot!");
		}

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				// map.put("Opening Date", openDateVal);
				for (Map.Entry<String, String> entry : map.entrySet()) {
					System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue());
					WebElement element = null;

					if (entry.getKey().contains("doc")) {
						element = driver.findElement(By.xpath(".//input[contains(@name,'" + entry.getKey().toLowerCase() + "') and @type='file']"));
						fc.utobj().sendKeys(driver, element, "C:\\Selenium_Test_Input\\testData\\document\\" + entry.getValue());

					} else if (entry.getKey().contains("radio")) {
						String value = entry.getValue();
						String intValue = value.replaceAll("[^0-9]", "");
						element = driver.findElement(By.xpath(".//*[contains(@id,'" + entry.getKey() + "') and @value='" + intValue + "']"));
						fc.utobj().clickElement(driver, element);

					} else if (entry.getKey().contains("chk")) {
						String value = entry.getValue();
						String intValue = value.replaceAll("[^0-9]", "");
						if (action.equalsIgnoreCase("modify")) {
							WebElement element1 = driver.findElement(By.xpath(".//*[contains(@id,'" + entry.getKey() + "') and @value='1']"));
							fc.utobj().check(element1, "no");
						}
						element = driver.findElement(By.xpath(".//*[contains(@id,'" + entry.getKey() + "') and @value='" + intValue + "']"));
						fc.utobj().check(element, "yes");
					}
					else if (entry.getKey().contains("multi")) {

						element = driver.findElement(By.xpath(".//*[contains(@name,'" + entry.getKey().toLowerCase() + "')]/following::span[@class='placeholder']"));
						fc.utobj().selectValFromMultiSelect(driver, element, entry.getValue());

					} else if (entry.getKey().contains("country") || entry.getKey().contains("countrystate") || entry.getKey().contains("state") || entry.getKey().contains("drop")) {
						element = driver.findElement(By.xpath(".//*[contains(@name,'" + entry.getKey().toLowerCase() + "')]"));
						fc.utobj().selectDropDownByVisibleText(driver, element, entry.getValue());
					} else if (entry.getKey().contains("area")) {
						element = driver.findElement(By.xpath(".//*[contains(@name,'" + entry.getKey().toLowerCase() + "')]"));
						fc.utobj().sendKeys(driver, element, entry.getValue());
					} else {

						element = driver.findElement(By.xpath(".//*[contains(@name,'" + entry.getKey().toLowerCase() + "')]"));
						fc.utobj().sendKeys(driver, element, entry.getValue());
					}
				}
				if (action.equalsIgnoreCase("modify") && secType.equalsIgnoreCase("tabular")) {
					fc.utobj().clickElement(driver, ctab.Save);
				} else if (action.equalsIgnoreCase("modify") && secType.equalsIgnoreCase("regular")) {
					fc.utobj().clickElement(driver, ctab.Save);
				} else {
					fc.utobj().clickElement(driver, ctab.Add);
				}
				if (secType.equalsIgnoreCase("tabular")) {
					fc.utobj().clickElement(driver, ctab.Close);
				}
			} catch (Exception e) {

				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {

			fc.utobj().throwsSkipException("Error in " + action + " custom tab , please refer screenshot!");
		}

	}

	public WebDriver addAddresses(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("********************** Add Franchisee Addresses ****************** \n");

		String testCaseId = "TC_InFoMgr_CorporateLocations_Addressess_Add";

		Map<String, String> dsAddresses = fc.utobj().readTestData("infomgr", testCaseId);

		InfoMgrAddressesPage addressPage = new InfoMgrCorporateLocationsPage(driver).getAddressPage();

		String state = "";

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				// Switch to frame
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().sleep();

				fc.utobj().selectDropDownByPartialText(driver, addressPage.drpCenterInfoState, dsAddresses.get("state"));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, addressPage.btnSave);

				// After saving the address , verify that address has been saved
				fc.utobj().switchFrameToDefault(driver);
				fc.utobj().sleep();

				fc.utobj().clickElement(driver, addressPage.lnkAddresses);

				fc.utobj().sleep();

				// Switch to frame
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().sleep();

				state = fc.utobj().getSelectedOptioninDropDown(driver, addressPage.drpCenterInfoState);

				fc.utobj().clickElement(driver, addressPage.btnClose);

				if (state.equalsIgnoreCase(dsAddresses.get("state"))) {
					Reporter.log("Address has been saved successfully !!!");
				} else {
					fc.utobj().throwsException("Address couldn't be saved properly. Test failes !!! ");
				}
				driver = fc.utobj().switchFrameToDefault(driver);
				fc.utobj().sleep();
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in adding address , please refer screenshot!");
		}
		return driver;
	}

	public String logACall(WebDriver driver, Map<String, String> config, boolean isFromContactHistoryPage) throws Exception {
		Reporter.log("********************** Loggin a Call ****************** \n");

		String testCaseId = "TC_InFoMgr_CorporateLocations_LogACall";

		Map<String, String> dsLogACall = fc.utobj().readTestData("infomgr", testCaseId);
		InfoMgrLogACallPage logACallPage = new InfoMgrCorporateLocationsPage(driver).getLogACallPage();
		fc.commonMethods().switch_cboxIframe_frameId(driver);
		String subject = fc.utobj().generateTestData(dsLogACall.get("subject"));

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().sendKeys(driver, logACallPage.txtSubject, subject);
				fc.utobj().sendKeys(driver, logACallPage.txtDate, fc.utobj().getFutureDateUSFormat(5));
				fc.utobj().selectDropDownByPartialText(driver, logACallPage.drpStatus, dsLogACall.get("status"));
				fc.utobj().selectDropDownByPartialText(driver, logACallPage.txtTimeHour, dsLogACall.get("timeHour"));
				fc.utobj().selectDropDownByPartialText(driver, logACallPage.txtTimeMinute, dsLogACall.get("timeMin"));
				fc.utobj().selectDropDownByPartialText(driver, logACallPage.txtAMPM, dsLogACall.get("timeAMPM"));

				fc.utobj().selectDropDownByPartialText(driver, logACallPage.drpCallType, dsLogACall.get("typeofCall"));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, logACallPage.btnSubmit);
				fc.utobj().sleep();
				fc.utobj().clickElement(driver, logACallPage.btnNo);
				fc.utobj().sleep();
				fc.utobj().switchFrameToDefault(driver);

				if (isFromContactHistoryPage) {
					if (fc.utobj().assertLinkText(driver, subject)) {
						Reporter.log("Log a call set up has been done successfully");
					} else {
						fc.utobj().throwsException("Log a Call has been failed.");
					}
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in logging a call , please refer screenshot!");
		}
		return subject;
	}

	public String logaTask(WebDriver driver, Map<String, String> config, String corporateUser, boolean isFromContactHistoryPage) throws Exception {
		Reporter.log("********************** Loggin a Task ****************** \n");

		String testCaseId = "TC_InFoMgr_CorporateLocations_LogATask";

		fc.commonMethods().switch_cboxIframe_frameId(driver);

		Map<String, String> dsLogATask = fc.utobj().readTestData("infomgr", testCaseId);

		InfoMgrLogATaskPage logATaskPage = new InfoMgrCorporateLocationsPage(driver).getLogATaskPage();
		String subject = fc.utobj().generateTestData(dsLogATask.get("subject"));
		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().selectValFromMultiSelectWithoutReset(driver, logATaskPage.multiCheckBoxCorporateUsers, corporateUser);
				// fc.utobj().selectValFromMultiSelect(driver,
				// logATaskPage.multiCheckBoxCorporateUsers, corporateUser);
				fc.utobj().selectDropDownByPartialText(driver, logATaskPage.drpStatus, dsLogATask.get("status"));
				fc.utobj().selectDropDownByVisibleText(driver, logATaskPage.drpTaskType, dsLogATask.get("taskType"));
				fc.utobj().sendKeys(driver, logATaskPage.txtSubject, subject);
				fc.utobj().sendKeys(driver, logATaskPage.txtStartDate, fc.utobj().getFutureDateUSFormat(5));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, logATaskPage.btnAdd);
				fc.utobj().switchFrameToDefault(driver);
				if (isFromContactHistoryPage) {
					boolean isSubjectOnPage = fc.utobj().assertPageSource(driver, subject);
					if (isSubjectOnPage) {
						Reporter.log("Log a Task has been done successfully");
					} else {
						fc.utobj().throwsException("Log a Task failed. Test case fails ");
					}
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in loggin a task , please refer screenshot!");
		}
		return subject;
	}

	public void addContractSigning(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("**************************** Add Contract Signing Info  ***********");

		String testCaseId = "TC_InfoMgr_CorporateLocations_ContractSigning_Add";

		Map<String, String> dsAddresses = fc.utobj().readTestData("infomgr", testCaseId);

		// Click on contract signing tab
		InfoMgrContractSigningPage contractSigning = new InfoMgrCorporateLocationsPage(driver).getContractSigningPage();
		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().sendKeys(driver, contractSigning.txtContractReceivedSignedDate, fc.utobj().getFutureDateUSFormat(10));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, contractSigning.btnSubmit);
				if (fc.utobj().assertPageSource(driver, "Signing Commissions") && fc.utobj().assertPageSource(driver, "Ancillary Documents")) {
					Reporter.log("Contract Signing data have been saved successfully !!! ");
				} else {
					fc.utobj().throwsException("Contract Signing data couldn't be saved. Test failes !!! ");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in adding Contract Signing Information  , please refer screenshot!");
		}
	}

	public void addCustomerComplaints(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("****************************** Adding Customer Complaints ********************** \n");

		String testCaseId = "TC_InfoMgr_CorporateLocations_Add_Customer_Complaints";
		Map<String, String> dsCustomerComplaints = fc.utobj().readTestData("infomgr", testCaseId);
		InfoMgrCustomerComplaintsPage customerComplaintsPage = new InfoMgrCorporateLocationsPage(driver).getCustomerComplaintsPage();
		// Fill in the details
		String complaintID = fc.utobj().generateTestData(dsCustomerComplaints.get("complaintID"));
		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().sendKeys(driver, customerComplaintsPage.txtComplaintID, complaintID);
				fc.utobj().sendKeys(driver, customerComplaintsPage.txtComplaintDate, fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, customerComplaintsPage.txtComplaint, dsCustomerComplaints.get("complaint"));
				fc.utobj().selectDropDownByVisibleText(driver, customerComplaintsPage.drpComplaintType, dsCustomerComplaints.get("type"));
				fc.utobj().sendKeys(driver, customerComplaintsPage.txtIncidentDate, fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, customerComplaintsPage.txtReceivedVia, dsCustomerComplaints.get("receivedVia"));
				fc.utobj().sendKeys(driver, customerComplaintsPage.txtSummary, dsCustomerComplaints.get("summaryofIncident"));
				fc.utobj().sendKeys(driver, customerComplaintsPage.txtActionTaken, dsCustomerComplaints.get("actionTaken"));
				fc.utobj().sendKeys(driver, customerComplaintsPage.txtDocumentTitle, dsCustomerComplaints.get("documentTitle"));
				fc.utobj().sendKeys(driver, customerComplaintsPage.txtDocumentAttachment, dsCustomerComplaints.get("document"));
				fc.utobj().sendKeys(driver, customerComplaintsPage.txtComplaintBy, dsCustomerComplaints.get("complainantName"));
				fc.utobj().sendKeys(driver, customerComplaintsPage.txtStreetAddress, dsCustomerComplaints.get("streetAddress"));
				fc.utobj().sendKeys(driver, customerComplaintsPage.txtCity, dsCustomerComplaints.get("city"));
				fc.utobj().selectDropDownByPartialText(driver, customerComplaintsPage.drpCountry, dsCustomerComplaints.get("country"));
				fc.utobj().sendKeys(driver, customerComplaintsPage.txtZipCode, dsCustomerComplaints.get("zip"));
				fc.utobj().selectDropDownByVisibleText(driver, customerComplaintsPage.drpState, dsCustomerComplaints.get("state"));
				fc.utobj().sendKeys(driver, customerComplaintsPage.txtEmailIds, dsCustomerComplaints.get("email"));
				fc.utobj().sendKeys(driver, customerComplaintsPage.txtPhoneNumbers, dsCustomerComplaints.get("phone"));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, customerComplaintsPage.btnSubmit);
				boolean isComplaintIDPresent = fc.utobj().assertPageSource(driver, complaintID);
				if (isComplaintIDPresent) {
					Reporter.log("Complaint created successfully !!!");
				} else {
					fc.utobj().throwsException("Error in adding customer complaints !!! ");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in adding Customer Complaints , please refer screenshot!");
		}
	}

	public String addDocument(WebDriver driver, Map<String, String> config) throws Exception {
		String testCaseId = "TC_InfoMgr_CorporateLocations_Document_Add";
		InfoMgrDocumentsPage documentsPage = new InfoMgrCorporateLocationsPage(driver).getDocumentsPage();

		Map<String, String> dsDocuments = fc.utobj().readTestData("infomgr", testCaseId);

		String documentTitle = fc.utobj().generateTestData(dsDocuments.get("documentTitle"));
		try {
			fc.utobj().clickElement(driver, documentsPage.btnAddMore);
		} catch (Exception ex) {

		}
		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().sendKeys(driver, documentsPage.txtDocumentTitle, documentTitle);
				fc.utobj().sendKeys(driver, documentsPage.txtDocumentAttachment, dsDocuments.get("documentAttachment"));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, documentsPage.btnAdd);
				boolean isDocAdded = fc.utobj().assertPageSource(driver, documentTitle);
				if (isDocAdded) {
					Reporter.log("Document added successfully");
				} else {
					fc.utobj().throwsException("Document upload failed. !!! ");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
			fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

		} else {
			fc.utobj().throwsSkipException("Error in adding documents , please refer screenshot!");
		}
		return documentTitle;
	}

	public String addEmployee(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("*********************** Add Employess ****************************** ");

		String testCaseId = "TC_InFoMgr_CorporateLocations_Add_Employee";

		Map<String, String> dsEmployee = fc.utobj().readTestData("infomgr", testCaseId);

		InfoMgrEmployeesPage employeesPage = new InfoMgrCorporateLocationsPage(driver).getEmployeesPage();

		String firstEmployeeName = fc.utobj().generateTestData(dsEmployee.get("firstName"));
		try {
			fc.utobj().clickElement(driver, employeesPage.btnAddMoreEmp);
		} catch (Exception ex) {

		}

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().selectDropDownByVisibleText(driver, employeesPage.drpSalutation, dsEmployee.get("salutation"));
				fc.utobj().sendKeys(driver, employeesPage.txtFirstName, firstEmployeeName);
				fc.utobj().sendKeys(driver, employeesPage.txtLastName, dsEmployee.get("lastName"));
				fc.utobj().sendKeys(driver, employeesPage.txtAddress, dsEmployee.get("address"));
				fc.utobj().sendKeys(driver, employeesPage.txtCity, dsEmployee.get("city"));
				fc.utobj().selectDropDownByVisibleText(driver, employeesPage.drpCountry, dsEmployee.get("country"));
				fc.utobj().sendKeys(driver, employeesPage.txtZipcode, dsEmployee.get("zip"));
				fc.utobj().selectDropDownByVisibleText(driver, employeesPage.drpState, dsEmployee.get("state"));
				fc.utobj().sendKeys(driver, employeesPage.txtPhone1, dsEmployee.get("phone1"));
				fc.utobj().sendKeys(driver, employeesPage.txtPhone2, dsEmployee.get("phone2"));
				fc.utobj().sendKeys(driver, employeesPage.txtFax, dsEmployee.get("fax"));
				fc.utobj().sendKeys(driver, employeesPage.txtMobile, dsEmployee.get("mobile"));
				fc.utobj().sendKeys(driver, employeesPage.txtEmail, dsEmployee.get("email"));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, employeesPage.btnAdd);
				fc.utobj().sleep();
				fc.utobj().clickElement(driver, employeesPage.btnCancel);
				if (fc.utobj().assertPageSource(driver, firstEmployeeName)) {
					Reporter.log("Employees have been added successfully");
				} else {
					fc.utobj().throwsException("Employee couldn't be added . Test couldn't continue !!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in adding Employee , please refer screenshot!");
		}
		return firstEmployeeName;
	}

	public ArrayList<String> addTwoEmployees(WebDriver driver, Map<String, String> config, String franchiseID) throws Exception {
		Reporter.log("*********************** Add Two Employess ****************************** ");

		String testCaseID = "TC_InFo_Mgr_CorporateLocations_Add_Employees";

		ArrayList<String> list = new ArrayList<String>();
		searchFranchiseAndClick(driver, franchiseID);

		InfoMgrEmployeesPage employeesPage = new InfoMgrCorporateLocationsPage(driver).getEmployeesPage();
		fc.utobj().clickElement(driver, employeesPage.employeesTab);
		String firstEmployeeName = fc.utobj().generateTestData("Employee1");
		String secondEmployeeName = fc.utobj().generateTestData("Employee2");

		if (fc.utobj().validate(testCaseID) == true) {
			try {

				// Add first employee

				fc.utobj().sendKeys(driver, employeesPage.txtFirstName, firstEmployeeName);
				fc.utobj().sendKeys(driver, employeesPage.txtLastName, fc.utobj().generateTestData("LastName1"));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, employeesPage.btnAdd);
				fc.utobj().clickElement(driver, employeesPage.btnCancel);

				// Add second employee
				fc.utobj().clickElement(driver, employeesPage.btnAddMoreEmp);

				fc.utobj().sendKeys(driver, employeesPage.txtFirstName, secondEmployeeName);
				fc.utobj().sendKeys(driver, employeesPage.txtLastName, fc.utobj().generateTestData("LastName2"));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, employeesPage.btnAdd);
				fc.utobj().clickElement(driver, employeesPage.btnCancel);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseID);
			}
		} else {
			fc.utobj().throwsSkipException("Error in adding owner , please refer screenshot!");
		}

		// Verify that both the employees have been added
		list.add(firstEmployeeName);
		list.add(secondEmployeeName);

		if (fc.utobj().assertPageSource(driver, firstEmployeeName) && fc.utobj().assertPageSource(driver, firstEmployeeName)) {
			Reporter.log("Employees have been added successfully");
		} else {
			fc.utobj().throwsException("Employee couldn't be added . Test couldn't continue !!!");
		}

		return list;

	}

	public String addEntityDetails(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("*********************** Add Entity Details ****************************** ");

		String testCaseId = "TC_InFoMgr_CorporateLocations_Add_Entity_Details";

		Map<String, String> dsEntityDetails = fc.utobj().readTestData("infomgr", testCaseId);

		InfoMgrEntityDetailsPage entityDetailsPage = new InfoMgrCorporateLocationsPage(driver).getEntityDetailsPage();

		String entityName = fc.utobj().generateTestData(dsEntityDetails.get("entityName"));

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().sendKeys(driver, entityDetailsPage.txtEntityName, entityName);
				fc.utobj().sendKeys(driver, entityDetailsPage.txtAddress, dsEntityDetails.get("address"));
				fc.utobj().sendKeys(driver, entityDetailsPage.txtCity, dsEntityDetails.get("city"));
				fc.utobj().selectDropDownByVisibleText(driver, entityDetailsPage.drpCountry, dsEntityDetails.get("country"));
				fc.utobj().sendKeys(driver, entityDetailsPage.txtZipCode, dsEntityDetails.get("zip"));
				fc.utobj().selectDropDownByVisibleText(driver, entityDetailsPage.drpState, dsEntityDetails.get("state"));
				fc.utobj().sendKeys(driver, entityDetailsPage.txtPhoneNumber, dsEntityDetails.get("phone"));
				fc.utobj().sendKeys(driver, entityDetailsPage.txtExtension, dsEntityDetails.get("phoneExt"));
				fc.utobj().sendKeys(driver, entityDetailsPage.txtFaxNumber, dsEntityDetails.get("fax"));
				fc.utobj().sendKeys(driver, entityDetailsPage.txtMobileNumber, dsEntityDetails.get("mobile"));
				fc.utobj().sendKeys(driver, entityDetailsPage.txtEmailIds, dsEntityDetails.get("email"));
				fc.utobj().selectDropDownByVisibleText(driver, entityDetailsPage.drpCountryFormation, dsEntityDetails.get("countryofFormation"));
				fc.utobj().sleep();
				fc.utobj().sendKeys(driver, entityDetailsPage.txtDateFormation, fc.utobj().getFutureDateUSFormat(-5));
				fc.utobj().selectDropDownByVisibleText(driver, entityDetailsPage.drpStateFormation, dsEntityDetails.get("stateofFormation"));
				fc.utobj().sendKeys(driver, entityDetailsPage.txtTaxPayer, dsEntityDetails.get("taxpayerID"));
				fc.utobj().selectDropDownByVisibleText(driver, entityDetailsPage.drpEntityType, dsEntityDetails.get("entityType"));
				fc.utobj().sendKeys(driver, entityDetailsPage.txtFormationDocumentTitle, dsEntityDetails.get("formationDocTitle"));
				fc.utobj().sendKeys(driver, entityDetailsPage.txtFormationDocumentAttachment, dsEntityDetails.get("formationDocAttachment"));
				fc.utobj().sendKeys(driver, entityDetailsPage.txtGoverningDocumentTitle, dsEntityDetails.get("govereningDocumentTitle"));
				fc.utobj().sendKeys(driver, entityDetailsPage.txtGoverningDocumentAttachment, dsEntityDetails.get("govreeningDocAttachment"));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, entityDetailsPage.btnAdd);
				if (fc.utobj().assertPageSource(driver, entityName)) {
					Reporter.log("Entity details have been added successfully");
				} else {
					fc.utobj().throwsException("Entity detail couldn't be added . Test couldn't continue !!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in adding entity details , please refer screenshot!");
		}
		return entityName;
	}

	public String addEventDetails(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("*********************** Add Event Details ****************************** ");

		String testCaseId = "TC_InFoMgr_CorporateLocations_Add_Event_Details";

		Map<String, String> dsEventDetails = fc.utobj().readTestData("infomgr", testCaseId);

		InfoMgrEventsPage eventsPage = new InfoMgrCorporateLocationsPage(driver).getEventsPage();

		String eventSummary = fc.utobj().generateTestData(dsEventDetails.get("summary"));

		if (fc.utobj().validate(testCaseId) == true) {
			try {

				fc.utobj().sendKeys(driver, eventsPage.txtDate, fc.utobj().getFutureDateUSFormat(10));
				fc.utobj().sendKeys(driver, eventsPage.txtOrganizer, dsEventDetails.get("organizer"));
				fc.utobj().sendKeys(driver, eventsPage.txtType, dsEventDetails.get("type"));
				fc.utobj().sendKeys(driver, eventsPage.txtSummary, eventSummary);
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, eventsPage.btnAdd);
				if (fc.utobj().assertPageSource(driver, eventSummary)) {
					Reporter.log("Event details have been added successfully");
				} else {
					fc.utobj().throwsException("Event detail couldn't be added . Test couldn't continue !!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in adding event details , please refer screenshot!");
		}
		return eventSummary;
	}

	public String addGuarantor(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("*********************** Add Gurantor Details ****************************** ");

		String testCaseId = "TC_InFoMgr_CorporateLocations_Add_Gurantor_Details";

		Map<String, String> dsGuarantor = fc.utobj().readTestData("infomgr", testCaseId);

		InfoMgrGuarantorsPage guarantorPage = new InfoMgrCorporateLocationsPage(driver).getGuarantorsPage();

		String guarantorName = fc.utobj().generateTestData(dsGuarantor.get("firstName"));

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().selectDropDownByVisibleText(driver, guarantorPage.drpTitle, dsGuarantor.get("title"));
				fc.utobj().sendKeys(driver, guarantorPage.txtFirstName, guarantorName);
				fc.utobj().sendKeys(driver, guarantorPage.txtLastName, dsGuarantor.get("lastName"));
				fc.utobj().selectDropDownByVisibleText(driver, guarantorPage.drpEntityType, dsGuarantor.get("entityType"));
				fc.utobj().selectDropDownByVisibleText(driver, guarantorPage.drpResidencyState, dsGuarantor.get("stateOfFormation"));
				fc.utobj().sendKeys(driver, guarantorPage.txtTaxPayer, dsGuarantor.get("taxPayerID"));
				fc.utobj().sendKeys(driver, guarantorPage.txtPercentage, dsGuarantor.get("percentage"));
				fc.utobj().sendKeys(driver, guarantorPage.txtStreetAddress, dsGuarantor.get("address"));
				fc.utobj().sendKeys(driver, guarantorPage.txtCity, dsGuarantor.get("city"));
				fc.utobj().selectDropDownByVisibleText(driver, guarantorPage.drpCountry, dsGuarantor.get("country"));
				fc.utobj().sendKeys(driver, guarantorPage.txtZipCode, dsGuarantor.get("zip"));
				fc.utobj().selectDropDownByVisibleText(driver, guarantorPage.drpState, dsGuarantor.get("state"));
				fc.utobj().sendKeys(driver, guarantorPage.txtPhoneNumbers, dsGuarantor.get("phone"));
				fc.utobj().sendKeys(driver, guarantorPage.txtExtension, dsGuarantor.get("phoneExt"));
				fc.utobj().sendKeys(driver, guarantorPage.txtFaxNumbers, dsGuarantor.get("fax"));
				fc.utobj().sendKeys(driver, guarantorPage.txtMobileNumbers, dsGuarantor.get("mobile"));
				fc.utobj().sendKeys(driver, guarantorPage.txtEmailIds, dsGuarantor.get("email"));
				fc.utobj().sendKeys(driver, guarantorPage.txtComments, dsGuarantor.get("comments"));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, guarantorPage.btnAdd);
				if (fc.utobj().assertPageSource(driver, guarantorName)) {
					Reporter.log("guarantor details have been added successfully");
				} else {
					fc.utobj().throwsException("guarantor detail couldn't be added . Test couldn't continue !!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in adding guarantor details , please refer screenshot!");
		}
		return guarantorName;
	}

	public String addLegalViolation(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("*********************** Add Legal Violation Details ****************************** ");

		String testCaseId = "TC_InFoMgr_CorporateLocations_Add_Legal_Violation_Details";

		Map<String, String> dsLegalViolation = fc.utobj().readTestData("infomgr", testCaseId);

		InfoMgrLegalViolationPage legalViolationPage = new InfoMgrCorporateLocationsPage(driver).getLegalViolationPage();

		String legalViolationNumber = fc.utobj().generateRandomNumber();
		String subject = fc.utobj().generateTestData(dsLegalViolation.get("summary"));

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().sendKeys(driver, legalViolationPage.txtNumber, legalViolationNumber);
				fc.utobj().sendKeys(driver, legalViolationPage.txtDate, fc.utobj().getFutureDateUSFormat(10));
				fc.utobj().selectDropDownByVisibleText(driver, legalViolationPage.drpType, dsLegalViolation.get("type"));
				fc.utobj().selectDropDownByVisibleText(driver, legalViolationPage.drpStatus, dsLegalViolation.get("status"));
				fc.utobj().sendKeys(driver, legalViolationPage.txtCureDate, fc.utobj().getFutureDateUSFormat(10));
				fc.utobj().sendKeys(driver, legalViolationPage.txtDateCured, fc.utobj().getFutureDateUSFormat(15));
				fc.utobj().sendKeys(driver, legalViolationPage.txtSummary, subject);
				fc.utobj().sendKeys(driver, legalViolationPage.txtActionTaken, dsLegalViolation.get("actionTaken"));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, legalViolationPage.btnAdd);
				if (fc.utobj().assertPageSource(driver, subject)) {
					Reporter.log("Legal Violation details have been added successfully");
				} else {
					fc.utobj().throwsException("Legal violation detail couldn't be added . Test couldn't continue !!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in adding legal violation details , please refer screenshot!");
		}
		return subject;
	}

	public String addLenders(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("*********************** Add Lenders Details ****************************** ");

		String testCaseId = "TC_InFoMgr_CorporateLocations_Add_Lenders_Details";

		Map<String, String> dsLenders = fc.utobj().readTestData("infomgr", testCaseId);

		InfoMgrLendersPage lendersPage = new InfoMgrCorporateLocationsPage(driver).getLendersPage();

		String lendersFirstName = fc.utobj().generateTestData(dsLenders.get("firstName"));

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().selectDropDownByVisibleText(driver, lendersPage.drpTitle, dsLenders.get("title"));
				fc.utobj().sendKeys(driver, lendersPage.txtFirstName, lendersFirstName);
				fc.utobj().sendKeys(driver, lendersPage.txtLastName, dsLenders.get("lastName"));
				fc.utobj().sendKeys(driver, lendersPage.txtStreetaddress, dsLenders.get("streetAddress"));
				fc.utobj().sendKeys(driver, lendersPage.txtCity, dsLenders.get("city"));
				fc.utobj().selectDropDownByVisibleText(driver, lendersPage.drpCountry, dsLenders.get("country"));
				fc.utobj().sendKeys(driver, lendersPage.txtZipcCode, dsLenders.get("zip"));
				fc.utobj().selectDropDownByVisibleText(driver, lendersPage.drpstate, dsLenders.get("state"));
				fc.utobj().sendKeys(driver, lendersPage.txtCollateralAssignmentExpirationDate, fc.utobj().getFutureDateUSFormat(10));
				// fc.utobj().sendKeys(driver, lendersPage.txtComfortLetterForm,
				// dsLenders.get("formofComfortLetter") );
				fc.utobj().sendKeys(driver, lendersPage.txtComfortLetterDate, fc.utobj().getFutureDateUSFormat(10));

				fc.utobj().clickRadioButton(driver, lendersPage.rdoComfortAgreement, dsLenders.get("comfortAgreement"));

				fc.utobj().sendKeys(driver, lendersPage.txtDateComfortAgreementInfo, fc.utobj().getFutureDateUSFormat(10));

				fc.utobj().sendKeys(driver, lendersPage.txtDocumentTitle, dsLenders.get("formationDocumentTitle"));

				fc.utobj().sendKeys(driver, lendersPage.txtDocumentAttachment, dsLenders.get("documentAttachment"));

				fc.utobj().sendKeys(driver, lendersPage.txtLenderContactOne, dsLenders.get("contact1Name"));

				fc.utobj().sendKeys(driver, lendersPage.txtLenderContactTwo, dsLenders.get("contact2Name"));

				fc.utobj().sendKeys(driver, lendersPage.txtContact1Phone, dsLenders.get("contact1Phone"));
				fc.utobj().sendKeys(driver, lendersPage.txtContact2Phone, dsLenders.get("contact2Phone"));
				fc.utobj().sendKeys(driver, lendersPage.txtContact1PhoneExtn, dsLenders.get("contact1PhoneExtension"));
				fc.utobj().sendKeys(driver, lendersPage.txtContact2PhoneExtn, dsLenders.get("contact2PhoneExtension"));

				fc.utobj().sendKeys(driver, lendersPage.txtContact1Fax, dsLenders.get("contact1Fax"));
				fc.utobj().sendKeys(driver, lendersPage.txtContact2Fax, dsLenders.get("contact2Fax"));

				fc.utobj().sendKeys(driver, lendersPage.txtContact1Email, dsLenders.get("contact1Email"));

				fc.utobj().sendKeys(driver, lendersPage.txtContact2Email, dsLenders.get("contact2Email"));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, lendersPage.btnAdd);
				if (fc.utobj().assertPageSource(driver, lendersFirstName)) {
					Reporter.log("Lenders details have been added successfully");
				} else {
					fc.utobj().throwsException("Lenders detail couldn't be added . Test couldn't continue !!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in adding lenders details , please refer screenshot!");
		}
		return lendersFirstName;
	}

	public String addMarketingDetails(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("*********************** Add Marketing Details ****************************** ");

		String testCaseId = "TC_InFoMgr_CorporateLocations_Add_Marketing_Details";

		Map<String, String> dsMarketing = fc.utobj().readTestData("infomgr", testCaseId);

		InfoMgrMarketingPage marketingPage = new InfoMgrCorporateLocationsPage(driver).getMarketingPage();

		String marketingContactFirstName = fc.utobj().generateTestData(dsMarketing.get("firstName"));

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().selectDropDownByVisibleText(driver, marketingPage.drpTitle, dsMarketing.get("title"));
				fc.utobj().sendKeys(driver, marketingPage.txtFirstName, marketingContactFirstName);
				fc.utobj().sendKeys(driver, marketingPage.txtLastName, dsMarketing.get("lastName"));
				fc.utobj().sendKeys(driver, marketingPage.txtStreetAddress, dsMarketing.get("streetAddress"));
				fc.utobj().sendKeys(driver, marketingPage.txtCity, dsMarketing.get("city"));
				fc.utobj().selectDropDownByVisibleText(driver, marketingPage.drpCountry, dsMarketing.get("country"));
				fc.utobj().sendKeys(driver, marketingPage.txtZipCode, dsMarketing.get("zip"));
				fc.utobj().selectDropDownByVisibleText(driver, marketingPage.drpState, dsMarketing.get("state"));
				fc.utobj().sendKeys(driver, marketingPage.txtPhoneNumbers, dsMarketing.get("phone"));
				fc.utobj().sendKeys(driver, marketingPage.txtExtension, dsMarketing.get("phoneExtension"));
				fc.utobj().sendKeys(driver, marketingPage.txtFaxNumbers, dsMarketing.get("fax"));
				fc.utobj().sendKeys(driver, marketingPage.txtMobileNumbers, dsMarketing.get("mobile"));
				fc.utobj().sendKeys(driver, marketingPage.txtEmailIds, dsMarketing.get("email"));
				fc.utobj().clickRadioButton(driver, marketingPage.rdoGrandOpeningRequired, dsMarketing.get("grandOpeningRequired"));
				fc.utobj().sendKeys(driver, marketingPage.txtGrandOpeningCompletedDate, fc.utobj().getFutureDateUSFormat(10));

				fc.utobj().clickRadioButton(driver, marketingPage.rdoCouponRedemption, dsMarketing.get("couponRedemption"));

				fc.utobj().sendKeys(driver, marketingPage.txtCampaignName, dsMarketing.get("campaignName"));

				fc.utobj().clickRadioButton(driver, marketingPage.rdoCampaignParticipation, dsMarketing.get("campaignParticipation"));

				fc.utobj().sendKeys(driver, marketingPage.txtProgramName, dsMarketing.get("programName"));

				fc.utobj().clickRadioButton(driver, marketingPage.rdoProgramParticipation, dsMarketing.get("programParticipation"));

				fc.utobj().sendKeys(driver, marketingPage.txtDMA, dsMarketing.get("DMA"));
				fc.utobj().sendKeys(driver, marketingPage.txtComments, dsMarketing.get("comments"));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, marketingPage.btnSubmit);

				if (fc.utobj().assertPageSource(driver, marketingContactFirstName)) {
					Reporter.log("Marketing details have been added successfully");
				} else {
					fc.utobj().throwsException("MArketing detail couldn't be added . Test couldn't continue !!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in adding marketing details , please refer screenshot!");
		}
		return marketingContactFirstName;

	}

	public String addMystryReview(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("*********************** Add Mystry Review ****************************** ");

		String testCaseId = "TC_InFoMgr_CorporateLocations_Add_Mystry_Review_Details";

		Map<String, String> dsMystryReview = fc.utobj().readTestData("infomgr", testCaseId);

		InfoMgrMystryReviewPage mystryReviewPage = new InfoMgrCorporateLocationsPage(driver).getMystryReviewPage();

		String mystryReviewComments = fc.utobj().generateTestData(dsMystryReview.get("comments"));

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().sendKeys(driver, mystryReviewPage.txtInspectionDate, fc.utobj().getFutureDateUSFormat(-5));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtReportSentToFranchiseeDate, fc.utobj().getFutureDateUSFormat(-2));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtGeneralComments, mystryReviewComments);
				// Performance section
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValuePerformanceQ1, dsMystryReview.get("perfQuestion1"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValuePerformanceQ2, dsMystryReview.get("perfQuestion2"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValuePerformanceQ3, dsMystryReview.get("perfQuestion3"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValuePerformanceQ4, dsMystryReview.get("perfQuestion4"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValuePerformanceQ5, dsMystryReview.get("perfQuestion5"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValuePerformanceQ6, dsMystryReview.get("perfQuestion6"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValuePerformanceQ7, dsMystryReview.get("perfQuestion7"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValuePerformanceQ8, dsMystryReview.get("perfQuestion8"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtCommentsForPerformance, dsMystryReview.get("perfComments"));
				// Service Section
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValueServiceQ1, dsMystryReview.get("serviceQuestion1"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValueServiceQ2, dsMystryReview.get("serviceQuestion2"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValueServiceQ3, dsMystryReview.get("serviceQuestion3"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValueServiceQ4, dsMystryReview.get("serviceQuestion4"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValueServiceQ5, dsMystryReview.get("serviceQuestion5"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValueServiceQ6, dsMystryReview.get("serviceQuestion6"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValueServiceQ7, dsMystryReview.get("serviceQuestion7"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValueServiceQ8, dsMystryReview.get("serviceQuestion8"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValueServiceQ9, dsMystryReview.get("serviceQuestion9"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValueServiceQ10, dsMystryReview.get("serviceQuestion10"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValueServiceQ11, dsMystryReview.get("serviceQuestion11"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValueServiceQ12, dsMystryReview.get("serviceQuestion12"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtCommentsForService, dsMystryReview.get("serviceComments"));
				// Appearance section
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValueAppearanceQ1, dsMystryReview.get("appearanceQuestion1"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValueAppearanceQ2, dsMystryReview.get("appearanceQuestion2"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValueAppearanceQ3, dsMystryReview.get("appearanceQuestion3"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValueAppearanceQ4, dsMystryReview.get("appearanceQuestion4"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValueAppearanceQ5, dsMystryReview.get("appearanceQuestion5"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtCommentsForAppearance, dsMystryReview.get("appearanceComments"));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, mystryReviewPage.btnSubmit);
				if (fc.utobj().assertPageSource(driver, mystryReviewComments)) {
					Reporter.log("Mystry Review details have been added successfully");
				} else {
					fc.utobj().throwsException("Mystry review detail couldn't be added . Test couldn't continue !!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in adding mystry review details , please refer screenshot!");
		}
		return mystryReviewComments;
	}

	public String addPicture(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("*********************** Add Picture ****************************** ");

		String testCaseId = "TC_InFoMgr_CorporateLocations_Add_Picture";

		Map<String, String> dsPicture = fc.utobj().readTestData("infomgr", testCaseId);

		InfoMgrPicturesPage picturePage = new InfoMgrCorporateLocationsPage(driver).getPicturesPage();

		String pictureTitle = fc.utobj().generateTestData(dsPicture.get("title"));

		try {
			fc.utobj().clickElement(driver, picturePage.btnAddMore);
		} catch (Exception ex) {

		}

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().sendKeys(driver, picturePage.txtpictureTitle, pictureTitle);
				fc.utobj().sendKeys(driver, picturePage.txtpictureFilename, dsPicture.get("attachment"));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, picturePage.btnSubmit);
				if (fc.utobj().assertPageSource(driver, pictureTitle)) {
					Reporter.log("Picture has been added successfully");
				} else {
					fc.utobj().throwsException("Picture couldn't be added . Test couldn't continue !!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in adding picture , please refer screenshot!");
		}
		return pictureTitle;
	}

	public String addOwnerUser(WebDriver driver, Map<String, String> config, String owner) throws Exception {
		Reporter.log("*********************** Add User ****************************** ");

		String testCaseId = "TC_InFoMgr_CorporateLocations_Add_User";

		Map<String, String> dsUser = fc.utobj().readTestData("infomgr", testCaseId);

		InfoMgrUserPage userPage = new InfoMgrCorporateLocationsPage(driver).getUsersPage().getUserPage();
		;

		String loginID = fc.utobj().generateTestData(dsUser.get("loginID"));
		String phone = fc.utobj().generateRandomNumber();

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().sendKeys(driver, userPage.txtUserName, loginID);
				fc.utobj().sendKeys(driver, userPage.txtPassword, dsUser.get("password"));
				fc.utobj().sendKeys(driver, userPage.txtconfirmPassword, dsUser.get("confirmpassword"));
				fc.utobj().selectDropDownByVisibleText(driver, userPage.drpType, dsUser.get("type"));
				fc.utobj().selectValFromMultiSelect(driver, userPage.drpRoles, dsUser.get("role"));
				fc.utobj().check(userPage.chkIsDaylight, dsUser.get("allowDST"));
				fc.utobj().selectDropDownByVisibleText(driver, userPage.drpUserType, dsUser.get("userType"));
				fc.utobj().selectDropDownByVisibleText(driver, userPage.drpOwnerName, owner);
				fc.utobj().sendKeys(driver, userPage.txtJobTitle, dsUser.get("jobTitle"));
				// fc.utobj().selectDropDownByVisibleText(driver,
				// userPage.drpUserLanguage, dsUser.get("language") ); // No use
				// to change language
				fc.utobj().sendKeys(driver, userPage.txtAddress, dsUser.get("streetAddress"));
				fc.utobj().sendKeys(driver, userPage.txtCity, dsUser.get("city"));
				fc.utobj().selectDropDownByVisibleText(driver, userPage.drpCountry, dsUser.get("country"));
				fc.utobj().sendKeys(driver, userPage.txtZipcode, dsUser.get("zip"));
				fc.utobj().selectDropDownByVisibleText(driver, userPage.drpState, dsUser.get("state"));
				fc.utobj().sendKeys(driver, userPage.txtPhone1, phone);
				fc.utobj().sendKeys(driver, userPage.txtPhoneExt1, dsUser.get("phone1Ext"));
				fc.utobj().sendKeys(driver, userPage.txtPhone2, dsUser.get("phone2"));
				fc.utobj().sendKeys(driver, userPage.txtPhoneExt2, dsUser.get("phone2Ext"));
				fc.utobj().sendKeys(driver, userPage.txtFax, dsUser.get("fax"));
				fc.utobj().sendKeys(driver, userPage.txtMobile, dsUser.get("mobile"));
				fc.utobj().sendKeys(driver, userPage.txtEmail, dsUser.get("email"));
				fc.utobj().sendKeys(driver, userPage.txtIpAddress, dsUser.get("ipAddress"));
				fc.utobj().check(userPage.chkIsBillable, dsUser.get("isBillable"));
				fc.utobj().selectDropDownByPartialText(driver, userPage.drpBirthMonth, dsUser.get("birthMonth"));
				fc.utobj().selectDropDownByPartialText(driver, userPage.drpBirthDate, dsUser.get("birthDate"));
				fc.utobj().sendKeys(driver, userPage.btnUserPictureName, dsUser.get("userPicture"));
				fc.utobj().sendKeys(driver, userPage.chkSendNotification, dsUser.get("sendNotification"));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, userPage.btnSubmit);
				if (fc.utobj().assertPageSource(driver, phone)) {
					Reporter.log("User has been added successfully");
				} else {
					fc.utobj().throwsException("User couldn't be added . Test couldn't continue !!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in adding user , please refer screenshot!");
		}
		return loginID;
	}

	public void addRealState(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("*********************** Add Real Estate ****************************** ");

		String testCaseId = "TC_InFoMgr_CorporateLocations_Add_Real_Estate_Details";

		Map<String, String> dsRealState = fc.utobj().readTestData("infomgr", testCaseId);

		InfoMgrRealStatePage realStatePage = new InfoMgrCorporateLocationsPage(driver).getRealStatePage();

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().sendKeys(driver, realStatePage.drpOwnedLeased, dsRealState.get("ownedorLeased"));
				fc.utobj().sendKeys(driver, realStatePage.txtSiteAddress1, dsRealState.get("siteStreet1"));
				fc.utobj().sendKeys(driver, realStatePage.txtSiteAddress2, dsRealState.get("siteStreet2"));
				fc.utobj().sendKeys(driver, realStatePage.txtSiteCity, dsRealState.get("siteCity"));
				fc.utobj().sendKeys(driver, realStatePage.drpCountry, dsRealState.get("country"));
				fc.utobj().sleep();
				fc.utobj().sendKeys(driver, realStatePage.drpState, dsRealState.get("state/Province"));
				fc.utobj().sendKeys(driver, realStatePage.txtSquareFootage, dsRealState.get("buildingSize"));
				fc.utobj().sendKeys(driver, realStatePage.txtBuildingDimentionsX, dsRealState.get("buildingLength"));
				fc.utobj().sendKeys(driver, realStatePage.txtBuildingDimentionsY, dsRealState.get("buildingBreadth"));
				fc.utobj().sendKeys(driver, realStatePage.txtBuildingDimentionsZ, dsRealState.get("buildingHeight"));
				fc.utobj().sendKeys(driver, realStatePage.txtParkingSpaces, dsRealState.get("parkingSpaces"));
				fc.utobj().sendKeys(driver, realStatePage.txtDealType, dsRealState.get("dealType"));
				fc.utobj().sendKeys(driver, realStatePage.drpPremisesType, dsRealState.get("typeofPremises"));
				fc.utobj().sendKeys(driver, realStatePage.txtLoiSent, fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, realStatePage.txtLeaseSignedDate, fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, realStatePage.txtApprovalDate, fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, realStatePage.txtLeaseCommencementDate, fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, realStatePage.txtExpirationDate, fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, realStatePage.txtRentIncreases, fc.utobj().getFutureDateUSFormat(100));

				fc.utobj().sendKeys(driver, realStatePage.txtMonthlyRent, dsRealState.get("currentMonthlyRent"));
				fc.utobj().clickRadioButton(driver, realStatePage.rdoAcal, dsRealState.get("ACAL"));
				fc.utobj().sendKeys(driver, realStatePage.txtDocumentTitle, dsRealState.get("documentTitle"));
				fc.utobj().sendKeys(driver, realStatePage.txtDocumentAttachment, dsRealState.get("document"));

				// Term First
				fc.utobj().sendKeys(driver, realStatePage.txtRenewalTermFirst, dsRealState.get("termFirst(Years)"));
				fc.utobj().sendKeys(driver, realStatePage.txtRenewalDueDateFirst, fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, realStatePage.txtRenewalFeeFirst, dsRealState.get("Fee-First"));
				fc.utobj().sendKeys(driver, realStatePage.txtRenewalFeePaidFirst, fc.utobj().getFutureDateUSFormat(-10));

				// Term Second
				fc.utobj().sendKeys(driver, realStatePage.txtRenewalTermSecond, dsRealState.get("termSecond"));
				fc.utobj().sendKeys(driver, realStatePage.txtRenewalDueDateSecond, fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, realStatePage.txtRenewalFeeSecond, dsRealState.get("feeSecond"));
				fc.utobj().sendKeys(driver, realStatePage.txtRenewalFeePaidSecond, fc.utobj().getFutureDateUSFormat(-10));

				// Term Third
				fc.utobj().sendKeys(driver, realStatePage.txtRenewalTermThird, dsRealState.get("termThird"));
				fc.utobj().sendKeys(driver, realStatePage.txtRenewalDueDateThird, fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, realStatePage.txtRenewalFeeThird, dsRealState.get("feeThird"));
				fc.utobj().sendKeys(driver, realStatePage.txtRenewalFeePaidThird, fc.utobj().getFutureDateUSFormat(-10));

				fc.utobj().sendKeys(driver, realStatePage.txtRenewalOptions, dsRealState.get("optionTerm(Years)"));
				fc.utobj().clickRadioButton(driver, realStatePage.rdoPurchaseOption, dsRealState.get("purchaseOption"));
				fc.utobj().sendKeys(driver, realStatePage.txtProjectedOpeningDate, fc.utobj().getFutureDateUSFormat(-10));

				fc.utobj().clickRadioButton(driver, realStatePage.rdoGeneralContractorSelector, dsRealState.get("generalContractorSelected"));
				if (dsRealState.get("generalContractorSelected").equalsIgnoreCase("yes")) {
					fc.utobj().sendKeys(driver, realStatePage.txtNameGeneralContractor, dsRealState.get("generalContractorName"));
					fc.utobj().sendKeys(driver, realStatePage.txtAddressGeneralContractor, dsRealState.get("generalContractorAddress"));
				}

				fc.utobj().sendKeys(driver, realStatePage.txtPermitApplied, fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, realStatePage.txtPermitIssued, fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, realStatePage.txtCertificate, fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, realStatePage.txtTurnOverDate, fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, realStatePage.txtGrandOpeningDate, fc.utobj().getFutureDateUSFormat(-10));

				// Lessor details
				fc.utobj().selectDropDownByVisibleText(driver, realStatePage.drpLessorTitle, dsRealState.get("lessor_title"));
				fc.utobj().sendKeys(driver, realStatePage.txtLessorFirstName, dsRealState.get("lessor_firstName"));
				fc.utobj().sendKeys(driver, realStatePage.txtLessorLastName, dsRealState.get("lessor_lastName"));
				fc.utobj().sendKeys(driver, realStatePage.txtLessorStreetAddress, dsRealState.get("lessor_streetAddress"));
				fc.utobj().sendKeys(driver, realStatePage.txtLessorCity, dsRealState.get("lessor_city"));
				fc.utobj().selectDropDownByVisibleText(driver, realStatePage.drpLessorCountry, dsRealState.get("lessor_country"));
				fc.utobj().sleep();
				fc.utobj().sendKeys(driver, realStatePage.txtLessorZipCode, dsRealState.get("lessor_zip"));
				fc.utobj().selectDropDownByVisibleText(driver, realStatePage.drpLessorState, dsRealState.get("lessor_state"));
				fc.utobj().sendKeys(driver, realStatePage.txtLessorPhoneNumbers, dsRealState.get("lessor_phone"));
				fc.utobj().sendKeys(driver, realStatePage.txtLessorExtension, dsRealState.get("lessor_phoneExtension"));
				fc.utobj().sendKeys(driver, realStatePage.txtLessorFaxNumbers, dsRealState.get("lessor_fax"));
				fc.utobj().sendKeys(driver, realStatePage.txtLessorMobileNumbers, dsRealState.get("lessor_mobile"));
				fc.utobj().sendKeys(driver, realStatePage.txtLessorEmailIds, dsRealState.get("lessor_email"));

				// Sub-lessor details
				fc.utobj().selectDropDownByVisibleText(driver, realStatePage.drpSubLessorTitle, dsRealState.get("sublessor_title"));
				fc.utobj().sendKeys(driver, realStatePage.txtSublessorFirstName, dsRealState.get("sublessor_firstName"));
				fc.utobj().sendKeys(driver, realStatePage.txtSublessorLastName, dsRealState.get("sublessor_lastName"));
				fc.utobj().sendKeys(driver, realStatePage.txtSubLessorStreetAddress, dsRealState.get("sublessor_streetAddress"));
				fc.utobj().sendKeys(driver, realStatePage.txtSubLessorCity, dsRealState.get("sublessor_city"));
				fc.utobj().selectDropDownByVisibleText(driver, realStatePage.drpSubLessorCountry, dsRealState.get("sublessor_country"));
				fc.utobj().sleep();
				fc.utobj().sendKeys(driver, realStatePage.txtSubLessorZipCode, dsRealState.get("sublessor_zip"));
				fc.utobj().selectDropDownByVisibleText(driver, realStatePage.drpSubLessorState, dsRealState.get("sublessor_state"));
				fc.utobj().sendKeys(driver, realStatePage.txtSubLessorPhoneNumbers, dsRealState.get("sublessor_phone"));
				fc.utobj().sendKeys(driver, realStatePage.txtSubLessorExtension, dsRealState.get("sublessor_phoneExtension"));
				fc.utobj().sendKeys(driver, realStatePage.txtSubLessorFaxNumbers, dsRealState.get("sublessor_fax"));
				fc.utobj().sendKeys(driver, realStatePage.txtSubLessorMobileNumbers, dsRealState.get("sublessor_mobile"));
				fc.utobj().sendKeys(driver, realStatePage.txtSubLessorEmailIds, dsRealState.get("sublessor_email"));

				// Tenant Details
				fc.utobj().selectDropDownByVisibleText(driver, realStatePage.drpTenantTitle, dsRealState.get("tenant_title"));
				fc.utobj().sendKeys(driver, realStatePage.txtTenantFirstName, dsRealState.get("tenant_firstName"));
				fc.utobj().sendKeys(driver, realStatePage.txtTenantLastName, dsRealState.get("tenant_lastName"));
				fc.utobj().sendKeys(driver, realStatePage.txtTenantStreetAddress, dsRealState.get("tenant_streetAddress"));
				fc.utobj().sendKeys(driver, realStatePage.txtTenantCity, dsRealState.get("tenant_city"));
				fc.utobj().selectDropDownByVisibleText(driver, realStatePage.drpTenantCountry, dsRealState.get("tenant_country"));
				fc.utobj().sleep();
				fc.utobj().sendKeys(driver, realStatePage.txtTenantZipCode, dsRealState.get("tenant_zip"));
				fc.utobj().selectDropDownByVisibleText(driver, realStatePage.drpTenantState, dsRealState.get("tenant_state"));
				fc.utobj().sendKeys(driver, realStatePage.txtTenantPhoneNumbers, dsRealState.get("tenant_phone"));
				fc.utobj().sendKeys(driver, realStatePage.txtTenantExtension, dsRealState.get("tenant_phoneExtension"));
				fc.utobj().sendKeys(driver, realStatePage.txtTenantFaxNumbers, dsRealState.get("tenant_fax"));
				fc.utobj().sendKeys(driver, realStatePage.txtTenantMobileNumbers, dsRealState.get("tenant_mobile"));
				fc.utobj().sendKeys(driver, realStatePage.txtTenantEmailIds, dsRealState.get("tenant_email"));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, realStatePage.btnAdd);

				if (fc.utobj().assertLinkText(driver, "Modify")) {
					Reporter.log("Real Estate details has been added successfully");
				} else {
					fc.utobj().throwsException("Real Estate Details couldn't be added . Test couldn't continue !!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in adding real estate details , please refer screenshot!");
		}
	}

	public String addTerritory(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("*********************** Add Territory Details ****************************** ");

		String testCaseId = "TC_InFoMgr_CorporateLocations_Add_Territory_Details";

		Map<String, String> dsTerritory = fc.utobj().readTestData("infomgr", testCaseId);

		InfoMgrTerritoryPage territoryPage = new InfoMgrCorporateLocationsPage(driver).getTerritoryPage();

		String txtNotes = fc.utobj().generateTestData(dsTerritory.get("notes"));

		if (fc.utobj().validate(testCaseId) == true) {
			try {

				fc.utobj().clickRadioButton(driver, territoryPage.rdoTypeTerritory, dsTerritory.get("typeofTerritory"));
				fc.utobj().sendKeys(driver, territoryPage.txtSalesRequirement, dsTerritory.get("minimumSalesRequirement"));
				fc.utobj().sendKeys(driver, territoryPage.txtRestrictions, dsTerritory.get("restrictionsonFranchisee"));
				fc.utobj().sendKeys(driver, territoryPage.txtMarketingObligation, dsTerritory.get("franchiseesMarketingObligations"));
				fc.utobj().sendKeys(driver, territoryPage.txtOwned, dsTerritory.get("otherExclusiveTerritoriesOwned"));
				fc.utobj().sendKeys(driver, territoryPage.txtJuridiction, dsTerritory.get("jurisdiction"));
				fc.utobj().sendKeys(driver, territoryPage.txtDisputes, dsTerritory.get("disputesifany"));
				fc.utobj().sendKeys(driver, territoryPage.txtNotes, txtNotes);
				fc.utobj().sendKeys(driver, territoryPage.txtGeoCoordinates, dsTerritory.get("geographicCoordinates"));
				fc.utobj().sendKeys(driver, territoryPage.txtLocation, dsTerritory.get("location"));
				fc.utobj().sendKeys(driver, territoryPage.txtCounty, dsTerritory.get("county"));
				/*
				 * fc.utobj().sendKeys(driver, territoryPage.txtZip,
				 * dsTerritory.get("zipOwnedExclusively"));
				 * fc.utobj().clickRadioButton(driver,
				 * territoryPage.rdoZipLocatorIdentical,
				 * dsTerritory.get("exclusiveZipsLocatorZipsIdentical"));
				 * fc.utobj().sendKeys(driver,
				 * territoryPage.txtZipOwnedForLocator,
				 * dsTerritory.get("ZipOwnedForLocator"));
				 * fc.utobj().check(territoryPage.chkBoxSync,
				 * dsTerritory.get("updateAdminZipLocator"));
				 */
				fc.utobj().sendKeys(driver, territoryPage.txtLandBoundaries, dsTerritory.get("landBoundaries"));
				fc.utobj().sendKeys(driver, territoryPage.txtAraeSize, dsTerritory.get("areaSize"));
				fc.utobj().sendKeys(driver, territoryPage.txtNaturalHazards, dsTerritory.get("naturalHazards"));
				fc.utobj().sendKeys(driver, territoryPage.txtCompetitorsFranchise, dsTerritory.get("otherCompetitorsFranchiseTerritory"));
				fc.utobj().sendKeys(driver, territoryPage.txtProximity, dsTerritory.get("proximity"));
				fc.utobj().sendKeys(driver, territoryPage.txtCompetitionFive, dsTerritory.get("projectedCompetitionFiveYears"));
				fc.utobj().sendKeys(driver, territoryPage.txtCompetitionTen, dsTerritory.get("projectedCompetitionTenYear"));
				fc.utobj().sendKeys(driver, territoryPage.txtPopulation, dsTerritory.get("totalPopulation"));
				fc.utobj().sendKeys(driver, territoryPage.txtMedianIncome, dsTerritory.get("medianIncome"));
				fc.utobj().sendKeys(driver, territoryPage.txtPortsHarbors, dsTerritory.get("portsandHarbors"));
				fc.utobj().sendKeys(driver, territoryPage.txtAirport, dsTerritory.get("airports"));
				fc.utobj().sendKeys(driver, territoryPage.txtHeliports, dsTerritory.get("heliports"));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, territoryPage.btnSubmit);
				if (fc.utobj().assertPageSource(driver, txtNotes)) {
					Reporter.log("Territory details has been added successfully");
				} else {
					fc.utobj().throwsException("Territory details couldn't be added . Test couldn't continue !!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in adding territory details , please refer screenshot!");
		}
		return txtNotes;
	}

	public void addFinanceDetails(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("*********************** Add Finance Details ****************************** ");

		String testCaseId = "TC_InFoMgr_CorporateLocations_Add_Finance_Details";

		Map<String, String> dsFinance = fc.utobj().readTestData("infomgr", testCaseId);

		InfoMgrFinancialsPage financePage = new InfoMgrCorporateLocationsPage(driver).getFinancialsPage();

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().clickRadioButton(driver, financePage.rdoFinStatOnFile, dsFinance.get("financialStatementsonFile"));
				fc.utobj().clickRadioButton(driver, financePage.rdoFinStatApproved, dsFinance.get("financeApproved"));
				fc.utobj().sendKeys(driver, financePage.txtCashRevenue, dsFinance.get("initialFeePaidCash"));
				fc.utobj().sendKeys(driver, financePage.txtFranchiseeFee, dsFinance.get("initialFranchiseFeeTotal"));
				fc.utobj().sendKeys(driver, financePage.txtNoteRevenue, dsFinance.get("initialFeePaidNote"));

				fc.utobj().sendKeys(driver, financePage.txtPNPreparedDate, fc.utobj().getFutureDateUSFormat(10));
				fc.utobj().sendKeys(driver, financePage.txtPNDated, fc.utobj().getFutureDateUSFormat(10));
				fc.utobj().sendKeys(driver, financePage.txtPaymentPlanDated, fc.utobj().getFutureDateUSFormat(10));

				fc.utobj().sendKeys(driver, financePage.txtPaymentPlanAmount, dsFinance.get("ppAmount"));
				fc.utobj().sendKeys(driver, financePage.txtPNAmount, dsFinance.get("promissoryNoteAmount"));

				fc.utobj().sendKeys(driver, financePage.txtPlanFinalPaymentDated, fc.utobj().getFutureDateUSFormat(10));
				fc.utobj().sendKeys(driver, financePage.txtPromissoryFinalPaymentDated, fc.utobj().getFutureDateUSFormat(10));

				fc.utobj().sendKeys(driver, financePage.drpTitle, dsFinance.get("title"));
				fc.utobj().sendKeys(driver, financePage.txtFirstName, dsFinance.get("firstName"));
				fc.utobj().sendKeys(driver, financePage.txtLastName, dsFinance.get("lastName"));
				fc.utobj().sendKeys(driver, financePage.txtStreetAddress, dsFinance.get("streetAddress"));
				fc.utobj().sendKeys(driver, financePage.txtCity, dsFinance.get("city"));
				fc.utobj().sendKeys(driver, financePage.drpCountry, dsFinance.get("country"));
				fc.utobj().sleep();
				fc.utobj().sendKeys(driver, financePage.txtZipCode, dsFinance.get("zip"));
				fc.utobj().sendKeys(driver, financePage.drpState, dsFinance.get("state"));
				fc.utobj().sendKeys(driver, financePage.txtPhoneNumbers, dsFinance.get("phone"));
				fc.utobj().sendKeys(driver, financePage.txtExtension, dsFinance.get("phoneExtension"));
				fc.utobj().sendKeys(driver, financePage.txtFaxNumbers, dsFinance.get("fax"));
				fc.utobj().sendKeys(driver, financePage.txtMobileNumbers, dsFinance.get("mobile"));
				fc.utobj().sendKeys(driver, financePage.txtEmailIds, dsFinance.get("email"));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, financePage.btnAdd);
				if (fc.utobj().assertLinkText(driver, "Modify")) {
					Reporter.log("Finance details has been added successfully");
				} else {
					fc.utobj().throwsException("Finance details couldn't be added . Test couldn't continue !!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in adding finance details , please refer screenshot!");
		}
	}

	public void addInsuranceDetails(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("*********************** Add Insurance Details ****************************** ");

		String testCaseId = "TC_InFoMgr_CorporateLocations_Add_Insurance_Details";

		Map<String, String> dsInsurance = fc.utobj().readTestData("infomgr", testCaseId);

		InfoMgrInsurancePage insurancePage = new InfoMgrCorporateLocationsPage(driver).getInsurancePage();

		String policyTitle = fc.utobj().generateTestData(dsInsurance.get("policyTitle"));

		if (fc.utobj().validate(testCaseId) == true) {
			try {

				fc.utobj().sendKeys(driver, insurancePage.txtPolicyTitle, policyTitle);
				fc.utobj().sendKeys(driver, insurancePage.txtInsuranceEffectiveDate, fc.utobj().getFutureDateUSFormat(10));
				fc.utobj().sendKeys(driver, insurancePage.txtInsuranceCompanyName, dsInsurance.get("company"));
				fc.utobj().sendKeys(driver, insurancePage.txtRating, dsInsurance.get("bestRating"));
				fc.utobj().sendKeys(driver, insurancePage.txtPolicyNo, dsInsurance.get("policyNumber"));

				fc.utobj().sendKeys(driver, insurancePage.txtInsuranceExpDate, fc.utobj().getFutureDateUSFormat(10));
				fc.utobj().clickRadioButton(driver, insurancePage.rdoWCPresent, dsInsurance.get("wcPresent"));
				fc.utobj().sendKeys(driver, insurancePage.txtVehicleRequired, dsInsurance.get("numberofVehicleRequired"));
				fc.utobj().clickRadioButton(driver, insurancePage.rdoVehiclePresent, dsInsurance.get("vehiclePresent"));
				fc.utobj().sendKeys(driver, insurancePage.txtGLRequired, dsInsurance.get("amountofGLRequired"));
				fc.utobj().clickRadioButton(driver, insurancePage.rdoGLPresent, dsInsurance.get("glPresent"));
				fc.utobj().sendKeys(driver, insurancePage.txtWCCoverageRequired, dsInsurance.get("wcCoverageRequired"));
				fc.utobj().sendKeys(driver, insurancePage.txtWCCoveragePresent, dsInsurance.get("wcCoveragePresent"));
				fc.utobj().sendKeys(driver, insurancePage.txtGLCoverageRequired, dsInsurance.get("glCoverageRequired"));
				fc.utobj().sendKeys(driver, insurancePage.txtGLCoveragePresent, dsInsurance.get("gmCoveragePresent"));
				fc.utobj().sendKeys(driver, insurancePage.txtGLEachOccurrence, dsInsurance.get("glEachOccurrence"));
				fc.utobj().sendKeys(driver, insurancePage.txtAgreegate, dsInsurance.get("aggregate"));
				fc.utobj().sendKeys(driver, insurancePage.txtPropertyCoverageRequired, dsInsurance.get("propertyCoverageRequired"));
				fc.utobj().sendKeys(driver, insurancePage.txtPropertyCoveragePresent, dsInsurance.get("propertyCoveragePresent"));
				fc.utobj().sendKeys(driver, insurancePage.txPropertyDeductible, dsInsurance.get("deductible"));
				fc.utobj().sendKeys(driver, insurancePage.txtVehicleCoverageRequired, dsInsurance.get("vehicleCoverageRequired"));
				fc.utobj().sendKeys(driver, insurancePage.txtVehicleCoveragePresent, dsInsurance.get("vehicleCoveragePresent"));
				fc.utobj().sendKeys(driver, insurancePage.txtOtherCoverageRequired, dsInsurance.get("other-1CoverageRequired"));
				fc.utobj().sendKeys(driver, insurancePage.txtOtherCoveragePresent, dsInsurance.get("other-1CoveragePresent"));
				fc.utobj().sendKeys(driver, insurancePage.txtOtherPolicyDescription, dsInsurance.get("policyDescription"));
				fc.utobj().sendKeys(driver, insurancePage.txtOtherComments, dsInsurance.get("other-1Comments"));
				fc.utobj().sendKeys(driver, insurancePage.txtOther2CoverageRequired, dsInsurance.get("other-2CoverageRequired"));
				fc.utobj().sendKeys(driver, insurancePage.txtOther2CoveragePresent, dsInsurance.get("other2-CoveragePresent"));
				fc.utobj().sendKeys(driver, insurancePage.txtOther2Comments, dsInsurance.get("other-2Comments"));
				fc.utobj().sendKeys(driver, insurancePage.txtTotalCoverage, dsInsurance.get("totalCoverage"));
				fc.utobj().sendKeys(driver, insurancePage.txtComments, dsInsurance.get("insuranceComments"));
				fc.utobj().sendKeys(driver, insurancePage.txtDocumentTitle, dsInsurance.get("documentTitle"));
				fc.utobj().sendKeys(driver, insurancePage.txtDocumentAttachment, dsInsurance.get("documentAttachment"));

				// Contact Details
				fc.utobj().selectDropDownByVisibleText(driver, insurancePage.drpTitle, dsInsurance.get("title"));
				fc.utobj().sendKeys(driver, insurancePage.txtFirstName, dsInsurance.get("firstName"));
				fc.utobj().sendKeys(driver, insurancePage.txtLastName, dsInsurance.get("lastName"));
				fc.utobj().sendKeys(driver, insurancePage.txtStreetAddress, dsInsurance.get("streetAddress"));
				fc.utobj().sendKeys(driver, insurancePage.txtCity, dsInsurance.get("city"));
				fc.utobj().selectDropDownByVisibleText(driver, insurancePage.drpCountry, dsInsurance.get("country"));
				fc.utobj().sleep();
				fc.utobj().sendKeys(driver, insurancePage.txtZipCode, dsInsurance.get("zip"));
				fc.utobj().selectDropDownByVisibleText(driver, insurancePage.drpState, dsInsurance.get("state"));
				fc.utobj().sendKeys(driver, insurancePage.txtPhoneNumbers, dsInsurance.get("phone"));
				fc.utobj().sendKeys(driver, insurancePage.txtExtension, dsInsurance.get("phoneExtension"));
				fc.utobj().sendKeys(driver, insurancePage.txtFaxNumbers, dsInsurance.get("fax"));
				fc.utobj().sendKeys(driver, insurancePage.txtMobileNumbers, dsInsurance.get("mobile"));
				fc.utobj().sendKeys(driver, insurancePage.txtEmailIds, dsInsurance.get("email"));

				// Agency details
				fc.utobj().sendKeys(driver, insurancePage.txtAgencyName, dsInsurance.get("agencyName"));
				fc.utobj().sendKeys(driver, insurancePage.txtAgencyStreetAddress, dsInsurance.get("agencyAddress"));
				fc.utobj().sendKeys(driver, insurancePage.txtAgencyCity, dsInsurance.get("agencyCity"));
				fc.utobj().selectDropDownByVisibleText(driver, insurancePage.drpCountry, dsInsurance.get("agencyCountry"));
				fc.utobj().sleep();
				fc.utobj().sendKeys(driver, insurancePage.txtAgencyZipCode, dsInsurance.get("agencyZip"));
				fc.utobj().selectDropDownByVisibleText(driver, insurancePage.drpAgencyState, dsInsurance.get("agencyState"));
				fc.utobj().sendKeys(driver, insurancePage.txtAgencyPhoneNumbers, dsInsurance.get("agencyPhone"));
				fc.utobj().sendKeys(driver, insurancePage.txtAgencyExtension, dsInsurance.get("agencyPhoneExtension"));
				fc.utobj().sendKeys(driver, insurancePage.txtAgencyFaxNumbers, dsInsurance.get("agencyFax"));
				fc.utobj().sendKeys(driver, insurancePage.txtAgencyMobileNumbers, dsInsurance.get("agencyMobile"));
				fc.utobj().sendKeys(driver, insurancePage.txtAgencyFaxNumbers, dsInsurance.get("agencyFax"));
				fc.utobj().sendKeys(driver, insurancePage.txtAgencyMobileNumbers, dsInsurance.get("agencyMobile"));
				fc.utobj().sendKeys(driver, insurancePage.txtAgencyEmailIds, dsInsurance.get("agencyEmail"));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, insurancePage.btnAdd);
				if (fc.utobj().assertPageSource(driver, policyTitle)) {
					Reporter.log("Insurance details has been added successfully");
				} else {
					fc.utobj().throwsException("Insurance details couldn't be added . Test couldn't continue !!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in adding insurance details , please refer screenshot!");
		}
	}

	public String CreateVisitForm(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("*********************** Create Visit Form ****************************** ");

		String testCaseId = "TC_60_Create_Visit_At_Visit";
		String visitFormName = "";
		FieldOpsVisitsPage pobj = new FieldOpsVisitsPage(driver);

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				Map<String, String> dataSet = fc.utobj().readTestData("infomgr", testCaseId);
				AdminFieldOpsManageVisitFormPageTest visitFormPage = new AdminFieldOpsManageVisitFormPageTest();
				visitFormName = visitFormPage.addVisitFormPrivate(driver, dataSet);

				// Add Question
				fc.utobj().clickElement(driver, pobj.addQuestion);

				AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest customizeVisit = new AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest();
				customizeVisit.addQuestion(driver, dataSet);

				AdminFieldOpsQuestionLibraryPageTest questionLibrary = new AdminFieldOpsQuestionLibraryPageTest();
				fc.utobj().clickElement(driver, pobj.addQuestion);
				questionLibrary.addQuestionRatingType(driver, dataSet);

				fc.utobj().clickElement(driver, pobj.addQuestion);
				questionLibrary.addQuestionSingleSelectDropDownType(driver, dataSet);

				fc.utobj().clickElement(driver, pobj.addQuestion);
				questionLibrary.addQuestionSingleSelectRadioButtonType(driver, dataSet);

				fc.utobj().clickElement(driver, pobj.finishBtn);
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in creating visit form , please refer screenshot!");
		}
		return visitFormName;
	}

	public void addVisit(WebDriver driver, Map<String, String> config, String franchiseID, String corporateUser) throws Exception {
		// Create Visit Form
		String visitForm = CreateVisitForm(driver, config);

		Reporter.log("*********************** Add Visit ****************************** ");

		String testCaseId = "TC_InfoMgr__CorporateLocations_Create_Visit";
		Map<String, String> dsVisit = fc.utobj().readTestData("infomgr", testCaseId);

		FieldOpsVisitsPage visitPage = new FieldOpsVisitsPage(driver);
		InfoMgrQAHistoryPage qaHistoryPage = new InfoMgrCorporateLocationsPage(driver).getQAHistoryPage();
		String comments = fc.utobj().generateTestData(dsVisit.get("comments"));
		searchFranchiseAndClick(driver, franchiseID);
		fc.utobj().clickElement(driver, qaHistoryPage.lnkQAHistory);
		fc.utobj().clickElement(driver, qaHistoryPage.lnkCreateVisit);

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				selectValFromRadioDrodown(driver, visitPage.drpVisitFormMainDiv, visitForm);
				selectValFromRadioDrodown(driver, visitPage.consultantSearchBoxDiv, corporateUser);
				fc.utobj().sendKeys(driver, visitPage.scheduleDateTime, fc.utobj().getFutureDateUSFormat(10));
				fc.utobj().sendKeys(driver, visitPage.comments, comments);

				JavascriptExecutor executor = (JavascriptExecutor) driver;
				executor.executeScript("arguments[0].click();", visitPage.schedule);
				;

				boolean isStatusPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[.='" + franchiseID + "']/ancestor::tr/td[contains(text () ,'Scheduled')]");
				if (isStatusPresent == false) {
					fc.utobj().throwsException("Was not able to create Visit");
				}

				// Verify that Visit is displayed on the QA History Page
				searchFranchiseAndClick(driver, franchiseID);
				fc.utobj().clickElement(driver, qaHistoryPage.lnkQAHistory);

				isStatusPresent = fc.utobj().assertPageSource(driver, corporateUser);
				if (isStatusPresent) {
					Reporter.log("Created visit is displayed on the QA history page");
				} else {
					fc.utobj().throwsException("Created visit is not displayed on the QA History page");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in adding visit, please refer screenshot!");
		}

	}

	// *************************************** Common methods
	// ************************************************

	public void selectValFromRadioDrodown(WebDriver driver, WebElement element, String value) throws Exception {
		try {
			fc.utobj().sleep(500);
			fc.utobj().clickElement(driver, element);
			fc.utobj().sleep(500);

			WebElement searchTextBox = element.findElement(By.xpath(".//input[@type='text']"));
			fc.utobj().sendKeys(driver, searchTextBox, value);
			fc.utobj().sleep(500);

			WebElement elementRadio = element.findElement(By.xpath(".//label[contains(text(),'" + value + "')]/input"));
			try {

				if (!elementRadio.isSelected()) {
					fc.utobj().clickElement(driver, elementRadio);
				}

			} catch (Exception e) {
				fc.utobj().throwsException("Search Value not present in Drop Down Box");
			}
		} catch (Exception e) {
			fc.utobj().throwsException("Problem in Drop Down Box");
		}
	}

	public void inDevelopmentPageShowAll(WebDriver driver) throws Exception {
		fc.infomgr().infomgr_common().InfoMgrInDevelopment(driver);
		if (fc.utobj().assertLinkText(driver, "Show All")) {
			fc.utobj().clickLink(driver, "Show All");
		}
	}

	public void checkLocation(WebDriver driver, String location) throws Exception {
		String xpath = ".//*[@id='siteMainTable']//a[text()='" + location + "']/preceding::input[@type='checkbox'][1]";
		WebElement checkBox = fc.utobj().getElementByXpath(driver, xpath);
		fc.utobj().clickElement(driver, checkBox);
	}

	public void VerifyPrintPreview(WebDriver driver, String textTobeVerified) throws Exception {
		Set<String> windows = new HashSet<String>();
		windows = driver.getWindowHandles();

		boolean isTextDisplayed = false;

		String currentWindow = driver.getWindowHandle();
		for (String window : windows) {
			if (!window.equals(currentWindow)) {

				driver.switchTo().window(window);
				if (driver.getCurrentUrl().contains("printcheck")) {
					isTextDisplayed = fc.utobj().assertPageSource(driver, textTobeVerified);
				}
			}
		}
		driver.close();

		if (isTextDisplayed) {
			Reporter.log(textTobeVerified + "  is displayed on the print preview page. !!! ");
		} else {
			fc.utobj().throwsException(textTobeVerified + "  is not displayed on the print preview page. !!! ");
		}

		driver.switchTo().window(currentWindow);
	}

	private void SelectToEmail(WebDriver driver, String corporateUser) throws Exception {
		fc.commonMethods().switch_cboxIframe_frameId(driver);
		fc.utobj().sleep();

		InfoMgrAddressBook addressBook = new InfoMgrAddressBook(driver);
		fc.utobj().sleep();
		fc.utobj().sendKeys(driver, addressBook.txtCorpUsers, corporateUser);
		fc.utobj().sleep();
		fc.utobj().clickElement(driver, addressBook.btnSearchCorporateUser);
		fc.utobj().sleep();
		fc.utobj().clickElement(driver, addressBook.chkCorporateUserName);
		fc.utobj().clickElement(driver, addressBook.btnAdd);

	}

	// *********************************************************** Modify Tabs
	// Data ******************************************

	public void modifyCenterInfo(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("************************** Modifying Center Info ******************* ");

		String testCaseId = "TC_InFoMgr_CorporateLocations_CenterInfo_Modify";

		Map<String, String> dsCenterInfo = fc.utobj().readTestData("infomgr", testCaseId);

		// fc.utobj().clickLink(driver, "Modify");
		InfoMgrCenterInfoPage centerInfoPage = new InfoMgrCorporateLocationsPage(driver).getCenterInfoPage();

		String centerName = fc.utobj().generateTestData(dsCenterInfo.get("centerName"));

		if (fc.utobj().validate(testCaseId) == true) {
			try {

				fc.utobj().sendKeys(driver, centerInfoPage.txtCenterName, centerName);
				fc.utobj().sendKeys(driver, centerInfoPage.txtLicenseNo, dsCenterInfo.get("licenseNumber"));
				fc.utobj().sendKeys(driver, centerInfoPage.txtReportPeriodStartDate, fc.utobj().getFutureDateUSFormat(10));
				fc.utobj().sendKeys(driver, centerInfoPage.txtStoreOpeningDate, fc.utobj().getFutureDateUSFormat(10));
				fc.utobj().sendKeys(driver, centerInfoPage.txtStreetAddress, dsCenterInfo.get("streetAddress"));
				fc.utobj().sendKeys(driver, centerInfoPage.txtAddress2, dsCenterInfo.get("address2"));
				fc.utobj().sendKeys(driver, centerInfoPage.txtCity, dsCenterInfo.get("city"));
				fc.utobj().selectDropDownByVisibleText(driver, centerInfoPage.drpCountry, dsCenterInfo.get("country"));
				fc.utobj().sleep();
				fc.utobj().sendKeys(driver, centerInfoPage.txtZipCode, dsCenterInfo.get("zip"));
				fc.utobj().selectDropDownByVisibleText(driver, centerInfoPage.drpState, dsCenterInfo.get("state"));
				fc.utobj().sendKeys(driver, centerInfoPage.txtPhone, dsCenterInfo.get("phone"));
				fc.utobj().sendKeys(driver, centerInfoPage.txtExtension, dsCenterInfo.get("phoneExtension"));
				fc.utobj().sendKeys(driver, centerInfoPage.txtStoreFax, dsCenterInfo.get("fax"));
				fc.utobj().sendKeys(driver, centerInfoPage.txtStoreMobile, dsCenterInfo.get("mobile"));
				fc.utobj().sendKeys(driver, centerInfoPage.txtStoreEmail, dsCenterInfo.get("email"));
				fc.utobj().selectDropDownByVisibleText(driver, centerInfoPage.drpContactSalutation, dsCenterInfo.get("salutation"));
				fc.utobj().sendKeys(driver, centerInfoPage.txtContactFirstName, dsCenterInfo.get("contactFirstName"));
				fc.utobj().sendKeys(driver, centerInfoPage.txtContactLastName, dsCenterInfo.get("contactLastName"));
				fc.utobj().sendKeys(driver, centerInfoPage.txtContactPhone, dsCenterInfo.get("contactPhone"));
				fc.utobj().sendKeys(driver, centerInfoPage.txtContactExtension, dsCenterInfo.get("contactPhoneExtension"));
				fc.utobj().sendKeys(driver, centerInfoPage.txtContactEmail, dsCenterInfo.get("contactEmail"));
				fc.utobj().sendKeys(driver, centerInfoPage.txtContactFax, dsCenterInfo.get("contactFax"));
				fc.utobj().sendKeys(driver, centerInfoPage.txtContactMobile, dsCenterInfo.get("contactMobile"));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, centerInfoPage.btnSumbit);

				if (fc.utobj().assertPageSource(driver, centerName)) {
					Reporter.log("Center Info data have been updated successfully !!!");
				} else {
					fc.utobj().throwsException("Center Info Data couldn't be updated !!! ");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in modifying center info , please refer screenshot!");
		}
	}

	public WebDriver modifyAddresses(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("********************** Modify Franchisee Addresses ****************** \n");

		String testCaseId = "TC_InFoMgr_CorporateLocations_Addressess_Modify";

		Map<String, String> dsAddresses = fc.utobj().readTestData("infomgr", testCaseId);

		InfoMgrAddressesPage addressPage = new InfoMgrCorporateLocationsPage(driver).getAddressPage();
		String state = "";

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				// Switch to frame
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				Thread.sleep(2000);

				fc.utobj().selectDropDownByPartialText(driver, addressPage.drpCenterInfoState, dsAddresses.get("state"));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, addressPage.btnSave);

				// After saving the address , verify that address has been saved
				fc.utobj().switchFrameToDefault(driver);
				fc.utobj().clickElement(driver, addressPage.lnkAddresses);
				fc.utobj().sleep();

				// Switch to frame
				fc.commonMethods().switch_cboxIframe_frameId(driver);

				state = fc.utobj().getSelectedOptioninDropDown(driver, addressPage.drpCenterInfoState);

				fc.utobj().clickElement(driver, addressPage.btnClose);

				if (state.equalsIgnoreCase(dsAddresses.get("state"))) {
					Reporter.log("Address has been modified successfully !!!");
				} else {
					fc.utobj().throwsException("Address couldn't be modified properly. Test failes !!! ");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in modifying address , please refer screenshot!");
		}
		driver = fc.utobj().switchFrameToDefault(driver);
		return driver;

	}

	public WebDriver modifyAgreement(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("********************** modify Agreement ****************** \n");

		String testCaseId = "TC_InFoMgr_CorporateLocations_Agreement_Modify";

		Map<String, String> dsAgreement = fc.utobj().readTestData("infomgr", testCaseId);

		InfoMgrAgreementPage infoMgrAgreementPage = new InfoMgrCorporateLocationsPage(driver).getAgreementPage();

		fc.utobj().clickLink(driver, "Modify");

		String openDateVal = fc.utobj().getText(driver, infoMgrAgreementPage.lblOpeningDate);
		SimpleDateFormat sdfDate = new SimpleDateFormat("MM/dd/yyyy");
		Date closingDate = new Date(openDateVal);
		closingDate = DateUtils.addDays(closingDate, 20);
		String closingDateStr = sdfDate.format(closingDate);

		String comments = fc.utobj().generateTestData(dsAgreement.get("comments"));

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtApprovedDate, fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtDateExecuted, fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtEffectiveDate, fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtExpirationDate, closingDateStr);
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtClosingDate, closingDateStr);

				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtStateAddendum, dsAgreement.get("stateAddendum"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtOtherAddendum, dsAgreement.get("otherAddendum"));
				fc.utobj().clickRadioButton(driver, infoMgrAgreementPage.radioRightsOfFirstRefusal, dsAgreement.get("rightsofFirstRefusal"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtProtectedTerritory, dsAgreement.get("protectedTerritory"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtSalesperson, dsAgreement.get("salesPerson"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtPreviousLicenseNumber, dsAgreement.get("previousLicenseNumber"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRelatedCenter, dsAgreement.get("relatedCenter"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRequiredOpeningDate, fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtStoreSoldDate, fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtComments, comments);
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtInitialTerm, dsAgreement.get("initialTerm"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRenewalTermFirst, dsAgreement.get("termFirst"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRenewalDueDateFirst, fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRenewalFeeFirst, dsAgreement.get("feeFirst"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRenewalFeePaidFirstDate, fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRenewalTermSecond, dsAgreement.get("termSecond"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRenewalDueDateSecond, fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRenewalFeeSecond, dsAgreement.get("feeSecond"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRenewalFeePaidSecondDate, fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRenewalTermThird, dsAgreement.get("termThird"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRenewalDueDateThird, fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRenewalFeeThird, dsAgreement.get("feeThird"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRenewalFeePaidThirdDate, fc.utobj().getFutureDateUSFormat(-10));
				// Contact Details
				fc.utobj().selectDropDownByVisibleText(driver, infoMgrAgreementPage.drpContactTitle, dsAgreement.get("title"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtFirstName, dsAgreement.get("firstName"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtLastName, dsAgreement.get("lastName"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtStreetAddress, dsAgreement.get("streetAddress"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtCity, dsAgreement.get("city"));
				fc.utobj().selectDropDownByVisibleText(driver, infoMgrAgreementPage.drpCountry, dsAgreement.get("country"));
				fc.utobj().sleep();
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtZipcode, dsAgreement.get("zip"));
				fc.utobj().selectDropDownByVisibleText(driver, infoMgrAgreementPage.drpState, dsAgreement.get("state"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtPhoneNumbers, dsAgreement.get("phone"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtExtn, dsAgreement.get("phoneExtension"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtFaxNumbers, dsAgreement.get("fax"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtMobileNumbers, dsAgreement.get("mobile"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtMailIds, dsAgreement.get("email"));

				// Financial Notes
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtDescription1, dsAgreement.get("description1"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtAmount1, dsAgreement.get("amount1"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtAmountTerm1, dsAgreement.get("amountTerm1"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtInterest1, dsAgreement.get("interest1"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtComment1, dsAgreement.get("comments1"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtDescription2, dsAgreement.get("description2"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtAmount2, dsAgreement.get("amount2"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtAmountTerm2, dsAgreement.get("amountTerm2"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtInterest2, dsAgreement.get("interest2"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtComment2, dsAgreement.get("comments2"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtDescription3, dsAgreement.get("description3"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtAmount3, dsAgreement.get("amount3"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtAmountTerm3, dsAgreement.get("amountTerm3"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtInterest3, dsAgreement.get("interest3"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtComment3, dsAgreement.get("comments3"));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, infoMgrAgreementPage.btnsubmit);

				if (fc.utobj().assertPageSource(driver, comments)) {
					Reporter.log("Agreement has been modified successfully");
				} else {
					fc.utobj().throwsException("Agreement couldn't be modified . Test couldn't continue !!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in modifying agreement , please refer screenshot!");
		}
		return driver;
	}

	public String modifyATask(WebDriver driver, Map<String, String> config, boolean isFromContactHistoryPage) throws Exception {
		Reporter.log("********************** Modifying a Task ****************** \n");

		String testCaseId = "TC_InFoMgr_CorporateLocations_LogATask_Modify";

		fc.commonMethods().switch_cboxIframe_frameId(driver);

		Map<String, String> dsLogATask = fc.utobj().readTestData("infomgr", testCaseId);

		InfoMgrLogATaskPage logATaskPage = new InfoMgrCorporateLocationsPage(driver).getLogATaskPage();
		String subject = fc.utobj().generateTestData(dsLogATask.get("subject"));
		if (fc.utobj().validate(testCaseId) == true) {
			try {

				fc.utobj().selectDropDownByPartialText(driver, logATaskPage.drpStatus, dsLogATask.get("status"));
				fc.utobj().selectDropDownByVisibleText(driver, logATaskPage.drpTaskType, dsLogATask.get("taskType"));
				fc.utobj().sendKeys(driver, logATaskPage.txtSubject, subject);
				fc.utobj().sendKeys(driver, logATaskPage.txtStartDate, fc.utobj().getFutureDateUSFormat(5));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, logATaskPage.btnAdd);
				fc.utobj().switchFrameToDefault(driver);

				if (isFromContactHistoryPage) {
					boolean isSubjectOnPage = fc.utobj().assertPageSource(driver, subject);
					if (isSubjectOnPage) {
						Reporter.log("Task has been modified successfully");
					} else {
						fc.utobj().throwsException("Task modification failed. Test case fails ");
					}
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in modifying a task , please refer screenshot!");
		}
		return subject;
	}

	public String modifyACall(WebDriver driver, Map<String, String> config, boolean isFromContactHistoryPage) throws Exception {
		Reporter.log("********************** Modifying a Call ****************** \n");

		String testCaseId = "TC_InFoMgr_CorporateLocations_LogACall_Modify";

		Map<String, String> dsLogACall = fc.utobj().readTestData("infomgr", testCaseId);
		InfoMgrLogACallPage logACallPage = new InfoMgrCorporateLocationsPage(driver).getLogACallPage();
		fc.commonMethods().switch_cboxIframe_frameId(driver);
		String subject = fc.utobj().generateTestData(dsLogACall.get("subject"));

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().sendKeys(driver, logACallPage.txtSubject, subject);
				fc.utobj().sendKeys(driver, logACallPage.txtDate, fc.utobj().getFutureDateUSFormat(5));
				fc.utobj().selectDropDownByPartialText(driver, logACallPage.drpStatus, dsLogACall.get("status"));
				fc.utobj().selectDropDownByPartialText(driver, logACallPage.txtTimeHour, dsLogACall.get("timeHour"));
				fc.utobj().selectDropDownByPartialText(driver, logACallPage.txtTimeMinute, dsLogACall.get("timeMin"));
				fc.utobj().selectDropDownByPartialText(driver, logACallPage.txtAMPM, dsLogACall.get("timeAMPM"));

				fc.utobj().selectDropDownByPartialText(driver, logACallPage.drpCallType, dsLogACall.get("typeofCall"));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, logACallPage.btnSubmit);
				fc.utobj().sleep();
				fc.utobj().clickElement(driver, logACallPage.btnNo);
				fc.utobj().sleep();
				fc.utobj().switchFrameToDefault(driver);
				if (isFromContactHistoryPage) {
					if (fc.utobj().assertLinkText(driver, subject)) {
						Reporter.log("Log a call set up has been modified successfully");
					} else {
						fc.utobj().throwsException("Log a Call modification has been failed.");
					}
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in modifying a call , please refer screenshot!");
		}
		return subject;
	}

	public String modifyRemark(WebDriver driver, Map<String, String> config, Object remarksObj) throws Exception {
		Reporter.log("******************* Adding Remarks ********************** \n");

		String testCaseId = "TC_InFoMgr_CorporateLocations_modify_Remarks";

		fc.commonMethods().switch_cboxIframe_frameId(driver);
		String remark = fc.utobj().generateTestData("Test Remarks modified");

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				if (remarksObj instanceof InfoMgrContactHistoryPage) {
					InfoMgrContactHistoryPage contactHistory = (InfoMgrContactHistoryPage) remarksObj;
					fc.utobj().sendKeys(driver, contactHistory.txtRemarks, remark);
					fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																					// for
																					// Custom
																					// Manadatory
																					// Fields
																					// Of
																					// Client
					fc.utobj().clickElement(driver, contactHistory.btnSubmit);
					fc.utobj().clickElement(driver, contactHistory.btnClose);
				} else if (remarksObj instanceof InfoMgrOwnersPage) {
					InfoMgrOwnersPage ownersPage = (InfoMgrOwnersPage) remarksObj;
					fc.utobj().sendKeys(driver, ownersPage.txtRemarks, remark);
					fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																					// for
																					// Custom
																					// Manadatory
																					// Fields
																					// Of
																					// Client
					fc.utobj().clickElement(driver, ownersPage.btnSubmit);
					fc.utobj().clickElement(driver, ownersPage.btnClose);
				}
				Thread.sleep(2000);
				boolean isRemarkOnPage = fc.utobj().assertPageSource(driver, remark);
				if (isRemarkOnPage) {
					Reporter.log("Remarks have been modified successfully !!! ");
				} else {
					fc.utobj().throwsException("Some problem occured while modifying remarks !!!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in modifying remarks , please refer screenshot!");
		}
		return remark;
	}

	public void modifyContractSigning(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("**************************** Modify Contract Signing Info  ***********");

		String testCaseId = "TC_InfoMgr_CorporateLocations_ContractSigning_Modify";

		fc.utobj().clickLink(driver, "Modify");
		InfoMgrContractSigningPage contractSigning = new InfoMgrCorporateLocationsPage(driver).getContractSigningPage();
		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().sendKeys(driver, contractSigning.txtContractReceivedSignedDate, fc.utobj().getFutureDateUSFormat(10));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, contractSigning.btnSubmit);
				if (fc.utobj().assertPageSource(driver, "Signing Commissions") && fc.utobj().assertPageSource(driver, "Ancillary Documents")) {
					Reporter.log("Contract Signing data have been modified successfully !!! ");
				} else {
					fc.utobj().throwsException("Contract Signing data couldn't be modified. Test failes !!! ");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in modifying Contract Signing Information  , please refer screenshot!");
		}

	}

	public void modifyCustomerComplaints(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("****************************** Modifying Customer Complaints ********************** \n");

		String testCaseId = "TC_InfoMgr_CorporateLocations_Modify_Customer_Complaints";

		Map<String, String> dsCustomerComplaints = fc.utobj().readTestData("infomgr", testCaseId);

		InfoMgrCustomerComplaintsPage customerComplaintsPage = new InfoMgrCorporateLocationsPage(driver).getCustomerComplaintsPage();
		fc.utobj().clickLink(driver, "Modify");
		String complaintID = fc.utobj().generateTestData(dsCustomerComplaints.get("complaintID"));

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().sendKeys(driver, customerComplaintsPage.txtComplaintID, complaintID);
				fc.utobj().sendKeys(driver, customerComplaintsPage.txtComplaintDate, fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, customerComplaintsPage.txtComplaint, dsCustomerComplaints.get("complaint"));
				fc.utobj().selectDropDownByVisibleText(driver, customerComplaintsPage.drpComplaintType, dsCustomerComplaints.get("type"));
				fc.utobj().sendKeys(driver, customerComplaintsPage.txtIncidentDate, fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, customerComplaintsPage.txtReceivedVia, dsCustomerComplaints.get("receivedVia"));
				fc.utobj().sendKeys(driver, customerComplaintsPage.txtSummary, dsCustomerComplaints.get("summaryofIncident"));
				fc.utobj().sendKeys(driver, customerComplaintsPage.txtActionTaken, dsCustomerComplaints.get("actionTaken"));
				fc.utobj().sendKeys(driver, customerComplaintsPage.txtDocumentTitle, dsCustomerComplaints.get("documentTitle"));
				fc.utobj().sendKeys(driver, customerComplaintsPage.txtDocumentAttachment, dsCustomerComplaints.get("document"));
				fc.utobj().sendKeys(driver, customerComplaintsPage.txtComplaintBy, dsCustomerComplaints.get("complainantName"));
				fc.utobj().sendKeys(driver, customerComplaintsPage.txtStreetAddress, dsCustomerComplaints.get("streetAddress"));
				fc.utobj().sendKeys(driver, customerComplaintsPage.txtCity, dsCustomerComplaints.get("city"));
				fc.utobj().selectDropDownByPartialText(driver, customerComplaintsPage.drpCountry, dsCustomerComplaints.get("country"));
				fc.utobj().sendKeys(driver, customerComplaintsPage.txtZipCode, dsCustomerComplaints.get("zip"));
				fc.utobj().selectDropDownByVisibleText(driver, customerComplaintsPage.drpState, dsCustomerComplaints.get("state"));
				fc.utobj().sendKeys(driver, customerComplaintsPage.txtEmailIds, dsCustomerComplaints.get("email"));
				fc.utobj().sendKeys(driver, customerComplaintsPage.txtPhoneNumbers, dsCustomerComplaints.get("phone"));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, customerComplaintsPage.btnSubmit);

				boolean isComplaintIDPresent = fc.utobj().assertPageSource(driver, complaintID);

				if (isComplaintIDPresent) {
					Reporter.log("Complaint modified successfully !!!");
				} else {
					fc.utobj().throwsException("Error in modifying customer complaints !!! ");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			}

			catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in modifying Customer Complaints , please refer screenshot!");
		}
	}

	public String modifyDocument(WebDriver driver, Map<String, String> config) throws Exception {
		String testCaseId = "TC_InfoMgr_CorporateLocations_Document_Modify";
		InfoMgrDocumentsPage documentsPage = new InfoMgrCorporateLocationsPage(driver).getDocumentsPage();

		Map<String, String> dsDocuments = fc.utobj().readTestData("infomgr", testCaseId);

		String documentTitle = fc.utobj().generateTestData(dsDocuments.get("documentTitle"));

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().sendKeys(driver, documentsPage.txtDocumentTitle, documentTitle);
				fc.utobj().sendKeys(driver, documentsPage.txtDocumentAttachment, dsDocuments.get("documentAttachment"));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, documentsPage.btnAdd);

				boolean isDocAdded = fc.utobj().assertPageSource(driver, documentTitle);
				if (isDocAdded) {
					Reporter.log("Document added successfully");
				} else {
					fc.utobj().throwsException("Document modification failed. !!! ");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in modifying documents , please refer screenshot!");
		}
		return documentTitle;

	}

	public String modifyEmployee(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("*********************** Modify Employess ****************************** ");

		String testCaseId = "TC_InFoMgr_CorporateLocations_Modify_Employee";

		Map<String, String> dsEmployee = fc.utobj().readTestData("infomgr", testCaseId);

		InfoMgrEmployeesPage employeesPage = new InfoMgrCorporateLocationsPage(driver).getEmployeesPage();
		try {
			fc.utobj().clickLink(driver, "Modify");
		} catch (Exception e) {
			fc.utobj().actionImgOption(driver, dsEmployee.get("firstName"), "Modify");
		}

		String employeeName = fc.utobj().generateTestData(dsEmployee.get("firstName"));

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().selectDropDownByVisibleText(driver, employeesPage.drpSalutation, dsEmployee.get("salutation"));
				fc.utobj().sendKeys(driver, employeesPage.txtFirstName, employeeName);
				fc.utobj().sendKeys(driver, employeesPage.txtLastName, dsEmployee.get("lastName"));
				fc.utobj().sendKeys(driver, employeesPage.txtAddress, dsEmployee.get("address"));
				fc.utobj().sendKeys(driver, employeesPage.txtCity, dsEmployee.get("city"));
				fc.utobj().selectDropDownByVisibleText(driver, employeesPage.drpCountry, dsEmployee.get("country"));
				fc.utobj().sendKeys(driver, employeesPage.txtZipcode, dsEmployee.get("zip"));
				fc.utobj().selectDropDownByVisibleText(driver, employeesPage.drpState, dsEmployee.get("state"));
				fc.utobj().sendKeys(driver, employeesPage.txtPhone1, dsEmployee.get("phone1"));
				fc.utobj().sendKeys(driver, employeesPage.txtPhone2, dsEmployee.get("phone2"));
				fc.utobj().sendKeys(driver, employeesPage.txtFax, dsEmployee.get("fax"));
				fc.utobj().sendKeys(driver, employeesPage.txtMobile, dsEmployee.get("mobile"));
				fc.utobj().sendKeys(driver, employeesPage.txtEmail, dsEmployee.get("email"));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, employeesPage.btnAdd);

				// Verify that the employee have been modified

				if (fc.utobj().assertPageSource(driver, employeeName)) {
					Reporter.log("Employees have been modified successfully");
				} else {
					fc.utobj().throwsException("Employee couldn't be modified . Test couldn't continue !!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in modifying Employee , please refer screenshot!");
		}

		return employeeName;

	}

	public String modifyEntityDetails(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("*********************** Modify Entity Details ****************************** ");

		String testCaseId = "TC_InFoMgr_CorporateLocations_Modify_Entity_Details";

		Map<String, String> dsEntityDetails = fc.utobj().readTestData("infomgr", testCaseId);

		InfoMgrEntityDetailsPage entityDetailsPage = new InfoMgrCorporateLocationsPage(driver).getEntityDetailsPage();
		fc.utobj().clickLink(driver, "Modify");
		String entityName = fc.utobj().generateTestData(dsEntityDetails.get("entityName"));

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().sendKeys(driver, entityDetailsPage.txtEntityName, entityName);
				fc.utobj().sendKeys(driver, entityDetailsPage.txtAddress, dsEntityDetails.get("address"));
				fc.utobj().sendKeys(driver, entityDetailsPage.txtCity, dsEntityDetails.get("city"));
				fc.utobj().selectDropDownByVisibleText(driver, entityDetailsPage.drpCountry, dsEntityDetails.get("country"));
				fc.utobj().sendKeys(driver, entityDetailsPage.txtZipCode, dsEntityDetails.get("zip"));
				fc.utobj().selectDropDownByVisibleText(driver, entityDetailsPage.drpState, dsEntityDetails.get("state"));
				fc.utobj().sendKeys(driver, entityDetailsPage.txtPhoneNumber, dsEntityDetails.get("phone"));
				fc.utobj().sendKeys(driver, entityDetailsPage.txtExtension, dsEntityDetails.get("phoneExt"));
				fc.utobj().sendKeys(driver, entityDetailsPage.txtFaxNumber, dsEntityDetails.get("fax"));
				fc.utobj().sendKeys(driver, entityDetailsPage.txtMobileNumber, dsEntityDetails.get("mobile"));
				fc.utobj().sendKeys(driver, entityDetailsPage.txtEmailIds, dsEntityDetails.get("email"));
				fc.utobj().selectDropDownByVisibleText(driver, entityDetailsPage.drpCountryFormation, dsEntityDetails.get("countryofFormation"));
				fc.utobj().sleep();
				fc.utobj().sendKeys(driver, entityDetailsPage.txtDateFormation, fc.utobj().getFutureDateUSFormat(-5));
				fc.utobj().selectDropDownByVisibleText(driver, entityDetailsPage.drpStateFormation, dsEntityDetails.get("stateofFormation"));
				fc.utobj().selectDropDownByVisibleText(driver, entityDetailsPage.drpEntityType, dsEntityDetails.get("entityType"));
				fc.utobj().sendKeys(driver, entityDetailsPage.txtFormationDocumentTitle, dsEntityDetails.get("formationDocTitle"));
				fc.utobj().sendKeys(driver, entityDetailsPage.txtFormationDocumentAttachment, dsEntityDetails.get("formationDocAttachment"));
				fc.utobj().sendKeys(driver, entityDetailsPage.txtGoverningDocumentTitle, dsEntityDetails.get("govereningDocumentTitle"));
				fc.utobj().sendKeys(driver, entityDetailsPage.txtGoverningDocumentAttachment, dsEntityDetails.get("govreeningDocAttachment"));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, entityDetailsPage.btnAdd);
				if (fc.utobj().assertPageSource(driver, entityName)) {
					Reporter.log("Entity details have been modified successfully");
				} else {
					fc.utobj().throwsException("Entity detail couldn't be modified . Test couldn't continue !!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in modifying entity details , please refer screenshot!");
		}

		return entityName;

	}

	public String modifyEventDetails(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("*********************** Modify Event Details ****************************** ");

		String testCaseId = "TC_InFoMgr_CorporateLocations_Modify_Event_Details";
		Map<String, String> dsEventDetails = fc.utobj().readTestData("infomgr", testCaseId);

		InfoMgrEventsPage eventsPage = new InfoMgrCorporateLocationsPage(driver).getEventsPage();
		fc.utobj().clickLink(driver, "Modify");
		String eventSummary = fc.utobj().generateTestData(dsEventDetails.get("summary"));
		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().sendKeys(driver, eventsPage.txtDate, fc.utobj().getFutureDateUSFormat(10));
				fc.utobj().sendKeys(driver, eventsPage.txtOrganizer, dsEventDetails.get("organizer"));
				fc.utobj().sendKeys(driver, eventsPage.txtType, dsEventDetails.get("type"));
				fc.utobj().sendKeys(driver, eventsPage.txtSummary, eventSummary);
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, eventsPage.btnAdd);
				if (fc.utobj().assertPageSource(driver, eventSummary)) {
					Reporter.log("Event details have been modified successfully");
				} else {
					fc.utobj().throwsException("Event detail couldn't be modified . Test couldn't continue !!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in modifying event details , please refer screenshot!");
		}

		return eventSummary;
	}

	public void modifyFinanceDetails(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("*********************** Modify Finance Details ****************************** ");

		String testCaseId = "TC_InFoMgr_CorporateLocations_Modify_Finance_Details";
		Map<String, String> dsFinance = fc.utobj().readTestData("infomgr", testCaseId);

		InfoMgrFinancialsPage financePage = new InfoMgrCorporateLocationsPage(driver).getFinancialsPage();
		fc.utobj().clickLink(driver, "Modify");

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().clickRadioButton(driver, financePage.rdoFinStatOnFile, dsFinance.get("financialStatementsonFile"));
				fc.utobj().clickRadioButton(driver, financePage.rdoFinStatApproved, dsFinance.get("financeApproved"));
				fc.utobj().sendKeys(driver, financePage.txtCashRevenue, dsFinance.get("initialFeePaidCash"));
				fc.utobj().sendKeys(driver, financePage.txtFranchiseeFee, dsFinance.get("initialFranchiseFeeTotal"));
				fc.utobj().sendKeys(driver, financePage.txtNoteRevenue, dsFinance.get("initialFeePaidNote"));

				fc.utobj().sendKeys(driver, financePage.txtPNPreparedDate, fc.utobj().getFutureDateUSFormat(10));
				fc.utobj().sendKeys(driver, financePage.txtPNDated, fc.utobj().getFutureDateUSFormat(10));
				fc.utobj().sendKeys(driver, financePage.txtPaymentPlanDated, fc.utobj().getFutureDateUSFormat(10));

				fc.utobj().sendKeys(driver, financePage.txtPaymentPlanAmount, dsFinance.get("ppAmount"));
				fc.utobj().sendKeys(driver, financePage.txtPNAmount, dsFinance.get("promissoryNoteAmount"));

				fc.utobj().sendKeys(driver, financePage.txtPlanFinalPaymentDated, fc.utobj().getFutureDateUSFormat(10));
				fc.utobj().sendKeys(driver, financePage.txtPromissoryFinalPaymentDated, fc.utobj().getFutureDateUSFormat(10));

				fc.utobj().sendKeys(driver, financePage.drpTitle, dsFinance.get("title"));
				fc.utobj().sendKeys(driver, financePage.txtFirstName, dsFinance.get("firstName"));
				fc.utobj().sendKeys(driver, financePage.txtLastName, dsFinance.get("lastName"));
				fc.utobj().sendKeys(driver, financePage.txtStreetAddress, dsFinance.get("streetAddress"));
				fc.utobj().sendKeys(driver, financePage.txtCity, dsFinance.get("city"));
				fc.utobj().sendKeys(driver, financePage.drpCountry, dsFinance.get("country"));
				fc.utobj().sleep();
				fc.utobj().sendKeys(driver, financePage.txtZipCode, dsFinance.get("zip"));
				fc.utobj().sendKeys(driver, financePage.drpState, dsFinance.get("state"));
				fc.utobj().sendKeys(driver, financePage.txtPhoneNumbers, dsFinance.get("phone"));
				fc.utobj().sendKeys(driver, financePage.txtExtension, dsFinance.get("phoneExtension"));
				fc.utobj().sendKeys(driver, financePage.txtFaxNumbers, dsFinance.get("fax"));
				fc.utobj().sendKeys(driver, financePage.txtMobileNumbers, dsFinance.get("mobile"));
				fc.utobj().sendKeys(driver, financePage.txtEmailIds, dsFinance.get("email"));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, financePage.btnAdd);
				if (fc.utobj().assertLinkText(driver, "Modify")) {
					Reporter.log("Finance details has been modified successfully");
				} else {
					fc.utobj().throwsException("Finance details couldn't be modified . Test couldn't continue !!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in modifying finance details , please refer screenshot!");
		}

	}

	public String modifyGuarantor(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("*********************** Modify Gurantor Details ****************************** ");

		String testCaseId = "TC_InFoMgr_CorporateLocations_Modify_Gurantor_Details";
		Map<String, String> dsGuarantor = fc.utobj().readTestData("infomgr", testCaseId);

		InfoMgrGuarantorsPage guarantorPage = new InfoMgrCorporateLocationsPage(driver).getGuarantorsPage();
		fc.utobj().clickLink(driver, "Modify");
		String guarantorName = fc.utobj().generateTestData(dsGuarantor.get("firstName"));

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().selectDropDownByVisibleText(driver, guarantorPage.drpTitle, dsGuarantor.get("title"));
				fc.utobj().sendKeys(driver, guarantorPage.txtFirstName, guarantorName);
				fc.utobj().sendKeys(driver, guarantorPage.txtLastName, dsGuarantor.get("lastName"));
				fc.utobj().selectDropDownByVisibleText(driver, guarantorPage.drpEntityType, dsGuarantor.get("entityType"));
				fc.utobj().selectDropDownByVisibleText(driver, guarantorPage.drpResidencyState, dsGuarantor.get("stateOfFormation"));
				fc.utobj().sendKeys(driver, guarantorPage.txtTaxPayer, dsGuarantor.get("taxPayerID"));
				fc.utobj().sendKeys(driver, guarantorPage.txtPercentage, dsGuarantor.get("percentage"));
				fc.utobj().sendKeys(driver, guarantorPage.txtStreetAddress, dsGuarantor.get("address"));
				fc.utobj().sendKeys(driver, guarantorPage.txtCity, dsGuarantor.get("city"));
				fc.utobj().selectDropDownByVisibleText(driver, guarantorPage.drpCountry, dsGuarantor.get("country"));
				fc.utobj().sendKeys(driver, guarantorPage.txtZipCode, dsGuarantor.get("zip"));
				fc.utobj().selectDropDownByVisibleText(driver, guarantorPage.drpState, dsGuarantor.get("state"));
				fc.utobj().sendKeys(driver, guarantorPage.txtPhoneNumbers, dsGuarantor.get("phone"));
				fc.utobj().sendKeys(driver, guarantorPage.txtExtension, dsGuarantor.get("phoneExt"));
				fc.utobj().sendKeys(driver, guarantorPage.txtFaxNumbers, dsGuarantor.get("fax"));
				fc.utobj().sendKeys(driver, guarantorPage.txtMobileNumbers, dsGuarantor.get("mobile"));
				fc.utobj().sendKeys(driver, guarantorPage.txtEmailIds, dsGuarantor.get("email"));
				fc.utobj().sendKeys(driver, guarantorPage.txtComments, dsGuarantor.get("comments"));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, guarantorPage.btnAdd);
				if (fc.utobj().assertPageSource(driver, guarantorName)) {
					Reporter.log("guarantor details have been modified successfully");
				} else {
					fc.utobj().throwsException("guarantor detail couldn't be modified . Test couldn't continue !!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in modifying guarantor details , please refer screenshot!");
		}
		return guarantorName;
	}

	public void modifyInsuranceDetails(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("*********************** Modify Insurance Details ****************************** ");

		String testCaseId = "TC_InFoMgr_CorporateLocations_Modify_Insurance_Details";
		Map<String, String> dsInsurance = fc.utobj().readTestData("infomgr", testCaseId);

		InfoMgrInsurancePage insurancePage = new InfoMgrCorporateLocationsPage(driver).getInsurancePage();
		fc.utobj().clickLink(driver, "Modify");
		String policyTitle = fc.utobj().generateTestData(dsInsurance.get("policyTitle"));

		if (fc.utobj().validate(testCaseId) == true) {
			try {

				fc.utobj().sendKeys(driver, insurancePage.txtPolicyTitle, policyTitle);
				fc.utobj().sendKeys(driver, insurancePage.txtInsuranceEffectiveDate, fc.utobj().getFutureDateUSFormat(10));
				fc.utobj().sendKeys(driver, insurancePage.txtInsuranceCompanyName, dsInsurance.get("company"));
				fc.utobj().sendKeys(driver, insurancePage.txtRating, dsInsurance.get("bestRating"));
				fc.utobj().sendKeys(driver, insurancePage.txtPolicyNo, dsInsurance.get("policyNumber"));

				fc.utobj().sendKeys(driver, insurancePage.txtInsuranceExpDate, fc.utobj().getFutureDateUSFormat(10));
				fc.utobj().clickRadioButton(driver, insurancePage.rdoWCPresent, dsInsurance.get("wcPresent"));
				fc.utobj().sendKeys(driver, insurancePage.txtVehicleRequired, dsInsurance.get("numberofVehicleRequired"));
				fc.utobj().clickRadioButton(driver, insurancePage.rdoVehiclePresent, dsInsurance.get("vehiclePresent"));
				fc.utobj().sendKeys(driver, insurancePage.txtGLRequired, dsInsurance.get("amountofGLRequired"));
				fc.utobj().clickRadioButton(driver, insurancePage.rdoGLPresent, dsInsurance.get("glPresent"));
				fc.utobj().sendKeys(driver, insurancePage.txtWCCoverageRequired, dsInsurance.get("wcCoverageRequired"));
				fc.utobj().sendKeys(driver, insurancePage.txtWCCoveragePresent, dsInsurance.get("wcCoveragePresent"));
				fc.utobj().sendKeys(driver, insurancePage.txtGLCoverageRequired, dsInsurance.get("glCoverageRequired"));
				fc.utobj().sendKeys(driver, insurancePage.txtGLCoveragePresent, dsInsurance.get("gmCoveragePresent"));
				fc.utobj().sendKeys(driver, insurancePage.txtGLEachOccurrence, dsInsurance.get("glEachOccurrence"));
				fc.utobj().sendKeys(driver, insurancePage.txtAgreegate, dsInsurance.get("aggregate"));
				fc.utobj().sendKeys(driver, insurancePage.txtPropertyCoverageRequired, dsInsurance.get("propertyCoverageRequired"));
				fc.utobj().sendKeys(driver, insurancePage.txtPropertyCoveragePresent, dsInsurance.get("propertyCoveragePresent"));
				fc.utobj().sendKeys(driver, insurancePage.txPropertyDeductible, dsInsurance.get("deductible"));
				fc.utobj().sendKeys(driver, insurancePage.txtVehicleCoverageRequired, dsInsurance.get("vehicleCoverageRequired"));
				fc.utobj().sendKeys(driver, insurancePage.txtVehicleCoveragePresent, dsInsurance.get("vehicleCoveragePresent"));
				fc.utobj().sendKeys(driver, insurancePage.txtOtherCoverageRequired, dsInsurance.get("other-1CoverageRequired"));
				fc.utobj().sendKeys(driver, insurancePage.txtOtherCoveragePresent, dsInsurance.get("other-1CoveragePresent"));
				fc.utobj().sendKeys(driver, insurancePage.txtOtherPolicyDescription, dsInsurance.get("policyDescription"));
				fc.utobj().sendKeys(driver, insurancePage.txtOtherComments, dsInsurance.get("other-1Comments"));
				fc.utobj().sendKeys(driver, insurancePage.txtOther2CoverageRequired, dsInsurance.get("other-2CoverageRequired"));
				fc.utobj().sendKeys(driver, insurancePage.txtOther2CoveragePresent, dsInsurance.get("other2-CoveragePresent"));
				fc.utobj().sendKeys(driver, insurancePage.txtOther2Comments, dsInsurance.get("other-2Comments"));
				fc.utobj().sendKeys(driver, insurancePage.txtTotalCoverage, dsInsurance.get("totalCoverage"));
				fc.utobj().sendKeys(driver, insurancePage.txtComments, dsInsurance.get("insuranceComments"));
				fc.utobj().sendKeys(driver, insurancePage.txtDocumentTitle, dsInsurance.get("documentTitle"));
				fc.utobj().sendKeys(driver, insurancePage.txtDocumentAttachment, dsInsurance.get("documentAttachment"));

				// Contact Details
				fc.utobj().selectDropDownByVisibleText(driver, insurancePage.drpTitle, dsInsurance.get("title"));
				fc.utobj().sendKeys(driver, insurancePage.txtFirstName, dsInsurance.get("firstName"));
				fc.utobj().sendKeys(driver, insurancePage.txtLastName, dsInsurance.get("lastName"));
				fc.utobj().sendKeys(driver, insurancePage.txtStreetAddress, dsInsurance.get("streetAddress"));
				fc.utobj().sendKeys(driver, insurancePage.txtCity, dsInsurance.get("city"));
				fc.utobj().selectDropDownByVisibleText(driver, insurancePage.drpCountry, dsInsurance.get("country"));
				fc.utobj().sleep();
				fc.utobj().sendKeys(driver, insurancePage.txtZipCode, dsInsurance.get("zip"));
				fc.utobj().selectDropDownByVisibleText(driver, insurancePage.drpState, dsInsurance.get("state"));
				fc.utobj().sendKeys(driver, insurancePage.txtPhoneNumbers, dsInsurance.get("phone"));
				fc.utobj().sendKeys(driver, insurancePage.txtExtension, dsInsurance.get("phoneExtension"));
				fc.utobj().sendKeys(driver, insurancePage.txtFaxNumbers, dsInsurance.get("fax"));
				fc.utobj().sendKeys(driver, insurancePage.txtMobileNumbers, dsInsurance.get("mobile"));
				fc.utobj().sendKeys(driver, insurancePage.txtEmailIds, dsInsurance.get("email"));

				// Agency details
				fc.utobj().sendKeys(driver, insurancePage.txtAgencyName, dsInsurance.get("agencyName"));
				fc.utobj().sendKeys(driver, insurancePage.txtAgencyStreetAddress, dsInsurance.get("agencyAddress"));
				fc.utobj().sendKeys(driver, insurancePage.txtAgencyCity, dsInsurance.get("agencyCity"));
				fc.utobj().selectDropDownByVisibleText(driver, insurancePage.drpCountry, dsInsurance.get("agencyCountry"));
				fc.utobj().sleep();
				fc.utobj().sendKeys(driver, insurancePage.txtAgencyZipCode, dsInsurance.get("agencyZip"));
				fc.utobj().selectDropDownByVisibleText(driver, insurancePage.drpAgencyState, dsInsurance.get("agencyState"));
				fc.utobj().sendKeys(driver, insurancePage.txtAgencyPhoneNumbers, dsInsurance.get("agencyPhone"));
				fc.utobj().sendKeys(driver, insurancePage.txtAgencyExtension, dsInsurance.get("agencyPhoneExtension"));
				fc.utobj().sendKeys(driver, insurancePage.txtAgencyFaxNumbers, dsInsurance.get("agencyFax"));
				fc.utobj().sendKeys(driver, insurancePage.txtAgencyMobileNumbers, dsInsurance.get("agencyMobile"));
				fc.utobj().sendKeys(driver, insurancePage.txtAgencyFaxNumbers, dsInsurance.get("agencyFax"));
				fc.utobj().sendKeys(driver, insurancePage.txtAgencyMobileNumbers, dsInsurance.get("agencyMobile"));
				fc.utobj().sendKeys(driver, insurancePage.txtAgencyEmailIds, dsInsurance.get("agencyEmail"));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, insurancePage.btnAdd);
				if (fc.utobj().assertPageSource(driver, policyTitle)) {
					Reporter.log("Insurance details has been modified successfully");
				} else {
					fc.utobj().throwsException("Insurance details couldn't be added . Test couldn't continue !!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in modifying insurance details , please refer screenshot!");
		}
	}

	public String modifyLegalViolation(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("*********************** Modify Legal Violation Details ****************************** ");

		String testCaseId = "TC_InFoMgr_CorporateLocations_Modify_Legal_Violation_Details";

		Map<String, String> dsLegalViolation = fc.utobj().readTestData("infomgr", testCaseId);

		InfoMgrLegalViolationPage legalViolationPage = new InfoMgrCorporateLocationsPage(driver).getLegalViolationPage();
		fc.utobj().clickLink(driver, "Modify");
		String legalViolationNumber = fc.utobj().generateRandomNumber();
		String subject = fc.utobj().generateTestData(dsLegalViolation.get("summary"));

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().sendKeys(driver, legalViolationPage.txtNumber, legalViolationNumber);
				fc.utobj().sendKeys(driver, legalViolationPage.txtDate, fc.utobj().getFutureDateUSFormat(10));
				fc.utobj().selectDropDownByVisibleText(driver, legalViolationPage.drpType, dsLegalViolation.get("type"));
				fc.utobj().selectDropDownByVisibleText(driver, legalViolationPage.drpStatus, dsLegalViolation.get("status"));
				fc.utobj().sendKeys(driver, legalViolationPage.txtCureDate, fc.utobj().getFutureDateUSFormat(10));
				fc.utobj().sendKeys(driver, legalViolationPage.txtDateCured, fc.utobj().getFutureDateUSFormat(15));
				fc.utobj().sendKeys(driver, legalViolationPage.txtSummary, subject);
				fc.utobj().sendKeys(driver, legalViolationPage.txtActionTaken, dsLegalViolation.get("actionTaken"));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, legalViolationPage.btnAdd);
				if (fc.utobj().assertPageSource(driver, subject)) {
					Reporter.log("Legal Violation details have been modified successfully");
				} else {
					fc.utobj().throwsException("Legal violation detail couldn't be modified . Test couldn't continue !!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in modifying legal violation details , please refer screenshot!");
		}

		return subject;

	}

	public String modifyLenders(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("*********************** Modify Lenders Details ****************************** ");

		String testCaseId = "TC_InFoMgr_CorporateLocations_Modify_Lenders_Details";
		Map<String, String> dsLenders = fc.utobj().readTestData("infomgr", testCaseId);

		InfoMgrLendersPage lendersPage = new InfoMgrCorporateLocationsPage(driver).getLendersPage();
		fc.utobj().clickLink(driver, "Modify");
		String lendersFirstName = fc.utobj().generateTestData(dsLenders.get("firstName"));

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().selectDropDownByVisibleText(driver, lendersPage.drpTitle, dsLenders.get("title"));
				fc.utobj().sendKeys(driver, lendersPage.txtFirstName, lendersFirstName);
				fc.utobj().sendKeys(driver, lendersPage.txtLastName, dsLenders.get("lastName"));
				fc.utobj().sendKeys(driver, lendersPage.txtStreetaddress, dsLenders.get("streetAddress"));
				fc.utobj().sendKeys(driver, lendersPage.txtCity, dsLenders.get("city"));
				fc.utobj().selectDropDownByVisibleText(driver, lendersPage.drpCountry, dsLenders.get("country"));
				fc.utobj().sendKeys(driver, lendersPage.txtZipcCode, dsLenders.get("zip"));
				fc.utobj().selectDropDownByVisibleText(driver, lendersPage.drpstate, dsLenders.get("state"));
				fc.utobj().sendKeys(driver, lendersPage.txtCollateralAssignmentExpirationDate, fc.utobj().getFutureDateUSFormat(10));
				fc.utobj().sendKeys(driver, lendersPage.txtComfortLetterDate, fc.utobj().getFutureDateUSFormat(10));
				fc.utobj().clickRadioButton(driver, lendersPage.rdoComfortAgreement, dsLenders.get("comfortAgreement"));
				fc.utobj().sendKeys(driver, lendersPage.txtDateComfortAgreementInfo, fc.utobj().getFutureDateUSFormat(10));
				fc.utobj().sendKeys(driver, lendersPage.txtDocumentTitle, dsLenders.get("formationDocumentTitle"));
				fc.utobj().sendKeys(driver, lendersPage.txtDocumentAttachment, dsLenders.get("documentAttachment"));
				fc.utobj().sendKeys(driver, lendersPage.txtLenderContactOne, dsLenders.get("contact1Name"));
				fc.utobj().sendKeys(driver, lendersPage.txtLenderContactTwo, dsLenders.get("contact2Name"));
				fc.utobj().sendKeys(driver, lendersPage.txtContact1Phone, dsLenders.get("contact1Phone"));
				fc.utobj().sendKeys(driver, lendersPage.txtContact2Phone, dsLenders.get("contact2Phone"));
				fc.utobj().sendKeys(driver, lendersPage.txtContact1PhoneExtn, dsLenders.get("contact1PhoneExtension"));
				fc.utobj().sendKeys(driver, lendersPage.txtContact2PhoneExtn, dsLenders.get("contact2PhoneExtension"));
				fc.utobj().sendKeys(driver, lendersPage.txtContact1Fax, dsLenders.get("contact1Fax"));
				fc.utobj().sendKeys(driver, lendersPage.txtContact2Fax, dsLenders.get("contact2Fax"));
				fc.utobj().sendKeys(driver, lendersPage.txtContact1Email, dsLenders.get("contact1Email"));
				fc.utobj().sendKeys(driver, lendersPage.txtContact2Email, dsLenders.get("contact2Email"));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, lendersPage.btnAdd);
				if (fc.utobj().assertPageSource(driver, lendersFirstName)) {
					Reporter.log("Lenders details have been modified successfully");
				} else {
					fc.utobj().throwsException("Lenders detail couldn't be modified . Test couldn't continue !!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in modifying lenders details , please refer screenshot!");
		}

		return lendersFirstName;

	}

	public String modifyMarketingDetails(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("*********************** Modify Marketing Details ****************************** ");

		String testCaseId = "TC_InFoMgr_CorporateLocations_Modify_Marketing_Details";
		Map<String, String> dsMarketing = fc.utobj().readTestData("infomgr", testCaseId);

		InfoMgrMarketingPage marketingPage = new InfoMgrCorporateLocationsPage(driver).getMarketingPage();
		fc.utobj().clickLink(driver, "Modify");
		String marketingContactFirstName = fc.utobj().generateTestData(dsMarketing.get("firstName"));

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().selectDropDownByVisibleText(driver, marketingPage.drpTitle, dsMarketing.get("title"));
				fc.utobj().sendKeys(driver, marketingPage.txtFirstName, marketingContactFirstName);
				fc.utobj().sendKeys(driver, marketingPage.txtLastName, dsMarketing.get("lastName"));
				fc.utobj().sendKeys(driver, marketingPage.txtStreetAddress, dsMarketing.get("streetAddress"));
				fc.utobj().sendKeys(driver, marketingPage.txtCity, dsMarketing.get("city"));
				fc.utobj().selectDropDownByVisibleText(driver, marketingPage.drpCountry, dsMarketing.get("country"));
				fc.utobj().sendKeys(driver, marketingPage.txtZipCode, dsMarketing.get("zip"));
				fc.utobj().selectDropDownByVisibleText(driver, marketingPage.drpState, dsMarketing.get("state"));
				fc.utobj().sendKeys(driver, marketingPage.txtPhoneNumbers, dsMarketing.get("phone"));
				fc.utobj().sendKeys(driver, marketingPage.txtExtension, dsMarketing.get("phoneExtension"));
				fc.utobj().sendKeys(driver, marketingPage.txtFaxNumbers, dsMarketing.get("fax"));
				fc.utobj().sendKeys(driver, marketingPage.txtMobileNumbers, dsMarketing.get("mobile"));
				fc.utobj().sendKeys(driver, marketingPage.txtEmailIds, dsMarketing.get("email"));
				fc.utobj().clickRadioButton(driver, marketingPage.rdoGrandOpeningRequired, dsMarketing.get("grandOpeningRequired"));
				fc.utobj().sendKeys(driver, marketingPage.txtGrandOpeningCompletedDate, fc.utobj().getFutureDateUSFormat(10));

				fc.utobj().clickRadioButton(driver, marketingPage.rdoCouponRedemption, dsMarketing.get("couponRedemption"));

				fc.utobj().sendKeys(driver, marketingPage.txtCampaignName, dsMarketing.get("campaignName"));

				fc.utobj().clickRadioButton(driver, marketingPage.rdoCampaignParticipation, dsMarketing.get("campaignParticipation"));

				fc.utobj().sendKeys(driver, marketingPage.txtProgramName, dsMarketing.get("programName"));

				fc.utobj().clickRadioButton(driver, marketingPage.rdoProgramParticipation, dsMarketing.get("programParticipation"));

				fc.utobj().sendKeys(driver, marketingPage.txtDMA, dsMarketing.get("DMA"));
				fc.utobj().sendKeys(driver, marketingPage.txtComments, dsMarketing.get("comments"));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, marketingPage.btnSubmit);

				if (fc.utobj().assertPageSource(driver, marketingContactFirstName)) {
					Reporter.log("Marketing details have been modified successfully");
				} else {
					fc.utobj().throwsException("Marketing detail couldn't be modified . Test couldn't continue !!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in modifying marketing details , please refer screenshot!");
		}

		return marketingContactFirstName;
	}

	public String modifyMystryReview(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("*********************** Modify Mystry Review ****************************** ");

		String testCaseId = "TC_InFoMgr_CorporateLocations_Modify_Mystry_Review_Details";
		Map<String, String> dsMystryReview = fc.utobj().readTestData("infomgr", testCaseId);

		InfoMgrMystryReviewPage mystryReviewPage = new InfoMgrCorporateLocationsPage(driver).getMystryReviewPage();
		fc.utobj().clickLink(driver, "Modify");
		String mystryReviewComments = fc.utobj().generateTestData(dsMystryReview.get("comments"));

		if (fc.utobj().validate(testCaseId) == true) {
			try {

				fc.utobj().sendKeys(driver, mystryReviewPage.txtInspectionDate, fc.utobj().getFutureDateUSFormat(-5));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtReportSentToFranchiseeDate, fc.utobj().getFutureDateUSFormat(-2));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtGeneralComments, mystryReviewComments);

				// Performance section
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValuePerformanceQ1, dsMystryReview.get("perfQuestion1"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValuePerformanceQ2, dsMystryReview.get("perfQuestion2"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValuePerformanceQ3, dsMystryReview.get("perfQuestion3"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValuePerformanceQ4, dsMystryReview.get("perfQuestion4"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValuePerformanceQ5, dsMystryReview.get("perfQuestion5"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValuePerformanceQ6, dsMystryReview.get("perfQuestion6"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValuePerformanceQ7, dsMystryReview.get("perfQuestion7"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValuePerformanceQ8, dsMystryReview.get("perfQuestion8"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtCommentsForPerformance, dsMystryReview.get("perfComments"));

				// Service Section
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValueServiceQ1, dsMystryReview.get("serviceQuestion1"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValueServiceQ2, dsMystryReview.get("serviceQuestion2"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValueServiceQ3, dsMystryReview.get("serviceQuestion3"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValueServiceQ4, dsMystryReview.get("serviceQuestion4"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValueServiceQ5, dsMystryReview.get("serviceQuestion5"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValueServiceQ6, dsMystryReview.get("serviceQuestion6"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValueServiceQ7, dsMystryReview.get("serviceQuestion7"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValueServiceQ8, dsMystryReview.get("serviceQuestion8"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValueServiceQ9, dsMystryReview.get("serviceQuestion9"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValueServiceQ10, dsMystryReview.get("serviceQuestion10"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValueServiceQ11, dsMystryReview.get("serviceQuestion11"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValueServiceQ12, dsMystryReview.get("serviceQuestion12"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtCommentsForService, dsMystryReview.get("serviceComments"));

				// Appearance section
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValueAppearanceQ1, dsMystryReview.get("appearanceQuestion1"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValueAppearanceQ2, dsMystryReview.get("appearanceQuestion2"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValueAppearanceQ3, dsMystryReview.get("appearanceQuestion3"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValueAppearanceQ4, dsMystryReview.get("appearanceQuestion4"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValueAppearanceQ5, dsMystryReview.get("appearanceQuestion5"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtCommentsForAppearance, dsMystryReview.get("appearanceComments"));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, mystryReviewPage.btnSubmit);

				if (fc.utobj().assertPageSource(driver, mystryReviewComments)) {
					Reporter.log("Mystry Review details have been modified successfully");
				} else {
					fc.utobj().throwsException("Mystry review detail couldn't be modified . Test couldn't continue !!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in modifying mystry review details , please refer screenshot!");
		}

		return mystryReviewComments;

	}

	public String modifyPicture(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("*********************** Modify Picture ****************************** ");

		String testCaseId = "TC_InFoMgr_CorporateLocations_Modify_Picture";
		Map<String, String> dsPicture = fc.utobj().readTestData("infomgr", testCaseId);

		InfoMgrPicturesPage picturePage = new InfoMgrCorporateLocationsPage(driver).getPicturesPage();
		String pictureTitle = fc.utobj().generateTestData(dsPicture.get("title"));

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().sendKeys(driver, picturePage.txtpictureTitle, pictureTitle);
				fc.utobj().sendKeys(driver, picturePage.txtpictureFilename, dsPicture.get("attachment"));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, picturePage.btnSubmit);
				if (fc.utobj().assertPageSource(driver, pictureTitle)) {
					Reporter.log("Picture has been modified successfully");
				} else {
					fc.utobj().throwsException("Picture couldn't be modified . Test couldn't continue !!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in adding picture , please refer screenshot!");
		}
		return pictureTitle;
	}

	public String modifyOwnerUser(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("*********************** Modify User ****************************** ");

		String testCaseId = "TC_InFoMgr_CorporateLocations_Modify_User";
		Map<String, String> dsUser = fc.utobj().readTestData("infomgr", testCaseId);

		InfoMgrUserPage userPage = new InfoMgrCorporateLocationsPage(driver).getUsersPage().getUserPage();
		;
		String loginID = fc.utobj().generateTestData(dsUser.get("loginID"));
		String phone = fc.utobj().generateRandomNumber();

		if (fc.utobj().validate(testCaseId) == true) {
			try {

				fc.utobj().sendKeys(driver, userPage.txtUserName, loginID);
				fc.utobj().selectDropDownByVisibleText(driver, userPage.drpType, dsUser.get("type"));
				fc.utobj().selectValFromMultiSelect(driver, userPage.drpRoles, dsUser.get("role"));
				fc.utobj().check(userPage.chkIsDaylight, dsUser.get("allowDST"));

				fc.utobj().sendKeys(driver, userPage.txtJobTitle, dsUser.get("jobTitle"));
				// fc.utobj().selectDropDownByVisibleText(driver,
				// userPage.drpUserLanguage, dsUser.get("language") ); // No
				// user to chnage language
				fc.utobj().sendKeys(driver, userPage.txtAddress, dsUser.get("streetAddress"));
				fc.utobj().sendKeys(driver, userPage.txtCity, dsUser.get("city"));
				fc.utobj().selectDropDownByVisibleText(driver, userPage.drpCountry, dsUser.get("country"));
				fc.utobj().sendKeys(driver, userPage.txtZipcode, dsUser.get("zip"));
				fc.utobj().selectDropDownByVisibleText(driver, userPage.drpState, dsUser.get("state"));
				fc.utobj().sendKeys(driver, userPage.txtPhone1, phone);
				fc.utobj().sendKeys(driver, userPage.txtPhoneExt1, dsUser.get("phone1Ext"));
				fc.utobj().sendKeys(driver, userPage.txtPhone2, dsUser.get("phone2"));
				fc.utobj().sendKeys(driver, userPage.txtPhoneExt2, dsUser.get("phone2Ext"));
				fc.utobj().sendKeys(driver, userPage.txtFax, dsUser.get("fax"));
				fc.utobj().sendKeys(driver, userPage.txtMobile, dsUser.get("mobile"));
				fc.utobj().sendKeys(driver, userPage.txtEmail, dsUser.get("email"));
				fc.utobj().sendKeys(driver, userPage.txtIpAddress, dsUser.get("ipAddress"));
				fc.utobj().check(userPage.chkIsBillable, dsUser.get("isBillable"));
				fc.utobj().selectDropDownByPartialText(driver, userPage.drpBirthMonth, dsUser.get("birthMonth"));
				fc.utobj().selectDropDownByPartialText(driver, userPage.drpBirthDate, dsUser.get("birthDate"));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, userPage.btnSubmit);
				if (fc.utobj().assertPageSource(driver, phone)) {
					Reporter.log("User has been modified successfully");
				} else {
					fc.utobj().throwsException("User couldn't be modified . Test couldn't continue !!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in modifying user , please refer screenshot!");
		}
		return loginID;
	}

	public void modifyRealState(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("*********************** Modify Real Estate ****************************** ");

		String testCaseId = "TC_InFoMgr_CorporateLocations_Modify_Real_Estate_Details";
		Map<String, String> dsRealState = fc.utobj().readTestData("infomgr", testCaseId);

		InfoMgrRealStatePage realStatePage = new InfoMgrCorporateLocationsPage(driver).getRealStatePage();
		fc.utobj().clickLink(driver, "Modify");
		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().sendKeys(driver, realStatePage.drpOwnedLeased, dsRealState.get("ownedorLeased"));
				fc.utobj().sendKeys(driver, realStatePage.txtSiteAddress1, dsRealState.get("siteStreet1"));
				fc.utobj().sendKeys(driver, realStatePage.txtSiteAddress2, dsRealState.get("siteStreet2"));
				fc.utobj().sendKeys(driver, realStatePage.txtSiteCity, dsRealState.get("siteCity"));
				fc.utobj().sendKeys(driver, realStatePage.drpCountry, dsRealState.get("country"));
				fc.utobj().sleep();
				fc.utobj().sendKeys(driver, realStatePage.drpState, dsRealState.get("state/Province"));
				fc.utobj().sendKeys(driver, realStatePage.txtSquareFootage, dsRealState.get("buildingSize"));
				fc.utobj().sendKeys(driver, realStatePage.txtBuildingDimentionsX, dsRealState.get("buildingLength"));
				fc.utobj().sendKeys(driver, realStatePage.txtBuildingDimentionsY, dsRealState.get("buildingBreadth"));
				fc.utobj().sendKeys(driver, realStatePage.txtBuildingDimentionsZ, dsRealState.get("buildingHeight"));
				fc.utobj().sendKeys(driver, realStatePage.txtParkingSpaces, dsRealState.get("parkingSpaces"));
				fc.utobj().sendKeys(driver, realStatePage.txtDealType, dsRealState.get("dealType"));
				fc.utobj().sendKeys(driver, realStatePage.drpPremisesType, dsRealState.get("typeofPremises"));
				fc.utobj().sendKeys(driver, realStatePage.txtLoiSent, fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, realStatePage.txtLeaseSignedDate, fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, realStatePage.txtApprovalDate, fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, realStatePage.txtLeaseCommencementDate, fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, realStatePage.txtExpirationDate, fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, realStatePage.txtRentIncreases, fc.utobj().getFutureDateUSFormat(100));

				fc.utobj().sendKeys(driver, realStatePage.txtMonthlyRent, dsRealState.get("currentMonthlyRent"));
				fc.utobj().clickRadioButton(driver, realStatePage.rdoAcal, dsRealState.get("ACAL"));
				fc.utobj().sendKeys(driver, realStatePage.txtDocumentTitle, dsRealState.get("documentTitle"));
				fc.utobj().sendKeys(driver, realStatePage.txtDocumentAttachment, dsRealState.get("document"));

				// Term First
				fc.utobj().sendKeys(driver, realStatePage.txtRenewalTermFirst, dsRealState.get("termFirst(Years)"));
				fc.utobj().sendKeys(driver, realStatePage.txtRenewalDueDateFirst, fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, realStatePage.txtRenewalFeeFirst, dsRealState.get("Fee-First"));
				fc.utobj().sendKeys(driver, realStatePage.txtRenewalFeePaidFirst, fc.utobj().getFutureDateUSFormat(-10));

				// Term Second
				fc.utobj().sendKeys(driver, realStatePage.txtRenewalTermSecond, dsRealState.get("termSecond"));
				fc.utobj().sendKeys(driver, realStatePage.txtRenewalDueDateSecond, fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, realStatePage.txtRenewalFeeSecond, dsRealState.get("feeSecond"));
				fc.utobj().sendKeys(driver, realStatePage.txtRenewalFeePaidSecond, fc.utobj().getFutureDateUSFormat(-10));

				// Term Third
				fc.utobj().sendKeys(driver, realStatePage.txtRenewalTermThird, dsRealState.get("termThird"));
				fc.utobj().sendKeys(driver, realStatePage.txtRenewalDueDateThird, fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, realStatePage.txtRenewalFeeThird, dsRealState.get("feeThird"));
				fc.utobj().sendKeys(driver, realStatePage.txtRenewalFeePaidThird, fc.utobj().getFutureDateUSFormat(-10));

				fc.utobj().sendKeys(driver, realStatePage.txtRenewalOptions, dsRealState.get("optionTerm(Years)"));
				fc.utobj().clickRadioButton(driver, realStatePage.rdoPurchaseOption, dsRealState.get("purchaseOption"));
				fc.utobj().sendKeys(driver, realStatePage.txtProjectedOpeningDate, fc.utobj().getFutureDateUSFormat(-10));

				fc.utobj().clickRadioButton(driver, realStatePage.rdoGeneralContractorSelector, dsRealState.get("generalContractorSelected"));
				if (dsRealState.get("generalContractorSelected").equalsIgnoreCase("yes")) {
					fc.utobj().sendKeys(driver, realStatePage.txtNameGeneralContractor, dsRealState.get("generalContractorName"));
					fc.utobj().sendKeys(driver, realStatePage.txtAddressGeneralContractor, dsRealState.get("generalContractorAddress"));
				}

				fc.utobj().sendKeys(driver, realStatePage.txtPermitApplied, fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, realStatePage.txtPermitIssued, fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, realStatePage.txtCertificate, fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, realStatePage.txtTurnOverDate, fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, realStatePage.txtGrandOpeningDate, fc.utobj().getFutureDateUSFormat(-10));

				// Lessor details
				fc.utobj().selectDropDownByVisibleText(driver, realStatePage.drpLessorTitle, dsRealState.get("lessor_title"));
				fc.utobj().sendKeys(driver, realStatePage.txtLessorFirstName, dsRealState.get("lessor_firstName"));
				fc.utobj().sendKeys(driver, realStatePage.txtLessorLastName, dsRealState.get("lessor_lastName"));
				fc.utobj().sendKeys(driver, realStatePage.txtLessorStreetAddress, dsRealState.get("lessor_streetAddress"));
				fc.utobj().sendKeys(driver, realStatePage.txtLessorCity, dsRealState.get("lessor_city"));
				fc.utobj().selectDropDownByVisibleText(driver, realStatePage.drpLessorCountry, dsRealState.get("lessor_country"));
				fc.utobj().sleep();
				fc.utobj().sendKeys(driver, realStatePage.txtLessorZipCode, dsRealState.get("lessor_zip"));
				fc.utobj().selectDropDownByVisibleText(driver, realStatePage.drpLessorState, dsRealState.get("lessor_state"));
				fc.utobj().sendKeys(driver, realStatePage.txtLessorPhoneNumbers, dsRealState.get("lessor_phone"));
				fc.utobj().sendKeys(driver, realStatePage.txtLessorExtension, dsRealState.get("lessor_phoneExtension"));
				fc.utobj().sendKeys(driver, realStatePage.txtLessorFaxNumbers, dsRealState.get("lessor_fax"));
				fc.utobj().sendKeys(driver, realStatePage.txtLessorMobileNumbers, dsRealState.get("lessor_mobile"));
				fc.utobj().sendKeys(driver, realStatePage.txtLessorEmailIds, dsRealState.get("lessor_email"));

				// Sub-lessor details
				fc.utobj().selectDropDownByVisibleText(driver, realStatePage.drpSubLessorTitle, dsRealState.get("sublessor_title"));
				fc.utobj().sendKeys(driver, realStatePage.txtSublessorFirstName, dsRealState.get("sublessor_firstName"));
				fc.utobj().sendKeys(driver, realStatePage.txtSublessorLastName, dsRealState.get("sublessor_lastName"));
				fc.utobj().sendKeys(driver, realStatePage.txtSubLessorStreetAddress, dsRealState.get("sublessor_streetAddress"));
				fc.utobj().sendKeys(driver, realStatePage.txtSubLessorCity, dsRealState.get("sublessor_city"));
				fc.utobj().selectDropDownByVisibleText(driver, realStatePage.drpSubLessorCountry, dsRealState.get("sublessor_country"));
				fc.utobj().sleep();
				fc.utobj().sendKeys(driver, realStatePage.txtSubLessorZipCode, dsRealState.get("sublessor_zip"));
				fc.utobj().selectDropDownByVisibleText(driver, realStatePage.drpSubLessorState, dsRealState.get("sublessor_state"));
				fc.utobj().sendKeys(driver, realStatePage.txtSubLessorPhoneNumbers, dsRealState.get("sublessor_phone"));
				fc.utobj().sendKeys(driver, realStatePage.txtSubLessorExtension, dsRealState.get("sublessor_phoneExtension"));
				fc.utobj().sendKeys(driver, realStatePage.txtSubLessorFaxNumbers, dsRealState.get("sublessor_fax"));
				fc.utobj().sendKeys(driver, realStatePage.txtSubLessorMobileNumbers, dsRealState.get("sublessor_mobile"));
				fc.utobj().sendKeys(driver, realStatePage.txtSubLessorEmailIds, dsRealState.get("sublessor_email"));

				// Tenant Details
				fc.utobj().selectDropDownByVisibleText(driver, realStatePage.drpTenantTitle, dsRealState.get("tenant_title"));
				fc.utobj().sendKeys(driver, realStatePage.txtTenantFirstName, dsRealState.get("tenant_firstName"));
				fc.utobj().sendKeys(driver, realStatePage.txtTenantLastName, dsRealState.get("tenant_lastName"));
				fc.utobj().sendKeys(driver, realStatePage.txtTenantStreetAddress, dsRealState.get("tenant_streetAddress"));
				fc.utobj().sendKeys(driver, realStatePage.txtTenantCity, dsRealState.get("tenant_city"));
				fc.utobj().selectDropDownByVisibleText(driver, realStatePage.drpTenantCountry, dsRealState.get("tenant_country"));
				fc.utobj().sleep();
				fc.utobj().sendKeys(driver, realStatePage.txtTenantZipCode, dsRealState.get("tenant_zip"));
				fc.utobj().selectDropDownByVisibleText(driver, realStatePage.drpTenantState, dsRealState.get("tenant_state"));
				fc.utobj().sendKeys(driver, realStatePage.txtTenantPhoneNumbers, dsRealState.get("tenant_phone"));
				fc.utobj().sendKeys(driver, realStatePage.txtTenantExtension, dsRealState.get("tenant_phoneExtension"));
				fc.utobj().sendKeys(driver, realStatePage.txtTenantFaxNumbers, dsRealState.get("tenant_fax"));
				fc.utobj().sendKeys(driver, realStatePage.txtTenantMobileNumbers, dsRealState.get("tenant_mobile"));
				fc.utobj().sendKeys(driver, realStatePage.txtTenantEmailIds, dsRealState.get("tenant_email"));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, realStatePage.btnAdd);
				if (fc.utobj().assertLinkText(driver, "Modify")) {
					Reporter.log("Real Estate details has been modifying successfully");
				} else {
					fc.utobj().throwsException("Real Estate Details couldn't be modified . Test couldn't continue !!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in modifying real estate details , please refer screenshot!");
		}
	}

	public String modifyTerritory(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("*********************** Modify Territory Details ****************************** ");

		String testCaseId = "TC_InFoMgr_CorporateLocations_Modify_Territory_Details";
		Map<String, String> dsTerritory = fc.utobj().readTestData("infomgr", testCaseId);

		InfoMgrTerritoryPage territoryPage = new InfoMgrCorporateLocationsPage(driver).getTerritoryPage();
		fc.utobj().clickLink(driver, "Modify");
		String txtNotes = fc.utobj().generateTestData(dsTerritory.get("notes"));

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().clickRadioButton(driver, territoryPage.rdoTypeTerritory, dsTerritory.get("typeofTerritory"));
				fc.utobj().sendKeys(driver, territoryPage.txtSalesRequirement, dsTerritory.get("minimumSalesRequirement"));
				fc.utobj().sendKeys(driver, territoryPage.txtRestrictions, dsTerritory.get("restrictionsonFranchisee"));
				fc.utobj().sendKeys(driver, territoryPage.txtMarketingObligation, dsTerritory.get("franchiseesMarketingObligations"));
				fc.utobj().sendKeys(driver, territoryPage.txtOwned, dsTerritory.get("otherExclusiveTerritoriesOwned"));
				fc.utobj().sendKeys(driver, territoryPage.txtJuridiction, dsTerritory.get("jurisdiction"));
				fc.utobj().sendKeys(driver, territoryPage.txtDisputes, dsTerritory.get("disputesifany"));
				fc.utobj().sendKeys(driver, territoryPage.txtNotes, txtNotes);
				fc.utobj().sendKeys(driver, territoryPage.txtGeoCoordinates, dsTerritory.get("geographicCoordinates"));
				fc.utobj().sendKeys(driver, territoryPage.txtLocation, dsTerritory.get("location"));
				fc.utobj().sendKeys(driver, territoryPage.txtCounty, dsTerritory.get("county"));
				/*
				 * fc.utobj().sendKeys(driver, territoryPage.txtZip,
				 * dsTerritory.get("zipOwnedExclusively"));
				 * fc.utobj().clickRadioButton(driver,
				 * territoryPage.rdoZipLocatorIdentical,
				 * dsTerritory.get("exclusiveZipsLocatorZipsIdentical"));
				 * fc.utobj().sendKeys(driver,
				 * territoryPage.txtZipOwnedForLocator,
				 * dsTerritory.get("ZipOwnedForLocator"));
				 * fc.utobj().check(territoryPage.chkBoxSync,
				 * dsTerritory.get("updateAdminZipLocator"));
				 */
				fc.utobj().sendKeys(driver, territoryPage.txtLandBoundaries, dsTerritory.get("landBoundaries"));
				fc.utobj().sendKeys(driver, territoryPage.txtAraeSize, dsTerritory.get("areaSize"));
				fc.utobj().sendKeys(driver, territoryPage.txtNaturalHazards, dsTerritory.get("naturalHazards"));
				fc.utobj().sendKeys(driver, territoryPage.txtCompetitorsFranchise, dsTerritory.get("otherCompetitorsFranchiseTerritory"));
				fc.utobj().sendKeys(driver, territoryPage.txtProximity, dsTerritory.get("proximity"));
				fc.utobj().sendKeys(driver, territoryPage.txtCompetitionFive, dsTerritory.get("projectedCompetitionFiveYears"));
				fc.utobj().sendKeys(driver, territoryPage.txtCompetitionTen, dsTerritory.get("projectedCompetitionTenYear"));
				fc.utobj().sendKeys(driver, territoryPage.txtPopulation, dsTerritory.get("totalPopulation"));
				fc.utobj().sendKeys(driver, territoryPage.txtMedianIncome, dsTerritory.get("medianIncome"));
				fc.utobj().sendKeys(driver, territoryPage.txtPortsHarbors, dsTerritory.get("portsandHarbors"));
				fc.utobj().sendKeys(driver, territoryPage.txtAirport, dsTerritory.get("airports"));
				fc.utobj().sendKeys(driver, territoryPage.txtHeliports, dsTerritory.get("heliports"));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, territoryPage.btnSubmit);
				fc.utobj().sleep();
				if (fc.utobj().assertPageSource(driver, txtNotes)) {
					Reporter.log("Territory details has been modified successfully");
				} else {
					fc.utobj().throwsException("Territory details couldn't be modified . Test couldn't continue !!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in modifying territory details , please refer screenshot!");
		}
		return txtNotes;
	}

	public Map<String, String> modifyOwner(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("********************** Modifying Franchisee Owner ***********************");

		String testCaseId = "TC_InFoMgr_CorporateLocations_Modify_Owner";
		Map<String, String> dsOwners = fc.utobj().readTestData("infomgr", testCaseId);

		Map<String, String> lstOwnerDetails = new HashMap<>();
		lstOwnerDetails.put("FirstName", fc.utobj().generateTestData(dsOwners.get("firstName")));
		lstOwnerDetails.put("LastName", dsOwners.get("lastName"));
		lstOwnerDetails.put("Email", dsOwners.get("email"));
		lstOwnerDetails.put("Country", dsOwners.get("country"));
		lstOwnerDetails.put("State", dsOwners.get("state"));
		lstOwnerDetails.put("SpouseFirstName", fc.utobj().generateTestData("SpouseFirstName1"));
		lstOwnerDetails.put("SpouseLastName", fc.utobj().generateTestData("SpouseLastName1"));

		InfoMgrOwnersPage objAddOwnersPage = new InfoMgrCorporateLocationsPage(driver).getOwnersPage();
		if (fc.utobj().validate(testCaseId) == true) {
			try {
				// fc.utobj().sendKeys(driver, objAddOwnersPage.txtFirstName,
				// lstOwnerDetails.get("FirstName"));
				// fc.utobj().sendKeys(driver, objAddOwnersPage.txtLastName,
				// lstOwnerDetails.get("LastName"));

				fc.utobj().sendKeys(driver, objAddOwnersPage.txtEmail, lstOwnerDetails.get("Email"));
				fc.utobj().selectDropDownByVisibleText(driver, objAddOwnersPage.drpHomeCountry, lstOwnerDetails.get("Country"));
				fc.utobj().sleep();
				fc.utobj().selectDropDownByVisibleText(driver, objAddOwnersPage.drpHomeState, lstOwnerDetails.get("State"));
				fc.utobj().sendKeys(driver, objAddOwnersPage.txtSpouseFirstName, lstOwnerDetails.get("SpouseFirstName"));
				fc.utobj().sendKeys(driver, objAddOwnersPage.txtSpouseLastName, lstOwnerDetails.get("SpouseLastName"));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, objAddOwnersPage.btnAdd);
				boolean ownerAddedFlag = fc.utobj().assertPageSource(driver, fc.utobj().translateString("Owner Details"));
				if (ownerAddedFlag) {
					Reporter.log("Owner modified successfully !!!");
				} else {
					fc.utobj().throwsException("Error in modifying owner !!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in modifying owner , please refer screenshot!");
		}
		return lstOwnerDetails;
	}

	public void modifyTraining(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("********************** modify Training ****************** \n");

		String testCaseId = "TC_InFoMgr_CorporateLocations_Training_Modify";
		Map<String, String> dsTraining = fc.utobj().readTestData("infomgr", testCaseId);

		InfoMgrTrainingPage objAddTrainingPage = new InfoMgrCorporateLocationsPage(driver).getTrainingPage();
		String trainingProgram = fc.utobj().generateTestData(dsTraining.get("trainingProgram"));
		fc.utobj().clickLink(driver, "Modify");
		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().sendKeys(driver, objAddTrainingPage.txtTrainingProgName, trainingProgram);
				fc.utobj().selectDropDownByPartialText(driver, objAddTrainingPage.drpTrainingType, dsTraining.get("trainingType"));
				fc.utobj().sendKeys(driver, objAddTrainingPage.txtAttendee, dsTraining.get("attendee"));
				fc.utobj().sendKeys(driver, objAddTrainingPage.txtCompletionDate, fc.utobj().getFutureDateUSFormat(10));
				fc.utobj().sendKeys(driver, objAddTrainingPage.txtGrade, dsTraining.get("grade"));
				fc.utobj().sendKeys(driver, objAddTrainingPage.txtInstructor, dsTraining.get("instructor"));
				fc.utobj().sendKeys(driver, objAddTrainingPage.txtLocation, dsTraining.get("location"));
				fc.utobj().sendKeys(driver, objAddTrainingPage.txtAttendeeTitle, dsTraining.get("attendeeTitle"));
				fc.utobj().sendKeys(driver, objAddTrainingPage.txtScore, dsTraining.get("score"));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, objAddTrainingPage.btnSubmit);
				fc.utobj().sleep();
				String txtTraining = fc.utobj().getText(driver, objAddTrainingPage.tblTrainings);
				if (txtTraining.contains(trainingProgram)) {
					Reporter.log("Training modified successfully");
				} else {
					fc.utobj().throwsException("Error - Training didn't got modified. Test Case failes !!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in modifying training , please refer screenshot!");
		}

	}

	// Common Methods

	public void navigateToFeedback(WebDriver driver, String url) throws InterruptedException {
		int index = -1;
		String feedbackUrl = "";

		if (url.toLowerCase().contains("fcautomation/")) {
			index = url.indexOf("fcautomation/");
			feedbackUrl = url.substring(0, index);
			feedbackUrl = feedbackUrl.concat("/fcautomation/feedback.jsp");
			driver.navigate().to(feedbackUrl);
		}
		if (url.toLowerCase().contains("franconnect/")) {
			index = url.indexOf("franconnect/");
			feedbackUrl = url.substring(0, index);
			feedbackUrl = feedbackUrl.concat("/franconnect/feedback.jsp");
			driver.navigate().to(feedbackUrl);
		}

		Thread.sleep(2000);
	}

	public void searchFranchiseAndClick(WebDriver driver, String franchiseID) throws Exception {
		Reporter.log("**********************Search Franchisee Location and Click ****************** \n");

		String testCaseId = "TC_InFoMgr_CorporateLocations_Search_And_Click";

		fc.infomgr().infomgr_common().InfoMgrCorporateLocations(driver);

		InfoMgrCorporateLocationsPage corporateLocationsPage = new InfoMgrCorporateLocationsPage(driver);

		fc.infomgr().infomgr_common().searchLocationFromShowFilter(driver, franchiseID);
		fc.utobj().actionImgOption(driver, franchiseID, "Modify");
	}

	public void searchFranchise(WebDriver driver, String franchiseID) throws Exception {
		Reporter.log("********************** Search Franchisee ****************** \n");

		fc.infomgr().infomgr_common().InfoMgrCorporateLocations(driver);

		// Search Franchisee and click on the franchiseID
		InfoMgrCorporateLocationsPage corporateLocationsPage = new InfoMgrCorporateLocationsPage(driver);

		fc.utobj().sendKeys(driver, corporateLocationsPage.txtSearchFranchisee, franchiseID);
		fc.utobj().clickElement(driver, corporateLocationsPage.imgFranchiseesSearchButton);

	}

	public void checkCheckBox(WebDriver driver, String franchiseeID) throws Exception {
		int countOfElements = driver.findElements(By.xpath(".//*[text()='" + franchiseeID + "']/preceding::input[@type='checkbox']")).size();

		fc.utobj().getElementByXpath(driver, "(.//*[text()='" + franchiseeID + "']/preceding::input[@type='checkbox'])[" + countOfElements + "]").click();

	}

	public void corporateLocationsPageShowAll(WebDriver driver) throws Exception {
		fc.infomgr().infomgr_common().InfoMgrCorporateLocations(driver);
		if (fc.utobj().assertLinkText(driver, fc.utobj().translateString("Show All"))) {
			fc.utobj().clickLink(driver, fc.utobj().translateString("Show All"));
		}
	}

	// ********************************************************* Delete Section
	// *******************************

	public void deleteTabData(WebDriver driver, Map<String, String> config, WebElement element, String rowToDelete) throws Exception {
		String testCaseId = "TC_InFoMgr_CorporateLocations_TabData_Delete";

		Reporter.log("Deleting" + element.getText() + " data");

		fc.utobj().clickElement(driver, element);
		fc.utobj().sleep();
		if (fc.utobj().validate(testCaseId) == true) {
			try {
				if (rowToDelete == null) {
					fc.utobj().clickLink(driver, "Delete");
				} else {
					fc.utobj().actionImgOption(driver, rowToDelete, "Delete");
				}
				driver.switchTo().alert().accept();
				fc.utobj().switchFrameToDefault(driver);
				fc.utobj().sleep();
				// Verify that there is no delete link displayed on the page

				boolean rowDeleted = true;

				if (rowToDelete == null) {
					rowDeleted = fc.utobj().assertLinkText(driver, "Delete");
				} else {
					rowDeleted = fc.utobj().assertPageSource(driver, rowToDelete);
				}
				if (!rowDeleted) {
					Reporter.log(element.getText() + " data has been deleted");
				} else {
					fc.utobj().throwsException("Some error occured while deleting the " + element.getText() + " data");
				}
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in deleting data , please refer screenshot!");
		}
	}

	// ********************************************************* Default and
	// Termination Section *******************************

	public void terminateFranchiseLocation(WebDriver driver, Map<String, String> config, String franchiseID) throws Exception {
		String testCaseId = "TC_InFoMgr_Franchisees_Terminate";

		fc.utobj().printTestStep("Terminating franchise location ");

		FranchiseesCommonMethods franchisees = new FranchiseesCommonMethods();
		InfoMgrDefaultAndTerminationPage defaultAndTerminationPage = new InfoMgrCorporateLocationsPage(driver).getDefaultAndTerminationPage();
		franchisees.searchFranchiseAndClick(driver, franchiseID);

		fc.utobj().clickElement(driver, defaultAndTerminationPage.defaultTerminationTab);

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().sendKeys(driver, defaultAndTerminationPage.txtCommitteApprovedDate, fc.utobj().getFutureDateUSFormat(10));
				fc.utobj().sendKeys(driver, defaultAndTerminationPage.txtDateTerminated, fc.utobj().getFutureDateUSFormat(15));
				fc.utobj().selectDropDownByVisibleText(driver, defaultAndTerminationPage.drpReason, "Abandonment");
				fc.utobj().selectDropDownByVisibleText(driver, defaultAndTerminationPage.drpTypeOfAction, "Termination");
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, defaultAndTerminationPage.btnSubmit);

				driver.switchTo().alert().accept();
				fc.utobj().switchFrameToDefault(driver);

				franchisees.franchiseesPageShowAll(driver);

				if (!fc.utobj().assertLinkText(driver, franchiseID)) {
					Reporter.log("Franchise Location has been terminated successfully");
				} else {
					fc.utobj().throwsException("Some error occured while terminating the franchise location");
				}
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in terminating the franchise location , please refer screenshot!");
		}
	}

	public void addToGroup(WebDriver driver, Map<String, String> config) throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 60);
		InfoMgrAddToGroupsPge addtoGroupPage = new InfoMgrAddToGroupsPge(driver);
		InfoMgrGroupsPage objGroupsPage = new InfoMgrGroupsPage(driver);

		String testCaseId = "TC_InfoMgr_CorporateLocations_Add_To_Group";
		String groupName = fc.utobj().generateTestData("Group1");

		fc.commonMethods().switch_cboxIframe_frameId(driver);
		fc.utobj().clickElement(driver, addtoGroupPage.AddtoGroup);

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().sendKeys(driver, objGroupsPage.txtGroupName, groupName);
				fc.utobj().sendKeys(driver, objGroupsPage.txtGroupDesc, "Group Desc");
				fc.utobj().clickElement(driver, objGroupsPage.btnAdd);
				if (fc.utobj().assertLinkText(driver, "Show All")) {
					fc.utobj().clickLink(driver, "Show All");
				}
				boolean isGrouponPage = fc.utobj().assertLinkText(driver, groupName);
				if (isGrouponPage) {
					Reporter.log("Group added successfully !!!");
				} else {
					fc.utobj().throwsException("Error in adding group !!!");
				}

				WebElement lnkFranchiseCount = driver.findElement(By.xpath(".//*[text()='" + groupName + "']/following::a[1]"));
				if (fc.utobj().getText(driver, lnkFranchiseCount).contains("1")) {
					Reporter.log(groupName + " group has been added to franchisee");
				} else {
					fc.utobj().throwsException("Some error occured while adding the group to corporate location ");
				}

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in additing group to corporate location , please refer screenshot!");
		}
	}

	public void search(WebDriver driver, Map<String, String> config, InfoMgrFranchiseeFilter franchisee) throws Exception {
		Reporter.log("************************ Verify Search ***********");
		String testCaseId = "TC_InfoMgr_CorporateLocations_Search";
		InfoMgrFranchiseeFilterPage franchiseeFilterPage = new InfoMgrCorporateLocationsPage(driver).getFranchiseeFilterPage();
		fc.utobj().sleep();
		if (fc.utobj().assertLinkText(driver, "Show Filters")) {
			fc.utobj().clickLink(driver, "Show Filters");
		}

		if (fc.utobj().validate(testCaseId) == true) {
			try {

				fc.utobj().setToDefault(driver, franchiseeFilterPage.drpStoreTypeId);
				fc.utobj().selectValFromMultiSelect(driver, franchiseeFilterPage.drpStoreTypeId, franchisee.getStoreTypeID());
				// fc.utobj().clickElement(driver,
				// franchiseeFilterPage.drpStoreTypeId);

				//fc.utobj().setToDefault(driver, franchiseeFilterPage.drpFranchiseeID);
				fc.utobj().selectValFromMultiSelect(driver, franchiseeFilterPage.drpFranchiseeID, franchisee.getFranchiseeID());

				//fc.utobj().setToDefault(driver, franchiseeFilterPage.drpDivision);
				fc.utobj().selectValFromMultiSelect(driver, franchiseeFilterPage.drpDivision, franchisee.getDivisionID());

				//fc.utobj().setToDefault(driver, franchiseeFilterPage.drpOwnerId);
				fc.utobj().selectValFromMultiSelect(driver, franchiseeFilterPage.drpOwnerId, franchisee.getOwnerFirstName() + " " + franchisee.getOwnerLastName());

				//fc.utobj().setToDefault(driver, franchiseeFilterPage.drpAreaRegionOwnerId);
				fc.utobj().selectValFromMultiSelect(driver, franchiseeFilterPage.drpAreaRegionOwnerId, franchisee.getAreaRegionOwner());

				//fc.utobj().setToDefault(driver, franchiseeFilterPage.drpAreaID);
				fc.utobj().selectValFromMultiSelect(driver, franchiseeFilterPage.drpAreaID, franchisee.getAreaRegionName());
				// fc.utobj().clickElement(driver,
				// franchiseeFilterPage.drpAreaID);

				//fc.utobj().setToDefault(driver, franchiseeFilterPage.drpCity);
				fc.utobj().selectValFromMultiSelect(driver, franchiseeFilterPage.drpCity, franchisee.getCity());

				//fc.utobj().setToDefault(driver, franchiseeFilterPage.drpCountryId);
				fc.utobj().selectValFromMultiSelect(driver, franchiseeFilterPage.drpCountryId, franchisee.getCountry());

				//fc.utobj().setToDefault(driver, franchiseeFilterPage.drpStateId);
				fc.utobj().selectValFromMultiSelect(driver, franchiseeFilterPage.drpStateId, franchisee.getState());

				fc.utobj().sendKeys(driver, franchiseeFilterPage.txtContactName, franchisee.getContactFirstName());
				fc.utobj().sendKeys(driver, franchiseeFilterPage.txtSpouseName, franchisee.getSpouseFirstName());
				fc.utobj().clickElement(driver, franchiseeFilterPage.btnSearch);

				boolean isFranchiseeOnPage = fc.utobj().assertLinkText(driver, franchisee.getFranchiseeID());
				List<WebElement> lstchkbox = driver.findElements(By.xpath(".//*[@name='mailMergeForm']//input[@type='checkbox']"));
				if (isFranchiseeOnPage && lstchkbox.size() == 2) {
					Reporter.log("Search filter is working fine !!!");
				} else {
					fc.utobj().throwsException("Error in searching franchisee !!!");
				}

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in searching franchisee, please refer screenshot!");
		}
	}
}
