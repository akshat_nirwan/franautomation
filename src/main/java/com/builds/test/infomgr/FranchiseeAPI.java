package com.builds.test.infomgr;

import java.util.Map;

import org.testng.Assert;
import org.testng.Reporter;

import com.builds.test.admin.CorporateUser;
import com.builds.test.common.Location;
import com.builds.test.common.WebService;
import com.builds.test.admin.CorporateUserAPI;
import com.builds.utilities.BaseUtil;
import com.builds.utilities.CodeUtility;
import com.builds.utilities.FranconnectUtil;

public class FranchiseeAPI extends BaseUtil {

	FranconnectUtil fc = new FranconnectUtil();
	Location loc = new Location();

	String module = "admin";
    String isISODate = "no";
	CodeUtility cu = new CodeUtility();

	public String createDefaultFranchiseeAPI(String regionName , String LocationType) throws Exception {

		try {
			String subModule = "Franchisee";
			WebService service = new WebService();
			Map<String, String> map = readTestDatawithsqllite("info", "TC_DefaultFranchisee", "AddFranchisee");
			String FranchiseeID = "LOC" + fc.utobj().generateRandomNumber();

			fc.utobj().printTestStep("Adding Franchisee through Web Services.");

			loc.setCorporateLocation(map.get("status"));
			loc.setFranchiseID(FranchiseeID);
			loc.setCenterName(map.get("centerName"));
			loc.setAreaRegion(regionName);
			loc.setRoyaltyReportingStartDate(fc.utobj().generatefutureDatewithformat("YYYY-MM-dd", 0));
			loc.setOpeningDate(fc.utobj().generatefutureDatewithformat("YYYY-MM-dd", 0));
			loc.setStoreType(map.get("storeTypeId"));
			loc.setStreetAddress(map.get("address"));
			loc.setAddress2(map.get("address2"));
			loc.setCity(map.get("city"));
			loc.setCountry(map.get("country"));
			loc.setState(map.get("state"));
			loc.setZipCode(map.get("zipcode"));
			loc.setPhone(map.get("franchiseeName"));
			loc.setPhoneExtension(map.get("storePhoneExtn"));
			loc.setFax(map.get("storeFax"));
			loc.setMobile(map.get("storeMobile"));
			loc.setEmail(map.get("storeEmail"));
			loc.setTitle(map.get("contactTitle"));
			loc.setFirstName(map.get("contactFirstName"));
			loc.setLastName(map.get("contactLastName"));
			loc.setPhone(map.get("phone1"));
			loc.setPhoneExtension(map.get("phone1Extn"));
			loc.setFax(map.get("fax"));
			loc.setMobile(map.get("mobile"));
			loc.setEmail(map.get("emailID"));
			loc.setOwnerTypeIndividualEntity(map.get("entityDetail"));
			loc.setOwnerTypeIndividual(map.get("ownerType"));
			loc.setTitle(map.get("ownerTitle"));
			//loc.setTitle("gdsahv");

			loc.setOwnerFirstName(map.get("firstName"));
			loc.setOwnerLastName(map.get("lastName"));
			loc.setMoveDocuments(map.get("shiftdoc"));

			String xmlString = "<fcRequest>" + "<adminFranchisee>" + "<status>" + LocationType + "</status>"
					+ "<franchiseeName>" + loc.getFranchiseID() + "</franchiseeName>" + "<centerName>"
					+ loc.getCenterName() + "</centerName>" + "<areaID>" + loc.getAreaRegion() + "</areaID>"
					+ "<licenseNo>" + loc.getLicenseNo() + "</licenseNo>" + "<reportPeriodStartDate>"
					+ loc.getRoyaltyReportingStartDate() + "</reportPeriodStartDate>" + "<fbc>" + loc.getFBC()
					+ "</fbc>" + "<openingDate>" + loc.getOpeningDate() + "</openingDate>" + "<storeTypeId>"
					+ loc.getStoreType() + "</storeTypeId>" + "<address>" + loc.getStreetAddress() + "</address>"
					+ "<address2>" + loc.getAddMore() + "</address2>" + "<city>" + loc.getCity() + "</city>"
					+ "<country>" + loc.getCountry() + "</country>" + "<state>" + loc.getState() + "</state>"
					+ "<zipcode>" + loc.getZipCode() + "</zipcode>" + "<storePhone>" + loc.getPhone() + "</storePhone>"
					+ "<storePhoneExtn>" + loc.getPhoneExtension() + "</storePhoneExtn>" + "<storeFax>" + loc.getFax()
					+ "</storeFax>" + "<storeMobile>" + loc.getMobile() + "</storeMobile>" + "<storeEmail>"
					+ loc.getEmail() + "</storeEmail>" + "<storeWebsite>" + loc.getWebsite() + "</storeWebsite>"
					+ "<contactTitle>" + loc.getTitle() + "</contactTitle>" + "<contactFirstName>" + loc.getFirstName()
					+ "</contactFirstName>" + "<contactLastName>" + loc.getLastName() + "</contactLastName>"
					+ "<phone1>" + loc.getPhone() + "</phone1>" + "<phone1Extn>" + loc.getPhoneExtension()
					+ "</phone1Extn>" + "<fax>" + loc.getFax() + "</fax>" + "<mobile>" + loc.getMobile() + "</mobile>"
					+ "<emailID>" + loc.getEmail() + "</emailID>" + "<lastModifiedBy>" + "" + "</lastModifiedBy>"
					+ "<versionID>" + "" + "</versionID>" + "<entityDetail>" + loc.getOwnerTypeIndividualEntity()
					+ "</entityDetail>" + "<entityType>" + "</entityType>" + "<fimCbEntityType>" + ""
					+ "</fimCbEntityType>" + "<fimTtEntityName>" + "" + "</fimTtEntityName>" + "<fimTtTaxpayer>" + ""
					+ "</fimTtTaxpayer>" + "<fimCbStateOfFormation>" + "" + "</fimCbStateOfFormation>"
					+ "<fimCbCountryOfFormation>" + "" + "</fimCbCountryOfFormation>" + "<ownerType>"
					+ loc.getOwnerTypeIndividual() + "</ownerType>" + "<ownerTitle>" + loc.getTitle() + "</ownerTitle>"
					+ "<firstName>" + loc.getOwnerFirstName() + "</firstName>" + "<lastName>" + loc.getOwnerLastName()
					+ "</lastName>" + "<muidValue>" + loc.getMUID() + "</muidValue>" + "<leadReferenceId>" + ""
					+ "</leadReferenceId>" + "<leadTitle>" + "" + "</leadTitle>" + "<leadFirstName>" + ""
					+ "</leadFirstName>" + "<leadLastName>" + "" + "</leadLastName>" + "<shiftdoc>"
					+ loc.getMoveDocuments() + "</shiftdoc>" + "</adminFranchisee>" + "</fcRequest>";

			String responseMessage = service.create(module, subModule, xmlString, isISODate);
			Assert.assertNotEquals(cu.getValFromXML(responseMessage, "responseStatus"), "Error");
			System.out.println("Response message --->" + responseMessage);

			String xmlString1 = "<fcRequest>" + "<filter>" + "<referenceId></referenceId>" + "<franchiseeName>"

					+ loc.getFranchiseID() + "</franchiseeName>" + "</filter>" + "</fcRequest>";

			String responseMessage1 = service.retrieve(module, subModule, xmlString1, isISODate);

			// Assert.assertEquals(cu.getValFromXML(responseMessage1, "responseStatus"), FranchiseeID);
			Assert.assertNotEquals(cu.getValFromXML(responseMessage1, "responseStatus"), "Error");
			Reporter.log("Franchisee Location Created....");
			return FranchiseeID;
		} catch (Exception e) {
			e.getMessage();
			return null;
		}
	}
	public String createDefaultFranchiseeAPI(String regionName , String LocationType ,String FranchiseeID) throws Exception {

		try {
			String subModule = "Franchisee";
			WebService service = new WebService();
			Map<String, String> map = readTestDatawithsqllite("info", "TC_DefaultFranchisee", "AddFranchisee");
			

			fc.utobj().printTestStep("Adding Franchisee through Web Services.");

			loc.setCorporateLocation(map.get("status"));
			loc.setFranchiseID(FranchiseeID);
			loc.setCenterName(map.get("centerName"));
			loc.setAreaRegion(regionName);
			loc.setRoyaltyReportingStartDate(fc.utobj().generatefutureDatewithformat("YYYY-MM-dd", 0));
			loc.setOpeningDate(fc.utobj().generatefutureDatewithformat("YYYY-MM-dd", 0));
			loc.setStoreType(map.get("storeTypeId"));
			loc.setStreetAddress(map.get("address"));
			loc.setAddress2(map.get("address2"));
			loc.setCity(map.get("city"));
			loc.setCountry(map.get("country"));
			loc.setState(map.get("state"));
			loc.setZipCode(map.get("zipcode"));
			loc.setPhone(map.get("franchiseeName"));
			loc.setPhoneExtension(map.get("storePhoneExtn"));
			loc.setFax(map.get("storeFax"));
			loc.setMobile(map.get("storeMobile"));
			loc.setEmail(map.get("storeEmail"));
			loc.setTitle(map.get("contactTitle"));
			loc.setFirstName(map.get("contactFirstName"));
			loc.setLastName(map.get("contactLastName"));
			loc.setPhone(map.get("phone1"));
			loc.setPhoneExtension(map.get("phone1Extn"));
			loc.setFax(map.get("fax"));
			loc.setMobile(map.get("mobile"));
			loc.setEmail(map.get("emailID"));
			loc.setOwnerTypeIndividualEntity(map.get("entityDetail"));
			loc.setOwnerTypeIndividual(map.get("ownerType"));
			loc.setTitle(map.get("ownerTitle"));
			//loc.setTitle("gdsahv");

			loc.setOwnerFirstName(map.get("firstName"));
			loc.setOwnerLastName(map.get("lastName"));
			loc.setMoveDocuments(map.get("shiftdoc"));

			String xmlString = "<fcRequest>" + "<adminFranchisee>" + "<status>" + LocationType + "</status>"
					+ "<franchiseeName>" + loc.getFranchiseID() + "</franchiseeName>" + "<centerName>"
					+ loc.getCenterName() + "</centerName>" + "<areaID>" + loc.getAreaRegion() + "</areaID>"
					+ "<licenseNo>" + loc.getLicenseNo() + "</licenseNo>" + "<reportPeriodStartDate>"
					+ loc.getRoyaltyReportingStartDate() + "</reportPeriodStartDate>" + "<fbc>" + loc.getFBC()
					+ "</fbc>" + "<openingDate>" + loc.getOpeningDate() + "</openingDate>" + "<storeTypeId>"
					+ loc.getStoreType() + "</storeTypeId>" + "<address>" + loc.getStreetAddress() + "</address>"
					+ "<address2>" + loc.getAddMore() + "</address2>" + "<city>" + loc.getCity() + "</city>"
					+ "<country>" + loc.getCountry() + "</country>" + "<state>" + loc.getState() + "</state>"
					+ "<zipcode>" + loc.getZipCode() + "</zipcode>" + "<storePhone>" + loc.getPhone() + "</storePhone>"
					+ "<storePhoneExtn>" + loc.getPhoneExtension() + "</storePhoneExtn>" + "<storeFax>" + loc.getFax()
					+ "</storeFax>" + "<storeMobile>" + loc.getMobile() + "</storeMobile>" + "<storeEmail>"
					+ loc.getEmail() + "</storeEmail>" + "<storeWebsite>" + loc.getWebsite() + "</storeWebsite>"
					+ "<contactTitle>" + loc.getTitle() + "</contactTitle>" + "<contactFirstName>" + loc.getFirstName()
					+ "</contactFirstName>" + "<contactLastName>" + loc.getLastName() + "</contactLastName>"
					+ "<phone1>" + loc.getPhone() + "</phone1>" + "<phone1Extn>" + loc.getPhoneExtension()
					+ "</phone1Extn>" + "<fax>" + loc.getFax() + "</fax>" + "<mobile>" + loc.getMobile() + "</mobile>"
					+ "<emailID>" + loc.getEmail() + "</emailID>" + "<lastModifiedBy>" + "" + "</lastModifiedBy>"
					+ "<versionID>" + "" + "</versionID>" + "<entityDetail>" + loc.getOwnerTypeIndividualEntity()
					+ "</entityDetail>" + "<entityType>" + "</entityType>" + "<fimCbEntityType>" + ""
					+ "</fimCbEntityType>" + "<fimTtEntityName>" + "" + "</fimTtEntityName>" + "<fimTtTaxpayer>" + ""
					+ "</fimTtTaxpayer>" + "<fimCbStateOfFormation>" + "" + "</fimCbStateOfFormation>"
					+ "<fimCbCountryOfFormation>" + "" + "</fimCbCountryOfFormation>" + "<ownerType>"
					+ loc.getOwnerTypeIndividual() + "</ownerType>" + "<ownerTitle>" + loc.getTitle() + "</ownerTitle>"
					+ "<firstName>" + loc.getOwnerFirstName() + "</firstName>" + "<lastName>" + loc.getOwnerLastName()
					+ "</lastName>" + "<muidValue>" + loc.getMUID() + "</muidValue>" + "<leadReferenceId>" + ""
					+ "</leadReferenceId>" + "<leadTitle>" + "" + "</leadTitle>" + "<leadFirstName>" + ""
					+ "</leadFirstName>" + "<leadLastName>" + "" + "</leadLastName>" + "<shiftdoc>"
					+ loc.getMoveDocuments() + "</shiftdoc>" + "</adminFranchisee>" + "</fcRequest>";

			String responseMessage = service.create(module, subModule, xmlString, isISODate);
			Assert.assertNotEquals(cu.getValFromXML(responseMessage, "responseStatus"), "Error");
			System.out.println("Response message --->" + responseMessage);

			String xmlString1 = "<fcRequest>" + "<filter>" + "<referenceId></referenceId>" + "<franchiseeName>"

					+ loc.getFranchiseID() + "</franchiseeName>" + "</filter>" + "</fcRequest>";

			String responseMessage1 = service.retrieve(module, subModule, xmlString1, isISODate);

			// Assert.assertEquals(cu.getValFromXML(responseMessage1, "responseStatus"), FranchiseeID);
			Assert.assertNotEquals(cu.getValFromXML(responseMessage1, "responseStatus"), "Error");
			Reporter.log("Franchisee Location Created....");
			return FranchiseeID;
		} catch (Exception e) {
			e.getMessage();
			return null;
		}
	}
	public CorporateUserAPI createDefaultCorporateUser() throws Exception {

		try {
			String subModule = "User";
			CorporateUserAPI cuser = new CorporateUserAPI();
			WebService service = new WebService();
			Map<String, String> map = readTestDatawithsqllite("info", "TC_DefaultCorporateUser", "AddCorporateUser");
			// String corporateUser = "CORP123222";

			fc.utobj().printTestStep("Adding corporate User through Web Services.");
			String userID = "userID" + fc.utobj().generateRandomNumber();
			cuser.setUserID(userID);
			cuser.setPassword(map.get("password"));
			cuser.setUserLevel(map.get("userLevel"));
			cuser.setUserLanguage(map.get("userLanguage"));
			cuser.setExpirationTime(map.get("expirationTime"));
			cuser.setRole(map.get("role"));
			cuser.setJobTitle(map.get("jobTitle"));
			cuser.setFirstname("FN" + fc.utobj().generateRandomChar());
			cuser.setLastname("LN" + fc.utobj().generateRandomChar());
			cuser.setIsAuditor(map.get("isAuditor"));
			cuser.setAddress(map.get("address"));
			cuser.setCity(map.get("city"));
			cuser.setState(map.get("state"));
			cuser.setZipCode(map.get("zipCode"));
			cuser.setCountry(map.get("country"));
			cuser.setPhone1(map.get("phone1"));
			cuser.setPhone2(map.get("phone2"));
			cuser.setPhoneExt2(map.get("phoneExt2"));
			cuser.setFax(map.get("fax"));
			cuser.setMobile(map.get("mobile"));
			cuser.setEmail(map.get("email"));
			cuser.setSendNotification(map.get("sendNotification"));
			cuser.setLoginUserIp(map.get("loginUserIp"));
			cuser.setSendNotification(map.get("sendNotification"));
			cuser.setTimeZone(map.get("timeZone"));

			String xmlString = "<fcRequest>" + "<adminUser>" + "<userID>" + cuser.getUserID() + "</userID>"
					+ "<password>" + cuser.getPassword() + "</password>" + "<userLevel>" + cuser.getUserLevel()
					+ "</userLevel>" + "<userLanguage>" + cuser.getUserLanguage() + "</userLanguage>" + "<timezone>"
					+ cuser.getTimeZone() + "</timezone>" + "<type>" + "" + "</type>" + "<expirationTime>"
					+ cuser.getExpirationTime() + "</expirationTime>" + "<role>" + cuser.getRole() + "</role>"
					+ "<jobTitle>" + cuser.getJobTitle() + "</jobTitle>" + "<firstname>" + cuser.getFirstname()
					+ "</firstname>" + "<lastname>" + cuser.getLastname() + "</lastname>" + "<isAuditor>"
					+ cuser.getIsAuditor() + "</isAuditor>" + "<address>" + cuser.getAddress() + "</address>" + "<city>"
					+ cuser.getCity() + "</city>" + "<state>" + cuser.getState() + "</state>" + "<zipCode>"
					+ cuser.getZipCode() + "</zipCode>" + "<country>" + cuser.getCountry() + "</country>" + "<phone1>"
					+ cuser.getPhone1() + "</phone1>" + "<phoneExt1>" + cuser.getPhone2() + "</phoneExt1>" + "<phone2>"
					+ cuser.getPhone2() + "</phone2>" + "<phoneExt2>" + cuser.getPhoneExt2() + "</phoneExt2>" + "<fax>"
					+ cuser.getFax() + "</fax>" + "<mobile>" + cuser.getMobile() + "</mobile>" + "<email>"
					+ cuser.getEmail() + "</email>" + "<sendNotification>" + "" + "</sendNotification>"
					+ "<loginUserIp>" + "" + "</loginUserIp>" + "<isDaylight>" + "" + "</isDaylight>" + "</adminUser>"
					+ "</fcRequest>";

			String responseMessage = service.create(module, subModule, xmlString, isISODate);
			System.out.println("Response message --->" + responseMessage);
			String xmlString1 = "<fcRequest>" + "<filter>" + "<referenceId></referenceId>" + "<userID>"

					+ userID + "</userID>" + "</filter>" + "</fcRequest>";

			String responseMessage1 = service.retrieve(module, subModule, xmlString1, isISODate);
			Assert.assertNotEquals(cu.getValFromXML(responseMessage1, "responseStatus"), "Error");
			String CorpName = cuser.getFirstname() /*+ " " + cuser.getLastname()*/;
			Reporter.log("Corporate User Created....");
			return cuser;
		} catch (Exception e) {
			e.getMessage();
			return null;
		}
	}
}
