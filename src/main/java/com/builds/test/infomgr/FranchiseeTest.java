package com.builds.test.infomgr;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminAreaRegionAddAreaRegionPageTest;
import com.builds.test.admin.LocationData;
import com.builds.test.common.Location;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class FranchiseeTest {
	
	FranconnectUtil fc = new FranconnectUtil();
	InfoMgr_Common imc = new InfoMgr_Common();

	@Test(groups = { "infomgr" })
	@TestCase(createdOn = "2018-05-27", updatedOn = "2018-05-28", testCaseId = "TC_IM_Franchisee/Corporate_01", testCaseDescription = "Add Data in Franchisee Agreement Tab")
	private void addLocationAdmin01() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("infomgr3", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Location location = new Location();
		LocationData ld = new LocationData();
		Agreement agreement = new Agreement();

		try {
			driver = fc.loginpage().login(driver);
			AdminAreaRegionAddAreaRegionPageTest region_page = new AdminAreaRegionAddAreaRegionPageTest();
			String region_name = fc.utobj().generateTestData("R1");
			region_page.addAreaRegion(driver, region_name);

			location = fc.commonMethods().getDefaultDataFor_Location(location);
			fc.adminpage().adminAddNewFranchiseLocationLnk(driver);
			location.setAreaRegion(region_name);
			ld.filllocationInfo(driver, location).submit(driver);
			System.out.println(location.getFranchiseID());
			fc.adminpage().manageFranchiseLocationLnk(driver).SearchFranchise(driver, location).VerifyLocationExists(driver, location);
			imc.fimModule(driver);
			imc.InfoMgrFranchisees(driver);
			
			imc.SearchFranchiseeAndClick(driver, location.getFranchiseID());
			imc.InfoMgrFranchiseeAgreement(driver).FillAgreementTabData(driver, imc.getDefaultDataFor_FranchiseeAgreementTab(agreement)).submit(driver);
			if(fc.utobj().assertPageSource(driver, agreement.getSalesperson())==false) {
				fc.utobj().printBugStatus("Agreement Tab Data not added");
			}
			fc.utobj().printBugStatus("Agreement Tab Data Added Successfully");
			
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
}
