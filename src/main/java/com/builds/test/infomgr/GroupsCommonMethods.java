package com.builds.test.infomgr;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;

import com.builds.uimaps.infomgr.InfoMgrGroupsPage;
import com.builds.uimaps.infomgr.InfoMgrSearchPage;
import com.builds.utilities.FranconnectUtil;

public class GroupsCommonMethods {

	FranconnectUtil fc = new FranconnectUtil();

	public String addPublicGroup(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("********************** Adding Public Groups  ***********************");

		String testCaseId = "TC_InfoMgr_Groups_Add_Public_Group";
		Map<String, String> dsGroups = fc.utobj().readTestData("infomgr", testCaseId);
		String groupName = fc.utobj().generateTestData(dsGroups.get("groupName"));

		InfoMgrGroupsPage objGroupsPage = new InfoMgrGroupsPage(driver);

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().sendKeys(driver, objGroupsPage.txtGroupName, groupName);
				fc.utobj().sendKeys(driver, objGroupsPage.txtGroupDesc, dsGroups.get("groupDesc"));
				fc.utobj().clickElement(driver, objGroupsPage.btnAdd);
				groupsPageShowAll(driver);
				boolean isGrouponPage = fc.utobj().assertLinkText(driver, groupName);
				if (isGrouponPage) {
					Reporter.log("Group added successfully !!!");
				} else {
					fc.utobj().throwsException("Error in adding group !!!");
				}

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in adding groups , please refer screenshot!");
		}

		return groupName;

	}

	public String addPrivateGroup(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("********************** Adding Private Groups  ***********************");

		String testCaseId = "TC_InfoMgr_Groups_Add_Private_Group";
		Map<String, String> dsGroups = fc.utobj().readTestData("infomgr", testCaseId);
		String groupName = fc.utobj().generateTestData(dsGroups.get("groupName"));

		InfoMgrGroupsPage objGroupsPage = new InfoMgrGroupsPage(driver);

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().sendKeys(driver, objGroupsPage.txtGroupName, groupName);
				fc.utobj().sendKeys(driver, objGroupsPage.txtGroupDesc, dsGroups.get("groupDesc"));
				fc.utobj().clickRadioButton(driver, objGroupsPage.rdoGroupType, dsGroups.get("groupType"));
				fc.utobj().clickElement(driver, objGroupsPage.btnAdd);
				groupsPageShowAll(driver);
				boolean isGrouponPage = fc.utobj().assertLinkText(driver, groupName);
				if (isGrouponPage) {
					Reporter.log("Private Group added successfully !!!");
				} else {
					fc.utobj().throwsException("Error in adding private group !!!");
				}

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in adding private groups , please refer screenshot!");
		}

		return groupName;
	}

	public String modifyGroup(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("********************** Modify Public Groups  ***********************");

		String testCaseId = "TC_InfoMgr_Groups_Modify";
		Map<String, String> dsGroups = fc.utobj().readTestData("infomgr", testCaseId);
		String groupName = fc.utobj().generateTestData(dsGroups.get("groupName"));

		InfoMgrGroupsPage objGroupsPage = new InfoMgrGroupsPage(driver);

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().sendKeys(driver, objGroupsPage.txtGroupName, groupName);
				fc.utobj().sendKeys(driver, objGroupsPage.txtGroupDesc, dsGroups.get("groupDesc"));
				fc.utobj().clickElement(driver, objGroupsPage.btnAdd);
				groupsPageShowAll(driver);
				boolean isGrouponPage = fc.utobj().assertLinkText(driver, groupName);
				if (isGrouponPage) {
					Reporter.log("Group modified successfully !!!");
				} else {
					fc.utobj().throwsException("Error in modifying group !!!");
				}

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}

		} else {
			fc.utobj().throwsSkipException("Error in modifying groups , please refer screenshot!");
		}
		return groupName;
	}

	public void VerifyPrintPreview(WebDriver driver, String textTobeVerified) throws Exception {
		Set<String> windows = new HashSet<String>();
		windows = driver.getWindowHandles();

		boolean isTextDisplayed = false;

		String currentWindow = driver.getWindowHandle();
		for (String window : windows) {
			if (!window.equals(currentWindow)) {

				driver.switchTo().window(window);
				if (driver.getCurrentUrl().contains("printcheck")) {
					isTextDisplayed = fc.utobj().assertPageSource(driver, textTobeVerified);
				}
			}
		}
		driver.close();

		if (isTextDisplayed) {
			Reporter.log(textTobeVerified + "  is displayed on the print preview page. !!! ");
		} else {
			fc.utobj().throwsException(textTobeVerified + "  is not displayed on the print preview page. !!! ");
		}

		driver.switchTo().window(currentWindow);
	}

	public void deleteGroup(WebDriver driver, Map<String, String> config, String txtGroupName, boolean fromActionLnk)
			throws Exception {
		String testCaseId = "TC_InFoMgr_Group_Delete";
		InfoMgrGroupsPage groupsPage = new InfoMgrGroupsPage(driver);
		groupsPageShowAll(driver);
		if (fc.utobj().validate(testCaseId) == true) {
			try {
				if (fromActionLnk == false) {
					checkLocation(driver, txtGroupName);
					fc.utobj().clickElement(driver, groupsPage.btnDelete);
				} else {
					fc.utobj().actionImgOption(driver, txtGroupName, "Delete");
				}

				driver.switchTo().alert().accept();
				fc.utobj().switchFrameToDefault(driver);
				fc.utobj().sleep();
				groupsPageShowAll(driver);

				if (!fc.utobj().assertLinkText(driver, txtGroupName)) {
					Reporter.log(txtGroupName + " has been deleted");
				} else {
					fc.utobj().throwsException("Some error occured while deleting the " + txtGroupName);
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in deleting group , please refer screenshot!");
		}
	}

	public void archiveGroup(WebDriver driver, Map<String, String> config, String txtGroupName, boolean fromActionLnk)
			throws Exception {
		String testCaseId = "TC_InFoMgr_Group_Archive";
		InfoMgrGroupsPage groupsPage = new InfoMgrGroupsPage(driver);
		groupsPageShowAll(driver);
		if (fc.utobj().validate(testCaseId) == true) {
			try {
				if (fromActionLnk == false) {
					checkLocation(driver, txtGroupName);
					fc.utobj().clickElement(driver, groupsPage.btnArchive);
				} else {
					fc.utobj().actionImgOption(driver, txtGroupName, "Archive");
				}
				driver.switchTo().alert().accept();
				fc.utobj().switchFrameToDefault(driver);
				fc.utobj().sleep();
				fc.utobj().clickElement(driver, groupsPage.lnkArchivedGroups);
				groupsPageShowAll(driver);
				if (fc.utobj().assertLinkText(driver, txtGroupName)) {
					Reporter.log(txtGroupName + " has been archived");
				} else {
					fc.utobj().throwsException("Some error occured while archiving the group -  " + txtGroupName);
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in archiving the group , please refer screenshot!");
		}
	}

	public void restoreGroup(WebDriver driver, Map<String, String> config, String txtGroupName, boolean fromActionLnk)
			throws Exception {
		String testCaseId = "TC_InFoMgr_Group_Restore";
		InfoMgrGroupsPage groupsPage = new InfoMgrGroupsPage(driver);
		if (fc.utobj().validate(testCaseId) == true) {
			try {
				if (fromActionLnk == false) {
					checkLocation(driver, txtGroupName);
					fc.utobj().clickElement(driver, groupsPage.btnRestore);
				} else {
					fc.utobj().actionImgOption(driver, txtGroupName, "Restore");
				}
				driver.switchTo().alert().accept();
				fc.utobj().switchFrameToDefault(driver);
				fc.utobj().sleep();
				fc.utobj().clickElement(driver, groupsPage.lnkGroups);
				groupsPageShowAll(driver);

				if (fc.utobj().assertLinkText(driver, txtGroupName)) {
					Reporter.log(txtGroupName + " group has been restored !");
				} else {
					fc.utobj().throwsException("Some error occured while restoring the group -  " + txtGroupName);
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in restoring the group , please refer screenshot!");
		}
	}

	public void associateWithFranchisee(WebDriver driver, Map<String, String> config, String franchiseID,
			String groupName) throws Exception {
		InfoMgrSearchPage searchPage = new InfoMgrSearchPage(driver);
		WebDriverWait wait = new WebDriverWait(driver, 60);

		String testCaseId = "TC_InFoMgr_Group_Associate_With_Franchisee";

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().actionImgOption(driver, groupName, "Associate with Franchisees");
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().sendKeys(driver, searchPage.txtFranchiseID, franchiseID);
				fc.utobj().clickElement(driver, searchPage.btnSearch);
				fc.utobj().clickElement(driver, searchPage.btnYes);
				fc.utobj().clickElement(driver, searchPage.btnClose);
				fc.utobj().switchFrameToDefault(driver);
				fc.utobj().sleep();
				WebElement lnkFranchiseCount = driver
						.findElement(By.xpath(".//*[text()='" + groupName + "']/following::a[1]"));
				if (fc.utobj().getText(driver, lnkFranchiseCount).contains("1")) {
					Reporter.log(groupName + " group has been associated with franchisee " + franchiseID);
				} else {
					fc.utobj().throwsException("Some error occured while associating the franchisees with the group ");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException(
					"Error in associating the franchisees with the group , please refer screenshot!");
		}
	}

	public String search(WebDriver driver, Map<String, String> config, String groupName) throws Exception {
		Reporter.log("********************** Adding Public Groups  ***********************");

		String testCaseId = "TC_InfoMgr_Groups_Search";
		Map<String, String> dsGroups = fc.utobj().readTestData("infomgr", testCaseId);

		InfoMgrGroupsPage objGroupsPage = new InfoMgrGroupsPage(driver);

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().sendKeys(driver, objGroupsPage.txtGroupSearch, groupName);
				fc.utobj().clickElement(driver, objGroupsPage.btnSearch);
				boolean isGrouponPage = fc.utobj().assertLinkText(driver, groupName);
				if (isGrouponPage) {
					Reporter.log("Group searched successfully !!!");
				} else {
					fc.utobj().throwsException("Error in searching group !!!");
				}

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in searching groups , please refer screenshot!");
		}

		return groupName;

	}

	public void checkLocation(WebDriver driver, String location) throws Exception {
		String xpath = ".//*[@id='siteMainTable']//a[text()='" + location + "']/preceding::input[@type='checkbox'][1]";
		WebElement checkBox = fc.utobj().getElementByXpath(driver, xpath);
		fc.utobj().clickElement(driver, checkBox);
	}

	public void groupsPageShowAll(WebDriver driver) throws Exception {
		if (fc.utobj().assertLinkText(driver, "Show All")) {
			fc.utobj().clickLink(driver, "Show All");
		}
	}

}
