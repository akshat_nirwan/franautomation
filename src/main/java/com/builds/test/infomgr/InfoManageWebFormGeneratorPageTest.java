package com.builds.test.infomgr;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.builds.uimaps.fs.AdminFranchiseSalesManageWebFormGeneratorPage;
import com.builds.uimaps.fs.FSLeadSummaryAddLeadPage;
import com.builds.uimaps.fs.FSLeadSummaryPrimaryInfoPage;
import com.builds.uimaps.fs.LeadSummaryUI;
import com.builds.uimaps.infomgr.InfoMgrFranchiseesPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class InfoManageWebFormGeneratorPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	/* Harish Dwivedi TC_INFOMGR_WebForm_002 */

	@Test(groups = { "InfoFormGenerate" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_INFOMGR_WebForm_002", testCaseDescription = "Verify Modify details filled Web From Generator in Web Form")
	private void verifyModifydSalesWebform() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("infomgr", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			// String fieldName = dataSet.get("displayName");
			// String docTitle = dataSet.get("docTitle");
			String fieldName = fc.utobj().generateTestData(dataSet.get("displayName"));
			AdminInfoMgrManageWebFormGeneratorPageTest p1 = new AdminInfoMgrManageWebFormGeneratorPageTest();
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Web Form Generator");
			fc.infomgr().infomgr_common().AdminInfoManageWebFormGenerator(driver);

			AdminFranchiseSalesManageWebFormGeneratorPage pobj = new AdminFranchiseSalesManageWebFormGeneratorPage(
					driver);
			// Click on Create Form
			String formName = p1.addLeadFromWebForm(driver, fieldName);
			fc.utobj().sendKeys(driver, pobj.searchMyForm, formName);
			fc.utobj().clickElement(driver, pobj.searchMyFormBtn);
			fc.utobj().printTestStep("Modify Form");
			fc.utobj().actionImgOption(driver, formName, "Modify");

			String newfromname = "ModifiedForm" + fc.utobj().generateRandomNumber();

			fc.utobj().sendKeys(driver, pobj.formName, newfromname);
			fc.utobj().clickElement(driver, pobj.saveNextBtn);
			fc.utobj().clickElement(driver, pobj.saveAndNextBtnDesign);
			fc.utobj().clickElement(driver, pobj.afterSubmissionMsg);

			fc.utobj().clickElement(driver, pobj.finishBtn);
			if (!pobj.hostURL.isSelected()) {
				fc.utobj().clickElement(driver, pobj.hostURL);
			}
			fc.utobj().clickElement(driver, pobj.okBtn);
			fc.utobj().printTestStep("Search modfied Form");
			fc.utobj().sendKeys(driver, pobj.searchMyForm, newfromname);
			fc.utobj().clickElement(driver, pobj.searchMyFormBtn);

			fc.utobj().printTestStep("Verify Modify Form");

			boolean isformexits = fc.utobj().verifyElementOnVisible_ByXpath(driver, " .//*[contains(text(),'" + formName + "')]");
			if (isformexits == false) {
				// fc.utobj().throwsException("was not able to verify
				// browserName Field");
			} else {
				fc.utobj().throwsException("was not able to verify browserName Field");

			}

			fc.utobj().printTestStep("Search Form");
			fc.utobj().sendKeys(driver, pobj.searchMyForm, newfromname);
			fc.utobj().clickElement(driver, pobj.searchMyFormBtn);

			fc.utobj().printTestStep("Deactivate Form");
			fc.utobj().actionImgOption(driver, newfromname, "Deactivate");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().sendKeys(driver, pobj.searchMyForm, newfromname);
			fc.utobj().clickElement(driver, pobj.searchMyFormBtn);

			fc.utobj().printTestStep("Verify The DeActivate Form");
			fc.utobj().moveToElement(driver, fc.utobj().getElementByXpath(driver,".//*[contains(text () ,'" + newfromname + "')]/ancestor::tr/td/div[@id='menuBar']/layer"));
			String alterText = driver
					.findElement(By.xpath(
							".//*[contains(text () ,'" + newfromname + "')]/ancestor::tr/td/div[@id='menuBar']/layer"))
					.getAttribute("id").trim();
			alterText = alterText.replace("Actions_dynamicmenu", "");
			alterText = alterText.replace("Bar", "");

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[contains(text () ,'" + newfromname + "')]/ancestor::tr/td/div[@id='menuBar']/layer/a/img"));

			boolean formStatus = false;
			List<WebElement> listElements = driver
					.findElements(By.xpath(".//div[@id='Actions_dynamicmenu" + alterText + "Menu']/span"));

			for (int i = 0; i < listElements.size(); i++) {

				if (listElements.get(i).getText().trim().equalsIgnoreCase("Activate")) {
					formStatus = true;
					break;
				}
			}

			if (formStatus == false) {
				fc.utobj().throwsException("was not able to deactivate Form");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/* Harish Dwivedi TC_INFOMGR_WebForm_005 */

	@Test(groups = { "InfoFormGenerate" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_INFOMGR_WebForm_005", testCaseDescription = "Verify that the form is getting copied and customized details filled in Web Form Generator.")
	private void verifyCopyCustomizeSalesWebform() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("infomgr", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			// String fieldName = dataSet.get("displayName");
			// String docTitle = dataSet.get("docTitle");
			String fieldName = fc.utobj().generateTestData(dataSet.get("displayName"));
			AdminInfoMgrManageWebFormGeneratorPageTest p1 = new AdminInfoMgrManageWebFormGeneratorPageTest();
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Web Form Generator");
			fc.infomgr().infomgr_common().AdminInfoManageWebFormGenerator(driver);

			AdminFranchiseSalesManageWebFormGeneratorPage pobj = new AdminFranchiseSalesManageWebFormGeneratorPage(
					driver);
			// Click on Create Form
			String formName = p1.addLeadFromWebForm(driver, fieldName);
			fc.utobj().sendKeys(driver, pobj.searchMyForm, formName);
			fc.utobj().clickElement(driver, pobj.searchMyFormBtn);
			fc.utobj().printTestStep("Copy and Customize Form");
			fc.utobj().actionImgOption(driver, formName, "Copy and Customize");

			String newfromname = "CopyCustomize" + fc.utobj().generateRandomNumber();

			fc.utobj().sendKeys(driver, pobj.formName, newfromname);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByID(driver, "formUrl"), newfromname);
			fc.utobj().clickElement(driver, pobj.saveNextBtn);
			fc.utobj().clickElement(driver, pobj.saveAndNextBtnDesign);
			fc.utobj().clickElement(driver, pobj.afterSubmissionMsg);

			fc.utobj().clickElement(driver, pobj.finishBtn);
			if (!pobj.hostURL.isSelected()) {
				fc.utobj().clickElement(driver, pobj.hostURL);
			}
			fc.utobj().clickElement(driver, pobj.okBtn);
			fc.utobj().printTestStep("Search Copy and Customize For");
			fc.utobj().sendKeys(driver, pobj.searchMyForm, newfromname);
			fc.utobj().clickElement(driver, pobj.searchMyFormBtn);

			fc.utobj().printTestStep("Verify Copy and Customize Form");

			boolean isformexits = fc.utobj().verifyElementOnVisible_ByXpath(driver, " .//*[contains(text(),'" + newfromname + "')]");
			if (isformexits == false) {
				fc.utobj().throwsException("was not able to Verify Copy and Customize For");
			}

			// fc.utobj().printTestStep(testCaseId, "Search Form");
			// fc.utobj().sendKeys(driver, pobj.searchMyForm, newfromname);
			// fc.utobj().clickElement(driver, pobj.searchMyFormBtn);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/* Harish Dwivedi TC_INFOMGR_WebForm_006 */

	@Test(groups = { "InfoFormGenerate" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_INFOMGR_WebForm_006", testCaseDescription = "Verify WebForm With multiple tabs in lead that the lead is submitted with all the info.")
	private void verifyMultiStepWebform() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("infomgr", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();

		try {
			driver = fc.loginpage().login(driver);

			String tabName = dataSet.get("tabDisplayName");
			String sectionName = dataSet.get("sectionValue");
			// String fieldName = dataSet.get("displayName");
			// String docTitle = dataSet.get("docTitle");
			String fieldName = fc.utobj().generateTestData(dataSet.get("displayName"));

			fc.utobj().printTestStep("Add franchise location");
			InfoMgrFranchiseeFilter franchisee = adminInfoMgr.addFranchiseLocationForFilter(driver, false);

			AdminInfoMgrManageWebFormGeneratorPageTest p1 = new AdminInfoMgrManageWebFormGeneratorPageTest();
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Web Form Generator");
			fc.infomgr().infomgr_common().AdminInfoManageWebFormGenerator(driver);

			AdminFranchiseSalesManageWebFormGeneratorPage pobj = new AdminFranchiseSalesManageWebFormGeneratorPage(
					driver);
			// Click on Create Form
			// String formName = p1.addLeadFromWebForm(driver, fieldName);
			String displayOnWebPageText = fc.utobj().generateTestData(dataSet.get("text"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			String formFormat = dataSet.get("formFormat");
			formFormat = "Multi Page";
			String header = fc.utobj().generateTestData(dataSet.get("header"));
			String textLabel = fc.utobj().generateTestData(dataSet.get("textLabel"));
			String fieldLabel = fc.utobj().generateTestData(dataSet.get("fieldLabel"));
			String footer = fc.utobj().generateTestData(dataSet.get("footer"));
			String submissionType = dataSet.get("submissionType");
			String formName = fc.utobj().generateTestData(dataSet.get("formName"));
			// String
			// formTitle=fc.utobj().generateTestData(dataSet.get("formTitle"));
			String formTitle = "WebFrom Title";
			description = fc.utobj().generateTestData(dataSet.get("description"));
			String formUrl = fc.utobj().generateTestData(dataSet.get("formUrl"));
			String afterSubmission = dataSet.get("afterSubmission");
			String redirectedUrl = "";

			p1.createNewFormContactType(driver, formName, formTitle, description, formFormat, submissionType, formUrl,
					sectionName, tabName, header, textLabel, fieldLabel, footer, afterSubmission, redirectedUrl);

			String parentWindow = driver.getWindowHandle();
			fc.utobj().sendKeys(driver, pobj.searchMyForm, formName);
			fc.utobj().clickElement(driver, pobj.searchMyFormBtn);
			fc.utobj().actionImgOption(driver, formName, "Launch & Test");
			Thread.sleep(5000);

			fc.utobj().printTestStep("Launc And Test Form");
			Set<String> allWindows2 = driver.getWindowHandles();
			for (String currentWindow : allWindows2) {
				// Thread.sleep(2000);
				if (!currentWindow.equalsIgnoreCase(parentWindow)) {
					driver.switchTo().window(currentWindow);
					// Thread.sleep(2000);
					String titleTextCurrent = driver.getTitle();
					if (titleTextCurrent.equalsIgnoreCase(formTitle)) {
						// fill the form with data
						// Thread.sleep(2000);

						fc.utobj().selectDropDownByVisibleText(driver,
								fc.utobj().getElementByXpath(driver, "//*[contains(@id, 'franchiseeNo')]"),
								franchisee.getFranchiseeID() + " - " + franchisee.getCenterName()); // work
						fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='centerName']"),
								franchisee.getCenterName() + "_Changed"); // work
						// fc.utobj().selectDropDownByVisibleText(driver,
						// fc.utobj().getElementByXpath(driver,".//*[@id='regionNo']")),
						// "Alabama"); // work

						fc.utobj().clickElement(driver, pobj.nextBtn);
						fc.utobj().clickElement(driver, pobj.submitBtn);

						Thread.sleep(1000);
						fc.utobj().isTextDisplayed(driver,
								"Thank you for submitting your information, we will get back to you shortly.",
								"was not able to verify confirmation Msg");

						driver.close();
					} else {
						driver.close();
						driver.switchTo().window(parentWindow);
					}
					driver.switchTo().window(parentWindow);
				}
				// Thread.sleep(1000);
			}
			Thread.sleep(1000);

			fc.utobj().printTestStep("Search franchise and click");
			searchFranchise(driver, (String) franchisee.getFranchiseeID());
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,("//*[@id='siteMainTable']//a[text()='" + (String) franchisee.getFranchiseeID() + "']")));

			fc.utobj().printTestStep("Verify for the Franchisee in Center Details ");
			boolean iLeadNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[@id='siteMainTable']/tbody//*[contains(text () ,'" + (String) franchisee.getFranchiseeID()
							+ "')]");
			if (iLeadNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Fracnhisee in Center Info Details ");
			}
			fc.utobj().printTestStep("Verify for the Center Name in Center Details ");
			iLeadNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[@id='centerInfoDIV']//*[contains(text () ,'"
					+ (String) franchisee.getCenterName() + "_Changed" + "')]");
			if (iLeadNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Center Name in Center Info Details ");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/* Harish Dwivedi TC_INFOMGR_WebForm_004 */

	@Test(groups = "InfoFormGenerate")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Copy Url , from Wen Form with Format Single Page Web Form Generator", testCaseId = "TC_INFOMGR_WebForm_004")
	public void copyUrlverifyfsWebform() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("infomgr", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();

		try {
			driver = fc.loginpage().login(driver);
			// String fieldName = dataSet.get("displayName");
			// String docTitle = dataSet.get("docTitle");
			String fieldName = fc.utobj().generateTestData(dataSet.get("displayName"));

			fc.utobj().printTestStep("Add franchise location");
			InfoMgrFranchiseeFilter franchisee = adminInfoMgr.addFranchiseLocationForFilter(driver, false);

			AdminInfoMgrManageWebFormGeneratorPageTest p1 = new AdminInfoMgrManageWebFormGeneratorPageTest();
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Web Form Generator");
			fc.infomgr().infomgr_common().AdminInfoManageWebFormGenerator(driver);

			AdminFranchiseSalesManageWebFormGeneratorPage pobj = new AdminFranchiseSalesManageWebFormGeneratorPage(
					driver);
			// Click on Create Form
			String formName = p1.addLeadFromWebForm(driver, fieldName);
			String parentWindow = driver.getWindowHandle();
			fc.utobj().sendKeys(driver, pobj.searchMyForm, formName);
			fc.utobj().clickElement(driver, pobj.searchMyFormBtn);

			fc.utobj().actionImgOption(driver, formName, "Copy URL");

			fc.utobj().printTestStep("Copy Url");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			if (!pobj.hostURL.isSelected()) {
				fc.utobj().clickElement(driver, pobj.hostURL);
			}
			String targetUrl = fc.utobj().getElementByXpath(driver, ".//*[@id='hostCodeBox']/a").getText().trim();
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//input[@value='Close']"));
			fc.utobj().switchFrameToDefault(driver);

			// driver.navigate().to(targetUrl);
			fc.utobj().printTestStep("Add Lead with Copy Url");
			if (FranconnectUtil.config.get("browserName").equalsIgnoreCase("Chrome")) {

				System.setProperty("webdriver.chrome.driver",
						FranconnectUtil.config.get("inputDirectory") + "\\exe\\chromedriver.exe");
				ChromeOptions options = new ChromeOptions();
				options.addArguments("disable-popup-blocking");
				DesiredCapabilities capabilities = DesiredCapabilities.chrome();
				capabilities.setCapability(ChromeOptions.CAPABILITY, options);
				// Browser Logs
				LoggingPreferences logPrefs = new LoggingPreferences();
				logPrefs.enable(LogType.BROWSER, Level.ALL);
				capabilities.setCapability(CapabilityType.LOGGING_PREFS, logPrefs);
				WebDriver driver1 = new ChromeDriver(capabilities);
				Thread.sleep(1000);
				driver1.get(targetUrl);

				// verify
				fc.utobj().selectDropDownByVisibleText(driver1,
						driver1.findElement(By.xpath("//*[contains(@id, 'franchiseeNo')]")),
						franchisee.getFranchiseeID() + " - " + franchisee.getCenterName()); // work
				fc.utobj().clickElement(driver1, driver1.findElement(By.xpath(".//*[@id='submitButton']")));

				boolean isSubmitted = fc.utobj().assertPageSource(driver1,
						"Thank you for submitting your information, we will get back to you shortly.");

				if (isSubmitted == false) {
					fc.utobj().throwsException("Unable to submit lead");
				}
				Thread.sleep(2000);

				driver1.quit();

			} else if (FranconnectUtil.config.get("browserName").equalsIgnoreCase("firefox")) {

			}

			String leadName = fieldName + " " + fieldName;

			// Need to add case according to FIM

			fc.utobj().printTestStep("Search franchise and click");
			searchFranchise(driver, (String) franchisee.getFranchiseeID());
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,("//*[@id='siteMainTable']//a[text()='" + (String) franchisee.getFranchiseeID() + "']")));

			fc.utobj().printTestStep("Verify for the Franchisee in Center Details ");
			boolean iLeadNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[@id='siteMainTable']/tbody//*[contains(text () ,'" + (String) franchisee.getFranchiseeID()
							+ "')]");
			if (iLeadNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Fracnhisee in Center Info Details ");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void searchFranchise(WebDriver driver, String franchiseID) throws Exception {
		Reporter.log("********************** Search Franchisee ****************** \n");

		fc.infomgr().infomgr_common().InfoMgrFranchisees(driver);

		// Search Franchisee and click on the franchiseID
		InfoMgrFranchiseesPage franchiseePage = new InfoMgrFranchiseesPage(driver);

		fc.utobj().sendKeys(driver, franchiseePage.txtSearchFranchisee, franchiseID);
		fc.utobj().clickElement(driver, franchiseePage.imgFranchiseesSearchButton);

	}

	public WebDriver gotoActivityHistorySection(WebDriver driver) throws Exception {

		Reporter.log("Go To Activity History Section");
		try {

			driver.switchTo().defaultContent();
		} catch (Exception E) {

		}

		FSLeadSummaryPrimaryInfoPage fs = new FSLeadSummaryPrimaryInfoPage(driver);
		fc.utobj().moveToElement(driver, fc.utobj().getElement(driver, fs.contactHistorySection));
		fc.utobj().switchFrameById(driver, "leadLogCallSummary");
		return driver;
	}

	public boolean verifySubjectCommentActivityHistory(WebDriver driver, String subjectComments) throws Exception {
		Reporter.log("Verifying Comment / Activity History for status : " + subjectComments);
		driver = gotoActivityHistorySection(driver);

		boolean isRemarksAdded = fc.utobj().assertPageSource(driver, subjectComments);
		if (isRemarksAdded == false) {
			fc.utobj().printBugStatus("Activity History Not Added : " + subjectComments);
		}
		driver.switchTo().defaultContent();
		return isRemarksAdded;
	}

	public boolean verifySubjectCommentActivityHistoryAddesField(WebDriver driver, String fieldNameRemark)
			throws Exception {
		Reporter.log("Verifying Comment / Activity History for status : " + fieldNameRemark);
		driver = gotoActivityHistorySection(driver);

		boolean isRemarksAdded = fc.utobj().assertPageSource(driver, fieldNameRemark);
		if (isRemarksAdded == false) {
			fc.utobj().printBugStatus("Activity History Not Added : " + fieldNameRemark);
		}
		driver.switchTo().defaultContent();
		return isRemarksAdded;
	}

	public void multiSelectListBox(WebDriver driver, WebElement multiSelectbox, String searchTxt) throws Exception {

		fc.utobj().clickElement(driver, multiSelectbox);
		WebElement newElement = multiSelectbox.findElement(By.xpath("./div/div/input[@class='searchInputMultiple']"));
		fc.utobj().sendKeys(driver, newElement, searchTxt);

		WebElement checkboxElement = multiSelectbox.findElement(By.xpath(".//input[@id='selectAll']"));
		checkboxElement.click();
		fc.utobj().clickElement(driver, multiSelectbox);
		Thread.sleep(1000);
	}

	public void actionImgOptionForFormBuilder(WebDriver driver, String fieldName, String option) throws Exception {

		fc.utobj().moveToElement(driver, driver.findElement(
				By.xpath(".//*[contains(text () ,'" + fieldName + "')]/ancestor::tr/td/div[@id='menuBar']/layer")));
		String alterText = driver
				.findElement(By
						.xpath(".//*[contains(text () ,'" + fieldName + "')]/ancestor::tr/td/div[@id='menuBar']/layer"))
				.getAttribute("id").trim();
		alterText = alterText.replace("Actions_dynamicmenu", "");
		alterText = alterText.replace("Bar", "");

		fc.utobj().clickElement(driver, driver.findElement(By
				.xpath(".//*[contains(text () ,'" + fieldName + "')]/ancestor::tr/td/div[@id='menuBar']/layer/a/img")));
		fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
				".//div[@id='Actions_dynamicmenu" + alterText + "Menu']/span[contains(text () , '" + option + "')]"));
	}

	public boolean actionImgOptionForFormBuilderDeleteOption(WebDriver driver, String fieldName, String option)
			throws Exception {

		boolean isDeletebale = false;

		fc.utobj().moveToElement(driver, driver.findElement(
				By.xpath(".//*[contains(text () ,'" + fieldName + "')]/ancestor::tr/td/div[@id='menuBar']/layer")));
		String alterText = driver
				.findElement(By
						.xpath(".//*[contains(text () ,'" + fieldName + "')]/ancestor::tr/td/div[@id='menuBar']/layer"))
				.getAttribute("id").trim();
		alterText = alterText.replace("Actions_dynamicmenu", "");
		alterText = alterText.replace("Bar", "");

		fc.utobj().clickElement(driver, driver.findElement(By
				.xpath(".//*[contains(text () ,'" + fieldName + "')]/ancestor::tr/td/div[@id='menuBar']/layer/a/img")));
		// fc.utobj().clickElement(driver,
		// fc.utobj().getElementByXpath(driver,".//div[@id='Actions_dynamicmenu"+alterText+"Menu']/span[contains(text
		// () , '"+option+"')]")));

		isDeletebale = fc.utobj().verifyElementOnVisible_ByXpath(driver,
				".//div[@id='Actions_dynamicmenu" + alterText + "Menu']/span[contains(text () , '" + option + "')]");
		if (isDeletebale) {
			isDeletebale = true;
		} else {
			isDeletebale = false;
		}

		return isDeletebale;

	}

	public void clickAddLeadLink(WebDriver driver) throws Exception {
		LeadSummaryUI pobj = new LeadSummaryUI(driver);

		try {
			fc.utobj().clickElement(driver, pobj.addLeadLnkAlternate);
		} catch (Exception e) {
			fc.utobj().clickElement(driver, pobj.addLeadLnk);
		}
		WebElement leadNameTxtBox = null;

		FSLeadSummaryAddLeadPage pobj2 = new FSLeadSummaryAddLeadPage(driver);

		try {
			leadNameTxtBox = pobj2.firstName;
			if (leadNameTxtBox.isDisplayed() == false) {
				fc.utobj().throwsException("Add Lead Page Not Opened!");
			}
		} catch (Exception e) {

		}
	}
}