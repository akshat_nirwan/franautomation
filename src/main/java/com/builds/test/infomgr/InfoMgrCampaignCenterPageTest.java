package com.builds.test.infomgr;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.reporting.EmailTemplate;
import com.builds.uimaps.infomgr.InfoMgrCampaignCenterEmailTemplatesPage;
import com.builds.uimaps.infomgr.InfoMgrCampaignCenterPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class InfoMgrCampaignCenterPageTest {

	FranconnectUtil fc = new FranconnectUtil();
	AdminInfoMgrCommonMethods adminInfoMgr = new AdminInfoMgrCommonMethods();
	InfoMgr_Common imcm = new InfoMgr_Common();

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_InfoMgr_CampaignCenter_AddandAssociateCampaign", testCaseDescription = "This test will add a campaign with Plain formate and associate that campaign to the location", reference = {
			"" })
	public void TC_InfoMgr_CampaignCenter_AddandAssociateCampaign() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		CampaignCenterCommonMethods campaign = fc.infomgr().campaignCenter();
		AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();
		try {
			driver = fc.loginpage().login(driver);

			//Add Franchise Location
			fc.utobj().printTestStep("Add a franchise location");
			String franchiseeID = adminInfoMgr.addFranchiseLocation(driver);
			
			//Add Campaign
			fc.infomgr().infomgr_common().InfoMgrCampaignCenter(driver);
			fc.utobj().printTestStep("Create a campaign");
			String campaignTitle = campaign.createCampaign(driver);

			fc.utobj().printTestStep("Click on Associate with Locations");
			fc.utobj().actionImgOption(driver, campaignTitle, "Associate with Locations");

			fc.utobj().printTestStep("Filter location");
			campaign.filterLocation(driver, franchiseeID);

			fc.utobj().printTestStep("Associate with campaign");
			campaign.associateWithCampaign(driver, campaignTitle);

			// fc.utobj().readMailBox(VistaTeat, expectedMessageBody, userName,
			// password);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_InfoMgr_CampaignCenter_ChooseCampaign_AssociateLocation", testCaseDescription = "This test will choose a campaign and associate that campaign to the location", reference = {
			"" })
	public void TC_InfoMgr_CampaignCenter_ChooseCampaign_AssociateLocation() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		CampaignCenterCommonMethods campaign = fc.infomgr().campaignCenter();
		AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();
		InfoMgrCampaignCenterPage campaignCenterPage = new InfoMgrCampaignCenterPage(driver);
		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Add a franchise location");
			String franchiseeID = adminInfoMgr.addFranchiseLocation(driver);
			fc.infomgr().infomgr_common().InfoMgrCampaignCenter(driver);
			fc.utobj().printTestStep("Create a campaign");
			String campaignTitle = campaign.createCampaign(driver);

			fc.utobj().printTestStep("Choose campaign");
			fc.infomgr().infomgr_common().InfoMgrCampaignCenter(driver);
			fc.utobj().clickElement(driver, campaignCenterPage.btnChooseCampaign);
			campaign.chooseCampaign(driver, campaignTitle);

			fc.utobj().printTestStep("Filter location");
			campaign.filterLocation(driver, franchiseeID);

			fc.utobj().printTestStep("Associate with Locations");
			campaign.associateWithCampaign(driver, campaignTitle);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_InfoMgr_CampaignCenter_FilterLocation", testCaseDescription = "This test will filter a location first  and then associate the campaign", reference = {
			"" })
	public void TC_InfoMgr_CampaignCenter_FilterLocation() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		CampaignCenterCommonMethods campaign = fc.infomgr().campaignCenter();
		AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();
		InfoMgrCampaignCenterPage campaignCenterPage = new InfoMgrCampaignCenterPage(driver);
		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Add a franchise location");
			String franchiseeID = adminInfoMgr.addFranchiseLocation(driver);
			fc.infomgr().infomgr_common().InfoMgrCampaignCenter(driver);
			fc.utobj().printTestStep("Create a campaign");
			String campaignTitle = campaign.createCampaign(driver);

			fc.utobj().printTestStep("Filter location");
			fc.infomgr().infomgr_common().InfoMgrCampaignCenter(driver);
			fc.utobj().clickElement(driver, campaignCenterPage.btnFilterLocation);
			campaign.filterLocation(driver, franchiseeID);

			fc.utobj().printTestStep("Choose Campaign");
			campaign.chooseCampaign(driver, campaignTitle);

			fc.utobj().printTestStep("Associate with location");
			campaign.associateWithCampaign(driver, campaignTitle);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_InfoMgr_CampaignCenter_AddEmailTemplates", testCaseDescription = "This test will add both graphical and text email template", reference = {
			"" })
	public void TC_InfoMgr_CampaignCenter_AddGraphicalEmailTemplate() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		CampaignCenterCommonMethods campaign = fc.infomgr().campaignCenter();
		try {
			driver = fc.loginpage().login(driver);
			campaign.gotoCampaignCenterEmailTemplate(driver);
			fc.utobj().printTestStep("Add a graphical email template");
			// campaign.addGraphicalEmailTemplate(driver, config);
			fc.utobj().printTestStep("Add a text email template");
			campaign.addTextEmailTemplate(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_InfoMgr_CampaignCenter_CreateCampaign_With_Existing_Templates", testCaseDescription = "This test will add a campaign and associate that campaign to the location", reference = {
			"" })
	public void TC_InfoMgr_CampaignCenter_CreateCampaign_With_Existing_Templates() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		CampaignCenterCommonMethods campaign = fc.infomgr().campaignCenter();
		ArrayList<String> lstEmailTemplates = new ArrayList<String>();
		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Add Two Text Email Templates");
			campaign.gotoCampaignCenterEmailTemplate(driver);
			String emailTemplate1 = campaign.addTextEmailTemplate(driver);
			String emailTemplate2 = campaign.addTextEmailTemplate(driver);
			lstEmailTemplates.add(emailTemplate1);
			lstEmailTemplates.add(emailTemplate2);

			fc.infomgr().infomgr_common().InfoMgrCampaignCenter(driver);
			fc.utobj().printTestStep("Create a campaign");
			campaign.createCampaignWithExistingTemplate(driver, lstEmailTemplates);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2018-09-14", updatedOn = "2018-10-10", testCaseId = "TC_InfoMgr_CampaignCenter_AddandAssociateGraphicalCampaign", testCaseDescription = "This test will add a Graphical Campaign and associate that campaign to the location", reference = {
			"" })
	public void TC_InfoMgr_CampaignCenter_AddandAssociate_GraphicalCampaign() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		CampaignCenterCommonMethods campaign = fc.infomgr().campaignCenter();
		AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();
		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Add a franchise location");

			String franchiseID = adminInfoMgr.addFranchiseLocationForInfoMgrName(driver, false);
			fc.infomgr().infomgr_common().InfoMgrCampaignCenter(driver);
			fc.utobj().printTestStep("Create a Graphical Campaign");
			String campaignTitle = campaign.createGraphicalCampaign(driver);

			fc.utobj().printTestStep("Click on Associate with Locations");
			fc.utobj().actionImgOption(driver, campaignTitle, "Associate with Locations");

			fc.utobj().printTestStep("Filter location");
			campaign.filterLocation(driver, franchiseID);

			fc.utobj().printTestStep("Associate with campaign");
			campaign.associateWithCampaign(driver, campaignTitle);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2018-09-18", updatedOn = "2018-10-10", testCaseId = "TC_InfoMgr_CampaignCenter_AddandAssociateHTMLCampaign", testCaseDescription = "This test will add a HTML campaign and associate with the Active location", reference = {
			"" })
	public void TC_InfoMgr_CampaignCenter_AddandAssociate_HTMLCampaign() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		CampaignCenterCommonMethods campaign = fc.infomgr().campaignCenter();
		AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();
		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Add a franchise location");

			String franchiseID = adminInfoMgr.addFranchiseLocationForInfoMgrName(driver, false);
			fc.infomgr().infomgr_common().InfoMgrCampaignCenter(driver);
			fc.utobj().printTestStep("Create a Graphical Campaign");
			String campaignTitle = campaign.createHTMLCampaign(driver);

			fc.utobj().printTestStep("Click on Associate with Locations");
			fc.utobj().actionImgOption(driver, campaignTitle, "Associate with Locations");

			fc.utobj().printTestStep("Filter location");
			campaign.filterLocation(driver, franchiseID);

			fc.utobj().printTestStep("Associate with campaign");
			campaign.associateWithCampaign(driver, campaignTitle);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2018-10-11", updatedOn = "2018-10-11", testCaseId = "TC_InfoMgr_CampaignCenter_AddandAssociateHTMLCampaign_Indevelopment", testCaseDescription = "This test will add a HTML campaign and associate with Indevelopment location", reference = {
			"" })
	public void TC_InfoMgr_CampaignCenter_AddandAssociate_HTMLCampaign1() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		CampaignCenterCommonMethods campaign = fc.infomgr().campaignCenter();
		AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();
		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Add a Indevelopment location");

			String franchiseID = adminInfoMgr.addInDevelopmentFranchiseLocationForInfoMgrName(driver, false);
			fc.infomgr().infomgr_common().InfoMgrCampaignCenter(driver);
			fc.utobj().printTestStep("Create a Graphical Campaign");
			String campaignTitle = campaign.createHTMLCampaign(driver);

			fc.utobj().printTestStep("Click on Associate with Locations");
			fc.utobj().actionImgOption(driver, campaignTitle, "Associate with Locations");

			fc.utobj().printTestStep("Filter location");
			campaign.filterLocation(driver, franchiseID);

			fc.utobj().printTestStep("Associate with campaign");
			campaign.associateWithCampaign(driver, campaignTitle);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_InfoMgr_CampaignCenter_CreateCampaign_With_Existing_Graphical_HTML_Plain_Templates", testCaseDescription = "This test will add a Graphical, Plain, and HTML Email Template and create campaign and associate that campaign to the location", reference = {
			"" })
	public void TC_InfoMgr_CampaignCenter_CreateCampaign_With_Existing_Graphical_HTML_Plain_Templates()
			throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		CampaignCenterCommonMethods campaign = fc.infomgr().campaignCenter();
		ArrayList<String> lstEmailTemplates = new ArrayList<String>();
		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Add a franchise location");
			String franchiseID = adminInfoMgr.addFranchiseLocationForInfoMgrName(driver, false);

			fc.utobj().printTestStep("Add Two Text Email Templates");
			campaign.gotoCampaignCenterEmailTemplate(driver);
			String emailTemplate1 = campaign.addTextEmailTemplate(driver);
			lstEmailTemplates.add(emailTemplate1);

			fc.utobj().printTestStep("Add a Graphical Email Templates");
			campaign.gotoCampaignCenterEmailTemplate(driver);
			String emailTemplate2 = campaign.addGraphicalEmailTemplate(driver);
			lstEmailTemplates.add(emailTemplate2);

			fc.utobj().printTestStep("Add a Graphical Email Templates");
			campaign.gotoCampaignCenterEmailTemplate(driver);
			String emailTemplate3 = campaign.addHTMLEmailTemplate(driver);
			lstEmailTemplates.add(emailTemplate3);

			fc.infomgr().infomgr_common().InfoMgrCampaignCenter(driver);
			fc.utobj().printTestStep("Create a campaign");
			String campaignTitle = campaign.createCampaignWithallExistingTemplate(driver, lstEmailTemplates);

			fc.utobj().printTestStep("Click on Associate with Locations");
			fc.utobj().actionImgOption(driver, campaignTitle, "Associate with Locations");

			fc.utobj().printTestStep("Filter location");
			campaign.filterLocation(driver, franchiseID);

			fc.utobj().printTestStep("Associate with campaign");
			campaign.associateWithCampaign(driver, campaignTitle);

			// fc.utobj().readMailBox("Campaigntest", expectedMessageBody,
			// userName, password);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2018-10-18", updatedOn = "2018-10-18", testCaseId = "TC_InfoMgr_CampaignCenter_AddandAssociateGraphicalCampaign_Regular", testCaseDescription = "This test will add a Graphical Regular Campaign and associate that campaign to the location", reference = {
			"" })
	public void TC_InfoMgr_CampaignCenter_AddandAssociate_GraphicalCampaign_Regular() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		CampaignCenterCommonMethods campaign = fc.infomgr().campaignCenter();
		AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();
		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Add a franchise location");
			String franchiseID = adminInfoMgr.addFranchiseLocationForInfoMgrName(driver, false);

			fc.infomgr().infomgr_common().InfoMgrCampaignCenter(driver);
			fc.utobj().printTestStep("Create a Graphical Campaign");
			String campaignTitle = campaign.createGraphicalRegularCampaign(driver);

			fc.utobj().printTestStep("Click on Associate with Locations");
			fc.utobj().actionImgOption(driver, campaignTitle, "Associate with Locations");

			fc.utobj().printTestStep("Filter location");
			campaign.filterLocation(driver, franchiseID);

			fc.utobj().printTestStep("Associate with campaign");
			campaign.associateWithCampaign(driver, campaignTitle);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			// e.printStackTrace();
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2018-11-13", updatedOn = "2018-11-15", testCaseId = "TC_InfoMgr_CampaignCenter_Copy_Modify_EmailTemplate", testCaseDescription = "This test will add an Email Template, Copy Template and send that copied template", reference = {
			"" })
	public void TC_InfoMgr_CampaignCenter_Center_Copy_Test_EmailTemplate() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		CampaignCenterCommonMethods campaign = fc.infomgr().campaignCenter();
		InfoMgrCampaignCenterEmailTemplatesPage emailtemplates = new InfoMgrCampaignCenterEmailTemplatesPage(driver);

		ArrayList<String> lstEmailTemplates = new ArrayList<String>();
		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Add Text Email Templates");
			campaign.gotoCampaignCenterEmailTemplate(driver);
			List<String> listEmail = campaign.CopyAndTestEmailTemplate(driver);

			fc.utobj().printTestStep("Add a franchise location");
			String franchiseID = adminInfoMgr.addFranchiseLocationForInfoMgrName(driver, false);

			imcm.SearchFranchiseeLocationAndClick(driver, franchiseID);
			fc.utobj().clickElement(driver, emailtemplates.Sendmailbtn);

			fc.utobj().selectDropDownByVisibleText(driver, emailtemplates.emaildropdown, listEmail.get(0));
			fc.utobj().clickElement(driver, emailtemplates.sendemailbtn);

			fc.utobj().readMailBox(listEmail.get(1), listEmail.get(2), "infomgrautomation@staffex.com", "sdg@1a@Hfs");

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2018-11-16", updatedOn = "2018-11-16", testCaseId = "TC_InfoMgr_CampaignCenter_Add_Modify_EmailTemplate", testCaseDescription = "This test will add an Email Template, Modify Template and send template", reference = {
			"" })
	public void TC_InfoMgr_CampaignCenter_Center_Add_Modify_EmailTemplate() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		CampaignCenterCommonMethods campaign = fc.infomgr().campaignCenter();
		InfoMgrCampaignCenterEmailTemplatesPage emailtemplates = new InfoMgrCampaignCenterEmailTemplatesPage(driver);

		ArrayList<String> lstEmailTemplates = new ArrayList<String>();
		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Add Text Email Templates");
			campaign.gotoCampaignCenterEmailTemplate(driver);
			List<String> listEmail = campaign.AddAndModifyEmailTemplate(driver);

			//Add Franchise location
			fc.utobj().printTestStep("Add a franchise location");
			String franchiseID = adminInfoMgr.addFranchiseLocationForInfoMgrName(driver, false);

			imcm.SearchFranchiseeLocationAndClick(driver, franchiseID);
			fc.utobj().clickElement(driver, emailtemplates.Sendmailbtn);

			fc.utobj().selectDropDownByVisibleText(driver, emailtemplates.emaildropdown, listEmail.get(0));

			fc.utobj().clickElement(driver, emailtemplates.sendemailbtn);

			fc.utobj().readMailBox(listEmail.get(1), listEmail.get(2), "infomgrautomation@staffex.com", "sdg@1a@Hfs");

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2018-11-13", updatedOn = "2018-11-15", testCaseId = "TC_InfoMgr_CampaignCenter_Add_Copy_Delete_EmailTemplate", testCaseDescription = "This test will add an Email Template, Copy Template and delete that template", reference = {
			"" })
	public void TC_InfoMgr_CampaignCenter_Center_Add_Modify_Delete_EmailTemplate() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		CampaignCenterCommonMethods campaign = fc.infomgr().campaignCenter();
		InfoMgrCampaignCenterEmailTemplatesPage emailtemplates = new InfoMgrCampaignCenterEmailTemplatesPage(driver);

		ArrayList<String> lstEmailTemplates = new ArrayList<String>();
		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Add Text Email Templates");
			campaign.gotoCampaignCenterEmailTemplate(driver);
			List<String> listEmail = campaign.AddCopyDeleteTestEmailTemplate(driver);


			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
}
