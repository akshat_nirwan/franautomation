package com.builds.test.infomgr;

import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.annotations.BeforeGroups;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.common.CorporateUser;
import com.builds.uimaps.infomgr.InfoMgrCenterInfoPage;
import com.builds.uimaps.infomgr.InfoMgrContactHistoryPage;
import com.builds.uimaps.infomgr.InfoMgrCorporateLocationsPage;
import com.builds.uimaps.infomgr.InfoMgrFranchiseesPage;
import com.builds.uimaps.infomgr.InfoMgrOwnersPage;
import com.builds.uimaps.infomgr.InfoMgrRegionalPage;
import com.builds.uimaps.infomgr.InfoMgrUsersPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class InfoMgrCorporateLocationsPageTest {

	FranconnectUtil fc = new FranconnectUtil();
	public static String flag = "true";
	public static final String franchiseID = "CorpLocation" + ThreadLocalRandom.current().nextInt(10000);
	InfoMgr_Common imc = new InfoMgr_Common();


	@BeforeGroups(groups = { "infomgr" ,"CorpLocation__Tabs_AMD","Contract"})
	public synchronized void createFranchise() throws Exception {		
		if (flag == "true") {
			flag = "false";
			String testCaseId = "BeforeGroupBlock to create Common Corporate Location";
			FranchiseeAPI fran = new FranchiseeAPI();
			WebDriver driver = fc.commonMethods().browsers().openBrowser();
			fc.loginpage().login(driver);
			System.out.println("inside before group");
			AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();
			
			fc.utobj().printTestStep("Add Area / Region ");
			String areaRegionID=adminInfoMgr.addAreaRegion(driver);
			Reporter.log("Area Region - " + areaRegionID + "created");
			System.out.println("Area Region created : " + areaRegionID);
			fran.createDefaultFranchiseeAPI(areaRegionID, "Yes", franchiseID);
			System.out.println("Corporate Location created : " + franchiseID);
			Reporter.log(" Corporate Location is Created" + franchiseID);
			fc.utobj().printTestStep("Add corporate user");									 
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		}
	}

	@Test(groups = {  "infomgr" ,"CorpLocation__Tabs_AMD" })
	// BugID - 69046 - Login with corporate user
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_CorporateLocation_AgreementTab_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of Agreement tab of Info Mgr > Corporate  Locations.", reference = { "" })
	public void InfoMgr_CorporateLocation_AgreementTab_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		CorporateLocationsCommonMethods corporateLocations = fc.infomgr().corporateLocations();

		try {
			fc.loginpage().login(driver);
			InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();
			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");
			corporateLocations.searchFranchiseAndClick(driver, franchiseID);
			fc.utobj().printTestStep("Add Agreement Tab Data");
			// Add Agreement
			fc.utobj().clickElement(driver, centerInfoPage.lnkAgreement);
			driver = corporateLocations.addAgreement(driver, config);
			// Modify Agreement
			fc.utobj().clickElement(driver, centerInfoPage.lnkAgreement);
			driver = corporateLocations.modifyAgreement(driver, config);
			// Delete Agreement
			corporateLocations.deleteTabData(driver, config, centerInfoPage.lnkAgreement, null);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" ,"CorpLocation__Tabs_AMD","Testddd"})

	// BugID - 69046 - Login with corporate user
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_CorporateLocation_AddressTab_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of Address tab of Info Mgr > Corporate  Locations.", reference = { "" })
	public void InfoMgr_CorporateLocation_AddressTab_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		CorporateLocationsCommonMethods corporateLocations = fc.infomgr().corporateLocations();
		try {
			fc.loginpage().login(driver);
			InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();
			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");
			corporateLocations.searchFranchiseAndClick(driver, franchiseID);

			// Add Address
			fc.utobj().printTestStep("Add Address Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkAddresses);
			driver = corporateLocations.addAddresses(driver, config);
			// Modify Address
			fc.utobj().printTestStep("Modify Address Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkAddresses);
			driver = corporateLocations.modifyAddresses(driver, config);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {  "infomgr" ,"CorpLocation__Tabs_AMD" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_CorporateLocation_ContactHistoryTab_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of ContactHistoryTab of Info Mgr > Corporate  Locations.", reference = { "" })
	public void InfoMgr_CorporateLocation_ContactHistoryTab_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		CorporateLocationsCommonMethods corporateLocations = fc.infomgr().corporateLocations();
		InfoMgrFranchiseesPage franchiseesPage = new InfoMgrFranchiseesPage(driver);

		try {
			System.out.println("inside InfoMgr_CorporateLocation_ContactHistoryTab_Add_Modify_Delete");
			System.out.println(franchiseID);
			fc.loginpage().login(driver);	
			InfoMgrContactHistoryPage contactHistoryPage = franchiseesPage.getContactHistoryPage();
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail("infomgrautomation@staffex.com");
			corpUser = corpTest.createDefaultUser(driver, corpUser);
			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");
			corporateLocations.searchFranchiseAndClick(driver, franchiseID);
			
			// Log a Task
						fc.utobj().clickElement(driver, contactHistoryPage.lnkContactHistory);

						fc.utobj().clickElement(driver, contactHistoryPage.btnLogaTask);
						String taskSubject = corporateLocations.logaTask(driver, config, corpUser.getuserFullName(), true);

						// Log a Call
						fc.utobj().clickElement(driver, contactHistoryPage.btnLogaCall);
						String callSubject = corporateLocations.logACall(driver, config, true);

						// Add Remark
						fc.utobj().clickElement(driver, contactHistoryPage.btnAddRemark);
						String remarkSubject = corporateLocations.addRemark(driver, config, contactHistoryPage);

			// Modify Contact History

			// Modify a task
			fc.utobj().clickElement(driver, contactHistoryPage.lnkContactHistory);
			fc.utobj().actionImgOption(driver, taskSubject, "Modify");
			corporateLocations.modifyATask(driver, config, true);

			// Modify a call
			fc.utobj().actionImgOption(driver, callSubject, "Modify");
			corporateLocations.modifyACall(driver, config, true);

			// Modify a remark
			fc.utobj().actionImgOption(driver, remarkSubject, "Modify");
			corporateLocations.modifyRemark(driver, config, contactHistoryPage);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" ,"CorpLocation__Tabs_AMD" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_CorporateLocation_ContractSigning_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of ContractSigning tabs of Info Mgr > Corporate  Locations.", reference = { "" })
	public void InfoMgr_CorporateLocation_ContractSigningTab_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		CorporateLocationsCommonMethods corporateLocations = fc.infomgr().corporateLocations();
		try {
			fc.loginpage().login(driver);	
			// adminInfoMgr.configureInfoMgr(driver, config);
			InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();
			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");
			corporateLocations.searchFranchiseAndClick(driver, franchiseID);
			// Adding Contract Signing
			fc.utobj().printTestStep("Add Contract History Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkContractSigning);
			corporateLocations.addContractSigning(driver, config);
			// Modify Contract Signing
			fc.utobj().printTestStep("Modify Contract History Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkContractSigning);
			corporateLocations.modifyContractSigning(driver, config);
			// Delete Contract Signing
			fc.utobj().printTestStep("Delete Contract History Tab Data");
			corporateLocations.deleteTabData(driver, config, centerInfoPage.lnkContractSigning, null);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" ,"CorpLocation__Tabs_AMD" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_CorporateLocation_CustomerComplaint_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of CustomerComplaint tabs of Info Mgr > Corporate  Locations.", reference = { "" })
	public void InfoMgr_CorporateLocation_CustomerComplaintTab_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		CorporateLocationsCommonMethods corporateLocations = fc.infomgr().corporateLocations();
		try {

			fc.loginpage().login(driver);
			// adminInfoMgr.configureInfoMgr(driver, config);
			InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();
			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");
			corporateLocations.searchFranchiseAndClick(driver, franchiseID);

			// Add Customer Complaints
			fc.utobj().printTestStep("Add Customer Complaints Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkCustomerComplaints);
			corporateLocations.addCustomerComplaints(driver, config);

			// Modify Customer Complaints
			fc.utobj().printTestStep("Modify Customer Complaints Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkCustomerComplaints);
			corporateLocations.modifyCustomerComplaints(driver, config);
			// Delete
			fc.utobj().printTestStep("Delete Customer Complaints Tab Data");
			corporateLocations.deleteTabData(driver, config, centerInfoPage.lnkCustomerComplaints, null);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {  "infomgr" ,"CorpLocation__Tabs_AMD" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_CorporateLocation_CenterInfo_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of _CenterInfo  tabs of Info Mgr > Corporate  Locations.", reference = { "" })
	public void InfoMgr_CorporateLocation_CenterInfo_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		CorporateLocationsCommonMethods corporateLocations = fc.infomgr().corporateLocations();
		InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();
		try {
			fc.loginpage().login(driver);
			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");
			corporateLocations.searchFranchiseAndClick(driver, franchiseID);
			// Modify Center Info
			fc.utobj().printTestStep("Modify Center Info Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkCenterInfo);
			fc.utobj().clickLink(driver, "Modify");
			corporateLocations.modifyCenterInfo(driver, config);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {  "infomgr" ,"CorpLocation__Tabs_AMD" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_CorporateLocation_Documents_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of Documents tabs of Info Mgr > Corporate  Locations.", reference = { "" })
	public void InfoMgr_CorporateLocation_Documents_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		CorporateLocationsCommonMethods corporateLocations = fc.infomgr().corporateLocations();
		InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();
		try {
			fc.loginpage().login(driver);
			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");		
			corporateLocations.searchFranchiseAndClick(driver, franchiseID);
			// Add Documents
			fc.utobj().printTestStep("Add  Documents Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkDocuments);
			String docTitle = corporateLocations.addDocument(driver, config);

			// Modify Documents
			fc.utobj().printTestStep("Modify  Documents Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkDocuments);
			fc.utobj().actionImgOption(driver, docTitle, "Modify");
			docTitle = corporateLocations.modifyDocument(driver, config);
			// Delete
			fc.utobj().printTestStep("Delete  Documents Tab Data");
			corporateLocations.deleteTabData(driver, config, centerInfoPage.lnkDocuments, docTitle);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {  "infomgr" ,"CorpLocation__Tabs_AMD" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_CorporateLocation_Employees_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of Employee tabs of Info Mgr > Corporate  Locations.", reference = { "" })
	public void InfoMgr_CorporateLocation_Employees_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		CorporateLocationsCommonMethods corporateLocations = fc.infomgr().corporateLocations();
		InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();
		try {
			fc.loginpage().login(driver);
			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");
			corporateLocations.searchFranchiseAndClick(driver, franchiseID);
			// Add Employees
			fc.utobj().printTestStep("Add  Employees Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkEmployees);
			String empName = corporateLocations.addEmployee(driver, config);
			// Modify Employee
			fc.utobj().printTestStep("Modify  Employees Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkEmployees);
			// fc.utobj().actionImgOption(driver, empName, "Modify");
			empName = corporateLocations.modifyEmployee(driver, config);
			fc.utobj().printTestStep("Delete  Employees Tab Data");
			corporateLocations.deleteTabData(driver, config, centerInfoPage.lnkEmployees, null);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" ,"CorpLocation__Tabs_AMD" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_CorporateLocation_Guarantor_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of Guarantor tab of Info Mgr > Corporate  Locations.", reference = { "" })
	public void InfoMgr_CorporateLocation_Guarantor_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		CorporateLocationsCommonMethods corporateLocations = fc.infomgr().corporateLocations();
		InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();
		try {

			fc.loginpage().login(driver);
			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");
			corporateLocations.searchFranchiseAndClick(driver, franchiseID);
			// Add Gurantor details
			fc.utobj().printTestStep("Add  Gurantor Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkGuarantors);
			corporateLocations.addGuarantor(driver, config);
			// Modify Guarantor
			fc.utobj().printTestStep("Modify  Gurantor Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkGuarantors);
			corporateLocations.modifyGuarantor(driver, config);
			// Delete Entity
			fc.utobj().printTestStep("Delete  Gurantor Tab Data");
			corporateLocations.deleteTabData(driver, config, centerInfoPage.guarantorsTab, null);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" ,"CorpLocation__Tabs_AMD" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_CorporateLocation_Entity_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of Entity Tab of Info Mgr > Corporate  Locations.", reference = { "" })
	public void InfoMgr_CorporateLocation_Entity_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		CorporateLocationsCommonMethods corporateLocations = fc.infomgr().corporateLocations();
		InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();
		try {
			fc.loginpage().login(driver);
			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");
			corporateLocations.searchFranchiseAndClick(driver, franchiseID);
			// Add
			fc.utobj().printTestStep("Add  Entity Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.linkEntityDetail);
			corporateLocations.addEntityDetails(driver, config);
			// Modify Entity Details
			fc.utobj().printTestStep("Modify  Entity Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.linkEntityDetail);
			corporateLocations.modifyEntityDetails(driver, config);
			// Delete Entity
			fc.utobj().printTestStep("Delete  Entity Tab Data");
			corporateLocations.deleteTabData(driver, config, centerInfoPage.linkEntityDetail, null);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {  "infomgr" ,"CorpLocation__Tabs_AMD" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_CorporateLocation_Marketing_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of Marketing Tab of Info Mgr > Corporate  Locations.", reference = { "" })
	public void InfoMgr_CorporateLocation_Marketing_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			CorporateLocationsCommonMethods corporateLocations = fc.infomgr().corporateLocations();
			InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();
			fc.loginpage().login(driver);
			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");
			corporateLocations.searchFranchiseAndClick(driver, franchiseID);

			// Add Marketing details
			fc.utobj().printTestStep("Add  Marketing Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkMarketingFinTab);
			corporateLocations.addMarketingDetails(driver, config);
			// Modify Marketing
			fc.utobj().printTestStep("Modify  Marketing Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkMarketingFinTab);
			corporateLocations.modifyMarketingDetails(driver, config);
			// Delete marketing
			fc.utobj().printTestStep("Delete  Marketing Tab Data");
			corporateLocations.deleteTabData(driver, config, centerInfoPage.marketingTab, null);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {  "infomgr" ,"CorpLocation__Tabs_AMD"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_CorporateLocation_Insurance_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of Insurance tabs of Info Mgr > Corporate  Locations.", reference = { "" })
	public void InfoMgr_CorporateLocation_Insurance_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		CorporateLocationsCommonMethods corporateLocations = fc.infomgr().corporateLocations();

		InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();
		try {

			fc.loginpage().login(driver);

			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");
			corporateLocations.searchFranchiseAndClick(driver, franchiseID);
			// Add Insurance Details
			fc.utobj().printTestStep("Add  Insurance Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkInsurance);
			corporateLocations.addInsuranceDetails(driver, config);

			// Modify Insurance
			fc.utobj().printTestStep("Modify  Insurance Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkInsurance);
			corporateLocations.modifyInsuranceDetails(driver, config);

			// Delete Insurance
			fc.utobj().printTestStep("Delete  Insurance Tab Data");
			corporateLocations.deleteTabData(driver, config, centerInfoPage.insuranceTab, null);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {  "infomgr" ,"CorpLocation__Tabs_AMD"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_CorporateLocation_LegalViolation_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of LegalViolation tabs of Info Mgr > Corporate  Locations.", reference = { "" })
	public void InfoMgr_CorporateLocation_LegalViolation_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		CorporateLocationsCommonMethods corporateLocations = fc.infomgr().corporateLocations();

		InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();
		try {

			fc.loginpage().login(driver);
			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");
			corporateLocations.searchFranchiseAndClick(driver, franchiseID);
			// Add legal violation
			fc.utobj().printTestStep("Add  Legal Violation Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkLegalViolation);
			corporateLocations.addLegalViolation(driver, config);
			// Modify Legal Violation
			fc.utobj().printTestStep("Modify  Legal Violation Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkLegalViolation);
			corporateLocations.modifyLegalViolation(driver, config);
			// Delete Legal Violation
			fc.utobj().printTestStep("Delete  Legal Violation Tab Data");
			corporateLocations.deleteTabData(driver, config, centerInfoPage.lnkLegalViolation, null);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" ,"CorpLocation__Tabs_AMD" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_CorporateLocation_Lenders_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of Lenders tabs of Info Mgr > Corporate  Locations.", reference = { "" })
	public void InfoMgr_CorporateLocation_Lenders_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		CorporateLocationsCommonMethods corporateLocations = fc.infomgr().corporateLocations();
		InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();
		try {

			fc.loginpage().login(driver);
			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");
			corporateLocations.searchFranchiseAndClick(driver, franchiseID);

			// Add Lenders
			fc.utobj().printTestStep("Add  Lenders Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkLenders);
			corporateLocations.addLenders(driver, config);
			// Modify Lenders Details
			fc.utobj().printTestStep("Modify  Lenders Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkLenders);
			corporateLocations.modifyLenders(driver, config);

			// Delete Lenders
			fc.utobj().printTestStep("Delete  Lenders Tab Data");
			corporateLocations.deleteTabData(driver, config, centerInfoPage.lnkLenders, null);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" ,"CorpLocation__Tabs_AMD" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_CorporateLocation_Finance_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of Finance of Info Mgr > Corporate  Locations.", reference = { "" })
	public void InfoMgr_CorporateLocation_Finance_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		CorporateLocationsCommonMethods corporateLocations = fc.infomgr().corporateLocations();
		InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();
		try {

			fc.loginpage().login(driver);

			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");
			corporateLocations.searchFranchiseAndClick(driver, franchiseID);

			// Add Finance Details
			fc.utobj().printTestStep("Add  Finance Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkFinancial);
			corporateLocations.addFinanceDetails(driver, config);

			// Modify Financials
			fc.utobj().printTestStep("Modify  Finance Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkFinancial);
			corporateLocations.modifyFinanceDetails(driver, config);

			// Delete Financials
			fc.utobj().printTestStep("Delete  Finance Tab Data");
			corporateLocations.deleteTabData(driver, config, centerInfoPage.lnkFinancial, null);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {  "infomgr" ,"CorpLocation__Tabs_AMD" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_CorporateLocation_MysteryReview_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of MysteryReview Tab of Info Mgr > Corporate  Locations.", reference = { "" })
	public void InfoMgr_CorporateLocation_MysteryReview_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		CorporateLocationsCommonMethods corporateLocations = fc.infomgr().corporateLocations();
		InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();
		try {
			fc.loginpage().login(driver);

			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");
			corporateLocations.searchFranchiseAndClick(driver, franchiseID);

			// Add Mystry Review
			fc.utobj().printTestStep("Add  MystryReview Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkMysteryReview);
			corporateLocations.addMystryReview(driver, config);
			// Modify Mystry Review
			fc.utobj().printTestStep("Modify  MystryReview Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkMysteryReview);
			corporateLocations.modifyMystryReview(driver, config);
			// Delete MysteryReview
			fc.utobj().printTestStep("Delete  MystryReview Tab Data");
			corporateLocations.deleteTabData(driver, config, centerInfoPage.lnkMysteryReview, null);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" ,"CorpLocation__Tabs_AMD"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_CorporateLocation_Picture_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of Picture tab of Info Mgr > Corporate  Locations.", reference = { "" })
	public void InfoMgr_CorporateLocation_Picture_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		CorporateLocationsCommonMethods corporateLocations = fc.infomgr().corporateLocations();
		InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();
		try {
			fc.loginpage().login(driver);
			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");
			corporateLocations.searchFranchiseAndClick(driver, franchiseID);

			// Add Picture
			fc.utobj().printTestStep("Add  Picture Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkPictures);
			String pictureTitle = corporateLocations.addPicture(driver, config);
			// Modify Pictures
			fc.utobj().printTestStep("Modify  Picture Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkPictures);
			fc.utobj().actionImgOption(driver, pictureTitle, "Modify");
			pictureTitle = corporateLocations.modifyPicture(driver, config);

			// Delete Pictures
			fc.utobj().printTestStep("Delete  Picture Tab Data");
			corporateLocations.deleteTabData(driver, config, centerInfoPage.lnkPictures, pictureTitle);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {  "infomgr" ,"CorpLocation__Tabs_AMD" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_CorporateLocation_OwnerAndUsers_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of Owner of Info Mgr > Corporate  Locations.", reference = { "" })
	public void InfoMgr_CorporateLocation_OwnerAndUsers_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		CorporateLocationsCommonMethods corporateLocations = fc.infomgr().corporateLocations();
		InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();
		try {
			fc.loginpage().login(driver);

			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");
			corporateLocations.searchFranchiseAndClick(driver, franchiseID);
			// Add Owner
			fc.utobj().printTestStep("Add  Owner Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.tabOwners);
			Map<String, String> owner = corporateLocations.addOwner(driver,config);
			// Add User
			fc.utobj().printTestStep("Add  Users Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkUsers);
			InfoMgrUsersPage usersPage = new InfoMgrCorporateLocationsPage(driver).getUsersPage();
			fc.utobj().clickElement(driver, usersPage.btnAddUser);
			String userName = corporateLocations.addOwnerUser(driver, config,
					owner.get("FirstName") + " " + owner.get("LastName"));
			// Modify Owners
			fc.utobj().printTestStep("Modify  Owner Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.tabOwners);
			fc.utobj().actionImgOption(driver, owner.get("FirstName"), "Modify");
			corporateLocations.modifyOwner(driver, config);
			// Modify Users
			fc.utobj().printTestStep("Modify  Users Tab Data");
			// Modify Users
						fc.utobj().clickElement(driver, centerInfoPage.lnkUsers);
						fc.utobj().actionImgOption(driver, owner.get("FirstName") + " " + owner.get("LastName"), "Modify");
						corporateLocations.modifyOwnerUser(driver, config);
			// Delete Owner
			fc.utobj().printTestStep("Delete  Owner Tab Data");
			corporateLocations.deleteTabData(driver, config, centerInfoPage.ownersTab, owner.get("FirstName"));
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" ,"CorpLocation__Tabs_AMD" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_CorporateLocation_Realestate_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of Realestate of Info Mgr > Corporate  Locations.", reference = { "" })
	public void InfoMgr_CorporateLocation_Realestate_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		CorporateLocationsCommonMethods corporateLocations = fc.infomgr().corporateLocations();
		InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();
		try {
			fc.loginpage().login(driver);
			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");
			corporateLocations.searchFranchiseAndClick(driver, franchiseID);
			// Add Real Estate
			fc.utobj().printTestStep("Add  RealEstate Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkRealEstate);
			corporateLocations.addRealState(driver, config);
			// Modify Real state
			fc.utobj().printTestStep("Modify  RealEstate Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkRealEstate);
			corporateLocations.modifyRealState(driver, config);
			// Delete RealEstate
			fc.utobj().printTestStep("Delete  RealEstate Tab Data");
			corporateLocations.deleteTabData(driver, config, centerInfoPage.lnkRealEstate, null);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {  "infomgr" ,"CorpLocation__Tabs_AMD","Testddd"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_CorporateLocation_Territory_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of Territory Tab of Info Mgr > Corporate  Locations.", reference = { "" })
	public void InfoMgr_CorporateLocation_Territory_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		CorporateLocationsCommonMethods corporateLocations = fc.infomgr().corporateLocations();

		InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();
		try {
			fc.loginpage().login(driver);
			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");
			corporateLocations.searchFranchiseAndClick(driver, franchiseID);
			// Add Territory
			fc.utobj().printTestStep("Add  Territory Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkTerritory);
			corporateLocations.addTerritory(driver, config);
			// Modify Territory
			fc.utobj().printTestStep("Modify  Territory Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkTerritory);
			corporateLocations.modifyTerritory(driver, config);
			// Delete Territory
			fc.utobj().printTestStep("delete  Territory Tab Data");
			corporateLocations.deleteTabData(driver, config, centerInfoPage.lnkTerritory, null);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {  "infomgr" ,"CorpLocation__Tabs_AMD"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_CorporateLocation_Training_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of Training Tab of Info Mgr > Corporate  Locations.", reference = { "" })
	public void InfoMgr_CorporateLocation_Training_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		CorporateLocationsCommonMethods corporateLocations = fc.infomgr().corporateLocations();
		InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();
		try {
			fc.loginpage().login(driver);
			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");
			corporateLocations.searchFranchiseAndClick(driver, franchiseID);
			// Add Training
			fc.utobj().printTestStep("Add  Training Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkTraining);
			corporateLocations.addTraining(driver, config);
			// Modify Training
			fc.utobj().printTestStep("Modify  Training Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkTraining);
			corporateLocations.modifyTraining(driver, config);
			// Delete Training
			fc.utobj().printTestStep("Delete  Training Tab Data");
			corporateLocations.deleteTabData(driver, config, centerInfoPage.lnkTraining, null);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/*@Test(groups = { "infomgr1","IM_ALL_Tabs" })

	// BugID - 69046 - Login with corporate user
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_CorporateLocations_AllTab_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of all tabs of Info Mgr > corporateLocations.", reference = {
			"" })
	public void InfoMgr_corporateLocations_AllTab_Add_Modify() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		CorporateLocationsCommonMethods corporateLocations = fc.infomgr().corporateLocations();
		AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();
		InfoMgrCorporateLocationsPage corporateLocationsPage = new InfoMgrCorporateLocationsPage(driver);
		

		try {
			fc.loginpage().login(driver);
			// adminInfoMgr.configureInfoMgr(driver, config);

			InfoMgrCenterInfoPage centerInfoPage = new InfoMgrCorporateLocationsPage(driver).getCenterInfoPage();

			fc.utobj().printTestStep("Add corporate user");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail("infomgrautomation@staffex.com");
			corpUser = corpTest.createDefaultUser(driver, corpUser);

			fc.utobj().printTestStep("Add corporate location");
			String franchiseID = adminInfoMgr.addCorporateLocation(driver, config);
			Reporter.log("FranchiseID - " + franchiseID + " Created");

			fc.utobj().printTestStep("Search and click ");
			corporateLocations.searchFranchiseAndClick(driver, franchiseID);

			InfoMgrContactHistoryPage contactHistoryPage = corporateLocationsPage.getContactHistoryPage();

			fc.utobj().printTestStep("Add data to all the tabs");
			fc.utobj().clickElement(driver, centerInfoPage.lnkAddresses);
			driver = corporateLocations.addAddresses(driver, config);

			// Add Agreement
			fc.utobj().clickElement(driver, centerInfoPage.lnkAgreement);
			driver = corporateLocations.addAgreement(driver, config);

			// Modify Center Info

			fc.utobj().clickElement(driver, centerInfoPage.lnkCenterInfo);
			fc.utobj().clickLink(driver, "Modify");
			corporateLocations.modifyCenterInfo(driver, config);

			// Log a Task
			fc.utobj().clickElement(driver, contactHistoryPage.lnkContactHistory);

			fc.utobj().clickElement(driver, contactHistoryPage.btnLogaTask);
			String taskSubject = corporateLocations.logaTask(driver, config, corpUser.getuserFullName(), true);

			// Log a Call
			fc.utobj().clickElement(driver, contactHistoryPage.btnLogaCall);
			String callSubject = corporateLocations.logACall(driver, config, true);

			// Add Remark
			fc.utobj().clickElement(driver, contactHistoryPage.btnAddRemark);
			String remarkSubject = corporateLocations.addRemark(driver, config, contactHistoryPage);

			// Send Message
			// fc.utobj().clickElement(driver,
			// contactHistoryPage.btnSendMessage);
			// corporateLocations.sendTextMessage(driver, config, true ,
			// corporateUser);

			// Send Mail
			// fc.utobj().clickElement(driver, contactHistoryPage.btnSendEmail);
			// corporateLocations.sendTextEmail(driver, config , true);

			// Sending HTML Message and mail (Pending)

			// Adding Contract Signing
			fc.utobj().clickElement(driver, contactHistoryPage.lnkContractSigning);
			corporateLocations.addContractSigning(driver, config);

			// Add Customer Complaints
			fc.utobj().clickElement(driver, contactHistoryPage.lnkCustomerComplaints);
			corporateLocations.addCustomerComplaints(driver, config);

			// Add Documents
			fc.utobj().clickElement(driver, contactHistoryPage.lnkDocuments);
			String docTitle = corporateLocations.addDocument(driver, config);

			// Add Employees
			fc.utobj().clickElement(driver, contactHistoryPage.lnkEmployees);
			String empName = corporateLocations.addEmployee(driver, config);

			// Add Entity Details
			fc.utobj().clickElement(driver, contactHistoryPage.linkEntityDetail);
			corporateLocations.addEntityDetails(driver, config);

			// Add Events Details
			fc.utobj().clickElement(driver, contactHistoryPage.lnkEvents);
			corporateLocations.addEventDetails(driver, config);

			// Add Finance Details
			fc.utobj().clickElement(driver, contactHistoryPage.lnkFinancial);
			corporateLocations.addFinanceDetails(driver, config);

			// Add Gurantor details
			fc.utobj().clickElement(driver, contactHistoryPage.lnkGuarantors);
			corporateLocations.addGuarantor(driver, config);

			// Add Insurance Details
			fc.utobj().clickElement(driver, contactHistoryPage.lnkInsurance);
			corporateLocations.addInsuranceDetails(driver, config);

			// Add legal violation
			fc.utobj().clickElement(driver, contactHistoryPage.lnkLegalViolation);
			corporateLocations.addLegalViolation(driver, config);

			// Add Lenders
			fc.utobj().clickElement(driver, contactHistoryPage.lnkLenders);
			corporateLocations.addLenders(driver, config);

			// Add Marketing details
			fc.utobj().clickElement(driver, contactHistoryPage.lnkMarketingFinTab); // Change
																					// by
																					// harish
			corporateLocations.addMarketingDetails(driver, config);

			// Add Mystry Review
			fc.utobj().clickElement(driver, contactHistoryPage.lnkMysteryReview);
			corporateLocations.addMystryReview(driver, config);

			// Add Owner
			fc.utobj().clickElement(driver, contactHistoryPage.tabOwners);
			Map<String, String> owner = corporateLocations.addOwner(driver, config);

			// Add Picture
			fc.utobj().clickElement(driver, contactHistoryPage.lnkPictures);
			String pictureTitle = corporateLocations.addPicture(driver, config);

			// Add Real Estate
			fc.utobj().clickElement(driver, contactHistoryPage.lnkRealEstate);
			corporateLocations.addRealState(driver, config);

			// Add Territory
			fc.utobj().clickElement(driver, contactHistoryPage.lnkTerritory);
			corporateLocations.addTerritory(driver, config);

			// Add Training
			fc.utobj().clickElement(driver, centerInfoPage.lnkTraining);
			corporateLocations.addTraining(driver, config);

			// Add User
			fc.utobj().clickElement(driver, contactHistoryPage.lnkUsers);
			InfoMgrUsersPage usersPage = new InfoMgrCorporateLocationsPage(driver).getUsersPage();
			fc.utobj().clickElement(driver, usersPage.btnAddUser);
			String userName = corporateLocations.addOwnerUser(driver, config,
					owner.get("FirstName") + " " + owner.get("LastName"));

			// ****************** Modify Data
			// **************************************
			fc.utobj().printTestStep("Modify data of all the tabs");

			corporateLocations.searchFranchiseAndClick(driver, franchiseID);

			// Modify Address
			fc.utobj().clickElement(driver, centerInfoPage.lnkAddresses);
			driver = corporateLocations.modifyAddresses(driver, config);

			// Modify Agreement
			fc.utobj().clickElement(driver, centerInfoPage.lnkAgreement);
			driver = corporateLocations.modifyAgreement(driver, config);

			// Modify Center Info
			fc.utobj().clickElement(driver, centerInfoPage.lnkCenterInfo);
			fc.utobj().clickLink(driver, "Modify");
			corporateLocations.modifyCenterInfo(driver, config);

			// Modify Contact History

			// Modify a task
			fc.utobj().clickElement(driver, contactHistoryPage.lnkContactHistory);
			fc.utobj().actionImgOption(driver, taskSubject, "Modify");
			corporateLocations.modifyATask(driver, config, true);

			// Modify a call
			fc.utobj().actionImgOption(driver, callSubject, "Modify");
			corporateLocations.modifyACall(driver, config, true);

			// Modify a remark
			fc.utobj().actionImgOption(driver, remarkSubject, "Modify");
			corporateLocations.modifyRemark(driver, config, contactHistoryPage);

			// Modify Contract Signing
			fc.utobj().clickElement(driver, centerInfoPage.lnkContractSigning);
			corporateLocations.modifyContractSigning(driver, config);

			// Modify Customer Complaints
			fc.utobj().clickElement(driver, centerInfoPage.lnkCustomerComplaints);
			corporateLocations.modifyCustomerComplaints(driver, config);

			// Modify Documents
			fc.utobj().clickElement(driver, centerInfoPage.lnkDocuments);
			fc.utobj().actionImgOption(driver, docTitle, "Modify");
			docTitle = corporateLocations.modifyDocument(driver, config);

			// Modify Employee
			fc.utobj().clickElement(driver, centerInfoPage.lnkEmployees);
			// fc.utobj().actionImgOption(driver, empName, "Modify");
			empName = corporateLocations.modifyEmployee(driver, config);

			// Modify Entity Details
			fc.utobj().clickElement(driver, centerInfoPage.linkEntityDetail);
			corporateLocations.modifyEntityDetails(driver, config);
			// Modify Events
			fc.utobj().clickElement(driver, centerInfoPage.lnkEvents);
			corporateLocations.modifyEventDetails(driver, config);

			// Modify Financials
			fc.utobj().clickElement(driver, centerInfoPage.lnkFinancial);
			corporateLocations.modifyFinanceDetails(driver, config);

			// Modify Guarantor
			fc.utobj().clickElement(driver, centerInfoPage.lnkGuarantors);
			corporateLocations.modifyGuarantor(driver, config);
			// Modify Insurance
			fc.utobj().clickElement(driver, centerInfoPage.lnkInsurance);
			corporateLocations.modifyInsuranceDetails(driver, config);

			// Modify Legal Violation
			fc.utobj().clickElement(driver, centerInfoPage.lnkLegalViolation);
			corporateLocations.modifyLegalViolation(driver, config);

			// Modify Lenders Details
			fc.utobj().clickElement(driver, centerInfoPage.lnkLenders);
			corporateLocations.modifyLenders(driver, config);

			// Modify Marketing
			fc.utobj().clickElement(driver, centerInfoPage.lnkMarketingFinTab); // Change
																				// by
																				// harish
			corporateLocations.modifyMarketingDetails(driver, config);

			// Modify Mystry Review
			fc.utobj().clickElement(driver, centerInfoPage.lnkMysteryReview);
			corporateLocations.modifyMystryReview(driver, config);

			// Modify Owners
			fc.utobj().clickElement(driver, centerInfoPage.tabOwners);
			fc.utobj().actionImgOption(driver, owner.get("FirstName"), "Modify");
			corporateLocations.modifyOwner(driver, config);

			// Modify Pictures
			fc.utobj().clickElement(driver, centerInfoPage.lnkPictures);
			fc.utobj().actionImgOption(driver, pictureTitle, "Modify");
			pictureTitle = corporateLocations.modifyPicture(driver, config);

			// Modify Real state
			fc.utobj().clickElement(driver, centerInfoPage.lnkRealEstate);
			corporateLocations.modifyRealState(driver, config);

			// Modify Territory
			fc.utobj().clickElement(driver, centerInfoPage.lnkTerritory);
			corporateLocations.modifyTerritory(driver, config);

			// Modify Training
			fc.utobj().clickElement(driver, centerInfoPage.lnkTraining);
			corporateLocations.modifyTraining(driver, config);

			// Modify Users
			fc.utobj().clickElement(driver, centerInfoPage.lnkUsers);
			fc.utobj().actionImgOption(driver, owner.get("FirstName") + " " + owner.get("LastName"), "Modify");
			corporateLocations.modifyOwnerUser(driver, config);

			// ************************************************* Delete Section
			// ************************************************

			fc.utobj().printTestStep("Delete data from all the tabs !! ");

			corporateLocations.deleteTabData(driver, config, centerInfoPage.lnkAgreement, null);

			corporateLocations.deleteTabData(driver, config, centerInfoPage.lnkContractSigning, null);

			corporateLocations.deleteTabData(driver, config, centerInfoPage.lnkCustomerComplaints, null);

			corporateLocations.deleteTabData(driver, config, centerInfoPage.lnkDocuments, docTitle);

			corporateLocations.deleteTabData(driver, config, centerInfoPage.lnkEmployees, empName);

			corporateLocations.deleteTabData(driver, config, centerInfoPage.linkEntityDetail, null);

			corporateLocations.deleteTabData(driver, config, centerInfoPage.lnkEvents, null);

			corporateLocations.deleteTabData(driver, config, centerInfoPage.lnkFinancial, null);

			corporateLocations.deleteTabData(driver, config, centerInfoPage.guarantorsTab, null);

			corporateLocations.deleteTabData(driver, config, centerInfoPage.insuranceTab, null);

			corporateLocations.deleteTabData(driver, config, centerInfoPage.lnkLegalViolation, null);

			corporateLocations.deleteTabData(driver, config, centerInfoPage.lnkLenders, null);

			corporateLocations.deleteTabData(driver, config, centerInfoPage.lnkMarketingFinTab, null); // Change
																										// by
																										// harish

			corporateLocations.deleteTabData(driver, config, centerInfoPage.lnkMysteryReview, null);

			corporateLocations.deleteTabData(driver, config, centerInfoPage.ownersTab, owner.get("FirstName"));

			corporateLocations.deleteTabData(driver, config, centerInfoPage.lnkPictures, pictureTitle);

			corporateLocations.deleteTabData(driver, config, centerInfoPage.lnkRealEstate, null);

			corporateLocations.deleteTabData(driver, config, centerInfoPage.lnkTerritory, null);

			corporateLocations.deleteTabData(driver, config, centerInfoPage.lnkTraining, null);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}*/

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Verify_InfoMgr_CorpLocations_Actions_SendMail", testCaseDescription = "Test to verify send mail functionality from action buttons.", reference = {
			"" })
	public void InfoMgr_CorpLocations_ActionsMenu_Actions_SendMail() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		AdminInfoMgrCommonMethods adminInfoMgr = new AdminInfoMgrCommonMethods();
		CorporateLocationsCommonMethods corporateLocations = fc.infomgr().corporateLocations();

		try {
			fc.loginpage().login(driver);

			InfoMgrCorporateLocationsPage corporateLocationsPage = new InfoMgrCorporateLocationsPage(driver);

			fc.utobj().printTestStep("Add Corporate Locations");
			String franchiseID = adminInfoMgr.addCorporateLocation(driver, config);
			corporateLocations.corporateLocationsPageShowAll(driver);
			fc.utobj().printTestStep("Clicking on Send mail link under action image button ");
			fc.utobj().actionImgOption(driver, franchiseID, "Send Email");
			String mailSubject = corporateLocations.sendTextEmail(driver, config, false);
			corporateLocations.searchFranchiseAndClick(driver, franchiseID);
			fc.utobj().clickElement(driver, corporateLocationsPage.lnkContactHistory);
			boolean assertMailSubjectonPage = fc.utobj().assertPageSource(driver, mailSubject);
			if (assertMailSubjectonPage) {
				Reporter.log("Text mail has been sent successfully");
			} else {
				Reporter.log("Text mail has not  been sent .");
			}

			fc.utobj().printTestStep("Send mail from action button displayed at the bottom of the page");
			corporateLocations.corporateLocationsPageShowAll(driver);
			corporateLocations.checkLocation(driver, franchiseID);
			fc.utobj().clickElement(driver, corporateLocationsPage.btnSendMail);
			String mailSubject2 = corporateLocations.sendTextEmail(driver, config, false);
			corporateLocations.searchFranchiseAndClick(driver, franchiseID);
			fc.utobj().clickElement(driver, corporateLocationsPage.lnkContactHistory);
			boolean assertMailSubjectonPage2 = fc.utobj().assertPageSource(driver, mailSubject2);
			if (assertMailSubjectonPage2) {
				Reporter.log("Text mail has been sent successfully");
			} else {
				Reporter.log("Text mail has not  been sent .");
			}

			/*
			 * fc.utobj().printTestStep(testCaseId, "Click on Send Email button"
			 * ); corporateLocations.corporateLocationsPageShowAll(driver);
			 * corporateLocations.checkLocation(driver, franchiseID);
			 * fc.utobj().selectActionMenuItemsWithTagA(driver, "Send Email");
			 * String mailSubject3 = corporateLocations.sendTextEmail(driver,
			 * config , false);
			 * corporateLocations.searchFranchiseAndClick(driver, franchiseID);
			 * fc.utobj().clickElement(driver,
			 * corporateLocationsPage.lnkContactHistory);
			 * 
			 * boolean assertMailSubjectonPage3 =
			 * fc.utobj().assertPageSource(driver, mailSubject3);
			 * if(assertMailSubjectonPage3) { Reporter.log(
			 * "Text mail has been sent successfully"); } else { Reporter.log(
			 * "Text mail has not  been sent ."); }
			 */

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Verify_InfoMgr_CorpLocations_Actions_LogaTask", testCaseDescription = "Test to verify Log a task functionality from action buttons ", reference = {
			"" })
	public void InfoMgr_CorpLocations_PageBottom_Actions_LogaTask() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		CorporateLocationsCommonMethods corporateLocations = fc.infomgr().corporateLocations();
		AdminInfoMgrCommonMethods adminInfoMgr = new AdminInfoMgrCommonMethods();

		InfoMgrCorporateLocationsPage corporateLocationsPage = new InfoMgrCorporateLocationsPage(driver);
		InfoMgrContactHistoryPage contactHistoryPage = corporateLocationsPage.getContactHistoryPage();

		try {
			fc.loginpage().login(driver);

			fc.utobj().printTestStep("Add corporate user");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail("infomgrautomation@staffex.com");
			corpUser = corpTest.createDefaultUser(driver, corpUser);

			corporateLocations.corporateLocationsPageShowAll(driver);

			fc.utobj().printTestStep("Add Corporate Locations");
			String franchiseID = adminInfoMgr.addCorporateLocation(driver, config);

			fc.utobj().printTestStep("Click on Log a Task button displayed at the bottom of the page");
			corporateLocations.checkLocation(driver, franchiseID);
			fc.utobj().clickElement(driver, corporateLocationsPage.btnLogATask);
			String taskSubject1 = corporateLocations.logaTask(driver, config, corpUser.getuserFullName(), false);
			corporateLocations.searchFranchiseAndClick(driver, franchiseID);
			fc.utobj().clickElement(driver, contactHistoryPage.lnkContactHistory);
			boolean assertTaskSubjectonPage1 = fc.utobj().assertPageSource(driver, taskSubject1);
			if (assertTaskSubjectonPage1) {
				Reporter.log("Log a Task has been done successfully");
			} else {
				Reporter.log("Log a Task failed .");
			}

			fc.utobj().printTestStep("Log a task from action image icon");
			corporateLocations.corporateLocationsPageShowAll(driver);
			fc.utobj().actionImgOption(driver, franchiseID, "Log a Task");
			String taskSubject2 = corporateLocations.logaTask(driver, config, corpUser.getuserFullName(), false);
			corporateLocations.searchFranchiseAndClick(driver, franchiseID);
			fc.utobj().clickElement(driver, contactHistoryPage.lnkContactHistory);
			boolean assertTaskSubjectonPage2 = fc.utobj().assertPageSource(driver, taskSubject2);
			if (assertTaskSubjectonPage2) {
				Reporter.log("Log a Task has been done successfully");
			} else {
				Reporter.log("Log a Task failed .");
			}

			/*
			 * corporateLocations.corporateLocationsPageShowAll(driver);
			 * fc.utobj().printTestStep(testCaseId,
			 * "Click on Log a Task link displayed under the action button click at the top of the page"
			 * ); corporateLocations.corporateLocationsPageShowAll(driver);
			 * corporateLocations.checkLocation(driver, franchiseID);
			 * fc.utobj().selectActionMenuItemsWithTagA(driver, "Log a Task");
			 * String taskSubject3 = corporateLocations.logaTask(driver, config,
			 * corporateUser , false);
			 * corporateLocations.searchFranchiseAndClick(driver , franchiseID);
			 * fc.utobj().clickElement(driver,
			 * contactHistoryPage.lnkContactHistory); boolean
			 * assertTaskSubjectonPage3 = fc.utobj().assertPageSource(driver,
			 * taskSubject3); if(assertTaskSubjectonPage3) { Reporter.log(
			 * "Log a Task has been done successfully"); } else { Reporter.log(
			 * "Log a Task failed ."); }
			 */

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Verify_InfoMgr_CorpLocations_Actions_LogaCall", testCaseDescription = "Test to verify log a call functionality from action buttons", reference = {
			"" })
	public void InfoMgr_CorpLocations_Actions_LogaCall() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		CorporateLocationsCommonMethods corporateLocations = fc.infomgr().corporateLocations();
		AdminInfoMgrCommonMethods adminInfoMgr = new AdminInfoMgrCommonMethods();

		InfoMgrCorporateLocationsPage corporateLocationsPage = new InfoMgrCorporateLocationsPage(driver);
		InfoMgrContactHistoryPage contactHistoryPage = corporateLocationsPage.getContactHistoryPage();
		try {
			fc.loginpage().login(driver);

			String franchiseID = adminInfoMgr.addCorporateLocation(driver, config);
			corporateLocations.corporateLocationsPageShowAll(driver);

			fc.utobj().printTestStep("Click on Log a Call button displayed at the bottom of the page ");
			corporateLocations.checkLocation(driver, franchiseID);
			fc.utobj().clickElement(driver, corporateLocationsPage.btnLogACall);
			String logACallSubject1 = corporateLocations.logACall(driver, config, false);
			corporateLocations.searchFranchiseAndClick(driver, franchiseID);
			fc.utobj().clickElement(driver, contactHistoryPage.lnkContactHistory);
			boolean assertCallTextOnPage1 = fc.utobj().assertPageSource(driver, logACallSubject1);
			if (assertCallTextOnPage1) {
				Reporter.log("Log a Call has been done successfully");
			} else {
				Reporter.log("Log a Call has failed .");
			}

			fc.utobj().printTestStep("Log a call from action menu");
			corporateLocations.corporateLocationsPageShowAll(driver);
			fc.utobj().actionImgOption(driver, franchiseID, "Log a Call");
			String logACallSubject2 = corporateLocations.logACall(driver, config, false);
			corporateLocations.searchFranchiseAndClick(driver, franchiseID);
			fc.utobj().clickElement(driver, contactHistoryPage.lnkContactHistory);
			boolean assertCallTextOnPage2 = fc.utobj().assertPageSource(driver, logACallSubject2);
			if (assertCallTextOnPage2) {
				Reporter.log("Log a Call has been done successfully");
			} else {
				Reporter.log("Log a Call has failed .");
			}

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Click on Log a Call link displayed under the top action button"
			 * ); corporateLocations.corporateLocationsPageShowAll(driver);
			 * corporateLocations.checkLocation(driver, franchiseID);
			 * fc.utobj().selectActionMenuItemsWithTagA(driver, "Log a Call");
			 * String logACallSubject3 = corporateLocations.logACall(driver,
			 * config , false);
			 * corporateLocations.searchFranchiseAndClick(driver , franchiseID);
			 * fc.utobj().clickElement(driver,
			 * contactHistoryPage.lnkContactHistory); boolean
			 * assertCallTextOnPage3 = fc.utobj().assertPageSource(driver,
			 * logACallSubject3); if(assertCallTextOnPage3) { Reporter.log(
			 * "Log a Call has been done successfully"); } else { Reporter.log(
			 * "Log a Call has failed ."); }
			 */

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-06", testCaseId = "TC_Verify_InfoMgr_CorpLocations_Actions_AddToGroup", testCaseDescription = "Test to verify Add to Group functionality from action buttons", reference = {
			"" })
	public void InfoMgr_CorpLocations_Actions_AddToGroup() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		CorporateLocationsCommonMethods corporateLocations = fc.infomgr().corporateLocations();
		InfoMgrCorporateLocationsPage corporateLocationsPage = new InfoMgrCorporateLocationsPage(driver);
		AdminInfoMgrCommonMethods adminInfoMgr = new AdminInfoMgrCommonMethods();

		try {
			fc.loginpage().login(driver);

			String franchiseID = adminInfoMgr.addCorporateLocation(driver, config);
			corporateLocations.corporateLocationsPageShowAll(driver);

			fc.utobj().printTestStep("Click on Add to Group button displayed at the bottom of the page");
			corporateLocations.checkLocation(driver, franchiseID);
			fc.utobj().clickElement(driver, corporateLocationsPage.btnAddtoGroup);
			corporateLocations.addToGroup(driver, config);

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Click on Add to Group link displayed under Action button displayed at the top of the page"
			 * ); corporateLocations.corporateLocationsPageShowAll(driver);
			 * corporateLocations.checkLocation(driver, franchiseID);
			 * fc.utobj().selectActionMenuItemsWithTagA(driver, "Add To Group");
			 * corporateLocations.addToGroup(driver, config);
			 */

			fc.utobj().printTestStep("Click on Add to Group button  displayed at the bottom of the page");
			corporateLocations.corporateLocationsPageShowAll(driver);
			corporateLocations.checkLocation(driver, franchiseID);
			fc.utobj().clickElement(driver, corporateLocationsPage.btnAddtoGroup);
			corporateLocations.addToGroup(driver, config);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr"})

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Verify_InfoMgr_CorpLocations_Actions_Print", testCaseDescription = "Test to verify print functionality from action buttons", reference = {
			"" })
	public void InfoMgr_CorpLocations_Actions_Print() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		CorporateLocationsCommonMethods corporateLocations = fc.infomgr().corporateLocations();
		InfoMgrCorporateLocationsPage corporateLocationsPage = new InfoMgrCorporateLocationsPage(driver);
		AdminInfoMgrCommonMethods adminInfoMgr = new AdminInfoMgrCommonMethods();

		try {
			fc.loginpage().login(driver);
			// adminInfoMgr.configureAllFieldsOnManageFormGeneratorPage(driver ,
			// config);

			String franchiseID = adminInfoMgr.addCorporateLocation(driver, config);
			corporateLocations.corporateLocationsPageShowAll(driver);

			fc.utobj().printTestStep("Click on Print button displayed at the bottom of the page");
			fc.utobj().clickElement(driver, corporateLocationsPage.btnPrint);
			corporateLocations.VerifyPrintPreview(driver, franchiseID);

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Click on Print link displayed under Action button displayed at the top of the page"
			 * ); fc.utobj().selectActionMenuItemsWithTagA(driver, "Print");
			 * corporateLocations.VerifyPrintPreview(driver, franchiseID);
			 */

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Verify_InfoMgr_CorpLocations_Actions_Modify", testCaseDescription = "Test to verify modify functionality from action buttons", reference = {
			"" })
	public void InfoMgr_CorpLocations_Actions_Modify() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		CorporateLocationsCommonMethods corporateLocations = fc.infomgr().corporateLocations();
		AdminInfoMgrCommonMethods adminInfoMgr = new AdminInfoMgrCommonMethods();

		try {
			fc.loginpage().login(driver);

			String franchiseID = adminInfoMgr.addCorporateLocation(driver, config);
			corporateLocations.corporateLocationsPageShowAll(driver);

			fc.utobj().printTestStep("Modify from action menu");
			fc.utobj().actionImgOption(driver, franchiseID, "Modify");
			corporateLocations.modifyCenterInfo(driver, config);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr","Summary_Filter" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-06", testCaseId = "TC_Verify_InfoMgr_CorporateLocations_Filter", testCaseDescription = "Test to verify filter displayed on the orporate locations Page", reference = {
			"" })
	public void InfoMgr_CorporateLocations_Filter_Verify() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		CorporateLocationsCommonMethods corporateLocations = fc.infomgr().corporateLocations();
		RegionalCommonMethods regional = new RegionalCommonMethods();
		AdminInfoMgrCommonMethods adminInfoMgr = new AdminInfoMgrCommonMethods();

		try {
			fc.loginpage().login(driver);

			InfoMgrFranchiseeFilter franchisee = adminInfoMgr.addFranchiseLocationForFilter(driver, true);
			//corporateLocations.corporateLocationsPageShowAll(driver);
			imc.InfoMgrCorporateLocations(driver);
			imc.searchLocationFromShowFilter(driver, franchisee.getFranchiseeID());
			fc.utobj().actionImgOption(driver, franchisee.getFranchiseeID(), "Modify");
			fc.utobj().printTestStep("Adding spouse details by modifying the owner");
			InfoMgrOwnersPage ownerPage = new InfoMgrCorporateLocationsPage(driver).getOwnersPage();
			fc.utobj().clickElement(driver, ownerPage.ownersTab);
			fc.utobj().actionImgOption(driver, franchisee.getOwnerFirstName(), "Modify");
			Map<String, String> lstOwner = corporateLocations.modifyOwner(driver, config);
			franchisee.setSpouseFirstName(lstOwner.get("SpouseFirstName"));

			fc.utobj().printTestStep("Adding area owner");
			InfoMgrOwnersPage regionalOwnerPage = new InfoMgrRegionalPage(driver).getOwnersPage();
			regional.searchAndClickAreaRegion(driver, franchisee.getAreaRegionName());
			fc.utobj().clickElement(driver, regionalOwnerPage.lnkAreaOwner);
			lstOwner = regional.addAreaRegionOwner(driver);
			franchisee.setAreaRegionOwner(lstOwner.get("FirstName"));

			fc.utobj().printTestStep("Verify Search Filter");
			fc.infomgr().infomgr_common().InfoMgrCorporateLocations(driver);
			corporateLocations.search(driver, config, franchisee);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Verify_InfoMgr_CorporateLocations_AddToCenterInfo_Fields", testCaseDescription = "Test to verify fileds added by manage form generator  to center info", reference = {
			"" })
	public void InfoMgr_CorporateLocations_CenterInfo_AddToCenterInfo_Fields() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		CorporateLocationsCommonMethods corporateLocations = fc.infomgr().corporateLocations();
		AdminInfoMgrCommonMethods adminInfoMgr = new AdminInfoMgrCommonMethods();
		String[] formsArray = { "Agreement", "Contract Signing", "Customer Complaints", "Default and Termination",
				"Employees", "Entity Details", "Events", "Financial", "Guarantors", "Insurance", "Legal Violation",
				"Lenders", "Marketing", "Mystery Review", "Owners", "Real Estate", "Renewal", "Territory", "Training",
				"Transfer" };
		try {
			fc.loginpage().login(driver);
			fc.utobj().printTestStep("Add Fileds to Center Info");
			Map<String, String> dsCenterInfoFields = adminInfoMgr.configure_Franchisees_AddToCenterInfo_AllForms(driver,
					config);
			fc.utobj().printTestStep("Adding franchise Location");
			String franchiseID = adminInfoMgr.addCorporateLocation(driver, config);
			corporateLocations.searchFranchiseAndClick(driver, franchiseID);
			corporateLocations.modifyCenterInfo(driver, config);
			fc.utobj().printTestStep("Verify added fields on center info page");
			for (int i = 0; i < formsArray.length; i++) {
				try {
					fc.utobj().getElementByXpath(driver, ".//*[@id='centerInfoDIV']//td[contains(text() , '"
							+ dsCenterInfoFields.get(formsArray[i]) + "')]");
					Reporter.log(
							"Field " + dsCenterInfoFields.get(formsArray[i]) + " is displayed on the Center Info page");
				} catch (Exception ex) {
					fc.utobj().throwsException("Field " + dsCenterInfoFields.get(formsArray[i])
							+ " is not displayed on the Center Info page");
				}
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr_test1" }, priority = 4)

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Verify_InfoMgr_CorporateLocations_CenterInfo_CustomFieldsandSection", testCaseDescription = "Test to verify custom fields added to center info by manage form generator.", reference = {
			"" })
	public void InfoMgr_CorporateLocations_CenterInfo_CustomFields() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		CorporateLocationsCommonMethods corporateLocations = fc.infomgr().corporateLocations();
		AdminInfoMgrCommonMethods adminInfoMgr = new AdminInfoMgrCommonMethods();

		try {
			fc.loginpage().login(driver);

			fc.utobj().printTestStep("Add custom fields to Center Info");
			Map<String, String> dsCenterInfoCustomFields = adminInfoMgr.ManageFormGenerator_Add_Section_Fields_To_Forms(
					driver, config, "Franchisee", "Center Info", "Corp");

			fc.utobj().printTestStep("Adding corporate location");
			String franchiseID = adminInfoMgr.addCorporateLocation(driver, config);

			corporateLocations.searchFranchiseAndClick(driver, franchiseID);
			fc.utobj().printTestStep("Verify that added custom fields are displayed on the center info page");

			for (String key : dsCenterInfoCustomFields.keySet()) {
				if (!key.contains("drpOption")) {
					if (fc.utobj().assertPageSource(driver, dsCenterInfoCustomFields.get(key))) {
						Reporter.log(
								"Custom field " + dsCenterInfoCustomFields.get(key) + " is displayed on the page !!");
					} else {
						fc.utobj().throwsException(
								"Custom field " + dsCenterInfoCustomFields.get(key) + " is not displayed on the page");
					}
				}
			}

			fc.utobj().printTestStep("Deleting the created section and its fields");
			adminInfoMgr.deleteSectionandFields(driver, config, "Franchisee", "Center Info", dsCenterInfoCustomFields);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

}
