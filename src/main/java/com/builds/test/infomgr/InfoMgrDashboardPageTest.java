package com.builds.test.infomgr;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterGroups;
import org.testng.annotations.Test;
import com.builds.test.admin.AdminAreaRegionAddAreaRegionPageTest;
import com.builds.test.admin.AdminDivisionAddDivisionPageTest;
import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.admin.AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest;
import com.builds.test.admin.AdminUsersManageRegionalUsersAddRegionalUserPageTest;
import com.builds.test.admin.LocationData;
import com.builds.test.common.CorporateUser;
import com.builds.test.common.Location;
import com.builds.uimaps.fs.TaskWindowUI;
import com.builds.uimaps.infomgr.InfoMgrAgreementPage;
import com.builds.uimaps.infomgr.InfoMgrBasePage;
import com.builds.uimaps.infomgr.InfoMgrDashboardPage;
import com.builds.uimaps.infomgr.InfoMgrDefaultAndTerminationPage;
import com.builds.uimaps.infomgr.InfoMgrLogACallPage;
import com.builds.uimaps.infomgr.InfoMgrLogATaskPage;
import com.builds.uimaps.infomgr.InfoMgrQAHistoryPage;
import com.builds.uimaps.infomgr.InfoMgrRenewalPage;
import com.builds.uimaps.infomgr.InfoMgrSendEmailPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;


public class InfoMgrDashboardPageTest {
	FranconnectUtil fc = new FranconnectUtil();
	AdminUsersManageRegionalUsersAddRegionalUserPageTest RUser= new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
	InfoMgr_Common imc = new InfoMgr_Common();
	AdminDivisionAddDivisionPageTest division = new AdminDivisionAddDivisionPageTest();
	AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest divUser = new AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest();
	TasksCommonMethods tcm = new TasksCommonMethods();
	AdminInfoMgrCommonMethods admininfomgrcommonmethod = new AdminInfoMgrCommonMethods();

	
	
	@Test(groups = { "infomgr","IM_Dashboard","IM1" })
	@TestCase(createdOn = "2018-06-29", updatedOn = "2018-06-29", testCaseId = "TC_IM_Dashboard_DivisionalUser_FranchiseeLast30Days", testCaseDescription = "To verify the IM Dashboard Franchisee System Summary Report Widget after Login via Divisional User for Franchise Location created in Last 30 Days")
	private void dashboardDiv1() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Location location = new Location();
		LocationData ld = new LocationData();
		InfoMgrDashboardPage imdp =new InfoMgrDashboardPage(driver);

		try {
			driver = fc.loginpage().login(driver);
			AdminAreaRegionAddAreaRegionPageTest region_page = new AdminAreaRegionAddAreaRegionPageTest();
			String region_name = fc.utobj().generateTestData("R1");
			region_page.addAreaRegion(driver, region_name);
			String division_name = division.addDivisionName(driver, fc.utobj().generateTestData("D1"));
			String Login_id=fc.utobj().generateTestData("Div1");
			divUser.addDivisionalUser(driver, Login_id, "admin123", division_name, "vimal.sharma@franconnect.com");
			
			location = fc.commonMethods().getDefaultDataFor_Location(location);
			
			fc.adminpage().adminAddNewFranchiseLocationLnk(driver);
			location.setAreaRegion(region_name);
			location.setBrands(division_name);
			location.setOpeningDate(fc.utobj().getFutureDateUSFormat(-2));
			ld.filllocationInfo(driver, location).submit(driver);
			fc.adminpage().manageFranchiseLocationLnk(driver).SearchFranchise(driver, location).VerifyLocationExists(driver, location);
			System.out.println(location.getFranchiseID());
			
			fc.home_page().logout(driver);
			driver = fc.loginpage().loginWithUserNameAndPassword(driver, Login_id, "admin123");
			imc.InfoMgrDashboard(driver);
			fc.utobj().printTestStep("IM Dashboard Franchisee System Summary Report Verififcation Start");
			WebElement ThirtyDays = imdp.Franchiseecount30DaysCount1;
			WebElement SixMonths = imdp.Franchiseecount6MonthsCount1;
			WebElement TwelveMonths = imdp.Franchiseecount12MonthsCount1;
			fc.utobj().isElementPresent(driver, ThirtyDays);
			fc.utobj().printTestStep("New Franchisee Count in last 30 days is coming as Expected value 1");
			fc.utobj().isElementPresent(driver, SixMonths);
			fc.utobj().printTestStep("New Franchisee Count in last 6 Months is coming as Expected value 1");
			fc.utobj().isElementPresent(driver, TwelveMonths);
			fc.utobj().printTestStep("New Franchisee Count in last 12 Months is coming as Expected value 1");
			
			String TotalNewLoc30Days = imdp.TotalNewLocation30Days.getText();
			//String ExpValue30Days ="1";
			//Assert.assertEquals(TotalNewLoc, ExpValue30Days);
			if(!"1".equalsIgnoreCase(TotalNewLoc30Days))
			{
				fc.utobj().printTestStep("Total New Locations in Last 30 Days Count not matched with Expected Value 1");
			}
			fc.utobj().printTestStep("Total New Locations Count in last 30 days is coming as Expected value 1");
			String TotalNewLoc6Months = imdp.TotalNewLocation6Months.getText();
			if(!"1".equalsIgnoreCase(TotalNewLoc6Months))
			{
				fc.utobj().printTestStep("Total New Locations in Last 6 Months  Count not matched with Expected Value 1");
			}
			fc.utobj().printTestStep("Total New Locations Count in last 6 Months is coming as Expected value 1");
			String TotalNewLoc12Months = imdp.TotalNewLocation12Months.getText();
			if(!"1".equalsIgnoreCase(TotalNewLoc12Months))
			{
				fc.utobj().printTestStep("Total New Locations in Last 12 Months  Count not matched with Expected Value 1");
			}		
			fc.utobj().printTestStep("Total New Locations Count in last 12 Months is coming as Expected value 1");
			
			String TotalActiveFranchisees = imdp.TotalActiveFranchisees.getText();
			if(!"1".equalsIgnoreCase(TotalActiveFranchisees))
			{
				fc.utobj().printTestStep("Total Active Franchisee Count not matched with Expected Value 1");
			}
			fc.utobj().printTestStep("Total Active Franchisee Count is coming as Expected value 1");
			String TotalActiveLocations = imdp.TotalActiveLocations.getText();
			if(!"1".equalsIgnoreCase(TotalActiveLocations))
			{
				fc.utobj().printTestStep("Total Active Franchisee Count not matched with Expected Value 1");
			}	
			fc.utobj().printTestStep("Total Active Locations Count is coming as Expected value 1");
			fc.utobj().clickElement(driver, imdp.Franchiseecount30DaysCount1);
			fc.utobj().assertPageSource(driver,location.getFranchiseID() );
			fc.utobj().printTestStep("Created Franchisee location is verified after clicks on New Franchisee created in Last 30 Days count");
			fc.utobj().printTestStep("IM Dashboard Franchisee System Summary Report Verififcation Completed");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	@Test(groups = { "infomgr","IM_Dashboard" })
	@TestCase(createdOn = "2018-07-02", updatedOn = "2018-07-02", testCaseId = "TC_IM_Dashboard_DivisionalUser_FranchiseeLast6Months", testCaseDescription = "To verify the IM Dashboard Franchisee System Summary Report Widget after Login via Divisional User for Franchise Location created in Last 6 Months")
	private void dashboardDiv2() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Location location = new Location();
		LocationData ld = new LocationData();
		InfoMgrDashboardPage imdp =new InfoMgrDashboardPage(driver);

		try {
			driver = fc.loginpage().login(driver);
			AdminAreaRegionAddAreaRegionPageTest region_page = new AdminAreaRegionAddAreaRegionPageTest();
			String region_name = fc.utobj().generateTestData("R1");
			region_page.addAreaRegion(driver, region_name);
			String division_name = division.addDivisionName(driver, fc.utobj().generateTestData("D1"));
			String Login_id=fc.utobj().generateTestData("Div1");
			divUser.addDivisionalUser(driver, Login_id, "admin123", division_name, "vimal.sharma@franconnect.com");
			location = fc.commonMethods().getDefaultDataFor_Location(location);
			fc.adminpage().adminAddNewFranchiseLocationLnk(driver);
			location.setAreaRegion(region_name);
			location.setBrands(division_name);
			location.setOpeningDate(fc.utobj().getFutureDateUSFormat(-33));
			ld.filllocationInfo(driver, location).submit(driver);
			fc.adminpage().manageFranchiseLocationLnk(driver).SearchFranchise(driver, location).VerifyLocationExists(driver, location);
			System.out.println(location.getFranchiseID());
			fc.home_page().logout(driver);
			driver = fc.loginpage().loginWithUserNameAndPassword(driver, Login_id, "admin123");
			
			imc.InfoMgrDashboard(driver);
			fc.utobj().printTestStep("IM Dashboard Franchisee System Summary Report Verififcation Start");
			
			String ThirtyDays = imdp.Franchiseecount30DaysCountGetTxt.getText();
			if(!"--".equalsIgnoreCase(ThirtyDays))
			{
				fc.utobj().printTestStep("New Franchisees in Last 30 Days Count not matched with Expected Value 1");
			}
			fc.utobj().printTestStep("New Franchisees Count in last 30 days is coming as Expected value --");
			
			WebElement SixMonths = imdp.Franchiseecount6MonthsCount1;
			WebElement TwelveMonths = imdp.Franchiseecount12MonthsCount1 ;
			fc.utobj().isElementPresent(driver, SixMonths);
			fc.utobj().printTestStep("New Franchisee Count in last 6 Months is coming as Expected value 1");
			fc.utobj().isElementPresent(driver, TwelveMonths);
			fc.utobj().printTestStep("New Franchisee Count in last 12 Months is coming as Expected value 1");
			
			String TotalNewLoc6Months = imdp.TotalNewLocation6Months.getText();
			if(!"1".equalsIgnoreCase(TotalNewLoc6Months))
			{
				fc.utobj().printTestStep("Total New Locations in Last 6 Months  Count not matched with Expected Value 1");
			}
			fc.utobj().printTestStep("Total New Locations Count in last 6 Months is coming as Expected value 1");
			String TotalNewLoc12Months = imdp.TotalNewLocation12Months.getText();
			if(!"1".equalsIgnoreCase(TotalNewLoc12Months))
			{
				fc.utobj().printTestStep("Total New Locations in Last 12 Months  Count not matched with Expected Value 1");
			}		
			fc.utobj().printTestStep("Total New Locations Count in last 12 Months is coming as Expected value 1");
			
			String TotalActiveFranchisees = imdp.TotalActiveFranchisees.getText();
			if(!"1".equalsIgnoreCase(TotalActiveFranchisees))
			{
				fc.utobj().printTestStep("Total Active Franchisee Count not matched with Expected Value 1");
			}
			fc.utobj().printTestStep("Total Active Franchisee Count is coming as Expected value 1");
			String TotalActiveLocations = imdp.TotalActiveLocations.getText();
			if(!"1".equalsIgnoreCase(TotalActiveLocations))
			{
				fc.utobj().printTestStep("Total Active Franchisee Count not matched with Expected Value 1");
			}	
			fc.utobj().printTestStep("Total Active Locations Count is coming as Expected value 1");
			fc.utobj().clickElement(driver, imdp.Franchiseecount6MonthsCount1);
			
			fc.utobj().assertPageSource(driver,location.getFranchiseID() );
			fc.utobj().printTestStep("Created Franchisee location is verified after clicks on New Franchisee created in Last 6 Months count");
			fc.utobj().printTestStep("IM Dashboard Franchisee System Summary Report Verififcation Completed");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	@Test(groups = { "infomgr","IM_Dashboard" })
	@TestCase(createdOn = "2018-07-02", updatedOn = "2018-07-02", testCaseId = "TC_IM_Dashboard_DivisionalUser_FranchiseeLast12Months", testCaseDescription = "To verify the IM Dashboard Franchisee System Summary Report Widget after Login via Divisional User for Franchise Location created in Last 12 Months")
	private void dashboardDiv3() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Location location = new Location();
		LocationData ld = new LocationData();
		InfoMgrDashboardPage imdp =new InfoMgrDashboardPage(driver);

		try {
			driver = fc.loginpage().login(driver);
			AdminAreaRegionAddAreaRegionPageTest region_page = new AdminAreaRegionAddAreaRegionPageTest();
			String region_name = fc.utobj().generateTestData("R1");
			region_page.addAreaRegion(driver, region_name);
			String division_name = division.addDivisionName(driver, fc.utobj().generateTestData("D1"));
			String Login_id=fc.utobj().generateTestData("Div1");
			divUser.addDivisionalUser(driver, Login_id, "admin123", division_name, "vimal.sharma@franconnect.com");
			location = fc.commonMethods().getDefaultDataFor_Location(location);
			fc.adminpage().adminAddNewFranchiseLocationLnk(driver);
			location.setAreaRegion(region_name);
			location.setBrands(division_name);
			location.setOpeningDate(fc.utobj().getFutureDateUSFormat(-183));
			ld.filllocationInfo(driver, location).submit(driver);
			fc.adminpage().manageFranchiseLocationLnk(driver).SearchFranchise(driver, location).VerifyLocationExists(driver, location);
			System.out.println(location.getFranchiseID());
			fc.home_page().logout(driver);
			driver = fc.loginpage().loginWithUserNameAndPassword(driver, Login_id, "admin123");
			
			imc.InfoMgrDashboard(driver);
			fc.utobj().printTestStep("IM Dashboard Franchisee System Summary Report Verififcation Start");
			String ThirtyDays = imdp.Franchiseecount30DaysCountGetTxt.getText();
			if(!"--".equalsIgnoreCase(ThirtyDays))
			{
				fc.utobj().printTestStep("New Franchisees in Last 30 Days Count not matched with Expected Value 1");
			}
			fc.utobj().printTestStep("New Franchisees Count in last 30 days is coming as Expected value --");
			
			String SixMonths = imdp.Franchiseecount6MonthsCountGetTxt.getText();
			if(!"--".equalsIgnoreCase(SixMonths))
			{
				fc.utobj().printTestStep("New Franchisees in Last 6 Months Count not matched with Expected Value 1");
			}
			fc.utobj().printTestStep("New Franchisees Count in 6 Months is coming as Expected value --");
			
			
			WebElement TwelveMonths = imdp.Franchiseecount12MonthsCount1 ;
			fc.utobj().isElementPresent(driver, TwelveMonths);
			fc.utobj().printTestStep("New Franchisee Count in last 12 Months is coming as Expected value 1");
			
			String TotalNewLoc12Months = imdp.TotalNewLocation12Months.getText();
			if(!"1".equalsIgnoreCase(TotalNewLoc12Months))
			{
				fc.utobj().printTestStep("Total New Locations in Last 12 Months  Count not matched with Expected Value 1");
			}		
			fc.utobj().printTestStep("Total New Locations Count in last 12 Months is coming as Expected value 1");
			
			String TotalActiveFranchisees = imdp.TotalActiveFranchisees.getText();
			if(!"1".equalsIgnoreCase(TotalActiveFranchisees))
			{
				fc.utobj().printTestStep("Total Active Franchisee Count not matched with Expected Value 1");
			}
			fc.utobj().printTestStep("Total Active Franchisee Count is coming as Expected value 1");
			String TotalActiveLocations = imdp.TotalActiveLocations.getText();
			if(!"1".equalsIgnoreCase(TotalActiveLocations))
			{
				fc.utobj().printTestStep("Total Active Franchisee Count not matched with Expected Value 1");
			}	
			fc.utobj().printTestStep("Total Active Locations Count is coming as Expected value 1");
			fc.utobj().clickElement(driver, imdp.Franchiseecount12MonthsCount1);
			fc.utobj().assertPageSource(driver,location.getFranchiseID() );
			fc.utobj().printTestStep("Created Franchisee location is verified after clicks on Franchisee created in Last 12 Months count");
			fc.utobj().printTestStep("IM Dashboard Franchisee System Summary Report Verififcation Completed");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	@Test(groups = { "infomgr","IM_Dashboard","IM" })
	@TestCase(createdOn = "2018-07-03", updatedOn = "2018-07-03", testCaseId = "TC_IM_Dashboard_DivisionalUser_CorporateLast30Days", testCaseDescription = "To verify the IM Dashboard Franchisee System Summary Report Widget after Login via Divisional User for Corporate Locations created in Last 30 Days")
	private void dashboardDiv4() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Location location = new Location();
		LocationData ld = new LocationData();
		InfoMgrDashboardPage imdp =new InfoMgrDashboardPage(driver);

		try {
			driver = fc.loginpage().login(driver);
			AdminAreaRegionAddAreaRegionPageTest region_page = new AdminAreaRegionAddAreaRegionPageTest();
			String region_name = fc.utobj().generateTestData("R1");
			region_page.addAreaRegion(driver, region_name);
			String division_name = division.addDivisionName(driver, fc.utobj().generateTestData("D1"));
			String Login_id=fc.utobj().generateTestData("Div1");
			divUser.addDivisionalUser(driver, Login_id, "admin123", division_name, "vimal.sharma@franconnect.com");
			location = fc.commonMethods().getDefaultDataFor_Location(location);
			fc.adminpage().adminAddNewFranchiseLocationLnk(driver);
			location.setAreaRegion(region_name);
			location.setBrands(division_name);
			location.setCorporateLocation("Yes");
			location.setOpeningDate(fc.utobj().getFutureDateUSFormat(-2));
			ld.filllocationInfo(driver, location).submit(driver);
			fc.adminpage().manageFranchiseLocationLnk(driver).SearchFranchise(driver, location).VerifyLocationExists(driver, location);
			System.out.println(location.getFranchiseID());
			fc.home_page().logout(driver);
			driver = fc.loginpage().loginWithUserNameAndPassword(driver, Login_id, "admin123");
			
			imc.InfoMgrDashboard(driver);
			fc.utobj().printTestStep("IM Dashboard Franchisee System Summary Report Verififcation Start");
			WebElement ThirtyDays = imdp.NewCorporateLoc30DaysCount1;
			WebElement SixMonths = imdp.NewCorporateLoc6MonthsCount1;
			WebElement TwelveMonths = imdp.NewCorporateLoc12MonthsCount1;
			fc.utobj().isElementPresent(driver, ThirtyDays);
			fc.utobj().printTestStep("New Corporate Locations Count in last 30 days is coming as Expected value 1");
			fc.utobj().isElementPresent(driver, SixMonths);
			fc.utobj().printTestStep("New Corporate Locations Count in last 6 Months is coming as Expected value 1");
			fc.utobj().isElementPresent(driver, TwelveMonths);
			fc.utobj().printTestStep("New Corporate Locations Count in last 12 Months is coming as Expected value 1");
			
			String TotalNewLoc30Days = imdp.TotalNewLocation30Days.getText();
			//String ExpValue30Days ="1";
			//Assert.assertEquals(TotalNewLoc, ExpValue30Days);
			if(!"1".equalsIgnoreCase(TotalNewLoc30Days))
			{
				fc.utobj().printTestStep("Total New Locations in Last 30 Days Count not matched with Expected Value 1");
			}
			fc.utobj().printTestStep("Total New Locations Count in last 30 days is coming as Expected value 1");
			String TotalNewLoc6Months = imdp.TotalNewLocation6Months.getText();
			if(!"1".equalsIgnoreCase(TotalNewLoc6Months))
			{
				fc.utobj().printTestStep("Total New Locations in Last 6 Months  Count not matched with Expected Value 1");
			}
			fc.utobj().printTestStep("Total New Locations Count in last 6 Months is coming as Expected value 1");
			String TotalNewLoc12Months = imdp.TotalNewLocation12Months.getText();
			if(!"1".equalsIgnoreCase(TotalNewLoc12Months))
			{
				fc.utobj().printTestStep("Total New Locations in Last 12 Months  Count not matched with Expected Value 1");
			}		
			fc.utobj().printTestStep("Total New Locations Count in last 12 Months is coming as Expected value 1");
			
			String TotalActiveFranchisees = imdp.TotalActiveFranchisees.getText();
			if(!"1".equalsIgnoreCase(TotalActiveFranchisees))
			{
				fc.utobj().printTestStep("Total Active Corporate Locations Count not matched with Expected Value 1");
			}
			fc.utobj().printTestStep("Total Active Franchisee Count is coming as Expected value 1");
			String TotalActiveLocations = imdp.TotalActiveLocations.getText();
			if(!"1".equalsIgnoreCase(TotalActiveLocations))
			{
				fc.utobj().printTestStep("Total Active Franchisee Count not matched with Expected Value 1");
			}	
			fc.utobj().printTestStep("Total Active Locations Count is coming as Expected value 1");
			fc.utobj().clickElement(driver, imdp.NewCorporateLoc30DaysCount1);
			fc.utobj().assertPageSource(driver,location.getFranchiseID() );
			fc.utobj().printTestStep("Created Franchisee location is verified after clicks on New Corporate Locations created in Last 30 Days count");
			fc.utobj().printTestStep("IM Dashboard Franchisee System Summary Report Verififcation Completed");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	@Test(groups = { "infomgr","IM_Dashboard" })
	@TestCase(createdOn = "2018-07-04", updatedOn = "2018-07-04", testCaseId = "TC_IM_Dashboard_DivisionalUser_CorporateLast6Months", testCaseDescription = "To verify the IM Dashboard Franchisee System Summary Report Widget after Login via Divisional User for Corporate Location created in Last 6 Months")
	private void dashboardDiv5() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Location location = new Location();
		LocationData ld = new LocationData();
		InfoMgrDashboardPage imdp =new InfoMgrDashboardPage(driver);

		try {
			driver = fc.loginpage().login(driver);
			AdminAreaRegionAddAreaRegionPageTest region_page = new AdminAreaRegionAddAreaRegionPageTest();
			String region_name = fc.utobj().generateTestData("R1");
			region_page.addAreaRegion(driver, region_name);
			String division_name = division.addDivisionName(driver, fc.utobj().generateTestData("D1"));
			String Login_id=fc.utobj().generateTestData("Div1");
			divUser.addDivisionalUser(driver, Login_id, "admin123", division_name, "vimal.sharma@franconnect.com");
			location = fc.commonMethods().getDefaultDataFor_Location(location);
			fc.adminpage().adminAddNewFranchiseLocationLnk(driver);
			location.setAreaRegion(region_name);
			location.setBrands(division_name);
			location.setCorporateLocation("Yes");
			location.setOpeningDate(fc.utobj().getFutureDateUSFormat(-33));
			ld.filllocationInfo(driver, location).submit(driver);
			fc.adminpage().manageFranchiseLocationLnk(driver).SearchFranchise(driver, location).VerifyLocationExists(driver, location);
			System.out.println(location.getFranchiseID());
			fc.home_page().logout(driver);
			driver = fc.loginpage().loginWithUserNameAndPassword(driver, Login_id, "admin123");
			
			imc.InfoMgrDashboard(driver);
			fc.utobj().printTestStep("IM Dashboard Franchisee System Summary Report Verififcation Start");
			String ThirtyDays = imdp.NewCorporateLoc30DaysGetTxt.getText();
			if(!"--".equalsIgnoreCase(ThirtyDays))
			{
				fc.utobj().printTestStep("New Corporate Locations in Last 30 Days Count not matched with Expected Value 1");
			}
			fc.utobj().printTestStep("New Corporate Locations Count in last 30 days is coming as Expected value --");
						
			WebElement SixMonths = imdp.NewCorporateLoc6MonthsCount1;
			WebElement TwelveMonths = imdp.NewCorporateLoc12MonthsCount1;
			fc.utobj().isElementPresent(driver, SixMonths);
			fc.utobj().printTestStep("New Corporate Locations Count in last 6 Months is coming as Expected value 1");
			fc.utobj().isElementPresent(driver, TwelveMonths);
			fc.utobj().printTestStep("New Corporate Locations Count in last 12 Months is coming as Expected value 1");
			
			String TotalNewLoc30Days = imdp.TotalNewLocation30Days.getText();
			//String ExpValue30Days ="1";
			//Assert.assertEquals(TotalNewLoc, ExpValue30Days);
			if(!"1".equalsIgnoreCase(TotalNewLoc30Days))
			{
				fc.utobj().printTestStep("Total New Locations in Last 30 Days Count not matched with Expected Value 1");
			}
			fc.utobj().printTestStep("Total New Locations Count in last 30 days is coming as Expected value 1");
			String TotalNewLoc6Months = imdp.TotalNewLocation6Months.getText();
			if(!"1".equalsIgnoreCase(TotalNewLoc6Months))
			{
				fc.utobj().printTestStep("Total New Locations in Last 6 Months  Count not matched with Expected Value 1");
			}
			fc.utobj().printTestStep("Total New Locations Count in last 6 Months is coming as Expected value 1");
			String TotalNewLoc12Months = imdp.TotalNewLocation12Months.getText();
			if(!"1".equalsIgnoreCase(TotalNewLoc12Months))
			{
				fc.utobj().printTestStep("Total New Locations in Last 12 Months  Count not matched with Expected Value 1");
			}		
			fc.utobj().printTestStep("Total New Locations Count in last 12 Months is coming as Expected value 1");
			
			String TotalActiveFranchisees = imdp.TotalActiveCorporateLocations.getText();
			if(!"1".equalsIgnoreCase(TotalActiveFranchisees))
			{
				fc.utobj().printTestStep("Total Active Corporate Locations Count not matched with Expected Value 1");
			}
			fc.utobj().printTestStep("Total Active Franchisee Count is coming as Expected value 1");
			String TotalActiveLocations = imdp.TotalActiveLocations.getText();
			if(!"1".equalsIgnoreCase(TotalActiveLocations))
			{
				fc.utobj().printTestStep("Total Active Franchisee Count not matched with Expected Value 1");
			}	
			fc.utobj().printTestStep("Total Active Locations Count is coming as Expected value 1");
			fc.utobj().clickElement(driver, imdp.NewCorporateLoc6MonthsCount1);
			fc.utobj().assertPageSource(driver,location.getFranchiseID() );
			fc.utobj().printTestStep("Created Franchisee location is verified after clicks on New Corporate Locations created in Last 6 Months count");
			fc.utobj().printTestStep("IM Dashboard Franchisee System Summary Report Verififcation Completed");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	@Test(groups = { "infomgr","IM_Dashboard" })
	@TestCase(createdOn = "2018-07-04", updatedOn = "2018-07-04", testCaseId = "TC_IM_Dashboard_DivisionalUser_CorporateLast12Months", testCaseDescription = "To verify the IM Dashboard Franchisee System Summary Report Widget after Login via Divisional User for Corporate Location in Last 12 Months")
	private void dashboardDiv6() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Location location = new Location();
		LocationData ld = new LocationData();
		InfoMgrDashboardPage imdp =new InfoMgrDashboardPage(driver);

		try {
			driver = fc.loginpage().login(driver);
			AdminAreaRegionAddAreaRegionPageTest region_page = new AdminAreaRegionAddAreaRegionPageTest();
			String region_name = fc.utobj().generateTestData("R1");
			region_page.addAreaRegion(driver, region_name);
			String division_name = division.addDivisionName(driver, fc.utobj().generateTestData("D1"));
			String Login_id=fc.utobj().generateTestData("Div1");
			divUser.addDivisionalUser(driver, Login_id, "admin123", division_name, "vimal.sharma@franconnect.com");
			location = fc.commonMethods().getDefaultDataFor_Location(location);
			fc.adminpage().adminAddNewFranchiseLocationLnk(driver);
			location.setAreaRegion(region_name);
			location.setBrands(division_name);
			location.setCorporateLocation("Yes");
			location.setOpeningDate(fc.utobj().getFutureDateUSFormat(-183));
			ld.filllocationInfo(driver, location).submit(driver);
			fc.adminpage().manageFranchiseLocationLnk(driver).SearchFranchise(driver, location).VerifyLocationExists(driver, location);
			System.out.println(location.getFranchiseID());
			fc.home_page().logout(driver);
			driver = fc.loginpage().loginWithUserNameAndPassword(driver, Login_id, "admin123");
			
			imc.InfoMgrDashboard(driver);
			fc.utobj().printTestStep("IM Dashboard Franchisee System Summary Report Verififcation Start");
			String ThirtyDays = imdp.NewCorporateLoc30DaysGetTxt.getText();
			if(!"--".equalsIgnoreCase(ThirtyDays))
			{
				fc.utobj().printTestStep("New Corporate Locations Count in last 30 days is not matched with Expected value --");
			}
			fc.utobj().printTestStep("New Corporate Locations Count in last 30 days is matched with Expected value --");
			
			String SixMonths = imdp.NewCorporateLoc6MonthsGetTxt.getText();
			if(!"--".equalsIgnoreCase(SixMonths))
			{
				fc.utobj().printTestStep("New Corporate Locations Count in last 6 Months is not matched with Expected value --");
			}
			
			fc.utobj().printTestStep("New Corporate Locations Count in last 6 Months is matched with Expected value --");
			
			
			WebElement TwelveMonths = imdp.NewCorporateLoc12MonthsCount1;
			
			
			fc.utobj().isElementPresent(driver, TwelveMonths);
			fc.utobj().printTestStep("New Corporate Locations Count in last 12 Months is coming as Expected value 1");
			
			String TotalNewLoc30Days = imdp.TotalNewLocation30Days.getText();
			//String ExpValue30Days ="1";
			//Assert.assertEquals(TotalNewLoc, ExpValue30Days);
			if(!"--".equalsIgnoreCase(TotalNewLoc30Days))
			{
				fc.utobj().printTestStep("Total New Locations in Last 30 Days Count not matched with Expected Value --");
			}
			fc.utobj().printTestStep("Total New Locations Count in last 30 days is coming as Expected value --");
			String TotalNewLoc6Months = imdp.TotalNewLocation6Months.getText();
			if(!"--".equalsIgnoreCase(TotalNewLoc6Months))
			{
				fc.utobj().printTestStep("Total New Locations in Last 6 Months  Count not matched with Expected Value --");
			}
			fc.utobj().printTestStep("Total New Locations Count in last 6 Months is coming as Expected value 1");
			String TotalNewLoc12Months = imdp.TotalNewLocation12Months.getText();
			if(!"1".equalsIgnoreCase(TotalNewLoc12Months))
			{
				fc.utobj().printTestStep("Total New Locations in Last 12 Months  Count not matched with Expected Value 1");
			}		
			fc.utobj().printTestStep("Total New Locations Count in last 12 Months is coming as Expected value 1");
			
			String TotalActiveFranchisees = imdp.TotalActiveCorporateLocations.getText();
			if(!"1".equalsIgnoreCase(TotalActiveFranchisees))
			{
				fc.utobj().printTestStep("Total Active Corporate Locations Count not matched with Expected Value 1");
			}
			fc.utobj().printTestStep("Total Active Franchisee Count is coming as Expected value 1");
			String TotalActiveLocations = imdp.TotalActiveLocations.getText();
			if(!"1".equalsIgnoreCase(TotalActiveLocations))
			{
				fc.utobj().printTestStep("Total Active Franchisee Count not matched with Expected Value 1");
			}	
			fc.utobj().printTestStep("Total Active Locations Count is coming as Expected value 1");
			fc.utobj().clickElement(driver, imdp.NewCorporateLoc12MonthsCount1);
			fc.utobj().assertPageSource(driver,location.getFranchiseID() );
			fc.utobj().printTestStep("Created Franchisee location is verified after clicks on New Corporate Locations created in Last 12 Months count");
			fc.utobj().printTestStep("IM Dashboard Franchisee System Summary Report Verififcation Completed");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	@Test(groups = { "infomgr","IM_Dashboard" })
	@TestCase(createdOn = "2018-07-05", updatedOn = "2018-07-05", testCaseId = "TC_IM_Dashboard_Divisional_Terminated_Franchisee_in_Last_30Days_6Months_12Months", testCaseDescription = "To verify the IM Dashboard Franchisee System Summary Report Widget after Login via Divisional User for Franchise Termination in Last 30 Days 6 Months and 12 Months")
	private void dashboardDiv7() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Location location = new Location();
		LocationData ld = new LocationData();
		InfoMgrDashboardPage imdp =new InfoMgrDashboardPage(driver);
		InfoMgrDefaultAndTerminationPage imdnt =new InfoMgrDefaultAndTerminationPage(driver);
		InfoMgrBasePage imbp = new InfoMgrBasePage(driver);

		try {
			driver = fc.loginpage().login(driver);
			AdminAreaRegionAddAreaRegionPageTest region_page = new AdminAreaRegionAddAreaRegionPageTest();
			String region_name = fc.utobj().generateTestData("R1");
			region_page.addAreaRegion(driver, region_name);
			String division_name = division.addDivisionName(driver, fc.utobj().generateTestData("D1"));
			String Login_id=fc.utobj().generateTestData("Div1");
			divUser.addDivisionalUser(driver, Login_id, "admin123", division_name, "vimal.sharma@franconnect.com");
			location = fc.commonMethods().getDefaultDataFor_Location(location);
			fc.adminpage().adminAddNewFranchiseLocationLnk(driver);
			location.setAreaRegion(region_name);
			location.setBrands(division_name);
			ld.filllocationInfo(driver, location).submit(driver);
			fc.adminpage().manageFranchiseLocationLnk(driver).SearchFranchise(driver, location).VerifyLocationExists(driver, location);
			System.out.println(location.getFranchiseID());
			fc.home_page().logout(driver);
			driver = fc.loginpage().loginWithUserNameAndPassword(driver, Login_id, "admin123");
			imc.InfoMgrFranchisees(driver);
			imc.SearchFranchiseeAndClick(driver, location.getFranchiseID());
			fc.utobj().clickElement(driver, imbp.defaultTerminationTab);
			
			fc.utobj().sendKeys(driver, imdnt.txtCommitteApprovedDate, fc.utobj().getFutureDateUSFormat(-2));
			fc.utobj().sendKeys(driver, imdnt.txtDateTerminated, fc.utobj().getCurrentDateUSFormat());
			fc.utobj().selectDropDownByVisibleText(driver, imdnt.drpReason, "Abandonment");
			fc.utobj().clickElement(driver, imdnt.btnSubmit);
			//driver.switchTo().alert().accept();
			fc.utobj().acceptAlertBox(driver);
			
			imc.InfoMgrDashboard(driver);
			fc.utobj().printTestStep("IM Dashboard Franchisee System Summary Report Verififcation Start for Franchisee Terminated in Last 30 Days");
			WebElement ThirtyDays = imdp.TerminatedFranchisee30DaysCount1;
			WebElement SixMonths = imdp.TerminatedFranchisee6MonthsCount1;
			WebElement TwelveMonths = imdp.TerminatedFranchisee12MonthsCount1;
			fc.utobj().isElementPresent(driver, ThirtyDays);
			fc.utobj().printTestStep("New Franchisee Count in last 30 days is coming as Expected value 1");
			fc.utobj().isElementPresent(driver, SixMonths);
			fc.utobj().printTestStep("New Franchisee Count in last 6 Months is coming as Expected value 1");
			fc.utobj().isElementPresent(driver, TwelveMonths);
			fc.utobj().printTestStep("New Franchisee Count in last 12 Months is coming as Expected value 1");
						
			fc.utobj().clickElement(driver, imdp.TerminatedFranchisee30DaysCount1);
			fc.utobj().assertPageSource(driver,location.getFranchiseID() );
			fc.utobj().printTestStep("Terminated Franchisee location is verified after clicks on Terminated Franchisee created in Last 30 Days count");
			fc.utobj().printTestStep("IM Dashboard Franchisee System Summary Report Verififcation Completed for Termination location in Last 30 Days");
			imc.InfoMgrTerminated(driver);
			imc.SearchFranchiseeAndClick(driver, location.getFranchiseID());
			
			fc.utobj().clickElement(driver,imbp.defaultTerminationTab);
			fc.utobj().clickLink(driver, "Modify");
			fc.utobj().sendKeys(driver, imdnt.txtCommitteApprovedDate, fc.utobj().getFutureDateUSFormat(-33));
			fc.utobj().sendKeys(driver, imdnt.txtDateTerminated, fc.utobj().getFutureDateUSFormat(-33));
			fc.utobj().selectDropDownByVisibleText(driver, imdnt.drpReason, "Abandonment");
			fc.utobj().clickElement(driver, imdnt.btnSubmit);
			imc.InfoMgrDashboard(driver);
			fc.utobj().printTestStep("IM Dashboard Franchisee System Summary Report Verififcation Start for Franchisee Terminated in Last 6 Months");
			
			String ThirtyDays1 = imdp.TerminatedFranchisee30DaysCountGetTxt.getText();
			if(!"--".equalsIgnoreCase(ThirtyDays1))
			{
				fc.utobj().printTestStep("Franchise Terminations Count in last 30 days is not matched with Expected value --");
			}
			fc.utobj().printTestStep("Franchise Terminations Count in last 30 days is matched with Expected value --");
			WebElement SixMonths1 = imdp.TerminatedFranchisee6MonthsCount1;
			WebElement TwelveMonths1 = imdp.TerminatedFranchisee12MonthsCount1;
			
			fc.utobj().isElementPresent(driver, SixMonths1);
			fc.utobj().printTestStep("New Franchisee Count in last 6 Months is coming as Expected value 1");
			fc.utobj().isElementPresent(driver, TwelveMonths1);
			fc.utobj().printTestStep("New Franchisee Count in last 12 Months is coming as Expected value 1");
						
			fc.utobj().clickElement(driver, imdp.TerminatedFranchisee6MonthsCount1);
			fc.utobj().assertPageSource(driver,location.getFranchiseID() );
			fc.utobj().printTestStep("Terminated Franchisee location is verified after clicks on Terminated Franchisee created in Last 6 Months count");
			fc.utobj().printTestStep("IM Dashboard Franchisee System Summary Report Verififcation Completed for Termination location in Last 6 Months");
			
			
			imc.InfoMgrTerminated(driver);
			imc.SearchFranchiseeAndClick(driver, location.getFranchiseID());
			
			fc.utobj().clickElement(driver, imbp.defaultTerminationTab);
			fc.utobj().clickLink(driver, "Modify");
			fc.utobj().sendKeys(driver, imdnt.txtCommitteApprovedDate, fc.utobj().getFutureDateUSFormat(-185));
			fc.utobj().sendKeys(driver, imdnt.txtDateTerminated, fc.utobj().getFutureDateUSFormat(-185));
			fc.utobj().selectDropDownByVisibleText(driver, imdnt.drpReason, "Abandonment");
			fc.utobj().clickElement(driver, imdnt.btnSubmit);
			imc.InfoMgrDashboard(driver);
			fc.utobj().printTestStep("IM Dashboard Franchisee System Summary Report Verififcation Start for Franchisee Terminated in Last 6 Months");
			
			String ThirtyDays2 = imdp.TerminatedFranchisee30DaysCountGetTxt.getText();
			if(!"--".equalsIgnoreCase(ThirtyDays2))
			{
				fc.utobj().printTestStep("Franchise Terminations Count in last 30 days is not matched with Expected value --");
			}
			fc.utobj().printTestStep("Franchise Terminations Count in last 30 days is matched with Expected value --");
			
			
			String SixMonths2 = imdp.TerminatedFranchisee6MonthsCountGetTxt.getText();
			if(!"--".equalsIgnoreCase(SixMonths2))
			{
				fc.utobj().printTestStep("Franchise Terminations Count in last 6 Months is not matched with Expected value --");
			}
			fc.utobj().printTestStep("Franchise Terminations Count in last 6 Months is matched with Expected value --");
			
			WebElement TwelveMonths2 = imdp.TerminatedFranchisee12MonthsCount1;
			fc.utobj().isElementPresent(driver, TwelveMonths2);
			fc.utobj().printTestStep("New Franchisee Count in last 12 Months is coming as Expected value 1");
						
			fc.utobj().clickElement(driver, imdp.TerminatedFranchisee12MonthsCount1);
			fc.utobj().assertPageSource(driver,location.getFranchiseID() );
			fc.utobj().printTestStep("Terminated Franchisee location is verified after clicks on Terminated Franchisee created in Last 12 Months count");
			fc.utobj().printTestStep("IM Dashboard Franchisee System Summary Report Verififcation Completed for Termination location in Last 12 Months");
			
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(groups = { "infomgr","IM_Dashboard" })
	@TestCase(createdOn = "2018-07-06", updatedOn = "2018-07-06", testCaseId = "TC_IM_Dashboard_Widget_Maximise_Minimise_Hide_DivisionalUser", testCaseDescription = "To verify the IM Dashboard Widgets Minimise_Maximise_Hide Options via Divisional User")
	private void dashboardDiv8() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			driver = fc.loginpage().login(driver);
			String division_name = division.addDivisionName(driver, fc.utobj().generateTestData("D1"));
			String Login_id=fc.utobj().generateTestData("Div1");
			divUser.addDivisionalUser(driver, Login_id, "admin123", division_name, "vimal.sharma@franconnect.com");
			fc.home_page().logout(driver);
			driver = fc.loginpage().loginWithUserNameAndPassword(driver, Login_id, "admin123");
			
			imc.InfoMgrDashboard(driver);
			
			InfoMgrDashboardPage imdp = new InfoMgrDashboardPage(driver);
			
			//Info Mgr Franchisee System Summary Report Widget
			
			boolean SearchElementPresent = imdp.FranchiseSystemSummaryReportWidget.isDisplayed();
			if (SearchElementPresent == true) {
				fc.utobj().moveToElementThroughAction(driver, imdp.FranchiseSystemSummaryReportWidgetMinMaxHover);
				fc.utobj().printTestStep("Click on Franchisee System Summary Report Minimise widget option");
				fc.utobj().clickElement(driver, imdp.FranchiseSystemSummaryReportWidgetMinMax);
				boolean SearchElementPresent1 = imdp.FranchiseSystemSummaryReportWidget.isDisplayed();
				if (SearchElementPresent1 == false) {
					fc.utobj().printTestStep("Franchisee System Summary Report Widget is minimized successfully");
					fc.utobj().printTestStep("Now Click on Franchisee System Summary Report Widget Maximize widget option");
					fc.utobj().moveToElementThroughAction(driver, imdp.FranchiseSystemSummaryReportWidgetMinMaxHover);
					fc.utobj().clickElement(driver, imdp.FranchiseSystemSummaryReportWidgetMinMax);

					boolean SearchElementPresent2 = imdp.FranchiseSystemSummaryReportWidget.isDisplayed();
					if (SearchElementPresent2 == true) {
						fc.utobj().printTestStep("Franchisee System Summary Report Widget is Maximized successfully");
					} else {
						fc.utobj().printTestStep("Franchisee System Summary ReportWidget is not Maximized");
					}
				} else {
					fc.utobj().printTestStep("Franchisee System Summary ReportWidget is not minimised");
				}
			} else {
				fc.utobj().moveToElementThroughAction(driver,imdp.FranchiseSystemSummaryReportWidgetMinMaxHover);
				fc.utobj().printTestStep("Click on Maximise widget option");
				fc.utobj().clickElement(driver,imdp.FranchiseSystemSummaryReportWidgetMinMax);
				boolean SearchElementPresent3 = imdp.FranchiseSystemSummaryReportWidget.isDisplayed();
				if (SearchElementPresent3 == true) {
					fc.utobj().printTestStep("Franchisee System Summary Report Widget is Maximised successfully");
					fc.utobj().printTestStep("Now Click on Franchisee System Summary Report Widget Minimise widget option");
					fc.utobj().moveToElementThroughAction(driver, imdp.FranchiseSystemSummaryReportWidgetMinMaxHover);
					fc.utobj().clickElement(driver, imdp.FranchiseSystemSummaryReportWidgetMinMax);
					boolean SearchElementPresent4 = imdp.FranchiseSystemSummaryReportWidget.isDisplayed();
					if (SearchElementPresent4 == false) {
						fc.utobj().printTestStep("Franchisee System Summary Report Widget is minimised successfully");
					} else {
						fc.utobj().printTestStep("Franchisee System Summary Report Widget is not Minimised");
					}
				} else {
					fc.utobj().printTestStep("Franchisee System Summary Report Widget is not maximised");
				}
			}

			fc.utobj().moveToElementThroughAction(driver,imdp.FranchiseSystemSummaryReportWidget);
			fc.utobj().printTestStep("Click on Hide widget option");
			fc.utobj().clickElement(driver,imdp.FranchiseSystemSummaryReportWidgetHideSignX);
			fc.utobj().acceptAlertBox(driver);
			boolean WidgetHide = imdp.FranchiseSystemSummaryReportWidget.isDisplayed();
			if (WidgetHide == false) {
				fc.utobj().printTestStep("Franchisee System Summary Report Widget is Hide Sucessfully");
			} else {
				fc.utobj().printTestStep("Franchisee System Summary Report Widget is not Hide");
			}

			
			//Info Mgr "Franchisees in Last 5 Yrs. and Total Franchisees" Widget
			
			
			
			boolean FranchiseeGraphWidget1 = imdp.GraphWidget.isDisplayed();
			if (FranchiseeGraphWidget1 == true) {
				fc.utobj().moveToElementThroughAction(driver, imdp.GraphWidgetMinMaxHover);
				fc.utobj().printTestStep("Click on Franchisees in Last 5 Yrs. and Total Franchisees Minimise widget option");
				fc.utobj().clickElement(driver, imdp.GraphWidgetMinMax);
				boolean FranchiseeGraphWidget2 = imdp.GraphWidget.isDisplayed();
				if (FranchiseeGraphWidget2 == false) {
					fc.utobj().printTestStep("Franchisees in Last 5 Yrs. and Total Franchisees Widget is minimized successfully");
					fc.utobj().printTestStep("Now Click on Franchisees in Last 5 Yrs. and Total Franchisees Widget Maximize widget option");
					fc.utobj().moveToElementThroughAction(driver, imdp.GraphWidgetMinMaxHover);
					fc.utobj().clickElement(driver, imdp.GraphWidgetMinMax);

					boolean FranchiseeGraphWidget3 = imdp.GraphWidget.isDisplayed();
					if (FranchiseeGraphWidget3 == true) {
						fc.utobj().printTestStep("Franchisees in Last 5 Yrs. and Total Franchisees Widget is Maximized successfully");
					} else {
						fc.utobj().printTestStep("Franchisees in Last 5 Yrs. and Total Franchisees Widget is not Maximized");
					}
				} else {
					fc.utobj().printTestStep("Franchisees in Last 5 Yrs. and Total Franchisees Widget is not minimised");
				}
			} else {
				fc.utobj().moveToElementThroughAction(driver,imdp.GraphWidgetMinMaxHover);
				fc.utobj().printTestStep("Click on Maximise widget option");
				fc.utobj().clickElement(driver,imdp.GraphWidgetHideSignX);
				boolean FranchiseeGraphWidget4 = imdp.GraphWidget.isDisplayed();
				if (FranchiseeGraphWidget4 == true) {
					fc.utobj().printTestStep("Franchisees in Last 5 Yrs. and Total Franchisees Widget is Maximised successfully");
					fc.utobj().printTestStep("Now Click on Franchisees in Last 5 Yrs. and Total Franchisees Widget Minimise widget option");
					fc.utobj().moveToElementThroughAction(driver, imdp.GraphWidgetMinMaxHover);
					fc.utobj().clickElement(driver, imdp.GraphWidgetMinMax);
					boolean FranchiseeGraphWidget5 = imdp.GraphWidget.isDisplayed();
					if (FranchiseeGraphWidget5 == false) {
						fc.utobj().printTestStep("Franchisees in Last 5 Yrs. and Total Franchisees Widget is minimised successfully");
					} else {
						fc.utobj().printTestStep("Franchisees in Last 5 Yrs. and Total Franchisees Widget is not Minimised");
					}
				} else {
					fc.utobj().printTestStep("Franchisees in Last 5 Yrs. and Total Franchisees Widget is not maximised");
				}
			}

			fc.utobj().moveToElementThroughAction(driver,imdp.GraphWidgetMinMaxHover);
			fc.utobj().printTestStep("Click on Hide widget option");
			fc.utobj().clickElement(driver, imdp.GraphWidgetHideSignX);
			fc.utobj().acceptAlertBox(driver);
			boolean WidgetHide1 = imdp.GraphWidget.isDisplayed();
			if (WidgetHide1 == false) {
				fc.utobj().printTestStep("Franchisees in Last 5 Yrs. and Total Franchisees Widget is Hide Sucessfully");
			} else {
				fc.utobj().printTestStep("Franchisees in Last 5 Yrs. and Total Franchisees Widget is not Hide");
			}

			
			//Info Mgr "Task View" Widget
			
			boolean TaskWidgetPresent1 = imdp.TaskViewWidget.isDisplayed();
			if (TaskWidgetPresent1 == true) {
				fc.utobj().moveToElementThroughAction(driver,imdp.TaskViewWidgetMinMaxHover);
				fc.utobj().printTestStep("Click on Task View Minimise widget option");
				fc.utobj().clickElement(driver,imdp.TaskViewWidgetMinMax);
				boolean TaskWidgetPresent2 = imdp.TaskViewWidget.isDisplayed();
				if (TaskWidgetPresent2 == false) {
					fc.utobj().printTestStep("Task View Widget is minimized successfully");
					fc.utobj().printTestStep("Now Click on Task View Widget Maximize widget option");
					fc.utobj().moveToElementThroughAction(driver,imdp.TaskViewWidgetMinMaxHover);
					fc.utobj().clickElement(driver, imdp.TaskViewWidgetMinMax);

					boolean TaskWidgetPresent3 = imdp.TaskViewWidget.isDisplayed();
					if (TaskWidgetPresent3 == true) {
						fc.utobj().printTestStep("Task View Widget is Maximized successfully");
					} else {
						fc.utobj().printTestStep("Task View Widget is not Maximized");
					}
				} else {
					fc.utobj().printTestStep("Task View Widget is not minimised");
				}
			} else {
				fc.utobj().moveToElementThroughAction(driver,imdp.TaskViewWidgetMinMaxHover);
				fc.utobj().printTestStep("Click on Maximise widget option");
				fc.utobj().clickElement(driver, imdp.TaskViewWidgetMinMax);
				boolean TaskWidgetPresent4 =imdp.TaskViewWidget.isDisplayed();
				if (TaskWidgetPresent4 == true) {
					fc.utobj().printTestStep("Task View Widget is Maximised successfully");
					fc.utobj().printTestStep("Now Click on Task View Widget Minimise widget option");
					fc.utobj().moveToElementThroughAction(driver,imdp.TaskViewWidgetMinMaxHover);
					fc.utobj().clickElement(driver,imdp.TaskViewWidgetMinMax);
					boolean TaskWidgetPresent5 = imdp.TaskViewWidget.isDisplayed();
					if (TaskWidgetPresent5 == false) {
						fc.utobj().printTestStep("Task View Widget is minimised successfully");
					} else {
						fc.utobj().printTestStep("Task View Widget is not Minimised");
					}
				} else {
					fc.utobj().printTestStep("Task View Widget is not maximised");
				}
			}

			fc.utobj().moveToElementThroughAction(driver,imdp.TaskViewWidgetMinMaxHover);
			fc.utobj().printTestStep("Click on Hide widget option");
			fc.utobj().clickElement(driver,imdp.TaskViewWidgetHideSignX);
			fc.utobj().acceptAlertBox(driver);
			boolean TaskWidgetHide = imdp.TaskViewWidget.isDisplayed();
			if (TaskWidgetHide == false) {
				fc.utobj().printTestStep("Task View Widget is Hide Sucessfully");
			} else {
				fc.utobj().printTestStep("Task View Widget is not Hide");
			}
			
			
			
			//Info Mgr "Renewals Due" Widget
			
			boolean RenewalsDueWidgetPresent1 = imdp.RenewalDueWidget.isDisplayed();
			if (RenewalsDueWidgetPresent1 == true) {
				fc.utobj().moveToElementThroughAction(driver, imdp.RenewalDueWidgetMinMaxHover);
				fc.utobj().printTestStep("Click on Renewals Due Minimise widget option");
				fc.utobj().clickElement(driver, imdp.RenewalDueWidgetMinMax);
				boolean RenewalsDueWidgetPresent2 = imdp.RenewalDueWidget.isDisplayed();
				if (RenewalsDueWidgetPresent2 == false) {
					fc.utobj().printTestStep("Renewals Due Widget is minimized successfully");
					fc.utobj().printTestStep("Now Click on Renewals Due Widget Maximize widget option");
					fc.utobj().moveToElementThroughAction(driver, imdp.RenewalDueWidgetMinMaxHover);
					fc.utobj().clickElement(driver, imdp.RenewalDueWidgetMinMax);

					boolean RenewalsDueWidgetPresent3 = imdp.RenewalDueWidget.isDisplayed();
					if (RenewalsDueWidgetPresent3 == true) {
						fc.utobj().printTestStep("Renewals Due Widget is Maximized successfully");
					} else {
						fc.utobj().printTestStep("Renewals Due Widget is not Maximized");
					}
				} else {
					fc.utobj().printTestStep("Renewals Due Widget is not minimised");
				}
			} else {
				fc.utobj().moveToElementThroughAction(driver,imdp.RenewalDueWidgetMinMaxHover);
				fc.utobj().printTestStep("Click on Maximise widget option");
				fc.utobj().clickElement(driver, imdp.RenewalDueWidgetMinMax);
				boolean RenewalsDueWidgetPresent4 = imdp.RenewalDueWidget.isDisplayed();
				if (RenewalsDueWidgetPresent4 == true) {
					fc.utobj().printTestStep("Renewals Due Widget is Maximised successfully");
					fc.utobj().printTestStep("Now Click on Renewals Due Widget Minimise widget option");
					fc.utobj().moveToElementThroughAction(driver,imdp.RenewalDueWidgetMinMaxHover);
					fc.utobj().clickElement(driver, imdp.RenewalDueWidgetMinMax);
					boolean RenewalsDueWidgetPresent5 = imdp.RenewalDueWidget.isDisplayed();
					if (RenewalsDueWidgetPresent5 == false) {
						fc.utobj().printTestStep("Renewals Due Widget is minimised successfully");
					} else {
						fc.utobj().printTestStep("Renewals Due Widget is not Minimised");
					}
				} else {
					fc.utobj().printTestStep("Renewals Due Widget is not maximised");
				}
			}

			fc.utobj().moveToElementThroughAction(driver,imdp.RenewalDueWidgetMinMaxHover);
			fc.utobj().printTestStep("Click on Hide widget option");
			fc.utobj().clickElement(driver, imdp.RenewalDueWidgetHideSignX);
			fc.utobj().acceptAlertBox(driver);
			boolean RenewalsDueWidgetHide = imdp.RenewalDueWidget.isDisplayed();
			if (RenewalsDueWidgetHide == false) {
				fc.utobj().printTestStep("Renewals Due Widget is Hide Sucessfully");
			} else {
				fc.utobj().printTestStep("Renewals Due Widget is not Hide");
			}
			
			
			
			//Info Mgr "Email History" Widget
			
			boolean EmailHistoryWidgetPresent1 = imdp.EmailHistoryWidget.isDisplayed();
			if (EmailHistoryWidgetPresent1 == true) {
				fc.utobj().moveToElementThroughAction(driver,imdp.EmailHistoryWidgetMinMaxHover);
				fc.utobj().printTestStep("Click on Email History Minimise widget option");
				fc.utobj().clickElement(driver, imdp.EmailHistoryWidgetMinMax);
				boolean EmailHistoryWidgetPresent2 = imdp.EmailHistoryWidget.isDisplayed();
				if (EmailHistoryWidgetPresent2 == false) {
					fc.utobj().printTestStep("Email History Widget is minimized successfully");
					fc.utobj().printTestStep("Now Click on Email History Widget Maximize widget option");
					fc.utobj().moveToElementThroughAction(driver,imdp.EmailHistoryWidgetMinMaxHover);
					fc.utobj().clickElement(driver, imdp.EmailHistoryWidgetMinMax);

					boolean EmailHistoryWidgetPresent3 = imdp.EmailHistoryWidget.isDisplayed();
					if (EmailHistoryWidgetPresent3 == true) {
						fc.utobj().printTestStep("Email History Widget is Maximized successfully");
					} else {
						fc.utobj().printTestStep("Email History Widget is not Maximized");
					}
				} else {
					fc.utobj().printTestStep("Email History Widget is not minimised");
				}
			} else {
				fc.utobj().moveToElementThroughAction(driver,imdp.EmailHistoryWidgetMinMaxHover);
				fc.utobj().printTestStep("Click on Maximise widget option");
				fc.utobj().clickElement(driver, imdp.EmailHistoryWidgetMinMax);
				boolean EmailHistoryWidgetPresent4 = imdp.EmailHistoryWidget.isDisplayed();
				if (EmailHistoryWidgetPresent4 == true) {
					fc.utobj().printTestStep("Email History Widget is Maximised successfully");
					fc.utobj().printTestStep("Now Click on Email History Widget Minimise widget option");
					fc.utobj().moveToElementThroughAction(driver,imdp.EmailHistoryWidgetMinMaxHover);
					fc.utobj().clickElement(driver, imdp.EmailHistoryWidgetMinMax);
					boolean EmailHistoryWidgetPresent5 = imdp.EmailHistoryWidget.isDisplayed();
					if (EmailHistoryWidgetPresent5 == false) {
						fc.utobj().printTestStep("Email History Widget is minimised successfully");
					} else {
						fc.utobj().printTestStep("Email History Widget is not Minimised");
					}
				} else {
					fc.utobj().printTestStep("Email History Widget is not maximised");
				}
			}

			fc.utobj().moveToElementThroughAction(driver, imdp.EmailHistoryWidgetMinMaxHover);
			fc.utobj().printTestStep("Click on Hide widget option");
			fc.utobj().clickElement(driver,imdp.EmailHistoryWidgetHideSignX);
			fc.utobj().acceptAlertBox(driver);
			boolean EmailHistoryWidgetHide = imdp.EmailHistoryWidget.isDisplayed();
			if (EmailHistoryWidgetHide == false) {
				fc.utobj().printTestStep("Email History Widget is Hide Sucessfully");
			} else {
				fc.utobj().printTestStep("Email History Widget is not Hide");
			}
			
			
			//Info Mgr "Recent 5 Visits / Recent 5 QA History Inspections" Widget
			
			boolean RecentVisitOrQAInspectionWidgetPresent1 = imdp.RecentQaHistoryWidget.isDisplayed();
			if (RecentVisitOrQAInspectionWidgetPresent1 == true) {
				fc.utobj().moveToElementThroughAction(driver, imdp.RecentQaHistoryWidgetMinMaxHover);
				fc.utobj().printTestStep("Click on Recent 5 Visits / Recent 5 QA History Inspections Minimise widget option");
				fc.utobj().clickElement(driver, imdp.RecentQaHistoryWidgetMinMax);
				boolean RecentVisitOrQAInspectionWidgetPresent2 = imdp.RecentQaHistoryWidget.isDisplayed();
				if (RecentVisitOrQAInspectionWidgetPresent2 == false) {
					fc.utobj().printTestStep("Recent 5 Visits / Recent 5 QA History Inspections Widget is minimized successfully");
					fc.utobj().printTestStep("Now Click on Recent 5 Visits / Recent 5 QA History Inspections Widget Maximize widget option");
					fc.utobj().moveToElementThroughAction(driver,imdp.RecentQaHistoryWidgetMinMaxHover);
					fc.utobj().clickElement(driver,imdp.RecentQaHistoryWidgetMinMax);

					boolean RecentVisitOrQAInspectionWidgetPresent3 = imdp.RecentQaHistoryWidget.isDisplayed();
					if (RecentVisitOrQAInspectionWidgetPresent3 == true) {
						fc.utobj().printTestStep("Recent 5 Visits / Recent 5 QA History Inspections Widget is Maximized successfully");
					} else {
						fc.utobj().printTestStep("Recent 5 Visits / Recent 5 QA History Inspections Widget is not Maximized");
					}
				} else {
					fc.utobj().printTestStep("Recent 5 Visits / Recent 5 QA History Inspections Widget is not minimised");
				}
			} else {
				fc.utobj().moveToElementThroughAction(driver,imdp.RecentQaHistoryWidgetMinMaxHover);
				fc.utobj().printTestStep("Click on Maximise widget option");
				fc.utobj().clickElement(driver, imdp.RecentQaHistoryWidgetMinMax);
				boolean RecentVisitOrQAInspectionWidgetPresent4 = imdp.RecentQaHistoryWidget.isDisplayed();
				if (RecentVisitOrQAInspectionWidgetPresent4 == true) {
					fc.utobj().printTestStep("Recent 5 Visits / Recent 5 QA History Inspections Widget is Maximised successfully");
					fc.utobj().printTestStep("Now Click on Recent 5 Visits / Recent 5 QA History Inspections Widget Minimise widget option");
					fc.utobj().moveToElementThroughAction(driver,imdp.RecentQaHistoryWidgetMinMaxHover);
					fc.utobj().clickElement(driver,imdp.RecentQaHistoryWidgetMinMax);
					boolean RecentVisitOrQAInspectionWidgetPresent5 = imdp.RecentQaHistoryWidget.isDisplayed();
					if (RecentVisitOrQAInspectionWidgetPresent5 == false) {
						fc.utobj().printTestStep("Recent 5 Visits / Recent 5 QA History Inspections Widget is minimised successfully");
					} else {
						fc.utobj().printTestStep("Recent 5 Visits / Recent 5 QA History Inspections Widget is not Minimised");
					}
				} else {
					fc.utobj().printTestStep("Recent 5 Visits / Recent 5 QA History Inspections Widget is not maximised");
				}
			}

			fc.utobj().moveToElementThroughAction(driver,imdp.RecentQaHistoryWidgetMinMaxHover);
			fc.utobj().printTestStep("Click on Hide widget option");
			fc.utobj().clickElement(driver, imdp.RecentQaHistoryWidgetHideSignX);
			fc.utobj().acceptAlertBox(driver);
			boolean RecentVisitOrQAInspectionWidgetHide = imdp.RecentQaHistoryWidget.isDisplayed();
			if (RecentVisitOrQAInspectionWidgetHide == false) {
				fc.utobj().printTestStep("Recent 5 Visits / Recent 5 QA History Inspections Widget is Hide Sucessfully");
			} else {
				fc.utobj().printTestStep("Recent 5 Visits / Recent 5 QA History Inspections Widget is not Hide");
			}
			
						

			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	@Test(groups = { "infomgr","IM_Dashboard" })
	@TestCase(createdOn = "2018-07-09", updatedOn = "2018-07-09", testCaseId = "TC_IM_Dashboard_Widget_ManageWidgets_Divisional_User", testCaseDescription = "To verify the IM Dashboard Manage Widgets Options via Divisional User")
	private void dashboardDiv9() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		InfoMgrDashboardPage imdp = new InfoMgrDashboardPage(driver);
		try {
			driver = fc.loginpage().login(driver);			
			String division_name = division.addDivisionName(driver, fc.utobj().generateTestData("D1"));
			String Login_id=fc.utobj().generateTestData("Div1");
			divUser.addDivisionalUser(driver, Login_id, "admin123", division_name, "vimal.sharma@franconnect.com");
			fc.home_page().logout(driver);
			driver = fc.loginpage().loginWithUserNameAndPassword(driver, Login_id, "admin123");
			
			// Navigate to Manage Widget Option and Remove Hide all Widgets from Info Mgr Dashaboard 
			
			imc.InfoMgrDashboard(driver);
			fc.utobj().printTestStep("Click on Info Mgr Manage Widget Option at Dashboard to hide all widgets");
			
			
			fc.utobj().clickElement(driver, imdp.manageWidget);
			fc.utobj().clickElement(driver, imdp.mngEmailHistoryWidget);
			fc.utobj().clickElement(driver, imdp.mngSummaryReportWidget);
			fc.utobj().clickElement(driver, imdp.mngTotalFranchiseeWidget);
			fc.utobj().clickElement(driver, imdp.mngRecentVisitWidget);
			fc.utobj().clickElement(driver, imdp.mngRenewalDueWidget);
			fc.utobj().clickElement(driver, imdp.mngTaskViewWidget);
			fc.utobj().clickElement(driver, imdp.mngWidgetSaveButton);
			
			fc.utobj().printTestStep("Info Mgr Dashboard all widgets are hide through Manage Widget Option and now verifying that all widgets are hide from dashboard");
			
			//Verify Franchise System Summary Report Widget is not displaying after hide through Manage Widget Option
			
			boolean FranchiseeSystemSummaryReportWidgetPresent = imdp.SummaryReportWidgetVisible.isDisplayed();
			if(FranchiseeSystemSummaryReportWidgetPresent==false)
			{
				fc.utobj().printTestStep("Franchise System Summary Report Widget is Hide Successfully through manage Widget Option ");
			}
			else
			{
				fc.utobj().printTestStep("Franchise System Summary Report Widget is not getting Hide through manage Widget Option ");
			}
			
			//Verify Franchisees in Last 5 Yrs. and Total Franchisees Widget is not displaying after hide through Manage Widget Option
			
			boolean TotalFranchiseeGraphWidgetPresent = imdp.TotalFranchiseeWidgetVisible.isDisplayed();
			if(TotalFranchiseeGraphWidgetPresent==false)
			{
				fc.utobj().printTestStep("Franchisees in Last 5 Yrs. and Total Franchisees Widget is Hide Successfully through manage Widget Option ");
			}
			else
			{
				fc.utobj().printTestStep("Franchisees in Last 5 Yrs. and Total Franchisees Widget is not getting Hide through manage Widget Option ");
			}
				
			//Verify Task View Widget is not displaying after hide through Manage Widget Option
			
			boolean TaskViewWidgetPresent = imdp.TaskViewWidgetVisible.isDisplayed();
			if(TaskViewWidgetPresent==false)
			{
				fc.utobj().printTestStep("Task View Widget is Hide Successfully through manage Widget Option ");
			}
			else
			{
				fc.utobj().printTestStep("Task View Widget is not getting Hide through manage Widget Option ");
			}
			
			//Verify Renewals Due Widget is not displaying after hide through Manage Widget Option
			
			boolean RenewalsDueWidgetPresent = imdp.RenewalDueWidgetVisible.isDisplayed();
			if(RenewalsDueWidgetPresent==false)
			{
				fc.utobj().printTestStep("Renewals Due Widget is Hide Successfully through manage Widget Option ");
			}
			else
			{
				fc.utobj().printTestStep("Renewals Due Widget is not getting Hide through manage Widget Option ");
			}		
			
			//Verify Email History Widget is not displaying after hide through Manage Widget Option
			
			boolean EmailHistoryWidgetPresent = imdp.EmailHistoryReportVisible.isDisplayed();
			if(EmailHistoryWidgetPresent==false)
			{
				fc.utobj().printTestStep("Email History Widget is Hide Successfully through manage Widget Option ");
			}
			else
			{
				fc.utobj().printTestStep("Email History Widget is not getting Hide through manage Widget Option ");
			}		
			
			//Verify Recent 5 Widget / QA Inspections Widget is not displaying after hide through Manage Widget Option
			
			boolean QAHistoryWidgetPresent = imdp.RecentVisitWidgetVisible.isDisplayed();
			if(QAHistoryWidgetPresent==false)
			{
				fc.utobj().printTestStep("Recent 5 Widget / QA Inspections Widget is Hide Successfully through manage Widget Option ");
			}
			else
			{
				fc.utobj().printTestStep("Recent 5 Widget / QA Inspections Widget is not getting Hide through manage Widget Option ");
			}
			
			//Click on Info Mgr Manage Widget Option at Dashboard to Unhide all Widgets
			
			fc.utobj().printTestStep("Click on Info Mgr Manage Widget Option at Dashboard to Unhide all Widgets");
			fc.utobj().clickElement(driver, imdp.manageWidget);
			fc.utobj().clickElement(driver, imdp.mngEmailHistoryWidget);
			fc.utobj().clickElement(driver, imdp.mngSummaryReportWidget);
			fc.utobj().clickElement(driver, imdp.mngTotalFranchiseeWidget);
			fc.utobj().clickElement(driver, imdp.mngRecentVisitWidget);
			fc.utobj().clickElement(driver, imdp.mngRenewalDueWidget);
			fc.utobj().clickElement(driver, imdp.mngTaskViewWidget);
			fc.utobj().clickElement(driver, imdp.mngWidgetSaveButton);
			
			fc.utobj().printTestStep("Info Mgr Dashboard all widgets are unhide through Manage Widget Option and now verifying that all widgets are visible on dashboard");
			
			//Verify Franchise System Summary Report Widget is displaying after unhide through Manage Widget Option
			
			boolean FranchiseeSystemSummaryReportWidgetPresent1 = imdp.SummaryReportWidgetVisible.isDisplayed();
			if(FranchiseeSystemSummaryReportWidgetPresent1==true)
			{
				fc.utobj().printTestStep("Franchise System Summary Report Widget is unhide Successfully through manage Widget Option ");
			}
			else
			{
				fc.utobj().printTestStep("Franchise System Summary Report Widget is not getting unhide through manage Widget Option ");
			}
			
			//Verify Franchisees in Last 5 Yrs. and Total Franchisees Widget is displaying after unhide through Manage Widget Option
			
			boolean TotalFranchiseeGraphWidgetPresent1 = imdp.TotalFranchiseeWidgetVisible.isDisplayed();
			if(TotalFranchiseeGraphWidgetPresent1==true)
			{
				fc.utobj().printTestStep("Franchisees in Last 5 Yrs. and Total Franchisees Widget is unhide Successfully through manage Widget Option ");
			}
			else
			{
				fc.utobj().printTestStep("Franchisees in Last 5 Yrs. and Total Franchisees Widget is not getting unhide through manage Widget Option ");
			}
				
			//Verify Task View Widget is displaying after unhide through Manage Widget Option
			
			boolean TaskViewWidgetPresent1 = imdp.TaskViewWidgetVisible.isDisplayed();
			if(TaskViewWidgetPresent1==true)
			{
				fc.utobj().printTestStep("Task View Widget is unhide Successfully through manage Widget Option ");
			}
			else
			{
				fc.utobj().printTestStep("Task View Widget is not getting unhide through manage Widget Option ");
			}
			
			//Verify Renewals Due Widget is displaying after unhide through Manage Widget Option
			
			boolean RenewalsDueWidgetPresent1 = imdp.RenewalDueWidgetVisible.isDisplayed();
			if(RenewalsDueWidgetPresent1==true)
			{
				fc.utobj().printTestStep("Renewals Due Widget is unhide Successfully through manage Widget Option ");
			}
			else
			{
				fc.utobj().printTestStep("Renewals Due Widget is not getting unhide through manage Widget Option ");
			}		
			
			//Verify Email History Widget is displaying after unhide through Manage Widget Option
			
			boolean EmailHistoryWidgetPresent1 = imdp.EmailHistoryReportVisible.isDisplayed();
			if(EmailHistoryWidgetPresent1==true)
			{
				fc.utobj().printTestStep("Email History Widget is Unhide Successfully through manage Widget Option ");
			}
			else
			{
				fc.utobj().printTestStep("Email History Widget is not getting unhide through manage Widget Option ");
			}		
			
			//Verify Recent 5 Widget / QA Inspections Widget is not displaying after unhide through Manage Widget Option
			
			boolean QAHistoryWidgetPresent1 = imdp.RecentVisitWidgetVisible.isDisplayed();
			if(QAHistoryWidgetPresent1==true)
			{
				fc.utobj().printTestStep("Recent 5 Widget / QA Inspections Widget is unhide Successfully through manage Widget Option ");
			}
			else
			{
				fc.utobj().printTestStep("Recent 5 Widget / QA Inspections Widget is not getting unhide through manage Widget Option ");
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			
			
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(groups = { "infomgr","IM_Dashboard" })
	@TestCase(createdOn = "2018-07-09", updatedOn = "2018-07-09", testCaseId = "TC_IM_Dashboard_ManageWidget_RestoreDefaultSettings_DivisionalUser", testCaseDescription = "To verify the IM Dashboard ManageWidget_RestoreDefaultSettings via Divisional User")
	private void dashboardDiv10() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		InfoMgrDashboardPage imdp = new InfoMgrDashboardPage(driver);
		try {
			driver = fc.loginpage().login(driver);			
			String division_name = division.addDivisionName(driver, fc.utobj().generateTestData("D1"));
			String Login_id=fc.utobj().generateTestData("Div1");
			divUser.addDivisionalUser(driver, Login_id, "admin123", division_name, "vimal.sharma@franconnect.com");
			fc.home_page().logout(driver);
			driver = fc.loginpage().loginWithUserNameAndPassword(driver, Login_id, "admin123");
			
			// Navigate to Manage Widget Option and Remove Hide all Widgets from Info Mgr Dashaboard 
			
			imc.InfoMgrDashboard(driver);
			fc.utobj().printTestStep("Click on Info Mgr Manage Widget Option at Dashboard to hide all widgets");
			fc.utobj().clickElement(driver, imdp.manageWidget);
			fc.utobj().clickElement(driver, imdp.mngEmailHistoryWidget);
			fc.utobj().clickElement(driver, imdp.mngSummaryReportWidget);
			fc.utobj().clickElement(driver, imdp.mngTotalFranchiseeWidget);
			fc.utobj().clickElement(driver, imdp.mngRecentVisitWidget);
			fc.utobj().clickElement(driver, imdp.mngRenewalDueWidget);
			fc.utobj().clickElement(driver, imdp.mngTaskViewWidget);
			fc.utobj().clickElement(driver, imdp.mngWidgetSaveButton);
			
			fc.utobj().printTestStep("Info Mgr Dashboard all widgets are hide through Manage Widget Option and now verifying that all widgets are hide from dashboard");
			
			//Verify Franchise System Summary Report Widget is not displaying after hide through Manage Widget Option
			
			boolean FranchiseeSystemSummaryReportWidgetPresent = imdp.SummaryReportWidgetVisible.isDisplayed();
			if(FranchiseeSystemSummaryReportWidgetPresent==false)
			{
				fc.utobj().printTestStep("Franchise System Summary Report Widget is Hide Successfully through manage Widget Option ");
			}
			else
			{
				fc.utobj().printTestStep("Franchise System Summary Report Widget is not getting Hide through manage Widget Option ");
			}
			
			//Verify Franchisees in Last 5 Yrs. and Total Franchisees Widget is not displaying after hide through Manage Widget Option
			
			boolean TotalFranchiseeGraphWidgetPresent = imdp.TotalFranchiseeWidgetVisible.isDisplayed();
			if(TotalFranchiseeGraphWidgetPresent==false)
			{
				fc.utobj().printTestStep("Franchisees in Last 5 Yrs. and Total Franchisees Widget is Hide Successfully through manage Widget Option ");
			}
			else
			{
				fc.utobj().printTestStep("Franchisees in Last 5 Yrs. and Total Franchisees Widget is not getting Hide through manage Widget Option ");
			}
				
			//Verify Task View Widget is not displaying after hide through Manage Widget Option
			
			boolean TaskViewWidgetPresent = imdp.TaskViewWidgetVisible.isDisplayed();
			if(TaskViewWidgetPresent==false)
			{
				fc.utobj().printTestStep("Task View Widget is Hide Successfully through manage Widget Option ");
			}
			else
			{
				fc.utobj().printTestStep("Task View Widget is not getting Hide through manage Widget Option ");
			}
			
			//Verify Renewals Due Widget is not displaying after hide through Manage Widget Option
			
			boolean RenewalsDueWidgetPresent = imdp.RenewalDueWidgetVisible.isDisplayed();
			if(RenewalsDueWidgetPresent==false)
			{
				fc.utobj().printTestStep("Renewals Due Widget is Hide Successfully through manage Widget Option ");
			}
			else
			{
				fc.utobj().printTestStep("Renewals Due Widget is not getting Hide through manage Widget Option ");
			}		
			
			//Verify Email History Widget is not displaying after hide through Manage Widget Option
			
			boolean EmailHistoryWidgetPresent = imdp.EmailHistoryReportVisible.isDisplayed();
			if(EmailHistoryWidgetPresent==false)
			{
				fc.utobj().printTestStep("Email History Widget is Hide Successfully through manage Widget Option ");
			}
			else
			{
				fc.utobj().printTestStep("Email History Widget is not getting Hide through manage Widget Option ");
			}		
			
			//Verify Recent 5 Widget / QA Inspections Widget is not displaying after hide through Manage Widget Option
			
			boolean QAHistoryWidgetPresent = imdp.RecentVisitWidgetVisible.isDisplayed();
			if(QAHistoryWidgetPresent==false)
			{
				fc.utobj().printTestStep("Recent 5 Widget / QA Inspections Widget is Hide Successfully through manage Widget Option ");
			}
			else
			{
				fc.utobj().printTestStep("Recent 5 Widget / QA Inspections Widget is not getting Hide through manage Widget Option ");
			}
			
			//Click on Info Mgr Manage Widget Option at Dashboard to Unhide all Widgets
			
			fc.utobj().printTestStep("Click on Info Mgr Manage Widget Option at Dashboard to Restore all Widgets");
			fc.utobj().clickElement(driver, imdp.manageWidget);
			fc.utobj().clickElement(driver, "//a[@class='bText12lnkNew']");
			
			Thread.sleep(5000);
			fc.utobj().printTestStep("Info Mgr Dashboard all widgets are unhide through Manage Widgets Restore All Widgets link and now verifying that all widgets are visible on dashboard");
			
			//Verify Franchise System Summary Report Widget is displaying after unhide through Manage Widget Option
			
			boolean FranchiseeSystemSummaryReportWidgetPresent1 = imdp.SummaryReportWidgetVisible.isDisplayed();
			if(FranchiseeSystemSummaryReportWidgetPresent1==true)
			{
				fc.utobj().printTestStep("Franchise System Summary Report Widget is unhide Successfully through manage Widget Option ");
			}
			else
			{
				fc.utobj().printTestStep("Franchise System Summary Report Widget is not getting unhide through manage Widget Option ");
			}
			
			//Verify Franchisees in Last 5 Yrs. and Total Franchisees Widget is displaying after unhide through Manage Widget Option
			
			boolean TotalFranchiseeGraphWidgetPresent1 = imdp.TotalFranchiseeWidgetVisible.isDisplayed();
			if(TotalFranchiseeGraphWidgetPresent1==true)
			{
				fc.utobj().printTestStep("Franchisees in Last 5 Yrs. and Total Franchisees Widget is unhide Successfully through manage Widget Option ");
			}
			else
			{
				fc.utobj().printTestStep("Franchisees in Last 5 Yrs. and Total Franchisees Widget is not getting unhide through manage Widget Option ");
			}
				
			//Verify Task View Widget is displaying after unhide through Manage Widget Option
			
			boolean TaskViewWidgetPresent1 = imdp.TaskViewWidgetVisible.isDisplayed();
			if(TaskViewWidgetPresent1==true)
			{
				fc.utobj().printTestStep("Task View Widget is unhide Successfully through manage Widget Option ");
			}
			else
			{
				fc.utobj().printTestStep("Task View Widget is not getting unhide through manage Widget Option ");
			}
			
			//Verify Renewals Due Widget is displaying after unhide through Manage Widget Option
			
			boolean RenewalsDueWidgetPresent1 = imdp.RenewalDueWidgetVisible.isDisplayed();
			if(RenewalsDueWidgetPresent1==true)
			{
				fc.utobj().printTestStep("Renewals Due Widget is unhide Successfully through manage Widget Option ");
			}
			else
			{
				fc.utobj().printTestStep("Renewals Due Widget is not getting unhide through manage Widget Option ");
			}		
			
			//Verify Email History Widget is displaying after unhide through Manage Widget Option
			
			boolean EmailHistoryWidgetPresent1 = imdp.EmailHistoryReportVisible.isDisplayed();
			if(EmailHistoryWidgetPresent1==true)
			{
				fc.utobj().printTestStep("Email History Widget is Unhide Successfully through manage Widget Option ");
			}
			else
			{
				fc.utobj().printTestStep("Email History Widget is not getting unhide through manage Widget Option ");
			}		
			
			//Verify Recent 5 Widget / QA Inspections Widget is not displaying after unhide through Manage Widget Option
			
			boolean QAHistoryWidgetPresent1 = imdp.RecentVisitWidgetVisible.isDisplayed();
			if(QAHistoryWidgetPresent1==true)
			{
				fc.utobj().printTestStep("Recent 5 Widget / QA Inspections Widget is unhide Successfully through manage Widget Option ");
			}
			else
			{
				fc.utobj().printTestStep("Recent 5 Widget / QA Inspections Widget is not getting unhide through manage Widget Option ");
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			
			
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	// Verify Task View Widget
	
	@Test(groups = { "infomgr","IM_Dashboard","IM" })
	@TestCase(createdOn = "2018-07-10", updatedOn = "2018-07-10", testCaseId = "TC_IM_Dashboard_DivisionalUser_TaskViewWidgetVerify", testCaseDescription = "To verify that Task Assigned to Logged in Divisional User are coming at IM Dashboard Task View Widget after Login via Divisional User ")
	private void dashboardDiv11() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Location location = new Location();
		LocationData ld = new LocationData();

		try {
			driver = fc.loginpage().login(driver);
			AdminAreaRegionAddAreaRegionPageTest region_page = new AdminAreaRegionAddAreaRegionPageTest();
			String region_name = fc.utobj().generateTestData("R1");
			region_page.addAreaRegion(driver, region_name);
			String division_name = division.addDivisionName(driver, fc.utobj().generateTestData("D1"));
			String Login_id=fc.utobj().generateTestData("Div1");
			divUser.addDivisionalUser(driver, Login_id, "admin123", division_name, "vimal.sharma@franconnect.com");
			location = fc.commonMethods().getDefaultDataFor_Location(location);
			fc.adminpage().adminAddNewFranchiseLocationLnk(driver);
			location.setAreaRegion(region_name);
			location.setBrands(division_name);
			ld.filllocationInfo(driver, location).submit(driver);
			fc.adminpage().manageFranchiseLocationLnk(driver).SearchFranchise(driver, location).VerifyLocationExists(driver, location);
			System.out.println(location.getFranchiseID());
			imc.InfoMgrFranchisees(driver);
			//imc.SearchFranchiseeAndClick(driver, location.getFranchiseID());
			imc.SearchFranchiseeAndClick(driver, location.getFranchiseID());
			
			
			TaskWindowUI taskui = new TaskWindowUI(driver);
			String DivUserName = Login_id+ " " +Login_id;
			fc.utobj().clickLink(driver, "Add Task");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().selectValFromMultiSelectwithoutSelectAll(driver, taskui.OtherUserDropDown_MultiSelect_ByID, DivUserName);
			fc.utobj().selectDropDownByVisibleText(driver, taskui.Status_Select, "Work In Progress");
			fc.utobj().sendKeys(driver, taskui.Subject, "Info Mgr Task Subject for Dashboard Widget Verififcation by Divisional User");
			fc.utobj().clickElement(driver, taskui.TimelessTaskId_CheckBox);
			//fc.utobj().sendKeys(driver, taskui.taskDescription, "Info Mgr Task Comments for Dashboard Widget Verififcation by Divisional User");
			fc.utobj().clickElement(driver, taskui.Create_Input_ByValue);
			fc.utobj().switchFrameToDefault(driver);			
			fc.utobj().assertPageSource(driver, "Info Mgr Task Subject for Dashboard Widget Verififcation by Divisional User");	
			fc.utobj().printTestStep("Task Created and visible at Center Info Page");
			
			fc.home_page().logout(driver);
			driver = fc.loginpage().loginWithUserNameAndPassword(driver, Login_id, "admin123");
			imc.InfoMgrDashboard(driver);
			String OwnerName= location.getOwnerFirstName()+" "+location.getOwnerLastName();
			fc.utobj().printTestStep("IM Dashboard Task View Widget Verififcation Start with Divisional User");
			
			verifyTaskViewDivisional(driver, "Info Mgr Task Subject for Dashboard Widget Verififcation by Divisional User");
			verifyTaskViewDivisional(driver, location.getFranchiseID());
			verifyTaskViewDivisional(driver, OwnerName);
			verifyTaskViewDivisional(driver, "Work In Progress");
			//verifyTaskViewDivisional(driver, fc.utobj().getCurrentDateUSFormat());
			
			fc.utobj().printTestStep("Task Assigned to Logged in User is visible on Dashboard Task View Widget.");
			
			//Now Verifying the More Info Link Redirection from Task View Widget.
			fc.utobj().printTestStep("Now Verifying the More Info Link Redirection from Task View Widget.");
			fc.utobj().clickElement(driver, ".//*[@id='taskView']//*[contains(text(),'More Info')]");
			
			List<String>  taskdetails = new ArrayList<String>();
			taskdetails.add(location.getFranchiseID());
			taskdetails.add(OwnerName);
			//taskdetails.add(fc.utobj().getCurrentDateUSFormat());
			taskdetails.add("Info Mgr Task Subject for Dashboard Widget Verififcation by Divisional User");
			taskdetails.add("Work In Progress");
			taskdetails.add("Associated With");
			
			fc.utobj().assertPageSourceWithMultipleRecords(driver, taskdetails);
			fc.utobj().printTestStep("More Info Link successfully Navigated to Task Page and Added Task details are also verified");
			
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	
	@Test(groups = { "infomgr","IM_Dashboard" })
	@TestCase(createdOn = "2018-07-11", updatedOn = "2018-07-11", testCaseId = "TC_IM_Dashboard_DivisionalUser_RenewalDueWidget", testCaseDescription = "To verify the IM Dashboard Email History Widget after Login via Divisional User")
	private void dashboardDiv12() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Location location = new Location();
		LocationData ld = new LocationData();
		
		InfoMgrBasePage imbp = new InfoMgrBasePage(driver);

		try {
			driver = fc.loginpage().login(driver);
			AdminAreaRegionAddAreaRegionPageTest region_page = new AdminAreaRegionAddAreaRegionPageTest();
			String region_name = fc.utobj().generateTestData("R1");
			region_page.addAreaRegion(driver, region_name);
			String division_name = division.addDivisionName(driver, fc.utobj().generateTestData("D1"));
			String Login_id=fc.utobj().generateTestData("Div1");
			divUser.addDivisionalUser(driver, Login_id, "admin123", division_name, "vimal.sharma@franconnect.com");
			location = fc.commonMethods().getDefaultDataFor_Location(location);
			fc.adminpage().adminAddNewFranchiseLocationLnk(driver);
			location.setAreaRegion(region_name);
			location.setBrands(division_name);
			ld.filllocationInfo(driver, location).submit(driver);
			fc.adminpage().manageFranchiseLocationLnk(driver).SearchFranchise(driver, location).VerifyLocationExists(driver, location);
			System.out.println(location.getFranchiseID());
			
			 /*String FranchiseeID="FIMs10111863";
			 String Login_id="div1j10111617";
			 
			 String OwnerName = "Dr. FIMn10111878 Ownerr10111844";*/
			
			
			fc.home_page().logout(driver);
			driver = fc.loginpage().loginWithUserNameAndPassword(driver, Login_id, "admin123");
			
			imc.InfoMgrFranchisees(driver);
			imc.SearchFranchiseeAndClick(driver, location.getFranchiseID());
			// Adding Data for Renewals Due Widgets in Agreement Tab
			
			fc.utobj().clickElement(driver, imbp.lnkAgreement);
			InfoMgrAgreementPage imag = new InfoMgrAgreementPage(driver);
			fc.utobj().sendKeys(driver, imag.txtApprovedDate, fc.utobj().getCurrentDateUSFormat());
			fc.utobj().sendKeys(driver, imag.txtExpirationDate, fc.utobj().getFutureDateUSFormat(2));
			fc.utobj().clickElementByJS(driver, imag.btnsubmit);	
			imc.InfoMgrDashboard(driver);
			
			String OwnerName= location.getSalutation()+" "+  location.getOwnerFirstName()+" "+location.getOwnerLastName();
			fc.utobj().printTestStep("IM Dashboard Renewals Due Widget Verififcation Start with Divisional User");
			
			verifyRenewalsDue(driver, OwnerName,location.getFranchiseID());
			verifyRenewalsDue(driver, fc.utobj().getFutureDateUSFormat(2),location.getFranchiseID());
			fc.utobj().printTestStep("Franchisee having Renewals Due are visible to logged in user at Info Mgr Dashboard 'Renewals Due WIdget' for Which Term Expiration Date is entered in Agreement Tab");
			
			
			// Now Adding Data in Renewal tabs to Verify that Term Expiration value in Dashboard  Renewal Due Widget is Overwrite by Renewals Due Widget Term Expiration Date if it is greater. 
			
			imc.InfoMgrFranchisees(driver);
			imc.SearchFranchiseeAndClick(driver, location.getFranchiseID());
			
			// Adding Data for Renewals Due Widgets in Agreement Tab wih Term Expiration Date is Greater than Agreement Tab Term Expiration Date.
			
			fc.utobj().clickElement(driver, imbp.lnkRenewal);
			InfoMgrRenewalPage imrp = new InfoMgrRenewalPage(driver);
			fc.utobj().sendKeys(driver, imrp.txtAsOf, fc.utobj().getCurrentDateUSFormat());
			fc.utobj().sendKeys(driver, imrp.txtNewExpirationDate, fc.utobj().getFutureDateUSFormat(3));
			fc.utobj().clickElement(driver, imrp.btnAdd);	
			imc.InfoMgrDashboard(driver);
			
			
			fc.utobj().printTestStep("IM Dashboard Renewals Due Widget Verififcation Start with Divisional User with Renewal Tab Term Expiration Date Greater than Agreement Tab Term Expiration Date");
			
			verifyRenewalsDue(driver, OwnerName,location.getFranchiseID());
			verifyRenewalsDue(driver, fc.utobj().getFutureDateUSFormat(3),location.getFranchiseID());
			fc.utobj().printTestStep("Term Expiration Date of Renewal Tab is greater than Agreement Tab and same is visible at Info Mgr Renewal Due Widget at Dashboard.");
			
			
			
			// Adding Data for Renewals Due Widgets in Agreement Tab wih Term Expiration Date is Less than Agreement Tab Term Expiration Date.
			imc.InfoMgrFranchisees(driver);
			imc.SearchFranchiseeAndClick(driver, location.getFranchiseID());
			fc.utobj().clickElement(driver, imbp.lnkRenewal);
			fc.utobj().clickLink(driver, "Modify");
			
			fc.utobj().sendKeys(driver, imrp.txtNewExpirationDate, fc.utobj().getFutureDateUSFormat(1));
			fc.utobj().clickElement(driver, imrp.btnAdd);	
			imc.InfoMgrDashboard(driver);
					
						
			fc.utobj().printTestStep("IM Dashboard Renewals Due Widget Verififcation Start with Divisional User with Renewal Tab Term Expiration Date Less than Agreement Tab Term Expiration Date");
						
			verifyRenewalsDue(driver, OwnerName, location.getFranchiseID());
			verifyRenewalsDue(driver, fc.utobj().getFutureDateUSFormat(2),  location.getFranchiseID());
			fc.utobj().printTestStep("Term Expiration Date of Agreement Tab is greater than Renewal Tab Thus Agreement Tab Term Expiration Date is visible at Info Mgr Renewal Due Widget at Dashboard.");
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	
	// Email History Widget
	
	@Test(groups = { "infomgr","IM_Dashboard" })
	@TestCase(createdOn = "2018-07-12", updatedOn = "2018-07-12", testCaseId = "TC_IM_Dashboard_DivisionalUser_Send_Email_WidgetVerify", testCaseDescription = "To verify the IM Dashboard Email History Widget after Login via Divisional User")
	private void dashboardDiv13() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Location location = new Location();
		LocationData ld = new LocationData();
		
		InfoMgrBasePage imbp = new InfoMgrBasePage(driver);
		
		try {
			driver = fc.loginpage().login(driver);
			AdminAreaRegionAddAreaRegionPageTest region_page = new AdminAreaRegionAddAreaRegionPageTest();
			String region_name = fc.utobj().generateTestData("R1");
			region_page.addAreaRegion(driver, region_name);
			String division_name = division.addDivisionName(driver, fc.utobj().generateTestData("D1"));
			String Login_id=fc.utobj().generateTestData("Div1");
			divUser.addDivisionalUser(driver, Login_id, "admin123", division_name, "vimal.sharma@franconnect.com");
			location = fc.commonMethods().getDefaultDataFor_Location(location);
			fc.adminpage().adminAddNewFranchiseLocationLnk(driver);
			location.setAreaRegion(region_name);
			location.setBrands(division_name);
			ld.filllocationInfo(driver, location).submit(driver);
			fc.adminpage().manageFranchiseLocationLnk(driver).SearchFranchise(driver, location).VerifyLocationExists(driver, location);
			System.out.println(location.getFranchiseID());
			
			/*String Login_id ="div1a12164718";
			location.setFranchiseID("FIMi12164934");*/
			
			
			
			fc.home_page().logout(driver);
			driver = fc.loginpage().loginWithUserNameAndPassword(driver, Login_id, "admin123");
			imc.InfoMgrFranchisees(driver);
			imc.searchLocationFromShowFilter(driver, location.getFranchiseID());
			// Sending email from Franchise Summary Page and then Verifying the email at Email History Dashboard Widget
			fc.utobj().actionImgOption(driver, location.getFranchiseID(), "Send Email");
			InfoMgrSendEmailPage imsendemail = new InfoMgrSendEmailPage(driver);
			fc.utobj().sendKeys(driver, imsendemail.txtSubject, "Info Mgr Send Email Subject from Franchisee Summary Page by Divisional User and then Verification on Dashboard Email Summary Widget");
			//fc.utobj().switchFrame(driver, driver.findElement(By.id("mailMessage_ifr")));
			fc.utobj().switchFrameById(driver, "mailMessage_ifr");
			fc.utobj().sendKeys(driver, driver.findElement(By.xpath(".//*[@id='tinymce']")), "Info Mgr Send Email Message from Franchisee Summary Page by Divisional User and then Verification on Dashboard Email Summary Widget");
			//fc.utobj().sendKeys(driver, imsendemail.htmltxtArea, "Info Mgr Send Email Message from Franchisee Summary Page by Divisional User and then Verification on Dashboard Email Summary Widget");
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, imsendemail.btnSend);
			imc.InfoMgrDashboard(driver);			
			
			/*verifyEmailHistory(driver,"Info Mgr Send Email Subject from Franchisee Summary Page by Divisional User and then Verification on Dashboard Email Summary Widget",location.getFranchiseID());
			fc.utobj().printTestStep("Email Sent through Francisee Summary Page is visible to logged in user at Info Mgr Dashboard 'Email History Widget' ");*/
			fc.utobj().clickLink(driver, "Info Mgr Send Email Subject from Franchisee Summary Page by Divisional User and then Verification on Dashboard Email Summary Widget");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			//fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			
			boolean msgpresent =fc.utobj().assertPageSource(driver, "Info Mgr Send Email Message from Franchisee Summary Page by Divisional User and then Verification on Dashboard Email Summary Widget");
			
			if(msgpresent==true)
			{
				fc.utobj().printTestStep("Message Content verified on popup of send email after click on Email subject at Info Mgr Dahboard Email History Widget ");
			}
			else
			{
				fc.utobj().printTestStep("Message Content Not Present on popup of send email after click on Email subject at Info Mgr Dahboard Email History Widget ");
			}
			
			//Send Email from Contact History Send Email button
			fc.utobj().clickElement(driver, ".//*[@value='Close']");
			fc.utobj().switchFrameToDefault(driver);
			imc.InfoMgrFranchisees(driver);
			imc.SearchFranchiseeAndClick(driver, location.getFranchiseID());
			
			fc.utobj().clickElement(driver, imbp.lnkContactHistory);
			fc.utobj().clickElement(driver, ".//*[@value='Send Email']");
			fc.utobj().sendKeys(driver, imsendemail.txtSubject, "Info Mgr Send Email Subject from Contact History Page by Divisional User and then Verification on Dashboard Email Summary Widget");
			fc.utobj().switchFrame(driver, driver.findElement(By.id("mailMessage_ifr")));
			fc.utobj().sendKeys(driver, driver.findElement(By.xpath(".//*[@id='tinymce']")), "Info Mgr Send Email Message from Contact History Page by Divisional User and then Verification on Dashboard Email Summary Widget");
			//fc.utobj().sendKeys(driver, imsendemail.htmltxtArea, "Info Mgr Send Email Message from Franchisee Summary Page by Divisional User and then Verification on Dashboard Email Summary Widget");
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, imsendemail.btnSend);
			imc.InfoMgrDashboard(driver);			
			
			/*verifyEmailHistory1(driver,"Info Mgr Send Email Subject from Contact History Page by Divisional User and then Verification on Dashboard Email Summary Widget",location.getFranchiseID());
			fc.utobj().printTestStep("Email Sent through Contact History Page is visible to logged in user at Info Mgr Dashboard 'Email History Widget' ");*/
			fc.utobj().clickLink(driver, "Info Mgr Send Email Subject from Contact History Page by Divisional User and then Verification on Dashboard Email Summary Widget");
			fc.utobj().switchFrame(driver, driver.findElement(By.id("cboxIframe")));
			boolean msgpresent1 =fc.utobj().assertPageSource(driver, "Info Mgr Send Email Message from Contact History Page by Divisional User and then Verification on Dashboard Email Summary Widget");
			
			if(msgpresent1==true)
			{
				fc.utobj().printTestStep("Message Content verified on popup of send email after click on Email subject at Info Mgr Dahboard Email History Widget ");
			}
			else
			{
				fc.utobj().printTestStep("Message Content Not Present on popup of send email after click on Email subject at Info Mgr Dahboard Email History Widget ");
			}
			
			fc.utobj().clickElement(driver, ".//*[@value='Close']");
			fc.utobj().switchFrameToDefault(driver);
			
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	
	
	@Test(groups = { "infomgr","IM_Dashboard","IM" })
	@TestCase(createdOn = "2018-07-13", updatedOn = "2018-07-13", testCaseId = "TC_IM_Dashboard_DivisionalUser_QAHistoryWidget", testCaseDescription = "To verify the IM Dashboard QA History Widget after Login via Divisional User.")
	private void dashboardDiv14() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Location location = new Location();
		LocationData ld = new LocationData();
		
		InfoMgrBasePage imbp = new InfoMgrBasePage(driver);
		
		try {
			driver = fc.loginpage().login(driver);
			AdminAreaRegionAddAreaRegionPageTest region_page = new AdminAreaRegionAddAreaRegionPageTest();
			String region_name = fc.utobj().generateTestData("R1");
			region_page.addAreaRegion(driver, region_name);
			String division_name = division.addDivisionName(driver, fc.utobj().generateTestData("D1"));
			String Login_id=fc.utobj().generateTestData("Div1");
			divUser.addDivisionalUser(driver, Login_id, "admin123", division_name, "vimal.sharma@franconnect.com");
			location = fc.commonMethods().getDefaultDataFor_Location(location);
			fc.adminpage().adminAddNewFranchiseLocationLnk(driver);
			location.setAreaRegion(region_name);
			location.setBrands(division_name);
			ld.filllocationInfo(driver, location).submit(driver);
			fc.adminpage().manageFranchiseLocationLnk(driver).SearchFranchise(driver, location).VerifyLocationExists(driver, location);
			System.out.println(location.getFranchiseID());
			
			/*String Login_id = "div1f13160347";
			location.setFranchiseID("FIMl13160570");*/
			
			admininfomgrcommonmethod.SetQATabIntregrationNo(driver);
			
			fc.home_page().logout(driver);
			driver = fc.loginpage().loginWithUserNameAndPassword(driver, Login_id, "admin123");
			
			imc.InfoMgrFranchisees(driver);
			imc.SearchFranchiseeAndClick(driver,location.getFranchiseID());
			fc.utobj().clickElement(driver, imbp.lnkQAHistory);
			InfoMgrQAHistoryPage imqa =new InfoMgrQAHistoryPage(driver);
			fc.utobj().sendKeys(driver, imqa.dtinspectionDate, fc.utobj().getFutureDateUSFormat(2));
			fc.utobj().sendKeys(driver, imqa.txtinspectionType, "InformalInspection ");
			fc.utobj().sendKeys(driver, imqa.txtfieldConsultant, "Mark");
			fc.utobj().sendKeys(driver, imqa.txtproductQualityScore, "20");
			fc.utobj().sendKeys(driver, imqa.txtserviceQualityScore, "30");
			fc.utobj().sendKeys(driver, imqa.txtfacilityScore, "40");
			fc.utobj().sendKeys(driver, imqa.txtadminScore, "50");
			fc.utobj().sendKeys(driver, imqa.dtcureDate, fc.utobj().getFutureDateUSFormat(3));
			fc.utobj().sendKeys(driver, imqa.dtnextInspectionDate, fc.utobj().getFutureDateUSFormat(4));
			fc.utobj().sendKeys(driver, imqa.txtMTM, "MTM_Value");
			fc.utobj().clickElement(driver, imqa.btnSubmit);
			imc.InfoMgrDashboard(driver);
			String OwnerName= location.getOwnerFirstName()+" "+location.getOwnerLastName();
			verifyQAInspection(driver,OwnerName,location.getFranchiseID());
			verifyQAInspection(driver,fc.utobj().getFutureDateUSFormat(2),location.getFranchiseID());
			fc.utobj().printTestStep("Data Verfied at QA History Dahboard Widget by Divisional User");
			fc.utobj().printTestStep("Now Clicking on More Info Link to verify that it is working and Redirected at QA History Report.");
			fc.utobj().clickElement(driver, ".//*[@id='recentVisit']//a[contains(text(),'More Info')]");
			
			List<String>  a = new ArrayList<String>();
			a.add(location.getFranchiseID());
			a.add(OwnerName);
			a.add(fc.utobj().getFutureDateUSFormat(2));
			a.add("Lists QA inspection details for all the Franchisees");
			a.add("QA Report");

			fc.utobj().assertPageSourceWithMultipleRecords(driver, a);
			
			fc.utobj().printTestStep("QA History Dashboard Widget More Info Link successfully redirected at QA History Report and Data also verified");
			
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	
	
	// Log a Call from Dashboard by Divisional User
		@Test(groups = { "infomgr","IM_Dashboard" })
		@TestCase(createdOn = "2018-07-23", updatedOn = "2018-07-23", testCaseId = "TC_IM_Dashboard_DivisionalUser_LogaCall", testCaseDescription = "To verify the IM Dashboard Log a Call Link by Divisional User .")
		private void dashboardDiv15() throws Exception {
			String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
			}.getClass().getEnclosingMethod().getName());

			WebDriver driver = fc.commonMethods().browsers().openBrowser();
			Location location = new Location();
			LocationData ld = new LocationData();
			InfoMgrDashboardPage imdp = new InfoMgrDashboardPage(driver);
			InfoMgrBasePage imbp = new InfoMgrBasePage(driver);
			InfoMgrLogACallPage logacall = new InfoMgrLogACallPage(driver);

			try {
				driver = fc.loginpage().login(driver);
				AdminAreaRegionAddAreaRegionPageTest region_page = new AdminAreaRegionAddAreaRegionPageTest();
				String region_name = fc.utobj().generateTestData("R1");
				region_page.addAreaRegion(driver, region_name);
				String division_name = division.addDivisionName(driver, fc.utobj().generateTestData("D1"));
				String Login_id=fc.utobj().generateTestData("Div1");
				divUser.addDivisionalUser(driver, Login_id, "admin123", division_name, "vimal.sharma@franconnect.com");
				location = fc.commonMethods().getDefaultDataFor_Location(location);
				fc.adminpage().adminAddNewFranchiseLocationLnk(driver);
				location.setAreaRegion(region_name);
				location.setBrands(division_name);
				ld.filllocationInfo(driver, location).submit(driver);
				fc.adminpage().manageFranchiseLocationLnk(driver).SearchFranchise(driver, location).VerifyLocationExists(driver, location);
				System.out.println(location.getFranchiseID());
				fc.home_page().logout(driver);
				fc.loginpage().loginWithUserNameAndPassword(driver, Login_id, "admin123");
				imc.InfoMgrDashboard(driver);
				fc.utobj().clickElement(driver, imdp.LogaCallLinkDashboard);

				fc.commonMethods().switch_cboxIframe_frameId(driver);

				fc.utobj().selectDropDownByVisibleText(driver, logacall.drpCategory, "Franchise ID");
				//fc.utobj().selectValFromMultiSelect(driver, logacall.drpFranchisee, location.getFranchiseID());
				fc.utobj().selectValFromMultiSelectwithoutSelectAll(driver, logacall.drpFranchisee, location.getFranchiseID());
				fc.utobj().clickElement(driver, logacall.btnContinue);
				fc.utobj().sendKeys(driver, logacall.txtSubject, "Log a Call by Divisional User from Dashboard Link");
				fc.utobj().sendKeys(driver, logacall.txtDate, fc.utobj().getCurrentDateUSFormat());
				fc.utobj().selectDropDownByVisibleText(driver, logacall.drpStatus, "Left Message");
				fc.utobj().selectDropDownByVisibleText(driver, logacall.drpCallType, "Phone");
				/*fc.utobj().sendKeys(driver, logacall.txtComments,
						"Info Mgr Dashboard Log a Call Link Verification by Regional User ");*/
				fc.utobj().clickElement(driver, logacall.btnSubmit);
				fc.utobj().clickElement(driver, logacall.btnNo);
				fc.utobj().switchFrameToDefault(driver);
				imc.InfoMgrFranchisees(driver);
				//imc.SearchFranchiseeAndClick(driver, location.getFranchiseID());
				imc.SearchFranchiseeAndClick(driver, location.getFranchiseID());
				fc.utobj().clickElement(driver, imbp.lnkContactHistory);
				fc.utobj().assertPageSource(driver, "Log a Call by Divisional User from Dashboard Link");
				fc.utobj().printTestStep(
						"Logged call from Dashboard from Divisional User successfully verified in Franchisee Contact History");

				fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

			} catch (Exception e) {

				fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			}
		}

		// Log a Task from Dashboard by Divisional User
		@Test(groups = { "infomgr","IM_Dashboard" })
		@TestCase(createdOn = "2018-07-23", updatedOn = "2018-07-23", testCaseId = "TC_IM_Dashboard_DivisionalUser_LogaTask", testCaseDescription = "To verify the IM Dashboard Log a Task Link by Divisional User .")
		private void dashboardDiv16() throws Exception {
			String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
			}.getClass().getEnclosingMethod().getName());

			WebDriver driver = fc.commonMethods().browsers().openBrowser();
			Location location = new Location();
			LocationData ld = new LocationData();
			InfoMgrDashboardPage imdp = new InfoMgrDashboardPage(driver);
			InfoMgrBasePage imbp = new InfoMgrBasePage(driver);
			InfoMgrLogATaskPage logatask = new InfoMgrLogATaskPage(driver);

			try {
				driver = fc.loginpage().login(driver);
				AdminAreaRegionAddAreaRegionPageTest region_page = new AdminAreaRegionAddAreaRegionPageTest();
				String region_name = fc.utobj().generateTestData("R1");
				region_page.addAreaRegion(driver, region_name);
				String division_name = division.addDivisionName(driver, fc.utobj().generateTestData("D1"));
				String Login_id=fc.utobj().generateTestData("Div1");
				divUser.addDivisionalUser(driver, Login_id, "admin123", division_name, "vimal.sharma@franconnect.com");
				location = fc.commonMethods().getDefaultDataFor_Location(location);
				fc.adminpage().adminAddNewFranchiseLocationLnk(driver);
				location.setAreaRegion(region_name);
				location.setBrands(division_name);
				ld.filllocationInfo(driver, location).submit(driver);
				fc.adminpage().manageFranchiseLocationLnk(driver).SearchFranchise(driver, location).VerifyLocationExists(driver, location);
				System.out.println(location.getFranchiseID());
				fc.home_page().logout(driver);
				fc.loginpage().loginWithUserNameAndPassword(driver, Login_id, "admin123");
				imc.InfoMgrDashboard(driver);
				fc.utobj().clickElement(driver, imdp.AddTaskLinkDashboard);

				fc.commonMethods().switch_cboxIframe_frameId(driver);

				fc.utobj().selectDropDownByVisibleText(driver, logatask.drpCategory, "Franchise ID");
				fc.utobj().selectValFromMultiSelect(driver, logatask.drpFranchisee, location.getFranchiseID());
				fc.utobj().clickElement(driver, logatask.btnContinue);
				fc.utobj().selectValFromMultiSelectwithoutSelectAll(driver, logatask.multiCheckBoxCorporateUsers, Login_id);
				fc.utobj().selectDropDownByVisibleText(driver, logatask.drpStatus, "Work In Progress");
				fc.utobj().sendKeys(driver, logatask.txtSubject, "Log a Task by Regional User from Dashboard Link");
				fc.utobj().clickElement(driver, logatask.TimelessTaskId_CheckBox);
				//fc.utobj().sendKeys(driver, logatask.comments, "Info Mgr Dashboard Log a Task Link Verification by Divisional User");
				fc.utobj().clickElement(driver, logatask.btnAdd);
				Thread.sleep(3000);
				fc.utobj().switchFrameToDefault(driver);
				
				imc.InfoMgrFranchisees(driver);
				//imc.SearchFranchiseeAndClick(driver, location.getFranchiseID());
				imc.SearchFranchiseeAndClick(driver, location.getFranchiseID());
				fc.utobj().clickElement(driver, imbp.lnkContactHistory);
				fc.utobj().assertPageSource(driver, "Log a Task by Divisional User from Dashboard Link");
				fc.utobj().printTestStep(
						"Logged Task from Dashboard from Divisional User successfully verified in Franchisee Contact History");

				fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

			} catch (Exception e) {

				fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			}
		}

	// Location Search via Dashboard Search Box by Divisional User
	@Test(groups = { "infomgr","IM_Dashboard"})
	@TestCase(createdOn = "2018-07-23", updatedOn = "2018-07-23", testCaseId = "TC_IM_Dashboard_DivisionalUser_LocationSearch", testCaseDescription = "To verify the IM Dashboard Location Search via Text Box.")
	private void dashboardDiv17() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Location location = new Location();
		LocationData ld = new LocationData();
		InfoMgrDashboardPage imdp = new InfoMgrDashboardPage(driver);

		try {
			driver = fc.loginpage().login(driver);
			AdminAreaRegionAddAreaRegionPageTest region_page = new AdminAreaRegionAddAreaRegionPageTest();
			String region_name = fc.utobj().generateTestData("R1");
			region_page.addAreaRegion(driver, region_name);
			String division_name = division.addDivisionName(driver, fc.utobj().generateTestData("D1"));
			String Login_id = fc.utobj().generateTestData("Divuser");
			divUser.addDivisionalUser(driver, Login_id, "admin123", division_name, "vimal.sharma@franconnect.com");
			location = fc.commonMethods().getDefaultDataFor_Location(location);
			fc.adminpage().adminAddNewFranchiseLocationLnk(driver);
			location.setAreaRegion(region_name);
			location.setBrands(division_name);
			ld.filllocationInfo(driver, location).submit(driver);
			fc.adminpage().manageFranchiseLocationLnk(driver).SearchFranchise(driver, location)
					.VerifyLocationExists(driver, location);
			System.out.println(location.getFranchiseID());
			fc.home_page().logout(driver);
			fc.loginpage().loginWithUserNameAndPassword(driver, Login_id, "admin123");
			imc.InfoMgrDashboard(driver);
			fc.utobj().sendKeys(driver, imdp.SearchBoxDashboard, location.getFranchiseID());
			fc.utobj().clickElement(driver, imdp.SearchBoxIconDashboard);
			fc.utobj().assertPageSource(driver, location.getFranchiseID());
			imc.InfoMgrDashboard(driver);
			fc.utobj().sendKeys(driver, imdp.SearchBoxDashboard, location.getFranchiseID());
			fc.utobj().assertPageSource(driver, location.getFranchiseID());
			fc.utobj().clickEnterOnElement(driver, imdp.SearchBoxDashboard);
			
			imc.InfoMgrDashboard(driver);
			imdp = new InfoMgrDashboardPage(driver);
			fc.utobj().sendKeys(driver, imdp.SearchBoxDashboard,location.getFranchiseID());
			fc.utobj().sleep(2000);
			fc.utobj().clickElementByJS(driver, fc.utobj().getElementByXpath(driver, ".//*[.='"+location.getFranchiseID()+"']"));
			

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	
// To Verify Dashboard Franchisees in Last 5 Yrs. and Total Franchisees Widget graph is present
	@Test(groups = { "infomgr","IM_Dashboard" })
	@TestCase(createdOn = "2018-07-27", updatedOn = "2018-07-27", testCaseId = "TC_IM_Dashboard_DivisionalUser_GraphPresent in Franchisees in Last 5 Yrs. and Total Franchisees Widget ", testCaseDescription = "To verify the IM Dashboard Franchisees in Last 5 Yrs. and Total Franchisees Graph is present after login via Divisional User.")
	private void dashboardDiv18() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Location location = new Location();
		LocationData ld = new LocationData();
		InfoMgrDashboardPage imdp = new InfoMgrDashboardPage(driver);

		try {
			driver = fc.loginpage().login(driver);
			AdminAreaRegionAddAreaRegionPageTest region_page = new AdminAreaRegionAddAreaRegionPageTest();
			String region_name = fc.utobj().generateTestData("R1");
			region_page.addAreaRegion(driver, region_name);
			String division_name = division.addDivisionName(driver, fc.utobj().generateTestData("D1"));
			String Login_id = fc.utobj().generateTestData("Div1");
			divUser.addDivisionalUser(driver, Login_id, "admin123", division_name, "vimal.sharma@franconnect.com");
			location = fc.commonMethods().getDefaultDataFor_Location(location);
			fc.adminpage().adminAddNewFranchiseLocationLnk(driver);
			location.setAreaRegion(region_name);
			location.setBrands(division_name);
			ld.filllocationInfo(driver, location).submit(driver);
			fc.adminpage().manageFranchiseLocationLnk(driver).SearchFranchise(driver, location)
					.VerifyLocationExists(driver, location);
			System.out.println(location.getFranchiseID());
			fc.home_page().logout(driver);
			fc.loginpage().loginWithUserNameAndPassword(driver, Login_id, "admin123");
			imc.InfoMgrDashboard(driver);
			if(imdp.GraphPresent.isDisplayed())
			{
				fc.utobj().printTestStep("Franchisees in Last 5 Yrs. and Total Franchisees Graph is visible to Regional user");
			}
			

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	
	// Recent 5 Visit Widget More Info Link Click from Dashboard by Divisional User
		@Test(groups = { "infomgr","IM_Dashboard" })
		@TestCase(createdOn = "2018-07-30", updatedOn = "2018-07-30", testCaseId = "TC_IM_Dashboard_DivisionalUser_Recent5VisitMoreInfoLink", testCaseDescription = "To verify the IM Dashboard Recent5VisitMoreInfoLink by Divisional User.")
		private void dashboardDiv19() throws Exception {
			String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
			}.getClass().getEnclosingMethod().getName());

			WebDriver driver = fc.commonMethods().browsers().openBrowser();
			InfoMgrDashboardPage imdp = new InfoMgrDashboardPage(driver);

			try {
				driver = fc.loginpage().login(driver);
				admininfomgrcommonmethod.SetQATabIntregrationYes(driver);
				String division_name = division.addDivisionName(driver, fc.utobj().generateTestData("D1"));
				String Login_id = fc.utobj().generateTestData("Div1");
				divUser.addDivisionalUser(driver, Login_id, "admin123", division_name, "vimal.sharma@franconnect.com");
				fc.home_page().logout(driver);
				fc.loginpage().loginWithUserNameAndPassword(driver, Login_id, "admin123");
				imc.InfoMgrDashboard(driver);

				// Now Verifying the More Info Link Redirection from Task View Widget.
				fc.utobj().printTestStep(
						"Now Verifying the More Info Link Redirection from Recent 5 Widget By Corporate User (QA Tab Integraion is Yes/ON).");
				fc.utobj().clickElement(driver, imdp.RecentQaHistoryWidgetMoreInfoLink);

				fc.utobj().assertPageSource(driver, "Visit Summary Report (All)");
				fc.utobj().printTestStep("More Info Link successfully Navigated to Field Ops Visit Summary Report Page");

				
				fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

			} catch (Exception e) {

				fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			}
		}

	
	/////Regional User Dashboard Verify
	@Test(groups = { "infomgr","IM_Dashboard","IM" })
	@TestCase(createdOn = "2018-07-16", updatedOn = "2018-07-16", testCaseId = "TC_IM_Dashboard_RegionalUser_FranchiseeLast30Days", testCaseDescription = "To verify the IM Dashboard Franchisee System Summary Report Widget after Login via Regional User for Franchise Location created in Last 30 Days")
	private void dashboardReg1() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Location location = new Location();
		LocationData ld = new LocationData();
		InfoMgrDashboardPage imdp =new InfoMgrDashboardPage(driver);

		try {
			driver = fc.loginpage().login(driver);
			AdminAreaRegionAddAreaRegionPageTest region_page = new AdminAreaRegionAddAreaRegionPageTest();
			String region_name = fc.utobj().generateTestData("R1");
			region_page.addAreaRegion(driver, region_name);
			String Login_id = fc.utobj().generateTestData("R1");
			
			RUser.addRegionalUser(driver, Login_id, "admin123", region_name, "vimal.sharma@franconnect.com");
			
			
			location = fc.commonMethods().getDefaultDataFor_Location(location);
			fc.adminpage().adminAddNewFranchiseLocationLnk(driver);
			location.setAreaRegion(region_name);
			location.setOpeningDate(fc.utobj().getFutureDateUSFormat(-3));
			ld.filllocationInfo(driver, location).submit(driver);
			fc.adminpage().manageFranchiseLocationLnk(driver).SearchFranchise(driver, location).VerifyLocationExists(driver, location);
			System.out.println(location.getFranchiseID());
			fc.home_page().logout(driver);
			driver = fc.loginpage().loginWithUserNameAndPassword(driver, Login_id, "admin123");
			
			imc.InfoMgrDashboard(driver);
			fc.utobj().printTestStep("IM Dashboard Franchisee System Summary Report Verififcation Start");
			WebElement ThirtyDays = imdp.Franchiseecount30DaysCount1;
			WebElement SixMonths = imdp.Franchiseecount6MonthsCount1;
			WebElement TwelveMonths = imdp.Franchiseecount12MonthsCount1 ;
			fc.utobj().isElementPresent(driver, ThirtyDays);
			fc.utobj().printTestStep("New Franchisee Count in last 30 days is coming as Expected value 1");
			fc.utobj().isElementPresent(driver, SixMonths);
			fc.utobj().printTestStep("New Franchisee Count in last 6 Months is coming as Expected value 1");
			fc.utobj().isElementPresent(driver, TwelveMonths);
			fc.utobj().printTestStep("New Franchisee Count in last 12 Months is coming as Expected value 1");
			
			String TotalNewLoc30Days = imdp.TotalNewLocation30Days.getText();
			//String ExpValue30Days ="1";
			//Assert.assertEquals(TotalNewLoc, ExpValue30Days);
			if(!"1".equalsIgnoreCase(TotalNewLoc30Days))
			{
				fc.utobj().printTestStep("Total New Locations in Last 30 Days Count not matched with Expected Value 1");
			}
			fc.utobj().printTestStep("Total New Locations Count in last 30 days is coming as Expected value 1");
			String TotalNewLoc6Months = imdp.TotalNewLocation6Months.getText();
			if(!"1".equalsIgnoreCase(TotalNewLoc6Months))
			{
				fc.utobj().printTestStep("Total New Locations in Last 6 Months  Count not matched with Expected Value 1");
			}
			fc.utobj().printTestStep("Total New Locations Count in last 6 Months is coming as Expected value 1");
			String TotalNewLoc12Months = imdp.TotalNewLocation12Months.getText();
			if(!"1".equalsIgnoreCase(TotalNewLoc12Months))
			{
				fc.utobj().printTestStep("Total New Locations in Last 12 Months  Count not matched with Expected Value 1");
			}		
			fc.utobj().printTestStep("Total New Locations Count in last 12 Months is coming as Expected value 1");
			
			String TotalActiveFranchisees = imdp.TotalActiveFranchisees.getText();
			if(!"1".equalsIgnoreCase(TotalActiveFranchisees))
			{
				fc.utobj().printTestStep("Total Active Franchisee Count not matched with Expected Value 1");
			}
			fc.utobj().printTestStep("Total Active Franchisee Count is coming as Expected value 1");
			String TotalActiveLocations = imdp.TotalActiveLocations.getText();
			if(!"1".equalsIgnoreCase(TotalActiveLocations))
			{
				fc.utobj().printTestStep("Total Active Franchisee Count not matched with Expected Value 1");
			}	
			fc.utobj().printTestStep("Total Active Locations Count is coming as Expected value 1");
			fc.utobj().clickElement(driver, imdp.Franchiseecount30DaysCount1);
			fc.utobj().assertPageSource(driver,location.getFranchiseID() );
			fc.utobj().printTestStep("Created Franchisee location is verified after clicks on New Franchisee created in Last 30 Days count");
			fc.utobj().printTestStep("IM Dashboard Franchisee System Summary Report Verififcation Completed");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	@Test(groups = { "infomgr","IM_Dashboard" })
	@TestCase(createdOn = "2018-07-16", updatedOn = "2018-07-16", testCaseId = "TC_IM_Dashboard_RegionalUser_FranchiseeLast6Months", testCaseDescription = "To verify the IM Dashboard Franchisee System Summary Report Widget after Login via Regional User for Franchise Location created in Last 6 Months")
	private void dashboardReg2() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Location location = new Location();
		LocationData ld = new LocationData();
		InfoMgrDashboardPage imdp =new InfoMgrDashboardPage(driver);

		try {
			driver = fc.loginpage().login(driver);
			AdminAreaRegionAddAreaRegionPageTest region_page = new AdminAreaRegionAddAreaRegionPageTest();
			String region_name = fc.utobj().generateTestData("R1");
			region_page.addAreaRegion(driver, region_name);
			
			String Login_id = fc.utobj().generateTestData("R1");
			
			RUser.addRegionalUser(driver, Login_id, "admin123", region_name, "vimal.sharma@franconnect.com");
			
			location = fc.commonMethods().getDefaultDataFor_Location(location);
			fc.adminpage().adminAddNewFranchiseLocationLnk(driver);
			location.setAreaRegion(region_name);
			//location.setBrands(division_name);
			location.setOpeningDate(fc.utobj().getFutureDateUSFormat(-33));
			ld.filllocationInfo(driver, location).submit(driver);
			fc.adminpage().manageFranchiseLocationLnk(driver).SearchFranchise(driver, location).VerifyLocationExists(driver, location);
			System.out.println(location.getFranchiseID());
			fc.home_page().logout(driver);
			driver = fc.loginpage().loginWithUserNameAndPassword(driver, Login_id, "admin123");
			
			imc.InfoMgrDashboard(driver);
			fc.utobj().printTestStep("IM Dashboard Franchisee System Summary Report Verififcation Start");
			
			String ThirtyDays = imdp.Franchiseecount30DaysCountGetTxt.getText();
			if(!"--".equalsIgnoreCase(ThirtyDays))
			{
				fc.utobj().printTestStep("New Franchisees in Last 30 Days Count not matched with Expected Value 1");
			}
			fc.utobj().printTestStep("New Franchisees Count in last 30 days is coming as Expected value --");
			
			WebElement SixMonths = imdp.Franchiseecount6MonthsCount1;
			WebElement TwelveMonths = imdp.Franchiseecount12MonthsCount1 ;
			fc.utobj().isElementPresent(driver, SixMonths);
			fc.utobj().printTestStep("New Franchisee Count in last 6 Months is coming as Expected value 1");
			fc.utobj().isElementPresent(driver, TwelveMonths);
			fc.utobj().printTestStep("New Franchisee Count in last 12 Months is coming as Expected value 1");
			
			String TotalNewLoc6Months = imdp.TotalNewLocation6Months.getText();
			if(!"1".equalsIgnoreCase(TotalNewLoc6Months))
			{
				fc.utobj().printTestStep("Total New Locations in Last 6 Months  Count not matched with Expected Value 1");
			}
			fc.utobj().printTestStep("Total New Locations Count in last 6 Months is coming as Expected value 1");
			String TotalNewLoc12Months = imdp.TotalNewLocation12Months.getText();
			if(!"1".equalsIgnoreCase(TotalNewLoc12Months))
			{
				fc.utobj().printTestStep("Total New Locations in Last 12 Months  Count not matched with Expected Value 1");
			}		
			fc.utobj().printTestStep("Total New Locations Count in last 12 Months is coming as Expected value 1");
			
			String TotalActiveFranchisees = imdp.TotalActiveFranchisees.getText();
			if(!"1".equalsIgnoreCase(TotalActiveFranchisees))
			{
				fc.utobj().printTestStep("Total Active Franchisee Count not matched with Expected Value 1");
			}
			fc.utobj().printTestStep("Total Active Franchisee Count is coming as Expected value 1");
			String TotalActiveLocations = imdp.TotalActiveLocations.getText();
			if(!"1".equalsIgnoreCase(TotalActiveLocations))
			{
				fc.utobj().printTestStep("Total Active Franchisee Count not matched with Expected Value 1");
			}	
			fc.utobj().printTestStep("Total Active Locations Count is coming as Expected value 1");
			fc.utobj().clickElement(driver, imdp.Franchiseecount6MonthsCount1);
			fc.utobj().assertPageSource(driver,location.getFranchiseID() );
			fc.utobj().printTestStep("Created Franchisee location is verified after clicks on New Franchisee created in Last 6 Months count");
			fc.utobj().printTestStep("IM Dashboard Franchisee System Summary Report Verififcation Completed");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	@Test(groups = { "infomgr","IM_Dashboard" })
	@TestCase(createdOn = "2018-07-16", updatedOn = "2018-07-16", testCaseId = "TC_IM_Dashboard_RegionalUser_FranchiseeLast12Months", testCaseDescription = "To verify the IM Dashboard Franchisee System Summary Report Widget after Login via Regional User for Franchise Location created in Last 12 Months")
	private void dashboardReg3() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Location location = new Location();
		LocationData ld = new LocationData();
		InfoMgrDashboardPage imdp =new InfoMgrDashboardPage(driver);

		try {
			driver = fc.loginpage().login(driver);
			AdminAreaRegionAddAreaRegionPageTest region_page = new AdminAreaRegionAddAreaRegionPageTest();
			String region_name = fc.utobj().generateTestData("R1");
			region_page.addAreaRegion(driver, region_name);
			/*String division_name = division.addDivisionName(driver, fc.utobj().generateTestData("D1"));
			String Login_id=fc.utobj().generateTestData("Div1");
			divUser.addDivisionalUser(driver, Login_id, "admin123", division_name, "vimal.sharma@franconnect.com");*/
			
			String Login_id = fc.utobj().generateTestData("R1");
			
			RUser.addRegionalUser(driver, Login_id, "admin123", region_name, "vimal.sharma@franconnect.com");
			
			location = fc.commonMethods().getDefaultDataFor_Location(location);
			fc.adminpage().adminAddNewFranchiseLocationLnk(driver);
			location.setAreaRegion(region_name);
			//location.setBrands(division_name);
			location.setOpeningDate(fc.utobj().getFutureDateUSFormat(-183));
			ld.filllocationInfo(driver, location).submit(driver);
			fc.adminpage().manageFranchiseLocationLnk(driver).SearchFranchise(driver, location).VerifyLocationExists(driver, location);
			System.out.println(location.getFranchiseID());
			fc.home_page().logout(driver);
			driver = fc.loginpage().loginWithUserNameAndPassword(driver, Login_id, "admin123");
			
			imc.InfoMgrDashboard(driver);
			fc.utobj().printTestStep("IM Dashboard Franchisee System Summary Report Verififcation Start");
			String ThirtyDays = imdp.Franchiseecount30DaysCountGetTxt.getText();
			if(!"--".equalsIgnoreCase(ThirtyDays))
			{
				fc.utobj().printTestStep("New Franchisees in Last 30 Days Count not matched with Expected Value 1");
			}
			fc.utobj().printTestStep("New Franchisees Count in last 30 days is coming as Expected value --");
			
			String SixMonths = imdp.Franchiseecount6MonthsCountGetTxt.getText();
			if(!"--".equalsIgnoreCase(SixMonths))
			{
				fc.utobj().printTestStep("New Franchisees in Last 6 Months Count not matched with Expected Value 1");
			}
			fc.utobj().printTestStep("New Franchisees Count in 6 Months is coming as Expected value --");
			
			
			WebElement TwelveMonths = imdp.Franchiseecount12MonthsCount1 ;
			fc.utobj().isElementPresent(driver, TwelveMonths);
			fc.utobj().printTestStep("New Franchisee Count in last 12 Months is coming as Expected value 1");
			
			String TotalNewLoc12Months = imdp.TotalNewLocation12Months.getText();
			if(!"1".equalsIgnoreCase(TotalNewLoc12Months))
			{
				fc.utobj().printTestStep("Total New Locations in Last 12 Months  Count not matched with Expected Value 1");
			}		
			fc.utobj().printTestStep("Total New Locations Count in last 12 Months is coming as Expected value 1");
			
			String TotalActiveFranchisees = imdp.TotalActiveFranchisees.getText();
			if(!"1".equalsIgnoreCase(TotalActiveFranchisees))
			{
				fc.utobj().printTestStep("Total Active Franchisee Count not matched with Expected Value 1");
			}
			fc.utobj().printTestStep("Total Active Franchisee Count is coming as Expected value 1");
			String TotalActiveLocations = imdp.TotalActiveLocations.getText();
			if(!"1".equalsIgnoreCase(TotalActiveLocations))
			{
				fc.utobj().printTestStep("Total Active Franchisee Count not matched with Expected Value 1");
			}	
			fc.utobj().printTestStep("Total Active Locations Count is coming as Expected value 1");
			fc.utobj().clickElement(driver, imdp.Franchiseecount12MonthsCount1);
			fc.utobj().assertPageSource(driver,location.getFranchiseID() );
			fc.utobj().printTestStep("Created Franchisee location is verified after clicks on Franchisee created in Last 12 Months count");
			fc.utobj().printTestStep("IM Dashboard Franchisee System Summary Report Verififcation Completed");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	@Test(groups = { "infomgr","IM_Dashboard","IM" })
	@TestCase(createdOn = "2018-07-16", updatedOn = "2018-07-16", testCaseId = "TC_IM_Dashboard_RegionalUser_CorporateLast30Days", testCaseDescription = "To verify the IM Dashboard Franchisee System Summary Report Widget after Login via Regional User for Corporate Locations created in Last 30 Days")
	private void dashboardReg4() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Location location = new Location();
		LocationData ld = new LocationData();
		InfoMgrDashboardPage imdp =new InfoMgrDashboardPage(driver);

		try {
			driver = fc.loginpage().login(driver);
			AdminAreaRegionAddAreaRegionPageTest region_page = new AdminAreaRegionAddAreaRegionPageTest();
			String region_name = fc.utobj().generateTestData("R1");
			region_page.addAreaRegion(driver, region_name);
			
			String Login_id = fc.utobj().generateTestData("R1");
			RUser.addRegionalUser(driver, Login_id, "admin123", region_name, "vimal.sharma@franconnect.com");
			
			location = fc.commonMethods().getDefaultDataFor_Location(location);
			fc.adminpage().adminAddNewFranchiseLocationLnk(driver);
			location.setAreaRegion(region_name);
			location.setCorporateLocation("Yes");
			location.setOpeningDate(fc.utobj().getFutureDateUSFormat(-3));
			ld.filllocationInfo(driver, location).submit(driver);
			fc.adminpage().manageFranchiseLocationLnk(driver).SearchFranchise(driver, location).VerifyLocationExists(driver, location);
			System.out.println(location.getFranchiseID());
			fc.home_page().logout(driver);
			driver = fc.loginpage().loginWithUserNameAndPassword(driver, Login_id, "admin123");
			
			imc.InfoMgrDashboard(driver);
			fc.utobj().printTestStep("IM Dashboard Franchisee System Summary Report Verififcation Start");
			WebElement ThirtyDays = imdp.NewCorporateLoc30DaysCount1;
			WebElement SixMonths = imdp.NewCorporateLoc6MonthsCount1;
			WebElement TwelveMonths = imdp.NewCorporateLoc12MonthsCount1;
			fc.utobj().isElementPresent(driver, ThirtyDays);
			fc.utobj().printTestStep("New Corporate Locations Count in last 30 days is coming as Expected value 1");
			fc.utobj().isElementPresent(driver, SixMonths);
			fc.utobj().printTestStep("New Corporate Locations Count in last 6 Months is coming as Expected value 1");
			fc.utobj().isElementPresent(driver, TwelveMonths);
			fc.utobj().printTestStep("New Corporate Locations Count in last 12 Months is coming as Expected value 1");
			
			String TotalNewLoc30Days = imdp.TotalNewLocation30Days.getText();
			//String ExpValue30Days ="1";
			//Assert.assertEquals(TotalNewLoc, ExpValue30Days);
			if(!"1".equalsIgnoreCase(TotalNewLoc30Days))
			{
				fc.utobj().printTestStep("Total New Locations in Last 30 Days Count not matched with Expected Value 1");
			}
			fc.utobj().printTestStep("Total New Locations Count in last 30 days is coming as Expected value 1");
			String TotalNewLoc6Months = imdp.TotalNewLocation6Months.getText();
			if(!"1".equalsIgnoreCase(TotalNewLoc6Months))
			{
				fc.utobj().printTestStep("Total New Locations in Last 6 Months  Count not matched with Expected Value 1");
			}
			fc.utobj().printTestStep("Total New Locations Count in last 6 Months is coming as Expected value 1");
			String TotalNewLoc12Months = imdp.TotalNewLocation12Months.getText();
			if(!"1".equalsIgnoreCase(TotalNewLoc12Months))
			{
				fc.utobj().printTestStep("Total New Locations in Last 12 Months  Count not matched with Expected Value 1");
			}		
			fc.utobj().printTestStep("Total New Locations Count in last 12 Months is coming as Expected value 1");
			
			String TotalActiveFranchisees = imdp.TotalActiveCorporateLocations.getText();
			if(!"1".equalsIgnoreCase(TotalActiveFranchisees))
			{
				fc.utobj().printTestStep("Total Active Corporate Locations Count not matched with Expected Value 1");
			}
			fc.utobj().printTestStep("Total Active Franchisee Count is coming as Expected value 1");
			String TotalActiveLocations = imdp.TotalActiveLocations.getText();
			if(!"1".equalsIgnoreCase(TotalActiveLocations))
			{
				fc.utobj().printTestStep("Total Active Franchisee Count not matched with Expected Value 1");
			}	
			fc.utobj().printTestStep("Total Active Locations Count is coming as Expected value 1");
			fc.utobj().clickElement(driver, imdp.NewCorporateLoc30DaysCount1);
			fc.utobj().assertPageSource(driver,location.getFranchiseID() );
			fc.utobj().printTestStep("Created Franchisee location is verified after clicks on New Corporate Locations created in Last 30 Days count");
			fc.utobj().printTestStep("IM Dashboard Franchisee System Summary Report Verififcation Completed");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	@Test(groups = { "infomgr","IM_Dashboard" })
	@TestCase(createdOn = "2018-07-16", updatedOn = "2018-07-16", testCaseId = "TC_IM_Dashboard_RegionalUser_CorporateLast6Months", testCaseDescription = "To verify the IM Dashboard Franchisee System Summary Report Widget after Login via Regional User for Corporate Location created in Last 6 Months")
	private void dashboardReg5() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Location location = new Location();
		LocationData ld = new LocationData();
		InfoMgrDashboardPage imdp =new InfoMgrDashboardPage(driver);

		try {
			driver = fc.loginpage().login(driver);
			AdminAreaRegionAddAreaRegionPageTest region_page = new AdminAreaRegionAddAreaRegionPageTest();
			String region_name = fc.utobj().generateTestData("R1");
			region_page.addAreaRegion(driver, region_name);
			
			String Login_id = fc.utobj().generateTestData("R1");
			RUser.addRegionalUser(driver, Login_id, "admin123", region_name, "vimal.sharma@franconnect.com");
			
			location = fc.commonMethods().getDefaultDataFor_Location(location);
			fc.adminpage().adminAddNewFranchiseLocationLnk(driver);
			location.setAreaRegion(region_name);
			location.setCorporateLocation("Yes");
			location.setOpeningDate(fc.utobj().getFutureDateUSFormat(-33));
			ld.filllocationInfo(driver, location).submit(driver);
			fc.adminpage().manageFranchiseLocationLnk(driver).SearchFranchise(driver, location).VerifyLocationExists(driver, location);
			System.out.println(location.getFranchiseID());
			fc.home_page().logout(driver);
			driver = fc.loginpage().loginWithUserNameAndPassword(driver, Login_id, "admin123");
			
			imc.InfoMgrDashboard(driver);
			fc.utobj().printTestStep("IM Dashboard Franchisee System Summary Report Verififcation Start");
			String ThirtyDays = imdp.NewCorporateLoc30DaysGetTxt.getText();
			if(!"--".equalsIgnoreCase(ThirtyDays))
			{
				fc.utobj().printTestStep("New Corporate Locations in Last 30 Days Count not matched with Expected Value 1");
			}
			fc.utobj().printTestStep("New Corporate Locations Count in last 30 days is coming as Expected value --");
						
			WebElement SixMonths = imdp.NewCorporateLoc6MonthsCount1;
			WebElement TwelveMonths = imdp.NewCorporateLoc12MonthsCount1;
			fc.utobj().isElementPresent(driver, SixMonths);
			fc.utobj().printTestStep("New Corporate Locations Count in last 6 Months is coming as Expected value 1");
			fc.utobj().isElementPresent(driver, TwelveMonths);
			fc.utobj().printTestStep("New Corporate Locations Count in last 12 Months is coming as Expected value 1");
			
			String TotalNewLoc30Days = imdp.TotalNewLocation30Days.getText();
			//String ExpValue30Days ="1";
			//Assert.assertEquals(TotalNewLoc, ExpValue30Days);
			if(!"1".equalsIgnoreCase(TotalNewLoc30Days))
			{
				fc.utobj().printTestStep("Total New Locations in Last 30 Days Count not matched with Expected Value 1");
			}
			fc.utobj().printTestStep("Total New Locations Count in last 30 days is coming as Expected value 1");
			String TotalNewLoc6Months = imdp.TotalNewLocation6Months.getText();
			if(!"1".equalsIgnoreCase(TotalNewLoc6Months))
			{
				fc.utobj().printTestStep("Total New Locations in Last 6 Months  Count not matched with Expected Value 1");
			}
			fc.utobj().printTestStep("Total New Locations Count in last 6 Months is coming as Expected value 1");
			String TotalNewLoc12Months = imdp.TotalNewLocation12Months.getText();
			if(!"1".equalsIgnoreCase(TotalNewLoc12Months))
			{
				fc.utobj().printTestStep("Total New Locations in Last 12 Months  Count not matched with Expected Value 1");
			}		
			fc.utobj().printTestStep("Total New Locations Count in last 12 Months is coming as Expected value 1");
			
			String TotalActiveFranchisees = imdp.TotalActiveCorporateLocations.getText();
			if(!"1".equalsIgnoreCase(TotalActiveFranchisees))
			{
				fc.utobj().printTestStep("Total Active Corporate Locations Count not matched with Expected Value 1");
			}
			fc.utobj().printTestStep("Total Active Franchisee Count is coming as Expected value 1");
			String TotalActiveLocations = imdp.TotalActiveLocations.getText();
			if(!"1".equalsIgnoreCase(TotalActiveLocations))
			{
				fc.utobj().printTestStep("Total Active Franchisee Count not matched with Expected Value 1");
			}	
			fc.utobj().printTestStep("Total Active Locations Count is coming as Expected value 1");
			fc.utobj().clickElement(driver, imdp.NewCorporateLoc6MonthsCount1);
			fc.utobj().assertPageSource(driver,location.getFranchiseID() );
			fc.utobj().printTestStep("Created Franchisee location is verified after clicks on New Corporate Locations created in Last 6 Months count");
			fc.utobj().printTestStep("IM Dashboard Franchisee System Summary Report Verififcation Completed");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	@Test(groups = { "infomgr","IM_Dashboard" })
	@TestCase(createdOn = "2018-07-16", updatedOn = "2018-07-16", testCaseId = "TC_IM_Dashboard_RegionalUser_CorporateLast12Months", testCaseDescription = "To verify the IM Dashboard Franchisee System Summary Report Widget after Login via Regional User for Corporate Location in Last 12 Months")
	private void dashboardReg6() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Location location = new Location();
		LocationData ld = new LocationData();
		InfoMgrDashboardPage imdp =new InfoMgrDashboardPage(driver);

		try {
			driver = fc.loginpage().login(driver);
			AdminAreaRegionAddAreaRegionPageTest region_page = new AdminAreaRegionAddAreaRegionPageTest();
			String region_name = fc.utobj().generateTestData("R1");
			region_page.addAreaRegion(driver, region_name);
			
			String Login_id = fc.utobj().generateTestData("R1");
			RUser.addRegionalUser(driver, Login_id, "admin123", region_name, "vimal.sharma@franconnect.com");
			
			location = fc.commonMethods().getDefaultDataFor_Location(location);
			fc.adminpage().adminAddNewFranchiseLocationLnk(driver);
			location.setAreaRegion(region_name);
			location.setCorporateLocation("Yes");
			location.setOpeningDate(fc.utobj().getFutureDateUSFormat(-183));
			ld.filllocationInfo(driver, location).submit(driver);
			fc.adminpage().manageFranchiseLocationLnk(driver).SearchFranchise(driver, location).VerifyLocationExists(driver, location);
			System.out.println(location.getFranchiseID());
			fc.home_page().logout(driver);
			driver = fc.loginpage().loginWithUserNameAndPassword(driver, Login_id, "admin123");
			
			imc.InfoMgrDashboard(driver);
			fc.utobj().printTestStep("IM Dashboard Franchisee System Summary Report Verififcation Start");
			String ThirtyDays = imdp.NewCorporateLoc30DaysGetTxt.getText();
			if(!"--".equalsIgnoreCase(ThirtyDays))
			{
				fc.utobj().printTestStep("New Corporate Locations Count in last 30 days is not matched with Expected value --");
			}
			fc.utobj().printTestStep("New Corporate Locations Count in last 30 days is matched with Expected value --");
			
			String SixMonths = imdp.NewCorporateLoc6MonthsGetTxt.getText();
			if(!"--".equalsIgnoreCase(SixMonths))
			{
				fc.utobj().printTestStep("New Corporate Locations Count in last 6 Months is not matched with Expected value --");
			}
			
			fc.utobj().printTestStep("New Corporate Locations Count in last 6 Months is matched with Expected value --");
			
			
			WebElement TwelveMonths = imdp.NewCorporateLoc12MonthsCount1;
			
			
			fc.utobj().isElementPresent(driver, TwelveMonths);
			fc.utobj().printTestStep("New Corporate Locations Count in last 12 Months is coming as Expected value 1");
			
			String TotalNewLoc30Days = imdp.TotalNewLocation30Days.getText();
			//String ExpValue30Days ="1";
			//Assert.assertEquals(TotalNewLoc, ExpValue30Days);
			if(!"--".equalsIgnoreCase(TotalNewLoc30Days))
			{
				fc.utobj().printTestStep("Total New Locations in Last 30 Days Count not matched with Expected Value --");
			}
			fc.utobj().printTestStep("Total New Locations Count in last 30 days is coming as Expected value --");
			String TotalNewLoc6Months = imdp.TotalNewLocation6Months.getText();
			if(!"--".equalsIgnoreCase(TotalNewLoc6Months))
			{
				fc.utobj().printTestStep("Total New Locations in Last 6 Months  Count not matched with Expected Value --");
			}
			fc.utobj().printTestStep("Total New Locations Count in last 6 Months is coming as Expected value 1");
			String TotalNewLoc12Months = imdp.TotalNewLocation12Months.getText();
			if(!"1".equalsIgnoreCase(TotalNewLoc12Months))
			{
				fc.utobj().printTestStep("Total New Locations in Last 12 Months  Count not matched with Expected Value 1");
			}		
			fc.utobj().printTestStep("Total New Locations Count in last 12 Months is coming as Expected value 1");
			
			String TotalActiveFranchisees = imdp.TotalActiveFranchisees.getText();
			if(!"1".equalsIgnoreCase(TotalActiveFranchisees))
			{
				fc.utobj().printTestStep("Total Active Corporate Locations Count not matched with Expected Value 1");
			}
			fc.utobj().printTestStep("Total Active Franchisee Count is coming as Expected value 1");
			String TotalActiveLocations = imdp.TotalActiveLocations.getText();
			if(!"1".equalsIgnoreCase(TotalActiveLocations))
			{
				fc.utobj().printTestStep("Total Active Franchisee Count not matched with Expected Value 1");
			}	
			fc.utobj().printTestStep("Total Active Locations Count is coming as Expected value 1");
			fc.utobj().clickElement(driver, imdp.NewCorporateLoc12MonthsCount1);
			fc.utobj().assertPageSource(driver,location.getFranchiseID() );
			fc.utobj().printTestStep("Created Franchisee location is verified after clicks on New Corporate Locations created in Last 12 Months count");
			fc.utobj().printTestStep("IM Dashboard Franchisee System Summary Report Verififcation Completed");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	@Test(groups = { "infomgr","IM_Dashboard" })
	@TestCase(createdOn = "2018-07-17", updatedOn = "2018-07-17", testCaseId = "TC_IM_Dashboard_RegionalUser_TerminatedFranchiseinLast_30Days_6Months_12Months", testCaseDescription = "To verify the IM Dashboard Franchisee System Summary Report Widget after Login via Regional User for Franchise Terminated in Last 30 Days 6 Months and 12 Months")
	private void dashboardReg7() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Location location = new Location();
		LocationData ld = new LocationData();
		InfoMgrDashboardPage imdp =new InfoMgrDashboardPage(driver);
		InfoMgrDefaultAndTerminationPage imdnt =new InfoMgrDefaultAndTerminationPage(driver);
		InfoMgrBasePage imbp = new InfoMgrBasePage(driver);
		try {
			driver = fc.loginpage().login(driver);
			AdminAreaRegionAddAreaRegionPageTest region_page = new AdminAreaRegionAddAreaRegionPageTest();
			String region_name = fc.utobj().generateTestData("R1");
			region_page.addAreaRegion(driver, region_name);
						
			String Login_id = fc.utobj().generateTestData("R1");
			RUser.addRegionalUser(driver, Login_id, "admin123", region_name, "vimal.sharma@franconnect.com");
			
			location = fc.commonMethods().getDefaultDataFor_Location(location);
			fc.adminpage().adminAddNewFranchiseLocationLnk(driver);
			location.setAreaRegion(region_name);
			ld.filllocationInfo(driver, location).submit(driver);
			fc.adminpage().manageFranchiseLocationLnk(driver).SearchFranchise(driver, location).VerifyLocationExists(driver, location);
			System.out.println(location.getFranchiseID());
			fc.home_page().logout(driver);
			driver = fc.loginpage().loginWithUserNameAndPassword(driver, Login_id, "admin123");
			imc.InfoMgrFranchisees(driver);
			imc.SearchFranchiseeAndClick(driver, location.getFranchiseID());
			fc.utobj().clickElement(driver, imbp.defaultTerminationTab);
			fc.utobj().sendKeys(driver, imdnt.txtCommitteApprovedDate, fc.utobj().getFutureDateUSFormat(-2));
			fc.utobj().sendKeys(driver, imdnt.txtDateTerminated, fc.utobj().getCurrentDateUSFormat());
			fc.utobj().selectDropDownByVisibleText(driver, imdnt.drpReason, "Abandonment");
			fc.utobj().clickElement(driver, imdnt.btnSubmit);
			//driver.switchTo().alert().accept();
			fc.utobj().acceptAlertBox(driver);
			
			imc.InfoMgrDashboard(driver);
			fc.utobj().printTestStep("IM Dashboard Franchisee System Summary Report Verififcation Start for Franchisee Terminated in Last 30 Days");
			WebElement ThirtyDays =imdp.TerminatedFranchisee30DaysCount1;
			WebElement SixMonths = imdp.TerminatedFranchisee6MonthsCount1;
			WebElement TwelveMonths = imdp.TerminatedFranchisee12MonthsCount1;
			fc.utobj().isElementPresent(driver, ThirtyDays);
			fc.utobj().printTestStep("New Franchisee Count in last 30 days is coming as Expected value 1");
			fc.utobj().isElementPresent(driver, SixMonths);
			fc.utobj().printTestStep("New Franchisee Count in last 6 Months is coming as Expected value 1");
			fc.utobj().isElementPresent(driver, TwelveMonths);
			fc.utobj().printTestStep("New Franchisee Count in last 12 Months is coming as Expected value 1");
						
			fc.utobj().clickElement(driver, imdp.TerminatedFranchisee30DaysCount1);
			fc.utobj().assertPageSource(driver,location.getFranchiseID() );
			fc.utobj().printTestStep("Terminated Franchisee location is verified after clicks on Terminated Franchisee created in Last 30 Days count");
			fc.utobj().printTestStep("IM Dashboard Franchisee System Summary Report Verififcation Completed for Termination location in Last 30 Days");
			imc.InfoMgrTerminated(driver);
			imc.SearchFranchiseeAndClick(driver, location.getFranchiseID());
			
			fc.utobj().clickElement(driver, imbp.defaultTerminationTab);
			fc.utobj().clickLink(driver, "Modify");
			fc.utobj().sendKeys(driver, imdnt.txtCommitteApprovedDate, fc.utobj().getFutureDateUSFormat(-33));
			fc.utobj().sendKeys(driver, imdnt.txtDateTerminated, fc.utobj().getFutureDateUSFormat(-33));
			fc.utobj().selectDropDownByVisibleText(driver, imdnt.drpReason, "Abandonment");
			fc.utobj().clickElement(driver, imdnt.btnSubmit);
			imc.InfoMgrDashboard(driver);
			fc.utobj().printTestStep("IM Dashboard Franchisee System Summary Report Verififcation Start for Franchisee Terminated in Last 6 Months");
			
			String ThirtyDays1 = imdp.TerminatedFranchisee30DaysCountGetTxt.getText();
			if(!"--".equalsIgnoreCase(ThirtyDays1))
			{
				fc.utobj().printTestStep("Franchise Terminations Count in last 30 days is not matched with Expected value --");
			}
			fc.utobj().printTestStep("Franchise Terminations Count in last 30 days is matched with Expected value --");
			WebElement SixMonths1 = imdp.TerminatedFranchisee6MonthsCount1;
			WebElement TwelveMonths1 = imdp.TerminatedFranchisee12MonthsCount1;
			
			fc.utobj().isElementPresent(driver, SixMonths1);
			fc.utobj().printTestStep("New Franchisee Count in last 6 Months is coming as Expected value 1");
			fc.utobj().isElementPresent(driver, TwelveMonths1);
			fc.utobj().printTestStep("New Franchisee Count in last 12 Months is coming as Expected value 1");
						
			fc.utobj().clickElement(driver, imdp.TerminatedFranchisee6MonthsCount1);
			fc.utobj().assertPageSource(driver,location.getFranchiseID() );
			fc.utobj().printTestStep("Terminated Franchisee location is verified after clicks on Terminated Franchisee created in Last 6 Months count");
			fc.utobj().printTestStep("IM Dashboard Franchisee System Summary Report Verififcation Completed for Termination location in Last 6 Months");
			
			
			imc.InfoMgrTerminated(driver);
			imc.SearchFranchiseeAndClick(driver, location.getFranchiseID());
			
			fc.utobj().clickElement(driver, imbp.defaultTerminationTab);
			fc.utobj().clickLink(driver, "Modify");
			fc.utobj().sendKeys(driver, imdnt.txtCommitteApprovedDate, fc.utobj().getFutureDateUSFormat(-185));
			fc.utobj().sendKeys(driver, imdnt.txtDateTerminated, fc.utobj().getFutureDateUSFormat(-185));
			fc.utobj().selectDropDownByVisibleText(driver, imdnt.drpReason, "Abandonment");
			fc.utobj().clickElement(driver, imdnt.btnSubmit);
			imc.InfoMgrDashboard(driver);
			fc.utobj().printTestStep("IM Dashboard Franchisee System Summary Report Verififcation Start for Franchisee Terminated in Last 6 Months");
			
			String ThirtyDays2 = imdp.TerminatedFranchisee30DaysCountGetTxt.getText();
			if(!"--".equalsIgnoreCase(ThirtyDays2))
			{
				fc.utobj().printTestStep("Franchise Terminations Count in last 30 days is not matched with Expected value --");
			}
			fc.utobj().printTestStep("Franchise Terminations Count in last 30 days is matched with Expected value --");
			
			
			String SixMonths2 = imdp.TerminatedFranchisee6MonthsCountGetTxt.getText();
			if(!"--".equalsIgnoreCase(SixMonths2))
			{
				fc.utobj().printTestStep("Franchise Terminations Count in last 6 Months is not matched with Expected value --");
			}
			fc.utobj().printTestStep("Franchise Terminations Count in last 6 Months is matched with Expected value --");
			
			WebElement TwelveMonths2 = imdp.TerminatedFranchisee12MonthsCount1;
			fc.utobj().isElementPresent(driver, TwelveMonths2);
			fc.utobj().printTestStep("New Franchisee Count in last 12 Months is coming as Expected value 1");
						
			fc.utobj().clickElement(driver, imdp.TerminatedFranchisee12MonthsCount1);
			fc.utobj().assertPageSource(driver,location.getFranchiseID() );
			fc.utobj().printTestStep("Terminated Franchisee location is verified after clicks on Terminated Franchisee created in Last 12 Months count");
			fc.utobj().printTestStep("IM Dashboard Franchisee System Summary Report Verififcation Completed for Termination location in Last 12 Months");
			
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(groups = { "infomgr","IM_Dashboard" })
	@TestCase(createdOn = "2018-07-17", updatedOn = "2018-07-17", testCaseId = "TC_IM_Dashboard_Widget_Maximise_Minimise_Hide_Via_RegionalUser", testCaseDescription = "To verify the IM Dashboard Widgets Minimise_Maximise_Hide Options after login via Regional User")
	private void dashboardReg8() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			driver = fc.loginpage().login(driver);
			AdminAreaRegionAddAreaRegionPageTest region_page = new AdminAreaRegionAddAreaRegionPageTest();
			String region_name = fc.utobj().generateTestData("R1");
			region_page.addAreaRegion(driver, region_name);
			
			String Login_id = fc.utobj().generateTestData("R1");
			RUser.addRegionalUser(driver, Login_id, "admin123", region_name, "vimal.sharma@franconnect.com");
			
			fc.home_page().logout(driver);
			driver = fc.loginpage().loginWithUserNameAndPassword(driver, Login_id, "admin123");
			
			imc.InfoMgrDashboard(driver);
			
			InfoMgrDashboardPage imdp = new InfoMgrDashboardPage(driver);
			
			//Info Mgr Franchisee System Summary Report Widget
			
			boolean SearchElementPresent = imdp.FranchiseSystemSummaryReportWidget.isDisplayed();
			if (SearchElementPresent == true) {
				fc.utobj().moveToElementThroughAction(driver, imdp.FranchiseSystemSummaryReportWidgetMinMaxHover);
				fc.utobj().printTestStep("Click on Franchisee System Summary Report Minimise widget option");
				fc.utobj().clickElement(driver, imdp.FranchiseSystemSummaryReportWidgetMinMax);
				boolean SearchElementPresent1 = imdp.FranchiseSystemSummaryReportWidget.isDisplayed();
				if (SearchElementPresent1 == false) {
					fc.utobj().printTestStep("Franchisee System Summary Report Widget is minimized successfully");
					fc.utobj().printTestStep("Now Click on Franchisee System Summary Report Widget Maximize widget option");
					fc.utobj().moveToElementThroughAction(driver, imdp.FranchiseSystemSummaryReportWidgetMinMaxHover);
					fc.utobj().clickElement(driver, imdp.FranchiseSystemSummaryReportWidgetMinMax);

					boolean SearchElementPresent2 = imdp.FranchiseSystemSummaryReportWidget.isDisplayed();
					if (SearchElementPresent2 == true) {
						fc.utobj().printTestStep("Franchisee System Summary Report Widget is Maximized successfully");
					} else {
						fc.utobj().printTestStep("Franchisee System Summary ReportWidget is not Maximized");
					}
				} else {
					fc.utobj().printTestStep("Franchisee System Summary ReportWidget is not minimised");
				}
			} else {
				fc.utobj().moveToElementThroughAction(driver,imdp.FranchiseSystemSummaryReportWidgetMinMaxHover);
				fc.utobj().printTestStep("Click on Maximise widget option");
				fc.utobj().clickElement(driver,imdp.FranchiseSystemSummaryReportWidgetMinMax);
				boolean SearchElementPresent3 = imdp.FranchiseSystemSummaryReportWidget.isDisplayed();
				if (SearchElementPresent3 == true) {
					fc.utobj().printTestStep("Franchisee System Summary Report Widget is Maximised successfully");
					fc.utobj().printTestStep("Now Click on Franchisee System Summary Report Widget Minimise widget option");
					fc.utobj().moveToElementThroughAction(driver, imdp.FranchiseSystemSummaryReportWidgetMinMaxHover);
					fc.utobj().clickElement(driver, imdp.FranchiseSystemSummaryReportWidgetMinMax);
					boolean SearchElementPresent4 = imdp.FranchiseSystemSummaryReportWidget.isDisplayed();
					if (SearchElementPresent4 == false) {
						fc.utobj().printTestStep("Franchisee System Summary Report Widget is minimised successfully");
					} else {
						fc.utobj().printTestStep("Franchisee System Summary Report Widget is not Minimised");
					}
				} else {
					fc.utobj().printTestStep("Franchisee System Summary Report Widget is not maximised");
				}
			}

			fc.utobj().moveToElementThroughAction(driver,imdp.FranchiseSystemSummaryReportWidget);
			fc.utobj().printTestStep("Click on Hide widget option");
			fc.utobj().clickElement(driver,imdp.FranchiseSystemSummaryReportWidgetHideSignX);
			fc.utobj().acceptAlertBox(driver);
			boolean WidgetHide = imdp.FranchiseSystemSummaryReportWidget.isDisplayed();
			if (WidgetHide == false) {
				fc.utobj().printTestStep("Franchisee System Summary Report Widget is Hide Sucessfully");
			} else {
				fc.utobj().printTestStep("Franchisee System Summary Report Widget is not Hide");
			}

			
			//Info Mgr "Franchisees in Last 5 Yrs. and Total Franchisees" Widget
			
			
			
			boolean FranchiseeGraphWidget1 = imdp.GraphWidget.isDisplayed();
			if (FranchiseeGraphWidget1 == true) {
				fc.utobj().moveToElementThroughAction(driver, imdp.GraphWidgetMinMaxHover);
				fc.utobj().printTestStep("Click on Franchisees in Last 5 Yrs. and Total Franchisees Minimise widget option");
				fc.utobj().clickElement(driver, imdp.GraphWidgetMinMax);
				boolean FranchiseeGraphWidget2 = imdp.GraphWidget.isDisplayed();
				if (FranchiseeGraphWidget2 == false) {
					fc.utobj().printTestStep("Franchisees in Last 5 Yrs. and Total Franchisees Widget is minimized successfully");
					fc.utobj().printTestStep("Now Click on Franchisees in Last 5 Yrs. and Total Franchisees Widget Maximize widget option");
					fc.utobj().moveToElementThroughAction(driver, imdp.GraphWidgetMinMaxHover);
					fc.utobj().clickElement(driver, imdp.GraphWidgetMinMax);

					boolean FranchiseeGraphWidget3 = imdp.GraphWidget.isDisplayed();
					if (FranchiseeGraphWidget3 == true) {
						fc.utobj().printTestStep("Franchisees in Last 5 Yrs. and Total Franchisees Widget is Maximized successfully");
					} else {
						fc.utobj().printTestStep("Franchisees in Last 5 Yrs. and Total Franchisees Widget is not Maximized");
					}
				} else {
					fc.utobj().printTestStep("Franchisees in Last 5 Yrs. and Total Franchisees Widget is not minimised");
				}
			} else {
				fc.utobj().moveToElementThroughAction(driver,imdp.GraphWidgetMinMaxHover);
				fc.utobj().printTestStep("Click on Maximise widget option");
				fc.utobj().clickElement(driver,imdp.GraphWidgetHideSignX);
				boolean FranchiseeGraphWidget4 = imdp.GraphWidget.isDisplayed();
				if (FranchiseeGraphWidget4 == true) {
					fc.utobj().printTestStep("Franchisees in Last 5 Yrs. and Total Franchisees Widget is Maximised successfully");
					fc.utobj().printTestStep("Now Click on Franchisees in Last 5 Yrs. and Total Franchisees Widget Minimise widget option");
					fc.utobj().moveToElementThroughAction(driver, imdp.GraphWidgetMinMaxHover);
					fc.utobj().clickElement(driver, imdp.GraphWidgetMinMax);
					boolean FranchiseeGraphWidget5 = imdp.GraphWidget.isDisplayed();
					if (FranchiseeGraphWidget5 == false) {
						fc.utobj().printTestStep("Franchisees in Last 5 Yrs. and Total Franchisees Widget is minimised successfully");
					} else {
						fc.utobj().printTestStep("Franchisees in Last 5 Yrs. and Total Franchisees Widget is not Minimised");
					}
				} else {
					fc.utobj().printTestStep("Franchisees in Last 5 Yrs. and Total Franchisees Widget is not maximised");
				}
			}

			fc.utobj().moveToElementThroughAction(driver,imdp.GraphWidgetMinMaxHover);
			fc.utobj().printTestStep("Click on Hide widget option");
			fc.utobj().clickElement(driver, imdp.GraphWidgetHideSignX);
			fc.utobj().acceptAlertBox(driver);
			boolean WidgetHide1 = imdp.GraphWidget.isDisplayed();
			if (WidgetHide1 == false) {
				fc.utobj().printTestStep("Franchisees in Last 5 Yrs. and Total Franchisees Widget is Hide Sucessfully");
			} else {
				fc.utobj().printTestStep("Franchisees in Last 5 Yrs. and Total Franchisees Widget is not Hide");
			}

			
			//Info Mgr "Task View" Widget
			
			boolean TaskWidgetPresent1 = imdp.TaskViewWidget.isDisplayed();
			if (TaskWidgetPresent1 == true) {
				fc.utobj().moveToElementThroughAction(driver,imdp.TaskViewWidgetMinMaxHover);
				fc.utobj().printTestStep("Click on Task View Minimise widget option");
				fc.utobj().clickElement(driver,imdp.TaskViewWidgetMinMax);
				boolean TaskWidgetPresent2 = imdp.TaskViewWidget.isDisplayed();
				if (TaskWidgetPresent2 == false) {
					fc.utobj().printTestStep("Task View Widget is minimized successfully");
					fc.utobj().printTestStep("Now Click on Task View Widget Maximize widget option");
					fc.utobj().moveToElementThroughAction(driver,imdp.TaskViewWidgetMinMaxHover);
					fc.utobj().clickElement(driver, imdp.TaskViewWidgetMinMax);

					boolean TaskWidgetPresent3 = imdp.TaskViewWidget.isDisplayed();
					if (TaskWidgetPresent3 == true) {
						fc.utobj().printTestStep("Task View Widget is Maximized successfully");
					} else {
						fc.utobj().printTestStep("Task View Widget is not Maximized");
					}
				} else {
					fc.utobj().printTestStep("Task View Widget is not minimised");
				}
			} else {
				fc.utobj().moveToElementThroughAction(driver,imdp.TaskViewWidgetMinMaxHover);
				fc.utobj().printTestStep("Click on Maximise widget option");
				fc.utobj().clickElement(driver, imdp.TaskViewWidgetMinMax);
				boolean TaskWidgetPresent4 =imdp.TaskViewWidget.isDisplayed();
				if (TaskWidgetPresent4 == true) {
					fc.utobj().printTestStep("Task View Widget is Maximised successfully");
					fc.utobj().printTestStep("Now Click on Task View Widget Minimise widget option");
					fc.utobj().moveToElementThroughAction(driver,imdp.TaskViewWidgetMinMaxHover);
					fc.utobj().clickElement(driver,imdp.TaskViewWidgetMinMax);
					boolean TaskWidgetPresent5 = imdp.TaskViewWidget.isDisplayed();
					if (TaskWidgetPresent5 == false) {
						fc.utobj().printTestStep("Task View Widget is minimised successfully");
					} else {
						fc.utobj().printTestStep("Task View Widget is not Minimised");
					}
				} else {
					fc.utobj().printTestStep("Task View Widget is not maximised");
				}
			}

			fc.utobj().moveToElementThroughAction(driver,imdp.TaskViewWidgetMinMaxHover);
			fc.utobj().printTestStep("Click on Hide widget option");
			fc.utobj().clickElement(driver,imdp.TaskViewWidgetHideSignX);
			fc.utobj().acceptAlertBox(driver);
			boolean TaskWidgetHide = imdp.TaskViewWidget.isDisplayed();
			if (TaskWidgetHide == false) {
				fc.utobj().printTestStep("Task View Widget is Hide Sucessfully");
			} else {
				fc.utobj().printTestStep("Task View Widget is not Hide");
			}
			
			
			
			//Info Mgr "Renewals Due" Widget
			
			boolean RenewalsDueWidgetPresent1 = imdp.RenewalDueWidget.isDisplayed();
			if (RenewalsDueWidgetPresent1 == true) {
				fc.utobj().moveToElementThroughAction(driver, imdp.RenewalDueWidgetMinMaxHover);
				fc.utobj().printTestStep("Click on Renewals Due Minimise widget option");
				fc.utobj().clickElement(driver, imdp.RenewalDueWidgetMinMax);
				boolean RenewalsDueWidgetPresent2 = imdp.RenewalDueWidget.isDisplayed();
				if (RenewalsDueWidgetPresent2 == false) {
					fc.utobj().printTestStep("Renewals Due Widget is minimized successfully");
					fc.utobj().printTestStep("Now Click on Renewals Due Widget Maximize widget option");
					fc.utobj().moveToElementThroughAction(driver, imdp.RenewalDueWidgetMinMaxHover);
					fc.utobj().clickElement(driver, imdp.RenewalDueWidgetMinMax);

					boolean RenewalsDueWidgetPresent3 = imdp.RenewalDueWidget.isDisplayed();
					if (RenewalsDueWidgetPresent3 == true) {
						fc.utobj().printTestStep("Renewals Due Widget is Maximized successfully");
					} else {
						fc.utobj().printTestStep("Renewals Due Widget is not Maximized");
					}
				} else {
					fc.utobj().printTestStep("Renewals Due Widget is not minimised");
				}
			} else {
				fc.utobj().moveToElementThroughAction(driver,imdp.RenewalDueWidgetMinMaxHover);
				fc.utobj().printTestStep("Click on Maximise widget option");
				fc.utobj().clickElement(driver, imdp.RenewalDueWidgetMinMax);
				boolean RenewalsDueWidgetPresent4 = imdp.RenewalDueWidget.isDisplayed();
				if (RenewalsDueWidgetPresent4 == true) {
					fc.utobj().printTestStep("Renewals Due Widget is Maximised successfully");
					fc.utobj().printTestStep("Now Click on Renewals Due Widget Minimise widget option");
					fc.utobj().moveToElementThroughAction(driver,imdp.RenewalDueWidgetMinMaxHover);
					fc.utobj().clickElement(driver, imdp.RenewalDueWidgetMinMax);
					boolean RenewalsDueWidgetPresent5 = imdp.RenewalDueWidget.isDisplayed();
					if (RenewalsDueWidgetPresent5 == false) {
						fc.utobj().printTestStep("Renewals Due Widget is minimised successfully");
					} else {
						fc.utobj().printTestStep("Renewals Due Widget is not Minimised");
					}
				} else {
					fc.utobj().printTestStep("Renewals Due Widget is not maximised");
				}
			}

			fc.utobj().moveToElementThroughAction(driver,imdp.RenewalDueWidgetMinMaxHover);
			fc.utobj().printTestStep("Click on Hide widget option");
			fc.utobj().clickElement(driver, imdp.RenewalDueWidgetHideSignX);
			fc.utobj().acceptAlertBox(driver);
			boolean RenewalsDueWidgetHide = imdp.RenewalDueWidget.isDisplayed();
			if (RenewalsDueWidgetHide == false) {
				fc.utobj().printTestStep("Renewals Due Widget is Hide Sucessfully");
			} else {
				fc.utobj().printTestStep("Renewals Due Widget is not Hide");
			}
			
			
			
			//Info Mgr "Email History" Widget
			
			boolean EmailHistoryWidgetPresent1 = imdp.EmailHistoryWidget.isDisplayed();
			if (EmailHistoryWidgetPresent1 == true) {
				fc.utobj().moveToElementThroughAction(driver,imdp.EmailHistoryWidgetMinMaxHover);
				fc.utobj().printTestStep("Click on Email History Minimise widget option");
				fc.utobj().clickElement(driver, imdp.EmailHistoryWidgetMinMax);
				boolean EmailHistoryWidgetPresent2 = imdp.EmailHistoryWidget.isDisplayed();
				if (EmailHistoryWidgetPresent2 == false) {
					fc.utobj().printTestStep("Email History Widget is minimized successfully");
					fc.utobj().printTestStep("Now Click on Email History Widget Maximize widget option");
					fc.utobj().moveToElementThroughAction(driver,imdp.EmailHistoryWidgetMinMaxHover);
					fc.utobj().clickElement(driver, imdp.EmailHistoryWidgetMinMax);

					boolean EmailHistoryWidgetPresent3 = imdp.EmailHistoryWidget.isDisplayed();
					if (EmailHistoryWidgetPresent3 == true) {
						fc.utobj().printTestStep("Email History Widget is Maximized successfully");
					} else {
						fc.utobj().printTestStep("Email History Widget is not Maximized");
					}
				} else {
					fc.utobj().printTestStep("Email History Widget is not minimised");
				}
			} else {
				fc.utobj().moveToElementThroughAction(driver,imdp.EmailHistoryWidgetMinMaxHover);
				fc.utobj().printTestStep("Click on Maximise widget option");
				fc.utobj().clickElement(driver, imdp.EmailHistoryWidgetMinMax);
				boolean EmailHistoryWidgetPresent4 = imdp.EmailHistoryWidget.isDisplayed();
				if (EmailHistoryWidgetPresent4 == true) {
					fc.utobj().printTestStep("Email History Widget is Maximised successfully");
					fc.utobj().printTestStep("Now Click on Email History Widget Minimise widget option");
					fc.utobj().moveToElementThroughAction(driver,imdp.EmailHistoryWidgetMinMaxHover);
					fc.utobj().clickElement(driver, imdp.EmailHistoryWidgetMinMax);
					boolean EmailHistoryWidgetPresent5 = imdp.EmailHistoryWidget.isDisplayed();
					if (EmailHistoryWidgetPresent5 == false) {
						fc.utobj().printTestStep("Email History Widget is minimised successfully");
					} else {
						fc.utobj().printTestStep("Email History Widget is not Minimised");
					}
				} else {
					fc.utobj().printTestStep("Email History Widget is not maximised");
				}
			}

			fc.utobj().moveToElementThroughAction(driver, imdp.EmailHistoryWidgetMinMaxHover);
			fc.utobj().printTestStep("Click on Hide widget option");
			fc.utobj().clickElement(driver,imdp.EmailHistoryWidgetHideSignX);
			fc.utobj().acceptAlertBox(driver);
			boolean EmailHistoryWidgetHide = imdp.EmailHistoryWidget.isDisplayed();
			if (EmailHistoryWidgetHide == false) {
				fc.utobj().printTestStep("Email History Widget is Hide Sucessfully");
			} else {
				fc.utobj().printTestStep("Email History Widget is not Hide");
			}
			
			
			//Info Mgr "Recent 5 Visits / Recent 5 QA History Inspections" Widget
			
			boolean RecentVisitOrQAInspectionWidgetPresent1 = imdp.RecentQaHistoryWidget.isDisplayed();
			if (RecentVisitOrQAInspectionWidgetPresent1 == true) {
				fc.utobj().moveToElementThroughAction(driver, imdp.RecentQaHistoryWidgetMinMaxHover);
				fc.utobj().printTestStep("Click on Recent 5 Visits / Recent 5 QA History Inspections Minimise widget option");
				fc.utobj().clickElement(driver, imdp.RecentQaHistoryWidgetMinMax);
				boolean RecentVisitOrQAInspectionWidgetPresent2 = imdp.RecentQaHistoryWidget.isDisplayed();
				if (RecentVisitOrQAInspectionWidgetPresent2 == false) {
					fc.utobj().printTestStep("Recent 5 Visits / Recent 5 QA History Inspections Widget is minimized successfully");
					fc.utobj().printTestStep("Now Click on Recent 5 Visits / Recent 5 QA History Inspections Widget Maximize widget option");
					fc.utobj().moveToElementThroughAction(driver,imdp.RecentQaHistoryWidgetMinMaxHover);
					fc.utobj().clickElement(driver,imdp.RecentQaHistoryWidgetMinMax);

					boolean RecentVisitOrQAInspectionWidgetPresent3 = imdp.RecentQaHistoryWidget.isDisplayed();
					if (RecentVisitOrQAInspectionWidgetPresent3 == true) {
						fc.utobj().printTestStep("Recent 5 Visits / Recent 5 QA History Inspections Widget is Maximized successfully");
					} else {
						fc.utobj().printTestStep("Recent 5 Visits / Recent 5 QA History Inspections Widget is not Maximized");
					}
				} else {
					fc.utobj().printTestStep("Recent 5 Visits / Recent 5 QA History Inspections Widget is not minimised");
				}
			} else {
				fc.utobj().moveToElementThroughAction(driver,imdp.RecentQaHistoryWidgetMinMaxHover);
				fc.utobj().printTestStep("Click on Maximise widget option");
				fc.utobj().clickElement(driver, imdp.RecentQaHistoryWidgetMinMax);
				boolean RecentVisitOrQAInspectionWidgetPresent4 = imdp.RecentQaHistoryWidget.isDisplayed();
				if (RecentVisitOrQAInspectionWidgetPresent4 == true) {
					fc.utobj().printTestStep("Recent 5 Visits / Recent 5 QA History Inspections Widget is Maximised successfully");
					fc.utobj().printTestStep("Now Click on Recent 5 Visits / Recent 5 QA History Inspections Widget Minimise widget option");
					fc.utobj().moveToElementThroughAction(driver,imdp.RecentQaHistoryWidgetMinMaxHover);
					fc.utobj().clickElement(driver,imdp.RecentQaHistoryWidgetMinMax);
					boolean RecentVisitOrQAInspectionWidgetPresent5 = imdp.RecentQaHistoryWidget.isDisplayed();
					if (RecentVisitOrQAInspectionWidgetPresent5 == false) {
						fc.utobj().printTestStep("Recent 5 Visits / Recent 5 QA History Inspections Widget is minimised successfully");
					} else {
						fc.utobj().printTestStep("Recent 5 Visits / Recent 5 QA History Inspections Widget is not Minimised");
					}
				} else {
					fc.utobj().printTestStep("Recent 5 Visits / Recent 5 QA History Inspections Widget is not maximised");
				}
			}

			fc.utobj().moveToElementThroughAction(driver,imdp.RecentQaHistoryWidgetMinMaxHover);
			fc.utobj().printTestStep("Click on Hide widget option");
			fc.utobj().clickElement(driver, imdp.RecentQaHistoryWidgetHideSignX);
			fc.utobj().acceptAlertBox(driver);
			boolean RecentVisitOrQAInspectionWidgetHide = imdp.RecentQaHistoryWidget.isDisplayed();
			if (RecentVisitOrQAInspectionWidgetHide == false) {
				fc.utobj().printTestStep("Recent 5 Visits / Recent 5 QA History Inspections Widget is Hide Sucessfully");
			} else {
				fc.utobj().printTestStep("Recent 5 Visits / Recent 5 QA History Inspections Widget is not Hide");
			}
			
						

			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	@Test(groups = { "infomgr","IM_Dashboard" })
	@TestCase(createdOn = "2018-07-17", updatedOn = "2018-07-17", testCaseId = "TC_IM_Dashboard_Widget_ManageWidgets_Via_RegionalUser", testCaseDescription = "To verify the IM Dashboard Manage Widgets Options after login via Regional User. It will firs remove all Widget from Dashboard and then again add on Dashboard.")
	private void dashboardReg9() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		InfoMgrDashboardPage imdp = new InfoMgrDashboardPage(driver);
		try {
			driver = fc.loginpage().login(driver);			
			AdminAreaRegionAddAreaRegionPageTest region_page = new AdminAreaRegionAddAreaRegionPageTest();
			String region_name = fc.utobj().generateTestData("R1");
			region_page.addAreaRegion(driver, region_name);
			
			String Login_id = fc.utobj().generateTestData("R1");
			RUser.addRegionalUser(driver, Login_id, "admin123", region_name, "vimal.sharma@franconnect.com");
			
			fc.home_page().logout(driver);
			driver = fc.loginpage().loginWithUserNameAndPassword(driver, Login_id, "admin123");
			
			
			// Navigate to Manage Widget Option and Remove Hide all Widgets from Info Mgr Dashaboard 
			
			imc.InfoMgrDashboard(driver);
			fc.utobj().printTestStep("Click on Info Mgr Manage Widget Option at Dashboard to hide all widgets");
			fc.utobj().clickElement(driver, imdp.manageWidget);
			fc.utobj().clickElement(driver, imdp.mngEmailHistoryWidget);
			fc.utobj().clickElement(driver, imdp.mngSummaryReportWidget);
			fc.utobj().clickElement(driver, imdp.mngTotalFranchiseeWidget);
			fc.utobj().clickElement(driver, imdp.mngRecentVisitWidget);
			fc.utobj().clickElement(driver, imdp.mngRenewalDueWidget);
			fc.utobj().clickElement(driver, imdp.mngTaskViewWidget);
			fc.utobj().clickElement(driver, imdp.mngWidgetSaveButton);
			
			fc.utobj().printTestStep("Info Mgr Dashboard all widgets are hide through Manage Widget Option and now verifying that all widgets are hide from dashboard");
			
			//Verify Franchise System Summary Report Widget is not displaying after hide through Manage Widget Option
			
			boolean FranchiseeSystemSummaryReportWidgetPresent = imdp.SummaryReportWidgetVisible.isDisplayed();
			if(FranchiseeSystemSummaryReportWidgetPresent==false)
			{
				fc.utobj().printTestStep("Franchise System Summary Report Widget is Hide Successfully through manage Widget Option ");
			}
			else
			{
				fc.utobj().printTestStep("Franchise System Summary Report Widget is not getting Hide through manage Widget Option ");
			}
			
			//Verify Franchisees in Last 5 Yrs. and Total Franchisees Widget is not displaying after hide through Manage Widget Option
			
			boolean TotalFranchiseeGraphWidgetPresent = imdp.TotalFranchiseeWidgetVisible.isDisplayed();
			if(TotalFranchiseeGraphWidgetPresent==false)
			{
				fc.utobj().printTestStep("Franchisees in Last 5 Yrs. and Total Franchisees Widget is Hide Successfully through manage Widget Option ");
			}
			else
			{
				fc.utobj().printTestStep("Franchisees in Last 5 Yrs. and Total Franchisees Widget is not getting Hide through manage Widget Option ");
			}
				
			//Verify Task View Widget is not displaying after hide through Manage Widget Option
			
			boolean TaskViewWidgetPresent = imdp.TaskViewWidgetVisible.isDisplayed();
			if(TaskViewWidgetPresent==false)
			{
				fc.utobj().printTestStep("Task View Widget is Hide Successfully through manage Widget Option ");
			}
			else
			{
				fc.utobj().printTestStep("Task View Widget is not getting Hide through manage Widget Option ");
			}
			
			//Verify Renewals Due Widget is not displaying after hide through Manage Widget Option
			
			boolean RenewalsDueWidgetPresent = imdp.RenewalDueWidgetVisible.isDisplayed();
			if(RenewalsDueWidgetPresent==false)
			{
				fc.utobj().printTestStep("Renewals Due Widget is Hide Successfully through manage Widget Option ");
			}
			else
			{
				fc.utobj().printTestStep("Renewals Due Widget is not getting Hide through manage Widget Option ");
			}		
			
			//Verify Email History Widget is not displaying after hide through Manage Widget Option
			
			boolean EmailHistoryWidgetPresent = imdp.EmailHistoryReportVisible.isDisplayed();
			if(EmailHistoryWidgetPresent==false)
			{
				fc.utobj().printTestStep("Email History Widget is Hide Successfully through manage Widget Option ");
			}
			else
			{
				fc.utobj().printTestStep("Email History Widget is not getting Hide through manage Widget Option ");
			}		
			
			//Verify Recent 5 Widget / QA Inspections Widget is not displaying after hide through Manage Widget Option
			
			boolean QAHistoryWidgetPresent = imdp.RecentVisitWidgetVisible.isDisplayed();
			if(QAHistoryWidgetPresent==false)
			{
				fc.utobj().printTestStep("Recent 5 Widget / QA Inspections Widget is Hide Successfully through manage Widget Option ");
			}
			else
			{
				fc.utobj().printTestStep("Recent 5 Widget / QA Inspections Widget is not getting Hide through manage Widget Option ");
			}
			
			//Click on Info Mgr Manage Widget Option at Dashboard to Unhide all Widgets
			
			fc.utobj().printTestStep("Click on Info Mgr Manage Widget Option at Dashboard to Unhide all Widgets");
			fc.utobj().clickElement(driver, imdp.manageWidget);
			fc.utobj().clickElement(driver, imdp.mngEmailHistoryWidget);
			fc.utobj().clickElement(driver, imdp.mngSummaryReportWidget);
			fc.utobj().clickElement(driver, imdp.mngTotalFranchiseeWidget);
			fc.utobj().clickElement(driver, imdp.mngRecentVisitWidget);
			fc.utobj().clickElement(driver, imdp.mngRenewalDueWidget);
			fc.utobj().clickElement(driver, imdp.mngTaskViewWidget);
			fc.utobj().clickElement(driver, imdp.mngWidgetSaveButton);
			
			fc.utobj().printTestStep("Info Mgr Dashboard all widgets are unhide through Manage Widget Option and now verifying that all widgets are visible on dashboard");
			
			//Verify Franchise System Summary Report Widget is displaying after unhide through Manage Widget Option
			
			boolean FranchiseeSystemSummaryReportWidgetPresent1 = imdp.SummaryReportWidgetVisible.isDisplayed();
			if(FranchiseeSystemSummaryReportWidgetPresent1==true)
			{
				fc.utobj().printTestStep("Franchise System Summary Report Widget is unhide Successfully through manage Widget Option ");
			}
			else
			{
				fc.utobj().printTestStep("Franchise System Summary Report Widget is not getting unhide through manage Widget Option ");
			}
			
			//Verify Franchisees in Last 5 Yrs. and Total Franchisees Widget is displaying after unhide through Manage Widget Option
			
			boolean TotalFranchiseeGraphWidgetPresent1 = imdp.TotalFranchiseeWidgetVisible.isDisplayed();
			if(TotalFranchiseeGraphWidgetPresent1==true)
			{
				fc.utobj().printTestStep("Franchisees in Last 5 Yrs. and Total Franchisees Widget is unhide Successfully through manage Widget Option ");
			}
			else
			{
				fc.utobj().printTestStep("Franchisees in Last 5 Yrs. and Total Franchisees Widget is not getting unhide through manage Widget Option ");
			}
				
			//Verify Task View Widget is displaying after unhide through Manage Widget Option
			
			boolean TaskViewWidgetPresent1 = imdp.TaskViewWidgetVisible.isDisplayed();
			if(TaskViewWidgetPresent1==true)
			{
				fc.utobj().printTestStep("Task View Widget is unhide Successfully through manage Widget Option ");
			}
			else
			{
				fc.utobj().printTestStep("Task View Widget is not getting unhide through manage Widget Option ");
			}
			
			//Verify Renewals Due Widget is displaying after unhide through Manage Widget Option
			
			boolean RenewalsDueWidgetPresent1 = imdp.RenewalDueWidgetVisible.isDisplayed();
			if(RenewalsDueWidgetPresent1==true)
			{
				fc.utobj().printTestStep("Renewals Due Widget is unhide Successfully through manage Widget Option ");
			}
			else
			{
				fc.utobj().printTestStep("Renewals Due Widget is not getting unhide through manage Widget Option ");
			}		
			
			//Verify Email History Widget is displaying after unhide through Manage Widget Option
			
			boolean EmailHistoryWidgetPresent1 = imdp.EmailHistoryReportVisible.isDisplayed();
			if(EmailHistoryWidgetPresent1==true)
			{
				fc.utobj().printTestStep("Email History Widget is Unhide Successfully through manage Widget Option ");
			}
			else
			{
				fc.utobj().printTestStep("Email History Widget is not getting unhide through manage Widget Option ");
			}		
			
			//Verify Recent 5 Widget / QA Inspections Widget is not displaying after unhide through Manage Widget Option
			
			boolean QAHistoryWidgetPresent1 = imdp.RecentVisitWidgetVisible.isDisplayed();
			if(QAHistoryWidgetPresent1==true)
			{
				fc.utobj().printTestStep("Recent 5 Widget / QA Inspections Widget is unhide Successfully through manage Widget Option ");
			}
			else
			{
				fc.utobj().printTestStep("Recent 5 Widget / QA Inspections Widget is not getting unhide through manage Widget Option ");
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			
			
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(groups = { "infomgr","IM_Dashboard" })
	@TestCase(createdOn = "2018-07-17", updatedOn = "2018-07-17", testCaseId = "TC_IM_Dashboard_ManageWidget_RestoreDefaultSettings_Via_RegionalUser", testCaseDescription = "To verify the IM Dashboard ManageWidget_RestoreDefaultSettings of Widget via Regional User ")
	private void dashboardReg10() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		InfoMgrDashboardPage imdp = new InfoMgrDashboardPage(driver);
		try {
			driver = fc.loginpage().login(driver);	
			AdminAreaRegionAddAreaRegionPageTest region_page = new AdminAreaRegionAddAreaRegionPageTest();
			String region_name = fc.utobj().generateTestData("R1");
			region_page.addAreaRegion(driver, region_name);
			String Login_id = fc.utobj().generateTestData("R1");
			RUser.addRegionalUser(driver, Login_id, "admin123", region_name, "vimal.sharma@franconnect.com");
			fc.home_page().logout(driver);
			driver = fc.loginpage().loginWithUserNameAndPassword(driver, Login_id, "admin123");
			
			// Navigate to Manage Widget Option and Remove Hide all Widgets from Info Mgr Dashaboard 
			
			imc.InfoMgrDashboard(driver);
			fc.utobj().printTestStep("Click on Info Mgr Manage Widget Option at Dashboard to hide all widgets");
			fc.utobj().clickElement(driver, imdp.manageWidget);
			fc.utobj().clickElement(driver, imdp.mngEmailHistoryWidget);
			fc.utobj().clickElement(driver, imdp.mngSummaryReportWidget);
			fc.utobj().clickElement(driver, imdp.mngTotalFranchiseeWidget);
			fc.utobj().clickElement(driver, imdp.mngRecentVisitWidget);
			fc.utobj().clickElement(driver, imdp.mngRenewalDueWidget);
			fc.utobj().clickElement(driver, imdp.mngTaskViewWidget);
			fc.utobj().clickElement(driver, imdp.mngWidgetSaveButton);
			
			fc.utobj().printTestStep("Info Mgr Dashboard all widgets are hide through Manage Widget Option and now verifying that all widgets are hide from dashboard");
			
			//Verify Franchise System Summary Report Widget is not displaying after hide through Manage Widget Option
			
			boolean FranchiseeSystemSummaryReportWidgetPresent = imdp.SummaryReportWidgetVisible.isDisplayed();
			if(FranchiseeSystemSummaryReportWidgetPresent==false)
			{
				fc.utobj().printTestStep("Franchise System Summary Report Widget is Hide Successfully through manage Widget Option ");
			}
			else
			{
				fc.utobj().printTestStep("Franchise System Summary Report Widget is not getting Hide through manage Widget Option ");
			}
			
			//Verify Franchisees in Last 5 Yrs. and Total Franchisees Widget is not displaying after hide through Manage Widget Option
			
			boolean TotalFranchiseeGraphWidgetPresent = imdp.TotalFranchiseeWidgetVisible.isDisplayed();
			if(TotalFranchiseeGraphWidgetPresent==false)
			{
				fc.utobj().printTestStep("Franchisees in Last 5 Yrs. and Total Franchisees Widget is Hide Successfully through manage Widget Option ");
			}
			else
			{
				fc.utobj().printTestStep("Franchisees in Last 5 Yrs. and Total Franchisees Widget is not getting Hide through manage Widget Option ");
			}
				
			//Verify Task View Widget is not displaying after hide through Manage Widget Option
			
			boolean TaskViewWidgetPresent = imdp.TaskViewWidgetVisible.isDisplayed();
			if(TaskViewWidgetPresent==false)
			{
				fc.utobj().printTestStep("Task View Widget is Hide Successfully through manage Widget Option ");
			}
			else
			{
				fc.utobj().printTestStep("Task View Widget is not getting Hide through manage Widget Option ");
			}
			
			//Verify Renewals Due Widget is not displaying after hide through Manage Widget Option
			
			boolean RenewalsDueWidgetPresent = imdp.RenewalDueWidgetVisible.isDisplayed();
			if(RenewalsDueWidgetPresent==false)
			{
				fc.utobj().printTestStep("Renewals Due Widget is Hide Successfully through manage Widget Option ");
			}
			else
			{
				fc.utobj().printTestStep("Renewals Due Widget is not getting Hide through manage Widget Option ");
			}		
			
			//Verify Email History Widget is not displaying after hide through Manage Widget Option
			
			boolean EmailHistoryWidgetPresent = imdp.EmailHistoryReportVisible.isDisplayed();
			if(EmailHistoryWidgetPresent==false)
			{
				fc.utobj().printTestStep("Email History Widget is Hide Successfully through manage Widget Option ");
			}
			else
			{
				fc.utobj().printTestStep("Email History Widget is not getting Hide through manage Widget Option ");
			}		
			
			//Verify Recent 5 Widget / QA Inspections Widget is not displaying after hide through Manage Widget Option
			
			boolean QAHistoryWidgetPresent = imdp.RecentVisitWidgetVisible.isDisplayed();
			if(QAHistoryWidgetPresent==false)
			{
				fc.utobj().printTestStep("Recent 5 Widget / QA Inspections Widget is Hide Successfully through manage Widget Option ");
			}
			else
			{
				fc.utobj().printTestStep("Recent 5 Widget / QA Inspections Widget is not getting Hide through manage Widget Option ");
			}
			
			//Click on Info Mgr Manage Widget Option at Dashboard to Unhide all Widgets
			
			fc.utobj().printTestStep("Click on Info Mgr Manage Widget Option at Dashboard to Restore all Widgets");
			fc.utobj().clickElement(driver, imdp.manageWidget);
			fc.utobj().clickElement(driver, "//a[@class='bText12lnkNew']");
			
			Thread.sleep(5000);
			fc.utobj().printTestStep("Info Mgr Dashboard all widgets are unhide through Manage Widgets Restore All Widgets link and now verifying that all widgets are visible on dashboard");
			
			//Verify Franchise System Summary Report Widget is displaying after unhide through Manage Widget Option
			
			boolean FranchiseeSystemSummaryReportWidgetPresent1 = imdp.SummaryReportWidgetVisible.isDisplayed();
			if(FranchiseeSystemSummaryReportWidgetPresent1==true)
			{
				fc.utobj().printTestStep("Franchise System Summary Report Widget is unhide Successfully through manage Widget Option ");
			}
			else
			{
				fc.utobj().printTestStep("Franchise System Summary Report Widget is not getting unhide through manage Widget Option ");
			}
			
			//Verify Franchisees in Last 5 Yrs. and Total Franchisees Widget is displaying after unhide through Manage Widget Option
			
			boolean TotalFranchiseeGraphWidgetPresent1 = imdp.TotalFranchiseeWidgetVisible.isDisplayed();
			if(TotalFranchiseeGraphWidgetPresent1==true)
			{
				fc.utobj().printTestStep("Franchisees in Last 5 Yrs. and Total Franchisees Widget is unhide Successfully through manage Widget Option ");
			}
			else
			{
				fc.utobj().printTestStep("Franchisees in Last 5 Yrs. and Total Franchisees Widget is not getting unhide through manage Widget Option ");
			}
				
			//Verify Task View Widget is displaying after unhide through Manage Widget Option
			
			boolean TaskViewWidgetPresent1 = imdp.TaskViewWidgetVisible.isDisplayed();
			if(TaskViewWidgetPresent1==true)
			{
				fc.utobj().printTestStep("Task View Widget is unhide Successfully through manage Widget Option ");
			}
			else
			{
				fc.utobj().printTestStep("Task View Widget is not getting unhide through manage Widget Option ");
			}
			
			//Verify Renewals Due Widget is displaying after unhide through Manage Widget Option
			
			boolean RenewalsDueWidgetPresent1 = imdp.RenewalDueWidgetVisible.isDisplayed();
			if(RenewalsDueWidgetPresent1==true)
			{
				fc.utobj().printTestStep("Renewals Due Widget is unhide Successfully through manage Widget Option ");
			}
			else
			{
				fc.utobj().printTestStep("Renewals Due Widget is not getting unhide through manage Widget Option ");
			}		
			
			//Verify Email History Widget is displaying after unhide through Manage Widget Option
			
			boolean EmailHistoryWidgetPresent1 = imdp.EmailHistoryReportVisible.isDisplayed();
			if(EmailHistoryWidgetPresent1==true)
			{
				fc.utobj().printTestStep("Email History Widget is Unhide Successfully through manage Widget Option ");
			}
			else
			{
				fc.utobj().printTestStep("Email History Widget is not getting unhide through manage Widget Option ");
			}		
			
			//Verify Recent 5 Widget / QA Inspections Widget is not displaying after unhide through Manage Widget Option
			
			boolean QAHistoryWidgetPresent1 = imdp.RecentVisitWidgetVisible.isDisplayed();
			if(QAHistoryWidgetPresent1==true)
			{
				fc.utobj().printTestStep("Recent 5 Widget / QA Inspections Widget is unhide Successfully through manage Widget Option ");
			}
			else
			{
				fc.utobj().printTestStep("Recent 5 Widget / QA Inspections Widget is not getting unhide through manage Widget Option ");
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			
			
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	// Verify Task View Widget by Regional User
	
	@Test(groups = { "infomgr","IM_Dashboard","IM" })
	@TestCase(createdOn = "2018-07-17", updatedOn = "2018-07-17", testCaseId = "TC_IM_Dashboard_RegionalUser_TaskView_WidgetVerify", testCaseDescription = "To verify that Task Assigned to Logged in Regional User are coming at IM Dashboard Task View Widget after Login via Regional User ")
	private void dashboardReg11() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Location location = new Location();
		LocationData ld = new LocationData();
		InfoMgrDashboardPage imdp =new InfoMgrDashboardPage(driver);

		try {
			driver = fc.loginpage().login(driver);
			AdminAreaRegionAddAreaRegionPageTest region_page = new AdminAreaRegionAddAreaRegionPageTest();
			String region_name = fc.utobj().generateTestData("R1");
			region_page.addAreaRegion(driver, region_name);
			String Login_id = fc.utobj().generateTestData("R1");
			RUser.addRegionalUser(driver, Login_id, "admin123", region_name, "vimal.sharma@franconnect.com");


			location = fc.commonMethods().getDefaultDataFor_Location(location);
			fc.adminpage().adminAddNewFranchiseLocationLnk(driver);
			location.setAreaRegion(region_name);
			ld.filllocationInfo(driver, location).submit(driver);
			fc.adminpage().manageFranchiseLocationLnk(driver).SearchFranchise(driver, location).VerifyLocationExists(driver, location);
			System.out.println(location.getFranchiseID());
			imc.InfoMgrFranchisees(driver);
			imc.SearchFranchiseeAndClick(driver, location.getFranchiseID());
			
			
			TaskWindowUI taskui = new TaskWindowUI(driver);
			String DivUserName = Login_id+ " " +Login_id;
			fc.utobj().clickLink(driver, "Add Task");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().selectValFromMultiSelectwithoutSelectAll(driver, taskui.OtherUserDropDown_MultiSelect_ByID, DivUserName);
			fc.utobj().selectDropDownByVisibleText(driver, taskui.Status_Select, "Work In Progress");
			fc.utobj().sendKeys(driver, taskui.Subject, "Info Mgr Task Subject for Dashboard Widget Verififcation by Regional User");
			fc.utobj().clickElement(driver, taskui.TimelessTaskId_CheckBox);
			//fc.utobj().sendKeys(driver, taskui.taskDescription, "Info Mgr Task Comments for Dashboard Widget Verififcation by Regional User");
			fc.utobj().clickElement(driver, taskui.Create_Input_ByValue);
			fc.utobj().switchFrameToDefault(driver);			
			fc.utobj().assertPageSource(driver, "Info Mgr Task Subject for Dashboard Widget Verififcation by Regional User");	
			fc.utobj().printTestStep("Task Created and visible at Center Info Page");
			
			fc.home_page().logout(driver);
			driver = fc.loginpage().loginWithUserNameAndPassword(driver, Login_id, "admin123");
			imc.InfoMgrDashboard(driver);
			String OwnerName= location.getOwnerFirstName()+" "+location.getOwnerLastName();
			fc.utobj().printTestStep("IM Dashboard Task View Widget Verififcation Start with Regional User");
			
			verifyTaskViewRegional(driver, "Info Mgr Task Subject for Dashboard Widget Verififcation by Regional User");
			verifyTaskViewRegional(driver, location.getFranchiseID());
			verifyTaskViewRegional(driver, OwnerName);
			verifyTaskViewRegional(driver, "Work In Progress");
			//verifyTaskViewRegional(driver, fc.utobj().getCurrentDateUSFormat());
			
			fc.utobj().printTestStep("Task Assigned to Logged in User is visible on Dashboard Task View Widget.");
			
			//Now Verifying the More Info Link Redirection from Task View Widget.
			fc.utobj().printTestStep("Now Verifying the More Info Link Redirection from Task View Widget.");
			fc.utobj().clickElement(driver, imdp.TaskViewMoreInfoLink);
			
			List<String>  taskdetails = new ArrayList<String>();
			taskdetails.add(location.getFranchiseID());
			taskdetails.add(OwnerName);
			//taskdetails.add(fc.utobj().getCurrentDateUSFormat());
			taskdetails.add("Info Mgr Task Subject for Dashboard Widget Verififcation by Regional User");
			taskdetails.add("Work In Progress");
			taskdetails.add("Associated With");
			
			fc.utobj().assertPageSourceWithMultipleRecords(driver, taskdetails);
			fc.utobj().printTestStep("More Info Link successfully Navigated to Task Page and Added Task details are also verified");
			
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	// Verify RenewalDue Widget by Regional User
	
	@Test(groups = { "infomgr","IM_Dashboard" })
	@TestCase(createdOn = "2018-07-17", updatedOn = "2018-07-17", testCaseId = "TC_IM_Dashboard_RegionalUser_RenewalDueWidget", testCaseDescription = "To verify the IM Dashboard Email History Widget after Login via Regional User")
	private void dashboardReg12() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Location location = new Location();
		LocationData ld = new LocationData();
		
		InfoMgrBasePage imbp = new InfoMgrBasePage(driver);

		try {
			driver = fc.loginpage().login(driver);
			AdminAreaRegionAddAreaRegionPageTest region_page = new AdminAreaRegionAddAreaRegionPageTest();
			String region_name = fc.utobj().generateTestData("R1");
			region_page.addAreaRegion(driver, region_name);
			String Login_id = fc.utobj().generateTestData("R1");
			RUser.addRegionalUser(driver, Login_id, "admin123", region_name, "vimal.sharma@franconnect.com");
			
			
			location = fc.commonMethods().getDefaultDataFor_Location(location);
			fc.adminpage().adminAddNewFranchiseLocationLnk(driver);
			location.setAreaRegion(region_name);
			ld.filllocationInfo(driver, location).submit(driver);
			fc.adminpage().manageFranchiseLocationLnk(driver).SearchFranchise(driver, location).VerifyLocationExists(driver, location);
			System.out.println(location.getFranchiseID());
			
			 /*String FranchiseeID="FIMs10111863";
			 String Login_id="div1j10111617";
			 
			 String OwnerName = "Dr. FIMn10111878 Ownerr10111844";*/
			
			
			fc.home_page().logout(driver);
			driver = fc.loginpage().loginWithUserNameAndPassword(driver, Login_id, "admin123");
			
			imc.InfoMgrFranchisees(driver);
			imc.SearchFranchiseeAndClick(driver, location.getFranchiseID());
			// Adding Data for Renewals Due Widgets in Agreement Tab
			
			fc.utobj().clickElement(driver, imbp.lnkAgreement);
			InfoMgrAgreementPage imag = new InfoMgrAgreementPage(driver);
			fc.utobj().sendKeys(driver, imag.txtApprovedDate, fc.utobj().getCurrentDateUSFormat());
			fc.utobj().sendKeys(driver, imag.txtExpirationDate, fc.utobj().getFutureDateUSFormat(2));
			fc.utobj().clickElementByJS(driver, imag.btnsubmit);	
			imc.InfoMgrDashboard(driver);
			
			String OwnerName= location.getSalutation()+" "+  location.getOwnerFirstName()+" "+location.getOwnerLastName();
			fc.utobj().printTestStep("IM Dashboard Renewals Due Widget Verififcation Start with Regional User");
			
			verifyRenewalsDue(driver, OwnerName,location.getFranchiseID());
			verifyRenewalsDue(driver, fc.utobj().getFutureDateUSFormat(2),location.getFranchiseID());
			fc.utobj().printTestStep("Franchisee having Renewals Due are visible to logged in user at Info Mgr Dashboard 'Renewals Due WIdget' for Which Term Expiration Date is entered in Agreement Tab");
			
			
			// Now Adding Data in Renewal tabs to Verify that Term Expiration value in Dashboard  Renewal Due Widget is Overwrite by Renewals Due Widget Term Expiration Date if it is greater. 
			
			imc.InfoMgrFranchisees(driver);
			imc.SearchFranchiseeAndClick(driver, location.getFranchiseID());
			
			// Adding Data for Renewals Due Widgets in Agreement Tab wih Term Expiration Date is Greater than Agreement Tab Term Expiration Date.
			
			fc.utobj().clickElement(driver, imbp.lnkRenewal);
			InfoMgrRenewalPage imrp = new InfoMgrRenewalPage(driver);
			fc.utobj().sendKeys(driver, imrp.txtAsOf, fc.utobj().getCurrentDateUSFormat());
			fc.utobj().sendKeys(driver, imrp.txtNewExpirationDate, fc.utobj().getFutureDateUSFormat(3));
			fc.utobj().clickElement(driver, imrp.btnAdd);	
			imc.InfoMgrDashboard(driver);
			
			
			fc.utobj().printTestStep("IM Dashboard Renewals Due Widget Verififcation Start with Regional User with Renewal Tab Term Expiration Date Greater than Agreement Tab Term Expiration Date");
			
			verifyRenewalsDue(driver, OwnerName,location.getFranchiseID());
			verifyRenewalsDue(driver, fc.utobj().getFutureDateUSFormat(3),location.getFranchiseID());
			fc.utobj().printTestStep("Term Expiration Date of Renewal Tab is greater than Agreement Tab and same is visible at Info Mgr Renewal Due Widget at Dashboard.");
			
			
			
			// Adding Data for Renewals Due Widgets in Agreement Tab wih Term Expiration Date is Less than Agreement Tab Term Expiration Date.
			imc.InfoMgrFranchisees(driver);
			imc.SearchFranchiseeAndClick(driver, location.getFranchiseID());
			
			fc.utobj().clickElement(driver, imbp.lnkRenewal);
			fc.utobj().clickLink(driver, "Modify");
			
			fc.utobj().sendKeys(driver, imrp.txtNewExpirationDate, fc.utobj().getFutureDateUSFormat(1));
			fc.utobj().clickElement(driver, imrp.btnAdd);	
			imc.InfoMgrDashboard(driver);
					
						
			fc.utobj().printTestStep("IM Dashboard Renewals Due Widget Verififcation Start with Regional User with Renewal Tab Term Expiration Date Less than Agreement Tab Term Expiration Date");
						
			verifyRenewalsDue(driver, OwnerName, location.getFranchiseID());
			verifyRenewalsDue(driver, fc.utobj().getFutureDateUSFormat(2),  location.getFranchiseID());
			fc.utobj().printTestStep("Term Expiration Date of Agreement Tab is greater than Renewal Tab Thus Agreement Tab Term Expiration Date is visible at Info Mgr Renewal Due Widget at Dashboard.");
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	
	// Email History Widget
	
	@Test(groups = { "infomgr","IM_Dashboard" })
	@TestCase(createdOn = "2018-07-18", updatedOn = "2018-07-18", testCaseId = "TC_IM_Dashboard_RegionalUser_SendEmailWidgetVerify", testCaseDescription = "To verify the IM Dashboard Email History Widget after Login via Regional User")
	private void dashboardReg13() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Location location = new Location();
		LocationData ld = new LocationData();
		
		InfoMgrBasePage imbp = new InfoMgrBasePage(driver);
		
		try {
			driver = fc.loginpage().login(driver);
			AdminAreaRegionAddAreaRegionPageTest region_page = new AdminAreaRegionAddAreaRegionPageTest();
			String region_name = fc.utobj().generateTestData("R1");
			region_page.addAreaRegion(driver, region_name);
			String Login_id = fc.utobj().generateTestData("R1");
			RUser.addRegionalUser(driver, Login_id, "admin123", region_name, "vimal.sharma@franconnect.com");
			location = fc.commonMethods().getDefaultDataFor_Location(location);
			fc.adminpage().adminAddNewFranchiseLocationLnk(driver);
			location.setAreaRegion(region_name);
			ld.filllocationInfo(driver, location).submit(driver);
			fc.adminpage().manageFranchiseLocationLnk(driver).SearchFranchise(driver, location).VerifyLocationExists(driver, location);
			System.out.println(location.getFranchiseID());
			
			/*String Login_id ="div1a12164718";
			location.setFranchiseID("FIMi12164934");*/
			
			
			
			fc.home_page().logout(driver);
			driver = fc.loginpage().loginWithUserNameAndPassword(driver, Login_id, "admin123");
			imc.InfoMgrFranchisees(driver);
			imc.searchLocationFromShowFilter(driver, location.getFranchiseID());
			// Sending email from Franchise Summary Page and then Verifying the email at Email History Dashboard Widget
			fc.utobj().actionImgOption(driver, location.getFranchiseID(), "Send Email");
			InfoMgrSendEmailPage imsendemail = new InfoMgrSendEmailPage(driver);
			fc.utobj().sendKeys(driver, imsendemail.txtSubject, "Info Mgr Send Email Subject from Franchisee Summary Page by Regional User and then Verification on Dashboard Email Summary Widget");
			//fc.utobj().switchFrame(driver, driver.findElement(By.id("mailMessage_ifr")));
			fc.utobj().switchFrameById(driver, "mailMessage_ifr");
			fc.utobj().sendKeys(driver, driver.findElement(By.xpath(".//*[@id='tinymce']")), "Info Mgr Send Email Message from Franchisee Summary Page by Regional User and then Verification on Dashboard Email Summary Widget");
			//fc.utobj().sendKeys(driver, imsendemail.htmltxtArea, "Info Mgr Send Email Message from Franchisee Summary Page by Regional User and then Verification on Dashboard Email Summary Widget");
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, imsendemail.btnSend);
			imc.InfoMgrDashboard(driver);			
			
			/*verifyEmailHistory(driver,"Info Mgr Send Email Subject from Franchisee Summary Page by Regional User and then Verification on Dashboard Email Summary Widget",location.getFranchiseID());
			fc.utobj().printTestStep("Email Sent through Francisee Summary Page is visible to logged in user at Info Mgr Dashboard 'Email History Widget' ");*/
			fc.utobj().clickLink(driver, "Info Mgr Send Email Subject from Franchisee Summary Page by Regional User and then Verification on Dashboard Email Summary Widget");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			//fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			
			boolean msgpresent =fc.utobj().assertPageSource(driver, "Info Mgr Send Email Message from Franchisee Summary Page by Regional User and then Verification on Dashboard Email Summary Widget");
			
			if(msgpresent==true)
			{
				fc.utobj().printTestStep("Message Content verified on popup of send email after click on Email subject at Info Mgr Dashboard Email History Widget ");
			}
			else
			{
				fc.utobj().printTestStep("Message Content Not Present on popup of send email after click on Email subject at Info Mgr Dashboard Email History Widget ");
			}
			
			//Send Email from Contact History Send Email button
			fc.utobj().clickElement(driver, ".//*[@value='Close']");
			fc.utobj().switchFrameToDefault(driver);
			imc.InfoMgrFranchisees(driver);
			imc.SearchFranchiseeAndClick(driver, location.getFranchiseID());
			
			fc.utobj().clickElement(driver, imbp.lnkContactHistory);
			fc.utobj().clickElement(driver, ".//*[@value='Send Email']");
			fc.utobj().sendKeys(driver, imsendemail.txtSubject, "Info Mgr Send Email Subject from Contact History Page by Regional User and then Verification on Dashboard Email Summary Widget");
			fc.utobj().switchFrame(driver, driver.findElement(By.id("mailMessage_ifr")));
			fc.utobj().sendKeys(driver, driver.findElement(By.xpath(".//*[@id='tinymce']")), "Info Mgr Send Email Message from Contact History Page by Regional User and then Verification on Dashboard Email Summary Widget");
			//fc.utobj().sendKeys(driver, imsendemail.htmltxtArea, "Info Mgr Send Email Message from Franchisee Summary Page by Regional User and then Verification on Dashboard Email Summary Widget");
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, imsendemail.btnSend);
			imc.InfoMgrDashboard(driver);			
			
			/*verifyEmailHistory1(driver,"Info Mgr Send Email Subject from Contact History Page by Divisional User and then Verification on Dashboard Email Summary Widget",location.getFranchiseID());
			fc.utobj().printTestStep("Email Sent through Contact History Page is visible to logged in user at Info Mgr Dashboard 'Email History Widget' ");*/
			fc.utobj().clickLink(driver, "Info Mgr Send Email Subject from Contact History Page by Regional User and then Verification on Dashboard Email Summary Widget");
			fc.utobj().switchFrame(driver, driver.findElement(By.id("cboxIframe")));
			boolean msgpresent1 =fc.utobj().assertPageSource(driver, "Info Mgr Send Email Message from Contact History Page by Regional User and then Verification on Dashboard Email Summary Widget");
			
			if(msgpresent1==true)
			{
				fc.utobj().printTestStep("Message Content verified on popup of send email after click on Email subject at Info Mgr Dahboard Email History Widget ");
			}
			else
			{
				fc.utobj().printTestStep("Message Content Not Present on popup of send email after click on Email subject at Info Mgr Dahboard Email History Widget ");
			}
			
			fc.utobj().clickElement(driver, ".//*[@value='Close']");
			fc.utobj().switchFrameToDefault(driver);
			
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	
	//QA History Widget Verification by Regional User
	@Test(groups = { "infomgr","IM_Dashboard" })
	@TestCase(createdOn = "2018-07-18", updatedOn = "2018-07-18", testCaseId = "TC_IM_Dashboard_RegionalUser_QAHistoryWidget", testCaseDescription = "To verify the IM Dashboard QA History Widget after Login via Regional User.")
	private void dashboardReg14() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Location location = new Location();
		LocationData ld = new LocationData();
		InfoMgrDashboardPage imdp =new InfoMgrDashboardPage(driver);
		
		InfoMgrBasePage imbp = new InfoMgrBasePage(driver);
		
		try {
			driver = fc.loginpage().login(driver);
			AdminAreaRegionAddAreaRegionPageTest region_page = new AdminAreaRegionAddAreaRegionPageTest();
			String region_name = fc.utobj().generateTestData("R1");
			region_page.addAreaRegion(driver, region_name);
			String Login_id = fc.utobj().generateTestData("R1");
			RUser.addRegionalUser(driver, Login_id, "admin123", region_name, "vimal.sharma@franconnect.com");
			location = fc.commonMethods().getDefaultDataFor_Location(location);
			fc.adminpage().adminAddNewFranchiseLocationLnk(driver);
			location.setAreaRegion(region_name);
			ld.filllocationInfo(driver, location).submit(driver);
			fc.adminpage().manageFranchiseLocationLnk(driver).SearchFranchise(driver, location).VerifyLocationExists(driver, location);
			System.out.println(location.getFranchiseID());
			
			/*String Login_id = "r1a17164215";
			location.setFranchiseID("FIMb17164401");*/
			
			admininfomgrcommonmethod.SetQATabIntregrationNo(driver);
			
			fc.home_page().logout(driver);
			driver = fc.loginpage().loginWithUserNameAndPassword(driver, Login_id, "admin123");
			
			imc.InfoMgrFranchisees(driver);
			imc.SearchFranchiseeAndClick(driver,location.getFranchiseID());
			fc.utobj().clickElement(driver, imbp.lnkQAHistory);
			InfoMgrQAHistoryPage imqa =new InfoMgrQAHistoryPage(driver);
			fc.utobj().sendKeys(driver, imqa.dtinspectionDate, fc.utobj().getFutureDateUSFormat(2));
			fc.utobj().sendKeys(driver, imqa.txtinspectionType, "InformalInspection ");
			fc.utobj().sendKeys(driver, imqa.txtfieldConsultant, "Mark");
			fc.utobj().sendKeys(driver, imqa.txtproductQualityScore, "20");
			fc.utobj().sendKeys(driver, imqa.txtserviceQualityScore, "30");
			fc.utobj().sendKeys(driver, imqa.txtfacilityScore, "40");
			fc.utobj().sendKeys(driver, imqa.txtadminScore, "50");
			fc.utobj().sendKeys(driver, imqa.dtcureDate, fc.utobj().getFutureDateUSFormat(3));
			fc.utobj().sendKeys(driver, imqa.dtnextInspectionDate, fc.utobj().getFutureDateUSFormat(4));
			fc.utobj().sendKeys(driver, imqa.txtMTM, "MTM_Value");
			fc.utobj().clickElement(driver, imqa.btnSubmit);
			imc.InfoMgrDashboard(driver);
			String OwnerName= location.getOwnerFirstName()+" "+location.getOwnerLastName();
			verifyQAInspection(driver,OwnerName,location.getFranchiseID());
			verifyQAInspection(driver,fc.utobj().getFutureDateUSFormat(2),location.getFranchiseID());
			fc.utobj().printTestStep("Data Verfied at QA History Dahboard Widget by Divisional User");
			fc.utobj().printTestStep("Now Clicking on More Info Link to verify that it is working and Redirected at QA History Report.");
			fc.utobj().clickElement(driver, imdp.RecentQaHistoryWidgetMoreInfoLink);
			
			List<String>  a = new ArrayList<String>();
			a.add(location.getFranchiseID());
			a.add(OwnerName);
			a.add(fc.utobj().getFutureDateUSFormat(2));
			a.add("Lists QA inspection details for all the Franchisees");
			a.add("QA Report");

			fc.utobj().assertPageSourceWithMultipleRecords(driver, a);
			
			fc.utobj().printTestStep("QA History Dashboard Widget More Info Link successfully redirected at QA History Report and Data also verified");
			
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	
	// Log a Call from Dashboard by Regional User
	@Test(groups = { "infomgr","IM_Dashboard" })
	@TestCase(createdOn = "2018-07-24", updatedOn = "2018-07-24", testCaseId = "TC_IM_Dashboard_RegionalUser_LogaCall", testCaseDescription = "To verify the IM Dashboard Log a Call Link by Regional User.")
	private void dashboardReg15() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Location location = new Location();
		LocationData ld = new LocationData();
		InfoMgrDashboardPage imdp = new InfoMgrDashboardPage(driver);
		InfoMgrBasePage imbp = new InfoMgrBasePage(driver);
		InfoMgrLogACallPage logacall = new InfoMgrLogACallPage(driver);

		try {
			driver = fc.loginpage().login(driver);
			AdminAreaRegionAddAreaRegionPageTest region_page = new AdminAreaRegionAddAreaRegionPageTest();
			String region_name = fc.utobj().generateTestData("R1");
			region_page.addAreaRegion(driver, region_name);
			String Login_id = fc.utobj().generateTestData("R1");
			RUser.addRegionalUser(driver, Login_id, "admin123", region_name, "vimal.sharma@franconnect.com");
			location = fc.commonMethods().getDefaultDataFor_Location(location);
			fc.adminpage().adminAddNewFranchiseLocationLnk(driver);
			location.setAreaRegion(region_name);
			ld.filllocationInfo(driver, location).submit(driver);
			fc.adminpage().manageFranchiseLocationLnk(driver).SearchFranchise(driver, location)
					.VerifyLocationExists(driver, location);
			System.out.println(location.getFranchiseID());
			fc.home_page().logout(driver);
			fc.loginpage().loginWithUserNameAndPassword(driver, Login_id, "admin123");
			imc.InfoMgrDashboard(driver);
			fc.utobj().clickElement(driver, imdp.LogaCallLinkDashboard);

			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().selectDropDownByVisibleText(driver, logacall.drpCategory, "Franchise ID");
			//fc.utobj().selectValFromMultiSelect(driver, logacall.drpFranchisee, location.getFranchiseID());
			fc.utobj().selectValFromMultiSelectwithoutSelectAll(driver, logacall.drpFranchisee, location.getFranchiseID());
			fc.utobj().clickElement(driver, logacall.btnContinue);
			fc.utobj().sendKeys(driver, logacall.txtSubject, "Log a Call by Regional User from Dashboard Link");
			fc.utobj().sendKeys(driver, logacall.txtDate, fc.utobj().getCurrentDateUSFormat());
			fc.utobj().selectDropDownByVisibleText(driver, logacall.drpStatus, "Left Message");
			fc.utobj().selectDropDownByVisibleText(driver, logacall.drpCallType, "Phone");
			/*fc.utobj().sendKeys(driver, logacall.txtComments,
					"Info Mgr Dashboard Log a Call Link Verification by Regional User ");*/
			fc.utobj().clickElement(driver, logacall.btnSubmit);
			fc.utobj().clickElement(driver, logacall.btnNo);
			fc.utobj().switchFrameToDefault(driver);
			imc.InfoMgrFranchisees(driver);
			//imc.SearchFranchiseeAndClick(driver, location.getFranchiseID());
			imc.SearchFranchiseeAndClick(driver, location.getFranchiseID());
			fc.utobj().clickElement(driver, imbp.lnkContactHistory);
			fc.utobj().assertPageSource(driver, "Log a Call by Regional User from Dashboard Link");
			fc.utobj().printTestStep(
					"Logged call from Dashboard from Regional User successfully verified in Franchisee Contact History");

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	// Log a Task from Dashboard by Regional User
	@Test(groups = { "infomgr","IM_Dashboard" })
	@TestCase(createdOn = "2018-07-24", updatedOn = "2018-07-24", testCaseId = "TC_IM_Dashboard_RegionalUser_LogaTask", testCaseDescription = "To verify the IM Dashboard Log a Task Link by Regional User.")
	private void dashboardReg16() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Location location = new Location();
		LocationData ld = new LocationData();
		InfoMgrDashboardPage imdp = new InfoMgrDashboardPage(driver);
		InfoMgrBasePage imbp = new InfoMgrBasePage(driver);
		InfoMgrLogATaskPage logatask = new InfoMgrLogATaskPage(driver);

		try {
			driver = fc.loginpage().login(driver);
			AdminAreaRegionAddAreaRegionPageTest region_page = new AdminAreaRegionAddAreaRegionPageTest();
			String region_name = fc.utobj().generateTestData("R1");
			region_page.addAreaRegion(driver, region_name);
			String Login_id = fc.utobj().generateTestData("R1");
			RUser.addRegionalUser(driver, Login_id, "admin123", region_name, "vimal.sharma@franconnect.com");
			location = fc.commonMethods().getDefaultDataFor_Location(location);
			fc.adminpage().adminAddNewFranchiseLocationLnk(driver);
			location.setAreaRegion(region_name);
			ld.filllocationInfo(driver, location).submit(driver);
			fc.adminpage().manageFranchiseLocationLnk(driver).SearchFranchise(driver, location)
					.VerifyLocationExists(driver, location);
			System.out.println(location.getFranchiseID());
			fc.home_page().logout(driver);
			fc.loginpage().loginWithUserNameAndPassword(driver, Login_id, "admin123");
			imc.InfoMgrDashboard(driver);
			fc.utobj().clickElement(driver, imdp.AddTaskLinkDashboard);

			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().selectDropDownByVisibleText(driver, logatask.drpCategory, "Franchise ID");
			fc.utobj().selectValFromMultiSelect(driver, logatask.drpFranchisee, location.getFranchiseID());
			fc.utobj().clickElement(driver, logatask.btnContinue);
			fc.utobj().selectValFromMultiSelectwithoutSelectAll(driver, logatask.multiCheckBoxCorporateUsers, Login_id);
			fc.utobj().selectDropDownByVisibleText(driver, logatask.drpStatus, "Work In Progress");
			fc.utobj().sendKeys(driver, logatask.txtSubject, "Log a Task by Regional User from Dashboard Link");
			fc.utobj().clickElement(driver, logatask.TimelessTaskId_CheckBox);
			//fc.utobj().sendKeys(driver, logatask.comments, "Info Mgr Dashboard Log a Task Link Verification by Regional User");
			fc.utobj().clickElement(driver, logatask.btnAdd);
			Thread.sleep(5000);
			fc.utobj().switchFrameToDefault(driver);
			
			imc.InfoMgrFranchisees(driver);
			//imc.SearchFranchiseeAndClick(driver, location.getFranchiseID());
			imc.SearchFranchiseeAndClick(driver, location.getFranchiseID());
			fc.utobj().clickElement(driver, imbp.lnkContactHistory);
			fc.utobj().assertPageSource(driver, "Log a Task by Regional User from Dashboard Link");
			fc.utobj().printTestStep(
					"Logged Task from Dashboard from Regional User successfully verified in Franchisee Contact History");

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(groups = { "infomgr","IM_Dashboard" })
	@TestCase(createdOn = "2018-07-27", updatedOn = "2018-07-27", testCaseId = "TC_IM_Dashboard_RegionalUser_SearchLocation", testCaseDescription = "To verify the IM Dashboard Location Search via Search Text Box.")
	private void dashboardReg17() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Location location = new Location();
		LocationData ld = new LocationData();
		InfoMgrDashboardPage imdp = new InfoMgrDashboardPage(driver);

		try {
			driver = fc.loginpage().login(driver);
			AdminAreaRegionAddAreaRegionPageTest region_page = new AdminAreaRegionAddAreaRegionPageTest();
			String region_name = fc.utobj().generateTestData("R1");
			region_page.addAreaRegion(driver, region_name);
			String Login_id = fc.utobj().generateTestData("R1");
			RUser.addRegionalUser(driver, Login_id, "admin123", region_name, "vimal.sharma@franconnect.com");
			location = fc.commonMethods().getDefaultDataFor_Location(location);
			fc.adminpage().adminAddNewFranchiseLocationLnk(driver);
			location.setAreaRegion(region_name);
			ld.filllocationInfo(driver, location).submit(driver);
			fc.adminpage().manageFranchiseLocationLnk(driver).SearchFranchise(driver, location)
					.VerifyLocationExists(driver, location);
			System.out.println(location.getFranchiseID());
			fc.home_page().logout(driver);
			fc.loginpage().loginWithUserNameAndPassword(driver, Login_id, "admin123");
			imc.InfoMgrDashboard(driver);
			fc.utobj().sendKeys(driver, imdp.SearchBoxDashboard, location.getFranchiseID());
			fc.utobj().clickElement(driver, imdp.SearchBoxIconDashboard);
			fc.utobj().assertPageSource(driver, location.getFranchiseID());
			imc.InfoMgrDashboard(driver);
			fc.utobj().sendKeys(driver, imdp.SearchBoxDashboard, location.getFranchiseID());
			fc.utobj().assertPageSource(driver, location.getFranchiseID());
			fc.utobj().clickEnterOnElement(driver, imdp.SearchBoxDashboard);
			
			imc.InfoMgrDashboard(driver);
			imdp = new InfoMgrDashboardPage(driver);
			fc.utobj().sendKeys(driver, imdp.SearchBoxDashboard,location.getFranchiseID());
			fc.utobj().sleep(2000);
			fc.utobj().clickElementByJS(driver, fc.utobj().getElementByXpath(driver, ".//*[.='"+location.getFranchiseID()+"']"));
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	
	@Test(groups = { "infomgr","IM_Dashboard" })
	@TestCase(createdOn = "2018-07-27", updatedOn = "2018-07-27", testCaseId = "TC_IM_Dashboard_RegionalUser_verify_Dashboard_Franchisees_in_Last_5_Yrs_and_Total_Franchisees_Widget_Graph", testCaseDescription  =  "To verify the IM Dashboard Location Search via Search Text Box.")
	private void dashboardReg18() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Location location = new Location();
		LocationData ld = new LocationData();
		InfoMgrDashboardPage imdp = new InfoMgrDashboardPage(driver);

		try {
			driver = fc.loginpage().login(driver);
			AdminAreaRegionAddAreaRegionPageTest region_page = new AdminAreaRegionAddAreaRegionPageTest();
			String region_name = fc.utobj().generateTestData("R1");
			region_page.addAreaRegion(driver, region_name);
			String Login_id = fc.utobj().generateTestData("R1");
			RUser.addRegionalUser(driver, Login_id, "admin123", region_name, "vimal.sharma@franconnect.com");
			location = fc.commonMethods().getDefaultDataFor_Location(location);
			fc.adminpage().adminAddNewFranchiseLocationLnk(driver);
			location.setAreaRegion(region_name);
			ld.filllocationInfo(driver, location).submit(driver);
			fc.adminpage().manageFranchiseLocationLnk(driver).SearchFranchise(driver, location)
					.VerifyLocationExists(driver, location);
			System.out.println(location.getFranchiseID());
			fc.home_page().logout(driver);
			fc.loginpage().loginWithUserNameAndPassword(driver, Login_id, "admin123");
			imc.InfoMgrDashboard(driver);
			if(imdp.GraphPresent.isDisplayed())
			{
				fc.utobj().printTestStep("Franchisees in Last 5 Yrs. and Total Franchisees Graph is visible to Regional user");
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	// Recent 5 Visit Widget More Info Link Click from Dashboard by Regional User
	@Test(groups = { "infomgr","IM_Dashboard" })
	@TestCase(createdOn = "2018-07-30", updatedOn = "2018-07-30", testCaseId = "TC_IM_Dashboard_RegionalUser_Recent5VisitMoreInfoLink", testCaseDescription = "To verify the IM Dashboard Recent5VisitMoreInfoLink by Regional User.")
	private void dashboardReg19() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		InfoMgrDashboardPage imdp = new InfoMgrDashboardPage(driver);

		try {
			driver = fc.loginpage().login(driver);
			

			admininfomgrcommonmethod.SetQATabIntregrationYes(driver);
			
			AdminAreaRegionAddAreaRegionPageTest region_page = new AdminAreaRegionAddAreaRegionPageTest();
			String region_name = fc.utobj().generateTestData("R1");
			region_page.addAreaRegion(driver, region_name);
			String Login_id = fc.utobj().generateTestData("R1");
			RUser.addRegionalUser(driver, Login_id, "admin123", region_name, "vimal.sharma@franconnect.com");

			fc.home_page().logout(driver);
			fc.loginpage().loginWithUserNameAndPassword(driver, Login_id, "admin123");
			imc.InfoMgrDashboard(driver);

			// Now Verifying the More Info Link Redirection from Task View Widget.
			fc.utobj().printTestStep(
					"Now Verifying the More Info Link Redirection from Recent 5 Widget By Corporate User (QA Tab Integraion is Yes/ON).");
			fc.utobj().clickElement(driver, imdp.RecentQaHistoryWidgetMoreInfoLink);

			fc.utobj().assertPageSource(driver, "Visit Summary Report (All)");
			fc.utobj().printTestStep("More Info Link successfully Navigated to Field Ops Visit Summary Report Page");

			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	
	
// Corporate User Cases
	
	// Corp Graph Check for Franchisees in Last 5 Yrs. and Total Franchisees Widget
	@Test(groups = { "infomgr","IM_Dashboard" })
	@TestCase(createdOn = "2018-07-27", updatedOn = "2018-07-27", testCaseId = "TC_IM_Dashboard_CorporateUser_verify_Dashboard_Franchisees_in_Last_5_Yrs._and_Total+_Franchisees_Widget_Graph", testCaseDescription = "To verify the IM Dashboard Location Search via Search Text Box by Corporate User.")
	private void dashboardCorp1() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Location location = new Location();
		LocationData ld = new LocationData();
		InfoMgrDashboardPage imdp = new InfoMgrDashboardPage(driver);

		try {
			driver = fc.loginpage().login(driver);
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail("vimal.sharma@franconnect.com");
			
			AdminUsersManageCorporateUsersAddCorporateUserPageTest addCorporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			String Login_id =fc.utobj().generateTestData("C1");
			corpUser.setUserName(Login_id);
			corpUser.setPassword("admin123");
			corpUser.setConfirmPassword("admin123");
			corpUser.setEmail("vimal.sharma@franconnect.com");
			corpUser = addCorporatePage.createDefaultUser(driver, corpUser);
			
			AdminAreaRegionAddAreaRegionPageTest region_page = new AdminAreaRegionAddAreaRegionPageTest();
			String region_name = fc.utobj().generateTestData("R1");
			region_page.addAreaRegion(driver, region_name);
			location = fc.commonMethods().getDefaultDataFor_Location(location);
			fc.adminpage().adminAddNewFranchiseLocationLnk(driver);
			location.setAreaRegion(region_name);
			ld.filllocationInfo(driver, location).submit(driver);
			fc.adminpage().manageFranchiseLocationLnk(driver).SearchFranchise(driver, location)
					.VerifyLocationExists(driver, location);
			
			fc.home_page().logout(driver);
			fc.loginpage().loginWithUserNameAndPassword(driver, Login_id, "admin123");

			imc.InfoMgrDashboard(driver);
			if (imdp.GraphPresent.isDisplayed()) {
				fc.utobj().printTestStep(
						"Franchisees in Last 5 Yrs. and Total Franchisees Graph is visible to Regional user");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	// Log a Call from Dashboard by Corporate User
	@Test(groups = { "infomgr"})
	@TestCase(createdOn = "2018-07-27", updatedOn = "2018-07-27", testCaseId = "TC_IM_Dashboard_CorporateUser_LogaCall", testCaseDescription = "To verify the IM Dashboard Log a Call Link by Corporate User.")
	private void dashboardCorp2() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Location location = new Location();
		LocationData ld = new LocationData();
		InfoMgrDashboardPage imdp = new InfoMgrDashboardPage(driver);
		InfoMgrBasePage imbp = new InfoMgrBasePage(driver);
		InfoMgrLogACallPage logacall = new InfoMgrLogACallPage(driver);

		try {
			driver = fc.loginpage().login(driver);
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail("vimal.sharma@franconnect.com");
			
			AdminUsersManageCorporateUsersAddCorporateUserPageTest addCorporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			String Login_id =fc.utobj().generateTestData("C1");
			corpUser.setUserName(Login_id);
			corpUser.setPassword("admin123");
			corpUser.setConfirmPassword("admin123");
			corpUser.setEmail("vimal.sharma@franconnect.com");
			corpUser = addCorporatePage.createDefaultUser(driver, corpUser);
			
			AdminAreaRegionAddAreaRegionPageTest region_page = new AdminAreaRegionAddAreaRegionPageTest();
			String region_name = fc.utobj().generateTestData("R1");
			region_page.addAreaRegion(driver, region_name);
			location = fc.commonMethods().getDefaultDataFor_Location(location);
			fc.adminpage().adminAddNewFranchiseLocationLnk(driver);
			location.setAreaRegion(region_name);
			ld.filllocationInfo(driver, location).submit(driver);
			fc.adminpage().manageFranchiseLocationLnk(driver).SearchFranchise(driver, location)
					.VerifyLocationExists(driver, location);
			
			fc.home_page().logout(driver);
			fc.loginpage().loginWithUserNameAndPassword(driver, Login_id, "admin123");
			imc.InfoMgrDashboard(driver);
			fc.utobj().clickElement(driver, imdp.LogaCallLinkDashboard);

			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().selectDropDownByVisibleText(driver, logacall.drpCategory, "Franchise ID");
			// fc.utobj().selectValFromMultiSelect(driver, logacall.drpFranchisee,
			// location.getFranchiseID());
			fc.utobj().selectValFromMultiSelectwithoutSelectAll(driver, logacall.drpFranchisee,
					location.getFranchiseID());
			fc.utobj().clickElement(driver, logacall.btnContinue);
			fc.utobj().sendKeys(driver, logacall.txtSubject, "Log a Call by Corporate User from Dashboard Link");
			fc.utobj().sendKeys(driver, logacall.txtDate, fc.utobj().getCurrentDateUSFormat());
			fc.utobj().selectDropDownByVisibleText(driver, logacall.drpStatus, "Left Message");
			fc.utobj().selectDropDownByVisibleText(driver, logacall.drpCallType, "Phone");
			/*fc.utobj().sendKeys(driver, logacall.txtComments,
					"Info Mgr Dashboard Log a Call Link Verification by Corporate User ");*/
			fc.utobj().clickElement(driver, logacall.btnSubmit);
			fc.utobj().clickElement(driver, logacall.btnNo);
			fc.utobj().switchFrameToDefault(driver);
			imc.InfoMgrFranchisees(driver);
			// imc.SearchFranchiseeAndClick(driver, location.getFranchiseID());
			imc.SearchFranchiseeAndClick(driver, location.getFranchiseID());
			fc.utobj().clickElement(driver, imbp.lnkContactHistory);
			fc.utobj().assertPageSource(driver, "Log a Call by Corporate User from Dashboard Link");
			fc.utobj().printTestStep(
					"Logged call from Dashboard from Corporate User successfully verified in Franchisee Contact History");

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	// Log a Task from Dashboard by Corporate User
	@Test(groups = { "infomgr", "IMFailedTC","IM_Dashboard","IM2" })
	@TestCase(createdOn = "2018-07-27", updatedOn = "2018-08-27", testCaseId = "TC_IM_Dashboard_CorporateUser_LogaTask", testCaseDescription = "To verify the IM Dashboard Log a Task Link by Corporate User.")
	private void dashboardCorp3() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Location location = new Location();
		LocationData ld = new LocationData();
		InfoMgrDashboardPage imdp = new InfoMgrDashboardPage(driver);
		InfoMgrBasePage imbp = new InfoMgrBasePage(driver);
		InfoMgrLogATaskPage logatask = new InfoMgrLogATaskPage(driver);

		try {
			driver = fc.loginpage().login(driver);
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail("vimal.sharma@franconnect.com");
			
			AdminUsersManageCorporateUsersAddCorporateUserPageTest addCorporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			String Login_id =fc.utobj().generateTestData("C1");
			corpUser.setUserName(Login_id);
			String FirstName =fc.utobj().generateTestData("AdamC1");
			corpUser.setFirstName(FirstName);
			String LastName =fc.utobj().generateTestData("Sandler");
			corpUser.setLastName(LastName);
			
			corpUser.setPassword("admin123");
			corpUser.setConfirmPassword("admin123");
			corpUser.setEmail("vimal.sharma@franconnect.com");
			corpUser = addCorporatePage.createDefaultUser(driver, corpUser);
			
			AdminAreaRegionAddAreaRegionPageTest region_page = new AdminAreaRegionAddAreaRegionPageTest();
			String region_name = fc.utobj().generateTestData("R1");
			region_page.addAreaRegion(driver, region_name);
			location = fc.commonMethods().getDefaultDataFor_Location(location);
			fc.adminpage().adminAddNewFranchiseLocationLnk(driver);
			location.setAreaRegion(region_name);
			ld.filllocationInfo(driver, location).submit(driver);
			fc.adminpage().manageFranchiseLocationLnk(driver).SearchFranchise(driver, location)
					.VerifyLocationExists(driver, location);
			
			//location.setFranchiseID("FIMi30114662");
			String OwnerName = location.getOwnerFirstName()+" "+location.getOwnerLastName();
			//String OwnerName = "FIMc30114617 Ownerw30114627";
			imc.InfoMgrDashboard(driver);
			fc.utobj().clickElement(driver, imdp.AddTaskLinkDashboard);

			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().selectDropDownByVisibleText(driver, logatask.drpCategory, "Franchise ID");
			fc.utobj().selectValFromMultiSelectWithoutReset(driver, logatask.drpFranchisee, location.getFranchiseID());
			fc.utobj().clickElement(driver, logatask.btnContinue);
			fc.utobj().	selectValFromMultiSelectWithoutReset(driver, logatask.multiCheckBoxCorporateUsers, FirstName);
			fc.utobj().selectDropDownByVisibleText(driver, logatask.drpStatus, "Work In Progress");
			fc.utobj().sendKeys(driver, logatask.txtSubject, "Log a Task by Corporate User from Dashboard Link By Corporate User");
			fc.utobj().clickElement(driver, logatask.TimelessTaskId_CheckBox);
			/*fc.utobj().sendKeys(driver, logatask.comments,
					"Info Mgr Dashboard Log a Task Link Verification by Corporate User");*/
			fc.utobj().clickElement(driver, logatask.btnAdd);
			fc.utobj().switchFrameToDefault(driver);

			imc.InfoMgrFranchisees(driver);
			imc.SearchFranchiseeAndClick(driver, location.getFranchiseID());
			fc.utobj().clickElement(driver, imbp.lnkContactHistory);
			fc.utobj().assertPageSource(driver, "Log a Task by Corporate User from Dashboard Link By Corporate User");
			fc.utobj().printTestStep(
					"Logged Task from Dashboard from Corporate User successfully verified in Franchisee Contact History");
			
			
			fc.home_page().logout(driver);
			fc.loginpage().loginWithUserNameAndPassword(driver, Login_id, "admin123");
			imc.InfoMgrDashboard(driver);
			verifyTaskViewCorporate(driver, "Log a Task by Corporate User from Dashboard Link By Corporate User");
			verifyTaskViewCorporate(driver, location.getFranchiseID());
			verifyTaskViewCorporate(driver, OwnerName);
			verifyTaskViewCorporate(driver, "Work In Progress");
			//verifyTaskViewCorporate(driver, fc.utobj().getCurrentDateUSFormat());
			
			fc.utobj().printTestStep("Task Assigned to Logged in User is visible on Dashboard Task View Widget.");
			
			//Now Verifying the More Info Link Redirection from Task View Widget.
			fc.utobj().printTestStep("Now Verifying the More Info Link Redirection from Task View Widget.");
			fc.utobj().clickElement(driver, imdp.TaskViewMoreInfoLink);
			
			List<String>  taskdetails = new ArrayList<String>();
			taskdetails.add(location.getFranchiseID());
			taskdetails.add(FirstName);
			//taskdetails.add(fc.utobj().getCurrentDateUSFormat());
			taskdetails.add("Log a Task by Corporate User from Dashboard Link By Corporate User");
			taskdetails.add("Work In Progress");
			taskdetails.add("Associated With");
			
			fc.utobj().assertPageSourceWithMultipleRecords(driver, taskdetails);
			fc.utobj().printTestStep("More Info Link successfully Navigated to Task Page and Added Task details are also verified");
			
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	
	
	// Recent 5 Visit Widget More Info Link Click from Dashboard by Corporate User
		@AfterGroups("infomgr")
		@Test(groups = {"infomgr","IM_Dashboard","IM" })
		@TestCase(createdOn = "2018-07-30", updatedOn = "2018-07-30", testCaseId = "TC_IM_Dashboard_CorporateUser_Recent5VisitMoreInfoLink", testCaseDescription = "To verify the IM Dashboard Recent5VisitMoreInfoLink by Corporate User.")
		private void dashboardCorp4() throws Exception {
			String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
			}.getClass().getEnclosingMethod().getName());

			WebDriver driver = fc.commonMethods().browsers().openBrowser();
			InfoMgrDashboardPage imdp = new InfoMgrDashboardPage(driver);

			try {
				driver = fc.loginpage().login(driver);
				CorporateUser corpUser = new CorporateUser();
				corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
				corpUser.setEmail("vimal.sharma@franconnect.com");
				
				AdminUsersManageCorporateUsersAddCorporateUserPageTest addCorporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
				corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
				String Login_id =fc.utobj().generateTestData("C1");
				corpUser.setUserName(Login_id);
				String FirstName =fc.utobj().generateTestData("AdamC1");
				corpUser.setFirstName(FirstName);
				String LastName =fc.utobj().generateTestData("Sandler");
				corpUser.setLastName(LastName);
				
				corpUser.setPassword("admin123");
				corpUser.setConfirmPassword("admin123");
				corpUser.setEmail("vimal.sharma@franconnect.com");
				corpUser = addCorporatePage.createDefaultUser(driver, corpUser);
				
				admininfomgrcommonmethod.SetQATabIntregrationYes(driver);
				
				fc.home_page().logout(driver);
				fc.loginpage().loginWithUserNameAndPassword(driver, Login_id, "admin123");
				imc.InfoMgrDashboard(driver);
				
				//Now Verifying the More Info Link Redirection from Task View Widget.
				fc.utobj().printTestStep("Now Verifying the More Info Link Redirection from Recent 5 Widget By Corporate User (QA Tab Integraion is Yes/ON).");
				fc.utobj().clickElement(driver, imdp.RecentQaHistoryWidgetMoreInfoLink);
				
				fc.utobj().assertPageSource(driver,"Visit Summary Report (All)");
				fc.utobj().printTestStep("More Info Link successfully Navigated to Field Ops Visit Summary Report Page");
				
				admininfomgrcommonmethod.SetQATabIntregrationNo(driver);
				imc.InfoMgrDashboard(driver);
				
				fc.utobj().printTestStep("Now Verifying the More Info Link Redirection from Recent 5 Widget By Corporate User (QA Tab Integraion is No/ OFF).");
				fc.utobj().clickElement(driver, imdp.RecentQaHistoryWidgetMoreInfoLink);
				
				fc.utobj().assertPageSource(driver,"QA Report");
				fc.utobj().printTestStep("More Info Link successfully Navigated to QA Report Report Page in Info Mgr");
				
				fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

			} catch (Exception e) {

				fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			}
		}
		
	@Test(groups = { "infomgr","IM_Dashboard" })
	@TestCase(createdOn = "2018-07-27", updatedOn = "2018-09-04", testCaseId = "TC_IM_Dashboard_CorpUser_SearchLocation", testCaseDescription = "To verify the Location Search via Dasboard Search Box  by Corporate User.")
	private void dashboardCorp5() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Location location = new Location();
		LocationData ld = new LocationData();
		InfoMgrDashboardPage imdp = new InfoMgrDashboardPage(driver);

		try {
			driver = fc.loginpage().login(driver);
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail("vimal.sharma@franconnect.com");
			
			AdminUsersManageCorporateUsersAddCorporateUserPageTest addCorporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			String Login_id =fc.utobj().generateTestData("C1");
			corpUser.setUserName(Login_id);
			corpUser.setPassword("admin123");
			corpUser.setConfirmPassword("admin123");
			corpUser.setEmail("vimal.sharma@franconnect.com");
			corpUser = addCorporatePage.createDefaultUser(driver, corpUser);
			
			AdminAreaRegionAddAreaRegionPageTest region_page = new AdminAreaRegionAddAreaRegionPageTest();
			String region_name = fc.utobj().generateTestData("R1");
			region_page.addAreaRegion(driver, region_name);
			location = fc.commonMethods().getDefaultDataFor_Location(location);
			fc.adminpage().adminAddNewFranchiseLocationLnk(driver);
			location.setAreaRegion(region_name);
			location.setFranchiseID(fc.utobj().generateTestData("Loc_Search"));
			ld.filllocationInfo(driver, location).submit(driver);
			fc.adminpage().manageFranchiseLocationLnk(driver).SearchFranchise(driver, location)
					.VerifyLocationExists(driver, location);
			
			fc.home_page().logout(driver);
			fc.loginpage().loginWithUserNameAndPassword(driver, Login_id, "admin123");
			imc.InfoMgrDashboard(driver);
			fc.utobj().sendKeys(driver, imdp.SearchBoxDashboard, location.getFranchiseID());
			fc.utobj().clickElement(driver, imdp.SearchBoxIconDashboard);
			fc.utobj().assertPageSource(driver, location.getFranchiseID());
			imc.InfoMgrDashboard(driver);
			fc.utobj().sendKeys(driver, imdp.SearchBoxDashboard, location.getFranchiseID());
			fc.utobj().assertPageSource(driver, location.getFranchiseID());
			fc.utobj().clickEnterOnElement(driver, imdp.SearchBoxDashboard);
			
			imc.InfoMgrDashboard(driver);
			imdp = new InfoMgrDashboardPage(driver);
			fc.utobj().sendKeys(driver, imdp.SearchBoxDashboard,location.getFranchiseID());
			
			fc.utobj().sleep(2000);
			fc.utobj().clickElementByJS(driver, fc.utobj().getElementByXpath(driver, ".//*[.='"+location.getFranchiseID()+"']"));
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	
	@Test(groups = { "infomgr","IM_Dashboard" })
	@TestCase(createdOn = "2018-07-25", updatedOn = "2018-07-25", testCaseId = "TC_IM_Dashboard_Widget_Maximise_Minimise_Hide_Via_CorporateUser", testCaseDescription = "To verify the IM Dashboard Widgets Minimise_Maximise_Hide Options after login via Corporate User")
	private void dashboardCorp6() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Location location = new Location();
		LocationData ld = new LocationData();
		InfoMgrDashboardPage imdp = new InfoMgrDashboardPage(driver);
		
		try {
			driver = fc.loginpage().login(driver);
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail("vimal.sharma@franconnect.com");
			
			AdminUsersManageCorporateUsersAddCorporateUserPageTest addCorporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			String Login_id =fc.utobj().generateTestData("C1");
			corpUser.setUserName(Login_id);
			corpUser.setPassword("admin123");
			corpUser.setConfirmPassword("admin123");
			corpUser.setEmail("vimal.sharma@franconnect.com");
			corpUser = addCorporatePage.createDefaultUser(driver, corpUser);
			
			
			AdminAreaRegionAddAreaRegionPageTest region_page = new AdminAreaRegionAddAreaRegionPageTest();
			String region_name = fc.utobj().generateTestData("R1");
			region_page.addAreaRegion(driver, region_name);
			location = fc.commonMethods().getDefaultDataFor_Location(location);
			fc.adminpage().adminAddNewFranchiseLocationLnk(driver);
			location.setAreaRegion(region_name);
			ld.filllocationInfo(driver, location).submit(driver);
			fc.adminpage().manageFranchiseLocationLnk(driver).SearchFranchise(driver, location)
					.VerifyLocationExists(driver, location);
			
			fc.home_page().logout(driver);
			fc.loginpage().loginWithUserNameAndPassword(driver, Login_id, "admin123");
			
			imc.InfoMgrDashboard(driver);
			
					
			//Info Mgr Franchisee System Summary Report Widget
			
			boolean SearchElementPresent = imdp.FranchiseSystemSummaryReportWidget.isDisplayed();
			if (SearchElementPresent == true) {
				fc.utobj().moveToElementThroughAction(driver, imdp.FranchiseSystemSummaryReportWidgetMinMaxHover);
				fc.utobj().printTestStep("Click on Franchisee System Summary Report Minimise widget option");
				fc.utobj().clickElement(driver, imdp.FranchiseSystemSummaryReportWidgetMinMax);
				boolean SearchElementPresent1 = imdp.FranchiseSystemSummaryReportWidget.isDisplayed();
				if (SearchElementPresent1 == false) {
					fc.utobj().printTestStep("Franchisee System Summary Report Widget is minimized successfully");
					fc.utobj().printTestStep("Now Click on Franchisee System Summary Report Widget Maximize widget option");
					fc.utobj().moveToElementThroughAction(driver, imdp.FranchiseSystemSummaryReportWidgetMinMaxHover);
					fc.utobj().clickElement(driver, imdp.FranchiseSystemSummaryReportWidgetMinMax);

					boolean SearchElementPresent2 = imdp.FranchiseSystemSummaryReportWidget.isDisplayed();
					if (SearchElementPresent2 == true) {
						fc.utobj().printTestStep("Franchisee System Summary Report Widget is Maximized successfully");
					} else {
						fc.utobj().printTestStep("Franchisee System Summary ReportWidget is not Maximized");
					}
				} else {
					fc.utobj().printTestStep("Franchisee System Summary ReportWidget is not minimised");
				}
			} else {
				fc.utobj().moveToElementThroughAction(driver,imdp.FranchiseSystemSummaryReportWidgetMinMaxHover);
				fc.utobj().printTestStep("Click on Maximise widget option");
				fc.utobj().clickElement(driver,imdp.FranchiseSystemSummaryReportWidgetMinMax);
				boolean SearchElementPresent3 = imdp.FranchiseSystemSummaryReportWidget.isDisplayed();
				if (SearchElementPresent3 == true) {
					fc.utobj().printTestStep("Franchisee System Summary Report Widget is Maximised successfully");
					fc.utobj().printTestStep("Now Click on Franchisee System Summary Report Widget Minimise widget option");
					fc.utobj().moveToElementThroughAction(driver, imdp.FranchiseSystemSummaryReportWidgetMinMaxHover);
					fc.utobj().clickElement(driver, imdp.FranchiseSystemSummaryReportWidgetMinMax);
					boolean SearchElementPresent4 = imdp.FranchiseSystemSummaryReportWidget.isDisplayed();
					if (SearchElementPresent4 == false) {
						fc.utobj().printTestStep("Franchisee System Summary Report Widget is minimised successfully");
					} else {
						fc.utobj().printTestStep("Franchisee System Summary Report Widget is not Minimised");
					}
				} else {
					fc.utobj().printTestStep("Franchisee System Summary Report Widget is not maximised");
				}
			}

			fc.utobj().moveToElementThroughAction(driver,imdp.FranchiseSystemSummaryReportWidget);
			fc.utobj().printTestStep("Click on Hide widget option");
			fc.utobj().clickElement(driver,imdp.FranchiseSystemSummaryReportWidgetHideSignX);
			fc.utobj().acceptAlertBox(driver);
			boolean WidgetHide = imdp.FranchiseSystemSummaryReportWidget.isDisplayed();
			if (WidgetHide == false) {
				fc.utobj().printTestStep("Franchisee System Summary Report Widget is Hide Sucessfully");
			} else {
				fc.utobj().printTestStep("Franchisee System Summary Report Widget is not Hide");
			}

			
			//Info Mgr "Franchisees in Last 5 Yrs. and Total Franchisees" Widget
			
			
			
			boolean FranchiseeGraphWidget1 = imdp.GraphPresent.isDisplayed();
			
			if (FranchiseeGraphWidget1 ) {
				fc.utobj().moveToElementThroughAction(driver, imdp.GraphWidgetMinMaxHover);
				fc.utobj().printTestStep("Click on Franchisees in Last 5 Yrs. and Total Franchisees Minimise widget option");
				fc.utobj().clickElement(driver, imdp.GraphWidgetMinMax);
				boolean FranchiseeGraphWidget2 = imdp.GraphPresent.isDisplayed();
				if (!FranchiseeGraphWidget2) {
					fc.utobj().printTestStep("Franchisees in Last 5 Yrs. and Total Franchisees Widget is minimized successfully");
					fc.utobj().printTestStep("Now Click on Franchisees in Last 5 Yrs. and Total Franchisees Widget Maximize widget option");
					fc.utobj().moveToElementThroughAction(driver, imdp.GraphWidgetMinMaxHover);
					fc.utobj().clickElement(driver, imdp.GraphWidgetMinMax);

					boolean FranchiseeGraphWidget3 = imdp.GraphPresent.isDisplayed();
					if (FranchiseeGraphWidget3) {
						fc.utobj().printTestStep("Franchisees in Last 5 Yrs. and Total Franchisees Widget is Maximized successfully");
					} else {
						fc.utobj().printTestStep("Franchisees in Last 5 Yrs. and Total Franchisees Widget is not Maximized");
					}
				} else {
					fc.utobj().printTestStep("Franchisees in Last 5 Yrs. and Total Franchisees Widget is not minimised");
				}
			} else {
				fc.utobj().moveToElementThroughAction(driver,imdp.GraphWidgetMinMaxHover);
				fc.utobj().printTestStep("Click on Maximise widget option");
				fc.utobj().clickElement(driver,imdp.GraphWidgetHideSignX);
				boolean FranchiseeGraphWidget4 = imdp.GraphPresent.isDisplayed();
				if (FranchiseeGraphWidget4) {
					fc.utobj().printTestStep("Franchisees in Last 5 Yrs. and Total Franchisees Widget is Maximised successfully");
					fc.utobj().printTestStep("Now Click on Franchisees in Last 5 Yrs. and Total Franchisees Widget Minimise widget option");
					fc.utobj().moveToElementThroughAction(driver, imdp.GraphWidgetMinMaxHover);
					fc.utobj().clickElement(driver, imdp.GraphWidgetMinMax);
					boolean FranchiseeGraphWidget5 = imdp.GraphPresent.isDisplayed();
					if (!FranchiseeGraphWidget5) {
						fc.utobj().printTestStep("Franchisees in Last 5 Yrs. and Total Franchisees Widget is minimised successfully");
					} else {
						fc.utobj().printTestStep("Franchisees in Last 5 Yrs. and Total Franchisees Widget is not Minimised");
					}
				} else {
					fc.utobj().printTestStep("Franchisees in Last 5 Yrs. and Total Franchisees Widget is not maximised");
				}
			}

			fc.utobj().moveToElementThroughAction(driver,imdp.GraphWidgetMinMaxHover);
			fc.utobj().printTestStep("Click on Hide widget option");
			fc.utobj().clickElement(driver, imdp.GraphWidgetHideSignX);
			fc.utobj().acceptAlertBox(driver);
			boolean WidgetHide1 = imdp.GraphPresent.isDisplayed();
			if (!WidgetHide1) {
				fc.utobj().printTestStep("Franchisees in Last 5 Yrs. and Total Franchisees Widget is Hide Sucessfully");
			} else {
				fc.utobj().printTestStep("Franchisees in Last 5 Yrs. and Total Franchisees Widget is not Hide");
			}

			
			//Info Mgr "Task View" Widget
			
			boolean TaskWidgetPresent1 = imdp.TaskViewMoreInfoLink.isDisplayed();
			if (TaskWidgetPresent1 == true) {
				fc.utobj().moveToElementThroughAction(driver,imdp.TaskViewWidgetMinMaxHover);
				fc.utobj().printTestStep("Click on Task View Minimise widget option");
				fc.utobj().clickElement(driver,imdp.TaskViewWidgetMinMax);
				boolean TaskWidgetPresent2 = imdp.TaskViewMoreInfoLink.isDisplayed();
				if (TaskWidgetPresent2 == false) {
					fc.utobj().printTestStep("Task View Widget is minimized successfully");
					fc.utobj().printTestStep("Now Click on Task View Widget Maximize widget option");
					fc.utobj().moveToElementThroughAction(driver,imdp.TaskViewWidgetMinMaxHover);
					fc.utobj().clickElement(driver, imdp.TaskViewWidgetMinMax);

					boolean TaskWidgetPresent3 = imdp.TaskViewMoreInfoLink.isDisplayed();
					if (TaskWidgetPresent3 == true) {
						fc.utobj().printTestStep("Task View Widget is Maximized successfully");
					} else {
						fc.utobj().printTestStep("Task View Widget is not Maximized");
					}
				} else {
					fc.utobj().printTestStep("Task View Widget is not minimised");
				}
			} else {
				fc.utobj().moveToElementThroughAction(driver,imdp.TaskViewWidgetMinMaxHover);
				fc.utobj().printTestStep("Click on Maximise widget option");
				fc.utobj().clickElement(driver, imdp.TaskViewWidgetMinMax);
				boolean TaskWidgetPresent4 =imdp.TaskViewMoreInfoLink.isDisplayed();
				if (TaskWidgetPresent4 == true) {
					fc.utobj().printTestStep("Task View Widget is Maximised successfully");
					fc.utobj().printTestStep("Now Click on Task View Widget Minimise widget option");
					fc.utobj().moveToElementThroughAction(driver,imdp.TaskViewWidgetMinMaxHover);
					fc.utobj().clickElement(driver,imdp.TaskViewWidgetMinMax);
					boolean TaskWidgetPresent5 = imdp.TaskViewMoreInfoLink.isDisplayed();
					if (TaskWidgetPresent5 == false) {
						fc.utobj().printTestStep("Task View Widget is minimised successfully");
					} else {
						fc.utobj().printTestStep("Task View Widget is not Minimised");
					}
				} else {
					fc.utobj().printTestStep("Task View Widget is not maximised");
				}
			}

			fc.utobj().moveToElementThroughAction(driver,imdp.TaskViewWidgetMinMaxHover);
			fc.utobj().printTestStep("Click on Hide widget option");
			fc.utobj().clickElement(driver,imdp.TaskViewWidgetHideSignX);
			fc.utobj().acceptAlertBox(driver);
			boolean TaskWidgetHide = imdp.TaskViewMoreInfoLink.isDisplayed();
			if (TaskWidgetHide == false) {
				fc.utobj().printTestStep("Task View Widget is Hide Sucessfully");
			} else {
				fc.utobj().printTestStep("Task View Widget is not Hide");
			}
			
			
			
			//Info Mgr "Renewals Due" Widget
			
			boolean RenewalsDueWidgetPresent1 = imdp.RenewalDueWidget.isDisplayed();
			//if (RenewalsDueWidgetPresent1 || driver.findElements(By.xpath(".//*[@id='renewalDues']//*[contains(text(),'Expiration Date')]")).size()>0) {
			if (RenewalsDueWidgetPresent1) {
				fc.utobj().moveToElementThroughAction(driver, imdp.RenewalDueWidgetMinMaxHover);
				fc.utobj().printTestStep("Click on Renewals Due Minimise widget option");
				fc.utobj().clickElement(driver, imdp.RenewalDueWidgetMinMax);
				boolean RenewalsDueWidgetPresent2 = imdp.RenewalDueWidget.isDisplayed();
				//if (!RenewalsDueWidgetPresent2  &&  driver.findElements(By.xpath(".//*[@id='renewalDues']//*[contains(text(),'Expiration Date')]")).size()<1) {
				if (!RenewalsDueWidgetPresent2) {
					fc.utobj().printTestStep("Renewals Due Widget is minimized successfully");
					fc.utobj().printTestStep("Now Click on Renewals Due Widget Maximize widget option");
					fc.utobj().moveToElementThroughAction(driver, imdp.RenewalDueWidgetMinMaxHover);
					fc.utobj().clickElement(driver, imdp.RenewalDueWidgetMinMax);

					boolean RenewalsDueWidgetPresent3 = imdp.RenewalDueWidget.isDisplayed();
					if (RenewalsDueWidgetPresent3) {
						fc.utobj().printTestStep("Renewals Due Widget is Maximized successfully");
					} else {
						fc.utobj().printTestStep("Renewals Due Widget is not Maximized");
					}
				} else {
					fc.utobj().printTestStep("Renewals Due Widget is not minimised");
					
				}
			} else {
				fc.utobj().moveToElementThroughAction(driver,imdp.RenewalDueWidgetMinMaxHover);
				fc.utobj().printTestStep("Click on Maximise widget option");
				fc.utobj().clickElement(driver, imdp.RenewalDueWidgetMinMax);
				boolean RenewalsDueWidgetPresent4 = imdp.RenewalDueWidget.isDisplayed();
				if (RenewalsDueWidgetPresent4) {
					fc.utobj().printTestStep("Renewals Due Widget is Maximised successfully");
					fc.utobj().printTestStep("Now Click on Renewals Due Widget Minimise widget option");
					fc.utobj().moveToElementThroughAction(driver,imdp.RenewalDueWidgetMinMaxHover);
					fc.utobj().clickElement(driver, imdp.RenewalDueWidgetMinMax);
					boolean RenewalsDueWidgetPresent5 = imdp.RenewalDueWidget.isDisplayed();
					if (!RenewalsDueWidgetPresent5 && driver.findElements(By.xpath(".//*[@id='renewalDues']//*[contains(text(),'Expiration Date')]")).size()<1) {
						fc.utobj().printTestStep("Renewals Due Widget is minimised successfully");
					} else {
						fc.utobj().printTestStep("Renewals Due Widget is not Minimised");
					}
				} else {
					fc.utobj().printTestStep("Renewals Due Widget is not maximised");
				}
			}

			fc.utobj().moveToElementThroughAction(driver,imdp.RenewalDueWidgetMinMaxHover);
			fc.utobj().printTestStep("Click on Hide widget option");
			fc.utobj().clickElement(driver, imdp.RenewalDueWidgetHideSignX);
			fc.utobj().acceptAlertBox(driver);
			boolean RenewalsDueWidgetHide = imdp.RenewalDueWidget.isDisplayed();
			if (!RenewalsDueWidgetHide) {
				fc.utobj().printTestStep("Renewals Due Widget is Hide Sucessfully");
			} else {
				fc.utobj().printTestStep("Renewals Due Widget is not Hide");
			}
			
			
			
			//Info Mgr "Email History" Widget
			
			boolean EmailHistoryWidgetPresent1 = imdp.EmailHistoryWidget.isDisplayed();
			if (EmailHistoryWidgetPresent1) {
				fc.utobj().moveToElementThroughAction(driver,imdp.EmailHistoryWidgetMinMaxHover);
				fc.utobj().printTestStep("Click on Email History Minimise widget option");
				fc.utobj().clickElement(driver, imdp.EmailHistoryWidgetMinMax);
				boolean EmailHistoryWidgetPresent2 = imdp.EmailHistoryWidget.isDisplayed();
				if (!EmailHistoryWidgetPresent2 ) {
					fc.utobj().printTestStep("Email History Widget is minimized successfully");
					fc.utobj().printTestStep("Now Click on Email History Widget Maximize widget option");
					fc.utobj().moveToElementThroughAction(driver,imdp.EmailHistoryWidgetMinMaxHover);
					fc.utobj().clickElement(driver, imdp.EmailHistoryWidgetMinMax);

					boolean EmailHistoryWidgetPresent3 = imdp.EmailHistoryWidget.isDisplayed();
					if (EmailHistoryWidgetPresent3) {
						fc.utobj().printTestStep("Email History Widget is Maximized successfully");
					} else {
						fc.utobj().printTestStep("Email History Widget is not Maximized");
					}
				} else {
					fc.utobj().printTestStep("Email History Widget is not minimised");
				}
			} else {
				fc.utobj().moveToElementThroughAction(driver,imdp.EmailHistoryWidgetMinMaxHover);
				fc.utobj().printTestStep("Click on Maximise widget option");
				fc.utobj().clickElement(driver, imdp.EmailHistoryWidgetMinMax);
				boolean EmailHistoryWidgetPresent4 = imdp.EmailHistoryWidget.isDisplayed();
				if (EmailHistoryWidgetPresent4) {
					fc.utobj().printTestStep("Email History Widget is Maximised successfully");
					fc.utobj().printTestStep("Now Click on Email History Widget Minimise widget option");
					fc.utobj().moveToElementThroughAction(driver,imdp.EmailHistoryWidgetMinMaxHover);
					fc.utobj().clickElement(driver, imdp.EmailHistoryWidgetMinMax);
					boolean EmailHistoryWidgetPresent5 = imdp.EmailHistoryWidget.isDisplayed();
					if (!EmailHistoryWidgetPresent5) {
						fc.utobj().printTestStep("Email History Widget is minimised successfully");
					} else {
						fc.utobj().printTestStep("Email History Widget is not Minimised");
					}
				} else {
					fc.utobj().printTestStep("Email History Widget is not maximised");
				}
			}

			fc.utobj().moveToElementThroughAction(driver, imdp.EmailHistoryWidgetMinMaxHover);
			fc.utobj().printTestStep("Click on Hide widget option");
			fc.utobj().clickElement(driver,imdp.EmailHistoryWidgetHideSignX);
			fc.utobj().acceptAlertBox(driver);
			boolean EmailHistoryWidgetHide = imdp.EmailHistoryWidget.isDisplayed();
			if (!EmailHistoryWidgetHide) {
				fc.utobj().printTestStep("Email History Widget is Hide Sucessfully");
			} else {
				fc.utobj().printTestStep("Email History Widget is not Hide");
			}
			
			
			//Info Mgr "Recent 5 Visits / Recent 5 QA History Inspections" Widget
			
			boolean RecentVisitOrQAInspectionWidgetPresent1 = imdp.RecentQaHistoryWidgetMoreInfoLink.isDisplayed();
			if (RecentVisitOrQAInspectionWidgetPresent1 == true) {
				fc.utobj().moveToElementThroughAction(driver, imdp.RecentQaHistoryWidgetMinMaxHover);
				fc.utobj().printTestStep("Click on Recent 5 Visits / Recent 5 QA History Inspections Minimise widget option");
				fc.utobj().clickElement(driver, imdp.RecentQaHistoryWidgetMinMax);
				boolean RecentVisitOrQAInspectionWidgetPresent2 = imdp.RecentQaHistoryWidgetMoreInfoLink.isDisplayed();
				if (RecentVisitOrQAInspectionWidgetPresent2 == false) {
					fc.utobj().printTestStep("Recent 5 Visits / Recent 5 QA History Inspections Widget is minimized successfully");
					fc.utobj().printTestStep("Now Click on Recent 5 Visits / Recent 5 QA History Inspections Widget Maximize widget option");
					fc.utobj().moveToElementThroughAction(driver,imdp.RecentQaHistoryWidgetMinMaxHover);
					fc.utobj().clickElement(driver,imdp.RecentQaHistoryWidgetMinMax);

					boolean RecentVisitOrQAInspectionWidgetPresent3 = imdp.RecentQaHistoryWidgetMoreInfoLink.isDisplayed();
					if (RecentVisitOrQAInspectionWidgetPresent3 == true) {
						fc.utobj().printTestStep("Recent 5 Visits / Recent 5 QA History Inspections Widget is Maximized successfully");
					} else {
						fc.utobj().printTestStep("Recent 5 Visits / Recent 5 QA History Inspections Widget is not Maximized");
					}
				} else {
					fc.utobj().printTestStep("Recent 5 Visits / Recent 5 QA History Inspections Widget is not minimised");
				}
			} else {
				fc.utobj().moveToElementThroughAction(driver,imdp.RecentQaHistoryWidgetMinMaxHover);
				fc.utobj().printTestStep("Click on Maximise widget option");
				fc.utobj().clickElement(driver, imdp.RecentQaHistoryWidgetMinMax);
				boolean RecentVisitOrQAInspectionWidgetPresent4 = imdp.RecentQaHistoryWidgetMoreInfoLink.isDisplayed();
				if (RecentVisitOrQAInspectionWidgetPresent4 == true) {
					fc.utobj().printTestStep("Recent 5 Visits / Recent 5 QA History Inspections Widget is Maximised successfully");
					fc.utobj().printTestStep("Now Click on Recent 5 Visits / Recent 5 QA History Inspections Widget Minimise widget option");
					fc.utobj().moveToElementThroughAction(driver,imdp.RecentQaHistoryWidgetMinMaxHover);
					fc.utobj().clickElement(driver,imdp.RecentQaHistoryWidgetMinMax);
					boolean RecentVisitOrQAInspectionWidgetPresent5 = imdp.RecentQaHistoryWidgetMoreInfoLink.isDisplayed();
					if (RecentVisitOrQAInspectionWidgetPresent5 == false) {
						fc.utobj().printTestStep("Recent 5 Visits / Recent 5 QA History Inspections Widget is minimised successfully");
					} else {
						fc.utobj().printTestStep("Recent 5 Visits / Recent 5 QA History Inspections Widget is not Minimised");
					}
				} else {
					fc.utobj().printTestStep("Recent 5 Visits / Recent 5 QA History Inspections Widget is not maximised");
				}
			}

			fc.utobj().moveToElementThroughAction(driver,imdp.RecentQaHistoryWidgetMinMaxHover);
			fc.utobj().printTestStep("Click on Hide widget option");
			fc.utobj().clickElement(driver, imdp.RecentQaHistoryWidgetHideSignX);
			fc.utobj().acceptAlertBox(driver);
			boolean RecentVisitOrQAInspectionWidgetHide = imdp.RecentQaHistoryWidgetMoreInfoLink.isDisplayed();
			if (RecentVisitOrQAInspectionWidgetHide == false) {
				fc.utobj().printTestStep("Recent 5 Visits / Recent 5 QA History Inspections Widget is Hide Sucessfully");
			} else {
				fc.utobj().printTestStep("Recent 5 Visits / Recent 5 QA History Inspections Widget is not Hide");
			}
			
						

			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	@Test(groups = { "infomgr","IM_Dashboard" })
	@TestCase(createdOn = "2018-07-25", updatedOn = "2018-07-25", testCaseId = "TC_IM_Dashboard_Widget_ManageWidgets_Via_CorporateUser", testCaseDescription = "To verify the IM Dashboard Manage Widgets Options after login via Corporate User. It will firs remove all Widget from Dashboard and then again add on Dashboard.")
	private void dashboardCorp7() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		InfoMgrDashboardPage imdp = new InfoMgrDashboardPage(driver);
		try {
			driver = fc.loginpage().login(driver);
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail("vimal.sharma@franconnect.com");
			
			
			AdminUsersManageCorporateUsersAddCorporateUserPageTest addCorporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			String Login_id =fc.utobj().generateTestData("C1");
			corpUser.setUserName(Login_id);
			corpUser.setPassword("admin123");
			corpUser.setConfirmPassword("admin123");
			corpUser.setEmail("vimal.sharma@franconnect.com");
			corpUser = addCorporatePage.createDefaultUser(driver, corpUser);
			
			fc.home_page().logout(driver);
			fc.loginpage().loginWithUserNameAndPassword(driver, Login_id, "admin123");
			
			
			// Navigate to Manage Widget Option and Remove Hide all Widgets from Info Mgr Dashaboard 
			
			imc.InfoMgrDashboard(driver);
			fc.utobj().printTestStep("Click on Info Mgr Manage Widget Option at Dashboard to hide all widgets");
			fc.utobj().clickElement(driver, imdp.manageWidget);
			fc.utobj().clickElement(driver, imdp.mngEmailHistoryWidget);
			fc.utobj().clickElement(driver, imdp.mngSummaryReportWidget);
			fc.utobj().clickElement(driver, imdp.mngTotalFranchiseeWidget);
			fc.utobj().clickElement(driver, imdp.mngRecentVisitWidget);
			fc.utobj().clickElement(driver, imdp.mngRenewalDueWidget);
			fc.utobj().clickElement(driver, imdp.mngTaskViewWidget);
			fc.utobj().clickElement(driver, imdp.mngWidgetSaveButton);
			
			fc.utobj().printTestStep("Info Mgr Dashboard all widgets are hide through Manage Widget Option and now verifying that all widgets are hide from dashboard");
			
			//Verify Franchise System Summary Report Widget is not displaying after hide through Manage Widget Option
			
			boolean FranchiseeSystemSummaryReportWidgetPresent = imdp.SummaryReportWidgetVisible.isDisplayed();
			if(FranchiseeSystemSummaryReportWidgetPresent==false)
			{
				fc.utobj().printTestStep("Franchise System Summary Report Widget is Hide Successfully through manage Widget Option ");
			}
			else
			{
				fc.utobj().printTestStep("Franchise System Summary Report Widget is not getting Hide through manage Widget Option ");
			}
			
			//Verify Franchisees in Last 5 Yrs. and Total Franchisees Widget is not displaying after hide through Manage Widget Option
			
			boolean TotalFranchiseeGraphWidgetPresent = imdp.TotalFranchiseeWidgetVisible.isDisplayed();
			if(TotalFranchiseeGraphWidgetPresent==false)
			{
				fc.utobj().printTestStep("Franchisees in Last 5 Yrs. and Total Franchisees Widget is Hide Successfully through manage Widget Option ");
			}
			else
			{
				fc.utobj().printTestStep("Franchisees in Last 5 Yrs. and Total Franchisees Widget is not getting Hide through manage Widget Option ");
			}
				
			//Verify Task View Widget is not displaying after hide through Manage Widget Option
			
			boolean TaskViewWidgetPresent = imdp.TaskViewWidgetVisible.isDisplayed();
			if(TaskViewWidgetPresent==false)
			{
				fc.utobj().printTestStep("Task View Widget is Hide Successfully through manage Widget Option ");
			}
			else
			{
				fc.utobj().printTestStep("Task View Widget is not getting Hide through manage Widget Option ");
			}
			
			//Verify Renewals Due Widget is not displaying after hide through Manage Widget Option
			
			boolean RenewalsDueWidgetPresent = imdp.RenewalDueWidgetVisible.isDisplayed();
			if(RenewalsDueWidgetPresent==false)
			{
				fc.utobj().printTestStep("Renewals Due Widget is Hide Successfully through manage Widget Option ");
			}
			else
			{
				fc.utobj().printTestStep("Renewals Due Widget is not getting Hide through manage Widget Option ");
			}		
			
			//Verify Email History Widget is not displaying after hide through Manage Widget Option
			
			boolean EmailHistoryWidgetPresent = imdp.EmailHistoryReportVisible.isDisplayed();
			if(EmailHistoryWidgetPresent==false)
			{
				fc.utobj().printTestStep("Email History Widget is Hide Successfully through manage Widget Option ");
			}
			else
			{
				fc.utobj().printTestStep("Email History Widget is not getting Hide through manage Widget Option ");
			}		
			
			//Verify Recent 5 Widget / QA Inspections Widget is not displaying after hide through Manage Widget Option
			
			boolean QAHistoryWidgetPresent = imdp.RecentVisitWidgetVisible.isDisplayed();
			if(QAHistoryWidgetPresent==false)
			{
				fc.utobj().printTestStep("Recent 5 Widget / QA Inspections Widget is Hide Successfully through manage Widget Option ");
			}
			else
			{
				fc.utobj().printTestStep("Recent 5 Widget / QA Inspections Widget is not getting Hide through manage Widget Option ");
			}
			
			//Click on Info Mgr Manage Widget Option at Dashboard to Unhide all Widgets
			
			fc.utobj().printTestStep("Click on Info Mgr Manage Widget Option at Dashboard to Unhide all Widgets");
			fc.utobj().clickElement(driver, imdp.manageWidget);
			fc.utobj().clickElement(driver, imdp.mngEmailHistoryWidget);
			fc.utobj().clickElement(driver, imdp.mngSummaryReportWidget);
			fc.utobj().clickElement(driver, imdp.mngTotalFranchiseeWidget);
			fc.utobj().clickElement(driver, imdp.mngRecentVisitWidget);
			fc.utobj().clickElement(driver, imdp.mngRenewalDueWidget);
			fc.utobj().clickElement(driver, imdp.mngTaskViewWidget);
			fc.utobj().clickElement(driver, imdp.mngWidgetSaveButton);
			
			fc.utobj().printTestStep("Info Mgr Dashboard all widgets are unhide through Manage Widget Option and now verifying that all widgets are visible on dashboard");
			
			//Verify Franchise System Summary Report Widget is displaying after unhide through Manage Widget Option
			
			boolean FranchiseeSystemSummaryReportWidgetPresent1 = imdp.SummaryReportWidgetVisible.isDisplayed();
			if(FranchiseeSystemSummaryReportWidgetPresent1==true)
			{
				fc.utobj().printTestStep("Franchise System Summary Report Widget is unhide Successfully through manage Widget Option ");
			}
			else
			{
				fc.utobj().printTestStep("Franchise System Summary Report Widget is not getting unhide through manage Widget Option ");
			}
			
			//Verify Franchisees in Last 5 Yrs. and Total Franchisees Widget is displaying after unhide through Manage Widget Option
			
			boolean TotalFranchiseeGraphWidgetPresent1 = imdp.TotalFranchiseeWidgetVisible.isDisplayed();
			if(TotalFranchiseeGraphWidgetPresent1==true)
			{
				fc.utobj().printTestStep("Franchisees in Last 5 Yrs. and Total Franchisees Widget is unhide Successfully through manage Widget Option ");
			}
			else
			{
				fc.utobj().printTestStep("Franchisees in Last 5 Yrs. and Total Franchisees Widget is not getting unhide through manage Widget Option ");
			}
				
			//Verify Task View Widget is displaying after unhide through Manage Widget Option
			
			boolean TaskViewWidgetPresent1 = imdp.TaskViewWidgetVisible.isDisplayed();
			if(TaskViewWidgetPresent1==true)
			{
				fc.utobj().printTestStep("Task View Widget is unhide Successfully through manage Widget Option ");
			}
			else
			{
				fc.utobj().printTestStep("Task View Widget is not getting unhide through manage Widget Option ");
			}
			
			//Verify Renewals Due Widget is displaying after unhide through Manage Widget Option
			
			boolean RenewalsDueWidgetPresent1 = imdp.RenewalDueWidgetVisible.isDisplayed();
			if(RenewalsDueWidgetPresent1==true)
			{
				fc.utobj().printTestStep("Renewals Due Widget is unhide Successfully through manage Widget Option ");
			}
			else
			{
				fc.utobj().printTestStep("Renewals Due Widget is not getting unhide through manage Widget Option ");
			}		
			
			//Verify Email History Widget is displaying after unhide through Manage Widget Option
			
			boolean EmailHistoryWidgetPresent1 = imdp.EmailHistoryReportVisible.isDisplayed();
			if(EmailHistoryWidgetPresent1==true)
			{
				fc.utobj().printTestStep("Email History Widget is Unhide Successfully through manage Widget Option ");
			}
			else
			{
				fc.utobj().printTestStep("Email History Widget is not getting unhide through manage Widget Option ");
			}		
			
			//Verify Recent 5 Widget / QA Inspections Widget is not displaying after unhide through Manage Widget Option
			
			boolean QAHistoryWidgetPresent1 = imdp.RecentVisitWidgetVisible.isDisplayed();
			if(QAHistoryWidgetPresent1==true)
			{
				fc.utobj().printTestStep("Recent 5 Widget / QA Inspections Widget is unhide Successfully through manage Widget Option ");
			}
			else
			{
				fc.utobj().printTestStep("Recent 5 Widget / QA Inspections Widget is not getting unhide through manage Widget Option ");
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			
			
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(groups = { "infomgr","IM_Dashboard" })
	@TestCase(createdOn = "2018-07-25", updatedOn = "2018-07-25", testCaseId = "TC_IM_Dashboard_ManageWidget_RestoreDefaultSettings_Via_CorporateUser", testCaseDescription = "To verify the IM Dashboard ManageWidget_RestoreDefaultSettings of Widget via Corporate User ")
	private void dashboardCorp8() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		InfoMgrDashboardPage imdp = new InfoMgrDashboardPage(driver);
		try {
			driver = fc.loginpage().login(driver);
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail("vimal.sharma@franconnect.com");
			
			AdminUsersManageCorporateUsersAddCorporateUserPageTest addCorporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			String Login_id =fc.utobj().generateTestData("C1");
			corpUser.setUserName(Login_id);
			corpUser.setPassword("admin123");
			corpUser.setConfirmPassword("admin123");
			corpUser.setEmail("vimal.sharma@franconnect.com");
			corpUser = addCorporatePage.createDefaultUser(driver, corpUser);
			
			fc.home_page().logout(driver);
			fc.loginpage().loginWithUserNameAndPassword(driver, Login_id, "admin123");
			
			// Navigate to Manage Widget Option and Remove Hide all Widgets from Info Mgr Dashaboard 
			
			imc.InfoMgrDashboard(driver);
			fc.utobj().printTestStep("Click on Info Mgr Manage Widget Option at Dashboard to hide all widgets");
			fc.utobj().clickElement(driver, imdp.manageWidget);
			fc.utobj().clickElement(driver, imdp.mngEmailHistoryWidget);
			fc.utobj().clickElement(driver, imdp.mngSummaryReportWidget);
			fc.utobj().clickElement(driver, imdp.mngTotalFranchiseeWidget);
			fc.utobj().clickElement(driver, imdp.mngRecentVisitWidget);
			fc.utobj().clickElement(driver, imdp.mngRenewalDueWidget);
			fc.utobj().clickElement(driver, imdp.mngTaskViewWidget);
			fc.utobj().clickElement(driver, imdp.mngWidgetSaveButton);
			
			fc.utobj().printTestStep("Info Mgr Dashboard all widgets are hide through Manage Widget Option and now verifying that all widgets are hide from dashboard");
			
			//Verify Franchise System Summary Report Widget is not displaying after hide through Manage Widget Option
			
			boolean FranchiseeSystemSummaryReportWidgetPresent = imdp.SummaryReportWidgetVisible.isDisplayed();
			if(FranchiseeSystemSummaryReportWidgetPresent==false)
			{
				fc.utobj().printTestStep("Franchise System Summary Report Widget is Hide Successfully through manage Widget Option ");
			}
			else
			{
				fc.utobj().printTestStep("Franchise System Summary Report Widget is not getting Hide through manage Widget Option ");
			}
			
			//Verify Franchisees in Last 5 Yrs. and Total Franchisees Widget is not displaying after hide through Manage Widget Option
			
			boolean TotalFranchiseeGraphWidgetPresent = imdp.TotalFranchiseeWidgetVisible.isDisplayed();
			if(TotalFranchiseeGraphWidgetPresent==false)
			{
				fc.utobj().printTestStep("Franchisees in Last 5 Yrs. and Total Franchisees Widget is Hide Successfully through manage Widget Option ");
			}
			else
			{
				fc.utobj().printTestStep("Franchisees in Last 5 Yrs. and Total Franchisees Widget is not getting Hide through manage Widget Option ");
			}
				
			//Verify Task View Widget is not displaying after hide through Manage Widget Option
			
			boolean TaskViewWidgetPresent = imdp.TaskViewWidgetVisible.isDisplayed();
			if(TaskViewWidgetPresent==false)
			{
				fc.utobj().printTestStep("Task View Widget is Hide Successfully through manage Widget Option ");
			}
			else
			{
				fc.utobj().printTestStep("Task View Widget is not getting Hide through manage Widget Option ");
			}
			
			//Verify Renewals Due Widget is not displaying after hide through Manage Widget Option
			
			boolean RenewalsDueWidgetPresent = imdp.RenewalDueWidgetVisible.isDisplayed();
			if(RenewalsDueWidgetPresent==false)
			{
				fc.utobj().printTestStep("Renewals Due Widget is Hide Successfully through manage Widget Option ");
			}
			else
			{
				fc.utobj().printTestStep("Renewals Due Widget is not getting Hide through manage Widget Option ");
			}		
			
			//Verify Email History Widget is not displaying after hide through Manage Widget Option
			
			boolean EmailHistoryWidgetPresent = imdp.EmailHistoryReportVisible.isDisplayed();
			if(EmailHistoryWidgetPresent==false)
			{
				fc.utobj().printTestStep("Email History Widget is Hide Successfully through manage Widget Option ");
			}
			else
			{
				fc.utobj().printTestStep("Email History Widget is not getting Hide through manage Widget Option ");
			}		
			
			//Verify Recent 5 Widget / QA Inspections Widget is not displaying after hide through Manage Widget Option
			
			boolean QAHistoryWidgetPresent = imdp.RecentVisitWidgetVisible.isDisplayed();
			if(QAHistoryWidgetPresent==false)
			{
				fc.utobj().printTestStep("Recent 5 Widget / QA Inspections Widget is Hide Successfully through manage Widget Option ");
			}
			else
			{
				fc.utobj().printTestStep("Recent 5 Widget / QA Inspections Widget is not getting Hide through manage Widget Option ");
			}
			
			//Click on Info Mgr Manage Widget Option at Dashboard to Unhide all Widgets
			
			fc.utobj().printTestStep("Click on Info Mgr Manage Widget Option at Dashboard to Restore all Widgets");
			fc.utobj().clickElement(driver, imdp.manageWidget);
			fc.utobj().clickElement(driver, "//a[@class='bText12lnkNew']");
			
			Thread.sleep(5000);
			fc.utobj().printTestStep("Info Mgr Dashboard all widgets are unhide through Manage Widgets Restore All Widgets link and now verifying that all widgets are visible on dashboard");
			
			//Verify Franchise System Summary Report Widget is displaying after unhide through Manage Widget Option
			
			boolean FranchiseeSystemSummaryReportWidgetPresent1 = imdp.SummaryReportWidgetVisible.isDisplayed();
			if(FranchiseeSystemSummaryReportWidgetPresent1==true)
			{
				fc.utobj().printTestStep("Franchise System Summary Report Widget is unhide Successfully through manage Widget Option ");
			}
			else
			{
				fc.utobj().printTestStep("Franchise System Summary Report Widget is not getting unhide through manage Widget Option ");
			}
			
			//Verify Franchisees in Last 5 Yrs. and Total Franchisees Widget is displaying after unhide through Manage Widget Option
			
			boolean TotalFranchiseeGraphWidgetPresent1 = imdp.TotalFranchiseeWidgetVisible.isDisplayed();
			if(TotalFranchiseeGraphWidgetPresent1==true)
			{
				fc.utobj().printTestStep("Franchisees in Last 5 Yrs. and Total Franchisees Widget is unhide Successfully through manage Widget Option ");
			}
			else
			{
				fc.utobj().printTestStep("Franchisees in Last 5 Yrs. and Total Franchisees Widget is not getting unhide through manage Widget Option ");
			}
				
			//Verify Task View Widget is displaying after unhide through Manage Widget Option
			
			boolean TaskViewWidgetPresent1 = imdp.TaskViewWidgetVisible.isDisplayed();
			if(TaskViewWidgetPresent1==true)
			{
				fc.utobj().printTestStep("Task View Widget is unhide Successfully through manage Widget Option ");
			}
			else
			{
				fc.utobj().printTestStep("Task View Widget is not getting unhide through manage Widget Option ");
			}
			
			//Verify Renewals Due Widget is displaying after unhide through Manage Widget Option
			
			boolean RenewalsDueWidgetPresent1 = imdp.RenewalDueWidgetVisible.isDisplayed();
			if(RenewalsDueWidgetPresent1==true)
			{
				fc.utobj().printTestStep("Renewals Due Widget is unhide Successfully through manage Widget Option ");
			}
			else
			{
				fc.utobj().printTestStep("Renewals Due Widget is not getting unhide through manage Widget Option ");
			}		
			
			//Verify Email History Widget is displaying after unhide through Manage Widget Option
			
			boolean EmailHistoryWidgetPresent1 = imdp.EmailHistoryReportVisible.isDisplayed();
			if(EmailHistoryWidgetPresent1==true)
			{
				fc.utobj().printTestStep("Email History Widget is Unhide Successfully through manage Widget Option ");
			}
			else
			{
				fc.utobj().printTestStep("Email History Widget is not getting unhide through manage Widget Option ");
			}		
			
			//Verify Recent 5 Widget / QA Inspections Widget is not displaying after unhide through Manage Widget Option
			
			boolean QAHistoryWidgetPresent1 = imdp.RecentVisitWidgetVisible.isDisplayed();
			if(QAHistoryWidgetPresent1==true)
			{
				fc.utobj().printTestStep("Recent 5 Widget / QA Inspections Widget is unhide Successfully through manage Widget Option ");
			}
			else
			{
				fc.utobj().printTestStep("Recent 5 Widget / QA Inspections Widget is not getting unhide through manage Widget Option ");
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			
			
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	
	//Task Widget Value Check by Corporate User
		private void verifyTaskViewCorporate(WebDriver driver,String value) throws Exception
		{
			List<WebElement> tds = driver.findElements(By.xpath(".//*[@id='taskView']//*[contains(text(),'Log a Task by Corporate User from Dashboard Link By Corporate User')]/ancestor::tr[1]/td"));
			
			boolean isMatchFound = false;
			for(WebElement td : tds)
			{
				if(value.equalsIgnoreCase(td.getText()))
				{
					isMatchFound = true;
					fc.utobj().printTestStep("Task details Matched!!!");
					
				}
			}
			
			if(isMatchFound==false)
			{
				fc.utobj().throwsException("Task Details Not Matched!!!");
			}
		}
	
	//Task Widget Value Check by Divisional User
	private void verifyTaskViewDivisional(WebDriver driver,String value) throws Exception
	{
		List<WebElement> tds = driver.findElements(By.xpath(".//*[@id='taskView']//*[contains(text(),'Info Mgr Task Subject for Dashboard Widget Verififcation by Divisional User')]/ancestor::tr[1]/td"));
		
		boolean isMatchFound = false;
		for(WebElement td : tds)
		{
			if(value.equalsIgnoreCase(td.getText()))
			{
				isMatchFound = true;
				fc.utobj().printTestStep("Task details Matched!!!");
				
			}
		}
		
		if(isMatchFound==false)
		{
			fc.utobj().throwsException("Task Details Not Matched!!!");
		}
	}
	
	//Task Widget Value Check by Regional User
	private void verifyTaskViewRegional(WebDriver driver,String value) throws Exception
	{
		List<WebElement> tds = driver.findElements(By.xpath(".//*[@id='taskView']//*[contains(text(),'Info Mgr Task Subject for Dashboard Widget Verififcation by Regional User')]/ancestor::tr[1]/td"));
		
		boolean isMatchFound = false;
		for(WebElement td : tds)
		{
			if(value.equalsIgnoreCase(td.getText()))
			{
				isMatchFound = true;
				fc.utobj().printTestStep("Task details Matched!!!");
				
			}
		}
		
		if(isMatchFound==false)
		{
			fc.utobj().throwsException("Task Details Not Matched!!!");
		}
	}
	
	//Renewal Due Widget Value Check
	private void verifyRenewalsDue(WebDriver driver,String value,String FranchiseeID) throws Exception
	{
		List<WebElement> tds = driver.findElements(By.xpath(".//*[@id='renewalDues']//*[contains(text(),'"+FranchiseeID+"')]/ancestor::tr[1]/td"));
		
		boolean isMatchFound = false;
		for(WebElement td : tds)
		{
			if(value.equalsIgnoreCase(td.getText()))
			{
				isMatchFound = true;
				fc.utobj().printTestStep("Renewals Due Franchisee Data Matched!!!");
				
			}
		}
		
		if(isMatchFound==false)
		{
			fc.utobj().throwsException("Renewals Due Franchisee Data Not Matched!!!");
		}
	}
	
	//Email History Widget Value Check
	private void verifyQAInspection(WebDriver driver,String value,String FranchiseeID) throws Exception
	{
		List<WebElement> tds = driver.findElements(By.xpath(".//*[@id='recentVisit']//*[contains(text(),'"+FranchiseeID+"')]/ancestor::tr[1]/td"));
		
		boolean isMatchFound = false;
		for(WebElement td : tds)
		{
			if(value.equalsIgnoreCase(td.getText()))
			{
				isMatchFound = true;
				fc.utobj().printTestStep("QA History Widget Data Matched!!!");
				
			}
		}
		
		if(isMatchFound==false)
		{
			fc.utobj().throwsException("QA History Widget Data Not Matched!!!");
		}
	}
	
	
}
