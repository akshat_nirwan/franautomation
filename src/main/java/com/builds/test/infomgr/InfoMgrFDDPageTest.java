package com.builds.test.infomgr;

import java.util.HashSet;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.builds.uimaps.infomgr.InfoMgrCenterInfoPage;
import com.builds.uimaps.infomgr.InfoMgrFDDFDDSummaryPage;
import com.builds.uimaps.infomgr.InfoMgrFDDSearchFranchiseePage;
import com.builds.uimaps.infomgr.InfoMgrFDDSearchFranchiseesResultPage;
import com.builds.uimaps.infomgr.InfoMgrFDDSendFDDPage;
import com.builds.uimaps.infomgr.InfoMgrFranchiseesPage;
import com.builds.uimaps.infomgr.InfoMgrMultiUnitEntityPage;
import com.builds.uimaps.infomgr.InfoMgrMultiUnitInfoPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class InfoMgrFDDPageTest {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "infomgr", "IMFailedTC" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-09", testCaseId = "TC_InfoMgr_FDD_SendFDD", testCaseDescription = "This test will send FDD and verify that it is working as expected", reference = {
			"" })
	public void SendFDD() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		FranchiseesCommonMethods franchise = fc.infomgr().franchisees();
		AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Adding division ");

			String divisionName = adminInfoMgr.addDivisionName(driver);

			// Upload FDD
			fc.utobj().printTestStep("Uploading FDD with division ");
			String filePath = FranconnectUtil.config.get("inputDirectory") + "\\TestData\\pdf.pdf";
			adminInfoMgr.uploadFDDWithDivision(driver, filePath, divisionName);

			// FDD Email Template
			fc.utobj().printTestStep("Creating FDD Template");
			String fddTemplate = adminInfoMgr.createFDDTemplate(driver);

			// Add Franchisee Location
			fc.utobj().printTestStep("Add Franchise location with division");
			String franchiseID = adminInfoMgr.addFranchiseLocationWithDivision(driver, divisionName);

			// Search Franchisee
			fc.utobj().printTestStep("Search and click on Franchise Location");
			franchise.searchFranchiseAndClick(driver, franchiseID);

			// Add owners - required for sending FDD
			fc.utobj().printTestStep("Add Owners");
			InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();
			fc.utobj().clickElement(driver, centerInfoPage.tabOwners);
			franchise.addOwner(driver);

			// Send FDD
			Reporter.log("***** Send FDD ********");
			fc.utobj().printTestStep("Send FDD");
			fc.infomgr().infomgr_common().InfoMgrFDD(driver);
			InfoMgrFDDFDDSummaryPage objFDDSummaryPage = new InfoMgrFDDFDDSummaryPage(driver);
			fc.utobj().clickElement(driver, objFDDSummaryPage.btnSendFDD);
			Thread.sleep(2000);

			InfoMgrFDDSearchFranchiseePage objSearchFranchiseePage = objFDDSummaryPage.getFDDSearchFranchiseePage();
			fc.utobj().sendKeys(driver, objSearchFranchiseePage.txtFranchiseID, franchiseID);
			fc.utobj().selectValFromMultiSelect(driver, objSearchFranchiseePage.multiCheckBoxDiv, divisionName);
			// franchise.multiSelectListBox(driver,
			// objSearchFranchiseePage.multiCheckBoxDiv,divisionName);
			fc.utobj().clickElement(driver, objSearchFranchiseePage.btnSearch);

			franchise.checkCheckBox(driver, franchiseID);
			InfoMgrFDDSearchFranchiseesResultPage objSearchResult = objFDDSummaryPage
					.getFDDSearchFranchiseeResultPage();

			fc.utobj().clickElement(driver, objSearchResult.btnSendFDDEmail);

			InfoMgrFDDSendFDDPage objSendFDD = objFDDSummaryPage.getFDDSendFDDPage();
			fc.utobj().selectDropDownByVisibleText(driver, objSendFDD.drpMailTemplate, fddTemplate);
			fc.utobj().clickElement(driver, objSendFDD.btnSend);

			// Verify the division / brand is displayed on the page
			boolean boolDivisionFlag = fc.utobj().assertPageSource(driver, divisionName);
			if (boolDivisionFlag == true) {
				Reporter.log("Division Name column is displayed on the page : Divison Name =" + divisionName);
			} else {
				fc.utobj().throwsException("Division Name column is not displayed on the page. Test failes.... ");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr", "IMFailedTC" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-21", testCaseId = "TC_InfoMgr_FDD_WithandWithoutBrand_Verify", testCaseDescription = "This test case will verify FDD with and without brand/division.", reference = {
			"" })
	public void VerifyFDDWithAndWithoutBrand() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		FranchiseesCommonMethods franchise = fc.infomgr().franchisees();
		AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();

		try {
			driver = fc.loginpage().login(driver);

			// Add Division
			fc.utobj().printTestStep("Add Division");
			String divisionName = adminInfoMgr.addDivisionName(driver);
			Reporter.log("Division name is : " + divisionName);

			// Upload FDD with division
			fc.utobj().printTestStep("Upload FDD with division");
			String filePath1 = FranconnectUtil.config.get("inputDirectory") + "\\TestData\\pdf1.pdf";
			String fddWithDiv = adminInfoMgr.uploadFDDWithDivision(driver, filePath1, divisionName);
			Reporter.log("FDD with division is " + fddWithDiv);

			// Upload FDD without division
			fc.utobj().printTestStep("Upload FDD without division");
			String filePath2 = FranconnectUtil.config.get("inputDirectory") + "\\testdata\\pdf2.pdf";
			String fddWithoutDiv = adminInfoMgr.uploadFDDWithoutDivision(driver, filePath2);
			Reporter.log("FDD without division is " + fddWithoutDiv);

			// FDD Email Template
			fc.utobj().printTestStep("Create FDD template");
			String fddTemplate = adminInfoMgr.createFDDTemplate(driver);

			// Add Franchisee Location with division
			fc.utobj().printTestStep("Add Franchise location with division");
			String franchiseID = "franchiseeFDD";
			String franchiseID1 = adminInfoMgr.addFranchiseLocationWithDivision(driver, divisionName, franchiseID);

			// Search Franchisee
			fc.utobj().printTestStep("Search and click franchise location");
			franchise.searchFranchiseAndClick(driver, franchiseID1);

			// Add owners - required for sending FDD
			fc.utobj().printTestStep("Add Owners");
			InfoMgrMultiUnitInfoPage entityInfoPage = new InfoMgrMultiUnitEntityPage(driver).getMultiUnitInfoPage();
			fc.utobj().clickElement(driver, entityInfoPage.tabOwners);
			franchise.addOwner(driver);

			// Add second Franchisee Location
			fc.utobj().printTestStep("Add Franchise location");
			String franchiseID2 = adminInfoMgr.addFranchiseLocation(driver, franchiseID);

			// Search second Franchisee
			fc.utobj().printTestStep("Search and click franchise location");
			franchise.searchFranchiseAndClick(driver, franchiseID2);

			// Add owners - required for sending FDD
			fc.utobj().printTestStep("Add Owners");
			fc.utobj().clickElement(driver, entityInfoPage.tabOwners);
			franchise.addOwner(driver);

			// Send FDD to both franchisee
			fc.utobj().printTestStep("Search the FDD on the Search Franchisee page");

			fc.infomgr().infomgr_common().InfoMgrFDD(driver);
			InfoMgrFDDFDDSummaryPage objFDDSummaryPage = new InfoMgrFDDFDDSummaryPage(driver);
			fc.utobj().clickElement(driver, objFDDSummaryPage.btnSendFDD);
			Thread.sleep(2000);

			// Search Franchisee

			InfoMgrFDDSearchFranchiseePage objSearchFranchiseePage = objFDDSummaryPage.getFDDSearchFranchiseePage();
			fc.utobj().sendKeys(driver, objSearchFranchiseePage.txtFranchiseID, franchiseID);
			fc.utobj().clickElement(driver, objSearchFranchiseePage.btnSearch);

			// Verify that Associated FDD for the first Franchisee
			fc.utobj().printTestStep("Verify that associated FDD for the first franchise location - " + franchiseID1);
			String xpath = ".//*[text()='" + franchiseID1 + "']/ancestor::tr[1]";
			WebElement element = fc.utobj().getElementByXpath(driver, xpath);
			String franchiseeRowData = fc.utobj().getText(driver, element);
			if (franchiseeRowData.contains(fddWithDiv)) {
				Reporter.log("First Franchisee" + franchiseID1 + " is  associated with correct FDD : " + fddWithDiv);
			} else {
				fc.utobj().throwsException(
						"First Franchisee" + franchiseID1 + " is not associated with correct FDD : " + fddWithDiv);
			}

			// Verify that Associated FDD for the second Franchisee
			fc.utobj().printTestStep("Verify that associated FDD for the second franchise location - " + franchiseID2);
			xpath = ".//*[text()='" + franchiseID2 + "']/ancestor::tr[1]";
			element = fc.utobj().getElementByXpath(driver, xpath);
			franchiseeRowData = fc.utobj().getText(driver, element);
			if (franchiseeRowData.contains(fddWithoutDiv)) {
				Reporter.log(
						"Second Franchisee" + franchiseID2 + " is  associated with correct FDD : " + fddWithoutDiv);
			} else {
				fc.utobj().throwsException(
						"Second Franchisee" + franchiseID2 + " is not associated with correct FDD : " + fddWithoutDiv);
			}

			// Select both the franchisee and send the FDD

			fc.utobj().printTestStep("Send FDD to both the franchisee");

			franchise.checkCheckBox(driver, franchiseID1);
			franchise.checkCheckBox(driver, franchiseID2);
			InfoMgrFDDSearchFranchiseesResultPage objSearchResult = objFDDSummaryPage
					.getFDDSearchFranchiseeResultPage();
			fc.utobj().clickElement(driver, objSearchResult.btnSendFDDEmail);

			InfoMgrFDDSendFDDPage objSendFDD = objFDDSummaryPage.getFDDSendFDDPage();
			fc.utobj().selectDropDownByVisibleText(driver, objSendFDD.drpMailTemplate, fddTemplate);
			fc.utobj().clickElement(driver, objSendFDD.btnSend);

			// Verify that franchise1 and franchise2 are there on FDD summary
			// page
			boolean firstFranFlag = fc.utobj().assertLinkText(driver, franchiseID1);
			boolean secondFranFlag = fc.utobj().assertLinkText(driver, franchiseID2);

			if (firstFranFlag && secondFranFlag) {
				Reporter.log("FDD mails have been sent to both the franchisees successfully !!!");
			} else {
				fc.utobj().throwsException("Some error occured while sending FDD mails. Test failes !!!");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_InfoMgr_FDD_PrintPreview_verify", testCaseDescription = "This test case will verify print preview after sending FDD.", reference = {
			"81591" })
	// Bug Id = 81591
	public void VerifyFDDPrintPreview() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		FranchiseesCommonMethods franchise = fc.infomgr().franchisees();
		AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();

		try {
			driver = fc.loginpage().login(driver);

			// Add Division
			fc.utobj().printTestStep("Add Division");
			String divisionName = adminInfoMgr.addDivisionName(driver);

			// Upload FDD with division
			fc.utobj().printTestStep("Upload FDD with division");
			String filePath1 = FranconnectUtil.config.get("inputDirectory") + "\\TestData\\pdf1.pdf";
			String fddWithDiv = adminInfoMgr.uploadFDDWithDivision(driver, filePath1, divisionName);

			// FDD Email Template
			fc.utobj().printTestStep("Create FDD Template");
			String fddTemplate = adminInfoMgr.createFDDTemplate(driver);

			// Add Franchisee Location with division
			fc.utobj().printTestStep("Add Franchise location with division");
			String franchiseID1 = adminInfoMgr.addFranchiseLocationWithDivision(driver, divisionName);

			// Search franchisee
			fc.utobj().printTestStep("Search franchisee amd click");
			franchise.searchFranchiseAndClick(driver, franchiseID1);

			// Add owners - required for sending FDD
			fc.utobj().printTestStep("Adding Owner");
			InfoMgrMultiUnitInfoPage entityInfoPage = new InfoMgrMultiUnitEntityPage(driver).getMultiUnitInfoPage();
			fc.utobj().clickElement(driver, entityInfoPage.tabOwners);
			franchise.addOwner(driver);

			// Send FDD to franchisee
			fc.utobj().printTestStep("Sending FDD");
			fc.infomgr().infomgr_common().InfoMgrFDD(driver);
			InfoMgrFDDFDDSummaryPage objFDDSummaryPage = new InfoMgrFDDFDDSummaryPage(driver);
			fc.utobj().clickElement(driver, objFDDSummaryPage.btnSendFDD);
			Thread.sleep(2000);

			InfoMgrFDDSearchFranchiseePage objSearchFranchiseePage = objFDDSummaryPage.getFDDSearchFranchiseePage();
			fc.utobj().sendKeys(driver, objSearchFranchiseePage.txtFranchiseID, franchiseID1);
			fc.utobj().selectValFromMultiSelect(driver, objSearchFranchiseePage.multiCheckBoxDiv, divisionName);
			fc.utobj().clickElement(driver, objSearchFranchiseePage.btnSearch);

			franchise.checkCheckBox(driver, franchiseID1);
			InfoMgrFDDSearchFranchiseesResultPage objSearchResult = objFDDSummaryPage
					.getFDDSearchFranchiseeResultPage();

			fc.utobj().clickElement(driver, objSearchResult.btnSendFDDEmail);

			InfoMgrFDDSendFDDPage objSendFDD = objFDDSummaryPage.getFDDSendFDDPage();
			fc.utobj().selectDropDownByVisibleText(driver, objSendFDD.drpMailTemplate, fddTemplate);
			fc.utobj().clickElement(driver, objSendFDD.btnSend);

			fc.utobj().printTestStep("Verify Print Preview");
			fc.utobj().clickElement(driver, objFDDSummaryPage.btnPrint);

			Set<String> windows = new HashSet<String>();
			windows = driver.getWindowHandles();

			boolean isDivisionNameFound = false;

			String currentWindow = driver.getWindowHandle();
			for (String window : windows) {
				if (!window.equals(currentWindow)) {
					driver.switchTo().window(window);
					if (driver.getCurrentUrl().contains("printcheck")) {
						isDivisionNameFound = fc.utobj().assertPageSource(driver, divisionName);
					}
				}
			}
			if (isDivisionNameFound) {
				Reporter.log("Division Name column is displayed on the print preview . Test passes !!! ");
			} else {
				fc.utobj().throwsException(
						"Division Name column is not displayed on the print preview page. Test failes.... ");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr","InfoMgr7" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_InfoMgr_FDD_SendFDD_SearchResult_Verify", testCaseDescription = "This test case will verify that correct franchise location is displayed on the search franchisee page for FDD", reference = {
			"68378" })
	// Bug ID = 68378
	public void VerifyLocationsOnFDDSearchResultPage() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		FranchiseesCommonMethods franchise = fc.infomgr().franchisees();
		AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();

		try {
			driver = fc.loginpage().login(driver);

			// Add Division
			fc.utobj().printTestStep("Add Division");
			String divisionName = fc.utobj().generateTestData("Division1");
			divisionName = adminInfoMgr.addDivisionName(driver);

			// Upload FDD
			fc.utobj().printTestStep("Upload FDD");
			String filePath = FranconnectUtil.config.get("inputDirectory") + "\\TestData\\pdf.pdf";
			adminInfoMgr.uploadFDDWithDivision(driver, filePath, divisionName);

			// FDD Email Template
			fc.utobj().printTestStep("Create FDD Template");
			String fddTemplate = adminInfoMgr.createFDDTemplate(driver);

			// Add Franchisee Location
			fc.utobj().printTestStep("Add Franchise Location");
			String franchiseID = adminInfoMgr.addFranchiseLocationWithDivision(driver, divisionName);

			// Navigate and search franchisee
			fc.utobj().printTestStep("Search Franchise and click");
			franchise.searchFranchiseAndClick(driver, franchiseID);

			// Add owners - required for sending FDD
			fc.utobj().printTestStep("Adding owners");
			InfoMgrMultiUnitInfoPage entityInfoPage = new InfoMgrMultiUnitEntityPage(driver).getMultiUnitInfoPage();
			fc.utobj().clickElement(driver, entityInfoPage.tabOwners);
			franchise.addOwner(driver);

			// Send FDD
			fc.utobj().printTestStep("Sending FDD");
			fc.infomgr().infomgr_common().InfoMgrFDD(driver);
			InfoMgrFDDFDDSummaryPage objFDDSummaryPage = new InfoMgrFDDFDDSummaryPage(driver);
			fc.utobj().clickElement(driver, objFDDSummaryPage.btnSendFDD);
			Thread.sleep(2000);

			InfoMgrFDDSearchFranchiseePage objSearchFranchiseePage = objFDDSummaryPage.getFDDSearchFranchiseePage();
			fc.utobj().clickElement(driver, objSearchFranchiseePage.drpFranchiseeType);
			WebElement selectAllFranchisee = objSearchFranchiseePage.drpFranchiseeType.findElement(By.id("selectAll"));
			selectAllFranchisee.click();
			fc.utobj().clickElement(driver, objSearchFranchiseePage.btnSearch);

			InfoMgrFDDSearchFranchiseesResultPage objSearchResult = objFDDSummaryPage
					.getFDDSearchFranchiseeResultPage();

			try {
				fc.utobj().clickElement(driver, objSearchResult.lnkShowAll);
			} catch (Exception ex) {

			}

			boolean isFranchiseeIDLinkPresent = fc.utobj().assertLinkText(driver, franchiseID);

			// Verify that FranchiseeID is displayed on the page
			if (isFranchiseeIDLinkPresent) {
				Reporter.log(
						"Franchisees are displayed on the search result page for sening FDD. Test case passes !!!");
			} else {
				fc.utobj()
						.throwsException("Locations are not displayed on the search result page. Test case fails !!!");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
}
