package com.builds.test.infomgr;

public class InfoMgrFranchiseeFilter {

	private String franchiseeID;
	private String centerName;
	private String statusID;
	private String storeTypeID;
	private String divisionID;
	private String entityName;
	private String areaRegionName;
	private String areaRegionOwner;
	private String country;
	private String state;
	private String city;
	private String contactFirstName;
	private String contactLastName;
	private String spouseFirstName;
	private String spouseLastName;
	private String phone;
	private String zipCode;
	private String emailID;
	private String ownerType;
	private String storeStatus;
	private String storeEmail;

	public String getStoreStatus() {
		return storeStatus;
	}

	public void setStoreStatus(String storeStatus) {
		this.storeStatus = storeStatus;
	}

	public String getOwnerType() {
		return ownerType;
	}

	public void setOwnerType(String ownerType) {
		this.ownerType = ownerType;
	}

	public String getEmailID() {
		return emailID;
	}

	public void setEmailID(String emailID) {
		this.emailID = emailID;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getOwnerFirstName() {
		return ownerFirstName;
	}

	public void setOwnerFirstName(String ownerFirstName) {
		this.ownerFirstName = ownerFirstName;
	}

	public String getOwnerLastName() {
		return ownerLastName;
	}

	public void setOwnerLastName(String ownerLastName) {
		this.ownerLastName = ownerLastName;
	}

	private String ownerFirstName;
	private String ownerLastName;

	public String getSpouseFirstName() {
		return spouseFirstName;
	}

	public void setSpouseFirstName(String spouseFirstName) {
		this.spouseFirstName = spouseFirstName;
	}

	public String getSpouseLastName() {
		return spouseLastName;
	}

	public void setSpouseLastName(String spouseLastName) {
		this.spouseLastName = spouseLastName;
	}

	public String getCenterName() {
		return centerName;
	}

	public void setCenterName(String centerName) {
		this.centerName = centerName;
	}

	public String getContactFirstName() {
		return contactFirstName;
	}

	public void setContactFirstName(String contactFirstName) {
		this.contactFirstName = contactFirstName;
	}

	public String getContactLastName() {
		return contactLastName;
	}

	public void setContactLastName(String contactLastName) {
		this.contactLastName = contactLastName;
	}

	public String getStatusID() {
		return statusID;
	}

	public void setStatusID(String statusID) {
		this.statusID = statusID;
	}

	public String getStoreTypeID() {
		return storeTypeID;
	}

	public void setStoreTypeID(String storeTypeID) {
		this.storeTypeID = storeTypeID;
	}

	public String getDivisionID() {
		return divisionID;
	}

	public void setDivisionID(String divisionID) {
		this.divisionID = divisionID;
	}

	public String getEntityName() {
		return entityName;
	}

	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	public String getAreaRegionName() {
		return areaRegionName;
	}

	public void setAreaRegionName(String areaRegionName) {
		this.areaRegionName = areaRegionName;
	}

	public String getAreaRegionOwner() {
		return areaRegionOwner;
	}

	public void setAreaRegionOwner(String areaRegionOwner) {
		this.areaRegionOwner = areaRegionOwner;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getFranchiseeID() {
		return franchiseeID;
	}

	public void setFranchiseeID(String franchiseeID) {
		this.franchiseeID = franchiseeID;
	}

	public String getstoreEmail() {
		return storeEmail;
	}

	public void setstoreEmail(String storeEmail) {
		this.storeEmail = storeEmail;
	}

}
