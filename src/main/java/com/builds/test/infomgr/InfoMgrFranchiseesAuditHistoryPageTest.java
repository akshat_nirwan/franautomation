package com.builds.test.infomgr;

import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.builds.uimaps.infomgr.InfoMgrCenterInfoPage;
import com.builds.uimaps.infomgr.InfoMgrFranchiseesPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class InfoMgrFranchiseesAuditHistoryPageTest {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_InfoMgr_Franchisees_AuditHistory_Verify", testCaseDescription = "This test case will verify the audit history", reference = {
			"83812" })
	// Bug ID = 83812
	public void VerifyInfoMgrFranchiseesAuditHistoryPage() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		FranchiseesCommonMethods franchise = fc.infomgr().franchisees();
		AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();

		try {
			driver = fc.loginpage().login(driver);

			// Add new Franchisee Location
			fc.utobj().printTestStep("Add Franchise Location");
			String franchiseID = adminInfoMgr.addFranchiseLocation(driver);

			// Search franchisee and click on it.
			fc.utobj().printTestStep("Search Franchisee and click");
			franchise.searchFranchiseAndClick(driver, franchiseID);

			InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();

			// click on 'View Audit History' from action menu
			fc.utobj().printTestStep("Verify Audit History page");
			fc.utobj().selectDropDown(driver, centerInfoPage.drpActionMenu, "View Audit History");

			// Verify that there is no error page
			boolean isErrorPage = fc.utobj().assertPageSource(driver,
					"Oops! Looks like the page you requested is unavailable");
			if (isErrorPage) {
				fc.utobj().throwsException("Error page is there on the Audit History Page. Test case fails !!!");
			} else {
				Reporter.log("Audit history page loads successfully. Test case passes !!! ");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

}
