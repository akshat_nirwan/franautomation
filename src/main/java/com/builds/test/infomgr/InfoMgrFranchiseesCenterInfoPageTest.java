package com.builds.test.infomgr;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.builds.uimaps.infomgr.InfoMgrCenterInfoPage;
import com.builds.uimaps.infomgr.InfoMgrFranchiseesPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class InfoMgrFranchiseesCenterInfoPageTest {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "infomgr"})

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-11-23", testCaseId = "TC_InfoMgr_Franchisees_CenterInfo_Modify", testCaseDescription = "This test case will verify the center info modify functionality", reference = {
			"" })
	// Bug ID =
	public void VerifyInfoMgrFranchiseesCenterInfoModifyFunc() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		FranchiseesCommonMethods franchise = fc.infomgr().franchisees();
		AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();

		try {
			driver = fc.loginpage().login(driver);

			// Add new Franchisee Location
			fc.utobj().printTestStep("Add Franchise Location");
			String franchiseID = adminInfoMgr.addFranchiseLocationForInfoMgrName(driver, false);

			// Search franchisee and click on it
			fc.utobj().printTestStep("Search and Click Location");
			franchise.searchFranchiseAndClick(driver, franchiseID);

			InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();

			// Click on Save button
			fc.utobj().printTestStep("Modify center info and verify ");
			fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// for
																			// custom
																			// fields
			fc.utobj().clickElement(driver, centerInfoPage.btnSumbit);
			//String centerInfoDivContent = fc.utobj().getText(driver, centerInfoPage.centerInfoLastContactedLabel);
			boolean LastContacted= fc.utobj().isElementPresent(driver, centerInfoPage.centerInfoLastContactedLabel);
			if (LastContacted) {
				Reporter.log("Franchise location has been modifed successfully");
			} else {
				fc.utobj().throwsException("Center Info -> Modify has been failed !!!");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr"})

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-11-23", testCaseId = "TC_InfoMgr_Franchisees_CenterInfo_Modify_Page_Verify", testCaseDescription = "This test case will verify that center info page is not blank", reference = {
			"66055" })
	// Bug ID = 66055
	public void VerifyInfoMgrFranchiseesCenterInfoModifyPage() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		FranchiseesCommonMethods franchise = fc.infomgr().franchisees();
		AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();

		try {
			driver = fc.loginpage().login(driver);

			// Add new Franchisee Location
			fc.utobj().printTestStep("Add Franchise Location");
			String franchiseID = adminInfoMgr.addFranchiseLocationForInfoMgrName(driver, false);

			// Search franchisee and click on it
			fc.utobj().printTestStep("Search and Click Location");
			franchise.searchFranchiseAndClick(driver, franchiseID);

			InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();

			// Verify that Center Info Modify page is displaying properly
			fc.utobj().printTestStep("verify Center Info Page ");
			String tableData = fc.utobj().getText(driver, centerInfoPage.tblStartRecords);
			String temp =tableData.toLowerCase();
			//System.out.println(tableData);

			if (temp.contains("center details") && temp.contains("center contact details")
					&& temp.contains("legal notice details") && temp.contains("email campaign"))
			{
				Reporter.log("Page is not blank and it loads successfully !!!");
			} else {
				fc.utobj().throwsException("Modify page details are missing. Test case fails !!!");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Verify_InfoMgr_Franchisees_AddToCenterInfo_Fields", testCaseDescription = "Test to verify fileds added by manage form generator  to center info", reference = {
			"" })
	public void InfoMgr_Franchisee_CenterInfo_AddToCenterInfo_Fields() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		FranchiseesCommonMethods franchisees = fc.infomgr().franchisees();
		AdminInfoMgrCommonMethods adminInfoMgr = new AdminInfoMgrCommonMethods();
		String[] formsArray = { "Agreement", "Contract Signing", "Customer Complaints", "Default and Termination",
				"Employees", "Entity Details", "Events", "Financial", "Guarantors", "Insurance", "Legal Violation",
				"Lenders", "Marketing", "Mystery Review", "Owners", "Real Estate", "Renewal", "Territory", "Training",
				"Transfer" };

		try {
			fc.loginpage().login(driver);

			fc.utobj().printTestStep("Add Fileds to Center Info");
			Map<String, String> dsCenterInfoFields = adminInfoMgr.configure_Franchisees_AddToCenterInfo_AllForms(driver,
					config);

			fc.utobj().printTestStep("Adding franchise Location");
			String franchiseID = adminInfoMgr.addFranchiseLocation(driver);
			franchisees.searchFranchiseAndClick(driver, franchiseID);
			franchisees.modifyCenterInfo(driver, config);

			fc.utobj().printTestStep("Verify added fields on center info page");

			for (int i = 0; i < formsArray.length; i++) {
				try {
					fc.utobj().getElementByXpath(driver, ".//*[@id='centerInfoDIV']//td[contains(text() , '"
							+ dsCenterInfoFields.get(formsArray[i]) + "')]");
					Reporter.log(
							"Field " + dsCenterInfoFields.get(formsArray[i]) + " is displayed on the Center Info page");
				} catch (Exception ex) {
					fc.utobj().throwsException("Field " + dsCenterInfoFields.get(formsArray[i])
							+ " is not displayed on the Center Info page");
				}
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr_test1" }, priority = 2)

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Verify_InfoMgr_Franchisee_CustomFieldsandSection_CenterInfo", testCaseDescription = "Test to verify custom fields added to center info by manage form generator.", reference = {
			"" })
	public void InfoMgr_Franchisee_CenterInfo_CustomFields() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		FranchiseesCommonMethods franchisees = fc.infomgr().franchisees();
		AdminInfoMgrCommonMethods adminInfoMgr = new AdminInfoMgrCommonMethods();

		try {
			fc.loginpage().login(driver);

			fc.utobj().printTestStep("Add custom fields to Center Info");
			Map<String, String> dsCenterInfoCustomFields = adminInfoMgr.ManageFormGenerator_Add_Section_Fields_To_Forms(
					driver, config, "Franchisee", "Center Info", "fran");

			fc.utobj().printTestStep("Adding franchise Location");
			String franchiseID = adminInfoMgr.addFranchiseLocation(driver);
			franchisees.searchFranchiseAndClick(driver, franchiseID);
			fc.utobj().printTestStep("Verify that added custom fields are displayed on the center info page");

			for (String key : dsCenterInfoCustomFields.keySet()) {
				if (!key.contains("drpOption")) {
					if (fc.utobj().assertPageSource(driver, dsCenterInfoCustomFields.get(key))) {
						Reporter.log(
								"Custom field " + dsCenterInfoCustomFields.get(key) + " is displayed on the page !!");
					} else {
						fc.utobj().throwsException(
								"Custom field " + dsCenterInfoCustomFields.get(key) + " is not displayed on the page");
					}
				}
			}

			fc.utobj().printTestStep("Deleting the created section and its fields");
			adminInfoMgr.deleteSectionandFields(driver, config, "Franchisee", "Center Info", dsCenterInfoCustomFields);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

}
