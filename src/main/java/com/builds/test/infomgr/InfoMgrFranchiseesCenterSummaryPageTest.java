package com.builds.test.infomgr;

import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.builds.uimaps.infomgr.InfoMgrCenterInfoPage;
import com.builds.uimaps.infomgr.InfoMgrCenterSummaryPage;
import com.builds.uimaps.infomgr.InfoMgrFranchiseesPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class InfoMgrFranchiseesCenterSummaryPageTest {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "infomgr"})

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-12-04", testCaseId = "TC_InfoMgr_Franchisees_CenterInfo_CenterSummary_Page_Verify", testCaseDescription = "This test case will verify Center Summary Page", reference = {
			"66652" })
	// Bug ID = 66652
	// Script Updated by Govind
	public void VerifyInfoMgrFranchiseesCenterInfoViewCenterSummaryPage() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		FranchiseesCommonMethods franchise = fc.infomgr().franchisees();
		AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();

		try {
			driver = fc.loginpage().login(driver);

			/*// Add new Franchisee Location
			fc.utobj().printTestStep("Add Franchise Location");
			String franchiseID = adminInfoMgr.addFranchiseLocation(driver);*/
			
			//Add Franchise location
			fc.utobj().printTestStep("Add a franchise location");
			String franchiseID = adminInfoMgr.addFranchiseLocationForInfoMgrName(driver, false);

			// Search franchisee and click on it
			fc.utobj().printTestStep("Search and Click Location");
			franchise.searchFranchiseAndClick(driver, franchiseID);

			InfoMgrFranchiseesPage infoMgrFranchiseePage = new InfoMgrFranchiseesPage(driver);

			InfoMgrCenterInfoPage centerInfoPage = infoMgrFranchiseePage.getCenterInfoPage();

			// click on 'View Center Summary' from Action menu
			fc.utobj().printTestStep("Click on 'View Center Summary and verify the page");
			fc.utobj().selectDropDownByPartialText(driver, centerInfoPage.drpActionMenu, "View Center Summary");

			// Verify that Center Info Summary page displays the data

			InfoMgrCenterSummaryPage summaryPage = infoMgrFranchiseePage.getCenterSummaryPage();

			String tableData = fc.utobj().getText(driver, summaryPage.tblCenterSummary);
			
			String temp = tableData.toLowerCase();

			if (temp.contains("center details") && temp.contains("center contact details")
					&& temp.contains("legal notice details")) {
				Reporter.log("Page is not blank and summary page loads successfully !!!");
			} else {
				fc.utobj().throwsException("Center Summary page details are missing. Test case fails !!!");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

}
