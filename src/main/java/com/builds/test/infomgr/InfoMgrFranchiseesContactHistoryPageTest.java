package com.builds.test.infomgr;

import java.util.ArrayList;

import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.common.CorporateUser;
import com.builds.uimaps.infomgr.InfoMgrContactHistoryPage;
import com.builds.uimaps.infomgr.InfoMgrFranchiseesPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class InfoMgrFranchiseesContactHistoryPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "infomgr2030" })

	// Bug Id - 82028
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_InfoMgr_Franchisee_ContactHistory_AddRemarks", testCaseDescription = "This test case will verify Add Remarks from contact history page", reference = {
			"82028" })
	public void addRemarkContactHistoryPage() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		FranchiseesCommonMethods franchise = fc.infomgr().franchisees();
		AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();

		try {
			driver = fc.loginpage().login(driver);

			// Create a franchise location
			fc.utobj().printTestStep("Add Franchise Location");
			String franchiseID = adminInfoMgr.addFranchiseLocation(driver);

			// Search ad
			fc.utobj().printTestStep("Search and Click Location");
			franchise.searchFranchiseAndClick(driver, franchiseID);

			// Naviagte to contact history and add a remark
			fc.utobj().printTestStep("Add Remark");
			InfoMgrContactHistoryPage objContactHistory = new InfoMgrFranchiseesPage(driver).getContactHistoryPage();
			fc.utobj().clickElement(driver, objContactHistory.lnkContactHistory);
			fc.utobj().clickElement(driver, objContactHistory.btnAddRemark);
			String remark = franchise.addRemark(driver, objContactHistory);

			fc.utobj().assertSingleLinkText(driver, remark.trim());

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" })

	// Bug Id- 65268
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_InfoMgr_Franchisee_ActionMenu_LogACall_VerifyJavaScriptError", testCaseDescription = "This test case will log a call and verify its functionality", reference = {
			"65268" })
	public void InfoMgrFranchissesLogACallVerifyJavaScriptError() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		FranchiseesCommonMethods franchise = fc.infomgr().franchisees();
		AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();

		try {
			driver = fc.loginpage().login(driver);

			// Add new Franchisee Location
			fc.utobj().printTestStep("Add Franchise Location");
			String franchiseID = adminInfoMgr.addFranchiseLocation(driver);

			// Navigate to admin > InfoMgr > Manage Form Generator
			fc.utobj().printTestStep("Disable fields from Admin > Info Mgr > Manage form Generator tab");
			adminInfoMgr.disableFieldsInFormGenerator(driver, "Call", "Comments");

			// Search franchisee
			fc.utobj().printTestStep("Search Franchise and log a call from action menu");
			franchise.searchFranchise(driver, franchiseID);

			// Log a call
			fc.utobj().actionImgOption(driver, franchiseID, "Log a Call");
			franchise.logACall(driver, false);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" })

	// Bug Id- 65268
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-15", testCaseId = "TC_InfoMgr_Franchisee_ContactHistory_LogATask", testCaseDescription = "This test case will verify Log a task from contact history page", reference = {
			"65268" })
	public void InfoMgrFranchissesLogATaskVerifyJavaScriptError() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		FranchiseesCommonMethods franchise = fc.infomgr().franchisees();
		AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();

		try {
			driver = fc.loginpage().login(driver);
			// Add new Franchisee Location
			fc.utobj().printTestStep("Add Franchise Location");
			String franchiseID = adminInfoMgr.addFranchiseLocation(driver);

			// Add Corporate user
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail("infomgrautomation@staffex.com");
			corpUser = corpTest.createDefaultUser(driver, corpUser);

			// Navigate to admin > InfoMgr > Manage Form Generator
			fc.utobj().printTestStep("Disable fields from Admin > Info Mgr > Manage Form Generator page");
			adminInfoMgr.disableFieldsInFormGenerator(driver, "Task", "Task Description"); // Task
																							// Description
																							// lable
																							// change
																							// instead
																							// of
																							// comments

			// Navigate to Info Mgr > Franchisees
			fc.utobj().printTestStep("Search Franchise and click");
			franchise.searchFranchiseAndClick(driver, franchiseID);

			fc.utobj().printTestStep("Log a task");
			InfoMgrContactHistoryPage contactHistoryPage = new InfoMgrFranchiseesPage(driver).getContactHistoryPage();
			fc.utobj().clickElement(driver, contactHistoryPage.lnkContactHistory);
			fc.utobj().clickElement(driver, contactHistoryPage.btnLogaTask);

			String subject = franchise.logaTask(driver, corpUser.getuserFullName(), true);
			// Verify that task has been created successfully

			InfoMgrContactHistoryPage objContactHistory = new InfoMgrFranchiseesPage(driver).getContactHistoryPage();

			String taskTableContent = fc.utobj().getText(driver, objContactHistory.tableTaskHistory);
			if (taskTableContent.contains(subject)) {
				Reporter.log("Task has been created successfully");
			} else {
				fc.utobj().throwsException("Task Creation failed");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgrLOgCal01" })

	// Bug Id- 69591
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-06", testCaseId = "TC_InfoMgr_Franchisees_ContactHistory_LogACall_Page_Verify", testCaseDescription = "This test case will verify log a call from contact history page", reference = {
			"69591" })
	public void InfoMgrFranchissesContactHistoryLogACallPage() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		FranchiseesCommonMethods franchise = fc.infomgr().franchisees();
		AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();

		try {
			driver = fc.loginpage().login(driver);

			// Add new Franchisee Location
			fc.utobj().printTestStep("Add Franchise Location");
			String franchiseID = adminInfoMgr.addFranchiseLocation(driver);

			fc.utobj().printTestStep("Search Franchisee and click");
			franchise.searchFranchiseAndClick(driver, franchiseID);

			fc.utobj().printTestStep("Navigate to contact history and click on log a call");
			InfoMgrContactHistoryPage contactHistoryPage = new InfoMgrFranchiseesPage(driver).getContactHistoryPage();
			fc.utobj().clickElement(driver, contactHistoryPage.lnkContactHistory);
			fc.utobj().clickElement(driver, contactHistoryPage.btnLogaCall);

			fc.utobj().printTestStep("Verify Log a call page");

			fc.commonMethods().switch_cboxIframe_frameId(driver);

			if (fc.utobj().assertPageSource(driver, fc.utobj().translateString("Franchise ID"))
					&& fc.utobj().assertPageSource(driver, fc.utobj().translateString("Subject"))
					&& fc.utobj().assertPageSource(driver, fc.utobj().translateString("Status"))
					&& fc.utobj().assertPageSource(driver, fc.utobj().translateString("Type of Call"))) {
				Reporter.log("Log a Call page loads successfully !!!");
			} else {
				fc.utobj().throwsException("Page doesn't load properly. Test Failes !!!");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" })

	// Bug ID - 65265
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_InfoMgr_Franchisee_ContactHistory_Remarks_CustomFileds", testCaseDescription = "THis test case is used to verify custom fields on add remarks form.", reference = {
			"65265" })
	public void verifyRemarksCustomFieldsOnContactHistory() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		FranchiseesCommonMethods franchise = fc.infomgr().franchisees();
		AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();

		try {
			driver = fc.loginpage().login(driver);

			// Add Custom fields to form Remarks
			fc.utobj().printTestStep("Add fields to Remarks form ");
			ArrayList<String> lstFields = adminInfoMgr.addFields(driver, "Franchisee", "Remark");

			// Create a franchise location
			fc.utobj().printTestStep("Add Franchise Location ");
			String franchiseID = adminInfoMgr.addFranchiseLocation(driver);

			// Search franchise location
			fc.utobj().printTestStep("Search Franchise and click ");
			franchise.searchFranchiseAndClick(driver, franchiseID);

			// Navigate to contact history and add a remark
			fc.utobj().printTestStep("Add Remark and verify the custom fields");
			InfoMgrContactHistoryPage objContactHistory = new InfoMgrFranchiseesPage(driver).getContactHistoryPage();
			fc.utobj().clickElement(driver, objContactHistory.lnkContactHistory);
			fc.utobj().clickElement(driver, objContactHistory.btnAddRemark);
			String remark = franchise.addRemark(driver, objContactHistory);

			// Click on the Remarks added and verify that custom fields are
			// displayed on the Remarks form
			boolean isRemarksLinkPresent = fc.utobj().assertLinkText(driver, remark);
			if (!isRemarksLinkPresent) {
				fc.utobj().throwsException("Remarks link is not displayed on the page. Testcase can't continue !!!");
			}

			Reporter.log(
					"***************** Verifying that custom fields are displayed on the remarks page ***************** \n");

			fc.utobj().clickLink(driver, remark);

			fc.commonMethods().switch_cboxIframe_frameId(driver);

			boolean isCustomFieldsPresent = fc.utobj().assertPageSource(driver, lstFields.get(0))
					&& fc.utobj().assertPageSource(driver, lstFields.get(1));
			if (isCustomFieldsPresent) {
				fc.utobj().switchFrameToDefault(driver);

				Reporter.log("**** Pass **** - Custom fields are displayed on the page . Test case passes !!!");
			} else {
				fc.utobj().throwsException(
						"**** Fail **** - Custom fields are not displayed on the remarks section. Test case fails !!!");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

}
