package com.builds.test.infomgr;

import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.builds.uimaps.infomgr.InfoMgrCustomTabPage;
import com.builds.uimaps.infomgr.InfoMgrFranchiseesPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class InfoMgrFranchiseesCutomTabPageTest {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "InfoFormGenerate" })

	// Bug id - 78676
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "InfoMgr_Franchisees_CustomTab_NoDataMessage", testCaseDescription = "This test case will verify that no data message is displayed for new tab.", reference = {
			"78676" })
	public void VerifyInfoMgrFranchiseesAddonTabNoDataMessage() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		FranchiseesCommonMethods franchiseMethods = fc.infomgr().franchisees();
		AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();

		try {
			driver = fc.loginpage().login(driver);

			// Add new custome tab to franchisee
			fc.utobj().printTestStep("Add new tab");
			String SectionName = "TABULARSECTION";
			SectionName = fc.utobj().generateTestData(SectionName);
			String newTab = adminInfoMgr.addNewTab(driver, "Franchisee");
			adminInfoMgr.addSectionAndFieldsToTab(driver, "Franchisee", newTab,SectionName);

			// Add new Franchisee Location
			fc.utobj().printTestStep("Add franchise location");
			String franchiseID = adminInfoMgr.addFranchiseLocation(driver);

			// Search franchisee and click on it
			fc.utobj().printTestStep("Search Franchise location and click");
			franchiseMethods.searchFranchiseAndClick(driver, franchiseID);

			fc.utobj().printTestStep("Click on new tab and verify No Data Message");

			InfoMgrCustomTabPage modulePageObj = new InfoMgrFranchiseesPage(driver).getCustomTabPage();
			try {
				fc.utobj().clickLink(driver, newTab);
			} catch (Exception ex) {
				if (fc.utobj().searchInSelectBoxSingleValue(driver, modulePageObj.drpSelectAction,
						"View Tabs on Sidebar")) {
					fc.utobj().selectDropDownByVisibleText(driver, modulePageObj.drpSelectAction,
							"View Tabs on Sidebar");
				}

				try {
					fc.utobj().clickLink(driver, newTab);
				} catch (Exception e) {
					fc.utobj().throwsException(
							"Not able to click the tab link. Test can't continue. Test case fails !!! ");
				}
			}

			String customTabData = fc.utobj().getText(driver, modulePageObj.customTabData);

			if (customTabData.contains(fc.utobj().translateString("No records found"))) {
				Reporter.log("No record data message is displayed on the page. Test case passes !!!");
			} else {
				fc.utobj().throwsException("No data message is not displayed on the page. Test case failes !!!");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "InfoFormGenerate" })

	// Bug Id -- 72199
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_InfoMgr_Franchisees_AddonTabs_Cancel_Verify", testCaseDescription = "This test case will verify custom tabs cancel functionality.", reference = {
			"72199" })
	public void VerifyInfoMgrFranchiseesCustomTabCancelButtonFunc() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			driver = fc.loginpage().login(driver);

			FranchiseesCommonMethods franchiseMethods = fc.infomgr().franchisees();
			AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();

			// Add new custome tab to franchisee
			fc.utobj().printTestStep("Add new tab ");
			String newTab = adminInfoMgr.addNewTab(driver, "Franchisee");
			fc.utobj().printTestStep("Add section and fileds to tab ");
			String tabSection = "TABULARSECTION";
			String regSection = "REGULARLARSECTION";
			tabSection = tabSection + fc.utobj().generateRandomNumber();
			regSection = regSection + fc.utobj().generateRandomNumber();
			adminInfoMgr.addSectionAndFieldsToTab(driver, "Franchisee", newTab, tabSection);

			// Add new Franchisee Location
			fc.utobj().printTestStep("Add franchise location ");
			String franchiseID = adminInfoMgr.addFranchiseLocation(driver);

			// Search franchisee and click on it
			fc.utobj().printTestStep("Search franchise and click ");
			franchiseMethods.searchFranchiseAndClick(driver, franchiseID);

			// click on the tab and verify that "No data message" is showing up
			// on the page
			fc.utobj().printTestStep("Click on new tab and then cancel ");
			fc.utobj().clickLink(driver, newTab);

			InfoMgrCustomTabPage modulePageObj = new InfoMgrFranchiseesPage(driver).getCustomTabPage();

			fc.utobj().clickElement(driver, modulePageObj.btnCancel);

			if (fc.utobj().assertPageSource(driver, "Center Info")
					&& fc.utobj().assertPageSource(driver, "Email Campaign")) {
				Reporter.log("Cancel button click redirects to Center Info. Test  case passes !!! ");
			} else {
				fc.utobj().throwsException("Cancel button functionality is  not working properly. Test case failes!!!");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

}
