package com.builds.test.infomgr;

import java.util.ArrayList;

import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class InfoMgrFranchiseesEmployessPageTest {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_InfoMgr_Franchisees_Employess_View", testCaseDescription = "This test case will verify the employees page ", reference = {
			"83099" })
	// Bug ID = 83099
	public void VerifyInfoMgrFranchiseeseEmployeeView() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		FranchiseesCommonMethods franchise = fc.infomgr().franchisees();
		AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();

		try {
			driver = fc.loginpage().login(driver);

			// Navigate to admin and configure tabular view for employees form
			ArrayList<String> lstFields = new ArrayList<String>();
			lstFields.add("First Name");
			lstFields.add("Last Name");
			lstFields.add("City");
			lstFields.add("Country");
			lstFields.add("Fax");
			fc.utobj().printTestStep("Configure tabular view for Employees page ");
			adminInfoMgr.configureTabularViewInfoMgrFranchiseesForms(driver, "Employees", lstFields);

			// Add new Franchisee Location
			fc.utobj().printTestStep("Add franchise location ");
			String franchiseID = adminInfoMgr.addFranchiseLocation(driver);
			// String franchiseID = "FranchiseFDDNew28103351";

			// add two employees
			fc.utobj().printTestStep("Add two employees ");
			ArrayList<String> lstEmployees = franchise.addTwoEmployees(driver, franchiseID);

			// Click on view from the action menu
			fc.utobj().printTestStep("Click on single employee and verify the view ");
			fc.utobj().actionImgOption(driver, lstEmployees.get(0), "View");

			// Verify that after clicking on view from action menu - only first
			// employee is displayed on the page and not the second employee
			boolean isFirstEmployeeDisplayed = fc.utobj().assertPageSource(driver, lstEmployees.get(0));
			boolean isSecondEmployeeDisplayed = fc.utobj().assertPageSource(driver, lstEmployees.get(1));
			if (isFirstEmployeeDisplayed && isSecondEmployeeDisplayed == false) {
				Reporter.log("Only selected employee is displayed on the page. Test case passes !!!");
			} else {
				fc.utobj().throwsException("Single Employee view is not correct. Test case fails !!!");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
}
