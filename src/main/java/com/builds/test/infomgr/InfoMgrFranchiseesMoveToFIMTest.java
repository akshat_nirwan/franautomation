package com.builds.test.infomgr;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminAreaRegionAddAreaRegionPageTest;
import com.builds.test.admin.AdminConfigurationConfigureStoreTypePageTest;
import com.builds.test.fs.Call;
import com.builds.test.fs.CallTest;
import com.builds.test.fs.CoApplicantWithoutSeperateLead;
import com.builds.test.fs.CoApplicantsTest;
import com.builds.test.fs.Email;
import com.builds.test.fs.FSLeadSummaryPageTest;
import com.builds.test.fs.FSSearchPageTest;
import com.builds.test.fs.LeadManagementTest;
import com.builds.test.fs.Sales;
import com.builds.test.fs.Sales_Common;
import com.builds.test.fs.SendEmailTest;
import com.builds.test.fs.Task;
import com.builds.test.fs.TaskTest;
import com.builds.uimaps.admin.AdminFranchiseLocationAddFranchiseLocationPage;
import com.builds.uimaps.admin.AdminFranchiseLocationManageFranchiseLocationsAddUserPage;
import com.builds.uimaps.fs.FSLeadSummaryCompliancePage;
import com.builds.uimaps.fs.FSLeadSummaryDocumentsPage;
import com.builds.uimaps.fs.FSLeadSummaryPersonalProfilePage;
import com.builds.uimaps.fs.FSLeadSummaryQualificationDetailsPage;
import com.builds.uimaps.fs.FSLeadSummaryRealEstatePage;
import com.builds.uimaps.fs.LeadManagementUI;
import com.builds.uimaps.fs.LeadSummaryUI;
import com.builds.uimaps.fs.PrimaryInfoUI;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class InfoMgrFranchiseesMoveToFIMTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "infomgr", "Run12" })

	@TestCase(createdOn = "2017-10-13", updatedOn = "2017-10-13", testCaseId = "TC_Move_To_InfoMgr_001", testCaseDescription = "Verify the Move to Info Mgr link in Sales Lead .", reference = { "" })
	public void InfoMgr_Franchisee_Move_To_InfoMgr() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet1 = fc.utobj().readTestDatawithsqllite("infomgr", testCaseId, "Lead");

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		FSLeadSummaryQualificationDetailsPage npobj = new FSLeadSummaryQualificationDetailsPage(driver);
		FSLeadSummaryRealEstatePage realestate = new FSLeadSummaryRealEstatePage(driver);
		FSLeadSummaryDocumentsPage doc = new FSLeadSummaryDocumentsPage(driver);
		FSLeadSummaryCompliancePage compl = new FSLeadSummaryCompliancePage(driver);
		FSLeadSummaryPersonalProfilePage profile = new FSLeadSummaryPersonalProfilePage(driver);
		AdminAreaRegionAddAreaRegionPageTest p1 = new AdminAreaRegionAddAreaRegionPageTest();
		FSLeadSummaryPageTest addObj = new FSLeadSummaryPageTest();
		LeadSummaryUI pobj = new LeadSummaryUI(driver);
		PrimaryInfoUI ui = new PrimaryInfoUI(driver);
		TaskTest taskTest = new TaskTest();
		LeadManagementTest lm = new LeadManagementTest(driver);
		Task task = new Task();
		Email email = new Email();
		CallTest calltest = new CallTest();
		SendEmailTest emailTest = new SendEmailTest();
		Sales fsmod = new Sales();
		FranchiseesCommonMethods franchisees = fc.infomgr().franchisees();
		Call call = new Call();
		Sales_Common  salesComm= new  Sales_Common();
		CoApplicantWithoutSeperateLead additionalContact = new CoApplicantWithoutSeperateLead();
		CoApplicantsTest coApplicantTest = new CoApplicantsTest();
		try {
			driver = fc.loginpage().login(driver);

			// add lead
			String firstName = fc.utobj().generateTestData(dataSet1.get("FirstName"));
			String lastName = fc.utobj().generateTestData(dataSet1.get("LastName"));
			String firstName1 = fc.utobj().generateTestData(dataSet1.get("FirstName"));
			String lastName1 = fc.utobj().generateTestData(dataSet1.get("LastName"));
			String franchiseName = fc.utobj().generateTestData(dataSet1.get("franchiseName"));
			String centerName = fc.utobj().generateTestData(dataSet1.get("centerName"));
			String areaRegion = fc.utobj().generateTestData(dataSet1.get("areaRegion"));
			String fddDate = fc.utobj().getFutureDateUSFormat(5);
			String userName = dataSet1.get("userName");
			String leadFullName = firstName + " " + lastName;
			String leadFullName1 = firstName1 + " " + lastName1;
			//areaRegion = "area1";
			areaRegion = p1.addAreaRegion(driver, areaRegion);
			fc.utobj().printTestStep("Add a new  lead");

			Map<String, String> lead = addObj.addLeadSummaryWithLeadNameOwnerName(driver, firstName, lastName, userName);

			fc.utobj().printTestStep("Click Log A Task - Top Link");
			task = fc.sales().sales_common().fillDefaultValue_TaskDetails(task);
			fc.utobj().clickElement(driver, ui.LogaTask_Link);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			taskTest.fillTaskAndClickCreate(driver, task);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text(),'" + "Task Subject" + "')]");

			fc.utobj().printTestStep("Send Email");
			fc.utobj().clickElementByJS(driver, ui.SendEmail_Link);
			email.setSubject(dataSet1.get("Subject"));
			email.setCC(dataSet1.get("CC"));
			email.setBody(dataSet1.get("Body"));
			emailTest.fillEmailDetails_AndClickSendEmail_Bottom_Button(driver, email);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text(),'" + dataSet1.get("Subject") + "')]");

			fc.utobj().printTestStep("Log A call");
			fc.utobj().clickElement(driver, ui.LogaCall_Link);
			call.setSubject(dataSet1.get("CallSubject"));
			call.setCallStatus(dataSet1.get("callStatus"));
			call.setComments(dataSet1.get("comments"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			calltest.fill_And_Add_Call_Details_ScheduleTask_No(driver, call);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text(),'" + dataSet1.get("CallSubject") + "')]");
			
			fc.utobj().printTestStep("Add Co Applicant through co applicant Tab");
			fc.utobj().clickElement(driver, pobj.coApplicantsTab);
			
			additionalContact.setFirstName(dataSet1.get("CoApplicant FirstName"));
			additionalContact.setLastName(dataSet1.get("CoApplicant LName"));
			additionalContact.setCoApplicantRelationship(dataSet1.get("CoSpouse"));
			additionalContact.setPhone(dataSet1.get("CoPhone"));
			additionalContact.setExt(dataSet1.get("CoExt"));
			additionalContact.setFax(dataSet1.get("CoFax"));
			additionalContact.setEmailID(dataSet1.get("CoEmailId"));
			additionalContact.setAddress(dataSet1.get("CoAddress"));
			additionalContact.setCity(dataSet1.get("CoCity"));
			additionalContact.setCountry(dataSet1.get("CoCountry"));
			additionalContact.setStateID(dataSet1.get("CoState"));
			additionalContact.setZip(dataSet1.get("CoZip"));
			
			 coApplicantTest.fillAndSubmitCoApplicantsWithoutSeperateLead(driver,additionalContact);											
			fc.utobj().printTestStep("Add a new coapplicant");
			Map<String, String> lead1 = addObj.addLeadSummaryWithLeadNameOwnerName(driver, firstName1, lastName1, userName);

			fsmod.leadManagement(driver);
			fc.utobj().actionImgOption(driver, leadFullName, "Associate Co-Applicant");
			fc.sales();
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.coApplicantSearch, leadFullName1);

			try {
				fc.utobj().clickEnterOnElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='contentdivTopSearch']/table/tbody/tr[1]/td/a/span/u"));
				Thread.sleep(2000);
			} catch (Exception e) {
				try {
					fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[contains(text(),'" + leadFullName1 + "')]"));
				} catch (Exception e2) {
					// TODO: handle exception
				}
			}
			fc.utobj().selectDropDownByPartialText(driver, pobj.coApplicantRelationshipIDDrp, "Relative");
			fc.utobj().clickElement(driver, pobj.addCboxBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);
			fsmod.leadManagement(driver);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[contains(text(),'" + leadFullName + "')]"));

			fc.utobj().printTestStep("Add Qualification data");
			fc.utobj().clickElement(driver, pobj.qualificationDetailsTab);
			addQualifictaionData(driver, npobj, dataSet1);

			fc.utobj().printTestStep("Add document");
			fc.utobj().clickElement(driver, pobj.documentsTab);
			String docTitle = addDocumentData(driver, doc, dataSet1);

			fc.utobj().printTestStep("Submit personal profile");
			fc.utobj().clickElement(driver, pobj.personalProfileTab);
			addProfileData(driver, profile, dataSet1);

			fc.utobj().printTestStep("Add real estate");
			fc.utobj().clickElement(driver, pobj.realEstate);
			List<String> real = addRealEstateData(driver, realestate, dataSet1);

			fc.utobj().printTestStep("Add compliance");
			fc.utobj().clickElement(driver, pobj.complianceTab);
			List<String> compliance = addComplianceData(driver, compl, dataSet1);

			fsmod.leadManagement(driver);
		
			fc.utobj().printTestStep("Move lead to info mgr from primary info > move to info mgr from summary page");
			fc.utobj().actionImgOption(driver, leadFullName, "Move to Info Mgr");
			fillFranchiseDetails(driver, pobj, dataSet1, areaRegion, franchiseName, leadFullName);
			fillUserDetails(driver, pobj, dataSet1);
			fc.utobj().printTestStep("Search franchise and Click "+franchiseName);
			franchisees.searchFranchiseAndClick(driver, franchiseName);
						
			fc.utobj().clickElement(driver, pobj.infoMgrDocumentsTab);

			fc.utobj().printTestStep("Verify  Personal Profile");
			boolean verifyDocumentPersonalProfile = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text(),'Personal Profile')]");
			
			if (verifyDocumentPersonalProfile == false) {
				fc.utobj().throwsException("Document tab Personal Profile details not visible");
			}
			else
			{
				fc.utobj().clickElement(driver, driver.findElement(By.xpath(".//a[contains(text(),'View Document') and contains(@onclick,'Per')]")));
				boolean isErrorPage = fc.utobj().assertPageSource(driver,
						"Oops! Looks like the page you requested is unavailable");
				if (isErrorPage) {
					fc.utobj().throwsException("Error page is there on Personal Profile. Test case fails !!!");
				} else {
					Reporter.log(" Personal Profile downloaded successfully. Test case passes !!! ");
				}
			}
			fc.utobj().printTestStep("Verify  Qualification Details");
			boolean verifyDocumentQualification = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text(),'" + "Qualification Details" + "')]");
			if (verifyDocumentQualification == false) {
				fc.utobj().throwsException("Qualification details not visible");
			}
			else
			{
				fc.utobj().clickElement(driver, driver.findElement(By.xpath(".//a[contains(text(),'View Document') and contains(@onclick,'Qua')]")));
				boolean isErrorPage = fc.utobj().assertPageSource(driver,
						"Oops! Looks like the page you requested is unavailable");
				if (isErrorPage) {
					fc.utobj().throwsException("Error page is there on Qualification . Test case fails !!!");
				} else {
					Reporter.log("Qualification downloaded successfully. Test case passes !!! ");
				}
			}
			fc.utobj().printTestStep("Verify  Document upload Details");
			boolean verifyDocumentUpload = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text(),'" + docTitle + "')]");
			if (verifyDocumentUpload == false) {
				fc.utobj().throwsException("Document tab document not visible");
			}
			else
			{
				fc.utobj().clickElement(driver, driver.findElement(By.xpath("//a[contains(text(),'View Document') and contains(@onclick,'finan')]")));
				boolean isErrorPage = fc.utobj().assertPageSource(driver,
						"Oops! Looks like the page you requested is unavailable");
				if (isErrorPage) {
					fc.utobj().throwsException("Error page is there on Document tab. Test case fails !!!");
				} else {
					Reporter.log("Document tab downloaded successfully. Test case passes !!! ");
				}
			}
			fc.utobj().printTestStep("Verify  Owners Tab");
			fc.utobj().clickElement(driver, pobj.ownersTab);
			boolean verifyCoAppContact = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + dataSet1.get("CoApplicant FirstName")+" "+ dataSet1.get("CoApplicant LName") + "')]");
			if (verifyCoAppContact == false) {
				fc.utobj().throwsException("Co applicant  contact not verified at Owners tab");
			}
			
			
			
			boolean verifyAdditionalContact = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + leadFullName1 + "')]");
			if (verifyAdditionalContact == false) {
				fc.utobj().throwsException("Additional contact not verified at Owners tab");
			}
			fc.utobj().printTestStep("Verify  RealEstate  Tab");
			fc.utobj().clickElement(driver, pobj.realEstateTab);
			for (String temp : real) {
				boolean verifyRealState = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + temp + "')]");
				if (verifyRealState == false) {

					fc.utobj().throwsException("Real State data not verified at real state tab " + temp);
				}
			}

			fc.utobj().printTestStep("Verify Contract Signing Tab");
			fc.utobj().clickElement(driver, pobj.contractSigning);
			for (String temp : compliance) {
				System.out.println(temp);
				boolean verifyContratSingning1 = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + temp + "')]");
                     
				if (verifyContratSingning1 == false) {
					fc.utobj().throwsException("compliance data not verified at contract singning tab " + temp);
				}
			}
					
			fc.utobj().printTestStep("Verify Contact History Tab for Email ,Task, Call");
			fc.utobj().clickElement(driver, pobj.contactHistory);

			try {
				fc.utobj().clickPartialLinkText(driver, email.getSubject());
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				
				boolean Val = fc.utobj().verifyElementOnVisible_ByXpath(driver,  ".//*[contains(text(),'" + email.getBody() + "')]");
				if (Val == false) {

					fc.utobj().throwsException("Email Body   not verified at Contact History tab " + email.getBody());
				}
				fc.commonMethods().Click_Close_Input_ByValue(driver);
				fc.utobj().switchFrameToDefault(driver);
				fc.utobj().printTestStep("Verified Email in Contact History");
				
			} catch (Exception e) {
				fc.utobj().throwsException("Email  not verified at Contact History tab " + email.getSubject());
			}

			try {

				fc.utobj().clickPartialLinkText(driver, "FIM Call validation");
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				boolean Val = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + call.getCallStatus() + "')]");
				if (Val == false) {

					fc.utobj().throwsException("Call  not verified at Contact History tab " + call.getComments());
				}
				fc.commonMethods().Click_Close_Input_ByValue(driver);
				fc.utobj().switchFrameToDefault(driver);
				fc.utobj().printTestStep("Verified Call in Contact History");

			} catch (Exception e) {
				fc.utobj().throwsException("Call  not verified at Contact History tab ");
			}
			try {
				fc.utobj().clickPartialLinkText(driver, "Task Subject");
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				boolean Val = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + "fimfran" + "')]");
				if (Val == false) {

					fc.utobj().throwsException("Task  not verified at Contact History tab "  );
				}
				fc.commonMethods().Click_Close_Input_ByValue(driver);
				fc.utobj().switchFrameToDefault(driver);
				fc.utobj().printTestStep("Verified Task in Contact History");
			} catch (Exception e) {
				fc.utobj().throwsException("Task Verification Failed in contact History");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
		
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2017-10-13", updatedOn = "2017-10-13", testCaseId = "TC_Move_To_Opener_001", testCaseDescription = "Verify Move To Opener with document and check document checklist", reference = { "" })
	public void InfoMgr_Franchisee_MoveToOpener() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		String testCaseIdDb = "TC_Move_To_InfoMgr_001";

		Map<String, String> dataSet1 = fc.utobj().readTestDatawithsqllite("infomgr", testCaseIdDb, "Lead");

		FSSearchPageTest fssearch= new FSSearchPageTest();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		FSLeadSummaryQualificationDetailsPage npobj = new FSLeadSummaryQualificationDetailsPage(driver);
		FSLeadSummaryRealEstatePage realestate = new FSLeadSummaryRealEstatePage(driver);
		FSLeadSummaryDocumentsPage doc = new FSLeadSummaryDocumentsPage(driver);
		FSLeadSummaryCompliancePage compl = new FSLeadSummaryCompliancePage(driver);
		FSLeadSummaryPersonalProfilePage profile = new FSLeadSummaryPersonalProfilePage(driver);
		AdminAreaRegionAddAreaRegionPageTest p1 = new AdminAreaRegionAddAreaRegionPageTest();
		FSLeadSummaryPageTest addObj = new FSLeadSummaryPageTest();
		LeadSummaryUI pobj = new LeadSummaryUI(driver);
		PrimaryInfoUI ui = new PrimaryInfoUI(driver);
		TaskTest taskTest = new TaskTest();
		LeadManagementTest lm = new LeadManagementTest(driver);
		Task task = new Task();
		Email email = new Email();
		CallTest calltest = new CallTest();
		SendEmailTest emailTest = new SendEmailTest();
		Sales fsmod = new Sales();
		InDevelopmentCommonMethods franchisees = fc.infomgr().inDevelopment();
		Call call = new Call();
		Sales_Common  salesComm= new  Sales_Common();
		CoApplicantWithoutSeperateLead additionalContact = new CoApplicantWithoutSeperateLead();
		CoApplicantsTest coApplicantTest = new CoApplicantsTest();

		try {
			driver = fc.loginpage().login(driver);
			
			// add lead
			String firstName = fc.utobj().generateTestData(dataSet1.get("FirstName"));
			String lastName = fc.utobj().generateTestData(dataSet1.get("LastName"));
			String firstName1 = fc.utobj().generateTestData(dataSet1.get("FirstName"));
			String lastName1 = fc.utobj().generateTestData(dataSet1.get("LastName"));
			String franchiseName = fc.utobj().generateTestData(dataSet1.get("franchiseName"));
			String areaRegion = fc.utobj().generateTestData(dataSet1.get("areaRegion"));
			String userName = dataSet1.get("userName");
			String leadFullName = firstName + " " + lastName;
			String leadFullName1 = firstName1 + " " + lastName1;
			
			areaRegion = p1.addAreaRegion(driver, areaRegion);
			fc.utobj().printTestStep("Add a new  lead");

			Map<String, String> lead = addObj.addLeadSummaryWithLeadNameOwnerName(driver, firstName, lastName, userName);

			fc.utobj().printTestStep("Click Log A Task - Top Link");
			task = fc.sales().sales_common().fillDefaultValue_TaskDetails(task);
			fc.utobj().clickElement(driver, ui.LogaTask_Link);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			taskTest.fillTaskAndClickCreate(driver, task);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text(),'" + "Task Subject" + "')]");

			fc.utobj().printTestStep("Send Email");
			fc.utobj().clickElementByJS(driver, ui.SendEmail_Link);
			email.setSubject(dataSet1.get("Subject"));
			email.setCC(dataSet1.get("CC"));
			email.setBody(dataSet1.get("Body"));
			
			emailTest.fillEmailDetails_AndClickSendEmail_Bottom_Button(driver, email);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text(),'" + dataSet1.get("Subject") + "')]");

			fc.utobj().printTestStep("Log A call");
			fc.utobj().clickElement(driver, ui.LogaCall_Link);
			call.setSubject(dataSet1.get("CallSubject"));
			call.setCallStatus(dataSet1.get("callStatus"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			calltest.fill_And_Add_Call_Details_ScheduleTask_No(driver, call);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text(),'" + dataSet1.get("CallSubject") + "')]");
			
			fc.utobj().printTestStep("Add Co Applicant through co applicant Tab");
			fc.utobj().clickElement(driver, pobj.coApplicantsTab);
			
			additionalContact.setFirstName(dataSet1.get("CoApplicant FirstName"));
			additionalContact.setLastName(dataSet1.get("CoApplicant LName"));
			additionalContact.setCoApplicantRelationship(dataSet1.get("CoSpouse"));
			additionalContact.setPhone(dataSet1.get("CoPhone"));
			additionalContact.setExt(dataSet1.get("CoExt"));
			additionalContact.setFax(dataSet1.get("CoFax"));
			additionalContact.setEmailID(dataSet1.get("CoEmailId"));
			additionalContact.setAddress(dataSet1.get("CoAddress"));
			additionalContact.setCity(dataSet1.get("CoCity"));
			additionalContact.setCountry(dataSet1.get("CoCountry"));
			additionalContact.setStateID(dataSet1.get("CoState"));
			additionalContact.setZip(dataSet1.get("CoZip"));
			
			 coApplicantTest.fillAndSubmitCoApplicantsWithoutSeperateLead(driver,additionalContact);
			
			
			 
										
			fc.utobj().printTestStep("Add a new coapplicant");
			Map<String, String> lead1 = addObj.addLeadSummaryWithLeadNameOwnerName(driver, firstName1, lastName1, userName);

			fsmod.leadManagement(driver);			
			fc.utobj().actionImgOption(driver, leadFullName, "Associate Co-Applicant");
			fc.sales();
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.coApplicantSearch, leadFullName1);

			try {
				fc.utobj().clickEnterOnElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='contentdivTopSearch']/table/tbody/tr[1]/td/a/span/u"));
				Thread.sleep(2000);
			} catch (Exception e) {
				try {
					fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[contains(text(),'" + leadFullName1 + "')]"));
				} catch (Exception e2) {
					// TODO: handle exception
				}
			}
			fc.utobj().selectDropDownByPartialText(driver, pobj.coApplicantRelationshipIDDrp, "Relative");
			fc.utobj().clickElement(driver, pobj.addCboxBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);
			fsmod.leadManagement(driver);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[contains(text(),'" + leadFullName + "')]"));

			fc.utobj().printTestStep("Add Qualification data");
			fc.utobj().clickElement(driver, pobj.qualificationDetailsTab);
			addQualifictaionData(driver, npobj, dataSet1);

			fc.utobj().printTestStep("Add document");
			fc.utobj().clickElement(driver, pobj.documentsTab);
			String docTitle = addDocumentData(driver, doc, dataSet1);

			fc.utobj().printTestStep("Submit personal profile");
			fc.utobj().clickElement(driver, pobj.personalProfileTab);
			addProfileData(driver, profile, dataSet1);

			fc.utobj().printTestStep("Add real estate");
			fc.utobj().clickElement(driver, pobj.realEstate);
			List<String> real = addRealEstateData(driver, realestate, dataSet1);

			fc.utobj().printTestStep("Add compliance");
			fc.utobj().clickElement(driver, pobj.complianceTab);
			List<String> compliance = addComplianceData(driver, compl, dataSet1);

			fsmod.leadManagement(driver);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[contains(text(),'" + leadFullName + "')]"));
			
			fc.utobj().printTestStep("Move Lead to Opener");
			
		
			fc.utobj().printTestStep("Go to Primary Info ");
			fc.utobj().clickElement(driver, pobj.leadPrimaryInfo);
			fc.utobj().printTestStep("Click on move to Opener from Botton Button");
			fc.utobj().clickElement(driver, pobj.moveToOpener);
			
			fillFranchiseDetailsOpener(driver, pobj, dataSet1, areaRegion, franchiseName, leadFullName);
			fillUserDetails(driver, pobj, dataSet1);
			fc.utobj().printTestStep("Search franchise and Click "+franchiseName);
			franchisees.searchFranchiseAndClick(driver, franchiseName);
						
			fc.utobj().clickElement(driver, pobj.infoMgrDocumentsTab);

			fc.utobj().printTestStep("Verify  Personal Profile");
			boolean verifyDocumentPersonalProfile = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text(),'Personal Profile')]");
			
			if (verifyDocumentPersonalProfile == false) {
				fc.utobj().throwsException("Document tab Personal Profile details not visible");
			}
			else
			{
				fc.utobj().clickElement(driver, driver.findElement(By.xpath(".//a[contains(text(),'View Document') and contains(@onclick,'Per')]")));
				boolean isErrorPage = fc.utobj().assertPageSource(driver,
						"Oops! Looks like the page you requested is unavailable");
				if (isErrorPage) {
					fc.utobj().throwsException("Error page is there on Personal Profile. Test case fails !!!");
				} else {
					Reporter.log(" Personal Profile downloaded successfully. Test case passes !!! ");
				}
			}
			fc.utobj().printTestStep("Verify  Qualification Details");
			boolean verifyDocumentQualification = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text(),'" + "Qualification Details" + "')]");
			if (verifyDocumentQualification == false) {
				fc.utobj().throwsException("Qualification details not visible");
			}
			else
			{
				fc.utobj().clickElement(driver, driver.findElement(By.xpath(".//a[contains(text(),'View Document') and contains(@onclick,'Qua')]")));
				boolean isErrorPage = fc.utobj().assertPageSource(driver,
						"Oops! Looks like the page you requested is unavailable");
				if (isErrorPage) {
					fc.utobj().throwsException("Error page is there on Qualification . Test case fails !!!");
				} else {
					Reporter.log("Qualification downloaded successfully. Test case passes !!! ");
				}
			}
			fc.utobj().printTestStep("Verify  Document upload Details");
			boolean verifyDocumentUpload = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text(),'" + docTitle + "')]");
			if (verifyDocumentUpload == false) {
				fc.utobj().throwsException("Document tab document not visible");
			}
			else
			{
				fc.utobj().clickElement(driver, driver.findElement(By.xpath("//a[contains(text(),'View Document') and contains(@onclick,'finan')]")));
				boolean isErrorPage = fc.utobj().assertPageSource(driver,
						"Oops! Looks like the page you requested is unavailable");
				if (isErrorPage) {
					fc.utobj().throwsException("Error page is there on Document tab. Test case fails !!!");
				} else {
					Reporter.log("Document tab downloaded successfully. Test case passes !!! ");
				}
			}
			fc.utobj().printTestStep("Verify  Owners Tab");
			fc.utobj().clickElement(driver, pobj.ownersTab);
			boolean verifyCoAppContact = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + dataSet1.get("CoApplicant FirstName")+" "+ dataSet1.get("CoApplicant LName") + "')]");
			if (verifyCoAppContact == false) {
				fc.utobj().throwsException("Co applicant  contact not verified at Owners tab");
			}
			
			
			
			boolean verifyAdditionalContact = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + leadFullName1 + "')]");
			if (verifyAdditionalContact == false) {
				fc.utobj().throwsException("Additional contact not verified at Owners tab");
			}
			fc.utobj().printTestStep("Verify  RealEstate  Tab");
			fc.utobj().clickElement(driver, pobj.realEstateTab);
			for (String temp : real) {
				boolean verifyRealState = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + temp + "')]");
				if (verifyRealState == false) {

					fc.utobj().throwsException("Real State data not verified at real state tab " + temp);
				}
			}

			fc.utobj().printTestStep("Verify Contract Signing Tab");
			fc.utobj().clickElement(driver, pobj.contractSigning);
			for (String temp : compliance) {
				System.out.println(temp);
				boolean verifyContratSingning1 = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + temp + "')]");
                     
				if (verifyContratSingning1 == false) {
					fc.utobj().throwsException("compliance data not verified at contract singning tab " + temp);
				}
			}
					
			fc.utobj().printTestStep("Verify Contact History Tab for Email ,Task, Call");
			fc.utobj().clickElement(driver, pobj.contactHistory);

			try {
				fc.utobj().clickPartialLinkText(driver, email.getSubject());
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				
				boolean Val = fc.utobj().verifyElementOnVisible_ByXpath(driver,  ".//*[contains(text(),'" + email.getBody() + "')]");
				if (Val == false) {

					fc.utobj().throwsException("Email Body   not verified at Contact History tab " + email.getBody());
				}
				fc.commonMethods().Click_Close_Input_ByValue(driver);
				fc.utobj().switchFrameToDefault(driver);
				fc.utobj().printTestStep("Verified Email in Contact History");
				
			} catch (Exception e) {
				fc.utobj().throwsException("Email  not verified at Contact History tab " + email.getSubject());
			}

			try {

				fc.utobj().clickPartialLinkText(driver, "FIM Call validation");
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				boolean Val = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + call.getCallStatus() + "')]");
				if (Val == false) {

					fc.utobj().throwsException("Call  not verified at Contact History tab " + call.getComments());
				}
				fc.commonMethods().Click_Close_Input_ByValue(driver);
				fc.utobj().switchFrameToDefault(driver);
				fc.utobj().printTestStep("Verified Call in Contact History");

			} catch (Exception e) {
				fc.utobj().throwsException("Call  not verified at Contact History tab ");
			}
			try {
				fc.utobj().clickPartialLinkText(driver, "Task Subject");
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				boolean Val = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + "fimfran" + "')]");
				if (Val == false) {

					fc.utobj().throwsException("Task  not verified at Contact History tab , Franchise Id not found "  );
				}
				fc.commonMethods().Click_Close_Input_ByValue(driver);
				fc.utobj().switchFrameToDefault(driver);
				fc.utobj().printTestStep("Verified Task in Contact History");
			} catch (Exception e) {
				fc.utobj().throwsException("Task Verification Failed in Info Mgr contact History Tab");
			}
 
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}

	public List<String> addRealEstateData(WebDriver driver, FSLeadSummaryRealEstatePage realestate, Map<String, String> dataSet) throws Exception {
		// Real estate
		List<String> list = new ArrayList<String>();
		try {

			fc.utobj().sendKeys(driver, realestate.siteAddress1, dataSet.get("siteAddress1"));
			list.add(dataSet.get("siteAddress1"));
			fc.utobj().sendKeys(driver, realestate.siteAddress2, dataSet.get("siteAddress2"));
			list.add(dataSet.get("siteAddress2"));
			fc.utobj().sendKeys(driver, realestate.siteCity, dataSet.get("siteCity"));
			list.add(dataSet.get("siteCity"));
			fc.utobj().selectDropDownByVisibleText(driver, realestate.siteCountry, dataSet.get("siteCountry"));
			list.add(dataSet.get("siteCountry"));
			fc.utobj().selectDropDownByVisibleText(driver, realestate.siteState, dataSet.get("siteState"));
			list.add(dataSet.get("siteState"));
			fc.utobj().sendKeys(driver, realestate.buildingSize, dataSet.get("buildingSize"));
			list.add(dataSet.get("buildingSize"));
			fc.utobj().sendKeys(driver, realestate.buildingDimentionsX, dataSet.get("buildingDimentionsX"));
			list.add(dataSet.get("buildingDimentionsX"));
			fc.utobj().sendKeys(driver, realestate.buildingDimentionsY, dataSet.get("buildingDimentionsY"));
			list.add(dataSet.get("buildingDimentionsY"));
			fc.utobj().sendKeys(driver, realestate.buildingDimentionsZ, dataSet.get("buildingDimentionsZ"));
			list.add(dataSet.get("buildingDimentionsZ"));
			fc.utobj().sendKeys(driver, realestate.parkingSpaces, dataSet.get("parkingSpaces"));
			list.add(dataSet.get("parkingSpaces"));
			fc.utobj().sendKeys(driver, realestate.dealType, dataSet.get("dealType"));
			list.add(dataSet.get("dealType"));
			fc.utobj().sendKeys(driver, realestate.loiSent, dataSet.get("loiSent"));
			list.add(dataSet.get("loiSent"));
			fc.utobj().sendKeys(driver, realestate.loiSigned, dataSet.get("loiSigned"));
			list.add(dataSet.get("loiSigned"));

			fc.utobj().sendKeys(driver, realestate.approvalDate, dataSet.get("approvalDate"));
			list.add(dataSet.get("approvalDate"));
			fc.utobj().sendKeys(driver, realestate.leaseCommencement, dataSet.get("leaseCommencement"));
			list.add(dataSet.get("leaseCommencement"));
			fc.utobj().sendKeys(driver, realestate.leaseExpiration, dataSet.get("leaseExpiration"));
			list.add(dataSet.get("leaseExpiration"));
			fc.utobj().sendKeys(driver, realestate.initialTerm, dataSet.get("initialTerm"));
			list.add(dataSet.get("initialTerm"));
			fc.utobj().sendKeys(driver, realestate.optionTerm, dataSet.get("optionTerm"));
			list.add(dataSet.get("optionTerm"));

			fc.utobj().clickRadioButton(driver, realestate.purchaseOption, dataSet.get("purchaseOption"));
			list.add(dataSet.get("purchaseOption"));
			fc.utobj().sendKeys(driver, realestate.projectedOpeningDate, dataSet.get("projectedOpeningDate"));
			list.add(dataSet.get("projectedOpeningDate"));
			fc.utobj().clickRadioButton(driver, realestate.generalContractorSelector, dataSet.get("generalContractorSelector"));
			list.add(dataSet.get("generalContractorSelector"));
			fc.utobj().sendKeys(driver, realestate.nameGeneralContractor, dataSet.get("nameGeneralContractor"));
			list.add(dataSet.get("nameGeneralContractor"));
			fc.utobj().sendKeys(driver, realestate.addressGeneralContractor, dataSet.get("addressGeneralContractor"));
			list.add(dataSet.get("addressGeneralContractor"));
			fc.utobj().sendKeys(driver, realestate.permitApplied, dataSet.get("permitApplied"));
			list.add(dataSet.get("permitApplied"));
			fc.utobj().sendKeys(driver, realestate.permitIssued, dataSet.get("permitIssued"));
			list.add(dataSet.get("permitIssued"));
			fc.utobj().sendKeys(driver, realestate.certificate, dataSet.get("certificate"));
			list.add(dataSet.get("certificate"));
			fc.utobj().sendKeys(driver, realestate.turnOverDate, dataSet.get("turnOverDate"));
			list.add(dataSet.get("turnOverDate"));
			String grandOpeningDates = fc.utobj().getFutureDateUSFormat(5);
			list.add(grandOpeningDates);
			fc.utobj().sendKeys(driver, realestate.grandOpeningDate, grandOpeningDates);

			fc.utobj().clickElement(driver, realestate.saveBtn);
		} catch (Exception e) {
			
			fc.utobj().throwsException("Unable to add data in real Estate");
		}
		return list;
	}

	public List<String> addComplianceData(WebDriver driver, FSLeadSummaryCompliancePage compl, Map<String, String> dataSet) throws Exception {
		List<String> list = new ArrayList<String>();
		try {

			fc.utobj().sendKeys(driver, compl.fddDate, dataSet.get("fddDate"));
			list.add(dataSet.get("fddDate"));
			fc.utobj().sendKeys(driver, compl.recByFrancDate1, dataSet.get("recByFrancDate1"));
			list.add(dataSet.get("recByFrancDate1"));
			fc.utobj().sendKeys(driver, compl.bussDayExp10Date, dataSet.get("bussDayExp10Date"));
			list.add(dataSet.get("bussDayExp10Date"));
			fc.utobj().sendKeys(driver, compl.versionOfUfoc, dataSet.get("versionOfUfoc"));
			list.add(dataSet.get("versionOfUfoc"));
			fc.utobj().sendKeys(driver, compl.ipAddress, dataSet.get("ipAddress"));
			list.add(dataSet.get("ipAddress"));
			fc.utobj().sendKeys(driver, compl.browserType, dataSet.get("browserType"));
			list.add(dataSet.get("browserType"));
			fc.utobj().sendKeys(driver, compl.firstFrancPaymentDate, dataSet.get("firstFrancPaymentDate"));
			list.add(dataSet.get("firstFrancPaymentDate"));
			fc.utobj().clickRadioButton(driver, compl.stateRegReq, dataSet.get("stateRegReq"));
			list.add(dataSet.get("stateRegReq"));
			fc.utobj().sendKeys(driver, compl.secondFrancPaymentDate, dataSet.get("secondFrancPaymentDate"));
			list.add(dataSet.get("secondFrancPaymentDate"));
			// fc.utobj().clickRadioButton(driver, compl.stateAddendumReq,
			// dataSet.get("permitApplied"));
			fc.utobj().clickRadioButton(driver, compl.francCommiteeApprovalYes, dataSet.get("francCommiteeApprovalYes"));
			list.add(dataSet.get("francCommiteeApprovalYes"));
			fc.utobj().sendKeys(driver, compl.faRequestedDate, dataSet.get("faRequestedDate"));

			fc.utobj().sendKeys(driver, compl.faReceivedDate, dataSet.get("faReceivedDate"));

			fc.utobj().sendKeys(driver, compl.francRecAgrDate, dataSet.get("francRecAgrDate"));

			fc.utobj().sendKeys(driver, compl.bussDay5ExpDate, dataSet.get("bussDay5ExpDate"));

			fc.utobj().sendKeys(driver, compl.francSignAgrDate, dataSet.get("francSignAgrDate"));

			fc.utobj().sendKeys(driver, compl.versionFrancAgr, dataSet.get("versionFrancAgr"));

			fc.utobj().sendKeys(driver, compl.franFeeAmt, dataSet.get("franFeeAmt"));

			fc.utobj().sendKeys(driver, compl.franFeeDate, dataSet.get("franFeeDate"));

			fc.utobj().sendKeys(driver, compl.areaFeeAmt, dataSet.get("areaFeeAmt"));

			fc.utobj().sendKeys(driver, compl.areaFeeDate, dataSet.get("areaFeeDate"));

			fc.utobj().sendKeys(driver, compl.adaExecutionDate, dataSet.get("adaExecutionDate"));

			fc.utobj().sendKeys(driver, compl.faExecutionDate, dataSet.get("faExecutionDate"));

			fc.utobj().clickRadioButton(driver, compl.contractRecSign, dataSet.get("contractRecSign"));
			fc.utobj().clickRadioButton(driver, compl.licAgrProperlySign, dataSet.get("licAgrProperlySign"));
			fc.utobj().clickRadioButton(driver, compl.perCovenantAgrProperlySign, dataSet.get("perCovenantAgrProperlySign"));
			fc.utobj().clickRadioButton(driver, compl.guaranteeProperlySign, dataSet.get("guaranteeProperlySign"));
			fc.utobj().clickRadioButton(driver, compl.stateReqAddendumProperlySign, dataSet.get("stateReqAddendumProperlySign"));
			fc.utobj().clickRadioButton(driver, compl.otherAttendaProperlySign, dataSet.get("otherAttendaProperlySign"));
			fc.utobj().clickRadioButton(driver, compl.leaseRiderProperlySign, dataSet.get("leaseRiderProperlySign"));
			fc.utobj().clickRadioButton(driver, compl.promNotePropSign, dataSet.get("promNotePropSign"));
			fc.utobj().clickRadioButton(driver, compl.otherDocProperlySign, dataSet.get("otherDocProperlySign"));
			fc.utobj().clickRadioButton(driver, compl.handWrittenChanges, dataSet.get("handWrittenChanges"));
			fc.utobj().clickRadioButton(driver, compl.proofControlOverRealEstate, dataSet.get("proofControlOverRealEstate"));
			fc.utobj().clickRadioButton(driver, compl.ufocRecProperlySign, dataSet.get("ufocRecProperlySign"));
			fc.utobj().clickElementByJS(driver, compl.saveBtn);
		} catch (Exception e) {
			fc.utobj().throwsException("Not able to add Compliance data tab  ");
		}
		return list;
	}

	public String addDocumentData(WebDriver driver, FSLeadSummaryDocumentsPage doc, Map<String, String> dataSet) throws Exception {
		try {
			fc.utobj().sendKeys(driver, doc.docTitleText, dataSet.get("docTitleText"));
			fc.utobj().sendKeys(driver, doc.uploadDoc, dataSet.get("uploadDoc"));
			fc.utobj().clickElementByJS(driver, doc.saveBtn);
		} catch (Exception e) {
			fc.utobj().throwsException("Not able to add Document data ");
		}
		return dataSet.get("docTitleText");
	}

	public void addQualifictaionData(WebDriver driver, FSLeadSummaryQualificationDetailsPage qual, Map<String, String> dataSet) throws Exception {
		try {
			fc.utobj().clickElement(driver, qual.gender_male);
			fc.utobj().sendKeys(driver, qual.homeCity, dataSet.get("homeCity"));
			fc.utobj().selectDropDownByVisibleText(driver, qual.homeCountry, dataSet.get("homeCountry"));
			fc.utobj().selectDropDownByVisibleText(driver, qual.homeState, dataSet.get("homeState"));
			fc.utobj().sendKeys(driver, qual.Email, dataSet.get("Email"));
			// fc.utobj().clickElement(driver, qual.homeOwnership_own);
			// fc.utobj().clickElement(driver, qual.maritalStatus_married);
			// fc.utobj().sendKeys(driver, qual.spouseName,
			// dataSet.get("spouseName"));
			fc.utobj().clickElement(driver, qual.saveButton);
		} catch (Exception e) {
			fc.utobj().throwsException("Not able to add Qualification data ");
		}
	}

	public void addProfileData(WebDriver driver, FSLeadSummaryPersonalProfilePage profile, Map<String, String> dataSet) throws Exception {
		try {

			fc.utobj().sendKeys(driver, profile.homeCity, dataSet.get("homeCity"));
			fc.utobj().selectDropDownByVisibleText(driver, profile.homeCountry, dataSet.get("homeCountry"));
			fc.utobj().selectDropDownByVisibleText(driver, profile.homeState, dataSet.get("homeState"));
			fc.utobj().clickElementByJS(driver, profile.saveBtn);
		} catch (Exception e) {
			fc.utobj().throwsException("Not able to add Profile data  ");
		}
	}

	public void fillFranchiseDetails(WebDriver driver, LeadSummaryUI pobj , Map<String, String> dataSet ,String areaRegion ,String franchiseName, String leadFullName) throws Exception {
		try {
			
			
			// Move to INfoMgr
						
						fc.utobj().sendKeys(driver, pobj.franchiseeName, franchiseName);
						fc.utobj().sendKeys(driver, pobj.centerName, dataSet.get("centerName"));
						fc.utobj().selectDropDownByPartialText(driver, pobj.areaRegion, areaRegion);
						fc.utobj().selectDropDownByPartialText(driver, pobj.storeTypeId, "default");
						fc.utobj().sendKeys(driver, pobj.openingDate, dataSet.get("fddDate"));
						fc.utobj().selectDropDownByVisibleText(driver, pobj.countryID, dataSet.get("siteCountry"));
						fc.utobj().selectDropDownByVisibleText(driver, pobj.state, dataSet.get("siteState"));
						fc.utobj().sendKeys(driver, pobj.InfoMgrcity, dataSet.get("siteCity"));
						fc.utobj().sendKeys(driver, pobj.storePhone, dataSet.get("storePhone"));
						fc.utobj().clickElement(driver, pobj.moveDocuments);
						fc.utobj().clickElement(driver, pobj.addBtn);
						fc.utobj().printTestStep("Submit Location Details > Save");
						
		} catch (Exception e) {
			fc.utobj().throwsException("Error in moving Lead to InfoMgr  ");
		}
	}
	public void fillFranchiseDetailsOpener(WebDriver driver, LeadSummaryUI pobj , Map<String, String> dataSet ,String areaRegion ,String franchiseName, String leadFullName) throws Exception {
		try {
			
			
			// Move to INfoMgr
						
						fc.utobj().sendKeys(driver, pobj.franchiseeName, franchiseName);
						fc.utobj().sendKeys(driver, pobj.centerName, dataSet.get("centerName"));
						fc.utobj().selectDropDownByPartialText(driver, pobj.areaRegion, areaRegion);
						fc.utobj().selectDropDownByPartialText(driver, pobj.storeTypeId, "default");
						fc.utobj().sendKeys(driver, pobj.OpeningDate, dataSet.get("fddDate"));						
						fc.utobj().selectDropDownByVisibleText(driver, pobj.countryID, dataSet.get("siteCountry"));
						fc.utobj().selectDropDownByVisibleText(driver, pobj.state, dataSet.get("siteState"));
						fc.utobj().sendKeys(driver, pobj.InfoMgrcity, dataSet.get("siteCity"));
						fc.utobj().sendKeys(driver, pobj.storePhone, dataSet.get("storePhone"));				
						fc.utobj().clickElement(driver, pobj.addBtn);
						fc.utobj().printTestStep("Submit Location Details > Save");
						
		} catch (Exception e) {
			fc.utobj().throwsException("Error in fillFranchiseDetailsOpener");
		}
	}
	public void fillUserDetails(WebDriver driver, LeadSummaryUI pobj , Map<String, String> dataSet ) throws Exception {
		try {
			String loginId = fc.utobj().generateTestData(dataSet.get("loginId"));
			AdminFranchiseLocationManageFranchiseLocationsAddUserPage pobj2 = new AdminFranchiseLocationManageFranchiseLocationsAddUserPage(driver);
			fc.utobj().sendKeys(driver, pobj2.loginId, loginId);
			fc.utobj().sendKeys(driver, pobj2.password, dataSet.get("password"));
			fc.utobj().sendKeys(driver, pobj2.confirmPassword, dataSet.get("confirmPassword"));
			fc.utobj().selectValFromMultiSelect(driver, pobj2.rolesId, dataSet.get("rolesId"));
			fc.utobj().sendKeys(driver, pobj2.email, dataSet.get("Email"));
			fc.utobj().clickElement(driver, pobj2.submit);
		
	} catch (Exception e) {
		fc.utobj().throwsException("Error in fillUserDetails  ");
	}
		}
}
