package com.builds.test.infomgr;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.uimaps.infomgr.InfoMgrFranchiseesPage;
import com.builds.uimaps.infomgr.InfoMgrMultiUnitEntityPage;
import com.builds.uimaps.infomgr.InfoMgrMultiUnitInfoPage;
import com.builds.uimaps.infomgr.InfoMgrOwnersPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class InfoMgrFranchiseesOwnersPageTest {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_InfoMgr_Franchisees_Owners_AddRemark", testCaseDescription = "This test case will verify add remark functionality from owners page.", reference = {
			"" })
	public void addRemarksOwnersPage() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		FranchiseesCommonMethods franchise = fc.infomgr().franchisees();
		AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();

		try {
			driver = fc.loginpage().login(driver);
			InfoMgrOwnersPage objFranOwnersPage = new InfoMgrFranchiseesPage(driver).getOwnersPage();

			// Add Franchise Location
			fc.utobj().printTestStep("Add Location");
			String franchiseID = adminInfoMgr.addFranchiseLocation(driver);

			// Search Franchisee Location and click
			fc.utobj().printTestStep("Search franchisee and click");
			franchise.searchFranchiseAndClick(driver, franchiseID);

			// Add owners
			fc.utobj().printTestStep("Add owners");
			InfoMgrMultiUnitInfoPage entityInfoPage = new InfoMgrMultiUnitEntityPage(driver).getMultiUnitInfoPage();
			fc.utobj().clickElement(driver, entityInfoPage.tabOwners);
			Map<String, String> lstOwnerDetails = franchise.addOwner(driver);
			String ownerName = lstOwnerDetails.get("FirstName");

			// Choose Add Remark from Action menu and add remark
			fc.utobj().printTestStep("Add remark");
			fc.utobj().actionImgOption(driver, ownerName, "Add Remarks");
			franchise.addRemark(driver, objFranOwnersPage);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception ex) {

			fc.utobj().quitBrowserOnCatch(driver, ex, testCaseId);
		}
	}

}
