package com.builds.test.infomgr;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.builds.test.common.Location;
import com.builds.uimaps.admin.AdminConfigurationPIIConfigurationPage;
import com.builds.uimaps.fs.AdminFranchiseSalesManageFormGeneratorPage;
import com.builds.uimaps.infomgr.InfoMgrFranchiseesPage;
import com.builds.uimaps.infomgr.InfoMgrOwnersPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class InfoMgrFranchiseesPIILevelTest {

	FranconnectUtil fc = new FranconnectUtil();
	InfoMgr_Common imc = new InfoMgr_Common();

	@Test(groups = { "infomgr" , "IMFailedTC","IM_PII" })

	@TestCase(createdOn = "2017-10-11", updatedOn = "2018-06-22", testCaseId = "TC_InfoMgr_PII_Owner_TaxpayerID_locked", testCaseDescription = "Test to verify Owner Taxpayer ID displying locked or not.", reference = {
			"" })
	public void InfoMgr_Franchisee_PII_OwnerTaxDisplay() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		FranchiseesCommonMethods franchisees = fc.infomgr().franchisees();

		AdminInfoMgrCommonMethods adminInfoMgr = new AdminInfoMgrCommonMethods();

		try {
			fc.loginpage().login(driver);

			adminInfoMgr.EnablePIIunderAdmin(driver);

			InfoMgrFranchiseeFilter franchisee = adminInfoMgr.addFranchiseLocationForFilter(driver, false);
			franchisees.franchiseesPageShowAll(driver);

			fc.utobj().actionImgOption(driver, franchisee.getFranchiseeID(), "Modify");

			fc.utobj().printTestStep("Adding  Taxpayer ID details by modifying the owner");
			InfoMgrOwnersPage ownerPage = new InfoMgrFranchiseesPage(driver).getOwnersPage();
			fc.utobj().clickElement(driver, ownerPage.ownersTab);
			fc.utobj().actionImgOption(driver, franchisee.getOwnerFirstName(), "Modify");
			
			fc.utobj().sendKeys(driver,driver.findElement(By.id("taxpayerID")), "PII_Taxpayer_ID");
			fc.utobj().clickElement(driver, driver.findElement(By.name("Submit")));

			fc.utobj().printTestStep("Verify Owner Taxpayer ID");
			fc.utobj().clickElement(driver, ownerPage.ownersTab);
			fc.utobj().actionImgOption(driver, franchisee.getOwnerFirstName(), "View Details");

			boolean isTaxperID = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text(),'Taxpayer ID')]/following-sibling::td/div/a/img");

			if (isTaxperID) {
				Reporter.log("Taxpayer ID is working as expected displying locked.");
			} else {
				fc.utobj().throwsException("Taxpayer ID is not work as expected or displying unlocked !!!");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr","IM_PII" })

	@TestCase(createdOn = "2017-10-11", updatedOn = "2018-06-25", testCaseId = "TC_InfoMgr_PII_Owner_TaxpayerID", testCaseDescription = "Test to verify Owner Taxpayer ID in Owner detail page.", reference = {
			"" })
	public void InfoMgr_Franchisee_PII_Verify_OwnerTaxPayer() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		FranchiseesCommonMethods franchisees = fc.infomgr().franchisees();

		AdminInfoMgrCommonMethods adminInfoMgr = new AdminInfoMgrCommonMethods();

		try {
			fc.loginpage().login(driver);

			adminInfoMgr.EnablePIIunderAdmin(driver);

			InfoMgrFranchiseeFilter franchisee = adminInfoMgr.addFranchiseLocationForFilter(driver, false);
			franchisees.franchiseesPageShowAll(driver);

			fc.utobj().actionImgOption(driver, franchisee.getFranchiseeID(), "Modify");

			fc.utobj().printTestStep("Adding  Taxpayer ID details by modifying the owner");
			
			
			InfoMgrOwnersPage ownerPage = new InfoMgrFranchiseesPage(driver).getOwnersPage();
			fc.utobj().clickElement(driver, ownerPage.ownersTab);
			fc.utobj().actionImgOption(driver, franchisee.getOwnerFirstName(), "Modify");
			
			fc.utobj().sendKeys(driver, driver.findElement(By.id("taxpayerID")), "Owner_Taxpayer_ID");
			fc.utobj().clickElement(driver, driver.findElement(By.name("Submit")));
			
			fc.utobj().printTestStep("Verify Owner Taxpayer ID");
			fc.utobj().clickElement(driver, ownerPage.ownersTab);
			fc.utobj().actionImgOption(driver, franchisee.getOwnerFirstName(), "View Details");

			boolean isTaxperID = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text(),'Taxpayer ID')]/following-sibling::td/div/a/img");

			if (isTaxperID) {
				Reporter.log("Taxpayer ID is working as expected displying locked.");
			} else {
				fc.utobj().throwsException("Taxpayer ID is not work as expected or displying unlocked !!!");
			}

			if (isTaxperID) {
				isTaxperID = franchisees.checkPIIFieldVerification(driver, "Taxpayer ID", "Owner_Taxpayer_ID");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr","InfoMgr6","IM_PII" })

	@TestCase(createdOn = "2017-10-11", updatedOn = "2018-06-26", testCaseId = "TC_InfoMgr_PII_Owner_TaxpayerID_AddtoCenter", testCaseDescription = "Test to verify Owner Taxpayer ID in Center Detail page.", reference = {
			"" })
	public void InfoMgr_Franchisee_PII_Verify_OwnerTaxPayeraddtoCenter() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		FranchiseesCommonMethods franchisees = fc.infomgr().franchisees();

		AdminInfoMgrCommonMethods adminInfoMgr = new AdminInfoMgrCommonMethods();

		try {
			fc.loginpage().login(driver);
			adminInfoMgr.EnablePIIunderAdmin(driver);
			
			franchisees.checkPIIFieldAddtoCenter(driver, testCaseId, "Taxpayer ID", "Owners");

			InfoMgrFranchiseeFilter franchisee = adminInfoMgr.addFranchiseLocationForFilter(driver, false);
			franchisees.franchiseesPageShowAll(driver);

			fc.utobj().actionImgOption(driver, franchisee.getFranchiseeID(), "Modify");

			fc.utobj().printTestStep("Adding  Taxpayer ID details by modifying the owner");
			InfoMgrOwnersPage ownerPage = new InfoMgrFranchiseesPage(driver).getOwnersPage();
			fc.utobj().clickElement(driver, ownerPage.ownersTab);
			fc.utobj().actionImgOption(driver, franchisee.getOwnerFirstName(), "Modify");
			
			fc.utobj().sendKeys(driver, driver.findElement(By.id("taxpayerID")), "Owner_Taxpayer_ID");
			fc.utobj().clickElement(driver,driver.findElement(By.name("Submit")));

			fc.utobj().printTestStep("Verify Owner Taxpayer ID");
			fc.utobj().clickElement(driver, ownerPage.ownersTab);
			fc.utobj().actionImgOption(driver, franchisee.getOwnerFirstName(), "View Details");

			boolean isTaxperID = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text(),'Taxpayer ID')]/following-sibling::td/div/a/img");

			if (isTaxperID) {
				Reporter.log("Taxpayer ID is working as expected displying locked.");
			} else {
				fc.utobj().throwsException("Taxpayer ID is not work as expected or displying unlocked !!!");
			}

			fc.utobj().clickElement(driver, ownerPage.lnkCenterInfo);

			isTaxperID = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text(),'Taxpayer ID')]/following-sibling::td/div/a/img");

			if (isTaxperID) {
				Reporter.log("Taxpayer ID is working as expected displying locked in center info.");
			} else {
				fc.utobj().throwsException(
						"Taxpayer ID is not work as expected or displying unlocked in center info. !!!");
			}

			if (isTaxperID) {
				isTaxperID = franchisees.checkPIIFieldVerification(driver, "Taxpayer ID", "Owner_Taxpayer_ID");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr","IM_PII" })

	@TestCase(createdOn = "2017-10-11", updatedOn = "2017-10-11", testCaseId = "TC_InfoMgr_PII_EntityDetails_TaxpayerID", testCaseDescription = "Test to verify Entity Details Taxpayer ID in View detail page.", reference = {
			"" })
	public void InfoMgr_Franchisee_PII_Verify_EntityDetailsTaxPayer() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		FranchiseesCommonMethods franchisees = fc.infomgr().franchisees();

		AdminInfoMgrCommonMethods adminInfoMgr = new AdminInfoMgrCommonMethods();

		try {
			fc.loginpage().login(driver);

			// franchisees.checkPIIFieldAddtoCenter(driver,testCaseId ,
			// "Taxpayer ID", "Entity Details");

			adminInfoMgr.EnablePIIunderAdmin(driver);

			InfoMgrFranchiseeFilter franchisee = adminInfoMgr.addFranchiseLocationForFilter(driver, false);
			franchisees.franchiseesPageShowAll(driver);

			fc.utobj().actionImgOption(driver, franchisee.getFranchiseeID(), "Modify");

			fc.utobj().printTestStep("Adding  Taxpayer ID details by adding the Entity Detail");
			InfoMgrOwnersPage ownerPage = new InfoMgrFranchiseesPage(driver).getOwnersPage();
			fc.utobj().clickElement(driver, ownerPage.linkEntityDetail);
			String entityName = franchisees.addEntityDetails(driver, config);

			fc.utobj().printTestStep("Verify Entity Detail Taxpayer ID");

			boolean isTaxperID = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text(),'Taxpayer ID')]/following-sibling::td/div/a/img");

			if (isTaxperID) {
				Reporter.log("Taxpayer ID is working as expected displying locked.");
			} else {
				fc.utobj().throwsException("Taxpayer ID is not work as expected or displying unlocked !!!");
			}

			if (isTaxperID) {
				isTaxperID = franchisees.checkPIIFieldVerification(driver, "Taxpayer ID", "Tax Payer ID");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" , "IMFailedTC","IM_PII2"})

	@TestCase(createdOn = "2017-10-11", updatedOn = "2017-10-11", testCaseId = "TC_InfoMgr_PII_EntityDetails_AddtoCenter_TaxpayerID", testCaseDescription = "Test to verify Entity Details Taxpayer ID in Center detail page.", reference = {
			"" })
	public void InfoMgr_Franchisee_PII_Verify_EntityDetailsAddtoCenter() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		FranchiseesCommonMethods franchisees = fc.infomgr().franchisees();
		AdminInfoMgrCommonMethods adminInfoMgr = new AdminInfoMgrCommonMethods();

		try {
			fc.loginpage().login(driver);

			adminInfoMgr.EnablePIIunderAdmin(driver);

			franchisees.checkPIIFieldAddtoCenter(driver, testCaseId, "Taxpayer ID", "Entity Details");

			Location location=adminInfoMgr.addFranchiseLocationForInfoMgr(driver, false);
			
			//InfoMgrFranchiseeFilter franchisee = adminInfoMgr.addFranchiseLocationForFilter(driver, false);	
			franchisees.searchFranchise(driver, location.getFranchiseID());
			fc.utobj().actionImgOption(driver, location.getFranchiseID(), "Modify");

			fc.utobj().printTestStep("Adding  Taxpayer ID details by adding the Entity Detail");
			InfoMgrOwnersPage ownerPage = new InfoMgrFranchiseesPage(driver).getOwnersPage();
			fc.utobj().clickElement(driver, ownerPage.linkEntityDetail);
			String entityName = franchisees.addEntityDetails(driver, config);

			fc.utobj().clickElement(driver, ownerPage.lnkCenterInfo);

			fc.utobj().printTestStep("Verify Entity Detail Taxpayer ID");

			boolean isTaxperID = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text(),'Taxpayer ID/FEIN')]/following-sibling::td/div/a/img");

			if (isTaxperID) {
				Reporter.log("Taxpayer ID is working as expected displying locked.");
			} else {
				fc.utobj().throwsException("Taxpayer ID is not work as expected or displying unlocked !!!");
			}

			if (isTaxperID) {
				isTaxperID = franchisees.checkPIIFieldVerification(driver, "Taxpayer ID/FEIN", "Tax Payer ID");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" ,"IM_PII"})

	@TestCase(createdOn = "2017-10-11", updatedOn = "2017-10-11", testCaseId = "TC_InfoMgr_PII_TASK_CustomField", testCaseDescription = "Test to verify PII Field in Task detail page.", reference = {
			"" })
	public void InfoMgr_Franchisee_PII_Verify_TaskCustomField() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		FranchiseesCommonMethods franchisees = fc.infomgr().franchisees();

		AdminInfoMgrCommonMethods adminInfoMgr = new AdminInfoMgrCommonMethods();
		AdminInfoMgrManageFormGeneratorPageTest p1 = new AdminInfoMgrManageFormGeneratorPageTest();
		try {
			fc.loginpage().login(driver);

			adminInfoMgr.EnablePIIunderAdmin(driver);

			fc.utobj().printTestStep("Go to Admin > Info Mgr > Manage Form Generator");
			fc.infomgr().infomgr_common().AdminFIMManageFormGenerator(driver);
			AdminFranchiseSalesManageFormGeneratorPage pobjsales = new AdminFranchiseSalesManageFormGeneratorPage(
					driver);
			fc.utobj().clickElement(driver, pobjsales.continueBtn);

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, "//*[@id='tabdetails']//*[text()='Task']"));
			fc.utobj().clickElement(driver, pobjsales.PrimaryInfoAddNewFied);
			String fieldNameTask = p1.addFieldNamePrimaryInfo(driver, "Task", "PIITask");
			fc.utobj().refresh(driver);

			boolean isFieldNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + fieldNameTask + "')]");
			if (isFieldNamePresent == false) {
/*				fc.utobj().printTestStep("Alert :: Added field is not displaying on the Task page in First Attempt");
				fc.utobj().printTestStep("Trying 2nd attemt to add the Custom field");
									
					fc.utobj().clickElement(driver, pobjsales.PrimaryInfoAddNewFied);
					String fieldNameTask1 = p1.addFieldNamePrimaryInfo(driver, "Task", "PIITask");
					driver.navigate().refresh();			

					boolean isFieldNamePresent1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
							".//td[contains(text () , '" + fieldNameTask1 + "')]");
					driver.navigate().refresh();
					
					if (isFieldNamePresent1 == false)*/
						fc.utobj().throwsException("Was not able to verify added fieldName under Info Mgr Task Tabs of Manage Form Generator");
			}
			try {
				
				List<WebElement> element = driver
						.findElements(By.xpath(".//*[@id='siteMainTable']//td[contains(text(), '" + fieldNameTask
								+ "')]/ancestor::tr[1]//img[@title='Click to Enable PII']"));
				
				if (element.size()==0) {
					fc.utobj().refresh(driver);
					element = driver
							.findElements(By.xpath(".//*[@id='siteMainTable']//td[contains(text(), '" + fieldNameTask
									+ "')]/ancestor::tr[1]//img[@title='Click to Enable PII']"));
				} 
				if (element.size()>0) {
					fc.utobj().clickElement(driver, element.get(0));
					fc.utobj().printTestStep("Field is clicked to Enable PII");
				}
				
				
					
			} catch (Exception e) {
				Reporter.log(e.getMessage());
			}

			InfoMgrFranchiseeFilter franchisee = adminInfoMgr.addFranchiseLocationForFilter(driver, false);
			franchisees.franchiseesPageShowAll(driver);

			fc.utobj().actionImgOption(driver, franchisee.getFranchiseeID(), "Modify");

			fc.utobj().printTestStep("Go to Contact History");
			InfoMgrOwnersPage ownerPage = new InfoMgrFranchiseesPage(driver).getOwnersPage();
			fc.utobj().clickElement(driver, ownerPage.lnkContactHistory);

			// test close
			String taskSubject = "PTSK";
			fc.utobj().printTestStep("Verify the Log a Task with PII Field");
			fc.utobj().clickPartialLinkText(driver, "Add Task");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			taskSubject = fc.utobj().generateTestData(taskSubject);
			fc.utobj().selectValFromMultiSelect(driver, fc.utobj().getElementByID(driver, "ms-parentassignTo"),
					"FranConnect Administrator");
			fc.utobj().selectDropDownByVisibleText(driver, fc.utobj().getElementByID(driver, "status"), "Not Started");
			fc.utobj().sendKeys(driver, pobjsales.taskSubject, taskSubject);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver,
					"//input[contains(@name, '_" + fieldNameTask.toLowerCase() + "')]"), fieldNameTask);
			fc.utobj().clickElement(driver, pobjsales.addTaskBtn);
			fc.utobj().clickPartialLinkText(driver, taskSubject);
			driver = fc.utobj().switchFrameToDefault(driver);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().printTestStep("Verify in Task Detail for PII Field");

			boolean isTaxperID = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text(),'" + fieldNameTask + "')]/following-sibling::td/div/a/img");
			if (isTaxperID) {
				Reporter.log("PII Field is working as expected displying locked.");
			} else {
				fc.utobj().throwsException("PII Field is not work as expected or displying unlocked !!!");
			}

			if (isTaxperID) {
				String lable = fieldNameTask;
				boolean isTextPresent = false;

				fc.utobj().clickElement(driver, driver.findElement(
						By.xpath(".//td[contains(text(),'" + lable + "')]/following-sibling::td/div/a/img")));
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='password']"),
						"piisupervisior");

				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='Submit']"));

				fc.utobj().switchFrameToDefault(driver);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				isTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text () , '" + fieldNameTask + "')]");
				if (isTextPresent == false) {
					fc.utobj().throwsException("was not able to verify text for " + fieldNameTask + " in Task Detail");
				} else {
					Reporter.log("PII Field is working as expected in Task Detail.");
				}

			}
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//input[@value='Close']"));

			driver = fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr", "IMFailedTC","IM_PII"})

	@TestCase(createdOn = "2017-10-11", updatedOn = "2018-06-06", testCaseId = "TC_InfoMgr_PII_Access_Logs", testCaseDescription = "Test to verify PII Access Logs.", reference = {
			"" })
	public void InfoMgr_Franchisee_PII_AccessLog() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		FranchiseesCommonMethods franchisees = fc.infomgr().franchisees();

		AdminInfoMgrCommonMethods adminInfoMgr = new AdminInfoMgrCommonMethods();

		try {
			fc.loginpage().login(driver);
			adminInfoMgr.EnablePIIunderAdmin(driver);
			franchisees.checkPIIFieldAddtoCenter(driver, testCaseId, "Taxpayer ID", "Owners");

			InfoMgrFranchiseeFilter franchisee = adminInfoMgr.addFranchiseLocationForFilter(driver, false);
			franchisees.franchiseesPageShowAll(driver);
			/*imc.fimModule(driver);
			imc.InfoMgrFranchisees(driver);
			imc.SearchFranchiseeAndClick(driver, "franchiseIDd06145042");*/
			
			
			fc.utobj().printTestStep("Click on Modify Location");
			fc.utobj().actionImgOption(driver, franchisee.getOwnerFirstName(), "Modify");
			fc.utobj().printTestStep("Location Modify Page Open");
			
			fc.utobj().printTestStep("Now Adding  Taxpayer ID details by modifying the owner");
			InfoMgrOwnersPage ownerPage = new InfoMgrFranchiseesPage(driver).getOwnersPage();
			fc.utobj().clickElement(driver, ownerPage.ownersTab);
			fc.utobj().actionImgOption(driver, franchisee.getOwnerFirstName(), "Modify");
			fc.utobj().sendKeys(driver, driver.findElement(By.id("taxpayerID")), "Test_Taxpayer_ID");
			fc.utobj().clickElement(driver, driver.findElement(By.name("Submit")));
			
			fc.utobj().printTestStep("Verify Owner Taxpayer ID");
			fc.utobj().clickElement(driver, ownerPage.ownersTab);
			fc.utobj().actionImgOption(driver, franchisee.getOwnerFirstName(), "View Details");

			boolean isTaxperID = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text(),'Taxpayer ID')]/following-sibling::td/div/a/img");

			if (isTaxperID) {
				Reporter.log("Taxpayer ID is working as expected displying locked.");
			} else {
				fc.utobj().throwsException("Taxpayer ID is not work as expected or displying unlocked !!!");
			}

			fc.utobj().clickElement(driver, ownerPage.lnkCenterInfo);

			isTaxperID = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text(),'Taxpayer ID')]/following-sibling::td/div/a/img");

			if (isTaxperID) {
				Reporter.log("Taxpayer ID is working as expected displying locked in center info.");
			} else {
				fc.utobj().throwsException(
						"Taxpayer ID is not work as expected or displying unlocked in center info. !!!");
			}

			if (isTaxperID) {
				isTaxperID = franchisees.checkPIIFieldVerification(driver, "Taxpayer ID", "Test_Taxpayer_ID");
			}

			Reporter.log("Verify the PII Access Logs.");
			fc.adminpage().adminConfigurationPIIConfiguration(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@qat_tabname='PII Access Logs']"));

			isTaxperID = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text(),'Owners')]/following-sibling::td[contains(text(),'Taxpayer ID')]");

			if (isTaxperID == false) {
				fc.utobj().throwsException("not able to verify PII access log details of all the users.");
			} else {
				Reporter.log("PII access log details of all the users working.");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgrNotRequired" })

	@TestCase(createdOn = "2017-10-11", updatedOn = "2017-10-11", testCaseId = "TC_offPII_01", testCaseDescription = "To verify that user can configure its own password.", reference = {
			"" })
	public void InfoMgroffPII() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			fc.loginpage().login(driver);
			try {
				fc.adminpage().adminConfigurationPIIConfiguration(driver);
				AdminConfigurationPIIConfigurationPage pobj = new AdminConfigurationPIIConfigurationPage(driver);
				fc.utobj().clickElement(driver, pobj.passwordFunctionalityOff);
				fc.utobj().sendKeys(driver, pobj.confirmPasswordText, "piisupervisior");
				fc.utobj().clickElement(driver, pobj.saveBtn);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}

}
