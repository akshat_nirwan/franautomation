package com.builds.test.infomgr;

import org.testng.annotations.BeforeGroups;
import org.testng.annotations.Test;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.admin.CorporateUserAPI;
import com.builds.test.common.CorporateUser;
import com.builds.uimaps.infomgr.InfoMgrAgreementPage;
import com.builds.uimaps.infomgr.InfoMgrAreaOwnerPage;
import com.builds.uimaps.infomgr.InfoMgrCenterInfoPage;
import com.builds.uimaps.infomgr.InfoMgrContactHistoryPage;
import com.builds.uimaps.infomgr.InfoMgrCorporateLocationsPage;
import com.builds.uimaps.infomgr.InfoMgrCustomTabPage;
import com.builds.uimaps.infomgr.InfoMgrFranchiseesPage;
import com.builds.uimaps.infomgr.InfoMgrOwnersPage;
import com.builds.uimaps.infomgr.InfoMgrRegionalPage;
import com.builds.uimaps.infomgr.InfoMgrSearchFilterPage;
import com.builds.uimaps.infomgr.InfoMgrUsersPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class InfoMgrFranchiseesPageTest {

	FranconnectUtil fc = new FranconnectUtil();
	InfoMgr_Common imc = new InfoMgr_Common();
	public static final String RECORDADDED = "Record Added";
	public static final String RECORDMODIFIED = "Record Modified";
	public static final String RECORDDELETED = "Record Deleted";
	// public static final String PASSWORDGMAIL = "Welcome@01";
	public static final String PASSWORD = "sdg@1a@Hfs";
	public static final String AGREEMENT = "Agreement";
	public static final String EMAIL = "salesautomation@staffex.com";
	public static final String REGULAR = "regular";
	public static final String TABULAR = "tabular";
	public static final String ADD = "add";
	public static final String MODIFY = "modify";
	public static String flag = "true";
	public static final String franchiseID = "Franchise" + ThreadLocalRandom.current().nextInt(10000);

	@BeforeGroups(groups = { "infomgr" ,"Franchisee_Tabs_AMD","EMPLOEE"})
	public synchronized void createFranchise() throws Exception {
		String testCaseId = "BeforeGroupBlock to create Common franchisee Location";

		
		if (flag == "true") {
			flag = "false";
			FranchiseeAPI fran = new FranchiseeAPI();
			WebDriver driver = fc.commonMethods().browsers().openBrowser();
			System.out.println("inside before group Franchisee_Tabs_AMD");
			AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();
			fc.loginpage().login(driver);
			fc.utobj().printTestStep("Add Area / Region ");
			String areaRegionID=adminInfoMgr.addAreaRegion(driver);
			Reporter.log("Area Region - " + areaRegionID + "created");
			System.out.println("Area Region created : " + areaRegionID);
			fran.createDefaultFranchiseeAPI(areaRegionID, "No", franchiseID);
			System.out.println("franchise created : " + franchiseID);
			Reporter.log("Franchise is Created" + franchiseID);
			fc.utobj().printTestStep("Add corporate user");									 
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		}
	}

	@Test(groups = {  "infomgr" ,"Franchisee_Tabs_AMD" })
	// BugID - 69046 - Login with corporate user
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_Franchisee_AgreementTab_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of Agreement tab of Info Mgr > Franchisees.", reference = { "" })
	public void InfoMgr_Franchisees_AgreementTab_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		FranchiseesCommonMethods franchisees = fc.infomgr().franchisees();

		try {
			fc.loginpage().login(driver);
			InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();
			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");
			franchisees.searchFranchiseAndClick(driver, franchiseID);
			fc.utobj().printTestStep("Add Agreement Tab Data");
			// Add Agreement
			fc.utobj().clickElement(driver, centerInfoPage.lnkAgreement);
			driver = franchisees.addAgreement(driver, config);
			// Modify Agreement
			fc.utobj().clickElement(driver, centerInfoPage.lnkAgreement);
			driver = franchisees.modifyAgreement(driver, config);
			// Delete Agreement
			franchisees.deleteTabData(driver, config, centerInfoPage.lnkAgreement, null);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" ,"Franchisee_Tabs_AMD","Testddd"})

	// BugID - 69046 - Login with corporate user
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_Franchisees_AddressTab_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of Address tab of Info Mgr > Franchisees.", reference = { "" })
	public void InfoMgr_Franchisees_AddressTab_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		FranchiseesCommonMethods franchisees = fc.infomgr().franchisees();
		try {
			fc.loginpage().login(driver);
			InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();
			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");
			franchisees.searchFranchiseAndClick(driver, franchiseID);

			// Add Address
			fc.utobj().printTestStep("Add Address Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkAddresses);
			driver = franchisees.addAddresses(driver, config);
			// Modify Address
			fc.utobj().printTestStep("Modify Address Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkAddresses);
			driver = franchisees.modifyAddresses(driver, config);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {  "infomgr" ,"Franchisee_Tabs_AMD" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_Franchisee_ContactHistoryTab_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of ContactHistoryTab of Info Mgr > Franchisees.", reference = { "" })
	public void InfoMgr_Franchisees_ContactHistoryTab_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		FranchiseesCommonMethods franchisees = fc.infomgr().franchisees();
		InfoMgrFranchiseesPage franchiseesPage = new InfoMgrFranchiseesPage(driver);

		try {
			System.out.println("inside InfoMgr_Franchisees_ContactHistoryTab_Add_Modify_Delete");
			System.out.println(franchiseID);
			fc.loginpage().login(driver);	
			InfoMgrContactHistoryPage contactHistoryPage = franchiseesPage.getContactHistoryPage();
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail("infomgrautomation@staffex.com");
			corpUser = corpTest.createDefaultUser(driver, corpUser);
			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");
			franchisees.searchFranchiseAndClick(driver, franchiseID);
			
			// Log a Task
			fc.utobj().clickElement(driver, contactHistoryPage.lnkContactHistory);

			fc.utobj().clickElement(driver, contactHistoryPage.btnLogaTask);
			String taskSubject = franchisees.logaTask(driver, corpUser.getuserFullName(), true);

			// Log a Call
			fc.utobj().clickElement(driver, contactHistoryPage.btnLogaCall);
			String callSubject = franchisees.logACall(driver, true);

			// Add Remark
			fc.utobj().clickElement(driver, contactHistoryPage.btnAddRemark);
			String remarkSubject = franchisees.addRemark(driver, contactHistoryPage);

			// Modify Contact History

			// Modify a task
			fc.utobj().clickElement(driver, contactHistoryPage.lnkContactHistory);
			fc.utobj().actionImgOption(driver, taskSubject, "Modify");
			franchisees.modifyATask(driver, config, true);

			// Modify a call
			fc.utobj().actionImgOption(driver, callSubject, "Modify");
			franchisees.modifyACall(driver, config, true);

			// Modify a remark
			fc.utobj().actionImgOption(driver, remarkSubject, "Modify");
			franchisees.modifyRemark(driver, config, contactHistoryPage);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" ,"Franchisee_Tabs_AMD" ,"EMPLOEE"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_Franchisee_ContractSigning_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of ContractSigning tabs of Info Mgr > Franchisees.", reference = { "" })
	public void InfoMgr_Franchisees_ContractSigningTab_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		FranchiseesCommonMethods franchisees = fc.infomgr().franchisees();
		try {
			fc.loginpage().login(driver);	
			// adminInfoMgr.configureInfoMgr(driver, config);
			InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();
			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");
			franchisees.searchFranchiseAndClick(driver, franchiseID);
			// Adding Contract Signing
			fc.utobj().printTestStep("Add Contract History Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkContractSigning);
			franchisees.addContractSigning(driver, config);
			// Modify Contract Signing
			fc.utobj().printTestStep("Modify Contract History Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkContractSigning);
			franchisees.modifyContractSigning(driver, config);
			// Delete Contract Signing
			fc.utobj().printTestStep("Delete Contract History Tab Data");
			franchisees.deleteTabData(driver, config, centerInfoPage.lnkContractSigning, null);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" ,"Franchisee_Tabs_AMD" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_Franchisee_CustomerComplaint_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of CustomerComplaint tabs of Info Mgr > Franchisees.", reference = { "" })
	public void InfoMgr_Franchisees_CustomerComplaintTab_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		FranchiseesCommonMethods franchisees = fc.infomgr().franchisees();
		try {

			fc.loginpage().login(driver);
			// adminInfoMgr.configureInfoMgr(driver, config);
			InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();
			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");
			franchisees.searchFranchiseAndClick(driver, franchiseID);

			// Add Customer Complaints
			fc.utobj().printTestStep("Add Customer Complaints Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkCustomerComplaints);
			franchisees.addCustomerComplaints(driver, config);

			// Modify Customer Complaints
			fc.utobj().printTestStep("Modify Customer Complaints Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkCustomerComplaints);
			franchisees.modifyCustomerComplaints(driver, config);
			// Delete
			fc.utobj().printTestStep("Delete Customer Complaints Tab Data");
			franchisees.deleteTabData(driver, config, centerInfoPage.lnkCustomerComplaints, null);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {  "infomgr" ,"Franchisee_Tabs_AMD" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_Franchisee_CenterInfo_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of _CenterInfo  tabs of Info Mgr > Franchisees.", reference = { "" })
	public void InfoMgr_Franchisees_CenterInfo_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		FranchiseesCommonMethods franchisees = fc.infomgr().franchisees();
		InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();
		try {
			fc.loginpage().login(driver);
			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");
			franchisees.searchFranchiseAndClick(driver, franchiseID);
			// Modify Center Info
			fc.utobj().printTestStep("Modify Center Info Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkCenterInfo);
			fc.utobj().clickLink(driver, "Modify");
			franchisees.modifyCenterInfo(driver, config);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {  "infomgr" ,"Franchisee_Tabs_AMD" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_Franchisee_Documents_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of Documents tabs of Info Mgr > Franchisees.", reference = { "" })
	public void InfoMgr_Franchisees_Documents_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		FranchiseesCommonMethods franchisees = fc.infomgr().franchisees();
		InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();
		try {
			fc.loginpage().login(driver);
			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");		
			franchisees.searchFranchiseAndClick(driver, franchiseID);
			// Add Documents
			fc.utobj().printTestStep("Add  Documents Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkDocuments);
			String docTitle = franchisees.addDocument(driver, config);

			// Modify Documents
			fc.utobj().printTestStep("Modify  Documents Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkDocuments);
			fc.utobj().actionImgOption(driver, docTitle, "Modify");
			docTitle = franchisees.modifyDocument(driver, config);
			// Delete
			fc.utobj().printTestStep("Delete  Documents Tab Data");
			franchisees.deleteTabData(driver, config, centerInfoPage.lnkDocuments, docTitle);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {  "infomgr" ,"Franchisee_Tabs_AMD" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_Franchisee_Employees_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of Employee tabs of Info Mgr > Franchisees.", reference = { "" })
	public void InfoMgr_Franchisees_Employees_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		FranchiseesCommonMethods franchisees = fc.infomgr().franchisees();
		InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();
		try {
			fc.loginpage().login(driver);
			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");
			franchisees.searchFranchiseAndClick(driver, franchiseID);
			// Add Employees
			fc.utobj().printTestStep("Add  Employees Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkEmployees);
			String empName = franchisees.addEmployee(driver, config);
			// Modify Employee
			fc.utobj().printTestStep("Modify  Employees Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkEmployees);
			// fc.utobj().actionImgOption(driver, empName, "Modify");
			empName = franchisees.modifyEmployee(driver, config);
			fc.utobj().printTestStep("Delete  Employees Tab Data");
			franchisees.deleteTabData(driver, config, centerInfoPage.lnkEmployees, null);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" ,"Franchisee_Tabs_AMD" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_Franchisee_Guarantor_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of Guarantor tab of Info Mgr > Franchisees.", reference = { "" })
	public void InfoMgr_Franchisees_Guarantor_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		FranchiseesCommonMethods franchisees = fc.infomgr().franchisees();
		InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();
		try {

			fc.loginpage().login(driver);
			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");
			franchisees.searchFranchiseAndClick(driver, franchiseID);
			// Add Gurantor details
			fc.utobj().printTestStep("Add  Gurantor Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkGuarantors);
			franchisees.addGuarantor(driver, config);
			// Modify Guarantor
			fc.utobj().printTestStep("Modify  Gurantor Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkGuarantors);
			franchisees.modifyGuarantor(driver, config);
			// Delete Entity
			fc.utobj().printTestStep("Delete  Gurantor Tab Data");
			franchisees.deleteTabData(driver, config, centerInfoPage.guarantorsTab, null);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" ,"Franchisee_Tabs_AMD" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_Franchisee_Entity_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of Entity Tab of Info Mgr > Franchisees.", reference = { "" })
	public void InfoMgr_Franchisees_Entity_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		FranchiseesCommonMethods franchisees = fc.infomgr().franchisees();
		InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();
		try {
			fc.loginpage().login(driver);
			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");
			franchisees.searchFranchiseAndClick(driver, franchiseID);
			// Add
			fc.utobj().printTestStep("Add  Entity Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.linkEntityDetail);
			franchisees.addEntityDetails(driver, config);
			// Modify Entity Details
			fc.utobj().printTestStep("Modify  Entity Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.linkEntityDetail);
			franchisees.modifyEntityDetails(driver, config);
			// Delete Entity
			fc.utobj().printTestStep("Delete  Entity Tab Data");
			franchisees.deleteTabData(driver, config, centerInfoPage.linkEntityDetail, null);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {  "infomgr" ,"Franchisee_Tabs_AMD" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_Franchisee_Marketing_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of Marketing Tab of Info Mgr > Franchisees.", reference = { "" })
	public void InfoMgr_Franchisees_Marketing_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			FranchiseesCommonMethods franchisees = fc.infomgr().franchisees();
			InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();
			fc.loginpage().login(driver);
			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");
			franchisees.searchFranchiseAndClick(driver, franchiseID);

			// Add Marketing details
			fc.utobj().printTestStep("Add  Marketing Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkMarketingFinTab);
			franchisees.addMarketingDetails(driver, config);
			// Modify Marketing
			fc.utobj().printTestStep("Modify  Marketing Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkMarketingFinTab);
			franchisees.modifyMarketingDetails(driver, config);
			// Delete marketing
			fc.utobj().printTestStep("Delete  Marketing Tab Data");
			franchisees.deleteTabData(driver, config, centerInfoPage.marketingTab, null);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {  "infomgr" ,"Franchisee_Tabs_AMD"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_Franchisee_Insurance_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of Insurance tabs of Info Mgr > Franchisees.", reference = { "" })
	public void InfoMgr_Franchisees_Insurance_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		FranchiseesCommonMethods franchisees = fc.infomgr().franchisees();

		InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();
		try {

			fc.loginpage().login(driver);

			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");
			franchisees.searchFranchiseAndClick(driver, franchiseID);
			// Add Insurance Details
			fc.utobj().printTestStep("Add  Insurance Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkInsurance);
			franchisees.addInsuranceDetails(driver, config);

			// Modify Insurance
			fc.utobj().printTestStep("Modify  Insurance Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkInsurance);
			franchisees.modifyInsuranceDetails(driver, config);

			// Delete Insurance
			fc.utobj().printTestStep("Delete  Insurance Tab Data");
			franchisees.deleteTabData(driver, config, centerInfoPage.insuranceTab, null);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {  "infomgr" ,"Franchisee_Tabs_AMD"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_Franchisee_LegalViolation_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of LegalViolation tabs of Info Mgr > Franchisees.", reference = { "" })
	public void InfoMgr_Franchisees_LegalViolation_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		FranchiseesCommonMethods franchisees = fc.infomgr().franchisees();

		InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();
		try {

			fc.loginpage().login(driver);
			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");
			franchisees.searchFranchiseAndClick(driver, franchiseID);
			// Add legal violation
			fc.utobj().printTestStep("Add  Legal Violation Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkLegalViolation);
			franchisees.addLegalViolation(driver, config);
			// Modify Legal Violation
			fc.utobj().printTestStep("Modify  Legal Violation Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkLegalViolation);
			franchisees.modifyLegalViolation(driver, config);
			// Delete Legal Violation
			fc.utobj().printTestStep("Delete  Legal Violation Tab Data");
			franchisees.deleteTabData(driver, config, centerInfoPage.lnkLegalViolation, null);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" ,"Franchisee_Tabs_AMD" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_Franchisee_Lenders_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of Lenders tabs of Info Mgr > Franchisees.", reference = { "" })
	public void InfoMgr_Franchisees_Lenders_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		FranchiseesCommonMethods franchisees = fc.infomgr().franchisees();
		InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();
		try {

			fc.loginpage().login(driver);
			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");
			franchisees.searchFranchiseAndClick(driver, franchiseID);

			// Add Lenders
			fc.utobj().printTestStep("Add  Lenders Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkLenders);
			franchisees.addLenders(driver, config);
			// Modify Lenders Details
			fc.utobj().printTestStep("Modify  Lenders Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkLenders);
			franchisees.modifyLenders(driver, config);

			// Delete Lenders
			fc.utobj().printTestStep("Delete  Lenders Tab Data");
			franchisees.deleteTabData(driver, config, centerInfoPage.lnkLenders, null);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" ,"Franchisee_Tabs_AMD" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_Franchisee_Finance_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of Finance of Info Mgr > Franchisees.", reference = { "" })
	public void InfoMgr_Franchisees_Finance_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		FranchiseesCommonMethods franchisees = fc.infomgr().franchisees();
		InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();
		try {

			fc.loginpage().login(driver);

			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");
			franchisees.searchFranchiseAndClick(driver, franchiseID);

			// Add Finance Details
			fc.utobj().printTestStep("Add  Finance Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkFinancial);
			franchisees.addFinanceDetails(driver, config);

			// Modify Financials
			fc.utobj().printTestStep("Modify  Finance Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkFinancial);
			franchisees.modifyFinanceDetails(driver, config);

			// Delete Financials
			fc.utobj().printTestStep("Delete  Finance Tab Data");
			franchisees.deleteTabData(driver, config, centerInfoPage.lnkFinancial, null);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {  "infomgr" ,"Franchisee_Tabs_AMD" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_Franchisee_MysteryReview_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of MysteryReview Tab of Info Mgr > Franchisees.", reference = { "" })
	public void InfoMgr_Franchisees_MysteryReview_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		FranchiseesCommonMethods franchisees = fc.infomgr().franchisees();
		InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();
		try {
			fc.loginpage().login(driver);

			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");
			franchisees.searchFranchiseAndClick(driver, franchiseID);

			// Add Mystry Review
			fc.utobj().printTestStep("Add  MystryReview Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkMysteryReview);
			franchisees.addMystryReview(driver, config);
			// Modify Mystry Review
			fc.utobj().printTestStep("Modify  MystryReview Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkMysteryReview);
			franchisees.modifyMystryReview(driver, config);
			// Delete MysteryReview
			fc.utobj().printTestStep("Delete  MystryReview Tab Data");
			franchisees.deleteTabData(driver, config, centerInfoPage.lnkMysteryReview, null);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" ,"Franchisee_Tabs_AMD"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_Franchisee_Picture_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of Picture tab of Info Mgr > Franchisees.", reference = { "" })
	public void InfoMgr_Franchisees_Picture_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		FranchiseesCommonMethods franchisees = fc.infomgr().franchisees();
		InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();
		try {
			fc.loginpage().login(driver);
			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");
			franchisees.searchFranchiseAndClick(driver, franchiseID);

			// Add Picture
			fc.utobj().printTestStep("Add  Picture Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkPictures);
			String pictureTitle = franchisees.addPicture(driver, config);
			// Modify Pictures
			fc.utobj().printTestStep("Modify  Picture Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkPictures);
			fc.utobj().actionImgOption(driver, pictureTitle, "Modify");
			pictureTitle = franchisees.modifyPicture(driver, config);

			// Delete Pictures
			fc.utobj().printTestStep("Delete  Picture Tab Data");
			franchisees.deleteTabData(driver, config, centerInfoPage.lnkPictures, pictureTitle);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {  "infomgr" ,"Franchisee_Tabs_AMD" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_Franchisee_Owner_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of Owner of Info Mgr > Franchisees.", reference = { "" })
	public void InfoMgr_Franchisees_Owner_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		FranchiseesCommonMethods franchisees = fc.infomgr().franchisees();
		InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();
		try {
			fc.loginpage().login(driver);

			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");
			franchisees.searchFranchiseAndClick(driver, franchiseID);
			// Add Owner
			fc.utobj().printTestStep("Add  Owner Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.tabOwners);
			Map<String, String> owner = franchisees.addOwner(driver);
			// Add User
			fc.utobj().printTestStep("Add  User Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkUsers);
			InfoMgrUsersPage usersPage = new InfoMgrFranchiseesPage(driver).getUsersPage();
			fc.utobj().clickElement(driver, usersPage.btnAddUser);
			String userName = franchisees.addOwnerUser(driver, config, owner.get("FirstName") + " " + owner.get("LastName"));
			// Modify Owners
			fc.utobj().printTestStep("Modify  Owner Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.tabOwners);
			fc.utobj().actionImgOption(driver, owner.get("FirstName"), "Modify");
			franchisees.modifyOwner(driver, config);
			// Modify Users
			fc.utobj().printTestStep("Modify  Users Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkUsers);
			fc.utobj().actionImgOption(driver, owner.get("FirstName") + " " + owner.get("LastName"), "Modify");
			franchisees.modifyOwnerUser(driver, config);
			// Delete Owner
			fc.utobj().printTestStep("Delete  Owner Tab Data");
			franchisees.deleteTabData(driver, config, centerInfoPage.ownersTab, owner.get("FirstName"));
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" ,"Franchisee_Tabs_AMD" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_Franchisee_Realestate_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of Realestate of Info Mgr > Franchisees.", reference = { "" })
	public void InfoMgr_Franchisees_Realestate_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		FranchiseesCommonMethods franchisees = fc.infomgr().franchisees();
		InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();
		try {
			fc.loginpage().login(driver);
			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");
			franchisees.searchFranchiseAndClick(driver, franchiseID);
			// Add Real Estate
			fc.utobj().printTestStep("Add  RealEstate Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkRealEstate);
			franchisees.addRealState(driver, config);
			// Modify Real state
			fc.utobj().printTestStep("Modify  RealEstate Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkRealEstate);
			franchisees.modifyRealState(driver, config);
			// Delete RealEstate
			fc.utobj().printTestStep("Delete  RealEstate Tab Data");
			franchisees.deleteTabData(driver, config, centerInfoPage.lnkRealEstate, null);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {  "infomgr" ,"Franchisee_Tabs_AMD","Testddd"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_Franchisee_Territory_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of Territory Tab of Info Mgr > Franchisees.", reference = { "" })
	public void InfoMgr_Franchisees_Territory_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		FranchiseesCommonMethods franchisees = fc.infomgr().franchisees();

		InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();
		try {
			fc.loginpage().login(driver);
			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");
			franchisees.searchFranchiseAndClick(driver, franchiseID);
			// Add Territory
			fc.utobj().printTestStep("Add  Territory Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkTerritory);
			franchisees.addTerritory(driver, config);
			// Modify Territory
			fc.utobj().printTestStep("Modify  Territory Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkTerritory);
			franchisees.modifyTerritory(driver, config);
			// Delete Territory
			fc.utobj().printTestStep("delete  Territory Tab Data");
			franchisees.deleteTabData(driver, config, centerInfoPage.lnkTerritory, null);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {  "infomgr" ,"Franchisee_Tabs_AMD"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_Franchisee_Training_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of Training Tab of Info Mgr > Franchisees.", reference = { "" })
	public void InfoMgr_Franchisees_Training_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		FranchiseesCommonMethods franchisees = fc.infomgr().franchisees();
		InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();
		try {
			fc.loginpage().login(driver);
			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");
			franchisees.searchFranchiseAndClick(driver, franchiseID);
			// Add Training
			fc.utobj().printTestStep("Add  Training Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkTraining);
			franchisees.addTraining(driver, config);
			// Modify Training
			fc.utobj().printTestStep("Modify  Training Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkTraining);
			franchisees.modifyTraining(driver, config);
			// Delete Training
			fc.utobj().printTestStep("Delete  Training Tab Data");
			franchisees.deleteTabData(driver, config, centerInfoPage.lnkTraining, null);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" })

	// BugID - 69046 - Login with corporate user
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_InfoMgr_Franchisee_Page_Verify", testCaseDescription = "This test case will verify franchisee page.", reference = { "69046" })
	public void VerifyFranchissesPage() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		FranchiseesCommonMethods franchise = fc.infomgr().franchisees();
		AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();

		try {
			driver = fc.loginpage().login(driver);
			// Add new Franchisee Location
			fc.utobj().printTestStep("Add Franchise Location");
			String franchiseID = adminInfoMgr.addFranchiseLocation(driver);
			fc.utobj().printTestStep("Search Franchisee and verify the page");
			franchise.franchiseesPageShowAll(driver);
			boolean pageSource = fc.utobj().assertLinkText(driver, franchiseID);
			if (pageSource) {
				Reporter.log("There is no error page and page loads successfully. Test Passes !!!");
			} else {
				fc.utobj().throwsException("Franchisees page didn't load properly . Test Failes !!!");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" })

	// BugID - 82718
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_InfoMgr_Franchisee_SearchFilter_AreaOwner", testCaseDescription = "This test case will verify search page based on area owner filter", reference = { "82718" })
	public void InfoMgrFranchissesSearchFilterByAreaOwner() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		RegionalCommonMethods regional = fc.infomgr().regional();
		AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();

		try {
			driver = fc.loginpage().login(driver);
			// Add region
			fc.utobj().printTestStep("Add First Area Region");
			String region1 = adminInfoMgr.addAreaRegion(driver);

			String franchise1 = adminInfoMgr.addFranchiseLocationWithRegion(driver, region1);

			// Search and navigate to area region
			fc.utobj().printTestStep("Search and click Area / Region");
			regional.searchAndClickAreaRegion(driver, region1);

			// Add areaOwner
			fc.utobj().printTestStep("Add Area Owner");
			InfoMgrAreaOwnerPage objAreaOwnerPage = new InfoMgrRegionalPage(driver).getAreaOwnerPage();
			fc.utobj().clickElement(driver, objAreaOwnerPage.lnkAreaOwner);
			Map<String, String> lstOwner = regional.addAreaRegionOwner(driver);

			// Add another region
			fc.utobj().printTestStep("Add Second Area Region");
			String region2 = adminInfoMgr.addAreaRegion(driver);

			String franchise2 = adminInfoMgr.addFranchiseLocationWithRegion(driver, region2);

			// Search and navigate to area region
			fc.utobj().printTestStep("Search and click Area / Region");
			regional.searchAndClickAreaRegion(driver, region2);

			// Add areaOwner
			fc.utobj().printTestStep("Add Area Owner");
			fc.utobj().clickElement(driver, objAreaOwnerPage.lnkAreaOwner);
			regional.addAreaRegionExistingOwner(driver, region1, lstOwner.get("FirstName"));

			// Search from franchisees with area owner filter
			fc.utobj().printTestStep("Verify search page with area owner filter");
			fc.infomgr().infomgr_common().InfoMgrFranchisees(driver);
			boolean isShowFilterPresent = fc.utobj().assertLinkText(driver, "Show Filters");
			if (isShowFilterPresent) {
				fc.utobj().sleep();
				fc.utobj().clickLink(driver, "Show Filters");
			}
			InfoMgrSearchFilterPage searchPage = new InfoMgrFranchiseesPage(driver).getSearchFilterPage();
			fc.utobj().setToDefault(driver, searchPage.drpAreaOwner);
			fc.utobj().selectValFromMultiSelect(driver, searchPage.drpAreaOwner, lstOwner.get("FirstName") + " " + lstOwner.get("LastName"));
			fc.utobj().clickElement(driver, searchPage.btnSearch);

			boolean region1Displayed = fc.utobj().assertLinkText(driver, franchise1);
			boolean region2Displayed = fc.utobj().assertLinkText(driver, franchise2);

			if (region1Displayed && region2Displayed) {
				Reporter.log("Both the additions are displayed on the page");
			} else {
				fc.utobj().throwsException("Search by Area Owner region is not working properyly. Test case fails !!! ");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Verify_InfoMgr_Franchisees_Actions_SendMail", testCaseDescription = "Test to verify send mail functionality from action buttons.", reference = { "" })
	public void InfoMgr_Franchisees_ActionsMenu_Actions_SendMail() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		FranchiseesCommonMethods franchisees = fc.infomgr().franchisees();
		AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();
		InfoMgrFranchiseesPage franchiseesPage = new InfoMgrFranchiseesPage(driver);
		InfoMgrContactHistoryPage contactHistoryPage = franchiseesPage.getContactHistoryPage();

		try {
			fc.loginpage().login(driver);

			fc.utobj().printTestStep("Add Franchise Location");
			String franchiseID = adminInfoMgr.addFranchiseLocation(driver);
			// franchisees.franchiseesPageShowAll(driver);
			franchisees.searchFranchise(driver, franchiseID);
			fc.utobj().printTestStep("Clicking on Send mail link under action image button ");
			fc.utobj().actionImgOption(driver, franchiseID, "Send Email");
			String mailSubject = franchisees.sendTextEmail(driver, config, false);
			franchisees.searchFranchiseAndClick(driver, franchiseID);
			fc.utobj().clickElement(driver, contactHistoryPage.lnkContactHistory);
			boolean assertMailSubjectonPage = fc.utobj().assertPageSource(driver, mailSubject);
			if (assertMailSubjectonPage) {
				Reporter.log("Text mail has been sent successfully");
			} else {
				fc.utobj().throwsException("Text mail has not  been sent .");
			}

			fc.utobj().printTestStep("Send mail from action button displayed at the bottom of the page");
			// franchisees.franchiseesPageShowAll(driver);
			franchisees.searchFranchise(driver, franchiseID);
			franchisees.checkLocation(driver, franchiseID);
			fc.utobj().clickElement(driver, franchiseesPage.btnSendMail);
			String mailSubject2 = franchisees.sendTextEmail(driver, config, false);
			franchisees.searchFranchiseAndClick(driver, franchiseID);
			fc.utobj().clickElement(driver, contactHistoryPage.lnkContactHistory);
			boolean assertMailSubjectonPage2 = fc.utobj().assertPageSource(driver, mailSubject2);
			if (assertMailSubjectonPage2) {
				Reporter.log("Text mail has been sent successfully");
			} else {
				fc.utobj().throwsException("Text mail has not  been sent .");
			}

			/*
			 * fc.utobj().printTestStep(testCaseId, "Click on Send Email button"
			 * ); franchisees.franchiseesPageShowAll(driver);
			 * franchisees.checkLocation(driver, franchiseID);
			 * fc.utobj().selectActionMenuItemsWithTagA(driver, "Send Email");
			 * String mailSubject3 = franchisees.sendTextEmail(driver, config ,
			 * false); franchisees.searchFranchiseAndClick(driver, franchiseID);
			 * fc.utobj().clickElement(driver,
			 * contactHistoryPage.lnkContactHistory); boolean
			 * assertMailSubjectonPage3 = fc.utobj().assertPageSource(driver,
			 * mailSubject3); if(assertMailSubjectonPage3) { Reporter.log(
			 * "Text mail has been sent successfully"); } else {
			 * fc.utobj().throwsException("Text mail has not  been sent ."); }
			 */

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr", "IMFailedTC" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Verify_InfoMgr_Franchisees_Actions_LogaTask", testCaseDescription = "Test to verify Log a task functionality from action buttons ", reference = { "" })
	public void InfoMgr_Franchisees_PageBottom_Actions_LogaTask() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		FranchiseesCommonMethods franchisees = fc.infomgr().franchisees();
		AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();

		InfoMgrFranchiseesPage franchiseesPage = new InfoMgrFranchiseesPage(driver);
		InfoMgrContactHistoryPage contactHistoryPage = franchiseesPage.getContactHistoryPage();

		try {
			fc.loginpage().login(driver);

			fc.utobj().printTestStep("Add corporate user");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail("infomgrautomation@staffex.com");
			corpUser = corpTest.createDefaultUser(driver, corpUser);

			fc.utobj().printTestStep("Add Franchise Location");
			String franchiseID = adminInfoMgr.addFranchiseLocation(driver);
			franchisees.franchiseesPageShowAll(driver);

			fc.utobj().printTestStep("Click on Log a Task button displayed at the bottom of the page");
			franchisees.checkLocation(driver, franchiseID);
			fc.utobj().clickElement(driver, franchiseesPage.btnLogATask);
			String taskSubject1 = franchisees.logaTask(driver, corpUser.getuserFullName(), false);
			franchisees.searchFranchiseAndClick(driver, franchiseID);
			fc.utobj().clickElement(driver, contactHistoryPage.lnkContactHistory);
			boolean assertTaskSubjectonPage1 = fc.utobj().assertPageSource(driver, taskSubject1);
			if (assertTaskSubjectonPage1) {
				Reporter.log("Log a Task has been done successfully");
			} else {
				fc.utobj().throwsException("Log a Task failed .");
			}

			fc.utobj().printTestStep("Log a task from action image icon");
			franchisees.franchiseesPageShowAll(driver);
			fc.utobj().actionImgOption(driver, franchiseID, "Log a Task");
			String taskSubject2 = franchisees.logaTask(driver, corpUser.getuserFullName(), false);
			franchisees.searchFranchiseAndClick(driver, franchiseID);
			fc.utobj().clickElement(driver, contactHistoryPage.lnkContactHistory);
			boolean assertTaskSubjectonPage2 = fc.utobj().assertPageSource(driver, taskSubject2);
			if (assertTaskSubjectonPage2) {
				Reporter.log("Log a Task has been done successfully");
			} else {
				fc.utobj().throwsException("Log a Task failed .");
			}

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Click on Log a Task link displayed under the action button click at the top of the page"
			 * ); franchisees.franchiseesPageShowAll(driver);
			 * franchisees.checkLocation(driver, franchiseID);
			 * fc.utobj().selectActionMenuItemsWithTagA(driver, "Log a Task");
			 * String taskSubject3 = franchisees.logaTask(driver, config,
			 * corporateUser , false);
			 * franchisees.searchFranchiseAndClick(driver, franchiseID);
			 * fc.utobj().clickElement(driver,
			 * contactHistoryPage.lnkContactHistory); boolean
			 * assertTaskSubjectonPage3 = fc.utobj().assertPageSource(driver,
			 * taskSubject3); if(assertTaskSubjectonPage3) { Reporter.log(
			 * "Log a Task has been done successfully"); } else {
			 * fc.utobj().throwsException("Log a Task failed ."); }
			 */

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_Verify_InfoMgr_Franchisees_Actions_LogaCall", testCaseDescription = "Test to verify log a call functionality from action buttons", reference = { "" })
	public void InfoMgr_Franchisees_Actions_LogaCall() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		FranchiseesCommonMethods franchisees = fc.infomgr().franchisees();
		AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();

		InfoMgrFranchiseesPage franchiseesPage = new InfoMgrFranchiseesPage(driver);
		InfoMgrContactHistoryPage contactHistoryPage = franchiseesPage.getContactHistoryPage();
		try {
			fc.loginpage().login(driver);
			// adminInfoMgr.configureAllFieldsOnManageFormGeneratorPage(driver ,
			// config);

			String franchiseID = adminInfoMgr.addFranchiseLocation(driver);
			// franchisees.franchiseesPageShowAll(driver);
			imc.InfoMgrFranchisees(driver);
			imc.searchLocationFromShowFilter(driver, franchiseID);
			fc.utobj().printTestStep("Click on Log a Call button displayed at the bottom of the page ");
			franchisees.checkLocation(driver, franchiseID);
			fc.utobj().clickElement(driver, franchiseesPage.btnLogACall);
			String logACallSubject1 = franchisees.logACall(driver, false);
			franchisees.searchFranchiseAndClick(driver, franchiseID);
			fc.utobj().clickElement(driver, contactHistoryPage.lnkContactHistory);
			boolean assertCallTextOnPage1 = fc.utobj().assertPageSource(driver, logACallSubject1);
			if (assertCallTextOnPage1) {
				Reporter.log("Log a Call has been done successfully");
			} else {
				fc.utobj().throwsException("Log a call failed .");
			}

			fc.utobj().printTestStep("Log a call from action menu");
			// franchisees.franchiseesPageShowAll(driver);
			imc.InfoMgrFranchisees(driver);
			imc.searchLocationFromShowFilter(driver, franchiseID);
			fc.utobj().actionImgOption(driver, franchiseID, "Log a Call");
			String logACallSubject2 = franchisees.logACall(driver, false);
			franchisees.searchFranchiseAndClick(driver, franchiseID);
			fc.utobj().clickElement(driver, contactHistoryPage.lnkContactHistory);
			boolean assertCallTextOnPage2 = fc.utobj().assertPageSource(driver, logACallSubject2);
			if (assertCallTextOnPage2) {
				Reporter.log("Log a Call has been done successfully");
			} else {
				fc.utobj().throwsException("Log a call failed .");
			}

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Click on Log a Call link displayed under the top action button"
			 * ); franchisees.franchiseesPageShowAll(driver);
			 * franchisees.checkLocation(driver, franchiseID);
			 * fc.utobj().selectActionMenuItemsWithTagA(driver, "Log a Call");
			 * String logACallSubject3 = franchisees.logACall(driver, config ,
			 * false); franchisees.searchFranchiseAndClick(driver, franchiseID);
			 * fc.utobj().clickElement(driver,
			 * contactHistoryPage.lnkContactHistory); boolean
			 * assertCallTextOnPage3 = fc.utobj().assertPageSource(driver,
			 * logACallSubject3); if(assertCallTextOnPage3) { Reporter.log(
			 * "Log a Call has been done successfully"); } else {
			 * fc.utobj().throwsException("Log a call failed ."); }
			 */
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" })

	// Update Script by Govind

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-10-26", testCaseId = "TC_Verify_InfoMgr_Franchisees_Actions_AddToGroup", testCaseDescription = "Test to verify Add to Group functionality from action buttons", reference = { "" })
	public void InfoMgr_Franchisees_Actions_AddToGroup() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		FranchiseesCommonMethods franchisees = fc.infomgr().franchisees();
		AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();

		InfoMgrFranchiseesPage franchiseesPage = new InfoMgrFranchiseesPage(driver);
		try {
			fc.loginpage().login(driver);

			String franchiseID = adminInfoMgr.addFranchiseLocationForInfoMgrName(driver, false);
			// String franchiseID = adminInfoMgr.addFranchiseLocation(driver);
			imc.InfoMgrFranchisees(driver);
			imc.searchLocationFromShowFilter(driver, franchiseID);

			fc.utobj().printTestStep("Click on Add to Group button displayed at the bottom of the page");
			franchisees.checkLocation(driver, franchiseID);
			fc.utobj().clickElement(driver, franchiseesPage.btnAddtoGroup);
			franchisees.addToGroup(driver, config);

			fc.utobj().printTestStep("Click on Add to Group link displayed under Action button displayed at the top of the page");
			imc.InfoMgrFranchisees(driver);
			imc.searchLocationFromShowFilter(driver, franchiseID);
			franchisees.checkLocation(driver, franchiseID);
			fc.utobj().selectActionMenuItemsWithTagA(driver, "Add To Group");
			franchisees.addToGroup(driver, config);

			fc.utobj().printTestStep("Click on Add to Group button  displayed at the bottom of the page");
			// franchisees.franchiseesPageShowAll(driver);
			imc.InfoMgrFranchisees(driver);
			imc.searchLocationFromShowFilter(driver, franchiseID);
			franchisees.checkLocation(driver, franchiseID);
			fc.utobj().clickElement(driver, franchiseesPage.btnAddtoGroup);
			franchisees.addToGroup(driver, config);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Verify_InfoMgr_Franchisees_Actions_Print", testCaseDescription = "Test to verify print functionality from action buttons", reference = { "" })
	public void InfoMgr_Franchisees_Actions_Print() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		FranchiseesCommonMethods franchisees = fc.infomgr().franchisees();
		AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();
		InfoMgrFranchiseesPage franchiseesPage = new InfoMgrFranchiseesPage(driver);

		try {
			fc.loginpage().login(driver);
			// adminInfoMgr.configureAllFieldsOnManageFormGeneratorPage(driver ,
			// config);

			String franchiseID = adminInfoMgr.addFranchiseLocation(driver);
			franchisees.franchiseesPageShowAll(driver);

			fc.utobj().printTestStep("Click on Print button displayed at the bottom of the page");
			fc.utobj().clickElement(driver, franchiseesPage.btnPrint);
			franchisees.VerifyPrintPreview(driver, franchiseID);

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Click on Print link displayed under Action button displayed at the top of the page"
			 * ); fc.utobj().selectActionMenuItemsWithTagA(driver, "Print");
			 * franchisees.VerifyPrintPreview(driver, franchiseID);
			 */

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-30", testCaseId = "TC_Verify_InfoMgr_Franchisees_Actions_Modify", testCaseDescription = "Test to verify modify functionality from action buttons", reference = { "" })
	public void InfoMgr_Franchisees_Actions_Modify() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		FranchiseesCommonMethods franchisees = fc.infomgr().franchisees();
		AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();

		try {
			fc.loginpage().login(driver);
			// adminInfoMgr.configureAllFieldsOnManageFormGeneratorPage(driver ,
			// config);

			String franchiseID = adminInfoMgr.addFranchiseLocation(driver);
			franchisees.franchiseesPageShowAll(driver);

			fc.utobj().printTestStep("Modify from action menu");
			fc.utobj().actionImgOption(driver, franchiseID, "Modify");
			franchisees.modifyCenterInfo(driver, config);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr","Summary_Filter" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-16", testCaseId = "TC_Verify_InfoMgr_Franchisees_Filter", testCaseDescription = "Test to verify filter displayed on the franchisees Page", reference = { "" })
	public void InfoMgr_Franchisee_Filter_Verify() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		FranchiseesCommonMethods franchisees = fc.infomgr().franchisees();
		RegionalCommonMethods regional = new RegionalCommonMethods();
		AdminInfoMgrCommonMethods adminInfoMgr = new AdminInfoMgrCommonMethods();

		try {
			fc.loginpage().login(driver);

			InfoMgrFranchiseeFilter franchisee = adminInfoMgr.addFranchiseLocationForFilter(driver, false);
			//franchisees.franchiseesPageShowAll(driver);
			imc.InfoMgrFranchisees(driver);
			imc.searchLocationFromShowFilter(driver, franchisee.getFranchiseeID());
			fc.utobj().actionImgOption(driver, franchisee.getFranchiseeID(), "Modify");
			fc.utobj().printTestStep("Adding spouse details by modifying the owner");
			InfoMgrOwnersPage ownerPage = new InfoMgrFranchiseesPage(driver).getOwnersPage();
			fc.utobj().clickElement(driver, ownerPage.ownersTab);
			fc.utobj().actionImgOption(driver, franchisee.getOwnerFirstName(), "Modify");
			Map<String, String> lstOwner = franchisees.modifyOwner(driver, config);
			franchisee.setSpouseFirstName(lstOwner.get("SpouseFirstName"));

			fc.utobj().printTestStep("Adding area owner");
			InfoMgrOwnersPage regionalOwnerPage = new InfoMgrRegionalPage(driver).getOwnersPage();
			regional.searchAndClickAreaRegion(driver, franchisee.getAreaRegionName());
			fc.utobj().clickElement(driver, regionalOwnerPage.lnkAreaOwner);
			lstOwner = regional.addAreaRegionOwner(driver);
			franchisee.setAreaRegionOwner(lstOwner.get("FirstName"));

			System.out.println(franchisee.getStoreTypeID());

			fc.utobj().printTestStep("Verify Search Filter");
			fc.infomgr().infomgr_common().InfoMgrFranchisees(driver);
			franchisees.search(driver, config, franchisee);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	//
	@Test(groups = {  "infomgr"})
	@TestCase(createdOn = "2018-09-09", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_Franchisee_Agreement_Audit_Trigger", testCaseDescription = "This test case is used to verify data addition , modification and deletion of Agreement tab of Info Mgr and Verify audit History and Email triggers .", reference = { "" })
	public void InfoMgr_Franchisees_Agreement_Audit_Triggers() throws Exception {
		FranchiseeAPI fran = new FranchiseeAPI();
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		AdminInfoMgrCommonMethods adminInfoMgr = new AdminInfoMgrCommonMethods();
		CorporateLocationsCommonMethods corporateLocations = fc.infomgr().corporateLocations();
		CorporateLocationsCommonMethods franchise = fc.infomgr().corporateLocations();
		InfoMgr_Common cmn = new InfoMgr_Common();
		InfoMgrAgreementPage infoMgrAgreementPage = new InfoMgrCorporateLocationsPage(driver).getAgreementPage();
		InfoMgrCenterInfoPage centerInfoPage = new InfoMgrCorporateLocationsPage(driver).getCenterInfoPage();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		try {

			fc.utobj().printTestStep("Logging In FCSKY ");
			fc.loginpage().login(driver);

			// ---------------------------Add ARea Region------

			fc.utobj().printTestStep("Add Area / Region ");
			String areaRegionID = adminInfoMgr.addAreaRegion(driver);
			Reporter.log("Area Region - " + areaRegionID + "created");

			System.out.println("Area Region created : " + areaRegionID);
			String franchiseID = fran.createDefaultFranchiseeAPI(areaRegionID, "Yes");
			System.out.println("franchise created : " + franchiseID);
			Reporter.log("Franchise is Created" + franchiseID);
			// ---------------------------create corporate user
			CorporateUserAPI cuser = fran.createDefaultCorporateUser();
			String corporateUserName = cuser.getFirstname();
			fc.utobj().printTestStep("corporateUser is Created" + corporateUserName);

			fc.utobj().printTestStep("enabling audit history and triggers");
			cmn.enableAuditHistoryAndTrigger(driver, cmn, corporateUserName, EMAIL);

			// ------------------------------Add Agreement
			franchise.searchFranchiseAndClick(driver, franchiseID);
			fc.utobj().clickElement(driver, centerInfoPage.lnkAgreement);
			fc.utobj().printTestStep("Add data to Agreement tab");
			Map<String, String> fieldLabelValue = corporateLocations.addAgreementAndGetFieldLabels(driver, config, "Franchisee");
			String currentDate =fc.utobj().getCurrentDateUSTimeZone();
			fc.utobj().selectDropDown(driver, infoMgrAgreementPage.historyDD, "View Audit History");
			cmn.validateFormAuditHistory(driver, AGREEMENT, RECORDADDED, currentDate);
			cmn.validateFieldAuditHistory(driver, fieldLabelValue, RECORDADDED, currentDate);

			// ----------------------------- Modify Agreement
			franchise.searchFranchiseAndClick(driver, franchiseID);
			fc.utobj().clickElement(driver, centerInfoPage.lnkAgreement);
			fc.utobj().printTestStep("Modify data to Agreement tab");
			Map<String, String> fieldLabelValueMod = corporateLocations.ModifyAgreementAndGetFieldLabels(driver, config, "Franchisee");

			fc.utobj().selectDropDown(driver, infoMgrAgreementPage.historyDD, "View Audit History");
			currentDate = fc.utobj().getCurrentDateUSTimeZone();
			cmn.validateFormAuditHistory(driver, AGREEMENT, RECORDMODIFIED, currentDate);
			cmn.validateFieldAuditHistory(driver, fieldLabelValueMod, RECORDMODIFIED, currentDate);

			// ---------------------------Delete Agreement

			franchise.searchFranchiseAndClick(driver, franchiseID);
			fc.utobj().printTestStep("Deleting Agreement TAB");
			fc.utobj().clickElement(driver, infoMgrAgreementPage.lnkAgreement);
			fc.utobj().clickLink(driver, "Delete");
			fc.utobj().acceptAlertBox(driver);
			fc.utobj().selectDropDown(driver, infoMgrAgreementPage.historyDD, "View Audit History");
			currentDate =fc.utobj().getCurrentDateUSTimeZone();
			cmn.validateFormAuditHistory(driver, AGREEMENT, RECORDDELETED, currentDate);
			cmn.validateTriggerAlertMailForForm(AGREEMENT, franchiseID, EMAIL, PASSWORD);
			cmn.validateTriggerAlertMailForForm(AGREEMENT, franchiseID, cuser.getEmail(), PASSWORD);

			Iterator<Map.Entry<String, String>> itr1 = fieldLabelValue.entrySet().iterator();

			while (itr1.hasNext()) {
				Map.Entry<String, String> entry = itr1.next();

				fc.utobj().printTestStep("Validate  Add Event Field Level Triggers Alert Email for" + entry.getKey());

				cmn.validateTriggerAlertMailForField(AGREEMENT, franchiseID, EMAIL, PASSWORD, entry.getKey(), entry.getValue());

				cmn.validateTriggerAlertMailForField(AGREEMENT, franchiseID, cuser.getEmail(), PASSWORD, entry.getKey(), entry.getValue());
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {  "infomgr"})
	@TestCase(createdOn = "2018-09-09", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_CustomTab_Audit_trigger", testCaseDescription = "This test case is used to verify data addition , modification and deletion of custom tab of Info Mgr > Franchisees and validate Audit History and Email Trigger.", reference = { "" })
	public void InfoMgr_CustomTab_Audit_Trigger() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		FranchiseeAPI fran = new FranchiseeAPI();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		AdminInfoMgrCommonMethods adminInfoMgr = new AdminInfoMgrCommonMethods();
		InfoMgrCustomTabPage ctab = new InfoMgrFranchiseesPage(driver).getCustomTabPage();
		CorporateLocationsCommonMethods franchise = fc.infomgr().corporateLocations();
		InfoMgr_Common cmn = new InfoMgr_Common();
		CorporateLocationsCommonMethods corporateLocations = fc.infomgr().corporateLocations();
		Map<String, String> newmap = new HashMap<String, String>();
		try {
			String currentDate  = fc.utobj().getCurrentDateUSTimeZone();
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Generate  Section Names");
			String tabSection = "TABULARSECTION";
			String regSection = "REGULARLARSECTION";
			tabSection = tabSection + fc.utobj().generateRandomNumber();
			regSection = regSection + fc.utobj().generateRandomNumber();

			fc.utobj().printTestStep("Add new Region and Franchise");
			String areaRegionID = adminInfoMgr.addAreaRegion(driver);
			fc.utobj().printTestStep("Add Area / Region ");
			Reporter.log("Area Region - " + areaRegionID + "created");
			fc.utobj().printTestStep("Create Default Franchisee");
			String franchiseID = fran.createDefaultFranchiseeAPI(areaRegionID, "Yes");

			fc.utobj().printTestStep("Create Corporate User using API");
			CorporateUserAPI cuser = fran.createDefaultCorporateUser();
			String corporateUserName = cuser.getFirstname();

			fc.utobj().printTestStep("Add new Custom tab");
			String newTab = adminInfoMgr.addNewTab(driver, "Franchisee");
			ArrayList<Map<String, String>> list = adminInfoMgr.addSectionAndFieldsToTab(driver, "Franchisee", newTab, regSection);
			ArrayList<Map<String, String>> tablist = adminInfoMgr.addSectionAndFieldsToTab(driver, "Franchisee", newTab, tabSection);
			Map<String, String> regFieldAdd = list.get(0);
			Map<String, String> regFieldModify = list.get(1);
			Map<String, String> tabFieldAdd = tablist.get(0);
			Map<String, String> tabFieldModify = tablist.get(1);
			newmap.putAll(tabFieldAdd);
			newmap.putAll(regFieldAdd);

			adminInfoMgr.enableAuditHistoryAndTriggercustomtab(driver, newTab, cmn, corporateUserName, newmap, EMAIL);

			fc.utobj().printTestStep("Add data for custom Tab in Regular Section");
			franchise.searchFranchiseAndClick(driver, franchiseID);
			
			corporateLocations.addCustomTabData(driver, config, "Franchisee", regFieldAdd, newTab, REGULAR, regSection, ADD);
			fc.utobj().printTestStep("Add data for custom Tab in Tabular Section");
			corporateLocations.addCustomTabData(driver, config, "Franchisee", tabFieldAdd, newTab, TABULAR, tabSection, ADD);

			fc.utobj().selectDropDown(driver, ctab.selectHistory, "View Audit History");
			currentDate = fc.utobj().getCurrentDateUSTimeZone();
			cmn.validateFormAuditHistory(driver, newTab, RECORDADDED, currentDate);
			cmn.validateFieldAuditHistory(driver, newmap, RECORDADDED, currentDate);
			cmn.validateTriggerAlertMailForForm(newTab, franchiseID, EMAIL, PASSWORD);
			Iterator<Map.Entry<String, String>> itr2 = newmap.entrySet().iterator();

			while (itr2.hasNext()) {
				Map.Entry<String, String> entry = itr2.next();

				fc.utobj().printTestStep("Validate  Add Event Field Level Triggers Alert Email for" + entry.getKey());

				cmn.validateTriggerAlertMailForField(newTab, franchiseID, EMAIL, PASSWORD, entry.getKey(), entry.getValue());
			}

			fc.utobj().printTestStep("Modifying Custom TAB data" + newTab);
			franchise.searchFranchiseAndClick(driver, franchiseID);
			corporateLocations.addCustomTabData(driver, config, "Franchisee", regFieldModify, newTab, REGULAR, regSection, MODIFY);
			corporateLocations.addCustomTabData(driver, config, "Franchisee", tabFieldModify, newTab, TABULAR, tabSection, MODIFY);
			fc.utobj().selectDropDown(driver, ctab.selectHistory, "View Audit History");
			currentDate = fc.utobj().getCurrentDateUSTimeZone();
			cmn.validateFormAuditHistory(driver, newTab, RECORDMODIFIED, currentDate);
			cmn.validateFieldAuditHistory(driver, tabFieldModify, RECORDMODIFIED, currentDate);
			cmn.validateFieldAuditHistory(driver, regFieldModify, RECORDMODIFIED, currentDate);

			fc.utobj().printTestStep("Deleting Custom TAB data" + newTab);
			franchise.searchFranchiseAndClick(driver, franchiseID);
			fc.utobj().clickLink(driver, newTab);
			fc.utobj().actionImgOption(driver, ctab.date.getText(), "Delete");
			fc.utobj().acceptAlertBox(driver);
			fc.utobj().clickLink(driver, "Delete");
			currentDate =fc.utobj().getCurrentDateUSTimeZone();
			fc.utobj().acceptAlertBox(driver);
			fc.utobj().selectDropDown(driver, ctab.selectHistory, "View Audit History");

			cmn.validateFormAuditHistory(driver, newTab, RECORDDELETED, currentDate);
			cmn.validateTriggerAlertMailForForm(newTab, franchiseID, EMAIL, PASSWORD);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}

}
