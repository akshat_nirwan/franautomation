package com.builds.test.infomgr;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.uimaps.infomgr.InfoMgrGroupsPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class InfoMgrGroupsPageTest {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Verify_InfoMgr_Groups_Add_Public_Group", testCaseDescription = "Test to verify public group add functionality", reference = {
			"" })
	public void InfoMgr_Groups_Add_Public_Group() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		GroupsCommonMethods groups = fc.infomgr().groups();
		InfoMgrGroupsPage groupsPage = new InfoMgrGroupsPage(driver);

		try {
			fc.loginpage().login(driver);

			fc.infomgr().infomgr_common().InfoMgrGroups(driver);
			fc.utobj().printTestStep("Create Public Group");
			fc.utobj().clickElement(driver, groupsPage.btnAddGroups);
			groups.addPublicGroup(driver, config);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Verify_InfoMgr_Groups_Add_Private_Group", testCaseDescription = "Test to verify private group add functionality", reference = {
			"" })
	public void InfoMgr_Groups_Add_Private_Group() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		GroupsCommonMethods groups = fc.infomgr().groups();
		InfoMgrGroupsPage groupsPage = new InfoMgrGroupsPage(driver);

		try {
			fc.loginpage().login(driver);

			fc.infomgr().infomgr_common().InfoMgrGroups(driver);
			fc.utobj().printTestStep("Create Private Group");
			fc.utobj().clickElement(driver, groupsPage.btnAddGroups);
			groups.addPrivateGroup(driver, config);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Verify_InfoMgr_Groups_PrintButton", testCaseDescription = "Test to verify print functionality", reference = {
			"" })
	public void InfoMgr_Groups_Print() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		GroupsCommonMethods groups = fc.infomgr().groups();

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		InfoMgrGroupsPage groupsPage = new InfoMgrGroupsPage(driver);

		try {
			fc.loginpage().login(driver);

			fc.infomgr().infomgr_common().InfoMgrGroups(driver);
			fc.utobj().printTestStep("Create Public Group");
			fc.utobj().clickElement(driver, groupsPage.btnAddGroups);
			String txtGroupName = groups.addPublicGroup(driver, config);
			fc.utobj().printTestStep("Verify Print");
			fc.utobj().clickElement(driver, groupsPage.btnPrint);
			groups.VerifyPrintPreview(driver, txtGroupName);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Verify_InfoMgr_Groups_DeleteButton", testCaseDescription = "Test to verify delete functionality", reference = {
			"" })
	public void InfoMgr_Groups_DeleteButton() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		GroupsCommonMethods groups = fc.infomgr().groups();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		InfoMgrGroupsPage groupsPage = new InfoMgrGroupsPage(driver);

		try {
			fc.loginpage().login(driver);
			fc.infomgr().infomgr_common().InfoMgrGroups(driver);
			fc.utobj().printTestStep("Create Public Group");
			fc.utobj().clickElement(driver, groupsPage.btnAddGroups);
			String txtGroupName = groups.addPublicGroup(driver, config);
			fc.utobj().printTestStep("Delete Group");
			groups.deleteGroup(driver, config, txtGroupName, false);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Verify_InfoMgr_Groups_ActionLink_AssociatewithFranchisees", testCaseDescription = "Test to verify delete functionality", reference = {
			"" })
	public void InfoMgr_Groups_ActionLink_AssociatewithFranchisees() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		GroupsCommonMethods groups = fc.infomgr().groups();
		AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		InfoMgrGroupsPage groupsPage = new InfoMgrGroupsPage(driver);

		try {
			fc.loginpage().login(driver);
			String franchiseID = adminInfoMgr.addFranchiseLocation(driver);
			fc.infomgr().infomgr_common().InfoMgrGroups(driver);
			fc.utobj().printTestStep("Create Public Group");
			fc.utobj().clickElement(driver, groupsPage.btnAddGroups);
			String txtGroupName = groups.addPublicGroup(driver, config);
			fc.utobj().printTestStep("Associateing with franchiseID - " + franchiseID);
			groups.associateWithFranchisee(driver, config, franchiseID, txtGroupName);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Verify_InfoMgr_Groups_ActionLink_Delete", testCaseDescription = "Test to verify delete functionality", reference = {
			"" })
	public void TC_Verify_InfoMgr_Groups_ActionLink_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		GroupsCommonMethods groups = fc.infomgr().groups();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		InfoMgrGroupsPage groupsPage = new InfoMgrGroupsPage(driver);

		try {
			fc.loginpage().login(driver);
			fc.infomgr().infomgr_common().InfoMgrGroups(driver);
			fc.utobj().printTestStep("Create Public Group");
			fc.utobj().clickElement(driver, groupsPage.btnAddGroups);
			String txtGroupName = groups.addPublicGroup(driver, config);
			fc.utobj().printTestStep("Delete Group");
			groups.deleteGroup(driver, config, txtGroupName, true);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Verify_InfoMgr_Groups_ArchiveButton", testCaseDescription = "Test to verify archive functionality", reference = {
			"" })
	public void TC_Verify_InfoMgr_Groups_ArchiveButton() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		GroupsCommonMethods groups = fc.infomgr().groups();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		InfoMgrGroupsPage groupsPage = new InfoMgrGroupsPage(driver);

		try {
			fc.loginpage().login(driver);
			fc.infomgr().infomgr_common().InfoMgrGroups(driver);
			fc.utobj().printTestStep("Create Public Group");
			fc.utobj().clickElement(driver, groupsPage.btnAddGroups);
			String txtGroupName = groups.addPublicGroup(driver, config);
			fc.utobj().printTestStep("Archive Group");
			groups.archiveGroup(driver, config, txtGroupName, false);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Verify_InfoMgr_Groups_ActionLink_Archive", testCaseDescription = "Test to verify Archive functionality", reference = {
			"" })
	public void TC_Verify_InfoMgr_Groups_ActionLink_Archive() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		GroupsCommonMethods groups = fc.infomgr().groups();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		InfoMgrGroupsPage groupsPage = new InfoMgrGroupsPage(driver);

		try {
			fc.loginpage().login(driver);
			fc.infomgr().infomgr_common().InfoMgrGroups(driver);
			fc.utobj().printTestStep("Create Public Group");
			fc.utobj().clickElement(driver, groupsPage.btnAddGroups);
			String txtGroupName = groups.addPublicGroup(driver, config);
			fc.utobj().printTestStep("Archive Group");
			groups.archiveGroup(driver, config, txtGroupName, true);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Verify_InfoMgr_Groups_RestoreButton", testCaseDescription = "Test to verify restore functionality", reference = {
			"" })
	public void TC_Verify_InfoMgr_Groups_RestoreButton() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		GroupsCommonMethods groups = fc.infomgr().groups();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		InfoMgrGroupsPage groupsPage = new InfoMgrGroupsPage(driver);

		try {
			fc.loginpage().login(driver);
			fc.infomgr().infomgr_common().InfoMgrGroups(driver);
			fc.utobj().printTestStep("Create Public Group");
			fc.utobj().clickElement(driver, groupsPage.btnAddGroups);
			String txtGroupName = groups.addPublicGroup(driver, config);
			fc.utobj().printTestStep("Archive Group");
			groups.archiveGroup(driver, config, txtGroupName, false);
			fc.utobj().printTestStep("Restore Group");
			groups.restoreGroup(driver, config, txtGroupName, false);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Verify_InfoMgr_Groups_ActionLink_Restore", testCaseDescription = "Test to verify restore functionality", reference = {
			"" })
	public void TC_Verify_InfoMgr_Groups_ActionLink_Restore() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		GroupsCommonMethods groups = fc.infomgr().groups();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		InfoMgrGroupsPage groupsPage = new InfoMgrGroupsPage(driver);

		try {
			fc.loginpage().login(driver);
			fc.infomgr().infomgr_common().InfoMgrGroups(driver);
			fc.utobj().printTestStep("Create Public Group");
			fc.utobj().clickElement(driver, groupsPage.btnAddGroups);
			String txtGroupName = groups.addPublicGroup(driver, config);
			fc.utobj().printTestStep("Archive Group");
			groups.archiveGroup(driver, config, txtGroupName, true);
			fc.utobj().printTestStep("Restore Group");
			groups.restoreGroup(driver, config, txtGroupName, true);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Verify_InfoMgr_Groups_Modify", testCaseDescription = "Test to verify  modify functionality", reference = {
			"" })
	public void TC_Verify_InfoMgr_Groups_Modify() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		GroupsCommonMethods groups = fc.infomgr().groups();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		InfoMgrGroupsPage groupsPage = new InfoMgrGroupsPage(driver);

		try {
			fc.loginpage().login(driver);
			fc.infomgr().infomgr_common().InfoMgrGroups(driver);
			fc.utobj().printTestStep("Create Public Group");
			fc.utobj().clickElement(driver, groupsPage.btnAddGroups);
			String txtGroupName = groups.addPublicGroup(driver, config);
			fc.utobj().printTestStep("Modify Group");
			fc.utobj().actionImgOption(driver, txtGroupName, "Modify");
			groups.modifyGroup(driver, config);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Verify_InfoMgr_Groups_Search", testCaseDescription = "Test to verify  search functionality", reference = {
			"" })
	public void TC_Verify_InfoMgr_Groups_Search() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		GroupsCommonMethods groups = fc.infomgr().groups();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		InfoMgrGroupsPage groupsPage = new InfoMgrGroupsPage(driver);

		try {
			fc.loginpage().login(driver);
			fc.infomgr().infomgr_common().InfoMgrGroups(driver);
			fc.utobj().printTestStep("Create Public Group");
			fc.utobj().clickElement(driver, groupsPage.btnAddGroups);
			String txtGroupName = groups.addPublicGroup(driver, config);
			fc.utobj().printTestStep("Search Group");
			groups.search(driver, config, txtGroupName);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
}
