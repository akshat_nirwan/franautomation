package com.builds.test.infomgr;

import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.annotations.BeforeGroups;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminAreaRegionAddAreaRegionPageTest;
import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.admin.LocationData;
import com.builds.test.common.CorporateUser;
import com.builds.test.common.Location;
import com.builds.uimaps.infomgr.InfoMgrCenterInfoPage;
import com.builds.uimaps.infomgr.InfoMgrContactHistoryPage;
import com.builds.uimaps.infomgr.InfoMgrDashboardPage;
import com.builds.uimaps.infomgr.InfoMgrFranchiseesPage;
import com.builds.uimaps.infomgr.InfoMgrInDevelopmentPage;
import com.builds.uimaps.infomgr.InfoMgrOwnersPage;
import com.builds.uimaps.infomgr.InfoMgrRegionalPage;
import com.builds.uimaps.infomgr.InfoMgrUsersPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;


public class InfoMgrInDevelopmentPageTest {

	public static String flag = "true";
	public static final String franchiseID = "InDevFranchisee" + ThreadLocalRandom.current().nextInt(10000);
	public static final String INFOMGR="infomgr";
	//public static final String franchiseID = "InDevFranchisee6737";

	FranconnectUtil fc = new FranconnectUtil();
	InfoMgr_Common imc = new InfoMgr_Common();
	AdminInfoMgrCommonMethods adminInfoMgr = new AdminInfoMgrCommonMethods();
	CorporateUser corpUser = new CorporateUser();
	
	@BeforeGroups(groups = { "infomgr" ,"InDevFranchisee_Tabs_AMD","contract"})
	public synchronized void createIndevelopmentFranchise() throws Exception {
		

		
		if (flag == "true") {
			flag = "false";		
			String testCaseId = "BeforeGroupBlock to create Common InDevelopment franchisee Location";
			WebDriver driver = fc.commonMethods().browsers().openBrowser();
			System.out.println("inside before group method 'createIndevelopmentFranchise'");
			
			InDevelopmentCommonMethods inDevelopment = fc.infomgr().inDevelopment();
			fc.loginpage().login(driver);	
			Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
			fc.utobj().printTestStep("Add corporate user");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail("infomgrautomation@staffex.com");
			corpUser = corpTest.createDefaultUser(driver, corpUser);

			fc.utobj().printTestStep("Add in development location");
			 inDevelopment.addDevelopmentLocation(driver, config,franchiseID);
			 System.out.println(franchiseID);
			Reporter.log("FranchiseID - " + franchiseID + " Created");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		}
	}

	@Test(groups = {  "infomgr" ,"InDevFranchisee_Tabs_AMD" })
	// BugID - 69046 - Login with corporate user
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_InDevFranchisee_AgreementTab_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of Agreement tab of Info Mgr > inDevelopment.", reference = { "" })
	public void InfoMgr_InDevFranchisees_AgreementTab_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		InDevelopmentCommonMethods inDevelopment = fc.infomgr().inDevelopment();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		

		try {
			fc.loginpage().login(driver);
			InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();
			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");
			inDevelopment.searchFranchiseAndClick(driver, franchiseID);
			fc.utobj().printTestStep("Add Agreement Tab Data");
			// Add Agreement
			fc.utobj().clickElement(driver, centerInfoPage.lnkAgreement);
			driver = inDevelopment.addAgreement(driver, config);
			// Modify Agreement
			fc.utobj().printTestStep("Modify Agreement Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkAgreement);
			driver = inDevelopment.modifyAgreement(driver, config);
			// Delete Agreement
			fc.utobj().printTestStep("Delete Agreement Tab Data");
			inDevelopment.deleteTabData(driver, config, centerInfoPage.lnkAgreement, null);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" ,"InDevFranchisee_Tabs_AMD","Testddd"})

	// BugID - 69046 - Login with corporate user
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_InDevFranchisees_AddressTab_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of Address tab of Info Mgr > inDevelopment.", reference = { "" })
	public void InfoMgr_InDevFranchisees_AddressTab_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		InDevelopmentCommonMethods inDevelopment = fc.infomgr().inDevelopment();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		try {
			fc.loginpage().login(driver);
			InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();
			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");
			inDevelopment.searchFranchiseAndClick(driver, franchiseID);

			// Add Address
			fc.utobj().printTestStep("Add Address Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkAddresses);
			driver = inDevelopment.addAddresses(driver, config);
			// Modify Address
			fc.utobj().printTestStep("Modify Address Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkAddresses);
			driver = inDevelopment.modifyAddresses(driver, config);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {  "infomgr" ,"InDevFranchisee_Tabs_AMD" ,"1234"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_InDevFranchisee_ContactHistoryTab_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of ContactHistoryTab of Info Mgr > inDevelopment.", reference = { "" })
	public void InfoMgr_InDevFranchisees_ContactHistoryTab_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
	
		InfoMgrFranchiseesPage franchiseesPage = new InfoMgrFranchiseesPage(driver);
		InDevelopmentCommonMethods inDevelopment = fc.infomgr().inDevelopment();
		try {
			
			fc.loginpage().login(driver);
			InfoMgrContactHistoryPage contactHistoryPage = franchiseesPage.getContactHistoryPage();
			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");
			inDevelopment.searchFranchiseAndClick(driver, franchiseID);
			
			// Log a Task
			fc.utobj().clickElement(driver, contactHistoryPage.lnkContactHistory);

			fc.utobj().clickElement(driver, contactHistoryPage.btnLogaTask);
			String taskSubject = inDevelopment.logaTask(driver, config, corpUser.getuserFullName(), true);

			// Log a Call
			fc.utobj().clickElement(driver, contactHistoryPage.btnLogaCall);
			String callSubject = inDevelopment.logACall(driver, config, true);

			// Add Remark
			fc.utobj().clickElement(driver, contactHistoryPage.btnAddRemark);
			String remarkSubject = inDevelopment.addRemark(driver, config, contactHistoryPage);

			// Modify Contact History

			// Modify a task
			fc.utobj().clickElement(driver, contactHistoryPage.lnkContactHistory);
			fc.utobj().actionImgOption(driver, taskSubject, "Modify");
			inDevelopment.modifyATask(driver, config, true);

			// Modify a call
			fc.utobj().actionImgOption(driver, callSubject, "Modify");
			inDevelopment.modifyACall(driver, config, true);

			// Modify a remark
			fc.utobj().actionImgOption(driver, remarkSubject, "Modify");
			inDevelopment.modifyRemark(driver, config, contactHistoryPage);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" ,"InDevFranchisee_Tabs_AMD" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_InDevFranchisee_ContractSigning_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of ContractSigning tabs of Info Mgr > inDevelopment.", reference = { "" })
	public void InfoMgr_InDevFranchisees_ContractSigningTab_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		InDevelopmentCommonMethods inDevelopment = fc.infomgr().inDevelopment();
		
		try {
			fc.loginpage().login(driver);
			InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();
			// adminInfoMgr.configureInfoMgr(driver, config);
			
			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");
			inDevelopment.searchFranchiseAndClick(driver, franchiseID);
			// Adding Contract Signing
			fc.utobj().printTestStep("Add Contract History Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkContractSigning);
			inDevelopment.addContractSigning(driver, config);
			// Modify Contract Signing
			fc.utobj().printTestStep("Modify Contract History Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkContractSigning);
			inDevelopment.modifyContractSigning(driver, config);
			// Delete Contract Signing
			fc.utobj().printTestStep("Delete Contract History Tab Data");
			inDevelopment.deleteTabData(driver, config, centerInfoPage.lnkContractSigning, null);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" ,"InDevFranchisee_Tabs_AMD" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_InDevFranchisee_CustomerComplaint_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of CustomerComplaint tabs of Info Mgr > inDevelopment.", reference = { "" })
	public void InfoMgr_InDevFranchisees_CustomerComplaintTab_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		InDevelopmentCommonMethods inDevelopment = fc.infomgr().inDevelopment();
		try {

			fc.loginpage().login(driver);
			// adminInfoMgr.configureInfoMgr(driver, config);
			InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();
			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");
			inDevelopment.searchFranchiseAndClick(driver, franchiseID);

			// Add Customer Complaints
			fc.utobj().printTestStep("Add Customer Complaints Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkCustomerComplaints);
			inDevelopment.addCustomerComplaints(driver, config);

			// Modify Customer Complaints
			fc.utobj().printTestStep("Modify Customer Complaints Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkCustomerComplaints);
			inDevelopment.modifyCustomerComplaints(driver, config);
			// Delete
			fc.utobj().printTestStep("Delete Customer Complaints Tab Data");
			inDevelopment.deleteTabData(driver, config, centerInfoPage.lnkCustomerComplaints, null);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {  "infomgr" ,"InDevFranchisee_Tabs_AMD" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_InDevFranchisee_CenterInfo_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of _CenterInfo  tabs of Info Mgr > inDevelopment.", reference = { "" })
	public void InfoMgr_InDevFranchisees_CenterInfo_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();
		InDevelopmentCommonMethods inDevelopment = fc.infomgr().inDevelopment();
		try {
			fc.loginpage().login(driver);
			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");
			inDevelopment.searchFranchiseAndClick(driver, franchiseID);
			// Modify Center Info
			fc.utobj().printTestStep("Modify Center Info Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkCenterInfo);
			fc.utobj().clickLink(driver, "Modify");
			inDevelopment.modifyCenterInfo(driver, config);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {  "infomgr" ,"InDevFranchisee_Tabs_AMD" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_InDevFranchisee_Documents_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of Documents tabs of Info Mgr > inDevelopment.", reference = { "" })
	public void InfoMgr_InDevFranchisees_Documents_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		InDevelopmentCommonMethods inDevelopment = fc.infomgr().inDevelopment();
		
		InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();
		try {
			fc.loginpage().login(driver);
			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");		
			inDevelopment.searchFranchiseAndClick(driver, franchiseID);
			// Add Documents
			fc.utobj().printTestStep("Add  Documents Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkDocuments);
			String docTitle = inDevelopment.addDocument(driver, config);

			// Modify Documents
			fc.utobj().printTestStep("Modify  Documents Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkDocuments);
			fc.utobj().actionImgOption(driver, docTitle, "Modify");
			docTitle = inDevelopment.modifyDocument(driver, config);
			// Delete
			fc.utobj().printTestStep("Delete  Documents Tab Data");
			inDevelopment.deleteTabData(driver, config, centerInfoPage.lnkDocuments, docTitle);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {  "infomgr" ,"InDevFranchisee_Tabs_AMD","Empl" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_InDevFranchisee_Employees_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of Employee tabs of Info Mgr > inDevelopment.", reference = { "" })
	public void InfoMgr_InDevFranchisees_Employees_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		InDevelopmentCommonMethods inDevelopment = fc.infomgr().inDevelopment();
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();
		try {
			fc.loginpage().login(driver);
			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");
			inDevelopment.searchFranchiseAndClick(driver, franchiseID);
			// Add Employees
			fc.utobj().printTestStep("Add  Employees Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkEmployees);
			String empName = inDevelopment.addEmployee(driver, config);
			// Modify Employee
			fc.utobj().printTestStep("Modify  Employees Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkEmployees);
			// fc.utobj().actionImgOption(driver, empName, "Modify");
			empName = inDevelopment.modifyEmployee(driver, config);
			fc.utobj().printTestStep("Delete  Employees Tab Data");
			inDevelopment.deleteTabData(driver, config, centerInfoPage.lnkEmployees, null);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" ,"InDevFranchisee_Tabs_AMD" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_InDevFranchisee_Guarantor_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of Guarantor tab of Info Mgr > inDevelopment.", reference = { "" })
	public void InfoMgr_InDevFranchisees_Guarantor_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		InDevelopmentCommonMethods inDevelopment = fc.infomgr().inDevelopment();
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();
		try {

			fc.loginpage().login(driver);
			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");
			inDevelopment.searchFranchiseAndClick(driver, franchiseID);
			// Add Gurantor details
			fc.utobj().printTestStep("Add  Gurantor Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkGuarantors);
			inDevelopment.addGuarantor(driver, config);
			// Modify Guarantor
			fc.utobj().printTestStep("Modify  Gurantor Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkGuarantors);
			inDevelopment.modifyGuarantor(driver, config);
			// Delete Entity
			fc.utobj().printTestStep("Delete  Gurantor Tab Data");
			inDevelopment.deleteTabData(driver, config, centerInfoPage.guarantorsTab, null);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" ,"InDevFranchisee_Tabs_AMD" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_InDevFranchisee_Entity_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of Entity Tab of Info Mgr > inDevelopment.", reference = { "" })
	public void InfoMgr_InDevFranchisees_Entity_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		InDevelopmentCommonMethods inDevelopment = fc.infomgr().inDevelopment();
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();
		try {
			fc.loginpage().login(driver);
			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");
			inDevelopment.searchFranchiseAndClick(driver, franchiseID);
			// Add
			fc.utobj().printTestStep("Add  Entity Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.linkEntityDetail);
			inDevelopment.addEntityDetails(driver, config);
			// Modify Entity Details
			fc.utobj().printTestStep("Modify  Entity Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.linkEntityDetail);
			inDevelopment.modifyEntityDetails(driver, config);
			// Delete Entity
			fc.utobj().printTestStep("Delete  Entity Tab Data");
			inDevelopment.deleteTabData(driver, config, centerInfoPage.linkEntityDetail, null);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {  "infomgr" ,"InDevFranchisee_Tabs_AMD" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_InDevFranchisee_Marketing_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of Marketing Tab of Info Mgr > inDevelopment.", reference = { "" })
	public void InfoMgr_InDevFranchisees_Marketing_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		InDevelopmentCommonMethods inDevelopment = fc.infomgr().inDevelopment();
		try {

		
			InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();
			fc.loginpage().login(driver);
			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");
			inDevelopment.searchFranchiseAndClick(driver, franchiseID);

			// Add Marketing details
			fc.utobj().printTestStep("Add  Marketing Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkMarketingFinTab);
			inDevelopment.addMarketingDetails(driver, config);
			// Modify Marketing
			fc.utobj().printTestStep("Modify  Marketing Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkMarketingFinTab);
			inDevelopment.modifyMarketingDetails(driver, config);
			// Delete marketing
			fc.utobj().printTestStep("Delete  Marketing Tab Data");
			inDevelopment.deleteTabData(driver, config, centerInfoPage.marketingTab, null);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {  "infomgr" ,"InDevFranchisee_Tabs_AMD"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_InDevFranchisee_Insurance_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of Insurance tabs of Info Mgr > inDevelopment.", reference = { "" })
	public void InfoMgr_InDevFranchisees_Insurance_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
	
		InDevelopmentCommonMethods inDevelopment = fc.infomgr().inDevelopment();
		InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();
		try {

			fc.loginpage().login(driver);

			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");
			inDevelopment.searchFranchiseAndClick(driver, franchiseID);
			// Add Insurance Details
			fc.utobj().printTestStep("Add  Insurance Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkInsurance);
			inDevelopment.addInsuranceDetails(driver, config);

			// Modify Insurance
			fc.utobj().printTestStep("Modify  Insurance Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkInsurance);
			inDevelopment.modifyInsuranceDetails(driver, config);

			// Delete Insurance
			fc.utobj().printTestStep("Delete  Insurance Tab Data");
			inDevelopment.deleteTabData(driver, config, centerInfoPage.insuranceTab, null);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {  "infomgr" ,"InDevFranchisee_Tabs_AMD"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_InDevFranchisee_LegalViolation_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of LegalViolation tabs of Info Mgr > inDevelopment.", reference = { "" })
	public void InfoMgr_InDevFranchisees_LegalViolation_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		InDevelopmentCommonMethods inDevelopment = fc.infomgr().inDevelopment();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();
		try {

			fc.loginpage().login(driver);
			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");
			inDevelopment.searchFranchiseAndClick(driver, franchiseID);
			// Add legal violation
			fc.utobj().printTestStep("Add  Legal Violation Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkLegalViolation);
			inDevelopment.addLegalViolation(driver, config);
			// Modify Legal Violation
			fc.utobj().printTestStep("Modify  Legal Violation Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkLegalViolation);
			inDevelopment.modifyLegalViolation(driver, config);
			// Delete Legal Violation
			fc.utobj().printTestStep("Delete  Legal Violation Tab Data");
			inDevelopment.deleteTabData(driver, config, centerInfoPage.lnkLegalViolation, null);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" ,"InDevFranchisee_Tabs_AMD" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_InDevFranchisee_Lenders_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of Lenders tabs of Info Mgr > inDevelopment.", reference = { "" })
	public void InfoMgr_InDevFranchisees_Lenders_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		InDevelopmentCommonMethods inDevelopment = fc.infomgr().inDevelopment();
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();
		try {

			fc.loginpage().login(driver);
			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");
			inDevelopment.searchFranchiseAndClick(driver, franchiseID);

			// Add Lenders
			fc.utobj().printTestStep("Add  Lenders Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkLenders);
			inDevelopment.addLenders(driver, config);
			// Modify Lenders Details
			fc.utobj().printTestStep("Modify  Lenders Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkLenders);
			inDevelopment.modifyLenders(driver, config);

			// Delete Lenders
			fc.utobj().printTestStep("Delete  Lenders Tab Data");
			inDevelopment.deleteTabData(driver, config, centerInfoPage.lnkLenders, null);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" ,"InDevFranchisee_Tabs_AMD" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_InDevFranchisee_Finance_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of Finance of Info Mgr > inDevelopment.", reference = { "" })
	public void InfoMgr_InDevFranchisees_Finance_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		InDevelopmentCommonMethods inDevelopment = fc.infomgr().inDevelopment();
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();
		try {

			fc.loginpage().login(driver);

			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");
			inDevelopment.searchFranchiseAndClick(driver, franchiseID);

			// Add Finance Details
			fc.utobj().printTestStep("Add  Finance Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkFinancial);
			inDevelopment.addFinanceDetails(driver, config);

			// Modify Financials
			fc.utobj().printTestStep("Modify  Finance Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkFinancial);
			inDevelopment.modifyFinanceDetails(driver, config);

			// Delete Financials
			fc.utobj().printTestStep("Delete  Finance Tab Data");
			inDevelopment.deleteTabData(driver, config, centerInfoPage.lnkFinancial, null);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {  "infomgr" ,"InDevFranchisee_Tabs_AMD" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_InDevFranchisee_MysteryReview_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of MysteryReview Tab of Info Mgr > inDevelopment.", reference = { "" })
	public void InfoMgr_InDevFranchisees_MysteryReview_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		InDevelopmentCommonMethods inDevelopment = fc.infomgr().inDevelopment();
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();
		try {
			fc.loginpage().login(driver);

			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");
			inDevelopment.searchFranchiseAndClick(driver, franchiseID);

			// Add Mystry Review
			fc.utobj().printTestStep("Add  MystryReview Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkMysteryReview);
			inDevelopment.addMystryReview(driver, config);
			// Modify Mystry Review
			fc.utobj().printTestStep("Modify  MystryReview Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkMysteryReview);
			inDevelopment.modifyMystryReview(driver, config);
			// Delete MysteryReview
			fc.utobj().printTestStep("Delete  MystryReview Tab Data");
			inDevelopment.deleteTabData(driver, config, centerInfoPage.lnkMysteryReview, null);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" ,"InDevFranchisee_Tabs_AMD"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_InDevFranchisee_Picture_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of Picture tab of Info Mgr > inDevelopment.", reference = { "" })
	public void InfoMgr_InDevFranchisees_Picture_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		InDevelopmentCommonMethods inDevelopment = fc.infomgr().inDevelopment();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();
		try {
			fc.loginpage().login(driver);
			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");
			inDevelopment.searchFranchiseAndClick(driver, franchiseID);

			// Add Picture
			fc.utobj().printTestStep("Add  Picture Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkPictures);
			String pictureTitle = inDevelopment.addPicture(driver, config);
			// Modify Pictures
			fc.utobj().printTestStep("Modify  Picture Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkPictures);
			fc.utobj().actionImgOption(driver, pictureTitle, "Modify");
			pictureTitle = inDevelopment.modifyPicture(driver, config);

			// Delete Pictures
			fc.utobj().printTestStep("Delete  Picture Tab Data");
			inDevelopment.deleteTabData(driver, config, centerInfoPage.lnkPictures, pictureTitle);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {  "infomgr" ,"InDevFranchisee_Tabs_AMD" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_InDevFranchisee_Owner_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of Owner of Info Mgr > inDevelopment.", reference = { "" })
	public void InfoMgr_InDevFranchisees_Owner_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		InDevelopmentCommonMethods inDevelopment = fc.infomgr().inDevelopment();
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();
		try {
			fc.loginpage().login(driver);

			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");
			inDevelopment.searchFranchiseAndClick(driver, franchiseID);
			// Add Owner
			fc.utobj().printTestStep("Add  Owner Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.tabOwners);
			Map<String, String> owner = inDevelopment.addOwner(driver,config);
			// Add User
			fc.utobj().printTestStep("Add  User Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkUsers);
			InfoMgrUsersPage usersPage = new InfoMgrFranchiseesPage(driver).getUsersPage();
			fc.utobj().clickElement(driver, usersPage.btnAddUser);
			String userName = inDevelopment.addOwnerUser(driver, config, owner.get("FirstName") + " " + owner.get("LastName"));
			// Modify Owners
			fc.utobj().printTestStep("Modify  Owner Tab Data");
						fc.utobj().clickElement(driver, centerInfoPage.tabOwners);
						fc.utobj().actionImgOption(driver, owner.get("FirstName"), "Modify");
						inDevelopment.modifyOwner(driver, config);
			// Modify Users
			fc.utobj().printTestStep("Modify  Users Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkUsers);
			fc.utobj().actionImgOption(driver, owner.get("FirstName") + " " + owner.get("LastName"), "Modify");
			inDevelopment.modifyOwnerUser(driver, config);
			// Delete Owner
			fc.utobj().printTestStep("Delete  Owner Tab Data");
			inDevelopment.deleteTabData(driver, config, centerInfoPage.ownersTab, owner.get("FirstName"));
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" ,"InDevFranchisee_Tabs_AMD" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_InDevFranchisee_Realestate_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of Realestate of Info Mgr > inDevelopment.", reference = { "" })
	public void InfoMgr_InDevFranchisees_Realestate_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		InDevelopmentCommonMethods inDevelopment = fc.infomgr().inDevelopment();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();
		try {
			fc.loginpage().login(driver);
			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");
			inDevelopment.searchFranchiseAndClick(driver, franchiseID);
			// Add Real Estate
			fc.utobj().printTestStep("Add  RealEstate Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkRealEstate);
			inDevelopment.addRealState(driver, config);
			// Modify Real state
			fc.utobj().printTestStep("Modify  RealEstate Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkRealEstate);
			inDevelopment.modifyRealState(driver, config);
			// Delete RealEstate
			fc.utobj().printTestStep("Delete  RealEstate Tab Data");
			inDevelopment.deleteTabData(driver, config, centerInfoPage.lnkRealEstate, null);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {  "infomgr" ,"InDevFranchisee_Tabs_AMD","Testddd"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_InDevFranchisee_Territory_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of Territory Tab of Info Mgr > inDevelopment.", reference = { "" })
	public void InfoMgr_InDevFranchisees_Territory_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		InDevelopmentCommonMethods inDevelopment = fc.infomgr().inDevelopment();
		InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();
		try {
			fc.loginpage().login(driver);
			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");
			inDevelopment.searchFranchiseAndClick(driver, franchiseID);
			// Add Territory
			fc.utobj().printTestStep("Add  Territory Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkTerritory);
			inDevelopment.addTerritory(driver, config);
			// Modify Territory
			fc.utobj().printTestStep("Modify  Territory Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkTerritory);
			inDevelopment.modifyTerritory(driver, config);
			// Delete Territory
			fc.utobj().printTestStep("delete  Territory Tab Data");
			inDevelopment.deleteTabData(driver, config, centerInfoPage.lnkTerritory, null);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {  "infomgr" ,"InDevFranchisee_Tabs_AMD"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_InDevFranchisee_Training_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of Training Tab of Info Mgr > inDevelopment.", reference = { "" })
	public void InfoMgr_InDevFranchisees_Training_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		InDevelopmentCommonMethods inDevelopment = fc.infomgr().inDevelopment();
		InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();
		try {
			fc.loginpage().login(driver);
			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");
			inDevelopment.searchFranchiseAndClick(driver, franchiseID);
			// Add Training
			fc.utobj().printTestStep("Add  Training Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkTraining);
			inDevelopment.addTraining(driver, config);
			// Modify Training
			fc.utobj().printTestStep("Modify  Training Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkTraining);
			inDevelopment.modifyTraining(driver, config);
			// Delete Training
			fc.utobj().printTestStep("Delete  Training Tab Data");
			inDevelopment.deleteTabData(driver, config, centerInfoPage.lnkTraining, null);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Verify_InfoMgr_InDev_Actions_SendMail", testCaseDescription = "Test to verify send mail functionality from action buttons.", reference = {
			"" })
	public void InfoMgr_InDevlopment_ActionsMenu_Actions_SendMail() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		InDevelopmentCommonMethods inDevelopment = fc.infomgr().inDevelopment();
		Location location = new Location();
		LocationData ld = new LocationData();

		try {
			fc.loginpage().login(driver);

			InfoMgrInDevelopmentPage inDevelopmentPage = new InfoMgrInDevelopmentPage(driver);
			InfoMgrContactHistoryPage contactHistoryPage = inDevelopmentPage.getContactHistoryPage();

			fc.utobj().printTestStep("Add in development location");

			AdminAreaRegionAddAreaRegionPageTest region_page = new AdminAreaRegionAddAreaRegionPageTest();
			String region_name = fc.utobj().generateTestData("R1");
			region_page.addAreaRegion(driver, region_name);
			imc.InfoMgrInDevelopment(driver);
			fc.utobj().clickLink(driver, "Add In Development Location");
			location = fc.commonMethods().getDefaultDataFor_Location(location);
			location.setAreaRegion(region_name);
			ld.filllocationInfo(driver, location).submit(driver);
			// String franchiseID = inDevelopment.addDevelopmentLocation(driver, config);
			// String franchiseID = "openerlocation";
			imc.InfoMgrInDevelopment(driver);
			imc.searchLocationFromShowFilter(driver, location.getFranchiseID());
			// inDevelopment.inDevelopmentPageShowAll(driver);
			fc.utobj().printTestStep("Clicking on Send mail link under action image button ");
			fc.utobj().actionImgOption(driver, location.getFranchiseID(), "Send Email");
			String mailSubject = inDevelopment.sendTextEmail(driver, config, false);
			inDevelopment.searchFranchiseAndClick(driver, location.getFranchiseID());
			fc.utobj().clickElement(driver, contactHistoryPage.lnkContactHistory);
			boolean assertMailSubjectonPage = fc.utobj().assertPageSource(driver, mailSubject);
			if (assertMailSubjectonPage) {
				Reporter.log("Text mail has been sent successfully");
			} else {
				fc.utobj().throwsException("Mail couldn't be sent .");
			}

			fc.utobj().printTestStep("Send mail from action button displayed at the bottom of the page");
			// inDevelopment.inDevelopmentPageShowAll(driver);
			imc.InfoMgrInDevelopment(driver);
			imc.searchLocationFromShowFilter(driver, location.getFranchiseID());

			inDevelopment.checkLocation(driver, location.getFranchiseID());
			fc.utobj().clickElement(driver, inDevelopmentPage.btnSendMail);
			String mailSubject2 = inDevelopment.sendTextEmail(driver, config, false);
			inDevelopment.searchFranchiseAndClick(driver, location.getFranchiseID());
			fc.utobj().clickElement(driver, contactHistoryPage.lnkContactHistory);
			boolean assertMailSubjectonPage2 = fc.utobj().assertPageSource(driver, mailSubject2);
			if (assertMailSubjectonPage2) {
				Reporter.log("Text mail has been sent successfully");
			} else {
				fc.utobj().throwsException("Mail couldn't be sent .");
			}
			fc.utobj().printTestStep("Click on Send Email button");
			inDevelopment.inDevelopmentPageShowAll(driver);
			inDevelopment.checkLocation(driver, location.getFranchiseID());
			fc.utobj().selectActionMenuItemsWithTagA(driver, "Send Email");
			String mailSubject3 = inDevelopment.sendTextEmail(driver, config, false);
			inDevelopment.searchFranchiseAndClick(driver, location.getFranchiseID());
			fc.utobj().clickElement(driver, contactHistoryPage.lnkContactHistory);
			boolean assertMailSubjectonPage3 = fc.utobj().assertPageSource(driver, mailSubject3);
			if (assertMailSubjectonPage3) {
				Reporter.log("Text mail has been sent successfully");
			} else {
				fc.utobj().throwsException("Mail couldn't be sent .");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr", "IMFailedTC", "New", "SendMailS0S" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Verify_InfoMgr_InDev_Actions_LogaTask", testCaseDescription = "Test to verify Log a task functionality from action buttons ", reference = {
			"" })
	public void InfoMgr_InDevlopment_PageBottom_Actions_LogaTask() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		InDevelopmentCommonMethods inDevelopment = fc.infomgr().inDevelopment();

		InfoMgrInDevelopmentPage inDevelopmentPage = new InfoMgrInDevelopmentPage(driver);
		InfoMgrContactHistoryPage contactHistoryPage = inDevelopmentPage.getContactHistoryPage();

		try {
			fc.loginpage().login(driver);
			// adminInfoMgr.configureAllFieldsOnManageFormGeneratorPage(driver ,
			// config);

			fc.utobj().printTestStep("Add corporate user");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail("infomgrautomation@staffex.com");
			corpUser = corpTest.createDefaultUser(driver, corpUser);

			fc.utobj().printTestStep("Add in development location");
			String franchiseID = inDevelopment.addInDevelopmentFranchiseLocationForInfoMgrName(driver, false);

			// inDevelopment.inDevelopmentPageShowAll(driver);
			inDevelopment.searchFranchise(driver, franchiseID);
			
			//Test
			
			fc.utobj().actionImgOption(driver, franchiseID, "Log a Task");
			String taskSubject2 = inDevelopment.logaTask(driver, config, corpUser.getuserFullName(), false);
			inDevelopment.searchFranchiseAndClick(driver, franchiseID);
			
			
			
			
			
			
			//Test1
			fc.utobj().printTestStep("Click on Log a Task button displayed at the bottom of the page");
			inDevelopment.checkLocation(driver, franchiseID);
			fc.utobj().clickElement(driver, inDevelopmentPage.btnLogATask);
			String taskSubject1 = inDevelopment.logaTask(driver, config, corpUser.getuserFullName(), false);
			inDevelopment.searchFranchiseAndClick(driver, franchiseID);
			fc.utobj().clickElement(driver, contactHistoryPage.lnkContactHistory);
			boolean assertTaskSubjectonPage1 = fc.utobj().assertPageSource(driver, taskSubject1);
			if (assertTaskSubjectonPage1) {
				Reporter.log("Log a Task has been done successfully");
			} else {
				fc.utobj().throwsException("Log a Task failed .");
			}

			fc.utobj().printTestStep("Log a task from action image icon");
			// inDevelopment.inDevelopmentPageShowAll(driver);
			System.out.println("A");
			inDevelopment.searchFranchise(driver, franchiseID);
			System.out.println("B");
			//fc.utobj().selectMoreActionMenuItemsWithTagA(driver,"Log a Task");
			System.out.println("C");
			/*fc.utobj().actionImgOption(driver, franchiseID, "Log a Task");
			String taskSubject2 = inDevelopment.logaTask(driver, config, corpUser.getuserFullName(), false);
			inDevelopment.searchFranchiseAndClick(driver, franchiseID);*/
			fc.utobj().clickElement(driver, contactHistoryPage.lnkContactHistory);
			boolean assertTaskSubjectonPage2 = fc.utobj().assertPageSource(driver, taskSubject2);
			if (assertTaskSubjectonPage2) {
				Reporter.log("Log a Task has been done successfully");
			} else {
				fc.utobj().throwsException("Log a Task failed .");
			}

			// inDevelopment.inDevelopmentPageShowAll(driver);
			fc.utobj().printTestStep(
					"Click on Log a Task link displayed under the action button click at the top of the page");
			// inDevelopment.inDevelopmentPageShowAll(driver);
			inDevelopment.searchFranchise(driver, franchiseID);
			inDevelopment.checkLocation(driver, franchiseID);
			fc.utobj().sleep(1000);
			fc.utobj().selectActionMenuItemsWithTagA(driver, "Log a Task");
			String taskSubject3 = inDevelopment.logaTask(driver, config, corpUser.getuserFullName(), false);
			inDevelopment.searchFranchiseAndClick(driver, franchiseID);
			fc.utobj().clickElement(driver, contactHistoryPage.lnkContactHistory);
			boolean assertTaskSubjectonPage3 = fc.utobj().assertPageSource(driver, taskSubject3);
			if (assertTaskSubjectonPage3) {
				Reporter.log("Log a Task has been done successfully");
			} else {
				fc.utobj().throwsException("Log a Task failed .");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			//fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr", "Batch" })

	@TestCase(createdOn = "2017-09-14", updatedOn = "2018-09-14", testCaseId = "TC_Verify_InfoMgr_InDev_Actions_LogaTask_In_Batch_With_2Locations", testCaseDescription = "Test to verify Log a task functionality in batch from Top Action and Bottom Log a Task Button ", reference = {
			"" })
	public void InfoMgr_InDev_Actions_LogaTask_In_Batch_With_2Locations() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		InDevelopmentCommonMethods inDevelopment = fc.infomgr().inDevelopment();

		InfoMgrInDevelopmentPage inDevelopmentPage = new InfoMgrInDevelopmentPage(driver);
		InfoMgrContactHistoryPage contactHistoryPage = inDevelopmentPage.getContactHistoryPage();

		try {
			fc.loginpage().login(driver);
			// adminInfoMgr.configureAllFieldsOnManageFormGeneratorPage(driver ,
			// config);

			fc.utobj().printTestStep("Add corporate user");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail("infomgrautomation@staffex.com");
			corpUser = corpTest.createDefaultUser(driver, corpUser);

			fc.utobj().printTestStep("Add in development location");
			String franchiseID1 = inDevelopment.addInDevelopmentFranchiseLocationForInfoMgrName(driver, false);
			String franchiseID2 = inDevelopment.addInDevelopmentFranchiseLocationForInfoMgrName(driver, false);

			inDevelopment.inDevelopmentPageShowAll(driver);
			//inDevelopment.searchFranchise(driver, franchiseID1);
			fc.utobj().printTestStep("Click on Log a Task button displayed at the bottom of the page");
			inDevelopment.checkLocation(driver, franchiseID1);
			inDevelopment.checkLocation(driver, franchiseID2);
			
			fc.utobj().clickElement(driver, inDevelopmentPage.btnLogATask);
			String taskSubject1 = inDevelopment.logaTask(driver, config, corpUser.getuserFullName(), false);
			inDevelopment.searchFranchiseAndClick(driver, franchiseID1);
			fc.utobj().clickElement(driver, contactHistoryPage.lnkContactHistory);
			boolean assertTaskSubjectonPage1 = fc.utobj().assertPageSource(driver, taskSubject1);
			if (assertTaskSubjectonPage1) {
				Reporter.log("Log a Task has been done successfully");
			} else {
				fc.utobj().throwsException("Log a Task failed .");
			}
			inDevelopment.searchFranchiseAndClick(driver, franchiseID2);
			fc.utobj().clickElement(driver, contactHistoryPage.lnkContactHistory);
			boolean assertTaskSubjectonPage2 = fc.utobj().assertPageSource(driver, taskSubject1);
			if (assertTaskSubjectonPage2) {
				Reporter.log("Log a Task has been done successfully");
			} else {
				fc.utobj().throwsException("Log a Task failed .");
			}

			
			//inDevelopment.inDevelopmentPageShowAll(driver);
			fc.utobj().printTestStep(
					"Click on Log a Task link displayed under the action button click at the top of the page");
			inDevelopment.inDevelopmentPageShowAll(driver);
			//inDevelopment.searchFranchise(driver, franchiseID);
			inDevelopment.checkLocation(driver, franchiseID1);
			inDevelopment.checkLocation(driver, franchiseID2);
			fc.utobj().sleep(1000);
			fc.utobj().selectActionMenuItemsWithTagA(driver, "Log a Task");
			String taskSubject3 = inDevelopment.logaTask(driver, config, corpUser.getuserFullName(), false);
			inDevelopment.searchFranchiseAndClick(driver, franchiseID1);
			fc.utobj().clickElement(driver, contactHistoryPage.lnkContactHistory);
			boolean assertTaskSubjectonPage3 = fc.utobj().assertPageSource(driver, taskSubject3);
			if (assertTaskSubjectonPage3) {
				Reporter.log("Log a Task has been done successfully");
			} else {
				fc.utobj().throwsException("Log a Task failed .");
			}
			
			inDevelopment.searchFranchiseAndClick(driver, franchiseID2);
			fc.utobj().clickElement(driver, contactHistoryPage.lnkContactHistory);
			boolean assertTaskSubjectonPage4 = fc.utobj().assertPageSource(driver, taskSubject3);
			if (assertTaskSubjectonPage4) {
				Reporter.log("Log a Task has been done successfully");
			} else {
				fc.utobj().throwsException("Log a Task failed .");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(groups = { "infomgr"})

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Verify_InfoMgr_InDev_Actions_LogaCall", testCaseDescription = "Test to verify log a call functionality from action buttons", reference = {
			"" })
	public void InfoMgr_InDevlopment_Actions_LogaCall() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		InDevelopmentCommonMethods inDevelopment = fc.infomgr().inDevelopment();

		InfoMgrInDevelopmentPage inDevelopmentPage = new InfoMgrInDevelopmentPage(driver);
		InfoMgrContactHistoryPage contactHistoryPage = inDevelopmentPage.getContactHistoryPage();
		Location location = new Location();
		LocationData ld = new LocationData();
		try {
			fc.loginpage().login(driver);
			// adminInfoMgr.configureAllFieldsOnManageFormGeneratorPage(driver ,
			// config);

			AdminAreaRegionAddAreaRegionPageTest region_page = new AdminAreaRegionAddAreaRegionPageTest();
			String region_name = fc.utobj().generateTestData("R1");
			region_page.addAreaRegion(driver, region_name);
			imc.InfoMgrInDevelopment(driver);
			fc.utobj().clickLink(driver, "Add In Development Location");
			location = fc.commonMethods().getDefaultDataFor_Location(location);
			location.setAreaRegion(region_name);
			ld.filllocationInfo(driver, location).submit(driver);

			// location.setFranchiseID("openerlocation");
			inDevelopment.inDevelopmentPageShowAll(driver);
			fc.utobj().printTestStep("Click on Log a Call button displayed at the bottom of the page ");
			inDevelopment.checkLocation(driver, location.getFranchiseID());

			fc.utobj().clickElement(driver, inDevelopmentPage.btnLogACall);
			String logACallSubject1 = inDevelopment.logACall(driver, config, false);
			inDevelopment.searchFranchiseAndClick(driver, location.getFranchiseID());
			fc.utobj().clickElement(driver, contactHistoryPage.lnkContactHistory);
			boolean assertCallTextOnPage1 = fc.utobj().assertPageSource(driver, logACallSubject1);
			if (assertCallTextOnPage1) {
				Reporter.log("Log a Call has been done successfully");
			} else {
				Reporter.log("Log a Call has failed .");
			}

			fc.utobj().printTestStep("Log a call from action menu");
			inDevelopment.inDevelopmentPageShowAll(driver);
			fc.utobj().actionImgOption(driver, location.getFranchiseID(), "Log a Call");
			String logACallSubject2 = inDevelopment.logACall(driver, config, false);
			inDevelopment.searchFranchiseAndClick(driver, location.getFranchiseID());
			fc.utobj().clickElement(driver, contactHistoryPage.lnkContactHistory);
			boolean assertCallTextOnPage2 = fc.utobj().assertPageSource(driver, logACallSubject2);
			if (assertCallTextOnPage2) {
				Reporter.log("Log a Call has been done successfully");
			} else {
				fc.utobj().throwsException("Log a Call has failed .");
			}

			fc.utobj().printTestStep("Click on Log a Call link displayed under the top action button");
			inDevelopment.inDevelopmentPageShowAll(driver);
			inDevelopment.checkLocation(driver, location.getFranchiseID());
			fc.utobj().selectActionMenuItemsWithTagA(driver, "Log a Call");
			String logACallSubject3 = inDevelopment.logACall(driver, config, false);
			inDevelopment.searchFranchiseAndClick(driver, location.getFranchiseID());
			fc.utobj().clickElement(driver, contactHistoryPage.lnkContactHistory);
			boolean assertCallTextOnPage3 = fc.utobj().assertPageSource(driver, logACallSubject3);
			if (assertCallTextOnPage3) {
				Reporter.log("Log a Call has been done successfully");
			} else {
				fc.utobj().throwsException("Log a Call has failed .");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2017-09-13", updatedOn = "2018-09-13", testCaseId = "TC_Verify_InfoMgr_InDev_Actions_LogaCall_In_Batch_With_2Locations", testCaseDescription = "Test to verify log a call functionality from action buttons in Batch for 2 Locations.", reference = {
			"" })
	public void InfoMgr_InDevlopment_Actions_LogaCall_In_Batch_With_2Locations() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		InDevelopmentCommonMethods inDevelopment = fc.infomgr().inDevelopment();

		InfoMgrInDevelopmentPage inDevelopmentPage = new InfoMgrInDevelopmentPage(driver);
		InfoMgrContactHistoryPage contactHistoryPage = inDevelopmentPage.getContactHistoryPage();
		try {
			fc.loginpage().login(driver);
			String franchiseID1 = inDevelopment.addInDevelopmentFranchiseLocationForInfoMgrName(driver, false);
			String franchiseID2 = inDevelopment.addInDevelopmentFranchiseLocationForInfoMgrName(driver, false);

			inDevelopment.inDevelopmentPageShowAll(driver);
			fc.utobj().printTestStep("Click on Log a Call button displayed at the bottom of the page ");
			inDevelopment.checkLocation(driver, franchiseID1);
			inDevelopment.checkLocation(driver, franchiseID2);

			fc.utobj().clickElement(driver, inDevelopmentPage.btnLogACall);
			String logACallSubject1 = inDevelopment.logACall(driver, config, false);
			inDevelopment.searchFranchiseAndClick(driver, franchiseID1);
			fc.utobj().clickElement(driver, contactHistoryPage.lnkContactHistory);
			boolean assertCallTextOnPage1 = fc.utobj().assertPageSource(driver, logACallSubject1);
			if (assertCallTextOnPage1) {
				Reporter.log("Log a Call has been done successfully");
			} else {
				Reporter.log("Log a Call has failed .");
			}
			
			inDevelopment.searchFranchiseAndClick(driver, franchiseID2);
			fc.utobj().clickElement(driver, contactHistoryPage.lnkContactHistory);
			boolean assertCallTextOnPage2 = fc.utobj().assertPageSource(driver, logACallSubject1);
			if (assertCallTextOnPage2) {
				Reporter.log("Log a Call has been done successfully");
			} else {
				Reporter.log("Log a Call has failed .");
			}
			
			

			fc.utobj().printTestStep("Click on Log a Call link displayed under the top action button");
			inDevelopment.inDevelopmentPageShowAll(driver);
			inDevelopment.checkLocation(driver, franchiseID1);
			inDevelopment.checkLocation(driver, franchiseID2);
			fc.utobj().selectActionMenuItemsWithTagA(driver, "Log a Call");
			String logACallSubject2 = inDevelopment.logACall(driver, config, false);
			inDevelopment.searchFranchiseAndClick(driver, franchiseID1);
			fc.utobj().clickElement(driver, contactHistoryPage.lnkContactHistory);
			boolean assertCallTextOnPage3 = fc.utobj().assertPageSource(driver, logACallSubject2);
			if (assertCallTextOnPage3) {
				Reporter.log("Log a Call has been done successfully");
			} else {
				fc.utobj().throwsException("Log a Call has failed .");
			}
			
			inDevelopment.searchFranchiseAndClick(driver, franchiseID2);
			fc.utobj().clickElement(driver, contactHistoryPage.lnkContactHistory);
			boolean assertCallTextOnPage4 = fc.utobj().assertPageSource(driver, logACallSubject2);
			if (assertCallTextOnPage4) {
				Reporter.log("Log a Call has been done successfully");
			} else {
				fc.utobj().throwsException("Log a Call has failed .");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	@Test(groups = { "infomgr" })
	
	//Update Script by Govind

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-10-26", testCaseId = "TC_Verify_InfoMgr_InDev_Actions_AddToGroup", testCaseDescription = "Test to verify Add to Group functionality from action buttons", reference = {
			"" })
	public void InfoMgr_InDevlopment_Actions_AddToGroup() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		InDevelopmentCommonMethods inDevelopment = fc.infomgr().inDevelopment();

		InfoMgrInDevelopmentPage inDevelopmentPage = new InfoMgrInDevelopmentPage(driver);
		Location location = new Location();
		LocationData ld = new LocationData();

		try {
			fc.loginpage().login(driver);

			AdminAreaRegionAddAreaRegionPageTest region_page = new AdminAreaRegionAddAreaRegionPageTest();
			String region_name = fc.utobj().generateTestData("R1");
			region_page.addAreaRegion(driver, region_name);
			imc.InfoMgrInDevelopment(driver);
			fc.utobj().clickLink(driver, "Add In Development Location");
			location = fc.commonMethods().getDefaultDataFor_Location(location);
			location.setAreaRegion(region_name);
			ld.filllocationInfo(driver, location).submit(driver);
			// location.setFranchiseID("FIMs10164384");
			inDevelopment.inDevelopmentPageShowAll(driver);
			fc.utobj().printTestStep("Click on Add to Group button displayed at the bottom of the page");
			inDevelopment.checkLocation(driver, location.getFranchiseID());
			fc.utobj().clickElement(driver, inDevelopmentPage.btnAddtoGroup);
			inDevelopment.addToGroup(driver, config);

			fc.utobj().printTestStep(
			"Click on Add to Group link displayed under Action button displayed at the top of the page");
			inDevelopment.inDevelopmentPageShowAll(driver);
			inDevelopment.checkLocation(driver, location.getFranchiseID());
			fc.utobj().selectActionMenuItemsWithTagA(driver, "Add To Group");
			inDevelopment.addToGroup(driver, config);

			fc.utobj().printTestStep("Click on Add to Group button  displayed at the bottom of the page");
			inDevelopment.inDevelopmentPageShowAll(driver);
			inDevelopment.checkLocation(driver, location.getFranchiseID());
			fc.utobj().clickElement(driver, inDevelopmentPage.btnAddtoGroup);
			inDevelopment.addToGroup(driver, config);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	/*@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-08-10", testCaseId = "TC_Verify_InfoMgr_InDev_Actions_AddToGroup_In_Batch_With_2Locations", testCaseDescription = "Test to verify Add to Group functionality in Batch with 2 Locations.", reference = {
			"" })
	public void InfoMgr_InDevlopment_Actions_AddToGroup_In_Batch_With_2Locations() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		InDevelopmentCommonMethods inDevelopment = fc.infomgr().inDevelopment();

		InfoMgrInDevelopmentPage inDevelopmentPage = new InfoMgrInDevelopmentPage(driver);
		Location location = new Location();
		LocationData ld = new LocationData();

		try {
			fc.loginpage().login(driver);

			AdminAreaRegionAddAreaRegionPageTest region_page = new AdminAreaRegionAddAreaRegionPageTest();
			String region_name = fc.utobj().generateTestData("R1");
			region_page.addAreaRegion(driver, region_name);
			imc.InfoMgrInDevelopment(driver);
			fc.utobj().clickLink(driver, "Add In Development Location");
			location = fc.commonMethods().getDefaultDataFor_Location(location);
			location.setAreaRegion(region_name);
			ld.filllocationInfo(driver, location).submit(driver);
			// location.setFranchiseID("FIMs10164384");
			imc.InfoMgrInDevelopment(driver);
			fc.utobj().clickLink(driver, "Add In Development Location");
			location = fc.commonMethods().getDefaultDataFor_Location(location);
			location.setAreaRegion(region_name);
			ld.filllocationInfo(driver, location).submit(driver);
			//inDevelopment.inDevelopmentPageShowAll(driver);
			inDevelopment.searchFranchiseviaRegion(driver, region_name);
			fc.utobj().printTestStep("Click on Add to Group button displayed at the bottom of the page");
			inDevelopment.checkLocation(driver, location.getFranchiseID());
			inDevelopment.checkLocation(driver, location.getFranchiseID());
			fc.utobj().clickElement(driver, inDevelopmentPage.btnAddtoGroup);
			inDevelopment.addToGroup(driver, config);

			fc.utobj().printTestStep(
					"Click on Add to Group link displayed under Action button displayed at the top of the page");
			inDevelopment.inDevelopmentPageShowAll(driver);
			inDevelopment.checkLocation(driver, location.getFranchiseID());
			fc.utobj().selectActionMenuItemsWithTagA(driver, "Add To Group");
			inDevelopment.addToGroup(driver, config);

			fc.utobj().printTestStep("Click on Add to Group button  displayed at the bottom of the page");
			inDevelopment.inDevelopmentPageShowAll(driver);
			inDevelopment.checkLocation(driver, location.getFranchiseID());
			fc.utobj().clickElement(driver, inDevelopmentPage.btnAddtoGroup);
			inDevelopment.addToGroup(driver, config);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
*/

	@Test(groups = { "infomgr", "IMFailedTC" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-08-10", testCaseId = "TC_Verify_InfoMgr_InDev_Actions_Print", testCaseDescription = "Test to verify print functionality from action buttons", reference = {
			"" })
	public void InfoMgr_InDevlopment_Actions_Print() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		InDevelopmentCommonMethods inDevelopment = fc.infomgr().inDevelopment();
		InfoMgrInDevelopmentPage inDevelopmentPage = new InfoMgrInDevelopmentPage(driver);
		Location location = new Location();
		LocationData ld = new LocationData();
		try {
			fc.loginpage().login(driver);
			AdminAreaRegionAddAreaRegionPageTest region_page = new AdminAreaRegionAddAreaRegionPageTest();
			String region_name = fc.utobj().generateTestData("R1");
			region_page.addAreaRegion(driver, region_name);
			imc.InfoMgrInDevelopment(driver);
			fc.utobj().clickLink(driver, "Add In Development Location");
			location = fc.commonMethods().getDefaultDataFor_Location(location);
			location.setAreaRegion(region_name);
			ld.filllocationInfo(driver, location).submit(driver);
			inDevelopment.inDevelopmentPageShowAll(driver);
			fc.utobj().printTestStep("Click on Print button displayed at the bottom of the page");
			fc.utobj().clickElement(driver, inDevelopmentPage.btnPrint);
			inDevelopment.VerifyPrintPreview(driver, location.getFranchiseID());
			fc.utobj().printTestStep(
					"Click on Print link displayed under Action button displayed at the top of the page");
			inDevelopment.inDevelopmentPageShowAll(driver);
			fc.utobj().selectActionMenuItemsWithTagA(driver, "Print");
			inDevelopment.VerifyPrintPreview(driver, location.getFranchiseID());
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr", "IMFailedTC" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Verify_InfoMgr_InDev_Actions_Modify", testCaseDescription = "Test to verify modify functionality from action buttons", reference = {
			"" })
	public void InfoMgr_InDevlopment_Actions_Modify() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		InDevelopmentCommonMethods inDevelopment = fc.infomgr().inDevelopment();

		try {
			fc.loginpage().login(driver);
			// adminInfoMgr.configureAllFieldsOnManageFormGeneratorPage(driver ,
			// config);

			String franchiseID = inDevelopment.addDevelopmentLocation(driver, config);
			inDevelopment.inDevelopmentPageShowAll(driver);

			fc.utobj().printTestStep("Modify from action menu");
			fc.utobj().actionImgOption(driver, franchiseID, "Modify");
			inDevelopment.modifyCenterInfo(driver, config);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr"})

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Verify_InfoMgr_InDev_TopActions_Archive", testCaseDescription = "Test to verify archive functionality from action buttons", reference = {
			"" })
	public void InfoMgr_InDevlopment_TopActions_Archive() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		InDevelopmentCommonMethods inDevelopment = fc.infomgr().inDevelopment();

		try {
			fc.loginpage().login(driver);
			// adminInfoMgr.configureAllFieldsOnManageFormGeneratorPage(driver ,
			// config);

			String franchiseID = inDevelopment.addDevelopmentLocation(driver, config);
			imc.InfoMgrInDevelopment(driver);
			imc.searchLocationFromShowFilter(driver, franchiseID);
			fc.utobj().printTestStep("Click on archive link displayed under the top action button");
			inDevelopment.checkLocation(driver, franchiseID);
			fc.utobj().selectActionMenuItemsWithTagA(driver, "Archive");
			driver.switchTo().alert().accept();
			fc.utobj().switchFrameToDefault(driver);
			imc.InfoMgrInDevelopment(driver);
			imc.searchLocationFromShowFilter(driver, franchiseID);
			boolean isLocationDisplayed = fc.utobj().assertLinkText(driver, franchiseID);
			if (isLocationDisplayed == false) {
				Reporter.log("Franchise Location has been archived and not displaying in Active In Development Location List.");
			} else {
				fc.utobj().throwsException("Some problem occured while archiving the franchise location as the location is still displaying at the Active in Development Location Summary List.");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr", "IMFailedTC"})

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Verify_InfoMgr_InDev_ActionImage_Archive", testCaseDescription = "Test to verify archive functionality from action image against a location", reference = {
			"" })
	public void InfoMgr_InDevlopment_ActionImage_Archive() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		InDevelopmentCommonMethods inDevelopment = fc.infomgr().inDevelopment();
		try {
			fc.loginpage().login(driver);

			String franchiseID = inDevelopment.addDevelopmentLocation(driver, config);
			inDevelopment.inDevelopmentPageShowAll(driver);
			fc.utobj().printTestStep("Archive from action menu");
			fc.utobj().actionImgOption(driver, franchiseID, "Archive");
			driver.switchTo().alert().accept();
			fc.utobj().switchFrameToDefault(driver);
			inDevelopment.inDevelopmentPageShowAll(driver);
			boolean isLocationDisplayed1 = fc.utobj().assertLinkText(driver, franchiseID);
			if (isLocationDisplayed1 == false) {
				Reporter.log("Franchise Location has been archived and not present at Active In Development location Summary.");
			} else {
				fc.utobj().throwsException("Some problem occured while archiving the franchise location ");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr"})

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Verify_InfoMgr_InDev_SummaryBottomButton_Archive", testCaseDescription = "Test to verify archive functionality from bottom Summary Archive Button", reference = {
			"" })
	public void InfoMgr_InDevlopment_SummaryBottomButton_Archive() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		InDevelopmentCommonMethods inDevelopment = fc.infomgr().inDevelopment();
		InfoMgrInDevelopmentPage inDevelopmentPage = new InfoMgrInDevelopmentPage(driver);

		try {
			fc.loginpage().login(driver);
			String franchiseID = inDevelopment.addDevelopmentLocation(driver, config);
			imc.InfoMgrInDevelopment(driver);
			imc.searchLocationFromShowFilter(driver, franchiseID);
			fc.utobj().printTestStep("Click on archive button displayed at the bottom of the page ");
			inDevelopment.checkLocation(driver, franchiseID);
			fc.utobj().clickElement(driver, inDevelopmentPage.btnArchive);
			driver.switchTo().alert().accept();
			fc.utobj().switchFrameToDefault(driver);
			imc.InfoMgrInDevelopment(driver);
			imc.searchLocationFromShowFilter(driver, franchiseID);
			boolean isLocationDisplayed2 = fc.utobj().assertLinkText(driver, franchiseID);
			if (isLocationDisplayed2 == false) {
				Reporter.log("Franchise Location has been archived");
			} else {
				fc.utobj().throwsException("Some problem occured while archiving the franchise location ");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	@Test(groups = { "infomgr"})

	@TestCase(createdOn = "2017-09-24", updatedOn = "2018-09-24", testCaseId = "TC_Verify_InfoMgr_InDev_CenterInfoAction_Archive", testCaseDescription = "Test to verify archive functionality from Center info Action Drop Down Archive Link", reference = {
			"" })
	public void InfoMgr_InDevlopment_CenterinfoAction_Archive() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		InDevelopmentCommonMethods inDevelopment = fc.infomgr().inDevelopment();
		InfoMgrCenterInfoPage ci= new InfoMgrCenterInfoPage(driver);

		try {
			fc.loginpage().login(driver);

			String franchiseID = inDevelopment.addDevelopmentLocation(driver, config);
			imc.InfoMgrInDevelopment(driver);
			imc.SearchFranchiseeAndClick(driver, franchiseID);
			fc.utobj().printTestStep("Click on archive link from Center info Actio Drop Down. ");
			fc.utobj().selectDropDown(driver, ci.drpActionMenu, "Archive");
			driver.switchTo().alert().accept();
			fc.utobj().switchFrameToDefault(driver);
			imc.InfoMgrInDevelopment(driver);
			imc.searchLocationFromShowFilter(driver, franchiseID);
			boolean isLocationDisplayed1 = fc.utobj().assertLinkText(driver, franchiseID);
			if (isLocationDisplayed1 == false) {
				Reporter.log("Franchise Location has been archived successfully and not displaying at Active In Development Summary.");
			} else {
				fc.utobj().throwsException("Some problem occured while archiving the franchise location as location is still displaying at Active In Development Summary.");
			}
			imc.searchArhivedLocationFromShowFilter(driver, franchiseID);
			boolean isLocationDisplayed2 = fc.utobj().assertPageSource(driver, franchiseID);
			if (isLocationDisplayed2 == true) {
				Reporter.log("Franchise Location has been archived Successfully and present at Archive in Development Summary.");
			} else {
				fc.utobj().throwsException("Some problem occured while archiving the franchise location as location is not present at Archive in development summary.");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	
	@Test(groups = { "infomgr", "IMFailedTC","Summary_Filter"})

	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-21", testCaseId = "TC_Verify_InfoMgr_InDev_Filter", testCaseDescription = "Test to verify filter displayed on the In Development Page", reference = {
			"" })
	public void InfoMgr_InDevlopment_Filter_Verify() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		InDevelopmentCommonMethods inDevelopment = fc.infomgr().inDevelopment();
		RegionalCommonMethods regional = new RegionalCommonMethods();

		try {
			fc.loginpage().login(driver);

			InfoMgrFranchiseeFilter franchisee = inDevelopment.addDevelopmentLocationForFilter(driver, config);
			//inDevelopment.inDevelopmentPageShowAll(driver);
			imc.InfoMgrInDevelopment(driver);
			imc.searchLocationFromShowFilter(driver, franchisee.getFranchiseeID());

			fc.utobj().actionImgOption(driver, franchisee.getFranchiseeID(), "Modify");

			fc.utobj().printTestStep("Adding spouse details by modifying the owner");
			InfoMgrOwnersPage ownerPage = new InfoMgrInDevelopmentPage(driver).getOwnersPage();
			fc.utobj().clickElement(driver, ownerPage.ownersTab);
			fc.utobj().actionImgOption(driver, franchisee.getOwnerFirstName(), "Modify");
			Map<String, String> lstOwner = inDevelopment.modifyOwner(driver, config);
			franchisee.setSpouseFirstName(lstOwner.get("SpouseFirstName"));

			fc.utobj().printTestStep("Adding area owner");
			InfoMgrOwnersPage regionalOwnerPage = new InfoMgrRegionalPage(driver).getOwnersPage();
			regional.searchAndClickAreaRegion(driver, franchisee.getAreaRegionName());
			fc.utobj().clickElement(driver, regionalOwnerPage.lnkAreaOwner);
			lstOwner = regional.addAreaRegionOwner(driver);
			franchisee.setAreaRegionOwner(lstOwner.get("FirstName"));

			System.out.println(franchisee.getStoreTypeID());

			fc.utobj().printTestStep("Verify Search Filter");
			fc.infomgr().infomgr_common().InfoMgrInDevelopment(driver);
			inDevelopment.search(driver, config, franchisee);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr", "IMFailedTC" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Verify_InfoMgr_InDevelopment_AddToCenterInfo_Fields", testCaseDescription = "Test to verify fileds added by manage form generator  to center info", reference = {
			"" })
	public void InfoMgr_InDevelopment_CenterInfo_AddToCenterInfo_Fields() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		InDevelopmentCommonMethods inDevelopment = fc.infomgr().inDevelopment();
		AdminInfoMgrCommonMethods adminInfoMgr = new AdminInfoMgrCommonMethods();
		String[] formsArray = { "Agreement", "Contract Signing", "Customer Complaints", "Employees", "Entity Details",
				"Events", "Financial", "Guarantors", "Insurance", "Legal Violation", "Lenders", "Marketing",
				"Mystery Review", "Owners", /* "Real Estate" , */ "Renewal", "Territory", "Training" };
		try {
			fc.loginpage().login(driver);
			fc.utobj().printTestStep("Add Fileds to Center Info");
			Map<String, String> dsCenterInfoFields = adminInfoMgr.configure_Franchisees_AddToCenterInfo_AllForms(driver,
					config);

			fc.utobj().printTestStep("Adding franchise Location");
			String franchiseID = inDevelopment.addDevelopmentLocation(driver, config);

			inDevelopment.searchFranchiseAndClick(driver, franchiseID);

			inDevelopment.modifyCenterInfo(driver, config);
			fc.utobj().printTestStep("Verify added fields on center info page");
			for (int i = 0; i < formsArray.length; i++) {
				try {
					fc.utobj().getElementByXpath(driver, ".//*[@id='centerInfoDIV']//td[contains(text() , '"
							+ dsCenterInfoFields.get(formsArray[i]) + "')]");
					Reporter.log(
							"Field " + dsCenterInfoFields.get(formsArray[i]) + " is displayed on the Center Info page");
				} catch (Exception ex) {
					fc.utobj().throwsException("Field " + dsCenterInfoFields.get(formsArray[i])
							+ " is not displayed on the Center Info page");
				}
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr_test1" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Verify_InfoMgr_InDevelopment_CustomFieldsandSection_CenterInfo", testCaseDescription = "Test to verify custom fields added to center info by manage form generator.", reference = {
			"" })
	public void InfoMgr_InDevelopment_CenterInfo_CustomFields() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		InDevelopmentCommonMethods inDevelopment = fc.infomgr().inDevelopment();
		AdminInfoMgrCommonMethods adminInfoMgr = new AdminInfoMgrCommonMethods();

		try {
			fc.loginpage().login(driver);

			fc.utobj().printTestStep("Add custom fields to Center Info");
			Map<String, String> dsCenterInfoCustomFields = adminInfoMgr.ManageFormGenerator_Add_Section_Fields_To_Forms(
					driver, config, "Franchisee", "Center Info", "inDev");

			fc.utobj().printTestStep("Adding in development Location");
			String franchiseID = inDevelopment.addDevelopmentLocation(driver, config);
			inDevelopment.searchFranchiseAndClick(driver, franchiseID);
			fc.utobj().printTestStep("Verify that added custom fields are displayed on the center info page");

			for (String key : dsCenterInfoCustomFields.keySet()) {
				if (!key.contains("drpOption")) {
					if (fc.utobj().assertPageSource(driver, dsCenterInfoCustomFields.get(key))) {
						Reporter.log(
								"Custom field " + dsCenterInfoCustomFields.get(key) + " is displayed on the page !!");
					} else {
						fc.utobj().throwsException(
								"Custom field " + dsCenterInfoCustomFields.get(key) + " is not displayed on the page");
					}
				}
			}

			fc.utobj().printTestStep("Deleting the created section and its fields");
			adminInfoMgr.deleteSectionandFields(driver, config, "Franchisee", "Center Info", dsCenterInfoCustomFields);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2018-08-07", updatedOn = "2018-08-07", testCaseId = "TC_Verify_InfoMgr_InDev_TopAction_MarkAsOpen", testCaseDescription = "Test to verify Mark as Open functionality from Top Action", reference = {
			"" })
	public void InfoMgr_InDevlopment_TopAction_MarkAsOpen() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		InDevelopmentCommonMethods inDevelopment = fc.infomgr().inDevelopment();
		try {
			fc.loginpage().login(driver);

			String franchiseID = adminInfoMgr.addInDevelopmentFranchiseLocationForInfoMgrName(driver, false);
			imc.InfoMgrInDevelopment(driver);
			imc.searchLocationFromShowFilter(driver, franchiseID);
			fc.utobj().printTestStep("Click on Mark as Open link displayed under the top action button");
			inDevelopment.checkLocation(driver,franchiseID);
			fc.utobj().selectActionMenuItemsWithTagA(driver, "Mark As Open");
			driver.switchTo().alert().accept();
			fc.utobj().switchFrameToDefault(driver);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, ".//*[contains(@value,'OK')]");
			fc.utobj().switchFrameToDefault(driver);
			imc.InfoMgrInDevelopment(driver);
			imc.searchLocationFromShowFilter(driver, franchiseID);
			boolean isLocationDisplayed = fc.utobj().assertLinkText(driver,franchiseID);
			if (isLocationDisplayed == false) {
				Reporter.log("Franchise Location has been Mark As Open and location is remove from In Devepment Summary Page");
			} else {
				fc.utobj().throwsException("Some problem occurred while making location as Open as Location still present at In Development Summary Page. ");
			}

			imc.InfoMgrFranchisees(driver);
			imc.searchLocationFromShowFilter(driver,franchiseID);
			boolean isLocationDisplayed1 = fc.utobj().assertLinkText(driver,franchiseID);
			if (isLocationDisplayed1 == true) {
				Reporter.log("Franchise Location has been Mark As Open and location is present at Franchisee Summary Page");
			} else {
				fc.utobj().throwsException("Some problem occurred while making location as Open and location is not present at Franchisee Summary Page. ");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr"})

	@TestCase(createdOn = "2018-08-07", updatedOn = "2018-08-07", testCaseId = "TC_Verify_InfoMgr_InDev_ActionIcon_MarkAsOpen", testCaseDescription = "Test to verify Mark as Open functionality from Action in front of In Development location.", reference = {
			"" })
	public void InfoMgr_InDevlopment_ActionIcon_MarkAsOpen() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			fc.loginpage().login(driver);
			String franchiseID = adminInfoMgr.addInDevelopmentFranchiseLocationForInfoMgrName(driver, false);
			imc.InfoMgrInDevelopment(driver);
			imc.searchLocationFromShowFilter(driver, franchiseID);
			fc.utobj().printTestStep("Click on Mark as Open link displayed under the top action button");
			fc.utobj().actionImgOption(driver, franchiseID, "Mark As Open");
			driver.switchTo().alert().accept();
			fc.utobj().switchFrameToDefault(driver);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, ".//*[contains(@value,'OK')]");
			fc.utobj().switchFrameToDefault(driver);
			imc.InfoMgrInDevelopment(driver);
			imc.searchLocationFromShowFilter(driver, franchiseID);
			boolean isLocationDisplayed = fc.utobj().assertLinkText(driver,franchiseID);
			if (isLocationDisplayed == false) {
				Reporter.log("Franchise Location has been Mark As Open and location is remove from In Devepment Summary Page");
			} else {
				fc.utobj().throwsException("Some problem occurred while making location as Open as Location still present at In Development Summary Page. ");
			}
			imc.InfoMgrFranchisees(driver);
			imc.searchLocationFromShowFilter(driver,franchiseID);
			boolean isLocationDisplayed1 = fc.utobj().assertLinkText(driver, franchiseID);
			if (isLocationDisplayed1 == true) {
				Reporter.log("Franchise Location has been Mark As Open and location is present at Franchisee Summary Page");
			} else {
				fc.utobj().throwsException("Some problem occurred while making location as Open and location is not present at Franchisee Summary Page.");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	@Test(groups = { "infomgr"})

	@TestCase(createdOn = "2018-09-19", updatedOn = "2018-09-19", testCaseId = "TC_Verify_InfoMgr_InDev_OpenLocation_CenterInfoActionMenu_MarkAsOpen", testCaseDescription = "Test to verify Mark as Open functionality from Center Info Action Drop Down.", reference = {
			"" })
	public void InfoMgr_InDevlopment_CenterInfoAction_MarkAsOpen() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		InfoMgrCenterInfoPage ci= new InfoMgrCenterInfoPage(driver);
		try {
			fc.loginpage().login(driver);

			String franchiseID = adminInfoMgr.addInDevelopmentFranchiseLocationForInfoMgrName(driver, false);
			imc.InfoMgrInDevelopment(driver);
			imc.SearchFranchiseeAndClick(driver, franchiseID);
			fc.utobj().printTestStep("Click on Mark as Open link displayed under the Action Drop Down at Center Info");
			fc.utobj().selectDropDown(driver, ci.drpActionMenu, "Mark As Open");
			driver.switchTo().alert().accept();
			fc.utobj().switchFrameToDefault(driver);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, ".//*[contains(@value,'OK')]");
			fc.utobj().switchFrameToDefault(driver);
			imc.InfoMgrInDevelopment(driver);
			imc.searchLocationFromShowFilter(driver, franchiseID);
			boolean isLocationDisplayed = fc.utobj().assertLinkText(driver,franchiseID);
			if (isLocationDisplayed == false) {
				Reporter.log("Franchise Location has been Mark As Open and not present at In Development Summary.");
			} else {
				fc.utobj().throwsException("Some problem occurred while making location as Open as the location still present at In Development Summary. ");
			}

			imc.InfoMgrFranchisees(driver);
			imc.searchLocationFromShowFilter(driver, franchiseID);
			boolean isLocationDisplayed1 = fc.utobj().assertLinkText(driver,franchiseID);
			if (isLocationDisplayed1 == true) {
				Reporter.log("Franchise Location has been Mark As Open and present at Franchisee Summary.");
			} else {
				fc.utobj().throwsException("Some problem occurred while making location as Open as Location is not displaying at Franchisee Summary page.");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}


	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2018-08-08", updatedOn = "2018-08-08", testCaseId = "TC_Verify_InfoMgr_InDev_Summary_TopSearch", testCaseDescription = "Test to verify Top Search for In Development locations.", reference = {
			"" })
	public void InfoMgr_InDevlopment_Summary_TopSearch() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Location location = new Location();
		LocationData ld = new LocationData();
		InfoMgrDashboardPage imdp = new InfoMgrDashboardPage(driver);
		try {
			fc.loginpage().login(driver);
			AdminAreaRegionAddAreaRegionPageTest region_page = new AdminAreaRegionAddAreaRegionPageTest();
			String region_name = fc.utobj().generateTestData("R1");
			region_page.addAreaRegion(driver, region_name);
			imc.InfoMgrInDevelopment(driver);
			fc.utobj().clickLink(driver, "Add In Development Location");
			location = fc.commonMethods().getDefaultDataFor_Location(location);
			location.setAreaRegion(region_name);
			ld.filllocationInfo(driver, location).submit(driver);

			// location.setFranchiseID("Default08030235i");
			imc.InfoMgrInDevelopment(driver);
			fc.utobj().sendKeys(driver, imdp.SearchBoxDashboard, location.getFranchiseID());
			fc.utobj().clickElement(driver, imdp.SearchBoxIconDashboard);
			fc.utobj().assertPageSource(driver, location.getFranchiseID());
			imc.InfoMgrInDevelopment(driver);
			fc.utobj().sendKeys(driver, imdp.SearchBoxDashboard, location.getFranchiseID());
			fc.utobj().assertPageSource(driver, location.getFranchiseID());
			fc.utobj().clickEnterOnElement(driver, imdp.SearchBoxDashboard);

			imc.InfoMgrInDevelopment(driver);
			fc.utobj().sendKeys(driver, imdp.SearchBoxDashboard, location.getFranchiseID());
			fc.utobj().sleep(2000);
			fc.utobj().clickElementByJS(driver,
					fc.utobj().getElementByXpath(driver, ".//*[.='" + location.getFranchiseID() + "']"));
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2018-09-11", updatedOn = "2018-09-11", testCaseId = "TC_Verify_InfoMgr_InDev_Summary_Alert_Verify", testCaseDescription = "Test to verify Alert messages after click on Action image options at top without selecting any Franchisee to perform Actions.", reference = {
			"" })
	public void InfoMgr_InDevlopment_Summary_Alert_Verify() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("infomgr", testCaseId);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		InfoMgrInDevelopmentPage inDevelopmentPage = new InfoMgrInDevelopmentPage(driver);
		try {
			fc.loginpage().login(driver);
			// adminInfoMgr.addInDevelopmentFranchiseLocationForInfoMgrName(driver, false);
			imc.InfoMgrInDevelopment(driver);
			fc.utobj().printTestStep(
					"Click on Log a Task Action Link displayed at the Top of the page without selecting any location check box to verify the Alert.");
			fc.utobj().selectActionMenuItemsWithTagA(driver, "Log a Task");
			String AlertTxtLogaTask = fc.utobj().acceptAlertBox(driver);
			// String LogATaskAlertMessage ="Please select Franchisee(s) to Associate with
			// Task.";

			String LogATaskAlertMessage = dataSet.get("LogATaskAlertMessage");

			if (AlertTxtLogaTask.equals(LogATaskAlertMessage)) {
				fc.utobj().printTestStep(
						"Pass : Alert message after click on Log a Task Link without selecting any franchisee box is coming as Expected");
			} else {
				fc.utobj().printTestStep(
						"Fail : Alert message after click on Log A Task Link without selecting any franchisee box is not coming as Expected");
				fc.utobj().throwsException("Log a Task Alert Message not Matched with Expected!!! ");
			}
			// Log a Call Alert Verify
			fc.utobj().printTestStep(
					"Click on Log a Call Action Link displayed at the Top of the page without selecting any location check box to verify the Alert.");
			fc.utobj().selectActionMenuItemsWithTagA(driver, "Log a Call");
			String AlertTxtLogaCall = fc.utobj().acceptAlertBox(driver);
			// String LogACallAlertMessage ="Please select Franchisee(s) to Associate with
			// Call.";
			String LogACallAlertMessage = dataSet.get("LogACallAlertMessage");
			if (AlertTxtLogaCall.equals(LogACallAlertMessage)) {
				fc.utobj().printTestStep(
						"Pass : Alert message after click on Log a Call Link without selecting any franchisee box is coming as Expected");
			} else {
				fc.utobj().printTestStep(
						"Fail : Alert message after click on Log A Call Link without selecting any franchisee box is not coming as Expected");
				fc.utobj().throwsException("Log a Call Alert Message not Matched with Expected!!! ");
			}

			// Add to Group
			fc.utobj().printTestStep(
					"Click on Add to Group Action Link displayed at the Top of the page without selecting any location check box to verify the Alert.");
			fc.utobj().selectActionMenuItemsWithTagA(driver, "Add To Group");
			String AlertTxtAddToGroup = fc.utobj().acceptAlertBox(driver);
			// String AddToGroupAlertMessage ="Please select a Franchisee to add to group.";
			String AddToGroupAlertMessage = dataSet.get("AddToGroupAlertMessage");
			if (AlertTxtAddToGroup.equals(AddToGroupAlertMessage)) {
				fc.utobj().printTestStep(
						"Pass : Alert message after click on Add to Group Link without selecting any franchisee box is coming as Expected");
			} else {
				fc.utobj().printTestStep(
						"Fail : Alert message after click on Add to Group Link without selecting any franchisee box is not coming as Expected");
				fc.utobj().throwsException("Add to Group Alert Message not Matched with Expected!!! ");
			}

			// Mark As Open
			fc.utobj().printTestStep(
					"Click on Mark As Open Action Link displayed at the Top of the page without selecting any location check box to verify the Alert.");
			fc.utobj().selectActionMenuItemsWithTagA(driver, "Mark As Open");
			String AlertTxtmarkAsOpen = fc.utobj().acceptAlertBox(driver);
			// String MarkAsOpenAlertMessage ="Please select Franchisee(s) to Mark as
			// Open.";
			String MarkAsOpenAlertMessage = dataSet.get("MarkAsOpenAlertMessage");

			if (AlertTxtmarkAsOpen.equals(MarkAsOpenAlertMessage)) {
				fc.utobj().printTestStep(
						"Pass : Alert message after click on Mark As Open Link without selecting any franchisee box is coming as Expected");
			} else {
				fc.utobj().printTestStep(
						"Fail : Alert message after click on Mark As Open Link without selecting any franchisee box is not coming as Expected");
				fc.utobj().throwsException("Mark As Open Alert Message not Matched with Expected!!! ");
			}

			// Archive
			fc.utobj().printTestStep(
					"Click on Archive Action Link displayed at the Top of the page without selecting any location check box to verify the Alert.");
			fc.utobj().selectActionMenuItemsWithTagA(driver, "Archive");
			String AlertTxtArchive = fc.utobj().acceptAlertBox(driver);
			// String ArchiveAlertMessage ="Please select Franchisee(s) to Archive.";
			String ArchiveAlertMessage = dataSet.get("ArchiveAlertMessage");
			if (AlertTxtArchive.equals(ArchiveAlertMessage)) {
				fc.utobj().printTestStep(
						"Pass : Alert message after click on Archive Link without selecting any franchisee box is coming as Expected");
			} else {
				fc.utobj().printTestStep(
						"Fail : Alert message after click on Archive Link without selecting any franchisee box is not coming as Expected");
				fc.utobj().throwsException("Archive Alert Message not Matched with Expected!!! ");
			}

			// Associate with Campaign
			fc.utobj().printTestStep(
					"Click on Associate with Campaign Action Link displayed at the Top of the page without selecting any location check box to verify the Alert.");
			fc.utobj().selectActionMenuItemsWithTagA(driver, "Associate With Campaign");
			String AlertTxtAssociateWithCampaign = fc.utobj().acceptAlertBox(driver);
			// String AssociateWithCampaignAlertMessage ="Please select at least one
			// Franchisee to associate with campaign.";
			String AssociateWithCampaignAlertMessage = dataSet.get("AssociateWithCampaignAlertMessage");
			if (AlertTxtAssociateWithCampaign.equals(AssociateWithCampaignAlertMessage)) {
				fc.utobj().printTestStep(
						"Pass : Alert message after click on Associate with Campaign Link without selecting any franchisee box is coming as Expected");
			} else {
				fc.utobj().printTestStep(
						"Fail : Alert message after click on Associate with Campaign Link without selecting any franchisee box is not coming as Expected");
				fc.utobj().throwsException("Associate With Campaign Alert Message not Matched with Expected!!! ");
			}

			// Send Email
			fc.utobj().printTestStep(
					"Click on Send Email Action Link displayed at the Top of the page without selecting any location check box to verify the Alert.");
			fc.utobj().selectActionMenuItemsWithTagA(driver, "Send Email");
			String AlertTxtSendEmail = fc.utobj().acceptAlertBox(driver);
			// String SendEmailAlertMessage ="Please select Franchisee(s) to Send Email.";
			String SendEmailAlertMessage = dataSet.get("SendEmailAlertMessage");
			if (AlertTxtSendEmail.equals(SendEmailAlertMessage)) {
				fc.utobj().printTestStep(
						"Pass : Alert message after click on Send Email Link without selecting any franchisee box is coming as Expected");
			} else {
				fc.utobj().printTestStep(
						"Fail : Alert message after click on Send Email Link without selecting any franchisee box is not coming as Expected");
				fc.utobj().throwsException("Send Email Alert Message not Matched with Expected!!! ");
			}

			// Mail Merge
			fc.utobj().printTestStep(
					"Click on Mail Merge Action Link displayed at the Top of the page without selecting any location check box to verify the Alert.");
			fc.utobj().selectActionMenuItemsWithTagA(driver, "Mail Merge");
			String AlertTxtMailMerge = fc.utobj().acceptAlertBox(driver);
			// String MailMergeAlertMessage ="Please select at least one Franchisee.";
			String MailMergeAlertMessage = dataSet.get("MailMergeAlertMessage");
			if (AlertTxtMailMerge.equals(MailMergeAlertMessage)) {
				fc.utobj().printTestStep(
						"Pass : Alert message after click on Mail Merge Link without selecting any franchisee box is coming as Expected");
			} else {
				fc.utobj().printTestStep(
						"Fail : Alert message after click on Mail Merge Link without selecting any franchisee box is not coming as Expected");
				fc.utobj().throwsException("Mail Merge Alert Message not Matched with Expected!!! ");
			}

			// View in Map
			fc.utobj().printTestStep(
					"Click on View in Map Action Link displayed at the Top of the page without selecting any location check box to verify the Alert.");
			fc.utobj().selectActionMenuItemsWithTagA(driver, "View in Map");
			String AlertTxtViewinMap = fc.utobj().acceptAlertBox(driver);
			// String ViewinMapAlertMessage ="Please select at least one Franchise ID.";
			String ViewinMapAlertMessage = dataSet.get("ViewinMapAlertMessage");
			if (AlertTxtViewinMap.equals(ViewinMapAlertMessage)) {
				fc.utobj().printTestStep(
						"Pass : Alert message after click on View in Map Link without selecting any franchisee box is coming as Expected");
			} else {
				fc.utobj().printTestStep(
						"Fail : Alert message after click on View in Map Link without selecting any franchisee box is not coming as Expected");
				fc.utobj().throwsException("View In Map Alert Message not Matched with Expected!!! ");
			}

			// ********** Bottom button Alert Message Verify *************************

			fc.utobj().printTestStep(
					"Click on Log a Task Button displayed at the Bottom of the page without selecting any location check box to verify the Alert.");
			fc.utobj().clickElement(driver, inDevelopmentPage.btnLogATask);
			String AlertTxtLogaTask1 = fc.utobj().acceptAlertBox(driver);
			// String LogATaskAlertMessage ="Please select Franchisee(s) to Associate with
			// Task.";

			String LogATaskAlertMessage1 = dataSet.get("LogATaskAlertMessage");

			if (AlertTxtLogaTask1.equals(LogATaskAlertMessage1)) {
				fc.utobj().printTestStep(
						"Pass : Alert message after click on Log a Task Button without selecting any franchisee box is coming as Expected");
			} else {
				fc.utobj().printTestStep(
						"Fail : Alert message after click on Log A Task Button without selecting any franchisee box is not coming as Expected");
				fc.utobj().throwsException("Log a Task Alert Message not Matched with Expected!!! ");
			}
			// Log a Call Alert Verify
			fc.utobj().printTestStep(
					"Click on Log a Call Button displayed at the Bottom of the page without selecting any location check box to verify the Alert.");
			fc.utobj().clickElement(driver, inDevelopmentPage.btnLogACall);
			String AlertTxtLogaCall1 = fc.utobj().acceptAlertBox(driver);
			// String LogACallAlertMessage ="Please select Franchisee(s) to Associate with
			// Call.";
			String LogACallAlertMessage1 = dataSet.get("LogACallAlertMessage");
			if (AlertTxtLogaCall1.equals(LogACallAlertMessage1)) {
				fc.utobj().printTestStep(
						"Pass : Alert message after click on Log a Call Button without selecting any franchisee box is coming as Expected");
			} else {
				fc.utobj().printTestStep(
						"Fail : Alert message after click on Log A Call Button without selecting any franchisee box is not coming as Expected");
				fc.utobj().throwsException("Log a Call Alert Message not Matched with Expected!!! ");
			}

			// Add to Group
			fc.utobj().printTestStep(
					"Click on Add to Group Button displayed at the Bottom of the page without selecting any location check box to verify the Alert.");
			fc.utobj().clickElement(driver, inDevelopmentPage.btnAddtoGroup);
			String AlertTxtAddToGroup1 = fc.utobj().acceptAlertBox(driver);
			// String AddToGroupAlertMessage ="Please select a Franchisee to add to group.";
			String AddToGroupAlertMessage1 = dataSet.get("AddToGroupAlertMessage");
			if (AlertTxtAddToGroup1.equals(AddToGroupAlertMessage1)) {
				fc.utobj().printTestStep(
						"Pass : Alert message after click on Add to Group Button without selecting any franchisee box is coming as Expected");
			} else {
				fc.utobj().printTestStep(
						"Fail : Alert message after click on Add to Group Button without selecting any franchisee box is not coming as Expected");
				fc.utobj().throwsException("Add to Group Alert Message not Matched with Expected!!! ");
			}

			// Archive
			fc.utobj().printTestStep(
					"Click on Archive Action Link displayed at the Bottom of the page without selecting any location check box to verify the Alert.");
			fc.utobj().clickElement(driver, inDevelopmentPage.btnArchive);
			String AlertTxtArchive1 = fc.utobj().acceptAlertBox(driver);
			// String ArchiveAlertMessage ="Please select Franchisee(s) to Archive.";
			String ArchiveAlertMessage1 = dataSet.get("ArchiveAlertMessage");
			if (AlertTxtArchive1.equals(ArchiveAlertMessage1)) {
				fc.utobj().printTestStep(
						"Pass : Alert message after click on Archive Button without selecting any franchisee box is coming as Expected");
			} else {
				fc.utobj().printTestStep(
						"Fail : Alert message after click on Archive Button without selecting any franchisee box is not coming as Expected");
				fc.utobj().throwsException("Archive Alert Message not Matched with Expected!!! ");
			}

			// Associate with Campaign
			fc.utobj().printTestStep(
					"Click on Associate with Campaign Button displayed at the Bottom of the page without selecting any location check box to verify the Alert.");
			fc.utobj().clickElement(driver, inDevelopmentPage.associateWithCampaignBtn);
			String AlertTxtAssociateWithCampaign1 = fc.utobj().acceptAlertBox(driver);
			// String AssociateWithCampaignAlertMessage ="Please select at least one
			// Franchisee to associate with campaign.";
			String AssociateWithCampaignAlertMessage1 = dataSet.get("AssociateWithCampaignAlertMessage");
			if (AlertTxtAssociateWithCampaign1.equals(AssociateWithCampaignAlertMessage1)) {
				fc.utobj().printTestStep(
						"Pass : Alert message after click on Associate with Campaign Button without selecting any franchisee box is coming as Expected");
			} else {
				fc.utobj().printTestStep(
						"Fail : Alert message after click on Associate with Campaign Button without selecting any franchisee box is not coming as Expected");
				fc.utobj().throwsException("Associate With Campaign Alert Message not Matched with Expected!!! ");
			}

			// Send Email
			fc.utobj().printTestStep(
					"Click on Send Email buttom displayed at the Bottom of the page without selecting any location check box to verify the Alert.");
			fc.utobj().clickElement(driver, inDevelopmentPage.btnSendMail);
			String AlertTxtSendEmail1 = fc.utobj().acceptAlertBox(driver);
			// String SendEmailAlertMessage ="Please select Franchisee(s) to Send Email.";
			String SendEmailAlertMessage1 = dataSet.get("SendEmailAlertMessage");
			if (AlertTxtSendEmail1.equals(SendEmailAlertMessage1)) {
				fc.utobj().printTestStep(
						"Pass : Alert message after click on Send Email Button without selecting any franchisee box is coming as Expected");
			} else {
				fc.utobj().printTestStep(
						"Fail : Alert message after click on Send Email Button without selecting any franchisee box is not coming as Expected");
				fc.utobj().throwsException("Send Email Alert Message not Matched with Expected!!! ");
			}

			// Mail Merge
			fc.utobj().printTestStep(
					"Click on Mail Merge Button displayed at the Bottom of the page without selecting any location check box to verify the Alert.");
			fc.utobj().clickElement(driver, inDevelopmentPage.btnMailMerge);
			String AlertTxtMailMerge1 = fc.utobj().acceptAlertBox(driver);
			// String MailMergeAlertMessage ="Please select at least one Franchisee.";
			String MailMergeAlertMessage1 = dataSet.get("MailMergeAlertMessage");
			if (AlertTxtMailMerge1.equals(MailMergeAlertMessage1)) {
				fc.utobj().printTestStep(
						"Pass : Alert message after click on Mail Merge Button without selecting any franchisee box is coming as Expected");
			} else {
				fc.utobj().printTestStep(
						"Fail : Alert message after click on Mail Merge Button without selecting any franchisee box is not coming as Expected");
				fc.utobj().throwsException("Mail Merge Alert Message not Matched with Expected!!! ");
			}

			// View in Map
			fc.utobj().printTestStep(
					"Click on View in Map Button displayed at the Bottom of the page without selecting any location check box to verify the Alert.");
			fc.utobj().clickElement(driver, inDevelopmentPage.viewInMapBtn);
			String AlertTxtViewinMap1 = fc.utobj().acceptAlertBox(driver);
			// String ViewinMapAlertMessage ="Please select at least one Franchise ID.";
			String ViewinMapAlertMessage1 = dataSet.get("ViewinMapAlertMessage");
			if (AlertTxtViewinMap1.equals(ViewinMapAlertMessage1)) {
				fc.utobj().printTestStep(
						"Pass : Alert message after click on View in Map Button without selecting any franchisee box is coming as Expected");
			} else {
				fc.utobj().printTestStep(
						"Fail : Alert message after click on View in Map Button without selecting any franchisee box is not coming as Expected");
				fc.utobj().throwsException("View In Map Alert Message not Matched with Expected!!! ");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgrCoveredinAboveTC" })

	@TestCase(createdOn = "2018-09-11", updatedOn = "2018-09-11", testCaseId = "InfoMgr_InDevlopment_Summary_Alert_Verify_onBottomSummaryButtonClick", testCaseDescription = "Test to verify Alert messages after click on Action image options at top without selecting any Franchisee to perform Actions.", reference = {
			"" })
	public void InfoMgr_InDevlopment_Summary_Alert_Verify_onBottomSummaryButtonClick() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("infomgr", testCaseId);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		InfoMgrInDevelopmentPage inDevelopmentPage = new InfoMgrInDevelopmentPage(driver);

		try {
			fc.loginpage().login(driver);
			adminInfoMgr.addInDevelopmentFranchiseLocationForInfoMgrName(driver, false);
			imc.InfoMgrInDevelopment(driver);
			fc.utobj().printTestStep(
					"Click on Log a Task Action Link displayed at the Top of the page without selecting any location check box to verify the Alert.");
			fc.utobj().clickElement(driver, inDevelopmentPage.btnLogATask);
			String AlertTxtLogaTask = fc.utobj().acceptAlertBox(driver);
			// String LogATaskAlertMessage ="Please select Franchisee(s) to Associate with
			// Task.";

			String LogATaskAlertMessage = dataSet.get("LogATaskAlertMessage");

			if (AlertTxtLogaTask.equals(LogATaskAlertMessage)) {
				fc.utobj().printTestStep(
						"Pass : Alert message after click on Log a Task Link without selecting any franchisee box is coming as Expected");
			} else {
				fc.utobj().printTestStep(
						"Fail : Alert message after click on Log A Task Link without selecting any franchisee box is not coming as Expected");
				fc.utobj().throwsException("Log a Task Alert Message not Matched with Expected!!! ");
			}
			// Log a Call Alert Verify
			fc.utobj().printTestStep(
					"Click on Log a Call Action Link displayed at the Top of the page without selecting any location check box to verify the Alert.");
			fc.utobj().clickElement(driver, inDevelopmentPage.btnLogACall);
			String AlertTxtLogaCall = fc.utobj().acceptAlertBox(driver);
			// String LogACallAlertMessage ="Please select Franchisee(s) to Associate with
			// Call.";
			String LogACallAlertMessage = dataSet.get("LogACallAlertMessage");
			if (AlertTxtLogaCall.equals(LogACallAlertMessage)) {
				fc.utobj().printTestStep(
						"Pass : Alert message after click on Log a Call Link without selecting any franchisee box is coming as Expected");
			} else {
				fc.utobj().printTestStep(
						"Fail : Alert message after click on Log A Call Link without selecting any franchisee box is not coming as Expected");
				fc.utobj().throwsException("Log a Call Alert Message not Matched with Expected!!! ");
			}

			// Add to Group
			fc.utobj().printTestStep(
					"Click on Add to Group Action Link displayed at the Top of the page without selecting any location check box to verify the Alert.");
			fc.utobj().clickElement(driver, inDevelopmentPage.btnAddtoGroup);
			String AlertTxtAddToGroup = fc.utobj().acceptAlertBox(driver);
			// String AddToGroupAlertMessage ="Please select a Franchisee to add to group.";
			String AddToGroupAlertMessage = dataSet.get("AddToGroupAlertMessage");
			if (AlertTxtAddToGroup.equals(AddToGroupAlertMessage)) {
				fc.utobj().printTestStep(
						"Pass : Alert message after click on Add to Group Link without selecting any franchisee box is coming as Expected");
			} else {
				fc.utobj().printTestStep(
						"Fail : Alert message after click on Add to Group Link without selecting any franchisee box is not coming as Expected");
				fc.utobj().throwsException("Add to Group Alert Message not Matched with Expected!!! ");
			}

			// Archive
			fc.utobj().printTestStep(
					"Click on Archive Action Link displayed at the Top of the page without selecting any location check box to verify the Alert.");
			fc.utobj().clickElement(driver, inDevelopmentPage.btnArchive);
			String AlertTxtArchive = fc.utobj().acceptAlertBox(driver);
			// String ArchiveAlertMessage ="Please select Franchisee(s) to Archive.";
			String ArchiveAlertMessage = dataSet.get("ArchiveAlertMessage");
			if (AlertTxtArchive.equals(ArchiveAlertMessage)) {
				fc.utobj().printTestStep(
						"Pass : Alert message after click on Archive Link without selecting any franchisee box is coming as Expected");
			} else {
				fc.utobj().printTestStep(
						"Fail : Alert message after click on Archive Link without selecting any franchisee box is not coming as Expected");
				fc.utobj().throwsException("Archive Alert Message not Matched with Expected!!! ");
			}

			// Associate with Campaign
			fc.utobj().printTestStep(
					"Click on Associate with Campaign Action Link displayed at the Top of the page without selecting any location check box to verify the Alert.");
			fc.utobj().clickElement(driver, inDevelopmentPage.associateWithCampaignBtn);
			String AlertTxtAssociateWithCampaign = fc.utobj().acceptAlertBox(driver);
			// String AssociateWithCampaignAlertMessage ="Please select at least one
			// Franchisee to associate with campaign.";
			String AssociateWithCampaignAlertMessage = dataSet.get("AssociateWithCampaignAlertMessage");
			if (AlertTxtAssociateWithCampaign.equals(AssociateWithCampaignAlertMessage)) {
				fc.utobj().printTestStep(
						"Pass : Alert message after click on Associate with Campaign Link without selecting any franchisee box is coming as Expected");
			} else {
				fc.utobj().printTestStep(
						"Fail : Alert message after click on Associate with Campaign Link without selecting any franchisee box is not coming as Expected");
				fc.utobj().throwsException("Associate With Campaign Alert Message not Matched with Expected!!! ");
			}

			// Send Email
			fc.utobj().printTestStep(
					"Click on Send Email Action Link displayed at the Top of the page without selecting any location check box to verify the Alert.");
			fc.utobj().clickElement(driver, inDevelopmentPage.btnSendMail);
			String AlertTxtSendEmail = fc.utobj().acceptAlertBox(driver);
			// String SendEmailAlertMessage ="Please select Franchisee(s) to Send Email.";
			String SendEmailAlertMessage = dataSet.get("SendEmailAlertMessage");
			if (AlertTxtSendEmail.equals(SendEmailAlertMessage)) {
				fc.utobj().printTestStep(
						"Pass : Alert message after click on Send Email Link without selecting any franchisee box is coming as Expected");
			} else {
				fc.utobj().printTestStep(
						"Fail : Alert message after click on Send Email Link without selecting any franchisee box is not coming as Expected");
				fc.utobj().throwsException("Send Email Alert Message not Matched with Expected!!! ");
			}

			// Mail Merge
			fc.utobj().printTestStep(
					"Click on Mail Merge Action Link displayed at the Top of the page without selecting any location check box to verify the Alert.");
			fc.utobj().clickElement(driver, inDevelopmentPage.btnMailMerge);
			String AlertTxtMailMerge = fc.utobj().acceptAlertBox(driver);
			// String MailMergeAlertMessage ="Please select at least one Franchisee.";
			String MailMergeAlertMessage = dataSet.get("MailMergeAlertMessage");
			if (AlertTxtMailMerge.equals(MailMergeAlertMessage)) {
				fc.utobj().printTestStep(
						"Pass : Alert message after click on Mail Merge Link without selecting any franchisee box is coming as Expected");
			} else {
				fc.utobj().printTestStep(
						"Fail : Alert message after click on Mail Merge Link without selecting any franchisee box is not coming as Expected");
				fc.utobj().throwsException("Mail Merge Alert Message not Matched with Expected!!! ");
			}

			// View in Map
			fc.utobj().printTestStep(
					"Click on View in Map Action Link displayed at the Top of the page without selecting any location check box to verify the Alert.");
			fc.utobj().clickElement(driver, inDevelopmentPage.viewInMapBtn);
			String AlertTxtViewinMap = fc.utobj().acceptAlertBox(driver);
			// String ViewinMapAlertMessage ="Please select at least one Franchise ID.";
			String ViewinMapAlertMessage = dataSet.get("ViewinMapAlertMessage");
			if (AlertTxtViewinMap.equals(ViewinMapAlertMessage)) {
				fc.utobj().printTestStep(
						"Pass : Alert message after click on View in Map Link without selecting any franchisee box is coming as Expected");
			} else {
				fc.utobj().printTestStep(
						"Fail : Alert message after click on View in Map Link without selecting any franchisee box is not coming as Expected");
				fc.utobj().throwsException("View In Map Alert Message not Matched with Expected!!! ");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2017-09-10", updatedOn = "2018-09-10", testCaseId = "TC_Verify_InfoMgr_InDev_Activate_Archived_In_Development_Location", testCaseDescription = "Test to verify Activate an Archived In Development location", reference = {
			"" })
	public void InfoMgr_InDevlopment_Activate_Archived_IN_Development_Location() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		InDevelopmentCommonMethods inDevelopment = fc.infomgr().inDevelopment();
		InfoMgrInDevelopmentPage inDevelopmentPage = new InfoMgrInDevelopmentPage(driver);
		try {
			fc.loginpage().login(driver);

			String franchiseID = adminInfoMgr.addInDevelopmentFranchiseLocationForInfoMgrName(driver, false);
			inDevelopment.inDevelopmentPageShowAll(driver);
			fc.utobj().printTestStep("Click on archive button displayed at the bottom of the page ");
			inDevelopment.checkLocation(driver, franchiseID);
			fc.utobj().clickElement(driver, inDevelopmentPage.btnArchive);
			driver.switchTo().alert().accept();
			fc.utobj().switchFrameToDefault(driver);
			inDevelopment.inDevelopmentPageShowAll(driver);
			boolean isLocationDisplayed2 = fc.utobj().assertLinkText(driver, franchiseID);
			if (isLocationDisplayed2 == false) {
				Reporter.log("Franchise Location has been archived");
			} else {
				fc.utobj().throwsException("Some problem occured while archiving the franchise location ");
			}
			imc.searchArhivedLocationFromShowFilter(driver, franchiseID);
			inDevelopment.checkArchivedLocation(driver, franchiseID);
			fc.utobj().clickElement(driver, inDevelopmentPage.activeBtn);
			fc.utobj().acceptAlertBox(driver);
			imc.InfoMgrInDevelopment(driver);
			imc.searchLocationFromShowFilter(driver, franchiseID);
			boolean isFranchiseeActivated = fc.utobj().assertPageSource(driver, franchiseID);

			if (isFranchiseeActivated) {
				fc.utobj().printTestStep("Archived In Development Location is successfully Activated");
			} else {
				fc.utobj().printTestStep("There is an Error while Activating the Archived In Development Location");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2017-09-10", updatedOn = "2018-09-10", testCaseId = "InfoMgr_InDevlopment_Archived_InDevelopment_Summary_Print_Verify", testCaseDescription = "Test to verify archived In Development Summary Print Button Functionality", reference = {
			"" })
	public void InfoMgr_InDevlopment_Archived_InDevelopment_Summary_Print_Verify() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		InDevelopmentCommonMethods inDevelopment = fc.infomgr().inDevelopment();
		InfoMgrInDevelopmentPage inDevelopmentPage = new InfoMgrInDevelopmentPage(driver);

		try {
			fc.loginpage().login(driver);
			String franchiseID = adminInfoMgr.addInDevelopmentFranchiseLocationForInfoMgrName(driver, false);
			inDevelopment.inDevelopmentPageShowAll(driver);
			fc.utobj().printTestStep("Click on archive button displayed at the bottom of the page ");
			inDevelopment.checkLocation(driver, franchiseID);
			fc.utobj().clickElement(driver, inDevelopmentPage.btnArchive);
			driver.switchTo().alert().accept();
			fc.utobj().switchFrameToDefault(driver);
			inDevelopment.inDevelopmentPageShowAll(driver);
			boolean isLocationDisplayed2 = fc.utobj().assertLinkText(driver, franchiseID);
			if (isLocationDisplayed2 == false) {
				Reporter.log("Franchise Location has been archived");
			} else {
				fc.utobj().throwsException("Some problem occured while archiving the franchise location ");
			}
			imc.SwitchToArchivedInDevelopmentSumaryFromShowFilter(driver);
			inDevelopment.ArchivedinDevelopmentSummaryPageShowAll(driver);
			fc.utobj().printTestStep("Click on Print button displayed at the bottom of the page");
			fc.utobj().clickElement(driver, inDevelopmentPage.btnPrint);
			inDevelopment.VerifyPrintPreview(driver, franchiseID);
			fc.utobj().printTestStep(
					"Click on Print link displayed under Action button displayed at the top of the page");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2017-09-10", updatedOn = "2018-09-10", testCaseId = "InfoMgr_InDevlopment_Archived_InDevelopment_Summary_AlertMsg_Verify", testCaseDescription = "Test to verify Alert message after click on Active button at Archived In Development summary page without selecting any Franchisee.", reference = {
			"" })
	public void InfoMgr_InDevlopment_Archived_InDevelopment_Summary_AlertMsg_Verify() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("infomgr", testCaseId);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		InDevelopmentCommonMethods inDevelopment = fc.infomgr().inDevelopment();
		InfoMgrInDevelopmentPage inDevelopmentPage = new InfoMgrInDevelopmentPage(driver);

		try {
			fc.loginpage().login(driver);
			String franchiseID = adminInfoMgr.addInDevelopmentFranchiseLocationForInfoMgrName(driver, false);
			inDevelopment.inDevelopmentPageShowAll(driver);
			fc.utobj().printTestStep("Click on archive button displayed at the bottom of the page ");
			inDevelopment.checkLocation(driver, franchiseID);
			fc.utobj().clickElement(driver, inDevelopmentPage.btnArchive);
			driver.switchTo().alert().accept();
			fc.utobj().switchFrameToDefault(driver);
			inDevelopment.inDevelopmentPageShowAll(driver);
			boolean isLocationDisplayed2 = fc.utobj().assertLinkText(driver, franchiseID);
			if (isLocationDisplayed2 == false) {
				Reporter.log("Franchise Location has been archived");
			} else {
				fc.utobj().throwsException("Some problem occured while archiving the franchise location ");
			}
			imc.SwitchToArchivedInDevelopmentSumaryFromShowFilter(driver);
			inDevelopment.ArchivedinDevelopmentSummaryPageShowAll(driver);
			fc.utobj().printTestStep(
					"Click on Active button displayed at the bottom of the page without selecting any location check box to verify the Alert.");
			fc.utobj().clickElement(driver, inDevelopmentPage.activeBtn);
			String AlertTxt = fc.utobj().acceptAlertBox(driver);
			String ActiveAlertMessage = dataSet.get("ActiveAlertMessage");
			if (AlertTxt.equals(ActiveAlertMessage)) {
				fc.utobj().printTestStep(
						"Pass : Alert message after click on Active button without selecting any franchisee box is coming as Expected");
			} else {
				fc.utobj().printTestStep(
						"Fail : Alert message after click on Active button without selecting any franchisee box is not coming as Expected");
				fc.utobj().throwsException("Activate Alert Message not Matched with Expected!!! ");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	@Test(groups = { "infomgr"})

	@TestCase(createdOn = "2017-09-25", updatedOn = "2018-11-13", testCaseId = "InfoMgr_InDevlopment_CenterInfo_ViewTabOnLeft-Top", testCaseDescription = "Test to verify Tab Orientation at Center Info (Left side or on Top).", reference = {
			"" })
	public void InfoMgr_InDevlopment_CenterInfo_ViewTabOnLeftTop() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		InfoMgrCenterInfoPage ci= new InfoMgrCenterInfoPage(driver);

		try {
			fc.loginpage().login(driver);
			//Create a Corporate User through RestAPI
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail("vimal.sharma@franconnect.com");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest addCorporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			String Login_id =fc.utobj().generateTestData("C1");
			corpUser.setUserName(Login_id);
			corpUser.setFirstName(fc.utobj().generateTestData("Adam"));
			corpUser.setLastName(fc.utobj().generateTestData("Corp"));
			corpUser.setPassword("admin123");
			corpUser.setConfirmPassword("admin123");
			corpUser.setEmail("vimal.sharma@franconnect.com");
			corpUser = addCorporatePage.createDefaultUser(driver, corpUser);
			
			String franchiseID = adminInfoMgr.addInDevelopmentFranchiseLocationForInfoMgrName(driver, false);
			fc.home_page().logout(driver);
			fc.loginpage().loginWithUserNameAndPassword(driver, Login_id, "admin123");
			
			imc.InfoMgrInDevelopment(driver);
			imc.SearchFranchiseeAndClick(driver, franchiseID);
			
			boolean isTabViewOnLeft = fc.utobj().isElementPresent(driver, ci.leftTabView);
			if (isTabViewOnLeft) {
				fc.utobj().printTestStep("Left Tab View is configured by Default and now changing Tab view from Left to Top");
				fc.utobj().selectDropDown(driver, ci.drpActionMenu, "View Tabs on Top");
				boolean isTabViewOnTop = fc.utobj().isElementPresent(driver, ci.topTabView);
				if(isTabViewOnTop)
				{
					fc.utobj().printTestStep("Tab View is successfully changed to Top");
				}
				else
				{
					fc.utobj().printTestStep("There is some problem while configuring Tab View at Top");
				}
				
			} else {
				fc.utobj().printTestStep("Top Tab View is configured by Default and now changing Tab view from Top to Left");
				fc.utobj().selectDropDown(driver, ci.drpActionMenu, "View Tabs on Sidebar");
				boolean isTabViewOnLeft1 = fc.utobj().isElementPresent(driver, ci.leftTabView);
				if(isTabViewOnLeft1)
				{
					fc.utobj().printTestStep("Tab View is successfully changed to Left");
				}
				else
				{
					fc.utobj().printTestStep("There is some problem while configuring Tab View at Left");
				}
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
}
