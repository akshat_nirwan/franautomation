package com.builds.test.infomgr;

import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.common.CorporateUser;
import com.builds.test.infomgr.AdminInfoMgrCommonMethods;
import com.builds.test.infomgr.InfoMgr_Common;
import com.builds.test.support.AdminSupportManageDepartmentPageTest;
import com.builds.uimaps.fs.SearchUI;
import com.builds.uimaps.infomgr.InfoMgrBasePage;
import com.builds.uimaps.infomgr.InfoMgrContactHistoryPage;
import com.builds.uimaps.support.SupportSearchPage;
import com.builds.uimaps.support.SupportTicketsPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class InfoMgrIntegrationWithSupportTest {
	
	FranconnectUtil fc = new FranconnectUtil();
	InfoMgr_Common imc = new InfoMgr_Common();
	AdminInfoMgrCommonMethods adminInfoMgr = new AdminInfoMgrCommonMethods();
	
	
	@Test(groups = { "infomgr","IM_Support"})

	@TestCase(createdOn = "2017-10-15", updatedOn = "2018-10-24", testCaseId = "TOVerifyIntegrationOfSupportModuleWithInfoMgr", testCaseDescription = "Test to verify Integration of Support module with Info Mgr).", reference = {
			"" })
	public void InfoMgr_IntegrationWithSupport() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> dataSet = fc.utobj().readTestData("support", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			fc.loginpage().login(driver);
			SupportTicketsPage pobj = new SupportTicketsPage(driver);
			SupportTicketsPage ticketui = new SupportTicketsPage(driver);
			InfoMgrContactHistoryPage imch = new InfoMgrContactHistoryPage(driver);
			InfoMgrBasePage imbp = new InfoMgrBasePage(driver);
			AdminUsersManageCorporateUsersAddCorporateUserPageTest cu = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			String Login_id = fc.utobj().generateTestData("C1");
			corpUser.setUserName(Login_id);
			String FirstName = fc.utobj().generateTestData("AdamCorp");
			corpUser.setFirstName(FirstName);
			String LastName = fc.utobj().generateTestData("Sandler");
			corpUser.setLastName(LastName);
			corpUser.setPassword("admin123");
			corpUser.setConfirmPassword("admin123");
			corpUser.setEmail("vimal.sharma@franconnect.com");
			cu.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			String userName = corpUser.getuserFullName();
			fc.utobj().printTestStep("Navigate To Admin > Support > Manage Department");
			fc.utobj().printTestStep("Add Department");
			String departmentName = fc.utobj().generateTestData(dataSet.get("departmentName"));
			AdminSupportManageDepartmentPageTest departmentPage = new AdminSupportManageDepartmentPageTest();
			departmentPage.addDepartments(driver, departmentName, userName);
			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			String franchiseID = adminInfoMgr.addFranchiseLocationForInfoMgrName(driver, false);
			// String franchiseID = "FIMp24130670";
			fc.utobj().printTestStep("Navigate To Support > Ticket");
			fc.utobj().printTestStep("Create Ticket");
			fc.support().support_common().supportTickets(driver);
			fc.utobj().clickElement(driver, pobj.createTicketLink);
			fc.utobj().selectDropDown(driver, pobj.departmentDropDown, departmentName);
			fc.utobj().selectDropDown(driver, pobj.priorityDropDown, dataSet.get("priority"));
			fc.utobj().selectDropDown(driver, pobj.franchiseIdDropDown, franchiseID);
			fc.utobj().sendKeys(driver, pobj.phoneTextBox, "1236547893");
			// String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			String subject = fc.utobj().generateTestData("Support Ticket to Check Integration with Info Mgr");
			fc.utobj().sendKeys(driver, pobj.subjectTextBox, subject);
			// String description = fc.utobj().generateTestData(dataSet.get("description"));
			String description = "Test Support Ticket Description";
			fc.utobj().sendKeys(driver, pobj.descriptionTextBox, description);
			fc.utobj().clickElement(driver, pobj.submitButton);
			fc.utobj().clickElement(driver, pobj.okBtn);
			fc.utobj().sendKeys(driver, ticketui.supportTopSearch, subject);
			fc.utobj().clickEnterOnElement(driver, ticketui.supportTopSearch);
			String ticketNumber = fc.utobj().getText(driver,
					fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + subject
							+ "')]/preceding-sibling::td/a[contains(@href , 'supportTroubleTicketDetails')]"));
			fc.utobj().printTestStep("Verify Create Ticket");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, subject);
			if (isTextPresent == false) {
				fc.utobj().throwsSkipException("was not able to verify subject of Ticket");
			}
			boolean isTextPresent1 = fc.utobj().assertPageSource(driver, departmentName);
			if (isTextPresent1 == false) {
				fc.utobj().throwsSkipException("was not able to verify department Name");
			}
			imc.SearchFranchiseeLocationAndClick(driver, franchiseID);
			imc.InfoMgrFranchiseeContactHistory(driver);
			WebElement TicketNo = fc.utobj().getElementByXpath(driver,
					".//*[@id='ticketSection']//*[contains(text(),'" + ticketNumber + "')]");
			boolean TicketHistory = fc.utobj().isElementPresent(driver, TicketNo);
			if (TicketHistory) {
				fc.utobj().printTestStep(
						"Ticket History Section is present and now clicking on Ticket No to verify the Details");
				fc.utobj().clickElement(driver, TicketNo);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				boolean subject1 = fc.utobj().assertPageSource(driver, subject);
				if (subject1) {
					fc.utobj().printTestStep(
							"Subject Found when we open the ticket to view the details under franchisee contact history.");
				} else {
					fc.utobj().printTestStep(
							"Subject Not Found when we open the ticket to view the details under franchisee contact history.");
				}
				fc.utobj().switchFrameToDefault(driver);
				fc.utobj().clickElement(driver, ".//div[contains(text(),'close')]");
				
				fc.utobj().clickLinkIgnoreCase(driver, "Detailed Contact History");
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().clickLinkIgnoreCase(driver, "Ticket History");
				WebElement TicketNoDCH = fc.utobj().getElementByXpath(driver,
						".//*[@class='bText12lnk']//*[contains(text(),'" + ticketNumber + "')]");
				fc.utobj().clickElement(driver, TicketNoDCH);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				boolean subject2 = fc.utobj().assertPageSource(driver, subject);
				if (subject2) {
					fc.utobj()
							.printTestStep("Subjct Found at franchisee detailed contact history after open the ticket");
				} else {
					fc.utobj().printTestStep("Subject not found at detailed contact history after open the ticket");
				}
				fc.utobj().switchFrameToDefault(driver);
				fc.utobj().clickElement(driver, ".//div[contains(text(),'close')]");
				// Print
				fc.utobj().clickElement(driver, imch.btnPrint);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				boolean TicketHistoryPrint = fc.utobj().assertPageSource(driver, "Ticket History");
				if (TicketHistoryPrint) {
					fc.utobj().printTestStep("Ticket History Section is coming on Franchisee Print Window");
				} else {
					fc.utobj().printTestStep("Ticket History Section not present on Franchisee Print Window");
				}
				fc.utobj().switchFrameToDefault(driver);
				fc.utobj().clickElement(driver, ".//div[contains(text(),'close')]");

			} else {
				fc.utobj().throwsException("Ticket History Section is not present at Franchisee Contact History");
			}
			/*
			 * fc.utobj().clickElement(driver, imch.ticketHistoryPrintCheckox);
			 * fc.utobj().clickElement(driver, ".//*[@name='Submit']");
			 * imc.VerifyPrintPreview(driver, ticketNumber);
			 */

			//*******************************************************************************************************************************
			fc.utobj().printTestStep(
					"Ticket Details verified under franchisee contact History now verfifying under Entity Contact History");

			fc.utobj().clickElement(driver, ".//*[@qat_tabname='Entity Details']");

			String EntityName = fc.utobj().generateTestData("SupportEntity");
			fc.utobj().sendKeys(driver, driver.findElement(By.id("fimTtEntityName")), EntityName);
			fc.utobj().clickElement(driver, ".//*[@name='Submit']");
			imc.InfoMgrEntity(driver);
			fc.utobj().sleep(2000);
			imc.searchEntityThroughShowFilterAndClick(driver, EntityName);
			//fc.utobj().clickLink(driver,EntityName);
			fc.utobj().clickElement(driver, imbp.lnkContactHistory);

			WebElement TicketNo2 = fc.utobj().getElementByXpath(driver,
					".//*[@id='ticketSection']//*[contains(text(),'" + ticketNumber + "')]");
			boolean TicketHistory1 = fc.utobj().isElementPresent(driver, TicketNo2);
			if (TicketHistory1) {
				fc.utobj().printTestStep(
						"Ticket History Section is present and now clicking on Ticket No to verify the Details");
				fc.utobj().clickElement(driver, TicketNo2);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				boolean subject3 = fc.utobj().assertPageSource(driver, subject);
				if (subject3) {
					fc.utobj().printTestStep("Subject Found under Entity Ticket History details.");
				} else {
					fc.utobj().printTestStep("Subject not found under Entity Ticket History details.");
				}
				fc.utobj().switchFrameToDefault(driver);
				fc.utobj().clickElement(driver, ".//div[contains(text(),'close')]");

				// Print

				fc.utobj().clickElement(driver, imch.btnPrint);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				boolean TicketHistoryPrint1 = fc.utobj().assertPageSource(driver, "Ticket History");
				if (TicketHistoryPrint1) {
					fc.utobj().printTestStep("Ticket History Section is coming on Print Window");
				} else {
					fc.utobj().printTestStep("Ticket History Section not present on Print Window");
				}
				fc.utobj().switchFrameToDefault(driver);
				fc.utobj().clickElement(driver, ".//div[contains(text(),'close')]");
			} else {
				fc.utobj().throwsException(
						"Fail : Ticket History Section is not available under Entity Contact History.");
			}

			fc.utobj().printTestStep("Navigate To Support > Ticket to change the status of ticket to Closed");
			fc.support().support_common().supportTickets(driver);
			fc.utobj().sendKeys(driver, ticketui.supportTopSearch, subject);
			fc.utobj().clickEnterOnElement(driver, ticketui.supportTopSearch);

			fc.utobj().actionImgOption(driver, ticketNumber, "Update Status"); 
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sleep(2000);
			fc.utobj().selectDropDown(driver, ticketui.stausDropDown, "Closed");
			fc.utobj().clickElement(driver, ticketui.saveBtn);
			fc.utobj().sleep(2000);
			fc.utobj().clickElement(driver, ticketui.closeBtn);
			imc.SearchFranchiseeLocationAndClick(driver, franchiseID);
			imc.InfoMgrFranchiseeContactHistory(driver);
			boolean CloseTicket = fc.utobj().assertPageSource(driver, ticketNumber);
			if (!CloseTicket) {
				fc.utobj().printTestStep("Pass : Closed Ticket is not displaying in Franchisee Contact History");
			} else {
				fc.utobj().printTestStep("Fail : Closed Ticket is still displaying in Franchisee Contact History");
			}

			fc.utobj().clickElement(driver, imch.btnPrint);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			boolean TicketHistoryPrint1 = fc.utobj().assertPageSource(driver, "Ticket History");
			if (!TicketHistoryPrint1) {
				fc.utobj().printTestStep("Pass: Ticket History Section is Not coming on Print Window");
			} else {
				fc.utobj().printTestStep("Fail : Ticket History Section present on Print Window");
			}
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, ".//div[contains(text(),'close')]");
			// Verifying in Entity Contact History
			imc.InfoMgrEntity(driver);
			fc.utobj().sleep(2000);
			fc.utobj().clickLink(driver,EntityName);
			fc.utobj().clickElement(driver, imbp.lnkContactHistory);
			
			boolean CloseTicket1 = fc.utobj().assertPageSource(driver, ticketNumber);
			if (!CloseTicket1) {
				fc.utobj().printTestStep("Pass : Closed Ticket is not displaying in Franchisee Contact History");
			} else {
				fc.utobj().printTestStep("Fail : Closed Ticket is still displaying in Franchisee Contact History");
			}

			fc.utobj().clickElement(driver, imch.btnPrint);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			boolean TicketHistoryPrint2 = fc.utobj().assertPageSource(driver, "Ticket History");
			if (!TicketHistoryPrint2) {
				fc.utobj().printTestStep("Pass: Ticket History Section is Not coming on Print Window");
			} else {
				fc.utobj().printTestStep("Fail : Ticket History Section present on Print Window");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	public void searchTicketBySubject(WebDriver driver, String subject) throws Exception {

		SupportSearchPage pobj = new SupportSearchPage(driver);
		fc.utobj().clickElement(driver, pobj.searchTabLnk);
		fc.utobj().sendKeys(driver, pobj.subjectTextBox, subject);
		fc.utobj().clickElement(driver, pobj.searchBtn);
	}
	
}
