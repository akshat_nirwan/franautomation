package com.builds.test.infomgr;

public class InfoMgrModule {

	private InfoMgr_Common infomgrobj = null;

	public InfoMgr_Common infomgr_common() {
		if (infomgrobj == null) {
			infomgrobj = new InfoMgr_Common();
		}
		return infomgrobj;
	}

	public FranchiseesCommonMethods franchisees() {
		return new FranchiseesCommonMethods();
	}

	public InDevelopmentCommonMethods inDevelopment() {
		return new InDevelopmentCommonMethods();
	}

	public TerminatedCommonMethods terminated() {
		return new TerminatedCommonMethods();
	}

	public CorporateLocationsCommonMethods corporateLocations() {
		return new CorporateLocationsCommonMethods();
	}

	public RegionalCommonMethods regional() {
		return new RegionalCommonMethods();
	}

	public MultiUnitEntityCommonMethods multiUnitEntity() {
		return new MultiUnitEntityCommonMethods();
	}

	public CampaignCenterCommonMethods campaignCenter() {
		return new CampaignCenterCommonMethods();
	}

	public GroupsCommonMethods groups() {
		return new GroupsCommonMethods();
	}

	public TasksCommonMethods tasks() {
		return new TasksCommonMethods();
	}

	// Harish Dwivedi Info Mgr
	public ReportsCommonMethods reports() {
		return new ReportsCommonMethods();
	}

}
