package com.builds.test.infomgr;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.common.CorporateUser;
import com.builds.uimaps.infomgr.InfoMgrContactHistoryPage;
import com.builds.uimaps.infomgr.InfoMgrMultiUnitEntityPage;
import com.builds.uimaps.infomgr.InfoMgrMultiUnitInfoPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class InfoMgrMultiUnitEntityContactHistoryPageTest {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "infomgr5421" })

	// BugID - 82242
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_InfoMgr_MultiUnit_ContactHistory_DetailedContactHistory", testCaseDescription = "This test will verify Multi Unit's detailed contact history page", reference = {
			"82242" })
	public void InfoMgr_MultiUnit_DetailedTaskHistory() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		MultiUnitEntityCommonMethods multiUnit = fc.infomgr().multiUnitEntity();
		FranchiseesCommonMethods franchise = fc.infomgr().franchisees();
		AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();

		try {
			driver = fc.loginpage().login(driver);

			// Add First Franchisee

			// Add Region
			fc.utobj().printTestStep("Add area region");
			String regionName1 = adminInfoMgr.addAreaRegion(driver);

			// Add Division
			fc.utobj().printTestStep("Add division");
			String divName1 = adminInfoMgr.addDivisionName(driver);

			// Add Franchisee Location with division and region name
			fc.utobj().printTestStep("Add franchise location");
			String franchiseID1 = adminInfoMgr.addFranchiseLocationWithDivisionRegion(driver, divName1, regionName1);

			// search franchisee
			fc.utobj().printTestStep("Search franchises and click");
			franchise.searchFranchiseAndClick(driver, franchiseID1);

			// Add Owner
			fc.utobj().printTestStep("Add owners");
			InfoMgrMultiUnitInfoPage entityInfoPage = new InfoMgrMultiUnitEntityPage(driver).getMultiUnitInfoPage();
			fc.utobj().clickElement(driver, entityInfoPage.tabOwners);
			Map<String, String> lstOwnerDetails = franchise.addOwner(driver);

			/// ADd Second Franchisee
			fc.utobj().printTestStep("Add franchise location");
			// Add Franchisee Location with division and region name
			String franchiseID2 = adminInfoMgr.addFranchiseLocationWithDivisionRegion(driver, divName1, regionName1);

			// Search Franchisee
			fc.utobj().printTestStep("Search franchise and click");
			franchise.searchFranchiseAndClick(driver, franchiseID2);

			// Associate the owner of first franchisee to second franchisee
			fc.utobj().printTestStep("Add existing owner");
			fc.utobj().clickElement(driver, entityInfoPage.tabOwners);
			franchise.addExistingOwner(driver, lstOwnerDetails.get("FirstName"));

			// Create a corporate user
			fc.utobj().printTestStep("Add corporate user");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail("infomgrautomation@staffex.com");
			corpUser = corpTest.createDefaultUser(driver, corpUser);

			// Navigate to multi Unit and click on it
			fc.utobj().printTestStep("Search multi unit and click");
			multiUnit.searchMultiUnitAndClick(driver, lstOwnerDetails.get("FirstName"));

			fc.utobj().printTestStep("Log a task");
			InfoMgrContactHistoryPage contactHistory = new InfoMgrMultiUnitEntityPage(driver).getContactHistoryPage();
			fc.utobj().clickElement(driver, contactHistory.lnkContactHistory);
			fc.utobj().clickElement(driver, contactHistory.logATaskLnk);
			String remarks = multiUnit.logaTask(driver, corpUser.getuserFullName(), true);

			// Click on link "Detailed Contact History"
			fc.utobj().printTestStep("Click on detailed contact history and verify the task");
			fc.utobj().clickLink(driver, "Detailed Contact History");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			boolean isTaskSubjectPresent = fc.utobj().assertPageSource(driver, remarks);
			if (isTaskSubjectPresent) {
				Reporter.log("Task has been created successfully !!! ");
			} else {
				fc.utobj().throwsException("Task couldn't be added test fails !!!");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
}
