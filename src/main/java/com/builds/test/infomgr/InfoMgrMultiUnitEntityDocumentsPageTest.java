package com.builds.test.infomgr;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.uimaps.infomgr.InfoMgrMultiUnitEntityPage;
import com.builds.uimaps.infomgr.InfoMgrMultiUnitInfoPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class InfoMgrMultiUnitEntityDocumentsPageTest {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "infomgr" })

	// BugID - 72423
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_InfoMgr_MultiUnit_Documents_Delete_Verify", testCaseDescription = "This test case will verify documents delete functionality for multi unit", reference = {
			"72423" })
	public void Info_Mgr_MultiUnit_Documents_Delete_Func() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		MultiUnitEntityCommonMethods multiUnit = fc.infomgr().multiUnitEntity();
		FranchiseesCommonMethods franchise = fc.infomgr().franchisees();
		AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();

		try {
			driver = fc.loginpage().login(driver);

			// Add First Franchisee

			// Add Region
			fc.utobj().printTestStep("Add area region");
			String regionName1 = adminInfoMgr.addAreaRegion(driver);

			// Add Division
			fc.utobj().printTestStep("Add division");
			String divName1 = adminInfoMgr.addDivisionName(driver);

			// Add Franchisee Location with division and region name
			fc.utobj().printTestStep("Add franchise location");
			String franchiseID1 = adminInfoMgr.addFranchiseLocationWithDivisionRegion(driver, divName1, regionName1);

			// search franchisee
			fc.utobj().printTestStep("Search franchisee and click ");
			franchise.searchFranchiseAndClick(driver, franchiseID1);

			// Add Owner
			fc.utobj().printTestStep("Add owner");
			InfoMgrMultiUnitInfoPage entityInfoPage = new InfoMgrMultiUnitEntityPage(driver).getMultiUnitInfoPage();
			fc.utobj().clickElement(driver, entityInfoPage.tabOwners);
			Map<String, String> lstOwnerDetails = franchise.addOwner(driver);

			/// ADd Second Franchisee

			// Add Franchisee Location with division and region name
			fc.utobj().printTestStep("Add franchise location ");
			String franchiseID2 = adminInfoMgr.addFranchiseLocationWithDivisionRegion(driver, divName1, regionName1);

			// Search Franchisee
			fc.utobj().printTestStep("Search franchisee");
			franchise.searchFranchiseAndClick(driver, franchiseID2);

			// Associate the owner of first franchisee to second franchisee
			fc.utobj().printTestStep("Add existing owner");
			fc.utobj().clickElement(driver, entityInfoPage.tabOwners);
			franchise.addExistingOwner(driver, lstOwnerDetails.get("FirstName"));

			// Navigate to multi Unit and click on it
			fc.utobj().printTestStep("Search multi unit and click");
			multiUnit.searchMultiUnitAndClick(driver, lstOwnerDetails.get("FirstName"));

			// Add documents to multi Unit
			fc.utobj().printTestStep("Add document");
			String document = multiUnit.addDocument(driver);

			// Delete Document
			fc.utobj().printTestStep("Delete document");
			multiUnit.deleteDocument(driver, document);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

}
