package com.builds.test.infomgr;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.common.CorporateUser;
import com.builds.uimaps.infomgr.InfoMgrAgreementPage;
import com.builds.uimaps.infomgr.InfoMgrContactHistoryPage;
import com.builds.uimaps.infomgr.InfoMgrMultiUnitEntityPage;
import com.builds.uimaps.infomgr.InfoMgrMultiUnitInfoPage;
import com.builds.uimaps.infomgr.InfoMgrUsersPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class InfoMgrMultiUnitEntityPageTest {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "infomgr1","IM_ALL_Tabs" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-22", testCaseId = "TC_Verify_InfoMgr_MultiUnit_AllTabs_Add_Modify_Delete", testCaseDescription = "Test to add and modify data to all tabs of In Development franchise location", reference = {
			"" })
	public void InfoMgr_MultiUnit_AllTab_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		MultiUnitEntityCommonMethods multiUnit = fc.infomgr().multiUnitEntity();
		AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();
		InfoMgrMultiUnitInfoPage multiUnitInfoPage = new InfoMgrMultiUnitEntityPage(driver).getMultiUnitInfoPage();

		try {
			fc.loginpage().login(driver);
			// adminInfoMgr.configureInfoMgr(driver, config);

			fc.utobj().printTestStep("Add corporate user");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail("infomgrautomation@staffex.com");
			corpUser = corpTest.createDefaultUser(driver, corpUser);

			fc.utobj().printTestStep("Create Multi Unit");
			String multiUnitOwnerName = multiUnit.createMultiUnit(driver, config);
			Reporter.log("Multi Unit with owner  - " + multiUnitOwnerName + " Created");

			fc.utobj().printTestStep("Search and click ");
			multiUnit.searchMultiUnitAndClick(driver, multiUnitOwnerName);

			InfoMgrContactHistoryPage contactHistoryPage = new InfoMgrMultiUnitEntityPage(driver)
					.getContactHistoryPage();

			fc.utobj().printTestStep("Add data to all the tabs");

			// Add Agreement
			fc.utobj().clickElement(driver, multiUnitInfoPage.lnkAgreement);
			driver = multiUnit.addAgreement(driver, config);

			// ********** Add Contact History data ****************

			// Log a Task
			fc.utobj().clickElement(driver, contactHistoryPage.lnkContactHistory);

			fc.utobj().clickElement(driver, contactHistoryPage.btnLogaTask);
			String taskSubject = multiUnit.logaTask(driver, corpUser.getuserFullName(), true);

			// Log a Call
			fc.utobj().clickElement(driver, contactHistoryPage.btnLogaCall);
			String callSubject = multiUnit.logACall(driver, config, true);

			// Add Remark
			fc.utobj().clickElement(driver, contactHistoryPage.btnAddRemark);
			String remarkSubject = multiUnit.addRemark(driver, config, contactHistoryPage);

			// Send Message
			// fc.utobj().clickElement(driver,
			// contactHistoryPage.btnSendMessage);
			// multiUnit.sendTextMessage(driver, config, true , corporateUser);

			// Send Mail

			// fc.utobj().clickElement(driver, contactHistoryPage.btnSendEmail);
			// multiUnit.sendTextEmail(driver, config , true);

			// Sending HTML Message and mail (Pending)

			// Adding Contract Signing
			fc.utobj().clickElement(driver, contactHistoryPage.lnkContractSigning);
			multiUnit.addContractSigning(driver, config);

			// Add Customer Complaints
			fc.utobj().clickElement(driver, contactHistoryPage.lnkCustomerComplaints);
			multiUnit.addCustomerComplaints(driver, config);

			// Add Documents
			fc.utobj().clickElement(driver, contactHistoryPage.lnkDocuments);
			String docTitle = multiUnit.addDocument(driver);

			// Add Employees
			fc.utobj().clickElement(driver, contactHistoryPage.lnkEmployees);
			String empName = multiUnit.addEmployee(driver, config);

			// Add Events Details
			fc.utobj().clickElement(driver, contactHistoryPage.lnkEvents);
			multiUnit.addEventDetails(driver, config);

			// Add Finance Details
			fc.utobj().clickElement(driver, contactHistoryPage.lnkFinancial);
			multiUnit.addFinanceDetails(driver, config);

			// Add Gurantor details
			fc.utobj().clickElement(driver, contactHistoryPage.lnkGuarantors);
			multiUnit.addGuarantor(driver, config);

			// Add Insurance Details
			fc.utobj().clickElement(driver, contactHistoryPage.lnkInsurance);
			multiUnit.addInsuranceDetails(driver, config);

			// Add legal violation
			fc.utobj().clickElement(driver, contactHistoryPage.lnkLegalViolation);
			multiUnit.addLegalViolation(driver, config);

			// Add Lenders
			fc.utobj().clickElement(driver, contactHistoryPage.lnkLenders);
			multiUnit.addLenders(driver, config);

			// Add Marketing details
			fc.utobj().clickElement(driver, contactHistoryPage.lnkMarketingFinTab); // Change
																					// by
																					// Harish
																					// Dwivedi
			multiUnit.addMarketingDetails(driver, config);

			// Add Mystry Review
			fc.utobj().clickElement(driver, contactHistoryPage.lnkMysteryReview);
			multiUnit.addMystryReview(driver, config);

			// Add Owner
			fc.utobj().clickElement(driver, contactHistoryPage.tabOwners);
			Map<String, String> owner = multiUnit.addOwner(driver, config);

			// Add Picture
			fc.utobj().clickElement(driver, contactHistoryPage.lnkPictures);
			String pictureTitle = multiUnit.addPicture(driver, config);

			// Add Real Estate
			fc.utobj().clickElement(driver, contactHistoryPage.lnkRealEstate);
			multiUnit.addRealState(driver, config);

			// Add Territory
			fc.utobj().clickElement(driver, contactHistoryPage.lnkTerritory);
			multiUnit.addTerritory(driver, config);

			// Add Training
			fc.utobj().clickElement(driver, multiUnitInfoPage.lnkTraining);
			multiUnit.addTraining(driver, config);

			// Add User
			fc.utobj().clickElement(driver, contactHistoryPage.lnkUsers);
			InfoMgrUsersPage usersPage = new InfoMgrMultiUnitEntityPage(driver).getUsersPage();
			fc.utobj().clickElement(driver, usersPage.btnAddUser);
			multiUnit.addOwnerUser(driver, config, multiUnitOwnerName);

			// Add Visit
			// multiUnit.addVisit(driver, config, franchiseID, corporateUser);

			// ****************** Modify Data
			// **************************************
			fc.utobj().printTestStep("Modify data of all the tabs");

			multiUnit.searchMultiUnitAndClick(driver, multiUnitOwnerName);

			// Modify Agreement
			fc.utobj().clickElement(driver, multiUnitInfoPage.lnkAgreement);
			driver = multiUnit.modifyAgreement(driver, config);

			// Modify Center Info
			fc.utobj().clickElement(driver, multiUnitInfoPage.lnkMultiUnitInfo);
			fc.utobj().clickLink(driver, "Modify");
			multiUnit.modifyMultiUnitInfo(driver, config);

			// Modify Contact History

			// Modify a task
			fc.utobj().clickElement(driver, contactHistoryPage.lnkContactHistory);
			fc.utobj().actionImgOption(driver, taskSubject, "Modify");
			multiUnit.modifyATask(driver, config, true);

			// Modify a call
			fc.utobj().actionImgOption(driver, callSubject, "Modify");
			multiUnit.modifyACall(driver, config, true);

			// Modify a remark
			fc.utobj().actionImgOption(driver, remarkSubject, "Modify");
			multiUnit.modifyRemark(driver, config, contactHistoryPage);

			// Modify Contract Signing
			fc.utobj().clickElement(driver, multiUnitInfoPage.lnkContractSigning);
			multiUnit.modifyContractSigning(driver, config);

			// Modify Customer Complaints
			fc.utobj().clickElement(driver, multiUnitInfoPage.lnkCustomerComplaints);
			multiUnit.modifyCustomerComplaints(driver, config);

			// Modify Documents
			fc.utobj().clickElement(driver, multiUnitInfoPage.lnkDocuments);
			fc.utobj().actionImgOption(driver, docTitle, "Modify");
			docTitle = multiUnit.modifyDocument(driver, config);

			// Modify Employee
			fc.utobj().clickElement(driver, multiUnitInfoPage.lnkEmployees);
			empName = multiUnit.modifyEmployee(driver, config);

			// Modify Events
			fc.utobj().clickElement(driver, multiUnitInfoPage.lnkEvents);
			multiUnit.modifyEventDetails(driver, config);

			// Modify Financials
			fc.utobj().clickElement(driver, multiUnitInfoPage.lnkFinancial);
			multiUnit.modifyFinanceDetails(driver, config);

			// Modify Guarantor
			fc.utobj().clickElement(driver, multiUnitInfoPage.lnkGuarantors);
			multiUnit.modifyGuarantor(driver, config);
			// Modify Insurance
			fc.utobj().clickElement(driver, multiUnitInfoPage.lnkInsurance);
			multiUnit.modifyInsuranceDetails(driver, config);

			// Modify Legal Violation
			fc.utobj().clickElement(driver, multiUnitInfoPage.lnkLegalViolation);
			multiUnit.modifyLegalViolation(driver, config);

			// Modify Lenders Details
			fc.utobj().clickElement(driver, multiUnitInfoPage.lnkLenders);
			multiUnit.modifyLenders(driver, config);

			// Modify Marketing
			fc.utobj().clickElement(driver, multiUnitInfoPage.lnkMarketingFinTab); // Change
																					// By
																					// Harish
																					// Dwivedi
			multiUnit.modifyMarketingDetails(driver, config);

			// Modify Mystry Review
			fc.utobj().clickElement(driver, multiUnitInfoPage.lnkMysteryReview);
			multiUnit.modifyMystryReview(driver, config);

			// Modify Owners
			fc.utobj().clickElement(driver, multiUnitInfoPage.tabOwners);
			fc.utobj().actionImgOption(driver, owner.get("FirstName"), "Modify");
			multiUnit.modifyOwner(driver, config);

			// Modify Pictures
			fc.utobj().clickElement(driver, multiUnitInfoPage.lnkPictures);
			fc.utobj().actionImgOption(driver, pictureTitle, "Modify");
			pictureTitle = multiUnit.modifyPicture(driver, config);

			// Modify Real state
			fc.utobj().clickElement(driver, multiUnitInfoPage.lnkRealEstate);
			multiUnit.modifyRealState(driver, config);

			// Modify Territory
			fc.utobj().clickElement(driver, multiUnitInfoPage.lnkTerritory);
			multiUnit.modifyTerritory(driver, config);

			// Modify Training
			fc.utobj().clickElement(driver, multiUnitInfoPage.lnkTraining);
			multiUnit.modifyTraining(driver, config);

			// Modify Users
			fc.utobj().clickElement(driver, multiUnitInfoPage.lnkUsers);
			fc.utobj().actionImgOption(driver, multiUnitOwnerName, "Modify");
			multiUnit.modifyOwnerUser(driver, config);

			// ************************************************* Delete Section
			// ************************************************
			fc.utobj().printTestStep("Deleting data from all the tabs !!!");

			multiUnit.deleteTabData(driver, config, multiUnitInfoPage.lnkAgreement, null);

			multiUnit.deleteTabData(driver, config, multiUnitInfoPage.lnkContractSigning, null);

			multiUnit.deleteTabData(driver, config, multiUnitInfoPage.lnkCustomerComplaints, null);

			multiUnit.deleteTabData(driver, config, multiUnitInfoPage.lnkDocuments, docTitle);

			multiUnit.deleteTabData(driver, config, multiUnitInfoPage.lnkEmployees, empName);

			multiUnit.deleteTabData(driver, config, multiUnitInfoPage.lnkEvents, null);

			multiUnit.deleteTabData(driver, config, multiUnitInfoPage.lnkFinancial, null);

			multiUnit.deleteTabData(driver, config, multiUnitInfoPage.guarantorsTab, null);

			multiUnit.deleteTabData(driver, config, multiUnitInfoPage.insuranceTab, null);

			multiUnit.deleteTabData(driver, config, multiUnitInfoPage.lnkLegalViolation, null);

			multiUnit.deleteTabData(driver, config, multiUnitInfoPage.lnkLenders, null);

			multiUnit.deleteTabData(driver, config, multiUnitInfoPage.lnkMarketingFinTab, null); // Change
																									// by
																									// Harish
																									// Dwivedi

			multiUnit.deleteTabData(driver, config, multiUnitInfoPage.lnkMysteryReview, null);

			multiUnit.deleteTabData(driver, config, multiUnitInfoPage.lnkPictures, pictureTitle);

			multiUnit.deleteTabData(driver, config, multiUnitInfoPage.lnkRealEstate, null);

			multiUnit.deleteTabData(driver, config, multiUnitInfoPage.lnkTerritory, null);

			multiUnit.deleteTabData(driver, config, multiUnitInfoPage.lnkTraining, null);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr", "IMFailedTC" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-07", testCaseId = "TC_Verify_InfoMgr_MultiUnit_Actions_SendMail", testCaseDescription = "Test to verify send mail functionality from action buttons.", reference = {
			"" })
	public void InfoMgr_MultiUnit_ActionsMenu_Actions_SendMail() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		MultiUnitEntityCommonMethods multiUnitEntity = fc.infomgr().multiUnitEntity();

		try {
			fc.loginpage().login(driver);
			// adminInfoMgr.configureAllFieldsOnManageFormGeneratorPage(driver ,
			// config);

			InfoMgrMultiUnitEntityPage multiUnitEntityPage = new InfoMgrMultiUnitEntityPage(driver);
			InfoMgrContactHistoryPage contactHistoryPage = multiUnitEntityPage.getContactHistoryPage();

			fc.utobj().printTestStep("Create Multi Unit");
			String multiUnitOwnerName = multiUnitEntity.createMultiUnit(driver, config);

			fc.utobj().printTestStep("Send mail from action button displayed at the bottom of the page");
			multiUnitEntity.multiUnitEntityPageShowAll(driver);
			multiUnitEntity.checkLocation(driver, multiUnitOwnerName);
			fc.utobj().clickElement(driver, multiUnitEntityPage.btnSendMail);
			String mailSubject2 = multiUnitEntity.sendTextEmail(driver, config, false);
			multiUnitEntity.searchMultiUnitAndClick(driver, multiUnitOwnerName);
			fc.utobj().clickElement(driver, contactHistoryPage.lnkContactHistory);
			boolean assertMailSubjectonPage2 = fc.utobj().assertPageSource(driver, mailSubject2);
			if (assertMailSubjectonPage2) {
				Reporter.log("Text mail has been sent successfully");
			} else {
				fc.utobj().throwsException("Text mail has not  been sent .");
			}
			/*
			 * fc.utobj().printTestStep(testCaseId, "Click on Send Email button"
			 * ); multiUnitEntity.multiUnitEntityPageShowAll(driver);
			 * multiUnitEntity.checkLocation(driver, multiUnitOwnerName);
			 * fc.utobj().selectActionMenuItemsWithTagInput(driver, "Send Email"
			 * ); String mailSubject3 = multiUnitEntity.sendTextEmail(driver,
			 * config , false); multiUnitEntity.searchMultiUnitAndClick(driver,
			 * multiUnitOwnerName); fc.utobj().clickElement(driver,
			 * contactHistoryPage.lnkContactHistory); boolean
			 * assertMailSubjectonPage3 = fc.utobj().assertPageSource(driver,
			 * mailSubject3); if(assertMailSubjectonPage3) { Reporter.log(
			 * "Text mail has been sent successfully"); } else {
			 * fc.utobj().throwsException("Text mail has not  been sent ."); }
			 */

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-07", testCaseId = "TC_Verify_InfoMgr_MultiUnit_Actions_LogaTask", testCaseDescription = "Test to verify Log a task functionality from action buttons ", reference = {
			"" })
	public void InfoMgr_MultiUnit_Actions_LogaTask() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		MultiUnitEntityCommonMethods multiUnitEntity = fc.infomgr().multiUnitEntity();
		AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();

		InfoMgrMultiUnitEntityPage multiUnitEntityPage = new InfoMgrMultiUnitEntityPage(driver);
		InfoMgrContactHistoryPage contactHistoryPage = multiUnitEntityPage.getContactHistoryPage();

		try {
			fc.loginpage().login(driver);
			// adminInfoMgr.configureAllFieldsOnManageFormGeneratorPage(driver ,
			// config);

			fc.utobj().printTestStep("Add corporate user");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail("infomgrautomation@staffex.com");
			corpUser = corpTest.createDefaultUser(driver, corpUser);

			fc.utobj().printTestStep("Create Multi Unit");
			String multiUnitOwnerName = multiUnitEntity.createMultiUnit(driver, config);

			fc.utobj().printTestStep("Click on Log a Task button displayed at the bottom of the page");
			multiUnitEntity.multiUnitEntityPageShowAll(driver);
			multiUnitEntity.checkLocation(driver, multiUnitOwnerName);
			fc.utobj().clickElement(driver, multiUnitEntityPage.btnLogATask);
			String taskSubject1 = multiUnitEntity.logaTask(driver, corpUser.getuserFullName(), false);
			multiUnitEntity.searchMultiUnitAndClick(driver, multiUnitOwnerName);
			fc.utobj().clickElement(driver, contactHistoryPage.lnkContactHistory);
			boolean assertTaskSubjectonPage1 = fc.utobj().assertPageSource(driver, taskSubject1);
			if (assertTaskSubjectonPage1) {
				Reporter.log("Log a Task has been done successfully");
			} else {
				fc.utobj().throwsException("Log a Task failed .");
			}

			/*
			 * multiUnitEntity.multiUnitEntityPageShowAll(driver);
			 * fc.utobj().printTestStep(testCaseId,
			 * "Click on Log a Task link displayed under the action button click at the top of the page"
			 * ); multiUnitEntity.multiUnitEntityPageShowAll(driver);
			 * multiUnitEntity.checkLocation(driver, multiUnitOwnerName);
			 * fc.utobj().selectActionMenuItemsWithTagInput(driver, "Log a Task"
			 * ); String taskSubject3 = multiUnitEntity.logaTask(driver, config,
			 * corporateUser , false);
			 * multiUnitEntity.searchMultiUnitAndClick(driver,
			 * multiUnitOwnerName); fc.utobj().clickElement(driver,
			 * contactHistoryPage.lnkContactHistory); boolean
			 * assertTaskSubjectonPage3 = fc.utobj().assertPageSource(driver,
			 * taskSubject3); if(assertTaskSubjectonPage3) { Reporter.log(
			 * "Log a Task has been done successfully"); } else {
			 * fc.utobj().throwsException("Log a Task failed ."); }
			 */

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr", "IMFailedTC" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-07", testCaseId = "TC_Verify_InfoMgr_MultiUnit_Actions_LogaCall", testCaseDescription = "Test to verify log a call functionality from action buttons", reference = {
			"" })
	public void InfoMgr_MultiUnit_Actions_LogaCall() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		MultiUnitEntityCommonMethods multiUnitEntity = fc.infomgr().multiUnitEntity();

		InfoMgrMultiUnitEntityPage multiUnitEntityPage = new InfoMgrMultiUnitEntityPage(driver);
		InfoMgrContactHistoryPage contactHistoryPage = multiUnitEntityPage.getContactHistoryPage();
		try {
			fc.loginpage().login(driver);
			// adminInfoMgr.configureAllFieldsOnManageFormGeneratorPage(driver ,
			// config);

			String multiUnitOwnerName = multiUnitEntity.createMultiUnit(driver, config);

			fc.utobj().printTestStep("Click on Log a Call button displayed at the bottom of the page ");
			multiUnitEntity.multiUnitEntityPageShowAll(driver);
			multiUnitEntity.checkLocation(driver, multiUnitOwnerName);
			fc.utobj().clickElement(driver, multiUnitEntityPage.btnLogACall);
			String logACallSubject1 = multiUnitEntity.logACall(driver, config, false);
			multiUnitEntity.searchMultiUnitAndClick(driver, multiUnitOwnerName);
			fc.utobj().clickElement(driver, contactHistoryPage.lnkContactHistory);
			boolean assertCallTextOnPage1 = fc.utobj().assertPageSource(driver, logACallSubject1);
			if (assertCallTextOnPage1) {
				Reporter.log("Log a Call has been done successfully");
			} else {
				fc.utobj().throwsException("Log a call failed .");
			}

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Click on Log a Call link displayed under the top action button"
			 * ); multiUnitEntity.multiUnitEntityPageShowAll(driver);
			 * multiUnitEntity.checkLocation(driver, multiUnitOwnerName);
			 * fc.utobj().selectActionMenuItemsWithTagInput(driver, "Log a Call"
			 * ); String logACallSubject3 = multiUnitEntity.logACall(driver,
			 * config , false); multiUnitEntity.searchMultiUnitAndClick(driver,
			 * multiUnitOwnerName); fc.utobj().clickElement(driver,
			 * contactHistoryPage.lnkContactHistory); boolean
			 * assertCallTextOnPage3 = fc.utobj().assertPageSource(driver,
			 * logACallSubject3); if(assertCallTextOnPage3) { Reporter.log(
			 * "Log a Call has been done successfully"); } else {
			 * fc.utobj().throwsException("Log a Task failed ."); }
			 */

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Verify_InfoMgr_MultiUnit_Actions_Print", testCaseDescription = "Test to verify print functionality from action buttons", reference = {
			"" })
	public void InfoMgr_MultiUnit_Actions_Print() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		MultiUnitEntityCommonMethods multiUnitEntity = fc.infomgr().multiUnitEntity();
		InfoMgrMultiUnitEntityPage multiUnitEntityPage = new InfoMgrMultiUnitEntityPage(driver);

		try {
			fc.loginpage().login(driver);
			// adminInfoMgr.configureAllFieldsOnManageFormGeneratorPage(driver ,
			// config);

			String multiUnitOwnerName = multiUnitEntity.createMultiUnit(driver, config);

			fc.utobj().printTestStep("Click on Print button displayed at the bottom of the page");
			multiUnitEntity.multiUnitEntityPageShowAll(driver);
			fc.utobj().clickElement(driver, multiUnitEntityPage.btnPrint);
			multiUnitEntity.VerifyPrintPreview(driver, multiUnitOwnerName);

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Click on Print link displayed under Action button displayed at the top of the page"
			 * ); multiUnitEntity.multiUnitEntityPageShowAll(driver);
			 * fc.utobj().printTestStep(testCaseId, "Show All clicked");
			 * fc.utobj().selectActionMenuItemsWithTagInput(driver, "Print");
			 * multiUnitEntity.VerifyPrintPreview(driver, multiUnitOwnerName);
			 */

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr","Summary_Filter" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Verify_InfoMgr_MultiUnit_Filter", testCaseDescription = "Test to verify filter displayed on the multiUnit Page", reference = {
			"" })
	public void InfoMgr_MultiUnit_Filter_Verify() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		MultiUnitEntityCommonMethods multiUnit = fc.infomgr().multiUnitEntity();
		try {
			fc.loginpage().login(driver);

			fc.utobj().printTestStep("Create Multi Unit");
			String multiUnitOwnerName = multiUnit.createMultiUnit(driver, config);

			fc.utobj().printTestStep("Modify Multi Unit Info");
			multiUnit.searchMultiUnitAndClick(driver, multiUnitOwnerName);
			fc.utobj().clickLink(driver, "Modify");
			Map<String, String> dsCenterInfo = multiUnit.modifyMultiUnitInfo(driver, config);
			dsCenterInfo.put("multiUnitOwnerName", multiUnitOwnerName);

			fc.utobj().printTestStep("Verify Search Filter");
			fc.infomgr().infomgr_common().InfoMgrMultiUnitEntity(driver);

			multiUnit.search(driver, config, dsCenterInfo);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr_test1" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Verify_InfoMgr_MultiUnit_Agreement_CustomFieldsandSection", testCaseDescription = "Test to verify custom fields added to center info by manage form generator.", reference = {
			"" })
	public void InfoMgr_MultiUnit_Agreement_CustomFields() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		MultiUnitEntityCommonMethods multiUnit = fc.infomgr().multiUnitEntity();
		AdminInfoMgrCommonMethods adminInfoMgr = new AdminInfoMgrCommonMethods();
		InfoMgrAgreementPage agreementPage = new InfoMgrAgreementPage(driver);

		try {
			fc.loginpage().login(driver);

			fc.utobj().printTestStep("Add custom fields to Agreement");
			Map<String, String> dsCenterInfoCustomFields = adminInfoMgr
					.ManageFormGenerator_Add_Section_Fields_To_Forms(driver, config, "Multi-Unit", "Agreement", "mul");

			fc.utobj().printTestStep("Create a multi unit");
			String multiUnitOwnerName = multiUnit.createMultiUnit(driver, config);
			multiUnit.searchMultiUnitAndClick(driver, multiUnitOwnerName);
			fc.utobj()
					.printTestStep("Verify that added custom fields are displayed on the multi unit -> agreement page");
			fc.utobj().clickElement(driver, agreementPage.lnkAgreement);

			for (String key : dsCenterInfoCustomFields.keySet()) {
				if (!key.contains("drpOption")) {
					if (fc.utobj().assertPageSource(driver, dsCenterInfoCustomFields.get(key))) {
						Reporter.log(
								"Custom field " + dsCenterInfoCustomFields.get(key) + " is displayed on the page !!");
					} else {
						fc.utobj().throwsException(
								"Custom field " + dsCenterInfoCustomFields.get(key) + " is not displayed on the page");
					}
				}
			}

			fc.utobj().printTestStep("Deleting the created section and its fields");
			adminInfoMgr.deleteSectionandFields(driver, config, "Multi-Unit", "Agreement", dsCenterInfoCustomFields);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

}
