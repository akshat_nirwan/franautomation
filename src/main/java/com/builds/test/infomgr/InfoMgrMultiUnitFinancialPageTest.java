package com.builds.test.infomgr;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.builds.uimaps.infomgr.InfoMgrFinancialsPage;
import com.builds.uimaps.infomgr.InfoMgrMultiUnitEntityPage;
import com.builds.uimaps.infomgr.InfoMgrMultiUnitInfoPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class InfoMgrMultiUnitFinancialPageTest {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "infomgr"})

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-12-04", testCaseId = "TC_InfoMgr_MultiUnit_Financials_Page_Verify", testCaseDescription = "This test case will verify Multi Unit financials page.", reference = {
			"74022" })
	// Bug ID = 74022
	// Script Updated by Govind
	public void VerifyMultiUnitFinancialTab() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		FranchiseesCommonMethods franchise = fc.infomgr().franchisees();
		MultiUnitEntityCommonMethods multiUnit = fc.infomgr().multiUnitEntity();
		AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();

		try {
			driver = fc.loginpage().login(driver);
			
			//Add Franchise Location 1
			fc.utobj().printTestStep("Add a franchise location");
			String franchiseID1 = adminInfoMgr.addFranchiseLocationForInfoMgrName(driver, false);

			// search franchisee
			fc.utobj().printTestStep("Search franchisee and click");
			franchise.searchFranchiseAndClick(driver, franchiseID1);

			// Add Owner
			fc.utobj().printTestStep("Add owner");
			InfoMgrMultiUnitInfoPage entityInfoPage = new InfoMgrMultiUnitEntityPage(driver).getMultiUnitInfoPage();
			fc.utobj().clickElement(driver, entityInfoPage.tabOwners);
			Map<String, String> lstOwnerDetails = franchise.addOwner(driver);

			//Add Franchise Location 2
			fc.utobj().printTestStep("Add a franchise location");
			String franchiseID2 = adminInfoMgr.addFranchiseLocationForInfoMgrName(driver, false);
			
			// Search Franchisee
			fc.utobj().printTestStep("Search franchisee and click");
			franchise.searchFranchiseAndClick(driver, franchiseID2);

			// Associate the owner of first franchisee to second franchisee
			fc.utobj().printTestStep("Add existing owner");
			fc.utobj().clickElement(driver, entityInfoPage.tabOwners);
			String multiUnitID = franchise.addExistingOwner(driver, lstOwnerDetails.get("FirstName"));

			// Navigate to Info Mgr -> Multi - Unit \ Entity and verify that
			// multi unit is displayed on the page
			fc.utobj().printTestStep("Search multi unit and click");
			multiUnit.searchMultiUnitAndClick(driver, lstOwnerDetails.get("FirstName"));

			// Navigate to Financial tab and check for modify link
			fc.utobj().printTestStep("Verify financial page");
			InfoMgrFinancialsPage financialPage = new InfoMgrMultiUnitEntityPage(driver).getFinancialsPage();
			fc.utobj().clickElement(driver, financialPage.financialTab);

			boolean isData = fc.utobj().assertLinkText(driver, "Modify");
			if (isData) {
				fc.utobj().clickElement(driver, financialPage.lnkModify);
			}

			// Enter mandatory data on the financial page
			String firstName = fc.utobj().generateTestData("FirstName01");
			fc.utobj().sendKeys(driver, financialPage.txtFirstName, firstName);
			fc.utobj().clickElement(driver, financialPage.btnAdd);

			String multiUnitTableData = fc.utobj().getText(driver, financialPage.tblFinancialData);
			
			String temp = multiUnitTableData.toLowerCase();

			if (temp.contains("billing contact details")
					&& temp.contains("royalty minimums") && temp.contains("additional fees")
					&& temp.contains("royalties") && temp.contains("advertising")
					&& temp.contains("other charges")) {
				Reporter.log("All the sections of the financial page is displayed on the page. Test case passes");
			} else {
				fc.utobj().throwsException("All financial tab data is not displayed on the page. Test case fails !!!");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
}
