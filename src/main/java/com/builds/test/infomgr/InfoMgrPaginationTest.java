package com.builds.test.infomgr;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang3.time.DateUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Reporter;
import org.testng.annotations.Test;
import org.xml.sax.SAXException;

import com.builds.uimaps.infomgr.InfoMgrFranchiseesPage;
import com.builds.uimaps.infomgr.InfoMgrPagination;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class InfoMgrPaginationTest {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_InfoMgr_Paging_Report_Filter_Franchisees", testCaseDescription = "This test case will verify Franchisees paging for Info Mgr  summary", reference = {
			"" })
	public void VerifyInfoMgrFranchiseesSummaryPagingFranchisees() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("verify Paging for Franchisees Summary ");
			showPagingInfoMgr(config, driver, "Franchisees", 20);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_InfoMgr_Paging_Report_Filter_InDevelopment", testCaseDescription = "This test case will verify InDevelopment paging for Info Mgr  summary", reference = {
			"" })
	public void VerifyInfoMgrFranchiseesSummaryPagingInDevelopment() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("verify Paging for InDevelopment Summary ");
			showPagingInfoMgr(config, driver, "InDevelopment", 20);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_InfoMgr_Paging_Report_Filter_TerminatedFranchisees", testCaseDescription = "This test case will verify TerminatedFranchisees paging for Info Mgr  summary", reference = {
			"" })
	public void VerifyInfoMgrFranchiseesSummaryPagingTerminatedFranchisees() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("verify Paging for TerminatedFranchisees Summary ");
			showPagingInfoMgr(config, driver, "TerminatedFranchisees", 20);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_InfoMgr_Paging_Report_Filter_CorporateLocations", testCaseDescription = "This test case will verify CorporateLocations Franchisees paging for Info Mgr  summary", reference = {
			"" })
	public void VerifyInfoMgrFranchiseesSummaryPagingCorporateLocations() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("verify Paging for CorporateLocations Summary ");
			showPagingInfoMgr(config, driver, "CorporateLocations", 20);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_InfoMgr_Paging_Report_Filter_Regional", testCaseDescription = "This test case will verify Regional paging for Info Mgr  summary", reference = {
			"" })
	public void VerifyInfoMgrFranchiseesSummaryPagingRegional() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("verify Paging for Regional Summary ");
			showPagingInfoMgr(config, driver, "Regional", 20);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_InfoMgr_Paging_Report_Filter_MultiUnit", testCaseDescription = "This test case will verify  MultiUnit paging for Info Mgr  summary", reference = {
			"" })
	public void VerifyInfoMgrFranchiseesSummaryPagingMultiUnit() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("verify Paging for MultiUnit Summary ");
			showPagingInfoMgr(config, driver, "MultiUnit", 20);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_InfoMgr_Paging_Report_Filter_Tasks", testCaseDescription = "This test case will verify Tasks paging for Info Mgr  summary", reference = {
			"" })
	public void VerifyInfoMgrFranchiseesSummaryPagingTasks() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("verify Paging for Tasks Summary ");
			showPagingInfoMgr(config, driver, "Tasks", 20);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr","NewUI" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_InfoMgr_Paging_Report_Filter_Groups", testCaseDescription = "This test case will verify Groups paging for Info Mgr  summary", reference = {
			"" })
	public void VerifyInfoMgrFranchiseesSummaryPagingGroups() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("verify Paging for Groups Summary ");
			showPagingInfoMgrModuleNew(config, driver, "Groups", 20);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_InfoMgr_Paging_Report_Filter_Templates", testCaseDescription = "This test case will verify Templates paging for Info Mgr  summary", reference = {
			"" })
	public void VerifyInfoMgrFranchiseesSummaryPagingTemplates() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("verify Paging for Templates Summary ");
			showPagingInfoMgrModuleNew(config, driver, "Templates", 20);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_InfoMgr_Paging_Report_Filter_Campaigns", testCaseDescription = "This test case will verify Campaigns paging for Info Mgr  summary", reference = {
			"" })
	public void VerifyInfoMgrFranchiseesSummaryPagingCampaigns() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("verify Paging for Campaigns Summary ");
			showPagingInfoMgrModuleNew(config, driver, "Campaigns", 10);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_InfoMgr_Paging_Report_Filter_MailMerge", testCaseDescription = "This test case will verify MailMerge paging for Info Mgr  summary", reference = {
			"" })
	public void VerifyInfoMgrFranchiseesSummaryPagingMailMerge() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("verify Paging for Mail Merge Summary ");
			showPagingInfoMgrModuleNew(config, driver, "MailMerge", 20);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_InfoMgr_Paging_Report_Filter_FDD", testCaseDescription = "This test case will verify FDD paging for Info Mgr  summary", reference = {
			"" })
	public void VerifyInfoMgrFranchiseesSummaryPagingFDD() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("verify Paging for FDD Summary ");
			showPagingInfoMgrModuleNew(config, driver, "FDD", 20);

			// End Franchisee Sumary paging

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public boolean sortedListInfoMgr(WebDriver driver, Map<String, String> config, List<WebElement> records,
			String subModuleName) {

		boolean isSorttable = false;
		try {
			records = pagingRecordsInfoMgr(driver, records, subModuleName);
			ArrayList<String> obtainedList = new ArrayList<>();
			ArrayList<String> sortedList = new ArrayList<>();
			List<WebElement> elementList = records; // driver.findElements(By.xpath(YourLocator));
			for (WebElement we : elementList) {
				obtainedList.add(we.getText());
			}
			if (subModuleName != null) {
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//table[@class='summaryTblex']/tbody/tr[1]/td[2]//a[not(*)]"));
				records = pagingRecordsInfoMgr(driver, records, subModuleName);
				for (WebElement we : records) {
					sortedList.add(we.getText());
				}
			}
			try {
				if (sortedList != null) {
					for (int i = 0; i < sortedList.size(); i++) {
						if (sortedList.get(0) != obtainedList.get(0)) {
							isSorttable = true;
							break;
						}
					}
				}

			} catch (Exception e) {
				isSorttable = false;

			}

		} catch (Exception e) {
			isSorttable = false;
			// e.printStackTrace();
		}
		return isSorttable;
	}

	public void showPagingInfoMgrModuleNew(Map<String, String> config, WebDriver driver, String subModuleName,
			int viewPerPage)
			throws ParserConfigurationException, SAXException, IOException, InterruptedException, Exception

	{
		String testCaseId = "TC_InfoMgr_Paging_Validation";
		InfoMgrPagination InfoMgrPagination = new InfoMgrPagination(driver);

		if (subModuleName == null)
			subModuleName = "Groups";
		List<WebElement> records = null;

		fc.utobj().printTestStep("Search " + subModuleName + " and click");

		if (subModuleName != null && ("Groups".equals(subModuleName) || "Templates".equals(subModuleName)
				|| "Campaigns".equals(subModuleName) || "MailMerge".equals(subModuleName)
				|| "FDD".equals(subModuleName))) {

			if ("Groups".equals(subModuleName))
				fc.infomgr().infomgr_common().InfoMgrGroups(driver);
			else if ("Templates".equals(subModuleName)) {
				fc.infomgr().infomgr_common().InfoMgrCampaignCenter(driver);
				fc.utobj().printTestStep("Navigate to Manage Templates");
				fc.utobj().clickElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@class='pt']//*[text()='Email Templates']"));

			} else if ("Campaigns".equals(subModuleName)) {
				fc.infomgr().infomgr_common().InfoMgrCampaignCenter(driver);
				fc.utobj().printTestStep("Navigate to Manage Campaigns");
				fc.utobj().clickElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@class='pt']//*[text()='Choose']"));

			} else if ("FDD".equals(subModuleName)) {
				fc.infomgr().infomgr_common().InfoMgrFDD(driver);

			} else if ("MailMerge".equals(subModuleName)) {

				fc.infomgr().infomgr_common().fimModule(driver);
				fc.utobj().printTestStep("Navigate to Manage Mail Merge");
				fc.utobj().clickElement(driver, driver.findElement(
						By.xpath(".//a[contains(@href,'subMenuName=fimmailmerge')]")));

			}

			try {
				if ("Templates".equals(subModuleName)) {
					records = driver.findElements(
							By.xpath(".//*[@id='siteMainTable']/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[4]"));
				} else if ("Campaigns".equals(subModuleName)) {
					records = driver.findElements(
							By.xpath(".//*[@id='siteMainTable']/tbody/tr/td/table/tbody/tr/td/table/tbody"));
				} else if ("MailMerge".equals(subModuleName) || "FDD".equals(subModuleName)) {
					records = driver.findElements(By.xpath(".//*[@id='siteMainTable']/tbody/tr/td/table[2]"));
				} else {
					records = driver.findElements(By.xpath(".//*[@id='form1']"));
				}

				// Thread.sleep(1000);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		String header;
		int totalRecordCount = 0;
		int totalrecordPerPage = 0;
		try {
			header = fc.utobj().getElementByXpath(driver, ".//*[@id='pageid']").getText();
			String options[] = header.split(" ");
			if (options != null) {
				totalRecordCount = Integer.valueOf(options[5]);
				totalrecordPerPage = Integer.valueOf(options[3]);
			}
			fc.utobj().printTestStep(subModuleName + " ToTal Record " + totalRecordCount);

		} catch (Exception E) {
			fc.utobj().printTestStep("Pagination not found in " + subModuleName);
			// System.out.println("Pagination not found");
			totalRecordCount = 0;
		}

		if (totalRecordCount > 0 && totalRecordCount > viewPerPage) {
			fc.utobj().printTestStep("Pagination exists in " + subModuleName);

			int clickCount = totalRecordCount / viewPerPage;
			String optionsShoud[] = new String[clickCount + 1];
			;
			int inLastRerord = totalRecordCount - (viewPerPage * clickCount);
			for (int cnt = 0; cnt <= clickCount; cnt++) {

				optionsShoud[cnt] = viewPerPage + "";
				if (cnt == clickCount) {
					optionsShoud[cnt] = inLastRerord + "";
				}
			}

			int foundRecordCount = 0;
			int counter = 1;
			try {
				if (optionsShoud != null && optionsShoud.length > 0) {
					for (int i = 0; i < (optionsShoud.length); i++) {
						// System.out.println("===Val=="+lstOptions.get(i)
						// +"==New=="+lstOptions.get(i).getText()
						// +"===clickCount=="+clickCount+"optionsShoud=="+optionsShoud[i]);
						if (i != 0) {

							if (i == 1 && i != 0) {

								if (subModuleName != null && ("Workflows".equals(subModuleName))) {
									fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
											".//*[@class='count']/a[" + (i) + "]"));
									counter = i;
								} else {
									fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
											".//*[@id='pageid']/a[" + (i) + "]/u"));
									counter = i;
								}
							} else if (i > 1 && i != 0) {
								if (subModuleName != null && ("Workflows".equals(subModuleName))) {
									fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
											".//*[@class='count']/a[" + (i + 1) + "]"));
									counter = i + 1;
								} else {
									fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
											".//*[@id='pageid']/a[" + (i + 2) + "]/u"));
									counter = i + 2;
								}
							}
							foundRecordCount = pagingRecordsInfoNewMgrAll(driver, records, subModuleName).size();
							if (foundRecordCount > 0) {

								if (foundRecordCount == Integer.parseInt(optionsShoud[i])) {
									// System.out.println("found record
									// "+foundRecordCount+"= Test Case Pass for
									// the page "+(i));
									fc.utobj().printTestStep("" + foundRecordCount + " records for page = " + counter);
								} else {

									if (foundRecordCount <= Integer.parseInt(optionsShoud[i])) {
										// System.out.println("found record
										// "+foundRecordCount+"= Test Case Pass
										// for the Last page "+(i));
										fc.utobj().printTestStep(
												"" + foundRecordCount + " records for page = " + counter);
									} else {
										// System.out.println("found record
										// "+foundRecordCount+"= Test Case Fail
										// for the page "+(i));
										// fc.utobj().printTestStep(testCaseId,
										// "found record "+foundRecordCount+"=
										// Test Case Fail for the page "+(i));
										// fc.utobj().throwsException( "found
										// record "+foundRecordCount+"= Test
										// Case Fail for the page "+(i));
										fc.utobj().throwsException("found record " + foundRecordCount
												+ "= Test Case Fail for the page almost last Page " + counter);
									}
								}
							} else {
								fc.utobj().throwsException(
										"" + foundRecordCount + " records for page " + counter + " Test case fails");
							}
							// Check for paging after sort function
							if (subModuleName != null && !"Workflows".equals(subModuleName)) {
								fc.utobj().clickElement(driver, driver
										.findElement(By.xpath(".//table[@class='summaryTblex']/tbody/tr[1]/td[2]/a")));
								foundRecordCount = pagingRecordsInfoNewMgrAll(driver, records, subModuleName).size();
								if (foundRecordCount > 0) {

									if (foundRecordCount == Integer.parseInt(optionsShoud[i])) {
										// System.out.println("found record
										// "+foundRecordCount+"= Test Case Pass
										// for the page "+(i));
										fc.utobj().printTestStep(
												"After Sort record " + foundRecordCount + " for the page " + counter);
									} else {

										if (foundRecordCount <= Integer.parseInt(optionsShoud[i])) {
											// System.out.println("found record
											// "+foundRecordCount+"= Test Case
											// Pass for the Last page "+(i));
											fc.utobj().printTestStep("After Sort record " + foundRecordCount
													+ " for the page " + counter);
										} else {
											// System.out.println("found record
											// "+foundRecordCount+"= Test Case
											// Fail for the page "+(i));
											// fc.utobj().printTestStep(testCaseId,
											// "After Sort record
											// "+foundRecordCount+"= Test Case
											// Fail for the page "+(i));
											// fc.utobj().throwsException("After
											// Sort record "+foundRecordCount+"=
											// Test Case Fail for the page
											// "+(i));
											fc.utobj().throwsException("found record " + foundRecordCount
													+ "= Test Case Fail for the page Almost Last Page " + counter);
										}

									}
								} else {
									fc.utobj().throwsException("" + foundRecordCount + " records for page " + counter
											+ " Test case fails on click sort");
								}
								//
							}
						}
					}
				}

			} catch (Exception e) {
				e.printStackTrace();
			}

			try {
				// fc.utobj().printTestStep(testCaseId, "Reset our page to the
				// page 1 to check the results Per Page verification.");

				if (subModuleName != null && ("Workflows".equals(subModuleName))) {
					fc.utobj().clickElement(driver,
							fc.utobj().getElementByXpath(driver, ".//*[@class='print-active-name']/a[1]"));
					fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
							".//*[@id='siteMainTable']//*[@class='full-width']/div/div[3]//*[@class='dropdown-list direction']/li[3]/a[1]"));

				} else {
					fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='pageid']/a[1]/u"));
					fc.utobj().selectDropDown(driver, fc.utobj().getElementByID(driver, "resultsPerPage"), "100");
				}

				foundRecordCount = pagingRecordsInfoNewMgrAll(driver, records, subModuleName).size();
				if (foundRecordCount > 0) {

				}
				if (foundRecordCount <= 100) {
					// System.out.println("found record in Result Per page all
					// "+foundRecordCount+" test case passed at View Per Page");
					fc.utobj().printTestStep("View Per Page " + foundRecordCount + " records.");

				} else {
					if (foundRecordCount > 100) {
						fc.utobj().throwsException("View Per Page " + foundRecordCount + " records.");
					}
				}
				// Check for paging after click on Sort function

				if (subModuleName != null && ("Workflows".equals(subModuleName))) {
					fc.utobj().clickElement(driver, driver.findElement(
							By.xpath(".//table[@class='tbl-style tbl-hover tbl-sticky-header']/thead/tr/th[2]/a")));
				} else {
					fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
							".//table[@class='summaryTblex']/tbody/tr[1]/td[2]/a"));
				}
				foundRecordCount = pagingRecordsInfoNewMgrAll(driver, records, subModuleName).size();
				if (foundRecordCount > 0) {

				}
				if (foundRecordCount <= 100) {
					// System.out.println("found record in Result Per page all
					// "+foundRecordCount+" test case passed at View Per Page");
					fc.utobj().printTestStep("After Sort found record in Result Per page  " + foundRecordCount
							+ " test case passed at View Per Page");

				} else {
					if (foundRecordCount > 100) {
						fc.utobj().throwsException("View Per Page " + foundRecordCount + " records.");
					}
				}

			} catch (Exception E) {
				// System.out.println("resultsPerPage Field Not Found!");
				fc.utobj().printTestStep("View Per Page  Field Not Found! in " + subModuleName);

				// totalRecordCount = 0;
			}

			if (subModuleName != null && "Groups".equals(subModuleName) || "Templates".equals(subModuleName)
					|| "Campaigns".equals(subModuleName) || "MailMerge".equals(subModuleName)
					|| "FDD".equals(subModuleName)) {
				try {
					fc.utobj().clickLink(driver, "Show All");
					// System.out.println("totalRecordCount===in show
					// all"+totalRecordCount);
					fc.utobj()
							.printTestStep("totalRecordCount in show all " + subModuleName + " = " + totalRecordCount);
					foundRecordCount = pagingRecordsInfoNewMgrAll(driver, records, subModuleName).size();
					if (foundRecordCount > 0) {

					}
					if (totalRecordCount == foundRecordCount) {
						// System.out.println("found record in show all
						// "+foundRecordCount+" test case passed");
						fc.utobj().printTestStep("found record in show all " + foundRecordCount + " test case passed");
					}
					// System.out.println("found record "+foundRecordCount+" in
					// "+lstOptions.get(i).getText()+" th Page");

				} catch (Exception E) {
					// System.out.println("Show All Field Not Found!");
					fc.utobj().printTestStep("Show All field not found!");
					// totalRecordCount = 0;
				}
			}
		} else {
			// System.out.println("Pagination Not Exists");
			fc.utobj().printTestStep("Pagination Not Exists in " + subModuleName);
		}
		// sortedList
		try {
			boolean isSorttable = sortedListInfoMgrNew(driver, config, records, subModuleName);
			if (isSorttable == true) {
				fc.utobj().printTestStep("Sorting function working for  " + subModuleName);
			} else {
				fc.utobj().printTestStep("Sorting function not Working for  " + subModuleName + " OR not applicable.");
			}

		} catch (Exception e) {
			// e.printStackTrace();
			fc.utobj().printTestStep("Eception in Sorting function which Working for  " + subModuleName);
		}

		// End sortedList

		fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
	}

	public boolean sortedListInfoMgrNew(WebDriver driver, Map<String, String> config, List<WebElement> records,
			String subModuleName) {

		boolean isSorttable = false;
		try {
			records = pagingRecordsInfoNewMgrAll(driver, records, subModuleName);
			ArrayList<String> obtainedList = new ArrayList<>();
			ArrayList<String> sortedList = new ArrayList<>();
			List<WebElement> elementList = records; // driver.findElements(By.xpath(YourLocator));
			for (WebElement we : elementList) {
				obtainedList.add(we.getText());
			}
			if (subModuleName != null) {
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//table[@class='summaryTblex']/tbody/tr[1]/td[2]//a[not(*)]"));
				records = pagingRecordsInfoMgr(driver, records, subModuleName);
				for (WebElement we : records) {
					sortedList.add(we.getText());
				}
			}
			try {
				if (sortedList != null) {
					for (int i = 0; i < sortedList.size(); i++) {
						// System.out.println(sortedList.get(i));
						if (sortedList.get(0) != obtainedList.get(0)) {
							// System.out.println(sortedList.get(0)+"======="+obtainedList.get(0));
							isSorttable = true;
							break;
						}
					}
				}

			} catch (Exception e) {
				isSorttable = false;

			}

		} catch (Exception e) {
			isSorttable = false;
			// e.printStackTrace();
		}

		return isSorttable;
	}

	public void showPagingInfoMgr(Map<String, String> config, WebDriver driver, String subModuleName, int viewPerPage)
			throws ParserConfigurationException, SAXException, IOException, InterruptedException, Exception

	{
		String testCaseId = "TC_InfoMgr_Paging_Validation";
		InfoMgrPagination InfoMgrPagination = new InfoMgrPagination(driver);
		if (subModuleName == null)
			subModuleName = "Franchisees";

		List<WebElement> records = null;

		fc.utobj().printTestStep("Search Franchisee Summary and click");
		if (subModuleName != null && "Franchisees".equals(subModuleName)) {
			fc.infomgr().infomgr_common().InfoMgrFranchisees(driver);
		} else if (subModuleName != null && "InDevelopment".equals(subModuleName)) {
			fc.infomgr().infomgr_common().InfoMgrInDevelopment(driver);
		} else if (subModuleName != null && "TerminatedFranchisees".equals(subModuleName)) {
			fc.infomgr().infomgr_common().InfoMgrTerminated(driver);
		} else if (subModuleName != null && "CorporateLocations".equals(subModuleName)) {
			fc.infomgr().infomgr_common().InfoMgrCorporateLocations(driver);
		} else if (subModuleName != null && "Regional".equals(subModuleName)) {
			fc.infomgr().infomgr_common().InfoMgrRegional(driver);
		} else if (subModuleName != null && "MultiUnit".equals(subModuleName)) {
			fc.infomgr().infomgr_common().InfoMgrMultiUnitEntity(driver);
		} else if (subModuleName != null && "Tasks".equals(subModuleName)) {
			fc.infomgr().infomgr_common().InfoMgrTasks(driver);
		}
		// FDD / Gropup / Mail Merge / Email Templates

		if (subModuleName != null && ("Franchisees".equals(subModuleName) || "InDevelopment".equals(subModuleName)
				|| "TerminatedFranchisees".equals(subModuleName) || "CorporateLocations".equals(subModuleName)
				|| "Regional".equals(subModuleName) || "MultiUnit".equals(subModuleName)
				|| "Tasks".equals(subModuleName))) {
			try {
				records = driver.findElements(By.xpath(
						".//*[@id='siteMainTable']/tbody/tr/td/table/tbody/tr/td/form/table/tbody/tr/td/table/tbody/tr"));
				// Thread.sleep(1000);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		String header;
		int totalRecordCount = 0;
		int totalrecordPerPage = 0;
		try {
			header = fc.utobj().getElementByXpath(driver, ".//*[@id='pageid']").getText();
			String options[] = header.split(" ");
			if (options != null) {
				totalRecordCount = Integer.valueOf(options[5]);
				totalrecordPerPage = Integer.valueOf(options[3]);
			}
			fc.utobj().printTestStep("ToTal records " + subModuleName + " summary " + totalRecordCount);
		} catch (Exception E) {
			// System.out.println("Pagination not found");
			fc.utobj().printTestStep(
					"Pagination not found " + subModuleName + " summary total record " + totalRecordCount);
			totalRecordCount = 0;
		}

		if (totalRecordCount > 0 && totalRecordCount > viewPerPage) {

			fc.utobj().printTestStep("Pagination exists and total record " + totalRecordCount);

			int clickCount = totalRecordCount / viewPerPage;

			int inLastRerord = totalRecordCount - (viewPerPage * clickCount);

			List<WebElement> lstOptions = new Select(InfoMgrPagination.pageListGoto).getOptions();
			String optionsShoud[] = null;
			if (lstOptions != null && lstOptions.size() > 0) {
				optionsShoud = new String[lstOptions.size()];
				for (int i = 0; i < lstOptions.size(); i++) {
					// System.out.println("lstOptions=="+lstOptions.get(i)+"=="+lstOptions.get(i).getText());
					optionsShoud[i] = viewPerPage + "";
					if (i == (lstOptions.size() - 1)) {
						optionsShoud[i] = inLastRerord + "";
					}
				}
			}

			int foundRecordCount = 0;
			int counter = 1;
			try {

				lstOptions = new Select(InfoMgrPagination.pageListGoto).getOptions();

				if (lstOptions != null && lstOptions.size() > 0) {
					for (int i = 0; i < lstOptions.size(); i++) {
						lstOptions = new Select(InfoMgrPagination.pageListGoto).getOptions();

						// System.out.println("===Val=="+lstOptions.get(i)
						// +"==New=="+lstOptions.get(i).getText()
						// +"===clickCount=="+clickCount+"optionsShoud=="+optionsShoud[i]);
						if (true) {
							// driver.manage().timeouts().implicitlyWait(1000,
							// TimeUnit.MILLISECONDS);
							fc.utobj().clickElement(driver, lstOptions.get(i));
							counter = i + 1;
							foundRecordCount = pagingRecordsInfoMgr(driver, records, subModuleName).size();
							if (foundRecordCount > 0) {

								if (foundRecordCount == Integer.parseInt(optionsShoud[i])) {
									// System.out.println("found record
									// "+foundRecordCount+"= Test Case Pass for
									// the page "+(i));
									fc.utobj().printTestStep("" + foundRecordCount + " records for page = " + counter);

								} else {

									if (foundRecordCount <= Integer.parseInt(optionsShoud[i])) {
										// System.out.println("found record
										// "+foundRecordCount+"= Test Case Pass
										// for the Last page "+(i));
										fc.utobj().printTestStep(
												"" + foundRecordCount + " records for page = " + counter);
									} else {
										// System.out.println("found record
										// "+foundRecordCount+"= Test Case Fail
										// for the page "+(i));
										// fc.utobj().printTestStep(testCaseId,
										// "found record "+foundRecordCount+"=
										// Test Case Fail for the page "+(i));
										fc.utobj().throwsException("found record " + foundRecordCount
												+ "= Test Case Fail for the page Almost Last Page " + counter);
									}

								}
							} else {
								fc.utobj().throwsException(
										"" + foundRecordCount + " records for page " + counter + " Test case fails");
							}
							// Check for paging after sort function
							fc.utobj().clickElement(driver, driver
									.findElement(By.xpath(".//table[@class='summaryTblex']/tbody/tr[1]/td[2]/a")));
							foundRecordCount = pagingRecordsInfoMgr(driver, records, subModuleName).size();
							if (foundRecordCount > 0) {

								if (foundRecordCount == Integer.parseInt(optionsShoud[i])) {
									// System.out.println("found record
									// "+foundRecordCount+"= Test Case Pass for
									// the page "+(i));
									// fc.utobj().printTestStep(testCaseId,
									// "After Sort record "+foundRecordCount+"
									// for the page "+counter);
									fc.utobj().printTestStep(
											"After Sort record " + foundRecordCount + " for the page " + counter);
								} else {

									if (foundRecordCount <= Integer.parseInt(optionsShoud[i])) {
										// System.out.println("found record
										// "+foundRecordCount+"= Test Case Pass
										// for the Last page "+(i));
										// fc.utobj().printTestStep(testCaseId,
										// "After Sort record
										// "+foundRecordCount+" for the page
										// "+counter);
										fc.utobj().printTestStep(
												"After Sort record " + foundRecordCount + " for the page " + counter);
									} else {
										// System.out.println("found record
										// "+foundRecordCount+"= Test Case Fail
										// for the page "+(i));
										// fc.utobj().printTestStep(testCaseId,
										// "After Sort record
										// "+foundRecordCount+"= Test Case Fail
										// for the page "+(i));
										fc.utobj().throwsException("After Sort record " + foundRecordCount
												+ "= Test Case Fail for the page Almost Last Page " + counter);
									}

								}
							} else {
								fc.utobj().throwsException("" + foundRecordCount + " records for page " + counter
										+ " Test case fails on click sort");
							}

							//
						}

					}
				}

			} catch (Exception e) {
				e.printStackTrace();
			}

			try {
				fc.utobj().clickLink(driver, "Show All");
				// System.out.println("totalRecordCount===in show
				// all"+totalRecordCount);
				fc.utobj().printTestStep("Total in show all " + subModuleName + " = " + totalRecordCount);
				foundRecordCount = pagingRecordsInfoMgr(driver, records, subModuleName).size();
				if (foundRecordCount > 0) {

				}
				if (totalRecordCount == foundRecordCount) {
					// System.out.println("found record in show all
					// "+foundRecordCount+" test case passed");
					fc.utobj().printTestStep("found record in show all " + foundRecordCount + " test case passed");

				} else {
					if (foundRecordCount > 0) {
						if (totalRecordCount != foundRecordCount) {
							fc.utobj()
									.throwsException("Show All " + foundRecordCount + " records not working correct.");
						}
					}
				}
				// System.out.println("found record "+foundRecordCount+" in
				// "+lstOptions.get(i).getText()+" th Page");

				// Check for paging after sort function
				fc.utobj().clickElement(driver,
						fc.utobj().getElementByXpath(driver, ".//table[@class='summaryTblex']/tbody/tr[1]/td[2]/a"));
				foundRecordCount = pagingRecordsInfoMgr(driver, records, subModuleName).size();
				if (foundRecordCount > 0) {
					foundRecordCount = foundRecordCount - 1;
				}
				if (totalRecordCount == foundRecordCount) {
					// System.out.println("found record in show all
					// "+foundRecordCount+" test case passed");
					fc.utobj().printTestStep(
							"After Sort total found record in show all " + foundRecordCount + " test case passed");

				} else {
					if (foundRecordCount > 0) {
						if (totalRecordCount != foundRecordCount) {
							fc.utobj().throwsException(
									"Show All After Sort " + foundRecordCount + " records not working correct.");
						}
					}
				}
			} catch (Exception E) {
				// System.out.println("Show All Field Not Found!");
				fc.utobj().printTestStep("Show All field not found!");
				// totalRecordCount = 0;
			}

		} else {
			// System.out.println("Pagination Not Exists");
			fc.utobj().printTestStep("Pagination not exists");
		}
		// sortedList
		try {
			boolean isSorttable = sortedListInfoMgr(driver, config, records, subModuleName);
			if (isSorttable == true) {
				fc.utobj().printTestStep("Sorting function working for  " + subModuleName);
			} else {
				fc.utobj().printTestStep("Sorting function not Working for  " + subModuleName + " or not applicable.");
			}

		} catch (Exception e) {
			// e.printStackTrace();
			fc.utobj().printTestStep("Eception in Sorting function which Working for  " + subModuleName);
		}

		// End sortedList

		fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

	}

	public List<WebElement> pagingRecordsInfoMgr(WebDriver driver, List<WebElement> records, String subModuleName) {
		int rc = 0;
		List<WebElement> listofElements = driver
				.findElements(By.xpath(".//*[@class='summaryTblex']/tbody/tr/td[@class='botBorder colPadding'][2]"));
		rc = listofElements.size();
		return listofElements;

	}

	public List<WebElement> pagingRecordsInfoNewMgrAll(WebDriver driver, List<WebElement> records,
			String subModuleName) {

		if (subModuleName == null)
			subModuleName = "LeadSummary";
		List<WebElement> listofElements = null;
		int rc = 0;
		if (subModuleName != null && ("Groups".equals(subModuleName))) {
			listofElements = driver.findElements(
					By.xpath(".//table[@class='summaryTblex']/tbody/tr/td[@class='botBorder colPadding'][2]/a"));
		} else if (subModuleName != null && ("Templates".equals(subModuleName))) {
			listofElements = driver.findElements(
					By.xpath(".//table[@class='summaryTblex']/tbody/tr/td[@class='botBorder colPadding'][1]/a"));
		} else if (subModuleName != null && ("Campaigns".equals(subModuleName))) {
			listofElements = driver.findElements(By.xpath(
					".//*[@id='siteMainTable']/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[7]/td/table/tbody/tr/td[2]/div/table/tbody/tr/td[2]/table/tbody/tr/td/table/tbody/tr/td[1]/a"));
		} else if (subModuleName != null && ("MailMerge".equals(subModuleName)) || "FDD".equals(subModuleName)) {
			listofElements = driver.findElements(
					By.xpath(".//table[@class='summaryTblex']/tbody/tr/td[@class='botBorder colPadding'][1]/a"));
		}

		rc = listofElements.size();
		return listofElements;

	}

	public void searchFranchise(WebDriver driver, String franchiseID) throws Exception {
		Reporter.log("********************** Search Franchisee ****************** \n");

		fc.infomgr().infomgr_common().InfoMgrFranchisees(driver);

		// Search Franchisee and click on the franchiseID
		InfoMgrFranchiseesPage franchiseePage = new InfoMgrFranchiseesPage(driver);

		fc.utobj().sendKeys(driver, franchiseePage.txtSearchFranchisee, franchiseID);
		fc.utobj().clickElement(driver, franchiseePage.imgFranchiseesSearchButton);

	}

	public String getFutureDateUSFormat(String country, int afterNoOfDays) {

		String format = "MM/dd/yyyy";
		if ("AUS".equals(country)) {
			format = "dd/MM/yyyy";
		}

		SimpleDateFormat sdfDate = new SimpleDateFormat("" + format);
		Date now = DateUtils.addDays(new Date(), afterNoOfDays);
		String strDate = sdfDate.format(now);
		return strDate;
	}
}
