package com.builds.test.infomgr;

import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class InfoMgrRegionalAreaInfoPageTest {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "infomgr" })

	// Bug ID - 72284
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_InfoMgr_Regional_AreaInfo_ModifyLink_Verify", testCaseDescription = "This test case will verify Area Info modify functionality.", reference = {
			"72284" })
	public void verifyAreaInfoModifyLink() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();
		RegionalCommonMethods regional = fc.infomgr().regional();

		try {
			driver = fc.loginpage().login(driver);

			// Add region
			fc.utobj().printTestStep("Add area region");
			String region = adminInfoMgr.addAreaRegion(driver);

			// Search region and click on the element
			fc.utobj().printTestStep("search and click region");
			regional.searchAndClickAreaRegion(driver, region);

			// Add Area Region Details
			fc.utobj().printTestStep("Add area region details");
			regional.addAreaRegionDetails(driver);

			// Verify that Modify link is displayed on the page
			fc.utobj().printTestStep("Verify Modify link");
			boolean isModifyLink = fc.utobj().assertLinkText(driver, "Modify");
			if (isModifyLink) {
				Reporter.log("Modify link id displayed on the Area Info page. Test case passes !!! \n");
			} else {
				fc.utobj().throwsException("Modify link is not displayed on the page. Test case fails !!! ");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

}
