package com.builds.test.infomgr;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.common.CorporateUser;
import com.builds.uimaps.infomgr.InfoMgrAreaInfoPage;
import com.builds.uimaps.infomgr.InfoMgrContactHistoryPage;
import com.builds.uimaps.infomgr.InfoMgrOwnersPage;
import com.builds.uimaps.infomgr.InfoMgrRegionalPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class InfoMgrRegionalPageTest {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "infomgr1","IM_ALL_Tabs" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseId = "TC_Verify_InfoMgr_Regional_AllTabs_Add_Modify_Delete", testCaseDescription = "Test to add and modify data to all tabs of Regional location", reference = {
			"" })
	public void InfoMgr_Regional_AllTab_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		RegionalCommonMethods regional = fc.infomgr().regional();
		AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();
		InfoMgrRegionalPage regionalPage = new InfoMgrRegionalPage(driver);

		try {
			fc.loginpage().login(driver);
			// adminInfoMgr.configureInfoMgr(driver, config);

			InfoMgrAreaInfoPage areaInfoPge = new InfoMgrRegionalPage(driver).getAreaInfoPage();
			fc.utobj().printTestStep("Add corporate user");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail("infomgrautomation@staffex.com");
			corpUser = corpTest.createDefaultUser(driver, corpUser);

			fc.utobj().printTestStep("Add Area / Region ");
			String areaRegionID = adminInfoMgr.addAreaRegion(driver);
			Reporter.log("Area Region - " + areaRegionID + "created");

			fc.utobj().printTestStep("Search and click ");
			regional.searchAndClickAreaRegion(driver, areaRegionID);

			InfoMgrContactHistoryPage contactHistoryPage = regionalPage.getContactHistoryPage();

			// Add Agreement
			fc.utobj().clickElement(driver, areaInfoPge.lnkAgreement);
			regional.addAgreement(driver, config);

			fc.utobj().clickElement(driver, areaInfoPge.lnkAreaInfo);
			regional.addAreaRegionDetails(driver);
			fc.utobj().printTestStep("Data in Agreement Tab added successfully");

			// ********** Add Contact History data ****************

			// Log a Task
			fc.utobj().clickElement(driver, contactHistoryPage.lnkContactHistory);

			fc.utobj().clickElement(driver, contactHistoryPage.btnLogaTask);
			String taskSubject = regional.logaTask(driver, config, corpUser.getuserFullName(), true);
			fc.utobj().printTestStep("Task added successfully");
			
			// Log a Call
			fc.utobj().clickElement(driver, contactHistoryPage.btnLogaCall);
			String callSubject = regional.logACall(driver, config, true);
			fc.utobj().printTestStep("Call added successfully");

			// Add Remark
			fc.utobj().clickElement(driver, contactHistoryPage.btnAddRemark);
			String remarkSubject = regional.addRemark(driver, config, contactHistoryPage);
			fc.utobj().printTestStep("Remark added successfully");

			// Send Message
			// fc.utobj().clickElement(driver,
			// contactHistoryPage.btnSendMessage);
			// regional.sendTextMessage(driver, config, true , corporateUser);

			// Send Mail

			// fc.utobj().clickElement(driver, contactHistoryPage.btnSendEmail);
			// regional.sendTextEmail(driver, config , true);

			// Sending HTML Message and mail (Pending)

			// Adding Contract Signing
			fc.utobj().clickElement(driver, contactHistoryPage.lnkContractSigning);
			regional.addContractSigning(driver, config);
			fc.utobj().printTestStep("Data in Contract Signing tab added successfully");

			// Add Documents
			fc.utobj().clickElement(driver, contactHistoryPage.lnkDocuments);
			String docTitle = regional.addDocument(driver, config);
			fc.utobj().printTestStep("Document added successfully");

			// Add Entity Details
			fc.utobj().clickElement(driver, contactHistoryPage.linkEntityDetail);
			regional.addEntityDetails(driver, config);
			fc.utobj().printTestStep("Data in Entity tab added successfully");

			// Add Events Details
			fc.utobj().clickElement(driver, contactHistoryPage.lnkEvents);
			regional.addEventDetails(driver, config);
			fc.utobj().printTestStep("Data in Event tab added successfully");

			// Add Finance Details
			fc.utobj().clickElement(driver, contactHistoryPage.lnkFinancial);
			regional.addFinanceDetails(driver, config);
			fc.utobj().printTestStep("Data in Finance tab added successfully");

			// Add Gurantor details
			fc.utobj().clickElement(driver, contactHistoryPage.lnkGuarantors);
			regional.addGuarantor(driver, config);
			fc.utobj().printTestStep("Data in Guarantors tab added successfully");

			// Add Insurance Details
			fc.utobj().clickElement(driver, contactHistoryPage.lnkInsurance);
			regional.addInsuranceDetails(driver, config);
			fc.utobj().printTestStep("Data in Insurance tab added successfully");

			// Add legal violation
			fc.utobj().clickElement(driver, contactHistoryPage.lnkLegalViolation);
			regional.addLegalViolation(driver, config);
			fc.utobj().printTestStep("Data in Legal Violation tab added successfully");

			// Add Lenders
			fc.utobj().clickElement(driver, contactHistoryPage.lnkLenders);
			regional.addLenders(driver, config);
			fc.utobj().printTestStep("Data in Lenders tab added successfully");

			// Add Marketing details
			fc.utobj().clickElement(driver, contactHistoryPage.lnkMarketingFinTab); // Change
																					// By
																					// Harish
																					// Dwivedi
																					// in
																					// case
																					// of
																					// FC
																					// ZC.
			regional.addMarketingDetails(driver, config);
			fc.utobj().printTestStep("Data in Marketing tab added successfully");

			// Add Mystry Review
			fc.utobj().clickElement(driver, contactHistoryPage.lnkMysteryReview);
			regional.addMystryReview(driver, config);
			fc.utobj().printTestStep("Data in Mystery Review tab added successfully");

			// Add Owner
			fc.utobj().clickElement(driver, contactHistoryPage.lnkAreaOwner);
			Map<String, String> owner = regional.addAreaRegionOwner(driver);
			fc.utobj().printTestStep("Data in Owner tab added successfully");

			// Add Picture
			fc.utobj().clickElement(driver, contactHistoryPage.lnkPictures);
			String pictureTitle = regional.addPicture(driver, config);
			fc.utobj().printTestStep("Data in Picture tab added successfully");

			// Add Real Estate
			fc.utobj().clickElement(driver, contactHistoryPage.lnkRealEstate);
			regional.addRealState(driver, config);
			fc.utobj().printTestStep("Data in Real State tab added successfully");

			// Add Territory
			fc.utobj().clickElement(driver, contactHistoryPage.lnkTerritory);
			regional.addTerritory(driver, config);
			fc.utobj().printTestStep("Data in Territory tab added successfully");

			// Add Training
			// fc.utobj().clickElement(driver,areaInfoPge.lnkTraining);
			// regional.addTraining(driver, config);

			// Add Visit
			// regional.addVisit(driver, config, areaRegionID, corporateUser);

			// ****************** Modify Data
			// **************************************
			fc.utobj().printTestStep("Modify data of all the tabs");

			regional.searchAndClickAreaRegion(driver, areaRegionID);

			// Modify Agreement
			fc.utobj().clickElement(driver, areaInfoPge.lnkAgreement);
			regional.modifyAgreement(driver, config);
			fc.utobj().printTestStep("Data in Agreement tab Modified successfully");

			// Modify Area Info
			fc.utobj().clickElement(driver, areaInfoPge.lnkAreaInfo);
			fc.utobj().clickLink(driver, "Modify");
			regional.modifyAreaInfo(driver, config);
			fc.utobj().printTestStep("Data in Area Info tab Modified successfully");

			// Modify Contact History

			// Modify a task
			fc.utobj().clickElement(driver, contactHistoryPage.lnkContactHistory);
			fc.utobj().actionImgOption(driver, taskSubject, "Modify");
			regional.modifyATask(driver, config, true);
			fc.utobj().printTestStep("Task Modified successfully");

			// Modify a call
			fc.utobj().actionImgOption(driver, callSubject, "Modify");
			regional.modifyACall(driver, config, true);
			fc.utobj().printTestStep("Call modified successfully");

			// Modify a remark
			fc.utobj().actionImgOption(driver, remarkSubject, "Modify");
			regional.modifyRemark(driver, config, contactHistoryPage);
			fc.utobj().printTestStep("Remark modified successfully");

			// Modify Contract Signing
			fc.utobj().clickElement(driver, areaInfoPge.lnkContractSigning);
			regional.modifyContractSigning(driver, config);
			fc.utobj().printTestStep("Data in Contract Signing tab Modified successfully");

			// Modify Documents
			fc.utobj().clickElement(driver, areaInfoPge.lnkDocuments);
			fc.utobj().actionImgOption(driver, docTitle, "Modify");
			docTitle = regional.modifyDocument(driver, config);
			fc.utobj().printTestStep("Documents modified successfully");

			// Modify Entity Details
			fc.utobj().clickElement(driver, areaInfoPge.linkEntityDetail);
			regional.modifyEntityDetails(driver, config);
			fc.utobj().printTestStep("Data in Entity tab Modified successfully");

			// Modify Events
			fc.utobj().clickElement(driver, areaInfoPge.lnkEvents);
			regional.modifyEventDetails(driver, config);
			fc.utobj().printTestStep("Data in Event tab Modified successfully");

			// Modify Financials
			fc.utobj().clickElement(driver, areaInfoPge.lnkFinancial);
			regional.modifyFinanceDetails(driver, config);
			fc.utobj().printTestStep("Data in Financial tab Modified successfully");

			// Modify Guarantor
			fc.utobj().clickElement(driver, areaInfoPge.lnkGuarantors);
			regional.modifyGuarantor(driver, config);
			fc.utobj().printTestStep("Data in Guarantors tab Modified successfully");
			
			// Modify Insurance
			fc.utobj().clickElement(driver, areaInfoPge.lnkInsurance);
			regional.modifyInsuranceDetails(driver, config);
			fc.utobj().printTestStep("Data in Insurance tab Modified successfully");

			// Modify Legal Violation
			fc.utobj().clickElement(driver, areaInfoPge.lnkLegalViolation);
			regional.modifyLegalViolation(driver, config);
			fc.utobj().printTestStep("Data in Legal Violation tab Modified successfully");

			// Modify Lenders Details
			fc.utobj().clickElement(driver, areaInfoPge.lnkLenders);
			regional.modifyLenders(driver, config);
			fc.utobj().printTestStep("Data in Lenders tab Modified successfully");

			// Modify Marketing
			fc.utobj().clickElement(driver, areaInfoPge.lnkMarketingFinTab); // Change
																				// By
																				// Harish
																				// Dwivedi
																				// in
																				// case
																				// of
																				// FC
																				// ZC
			regional.modifyMarketingDetails(driver, config);
			fc.utobj().printTestStep("Data in Marketing tab Modified successfully");

			// Modify Mystry Review
			fc.utobj().clickElement(driver, areaInfoPge.lnkMysteryReview);
			regional.modifyMystryReview(driver, config);
			fc.utobj().printTestStep("Data in Mystery Review tab Modified successfully");

			// Modify Owners
			fc.utobj().clickElement(driver, areaInfoPge.lnkAreaOwner);
			try {
				fc.utobj().clickLink(driver, "Modify");
			} catch (Exception ex) {
				fc.utobj().actionImgOption(driver, owner.get("FirstName"), "Modify");
			}
			regional.modifyOwner(driver, config);
			fc.utobj().printTestStep("Data in Owner tab Modified successfully");

			// Modify Pictures
			fc.utobj().clickElement(driver, areaInfoPge.lnkPictures);
			fc.utobj().actionImgOption(driver, pictureTitle, "Modify");
			pictureTitle = regional.modifyPicture(driver, config);
			fc.utobj().printTestStep("Data in Picture tab Modified successfully");

			// Modify Real state
			fc.utobj().clickElement(driver, areaInfoPge.lnkRealEstate);
			regional.modifyRealState(driver, config);
			fc.utobj().printTestStep("Data in Real State tab Modified successfully");

			// Modify Territory
			fc.utobj().clickElement(driver, areaInfoPge.lnkTerritory);
			regional.modifyTerritory(driver, config);
			fc.utobj().printTestStep("Data in Territory tab Modified successfully");

			// Modify Training
			// fc.utobj().clickElement(driver, areaInfoPge.lnkTraining);
			// regional.modifyTraining(driver, config);

			// ************************************************* Delete Section
			// ************************************************

			fc.utobj().printTestStep("Deleting data from all the tabs !!! ");

			regional.deleteTabData(driver, config, areaInfoPge.lnkAgreement, null);
			fc.utobj().printTestStep("Data in Agreement tab deleted successfully");

			regional.deleteTabData(driver, config, areaInfoPge.lnkContractSigning, null);
			fc.utobj().printTestStep("Data in Contract Signing tab deleted successfully");

			regional.deleteTabData(driver, config, areaInfoPge.lnkDocuments, docTitle);
			fc.utobj().printTestStep("Data in document tab deleted successfully");

			regional.deleteTabData(driver, config, areaInfoPge.linkEntityDetail, null);
			fc.utobj().printTestStep("Data in Entity tab deleted successfully");

			regional.deleteTabData(driver, config, areaInfoPge.lnkEvents, null);
			fc.utobj().printTestStep("Data in Event tab deleted successfully");

			regional.deleteTabData(driver, config, areaInfoPge.lnkFinancial, null);
			fc.utobj().printTestStep("Data in Financial tab deleted successfully");

			regional.deleteTabData(driver, config, areaInfoPge.guarantorsTab, null);
			fc.utobj().printTestStep("Data in Guaranter tab deleted successfully");

			regional.deleteTabData(driver, config, areaInfoPge.insuranceTab, null);
			fc.utobj().printTestStep("Data in Insurance tab deleted successfully");

			regional.deleteTabData(driver, config, areaInfoPge.lnkLegalViolation, null);
			fc.utobj().printTestStep("Data in Legal Violation tab deleted successfully");

			regional.deleteTabData(driver, config, areaInfoPge.lnkLenders, null);
			fc.utobj().printTestStep("Data in Lenders tab deleted successfully");

			regional.deleteTabData(driver, config, areaInfoPge.lnkMarketingFinTab, null); // Change
																							// By
																							// Harish
																							// Dwivedi
																							// in
																							// Case
																							// of
																							// FC
																							// and
																							// ZC
			fc.utobj().printTestStep("Data in Marketing tab deleted successfully");

			regional.deleteTabData(driver, config, areaInfoPge.lnkMysteryReview, null);
			fc.utobj().printTestStep("Data in Mystery Review tab deleted successfully");

			regional.deleteTabData(driver, config, areaInfoPge.lnkAreaOwner, owner.get("FirstName"));
			fc.utobj().printTestStep("Data in Area Owner tab deleted successfully");

			regional.deleteTabData(driver, config, areaInfoPge.lnkPictures, pictureTitle);
			fc.utobj().printTestStep("Data in Picture tab deleted successfully");

			regional.deleteTabData(driver, config, areaInfoPge.lnkRealEstate, null);
			fc.utobj().printTestStep("Data in Real State tab deleted successfully");

			regional.deleteTabData(driver, config, areaInfoPge.lnkTerritory, null);
			fc.utobj().printTestStep("Data in Territory tab deleted successfully");

			// regional.deleteTabData(driver, config, areaInfoPge.lnkTraining,
			// null);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseId = "TC_Verify_InfoMgr_Regional_Actions_SendMail", testCaseDescription = "Test to verify send mail functionality from action buttons.", reference = {
			"" })
	public void InfoMgr_Regional_ActionsMenu_Actions_SendMail() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		AdminInfoMgrCommonMethods adminInfoMgr = new AdminInfoMgrCommonMethods();
		RegionalCommonMethods regional = fc.infomgr().regional();
		InfoMgrAreaInfoPage areaInfoPage = new InfoMgrRegionalPage(driver).getAreaInfoPage();

		try {
			fc.loginpage().login(driver);

			InfoMgrRegionalPage regionalPage = new InfoMgrRegionalPage(driver);
			InfoMgrContactHistoryPage contactHistoryPage = regionalPage.getContactHistoryPage();

			fc.utobj().printTestStep("Add Area / Region");
			String areaRegionID = adminInfoMgr.addAreaRegion(driver);

			regional.searchAndClickAreaRegion(driver, areaRegionID);

			fc.utobj().clickElement(driver, areaInfoPage.lnkAreaInfo);
			regional.addAreaRegionDetails(driver);

			fc.utobj().printTestStep("Send mail from action button displayed at the bottom of the page");
			regional.regionalPageShowAll(driver);
			regional.checkLocation(driver, areaRegionID);
			fc.utobj().clickElement(driver, regionalPage.btnSendMail);
			String mailSubject2 = regional.sendTextEmail(driver, config, false);
			regional.searchAndClickAreaRegion(driver, areaRegionID);
			fc.utobj().clickElement(driver, contactHistoryPage.lnkContactHistory);
			boolean assertMailSubjectonPage2 = fc.utobj().assertPageSource(driver, mailSubject2);
			if (assertMailSubjectonPage2) {
				Reporter.log("Text mail has been sent successfully");
			} else {
				fc.utobj().throwsException("Text mail not sent");
			}

			/*
			 * fc.utobj().printTestStep(testCaseId, "Click on Send Email button"
			 * ); regional.regionalPageShowAll(driver);
			 * regional.checkLocation(driver, areaRegionID);
			 * fc.utobj().selectActionMenuItemsWithTagInput(driver, "Send Email"
			 * ); String mailSubject3 = regional.sendTextEmail(driver, config ,
			 * false); regional.searchAndClickAreaRegion(driver, areaRegionID);
			 * fc.utobj().clickElement(driver,
			 * contactHistoryPage.lnkContactHistory); boolean
			 * assertMailSubjectonPage3 = fc.utobj().assertPageSource(driver,
			 * mailSubject3); if(assertMailSubjectonPage3) { Reporter.log(
			 * "Text mail has been sent successfully"); } else {
			 * fc.utobj().throwsException("Text mail not sent"); }
			 */
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseId = "TC_Verify_InfoMgr_Regional_Actions_LogaTask", testCaseDescription = "Test to verify Log a task functionality from action buttons ", reference = {
			"" })
	public void InfoMgr_Regional_PageBottom_Actions_LogaTask() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		RegionalCommonMethods regional = fc.infomgr().regional();
		AdminInfoMgrCommonMethods adminInfoMgr = new AdminInfoMgrCommonMethods();

		InfoMgrRegionalPage regionalPage = new InfoMgrRegionalPage(driver);
		InfoMgrContactHistoryPage contactHistoryPage = regionalPage.getContactHistoryPage();

		try {
			fc.loginpage().login(driver);
			// adminInfoMgr.configureAllFieldsOnManageFormGeneratorPage(driver ,
			// config);

			fc.utobj().printTestStep("Add corporate user");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail("infomgrautomation@staffex.com");
			corpUser = corpTest.createDefaultUser(driver, corpUser);

			fc.utobj().printTestStep("Add Area / Region");
			String areaRegionID = adminInfoMgr.addAreaRegion(driver);

			fc.utobj().printTestStep("Click on Log a Task button displayed at the bottom of the page");
			regional.regionalPageShowAll(driver);
			regional.checkLocation(driver, areaRegionID);
			fc.utobj().clickElement(driver, regionalPage.btnLogATask);
			String taskSubject1 = regional.logaTask(driver, config, corpUser.getuserFullName(), false);
			regional.searchAndClickAreaRegion(driver, areaRegionID);
			fc.utobj().clickElement(driver, contactHistoryPage.lnkContactHistory);
			boolean assertTaskSubjectonPage1 = fc.utobj().assertPageSource(driver, taskSubject1);
			if (assertTaskSubjectonPage1) {
				Reporter.log("Log a Task has been done successfully");
			} else {
				fc.utobj().throwsException("Log a task failed");
			}

			/*
			 * regional.regionalPageShowAll(driver);
			 * fc.utobj().printTestStep(testCaseId,
			 * "Click on Log a Task link displayed under the action button click at the top of the page"
			 * ); regional.regionalPageShowAll(driver);
			 * regional.checkLocation(driver, areaRegionID);
			 * fc.utobj().selectActionMenuItemsWithTagInput(driver, "Log a Task"
			 * ); String taskSubject3 = regional.logaTask(driver, config,
			 * corporateUser , false); regional.searchAndClickAreaRegion(driver,
			 * areaRegionID); fc.utobj().clickElement(driver,
			 * contactHistoryPage.lnkContactHistory); boolean
			 * assertTaskSubjectonPage3 = fc.utobj().assertPageSource(driver,
			 * taskSubject3); if(assertTaskSubjectonPage3) { Reporter.log(
			 * "Log a Task has been done successfully"); } else {
			 * fc.utobj().throwsException("Log a task failed"); }
			 */

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseId = "TC_Verify_InfoMgr_Regional_Actions_LogaCall", testCaseDescription = "Test to verify log a call functionality from action buttons", reference = {
			"" })
	public void InfoMgr_Regional_Actions_LogaCall() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		RegionalCommonMethods regional = fc.infomgr().regional();
		AdminInfoMgrCommonMethods adminInfoMgr = new AdminInfoMgrCommonMethods();

		InfoMgrRegionalPage regionalPage = new InfoMgrRegionalPage(driver);
		InfoMgrContactHistoryPage contactHistoryPage = regionalPage.getContactHistoryPage();
		try {
			fc.loginpage().login(driver);
			// adminInfoMgr.configureAllFieldsOnManageFormGeneratorPage(driver ,
			// config);

			String areaRegionID = adminInfoMgr.addAreaRegion(driver);

			fc.utobj().printTestStep("Click on Log a Call button displayed at the bottom of the page ");
			regional.regionalPageShowAll(driver);
			regional.checkLocation(driver, areaRegionID);
			fc.utobj().clickElement(driver, regionalPage.btnLogACall);
			String logACallSubject1 = regional.logACall(driver, config, false);
			regional.searchAndClickAreaRegion(driver, areaRegionID);
			fc.utobj().clickElement(driver, contactHistoryPage.lnkContactHistory);
			boolean assertCallTextOnPage1 = fc.utobj().assertPageSource(driver, logACallSubject1);
			if (assertCallTextOnPage1) {
				Reporter.log("Log a Call has been done successfully");
			} else {
				fc.utobj().throwsException("Log a call failed");
			}

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Click on Log a Call link displayed under the top action button"
			 * ); regional.regionalPageShowAll(driver);
			 * regional.checkLocation(driver, areaRegionID);
			 * fc.utobj().selectActionMenuItemsWithTagInput(driver, "Log a Call"
			 * ); String logACallSubject3 = regional.logACall(driver, config ,
			 * false); regional.searchAndClickAreaRegion(driver, areaRegionID);
			 * fc.utobj().clickElement(driver,
			 * contactHistoryPage.lnkContactHistory); boolean
			 * assertCallTextOnPage3 = fc.utobj().assertPageSource(driver,
			 * logACallSubject3); if(assertCallTextOnPage3) { Reporter.log(
			 * "Log a Call has been done successfully"); } else {
			 * fc.utobj().throwsException("Log a call failed"); }
			 */

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseId = "TC_Verify_InfoMgr_Regional_Actions_Print", testCaseDescription = "Test to verify print functionality from action buttons", reference = {
			"" })
	public void InfoMgr_Regional_Actions_Print() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		RegionalCommonMethods regional = fc.infomgr().regional();
		InfoMgrRegionalPage regionalPage = new InfoMgrRegionalPage(driver);
		AdminInfoMgrCommonMethods adminInfoMgr = new AdminInfoMgrCommonMethods();

		try {
			fc.loginpage().login(driver);
			// adminInfoMgr.configureAllFieldsOnManageFormGeneratorPage(driver ,
			// config);

			String areaRegionID = adminInfoMgr.addAreaRegion(driver);

			fc.utobj().printTestStep("Click on Print button displayed at the bottom of the page");
			regional.regionalPageShowAll(driver);
			regional.checkLocation(driver, areaRegionID);
			fc.utobj().clickElement(driver, regionalPage.btnPrint);
			regional.VerifyPrintPreview(driver, areaRegionID);

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Click on Print link displayed under Action button displayed at the top of the page"
			 * ); regional.regionalPageShowAll(driver);
			 * regional.checkLocation(driver, areaRegionID);
			 * fc.utobj().selectActionMenuItemsWithTagInput(driver, "Print");
			 * regional.VerifyPrintPreview(driver, areaRegionID);
			 */

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr","Summary_Filter" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseId = "TC_Verify_InfoMgr_Regional_Filter", testCaseDescription = "Test to verify filter displayed on the regional Page", reference = {
			"" })
	public void InfoMgr_Regional_Filter_Verify() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> regionFilter = new HashMap<>();

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		RegionalCommonMethods regional = fc.infomgr().regional();
		AdminInfoMgrCommonMethods adminInfoMgr = new AdminInfoMgrCommonMethods();

		try {
			fc.loginpage().login(driver);

			String areaRegion = adminInfoMgr.addAreaRegion(driver);
			regionFilter.put("areaRegionID", areaRegion);
			InfoMgrOwnersPage regionalOwnerPage = new InfoMgrRegionalPage(driver).getOwnersPage();

			fc.utobj().printTestStep("Modifying Area Info for Agreement Type");
			regional.searchAndClickAreaRegion(driver, areaRegion);
			String agreementType = regional.addAreaRegionDetails(driver);
			regionFilter.put("agreementType", agreementType);

			fc.utobj().printTestStep("Adding area owner");
			fc.utobj().clickElement(driver, regionalOwnerPage.lnkAreaOwner);
			Map<String, String> lstOwner = regional.addAreaRegionOwner(driver);
			regionFilter.put("areaOwnerID", lstOwner.get("FirstName"));

			fc.utobj().printTestStep("Verify Search Filter");
			fc.infomgr().infomgr_common().InfoMgrRegional(driver);
			regional.search(driver, config, regionFilter);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr_test1" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseId = "TC_Verify_InfoMgr_Regional_AreaInfo_CustomFieldsandSection", testCaseDescription = "Test to verify custom fields added to center info by manage form generator.", reference = {
			"" })
	public void InfoMgr_Regional_CenterInfo_CustomFields() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		RegionalCommonMethods regional = fc.infomgr().regional();
		AdminInfoMgrCommonMethods adminInfoMgr = new AdminInfoMgrCommonMethods();

		try {
			fc.loginpage().login(driver);

			fc.utobj().printTestStep("Add custom fields to Area Info");
			Map<String, String> dsCenterInfoCustomFields = adminInfoMgr
					.ManageFormGenerator_Add_Section_Fields_To_Forms(driver, config, "Regional", "Area Info", "reg");

			fc.utobj().printTestStep("Adding Area / Region ");
			String areaRegionID = adminInfoMgr.addAreaRegion(driver);
			regional.searchAndClickAreaRegion(driver, areaRegionID);
			fc.utobj().printTestStep("Verify that added custom fields are displayed on the area info page");

			for (String key : dsCenterInfoCustomFields.keySet()) {
				if (!key.contains("drpOption")) {
					if (fc.utobj().assertPageSource(driver, dsCenterInfoCustomFields.get(key))) {
						Reporter.log(
								"Custom field " + dsCenterInfoCustomFields.get(key) + " is displayed on the page !!");
					} else {
						fc.utobj().throwsException(
								"Custom field " + dsCenterInfoCustomFields.get(key) + " is not displayed on the page");
					}
				}
			}

			fc.utobj().printTestStep("Deleting the created section and its fields");
			adminInfoMgr.deleteSectionandFields(driver, config, "Regional", "Area Info", dsCenterInfoCustomFields);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
}
