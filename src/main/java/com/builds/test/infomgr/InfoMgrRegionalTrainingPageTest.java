package com.builds.test.infomgr;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.uimaps.infomgr.InfoMgrRegionalPage;
import com.builds.uimaps.infomgr.InfoMgrTrainingPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class InfoMgrRegionalTrainingPageTest {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "infomgr"})

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-10-09", testCaseId = "TC_InfoMgr_Regional_Training_Add", testCaseDescription = "This test case will verify training page of Regional ", reference = {
			"" })
	public void infoMgr_regional_AddTraining() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();
		RegionalCommonMethods regional = fc.infomgr().regional();

		try {
			driver = fc.loginpage().login(driver);

			// Add region
			fc.utobj().printTestStep("Add area region");
			String region = adminInfoMgr.addAreaRegion(driver);

			// Search region and click on the element
			fc.utobj().printTestStep("Search and click area region");
			regional.searchAndClickAreaRegion(driver, region);

			// Add training
			fc.utobj().printTestStep("Add training");
			InfoMgrTrainingPage regionalTrainingPage = new InfoMgrRegionalPage(driver).getTrainingPage();
			fc.utobj().clickElement(driver, regionalTrainingPage.lnkTraining);
			regional.addTraining(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

}
