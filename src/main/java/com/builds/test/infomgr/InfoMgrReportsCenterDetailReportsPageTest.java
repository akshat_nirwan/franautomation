package com.builds.test.infomgr;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.apache.commons.lang3.time.DateUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.builds.test.common.Location;
import com.builds.uimaps.infomgr.InfoMgrContactHistoryPage;
import com.builds.uimaps.infomgr.InfoMgrCorporateLocationsPage;
import com.builds.uimaps.infomgr.InfoMgrDefaultAndTerminationPage;
import com.builds.uimaps.infomgr.InfoMgrFranchiseesPage;
import com.builds.uimaps.infomgr.InfoMgrMultiUnitEntityPage;
import com.builds.uimaps.infomgr.InfoMgrReportsCenterAddressReportPage;
import com.builds.uimaps.infomgr.InfoMgrReportsCenterSummaryReportsPage;
import com.builds.uimaps.infomgr.InfoMgrReportsPage;
import com.builds.uimaps.infomgr.InfoMgrUsersPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class InfoMgrReportsCenterDetailReportsPageTest {

	FranconnectUtil fc = new FranconnectUtil();
	InfoMgr_Common imc = new InfoMgr_Common();

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_InfoMgr_Reports_Center_Address_Report_Filter", testCaseDescription = "This test case will verify Center address report with all the filters", reference = {
			"" })
	public void InfoMgr_Reports_Center_Address_Report_Filter() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		ReportsCommonMethods reports = fc.infomgr().reports();
		AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();
		InfoMgrReportsCenterAddressReportPage centerAddressReportsPage = new InfoMgrReportsCenterAddressReportPage(
				driver);

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Add franchise location");
			InfoMgrFranchiseeFilter franchisee = adminInfoMgr.addFranchiseLocationForFilter(driver, false);

			fc.utobj().printTestStep("Navigate to Center Address Report");
			reports.navigateToCenterAddressReport(driver);

			fc.utobj().printTestStep("Fill in the filters");
			//fc.utobj().setToDefault(driver, centerAddressReportsPage.drpFranchiseID);
			fc.utobj().selectValFromMultiSelect(driver, centerAddressReportsPage.drpFranchiseID,
					franchisee.getFranchiseeID());
			//fc.utobj().setToDefault(driver, centerAddressReportsPage.drpOwnerName);
			fc.utobj().selectValFromMultiSelect(driver, centerAddressReportsPage.drpOwnerName,
					franchisee.getOwnerFirstName());
			// fc.utobj().selectDropDownByVisibleText(driver,
			// centerAddressReportsPage.drpIsManagingOwner,franchisee.getOwnerType());
			//fc.utobj().setToDefault(driver, centerAddressReportsPage.drpFranchiseType);
			fc.utobj().selectValFromMultiSelect(driver, centerAddressReportsPage.drpFranchiseType,
					franchisee.getStatusID());
			//fc.utobj().setToDefault(driver, centerAddressReportsPage.drpDivision);
			fc.utobj().selectValFromMultiSelect(driver, centerAddressReportsPage.drpDivision,
					franchisee.getDivisionID());
			// fc.utobj().selectDropDownByVisibleText(driver,
			// centerAddressReportsPage.drpStatus,franchisee.getStoreStatus());
			fc.utobj().clickElement(driver, centerAddressReportsPage.btnViewReport);

			fc.utobj().printTestStep("Verify in Center Address report");
			boolean isFranchiseDisplayed = fc.utobj().assertPageSource(driver, franchisee.getFranchiseeID());
			if (isFranchiseDisplayed) {
				Reporter.log("Center Address Report filter is working properly");
			} else {
				fc.utobj().throwsException("Center Address Report filter is not working properly.");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_InfoMgr_Reports_CenterSummary_Filter", testCaseDescription = "This test case will verify Center summary report with all the filters", reference = {
			"" })
	public void InfoMgr_Reports_Center_Summary_Report_Filter() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		ReportsCommonMethods reports = fc.infomgr().reports();
		AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();
		InfoMgrReportsCenterSummaryReportsPage centerSummaryReportsPage = new InfoMgrReportsCenterSummaryReportsPage(
				driver);

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Add franchise location");
			InfoMgrFranchiseeFilter franchisee = adminInfoMgr.addFranchiseLocationForFilter(driver, false);

			fc.utobj().printTestStep("Navigate to Center Summary Report");
			reports.navigateToCenterSummaryReport(driver);

			fc.utobj().printTestStep("Fill in the filters");
			fc.utobj().selectDropDownByVisibleText(driver, centerSummaryReportsPage.drpFranchiseID,
					franchisee.getFranchiseeID());
			fc.utobj().clickElement(driver, centerSummaryReportsPage.btnViewReport);
			fc.utobj().printTestStep("Center Summary Report");
			boolean isFranchiseDisplayed = fc.utobj().assertPageSource(driver, franchisee.getFranchiseeID());
			if (isFranchiseDisplayed) {
				Reporter.log("Center Summary report filter is working properly");
			} else {
				fc.utobj().throwsException("Center Summary report filter is not working properly.");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Verify_Reports_Franchisee-To-Owner_Report", testCaseDescription = "This test case will verify Franchisee To Owner Report with all the filters", reference = {
			"" })
	public void InfoMgr_Reports_Reports_Franchisee_Owner_Report_Filter() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		ReportsCommonMethods reports = fc.infomgr().reports();
		AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();
		InfoMgrReportsCenterAddressReportPage centerAddressReportsPage = new InfoMgrReportsCenterAddressReportPage(
				driver);

		try {

			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Add franchise location");
			InfoMgrFranchiseeFilter franchisee = adminInfoMgr.addFranchiseLocationForFilter(driver, false);

			fc.utobj().printTestStep("Navigate to Franchisee To Owner Report");
			reports.navigateToFranchiseeOwnerReport(driver);

			fc.utobj().printTestStep("Fill in the filters");
			//fc.utobj().setToDefault(driver, centerAddressReportsPage.drpFranchiseID);
			fc.utobj().selectValFromMultiSelect(driver, centerAddressReportsPage.drpFranchiseID,
					franchisee.getFranchiseeID());
			//fc.utobj().setToDefault(driver, centerAddressReportsPage.drpRegionName);
			fc.utobj().selectValFromMultiSelect(driver, centerAddressReportsPage.drpRegionName,
					franchisee.getAreaRegionName());
			//fc.utobj().setToDefault(driver, centerAddressReportsPage.drpDivision);
			fc.utobj().selectValFromMultiSelect(driver, centerAddressReportsPage.drpDivision,
					franchisee.getDivisionID());
			fc.utobj().clickElement(driver, centerAddressReportsPage.btnViewReport);

			fc.utobj().printTestStep("Verify in Franchisee To Owner Report");
			boolean isFranchiseDisplayed = fc.utobj().assertPageSource(driver, franchisee.getFranchiseeID());
			if (isFranchiseDisplayed) {
				Reporter.log("Franchisee To Owner Reportt filter is working properly");
			} else {
				fc.utobj().throwsException("Franchisee To Owner Report is not working properly.");
			}

			fc.utobj().printTestStep("Navigate to Owner Details Report");
			reports.navigateToOwnerDetailsReport(driver);

			fc.utobj().printTestStep("Fill in the filters");
			//fc.utobj().setToDefault(driver, centerAddressReportsPage.drpFranchiseID);
			fc.utobj().selectValFromMultiSelect(driver, centerAddressReportsPage.drpFranchiseID,
					franchisee.getFranchiseeID());
			//fc.utobj().setToDefault(driver, centerAddressReportsPage.drpOwnerName);
			fc.utobj().selectValFromMultiSelect(driver, centerAddressReportsPage.drpOwnerName,
					franchisee.getOwnerFirstName());
			//fc.utobj().setToDefault(driver, centerAddressReportsPage.drpDivision);
			fc.utobj().selectValFromMultiSelect(driver, centerAddressReportsPage.drpDivision,
					franchisee.getDivisionID());
			fc.utobj().clickElement(driver, centerAddressReportsPage.btnViewReport);

			fc.utobj().printTestStep("Verify in Owner Details Report");

			isFranchiseDisplayed = fc.utobj().assertPageSource(driver, franchisee.getFranchiseeID());
			if (isFranchiseDisplayed) {
				Reporter.log("Owner Details Report filter is working properly");
			} else {
				fc.utobj().throwsException("Owner Details Report is not working properly.");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Verify_Reports_Entity_Details_Report", testCaseDescription = "This test case will verify Entity Details Report with all the filters", reference = {
			"" })
	public void InfoMgr_ReportsEntityDetailsReport_Filter() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		ReportsCommonMethods reports = fc.infomgr().reports();
		CorporateLocationsCommonMethods corporateLocations = fc.infomgr().corporateLocations();
		AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();
		InfoMgrReportsCenterAddressReportPage centerAddressReportsPage = new InfoMgrReportsCenterAddressReportPage(
				driver);
		InfoMgrCorporateLocationsPage corporateLocationsPage = new InfoMgrCorporateLocationsPage(driver);
		InfoMgrContactHistoryPage contactHistoryPage = corporateLocationsPage.getContactHistoryPage();
		MultiUnitEntityCommonMethods multiUnit = new MultiUnitEntityCommonMethods();
		InfoMgrMultiUnitEntityPage multiUnitPage = new InfoMgrMultiUnitEntityPage(driver);
		try {

			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Add franchise location");
			InfoMgrFranchiseeFilter franchisee = adminInfoMgr.addFranchiseLocationForFilter(driver, false);

			fc.utobj().printTestStep("Search franchise and click");
			searchFranchise(driver, (String) franchisee.getFranchiseeID());

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,("//*[@id='siteMainTable']//a[text()='" + (String) franchisee.getFranchiseeID() + "']")));

			// Add Entity Details
			fc.utobj().clickElement(driver, contactHistoryPage.linkEntityDetail);
			String entityName = corporateLocations.addEntityDetails(driver, config);

			// Navigate to Entity and click on it.
			fc.utobj().printTestStep("Search Entity and click");
			fc.infomgr().infomgr_common().InfoMgrMultiUnitEntity(driver);

			if (fc.utobj().assertLinkText(driver, "Change to Entity View")) {
				fc.utobj().clickLink(driver, "Change to Entity View");
				fc.utobj().printTestStep("Changed to Entity View");
			}

			// Navigate to multi Unit and click on it
			//fc.utobj().printTestStep("Search multi unit and click");

			/*
			 * fc.utobj().sendKeys(driver,
			 * multiUnitPage.txtSearchfimMUEntityString , entityName );
			 * fc.utobj().clickElement(driver,
			 * multiUnitPage.imgfimMUEntitySearchButton);
			 */

			boolean isFilterOpen = false;
			try {
				fc.utobj().clickElement(driver, ".//*[@id='showFilter']");
				isFilterOpen = fc.utobj().getElementByID(driver, "ms-parentcompanyNameId").isDisplayed();
			} catch (Exception e) {

			}
			if (isFilterOpen == false) {
				try {
					fc.utobj().clickElement(driver, ".//*[@id='showFilter']");
				} catch (Exception e) {

				}
			}
			fc.utobj().selectValFromMultiSelect(driver, fc.utobj().getElementByID(driver, "ms-parentcompanyNameId"),
					entityName);
			fc.utobj().clickElement(driver, fc.utobj().getElementByID(driver, "search"));

			fc.utobj().clickPartialLinkText(driver, entityName);

			fc.utobj().printTestStep("Modify multi unit and click");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, "//*[@id='siteMainTable']//span[text()='Modify']"));
			fc.utobj().selectValFromMultiSelectwithoutSelectAll(driver,driver.findElement(By.id("ms-parentexOwnerIDs")) , franchisee.getOwnerFirstName());
			

			/*try {

				WebElement ownerSearch = fc.utobj().getElementByID(driver, "ms-parentexOwnerIDs");
				fc.utobj().clickElement(driver, ownerSearch);
				WebElement searchTextBox = ownerSearch.findElement(By.xpath(".//input[@class='searchInputMultiple']"));
				fc.utobj().sendKeys(driver, searchTextBox, (String) franchisee.getOwnerFirstName());
				WebElement element3 = ownerSearch.findElement(
						By.xpath(".//label[contains(text(),'" + (String) franchisee.getOwnerFirstName() + "')]/input"));
				try {

					if (!element3.isSelected()) {
						fc.utobj().clickElement(driver, element3);
					}

				} catch (Exception e) {
					e.printStackTrace();
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
*/
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, "//*[@name='Submit']"));

			fc.utobj().printTestStep("Navigate to Entity Details Report");
			reports.navigateToEntityDetailsReport(driver);

			fc.utobj().printTestStep("Fill in the filters");
			////fc.utobj().setToDefault(driver, centerAddressReportsPage.drpFranchiseID);
			fc.utobj().selectValFromMultiSelect(driver, centerAddressReportsPage.drpFranchiseID,
					franchisee.getFranchiseeID());
			////fc.utobj().setToDefault(driver, centerAddressReportsPage.drpOwnerName);
			fc.utobj().selectValFromMultiSelect(driver, centerAddressReportsPage.drpOwnerName,
					franchisee.getOwnerFirstName());
			////fc.utobj().setToDefault(driver, centerAddressReportsPage.drpEntityName);
			fc.utobj().selectValFromMultiSelect(driver, centerAddressReportsPage.drpEntityName, entityName);
			////fc.utobj().setToDefault(driver, centerAddressReportsPage.drpDivision);
			fc.utobj().selectValFromMultiSelect(driver, centerAddressReportsPage.drpDivision,
					franchisee.getDivisionID());
			fc.utobj().clickElement(driver, centerAddressReportsPage.btnViewReport);
			fc.utobj().printTestStep("Verify in Entity Details Report ");
			boolean isFranchiseDisplayed = fc.utobj().verifyElementOnVisible_ByXpath(driver, "//a[text()='" + entityName + "']"); // fc.utobj().assertPageSource(driver,
																												// entityName);
			if (isFranchiseDisplayed) {
				Reporter.log("Entity Details Report  filter is working properly");
			} else {
				fc.utobj().throwsException("Entity Details Report is not working properly.");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr"})

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Verify_Reports_Email_Status_Report", testCaseDescription = "This test case will verify Email Status Report  with all the filters", reference = {
			"" })
	public void InfoMgr_ReportsEmailStatusReportFilter() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		ReportsCommonMethods reports = fc.infomgr().reports();
		CorporateLocationsCommonMethods corporateLocations = fc.infomgr().corporateLocations();
		AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();
		InfoMgrReportsCenterAddressReportPage centerAddressReportsPage = new InfoMgrReportsCenterAddressReportPage(
				driver);
		InfoMgrCorporateLocationsPage corporateLocationsPage = new InfoMgrCorporateLocationsPage(driver);
		InfoMgrContactHistoryPage contactHistoryPage = corporateLocationsPage.getContactHistoryPage();
		FranchiseesCommonMethods franchisees = fc.infomgr().franchisees();
		try {

			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Add franchise location");
			InfoMgrFranchiseeFilter franchisee = adminInfoMgr.addFranchiseLocationForFilter(driver, false);

			fc.utobj().printTestStep("Search franchise and click");
			searchFranchise(driver, (String) franchisee.getFranchiseeID());

			fc.utobj().printTestStep("Clicking on Send mail link under action image button ");
			fc.utobj().actionImgOption(driver, (String) franchisee.getFranchiseeID(), "Send Email");
			String mailSubject = franchisees.sendTextEmail(driver, config, false);

			fc.utobj().printTestStep("Navigate to Email Status Report");
			reports.navigateToEmailStatusReport(driver);

			fc.utobj().printTestStep("Fill in the filters");

			//fc.utobj().setToDefault(driver, centerAddressReportsPage.drpDivision);
			fc.utobj().selectValFromMultiSelect(driver, centerAddressReportsPage.drpDivision,
					franchisee.getDivisionID());
			fc.utobj().selectDropDown(driver, centerAddressReportsPage.drpEffectiveDate, "Today");

			fc.utobj().clickElement(driver, centerAddressReportsPage.btnViewReport);
			fc.utobj().printTestStep("Verify in Email Status Report");
			boolean isFranchiseDisplayed = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					"//a[text()='" + franchisee.getFranchiseeID() + "']");
			if (isFranchiseDisplayed) {
				Reporter.log("Email Status Report filter is working properly");
			} else {
				fc.utobj().throwsException("Email Status Report is not working properly.");
			}
			boolean ismailSubjectDisplayed = fc.utobj().verifyElementOnVisible_ByXpath(driver, "//a[text()='" + mailSubject + "']");
			if (ismailSubjectDisplayed) {
				Reporter.log("Email Status Report filter is working properly");
			} else {
				fc.utobj().throwsException("Email Status Report Subject is not working properly.");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Verify_Reports_Contact_History-Call_Report", testCaseDescription = "This test case will verify Contact History-Call Report  with all the filters", reference = {
			"" })
	public void InfoMgr_ReportsContactHistoryCallReportFilter() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		ReportsCommonMethods reports = fc.infomgr().reports();
		CorporateLocationsCommonMethods corporateLocations = fc.infomgr().corporateLocations();
		AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();
		InfoMgrReportsCenterAddressReportPage centerAddressReportsPage = new InfoMgrReportsCenterAddressReportPage(
				driver);
		InfoMgrCorporateLocationsPage corporateLocationsPage = new InfoMgrCorporateLocationsPage(driver);
		InfoMgrContactHistoryPage contactHistoryPage = corporateLocationsPage.getContactHistoryPage();
		FranchiseesCommonMethods franchisees = fc.infomgr().franchisees();
		try {

			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Add franchise location");
			InfoMgrFranchiseeFilter franchisee = adminInfoMgr.addFranchiseLocationForFilter(driver, false);

			fc.utobj().printTestStep("Search franchise and click");
			searchFranchise(driver, (String) franchisee.getFranchiseeID());

			fc.utobj().printTestStep("Verify Log a call page");
			fc.utobj().actionImgOption(driver, (String) franchisee.getFranchiseeID(), "Log a Call");
			String callSubject = franchisees.logACall(driver, false);

			fc.utobj().printTestStep("Navigate to Contact History-Call Report");
			reports.navigateToContactHistoryCallReport(driver);

			fc.utobj().printTestStep("Fill in the filters");

			//fc.utobj().setToDefault(driver, centerAddressReportsPage.drpFranchiseID);
			fc.utobj().selectValFromMultiSelect(driver, centerAddressReportsPage.drpFranchiseID,
					franchisee.getFranchiseeID());
			//fc.utobj().setToDefault(driver, centerAddressReportsPage.drpOwnerName);
			fc.utobj().selectValFromMultiSelect(driver, centerAddressReportsPage.drpOwnerName,
					franchisee.getOwnerFirstName());

			//fc.utobj().setToDefault(driver, centerAddressReportsPage.drpDivision);
			fc.utobj().selectValFromMultiSelect(driver, centerAddressReportsPage.drpDivision,
					franchisee.getDivisionID());

			fc.utobj().clickElement(driver, centerAddressReportsPage.btnViewReport);

			fc.utobj().printTestStep("Verify in Contact History-Call Report");
			boolean isFranchiseDisplayed = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					"//a[text()='" + franchisee.getFranchiseeID() + "']");
			if (isFranchiseDisplayed) {
				Reporter.log("Contact History-Call Report filter is working properly");
			} else {
				fc.utobj().throwsException("Contact History-Call Report is not working properly.");
			}
			boolean ismailSubjectDisplayed = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + callSubject + "')]");
			if (ismailSubjectDisplayed) {
				Reporter.log("Contact History-Call Report filter is working properly");
			} else {
				fc.utobj().throwsException("Contact History-Call Report Subject is not working properly.");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr", "IMFailedTC"})

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-31", testCaseId = "TC_Verify_Reports_Default_and_Termination_Report", testCaseDescription = "This test case will verify Default and Termination Report with all the filters", reference = {
			"" })
	public void InfoMgrDefaultandTerminationReport() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		WebDriver driver = fc.commonMethods().browsers().openBrowser();	
		ReportsCommonMethods reports = fc.infomgr().reports();
		AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();
		InfoMgrReportsCenterAddressReportPage centerAddressReportsPage = new InfoMgrReportsCenterAddressReportPage(
				driver);
		InfoMgrCorporateLocationsPage corporateLocationsPage = new InfoMgrCorporateLocationsPage(driver);
		InfoMgrContactHistoryPage contactHistoryPage = corporateLocationsPage.getContactHistoryPage();
		try {

			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Add franchise location");
			InfoMgrFranchiseeFilter franchisee = adminInfoMgr.addFranchiseLocationForFilter(driver, false);

			fc.utobj().printTestStep("Search franchise and click");
			imc.SearchFranchiseeAndClick(driver, franchisee.getFranchiseeID());
			//searchFranchise(driver, (String) franchisee.getFranchiseeID());
			//fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,("//*[@id='siteMainTable']//a[text()='" + (String) franchisee.getFranchiseeID() + "']")));
			
			//imc.SearchFranchiseeAndClick(driver, franchisee.getFranchiseeID());
			// Add Default and Termination Report Details
			fc.utobj().clickElement(driver, contactHistoryPage.defaultTerminationTab);
			// franchisees.terminateFranchiseLocation(driver,
			// config,franchisee.getFranchiseeID());
			fc.utobj().printTestStep("Franchisee ID is "+franchisee.getFranchiseeID()+"");
			InfoMgrDefaultAndTerminationPage defaultAndTerminationPage = new InfoMgrDefaultAndTerminationPage(driver);

			try {

				fc.utobj().sendKeys(driver, defaultAndTerminationPage.txtCommitteApprovedDate,
						getFutureDateUSFormat("USA", 0));
				fc.utobj().sendKeys(driver, defaultAndTerminationPage.txtDateTerminated,
						getFutureDateUSFormat("USA", 0));
				fc.utobj().selectDropDownByVisibleText(driver, defaultAndTerminationPage.drpReason, "Abandonment");
				fc.utobj().selectDropDownByVisibleText(driver, defaultAndTerminationPage.drpTypeOfAction,
						"Termination");
				fc.utobj().clickElement(driver, defaultAndTerminationPage.btnSubmit);
				fc.utobj().acceptAlertBox(driver);
			} catch (Exception e) {
				fc.utobj().throwsException("Default and Termination Tab is not working properly.");
			}

			fc.utobj().printTestStep("Navigate to Default and Termination Report");
			reports.navigateToDefaultandTerminationReport(driver);

			fc.utobj().printTestStep("Fill in the filters");
			//fc.utobj().setToDefault(driver, centerAddressReportsPage.drpFranchiseID);
			fc.utobj().selectValFromMultiSelect(driver, centerAddressReportsPage.drpFranchiseID,
					franchisee.getFranchiseeID());
			//fc.utobj().setToDefault(driver, centerAddressReportsPage.drpOwnerName);
			fc.utobj().selectValFromMultiSelect(driver, centerAddressReportsPage.drpOwnerName,
					franchisee.getOwnerFirstName());
			//fc.utobj().setToDefault(driver, centerAddressReportsPage.drpDivision);
			fc.utobj().selectValFromMultiSelect(driver, centerAddressReportsPage.drpDivision,
					franchisee.getDivisionID());
			fc.utobj().clickElement(driver, centerAddressReportsPage.btnViewReport);
			fc.utobj().printTestStep("Verify in Default and Termination Report ");
			/*boolean isFranchiseDisplayed = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					"//a[text()='" + franchisee.getFranchiseeID() + "']"); // fc.utobj().assertPageSource(driver,
*/			boolean isFranchiseDisplayed = fc.utobj().assertPageSource(driver, franchisee.getFranchiseeID());	// entityName);
			
			if (isFranchiseDisplayed) {
				Reporter.log("Default and Termination Report  filter is working properly");
				fc.utobj().printTestStep("Default and Termination Report  filter is working properly");
			} else {
				fc.utobj().throwsException("Default and Termination Report is not working properly.");
			}

			fc.utobj().printTestStep("Navigate to Franchise / Store Status Report");
			reports.navigateToFranchiseStoreStatusReport(driver);

			fc.utobj().selectDropDown(driver, centerAddressReportsPage.drpmatchType3, "On");
			fc.utobj().sendKeys(driver, centerAddressReportsPage.TerminationDate, fc.utobj().getFutureDateUSFormat(0));
			fc.utobj().clickElement(driver, centerAddressReportsPage.btnViewReport);
			/*isFranchiseDisplayed = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + franchisee.getOwnerFirstName() + "')]");*/
			isFranchiseDisplayed = fc.utobj().assertPageSource(driver, franchisee.getOwnerFirstName());
			if (isFranchiseDisplayed) {
				Reporter.log("Franchise Store Status report filter is working properly");
			} else {
				fc.utobj().throwsException("Franchise Store Status Report is not working properly.");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-21", testCaseId = "TC_Verify_Reports_FDD_Store_Franchise_List", testCaseDescription = "This test case will verify FDD Store / Franchise List with all the filters", reference = {
			"" })
	public void InfoMgr_FDDStoreFranchiseListFilter() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		ReportsCommonMethods reports = fc.infomgr().reports();
		AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();
		InfoMgrReportsCenterAddressReportPage centerAddressReportsPage = new InfoMgrReportsCenterAddressReportPage(
				driver);

		try {

			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Add franchise location");
			InfoMgrFranchiseeFilter franchisee = adminInfoMgr.addFranchiseLocationForFilter(driver, false);

			fc.utobj().printTestStep("Navigate to FDD Store / Franchise List");
			reports.navigateToFDDStoreFranchiseeListReport(driver);

			fc.utobj().printTestStep("Fill in the filters");
			//fc.utobj().setToDefault(driver, centerAddressReportsPage.drpDivision);
			fc.utobj().selectValFromMultiSelect(driver, centerAddressReportsPage.drpDivision,
					franchisee.getDivisionID());
			// fc.utobj().clickElement(driver,
			// fc.utobj().getElementByXpath(driver,".//*[@id='ms-parentcomboDivision']/button")));//
			// Element not clickable
			fc.utobj().clickElement(driver, centerAddressReportsPage.btnViewReport);

			fc.utobj().printTestStep("Verify in FDD Store / Franchise List Report");

			boolean isFranchiseDisplayed = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + franchisee.getOwnerFirstName() + "')]");
			if (isFranchiseDisplayed) {
				Reporter.log("FDD Store / Franchise List filter is working properly");
			} else {
				fc.utobj().throwsException("FDD Store / Franchise List is not working properly.");
			}
			boolean isStateDisplayed = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td/span[contains(text () , '" + franchisee.getState() + "')]");
			if (isStateDisplayed) {
				Reporter.log("FDD Store / Franchise List Sate  is working properly");
			} else {
				fc.utobj().throwsException("FDD Store / Franchise List Sate is not showing.");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-08-30", testCaseId = "TC_Verify_Reports_Birthday_Report", testCaseDescription = "This report lists the Birthday details of all Franchise Users with respect to Franchisees.", reference = {
			"" })
	public void InfoMgr_ReportsBirthday_Report_Filter() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		ReportsCommonMethods reports = fc.infomgr().reports();
		CorporateLocationsCommonMethods corporateLocations = fc.infomgr().corporateLocations();
		AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();
		InfoMgrReportsCenterAddressReportPage centerAddressReportsPage = new InfoMgrReportsCenterAddressReportPage(
				driver);
		InfoMgrCorporateLocationsPage corporateLocationsPage = new InfoMgrCorporateLocationsPage(driver);
		InfoMgrContactHistoryPage contactHistoryPage = corporateLocationsPage.getContactHistoryPage();
		FranchiseesCommonMethods franchiseescommon = fc.infomgr().franchisees();

		try {

			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Add franchise location");
			InfoMgrFranchiseeFilter franchisee = adminInfoMgr.addFranchiseLocationForFilter(driver, false);

			fc.utobj().printTestStep("Search franchise and click");
			searchFranchise(driver, (String) franchisee.getFranchiseeID());

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,("//*[@id='siteMainTable']//a[text()='" + (String) franchisee.getFranchiseeID() + "']")));

			// Add User
			fc.utobj().clickElement(driver, contactHistoryPage.lnkUsers);
			InfoMgrUsersPage usersPage = new InfoMgrFranchiseesPage(driver).getUsersPage();
			fc.utobj().clickElement(driver, usersPage.btnAddUser);
			Map<String, String> map=franchiseescommon.addOwnerUserBirthDayCurrent(driver, config,
					franchisee.getOwnerFirstName() + " " + franchisee.getOwnerLastName());

			
			System.out.println(map.get("month"));
			System.out.println(map.get("day"));
			String s = map.get("day");
			int birthdate=Integer.parseInt(s);
			
			
			fc.utobj().printTestStep("Navigate to Birthday Report");
			reports.navigateToBirthday_Report(driver);

			fc.utobj().printTestStep("Fill in the filters");
			fc.utobj().selectDropDown(driver, centerAddressReportsPage.matchType10, "On");
			fc.utobj().selectDropDown(driver, centerAddressReportsPage.BirthMonth, map.get("month"));
			fc.utobj().selectDropDown(driver, centerAddressReportsPage.BirthDate, birthdate+"");
			
			fc.utobj().clickElement(driver, centerAddressReportsPage.btnViewReport);

			fc.utobj().printTestStep("Verify in Birthday Report");
			boolean isFranchiseDisplayed = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					"//a[text()='" + franchisee.getFranchiseeID() + "']"); // fc.utobj().assertPageSource(driver,
																			// entityName);
			if (isFranchiseDisplayed) {
				Reporter.log("Birthday Report  filter is working properly");
			} else {
				fc.utobj().throwsException("Birthday Report is not working properly.");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr", "infomgrSerchChk" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Verify_Reports_Renewal_Status_Report", testCaseDescription = "This report lists the Renewal Expiration Date details for all the Franchisees.", reference = {
			"" })
	public void InfoMgr_Renewal_Status_Report_Filter() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		ReportsCommonMethods reports = fc.infomgr().reports();
		AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();
		InfoMgrReportsCenterAddressReportPage centerAddressReportsPage = new InfoMgrReportsCenterAddressReportPage(
				driver);
		InfoMgrCorporateLocationsPage corporateLocationsPage = new InfoMgrCorporateLocationsPage(driver);
		InfoMgrContactHistoryPage contactHistoryPage = corporateLocationsPage.getContactHistoryPage();
		FranchiseesCommonMethods franchiseescommon = fc.infomgr().franchisees();

		try {

			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Add franchise location");
			InfoMgrFranchiseeFilter franchisee = adminInfoMgr.addFranchiseLocationForFilter(driver, false);

			fc.utobj().printTestStep("Search franchise and click");
			searchFranchise(driver, (String) franchisee.getFranchiseeID());

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,("//*[@id='siteMainTable']//a[text()='" + (String) franchisee.getFranchiseeID() + "']")));

			// Add User
			fc.utobj().clickElement(driver, contactHistoryPage.lnkRenewal);
			String fimCbCurrentStatus = franchiseescommon.addRenewalTab(driver, config,
					franchisee.getOwnerFirstName() + " " + franchisee.getOwnerLastName());

			fc.utobj().printTestStep("Navigate to Renewal Status Report ");
			reports.navigateToRenewalStatusReport(driver);

			fc.utobj().printTestStep("Fill in the filters");
			//fc.utobj().setToDefault(driver, centerAddressReportsPage.drpFranchiseID);
			fc.utobj().selectValFromMultiSelect(driver, centerAddressReportsPage.drpFranchiseID,
					franchisee.getFranchiseeID());
			//fc.utobj().setToDefault(driver, centerAddressReportsPage.drpOwnerName);
			fc.utobj().selectValFromMultiSelect(driver, centerAddressReportsPage.drpOwnerName,
					franchisee.getOwnerFirstName());
			//fc.utobj().setToDefault(driver, centerAddressReportsPage.drpDivision);
			fc.utobj().selectValFromMultiSelect(driver, centerAddressReportsPage.drpDivision,
					franchisee.getDivisionID());
			fc.utobj().clickElement(driver, centerAddressReportsPage.btnViewReport);

			fc.utobj().printTestStep("Verify in Renewal Status Report ");
			boolean isFranchiseDisplayed = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					"//a[text()='" + franchisee.getFranchiseeID() + "']"); // fc.utobj().assertPageSource(driver,
																			// entityName);
			if (isFranchiseDisplayed) {
				Reporter.log("Renewal Status Report   filter is working properly");
			} else {
				fc.utobj().throwsException("Renewal Status Report is not working properly.");
			}

			fc.utobj().printTestStep("Search franchise and click");
			searchFranchise(driver, (String) franchisee.getFranchiseeID());

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,("//*[@id='siteMainTable']//a[text()='" + (String) franchisee.getFranchiseeID() + "']")));

			// Add Agreement
			fc.utobj().clickElement(driver, contactHistoryPage.lnkAgreement);
			driver = franchiseescommon.addAgreement(driver, config);

			fc.utobj().printTestStep("Navigate to Renewal Dues Report");
			reports.navigateToRenewalDuesReport(driver);

			fc.utobj().printTestStep("Fill in the filters");
			//fc.utobj().setToDefault(driver, centerAddressReportsPage.drpDivision);
			fc.utobj().selectValFromMultiSelect(driver, centerAddressReportsPage.drpDivision,
					franchisee.getDivisionID());
			fc.utobj().clickElement(driver, centerAddressReportsPage.btnViewReport);

			isFranchiseDisplayed = fc.utobj().verifyElementOnVisible_ByXpath(driver, "//a[text()='" + franchisee.getFranchiseeID() + "']"); // fc.utobj().assertPageSource(driver,
																														// entityName);
			if (isFranchiseDisplayed) {
				Reporter.log("Renewal Dues Report  filter is working properly");
			} else {
				fc.utobj().throwsException("Renewal Dues Report is not working properly.");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr"})

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-09-11", testCaseId = "TC_Verify_Reports_Legal_Report", testCaseDescription = "This report lists the Effective and Expiration Date details of Agreement for all the Franchisees. ", reference = {
			"" })
	public void InfoMgr_Legal_Report_Filter() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		ReportsCommonMethods reports = fc.infomgr().reports();
		AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();
		InfoMgrReportsCenterAddressReportPage centerAddressReportsPage = new InfoMgrReportsCenterAddressReportPage(
				driver);
		InfoMgrCorporateLocationsPage corporateLocationsPage = new InfoMgrCorporateLocationsPage(driver);
		InfoMgrContactHistoryPage contactHistoryPage = corporateLocationsPage.getContactHistoryPage();
		FranchiseesCommonMethods franchiseescommon = fc.infomgr().franchisees();

		try {

			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Add franchise location");
			//InfoMgrFranchiseeFilter franchisee = adminInfoMgr.addFranchiseLocationForFilter(driver, false);
			Location location = adminInfoMgr.addFranchiseLocationForInfoMgrWithDivision(driver, false);

			fc.utobj().printTestStep("Search franchise and click");
			searchFranchise(driver, (String) location.getFranchiseID());

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,("//*[@id='siteMainTable']//a[text()='" + (String) location.getFranchiseID() + "']")));

			// Add Agreement
			fc.utobj().printTestStep("Click to Add Agreement");
			fc.utobj().clickElement(driver, contactHistoryPage.lnkAgreement);
			driver = franchiseescommon.addAgreement(driver, config);

			fc.utobj().printTestStep("Navigate to  Legal Report");
			reports.navigateToLegalReport(driver);

			fc.utobj().printTestStep("Fill in the filters");
			//fc.utobj().setToDefault(driver, centerAddressReportsPage.drpFranchiseID);
			fc.utobj().selectValFromMultiSelect(driver, centerAddressReportsPage.drpFranchiseID,
					location.getFranchiseID());
			
			String OwnerFullName = location.getOwnerFirstName() +" "+ location.getOwnerLastName();
			//fc.utobj().setToDefault(driver, centerAddressReportsPage.drpOwnerName);
			fc.utobj().selectValFromMultiSelect(driver, centerAddressReportsPage.drpOwnerName,
					OwnerFullName);
			//fc.utobj().setToDefault(driver, centerAddressReportsPage.drpDivision);
			fc.utobj().selectValFromMultiSelect(driver, centerAddressReportsPage.drpDivision,
					location.getBrands());
			fc.utobj().clickElement(driver, centerAddressReportsPage.btnViewReport);

			fc.utobj().printTestStep("Verify in Legal Report");
			boolean isFranchiseDisplayed = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					"//a[text()='" + location.getFranchiseID() + "']"); // fc.utobj().assertPageSource(driver,
																			// entityName);
			if (isFranchiseDisplayed) {
				Reporter.log("Legal Report filter is working properly");
			} else {
				fc.utobj().throwsException("Legal Report is not working properly.");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Verify_Reports_Transfer_Report", testCaseDescription = "This report lists Transfer details for all the Franchisees. ", reference = {
			"" })
	public void InfoMgr_Transfer_Report_Filter() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		ReportsCommonMethods reports = fc.infomgr().reports();
		CorporateLocationsCommonMethods corporateLocations = fc.infomgr().corporateLocations();
		AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();
		InfoMgrReportsCenterAddressReportPage centerAddressReportsPage = new InfoMgrReportsCenterAddressReportPage(
				driver);
		InfoMgrCorporateLocationsPage corporateLocationsPage = new InfoMgrCorporateLocationsPage(driver);
		InfoMgrContactHistoryPage contactHistoryPage = corporateLocationsPage.getContactHistoryPage();
		FranchiseesCommonMethods franchiseescommon = fc.infomgr().franchisees();

		try {

			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Add franchise location");
			InfoMgrFranchiseeFilter franchisee = adminInfoMgr.addFranchiseLocationForFilter(driver, false);

			fc.utobj().printTestStep("Search franchise and click");
			searchFranchise(driver, (String) franchisee.getFranchiseeID());

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,("//*[@id='siteMainTable']//a[text()='" + (String) franchisee.getFranchiseeID() + "']")));

			// Add Transfer Details
			fc.utobj().printTestStep("Click to Add Transfer");
			fc.utobj().clickElement(driver, contactHistoryPage.transferTab);
			String newFranchiseeID = franchiseescommon.transferTabCaseNo(driver, config);

			fc.utobj().printTestStep("Navigate to Transfer Report");
			reports.navigateToTransferReport(driver);

			fc.utobj().printTestStep("Fill in the filters");
			//fc.utobj().setToDefault(driver, centerAddressReportsPage.drpFranchiseID);
			fc.utobj().selectValFromMultiSelect(driver, centerAddressReportsPage.drpFranchiseID,
					franchisee.getFranchiseeID());
			//fc.utobj().setToDefault(driver, centerAddressReportsPage.drpOwnerName);
			fc.utobj().selectValFromMultiSelect(driver, centerAddressReportsPage.drpOwnerName,
					franchisee.getOwnerFirstName());
			//fc.utobj().setToDefault(driver, centerAddressReportsPage.drpDivision);
			fc.utobj().selectValFromMultiSelect(driver, centerAddressReportsPage.drpDivision,
					franchisee.getDivisionID());
			fc.utobj().clickElement(driver, centerAddressReportsPage.btnViewReport);

			fc.utobj().printTestStep("Verify in Transfer Report");
			boolean isFranchiseDisplayed = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					"//a[text()='" + franchisee.getFranchiseeID() + "']"); // fc.utobj().assertPageSource(driver,
																			// entityName);
			if (isFranchiseDisplayed) {
				Reporter.log("Transfer Report filter is working properly");
			} else {
				fc.utobj().throwsException("Transfer Report is not working properly.");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	// Custom report

	@Test(groups = { "infomgr" })

	// BugID - 72366
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Verify_Reports_NewCustom_Report", testCaseDescription = "This test case will verify the print preview after modifying the field positions.", reference = {
			"72366" })
	public void createAndVerifyCusttomReport() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		AdminInfoMgrCommonMethods adminCommonMethods = fc.adminpage().adminInfoMgr();

		Map<String, String> dataSet = fc.utobj().readTestData("infomgr", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			String reportName = fc.utobj().generateTestData(dataSet.get("reportName"));
			String reportDescription = fc.utobj().generateTestData(dataSet.get("reportDescription"));
			fc.utobj().printTestStep("Navigating Info Mgr > New Reports");
			fc.infomgr().infomgr_common().InfoMgrReports(driver);
			InfoMgrReportsPage objReports = new InfoMgrReportsPage(driver);
			fc.utobj().clickElement(driver, objReports.lnkNewReport);
			fc.utobj().clickElement(driver, objReports.lnkNext);
			fc.utobj().printTestStep("Fill report details Reports");
			fc.utobj().sendKeys(driver, objReports.reportName, reportName);
			fc.utobj().sendKeys(driver, objReports.reportDescription, reportDescription);
			fc.utobj().clickElement(driver, objReports.lnkNext);
			Thread.sleep(2000);
			// fc.utobj().printTestStep(testCaseId, "Select Field to Filter by
			// Reports");

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='whereColList']//optgroup[@label='Center Info']/option[contains(text(),'Center Name')]"));
			fc.utobj().clickElement(driver, objReports.lnkGo);
			Thread.sleep(2000);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='whereColList']//optgroup[@label='Center Info']/option[contains(text(),'Franchise ID')]"));
			fc.utobj().clickElement(driver, objReports.lnkGo);
			Thread.sleep(2000);
			fc.utobj().clickElement(driver, objReports.lnkNext);
			// fc.utobj().printTestStep(testCaseId, "Select Available Fields to
			// Display");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='selColList']/optgroup[@label='Center Info']/option[contains(text(),'Center Name')]"));
			fc.utobj().clickElement(driver, objReports.lnkGo1);
			Thread.sleep(2000);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='selColList']/optgroup[@label='Center Info']/option[contains(text(),'Franchise ID')]"));
			fc.utobj().clickElement(driver, objReports.lnkGo1);
			Thread.sleep(2000);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='selColList']/optgroup[@label='Center Info']/option[contains(text(),'Country')]"));
			fc.utobj().clickElement(driver, objReports.lnkGo1);
			Thread.sleep(2000);
			fc.utobj().clickElement(driver, objReports.lnkNext);
			fc.utobj().clickElement(driver, objReports.saveBtn);

			boolean isCustomReportPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[@id='reportView']/tbody//*[contains(text(),'" + reportName + "')]");
			if (isCustomReportPresent == false) {
				fc.utobj().throwsException("was not able to verify visibility of reportName");
			}
			boolean isCustomReportPresentDescription = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[@id='reportView']/tbody//*[contains(text(),'" + reportDescription + "')]");
			if (isCustomReportPresentDescription == false) {
				fc.utobj().throwsException("was not able to verify visibility of reportDescription");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	// End Cusromer report

	public void searchFranchise(WebDriver driver, String franchiseID) throws Exception {
		Reporter.log("********************** Search Franchisee ****************** \n");

		fc.infomgr().infomgr_common().InfoMgrFranchisees(driver);

		// Search Franchisee and click on the franchiseID
		InfoMgrFranchiseesPage franchiseePage = new InfoMgrFranchiseesPage(driver);

		fc.infomgr().infomgr_common().searchLocationFromShowFilter(driver, franchiseID);

	}

	public String getFutureDateUSFormat(String country, int afterNoOfDays) {
		String format = "MM/dd/yyyy";
		if ("AUS".equals(country)) {
			format = "dd/MM/yyyy";
			
		}

		SimpleDateFormat sdfDate = new SimpleDateFormat("" + format);
		Date now = DateUtils.addDays(new Date(), afterNoOfDays);
		String strDate = sdfDate.format(now);
		return strDate;
	}
}
