package com.builds.test.infomgr;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.common.CorporateUser;
import com.builds.uimaps.infomgr.InfoMgrContactHistoryPage;
import com.builds.uimaps.infomgr.InfoMgrFranchiseesPage;
import com.builds.uimaps.infomgr.InfoMgrReportsContactHistoryTaskReportPage;
import com.builds.uimaps.infomgr.InfoMgrReportsPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class InfoMgrReportsContactHistoryTaskReportPageTest {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-21", testCaseId = "TC_InfoMgr_Reports_ContactHistory_TaskReport", testCaseDescription = "This test case will verify contact history task report sorting functionality", reference = {
			"" })
	public void VerifyContactHistoryTaskReportSorting() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		FranchiseesCommonMethods franchise = fc.infomgr().franchisees();
		AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Add franchise location");
			String franchiseID = adminInfoMgr.addFranchiseLocation(driver);

			fc.utobj().printTestStep("Add corporate user");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail("infomgrautomation@staffex.com");
			corpUser = corpTest.createDefaultUser(driver, corpUser);

			fc.utobj().printTestStep("Search franchisee and click");
			franchise.searchFranchiseAndClick(driver, franchiseID);

			fc.utobj().printTestStep("log a task and verify");
			InfoMgrContactHistoryPage contactHistoryPage = new InfoMgrFranchiseesPage(driver).getContactHistoryPage();
			;
			fc.utobj().clickElement(driver, contactHistoryPage.lnkContactHistory);
			fc.utobj().clickElement(driver, contactHistoryPage.btnLogaTask);
			String subject = franchise.logaTask(driver, corpUser.getuserFullName(), true);

			boolean isSubjectPresent = fc.utobj().assertPageSource(driver, subject);
			if (isSubjectPresent) {
				Reporter.log("Task has been created successfully");
			} else {
				fc.utobj().throwsException("Task Creation failed");
			}

			fc.utobj().printTestStep("Navigate to reports and verify contact history task report");
			fc.infomgr().infomgr_common().InfoMgrReports(driver);
			InfoMgrReportsPage reportsPage = new InfoMgrReportsPage(driver);
			fc.utobj().clickElement(driver, reportsPage.linkContactHistoryTaskReports);

			InfoMgrReportsContactHistoryTaskReportPage contacthistoryTaskReportPage = new InfoMgrReportsContactHistoryTaskReportPage(
					driver);
			fc.utobj().setToDefault(driver, contacthistoryTaskReportPage.drpFranchiseID);
			fc.utobj().selectValFromMultiSelect(driver, contacthistoryTaskReportPage.drpFranchiseID, franchiseID);
			// fc.utobj().clickElement(driver,
			// contacthistoryTaskReportPage.drpFranchiseID); // Element not
			// clickable
			fc.utobj().clickElement(driver, contacthistoryTaskReportPage.btnViewReport);

			// Retrieve the list of tasks in the task table
			int countofRecords = contacthistoryTaskReportPage.lstRecords.size();

			// Click on assigned to
			fc.utobj().clickElement(driver, contacthistoryTaskReportPage.lnkAssignedTo);
			Thread.sleep(2000);
			int noOfRecordsAfterSorting = contacthistoryTaskReportPage.lstRecords.size();
			if (noOfRecordsAfterSorting == countofRecords) {
				Reporter.log(countofRecords + " Records are intact after sorting too. Test case passes !!!");
			} else {
				fc.utobj().throwsException("Records are not same after sorting. Test case failes !!! ");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

}
