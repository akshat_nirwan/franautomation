package com.builds.test.infomgr;

import java.util.ArrayList;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.builds.uimaps.infomgr.InfoMgrLegalViolationPage;
import com.builds.uimaps.infomgr.InfoMgrReportsCustomerComplaintsReportPage;
import com.builds.uimaps.infomgr.InfoMgrReportsLegalViolationReportPage;
import com.builds.uimaps.infomgr.InfoMgrReportsPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class InfoMgrReportsCustomerComplaintLegalViolationReportPageTest {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "infomgr", "IMFailedTC" })
	// BugID - 82317 , 75210
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_InfoMgr_Reports_CustomerComplaints", testCaseDescription = "This test case will verify customer complaints reports page", reference = {
			"82317 , 75210" })
	public void VerifyComplaintsOnCustomerComplaintsReportsPage() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		FranchiseesCommonMethods franchise = fc.infomgr().franchisees();
		AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Add franchise location");
			String franchiseID = adminInfoMgr.addFranchiseLocation(driver);

			// Navigate to Feedback.jsp and log a complaint
			fc.utobj().printTestStep("Add feedback via web form");
			franchise.navigateToFeedback(driver, config.get("buildUrl"));

			ArrayList<String> lstFeedBackInfo = franchise.addFeedBack(driver, config, franchiseID);
			String complaintID = lstFeedBackInfo.get(0);
			String complainantName = lstFeedBackInfo.get(1);

			// Open the url , Navigate to Customer Complaints reports page
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate to customer complaints reports page and verify complaintid");

			fc.infomgr().infomgr_common().InfoMgrReports(driver);
			InfoMgrReportsPage objReports = new InfoMgrReportsPage(driver);
			fc.utobj().clickElement(driver, objReports.lnkCustComplaintRpt);

			// Search ComplaintID and assert that it is displayed on the page.

			InfoMgrReportsCustomerComplaintsReportPage objCustomerComplaintRpts = new InfoMgrReportsCustomerComplaintsReportPage(
					driver);
			fc.utobj().setToDefault(driver, objCustomerComplaintRpts.drpFranchiseID);
			fc.utobj().selectValFromMultiSelect(driver, objCustomerComplaintRpts.drpFranchiseID, franchiseID);
			fc.utobj().setToDefault(driver, objCustomerComplaintRpts.complaintIdsDiv);
			fc.utobj().selectValFromMultiSelect(driver, objCustomerComplaintRpts.complaintIdsDiv, complaintID);
			fc.utobj().sendKeys(driver, objCustomerComplaintRpts.txtComplainant, complainantName);
			fc.utobj().clickElement(driver, objCustomerComplaintRpts.btnViewReport);

			String tdComplaintID = fc.utobj().getText(driver, objCustomerComplaintRpts.tdComplaintID);

			String tdComplainantName = fc.utobj().getText(driver, objCustomerComplaintRpts.tdComplainantName);

			if (tdComplaintID.contains(complaintID) && tdComplainantName.contains(complainantName)) {
				Reporter.log("ComplaintID " + complaintID
						+ " is displayed in the ComplaintID column and complainant name is displayed in the complainant column. Test Passes !!!");
			} else {
				fc.utobj().throwsException(
						"ComplantID" + complaintID + " is not displayed in the ComplaintID column. Test Failes !!! ");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_InfoMgr_Reports_LegalViolation_Report_Filter", testCaseDescription = "This test case will verify legal violation report with all the filters", reference = {
			"" })
	public void InfoMgr_Reports_LegalViolation_Report_Filter() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		ReportsCommonMethods reports = fc.infomgr().reports();
		AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();
		FranchiseesCommonMethods franchise = new FranchiseesCommonMethods();
		InfoMgrLegalViolationPage legalViolationPage = new InfoMgrLegalViolationPage(driver);
		InfoMgrReportsLegalViolationReportPage legalViolationReportsPage = new InfoMgrReportsLegalViolationReportPage(
				driver);

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Add Franchise Location");
			InfoMgrFranchiseeFilter franchiseFilter = adminInfoMgr.addFranchiseLocationForFilter(driver, false);

			fc.utobj().printTestStep("Search franchisee and click");
			franchise.searchFranchiseAndClick(driver, franchiseFilter.getFranchiseeID());

			fc.utobj().printTestStep("Add Legal Violation details");
			fc.utobj().clickElement(driver, legalViolationPage.lnkLegalViolation);
			franchise.addLegalViolation(driver, config);

			fc.utobj().printTestStep("Navigate to Legal Violation Report Page");
			reports.navigateToLegalViolationReport(driver);

			fc.utobj().printTestStep("Fill in the filters");
			fc.utobj().setToDefault(driver, legalViolationReportsPage.drpFranchiseId);
			fc.utobj().selectValFromMultiSelect(driver, legalViolationReportsPage.drpFranchiseId,
					franchiseFilter.getFranchiseeID());
			fc.utobj().setToDefault(driver, legalViolationReportsPage.drpOwnerName);
			fc.utobj().selectValFromMultiSelect(driver, legalViolationReportsPage.drpOwnerName,
					franchiseFilter.getOwnerFirstName());
			fc.utobj().setToDefault(driver, legalViolationReportsPage.drpDivision);
			fc.utobj().selectValFromMultiSelect(driver, legalViolationReportsPage.drpDivision,
					franchiseFilter.getDivisionID());
			fc.utobj().clickElement(driver, legalViolationReportsPage.btnViewReport);

			fc.utobj().printTestStep("Verify in legal violation report ");
			boolean isFranchiseDisplayed = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					"//a[text()='" + franchiseFilter.getFranchiseeID() + "']");
			if (isFranchiseDisplayed) {
				Reporter.log("legal violation report  filter is working properly");
			} else {
				fc.utobj().throwsException("legal violation report  is not working properly.");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

}
