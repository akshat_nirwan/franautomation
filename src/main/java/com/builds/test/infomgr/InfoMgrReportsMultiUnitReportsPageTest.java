package com.builds.test.infomgr;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.builds.uimaps.infomgr.InfoMgrMultiUnitEntityPage;
import com.builds.uimaps.infomgr.InfoMgrMultiUnitInfoPage;
import com.builds.uimaps.infomgr.InfoMgrReportsMultiUnitReportsPage;
import com.builds.uimaps.infomgr.InfoMgrReportsPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class InfoMgrReportsMultiUnitReportsPageTest {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_InfoMgr_Reports_MultiUnit", testCaseDescription = "This test case will verify multi unit reports page", reference = {
			"" })
	public void VerifyMultiUnitReportsOnReportsPage() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		FranchiseesCommonMethods franchisee = fc.infomgr().franchisees();
		AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();
		MultiUnitEntityCommonMethods multiUnit = fc.infomgr().multiUnitEntity();

		try {
			driver = fc.loginpage().login(driver);

			// Add Region
			fc.utobj().printTestStep("Add area region");
			String regionName1 = adminInfoMgr.addAreaRegion(driver);

			// Add Division
			fc.utobj().printTestStep("Add division name");
			String divName1 = adminInfoMgr.addDivisionName(driver);

			// Add Franchisee Location with division and region name
			fc.utobj().printTestStep("Add first franchise location");
			String franchiseID1 = adminInfoMgr.addFranchiseLocationWithDivisionRegion(driver, divName1, regionName1);

			fc.utobj().printTestStep("Search franchise and click");
			franchisee.searchFranchiseAndClick(driver, franchiseID1);

			// Add Owner
			fc.utobj().printTestStep("Add owner");
			InfoMgrMultiUnitInfoPage entityInfoPage = new InfoMgrMultiUnitEntityPage(driver).getMultiUnitInfoPage();
			fc.utobj().clickElement(driver, entityInfoPage.tabOwners);
			Map<String, String> lstOwnerDetails = franchisee.addOwner(driver);

			fc.utobj().printTestStep("Add second franchise location");
			String franchiseID2 = adminInfoMgr.addFranchiseLocationWithDivisionRegion(driver, divName1, regionName1);

			fc.utobj().printTestStep("Search franchise and click");
			franchisee.searchFranchiseAndClick(driver, franchiseID2);

			fc.utobj().printTestStep("Add owners");
			fc.utobj().clickElement(driver, entityInfoPage.tabOwners);
			String multiUnitID = franchisee.addExistingOwner(driver, lstOwnerDetails.get("FirstName"));

			fc.utobj().printTestStep("Search multi unit and verify");
			multiUnit.searchMultiUnit(driver, lstOwnerDetails.get("FirstName"));

			boolean ismultiUnitIDDisplayed = fc.utobj().assertPageSource(driver, multiUnitID);

			if (ismultiUnitIDDisplayed) {
				Reporter.log("Multi Unit " + multiUnitID
						+ " is displayed on the page. Multi Unit creation is successfull !!!");
			} else {
				fc.utobj().throwsException("Multi Unit Creation has failed !!!");
			}

			fc.utobj().printTestStep("Navigate to multi unit reports and verify");

			fc.infomgr().infomgr_common().InfoMgrReports(driver);
			InfoMgrReportsPage reportsPage = new InfoMgrReportsPage(driver);
			fc.utobj().clickElement(driver, reportsPage.lnkMultiUnitReport);

			InfoMgrReportsMultiUnitReportsPage multiUnitReportsPage = new InfoMgrReportsMultiUnitReportsPage(driver);
			fc.utobj().setToDefault(driver, multiUnitReportsPage.drpMultiCheckBoxRegion);
			fc.utobj().selectValFromMultiSelect(driver, multiUnitReportsPage.drpMultiCheckBoxRegion, regionName1);

			fc.utobj().setToDefault(driver, multiUnitReportsPage.drpMultiCheckBoxMultiUnitOwner);
			fc.utobj().selectValFromMultiSelect(driver, multiUnitReportsPage.drpMultiCheckBoxMultiUnitOwner,
					lstOwnerDetails.get("FirstName"));

			fc.utobj().setToDefault(driver, multiUnitReportsPage.drpMultiCheckBoxCountry);
			fc.utobj().selectValFromMultiSelect(driver, multiUnitReportsPage.drpMultiCheckBoxCountry,
					lstOwnerDetails.get("Country"));

			fc.utobj().setToDefault(driver, multiUnitReportsPage.drpMultiCheckBoxState);
			fc.utobj().selectValFromMultiSelect(driver, multiUnitReportsPage.drpMultiCheckBoxState,
					lstOwnerDetails.get("State"));

			fc.utobj().setToDefault(driver, multiUnitReportsPage.drpMultiCheckBoxDivision);
			fc.utobj().selectValFromMultiSelect(driver, multiUnitReportsPage.drpMultiCheckBoxDivision, divName1);

			fc.utobj().clickElement(driver, multiUnitReportsPage.btnSaveView);

			String recordTxt = fc.utobj().getText(driver, multiUnitReportsPage.tblRecord);

			if (recordTxt.contains(lstOwnerDetails.get("FirstName"))) {
				Reporter.log("Multi-Unit reports are displayed on the page. Test case passes !!!");
			} else {
				fc.utobj().throwsException(
						"Error - Multi-Unit reports are not displayed on the page. Test case fails !!!");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
}
