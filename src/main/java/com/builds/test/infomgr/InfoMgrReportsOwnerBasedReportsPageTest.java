package com.builds.test.infomgr;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.builds.uimaps.infomgr.InfoMgrReportsEntityDetailsReportPage;
import com.builds.uimaps.infomgr.InfoMgrReportsFranchiseToOwnerPage;
import com.builds.uimaps.infomgr.InfoMgrReportsMultiUnitReportsPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class InfoMgrReportsOwnerBasedReportsPageTest {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "infomgr_test23" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_InfoMgr_Reports_Franchisee_To_Owner_Report_Filter", testCaseDescription = "This test case will verify Franchise to Owner Based report with all the filters", reference = {
			"" })
	public void InfoMgr_Reports_Franchisee_To_Owner_Based_Report_Filter() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		// ReportsCommonMethods reports = fc.infomgr().reports();
		AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();
		InfoMgrReportsFranchiseToOwnerPage franchiseToOwnerPage = new InfoMgrReportsFranchiseToOwnerPage(driver);

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Add franchise location");
			InfoMgrFranchiseeFilter franchisee = adminInfoMgr.addFranchiseLocationForFilter(driver, false);

			fc.utobj().printTestStep("Navigate to Franchise To Owner Report");
			// reports.navigateToFranchiseeToOwnerReport(driver);

			fc.utobj().printTestStep("Fill in the filters");
			fc.utobj().setToDefault(driver, franchiseToOwnerPage.drpFranchiseID);
			fc.utobj().selectValFromMultiSelect(driver, franchiseToOwnerPage.drpFranchiseID,
					franchisee.getFranchiseeID());
			fc.utobj().setToDefault(driver, franchiseToOwnerPage.drpAreaRegion);
			fc.utobj().selectValFromMultiSelect(driver, franchiseToOwnerPage.drpAreaRegion,
					franchisee.getAreaRegionName());
			fc.utobj().setToDefault(driver, franchiseToOwnerPage.drpCountry);
			fc.utobj().selectValFromMultiSelect(driver, franchiseToOwnerPage.drpCountry, franchisee.getCountry());
			fc.utobj().setToDefault(driver, franchiseToOwnerPage.drpDivision);
			fc.utobj().selectValFromMultiSelect(driver, franchiseToOwnerPage.drpDivision, franchisee.getDivisionID());
			fc.utobj().setToDefault(driver, franchiseToOwnerPage.drpState);
			fc.utobj().selectValFromMultiSelect(driver, franchiseToOwnerPage.drpState, franchisee.getState());
			fc.utobj().clickElement(driver, franchiseToOwnerPage.btnViewReport);

			boolean isFranchiseDisplayed = fc.utobj().assertPageSource(driver, franchisee.getFranchiseeID());
			if (isFranchiseDisplayed) {
				Reporter.log("Franchisee to Owner Report filter is working properly");
			} else {
				fc.utobj().throwsException("Franchisee to Owner Report filter is not working properly.");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr_test23" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_InfoMgr_Reports_Owner_Details_Report_Filter", testCaseDescription = "This test case will verify Owner Details Report with all the filters", reference = {
			"" })
	public void InfoMgr_Reports_Owner_Details_Report_Filter() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		// ReportsCommonMethods reports = fc.infomgr().reports();
		AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();
		InfoMgrReportsOwnerDetailsReportPage ownerDetailsReportPage = new InfoMgrReportsOwnerDetailsReportPage(driver);

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Add franchise location");
			InfoMgrFranchiseeFilter franchisee = adminInfoMgr.addFranchiseLocationForFilter(driver, false);

			fc.utobj().printTestStep("Navigate to Owner Details Report Page");
			// reports.navigateToOwnerDetailsReport(driver);

			fc.utobj().printTestStep("Fill in the filters");
			fc.utobj().setToDefault(driver, ownerDetailsReportPage.drpFranchiseId);
			fc.utobj().selectValFromMultiSelect(driver, ownerDetailsReportPage.drpFranchiseId,
					franchisee.getFranchiseeID());
			fc.utobj().setToDefault(driver, ownerDetailsReportPage.drpOwnerName);
			fc.utobj().selectValFromMultiSelect(driver, ownerDetailsReportPage.drpOwnerName,
					franchisee.getAreaRegionName());
			fc.utobj().setToDefault(driver, ownerDetailsReportPage.drpCountry);
			fc.utobj().selectValFromMultiSelect(driver, ownerDetailsReportPage.drpCountry, franchisee.getCountry());
			fc.utobj().setToDefault(driver, ownerDetailsReportPage.drpDivision);
			fc.utobj().selectValFromMultiSelect(driver, ownerDetailsReportPage.drpDivision, franchisee.getDivisionID());
			fc.utobj().setToDefault(driver, ownerDetailsReportPage.drpState);
			fc.utobj().selectValFromMultiSelect(driver, ownerDetailsReportPage.drpState, franchisee.getState());
			fc.utobj().clickElement(driver, ownerDetailsReportPage.btnViewReport);

			boolean isFranchiseDisplayed = fc.utobj().assertPageSource(driver, franchisee.getFranchiseeID());
			if (isFranchiseDisplayed) {
				Reporter.log("Owner Details Report filter is working properly");
			} else {
				fc.utobj().throwsException("Owner Details Report filter is not working properly.");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "pending" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_InfoMgr_Reports_MultiUnit_Report_Filter", testCaseDescription = "This test case will verify multi unit report with all the filters", reference = {
			"" })
	public void InfoMgr_Reports_MultiUnit_Report_Filter() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		MultiUnitEntityCommonMethods multiUnit = new MultiUnitEntityCommonMethods();
		// ReportsCommonMethods reports = fc.infomgr().reports();
		AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();
		InfoMgrReportsMultiUnitReportsPage multiUnitReportPage = new InfoMgrReportsMultiUnitReportsPage(driver);

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Add multi unit");
			String multiUnitOwnerName = multiUnit.createMultiUnit(driver, config);

			fc.utobj().printTestStep("Navigate to multi unit report Page");
			// reports.navigateToMultiUnitReport(driver);

			fc.utobj().printTestStep("Fill in the filters");

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "pending" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_InfoMgr_Reports_EntityDetails_Report_Filter", testCaseDescription = "This test case will verify entity details report with all the filters", reference = {
			"" })
	public void InfoMgr_Reports_EntityDetails_Report_Filter() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		// ReportsCommonMethods reports = fc.infomgr().reports();
		AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();
		InfoMgrReportsEntityDetailsReportPage entityDetailsReportsPage = new InfoMgrReportsEntityDetailsReportPage(
				driver);

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Add Franchise Location");
			String franchiseID1 = adminInfoMgr.addFranchiseLocation(driver);

			fc.utobj().printTestStep("Navigate to Entity details report Page");
			// reports.navigateToEntityReport(driver);

			fc.utobj().printTestStep("Fill in the filters");

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

}
