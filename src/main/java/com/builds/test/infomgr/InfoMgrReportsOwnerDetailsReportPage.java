package com.builds.test.infomgr;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrReportsOwnerDetailsReportPage {

	public InfoMgrReportsOwnerDetailsReportPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "ms-parentfranchiseeCombo")
	public WebElement drpFranchiseId;

	@FindBy(id = "ms-parentownerId")
	public WebElement drpOwnerName;

	@FindBy(id = "ms-parentcountry")
	public WebElement drpCountry;

	@FindBy(id = "ms-parentstate")
	public WebElement drpState;

	@FindBy(id = "ms-parentcomboDivision")
	public WebElement drpDivision;

	@FindBy(id = "button")
	public WebElement btnViewReport;

}
