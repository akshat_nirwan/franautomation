package com.builds.test.infomgr;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.test.support.SupportReportsPageTest;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class InfoMgrReportsPageTest extends FranconnectUtil {

	FranconnectUtil fc = new FranconnectUtil();

	SupportReportsPageTest rpPageTest = new SupportReportsPageTest();

	@Test(groups = { "infoMgrReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click Over Center Address Report", testCaseId = "TC_InfoMgr_Report_01")
	private void openerReports01() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("infomgr",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Info Mgr > Reports Page > Center Address Report");
			fc.infomgr().infomgr_common().InfoMgrReports(driver);
			rpPageTest.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'Center Address Report')]"));
			rpPageTest.setSelectAllMultiDropDown(driver);
			rpPageTest.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infoMgrReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click Over Center Summary Report", testCaseId = "TC_InfoMgr_Report_02")
	private void openerReports02() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("infomgr",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Info Mgr > Reports Page > Center Summary Report");
			fc.infomgr().infomgr_common().InfoMgrReports(driver);
			rpPageTest.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'Center Summary Report')]"));
			rpPageTest.setSelectAllMultiDropDown(driver);
			rpPageTest.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infoMgrReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click Over Franchisee-To-Owner Report", testCaseId = "TC_InfoMgr_Report_03")
	private void openerReports03() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("infomgr",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Info Mgr > Reports Page > Franchisee-To-Owner Report");
			fc.infomgr().infomgr_common().InfoMgrReports(driver);
			rpPageTest.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'Franchisee-To-Owner Report')]"));
			rpPageTest.setSelectAllMultiDropDown(driver);
			rpPageTest.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infoMgrReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click Over Owner Details Report", testCaseId = "TC_InfoMgr_Report_04")
	private void openerReports04() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("infomgr",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Info Mgr > Reports Page > Owner Details Report");
			fc.infomgr().infomgr_common().InfoMgrReports(driver);
			rpPageTest.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'Owner Details Report')]"));
			rpPageTest.setSelectAllMultiDropDown(driver);
			rpPageTest.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infoMgrReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click Over Multi-Unit Report", testCaseId = "TC_InfoMgr_Report_05")
	private void openerReports05() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("infomgr",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Info Mgr > Reports Page > Multi-Unit Report");
			fc.infomgr().infomgr_common().InfoMgrReports(driver);
			rpPageTest.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'Multi-Unit Report')]"));
			rpPageTest.setSelectAllMultiDropDown(driver);
			rpPageTest.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infoMgrReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click Over Entity Details Report", testCaseId = "TC_InfoMgr_Report_06")
	private void openerReports06() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("infomgr",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Info Mgr > Reports Page > Entity Details Report");
			fc.infomgr().infomgr_common().InfoMgrReports(driver);
			rpPageTest.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'Entity Details Report')]"));
			rpPageTest.setSelectAllMultiDropDown(driver);
			rpPageTest.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infoMgrReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click Over Birthday Report", testCaseId = "TC_InfoMgr_Report_07")
	private void openerReports07() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("infomgr",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Info Mgr > Reports Page > Birthday Report");
			fc.infomgr().infomgr_common().InfoMgrReports(driver);
			rpPageTest.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'Birthday Report')]"));
			rpPageTest.setSelectAllMultiDropDown(driver);
			rpPageTest.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infoMgrReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click Over Email Status Report", testCaseId = "TC_InfoMgr_Report_08")
	private void openerReports08() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("infomgr",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Info Mgr > Reports Page > Email Status Report");
			fc.infomgr().infomgr_common().InfoMgrReports(driver);
			rpPageTest.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'Email Status Report')]"));
			rpPageTest.setSelectAllMultiDropDown(driver);
			rpPageTest.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infoMgrReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click Over Contact History-Call Report", testCaseId = "TC_InfoMgr_Report_09")
	private void openerReports09() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("infomgr",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Info Mgr > Reports Page > Contact History-Call Report");
			fc.infomgr().infomgr_common().InfoMgrReports(driver);
			rpPageTest.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'Contact History-Call Report')]"));
			rpPageTest.setSelectAllMultiDropDown(driver);
			rpPageTest.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infoMgrReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click Over Contact History-Task Report", testCaseId = "TC_InfoMgr_Report_10")
	private void openerReports10() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("infomgr",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Info Mgr > Reports Page > Contact History-Task Report");
			fc.infomgr().infomgr_common().InfoMgrReports(driver);
			rpPageTest.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'Contact History-Task Report')]"));
			rpPageTest.setSelectAllMultiDropDown(driver);
			rpPageTest.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infoMgrReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click Over Customer Complaints Report", testCaseId = "TC_InfoMgr_Report_11")
	private void openerReports11() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("infomgr",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Info Mgr > Reports Page > Customer Complaints Report");
			fc.infomgr().infomgr_common().InfoMgrReports(driver);
			rpPageTest.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'Customer Complaints Report')]"));
			rpPageTest.setSelectAllMultiDropDown(driver);
			rpPageTest.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infoMgrReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click Over Legal Violation Report", testCaseId = "TC_InfoMgr_Report_12")
	private void openerReports12() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("infomgr",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Info Mgr > Reports Page > Legal Violation Report");
			fc.infomgr().infomgr_common().InfoMgrReports(driver);
			rpPageTest.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'Legal Violation Report')]"));
			rpPageTest.setSelectAllMultiDropDown(driver);
			rpPageTest.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infoMgrReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click Over Legal Report", testCaseId = "TC_InfoMgr_Report_13")
	private void openerReports13() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("infomgr",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Info Mgr > Reports Page > Legal Report");
			fc.infomgr().infomgr_common().InfoMgrReports(driver);
			rpPageTest.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'Legal Report')]"));
			rpPageTest.setSelectAllMultiDropDown(driver);
			rpPageTest.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infoMgrReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click Over Renewal Status Report", testCaseId = "TC_InfoMgr_Report_14")
	private void openerReports14() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("infomgr",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Info Mgr > Reports Page > Renewal Status Report");
			fc.infomgr().infomgr_common().InfoMgrReports(driver);
			rpPageTest.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'Renewal Status Report')]"));
			rpPageTest.setSelectAllMultiDropDown(driver);
			rpPageTest.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infoMgrReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click Over Renewal Dues Report", testCaseId = "TC_InfoMgr_Report_15")
	private void openerReports15() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("infomgr",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Info Mgr > Reports Page > Renewal Dues Report");
			fc.infomgr().infomgr_common().InfoMgrReports(driver);
			rpPageTest.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'Renewal Dues Report')]"));
			rpPageTest.setSelectAllMultiDropDown(driver);
			rpPageTest.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infoMgrReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click Over Transfer Report", testCaseId = "TC_InfoMgr_Report_16")
	private void openerReports16() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("infomgr",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Info Mgr > Reports Page > Transfer Report");
			fc.infomgr().infomgr_common().InfoMgrReports(driver);
			rpPageTest.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'Transfer Report')]"));
			rpPageTest.setSelectAllMultiDropDown(driver);
			rpPageTest.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infoMgrReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click Over Default and Termination Report", testCaseId = "TC_InfoMgr_Report_17")
	private void openerReports17() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("infomgr",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Info Mgr > Reports Page > Default and Termination Report");
			fc.infomgr().infomgr_common().InfoMgrReports(driver);
			rpPageTest.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'Default and Termination Report')]"));
			rpPageTest.setSelectAllMultiDropDown(driver);
			rpPageTest.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infoMgrReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click Over Franchise / Store Status Report", testCaseId = "TC_InfoMgr_Report_18")
	private void openerReports18() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("infomgr",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Info Mgr > Reports Page > Franchise / Store Status Report");
			fc.infomgr().infomgr_common().InfoMgrReports(driver);
			rpPageTest.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'Franchise / Store Status Report')]"));
			rpPageTest.setSelectAllMultiDropDown(driver);
			rpPageTest.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infoMgrReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click Over FDD Store / Franchise List", testCaseId = "TC_InfoMgr_Report_19")
	private void openerReports19() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("infomgr",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Info Mgr > Reports Page > FDD Store / Franchise List");
			fc.infomgr().infomgr_common().InfoMgrReports(driver);
			rpPageTest.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'FDD Store / Franchise List')]"));
			rpPageTest.setSelectAllMultiDropDown(driver);
			rpPageTest.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infoMgrReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click Over FDD Item 20 Report", testCaseId = "TC_InfoMgr_Report_20")
	private void openerReports20() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("infomgr",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Info Mgr > Reports Page > FDD Item 20 Report");
			fc.infomgr().infomgr_common().InfoMgrReports(driver);
			rpPageTest.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'FDD Item 20 Report')]"));
			rpPageTest.setSelectAllMultiDropDown(driver);
			rpPageTest.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infoMgrReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click Over Number of Emails Sent Report", testCaseId = "TC_InfoMgr_Report_21")
	private void openerReports21() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("infomgr",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Info Mgr > Reports Page > Number of Emails Sent Report");
			fc.infomgr().infomgr_common().InfoMgrReports(driver);
			rpPageTest.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'Number of Emails Sent Report')]"));
			rpPageTest.setSelectAllMultiDropDown(driver);
			rpPageTest.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infoMgrReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click Over Click Through Rate - Number of Clickthroughs / Number of emails opened Report", testCaseId = "TC_InfoMgr_Report_21")
	private void openerReports22() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("infomgr",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep(
					"Navigate To Info Mgr > Reports Page > Click Through Rate - Number of Clickthroughs / Number of emails opened Report");
			fc.infomgr().infomgr_common().InfoMgrReports(driver);
			rpPageTest.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'Click Through Rate - Number of Clickthroughs / Number of emails opened Report')]"));
			rpPageTest.setSelectAllMultiDropDown(driver);
			rpPageTest.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infoMgrReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click Over  New Report", testCaseId = "TC_InfoMgr_Report_23")
	private void openerReports23() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("infomgr",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Info Mgr > Reports Page >  New Report");
			fc.infomgr().infomgr_common().InfoMgrReports(driver);
			rpPageTest.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,' New Report')]"));
			rpPageTest.setSelectAllMultiDropDown(driver);
			rpPageTest.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
}
