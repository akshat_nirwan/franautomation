package com.builds.test.infomgr;

import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.builds.uimaps.infomgr.InfoMgrReportsPage;
import com.builds.uimaps.infomgr.InfoMgrReportsTransferReportPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class InfoMgrReportsTransferReportPageTest {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_InfoMgr_Reports_TransferReport", testCaseDescription = "This test case verifies transfer report page", reference = {
			"" })
	public void VerifyTransferReportsPage() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Verify that transfer reports page lods properly");
			fc.infomgr().infomgr_common().InfoMgrReports(driver);
			InfoMgrReportsPage objReportsPage = new InfoMgrReportsPage(driver);
			fc.utobj().clickElement(driver, objReportsPage.lnkTransferReport);
			InfoMgrReportsTransferReportPage transferReportPage = new InfoMgrReportsTransferReportPage(driver);

			if (transferReportPage.tblRecords.isDisplayed()) {
				Reporter.log("Page Loads successfully and there are no errors. Test case passes !!!");
			} else {
				fc.utobj().throwsException("Page didn't load properly. Test case failes !!!");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

}
