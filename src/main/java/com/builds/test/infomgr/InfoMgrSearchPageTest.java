package com.builds.test.infomgr;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.builds.uimaps.infomgr.InfoMgrFranchiseesPage;
import com.builds.uimaps.infomgr.InfoMgrOwnersPage;
import com.builds.uimaps.infomgr.InfoMgrSearchPage;
import com.builds.uimaps.infomgr.InfoMgrSearchResultPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class InfoMgrSearchPageTest {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "infomgr", "InfoMgr3_1" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_InfoMgr_Search_Func_verify", testCaseDescription = "This test case will verify info mgr search functionality", reference = {
			"66603" })
	// Bug ID = 66603
	public void VerifyFranchiseesByDivisionalUser() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Add store type");
			String storeType = adminInfoMgr.addStoreType(driver);

			fc.utobj().printTestStep("Add area region");
			String regionName = adminInfoMgr.addAreaRegion(driver);

			fc.utobj().printTestStep("Add division");
			String divisionName = adminInfoMgr.addDivisionName(driver);

			// Add divisional user
			fc.utobj().printTestStep("Add divisional user");
			String divisionalUserName = "DivisionalUser001";
			String divisionalUserPassword = "t0n1ght123";
			divisionalUserName = adminInfoMgr.addDivisionalUser(driver, divisionalUserName, divisionalUserPassword,
					divisionName);

			// logout
			fc.home_page().logout(driver);

			// login
			fc.utobj().printTestStep("Login with divisional user");
			fc.loginpage().loginWithParameter(driver, divisionalUserName, divisionalUserPassword);

			fc.utobj().printTestStep("Navigate to search and verify that there is no franchise location");

			fc.infomgr().infomgr_common().InfoMgrSearch(driver);

			InfoMgrSearchPage objSearchPage = new InfoMgrSearchPage(driver);

			// check 'Select All' in Franchisee Type dropdown
			WebElement drpdownElement = objSearchPage.drpFranchiseeType;
			fc.utobj().clickElement(driver, drpdownElement);
			WebElement selectAll = drpdownElement.findElement(By.id("selectAll"));
			selectAll.click();
			fc.utobj().clickElement(driver, objSearchPage.btnSearch);

			// Verify that there are no franchisees for this divisional user as
			// this is new user
			InfoMgrSearchResultPage searchResultPage = objSearchPage.getSearchResultPage();
			String franchiseesTableData = fc.utobj().getText(driver, searchResultPage.tblFranchisees);

			if (franchiseesTableData.contains(fc.utobj().translateString("No records found"))) {
				Reporter.log("There are no franchisees for this divisional user , so no record message is present !!!");
			} else {
				fc.utobj().throwsException(
						"Franchisees not related to this divisional user is displayed on the page !!!");
			}

			fc.utobj().printTestStep("Add franchise location");
			String franchiseeID = adminInfoMgr.addFranchiseLocationByDivisionalUser(driver, divisionName, regionName,
					storeType);

			fc.utobj().printTestStep("Navigate to search and verify that location is displayed.");
			fc.infomgr().infomgr_common().InfoMgrSearch(driver);

			// check 'Select All' in Franchisee Type dropdown
			fc.utobj().clickElement(driver, drpdownElement);
			selectAll = drpdownElement.findElement(By.id("selectAll"));
			selectAll.click();
			fc.utobj().clickElement(driver, objSearchPage.btnSearch);
			Thread.sleep(2000);

			// Verify that there franchisees added for this divisional user is
			// displayed on the page
			List<WebElement> lstRows = searchResultPage.tblFranchiseesRows;
			boolean isFranchiseeLink = fc.utobj().assertLinkText(driver, franchiseeID);
			Reporter.log(lstRows.size() + "");

			if (isFranchiseeLink) {
				Reporter.log("New location created is displayed on the page.");
				if (lstRows.size() == 2) {
					Reporter.log(
							"Only Franchisees Location corressponding to Divisional users is displayed on the page . !!!");
				} else {
					fc.utobj().throwsException(
							"Location not corressponding to divisional user is also displayed on the page . Test failes !!!");
				}
			} else {
				fc.utobj()
						.throwsException("Location for divisional user is not displayed on the page . Test failes !!!");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}

	@Test(groups = { "infomgr" , "IMFailedTC","Filter" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-16", testCaseId = "TC_Verify_InfoMgr_Search_Filter", testCaseDescription = "Test to verify filter displayed on the search Page", reference = {
			"" })
	public void InfoMgr_Search_Filter_Verify() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		FranchiseesCommonMethods franchisees = fc.infomgr().franchisees();
		SearchCommonMethods searchPage = new SearchCommonMethods();
		AdminInfoMgrCommonMethods adminInfoMgr = new AdminInfoMgrCommonMethods();

		try {
			fc.loginpage().login(driver);

			InfoMgrFranchiseeFilter franchiseeFilter = adminInfoMgr.addFranchiseLocationForFilter(driver, false);

			fc.utobj().printTestStep("Adding spouse details by modifying the owner");
			franchisees.franchiseesPageShowAll(driver);
			fc.utobj().actionImgOption(driver, franchiseeFilter.getFranchiseeID(), "Modify");
			InfoMgrOwnersPage ownerPage = new InfoMgrFranchiseesPage(driver).getOwnersPage();
			fc.utobj().clickElement(driver, ownerPage.ownersTab);
			fc.utobj().actionImgOption(driver, franchiseeFilter.getOwnerFirstName(), "Modify");
			Map<String, String> lstOwner = franchisees.modifyOwner(driver, config);
			franchiseeFilter.setSpouseFirstName(lstOwner.get("SpouseFirstName"));
			franchiseeFilter.setSpouseLastName(lstOwner.get("SpouseLastName"));

			fc.utobj().printTestStep("Verify Search Filter");
			fc.infomgr().infomgr_common().InfoMgrSearch(driver);
			searchPage.search(driver, config, franchiseeFilter);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

}
