package com.builds.test.infomgr;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.builds.uimaps.fs.AdminFranchiseSalesManageFormGeneratorPage;
import com.builds.uimaps.infomgr.InfoMgrCenterInfoPage;
import com.builds.uimaps.infomgr.InfoMgrFranchiseesPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class InfoMgrSummaryCustomSort {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "infomgr", "IMFailedTC","InfoMgr11" })

	@TestCase(createdOn = "2017-10-25", updatedOn = "2017-10-25", testCaseId = "TC_Sorting_Textfield_Summary", testCaseDescription = "To Verify the Sorting for Text Box field at Info Mgr Summary Pages.", reference = {
			"" })
	public void InfoMgr_SortingTextfieldSummary() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);
			AdminInfoMgrManageFormGeneratorPageTest p1 = new AdminInfoMgrManageFormGeneratorPageTest();
			String fieldName = "Sort";
			boolean isfieldNamePresent = false;

			fc.utobj().printTestStep("Go to Admin > Info Mgr > Manage Form Generator");
			fc.infomgr().infomgr_common().AdminFIMManageFormGenerator(driver);
			fc.utobj().clickElement(driver, pobj.continueBtn);
			fc.utobj().sleep(2000);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, "//*[@id='tabdetails']//*[text()='Center Info']"));
			fc.utobj().clickElement(driver, pobj.PrimaryInfoAddNewFied);
			fieldName = p1.addFieldNamePrimaryInfo(driver, "Center Details", fieldName);

			boolean isFieldNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + fieldName + "')]");
			if (isFieldNamePresent == false) {
				fc.utobj().throwsException("was not able to verify added fieldName in Manage Form Generator");
			}

			fc.utobj().printTestStep("Go to Admin > Configure Summary Display Columns");
			fc.infomgr().infomgr_common().adminpage().adminPage(driver);
			fc.utobj().clickElement(driver, pobj.configureSummaryDisplayColumns);
			fc.utobj().sleep(2000);
			fc.utobj().actionImgOption(driver, "In Development, Franchisees, Terminated, Corporate Locations",
					"Configure");
			Thread.sleep(1000);

			isfieldNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//select[@name='duplicateCriteriaLeft']/option[contains(text(),'" + fieldName + "')]");
			if (isfieldNamePresent == false) {
				fc.utobj().throwsException("Field name not present in available columns dropdown");
			}

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,(".//select[@name='duplicateCriteriaLeft']/option[contains(text(),'" + fieldName + "')]")));
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@class='cm_new_button' and @value='>>']"));
			Thread.sleep(1000);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@class='cm_new_button' and @value='Submit']"));
			Thread.sleep(1000);
			isfieldNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//select[@name='duplicateCriteriaRight']/option[contains(text(),'" + fieldName + "')]");
			if (isfieldNamePresent == false) {
				fc.utobj().throwsException("Field name not present in Configured Columns dropdown");
			}

			fc.utobj().printTestStep("Verify the field Name to InfoMgr Franchisees summary.");
			fc.infomgr().infomgr_common().InfoMgrFranchisees(driver);

			isfieldNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[@class='headText12b'  and text()='" + fieldName + "']");

			if (isfieldNamePresent == false) {
				fc.utobj().throwsException("Field are not present in Info Mgr >  Franchisees");
			}

			fc.utobj().printTestStep("Verify the field Name to Sortable for blank records.");
			fc.infomgr().infomgr_common().InfoMgrFranchisees(driver);
			fc.utobj().sleep(2000);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[@class='headText12b'  and text()='" + fieldName + "']"));

			String firstEle = "";
			String secondEle = "";
			boolean isSorttable = false;

			List<WebElement> elementList = driver.findElements(By.xpath(".//*[@qat_maintable='withoutheader']/td[2]"));
			try {
				if (elementList != null) {
					for (int i = 0; i < elementList.size(); i++) {
						if (elementList.get(0) != null) {
							isSorttable = true;
							firstEle = (String) elementList.get(0).getText();
							break;
						}
					}
				}

			} catch (Exception e) {
				isSorttable = false;

			}
			fc.utobj().sleep(2000);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[@class='headText12b'  and text()='" + fieldName + "']"));

			elementList = driver.findElements(By.xpath(".//*[@qat_maintable='withoutheader']/td[2]"));
			try {
				if (elementList != null) {
					for (int i = 0; i < elementList.size(); i++) {
						if (elementList.get(0) != null) {
							isSorttable = true;
							secondEle = (String) elementList.get(0).getText();
							break;
						}
					}
				}

			} catch (Exception e) {
				isSorttable = false;

			}

			// System.out.println("firstEle=="+firstEle+"====="+secondEle);

			if (firstEle.equals(secondEle)) {
				fc.utobj().printTestStep("field Name to Sortable Passesd!");
			} else {
				fc.utobj().throwsException("Field are not Sortable in Info Mgr >  Franchisees accordingly.");
			}

			FranchiseesCommonMethods franchisees = fc.infomgr().franchisees();
			AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();
			InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();

			fc.utobj().printTestStep("Add First Location. ");
			String franchiseID = adminInfoMgr.addFranchiseLocation(driver);
			//String franchiseID ="001A";
			franchisees.franchiseesPageShowAll(driver);

			fc.utobj().printTestStep("Modify from action menu");
			fc.utobj().actionImgOption(driver, franchiseID, "Modify");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver,
					"//input[contains(@name, '_" + fieldName.toLowerCase() + "')]"), "AAA");
			fc.utobj().clickElement(driver, centerInfoPage.btnSumbit);

			fc.utobj().printTestStep("Add Second Location. ");
			String franchiseIDSecond = adminInfoMgr.addFranchiseLocation(driver);
			//String franchiseIDSecond ="001C";
			franchisees.franchiseesPageShowAll(driver);

			fc.utobj().printTestStep("Modify from action menu");
			fc.utobj().actionImgOption(driver, franchiseIDSecond, "Modify");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver,
					"//input[contains(@name, '_" + fieldName.toLowerCase() + "')]"), "BBB");
			fc.utobj().clickElement(driver, centerInfoPage.btnSumbit);

			fc.utobj().printTestStep("Verify the field Name to sorting for custom TextBox.");
			fc.infomgr().infomgr_common().InfoMgrFranchisees(driver);
			fc.utobj().sleep(2000);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[@class='headText12b'  and text()='" + fieldName + "']"));
			fc.utobj().sleep(2000);
			firstEle = "";
			secondEle = "";
			isSorttable = false;

			elementList = driver.findElements(By.xpath(".//*[@qat_maintable='withoutheader']/td[2]"));
			try {
				if (elementList != null) {
					for (int i = 0; i < elementList.size(); i++) {
						if (elementList.get(0) != null) {
							isSorttable = true;
							firstEle = (String) elementList.get(0).getText();
							break;
						}
					}
				}

			} catch (Exception e) {
				isSorttable = false;

			}
			fc.utobj().sleep(2000);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[@class='headText12b'  and text()='" + fieldName + "']"));
			fc.utobj().sleep(2000);
			elementList = driver.findElements(By.xpath(".//*[@qat_maintable='withoutheader']/td[2]"));
			try {
				if (elementList != null) {
					for (int i = 0; i < elementList.size(); i++) {
						if (elementList.get(0) != null) {
							isSorttable = true;
							secondEle = (String) elementList.get(0).getText();
							break;
						}
					}
				}

			} catch (Exception e) {
				isSorttable = false;

			}

			// System.out.println("firstEle=="+firstEle+"====="+secondEle);
			boolean isSortingWork = false;
			elementList = driver.findElements(By.xpath(
					".//*[@qat_maintable='withoutheader']/.//td[contains(text(),'AAA') or contains(text(),'BBB')]"));
			try {
				if (elementList != null) {
					for (int i = 0; i < elementList.size(); i++) {
						if (elementList.get(i) != null) {
							if ("BBB".equals((String) elementList.get(0).getText())) {
								isSortingWork = true;
								break;
							}

						}
					}
				}

			} catch (Exception e) {

			}

			if (isSortingWork == false) {
				fc.utobj().throwsException("Field are not Sortable in Info Mgr >  Franchisees accordingly.");
			} else {
				fc.utobj().printTestStep("field Name to Sortable Passesd!");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}

	@Test(groups = { "infomgr", "IMFailedTC" })

	@TestCase(createdOn = "2017-10-25", updatedOn = "2017-10-25", testCaseId = "TC_Sorting_Phone_Summary", testCaseDescription = "To Verify the Sorting for custom phone field at Info Mgr Summary Pages.", reference = {
			"" })
	public void InfoMgr_SortingPhoneSummary() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);
			AdminInfoMgrManageFormGeneratorPageTest p1 = new AdminInfoMgrManageFormGeneratorPageTest();
			String fieldName = "Phone";
			boolean isfieldNamePresent = false;

			fc.utobj().printTestStep("Go to Admin > Info Mgr > Manage Form Generator");
			fc.infomgr().infomgr_common().AdminFIMManageFormGenerator(driver);
			fc.utobj().clickElement(driver, pobj.continueBtn);
			fc.utobj().sleep(2000);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, "//*[@id='tabdetails']//*[text()='Center Info']"));
			fc.utobj().clickElement(driver, pobj.PrimaryInfoAddNewFied);
			fieldName = addFieldNamePrimaryInfoPhone(driver, "Center Details", fieldName);

			boolean isFieldNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + fieldName + "')]");
			if (isFieldNamePresent == false) {
				fc.utobj().throwsException("was not able to verify added phone field in Manage Form Generator");
			}

			fc.utobj().printTestStep("Go to Admin > Configure Summary Display Columns");
			fc.infomgr().infomgr_common().adminpage().adminPage(driver);
			fc.utobj().clickElement(driver, pobj.configureSummaryDisplayColumns);
			fc.utobj().sleep(2000);
			fc.utobj().actionImgOption(driver, "In Development, Franchisees, Terminated, Corporate Locations",
					"Configure");
			Thread.sleep(1000);

			isfieldNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//select[@name='duplicateCriteriaLeft']/option[contains(text(),'" + fieldName + "')]");
			if (isfieldNamePresent == false) {
				fc.utobj().throwsException("phone field name not present in available columns dropdown");
			}

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,(".//select[@name='duplicateCriteriaLeft']/option[contains(text(),'" + fieldName + "')]")));
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@class='cm_new_button' and @value='>>']"));
			Thread.sleep(1000);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@class='cm_new_button' and @value='Submit']"));
			Thread.sleep(1000);
			isfieldNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//select[@name='duplicateCriteriaRight']/option[contains(text(),'" + fieldName + "')]");
			if (isfieldNamePresent == false) {
				fc.utobj().throwsException("phone field name not present in Configured Columns dropdown");
			}

			fc.utobj().printTestStep("Verify the field Name to InfoMgr Franchisees summary.");
			fc.infomgr().infomgr_common().InfoMgrFranchisees(driver);

			isfieldNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[@class='headText12b'  and text()='" + fieldName + "']");

			if (isfieldNamePresent == false) {
				fc.utobj().throwsException("phone field are not present in Info Mgr >  Franchisees");
			}

			fc.utobj().printTestStep("Verify the field Name to Sortable for blank records.");
			fc.infomgr().infomgr_common().InfoMgrFranchisees(driver);
			Thread.sleep(3000);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[@class='headText12b'  and text()='" + fieldName + "']"));
			fc.utobj().sleep(2000);
			String firstEle = "";
			String secondEle = "";
			boolean isSorttable = false;

			List<WebElement> elementList = driver.findElements(By.xpath(".//*[@qat_maintable='withoutheader']/td[2]"));
			try {
				if (elementList != null) {
					for (int i = 0; i < elementList.size(); i++) {
						if (elementList.get(0) != null) {
							isSorttable = true;
							firstEle = (String) elementList.get(0).getText();
							break;
						}
					}
				}

			} catch (Exception e) {
				isSorttable = false;

			}
			fc.utobj().sleep(2000);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[@class='headText12b'  and text()='" + fieldName + "']"));
			fc.utobj().sleep(2000);
			elementList = driver.findElements(By.xpath(".//*[@qat_maintable='withoutheader']/td[2]"));
			try {
				if (elementList != null) {
					for (int i = 0; i < elementList.size(); i++) {
						if (elementList.get(0) != null) {
							isSorttable = true;
							secondEle = (String) elementList.get(0).getText();
							break;
						}
					}
				}

			} catch (Exception e) {
				isSorttable = false;

			}

			// System.out.println("firstEle=="+firstEle+"====="+secondEle);

			if (firstEle.equals(secondEle)) {
				fc.utobj().printTestStep("phone field Name to Sortable Passesd!");
			} else {
				fc.utobj().throwsException("phone field are not Sortable in Info Mgr >  Franchisees accordingly.");
			}

			FranchiseesCommonMethods franchisees = fc.infomgr().franchisees();
			AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();
			InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();

			fc.utobj().printTestStep("Add First Location. ");
			String franchiseID = adminInfoMgr.addFranchiseLocation(driver);
			franchisees.franchiseesPageShowAll(driver);

			fc.utobj().printTestStep("Modify from action menu");
			fc.utobj().actionImgOption(driver, franchiseID, "Modify");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver,
					"//input[contains(@name, '_" + fieldName.toLowerCase() + "')]"), "1111111111");
			fc.utobj().clickElement(driver, centerInfoPage.btnSumbit);

			fc.utobj().printTestStep("Add Second Location. ");
			String franchiseIDSecond = adminInfoMgr.addFranchiseLocation(driver);
			franchisees.franchiseesPageShowAll(driver);

			fc.utobj().printTestStep("Modify from action menu");
			fc.utobj().actionImgOption(driver, franchiseIDSecond, "Modify");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver,
					"//input[contains(@name, '_" + fieldName.toLowerCase() + "')]"), "2222222222");
			fc.utobj().clickElement(driver, centerInfoPage.btnSumbit);

			fc.utobj().printTestStep("Verify the field Name to sorting for custom TextBox.");
			fc.infomgr().infomgr_common().InfoMgrFranchisees(driver);
			fc.utobj().sleep(2000);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[@class='headText12b'  and text()='" + fieldName + "']"));
			firstEle = "";
			secondEle = "";
			isSorttable = false;

			elementList = driver.findElements(By.xpath(".//*[@qat_maintable='withoutheader']/td[2]"));
			try {
				if (elementList != null) {
					for (int i = 0; i < elementList.size(); i++) {
						if (elementList.get(0) != null) {
							isSorttable = true;
							firstEle = (String) elementList.get(0).getText();
							break;
						}
					}
				}

			} catch (Exception e) {
				isSorttable = false;

			}
			fc.utobj().sleep(2000);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[@class='headText12b'  and text()='" + fieldName + "']"));

			elementList = driver.findElements(By.xpath(".//*[@qat_maintable='withoutheader']/td[2]"));
			try {
				if (elementList != null) {
					for (int i = 0; i < elementList.size(); i++) {
						if (elementList.get(0) != null) {
							isSorttable = true;
							secondEle = (String) elementList.get(0).getText();
							break;
						}
					}
				}

			} catch (Exception e) {
				isSorttable = false;

			}

			// System.out.println("firstEle=="+firstEle+"====="+secondEle);
			boolean isSortingWork = false;
			elementList = driver.findElements(By.xpath(
					".//*[@qat_maintable='withoutheader']/.//td[contains(text(),'1111111111') or contains(text(),'2222222222')]"));
			try {
				if (elementList != null) {
					for (int i = 0; i < elementList.size(); i++) {
						if (elementList.get(i) != null) {
							if ("2222222222".equals((String) elementList.get(0).getText())) {
								isSortingWork = true;
								break;
							}

						}
					}
				}

			} catch (Exception e) {

			}

			if (isSortingWork == false) {
				fc.utobj().throwsException("phone field are not Sortable in Info Mgr >  Franchisees accordingly.");
			} else {
				fc.utobj().printTestStep("phone field Name to Sortable Passesd!");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}

	@Test(groups = { "infomgr" , "IMFailedTC","InfoMgr7_1"})

	@TestCase(createdOn = "2017-10-25", updatedOn = "2017-10-25", testCaseId = "TC_Sorting_Email_Summary", testCaseDescription = "To Verify the Sorting for custom email field at Info Mgr Summary Pages.", reference = {
			"" })
	public void InfoMgr_SortingEmailSummary() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);
			String fieldName = "Email";
			boolean isfieldNamePresent = false;

			fc.utobj().printTestStep("Go to Admin > Info Mgr > Manage Form Generator");
			fc.infomgr().infomgr_common().AdminFIMManageFormGenerator(driver);
			fc.utobj().clickElement(driver, pobj.continueBtn);
			fc.utobj().sleep(2000);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, "//*[@id='tabdetails']//*[text()='Center Info']"));
			fc.utobj().clickElement(driver, pobj.PrimaryInfoAddNewFied);
			fieldName = addFieldNamePrimaryInfoEmail(driver, "Center Details", fieldName);

			boolean isFieldNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + fieldName + "')]");
			if (isFieldNamePresent == false) {
				fc.utobj().throwsException("was not able to verify added email field in Manage Form Generator");
			}

			fc.utobj().printTestStep("Go to Admin > Configure Summary Display Columns");
			fc.infomgr().infomgr_common().adminpage().adminPage(driver);
			fc.utobj().clickElement(driver, pobj.configureSummaryDisplayColumns);
			fc.utobj().sleep(2000);
			fc.utobj().actionImgOption(driver, "In Development, Franchisees, Terminated, Corporate Locations",
					"Configure");
			Thread.sleep(1000);

			isfieldNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//select[@name='duplicateCriteriaLeft']/option[contains(text(),'" + fieldName + "')]");
			if (isfieldNamePresent == false) {
				fc.utobj().throwsException("email field name not present in available columns dropdown");
			}

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,(".//select[@name='duplicateCriteriaLeft']/option[contains(text(),'" + fieldName + "')]")));
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@class='cm_new_button' and @value='>>']"));
			Thread.sleep(1000);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@class='cm_new_button' and @value='Submit']"));
			Thread.sleep(1000);
			isfieldNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//select[@name='duplicateCriteriaRight']/option[contains(text(),'" + fieldName + "')]");
			if (isfieldNamePresent == false) {
				fc.utobj().throwsException("email field name not present in Configured Columns dropdown");
			}

			fc.utobj().printTestStep("Verify the field Name to InfoMgr Franchisees summary.");
			fc.infomgr().infomgr_common().InfoMgrFranchisees(driver);

			isfieldNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[@class='headText12b'  and text()='" + fieldName + "']");

			if (isfieldNamePresent == false) {
				fc.utobj().throwsException("email field are not present in Info Mgr >  Franchisees");
			}

			fc.utobj().printTestStep("Verify the email field Name to Sortable for blank records.");
			fc.infomgr().infomgr_common().InfoMgrFranchisees(driver);
			fc.utobj().sleep(2000);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[@class='headText12b'  and text()='" + fieldName + "']"));
			fc.utobj().sleep(2000);
			String firstEle = "";
			String secondEle = "";
			boolean isSorttable = false;

			List<WebElement> elementList = driver.findElements(By.xpath(".//*[@qat_maintable='withoutheader']/td[2]"));
			try {
				if (elementList != null) {
					for (int i = 0; i < elementList.size(); i++) {
						if (elementList.get(0) != null) {
							isSorttable = true;
							firstEle = (String) elementList.get(0).getText();
							break;
						}
					}
				}

			} catch (Exception e) {
				isSorttable = false;

			}
			fc.utobj().sleep(2000);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[@class='headText12b'  and text()='" + fieldName + "']"));
			fc.utobj().sleep(2000);
			elementList = driver.findElements(By.xpath(".//*[@qat_maintable='withoutheader']/td[2]"));
			try {
				if (elementList != null) {
					for (int i = 0; i < elementList.size(); i++) {
						if (elementList.get(0) != null) {
							isSorttable = true;
							secondEle = (String) elementList.get(0).getText();
							break;
						}
					}
				}

			} catch (Exception e) {
				isSorttable = false;

			}

			// System.out.println("firstEle=="+firstEle+"====="+secondEle);

			if (firstEle.equals(secondEle)) {
				fc.utobj().printTestStep("email field Name to Sortable Passesd!");
			} else {
				fc.utobj().throwsException("email field are not Sortable in Info Mgr >  Franchisees accordingly.");
			}

			FranchiseesCommonMethods franchisees = fc.infomgr().franchisees();
			AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();
			InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();

			fc.utobj().printTestStep("Add First Location. ");
			//String franchiseID = adminInfoMgr.addFranchiseLocation(driver);
			String franchiseID = "Default27115857p";  	
			franchisees.franchiseesPageShowAll(driver);

			fc.utobj().printTestStep("Modify from action menu");
			fc.utobj().actionImgOption(driver, franchiseID, "Modify");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver,
					"//input[contains(@name, '_" + fieldName.toLowerCase() + "')]"), "aa@aa.com");
			fc.utobj().clickElement(driver, centerInfoPage.btnSumbit);

			fc.utobj().printTestStep("Add Second Location. ");
			//String franchiseIDSecond = adminInfoMgr.addFranchiseLocation(driver);
			String franchiseIDSecond = "FIMa27111857";
			franchisees.franchiseesPageShowAll(driver);

			fc.utobj().printTestStep("Modify from action menu");
			fc.utobj().actionImgOption(driver, franchiseIDSecond, "Modify");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver,
					"//input[contains(@name, '_" + fieldName.toLowerCase() + "')]"), "bb@bb.com");
			fc.utobj().clickElement(driver, centerInfoPage.btnSumbit);

			fc.utobj().printTestStep("Verify the field Name to sorting for custom TextBox.");
			fc.infomgr().infomgr_common().InfoMgrFranchisees(driver);
			fc.utobj().sleep(2000);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[@class='headText12b'  and text()='" + fieldName + "']"));

			firstEle = "";
			secondEle = "";
			isSorttable = false;

			elementList = driver.findElements(By.xpath(".//*[@qat_maintable='withoutheader']/td[2]"));
			try {
				if (elementList != null) {
					for (int i = 0; i < elementList.size(); i++) {
						if (elementList.get(0) != null) {
							isSorttable = true;
							firstEle = (String) elementList.get(0).getText();
							break;
						}
					}
				}

			} catch (Exception e) {
				isSorttable = false;

			}
			fc.utobj().sleep(2000);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[@class='headText12b'  and text()='" + fieldName + "']"));

			elementList = driver.findElements(By.xpath(".//*[@qat_maintable='withoutheader']/td[2]"));
			try {
				if (elementList != null) {
					for (int i = 0; i < elementList.size(); i++) {
						if (elementList.get(0) != null) {
							isSorttable = true;
							secondEle = (String) elementList.get(0).getText();
							break;
						}
					}
				}

			} catch (Exception e) {
				isSorttable = false;

			}

			// System.out.println("firstEle=="+firstEle+"====="+secondEle);
			boolean isSortingWork = false;
			elementList = driver.findElements(By.xpath(
					".//*[@qat_maintable='withoutheader']/.//a[contains(text(),'aa@aa.com') or contains(text(),'bb@bb.com')]"));
			try {
				if (elementList != null) {
					for (int i = 0; i < elementList.size(); i++) {
						if (elementList.get(i) != null) {
							if ("bb@bb.com".equals((String) elementList.get(0).getText())) {
								isSortingWork = true;
								break;
							}

						}
					}
				}

			} catch (Exception e) {

			}

			if (isSortingWork == false) {
				fc.utobj().throwsException("email field are not Sortable in Info Mgr >  Franchisees accordingly.");
			} else {
				fc.utobj().printTestStep("email field Name to Sortable Passesd!");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}

	@Test(groups = { "infomgr", "IMFailedTC","InfoMgr90" })

	@TestCase(createdOn = "2017-10-27", updatedOn = "2017-10-27", testCaseId = "TC_Sorting_URL_Summary", testCaseDescription = "To Verify the Sorting for custom url field at Info Mgr Summary Pages.", reference = {
			"" })
	public void InfoMgr_SortingURLSummary() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);
			String fieldName = "Url";
			boolean isfieldNamePresent = false;

			fc.utobj().printTestStep("Go to Admin > Info Mgr > Manage Form Generator");
			fc.infomgr().infomgr_common().AdminFIMManageFormGenerator(driver);
			fc.utobj().clickElement(driver, pobj.continueBtn);
			fc.utobj().sleep(2000);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, "//*[@id='tabdetails']//*[text()='Center Info']"));
			fc.utobj().clickElement(driver, pobj.PrimaryInfoAddNewFied);
			fieldName = addFieldNamePrimaryInfoURL(driver, "Center Details", fieldName);

			boolean isFieldNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + fieldName + "')]");
			if (isFieldNamePresent == false) {
				fc.utobj().throwsException("was not able to verify added url field in Manage Form Generator");
			}

			fc.utobj().printTestStep("Go to Admin > Configure Summary Display Columns");
			fc.infomgr().infomgr_common().adminpage().adminPage(driver);
			fc.utobj().clickElement(driver, pobj.configureSummaryDisplayColumns);
			fc.utobj().sleep(2000);
			fc.utobj().actionImgOption(driver, "In Development, Franchisees, Terminated, Corporate Locations",
					"Configure");
			Thread.sleep(1000);

			isfieldNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//select[@name='duplicateCriteriaLeft']/option[contains(text(),'" + fieldName + "')]");
			if (isfieldNamePresent == false) {
				fc.utobj().throwsException("url field name not present in available columns dropdown");
			}

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,(".//select[@name='duplicateCriteriaLeft']/option[contains(text(),'" + fieldName + "')]")));
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@class='cm_new_button' and @value='>>']"));
			Thread.sleep(1000);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@class='cm_new_button' and @value='Submit']"));
			Thread.sleep(1000);
			isfieldNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//select[@name='duplicateCriteriaRight']/option[contains(text(),'" + fieldName + "')]");
			if (isfieldNamePresent == false) {
				fc.utobj().throwsException("url field name not present in Configured Columns dropdown");
			}

			fc.utobj().printTestStep("Verify the field Name to InfoMgr Franchisees summary.");
			fc.infomgr().infomgr_common().InfoMgrFranchisees(driver);

			isfieldNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[@class='headText12b'  and text()='" + fieldName + "']");

			if (isfieldNamePresent == false) {
				fc.utobj().throwsException("url field are not present in Info Mgr >  Franchisees");
			}

			fc.utobj().printTestStep("Verify the url field Name to Sortable for blank records.");
			fc.infomgr().infomgr_common().InfoMgrFranchisees(driver);
			fc.utobj().sleep(2000);	
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[@class='headText12b'  and text()='" + fieldName + "']"));
			Thread.sleep(3000);
			String firstEle = "";
			String secondEle = "";
			boolean isSorttable = false;

			List<WebElement> elementList = driver.findElements(By.xpath(".//*[@qat_maintable='withoutheader']/td[2]"));
			try {
				if (elementList != null) {
					for (int i = 0; i < elementList.size(); i++) {
						if (elementList.get(0) != null) {
							isSorttable = true;
							firstEle = (String) elementList.get(0).getText();
							break;
						}
					}
				}

			} catch (Exception e) {
				isSorttable = false;

			}
			fc.utobj().sleep(2000);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[@class='headText12b'  and text()='" + fieldName + "']"));
			Thread.sleep(3000);
			elementList = driver.findElements(By.xpath(".//*[@qat_maintable='withoutheader']/td[2]"));
			try {
				if (elementList != null) {
					for (int i = 0; i < elementList.size(); i++) {
						if (elementList.get(0) != null) {
							isSorttable = true;
							secondEle = (String) elementList.get(0).getText();
							break;
						}
					}
				}

			} catch (Exception e) {
				isSorttable = false;

			}

			// System.out.println("firstEle=="+firstEle+"====="+secondEle);

			if (firstEle.equals(secondEle)) {
				fc.utobj().printTestStep("url field Name to Sortable Passesd!");
			} else {
				fc.utobj().throwsException("url field are not Sortable in Info Mgr >  Franchisees accordingly.");
			}

			FranchiseesCommonMethods franchisees = fc.infomgr().franchisees();
			AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();
			InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();

			fc.utobj().printTestStep("Add First Location. ");
			String franchiseID = adminInfoMgr.addFranchiseLocation(driver);
			franchisees.franchiseesPageShowAll(driver);

			fc.utobj().printTestStep("Modify from action menu");
			fc.utobj().actionImgOption(driver, franchiseID, "Modify");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver,
					"//input[contains(@name, '_" + fieldName.toLowerCase() + "')]"), "http://aa.com");
			fc.utobj().clickElement(driver, centerInfoPage.btnSumbit);

			fc.utobj().printTestStep("Add Second Location. ");
			String franchiseIDSecond = adminInfoMgr.addFranchiseLocation(driver);
			franchisees.franchiseesPageShowAll(driver);

			fc.utobj().printTestStep("Modify from action menu");
			fc.utobj().actionImgOption(driver, franchiseIDSecond, "Modify");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver,
					"//input[contains(@name, '_" + fieldName.toLowerCase() + "')]"), "http://bb.com");
			fc.utobj().clickElement(driver, centerInfoPage.btnSumbit);

			fc.utobj().printTestStep("Verify the url field Name to sorting for custom TextBox.");
			fc.infomgr().infomgr_common().InfoMgrFranchisees(driver);
			fc.utobj().sleep(2000);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[@class='headText12b'  and text()='" + fieldName + "']"));

			firstEle = "";
			secondEle = "";
			isSorttable = false;

			elementList = driver.findElements(By.xpath(".//*[@qat_maintable='withoutheader']/td[2]"));
			try {
				if (elementList != null) {
					for (int i = 0; i < elementList.size(); i++) {
						if (elementList.get(0) != null) {
							isSorttable = true;
							firstEle = (String) elementList.get(0).getText();
							break;
						}
					}
				}

			} catch (Exception e) {
				isSorttable = false;

			}
			fc.utobj().sleep(2000);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[@class='headText12b'  and text()='" + fieldName + "']"));

			elementList = driver.findElements(By.xpath(".//*[@qat_maintable='withoutheader']/td[2]"));
			try {
				if (elementList != null) {
					for (int i = 0; i < elementList.size(); i++) {
						if (elementList.get(0) != null) {
							isSorttable = true;
							secondEle = (String) elementList.get(0).getText();
							break;
						}
					}
				}

			} catch (Exception e) {
				isSorttable = false;

			}

			// System.out.println("firstEle=="+firstEle+"====="+secondEle);
			boolean isSortingWork = false;
			fc.utobj().sleep();
			elementList = driver.findElements(By.xpath(
					".//*[@qat_maintable='withoutheader']/.//td[contains(text(),'http://aa.com') or contains(text(),'http://bb.com')]"));
			System.out.println(elementList.size());
			try {
				if (elementList != null) {
					for (int i = 0; i < elementList.size(); i++) {
						if (elementList.get(i) != null) {
							if ("http://bb.com".equals((String) elementList.get(0).getText())) {
								isSortingWork = true;
								break;
							}

						}
					}
				}

			} catch (Exception e) {

			}

			if (isSortingWork == false) {
				fc.utobj().throwsException("url field are not Sortable in Info Mgr >  Franchisees accordingly.");
			} else {
				fc.utobj().printTestStep("url field Name to Sortable Passesd!");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}

	public String addFieldNamePrimaryInfoPhone(WebDriver driver, String sectionName, String fieldName)
			throws Exception {

		AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);
		fc.utobj().sleep();
		fieldName = fc.utobj().generateTestData(fieldName);
		String windowHandle = driver.getWindowHandle();
		fc.commonMethods().switch_cboxIframe_frameId(driver);
		fc.utobj().sleep();
		fc.utobj().sendKeys(driver, pobj.fieldDisplayName, fieldName);
		fc.utobj().selectDropDownByVisibleText(driver, pobj.fldValidationType, "Phone");
		fc.utobj().clickElement(driver, pobj.submitFieldCBoxButton);
		driver.switchTo().window(windowHandle);
		return fieldName;

	}

	public String addFieldNamePrimaryInfoEmail(WebDriver driver, String sectionName, String fieldName)
			throws Exception {

		AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);
		fc.utobj().sleep();
		fieldName = fc.utobj().generateTestData(fieldName);
		String windowHandle = driver.getWindowHandle();
		fc.commonMethods().switch_cboxIframe_frameId(driver);
		fc.utobj().sleep();
		fc.utobj().sendKeys(driver, pobj.fieldDisplayName, fieldName);
		fc.utobj().selectDropDownByVisibleText(driver, pobj.fldValidationType, "Email");
		fc.utobj().clickElement(driver, pobj.submitFieldCBoxButton);
		driver.switchTo().window(windowHandle);
		return fieldName;

	}

	public String addFieldNamePrimaryInfoURL(WebDriver driver, String sectionName, String fieldName) throws Exception {

		AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);
		fc.utobj().sleep();
		fieldName = fc.utobj().generateTestData(fieldName);
		String windowHandle = driver.getWindowHandle();
		fc.commonMethods().switch_cboxIframe_frameId(driver);
		fc.utobj().sleep();
		fc.utobj().sendKeys(driver, pobj.fieldDisplayName, fieldName);
		fc.utobj().selectDropDownByVisibleText(driver, pobj.fldValidationType, "Url");
		fc.utobj().clickElement(driver, pobj.submitFieldCBoxButton);
		driver.switchTo().window(windowHandle);
		return fieldName;

	}
}
