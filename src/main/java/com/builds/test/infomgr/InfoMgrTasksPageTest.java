package com.builds.test.infomgr;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.common.CorporateUser;
import com.builds.uimaps.infomgr.InfoMgrCenterInfoPage;
import com.builds.uimaps.infomgr.InfoMgrTasksPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class InfoMgrTasksPageTest {

	FranconnectUtil fc = new FranconnectUtil();
	AdminInfoMgrCommonMethods adminInfoMgrcommonmethod = new AdminInfoMgrCommonMethods();

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Verify_InfoMgr_Tasks_Add_Task", testCaseDescription = "Test to verify add task functionality", reference = {
			"" })
	public void InfoMgr_Task_Add_Task() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		TasksCommonMethods tasks = fc.infomgr().tasks();
		FranchiseesCommonMethods franchisee = fc.infomgr().franchisees();
		InfoMgrCenterInfoPage centerInfoPage = new InfoMgrCenterInfoPage(driver);
		AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();
		

		try {
			fc.loginpage().login(driver);
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail("infomgrautomation@staffex.com");
			corpUser = corpTest.createDefaultUser(driver, corpUser);

			fc.utobj().printTestStep("Add Franchise Location ");
			//String franchiseID = adminInfoMgr.addFranchiseLocation(driver);
			String franchiseID = adminInfoMgr.addFranchiseLocationForInfoMgrName(driver, false);
			fc.utobj().printTestStep("Add Franchise owner ");
			franchisee.searchFranchiseAndClick(driver, franchiseID);
			fc.utobj().clickElement(driver, centerInfoPage.ownersTab);
			Map<String, String> owner = franchisee.addOwner(driver);
			String ownerName = owner.get("FirstName") + " " + owner.get("LastName");

			fc.utobj().printTestStep("Add a task");
			fc.infomgr().infomgr_common().InfoMgrTasks(driver);
			tasks.addTask(driver, config, franchiseID, ownerName, corpUser.getuserFullName());
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr", "IMFailedTC" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Verify_InfoMgr_Tasks_TopAction_Delete", testCaseDescription = "Test to verify task delete functionality", reference = {
			"" })
	public void InfoMgr_Tasks_TopAction_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		TasksCommonMethods tasks = fc.infomgr().tasks();
		FranchiseesCommonMethods franchisee = fc.infomgr().franchisees();
		InfoMgrCenterInfoPage centerInfoPage = new InfoMgrCenterInfoPage(driver);
		AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();

		try {
			fc.loginpage().login(driver);
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail("infomgrautomation@staffex.com");
			corpUser = corpTest.createDefaultUser(driver, corpUser);

			fc.utobj().printTestStep("Add Franchise Location ");
			String franchiseID = adminInfoMgr.addFranchiseLocation(driver);
			fc.utobj().printTestStep("Add Franchise owner ");
			franchisee.searchFranchiseAndClick(driver, franchiseID);
			fc.utobj().clickElement(driver, centerInfoPage.ownersTab);
			Map<String, String> owner = franchisee.addOwner(driver);
			String ownerName = owner.get("FirstName") + " " + owner.get("LastName");

			fc.utobj().printTestStep("Add a task");
			fc.infomgr().infomgr_common().InfoMgrTasks(driver);
			String taskSubject = tasks.addTask(driver, config, franchiseID, ownerName, corpUser.getuserFullName());
			fc.utobj().printTestStep("Deleteing a task");
			tasks.deleteTask(driver, config, taskSubject, 1);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Verify_InfoMgr_Tasks_ImgAction_Delete", testCaseDescription = "Test to verify task delete functionality", reference = {
			"" })
	public void InfoMgr_Tasks_ImgAction_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		TasksCommonMethods tasks = fc.infomgr().tasks();
		FranchiseesCommonMethods franchisee = fc.infomgr().franchisees();
		InfoMgrCenterInfoPage centerInfoPage = new InfoMgrCenterInfoPage(driver);
		AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();

		try {
			fc.loginpage().login(driver);
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail("infomgrautomation@staffex.com");
			corpUser = corpTest.createDefaultUser(driver, corpUser);

			fc.utobj().printTestStep("Add Franchise Location ");
			String franchiseID = adminInfoMgr.addFranchiseLocation(driver);
			fc.utobj().printTestStep("Add Franchise owner ");
			franchisee.searchFranchiseAndClick(driver, franchiseID);
			fc.utobj().clickElement(driver, centerInfoPage.ownersTab);
			Map<String, String> owner = franchisee.addOwner(driver);
			String ownerName = owner.get("FirstName") + " " + owner.get("LastName");

			fc.utobj().printTestStep("Add a task");
			fc.infomgr().infomgr_common().InfoMgrTasks(driver);
			String taskSubject = tasks.addTask(driver, config, franchiseID, ownerName, corpUser.getuserFullName());
			fc.utobj().printTestStep("Deleteing a task");
			tasks.deleteTask(driver, config, taskSubject, 2);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-11", testCaseId = "TC_Verify_InfoMgr_Tasks_BottomAction_Delete", testCaseDescription = "Test to verify task delete functionality", reference = {
			"" })
	public void InfoMgr_Tasks_BottomAction_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		TasksCommonMethods tasks = fc.infomgr().tasks();
		FranchiseesCommonMethods franchisee = fc.infomgr().franchisees();
		InfoMgrCenterInfoPage centerInfoPage = new InfoMgrCenterInfoPage(driver);
		AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();

		try {
			fc.loginpage().login(driver);
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail("infomgrautomation@staffex.com");
			corpUser = corpTest.createDefaultUser(driver, corpUser);

			fc.utobj().printTestStep("Add Franchise Location ");
			String franchiseID = adminInfoMgr.addFranchiseLocation(driver);
			fc.utobj().printTestStep("Add Franchise owner ");
			franchisee.searchFranchiseAndClick(driver, franchiseID);
			fc.utobj().clickElement(driver, centerInfoPage.ownersTab);
			Map<String, String> owner = franchisee.addOwner(driver);
			String ownerName = owner.get("FirstName") + " " + owner.get("LastName");

			fc.utobj().printTestStep("Add a task");
			fc.infomgr().infomgr_common().InfoMgrTasks(driver);
			String taskSubject = tasks.addTask(driver, config, franchiseID, ownerName, corpUser.getuserFullName());
			fc.utobj().printTestStep("Deleteing a task");
			tasks.deleteTask(driver, config, taskSubject, 3);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Verify_InfoMgr_Tasks_TopAction_Print", testCaseDescription = "Test to verify task print functionality", reference = {
			"" })
	public void InfoMgr_Tasks_TopAction_Print() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		TasksCommonMethods tasks = fc.infomgr().tasks();
		FranchiseesCommonMethods franchisee = fc.infomgr().franchisees();
		InfoMgrCenterInfoPage centerInfoPage = new InfoMgrCenterInfoPage(driver);
		AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();

		try {
			fc.loginpage().login(driver);
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail("infomgrautomation@staffex.com");
			corpUser = corpTest.createDefaultUser(driver, corpUser);

			fc.utobj().printTestStep("Add Franchise Location ");
			String franchiseID = adminInfoMgr.addFranchiseLocation(driver);
			fc.utobj().printTestStep("Add Franchise owner ");
			franchisee.searchFranchiseAndClick(driver, franchiseID);
			fc.utobj().clickElement(driver, centerInfoPage.ownersTab);
			Map<String, String> owner = franchisee.addOwner(driver);
			String ownerName = owner.get("FirstName") + " " + owner.get("LastName");

			fc.utobj().printTestStep("Add a task");
			fc.infomgr().infomgr_common().InfoMgrTasks(driver);
			String taskSubject = tasks.addTask(driver, config, franchiseID, ownerName, corpUser.getuserFullName());
			fc.infomgr().infomgr_common().InfoMgrTasks(driver);
			fc.utobj().printTestStep("Print Verify");
			fc.utobj().selectActionMenuItemsWithTagInput(driver, "Print");
			tasks.VerifyPrintPreview(driver, taskSubject);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr", "IMFailedTC","InfoMgr99" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Verify_InfoMgr_Tasks_BottomAction_Print", testCaseDescription = "Test to verify task print functionality", reference = {
			"" })
	public void InfoMgr_Tasks_BottonAction_Print() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		TasksCommonMethods tasks = fc.infomgr().tasks();
		FranchiseesCommonMethods franchisee = fc.infomgr().franchisees();
		InfoMgrCenterInfoPage centerInfoPage = new InfoMgrCenterInfoPage(driver);
		AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();
		InfoMgrTasksPage taskPage = new InfoMgrTasksPage(driver);

		try {
			fc.loginpage().login(driver);
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail("infomgrautomation@staffex.com");
			corpUser = corpTest.createDefaultUser(driver, corpUser);

			fc.utobj().printTestStep("Add Franchise Location ");
			String franchiseID = adminInfoMgr.addFranchiseLocation(driver);
			fc.utobj().printTestStep("Add Franchise owner ");
			franchisee.searchFranchiseAndClick(driver, franchiseID);
			fc.utobj().clickElement(driver, centerInfoPage.ownersTab);
 			Map<String, String> owner = franchisee.addOwner(driver);
			String ownerName = owner.get("FirstName") + " " + owner.get("LastName");

			fc.utobj().printTestStep("Add a task");
			fc.infomgr().infomgr_common().InfoMgrTasks(driver);
			String taskSubject = tasks.addTask(driver, config, franchiseID, ownerName, corpUser.getuserFullName());
			fc.utobj().printTestStep("Print Verify");
			fc.utobj().clickElement(driver, taskPage.btnPrint);
			tasks.VerifyPrintPreview(driver, taskSubject);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Verify_InfoMgr_Tasks_ImageAction_Modify", testCaseDescription = "Test to verify modify task functionality", reference = {
			"" })
	public void InfoMgr_Tasks_ImgAction_Modify() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		TasksCommonMethods tasks = fc.infomgr().tasks();
		FranchiseesCommonMethods franchisee = fc.infomgr().franchisees();
		InfoMgrCenterInfoPage centerInfoPage = new InfoMgrCenterInfoPage(driver);
		AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();

		try {
			fc.loginpage().login(driver);
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail("infomgrautomation@staffex.com");
			corpUser = corpTest.createDefaultUser(driver, corpUser);

			fc.utobj().printTestStep("Add Franchise Location ");
			String franchiseID = adminInfoMgr.addFranchiseLocation(driver);
			fc.utobj().printTestStep("Add Franchise owner ");
			franchisee.searchFranchiseAndClick(driver, franchiseID);
			fc.utobj().clickElement(driver, centerInfoPage.ownersTab);
			Map<String, String> owner = franchisee.addOwner(driver);
			String ownerName = owner.get("FirstName") + " " + owner.get("LastName");

			fc.utobj().printTestStep("Add a task");
			fc.infomgr().infomgr_common().InfoMgrTasks(driver);
			String taskSubject = tasks.addTask(driver, config, franchiseID, ownerName, corpUser.getuserFullName());
			fc.utobj().printTestStep("Modify task");
			taskSubject = tasks.modifyTask(driver, config, taskSubject);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr", "IMFailedTC" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Verify_InfoMgr_Tasks_View_Func", testCaseDescription = "Test to verify view task functionality", reference = {
			"" })
	public void InfoMgr_Tasks_View_Func() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		TasksCommonMethods tasks = fc.infomgr().tasks();
		FranchiseesCommonMethods franchisee = fc.infomgr().franchisees();
		InfoMgrCenterInfoPage centerInfoPage = new InfoMgrCenterInfoPage(driver);
		AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();

		try {
			fc.loginpage().login(driver);
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail("infomgrautomation@staffex.com");
			corpUser = corpTest.createDefaultUser(driver, corpUser);

			fc.utobj().printTestStep("Add Franchise Location ");
			String franchiseID = adminInfoMgr.addFranchiseLocation(driver);
			fc.utobj().printTestStep("Add Franchise owner ");
			franchisee.searchFranchiseAndClick(driver, franchiseID);
			fc.utobj().clickElement(driver, centerInfoPage.ownersTab);
			Map<String, String> owner = franchisee.addOwner(driver);
			String ownerName = owner.get("FirstName") + " " + owner.get("LastName");

			fc.utobj().printTestStep("Add a task");
			fc.infomgr().infomgr_common().InfoMgrTasks(driver);
			String taskSubject = tasks.addTask(driver, config, franchiseID, ownerName, corpUser.getuserFullName());
			fc.utobj().printTestStep("View task from action menu");
			fc.utobj().actionImgOption(driver, taskSubject, "View");
			tasks.viewTask(driver, config, taskSubject);
			fc.utobj().printTestStep("Verify task view after clicking the task subjectu");
			fc.utobj().clickLink(driver, taskSubject);
			tasks.viewTask(driver, config, taskSubject);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Verify_InfoMgr_Tasks_ImageAction_Process", testCaseDescription = "Test to verify process task functionality", reference = {
			"" })
	public void InfoMgr_Tasks_ImgAction_Process() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		TasksCommonMethods tasks = fc.infomgr().tasks();
		FranchiseesCommonMethods franchisee = fc.infomgr().franchisees();
		InfoMgrCenterInfoPage centerInfoPage = new InfoMgrCenterInfoPage(driver);
		AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();

		try {
			fc.loginpage().login(driver);
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail("infomgrautomation@staffex.com");
			corpUser = corpTest.createDefaultUser(driver, corpUser);

			fc.utobj().printTestStep("Add Franchise Location ");
			String franchiseID = adminInfoMgr.addFranchiseLocation(driver);
			fc.utobj().printTestStep("Add Franchise owner ");
			franchisee.searchFranchiseAndClick(driver, franchiseID);
			fc.utobj().clickElement(driver, centerInfoPage.ownersTab);
			Map<String, String> owner = franchisee.addOwner(driver);
			String ownerName = owner.get("FirstName") + " " + owner.get("LastName");

			fc.utobj().printTestStep("Add a task");
			fc.infomgr().infomgr_common().InfoMgrTasks(driver);
			String taskSubject = tasks.addTask(driver, config, franchiseID, ownerName, corpUser.getuserFullName());
			fc.utobj().printTestStep("Process task");
			tasks.processTask(driver, config, taskSubject);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr", "IMFailedTC","Summary_Filter"})

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-22", testCaseId = "TC_Verify_InfoMgr_Tasks_Filter", testCaseDescription = "Test to verify filter displayed on the Tasks Page", reference = {
			"" })
	public void InfoMgr_Tasks_Filter_Verify() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		TasksCommonMethods tasks = fc.infomgr().tasks();
		FranchiseesCommonMethods franchisee = new FranchiseesCommonMethods();
		AdminInfoMgrCommonMethods adminInfoMgr = new AdminInfoMgrCommonMethods();
		InfoMgrCenterInfoPage centerInfoPage = new InfoMgrCenterInfoPage(driver);

		try {
			fc.loginpage().login(driver);
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail("infomgrautomation@staffex.com");
			corpUser = corpTest.createDefaultUser(driver, corpUser);

			fc.utobj().printTestStep("Add Franchise Location ");
			String franchiseID = adminInfoMgr.addFranchiseLocation(driver);
			fc.utobj().printTestStep("Add Franchise owner ");
			franchisee.searchFranchiseAndClick(driver, franchiseID);
			fc.utobj().clickElement(driver, centerInfoPage.ownersTab);
			Map<String, String> owner = franchisee.addOwner(driver);
			String ownerName = owner.get("FirstName") + " " + owner.get("LastName");

			fc.utobj().printTestStep("Add a task");
			fc.infomgr().infomgr_common().InfoMgrTasks(driver);
			Map<String, String> taskFilter = tasks.addTaskForFilter(driver, config, franchiseID, ownerName,
					corpUser.getuserFullName());

			fc.utobj().printTestStep("Verify search");
			tasks.search(driver, config, taskFilter);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}

}
