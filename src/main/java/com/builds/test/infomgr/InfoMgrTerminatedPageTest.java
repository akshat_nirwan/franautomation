package com.builds.test.infomgr;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

import org.apache.commons.lang3.time.DateUtils;
import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.annotations.BeforeGroups;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.common.CorporateUser;
import com.builds.uimaps.infomgr.InfoMgrCenterInfoPage;
import com.builds.uimaps.infomgr.InfoMgrContactHistoryPage;
import com.builds.uimaps.infomgr.InfoMgrCorporateLocationsPage;
import com.builds.uimaps.infomgr.InfoMgrDefaultAndTerminationPage;
import com.builds.uimaps.infomgr.InfoMgrFranchiseesPage;
import com.builds.uimaps.infomgr.InfoMgrModulePage;
import com.builds.uimaps.infomgr.InfoMgrOwnersPage;
import com.builds.uimaps.infomgr.InfoMgrRegionalPage;
import com.builds.uimaps.infomgr.InfoMgrTerminatedPage;
import com.builds.uimaps.infomgr.InfoMgrUsersPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class InfoMgrTerminatedPageTest {
	FranconnectUtil fc = new FranconnectUtil();
	public static String flag = "true";
	public static final String franchiseID = "Terminated" + ThreadLocalRandom.current().nextInt(10000);
	AdminInfoMgrCommonMethods adminInfoMgr = new AdminInfoMgrCommonMethods();
	CorporateUser corpUser = new CorporateUser();
	TerminatedCommonMethods terminated = fc.infomgr().terminated();
	
	@BeforeGroups(groups = { "infomgr" ,"Terminated_Tabs_AMD","contract","Owner"})
	public synchronized void createTerminatedFranchise() throws Exception {
		

		
		if (flag == "true") {
			flag = "false";		
			String testCaseId = "BeforeGroupBlock to create Common Terminated franchisee Location";
			FranchiseeAPI fran = new FranchiseeAPI();
			System.out.println("inside before group method 'createIndevelopmentFranchise'");
			Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
			WebDriver driver = fc.commonMethods().browsers().openBrowser();
			
			InfoMgrCenterInfoPage centerInfoPage = new InfoMgrTerminatedPage(driver).getCenterInfoPage();

			fc.utobj().printTestStep("Add corporate user");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail("infomgrautomation@staffex.com");
			corpUser = corpTest.createDefaultUser(driver, corpUser);
			fc.loginpage().login(driver);
			fc.utobj().printTestStep("Add Area / Region ");
			String areaRegionID=adminInfoMgr.addAreaRegion(driver);
			Reporter.log("Area Region - " + areaRegionID + "created");
			System.out.println("Area Region created : " + areaRegionID);
			fran.createDefaultFranchiseeAPI(areaRegionID, "No", franchiseID);
			System.out.println("Franchisee Location created : " + franchiseID);
			Reporter.log(" Franchisee Location is Created" + franchiseID);
			terminated.terminateFranchiseLocation(driver, config, franchiseID);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		}
	}

	@Test(groups = {  "infomgr" ,"Terminated_Tabs_AMD" })
	// BugID - 69046 - Login with corporate user
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_Terminated_AgreementTab_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of Agreement tab of Info Mgr > terminated.", reference = { "" })
	public void InfoMgr_Terminated_AgreementTab_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
	
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		

		try {
			fc.loginpage().login(driver);
			InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();
			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");
			terminated.searchFranchiseAndClick(driver, franchiseID);
			fc.utobj().printTestStep("Add Agreement Tab Data");
			// Add Agreement
			fc.utobj().clickElement(driver, centerInfoPage.lnkAgreement);
			driver = terminated.addAgreement(driver, config);
			// Modify Agreement
			fc.utobj().printTestStep("Modify Agreement Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkAgreement);
			driver = terminated.modifyAgreement(driver, config);
			// Delete Agreement
			fc.utobj().printTestStep("Delete Agreement Tab Data");
			terminated.deleteTabData(driver, config, centerInfoPage.lnkAgreement, null);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" ,"Terminated_Tabs_AMD","Testddd"})

	// BugID - 69046 - Login with corporate user
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_Terminated_AddressTab_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of Address tab of Info Mgr > terminated.", reference = { "" })
	public void InfoMgr_Terminated_AddressTab_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
	
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		try {
			fc.loginpage().login(driver);
			InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();
			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");
			terminated.searchFranchiseAndClick(driver, franchiseID);

			// Add Address
			fc.utobj().printTestStep("Add Address Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkAddresses);
			driver = terminated.addAddresses(driver, config);
			// Modify Address
			fc.utobj().printTestStep("Modify Address Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkAddresses);
			driver = terminated.modifyAddresses(driver, config);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {  "infomgr" ,"Terminated_Tabs_AMD" ,"1234"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_Terminated_ContactHistoryTab_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of ContactHistoryTab of Info Mgr > terminated.", reference = { "" })
	public void InfoMgr_Terminated_ContactHistoryTab_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
	
		InfoMgrFranchiseesPage franchiseesPage = new InfoMgrFranchiseesPage(driver);
	
		try {
			
			fc.loginpage().login(driver);
			InfoMgrContactHistoryPage contactHistoryPage = franchiseesPage.getContactHistoryPage();
			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");
			terminated.searchFranchiseAndClick(driver, franchiseID);
			
			// Log a Task
			fc.utobj().clickElement(driver, contactHistoryPage.lnkContactHistory);

			fc.utobj().clickElement(driver, contactHistoryPage.btnLogaTask);
			String taskSubject = terminated.logaTask(driver, config, corpUser.getuserFullName(), true);

			// Log a Call
			fc.utobj().clickElement(driver, contactHistoryPage.btnLogaCall);
			String callSubject = terminated.logACall(driver, config, true);

			// Add Remark
			fc.utobj().clickElement(driver, contactHistoryPage.btnAddRemark);
			String remarkSubject = terminated.addRemark(driver, config, contactHistoryPage);

			// Modify Contact History

			// Modify a task
			fc.utobj().clickElement(driver, contactHistoryPage.lnkContactHistory);
			fc.utobj().actionImgOption(driver, taskSubject, "Modify");
			terminated.modifyATask(driver, config, true);

			// Modify a call
			fc.utobj().actionImgOption(driver, callSubject, "Modify");
			terminated.modifyACall(driver, config, true);

			// Modify a remark
			fc.utobj().actionImgOption(driver, remarkSubject, "Modify");
			terminated.modifyRemark(driver, config, contactHistoryPage);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" ,"Terminated_Tabs_AMD" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_Terminated_ContractSigning_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of ContractSigning tabs of Info Mgr > terminated.", reference = { "" })
	public void InfoMgr_Terminated_ContractSigningTab_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
	
		
		try {
			fc.loginpage().login(driver);
			InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();
			// adminInfoMgr.configureInfoMgr(driver, config);
			
			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");
			terminated.searchFranchiseAndClick(driver, franchiseID);
			// Adding Contract Signing
			fc.utobj().printTestStep("Add Contract History Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkContractSigning);
			terminated.addContractSigning(driver, config);
			// Modify Contract Signing
			fc.utobj().printTestStep("Modify Contract History Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkContractSigning);
			terminated.modifyContractSigning(driver, config);
			// Delete Contract Signing
			fc.utobj().printTestStep("Delete Contract History Tab Data");
			terminated.deleteTabData(driver, config, centerInfoPage.lnkContractSigning, null);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" ,"Terminated_Tabs_AMD" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_Terminated_CustomerComplaint_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of CustomerComplaint tabs of Info Mgr > terminated.", reference = { "" })
	public void InfoMgr_Terminated_CustomerComplaintTab_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
	
		try {

			fc.loginpage().login(driver);
			// adminInfoMgr.configureInfoMgr(driver, config);
			InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();
			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");
			terminated.searchFranchiseAndClick(driver, franchiseID);

			// Add Customer Complaints
			fc.utobj().printTestStep("Add Customer Complaints Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkCustomerComplaints);
			terminated.addCustomerComplaints(driver, config);

			// Modify Customer Complaints
			fc.utobj().printTestStep("Modify Customer Complaints Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkCustomerComplaints);
			terminated.modifyCustomerComplaints(driver, config);
			// Delete
			fc.utobj().printTestStep("Delete Customer Complaints Tab Data");
			terminated.deleteTabData(driver, config, centerInfoPage.lnkCustomerComplaints, null);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {  "infomgr" ,"Terminated_Tabs_AMD" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_Terminated_CenterInfo_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of _CenterInfo  tabs of Info Mgr > terminated.", reference = { "" })
	public void InfoMgr_Terminated_CenterInfo_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();
	
		try {
			fc.loginpage().login(driver);
			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");
			terminated.searchFranchiseAndClick(driver, franchiseID);
			// Modify Center Info
			fc.utobj().printTestStep("Modify Center Info Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkCenterInfo);
			fc.utobj().clickLink(driver, "Modify");
			terminated.modifyCenterInfo(driver, config);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {  "infomgr" ,"Terminated_Tabs_AMD" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_Terminated_Documents_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of Documents tabs of Info Mgr > terminated.", reference = { "" })
	public void InfoMgr_Terminated_Documents_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
	
		
		InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();
		try {
			fc.loginpage().login(driver);
			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");		
			terminated.searchFranchiseAndClick(driver, franchiseID);
			// Add Documents
			fc.utobj().printTestStep("Add  Documents Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkDocuments);
			String docTitle = terminated.addDocument(driver, config);

			// Modify Documents
			fc.utobj().printTestStep("Modify  Documents Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkDocuments);
			fc.utobj().actionImgOption(driver, docTitle, "Modify");
			docTitle = terminated.modifyDocument(driver, config);
			// Delete
			fc.utobj().printTestStep("Delete  Documents Tab Data");
			terminated.deleteTabData(driver, config, centerInfoPage.lnkDocuments, docTitle);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {  "infomgr" ,"Terminated_Tabs_AMD","Empl" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_Terminated_Employees_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of Employee tabs of Info Mgr > terminated.", reference = { "" })
	public void InfoMgr_Terminated_Employees_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
	
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();
		try {
			fc.loginpage().login(driver);
			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");
			terminated.searchFranchiseAndClick(driver, franchiseID);
			// Add Employees
			fc.utobj().printTestStep("Add  Employees Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkEmployees);
			String empName = terminated.addEmployee(driver, config);
			// Modify Employee
			fc.utobj().printTestStep("Modify  Employees Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkEmployees);
			// fc.utobj().actionImgOption(driver, empName, "Modify");
			empName = terminated.modifyEmployee(driver, config);
			fc.utobj().printTestStep("Delete  Employees Tab Data");
			terminated.deleteTabData(driver, config, centerInfoPage.lnkEmployees, null);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" ,"Terminated_Tabs_AMD" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_Terminated_Guarantor_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of Guarantor tab of Info Mgr > terminated.", reference = { "" })
	public void InfoMgr_Terminated_Guarantor_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
	
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();
		try {

			fc.loginpage().login(driver);
			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");
			terminated.searchFranchiseAndClick(driver, franchiseID);
			// Add Gurantor details
			fc.utobj().printTestStep("Add  Gurantor Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkGuarantors);
			terminated.addGuarantor(driver, config);
			// Modify Guarantor
			fc.utobj().printTestStep("Modify  Gurantor Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkGuarantors);
			terminated.modifyGuarantor(driver, config);
			// Delete Entity
			fc.utobj().printTestStep("Delete  Gurantor Tab Data");
			terminated.deleteTabData(driver, config, centerInfoPage.guarantorsTab, null);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" ,"Terminated_Tabs_AMD" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_Terminated_Entity_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of Entity Tab of Info Mgr > terminated.", reference = { "" })
	public void InfoMgr_Terminated_Entity_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
	
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();
		try {
			fc.loginpage().login(driver);
			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");
			terminated.searchFranchiseAndClick(driver, franchiseID);
			// Add
			fc.utobj().printTestStep("Add  Entity Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.linkEntityDetail);
			terminated.addEntityDetails(driver, config);
			// Modify Entity Details
			fc.utobj().printTestStep("Modify  Entity Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.linkEntityDetail);
			terminated.modifyEntityDetails(driver, config);
			// Delete Entity
			fc.utobj().printTestStep("Delete  Entity Tab Data");
			terminated.deleteTabData(driver, config, centerInfoPage.linkEntityDetail, null);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {  "infomgr" ,"Terminated_Tabs_AMD" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_Terminated_Marketing_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of Marketing Tab of Info Mgr > terminated.", reference = { "" })
	public void InfoMgr_Terminated_Marketing_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
	
		try {

		
			InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();
			fc.loginpage().login(driver);
			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");
			terminated.searchFranchiseAndClick(driver, franchiseID);

			// Add Marketing details
			fc.utobj().printTestStep("Add  Marketing Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkMarketingFinTab);
			terminated.addMarketingDetails(driver, config);
			// Modify Marketing
			fc.utobj().printTestStep("Modify  Marketing Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkMarketingFinTab);
			terminated.modifyMarketingDetails(driver, config);
			// Delete marketing
			fc.utobj().printTestStep("Delete  Marketing Tab Data");
			terminated.deleteTabData(driver, config, centerInfoPage.marketingTab, null);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {  "infomgr" ,"Terminated_Tabs_AMD"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_Terminated_Insurance_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of Insurance tabs of Info Mgr > terminated.", reference = { "" })
	public void InfoMgr_Terminated_Insurance_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
	
	
		InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();
		try {

			fc.loginpage().login(driver);

			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");
			terminated.searchFranchiseAndClick(driver, franchiseID);
			// Add Insurance Details
			fc.utobj().printTestStep("Add  Insurance Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkInsurance);
			terminated.addInsuranceDetails(driver, config);

			// Modify Insurance
			fc.utobj().printTestStep("Modify  Insurance Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkInsurance);
			terminated.modifyInsuranceDetails(driver, config);

			// Delete Insurance
			fc.utobj().printTestStep("Delete  Insurance Tab Data");
			terminated.deleteTabData(driver, config, centerInfoPage.insuranceTab, null);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {  "infomgr" ,"Terminated_Tabs_AMD"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_Terminated_LegalViolation_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of LegalViolation tabs of Info Mgr > terminated.", reference = { "" })
	public void InfoMgr_Terminated_LegalViolation_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
	
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();
		try {

			fc.loginpage().login(driver);
			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");
			terminated.searchFranchiseAndClick(driver, franchiseID);
			// Add legal violation
			fc.utobj().printTestStep("Add  Legal Violation Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkLegalViolation);
			terminated.addLegalViolation(driver, config);
			// Modify Legal Violation
			fc.utobj().printTestStep("Modify  Legal Violation Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkLegalViolation);
			terminated.modifyLegalViolation(driver, config);
			// Delete Legal Violation
			fc.utobj().printTestStep("Delete  Legal Violation Tab Data");
			terminated.deleteTabData(driver, config, centerInfoPage.lnkLegalViolation, null);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" ,"Terminated_Tabs_AMD" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_Terminated_Lenders_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of Lenders tabs of Info Mgr > terminated.", reference = { "" })
	public void InfoMgr_Terminated_Lenders_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
	
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();
		try {

			fc.loginpage().login(driver);
			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");
			terminated.searchFranchiseAndClick(driver, franchiseID);

			// Add Lenders
			fc.utobj().printTestStep("Add  Lenders Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkLenders);
			terminated.addLenders(driver, config);
			// Modify Lenders Details
			fc.utobj().printTestStep("Modify  Lenders Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkLenders);
			terminated.modifyLenders(driver, config);

			// Delete Lenders
			fc.utobj().printTestStep("Delete  Lenders Tab Data");
			terminated.deleteTabData(driver, config, centerInfoPage.lnkLenders, null);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" ,"Terminated_Tabs_AMD" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_Terminated_Finance_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of Finance of Info Mgr > terminated.", reference = { "" })
	public void InfoMgr_Terminated_Finance_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
	
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();
		try {

			fc.loginpage().login(driver);

			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");
			terminated.searchFranchiseAndClick(driver, franchiseID);

			// Add Finance Details
			fc.utobj().printTestStep("Add  Finance Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkFinancial);
			terminated.addFinanceDetails(driver, config);

			// Modify Financials
			fc.utobj().printTestStep("Modify  Finance Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkFinancial);
			terminated.modifyFinanceDetails(driver, config);

			// Delete Financials
			fc.utobj().printTestStep("Delete  Finance Tab Data");
			terminated.deleteTabData(driver, config, centerInfoPage.lnkFinancial, null);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {  "infomgr" ,"Terminated_Tabs_AMD" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_Terminated_MysteryReview_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of MysteryReview Tab of Info Mgr > terminated.", reference = { "" })
	public void InfoMgr_Terminated_MysteryReview_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
	
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();
		try {
			fc.loginpage().login(driver);

			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");
			terminated.searchFranchiseAndClick(driver, franchiseID);

			// Add Mystry Review
			fc.utobj().printTestStep("Add  MystryReview Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkMysteryReview);
			terminated.addMystryReview(driver, config);
			// Modify Mystry Review
			fc.utobj().printTestStep("Modify  MystryReview Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkMysteryReview);
			terminated.modifyMystryReview(driver, config);
			// Delete MysteryReview
			fc.utobj().printTestStep("Delete  MystryReview Tab Data");
			terminated.deleteTabData(driver, config, centerInfoPage.lnkMysteryReview, null);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" ,"Terminated_Tabs_AMD"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_Terminated_Picture_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of Picture tab of Info Mgr > terminated.", reference = { "" })
	public void InfoMgr_Terminated_Picture_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
	
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();
		try {
			fc.loginpage().login(driver);
			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");
			terminated.searchFranchiseAndClick(driver, franchiseID);

			// Add Picture
			fc.utobj().printTestStep("Add  Picture Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkPictures);
			String pictureTitle = terminated.addPicture(driver, config);
			// Modify Pictures
			fc.utobj().printTestStep("Modify  Picture Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkPictures);
			fc.utobj().actionImgOption(driver, pictureTitle, "Modify");
			pictureTitle = terminated.modifyPicture(driver, config);

			// Delete Pictures
			fc.utobj().printTestStep("Delete  Picture Tab Data");
			terminated.deleteTabData(driver, config, centerInfoPage.lnkPictures, pictureTitle);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {  "infomgr" ,"Terminated_Tabs_AMD","Owner" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_Terminated_Owner_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of Owner of Info Mgr > terminated.", reference = { "" })
	public void InfoMgr_Terminated_Owner_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
	
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();
		try {
			fc.loginpage().login(driver);

			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");
			terminated.searchFranchiseAndClick(driver, franchiseID);
			// Add Owner
			fc.utobj().printTestStep("Add  Owner Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.tabOwners);
			Map<String, String> owner = terminated.addOwner(driver,config);
	
			// Modify Owners
			fc.utobj().printTestStep("Modify  Owner Tab Data");
						fc.utobj().clickElement(driver, centerInfoPage.tabOwners);
						fc.utobj().actionImgOption(driver, owner.get("FirstName"), "Modify");
						terminated.modifyOwner(driver, config);
			// Delete Owner
			fc.utobj().printTestStep("Delete  Owner Tab Data");
			terminated.deleteTabData(driver, config, centerInfoPage.ownersTab, owner.get("FirstName"));
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" ,"Terminated_Tabs_AMD" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_Terminated_Realestate_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of Realestate of Info Mgr > terminated.", reference = { "" })
	public void InfoMgr_Terminated_Realestate_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
	
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();
		try {
			fc.loginpage().login(driver);
			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");
			terminated.searchFranchiseAndClick(driver, franchiseID);
			// Add Real Estate
			fc.utobj().printTestStep("Add  RealEstate Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkRealEstate);
			terminated.addRealState(driver, config);
			// Modify Real state
			fc.utobj().printTestStep("Modify  RealEstate Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkRealEstate);
			terminated.modifyRealState(driver, config);
			// Delete RealEstate
			fc.utobj().printTestStep("Delete  RealEstate Tab Data");
			terminated.deleteTabData(driver, config, centerInfoPage.lnkRealEstate, null);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {  "infomgr" ,"Terminated_Tabs_AMD","Testddd"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_Terminated_Territory_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of Territory Tab of Info Mgr > terminated.", reference = { "" })
	public void InfoMgr_Terminated_Territory_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
	
		InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();
		try {
			fc.loginpage().login(driver);
			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");
			terminated.searchFranchiseAndClick(driver, franchiseID);
			// Add Territory
			fc.utobj().printTestStep("Add  Territory Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkTerritory);
			terminated.addTerritory(driver, config);
			// Modify Territory
			fc.utobj().printTestStep("Modify  Territory Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkTerritory);
			terminated.modifyTerritory(driver, config);
			// Delete Territory
			fc.utobj().printTestStep("delete  Territory Tab Data");
			terminated.deleteTabData(driver, config, centerInfoPage.lnkTerritory, null);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {  "infomgr" ,"Terminated_Tabs_AMD"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_Terminated_Training_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of Training Tab of Info Mgr > terminated.", reference = { "" })
	public void InfoMgr_Terminated_Training_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
	
		InfoMgrCenterInfoPage centerInfoPage = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();
		try {
			fc.loginpage().login(driver);
			fc.utobj().printTestStep("Search Franchise " + franchiseID + "  and click ");
			terminated.searchFranchiseAndClick(driver, franchiseID);
			// Add Training
			fc.utobj().printTestStep("Add  Training Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkTraining);
			terminated.addTraining(driver, config);
			// Modify Training
			fc.utobj().printTestStep("Modify  Training Tab Data");
			fc.utobj().clickElement(driver, centerInfoPage.lnkTraining);
			terminated.modifyTraining(driver, config);
			// Delete Training
			fc.utobj().printTestStep("Delete  Training Tab Data");
			terminated.deleteTabData(driver, config, centerInfoPage.lnkTraining, null);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	//
	@Test(groups = { "infomgr1","IM_ALL_Tabs" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-29", testCaseId = "TC_InfoMgr_Terminated_AllTab_Add_Modify_Delete", testCaseDescription = "This test case is used to verify data addition , modification and deletion of all tabs of Info Mgr > terminated.", reference = {
			"" })
	public void InfoMgr_terminated_AllTab_Add_Modify_Delete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		TerminatedCommonMethods terminated = fc.infomgr().terminated();
		AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();
		InfoMgrTerminatedPage terminatedPage = new InfoMgrTerminatedPage(driver);

		try {
			fc.loginpage().login(driver);
			// adminInfoMgr.configureAllFieldsOnManageFormGeneratorPage(driver ,
			// config);

			InfoMgrCenterInfoPage centerInfoPage = new InfoMgrTerminatedPage(driver).getCenterInfoPage();

			fc.utobj().printTestStep("Add corporate user");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail("infomgrautomation@staffex.com");
			corpUser = corpTest.createDefaultUser(driver, corpUser);

			fc.utobj().printTestStep("Add development Location");
			String franchiseID = adminInfoMgr.addFranchiseLocation(driver);
			Reporter.log("FranchiseID - " + franchiseID + " Created");

			terminated.terminateFranchiseLocation(driver, config, franchiseID);

			fc.utobj().printTestStep("Search and click ");
			terminated.searchFranchiseAndClick(driver, franchiseID);

			fc.utobj().printTestStep("Add data to all the tabs");
			fc.utobj().clickElement(driver, centerInfoPage.lnkAddresses);
			driver = terminated.addAddresses(driver, config);

			// Add Agreement
			fc.utobj().clickElement(driver, centerInfoPage.lnkAgreement);
			driver = terminated.addAgreement(driver, config);

			// Modify Center Info

			fc.utobj().clickElement(driver, centerInfoPage.lnkCenterInfo);
			fc.utobj().clickLink(driver, "Modify");
			terminated.modifyCenterInfo(driver, config);

			// Log a Task
			InfoMgrContactHistoryPage contactHistoryPage = terminatedPage.getContactHistoryPage();
			fc.utobj().clickElement(driver, contactHistoryPage.lnkContactHistory);

			fc.utobj().clickElement(driver, contactHistoryPage.btnLogaTask);
			String taskSubject = terminated.logaTask(driver, config, corpUser.getuserFullName(), true);

			// Log a Call
			fc.utobj().clickElement(driver, contactHistoryPage.btnLogaCall);
			String callSubject = terminated.logACall(driver, config, true);

			// Add Remark
			fc.utobj().clickElement(driver, contactHistoryPage.btnAddRemark);
			String remarkSubject = terminated.addRemark(driver, config, contactHistoryPage);

			// Send Message
			// fc.utobj().clickElement(driver,
			// contactHistoryPage.btnSendMessage);
			// terminated.sendTextMessage(driver, config, true , corporateUser);

			// Send Mail
			// fc.utobj().clickElement(driver, contactHistoryPage.btnSendEmail);
			// terminated.sendTextEmail(driver, config , true);

			// Sending HTML Message and mail (Pending)

			// Adding Contract Signing
			fc.utobj().clickElement(driver, contactHistoryPage.lnkContractSigning);
			terminated.addContractSigning(driver, config);

			// Add Customer Complaints
			fc.utobj().clickElement(driver, contactHistoryPage.lnkCustomerComplaints);
			terminated.addCustomerComplaints(driver, config);

			// Add Documents
			fc.utobj().clickElement(driver, contactHistoryPage.lnkDocuments);
			String docTitle = terminated.addDocument(driver, config);

			// Add Employees
			fc.utobj().clickElement(driver, contactHistoryPage.lnkEmployees);
			String empName = terminated.addEmployee(driver, config);

			// Add Event Details
			fc.utobj().clickElement(driver, contactHistoryPage.linkEntityDetail);
			terminated.addEntityDetails(driver, config);

			// Add Events Details
			fc.utobj().clickElement(driver, contactHistoryPage.lnkEvents);
			terminated.addEventDetails(driver, config);

			// Add Finance Details
			fc.utobj().clickElement(driver, contactHistoryPage.lnkFinancial);
			terminated.addFinanceDetails(driver, config);

			// Add Gurantor details
			fc.utobj().clickElement(driver, contactHistoryPage.lnkGuarantors);
			terminated.addGuarantor(driver, config);

			// Add Insurance Details
			fc.utobj().clickElement(driver, contactHistoryPage.lnkInsurance);
			terminated.addInsuranceDetails(driver, config);

			// Add legal violation
			fc.utobj().clickElement(driver, contactHistoryPage.lnkLegalViolation);
			terminated.addLegalViolation(driver, config);

			// Add Lenders
			fc.utobj().clickElement(driver, contactHistoryPage.lnkLenders);
			terminated.addLenders(driver, config);

			// Add Marketing details
			fc.utobj().clickElement(driver, contactHistoryPage.lnkMarketingFinTab);// Change
																					// by
																					// harish
																					// Dwivedi
			terminated.addMarketingDetails(driver, config);

			// Add Mystry Review
			fc.utobj().clickElement(driver, contactHistoryPage.lnkMysteryReview);
			terminated.addMystryReview(driver, config);

			// Add Owner
			fc.utobj().clickElement(driver, contactHistoryPage.tabOwners);
			Map<String, String> owner = terminated.addOwner(driver, config);

			// Add Picture
			fc.utobj().clickElement(driver, contactHistoryPage.lnkPictures);
			String pictureTitle = terminated.addPicture(driver, config);

			// Add Real Estate
			fc.utobj().clickElement(driver, contactHistoryPage.lnkRealEstate);
			terminated.addRealState(driver, config);

			// Add Territory
			fc.utobj().clickElement(driver, contactHistoryPage.lnkTerritory);
			terminated.addTerritory(driver, config);

			// Add Training
			fc.utobj().clickElement(driver, centerInfoPage.lnkTraining);
			terminated.addTraining(driver, config);

			// Add Visit
			// terminated.addVisit(driver, config, franchiseID, corporateUser);

			// ****************** Modify Data
			// **************************************
			fc.utobj().printTestStep("Modify data of all the tabs");

			terminated.searchFranchiseAndClick(driver, franchiseID);

			// Modify Address
			fc.utobj().clickElement(driver, centerInfoPage.lnkAddresses);
			driver = terminated.modifyAddresses(driver, config);

			// Modify Agreement
			fc.utobj().clickElement(driver, centerInfoPage.lnkAgreement);
			driver = terminated.modifyAgreement(driver, config);

			// Modify Center Info
			fc.utobj().clickElement(driver, centerInfoPage.lnkCenterInfo);
			fc.utobj().clickLink(driver, "Modify");
			terminated.modifyCenterInfo(driver, config);

			// Modify Contact History

			// Modify a task
			fc.utobj().clickElement(driver, contactHistoryPage.lnkContactHistory);
			fc.utobj().actionImgOption(driver, taskSubject, "Modify");
			terminated.modifyATask(driver, config, true);

			// Modify a call
			fc.utobj().actionImgOption(driver, callSubject, "Modify");
			terminated.modifyACall(driver, config, true);

			// Modify a remark
			fc.utobj().actionImgOption(driver, remarkSubject, "Modify");
			terminated.modifyRemark(driver, config, contactHistoryPage);

			// Modify Contract Signing
			fc.utobj().clickElement(driver, centerInfoPage.lnkContractSigning);
			terminated.modifyContractSigning(driver, config);

			// Modify Customer Complaints
			fc.utobj().clickElement(driver, centerInfoPage.lnkCustomerComplaints);
			terminated.modifyCustomerComplaints(driver, config);

			// Modify Documents
			fc.utobj().clickElement(driver, centerInfoPage.lnkDocuments);
			fc.utobj().actionImgOption(driver, docTitle, "Modify");
			docTitle = terminated.modifyDocument(driver, config);

			// Modify Employee
			fc.utobj().clickElement(driver, centerInfoPage.lnkEmployees);
			// fc.utobj().actionImgOption(driver, empName, "Modify");
			empName = terminated.modifyEmployee(driver, config);

			// Modify Entity Details
			fc.utobj().clickElement(driver, centerInfoPage.linkEntityDetail);
			terminated.modifyEntityDetails(driver, config);
			// Modify Events
			fc.utobj().clickElement(driver, centerInfoPage.lnkEvents);
			terminated.modifyEventDetails(driver, config);

			// Modify Financials
			fc.utobj().clickElement(driver, centerInfoPage.lnkFinancial);
			terminated.modifyFinanceDetails(driver, config);

			// Modify Guarantor
			fc.utobj().clickElement(driver, centerInfoPage.lnkGuarantors);
			terminated.modifyGuarantor(driver, config);
			// Modify Insurance
			fc.utobj().clickElement(driver, centerInfoPage.lnkInsurance);
			terminated.modifyInsuranceDetails(driver, config);

			// Modify Legal Violation
			fc.utobj().clickElement(driver, centerInfoPage.lnkLegalViolation);
			terminated.modifyLegalViolation(driver, config);

			// Modify Lenders Details
			fc.utobj().clickElement(driver, centerInfoPage.lnkLenders);
			terminated.modifyLenders(driver, config);

			// Modify Marketing
			fc.utobj().clickElement(driver, centerInfoPage.lnkMarketingFinTab); // Changes
																				// by
																				// harish
																				// Dwivedi
			terminated.modifyMarketingDetails(driver, config);

			// Modify Mystry Review
			fc.utobj().clickElement(driver, centerInfoPage.lnkMysteryReview);
			terminated.modifyMystryReview(driver, config);

			// Modify Owners
			fc.utobj().clickElement(driver, centerInfoPage.tabOwners);
			fc.utobj().actionImgOption(driver, owner.get("FirstName"), "Modify");
			terminated.modifyOwner(driver, config);

			// Modify Pictures
			fc.utobj().clickElement(driver, centerInfoPage.lnkPictures);
			fc.utobj().actionImgOption(driver, pictureTitle, "Modify");
			pictureTitle = terminated.modifyPicture(driver, config);

			// Modify Real state
			fc.utobj().clickElement(driver, centerInfoPage.lnkRealEstate);
			terminated.modifyRealState(driver, config);

			// Modify Territory
			fc.utobj().clickElement(driver, centerInfoPage.lnkTerritory);
			terminated.modifyTerritory(driver, config);

			// Modify Training
			fc.utobj().clickElement(driver, centerInfoPage.lnkTraining);
			terminated.modifyTraining(driver, config);

			// ************************************************* Delete Section
			// ************************************************

			fc.utobj().printTestStep("Delete data from all the tabs !! ");

			terminated.deleteTabData(driver, config, centerInfoPage.lnkAgreement, null);

			terminated.deleteTabData(driver, config, centerInfoPage.lnkContractSigning, null);

			terminated.deleteTabData(driver, config, centerInfoPage.lnkCustomerComplaints, null);

			terminated.deleteTabData(driver, config, centerInfoPage.lnkDocuments, docTitle);

			terminated.deleteTabData(driver, config, centerInfoPage.lnkEmployees, empName);

			terminated.deleteTabData(driver, config, centerInfoPage.linkEntityDetail, null);

			terminated.deleteTabData(driver, config, centerInfoPage.lnkEvents, null);

			terminated.deleteTabData(driver, config, centerInfoPage.lnkFinancial, null);

			terminated.deleteTabData(driver, config, centerInfoPage.guarantorsTab, null);

			terminated.deleteTabData(driver, config, centerInfoPage.insuranceTab, null);

			terminated.deleteTabData(driver, config, centerInfoPage.lnkLegalViolation, null);

			terminated.deleteTabData(driver, config, centerInfoPage.lnkLenders, null);

			terminated.deleteTabData(driver, config, centerInfoPage.lnkMarketingFinTab, null); // Changes
																								// By
																								// harish
																								// Dwivedi

			terminated.deleteTabData(driver, config, centerInfoPage.lnkMysteryReview, null);

			terminated.deleteTabData(driver, config, centerInfoPage.ownersTab, owner.get("FirstName"));

			terminated.deleteTabData(driver, config, centerInfoPage.lnkPictures, pictureTitle);

			terminated.deleteTabData(driver, config, centerInfoPage.lnkRealEstate, null);

			terminated.deleteTabData(driver, config, centerInfoPage.lnkTerritory, null);

			terminated.deleteTabData(driver, config, centerInfoPage.lnkTraining, null);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Verify_InfoMgr_Terminated_Actions_SendMail", testCaseDescription = "Test to verify send mail functionality from action buttons.", reference = {
			"" })
	public void InfoMgr_Terminated_ActionsMenu_Actions_SendMail() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		TerminatedCommonMethods terminated = fc.infomgr().terminated();
		AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();
		InfoMgrTerminatedPage terminatedPage = new InfoMgrTerminatedPage(driver);
		InfoMgrContactHistoryPage contactHistory = terminatedPage.getContactHistoryPage();

		try {
			fc.loginpage().login(driver);

			fc.utobj().printTestStep("Add Franchise Location");
			String franchiseID = adminInfoMgr.addFranchiseLocation(driver);

			terminated.terminateFranchiseLocation(driver, config, franchiseID);

			fc.utobj().printTestStep("Clicking on Send mail link under action image button ");
			terminated.terminatedPageShowAll(driver);
			fc.utobj().actionImgOption(driver, franchiseID, "Send Email");
			String mailSubject = terminated.sendTextEmail(driver, config, false);
			terminated.searchFranchiseAndClick(driver, franchiseID);
			fc.utobj().clickElement(driver, contactHistory.lnkContactHistory);
			boolean assertMailSubjectonPage = fc.utobj().assertPageSource(driver, mailSubject);
			if (assertMailSubjectonPage) {
				Reporter.log("Text mail has been sent successfully");
			} else {
				fc.utobj().throwsException("Text mail has not  been sent .");
			}

			fc.utobj().printTestStep("Send mail from action button displayed at the bottom of the page");
			terminated.terminatedPageShowAll(driver);
			terminated.checkLocation(driver, franchiseID);
			fc.utobj().clickElement(driver, terminatedPage.btnSendMail);
			String mailSubject2 = terminated.sendTextEmail(driver, config, false);
			terminated.searchFranchiseAndClick(driver, franchiseID);
			fc.utobj().clickElement(driver, contactHistory.lnkContactHistory);
			boolean assertMailSubjectonPage2 = fc.utobj().assertPageSource(driver, mailSubject2);
			if (assertMailSubjectonPage2) {
				Reporter.log("Text mail has been sent successfully");
			} else {
				fc.utobj().throwsException("Text mail has not  been sent .");
			}

			/*
			 * fc.utobj().printTestStep(testCaseId, "Click on Send Email button"
			 * ); terminated.terminatedPageShowAll(driver);
			 * terminated.checkLocation(driver, franchiseID);
			 * fc.utobj().selectActionMenuItemsWithTagA(driver, "Send Email");
			 * String mailSubject3 = terminated.sendTextEmail(driver, config ,
			 * false); terminated.searchFranchiseAndClick(driver, franchiseID);
			 * fc.utobj().clickElement(driver,
			 * contactHistory.lnkContactHistory); boolean
			 * assertMailSubjectonPage3 = fc.utobj().assertPageSource(driver,
			 * mailSubject3); if(assertMailSubjectonPage3) { Reporter.log(
			 * "Text mail has been sent successfully"); } else {
			 * fc.utobj().throwsException("Text mail has not  been sent ."); }
			 */

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Verify_InfoMgr_Terminated_Actions_LogaTask", testCaseDescription = "Test to verify Log a task functionality from action buttons ", reference = {
			"" })
	public void InfoMgr_Terminated_PageBottom_Actions_LogaTask() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		TerminatedCommonMethods terminated = fc.infomgr().terminated();
		AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();

		InfoMgrTerminatedPage terminatedPage = new InfoMgrTerminatedPage(driver);
		InfoMgrContactHistoryPage contactHistoryPage = terminatedPage.getContactHistoryPage();

		try {
			fc.loginpage().login(driver);
			// adminInfoMgr.configureAllFieldsOnManageFormGeneratorPage(driver ,
			// config);

			fc.utobj().printTestStep("Add corporate user");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail("infomgrautomation@staffex.com");
			corpUser = corpTest.createDefaultUser(driver, corpUser);

			fc.utobj().printTestStep("Add Franchise Location");
			String franchiseID = adminInfoMgr.addFranchiseLocation(driver);

			terminated.terminateFranchiseLocation(driver, config, franchiseID);

			fc.utobj().printTestStep("Click on Log a Task button displayed at the bottom of the page");
			terminated.terminatedPageShowAll(driver);
			terminated.checkLocation(driver, franchiseID);
			fc.utobj().clickElement(driver, terminatedPage.btnLogATask);
			String taskSubject1 = terminated.logaTask(driver, config, corpUser.getuserFullName(), false);
			terminated.searchFranchiseAndClick(driver, franchiseID);
			fc.utobj().clickElement(driver, contactHistoryPage.lnkContactHistory);
			boolean assertTaskSubjectonPage1 = fc.utobj().assertPageSource(driver, taskSubject1);
			if (assertTaskSubjectonPage1) {
				Reporter.log("Log a Task has been done successfully");
			} else {
				fc.utobj().throwsException("Log a Task failed .");
			}

			fc.utobj().printTestStep("Log a task from action image icon");
			terminated.terminatedPageShowAll(driver);
			fc.utobj().actionImgOption(driver, franchiseID, "Log a Task");
			String taskSubject2 = terminated.logaTask(driver, config, corpUser.getuserFullName(), false);
			terminated.searchFranchiseAndClick(driver, franchiseID);
			fc.utobj().clickElement(driver, contactHistoryPage.lnkContactHistory);
			boolean assertTaskSubjectonPage2 = fc.utobj().assertPageSource(driver, taskSubject2);
			if (assertTaskSubjectonPage2) {
				Reporter.log("Log a Task has been done successfully");
			} else {
				fc.utobj().throwsException("Log a Task failed .");
			}

			/*
			 * terminated.terminatedPageShowAll(driver);
			 * fc.utobj().printTestStep(testCaseId,
			 * "Click on Log a Task link displayed under the action button click at the top of the page"
			 * ); terminated.terminatedPageShowAll(driver);
			 * terminated.checkLocation(driver, franchiseID);
			 * fc.utobj().selectActionMenuItemsWithTagA(driver, "Log a Task");
			 * String taskSubject3 = terminated.logaTask(driver, config,
			 * corporateUser , false);
			 * terminated.searchFranchiseAndClick(driver, franchiseID);
			 * fc.utobj().clickElement(driver,
			 * contactHistoryPage.lnkContactHistory); boolean
			 * assertTaskSubjectonPage3 = fc.utobj().assertPageSource(driver,
			 * taskSubject3); if(assertTaskSubjectonPage3) { Reporter.log(
			 * "Log a Task has been done successfully"); } else {
			 * fc.utobj().throwsException("Log a Task failed ."); }
			 */

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Verify_InfoMgr_Terminated_Actions_LogaCall", testCaseDescription = "Test to verify log a call functionality from action buttons", reference = {
			"" })
	public void InfoMgr_Terminated_Actions_LogaCall() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		TerminatedCommonMethods terminated = fc.infomgr().terminated();
		AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();

		InfoMgrTerminatedPage terminatedPage = new InfoMgrTerminatedPage(driver);
		InfoMgrContactHistoryPage contactHistoryPage = terminatedPage.getContactHistoryPage();
		try {
			fc.loginpage().login(driver);
			// adminInfoMgr.configureAllFieldsOnManageFormGeneratorPage(driver ,
			// config);

			String franchiseID = adminInfoMgr.addFranchiseLocation(driver);

			terminated.terminateFranchiseLocation(driver, config, franchiseID);

			fc.utobj().printTestStep("Click on Log a Call link displayed under the top action button");
			terminated.terminatedPageShowAll(driver);
			terminated.checkLocation(driver, franchiseID);
			fc.utobj().selectActionMenuItemsWithTagA(driver, "Log a Call");
			String logACallSubject3 = terminated.logACall(driver, config, false);
			terminated.searchFranchiseAndClick(driver, franchiseID);
			fc.utobj().clickElement(driver, contactHistoryPage.lnkContactHistory);
			boolean assertCallTextOnPage3 = fc.utobj().assertPageSource(driver, logACallSubject3);
			if (assertCallTextOnPage3) {
				Reporter.log("Log a Call has been done successfully");
			} else {
				fc.utobj().throwsException("Log a Call has failed .");
			}

			fc.utobj().printTestStep("Click on Log a Call button displayed at the bottom of the page ");
			terminated.terminatedPageShowAll(driver);
			terminated.checkLocation(driver, franchiseID);
			fc.utobj().clickElement(driver, terminatedPage.btnLogACall);
			String logACallSubject1 = terminated.logACall(driver, config, false);
			terminated.searchFranchiseAndClick(driver, franchiseID);
			fc.utobj().clickElement(driver, contactHistoryPage.lnkContactHistory);
			boolean assertCallTextOnPage1 = fc.utobj().assertPageSource(driver, logACallSubject1);
			if (assertCallTextOnPage1) {
				Reporter.log("Log a Call has been done successfully");
			} else {
				fc.utobj().throwsException("Log a Call has failed .");
			}

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Log a call from action menu");
			 * terminated.terminatedPageShowAll(driver);
			 * fc.utobj().actionImgOption(driver, franchiseID, "Log a Call");
			 * String logACallSubject2 = terminated.logACall(driver, config ,
			 * false); terminated.searchFranchiseAndClick(driver, franchiseID);
			 * fc.utobj().clickElement(driver,
			 * contactHistoryPage.lnkContactHistory); boolean
			 * assertCallTextOnPage2 = fc.utobj().assertPageSource(driver,
			 * logACallSubject2); if(assertCallTextOnPage2) { Reporter.log(
			 * "Log a Call has been done successfully"); } else {
			 * fc.utobj().throwsException("Log a Call has failed ."); }
			 */

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" })
	
	//Updated Script by Govind

		
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-10-26", testCaseId = "TC_Verify_InfoMgr_Terminated_Actions_AddToGroup", testCaseDescription = "Test to verify Add to Group functionality from action buttons", reference = {
			"" })
	public void InfoMgr_Terminated_Actions_AddToGroup() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		TerminatedCommonMethods terminated = fc.infomgr().terminated();
		AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();

		InfoMgrTerminatedPage terminatedPage = new InfoMgrTerminatedPage(driver);

		try {
			fc.loginpage().login(driver);

			String franchiseID = adminInfoMgr.addFranchiseLocation(driver);

			terminated.terminateFranchiseLocation(driver, config, franchiseID);

			fc.utobj().printTestStep("Click on Add to Group button displayed at the bottom of the page");
			terminated.terminatedPageShowAll(driver);
			terminated.checkLocation(driver, franchiseID);
			fc.utobj().clickElement(driver, terminatedPage.btnAddtoGroup);
			terminated.addToGroup(driver, config);

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Click on Add to Group link displayed under Action button displayed at the top of the page"
			 * ); terminated.terminatedPageShowAll(driver);
			 * terminated.checkLocation(driver, franchiseID);
			 * fc.utobj().selectActionMenuItemsWithTagA(driver, "Add To Group");
			 * terminated.addToGroup(driver, config);
			 */

			fc.utobj().printTestStep("Click on Add to Group button  displayed at the bottom of the page");
			terminated.terminatedPageShowAll(driver);
			terminated.checkLocation(driver, franchiseID);
			fc.utobj().clickElement(driver, terminatedPage.btnAddtoGroup);
			terminated.addToGroup(driver, config);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Verify_InfoMgr_Terminated_Actions_Print", testCaseDescription = "Test to verify print functionality from action buttons", reference = {
			"" })
	public void InfoMgr_Terminated_Actions_Print() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		TerminatedCommonMethods terminated = fc.infomgr().terminated();
		AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();
		InfoMgrTerminatedPage terminatedPage = new InfoMgrTerminatedPage(driver);

		try {
			fc.loginpage().login(driver);

			// adminInfoMgr.configureInfoMgr(driver, config);

			String franchiseID = adminInfoMgr.addFranchiseLocation(driver);
			terminated.terminateFranchiseLocation(driver, config, franchiseID);

			fc.utobj().printTestStep("Click on Print button displayed at the bottom of the page");
			terminated.terminatedPageShowAll(driver);
			fc.utobj().clickElement(driver, terminatedPage.btnPrint);
			terminated.VerifyPrintPreview(driver, franchiseID);

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Click on Print link displayed under Action button displayed at the top of the page"
			 * ); fc.utobj().selectActionMenuItemsWithTagA(driver, "Print");
			 * terminated.VerifyPrintPreview(driver, franchiseID);
			 */

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Verify_InfoMgr_Terminated_Actions_Modify", testCaseDescription = "Test to verify modify functionality from action buttons", reference = {
			"" })
	public void InfoMgr_Terminated_Actions_Modify() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		TerminatedCommonMethods terminated = fc.infomgr().terminated();
		AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();

		try {
			fc.loginpage().login(driver);
			// adminInfoMgr.configureAllFieldsOnManageFormGeneratorPage(driver ,
			// config);

			String franchiseID = adminInfoMgr.addFranchiseLocation(driver);
			terminated.terminateFranchiseLocation(driver, config, franchiseID);

			fc.utobj().printTestStep("Modify from action menu");
			terminated.terminatedPageShowAll(driver);
			fc.utobj().actionImgOption(driver, franchiseID, "Modify");
			terminated.modifyCenterInfo(driver, config);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr","Summary_Filter" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Verify_InfoMgr_Terminated_Filter", testCaseDescription = "Test to verify filter displayed on the terminated Page", reference = {
			"" })
	public void InfoMgr_Terminated_Filter_Verify() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		TerminatedCommonMethods terminated = fc.infomgr().terminated();
		RegionalCommonMethods regional = new RegionalCommonMethods();
		InfoMgrModulePage infomudule = new InfoMgrModulePage(driver);
		AdminInfoMgrCommonMethods adminInfoMgr = new AdminInfoMgrCommonMethods();
		InfoMgrCorporateLocationsPage corporateLocationsPage = new InfoMgrCorporateLocationsPage(driver);
		InfoMgrContactHistoryPage contactHistoryPage = corporateLocationsPage.getContactHistoryPage();
		try {
			fc.loginpage().login(driver);

			InfoMgrFranchiseeFilter franchisee = adminInfoMgr.addFranchiseLocationForFilter(driver, false);
			/* Harish Dwivedi InfoMgr */
			/*
			 * terminated.terminateFranchiseLocation(driver, config,
			 * franchisee.getFranchiseeID());
			 * 
			 * terminated.terminatedPageShowAll(driver);
			 */

			fc.utobj().printTestStep("Search franchise and click");
			searchFranchise(driver, (String) franchisee.getFranchiseeID());

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,("//*[@id='siteMainTable']//a[text()='" + (String) franchisee.getFranchiseeID() + "']")));

			fc.utobj().printTestStep("Terminating franchise location ");
			// Add Default and Termination Report Details
			fc.utobj().clickElement(driver, contactHistoryPage.defaultTerminationTab);
			// franchisees.terminateFranchiseLocation(driver,
			// config,franchisee.getFranchiseeID());

			InfoMgrDefaultAndTerminationPage defaultAndTerminationPage = new InfoMgrDefaultAndTerminationPage(driver);

			try {

				fc.utobj().sendKeys(driver, defaultAndTerminationPage.txtCommitteApprovedDate,
						getFutureDateUSFormat("USA", 0));
				fc.utobj().sendKeys(driver, defaultAndTerminationPage.txtDateTerminated,
						getFutureDateUSFormat("USA", 0));
				fc.utobj().selectDropDownByVisibleText(driver, defaultAndTerminationPage.drpReason, "Abandonment");
				fc.utobj().selectDropDownByVisibleText(driver, defaultAndTerminationPage.drpTypeOfAction,
						"Termination");
				fc.utobj().clickElement(driver, defaultAndTerminationPage.btnSubmit);
				fc.utobj().acceptAlertBox(driver);
			} catch (Exception e) {
				fc.utobj().throwsException("Default and Termination is not saving properly.");
			}

			fc.utobj().clickElement(driver, infomudule.terminatedTab);
			fc.utobj().printTestStep("Search Termination franchise and click");
			searchterminatedFranchise(driver, (String) franchisee.getFranchiseeID());

			fc.utobj().actionImgOption(driver, franchisee.getFranchiseeID(), "Modify");

			fc.utobj().printTestStep("Adding spouse details by modifying the owner");
			InfoMgrOwnersPage ownerPage = new InfoMgrTerminatedPage(driver).getOwnersPage();
			fc.utobj().clickElement(driver, ownerPage.ownersTab);
			fc.utobj().actionImgOption(driver, franchisee.getOwnerFirstName(), "Modify");
			Map<String, String> lstOwner = terminated.modifyOwner(driver, config);
			franchisee.setSpouseFirstName(lstOwner.get("SpouseFirstName"));

			fc.utobj().printTestStep("Adding area owner");
			InfoMgrOwnersPage regionalOwnerPage = new InfoMgrRegionalPage(driver).getOwnersPage();
			regional.searchAndClickAreaRegion(driver, franchisee.getAreaRegionName());
			fc.utobj().clickElement(driver, regionalOwnerPage.lnkAreaOwner);
			lstOwner = regional.addAreaRegionOwner(driver);
			franchisee.setAreaRegionOwner(lstOwner.get("FirstName"));

			fc.utobj().printTestStep("Verify Search Filter");
			fc.infomgr().infomgr_common().InfoMgrTerminated(driver);
			terminated.search(driver, config, franchisee);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-07", testCaseId = "TC_Verify_InfoMgr_Terminated_AddToCenterInfo_Fields", testCaseDescription = "Test to verify fileds added by manage form generator  to center info", reference = {
			"" })
	public void InfoMgr_Terminated_CenterInfo_AddToCenterInfo_Fields() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		TerminatedCommonMethods terminated = new TerminatedCommonMethods();
		AdminInfoMgrCommonMethods adminInfoMgr = new AdminInfoMgrCommonMethods();
		String[] formsArray = { "Agreement", "Contract Signing", "Customer Complaints", "Default and Termination",
				"Employees", "Entity Details", "Events", "Financial", "Guarantors", "Insurance", "Legal Violation",
				"Lenders", "Marketing", "Mystery Review", "Owners", "Real Estate", "Renewal", "Territory", "Training",
				"Transfer" };
		try {
			fc.loginpage().login(driver);
			fc.utobj().printTestStep("Add Fileds to Center Info");
			Map<String, String> dsCenterInfoFields = adminInfoMgr.configure_Franchisees_AddToCenterInfo_AllForms(driver,
					config);

			fc.utobj().printTestStep("Adding franchise Location");
			String franchiseID = adminInfoMgr.addFranchiseLocation(driver);

			fc.utobj().printTestStep("Terminate franchise location");
			terminated.terminateFranchiseLocation(driver, config, franchiseID);

			terminated.searchFranchiseAndClick(driver, franchiseID);
			terminated.modifyCenterInfo(driver, config);
			fc.utobj().printTestStep("Verify added fields on center info page");
			for (int i = 0; i < formsArray.length; i++) {
				try {
					fc.utobj().getElementByXpath(driver, ".//*[@id='centerInfoDIV']//td[contains(text() , '"
							+ dsCenterInfoFields.get(formsArray[i]) + "')]");
					Reporter.log(
							"Field " + dsCenterInfoFields.get(formsArray[i]) + " is displayed on the Center Info page");
				} catch (Exception ex) {
					fc.utobj().throwsException("Field " + dsCenterInfoFields.get(formsArray[i])
							+ " is not displayed on the Center Info page");
				}
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgr_test1" }, priority = 3)

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Verify_InfoMgr_Terminated_CenterInfo_CustomFieldsandSection", testCaseDescription = "Test to verify custom fields added to center info by manage form generator.", reference = {
			"" })
	public void InfoMgr_Terminated_CenterInfo_CustomFields() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		TerminatedCommonMethods terminated = fc.infomgr().terminated();
		AdminInfoMgrCommonMethods adminInfoMgr = new AdminInfoMgrCommonMethods();

		try {
			fc.loginpage().login(driver);

			fc.utobj().printTestStep("Add custom fields to Center Info");
			Map<String, String> dsCenterInfoCustomFields = adminInfoMgr.ManageFormGenerator_Add_Section_Fields_To_Forms(
					driver, config, "Franchisee", "Center Info", "ter");

			fc.utobj().printTestStep("Adding franchise Location");
			String franchiseID = adminInfoMgr.addFranchiseLocation(driver);

			fc.utobj().printTestStep("Terminate franchise location");
			terminated.terminateFranchiseLocation(driver, config, franchiseID);

			terminated.searchFranchiseAndClick(driver, franchiseID);
			fc.utobj().printTestStep("Verify that added custom fields are displayed on the center info page");

			for (String key : dsCenterInfoCustomFields.keySet()) {
				if (!key.contains("drpOption")) {
					if (fc.utobj().assertPageSource(driver, dsCenterInfoCustomFields.get(key))) {
						Reporter.log(
								"Custom field " + dsCenterInfoCustomFields.get(key) + " is displayed on the page !!");
					} else {
						fc.utobj().throwsException(
								"Custom field " + dsCenterInfoCustomFields.get(key) + " is not displayed on the page");
					}
				}
			}

			fc.utobj().printTestStep("Deleting the created section and its fields");
			adminInfoMgr.deleteSectionandFields(driver, config, "Franchisee", "Center Info", dsCenterInfoCustomFields);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/* Harish Dwivedi InfoMgr */
	public void searchFranchise(WebDriver driver, String franchiseID) throws Exception {
		Reporter.log("********************** Search Franchisee ****************** \n");

		fc.infomgr().infomgr_common().InfoMgrFranchisees(driver);

		// Search Franchisee and click on the franchiseID
		InfoMgrFranchiseesPage franchiseePage = new InfoMgrFranchiseesPage(driver);

		fc.infomgr().infomgr_common().searchLocationFromShowFilter(driver, franchiseID);

	}

	public void searchterminatedFranchise(WebDriver driver, String franchiseID) throws Exception {
		Reporter.log("********************** Search Franchisee ****************** \n");

		fc.infomgr().infomgr_common().InfoMgrTerminated(driver);

		// Search Franchisee and click on the franchiseID
		InfoMgrFranchiseesPage franchiseePage = new InfoMgrFranchiseesPage(driver);

		fc.infomgr().infomgr_common().searchLocationFromShowFilter(driver, franchiseID);

	}

	public String getFutureDateUSFormat(String country, int afterNoOfDays) {

		String format = "MM/dd/yyyy";
		if ("AUS".equals(country)) {
			format = "dd/MM/yyyy";
		}

		SimpleDateFormat sdfDate = new SimpleDateFormat("" + format);
		Date now = DateUtils.addDays(new Date(), afterNoOfDays);
		String strDate = sdfDate.format(now);
		return strDate;
	}
}
