package com.builds.test.infomgr;
import org.testng.Assert;
import org.testng.AssertJUnit;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Reporter;
import com.builds.test.common.FCHomePageTest;
import com.builds.uimaps.admin.AdminPage;
import com.builds.uimaps.common.FCHomePage;
import com.builds.uimaps.infomgr.AdminInfoMgrTriggersAndAuditingPage;
import com.builds.uimaps.infomgr.InfoMgrAuditHistoryPage;
import com.builds.uimaps.infomgr.InfoMgrDashboardPage;
import com.builds.utilities.FranconnectUtil;

public class InfoMgr_Common extends FranconnectUtil {
	public static String staffexEmail = "infomgrautomation@staffex.com";
	public static String stateAddendum = "infomgrautomation@staffex.com";
	FCHomePageTest fchomepage = new FCHomePageTest();

	public void fimModule(WebDriver driver) throws Exception {
		

		Reporter.log("Navigating to InfoMgr...");
		/*try {
			Thread.sleep(2000);;
			utobj().clickElement(driver, pobj.fimModule);
		} catch (Exception e) {
			Thread.sleep(2000);
			utobj().clickElement(driver, pobj.moreLink);
			Thread.sleep(2000);
			utobj().clickElement(driver, pobj.fimModule);
		}*/
		fchomepage.openFIMModule(driver);
		
		Reporter.log("Navigated to Information Manager Module...");
	}

	private void navigateToAdmin(WebDriver driver) throws Exception {

		Reporter.log("Navigating to admin ....");
		FCHomePage pobj = new FCHomePage(driver);
		try {
			utobj().clickElement(driver, pobj.userOptions);
			utobj().clickElement(driver, pobj.adminLink);
		} catch (Exception E) {
			utobj().clickElement(driver, pobj.userOptions);
			utobj().clickElement(driver, pobj.adminLink);
		}
		Reporter.log("Navigated to admin ....");
	}

	public void adminFimTriggersAndAuditing(WebDriver driver) throws Exception {

		navigateToAdmin(driver);
		Reporter.log("Navigating to admin - > Fim - > TriggersAndAuditing...");
		AdminPage pobj = new AdminPage(driver);
		utobj().clickElementByJS(driver, pobj.triggersAndAuditingLnk);
		Reporter.log("Navigated to admin - > Fim - > TriggersAndAuditing...");
	}

	public void adminFimConfigureOwnershipTransferStatusPage(WebDriver driver) throws Exception {

		navigateToAdmin(driver);
		Reporter.log("Navigating to admin - > Fim - > ConfigureOwnershipTransferStatus...");
		AdminPage pobj = new AdminPage(driver);
		utobj().clickElementByJS(driver, pobj.configureOwnershipTransferStatusLnk);
		Reporter.log("Navigatied to admin - > Fim - > ConfigureOwnershipTransferStatus...");
	}

	public void adminFimConfigureOtherAddress(WebDriver driver) throws Exception {

		navigateToAdmin(driver);
		Reporter.log("Navigating to adminFimConfigureOtherAddress...");
		AdminPage pobj = new AdminPage(driver);
		home_page().utobj().clickElementByJS(driver, pobj.configureOtherAddressesHeadingLnk);
		Reporter.log("Navigated to adminFimConfigureOtherAddress...");

	}

	public void adminConfigureAreaRegionLabel(WebDriver driver) throws Exception {

		navigateToAdmin(driver);
		Reporter.log("Navigating to Configure Area / Region Label...");
		AdminPage pobj = new AdminPage(driver);
		home_page().utobj().clickElementByJS(driver, pobj.configureAreaRegionLblLnk);
		Reporter.log("Navigated to Configure Area / Region Label...");
	}

	public void InfoMgrDashboard(WebDriver driver) throws Exception {
		fimModule(driver);
		InfoMgrDashboardPage imdp = new InfoMgrDashboardPage(driver);
		if (!utobj().isElementPresent(driver, imdp.FranchiseSystemSummaryRptWidget)) {
			Reporter.log("Navigating to InfoMgrDashbnoard...");
			FCHomePage pobj = new FCHomePage(driver);
			home_page().utobj().clickElementByJS(driver, pobj.Dashboard);
			Reporter.log("Navigated to Info Mgr Franchisees...");
		} else {
			Reporter.log("Info Mgr Dashboard is already Opened...");
		}
	}

	public void InfoMgrFranchisees(WebDriver driver) throws Exception {

		fimModule(driver);
		Reporter.log("Navigating to InfoMgrFranchisees...");
		FCHomePage pobj = new FCHomePage(driver);
		home_page().utobj().clickElementByJS(driver, pobj.franchisees);
		Reporter.log("Navigated to Info Mgr Franchisees...");
	}
	
	public void InfoMgrFranchiseeSubMenu(WebDriver driver) throws Exception {

		Reporter.log("Navigating to InfoMgrFranchisees...");
		FCHomePage pobj = new FCHomePage(driver);
		home_page().utobj().clickElementByJS(driver, pobj.franchisees);
		Reporter.log("Navigated to Info Mgr Franchisees...");
	}

	public void InfoMgrInDevelopment(WebDriver driver) throws Exception {

		fimModule(driver);
		Reporter.log("Navigating to InfoMgr InDevelopment...");
		FCHomePage pobj = new FCHomePage(driver);
		utobj().clickElementByJS(driver, pobj.inDevelopment);
		Reporter.log("Navigated to Info Mgr InDevelopment...");
	}

	public void InfoMgrTerminated(WebDriver driver) throws Exception {

		fimModule(driver);
		Reporter.log("Navigating to InfoMgrTerminated...");
		FCHomePage pobj = new FCHomePage(driver);
		utobj().clickElementByJS(driver, pobj.terminated);
		Reporter.log("Navigating to Info Mgr Terminated...");
	}

	public void InfoMgrCorporateLocations(WebDriver driver) throws Exception {

		Reporter.log("Navigating to InfoMgrCorporateLocations...");
		fimModule(driver);
		FCHomePage pobj = new FCHomePage(driver);
		utobj().clickElementByJS(driver, pobj.corporateLocations);
		Reporter.log("Navigated to Info Mgr Corporate Locations...");
	}

	public void InfoMgrExport(WebDriver driver) throws Exception {

		Reporter.log("Navigating to ExportCorporateLocations...");
		fimModule(driver);
		FCHomePage pobj = new FCHomePage(driver);
		utobj().clickElementByJS(driver, pobj.exportpage);
		Reporter.log("Navigated to Info Mgr Export Page...");
	}

	public void InfoMgrGroups(WebDriver driver) throws Exception {

		fimModule(driver);
		Reporter.log("Navigating to InfoMgrGroups...");
		FCHomePage pobj = new FCHomePage(driver);
		utobj().clickElementByJS(driver, pobj.infoMgrGroup);
		Reporter.log("Navigated to Info Mgr Groups ..............");
	}

	public void InfoMgrTasks(WebDriver driver) throws Exception {

		fimModule(driver);
		Reporter.log("Navigating to InfoMgrTasks...");
		FCHomePage pobj = new FCHomePage(driver);
		home_page().utobj().clickElementByJS(driver, pobj.infoMgrTask);
		Reporter.log("Navigated to Info Mgr Tasks ..............");
	}

	public void InfoMgrCampaignCenter(WebDriver driver) throws Exception {

		fimModule(driver);
		Reporter.log("Navigating to InfoMgrCampaignCenter...");
		FCHomePage pobj = new FCHomePage(driver);
		home_page().utobj().clickElementByJS(driver, pobj.infoMgrCampaignCenter);
		Reporter.log("Navigated to Info Mgr CampaignCenter ..............");
	}

	public void InfoMgrMultiUnitEntity(WebDriver driver) throws Exception {

		fimModule(driver);
		Reporter.log("Navigating to InfoMgrMultiUnitEntity...");
		FCHomePage pobj = new FCHomePage(driver);
		utobj().clickElementByJS(driver, pobj.multiUnitEntity);
		Reporter.log("Navigated to Info Mgr MultiUnitEntity...");
	}
	
	public void InfoMgrMultiUnit(WebDriver driver) throws Exception {

		fimModule(driver);
		Reporter.log("Navigating to InfoMgrMultiUnit...");
		FCHomePage pobj = new FCHomePage(driver);
		utobj().clickElementByJS(driver, pobj.multiUnitEntity);
		boolean MU = utobj().assertPageSource(driver, "Change to Multi-Unit View");
		if(MU)
		{
			utobj().clickLinkIgnoreCase(driver, "Change to Multi-Unit View");
			utobj().sleep(2000);
		}
		Reporter.log("Navigated to Info Mgr MultiUnit Submenu...");
	}
	
	public void InfoMgrEntity(WebDriver driver) throws Exception {

		fimModule(driver);
		Reporter.log("Navigating to InfoMgrEntity...");
		FCHomePage pobj = new FCHomePage(driver);
		utobj().clickElementByJS(driver, pobj.multiUnitEntity);
		boolean Entity = utobj().assertPageSource(driver, "Change to Entity View");
		if(Entity)
		{
			utobj().sleep(2000);
			utobj().clickLinkIgnoreCase(driver, "Change to Entity View");
		}
		Reporter.log("Navigated to Info Mgr Entity Submenu...");
	}

	public void InfoMgrRegional(WebDriver driver) throws Exception {

		fimModule(driver);
		Reporter.log("Navigating to Info Mgr Regional...");
		FCHomePage pobj = new FCHomePage(driver);
		utobj().clickElementByJS(driver, pobj.regional);
		Reporter.log("Navigated to Info Mgr Regional...");

	}

	public void InfoMgrSearch(WebDriver driver) throws Exception {

		fimModule(driver);
		Reporter.log("Navigating to InfoMgrSearch..");
		FCHomePage pobj = new FCHomePage(driver);
		utobj().clickElementByJS(driver, pobj.infoMgrSearch);
		Reporter.log("Navigated to Info Mgr Search...");
	}

	public void InfoMgrReports(WebDriver driver) throws Exception {

		fimModule(driver);
		Reporter.log("Navigating to InfoMgrReports...");
		FCHomePage pobj = new FCHomePage(driver);
		utobj().clickElementByJS(driver, pobj.infoMgrReports);
		Reporter.log("Navigated to Info Mgr Reports...");
	}

	public void InfoMgrFDD(WebDriver driver) throws Exception {

		fimModule(driver);
		Reporter.log("Navigating to InfoMgrFDD...");
		FCHomePage pobj = new FCHomePage(driver);
		utobj().clickElementByJS(driver, pobj.fDD);
		Reporter.log("Navigated to Info Mgr FDD...");
	}

	public void AdminFIMManageFormGenerator(WebDriver driver) throws Exception {

		navigateToAdmin(driver);
		Reporter.log("Navigating to InfoMgrManageFormGenerator...");
		AdminPage pobj = new AdminPage(driver);
		utobj().clickElementByJS(driver, pobj.infoMgrManageWebFormGeneratorLink);
		Reporter.log("Navigated to InfoMgrManageFormGenerator...");
	}

	public void AdminInfoManageWebFormGenerator(WebDriver driver) throws Exception {

		navigateToAdmin(driver);
		Reporter.log("Navigating to InfoMgrManageWebFormGenerator...");
		AdminPage pobj = new AdminPage(driver);
		utobj().clickElementByJS(driver, pobj.manageWebFormGeneratorLink);
		Reporter.log("Navigated to InfoMgrManageWebFormGenerator...");
	}

	public void adminFDDManagement(WebDriver driver) throws Exception {

		navigateToAdmin(driver);
		Reporter.log("Navigating to fddManagementLnk...");
		AdminPage pobj = new AdminPage(driver);
		utobj().clickElementByJS(driver, pobj.fddManagementLnk);
		Reporter.log("Navigated to fddManagementLnk...");
	}

	public void adminConfigureInfoMgrTabs(WebDriver driver) throws Exception {

		navigateToAdmin(driver);
		Reporter.log("Navigating to ConfigureInfoMgrTabs...");
		AdminPage pobj = new AdminPage(driver);
		utobj().clickElementByJS(driver, pobj.configureInfoMgrTabsLnk);
		Reporter.log("Navigated to ConfigureInfoMgrTabs...");
	}

	public void searchLocationFromShowFilter(WebDriver driver, String franchiseID) throws Exception {
		boolean isFilterOpen = false;
		try {
			utobj().sleep();
			//utobj().clickLink(driver, "Show Filters");
			
			isFilterOpen = utobj().getElementByID(driver, "ms-parentfilterfranchiseeNo").isDisplayed();
		} catch (Exception e) {
			 isFilterOpen = false;
		}

		if(isFilterOpen==false) {
			try {
				//utobj().sleep();
			//utobj().clickLink(driver, "Show Filters");
				utobj().clickElement(driver, utobj().getElementByXpath(driver, ".//a[.='Show Filters']"));
			} catch (Exception e) {
				utobj().throwsException("Unable to click on Show Filter");
			}
		}

		utobj().selectValFromMultiSelect(driver, utobj().getElementByID(driver, "ms-parentfilterfranchiseeNo"), franchiseID);
		utobj().clickElement(driver, utobj().getElementByID(driver, "search"));

	}
	public void searchLocationFromShowFilterViaRegion(WebDriver driver, String regionName) throws Exception {
		boolean isFilterOpen = false;
		try {
			utobj().sleep();
			utobj().clickLink(driver, "Show Filters");
			isFilterOpen = utobj().getElementByID(driver, "ms-parentareaId").isDisplayed();
		} catch (Exception e) {

		}

		if (isFilterOpen == false) {
			try {
				utobj().sleep();
				utobj().clickLink(driver, "Show Filters");
			} catch (Exception e) {

			}
		}

		utobj().selectValFromMultiSelect(driver, utobj().getElementByID(driver, "ms-parentareaId"), regionName);
		utobj().clickElement(driver, utobj().getElementByID(driver, "search"));
	}

	public AgreementPage InfoMgrFranchiseeAgreement(WebDriver driver) throws Exception {
		utobj().printTestStep("Click On Agreement Tab");
		utobj().clickElement(driver, ".//a[@href='fimFranchiseAgreement']");
		return new AgreementPage();
	}

	public void InfoMgrFranchiseeCenterInfo(WebDriver driver) throws Exception {
		utobj().printTestStep("CenterInfo Tab Clicked");
		utobj().clickElement(driver, ".//a[@qat_tabname='Center Info']");

	}

	public void InfoMgrFranchiseeContactHistory(WebDriver driver) throws Exception {
		utobj().clickElement(driver, ".//a[@qat_tabname='Contact History']");
		utobj().printTestStep("CenterInfo Tab Clicked");
	}

	public AgreementPage InfoMgrFranchiseeDefaultandTermination(WebDriver driver) throws Exception {
		utobj().printTestStep("Click On Default and Termination Tab");
		utobj().clickElement(driver, ".//a[@href='fimDefaultAndTermination']");
		return new AgreementPage();
	}

	public Agreement getDefaultDataFor_FranchiseeAgreementTab(Agreement agreement) throws Exception {
		agreement.setApprovedDate(utobj().getFutureDateUSFormat(-2));
		agreement.setDateExecuted(utobj().getCurrentDateUSFormat());
		agreement.setEffectiveDate(utobj().getCurrentDateUSFormat());
		agreement.setTermExpirationDate(utobj().getCurrentDateUSFormat());
		agreement.setClosingDate(utobj().getCurrentDateUSFormat());
		agreement.setStateAddendum(utobj().generateTestData("StateAddendum"));
		agreement.setOtherAddendum(utobj().generateTestData("OtherAddendum"));
		agreement.setRightsofFirstRefusalYes("Yes");
		// agreement.setRightsofFirstRefusalNo("NO");
		agreement.setProtectedTerritory(utobj().generateTestData("PT_"));
		agreement.setSalesperson(utobj().generateTestData("SalesP"));
		agreement.setPreviousLicenseNumber("123456");
		agreement.setRelatedCenter("AG_RelatedCenter");
		agreement.setDateCenterSold(utobj().getCurrentDateUSFormat());
		agreement.setRequiredOpeningDate(utobj().getCurrentDateUSFormat());
		agreement.setComments("Agreement_Comment");
		agreement.setInitialTerm("InitialTerms");

		agreement.setRenewalTermFirst("RenewalTerm1");
		agreement.setRenewalDueDateFirst(utobj().getCurrentDateUSFormat());
		agreement.setRenewalFeeFirst("100");
		agreement.setRenewalFeePaidFirstDate(utobj().getCurrentDateUSFormat());

		agreement.setRenewalTermSecond("RenewalTerm2");
		agreement.setRenewalDueDateSecond(utobj().getCurrentDateUSFormat());
		agreement.setRenewalFeeSecond("200");
		agreement.setRenewalFeePaidSecondDate(utobj().getCurrentDateUSFormat());

		agreement.setRenewalTermThird("RenewalTerm3");
		agreement.setRenewalDueDateThird(utobj().getCurrentDateUSFormat());
		agreement.setRenewalFeeThird("300");
		agreement.setRenewalFeePaidThirdDate(utobj().getCurrentDateUSFormat());

		agreement.setContactTitle("Mr.");
		agreement.setFirstName(utobj().generateTestData("AG_FN_"));
		agreement.setLastName(utobj().generateTestData("AG_LN_"));
		agreement.setStreetAddress(("AG_St_Address<Br>C -94 <Br>Sector 8 Noida<Br>Uttar Pradesh 201301"));
		agreement.setCity(utobj().generateTestData("AG_City_"));
		agreement.setCountry("India");
		agreement.setZipcode("201301");
		agreement.setState("Delhi");
		agreement.setPhoneNumbers(utobj().generateRandomNumber6Digit() + 9876);
		agreement.setPhoneExt(utobj().generateRandomNumber());
		agreement.setFaxNumbers(utobj().generateRandomNumber());
		agreement.setMobileNumbers(utobj().generateRandomNumber() + 9898);
		agreement.setEmail("vimal.sharma@franconnect.com");

		agreement.setDescription1("Description1");
		agreement.setAmount1("111");
		agreement.setAmountTerm1("1Year");
		agreement.setInterest1("11");
		agreement.setComment1("Agreement_Comment1");

		agreement.setDescription2("Description1");
		agreement.setAmount2("222");
		agreement.setAmountTerm2("2Year");
		agreement.setInterest2("22");
		agreement.setComment2("Agreement_Comment1");

		agreement.setDescription3("Description1");
		agreement.setAmount3("333");
		agreement.setAmountTerm3("3Year");
		agreement.setInterest3("33");
		agreement.setComment3("Agreement_Comment3");

		return agreement;

	}

	public void SearchFranchiseeAndClick(WebDriver driver, String franchiseID) throws Exception {
		searchLocationFromShowFilter(driver, franchiseID);
		utobj().printTestStep("Click on Franchise Location");
		utobj().clickLink(driver, franchiseID);
	}

	// To Search Franchisee location from Franchisee Summary Page through filter
	// and click on Searched location
	public void SearchFranchiseeLocationAndClick(WebDriver driver, String franchiseID) throws Exception {
		InfoMgrFranchisees(driver);
		searchLocationFromShowFilter(driver, franchiseID);
		utobj().printTestStep("Click on Franchise Location");
		utobj().clickLink(driver, franchiseID);
	}

	// To Search Corporate location from Corporate Summary Page through filter
	// and click on Searched location
	public void SearchCorporateLocationAndClick(WebDriver driver, String franchiseID) throws Exception {
		InfoMgrCorporateLocations(driver);
		searchLocationFromShowFilter(driver, franchiseID);
		utobj().printTestStep("Click on Franchise Location");
		utobj().clickLink(driver, franchiseID);
	}

	// To Search Terminated location from Terminated Summary Page through filter
	// and click on Searched location
	public void SearchTerminatedLocationAndClick(WebDriver driver, String franchiseID) throws Exception {
		InfoMgrTerminated(driver);
		searchLocationFromShowFilter(driver, franchiseID);
		utobj().printTestStep("Click on Franchise Location");

		utobj().clickLink(driver, franchiseID);
	}

	// To Search In Development location from In Development Summary Page
	// through filter and click on Searched location
	public void SearchInDevelopmentLocationAndClick(WebDriver driver, String franchiseID) throws Exception {
		InfoMgrInDevelopment(driver);
		searchLocationFromShowFilter(driver, franchiseID);
		utobj().printTestStep("Click on Franchise Location");
		utobj().clickLink(driver, franchiseID);
	}

	public void searchArhivedLocationFromShowFilter(WebDriver driver, String franchiseID) throws Exception {
		boolean isFilterOpen = false;
		try {
			utobj().sleep();
			utobj().clickLink(driver, "Show Filters");
			isFilterOpen = utobj().getElementByID(driver, "ms-parentfilterfranchiseeNo").isDisplayed();
		} catch (Exception e) {

		}

		if (isFilterOpen == false) {
			try {
				utobj().sleep();
				utobj().clickLink(driver, "Show Filters");
			} catch (Exception e) {

			}
		}
		// utobj().selectValFromMultiSelect(driver,
		// driver.findElement(By.id("inDevelopmentStatusFilter")), "Archive");
		utobj().selectDropDownByVisibleText(driver, driver.findElement(By.id("inDevelopmentStatusFilter")), "Archive");
		utobj().sleep(2000);
		utobj().selectValFromMultiSelect(driver, utobj().getElementByID(driver, "ms-parentfilterfranchiseeNo"), franchiseID);
		utobj().clickElement(driver, utobj().getElementByID(driver, "search"));
	}
	
	public void searchEntityThroughShowFilterAndClick(WebDriver driver, String entityName) throws Exception {
		boolean isFilterOpen = false;
		try {
			utobj().sleep();
			utobj().clickLink(driver, "Show Filters");
			isFilterOpen = utobj().getElementByID(driver, "ms-parentcompanyNameId").isDisplayed();
		} catch (Exception e) {

		}

		if (isFilterOpen == false) {
			try {
				utobj().sleep();
				utobj().clickLink(driver, "Show Filters");
			} catch (Exception e) {

			}
		}
		utobj().selectValFromMultiSelect(driver, utobj().getElementByID(driver, "ms-parentcompanyNameId"), entityName);
		utobj().clickElement(driver, utobj().getElementByID(driver, "search"));
		utobj().clickLink(driver, entityName);
	}
	
	public void VerifyPrintPreview(WebDriver driver, String textTobeVerified) throws Exception {
		Set<String> windows = new HashSet<String>();
		windows = driver.getWindowHandles();

		boolean isTextDisplayed = false;

		String currentWindow = driver.getWindowHandle();
		for (String window : windows) {
			if (!window.equals(currentWindow)) {

				driver.switchTo().window(window);
				if (driver.getCurrentUrl().contains("bPrint")) {
					isTextDisplayed = utobj().assertPageSource(driver, textTobeVerified);
				}
			}
		}
		driver.close();

		if (isTextDisplayed) {
			Reporter.log(textTobeVerified + "  is displayed on the print preview page. !!! ");
		} else {
			utobj().throwsException(textTobeVerified + "  is not displayed on the print preview page. !!! ");
		}

		driver.switchTo().window(currentWindow);
	}

	public void SwitchToArchivedInDevelopmentSumaryFromShowFilter(WebDriver driver) throws Exception {
		boolean isFilterOpen = false;
		try {
			utobj().sleep();
			utobj().clickLink(driver, "Show Filters");
			isFilterOpen = utobj().getElementByID(driver, "ms-parentfilterfranchiseeNo").isDisplayed();
		} catch (Exception e) {

		}

		if (isFilterOpen == false) {
			try {
				utobj().sleep();
				utobj().clickLink(driver, "Show Filters");
			} catch (Exception e) {

			}
		}
		utobj().selectDropDownByVisibleText(driver, driver.findElement(By.id("inDevelopmentStatusFilter")), "Archive");
		utobj().sleep(2000);
		utobj().clickElement(driver, utobj().getElementByID(driver, "search"));
	}

	public String getFieldLabel(WebElement element) throws Exception {

		String str1[] = {};
		String newstr = "";
		try {
			str1 = element.getText().split(":");
			newstr = str1[0].replace("*", "");
			newstr = newstr.replace("($)", "");

		} catch (Exception e) {

		}
		return newstr.trim();

	}

	public void validateFormAuditHistory(WebDriver driver, String tabName, String action, String currDate) throws Exception {
		InfoMgrAuditHistoryPage history = new InfoMgrAuditHistoryPage(driver);

		try {
			utobj().printTestStep("validating Form Audit History for " + tabName + "  " + action);

			List<WebElement> form_rows = history.formHistoryTable;
			int rows_count = form_rows.size();
			if (rows_count == 0) {
				utobj().printTestStep("No row Avaialble in form Audit History Table " + tabName + "  " + action);

			} else {

				// To locate columns(cells) of that specific row.
				List<WebElement> Columns_row = form_rows.get(2).findElements(By.tagName("td"));

				// int columns_count = Columns_row.size();
				utobj().printTestStep("Validating  Form "+tabName);
				 utobj().assertCompareValues(tabName, Columns_row.get(0).getText());
				 utobj().printTestStep("Validating Audit Date Column");
				 utobj().assertCompareValues(currDate, Columns_row.get(2).getText());
				 utobj().printTestStep("Validating ACTION "+action);
				 utobj().assertCompareValues(action, Columns_row.get(3).getText());
				utobj().printTestStep("Form Audit History record is validated for " + tabName + " " + action);

			}
		} catch (Exception e) {
			utobj().throwsSkipException("Error in Validating  Form Audit History , please refer screenshot!");
		}
	}

	public void validateFieldAuditHistory(WebDriver driver, Map<String, String> map, String event,String currentDate) throws Exception {
		InfoMgrAuditHistoryPage history = new InfoMgrAuditHistoryPage(driver);
		try {
			utobj().printTestStep("validating Field Audit History " + event + " " + map);
			List<WebElement> field_rows = history.fieldAuditTable;
			int rows_count1 = field_rows.size();
			if (rows_count1 == 0) {
				utobj().printTestStep("No row Available in field  Audit History Table " + event + " " + map);
			} else {
				for (Map.Entry<String, String> entry : map.entrySet()) {
					String key = entry.getKey();
					String value = entry.getValue();
					if (!(key.contains("url") || key.contains("email"))) {
						for (int row = 1; row < rows_count1; row++) {
							List<WebElement> Columns_row = field_rows.get(row).findElements(By.tagName("td"));
							if (Columns_row.get(4).getText().equals(event) && Columns_row.get(1).getText().equals(key)) {
								utobj().printTestStep("validating Field Audit History FOR " + Columns_row.get(1).getText() + " " + Columns_row.get(7).getText());
								// System.out.println(Columns_row.get(1)+Columns_row.get(1).getText()
								// +" "+Columns_row.get(7).getText());
								 utobj().assertCompareValues(value, Columns_row.get(7).getText());
								break;
							}
						}
					}
				}
			}
		}

		catch (Exception e) {

			utobj().throwsSkipException("Error in validateFieldAuditHistory , pleaserefer screenshot!");
		}

	}

	public void configureFormLevelEmail(WebDriver driver) throws Exception {
		try {

			utobj().printTestStep("Configure Form Level Triggers Alert Email");
			utobj().selectActionMenuItems(driver, "Configure Form Level Triggers Alert Email");
			utobj().switchFrameById(driver, "cboxIframe");
			utobj().sendKeys(driver, driver.findElement(By.xpath(".//input[@name='mailSubject']")), "TRIGGER ALERT, $FORM_NAME$ Tab for $FRANCHISE_ID$");
			utobj().sendKeys(driver, driver.findElement(By.xpath(".//iframe[@id='alertMessage_ifr']")), "Dear $FRANCHISE_ID$,Tab has been Updated");
			utobj().clickElement(driver, driver.findElement(By.xpath(".//input[@type='button' and @name='submitButton']")));
			utobj().printTestStep("Configure Field Level Triggers Alert Email");

		} catch (Exception e) {
			utobj().throwsException("Error in configureFormLevelEmail , please refer screenshot!");

		}
	}

	public void configureFieldLevelEmail(WebDriver driver) throws Exception {

		try {
			utobj().printTestStep("Configure Field Level Triggers Alert Email");

			utobj().selectActionMenuItems(driver, "Configure Field Level Triggers Alert Email");
			utobj().switchFrameById(driver, "cboxIframe");
			utobj().sendKeys(driver, driver.findElement(By.xpath(".//input[@name='mailSubject']")), "TRIGGER ALERT, $FORM_NAME$, $FIELD_NAME$ Field $FRANCHISE_ID$");
			utobj().sendKeys(driver, driver.findElement(By.xpath(".//iframe[@id='alertMessage_ifr']")), "$FIELD_NAME$ Field has been updated to $FIELD_VALUE$ for $FRANCHISE_ID$");
			utobj().clickElement(driver, driver.findElement(By.xpath(".//input[@type='button' and @name='submitButton']")));
		} catch (Exception e) {
			utobj().throwsException("Error in configureFieldLevelEmail , please refer screenshot!");

		}
	}

	public void enableAuditHistoryAndTrigger(WebDriver driver, InfoMgr_Common cmn, String corporateUserName, String triggerEmail) throws Exception {
		{
			AdminInfoMgrTriggersAndAuditingPage tau = new AdminInfoMgrTriggersAndAuditingPage(driver);
			try {
				// Enable trigger and auditing for InfoMgr Agreement through
				// admin
				utobj().printTestStep("Switching to Admin Info Mgr Triggers and Auditing");
				String testCaseId = "TC_InFoMgr_CorporateLocations_Agreement_Add";

				// Map<String, String> dsAgreement =
				// fc.utobj().readTestData("infomgr",
				// testCaseId);
				Map<String, String> map = utobj().readTestDatawithsqllite("info", testCaseId, "Franchisee");
				cmn.adminFimTriggersAndAuditing(driver);
				utobj().selectActionMenuItems(driver, "Modify");
				utobj().clickElement(driver, tau.auditForm);
				utobj().clickElement(driver, tau.triggerForm);

				int offset = 10;
				utobj().selectValFromMultiSelectwithoutSelectAll(driver, tau.msParent, corporateUserName);
				utobj().sendKeys(driver, tau.recipient, triggerEmail);
				utobj().selectDropDown(driver, tau.onEvent, "Add / Modify");
				utobj().clickElement(driver, tau.tickTrigger);
				utobj().clickElement(driver, tau.tickRecepient);
				utobj().clickElement(driver, tau.tickOnevent);
				utobj().clickElement(driver, tau.submit);

				utobj().clickElement(driver, tau.setting);
				//utobj().clickElement(driver, tau.fieldLevelConfig);
				utobj().actionImgOption(driver, "Agreement", "Field Level Configuration");
				utobj().clickElement(driver, tau.enableAudit);
				utobj().clickElement(driver, tau.selectAllTrigger);
				utobj().selectValFromMultiSelectwithoutSelectAll(driver, tau.trigerDropDown, corporateUserName);
				utobj().clickElement(driver, tau.tickUser);
				utobj().sendKeys(driver, tau.email, triggerEmail);
				utobj().clickElement(driver, tau.tickRecipient);

				utobj().selectDropDownByVisibleText(driver, driver.findElement(By.xpath(".//select[@name='triggerEvent_0event']")), "Add / Modify");
				utobj().selectDropDownByVisibleText(driver, driver.findElement(By.xpath(".//select[@name='triggerEvent_1event']")), "Add / Modify");
				utobj().selectDropDownByVisibleText(driver, driver.findElement(By.xpath(".//select[@name='triggerEvent_2event']")), "Add / Modify");
				utobj().selectDropDownByVisibleText(driver, driver.findElement(By.xpath(".//select[@name='triggerEvent_3event']")), "Add / Modify");
				utobj().selectDropDownByVisibleText(driver, driver.findElement(By.xpath(".//select[@name='triggerEvent_4event']")), "Add / Modify");
				utobj().selectDropDownByVisibleText(driver, driver.findElement(By.xpath(".//select[@name='triggerEvent_5event']")), "Add / Modify");
				utobj().selectDropDownByVisibleText(driver, driver.findElement(By.xpath(".//select[@name='triggerEvent_6event']")), "Value Equals");
				utobj().sendKeys(driver, driver.findElement(By.xpath(".//input[@name='triggerEvent_6valueToCompare']")), map.get("stateAddendum"));

				utobj().selectDropDownByVisibleText(driver, driver.findElement(By.xpath(".//select[@name='triggerEvent_7event']")), "Value Equals");
				utobj().sendKeys(driver, driver.findElement(By.xpath(".//input[@name='triggerEvent_7valueToCompare']")), map.get("otherAddendum"));
				//
				utobj().selectDropDownByVisibleText(driver, driver.findElement(By.xpath(".//select[@name='triggerEvent_8event']")), "Value Equals");
				utobj().sendKeys(driver, driver.findElement(By.xpath(".//input[@name='triggerEvent_8valueToCompare']")), map.get("rightsofFirstRefusal"));
				//
				utobj().selectDropDownByVisibleText(driver, driver.findElement(By.xpath(".//select[@name='triggerEvent_9event']")), "Value Equals");
				utobj().sendKeys(driver, driver.findElement(By.xpath(".//input[@name='triggerEvent_9valueToCompare']")), map.get("protectedTerritory"));
				// salesperson
				utobj().selectDropDownByVisibleText(driver, driver.findElement(By.xpath(".//select[@name='triggerEvent_10event']")), "Value Equals");
				utobj().sendKeys(driver, driver.findElement(By.xpath(".//input[@name='triggerEvent_10valueToCompare']")), map.get("salesPerson"));
				// lisscense
				utobj().selectDropDownByVisibleText(driver, driver.findElement(By.xpath(".//select[@name='triggerEvent_11event']")), "Value Equals");
				utobj().sendKeys(driver, driver.findElement(By.xpath(".//input[@name='triggerEvent_11valueToCompare']")), map.get("previousLicenseNumber"));
				// related centre
				utobj().selectDropDownByVisibleText(driver, driver.findElement(By.xpath(".//select[@name='triggerEvent_12event']")), "Value Equals");
				utobj().sendKeys(driver, driver.findElement(By.xpath(".//input[@name='triggerEvent_12valueToCompare']")), map.get("relatedCenter"));
				utobj().selectDropDownByVisibleText(driver, driver.findElement(By.xpath(".//select[@name='triggerEvent_13event']")), "Add / Modify");
				utobj().selectDropDownByVisibleText(driver, driver.findElement(By.xpath(".//select[@name='triggerEvent_14event']")), "Add / Modify");
				// comments
				utobj().selectDropDownByVisibleText(driver, driver.findElement(By.xpath(".//select[@name='triggerEvent_15event']")), "Value Equals");
				utobj().sendKeys(driver, driver.findElement(By.xpath(".//input[@name='triggerEvent_15valueToCompare']")), map.get("comments"));
				// initalterm
				utobj().selectDropDownByVisibleText(driver, driver.findElement(By.xpath(".//select[@name='triggerEvent_16event']")), "Value Equals");
				utobj().sendKeys(driver, driver.findElement(By.xpath(".//input[@name='triggerEvent_16valueToCompare']")), map.get("initialTerm"));
				// term first
				utobj().selectDropDownByVisibleText(driver, driver.findElement(By.xpath(".//select[@name='triggerEvent_17event']")), "Value Equals");
				utobj().sendKeys(driver, driver.findElement(By.xpath(".//input[@name='triggerEvent_17valueToCompare']")), map.get("termFirst"));
				utobj().selectDropDownByVisibleText(driver, driver.findElement(By.xpath(".//select[@name='triggerEvent_18event']")), "Add / Modify");
				// renewal fee first
				utobj().selectDropDownByVisibleText(driver, driver.findElement(By.xpath(".//select[@name='triggerEvent_19event']")), "Value less than");
				utobj().sendKeys(driver, driver.findElement(By.xpath(".//input[@name='triggerEvent_19valueToCompare']")), String.valueOf(Float.parseFloat(map.get("feeFirst") + offset)));
				utobj().selectDropDownByVisibleText(driver, driver.findElement(By.xpath(".//select[@name='triggerEvent_20event']")), "Add / Modify");
				// renewal term sec
				utobj().selectDropDownByVisibleText(driver, driver.findElement(By.xpath(".//select[@name='triggerEvent_21event']")), "Value Equals");
				utobj().sendKeys(driver, driver.findElement(By.xpath(".//input[@name='triggerEvent_21valueToCompare']")), map.get("termSecond"));
				utobj().selectDropDownByVisibleText(driver, driver.findElement(By.xpath(".//select[@name='triggerEvent_22event']")), "Add / Modify");
				// renewal fee sec
				utobj().selectDropDownByVisibleText(driver, driver.findElement(By.xpath(".//select[@name='triggerEvent_23event']")), "Value less than");
				utobj().sendKeys(driver, driver.findElement(By.xpath(".//input[@name='triggerEvent_23valueToCompare']")), String.valueOf(Float.parseFloat(map.get("feeSecond") + offset)));
				utobj().selectDropDownByVisibleText(driver, driver.findElement(By.xpath(".//select[@name='triggerEvent_24event']")), "Add / Modify");
				// renewal term third
				utobj().selectDropDownByVisibleText(driver, driver.findElement(By.xpath(".//select[@name='triggerEvent_25event']")), "Value Equals");
				utobj().sendKeys(driver, driver.findElement(By.xpath(".//input[@name='triggerEvent_25valueToCompare']")), map.get("termThird"));
				// renewal due date third
				utobj().selectDropDownByVisibleText(driver, driver.findElement(By.xpath(".//select[@name='triggerEvent_26event']")), "Add / Modify");
				// renewal fee third
				utobj().selectDropDownByVisibleText(driver, driver.findElement(By.xpath(".//select[@name='triggerEvent_27event']")), "Value less than");
				utobj().sendKeys(driver, driver.findElement(By.xpath(".//input[@name='triggerEvent_27valueToCompare']")), String.valueOf(Float.parseFloat(map.get("feeThird") + offset)));
				// date
				utobj().selectDropDownByVisibleText(driver, driver.findElement(By.xpath(".//select[@name='triggerEvent_28event']")), "Add / Modify");
				// contact title
				utobj().selectDropDownByVisibleText(driver, driver.findElement(By.xpath(".//select[@name='triggerEvent_29event']")), "Add / Modify");
				// first name
				utobj().selectDropDownByVisibleText(driver, driver.findElement(By.xpath(".//select[@name='triggerEvent_30event']")), "Value Equals");
				utobj().sendKeys(driver, driver.findElement(By.xpath(".//input[@name='triggerEvent_30valueToCompare']")), map.get("firstName"));
				// last name
				utobj().selectDropDownByVisibleText(driver, driver.findElement(By.xpath(".//select[@name='triggerEvent_31event']")), "Value Equals");
				utobj().sendKeys(driver, driver.findElement(By.xpath(".//input[@name='triggerEvent_31valueToCompare']")), map.get("lastName"));
				// desc 1
				utobj().selectDropDownByVisibleText(driver, driver.findElement(By.xpath(".//select[@name='triggerEvent_32event']")), "Value Equals");
				utobj().sendKeys(driver, driver.findElement(By.xpath(".//input[@name='triggerEvent_32valueToCompare']")), map.get("description1"));
				// amount 1
				utobj().selectDropDownByVisibleText(driver, driver.findElement(By.xpath(".//select[@name='triggerEvent_33event']")), "Value less than");
				utobj().sendKeys(driver, driver.findElement(By.xpath(".//input[@name='triggerEvent_33valueToCompare']")), String.valueOf(Float.parseFloat(map.get("amount1") + offset)));
				// amount term 1
				utobj().selectDropDownByVisibleText(driver, driver.findElement(By.xpath(".//select[@name='triggerEvent_34event']")), "Value Equals");
				utobj().sendKeys(driver, driver.findElement(By.xpath(".//input[@name='triggerEvent_34valueToCompare']")), map.get("amountTerm1"));
				// interest 1
				utobj().selectDropDownByVisibleText(driver, driver.findElement(By.xpath(".//select[@name='triggerEvent_35event']")), "Value less than");
				utobj().sendKeys(driver, driver.findElement(By.xpath(".//input[@name='triggerEvent_35valueToCompare']")), String.valueOf(Float.parseFloat(map.get("interest1") + offset)));
				// comments 1
				utobj().selectDropDownByVisibleText(driver, driver.findElement(By.xpath(".//select[@name='triggerEvent_36event']")), "Value Equals");
				utobj().sendKeys(driver, driver.findElement(By.xpath(".//input[@name='triggerEvent_36valueToCompare']")), map.get("comments1"));
				// desc 2
				utobj().selectDropDownByVisibleText(driver, driver.findElement(By.xpath(".//select[@name='triggerEvent_37event']")), "Value Equals");
				utobj().sendKeys(driver, driver.findElement(By.xpath(".//input[@name='triggerEvent_37valueToCompare']")), map.get("description2"));
				// amount 2
				utobj().selectDropDownByVisibleText(driver, driver.findElement(By.xpath(".//select[@name='triggerEvent_38event']")), "Value less than");
				utobj().sendKeys(driver, driver.findElement(By.xpath(".//input[@name='triggerEvent_38valueToCompare']")), String.valueOf(Float.parseFloat(map.get("amount2") + offset)));
				// amount term 2
				utobj().selectDropDownByVisibleText(driver, driver.findElement(By.xpath(".//select[@name='triggerEvent_39event']")), "Value Equals");
				utobj().sendKeys(driver, driver.findElement(By.xpath(".//input[@name='triggerEvent_39valueToCompare']")), map.get("amountTerm2"));
				utobj().selectDropDownByVisibleText(driver, driver.findElement(By.xpath(".//select[@name='triggerEvent_40event']")), "Value less than");
				utobj().sendKeys(driver, driver.findElement(By.xpath(".//input[@name='triggerEvent_40valueToCompare']")), String.valueOf(Float.parseFloat(map.get("feeThird") + offset)));
				// comments 2
				utobj().selectDropDownByVisibleText(driver, driver.findElement(By.xpath(".//select[@name='triggerEvent_41event']")), "Value Equals");
				utobj().sendKeys(driver, driver.findElement(By.xpath(".//input[@name='triggerEvent_41valueToCompare']")), map.get("comments2"));
				// desc 3
				utobj().selectDropDownByVisibleText(driver, driver.findElement(By.xpath(".//select[@name='triggerEvent_42event']")), "Value Equals");
				utobj().sendKeys(driver, driver.findElement(By.xpath(".//input[@name='triggerEvent_42valueToCompare']")), map.get("description3"));
				// amount 3
				utobj().selectDropDownByVisibleText(driver, driver.findElement(By.xpath(".//select[@name='triggerEvent_43event']")), "Value less than");
				utobj().sendKeys(driver, driver.findElement(By.xpath(".//input[@name='triggerEvent_43valueToCompare']")), String.valueOf(Float.parseFloat(map.get("amount3") + offset)));
				// amount termm 3
				utobj().selectDropDownByVisibleText(driver, driver.findElement(By.xpath(".//select[@name='triggerEvent_44event']")), "Value Equals");
				utobj().sendKeys(driver, driver.findElement(By.xpath(".//input[@name='triggerEvent_44valueToCompare']")), map.get("amountTerm3"));
				// interest 3
				utobj().selectDropDownByVisibleText(driver, driver.findElement(By.xpath(".//select[@name='triggerEvent_45event']")), "Value less than");
				utobj().sendKeys(driver, driver.findElement(By.xpath(".//input[@name='triggerEvent_45valueToCompare']")), String.valueOf(Float.parseFloat(map.get("interest3") + offset)));
				// comments 3
				utobj().selectDropDownByVisibleText(driver, driver.findElement(By.xpath(".//select[@name='triggerEvent_46event']")), "Value Equals");
				utobj().sendKeys(driver, driver.findElement(By.xpath(".//input[@name='triggerEvent_46valueToCompare']")), map.get("comments3"));

				utobj().clickElement(driver, tau.submit1);
				configureFormLevelEmail(driver);
				configureFieldLevelEmail(driver);
			} catch (Exception e) {
				utobj().throwsException("Error in enableAuditHistoryAndTrigger , please refer screenshot!");

			}
		}

	}

	public void validateTriggerAlertMailForForm(String tabName, String franchiseID, String userName, String password) throws Exception {
		try {

			utobj().printTestStep("Validating Trigger Alert email  for Form Update " + tabName);
			String subForm = "TRIGGER ALERT, " + tabName + " Tab for " + franchiseID;
			String messageForm = "Dear " + franchiseID + ",Tab has been Updated";
		    System.out.println(subForm);
		    System.out.println(messageForm);
			Map<String, String> mailData = new HashMap<String, String>();
			mailData = utobj().readMailBox(subForm, messageForm, userName, password);
			utobj().printTestStep("Validating Trigger Alert email  for Form Update" + tabName);
			Assert.assertTrue(mailData.size() != 0);
		} catch (Exception e) {
			utobj().throwsException("Error in validateTriggerAlertMailForForm , please refer screenshot!");
		}

	}

	public void validateTriggerAlertMailForField(String tabName, String franchiseID, String emailID, String password, String Key, String value) throws Exception {
		try {
			utobj().printTestStep("Validating Trigger Alert email  for Field Update  " + tabName);
			String subjectField =  "TRIGGER ALERT, " + tabName + ", " + Key + " Field" + " " + franchiseID;
			
			utobj().printTestStep("Validating Trigger Alert email  for Field Update" + "in " + emailID + Key + " " + value);
			if (Key.contains("Contact")) {
				subjectField = "TRIGGER ALERT, " + tabName + ",  " + Key + " Field" + " " + franchiseID;
			} else if (Key.contains("Renewal Fee-")) {
				Key = Key + "(" + "$" + ")";
				subjectField = "TRIGGER ALERT, " + tabName + ", " + Key + " Field" + " " + franchiseID;
			} else if (Key.contains("Interest")) {
				String arr[] = Key.split(" ");
				Key = arr[0] + " (%)" + arr[1];
				subjectField = "TRIGGER ALERT, " + tabName + ", " + Key + " Field" + " " + franchiseID;
			}
			
			
			if (!Key.contains("url")) {
				
					String messageField = Key + " Field has been updated to " + value + " for " + franchiseID;				
				Map<String, String> mailData = new HashMap<String, String>();
				 System.out.println(subjectField);
				    System.out.println(messageField);
				mailData = utobj().readMailBox(subjectField, messageField, emailID, password);
				Assert.assertTrue(mailData.size() != 0);
			}
		} catch (Exception e) {
			utobj().throwsException("Error in validateTriggerAlertMailForForm , please refer screenshot!");
		}

	}
}
