package com.builds.test.infomgr;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;

import com.builds.test.fieldops.AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest;
import com.builds.test.fieldops.AdminFieldOpsManageVisitFormPageTest;
import com.builds.test.fieldops.AdminFieldOpsQuestionLibraryPageTest;
import com.builds.uimaps.fieldops.FieldOpsVisitsPage;
import com.builds.uimaps.infomgr.InfoMgrAddressBook;
import com.builds.uimaps.infomgr.InfoMgrAgreementPage;
import com.builds.uimaps.infomgr.InfoMgrCenterInfoPage;
import com.builds.uimaps.infomgr.InfoMgrContactHistoryPage;
import com.builds.uimaps.infomgr.InfoMgrContractSigningPage;
import com.builds.uimaps.infomgr.InfoMgrCustomerComplaintsPage;
import com.builds.uimaps.infomgr.InfoMgrDocumentsPage;
import com.builds.uimaps.infomgr.InfoMgrEmployeesPage;
import com.builds.uimaps.infomgr.InfoMgrEntityDetailsPage;
import com.builds.uimaps.infomgr.InfoMgrEventsPage;
import com.builds.uimaps.infomgr.InfoMgrFinancialsPage;
import com.builds.uimaps.infomgr.InfoMgrFranchiseeFilterPage;
import com.builds.uimaps.infomgr.InfoMgrFranchiseesPage;
import com.builds.uimaps.infomgr.InfoMgrGuarantorsPage;
import com.builds.uimaps.infomgr.InfoMgrInsurancePage;
import com.builds.uimaps.infomgr.InfoMgrLegalViolationPage;
import com.builds.uimaps.infomgr.InfoMgrLendersPage;
import com.builds.uimaps.infomgr.InfoMgrLogACallPage;
import com.builds.uimaps.infomgr.InfoMgrLogATaskPage;
import com.builds.uimaps.infomgr.InfoMgrMarketingPage;
import com.builds.uimaps.infomgr.InfoMgrMultiUnitEntityPage;
import com.builds.uimaps.infomgr.InfoMgrMultiUnitInfoPage;
import com.builds.uimaps.infomgr.InfoMgrMystryReviewPage;
import com.builds.uimaps.infomgr.InfoMgrOwnersPage;
import com.builds.uimaps.infomgr.InfoMgrPicturesPage;
import com.builds.uimaps.infomgr.InfoMgrQAHistoryPage;
import com.builds.uimaps.infomgr.InfoMgrRealStatePage;
import com.builds.uimaps.infomgr.InfoMgrSendEmailPage;
import com.builds.uimaps.infomgr.InfoMgrSendMessagePage;
import com.builds.uimaps.infomgr.InfoMgrTerritoryPage;
import com.builds.uimaps.infomgr.InfoMgrTrainingPage;
import com.builds.uimaps.infomgr.InfoMgrUserPage;
import com.builds.utilities.FranconnectUtil;

public class MultiUnitEntityCommonMethods {

	FranconnectUtil fc = new FranconnectUtil();

	public void searchMultiUnitAndClick(WebDriver driver, String multiUnitOwnerName) throws Exception {
		Reporter.log("***************************** Search Multi Unit and Click  ******************");

		fc.infomgr().infomgr_common().InfoMgrMultiUnitEntity(driver);

		ChangeToMultiUnitView(driver);
		InfoMgrMultiUnitEntityPage multiUnitPage = new InfoMgrMultiUnitEntityPage(driver);

		/*
		 * fc.utobj().sendKeys(driver, multiUnitPage.txtSearchMultiUnit ,
		 * multiUnitOwnerName ); fc.utobj().clickElement(driver,
		 * multiUnitPage.imgMultiUnitSearchButton);
		 * fc.utobj().clickPartialLinkText(driver,multiUnitOwnerName );
		 */

		searchMultiLocationFromShowFilter(driver, multiUnitOwnerName);
		fc.utobj().clickPartialLinkText(driver, multiUnitOwnerName);

	}

	public void searchMultiLocationFromShowFilter(WebDriver driver, String multiUnitOwnerName) throws Exception {
		boolean isFilterOpen = false;
		try {
			fc.utobj().clickElement(driver, ".//*[@id='showFilter']");
			isFilterOpen = fc.utobj().getElementByID(driver, "ms-parentownerId").isDisplayed();
		} catch (Exception e) {

		}
		if (isFilterOpen == false) {
			try {
				fc.utobj().clickElement(driver, ".//*[@id='showFilter']");
			} catch (Exception e) {

			}
		}
		fc.utobj().selectValFromMultiSelect(driver, fc.utobj().getElementByID(driver, "ms-parentownerId"),
				multiUnitOwnerName);
		fc.utobj().clickElement(driver, fc.utobj().getElementByID(driver, "search"));
	}

	public void searchMultiUnit(WebDriver driver, String multiUnitOwnerName) throws Exception {
		Reporter.log("***************************** Search Multi Unit ****************************");

		fc.infomgr().infomgr_common().InfoMgrMultiUnitEntity(driver);

		ChangeToMultiUnitView(driver);

		/*
		 * InfoMgrMultiUnitEntityPage multiUnitPage = new
		 * InfoMgrMultiUnitEntityPage(driver); fc.utobj().sendKeys(driver,
		 * multiUnitPage.txtSearchMultiUnit , multiUnitOwnerName );
		 * fc.utobj().clickElement(driver,
		 * multiUnitPage.imgMultiUnitSearchButton);
		 */

		searchMultiLocationFromShowFilter(driver, multiUnitOwnerName);
	}

	public void ChangeToMultiUnitView(WebDriver driver) throws Exception {
		if (fc.utobj().assertLinkText(driver, "Change to Multi-Unit View")) {
			fc.utobj().clickLink(driver, "Change to Multi-Unit View");
		}
	}

	public String addDocument(WebDriver driver) throws Exception {
		Reporter.log("************************************** Add Document *********************");
		String testCaseId = "TC_InfoMgr_MultiUnit_Document_Add";
		InfoMgrDocumentsPage documentsPage = new InfoMgrMultiUnitEntityPage(driver).getDocumentsPage();
		Map<String, String> dsDocuments = fc.utobj().readTestData("infomgr", testCaseId);

		String documentTitle = fc.utobj().generateTestData(dsDocuments.get("documentTitle"));
		fc.utobj().clickElement(driver, documentsPage.documentsTab);

		try {
			fc.utobj().clickElement(driver, documentsPage.btnAddMore);
		} catch (Exception ex) {

		}

		if (fc.utobj().validate(testCaseId) == true) {
			fc.utobj().sendKeys(driver, documentsPage.txtDocumentTitle, documentTitle);
			fc.utobj().sendKeys(driver, documentsPage.txtDocumentAttachment, dsDocuments.get("documentAttachment"));
			fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																			// for
																			// Custom
																			// Manadatory
																			// Fields
																			// Of
																			// Client
			fc.utobj().clickElement(driver, documentsPage.btnAdd);

			boolean isDocAdded = fc.utobj().assertPageSource(driver, documentTitle);
			if (isDocAdded) {
				Reporter.log("Document added successfully");
			} else {
				fc.utobj().throwsException("Document upload failed. !!! ");
			}
			fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
		} else {
			fc.utobj().throwsSkipException("Error in adding document , please refer screenshot!");
		}
		return documentTitle;
	}

	public void deleteDocument(WebDriver driver, String document) throws Exception {
		Reporter.log("************************************* Delete Document **************************");

		String testCaseId = "TC_InfoMgr_MultiUnit_Document_Delete";
		InfoMgrDocumentsPage documentsPage = new InfoMgrMultiUnitEntityPage(driver).getDocumentsPage();

		fc.utobj().clickElement(driver, documentsPage.documentsTab);

		if (fc.utobj().validate(testCaseId) == true) {
			fc.utobj().actionImgOption(driver, document, "Delete");
			fc.utobj().acceptAlertBox(driver);
			boolean isDocumentOnPage = fc.utobj().assertPageSource(driver, document);
			if (isDocumentOnPage == false) {
				Reporter.log("Document deleted successfully");
			} else {
				fc.utobj().throwsException("Some problem occured while deleting the document !!!");
			}
			fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
		} else {
			fc.utobj().throwsSkipException("Error in adding owner , please refer screenshot!");
		}
	}

	public void multiUnitEntityPageShowAll(WebDriver driver) throws Exception {
		fc.infomgr().infomgr_common().InfoMgrMultiUnitEntity(driver);
		ChangeToMultiUnitView(driver);
		if (fc.utobj().assertLinkText(driver, "Show All")) {
			fc.utobj().clickLink(driver, "Show All");
		}
	}

	// ******************************************************* Add Data
	// *******************************************************************

	public Map<String, String> addOwner(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("********************** Adding Multi unit Owner ***********************");

		String testCaseId = "TC_InFoMgr_MultiUnit_Add_Owner";
		Map<String, String> dsOwners = fc.utobj().readTestData("infomgr", testCaseId);

		Map<String, String> lstOwnerDetails = new HashMap<>();
		lstOwnerDetails.put("FirstName", fc.utobj().generateTestData(dsOwners.get("firstName")));
		lstOwnerDetails.put("LastName", dsOwners.get("lastName"));
		lstOwnerDetails.put("Email", dsOwners.get("email"));
		lstOwnerDetails.put("Country", dsOwners.get("country"));
		lstOwnerDetails.put("State", dsOwners.get("state"));

		InfoMgrOwnersPage objAddOwnersPage = new InfoMgrMultiUnitEntityPage(driver).getOwnersPage();
		fc.utobj().clickElement(driver, objAddOwnersPage.lnkAddOwner);

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().selectDropDownByIndex(driver, objAddOwnersPage.drpFranchiseID, 1);
				fc.utobj().sendKeys(driver, objAddOwnersPage.txtFirstName, lstOwnerDetails.get("FirstName"));
				fc.utobj().sendKeys(driver, objAddOwnersPage.txtLastName, lstOwnerDetails.get("LastName"));

				fc.utobj().sendKeys(driver, objAddOwnersPage.txtEmail, lstOwnerDetails.get("Email"));
				fc.utobj().selectDropDownByVisibleText(driver, objAddOwnersPage.drpHomeCountry,
						lstOwnerDetails.get("Country"));
				fc.utobj().sleep();
				fc.utobj().selectDropDownByVisibleText(driver, objAddOwnersPage.drpHomeState,
						lstOwnerDetails.get("State"));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, objAddOwnersPage.btnAdd);
				boolean ownerAddedFlag = fc.utobj().assertPageSource(driver, lstOwnerDetails.get("FirstName"));
				if (ownerAddedFlag) {
					Reporter.log("Owner added successfully !!!");
				} else {
					fc.utobj().throwsException("Error in adding owner !!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in adding owner , please refer screenshot!");
		}
		return lstOwnerDetails;
	}

	public String addExistingOwner(WebDriver driver, Map<String, String> config, String franchiseID, String owner)
			throws Exception {
		Reporter.log("******************** Adding Existing Owner ************************* ");

		String testCaseId = "TC_InFoMgr_MultiUnit_Add_Existing_Owner";

		WebDriverWait wait = new WebDriverWait(driver, 4);

		String multiUnitName = fc.utobj().generateTestData("Multi Unit 01");

		InfoMgrOwnersPage ownersPage = new InfoMgrMultiUnitEntityPage(driver).getOwnersPage();

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().clickElement(driver, ownersPage.tabOwners);

				fc.utobj().clickElement(driver, ownersPage.lnkAddOwner);

				fc.utobj().clickRadioButton(driver, ownersPage.rdoExistingOwner, "existing");

				// Wait for existing owner section to load
				fc.utobj().selectValFromMultiSelect(driver, ownersPage.drpMultiCheckboxOwnerID, owner);

				fc.utobj().sendKeys(driver, ownersPage.txtMuidName, multiUnitName);

				fc.utobj().clickElement(driver, ownersPage.btnAdd);
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in adding existing owner , please refer screenshot!");
		}

		return multiUnitName;
	}

	public String addRemark(WebDriver driver, Map<String, String> config, Object remarksObj) throws Exception {
		Reporter.log("******************* Adding Remarks ********************** \n");

		String testCaseId = "TC_InFoMgr_MultiUnit_Add_Remarks";

		fc.commonMethods().switch_cboxIframe_frameId(driver);
		String remark = fc.utobj().generateTestData("TestRemarks");

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				if (remarksObj instanceof InfoMgrContactHistoryPage) {
					InfoMgrContactHistoryPage contactHistory = (InfoMgrContactHistoryPage) remarksObj;
					fc.utobj().sendKeys(driver, contactHistory.txtRemarks, remark);
					fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																					// for
																					// Custom
																					// Manadatory
																					// Fields
																					// Of
																					// Client
					fc.utobj().clickElement(driver, contactHistory.btnSubmit);
					fc.utobj().clickElement(driver, contactHistory.btnClose);
				} else if (remarksObj instanceof InfoMgrOwnersPage) {
					InfoMgrOwnersPage ownersPage = (InfoMgrOwnersPage) remarksObj;
					fc.utobj().sendKeys(driver, ownersPage.txtRemarks, remark);
					fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																					// for
																					// Custom
																					// Manadatory
																					// Fields
																					// Of
																					// Client
					fc.utobj().clickElement(driver, ownersPage.btnSubmit);
					fc.utobj().clickElement(driver, ownersPage.btnClose);
					Thread.sleep(2000);

					boolean isRemarkOnPage = fc.utobj().assertPageSource(driver, remark);
					if (isRemarkOnPage) {
						Reporter.log("Remarks have been added successfully !!! ");
					} else {
						fc.utobj().throwsException("Some problem occured while adding remarks !!!!");
					}

				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in adding remarks , please refer screenshot!");
		}
		return remark;
	}

	public String sendTextMessage(WebDriver driver, Map<String, String> config, boolean isFRomContactHistoryPage,
			String corporateUser) throws Exception {
		Reporter.log("******************* Sending Text Message ********************** \n");

		InfoMgrSendMessagePage messagePage = new InfoMgrMultiUnitEntityPage(driver).getSendMessagePage();

		String testCaseId = "TC_InFoMgr_MultiUnit_Send_Text_Message";

		Map<String, String> dsMessage = fc.utobj().readTestData("infomgr", testCaseId);

		String txtSubject = fc.utobj().generateTestData(dsMessage.get("subject"));

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().clickLink(driver, "Address book");
				SelectToEmail(driver, corporateUser);
				fc.utobj().switchFrameToDefault(driver);
				fc.utobj().sendKeys(driver, messagePage.txtSubject, txtSubject);
				fc.utobj().clickRadioButton(driver, messagePage.rdoTxtMsg, dsMessage.get("messageType"));
				fc.utobj().sleep();
				fc.utobj().sendKeys(driver, messagePage.txtMessageText, dsMessage.get("textMessage"));
				fc.utobj().clickElement(driver, messagePage.btnSend);
				Thread.sleep(2000);

				boolean isSubjectOnPage = fc.utobj().assertPageSource(driver, txtSubject);
				if (isSubjectOnPage) {
					Reporter.log("Message have been sent successfully !!! ");
					fc.utobj().clickElement(driver, messagePage.btnOk);
				} else {
					fc.utobj().throwsException("Some problem occured while sending message !!!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in sending message, please refer screenshot!");
		}
		return txtSubject;
	}

	public String sendTextEmail(WebDriver driver, Map<String, String> config, boolean isFromContactHistoryPage)
			throws Exception {
		Reporter.log("******************* Sending Text Email ********************** \n");

		InfoMgrSendEmailPage emailPage = new InfoMgrMultiUnitEntityPage(driver).getSendMailPage();

		String testCaseId = "TC_InFoMgr_MultiUnit_Send_Text_Email";

		Map<String, String> dsEmail = fc.utobj().readTestData("infomgr", testCaseId);

		String txtSubject = fc.utobj().generateTestData(dsEmail.get("subject"));

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().sendKeys(driver, emailPage.txtSubject, txtSubject);
				fc.utobj().clickRadioButton(driver, emailPage.rdoTxtMail, dsEmail.get("emailType"));
				fc.utobj().sleep();
				fc.utobj().sendKeys(driver, emailPage.mailMessage, dsEmail.get("textMessage"));
				fc.utobj().clickElement(driver, emailPage.btnSend);
				Thread.sleep(2000);

				if (isFromContactHistoryPage) {
					boolean isSubjectOnPage = fc.utobj().assertPageSource(driver, txtSubject);
					if (isSubjectOnPage) {
						Reporter.log("Email have been sent successfully !!! ");
					} else {
						fc.utobj().throwsException("Some problem occured while sending mail !!!!");
					}
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in sending text mail, please refer screenshot!");
		}
		return txtSubject;
	}

	public String sendHTMLEmail(WebDriver driver, Map<String, String> config, boolean isFromContactHistoryPage)
			throws Exception {
		Reporter.log("******************* Sending HTML Email ********************** \n");

		InfoMgrSendEmailPage emailPage = new InfoMgrMultiUnitEntityPage(driver).getSendMailPage();

		String testCaseId = "TC_InFoMgr_MultiUnit_Send_HTML_Email";

		Map<String, String> dsEmail = fc.utobj().readTestData("infomgr", testCaseId);

		String txtSubject = fc.utobj().generateTestData(dsEmail.get("subject"));

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().sendKeys(driver, emailPage.txtSubject, txtSubject);
				fc.utobj().clickRadioButton(driver, emailPage.rdoTxtMail, dsEmail.get("emailType"));
				fc.utobj().sendKeys(driver, emailPage.mailMessage, dsEmail.get("textMessage"));
				fc.utobj().clickElement(driver, emailPage.btnSend);
				Thread.sleep(2000);

				if (isFromContactHistoryPage) {
					boolean isSubjectOnPage = fc.utobj().assertPageSource(driver, txtSubject);
					if (isSubjectOnPage) {
						Reporter.log("Mail have been sent successfully !!! ");
					} else {
						fc.utobj().throwsException("Some problem occured while sending mail !!!!");
					}
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in sending HTML mail, please refer screenshot!");
		}
		return txtSubject;
	}

	public String sendHtmlMessage(WebDriver driver, Map<String, String> config, boolean isFromContactHistoryPage)
			throws Exception {
		Reporter.log("******************* Sending HTML Message ********************** \n");

		InfoMgrSendMessagePage messagePage = new InfoMgrMultiUnitEntityPage(driver).getSendMessagePage();

		String testCaseId = "TC_InFoMgr_MultiUnit_Send_HTML_Message";

		Map<String, String> dsMessage = fc.utobj().readTestData("infomgr", testCaseId);

		String txtSubject = fc.utobj().generateTestData(dsMessage.get("subject"));

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().sendKeys(driver, messagePage.txtToEmail, dsMessage.get("toEmail"));
				fc.utobj().sendKeys(driver, messagePage.txtSubject, txtSubject);
				fc.utobj().clickRadioButton(driver, messagePage.rdoTxtMsg, dsMessage.get("messageType"));
				fc.utobj().clickElement(driver, messagePage.btnSend);
				Thread.sleep(2000);
				if (isFromContactHistoryPage) {
					boolean isSubjectOnPage = fc.utobj().assertPageSource(driver, txtSubject);
					if (isSubjectOnPage) {
						Reporter.log("HTML Message have been sent successfully !!! ");
						fc.utobj().clickElement(driver, messagePage.btnOk);
					} else {
						fc.utobj().throwsException("Some problem occured while sending HTML message !!!!");
					}
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in sending HTML message, please refer screenshot!");
		}
		return txtSubject;
	}

	public void addTraining(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("********************** Add Training ****************** \n");

		String testCaseId = "TC_InFoMgr_MultiUnit_Training_Add";

		Map<String, String> dsTraining = fc.utobj().readTestData("infomgr", testCaseId);

		// Check if the page has already training listed , if yes then click on
		// "Add More" button
		InfoMgrTrainingPage objAddTrainingPage = new InfoMgrMultiUnitEntityPage(driver).getTrainingPage();
		String tableText = fc.utobj().getText(driver, objAddTrainingPage.tblTrainings);

		if (tableText.contains("Additional Trainings")) {
			fc.utobj().clickElement(driver, objAddTrainingPage.btnAddMore);
		}

		String trainingProgram = fc.utobj().generateTestData(dsTraining.get("trainingProgram"));

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().selectDropDownByIndex(driver, objAddTrainingPage.drpFranchiseID, 1);

				fc.utobj().sendKeys(driver, objAddTrainingPage.txtTrainingProgName, trainingProgram);
				fc.utobj().selectDropDownByPartialText(driver, objAddTrainingPage.drpTrainingType,
						dsTraining.get("trainingType"));
				fc.utobj().sendKeys(driver, objAddTrainingPage.txtAttendee, dsTraining.get("attendee"));
				fc.utobj().sendKeys(driver, objAddTrainingPage.txtCompletionDate,
						fc.utobj().getFutureDateUSFormat(10));
				fc.utobj().sendKeys(driver, objAddTrainingPage.txtGrade, dsTraining.get("grade"));
				fc.utobj().sendKeys(driver, objAddTrainingPage.txtInstructor, dsTraining.get("instructor"));
				fc.utobj().sendKeys(driver, objAddTrainingPage.txtLocation, dsTraining.get("location"));
				fc.utobj().sendKeys(driver, objAddTrainingPage.txtAttendeeTitle, dsTraining.get("attendeeTitle"));
				fc.utobj().sendKeys(driver, objAddTrainingPage.txtScore, dsTraining.get("score"));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, objAddTrainingPage.btnSubmit);
				String txtTraining = fc.utobj().getText(driver, objAddTrainingPage.tblTrainings);
				if (txtTraining.contains(trainingProgram)) {
					Reporter.log("Training created successfully");
				} else {
					fc.utobj().throwsException("Error - Training didn't got created. Test Case failes !!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in adding training , please refer screenshot!");
		}
	}

	public WebDriver addAgreement(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("********************** Add Agreement ****************** \n");

		String testCaseId = "TC_InFoMgr_MultiUnit_Agreement_Add";

		Map<String, String> dsAgreement = fc.utobj().readTestData("infomgr", testCaseId);

		InfoMgrAgreementPage infoMgrAgreementPage = new InfoMgrMultiUnitEntityPage(driver).getAgreementPage();
		fc.utobj().clickElement(driver, infoMgrAgreementPage.lnkAgreement);

		/*
		 * String openDateVal = fc.utobj().getText(driver,
		 * infoMgrAgreementPage.lblOpeningDate); SimpleDateFormat sdfDate = new
		 * SimpleDateFormat("MM/dd/yyyy"); Date closingDate =new
		 * Date(openDateVal); closingDate = DateUtils.addDays(closingDate , 10);
		 * String closingDateStr = sdfDate.format(closingDate);
		 */

		String comments = fc.utobj().generateTestData(dsAgreement.get("comments"));

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtApprovedDate,
						fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtDateExecuted,
						fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtEffectiveDate,
						fc.utobj().getFutureDateUSFormat(-10));
				// fc.utobj().sendKeysDateField(driver,
				// infoMgrAgreementPage.txtExpirationDate, closingDateStr);
				// fc.utobj().sendKeysDateField(driver,
				// infoMgrAgreementPage.txtClosingDate, closingDateStr);

				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtStateAddendum, dsAgreement.get("stateAddendum"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtOtherAddendum, dsAgreement.get("otherAddendum"));
				fc.utobj().clickRadioButton(driver, infoMgrAgreementPage.radioRightsOfFirstRefusal,
						dsAgreement.get("rightsofFirstRefusal"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtProtectedTerritory,
						dsAgreement.get("protectedTerritory"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtSalesperson, dsAgreement.get("salesPerson"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtPreviousLicenseNumber,
						dsAgreement.get("previousLicenseNumber"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRelatedCenter, dsAgreement.get("relatedCenter"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRequiredOpeningDate,
						fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtStoreSoldDate,
						fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtComments, comments);
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtInitialTerm, dsAgreement.get("initialTerm"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRenewalTermFirst, dsAgreement.get("termFirst"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRenewalDueDateFirst,
						fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRenewalFeeFirst, dsAgreement.get("feeFirst"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRenewalFeePaidFirstDate,
						fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRenewalTermSecond, dsAgreement.get("termSecond"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRenewalDueDateSecond,
						fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRenewalFeeSecond, dsAgreement.get("feeSecond"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRenewalFeePaidSecondDate,
						fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRenewalTermThird, dsAgreement.get("termThird"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRenewalDueDateThird,
						fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRenewalFeeThird, dsAgreement.get("feeThird"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRenewalFeePaidThirdDate,
						fc.utobj().getFutureDateUSFormat(-10));
				// Contact Details
				fc.utobj().selectDropDownByVisibleText(driver, infoMgrAgreementPage.drpContactTitle,
						dsAgreement.get("title"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtFirstName, dsAgreement.get("firstName"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtLastName, dsAgreement.get("lastName"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtStreetAddress, dsAgreement.get("streetAddress"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtCity, dsAgreement.get("city"));
				fc.utobj().selectDropDownByVisibleText(driver, infoMgrAgreementPage.drpCountry,
						dsAgreement.get("country"));
				fc.utobj().sleep();
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtZipcode, dsAgreement.get("zip"));
				fc.utobj().selectDropDownByVisibleText(driver, infoMgrAgreementPage.drpState, dsAgreement.get("state"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtPhoneNumbers, dsAgreement.get("phone"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtExtn, dsAgreement.get("phoneExtension"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtFaxNumbers, dsAgreement.get("fax"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtMobileNumbers, dsAgreement.get("mobile"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtMailIds, dsAgreement.get("email"));

				// Financial Notes
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtDescription1, dsAgreement.get("description1"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtAmount1, dsAgreement.get("amount1"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtAmountTerm1, dsAgreement.get("amountTerm1"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtInterest1, dsAgreement.get("interest1"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtComment1, dsAgreement.get("comments1"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtDescription2, dsAgreement.get("description2"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtAmount2, dsAgreement.get("amount2"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtAmountTerm2, dsAgreement.get("amountTerm2"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtInterest2, dsAgreement.get("interest2"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtComment2, dsAgreement.get("comments2"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtDescription3, dsAgreement.get("description3"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtAmount3, dsAgreement.get("amount3"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtAmountTerm3, dsAgreement.get("amountTerm3"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtInterest3, dsAgreement.get("interest3"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtComment3, dsAgreement.get("comments3"));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, infoMgrAgreementPage.btnsubmit);

				if (fc.utobj().assertPageSource(driver, comments)) {
					Reporter.log("Agreement has been added successfully");
				} else {
					fc.utobj().throwsException("Agreement couldn't be added . Test couldn't continue !!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in adding agreement , please refer screenshot!");
		}
		return driver;
	}

	public String logACall(WebDriver driver, Map<String, String> config, boolean isFromContactHistoryPage)
			throws Exception {
		Reporter.log("********************** Loggin a Call ****************** \n");

		String testCaseId = "TC_InFoMgr_MultiUnit_LogACall";

		Map<String, String> dsLogACall = fc.utobj().readTestData("infomgr", testCaseId);
		InfoMgrLogACallPage logACallPage = new InfoMgrMultiUnitEntityPage(driver).getLogACallPage();
		fc.commonMethods().switch_cboxIframe_frameId(driver);
		String subject = fc.utobj().generateTestData(dsLogACall.get("subject"));

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().sendKeys(driver, logACallPage.txtSubject, subject);
				fc.utobj().sendKeys(driver, logACallPage.txtDate, fc.utobj().getFutureDateUSFormat(5));
				fc.utobj().selectDropDownByPartialText(driver, logACallPage.drpStatus, dsLogACall.get("status"));
				fc.utobj().selectDropDownByPartialText(driver, logACallPage.txtTimeHour, dsLogACall.get("timeHour"));
				fc.utobj().selectDropDownByPartialText(driver, logACallPage.txtTimeMinute, dsLogACall.get("timeMin"));
				fc.utobj().selectDropDownByPartialText(driver, logACallPage.txtAMPM, dsLogACall.get("timeAMPM"));

				fc.utobj().selectDropDownByPartialText(driver, logACallPage.drpCallType, dsLogACall.get("typeofCall"));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, logACallPage.btnSubmit);
				fc.utobj().sleep();
				fc.utobj().clickElement(driver, logACallPage.btnNo);
				fc.utobj().sleep();
				fc.utobj().switchFrameToDefault(driver);

				if (isFromContactHistoryPage) {
					if (fc.utobj().assertLinkText(driver, subject)) {
						Reporter.log("Log a call set up has been done successfully");
					} else {
						fc.utobj().throwsException("Log a Call has been failed.");
					}
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in logging a call , please refer screenshot!");
		}
		return subject;
	}

	public String logaTask(WebDriver driver, String corporateUser, boolean isFromContactHistoryPage) throws Exception {
		Reporter.log("********************** Loggin a Task ****************** \n");

		String testCaseId = "TC_InFoMgr_MultiUnit_LogATask";
		WebDriverWait wait = new WebDriverWait(driver, 60);

		Map<String, String> dsLogATask = fc.utobj().readTestData("infomgr", testCaseId);

		InfoMgrLogATaskPage logATaskPage = new InfoMgrMultiUnitEntityPage(driver).getLogATaskPage();
		String subject = fc.utobj().generateTestData(dsLogATask.get("subject"));
		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().selectValFromMultiSelectWithoutReset(driver, logATaskPage.multiCheckBoxCorporateUsers, corporateUser);
				//fc.utobj().selectValFromMultiSelect(driver, logATaskPage.multiCheckBoxCorporateUsers, corporateUser);
				fc.utobj().selectDropDownByPartialText(driver, logATaskPage.drpStatus, dsLogATask.get("status"));
				fc.utobj().selectDropDownByVisibleText(driver, logATaskPage.drpTaskType, dsLogATask.get("taskType"));
				fc.utobj().sendKeys(driver, logATaskPage.txtSubject, subject);
				fc.utobj().sendKeys(driver, logATaskPage.txtStartDate, fc.utobj().getFutureDateUSFormat(5));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");
				fc.utobj().clickElement(driver, logATaskPage.btnAdd);
				fc.utobj().switchFrameToDefault(driver);
				if (isFromContactHistoryPage) {
					boolean isSubjectOnPage = fc.utobj().assertPageSource(driver, subject);
					if (isSubjectOnPage) {
						Reporter.log("Log a Task has been done successfully");
					} else {
						fc.utobj().throwsException("Log a Task failed. Test case fails ");
					}
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in loggin a task , please refer screenshot!");
		}
		return subject;
	}

	public void addContractSigning(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("**************************** Add Contract Signing Info  ***********");

		String testCaseId = "TC_InfoMgr_MultiUnit_ContractSigning_Add";

		Map<String, String> dsAddresses = fc.utobj().readTestData("infomgr", testCaseId);

		// Click on contract signing tab
		InfoMgrContractSigningPage contractSigning = new InfoMgrMultiUnitEntityPage(driver).getContractSigningPage();
		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().sendKeys(driver, contractSigning.txtContractReceivedSignedDate,
						fc.utobj().getFutureDateUSFormat(10));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, contractSigning.btnSubmit);
				if (fc.utobj().assertPageSource(driver, "Signing Commissions")
						&& fc.utobj().assertPageSource(driver, "Ancillary Documents")) {
					Reporter.log("Contract Signing data have been saved successfully !!! ");
				} else {
					fc.utobj().throwsException("Contract Signing data couldn't be saved. Test failes !!! ");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in adding Contract Signing Information  , please refer screenshot!");
		}
	}

	public void addCustomerComplaints(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("****************************** Adding Customer Complaints ********************** \n");

		String testCaseId = "TC_InfoMgr_MultiUnit_Add_Customer_Complaints";
		Map<String, String> dsCustomerComplaints = fc.utobj().readTestData("infomgr", testCaseId);
		InfoMgrCustomerComplaintsPage customerComplaintsPage = new InfoMgrMultiUnitEntityPage(driver)
				.getCustomerComplaintsPage();
		// Fill in the details
		String complaintID = fc.utobj().generateTestData(dsCustomerComplaints.get("complaintID"));
		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().selectDropDownByIndex(driver, customerComplaintsPage.drpFranchiseID, 1);
				fc.utobj().sendKeys(driver, customerComplaintsPage.txtComplaintID, complaintID);
				fc.utobj().sendKeys(driver, customerComplaintsPage.txtComplaintDate,
						fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, customerComplaintsPage.txtComplaint, dsCustomerComplaints.get("complaint"));
				fc.utobj().selectDropDownByVisibleText(driver, customerComplaintsPage.drpComplaintType,
						dsCustomerComplaints.get("type"));
				fc.utobj().sendKeys(driver, customerComplaintsPage.txtIncidentDate,
						fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, customerComplaintsPage.txtReceivedVia,
						dsCustomerComplaints.get("receivedVia"));
				fc.utobj().sendKeys(driver, customerComplaintsPage.txtSummary,
						dsCustomerComplaints.get("summaryofIncident"));
				fc.utobj().sendKeys(driver, customerComplaintsPage.txtActionTaken,
						dsCustomerComplaints.get("actionTaken"));
				fc.utobj().sendKeys(driver, customerComplaintsPage.txtDocumentTitle,
						dsCustomerComplaints.get("documentTitle"));
				fc.utobj().sendKeys(driver, customerComplaintsPage.txtDocumentAttachment,
						dsCustomerComplaints.get("document"));
				fc.utobj().sendKeys(driver, customerComplaintsPage.txtComplaintBy,
						dsCustomerComplaints.get("complainantName"));
				fc.utobj().sendKeys(driver, customerComplaintsPage.txtStreetAddress,
						dsCustomerComplaints.get("streetAddress"));
				fc.utobj().sendKeys(driver, customerComplaintsPage.txtCity, dsCustomerComplaints.get("city"));
				fc.utobj().selectDropDownByPartialText(driver, customerComplaintsPage.drpCountry,
						dsCustomerComplaints.get("country"));
				fc.utobj().sendKeys(driver, customerComplaintsPage.txtZipCode, dsCustomerComplaints.get("zip"));
				fc.utobj().selectDropDownByVisibleText(driver, customerComplaintsPage.drpState,
						dsCustomerComplaints.get("state"));
				fc.utobj().sendKeys(driver, customerComplaintsPage.txtEmailIds, dsCustomerComplaints.get("email"));
				fc.utobj().sendKeys(driver, customerComplaintsPage.txtPhoneNumbers, dsCustomerComplaints.get("phone"));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, customerComplaintsPage.btnSubmit);
				boolean isComplaintIDPresent = fc.utobj().assertPageSource(driver, complaintID);
				if (isComplaintIDPresent) {
					Reporter.log("Complaint created successfully !!!");
				} else {
					fc.utobj().throwsException("Error in adding customer complaints !!! ");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in adding Customer Complaints , please refer screenshot!");
		}
	}

	public String addEmployee(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("*********************** Add Employess ****************************** ");

		String testCaseId = "TC_InFoMgr_MultiUnit_Add_Employee";

		Map<String, String> dsEmployee = fc.utobj().readTestData("infomgr", testCaseId);

		InfoMgrEmployeesPage employeesPage = new InfoMgrMultiUnitEntityPage(driver).getEmployeesPage();

		String firstEmployeeName = fc.utobj().generateTestData(dsEmployee.get("firstName"));

		try {
			fc.utobj().clickElement(driver, employeesPage.btnAddMoreEmp);
		} catch (Exception ex) {

		}

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().selectDropDownByIndex(driver, employeesPage.drpFranchiseID, 1);
				fc.utobj().selectDropDownByVisibleText(driver, employeesPage.drpSalutation,
						dsEmployee.get("salutation"));
				fc.utobj().sendKeys(driver, employeesPage.txtFirstName, firstEmployeeName);
				fc.utobj().sendKeys(driver, employeesPage.txtLastName, dsEmployee.get("lastName"));
				fc.utobj().sendKeys(driver, employeesPage.txtAddress, dsEmployee.get("address"));
				fc.utobj().sendKeys(driver, employeesPage.txtCity, dsEmployee.get("city"));
				fc.utobj().selectDropDownByVisibleText(driver, employeesPage.drpCountry, dsEmployee.get("country"));
				fc.utobj().sendKeys(driver, employeesPage.txtZipcode, dsEmployee.get("zip"));
				fc.utobj().selectDropDownByVisibleText(driver, employeesPage.drpState, dsEmployee.get("state"));
				fc.utobj().sendKeys(driver, employeesPage.txtPhone1, dsEmployee.get("phone1"));
				fc.utobj().sendKeys(driver, employeesPage.txtPhone2, dsEmployee.get("phone2"));
				fc.utobj().sendKeys(driver, employeesPage.txtFax, dsEmployee.get("fax"));
				fc.utobj().sendKeys(driver, employeesPage.txtMobile, dsEmployee.get("mobile"));
				fc.utobj().sendKeys(driver, employeesPage.txtEmail, dsEmployee.get("email"));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, employeesPage.btnAdd);
				fc.utobj().sleep();
				fc.utobj().clickElement(driver, employeesPage.btnCancel);
				if (fc.utobj().assertPageSource(driver, firstEmployeeName)) {
					Reporter.log("Employees have been added successfully");
				} else {
					fc.utobj().throwsException("Employee couldn't be added . Test couldn't continue !!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in adding Employee , please refer screenshot!");
		}
		return firstEmployeeName;
	}

	public String addEntityDetails(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("*********************** Add Entity Details ****************************** ");

		String testCaseId = "TC_InFoMgr_MultiUnit_Add_Entity_Details";

		Map<String, String> dsEntityDetails = fc.utobj().readTestData("infomgr", testCaseId);

		InfoMgrEntityDetailsPage entityDetailsPage = new InfoMgrMultiUnitEntityPage(driver).getEntityDetailsPage();

		String entityName = fc.utobj().generateTestData(dsEntityDetails.get("entityName"));

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().sendKeys(driver, entityDetailsPage.txtEntityName, entityName);
				fc.utobj().sendKeys(driver, entityDetailsPage.txtAddress, dsEntityDetails.get("address"));
				fc.utobj().sendKeys(driver, entityDetailsPage.txtCity, dsEntityDetails.get("city"));
				fc.utobj().selectDropDownByVisibleText(driver, entityDetailsPage.drpCountry,
						dsEntityDetails.get("country"));
				fc.utobj().sendKeys(driver, entityDetailsPage.txtZipCode, dsEntityDetails.get("zip"));
				fc.utobj().selectDropDownByVisibleText(driver, entityDetailsPage.drpState,
						dsEntityDetails.get("state"));
				fc.utobj().sendKeys(driver, entityDetailsPage.txtPhoneNumber, dsEntityDetails.get("phone"));
				fc.utobj().sendKeys(driver, entityDetailsPage.txtExtension, dsEntityDetails.get("phoneExt"));
				fc.utobj().sendKeys(driver, entityDetailsPage.txtFaxNumber, dsEntityDetails.get("fax"));
				fc.utobj().sendKeys(driver, entityDetailsPage.txtMobileNumber, dsEntityDetails.get("mobile"));
				fc.utobj().sendKeys(driver, entityDetailsPage.txtEmailIds, dsEntityDetails.get("email"));
				fc.utobj().selectDropDownByVisibleText(driver, entityDetailsPage.drpCountryFormation,
						dsEntityDetails.get("countryofFormation"));
				fc.utobj().sleep();
				fc.utobj().sendKeys(driver, entityDetailsPage.txtDateFormation,
						fc.utobj().getFutureDateUSFormat(-5));
				fc.utobj().selectDropDownByVisibleText(driver, entityDetailsPage.drpStateFormation,
						dsEntityDetails.get("stateofFormation"));
				fc.utobj().sendKeys(driver, entityDetailsPage.txtTaxPayer, dsEntityDetails.get("taxpayerID"));
				fc.utobj().selectDropDownByVisibleText(driver, entityDetailsPage.drpEntityType,
						dsEntityDetails.get("entityType"));
				fc.utobj().sendKeys(driver, entityDetailsPage.txtFormationDocumentTitle,
						dsEntityDetails.get("formationDocTitle"));
				fc.utobj().sendKeys(driver, entityDetailsPage.txtFormationDocumentAttachment,
						dsEntityDetails.get("formationDocAttachment"));
				fc.utobj().sendKeys(driver, entityDetailsPage.txtGoverningDocumentTitle,
						dsEntityDetails.get("govereningDocumentTitle"));
				fc.utobj().sendKeys(driver, entityDetailsPage.txtGoverningDocumentAttachment,
						dsEntityDetails.get("govreeningDocAttachment"));
				fc.utobj().clickElement(driver, entityDetailsPage.btnAdd);
				if (fc.utobj().assertPageSource(driver, entityName)) {
					Reporter.log("Entity details have been added successfully");
				} else {
					fc.utobj().throwsException("Entity detail couldn't be added . Test couldn't continue !!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in adding entity details , please refer screenshot!");
		}
		return entityName;
	}

	public String addEventDetails(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("*********************** Add Event Details ****************************** ");

		String testCaseId = "TC_InFoMgr_MultiUnit_Add_Event_Details";

		Map<String, String> dsEventDetails = fc.utobj().readTestData("infomgr", testCaseId);

		InfoMgrEventsPage eventsPage = new InfoMgrMultiUnitEntityPage(driver).getEventsPage();

		String eventSummary = fc.utobj().generateTestData(dsEventDetails.get("summary"));

		if (fc.utobj().validate(testCaseId) == true) {
			try {

				fc.utobj().sendKeys(driver, eventsPage.txtDate, fc.utobj().getFutureDateUSFormat(10));
				fc.utobj().sendKeys(driver, eventsPage.txtOrganizer, dsEventDetails.get("organizer"));
				fc.utobj().sendKeys(driver, eventsPage.txtType, dsEventDetails.get("type"));
				fc.utobj().sendKeys(driver, eventsPage.txtSummary, eventSummary);
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, eventsPage.btnAdd);
				if (fc.utobj().assertPageSource(driver, eventSummary)) {
					Reporter.log("Event details have been added successfully");
				} else {
					fc.utobj().throwsException("Event detail couldn't be added . Test couldn't continue !!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in adding event details , please refer screenshot!");
		}
		return eventSummary;
	}

	public String addGuarantor(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("*********************** Add Gurantor Details ****************************** ");

		String testCaseId = "TC_InFoMgr_MultiUnit_Add_Gurantor_Details";

		Map<String, String> dsGuarantor = fc.utobj().readTestData("infomgr", testCaseId);

		InfoMgrGuarantorsPage guarantorPage = new InfoMgrMultiUnitEntityPage(driver).getGuarantorsPage();

		String guarantorName = fc.utobj().generateTestData(dsGuarantor.get("firstName"));

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().selectDropDownByVisibleText(driver, guarantorPage.drpTitle, dsGuarantor.get("title"));
				fc.utobj().sendKeys(driver, guarantorPage.txtFirstName, guarantorName);
				fc.utobj().sendKeys(driver, guarantorPage.txtLastName, dsGuarantor.get("lastName"));
				fc.utobj().selectDropDownByVisibleText(driver, guarantorPage.drpResidencyState,
						dsGuarantor.get("stateOfFormation"));
				fc.utobj().sendKeys(driver, guarantorPage.txtPercentage, dsGuarantor.get("percentage"));
				fc.utobj().sendKeys(driver, guarantorPage.txtStreetAddress, dsGuarantor.get("address"));
				fc.utobj().sendKeys(driver, guarantorPage.txtCity, dsGuarantor.get("city"));
				fc.utobj().selectDropDownByVisibleText(driver, guarantorPage.drpCountry, dsGuarantor.get("country"));
				fc.utobj().sendKeys(driver, guarantorPage.txtZipCode, dsGuarantor.get("zip"));
				fc.utobj().selectDropDownByVisibleText(driver, guarantorPage.drpState, dsGuarantor.get("state"));
				fc.utobj().sendKeys(driver, guarantorPage.txtPhoneNumbers, dsGuarantor.get("phone"));
				fc.utobj().sendKeys(driver, guarantorPage.txtExtension, dsGuarantor.get("phoneExt"));
				fc.utobj().sendKeys(driver, guarantorPage.txtFaxNumbers, dsGuarantor.get("fax"));
				fc.utobj().sendKeys(driver, guarantorPage.txtMobileNumbers, dsGuarantor.get("mobile"));
				fc.utobj().sendKeys(driver, guarantorPage.txtEmailIds, dsGuarantor.get("email"));
				fc.utobj().sendKeys(driver, guarantorPage.txtComments, dsGuarantor.get("comments"));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, guarantorPage.btnAdd);
				if (fc.utobj().assertPageSource(driver, guarantorName)) {
					Reporter.log("guarantor details have been added successfully");
				} else {
					fc.utobj().throwsException("guarantor detail couldn't be added . Test couldn't continue !!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in adding guarantor details , please refer screenshot!");
		}
		return guarantorName;
	}

	public String addLegalViolation(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("*********************** Add Legal Violation Details ****************************** ");

		String testCaseId = "TC_InFoMgr_MultiUnit_Add_Legal_Violation_Details";

		Map<String, String> dsLegalViolation = fc.utobj().readTestData("infomgr", testCaseId);

		InfoMgrLegalViolationPage legalViolationPage = new InfoMgrMultiUnitEntityPage(driver).getLegalViolationPage();

		String legalViolationNumber = fc.utobj().generateRandomNumber();
		String subject = fc.utobj().generateTestData(dsLegalViolation.get("summary"));

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().sendKeys(driver, legalViolationPage.txtNumber, legalViolationNumber);
				fc.utobj().sendKeys(driver, legalViolationPage.txtDate, fc.utobj().getFutureDateUSFormat(10));
				fc.utobj().selectDropDownByVisibleText(driver, legalViolationPage.drpType,
						dsLegalViolation.get("type"));
				fc.utobj().selectDropDownByVisibleText(driver, legalViolationPage.drpStatus,
						dsLegalViolation.get("status"));
				fc.utobj().sendKeys(driver, legalViolationPage.txtCureDate,
						fc.utobj().getFutureDateUSFormat(10));
				fc.utobj().sendKeys(driver, legalViolationPage.txtDateCured,
						fc.utobj().getFutureDateUSFormat(15));
				fc.utobj().sendKeys(driver, legalViolationPage.txtSummary, subject);
				fc.utobj().sendKeys(driver, legalViolationPage.txtActionTaken, dsLegalViolation.get("actionTaken"));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, legalViolationPage.btnAdd);
				if (fc.utobj().assertPageSource(driver, subject)) {
					Reporter.log("Legal Violation details have been added successfully");
				} else {
					fc.utobj().throwsException("Legal violation detail couldn't be added . Test couldn't continue !!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in adding legal violation details , please refer screenshot!");
		}
		return subject;
	}

	public String addLenders(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("*********************** Add Lenders Details ****************************** ");

		String testCaseId = "TC_InFoMgr_MultiUnit_Add_Lenders_Details";

		Map<String, String> dsLenders = fc.utobj().readTestData("infomgr", testCaseId);

		InfoMgrLendersPage lendersPage = new InfoMgrMultiUnitEntityPage(driver).getLendersPage();

		String lendersFirstName = fc.utobj().generateTestData(dsLenders.get("firstName"));

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().selectDropDownByVisibleText(driver, lendersPage.drpTitle, dsLenders.get("title"));
				fc.utobj().sendKeys(driver, lendersPage.txtFirstName, lendersFirstName);
				fc.utobj().sendKeys(driver, lendersPage.txtLastName, dsLenders.get("lastName"));
				fc.utobj().sendKeys(driver, lendersPage.txtStreetaddress, dsLenders.get("streetAddress"));
				fc.utobj().sendKeys(driver, lendersPage.txtCity, dsLenders.get("city"));
				fc.utobj().selectDropDownByVisibleText(driver, lendersPage.drpCountry, dsLenders.get("country"));
				fc.utobj().sendKeys(driver, lendersPage.txtZipcCode, dsLenders.get("zip"));
				fc.utobj().selectDropDownByVisibleText(driver, lendersPage.drpstate, dsLenders.get("state"));
				fc.utobj().sendKeys(driver, lendersPage.txtCollateralAssignmentExpirationDate,
						fc.utobj().getFutureDateUSFormat(10));
				// fc.utobj().sendKeys(driver, lendersPage.txtComfortLetterForm,
				// dsLenders.get("formofComfortLetter") );
				fc.utobj().sendKeys(driver, lendersPage.txtComfortLetterDate,
						fc.utobj().getFutureDateUSFormat(10));

				fc.utobj().clickRadioButton(driver, lendersPage.rdoComfortAgreement, dsLenders.get("comfortAgreement"));

				fc.utobj().sendKeys(driver, lendersPage.txtDateComfortAgreementInfo,
						fc.utobj().getFutureDateUSFormat(10));

				fc.utobj().sendKeys(driver, lendersPage.txtMultiUnitDocumentTitle,
						dsLenders.get("formationDocumentTitle"));

				fc.utobj().sendKeys(driver, lendersPage.txtMultiUnitDocumentAttachment,
						dsLenders.get("documentAttachment"));

				fc.utobj().sendKeys(driver, lendersPage.txtLenderContactTitleOne, dsLenders.get("contact1Name"));

				fc.utobj().sendKeys(driver, lendersPage.txtLenderContactTitleTwo, dsLenders.get("contact2Name"));

				fc.utobj().sendKeys(driver, lendersPage.txtContact1Phone, dsLenders.get("contact1Phone"));
				fc.utobj().sendKeys(driver, lendersPage.txtContact2Phone, dsLenders.get("contact2Phone"));
				fc.utobj().sendKeys(driver, lendersPage.txtContact1PhoneExtn, dsLenders.get("contact1PhoneExtension"));
				fc.utobj().sendKeys(driver, lendersPage.txtContact2PhoneExtn, dsLenders.get("contact2PhoneExtension"));

				fc.utobj().sendKeys(driver, lendersPage.txtContact1Fax, dsLenders.get("contact1Fax"));
				fc.utobj().sendKeys(driver, lendersPage.txtContact2Fax, dsLenders.get("contact2Fax"));

				fc.utobj().sendKeys(driver, lendersPage.txtContact1Email, dsLenders.get("contact1Email"));

				fc.utobj().sendKeys(driver, lendersPage.txtContact2Email, dsLenders.get("contact2Email"));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, lendersPage.btnAdd);
				if (fc.utobj().assertPageSource(driver, lendersFirstName)) {
					Reporter.log("Lenders details have been added successfully");
				} else {
					fc.utobj().throwsException("Lenders detail couldn't be added . Test couldn't continue !!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in adding lenders details , please refer screenshot!");
		}
		return lendersFirstName;
	}

	public String addMarketingDetails(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("*********************** Add Marketing Details ****************************** ");

		String testCaseId = "TC_InFoMgr_MultiUnit_Add_Marketing_Details";

		Map<String, String> dsMarketing = fc.utobj().readTestData("infomgr", testCaseId);

		InfoMgrMarketingPage marketingPage = new InfoMgrMultiUnitEntityPage(driver).getMarketingPage();

		String marketingContactFirstName = fc.utobj().generateTestData(dsMarketing.get("firstName"));

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().selectDropDownByVisibleText(driver, marketingPage.drpTitle, dsMarketing.get("title"));
				fc.utobj().sendKeys(driver, marketingPage.txtFirstName, marketingContactFirstName);
				fc.utobj().sendKeys(driver, marketingPage.txtLastName, dsMarketing.get("lastName"));
				fc.utobj().sendKeys(driver, marketingPage.txtStreetAddress, dsMarketing.get("streetAddress"));
				fc.utobj().sendKeys(driver, marketingPage.txtCity, dsMarketing.get("city"));
				fc.utobj().selectDropDownByVisibleText(driver, marketingPage.drpCountry, dsMarketing.get("country"));
				fc.utobj().sendKeys(driver, marketingPage.txtZipCode, dsMarketing.get("zip"));
				fc.utobj().selectDropDownByVisibleText(driver, marketingPage.drpState, dsMarketing.get("state"));
				fc.utobj().sendKeys(driver, marketingPage.txtPhoneNumbers, dsMarketing.get("phone"));
				fc.utobj().sendKeys(driver, marketingPage.txtExtension, dsMarketing.get("phoneExtension"));
				fc.utobj().sendKeys(driver, marketingPage.txtFaxNumbers, dsMarketing.get("fax"));
				fc.utobj().sendKeys(driver, marketingPage.txtMobileNumbers, dsMarketing.get("mobile"));
				fc.utobj().sendKeys(driver, marketingPage.txtEmailIds, dsMarketing.get("email"));
				fc.utobj().clickRadioButton(driver, marketingPage.rdoGrandOpeningRequired,
						dsMarketing.get("grandOpeningRequired"));
				fc.utobj().sendKeys(driver, marketingPage.txtGrandOpeningCompletedDate,
						fc.utobj().getFutureDateUSFormat(10));

				fc.utobj().clickRadioButton(driver, marketingPage.rdoCouponRedemption,
						dsMarketing.get("couponRedemption"));

				fc.utobj().sendKeys(driver, marketingPage.txtCampaignName, dsMarketing.get("campaignName"));

				fc.utobj().clickRadioButton(driver, marketingPage.rdoCampaignParticipation,
						dsMarketing.get("campaignParticipation"));

				fc.utobj().sendKeys(driver, marketingPage.txtProgramName, dsMarketing.get("programName"));

				fc.utobj().clickRadioButton(driver, marketingPage.rdoProgramParticipation,
						dsMarketing.get("programParticipation"));

				fc.utobj().sendKeys(driver, marketingPage.txtDMA, dsMarketing.get("DMA"));
				fc.utobj().sendKeys(driver, marketingPage.txtComments, dsMarketing.get("comments"));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, marketingPage.btnSubmit);

				if (fc.utobj().assertPageSource(driver, marketingContactFirstName)) {
					Reporter.log("Marketing details have been added successfully");
				} else {
					fc.utobj().throwsException("Marketing detail couldn't be added . Test couldn't continue !!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in adding marketing details , please refer screenshot!");
		}
		return marketingContactFirstName;

	}

	public String addMystryReview(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("*********************** Add Mystry Review ****************************** ");

		String testCaseId = "TC_InFoMgr_MultiUnit_Add_Mystry_Review_Details";

		Map<String, String> dsMystryReview = fc.utobj().readTestData("infomgr", testCaseId);

		InfoMgrMystryReviewPage mystryReviewPage = new InfoMgrMultiUnitEntityPage(driver).getMystryReviewPage();

		String mystryReviewComments = fc.utobj().generateTestData(dsMystryReview.get("comments"));

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().sendKeys(driver, mystryReviewPage.txtInspectionDate,
						fc.utobj().getFutureDateUSFormat(-5));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtReportSentToFranchiseeDate,
						fc.utobj().getFutureDateUSFormat(-2));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtGeneralComments, mystryReviewComments);
				// Performance section
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValuePerformanceQ1,
						dsMystryReview.get("perfQuestion1"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValuePerformanceQ2,
						dsMystryReview.get("perfQuestion2"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValuePerformanceQ3,
						dsMystryReview.get("perfQuestion3"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValuePerformanceQ4,
						dsMystryReview.get("perfQuestion4"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValuePerformanceQ5,
						dsMystryReview.get("perfQuestion5"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValuePerformanceQ6,
						dsMystryReview.get("perfQuestion6"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValuePerformanceQ7,
						dsMystryReview.get("perfQuestion7"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValuePerformanceQ8,
						dsMystryReview.get("perfQuestion8"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtCommentsForPerformance,
						dsMystryReview.get("perfComments"));
				// Service Section
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValueServiceQ1,
						dsMystryReview.get("serviceQuestion1"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValueServiceQ2,
						dsMystryReview.get("serviceQuestion2"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValueServiceQ3,
						dsMystryReview.get("serviceQuestion3"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValueServiceQ4,
						dsMystryReview.get("serviceQuestion4"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValueServiceQ5,
						dsMystryReview.get("serviceQuestion5"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValueServiceQ6,
						dsMystryReview.get("serviceQuestion6"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValueServiceQ7,
						dsMystryReview.get("serviceQuestion7"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValueServiceQ8,
						dsMystryReview.get("serviceQuestion8"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValueServiceQ9,
						dsMystryReview.get("serviceQuestion9"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValueServiceQ10,
						dsMystryReview.get("serviceQuestion10"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValueServiceQ11,
						dsMystryReview.get("serviceQuestion11"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValueServiceQ12,
						dsMystryReview.get("serviceQuestion12"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtCommentsForService,
						dsMystryReview.get("serviceComments"));
				// Appearance section
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValueAppearanceQ1,
						dsMystryReview.get("appearanceQuestion1"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValueAppearanceQ2,
						dsMystryReview.get("appearanceQuestion2"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValueAppearanceQ3,
						dsMystryReview.get("appearanceQuestion3"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValueAppearanceQ4,
						dsMystryReview.get("appearanceQuestion4"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValueAppearanceQ5,
						dsMystryReview.get("appearanceQuestion5"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtCommentsForAppearance,
						dsMystryReview.get("appearanceComments"));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, mystryReviewPage.btnSubmit);
				if (fc.utobj().assertPageSource(driver, mystryReviewComments)) {
					Reporter.log("Mystry Review details have been added successfully");
				} else {
					fc.utobj().throwsException("Mystry review detail couldn't be added . Test couldn't continue !!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in adding mystry review details , please refer screenshot!");
		}
		return mystryReviewComments;
	}

	public String addPicture(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("*********************** Add Picture ****************************** ");

		String testCaseId = "TC_InFoMgr_MultiUnit_Add_Picture";

		Map<String, String> dsPicture = fc.utobj().readTestData("infomgr", testCaseId);

		InfoMgrPicturesPage picturePage = new InfoMgrMultiUnitEntityPage(driver).getPicturesPage();

		String pictureTitle = fc.utobj().generateTestData(dsPicture.get("title"));

		try {
			fc.utobj().clickElement(driver, picturePage.btnAddMore);
		} catch (Exception ex) {

		}

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().sendKeys(driver, picturePage.txtpictureTitle, pictureTitle);
				fc.utobj().sendKeys(driver, picturePage.txtpictureFilename, dsPicture.get("attachment"));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, picturePage.btnSubmit);
				if (fc.utobj().assertPageSource(driver, pictureTitle)) {
					Reporter.log("Picture has been added successfully");
				} else {
					fc.utobj().throwsException("Picture couldn't be added . Test couldn't continue !!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in adding picture , please refer screenshot!");
		}
		return pictureTitle;
	}

	public String addOwnerUser(WebDriver driver, Map<String, String> config, String owner) throws Exception {
		Reporter.log("*********************** Add User ****************************** ");

		String testCaseId = "TC_InFoMgr_MultiUnit_Add_User";

		Map<String, String> dsUser = fc.utobj().readTestData("infomgr", testCaseId);

		InfoMgrUserPage userPage = new InfoMgrMultiUnitEntityPage(driver).getUsersPage().getUserPage();
		;

		String loginID = fc.utobj().generateTestData(dsUser.get("loginID"));
		String phone = fc.utobj().generateRandomNumber();

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().selectDropDownByIndex(driver, userPage.drpFranchiseID, 1);
				fc.utobj().clickElement(driver, userPage.btnAddUser);
				WebDriverWait wait = new WebDriverWait(driver, 60);
				fc.utobj().sendKeys(driver, userPage.txtUserName, loginID);
				fc.utobj().sendKeys(driver, userPage.txtPassword, dsUser.get("password"));
				fc.utobj().sendKeys(driver, userPage.txtconfirmPassword, dsUser.get("confirmpassword"));
				fc.utobj().selectDropDownByVisibleText(driver, userPage.drpType, dsUser.get("type"));
				fc.utobj().selectValFromMultiSelect(driver, userPage.drpRoles, dsUser.get("role"));
				fc.utobj().check(userPage.chkIsDaylight, dsUser.get("allowDST"));
				fc.utobj().selectDropDownByVisibleText(driver, userPage.drpUserType, dsUser.get("userType"));
				fc.utobj().selectDropDownByVisibleText(driver, userPage.drpOwnerName, owner);
				fc.utobj().sendKeys(driver, userPage.txtJobTitle, dsUser.get("jobTitle"));
				// fc.utobj().selectDropDownByVisibleText(driver,
				// userPage.drpUserLanguage, dsUser.get("language") ); // No use
				// to change langaugae
				fc.utobj().sendKeys(driver, userPage.txtAddress, dsUser.get("streetAddress"));
				fc.utobj().sendKeys(driver, userPage.txtCity, dsUser.get("city"));
				fc.utobj().selectDropDownByVisibleText(driver, userPage.drpCountry, dsUser.get("country"));
				fc.utobj().sendKeys(driver, userPage.txtZipcode, dsUser.get("zip"));
				fc.utobj().selectDropDownByVisibleText(driver, userPage.drpState, dsUser.get("state"));
				fc.utobj().sendKeys(driver, userPage.txtPhone1, phone);
				fc.utobj().sendKeys(driver, userPage.txtPhoneExt1, dsUser.get("phone1Ext"));
				fc.utobj().sendKeys(driver, userPage.txtPhone2, dsUser.get("phone2"));
				fc.utobj().sendKeys(driver, userPage.txtPhoneExt2, dsUser.get("phone2Ext"));
				fc.utobj().sendKeys(driver, userPage.txtFax, dsUser.get("fax"));
				fc.utobj().sendKeys(driver, userPage.txtMobile, dsUser.get("mobile"));
				fc.utobj().sendKeys(driver, userPage.txtEmail, dsUser.get("email"));
				fc.utobj().sendKeys(driver, userPage.txtIpAddress, dsUser.get("ipAddress"));
				fc.utobj().check(userPage.chkIsBillable, dsUser.get("isBillable"));
				fc.utobj().selectDropDownByPartialText(driver, userPage.drpBirthMonth, dsUser.get("birthMonth"));
				fc.utobj().selectDropDownByPartialText(driver, userPage.drpBirthDate, dsUser.get("birthDate"));
				fc.utobj().sendKeys(driver, userPage.btnUserPictureName, dsUser.get("userPicture"));
				fc.utobj().sendKeys(driver, userPage.chkSendNotification, dsUser.get("sendNotification"));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, userPage.btnSubmit);
				if (fc.utobj().assertPageSource(driver, phone)) {
					Reporter.log("User has been added successfully");
				} else {
					fc.utobj().throwsException("User couldn't be added . Test couldn't continue !!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in adding user , please refer screenshot!");
		}
		return loginID;
	}

	public void addRealState(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("*********************** Add Real Estate ****************************** ");

		String testCaseId = "TC_InFoMgr_MultiUnit_Add_Real_Estate_Details";

		Map<String, String> dsRealState = fc.utobj().readTestData("infomgr", testCaseId);

		InfoMgrRealStatePage realStatePage = new InfoMgrMultiUnitEntityPage(driver).getRealStatePage();

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().sendKeys(driver, realStatePage.drpOwnedLeased, dsRealState.get("ownedorLeased"));
				fc.utobj().sendKeys(driver, realStatePage.txtSiteAddress1, dsRealState.get("siteStreet1"));
				fc.utobj().sendKeys(driver, realStatePage.txtSiteAddress2, dsRealState.get("siteStreet2"));
				fc.utobj().sendKeys(driver, realStatePage.txtSiteCity, dsRealState.get("siteCity"));
				fc.utobj().sendKeys(driver, realStatePage.drpCountry, dsRealState.get("country"));
				fc.utobj().sleep();
				fc.utobj().sendKeys(driver, realStatePage.drpState, dsRealState.get("state/Province"));
				fc.utobj().sendKeys(driver, realStatePage.txtSquareFootage, dsRealState.get("buildingSize"));
				fc.utobj().sendKeys(driver, realStatePage.txtBuildingDimentionsX, dsRealState.get("buildingLength"));
				fc.utobj().sendKeys(driver, realStatePage.txtBuildingDimentionsY, dsRealState.get("buildingBreadth"));
				fc.utobj().sendKeys(driver, realStatePage.txtBuildingDimentionsZ, dsRealState.get("buildingHeight"));
				fc.utobj().sendKeys(driver, realStatePage.txtParkingSpaces, dsRealState.get("parkingSpaces"));
				fc.utobj().sendKeys(driver, realStatePage.txtDealType, dsRealState.get("dealType"));
				fc.utobj().sendKeys(driver, realStatePage.drpPremisesType, dsRealState.get("typeofPremises"));
				fc.utobj().sendKeys(driver, realStatePage.txtLoiSent, fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, realStatePage.txtLeaseSignedDate,
						fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, realStatePage.txtApprovalDate,
						fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, realStatePage.txtLeaseCommencementDate,
						fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, realStatePage.txtExpirationDate,
						fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, realStatePage.txtRentIncreases,
						fc.utobj().getFutureDateUSFormat(100));

				fc.utobj().sendKeys(driver, realStatePage.txtMonthlyRent, dsRealState.get("currentMonthlyRent"));
				fc.utobj().clickRadioButton(driver, realStatePage.rdoAcal, dsRealState.get("ACAL"));
				fc.utobj().sendKeys(driver, realStatePage.txtMultiUnitDocumentTitle, dsRealState.get("documentTitle"));
				fc.utobj().sendKeys(driver, realStatePage.txtMultiUnitDocumentAttachment, dsRealState.get("document"));

				// Term First
				fc.utobj().sendKeys(driver, realStatePage.txtRenewalTermFirst, dsRealState.get("termFirst(Years)"));
				fc.utobj().sendKeys(driver, realStatePage.txtRenewalDueDateFirst,
						fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, realStatePage.txtRenewalFeeFirst, dsRealState.get("Fee-First"));
				fc.utobj().sendKeys(driver, realStatePage.txtRenewalFeePaidFirst,
						fc.utobj().getFutureDateUSFormat(-10));

				// Term Second
				fc.utobj().sendKeys(driver, realStatePage.txtRenewalTermSecond, dsRealState.get("termSecond"));
				fc.utobj().sendKeys(driver, realStatePage.txtRenewalDueDateSecond,
						fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, realStatePage.txtRenewalFeeSecond, dsRealState.get("feeSecond"));
				fc.utobj().sendKeys(driver, realStatePage.txtRenewalFeePaidSecond,
						fc.utobj().getFutureDateUSFormat(-10));

				// Term Third
				fc.utobj().sendKeys(driver, realStatePage.txtRenewalTermThird, dsRealState.get("termThird"));
				fc.utobj().sendKeys(driver, realStatePage.txtRenewalDueDateThird,
						fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, realStatePage.txtRenewalFeeThird, dsRealState.get("feeThird"));
				fc.utobj().sendKeys(driver, realStatePage.txtRenewalFeePaidThird,
						fc.utobj().getFutureDateUSFormat(-10));

				fc.utobj().sendKeys(driver, realStatePage.txtRenewalOptions, dsRealState.get("optionTerm(Years)"));
				fc.utobj().clickRadioButton(driver, realStatePage.rdoPurchaseOption, dsRealState.get("purchaseOption"));
				fc.utobj().sendKeys(driver, realStatePage.txtProjectedOpeningDate,
						fc.utobj().getFutureDateUSFormat(-10));

				fc.utobj().clickRadioButton(driver, realStatePage.rdoGeneralContractorSelector,
						dsRealState.get("generalContractorSelected"));
				if (dsRealState.get("generalContractorSelected").equalsIgnoreCase("yes")) {
					fc.utobj().sendKeys(driver, realStatePage.txtNameGeneralContractor,
							dsRealState.get("generalContractorName"));
					fc.utobj().sendKeys(driver, realStatePage.txtAddressGeneralContractor,
							dsRealState.get("generalContractorAddress"));
				}

				fc.utobj().sendKeys(driver, realStatePage.txtPermitApplied,
						fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, realStatePage.txtPermitIssued,
						fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, realStatePage.txtCertificate,
						fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, realStatePage.txtTurnOverDate,
						fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, realStatePage.txtGrandOpeningDate,
						fc.utobj().getFutureDateUSFormat(-10));

				// Lessor details
				fc.utobj().selectDropDownByVisibleText(driver, realStatePage.drpLessorTitle,
						dsRealState.get("lessor_title"));
				fc.utobj().sendKeys(driver, realStatePage.txtLessorFirstName, dsRealState.get("lessor_firstName"));
				fc.utobj().sendKeys(driver, realStatePage.txtLessorLastName, dsRealState.get("lessor_lastName"));
				fc.utobj().sendKeys(driver, realStatePage.txtLessorStreetAddress,
						dsRealState.get("lessor_streetAddress"));
				fc.utobj().sendKeys(driver, realStatePage.txtLessorCity, dsRealState.get("lessor_city"));
				fc.utobj().selectDropDownByVisibleText(driver, realStatePage.drpLessorCountry,
						dsRealState.get("lessor_country"));
				fc.utobj().sleep();
				fc.utobj().sendKeys(driver, realStatePage.txtLessorZipCode, dsRealState.get("lessor_zip"));
				fc.utobj().selectDropDownByVisibleText(driver, realStatePage.drpLessorState,
						dsRealState.get("lessor_state"));
				fc.utobj().sendKeys(driver, realStatePage.txtLessorPhoneNumbers, dsRealState.get("lessor_phone"));
				fc.utobj().sendKeys(driver, realStatePage.txtLessorExtension, dsRealState.get("lessor_phoneExtension"));
				fc.utobj().sendKeys(driver, realStatePage.txtLessorFaxNumbers, dsRealState.get("lessor_fax"));
				fc.utobj().sendKeys(driver, realStatePage.txtLessorMobileNumbers, dsRealState.get("lessor_mobile"));
				fc.utobj().sendKeys(driver, realStatePage.txtLessorEmailIds, dsRealState.get("lessor_email"));

				// Sub-lessor details
				fc.utobj().selectDropDownByVisibleText(driver, realStatePage.drpSubLessorTitle,
						dsRealState.get("sublessor_title"));
				fc.utobj().sendKeys(driver, realStatePage.txtSublessorFirstName,
						dsRealState.get("sublessor_firstName"));
				fc.utobj().sendKeys(driver, realStatePage.txtSublessorLastName, dsRealState.get("sublessor_lastName"));
				fc.utobj().sendKeys(driver, realStatePage.txtSubLessorStreetAddress,
						dsRealState.get("sublessor_streetAddress"));
				fc.utobj().sendKeys(driver, realStatePage.txtSubLessorCity, dsRealState.get("sublessor_city"));
				fc.utobj().selectDropDownByVisibleText(driver, realStatePage.drpSubLessorCountry,
						dsRealState.get("sublessor_country"));
				fc.utobj().sleep();
				fc.utobj().sendKeys(driver, realStatePage.txtSubLessorZipCode, dsRealState.get("sublessor_zip"));
				fc.utobj().selectDropDownByVisibleText(driver, realStatePage.drpSubLessorState,
						dsRealState.get("sublessor_state"));
				fc.utobj().sendKeys(driver, realStatePage.txtSubLessorPhoneNumbers, dsRealState.get("sublessor_phone"));
				fc.utobj().sendKeys(driver, realStatePage.txtSubLessorExtension,
						dsRealState.get("sublessor_phoneExtension"));
				fc.utobj().sendKeys(driver, realStatePage.txtSubLessorFaxNumbers, dsRealState.get("sublessor_fax"));
				fc.utobj().sendKeys(driver, realStatePage.txtSubLessorMobileNumbers,
						dsRealState.get("sublessor_mobile"));
				fc.utobj().sendKeys(driver, realStatePage.txtSubLessorEmailIds, dsRealState.get("sublessor_email"));

				// Tenant Details
				fc.utobj().selectDropDownByVisibleText(driver, realStatePage.drpTenantTitle,
						dsRealState.get("tenant_title"));
				fc.utobj().sendKeys(driver, realStatePage.txtTenantFirstName, dsRealState.get("tenant_firstName"));
				fc.utobj().sendKeys(driver, realStatePage.txtTenantLastName, dsRealState.get("tenant_lastName"));
				fc.utobj().sendKeys(driver, realStatePage.txtTenantStreetAddress,
						dsRealState.get("tenant_streetAddress"));
				fc.utobj().sendKeys(driver, realStatePage.txtTenantCity, dsRealState.get("tenant_city"));
				fc.utobj().selectDropDownByVisibleText(driver, realStatePage.drpTenantCountry,
						dsRealState.get("tenant_country"));
				fc.utobj().sleep();
				fc.utobj().sendKeys(driver, realStatePage.txtTenantZipCode, dsRealState.get("tenant_zip"));
				fc.utobj().selectDropDownByVisibleText(driver, realStatePage.drpTenantState,
						dsRealState.get("tenant_state"));
				fc.utobj().sendKeys(driver, realStatePage.txtTenantPhoneNumbers, dsRealState.get("tenant_phone"));
				fc.utobj().sendKeys(driver, realStatePage.txtTenantExtension, dsRealState.get("tenant_phoneExtension"));
				fc.utobj().sendKeys(driver, realStatePage.txtTenantFaxNumbers, dsRealState.get("tenant_fax"));
				fc.utobj().sendKeys(driver, realStatePage.txtTenantMobileNumbers, dsRealState.get("tenant_mobile"));
				fc.utobj().sendKeys(driver, realStatePage.txtTenantEmailIds, dsRealState.get("tenant_email"));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, realStatePage.btnAdd);

				if (fc.utobj().assertLinkText(driver, "Modify")) {
					Reporter.log("Real Estate details has been added successfully");
				} else {
					fc.utobj().throwsException("Real Estate Details couldn't be added . Test couldn't continue !!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in adding real estate details , please refer screenshot!");
		}
	}

	public String addTerritory(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("*********************** Add Territory Details ****************************** ");

		String testCaseId = "TC_InFoMgr_MultiUnit_Add_Territory_Details";

		Map<String, String> dsTerritory = fc.utobj().readTestData("infomgr", testCaseId);

		InfoMgrTerritoryPage territoryPage = new InfoMgrMultiUnitEntityPage(driver).getTerritoryPage();

		String txtNotes = fc.utobj().generateTestData(dsTerritory.get("notes"));

		if (fc.utobj().validate(testCaseId) == true) {
			try {

				fc.utobj().clickRadioButton(driver, territoryPage.rdoTypeTerritory, dsTerritory.get("typeofTerritory"));
				fc.utobj().sendKeys(driver, territoryPage.txtSalesRequirement,
						dsTerritory.get("minimumSalesRequirement"));
				fc.utobj().sendKeys(driver, territoryPage.txtRestrictions, dsTerritory.get("restrictionsonFranchisee"));
				fc.utobj().sendKeys(driver, territoryPage.txtMarketingObligation,
						dsTerritory.get("franchiseesMarketingObligations"));
				fc.utobj().sendKeys(driver, territoryPage.txtOwned, dsTerritory.get("otherExclusiveTerritoriesOwned"));
				fc.utobj().sendKeys(driver, territoryPage.txtJuridiction, dsTerritory.get("jurisdiction"));
				fc.utobj().sendKeys(driver, territoryPage.txtDisputes, dsTerritory.get("disputesifany"));
				fc.utobj().sendKeys(driver, territoryPage.txtNotes, txtNotes);
				fc.utobj().sendKeys(driver, territoryPage.txtGeoCoordinates, dsTerritory.get("geographicCoordinates"));
				fc.utobj().sendKeys(driver, territoryPage.txtLocation, dsTerritory.get("location"));
				fc.utobj().sendKeys(driver, territoryPage.txtCounty, dsTerritory.get("county"));
				fc.utobj().sendKeys(driver, territoryPage.txtZip, dsTerritory.get("zipOwnedExclusively"));
				fc.utobj().sendKeys(driver, territoryPage.txtLandBoundaries, dsTerritory.get("landBoundaries"));
				fc.utobj().sendKeys(driver, territoryPage.txtAraeSize, dsTerritory.get("areaSize"));
				fc.utobj().sendKeys(driver, territoryPage.txtNaturalHazards, dsTerritory.get("naturalHazards"));
				fc.utobj().sendKeys(driver, territoryPage.txtCompetitorsFranchise,
						dsTerritory.get("otherCompetitorsFranchiseTerritory"));
				fc.utobj().sendKeys(driver, territoryPage.txtProximity, dsTerritory.get("proximity"));
				fc.utobj().sendKeys(driver, territoryPage.txtCompetitionFive,
						dsTerritory.get("projectedCompetitionFiveYears"));
				fc.utobj().sendKeys(driver, territoryPage.txtCompetitionTen,
						dsTerritory.get("projectedCompetitionTenYear"));
				fc.utobj().sendKeys(driver, territoryPage.txtPopulation, dsTerritory.get("totalPopulation"));
				fc.utobj().sendKeys(driver, territoryPage.txtMedianIncome, dsTerritory.get("medianIncome"));
				fc.utobj().sendKeys(driver, territoryPage.txtPortsHarbors, dsTerritory.get("portsandHarbors"));
				fc.utobj().sendKeys(driver, territoryPage.txtAirport, dsTerritory.get("airports"));
				fc.utobj().sendKeys(driver, territoryPage.txtHeliports, dsTerritory.get("heliports"));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, territoryPage.btnSubmit);
				if (fc.utobj().assertPageSource(driver, txtNotes)) {
					Reporter.log("Territory details has been added successfully");
				} else {
					fc.utobj().throwsException("Territory details couldn't be added . Test couldn't continue !!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in adding territory details , please refer screenshot!");
		}
		return txtNotes;
	}

	public void addFinanceDetails(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("*********************** Add Finance Details ****************************** ");

		String testCaseId = "TC_InFoMgr_MultiUnit_Add_Finance_Details";

		Map<String, String> dsFinance = fc.utobj().readTestData("infomgr", testCaseId);

		InfoMgrFinancialsPage financePage = new InfoMgrMultiUnitEntityPage(driver).getFinancialsPage();

		if (fc.utobj().validate(testCaseId) == true) {
			try {

				fc.utobj().sendKeys(driver, financePage.drpTitle, dsFinance.get("title"));
				fc.utobj().sendKeys(driver, financePage.txtFirstName, dsFinance.get("firstName"));
				fc.utobj().sendKeys(driver, financePage.txtLastName, dsFinance.get("lastName"));
				fc.utobj().sendKeys(driver, financePage.txtStreetAddress, dsFinance.get("streetAddress"));
				fc.utobj().sendKeys(driver, financePage.txtCity, dsFinance.get("city"));
				fc.utobj().sendKeys(driver, financePage.drpCountry, dsFinance.get("country"));
				fc.utobj().sleep();
				fc.utobj().sendKeys(driver, financePage.txtZipCode, dsFinance.get("zip"));
				fc.utobj().sendKeys(driver, financePage.drpState, dsFinance.get("state"));
				fc.utobj().sendKeys(driver, financePage.txtPhoneNumbers, dsFinance.get("phone"));
				fc.utobj().sendKeys(driver, financePage.txtExtension, dsFinance.get("phoneExtension"));
				fc.utobj().sendKeys(driver, financePage.txtFaxNumbers, dsFinance.get("fax"));
				fc.utobj().sendKeys(driver, financePage.txtMobileNumbers, dsFinance.get("mobile"));
				fc.utobj().sendKeys(driver, financePage.txtEmailIds, dsFinance.get("email"));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, financePage.btnAdd);
				if (fc.utobj().assertLinkText(driver, "Modify")) {
					Reporter.log("Finance details has been added successfully");
				} else {
					fc.utobj().throwsException("Finance details couldn't be added . Test couldn't continue !!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in adding finance details , please refer screenshot!");
		}
	}

	public void addInsuranceDetails(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("*********************** Add Insurance Details ****************************** ");

		String testCaseId = "TC_InFoMgr_MultiUnit_Add_Insurance_Details";

		Map<String, String> dsInsurance = fc.utobj().readTestData("infomgr", testCaseId);

		InfoMgrInsurancePage insurancePage = new InfoMgrMultiUnitEntityPage(driver).getInsurancePage();

		String company = fc.utobj().generateTestData("CompanyName0");

		if (fc.utobj().validate(testCaseId) == true) {
			try {

				fc.utobj().sendKeys(driver, insurancePage.txtInsuranceCompanyName, company);

				// Contact Details
				fc.utobj().selectDropDownByVisibleText(driver, insurancePage.drpTitle, dsInsurance.get("title"));
				fc.utobj().sendKeys(driver, insurancePage.txtFirstName, dsInsurance.get("firstName"));
				fc.utobj().sendKeys(driver, insurancePage.txtLastName, dsInsurance.get("lastName"));
				fc.utobj().sendKeys(driver, insurancePage.txtStreetAddress, dsInsurance.get("streetAddress"));
				fc.utobj().sendKeys(driver, insurancePage.txtCity, dsInsurance.get("city"));
				fc.utobj().selectDropDownByVisibleText(driver, insurancePage.drpCountry, dsInsurance.get("country"));
				fc.utobj().sleep();
				fc.utobj().sendKeys(driver, insurancePage.txtZipCode, dsInsurance.get("zip"));
				fc.utobj().selectDropDownByVisibleText(driver, insurancePage.drpState, dsInsurance.get("state"));
				fc.utobj().sendKeys(driver, insurancePage.txtPhoneNumbers, dsInsurance.get("phone"));
				fc.utobj().sendKeys(driver, insurancePage.txtExtension, dsInsurance.get("phoneExtension"));
				fc.utobj().sendKeys(driver, insurancePage.txtFaxNumbers, dsInsurance.get("fax"));
				fc.utobj().sendKeys(driver, insurancePage.txtMobileNumbers, dsInsurance.get("mobile"));
				fc.utobj().sendKeys(driver, insurancePage.txtEmailIds, dsInsurance.get("email"));

				// Agency details
				fc.utobj().sendKeys(driver, insurancePage.txtAgencyName, dsInsurance.get("agencyName"));
				fc.utobj().sendKeys(driver, insurancePage.txtAgencyStreetAddress, dsInsurance.get("agencyAddress"));
				fc.utobj().sendKeys(driver, insurancePage.txtAgencyCity, dsInsurance.get("agencyCity"));
				fc.utobj().selectDropDownByVisibleText(driver, insurancePage.drpCountry,
						dsInsurance.get("agencyCountry"));
				fc.utobj().sleep();
				fc.utobj().sendKeys(driver, insurancePage.txtAgencyZipCode, dsInsurance.get("agencyZip"));
				fc.utobj().selectDropDownByVisibleText(driver, insurancePage.drpAgencyState,
						dsInsurance.get("agencyState"));
				fc.utobj().sendKeys(driver, insurancePage.txtAgencyPhoneNumbers, dsInsurance.get("agencyPhone"));
				fc.utobj().sendKeys(driver, insurancePage.txtAgencyExtension, dsInsurance.get("agencyPhoneExtension"));
				fc.utobj().sendKeys(driver, insurancePage.txtAgencyFaxNumbers, dsInsurance.get("agencyFax"));
				fc.utobj().sendKeys(driver, insurancePage.txtAgencyMobileNumbers, dsInsurance.get("agencyMobile"));
				fc.utobj().sendKeys(driver, insurancePage.txtAgencyFaxNumbers, dsInsurance.get("agencyFax"));
				fc.utobj().sendKeys(driver, insurancePage.txtAgencyMobileNumbers, dsInsurance.get("agencyMobile"));
				fc.utobj().sendKeys(driver, insurancePage.txtAgencyEmailIds, dsInsurance.get("agencyEmail"));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, insurancePage.btnAdd);
				if (fc.utobj().assertPageSource(driver, company)) {
					Reporter.log("Insurance details has been added successfully");
				} else {
					fc.utobj().throwsException("Insurance details couldn't be added . Test couldn't continue !!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in adding insurance details , please refer screenshot!");
		}
	}

	public String CreateVisitForm(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("*********************** Create Visit Form ****************************** ");

		String testCaseId = "TC_60_Create_Visit_At_Visit";
		String visitFormName = "";
		FieldOpsVisitsPage pobj = new FieldOpsVisitsPage(driver);

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				Map<String, String> dataSet = fc.utobj().readTestData("infomgr", testCaseId);
				AdminFieldOpsManageVisitFormPageTest visitFormPage = new AdminFieldOpsManageVisitFormPageTest();
				visitFormName = visitFormPage.addVisitFormPrivate(driver, dataSet);

				// Add Question
				fc.utobj().clickElement(driver, pobj.addQuestion);

				AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest customizeVisit = new AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest();
				customizeVisit.addQuestion(driver, dataSet);

				AdminFieldOpsQuestionLibraryPageTest questionLibrary = new AdminFieldOpsQuestionLibraryPageTest();
				fc.utobj().clickElement(driver, pobj.addQuestion);
				questionLibrary.addQuestionRatingType(driver, dataSet);

				fc.utobj().clickElement(driver, pobj.addQuestion);
				questionLibrary.addQuestionSingleSelectDropDownType(driver, dataSet);

				fc.utobj().clickElement(driver, pobj.addQuestion);
				questionLibrary.addQuestionSingleSelectRadioButtonType(driver, dataSet);

				fc.utobj().clickElement(driver, pobj.finishBtn);
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in creating visit form , please refer screenshot!");
		}
		return visitFormName;
	}

	public void addVisit(WebDriver driver, Map<String, String> config, String franchiseID, String corporateUser)
			throws Exception {
		// Create Visit Form
		String visitForm = CreateVisitForm(driver, config);

		Reporter.log("*********************** Add Visit ****************************** ");

		String testCaseId = "TC_InfoMgr__MultiUnit_Create_Visit";
		Map<String, String> dsVisit = fc.utobj().readTestData("infomgr", testCaseId);

		FieldOpsVisitsPage visitPage = new FieldOpsVisitsPage(driver);
		InfoMgrQAHistoryPage qaHistoryPage = new InfoMgrMultiUnitEntityPage(driver).getQAHistoryPage();
		String comments = fc.utobj().generateTestData(dsVisit.get("comments"));
		searchMultiUnitAndClick(driver, franchiseID);
		fc.utobj().clickElement(driver, qaHistoryPage.lnkQAHistory);
		fc.utobj().clickElement(driver, qaHistoryPage.lnkCreateVisit);

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				selectValFromRadioDrodown(driver, visitPage.drpVisitFormMainDiv, visitForm);
				selectValFromRadioDrodown(driver, visitPage.consultantSearchBoxDiv, corporateUser);
				fc.utobj().sendKeys(driver, visitPage.scheduleDateTime, fc.utobj().getFutureDateUSFormat(10));
				fc.utobj().sendKeys(driver, visitPage.comments, comments);

				JavascriptExecutor executor = (JavascriptExecutor) driver;
				executor.executeScript("arguments[0].click();", visitPage.schedule);
				;

				boolean isStatusPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
						".//*[.='" + franchiseID + "']/ancestor::tr/td[contains(text () ,'Scheduled')]");
				if (isStatusPresent == false) {
					fc.utobj().throwsException("Was not able to create Visit");
				}

				// Verify that Visit is displayed on the QA History Page
				searchMultiUnitAndClick(driver, franchiseID);
				fc.utobj().clickElement(driver, qaHistoryPage.lnkQAHistory);

				isStatusPresent = fc.utobj().assertPageSource(driver, corporateUser);
				if (isStatusPresent) {
					Reporter.log("Created visit is displayed on the QA history page");
				} else {
					fc.utobj().throwsException("Created visit is not displayed on the QA History page");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in adding visit, please refer screenshot!");
		}

	}

	// *************************************** Common methods
	// ************************************************

	public void selectValFromRadioDrodown(WebDriver driver, WebElement element, String value) throws Exception {
		try {
			fc.utobj().sleep(500);
			fc.utobj().clickElement(driver, element);
			fc.utobj().sleep(500);

			WebElement searchTextBox = element.findElement(By.xpath(".//input[@type='text']"));
			fc.utobj().sendKeys(driver, searchTextBox, value);
			fc.utobj().sleep(500);

			WebElement elementRadio = element.findElement(By.xpath(".//label[contains(text(),'" + value + "')]/input"));
			try {

				if (!elementRadio.isSelected()) {
					fc.utobj().clickElement(driver, elementRadio);
				}

			} catch (Exception e) {
				fc.utobj().throwsException("Search Value not present in Drop Down Box");
			}
		} catch (Exception e) {
			fc.utobj().throwsException("Problem in Drop Down Box");
		}
	}

	public void multiSelectListBox(WebDriver driver, WebElement multiSelectbox, String searchTxt) throws Exception {

		fc.utobj().clickElement(driver, multiSelectbox);
		WebElement newElement = multiSelectbox.findElement(By.xpath("./div/div/input[@class='searchInputMultiple']"));
		fc.utobj().sendKeys(driver, newElement, searchTxt);
		;

		WebElement checkboxElement = multiSelectbox.findElement(By.xpath(".//input[@id='selectAll']"));
		checkboxElement.click();
		fc.utobj().clickElement(driver, multiSelectbox);
		Thread.sleep(1000);
	}

	public void multiunitPageShowAll(WebDriver driver) throws Exception {
		fc.infomgr().infomgr_common().InfoMgrMultiUnitEntity(driver);
		ChangeToMultiUnitView(driver);
		if (fc.utobj().assertLinkText(driver, "Show All")) {
			fc.utobj().clickLink(driver, "Show All");
		}
	}

	public void checkLocation(WebDriver driver, String location) throws Exception {
		String xpath = ".//*[@id='siteMainTable']//a[text()='" + location + "']/preceding::input[@type='checkbox'][1]";
		WebElement checkBox = fc.utobj().getElementByXpath(driver, xpath);
		fc.utobj().clickElement(driver, checkBox);
	}

	public void VerifyPrintPreview(WebDriver driver, String textTobeVerified) throws Exception {
		Set<String> windows = new HashSet<String>();
		windows = driver.getWindowHandles();

		boolean isTextDisplayed = false;

		String currentWindow = driver.getWindowHandle();
		for (String window : windows) {
			if (!window.equals(currentWindow)) {

				driver.switchTo().window(window);
				if (driver.getCurrentUrl().contains("printcheck")) {
					isTextDisplayed = fc.utobj().assertPageSource(driver, textTobeVerified);
				}
			}
		}
		driver.close();

		if (isTextDisplayed) {
			Reporter.log(textTobeVerified + "  is displayed on the print preview page. !!! ");
		} else {
			fc.utobj().throwsException(textTobeVerified + "  is not displayed on the print preview page. !!! ");
		}

		driver.switchTo().window(currentWindow);
	}

	private void SelectToEmail(WebDriver driver, String corporateUser) throws Exception {
		;
		fc.commonMethods().switch_cboxIframe_frameId(driver);
		fc.utobj().sleep();
		InfoMgrAddressBook addressBook = new InfoMgrAddressBook(driver);
		fc.utobj().sleep();
		fc.utobj().sendKeys(driver, addressBook.txtCorpUsers, corporateUser);
		fc.utobj().sleep();
		fc.utobj().clickElement(driver, addressBook.btnSearchCorporateUser);
		fc.utobj().sleep();
		fc.utobj().clickElement(driver, addressBook.chkCorporateUserName);
		fc.utobj().clickElement(driver, addressBook.btnAdd);

	}

	// *********************************************************** Modify Tabs
	// Data ******************************************

	public Map<String, String> modifyMultiUnitInfo(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("************************** Modifying Center Info ******************* ");

		String testCaseId = "TC_InFoMgr_MultiUnit_CenterInfo_Modify";

		Map<String, String> dsCenterInfo = fc.utobj().readTestData("infomgr", testCaseId);

		// fc.utobj().clickLink(driver, "Modify");
		InfoMgrMultiUnitInfoPage multiUnitInfo = new InfoMgrMultiUnitEntityPage(driver).getMultiUnitInfoPage();

		if (fc.utobj().validate(testCaseId) == true) {
			try {

				fc.utobj().selectDropDownByVisibleText(driver, multiUnitInfo.drpOwnerTitle,
						dsCenterInfo.get("salutation"));
				fc.utobj().sendKeys(driver, multiUnitInfo.txtPhone, dsCenterInfo.get("contactPhone"));
				fc.utobj().sendKeys(driver, multiUnitInfo.txtExtension, dsCenterInfo.get("contactPhoneExtension"));
				fc.utobj().sendKeys(driver, multiUnitInfo.txtEmail, dsCenterInfo.get("contactEmail"));
				fc.utobj().sendKeys(driver, multiUnitInfo.txtFax, dsCenterInfo.get("contactFax"));
				fc.utobj().sendKeys(driver, multiUnitInfo.txtMobile, dsCenterInfo.get("contactMobile"));
				fc.utobj().selectDropDownByVisibleText(driver, multiUnitInfo.drpHomeCountry,
						dsCenterInfo.get("country"));
				fc.utobj().sendKeys(driver, multiUnitInfo.txtHomeCity, dsCenterInfo.get("city"));
				fc.utobj().sleep();
				fc.utobj().selectDropDownByVisibleText(driver, multiUnitInfo.drpHomeState, dsCenterInfo.get("state"));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, multiUnitInfo.submit);

				if (fc.utobj().assertLinkText(driver, "Modify")) {
					Reporter.log("Multi Unit Info have been updated successfully !!!");
				} else {
					fc.utobj().throwsException("Multi Unit Info Data couldn't be updated !!! ");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in modifying Multi Unit Info , please refer screenshot!");
		}

		return dsCenterInfo;
	}

	public WebDriver modifyAgreement(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("********************** modify Agreement ****************** \n");

		String testCaseId = "TC_InFoMgr_MultiUnit_Agreement_Modify";

		Map<String, String> dsAgreement = fc.utobj().readTestData("infomgr", testCaseId);

		InfoMgrAgreementPage infoMgrAgreementPage = new InfoMgrMultiUnitEntityPage(driver).getAgreementPage();

		fc.utobj().clickLink(driver, "Modify");

		String comments = fc.utobj().generateTestData(dsAgreement.get("comments"));

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtApprovedDate,
						fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtDateExecuted,
						fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtEffectiveDate,
						fc.utobj().getFutureDateUSFormat(-10));

				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtStateAddendum, dsAgreement.get("stateAddendum"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtOtherAddendum, dsAgreement.get("otherAddendum"));
				fc.utobj().clickRadioButton(driver, infoMgrAgreementPage.radioRightsOfFirstRefusal,
						dsAgreement.get("rightsofFirstRefusal"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtProtectedTerritory,
						dsAgreement.get("protectedTerritory"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtSalesperson, dsAgreement.get("salesPerson"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtPreviousLicenseNumber,
						dsAgreement.get("previousLicenseNumber"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRelatedCenter, dsAgreement.get("relatedCenter"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRequiredOpeningDate,
						fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtStoreSoldDate,
						fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtComments, comments);
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtInitialTerm, dsAgreement.get("initialTerm"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRenewalTermFirst, dsAgreement.get("termFirst"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRenewalDueDateFirst,
						fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRenewalFeeFirst, dsAgreement.get("feeFirst"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRenewalFeePaidFirstDate,
						fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRenewalTermSecond, dsAgreement.get("termSecond"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRenewalDueDateSecond,
						fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRenewalFeeSecond, dsAgreement.get("feeSecond"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRenewalFeePaidSecondDate,
						fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRenewalTermThird, dsAgreement.get("termThird"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRenewalDueDateThird,
						fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRenewalFeeThird, dsAgreement.get("feeThird"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtRenewalFeePaidThirdDate,
						fc.utobj().getFutureDateUSFormat(-10));
				// Contact Details
				fc.utobj().selectDropDownByVisibleText(driver, infoMgrAgreementPage.drpContactTitle,
						dsAgreement.get("title"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtFirstName, dsAgreement.get("firstName"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtLastName, dsAgreement.get("lastName"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtStreetAddress, dsAgreement.get("streetAddress"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtCity, dsAgreement.get("city"));
				fc.utobj().selectDropDownByVisibleText(driver, infoMgrAgreementPage.drpCountry,
						dsAgreement.get("country"));
				fc.utobj().sleep();
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtZipcode, dsAgreement.get("zip"));
				fc.utobj().selectDropDownByVisibleText(driver, infoMgrAgreementPage.drpState, dsAgreement.get("state"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtPhoneNumbers, dsAgreement.get("phone"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtExtn, dsAgreement.get("phoneExtension"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtFaxNumbers, dsAgreement.get("fax"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtMobileNumbers, dsAgreement.get("mobile"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtMailIds, dsAgreement.get("email"));

				// Financial Notes
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtDescription1, dsAgreement.get("description1"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtAmount1, dsAgreement.get("amount1"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtAmountTerm1, dsAgreement.get("amountTerm1"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtInterest1, dsAgreement.get("interest1"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtComment1, dsAgreement.get("comments1"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtDescription2, dsAgreement.get("description2"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtAmount2, dsAgreement.get("amount2"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtAmountTerm2, dsAgreement.get("amountTerm2"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtInterest2, dsAgreement.get("interest2"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtComment2, dsAgreement.get("comments2"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtDescription3, dsAgreement.get("description3"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtAmount3, dsAgreement.get("amount3"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtAmountTerm3, dsAgreement.get("amountTerm3"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtInterest3, dsAgreement.get("interest3"));
				fc.utobj().sendKeys(driver, infoMgrAgreementPage.txtComment3, dsAgreement.get("comments3"));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, infoMgrAgreementPage.btnsubmit);

				if (fc.utobj().assertPageSource(driver, comments)) {
					Reporter.log("Agreement has been modified successfully");
				} else {
					fc.utobj().throwsException("Agreement couldn't be modified . Test couldn't continue !!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in modifying agreement , please refer screenshot!");
		}
		return driver;
	}

	public String modifyATask(WebDriver driver, Map<String, String> config, boolean isFromContactHistoryPage)
			throws Exception {
		Reporter.log("********************** Modifying a Task ****************** \n");

		String testCaseId = "TC_InFoMgr_MultiUnit_LogATask_Modify";

		fc.commonMethods().switch_cboxIframe_frameId(driver);

		Map<String, String> dsLogATask = fc.utobj().readTestData("infomgr", testCaseId);

		InfoMgrLogATaskPage logATaskPage = new InfoMgrMultiUnitEntityPage(driver).getLogATaskPage();
		String subject = fc.utobj().generateTestData(dsLogATask.get("subject"));
		if (fc.utobj().validate(testCaseId) == true) {
			try {

				fc.utobj().selectDropDownByPartialText(driver, logATaskPage.drpStatus, dsLogATask.get("status"));
				fc.utobj().selectDropDownByVisibleText(driver, logATaskPage.drpTaskType, dsLogATask.get("taskType"));
				fc.utobj().sendKeys(driver, logATaskPage.txtSubject, subject);
				fc.utobj().sendKeys(driver, logATaskPage.txtStartDate, fc.utobj().getFutureDateUSFormat(5));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, logATaskPage.btnAdd);
				fc.utobj().switchFrameToDefault(driver);

				if (isFromContactHistoryPage) {
					boolean isSubjectOnPage = fc.utobj().assertPageSource(driver, subject);
					if (isSubjectOnPage) {
						Reporter.log("Task has been modified successfully");
					} else {
						fc.utobj().throwsException("Task modification failed. Test case fails ");
					}
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in modifying a task , please refer screenshot!");
		}
		return subject;
	}

	public String modifyACall(WebDriver driver, Map<String, String> config, boolean isFromContactHistoryPage)
			throws Exception {
		Reporter.log("********************** Modifying a Call ****************** \n");

		String testCaseId = "TC_InFoMgr_MultiUnit_LogACall_Modify";

		Map<String, String> dsLogACall = fc.utobj().readTestData("infomgr", testCaseId);
		InfoMgrLogACallPage logACallPage = new InfoMgrMultiUnitEntityPage(driver).getLogACallPage();
		fc.commonMethods().switch_cboxIframe_frameId(driver);
		String subject = fc.utobj().generateTestData(dsLogACall.get("subject"));

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().sendKeys(driver, logACallPage.txtSubject, subject);
				fc.utobj().sendKeys(driver, logACallPage.txtDate, fc.utobj().getFutureDateUSFormat(5));
				fc.utobj().selectDropDownByPartialText(driver, logACallPage.drpStatus, dsLogACall.get("status"));
				fc.utobj().selectDropDownByPartialText(driver, logACallPage.txtTimeHour, dsLogACall.get("timeHour"));
				fc.utobj().selectDropDownByPartialText(driver, logACallPage.txtTimeMinute, dsLogACall.get("timeMin"));
				fc.utobj().selectDropDownByPartialText(driver, logACallPage.txtAMPM, dsLogACall.get("timeAMPM"));

				fc.utobj().selectDropDownByPartialText(driver, logACallPage.drpCallType, dsLogACall.get("typeofCall"));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, logACallPage.btnSubmit);
				fc.utobj().sleep();
				fc.utobj().clickElement(driver, logACallPage.btnNo);
				fc.utobj().sleep();
				fc.utobj().switchFrameToDefault(driver);
				if (isFromContactHistoryPage) {
					if (fc.utobj().assertLinkText(driver, subject)) {
						Reporter.log("Log a call set up has been modified successfully");
					} else {
						fc.utobj().throwsException("Log a Call modification has been failed.");
					}
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in modifying a call , please refer screenshot!");
		}
		return subject;
	}

	public String modifyRemark(WebDriver driver, Map<String, String> config, Object remarksObj) throws Exception {
		Reporter.log("******************* Adding Remarks ********************** \n");

		String testCaseId = "TC_InFoMgr_MultiUnit_modify_Remarks";

		fc.commonMethods().switch_cboxIframe_frameId(driver);
		String remark = fc.utobj().generateTestData("TestRemarksmodified");

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				if (remarksObj instanceof InfoMgrContactHistoryPage) {
					InfoMgrContactHistoryPage contactHistory = (InfoMgrContactHistoryPage) remarksObj;
					fc.utobj().sendKeys(driver, contactHistory.txtRemarks, remark);
					fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																					// for
																					// Custom
																					// Manadatory
																					// Fields
																					// Of
																					// Client
					fc.utobj().clickElement(driver, contactHistory.btnSubmit);
					fc.utobj().clickElement(driver, contactHistory.btnClose);
				} else if (remarksObj instanceof InfoMgrOwnersPage) {
					InfoMgrOwnersPage ownersPage = (InfoMgrOwnersPage) remarksObj;
					fc.utobj().sendKeys(driver, ownersPage.txtRemarks, remark);
					fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																					// for
																					// Custom
																					// Manadatory
																					// Fields
																					// Of
																					// Client
					fc.utobj().clickElement(driver, ownersPage.btnSubmit);
					fc.utobj().clickElement(driver, ownersPage.btnClose);
				}
				Thread.sleep(2000);
				boolean isRemarkOnPage = fc.utobj().assertPageSource(driver, remark);
				if (isRemarkOnPage) {
					Reporter.log("Remarks have been modified successfully !!! ");
				} else {
					fc.utobj().throwsException("Some problem occured while modifying remarks !!!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in modifying remarks , please refer screenshot!");
		}
		return remark;
	}

	public void modifyContractSigning(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("**************************** Modify Contract Signing Info  ***********");

		String testCaseId = "TC_InfoMgr_MultiUnit_ContractSigning_Modify";

		fc.utobj().clickLink(driver, "Modify");
		InfoMgrContractSigningPage contractSigning = new InfoMgrMultiUnitEntityPage(driver).getContractSigningPage();
		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().sendKeys(driver, contractSigning.txtContractReceivedSignedDate,
						fc.utobj().getFutureDateUSFormat(10));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, contractSigning.btnSubmit);
				if (fc.utobj().assertPageSource(driver, "Signing Commissions")
						&& fc.utobj().assertPageSource(driver, "Ancillary Documents")) {
					Reporter.log("Contract Signing data have been modified successfully !!! ");
				} else {
					fc.utobj().throwsException("Contract Signing data couldn't be modified. Test failes !!! ");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj()
					.throwsSkipException("Error in modifying Contract Signing Information  , please refer screenshot!");
		}

	}

	public void modifyCustomerComplaints(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("****************************** Modifying Customer Complaints ********************** \n");

		String testCaseId = "TC_InfoMgr_MultiUnit_Modify_Customer_Complaints";

		Map<String, String> dsCustomerComplaints = fc.utobj().readTestData("infomgr", testCaseId);

		InfoMgrCustomerComplaintsPage customerComplaintsPage = new InfoMgrMultiUnitEntityPage(driver)
				.getCustomerComplaintsPage();
		fc.utobj().clickLink(driver, "Modify");
		String complaintID = fc.utobj().generateTestData(dsCustomerComplaints.get("complaintID"));

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().sendKeys(driver, customerComplaintsPage.txtComplaintID, complaintID);
				fc.utobj().sendKeys(driver, customerComplaintsPage.txtComplaintDate,
						fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, customerComplaintsPage.txtComplaint, dsCustomerComplaints.get("complaint"));
				fc.utobj().selectDropDownByVisibleText(driver, customerComplaintsPage.drpComplaintType,
						dsCustomerComplaints.get("type"));
				fc.utobj().sendKeys(driver, customerComplaintsPage.txtIncidentDate,
						fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, customerComplaintsPage.txtReceivedVia,
						dsCustomerComplaints.get("receivedVia"));
				fc.utobj().sendKeys(driver, customerComplaintsPage.txtSummary,
						dsCustomerComplaints.get("summaryofIncident"));
				fc.utobj().sendKeys(driver, customerComplaintsPage.txtActionTaken,
						dsCustomerComplaints.get("actionTaken"));
				fc.utobj().sendKeys(driver, customerComplaintsPage.txtDocumentTitle,
						dsCustomerComplaints.get("documentTitle"));
				fc.utobj().sendKeys(driver, customerComplaintsPage.txtDocumentAttachment,
						dsCustomerComplaints.get("document"));
				fc.utobj().sendKeys(driver, customerComplaintsPage.txtComplaintBy,
						dsCustomerComplaints.get("complainantName"));
				fc.utobj().sendKeys(driver, customerComplaintsPage.txtStreetAddress,
						dsCustomerComplaints.get("streetAddress"));
				fc.utobj().sendKeys(driver, customerComplaintsPage.txtCity, dsCustomerComplaints.get("city"));
				fc.utobj().selectDropDownByPartialText(driver, customerComplaintsPage.drpCountry,
						dsCustomerComplaints.get("country"));
				fc.utobj().sendKeys(driver, customerComplaintsPage.txtZipCode, dsCustomerComplaints.get("zip"));
				fc.utobj().selectDropDownByVisibleText(driver, customerComplaintsPage.drpState,
						dsCustomerComplaints.get("state"));
				fc.utobj().sendKeys(driver, customerComplaintsPage.txtEmailIds, dsCustomerComplaints.get("email"));
				fc.utobj().sendKeys(driver, customerComplaintsPage.txtPhoneNumbers, dsCustomerComplaints.get("phone"));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, customerComplaintsPage.btnSubmit);

				boolean isComplaintIDPresent = fc.utobj().assertPageSource(driver, complaintID);

				if (isComplaintIDPresent) {
					Reporter.log("Complaint modified successfully !!!");
				} else {
					fc.utobj().throwsException("Error in modifying customer complaints !!! ");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			}

			catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in modifying Customer Complaints , please refer screenshot!");
		}
	}

	public String modifyDocument(WebDriver driver, Map<String, String> config) throws Exception {
		String testCaseId = "TC_InfoMgr_MultiUnit_Document_Modify";
		InfoMgrDocumentsPage documentsPage = new InfoMgrMultiUnitEntityPage(driver).getDocumentsPage();

		Map<String, String> dsDocuments = fc.utobj().readTestData("infomgr", testCaseId);

		String documentTitle = fc.utobj().generateTestData(dsDocuments.get("documentTitle"));

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().sendKeys(driver, documentsPage.txtDocumentTitle, documentTitle);
				fc.utobj().sendKeys(driver, documentsPage.txtDocumentAttachment, dsDocuments.get("documentAttachment"));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, documentsPage.btnAdd);

				boolean isDocAdded = fc.utobj().assertPageSource(driver, documentTitle);
				if (isDocAdded) {
					Reporter.log("Document added successfully");
				} else {
					fc.utobj().throwsException("Document modification failed. !!! ");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in modifying documents , please refer screenshot!");
		}
		return documentTitle;
	}

	public String modifyEmployee(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("*********************** Modify Employess ****************************** ");

		String testCaseId = "TC_InFoMgr_MultiUnit_Modify_Employee";

		Map<String, String> dsEmployee = fc.utobj().readTestData("infomgr", testCaseId);
		InfoMgrEmployeesPage employeesPage = new InfoMgrMultiUnitEntityPage(driver).getEmployeesPage();

		String employeeName = fc.utobj().generateTestData(dsEmployee.get("firstName"));
		try {
			fc.utobj().clickLink(driver, "Modify");
			}
			catch(Exception e)
			{				
				fc.utobj().printTestStep("Employee Tabular View Configured.");
				fc.utobj().actionImgOption(driver, dsEmployee.get("firstName"), "Modify");
			}

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().selectDropDownByVisibleText(driver, employeesPage.drpSalutation,
						dsEmployee.get("salutation"));
				fc.utobj().sendKeys(driver, employeesPage.txtFirstName, employeeName);
				fc.utobj().sendKeys(driver, employeesPage.txtLastName, dsEmployee.get("lastName"));
				fc.utobj().sendKeys(driver, employeesPage.txtAddress, dsEmployee.get("address"));
				fc.utobj().sendKeys(driver, employeesPage.txtCity, dsEmployee.get("city"));
				fc.utobj().selectDropDownByVisibleText(driver, employeesPage.drpCountry, dsEmployee.get("country"));
				fc.utobj().sendKeys(driver, employeesPage.txtZipcode, dsEmployee.get("zip"));
				fc.utobj().selectDropDownByVisibleText(driver, employeesPage.drpState, dsEmployee.get("state"));
				fc.utobj().sendKeys(driver, employeesPage.txtPhone1, dsEmployee.get("phone1"));
				fc.utobj().sendKeys(driver, employeesPage.txtPhone2, dsEmployee.get("phone2"));
				fc.utobj().sendKeys(driver, employeesPage.txtFax, dsEmployee.get("fax"));
				fc.utobj().sendKeys(driver, employeesPage.txtMobile, dsEmployee.get("mobile"));
				fc.utobj().sendKeys(driver, employeesPage.txtEmail, dsEmployee.get("email"));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, employeesPage.btnAdd);

				// Verify that the employee have been modified

				if (fc.utobj().assertPageSource(driver, employeeName)) {
					Reporter.log("Employees have been modified successfully");
				} else {
					fc.utobj().throwsException("Employee couldn't be modified . Test couldn't continue !!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in modifying Employee , please refer screenshot!");
		}

		return employeeName;

	}

	public String modifyEntityDetails(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("*********************** Modify Entity Details ****************************** ");

		String testCaseId = "TC_InFoMgr_MultiUnit_Modify_Entity_Details";

		Map<String, String> dsEntityDetails = fc.utobj().readTestData("infomgr", testCaseId);

		InfoMgrEntityDetailsPage entityDetailsPage = new InfoMgrMultiUnitEntityPage(driver).getEntityDetailsPage();
		fc.utobj().clickLink(driver, "Modify");
		String entityName = fc.utobj().generateTestData(dsEntityDetails.get("entityName"));

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().sendKeys(driver, entityDetailsPage.txtEntityName, entityName);
				fc.utobj().sendKeys(driver, entityDetailsPage.txtAddress, dsEntityDetails.get("address"));
				fc.utobj().sendKeys(driver, entityDetailsPage.txtCity, dsEntityDetails.get("city"));
				fc.utobj().selectDropDownByVisibleText(driver, entityDetailsPage.drpCountry,
						dsEntityDetails.get("country"));
				fc.utobj().sendKeys(driver, entityDetailsPage.txtZipCode, dsEntityDetails.get("zip"));
				fc.utobj().selectDropDownByVisibleText(driver, entityDetailsPage.drpState,
						dsEntityDetails.get("state"));
				fc.utobj().sendKeys(driver, entityDetailsPage.txtPhoneNumber, dsEntityDetails.get("phone"));
				fc.utobj().sendKeys(driver, entityDetailsPage.txtExtension, dsEntityDetails.get("phoneExt"));
				fc.utobj().sendKeys(driver, entityDetailsPage.txtFaxNumber, dsEntityDetails.get("fax"));
				fc.utobj().sendKeys(driver, entityDetailsPage.txtMobileNumber, dsEntityDetails.get("mobile"));
				fc.utobj().sendKeys(driver, entityDetailsPage.txtEmailIds, dsEntityDetails.get("email"));
				fc.utobj().selectDropDownByVisibleText(driver, entityDetailsPage.drpCountryFormation,
						dsEntityDetails.get("countryofFormation"));
				fc.utobj().sleep();
				fc.utobj().sendKeys(driver, entityDetailsPage.txtDateFormation,
						fc.utobj().getFutureDateUSFormat(-5));
				fc.utobj().selectDropDownByVisibleText(driver, entityDetailsPage.drpStateFormation,
						dsEntityDetails.get("stateofFormation"));
				fc.utobj().selectDropDownByVisibleText(driver, entityDetailsPage.drpEntityType,
						dsEntityDetails.get("entityType"));
				fc.utobj().sendKeys(driver, entityDetailsPage.txtFormationDocumentTitle,
						dsEntityDetails.get("formationDocTitle"));
				fc.utobj().sendKeys(driver, entityDetailsPage.txtFormationDocumentAttachment,
						dsEntityDetails.get("formationDocAttachment"));
				fc.utobj().sendKeys(driver, entityDetailsPage.txtGoverningDocumentTitle,
						dsEntityDetails.get("govereningDocumentTitle"));
				fc.utobj().sendKeys(driver, entityDetailsPage.txtGoverningDocumentAttachment,
						dsEntityDetails.get("govreeningDocAttachment"));
				fc.utobj().clickElement(driver, entityDetailsPage.btnAdd);
				if (fc.utobj().assertPageSource(driver, entityName)) {
					Reporter.log("Entity details have been modified successfully");
				} else {
					fc.utobj().throwsException("Entity detail couldn't be modified . Test couldn't continue !!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in modifying entity details , please refer screenshot!");
		}

		return entityName;

	}

	public String modifyEventDetails(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("*********************** Modify Event Details ****************************** ");

		String testCaseId = "TC_InFoMgr_MultiUnit_Modify_Event_Details";
		Map<String, String> dsEventDetails = fc.utobj().readTestData("infomgr", testCaseId);

		InfoMgrEventsPage eventsPage = new InfoMgrMultiUnitEntityPage(driver).getEventsPage();
		fc.utobj().clickLink(driver, "Modify");
		String eventSummary = fc.utobj().generateTestData(dsEventDetails.get("summary"));
		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().sendKeys(driver, eventsPage.txtDate, fc.utobj().getFutureDateUSFormat(10));
				fc.utobj().sendKeys(driver, eventsPage.txtOrganizer, dsEventDetails.get("organizer"));
				fc.utobj().sendKeys(driver, eventsPage.txtType, dsEventDetails.get("type"));
				fc.utobj().sendKeys(driver, eventsPage.txtSummary, eventSummary);
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, eventsPage.btnAdd);
				if (fc.utobj().assertPageSource(driver, eventSummary)) {
					Reporter.log("Event details have been modified successfully");
				} else {
					fc.utobj().throwsException("Event detail couldn't be modified . Test couldn't continue !!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in modifying event details , please refer screenshot!");
		}

		return eventSummary;
	}

	public void modifyFinanceDetails(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("*********************** Modify Finance Details ****************************** ");

		String testCaseId = "TC_InFoMgr_MultiUnit_Modify_Finance_Details";
		Map<String, String> dsFinance = fc.utobj().readTestData("infomgr", testCaseId);

		InfoMgrFinancialsPage financePage = new InfoMgrMultiUnitEntityPage(driver).getFinancialsPage();
		fc.utobj().clickLink(driver, "Modify");

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().sendKeys(driver, financePage.drpTitle, dsFinance.get("title"));
				fc.utobj().sendKeys(driver, financePage.txtFirstName, dsFinance.get("firstName"));
				fc.utobj().sendKeys(driver, financePage.txtLastName, dsFinance.get("lastName"));
				fc.utobj().sendKeys(driver, financePage.txtStreetAddress, dsFinance.get("streetAddress"));
				fc.utobj().sendKeys(driver, financePage.txtCity, dsFinance.get("city"));
				fc.utobj().sendKeys(driver, financePage.drpCountry, dsFinance.get("country"));
				fc.utobj().sleep();
				fc.utobj().sendKeys(driver, financePage.txtZipCode, dsFinance.get("zip"));
				fc.utobj().sendKeys(driver, financePage.drpState, dsFinance.get("state"));
				fc.utobj().sendKeys(driver, financePage.txtPhoneNumbers, dsFinance.get("phone"));
				fc.utobj().sendKeys(driver, financePage.txtExtension, dsFinance.get("phoneExtension"));
				fc.utobj().sendKeys(driver, financePage.txtFaxNumbers, dsFinance.get("fax"));
				fc.utobj().sendKeys(driver, financePage.txtMobileNumbers, dsFinance.get("mobile"));
				fc.utobj().sendKeys(driver, financePage.txtEmailIds, dsFinance.get("email"));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, financePage.btnAdd);
				if (fc.utobj().assertLinkText(driver, "Modify")) {
					Reporter.log("Finance details has been modified successfully");
				} else {
					fc.utobj().throwsException("Finance details couldn't be modified . Test couldn't continue !!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in modifying finance details , please refer screenshot!");
		}

	}

	public String modifyGuarantor(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("*********************** Modify Gurantor Details ****************************** ");

		String testCaseId = "TC_InFoMgr_MultiUnit_Modify_Gurantor_Details";
		Map<String, String> dsGuarantor = fc.utobj().readTestData("infomgr", testCaseId);

		InfoMgrGuarantorsPage guarantorPage = new InfoMgrMultiUnitEntityPage(driver).getGuarantorsPage();
		fc.utobj().clickLink(driver, "Modify");
		String guarantorName = fc.utobj().generateTestData(dsGuarantor.get("firstName"));

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().selectDropDownByVisibleText(driver, guarantorPage.drpTitle, dsGuarantor.get("title"));
				fc.utobj().sendKeys(driver, guarantorPage.txtFirstName, guarantorName);
				fc.utobj().sendKeys(driver, guarantorPage.txtLastName, dsGuarantor.get("lastName"));
				fc.utobj().selectDropDownByVisibleText(driver, guarantorPage.drpResidencyState,
						dsGuarantor.get("stateOfFormation"));
				fc.utobj().sendKeys(driver, guarantorPage.txtPercentage, dsGuarantor.get("percentage"));
				fc.utobj().sendKeys(driver, guarantorPage.txtStreetAddress, dsGuarantor.get("address"));
				fc.utobj().sendKeys(driver, guarantorPage.txtCity, dsGuarantor.get("city"));
				fc.utobj().selectDropDownByVisibleText(driver, guarantorPage.drpCountry, dsGuarantor.get("country"));
				fc.utobj().sendKeys(driver, guarantorPage.txtZipCode, dsGuarantor.get("zip"));
				fc.utobj().selectDropDownByVisibleText(driver, guarantorPage.drpState, dsGuarantor.get("state"));
				fc.utobj().sendKeys(driver, guarantorPage.txtPhoneNumbers, dsGuarantor.get("phone"));
				fc.utobj().sendKeys(driver, guarantorPage.txtExtension, dsGuarantor.get("phoneExt"));
				fc.utobj().sendKeys(driver, guarantorPage.txtFaxNumbers, dsGuarantor.get("fax"));
				fc.utobj().sendKeys(driver, guarantorPage.txtMobileNumbers, dsGuarantor.get("mobile"));
				fc.utobj().sendKeys(driver, guarantorPage.txtEmailIds, dsGuarantor.get("email"));
				fc.utobj().sendKeys(driver, guarantorPage.txtComments, dsGuarantor.get("comments"));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, guarantorPage.btnAdd);
				if (fc.utobj().assertPageSource(driver, guarantorName)) {
					Reporter.log("guarantor details have been modified successfully");
				} else {
					fc.utobj().throwsException("guarantor detail couldn't be modified . Test couldn't continue !!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in modifying guarantor details , please refer screenshot!");
		}
		return guarantorName;
	}

	public void modifyInsuranceDetails(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("*********************** Modify Insurance Details ****************************** ");

		String testCaseId = "TC_InFoMgr_MultiUnit_Modify_Insurance_Details";
		Map<String, String> dsInsurance = fc.utobj().readTestData("infomgr", testCaseId);

		InfoMgrInsurancePage insurancePage = new InfoMgrMultiUnitEntityPage(driver).getInsurancePage();
		fc.utobj().clickLink(driver, "Modify");
		String company = fc.utobj().generateTestData("CompanyName01");

		if (fc.utobj().validate(testCaseId) == true) {
			try {

				fc.utobj().sendKeys(driver, insurancePage.txtInsuranceCompanyName, company);
				// Contact Details
				fc.utobj().selectDropDownByVisibleText(driver, insurancePage.drpTitle, dsInsurance.get("title"));
				fc.utobj().sendKeys(driver, insurancePage.txtFirstName, dsInsurance.get("firstName"));
				fc.utobj().sendKeys(driver, insurancePage.txtLastName, dsInsurance.get("lastName"));
				fc.utobj().sendKeys(driver, insurancePage.txtStreetAddress, dsInsurance.get("streetAddress"));
				fc.utobj().sendKeys(driver, insurancePage.txtCity, dsInsurance.get("city"));
				fc.utobj().selectDropDownByVisibleText(driver, insurancePage.drpCountry, dsInsurance.get("country"));
				fc.utobj().sleep();
				fc.utobj().sendKeys(driver, insurancePage.txtZipCode, dsInsurance.get("zip"));
				fc.utobj().selectDropDownByVisibleText(driver, insurancePage.drpState, dsInsurance.get("state"));
				fc.utobj().sendKeys(driver, insurancePage.txtPhoneNumbers, dsInsurance.get("phone"));
				fc.utobj().sendKeys(driver, insurancePage.txtExtension, dsInsurance.get("phoneExtension"));
				fc.utobj().sendKeys(driver, insurancePage.txtFaxNumbers, dsInsurance.get("fax"));
				fc.utobj().sendKeys(driver, insurancePage.txtMobileNumbers, dsInsurance.get("mobile"));
				fc.utobj().sendKeys(driver, insurancePage.txtEmailIds, dsInsurance.get("email"));

				// Agency details
				fc.utobj().sendKeys(driver, insurancePage.txtAgencyName, dsInsurance.get("agencyName"));
				fc.utobj().sendKeys(driver, insurancePage.txtAgencyStreetAddress, dsInsurance.get("agencyAddress"));
				fc.utobj().sendKeys(driver, insurancePage.txtAgencyCity, dsInsurance.get("agencyCity"));
				fc.utobj().selectDropDownByVisibleText(driver, insurancePage.drpCountry,
						dsInsurance.get("agencyCountry"));
				fc.utobj().sleep();
				fc.utobj().sendKeys(driver, insurancePage.txtAgencyZipCode, dsInsurance.get("agencyZip"));
				fc.utobj().selectDropDownByVisibleText(driver, insurancePage.drpAgencyState,
						dsInsurance.get("agencyState"));
				fc.utobj().sendKeys(driver, insurancePage.txtAgencyPhoneNumbers, dsInsurance.get("agencyPhone"));
				fc.utobj().sendKeys(driver, insurancePage.txtAgencyExtension, dsInsurance.get("agencyPhoneExtension"));
				fc.utobj().sendKeys(driver, insurancePage.txtAgencyFaxNumbers, dsInsurance.get("agencyFax"));
				fc.utobj().sendKeys(driver, insurancePage.txtAgencyMobileNumbers, dsInsurance.get("agencyMobile"));
				fc.utobj().sendKeys(driver, insurancePage.txtAgencyFaxNumbers, dsInsurance.get("agencyFax"));
				fc.utobj().sendKeys(driver, insurancePage.txtAgencyMobileNumbers, dsInsurance.get("agencyMobile"));
				fc.utobj().sendKeys(driver, insurancePage.txtAgencyEmailIds, dsInsurance.get("agencyEmail"));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, insurancePage.btnAdd);
				if (fc.utobj().assertPageSource(driver, company)) {
					Reporter.log("Insurance details has been modified successfully");
				} else {
					fc.utobj().throwsException("Insurance details couldn't be modified . Test couldn't continue !!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in modifying insurance details , please refer screenshot!");
		}
	}

	public String modifyLegalViolation(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("*********************** Modify Legal Violation Details ****************************** ");

		String testCaseId = "TC_InFoMgr_MultiUnit_Modify_Legal_Violation_Details";

		Map<String, String> dsLegalViolation = fc.utobj().readTestData("infomgr", testCaseId);

		InfoMgrLegalViolationPage legalViolationPage = new InfoMgrMultiUnitEntityPage(driver).getLegalViolationPage();
		fc.utobj().clickLink(driver, "Modify");
		String legalViolationNumber = fc.utobj().generateRandomNumber();
		String subject = fc.utobj().generateTestData(dsLegalViolation.get("summary"));

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().sendKeys(driver, legalViolationPage.txtNumber, legalViolationNumber);
				fc.utobj().sendKeys(driver, legalViolationPage.txtDate, fc.utobj().getFutureDateUSFormat(10));
				fc.utobj().selectDropDownByVisibleText(driver, legalViolationPage.drpType,
						dsLegalViolation.get("type"));
				fc.utobj().selectDropDownByVisibleText(driver, legalViolationPage.drpStatus,
						dsLegalViolation.get("status"));
				fc.utobj().sendKeys(driver, legalViolationPage.txtCureDate,
						fc.utobj().getFutureDateUSFormat(10));
				fc.utobj().sendKeys(driver, legalViolationPage.txtDateCured,
						fc.utobj().getFutureDateUSFormat(15));
				fc.utobj().sendKeys(driver, legalViolationPage.txtSummary, subject);
				fc.utobj().sendKeys(driver, legalViolationPage.txtActionTaken, dsLegalViolation.get("actionTaken"));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, legalViolationPage.btnAdd);
				if (fc.utobj().assertPageSource(driver, subject)) {
					Reporter.log("Legal Violation details have been modified successfully");
				} else {
					fc.utobj().throwsException(
							"Legal violation detail couldn't be modified . Test couldn't continue !!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in modifying legal violation details , please refer screenshot!");
		}

		return subject;

	}

	public String modifyLenders(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("*********************** Modify Lenders Details ****************************** ");

		String testCaseId = "TC_InFoMgr_MultiUnit_Modify_Lenders_Details";
		Map<String, String> dsLenders = fc.utobj().readTestData("infomgr", testCaseId);

		InfoMgrLendersPage lendersPage = new InfoMgrMultiUnitEntityPage(driver).getLendersPage();
		fc.utobj().clickLink(driver, "Modify");
		String lendersFirstName = fc.utobj().generateTestData(dsLenders.get("firstName"));

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().selectDropDownByVisibleText(driver, lendersPage.drpTitle, dsLenders.get("title"));
				fc.utobj().sendKeys(driver, lendersPage.txtFirstName, lendersFirstName);
				fc.utobj().sendKeys(driver, lendersPage.txtLastName, dsLenders.get("lastName"));
				fc.utobj().sendKeys(driver, lendersPage.txtStreetaddress, dsLenders.get("streetAddress"));
				fc.utobj().sendKeys(driver, lendersPage.txtCity, dsLenders.get("city"));
				fc.utobj().selectDropDownByVisibleText(driver, lendersPage.drpCountry, dsLenders.get("country"));
				fc.utobj().sendKeys(driver, lendersPage.txtZipcCode, dsLenders.get("zip"));
				fc.utobj().selectDropDownByVisibleText(driver, lendersPage.drpstate, dsLenders.get("state"));
				fc.utobj().sendKeys(driver, lendersPage.txtCollateralAssignmentExpirationDate,
						fc.utobj().getFutureDateUSFormat(10));
				fc.utobj().sendKeys(driver, lendersPage.txtComfortLetterDate,
						fc.utobj().getFutureDateUSFormat(10));
				fc.utobj().clickRadioButton(driver, lendersPage.rdoComfortAgreement, dsLenders.get("comfortAgreement"));
				fc.utobj().sendKeys(driver, lendersPage.txtDateComfortAgreementInfo,
						fc.utobj().getFutureDateUSFormat(10));
				fc.utobj().sendKeys(driver, lendersPage.txtMultiUnitDocumentTitle,
						dsLenders.get("formationDocumentTitle"));
				fc.utobj().sendKeys(driver, lendersPage.txtMultiUnitDocumentAttachment,
						dsLenders.get("documentAttachment"));
				fc.utobj().sendKeys(driver, lendersPage.txtLenderContactTitleOne, dsLenders.get("contact1Name"));
				fc.utobj().sendKeys(driver, lendersPage.txtLenderContactTitleTwo, dsLenders.get("contact2Name"));
				fc.utobj().sendKeys(driver, lendersPage.txtContact1Phone, dsLenders.get("contact1Phone"));
				fc.utobj().sendKeys(driver, lendersPage.txtContact2Phone, dsLenders.get("contact2Phone"));
				fc.utobj().sendKeys(driver, lendersPage.txtContact1PhoneExtn, dsLenders.get("contact1PhoneExtension"));
				fc.utobj().sendKeys(driver, lendersPage.txtContact2PhoneExtn, dsLenders.get("contact2PhoneExtension"));
				fc.utobj().sendKeys(driver, lendersPage.txtContact1Fax, dsLenders.get("contact1Fax"));
				fc.utobj().sendKeys(driver, lendersPage.txtContact2Fax, dsLenders.get("contact2Fax"));
				fc.utobj().sendKeys(driver, lendersPage.txtContact1Email, dsLenders.get("contact1Email"));
				fc.utobj().sendKeys(driver, lendersPage.txtContact2Email, dsLenders.get("contact2Email"));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, lendersPage.btnAdd);
				if (fc.utobj().assertPageSource(driver, lendersFirstName)) {
					Reporter.log("Lenders details have been modified successfully");
				} else {
					fc.utobj().throwsException("Lenders detail couldn't be modified . Test couldn't continue !!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in modifying lenders details , please refer screenshot!");
		}

		return lendersFirstName;

	}

	public String modifyMarketingDetails(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("*********************** Modify Marketing Details ****************************** ");

		String testCaseId = "TC_InFoMgr_MultiUnit_Modify_Marketing_Details";
		Map<String, String> dsMarketing = fc.utobj().readTestData("infomgr", testCaseId);

		InfoMgrMarketingPage marketingPage = new InfoMgrMultiUnitEntityPage(driver).getMarketingPage();
		fc.utobj().clickLink(driver, "Modify");
		String marketingContactFirstName = fc.utobj().generateTestData(dsMarketing.get("firstName"));

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().selectDropDownByVisibleText(driver, marketingPage.drpTitle, dsMarketing.get("title"));
				fc.utobj().sendKeys(driver, marketingPage.txtFirstName, marketingContactFirstName);
				fc.utobj().sendKeys(driver, marketingPage.txtLastName, dsMarketing.get("lastName"));
				fc.utobj().sendKeys(driver, marketingPage.txtStreetAddress, dsMarketing.get("streetAddress"));
				fc.utobj().sendKeys(driver, marketingPage.txtCity, dsMarketing.get("city"));
				fc.utobj().selectDropDownByVisibleText(driver, marketingPage.drpCountry, dsMarketing.get("country"));
				fc.utobj().sendKeys(driver, marketingPage.txtZipCode, dsMarketing.get("zip"));
				fc.utobj().selectDropDownByVisibleText(driver, marketingPage.drpState, dsMarketing.get("state"));
				fc.utobj().sendKeys(driver, marketingPage.txtPhoneNumbers, dsMarketing.get("phone"));
				fc.utobj().sendKeys(driver, marketingPage.txtExtension, dsMarketing.get("phoneExtension"));
				fc.utobj().sendKeys(driver, marketingPage.txtFaxNumbers, dsMarketing.get("fax"));
				fc.utobj().sendKeys(driver, marketingPage.txtMobileNumbers, dsMarketing.get("mobile"));
				fc.utobj().sendKeys(driver, marketingPage.txtEmailIds, dsMarketing.get("email"));
				fc.utobj().clickRadioButton(driver, marketingPage.rdoGrandOpeningRequired,
						dsMarketing.get("grandOpeningRequired"));
				fc.utobj().sendKeys(driver, marketingPage.txtGrandOpeningCompletedDate,
						fc.utobj().getFutureDateUSFormat(10));

				fc.utobj().clickRadioButton(driver, marketingPage.rdoCouponRedemption,
						dsMarketing.get("couponRedemption"));

				fc.utobj().sendKeys(driver, marketingPage.txtCampaignName, dsMarketing.get("campaignName"));

				fc.utobj().clickRadioButton(driver, marketingPage.rdoCampaignParticipation,
						dsMarketing.get("campaignParticipation"));

				fc.utobj().sendKeys(driver, marketingPage.txtProgramName, dsMarketing.get("programName"));

				fc.utobj().clickRadioButton(driver, marketingPage.rdoProgramParticipation,
						dsMarketing.get("programParticipation"));

				fc.utobj().sendKeys(driver, marketingPage.txtDMA, dsMarketing.get("DMA"));
				fc.utobj().sendKeys(driver, marketingPage.txtComments, dsMarketing.get("comments"));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, marketingPage.btnSubmit);

				if (fc.utobj().assertPageSource(driver, marketingContactFirstName)) {
					Reporter.log("Marketing details have been modified successfully");
				} else {
					fc.utobj().throwsException("Marketing detail couldn't be modified . Test couldn't continue !!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in modifying marketing details , please refer screenshot!");
		}

		return marketingContactFirstName;
	}

	public String modifyMystryReview(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("*********************** Modify Mystry Review ****************************** ");

		String testCaseId = "TC_InFoMgr_MultiUnit_Modify_Mystry_Review_Details";
		Map<String, String> dsMystryReview = fc.utobj().readTestData("infomgr", testCaseId);

		InfoMgrMystryReviewPage mystryReviewPage = new InfoMgrMultiUnitEntityPage(driver).getMystryReviewPage();
		fc.utobj().clickLink(driver, "Modify");
		String mystryReviewComments = fc.utobj().generateTestData(dsMystryReview.get("comments"));

		if (fc.utobj().validate(testCaseId) == true) {
			try {

				fc.utobj().sendKeys(driver, mystryReviewPage.txtInspectionDate,
						fc.utobj().getFutureDateUSFormat(-5));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtReportSentToFranchiseeDate,
						fc.utobj().getFutureDateUSFormat(-2));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtGeneralComments, mystryReviewComments);

				// Performance section
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValuePerformanceQ1,
						dsMystryReview.get("perfQuestion1"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValuePerformanceQ2,
						dsMystryReview.get("perfQuestion2"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValuePerformanceQ3,
						dsMystryReview.get("perfQuestion3"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValuePerformanceQ4,
						dsMystryReview.get("perfQuestion4"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValuePerformanceQ5,
						dsMystryReview.get("perfQuestion5"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValuePerformanceQ6,
						dsMystryReview.get("perfQuestion6"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValuePerformanceQ7,
						dsMystryReview.get("perfQuestion7"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValuePerformanceQ8,
						dsMystryReview.get("perfQuestion8"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtCommentsForPerformance,
						dsMystryReview.get("perfComments"));

				// Service Section
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValueServiceQ1,
						dsMystryReview.get("serviceQuestion1"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValueServiceQ2,
						dsMystryReview.get("serviceQuestion2"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValueServiceQ3,
						dsMystryReview.get("serviceQuestion3"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValueServiceQ4,
						dsMystryReview.get("serviceQuestion4"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValueServiceQ5,
						dsMystryReview.get("serviceQuestion5"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValueServiceQ6,
						dsMystryReview.get("serviceQuestion6"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValueServiceQ7,
						dsMystryReview.get("serviceQuestion7"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValueServiceQ8,
						dsMystryReview.get("serviceQuestion8"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValueServiceQ9,
						dsMystryReview.get("serviceQuestion9"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValueServiceQ10,
						dsMystryReview.get("serviceQuestion10"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValueServiceQ11,
						dsMystryReview.get("serviceQuestion11"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValueServiceQ12,
						dsMystryReview.get("serviceQuestion12"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtCommentsForService,
						dsMystryReview.get("serviceComments"));

				// Appearance section
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValueAppearanceQ1,
						dsMystryReview.get("appearanceQuestion1"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValueAppearanceQ2,
						dsMystryReview.get("appearanceQuestion2"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValueAppearanceQ3,
						dsMystryReview.get("appearanceQuestion3"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValueAppearanceQ4,
						dsMystryReview.get("appearanceQuestion4"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtPointValueAppearanceQ5,
						dsMystryReview.get("appearanceQuestion5"));
				fc.utobj().sendKeys(driver, mystryReviewPage.txtCommentsForAppearance,
						dsMystryReview.get("appearanceComments"));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, mystryReviewPage.btnSubmit);

				if (fc.utobj().assertPageSource(driver, mystryReviewComments)) {
					Reporter.log("Mystry Review details have been modified successfully");
				} else {
					fc.utobj()
							.throwsException("Mystry review detail couldn't be modified . Test couldn't continue !!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in modifying mystry review details , please refer screenshot!");
		}

		return mystryReviewComments;

	}

	public String modifyPicture(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("*********************** Modify Picture ****************************** ");

		String testCaseId = "TC_InFoMgr_MultiUnit_Modify_Picture";
		Map<String, String> dsPicture = fc.utobj().readTestData("infomgr", testCaseId);

		InfoMgrPicturesPage picturePage = new InfoMgrMultiUnitEntityPage(driver).getPicturesPage();
		String pictureTitle = fc.utobj().generateTestData(dsPicture.get("title"));

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().sendKeys(driver, picturePage.txtpictureTitle, pictureTitle);
				fc.utobj().sendKeys(driver, picturePage.txtpictureFilename, dsPicture.get("attachment"));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, picturePage.btnSubmit);
				if (fc.utobj().assertPageSource(driver, pictureTitle)) {
					Reporter.log("Picture has been modified successfully");
				} else {
					fc.utobj().throwsException("Picture couldn't be modified . Test couldn't continue !!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in adding picture , please refer screenshot!");
		}
		return pictureTitle;
	}

	public String modifyOwnerUser(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("*********************** Modify User ****************************** ");

		String testCaseId = "TC_InFoMgr_MultiUnit_Modify_User";
		Map<String, String> dsUser = fc.utobj().readTestData("infomgr", testCaseId);

		InfoMgrUserPage userPage = new InfoMgrMultiUnitEntityPage(driver).getUsersPage().getUserPage();
		;
		String loginID = fc.utobj().generateTestData(dsUser.get("loginID"));
		String phone = fc.utobj().generateRandomNumber();

		if (fc.utobj().validate(testCaseId) == true) {
			try {

				fc.utobj().sendKeys(driver, userPage.txtUserName, loginID);
				fc.utobj().selectDropDownByVisibleText(driver, userPage.drpType, dsUser.get("type"));
				fc.utobj().selectValFromMultiSelect(driver, userPage.drpRoles, dsUser.get("role"));
				fc.utobj().check(userPage.chkIsDaylight, dsUser.get("allowDST"));

				fc.utobj().sendKeys(driver, userPage.txtJobTitle, dsUser.get("jobTitle"));
				// fc.utobj().selectDropDownByVisibleText(driver,
				// userPage.drpUserLanguage, dsUser.get("language") ); // No use
				// to change language
				fc.utobj().sendKeys(driver, userPage.txtAddress, dsUser.get("streetAddress"));
				fc.utobj().sendKeys(driver, userPage.txtCity, dsUser.get("city"));
				fc.utobj().selectDropDownByVisibleText(driver, userPage.drpCountry, dsUser.get("country"));
				fc.utobj().sendKeys(driver, userPage.txtZipcode, dsUser.get("zip"));
				fc.utobj().selectDropDownByVisibleText(driver, userPage.drpState, dsUser.get("state"));
				fc.utobj().sendKeys(driver, userPage.txtPhone1, phone);
				fc.utobj().sendKeys(driver, userPage.txtPhoneExt1, dsUser.get("phone1Ext"));
				fc.utobj().sendKeys(driver, userPage.txtPhone2, dsUser.get("phone2"));
				fc.utobj().sendKeys(driver, userPage.txtPhoneExt2, dsUser.get("phone2Ext"));
				fc.utobj().sendKeys(driver, userPage.txtFax, dsUser.get("fax"));
				fc.utobj().sendKeys(driver, userPage.txtMobile, dsUser.get("mobile"));
				fc.utobj().sendKeys(driver, userPage.txtEmail, dsUser.get("email"));
				fc.utobj().sendKeys(driver, userPage.txtIpAddress, dsUser.get("ipAddress"));
				fc.utobj().check(userPage.chkIsBillable, dsUser.get("isBillable"));
				fc.utobj().selectDropDownByPartialText(driver, userPage.drpBirthMonth, dsUser.get("birthMonth"));
				fc.utobj().selectDropDownByPartialText(driver, userPage.drpBirthDate, dsUser.get("birthDate"));
				fc.utobj().clickElement(driver, userPage.btnSubmit);
				if (fc.utobj().assertPageSource(driver, phone)) {
					Reporter.log("User has been modified successfully");
				} else {
					fc.utobj().throwsException("User couldn't be modified . Test couldn't continue !!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in modifying user , please refer screenshot!");
		}
		return loginID;
	}

	public void modifyRealState(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("*********************** Modify Real Estate ****************************** ");

		String testCaseId = "TC_InFoMgr_MultiUnit_Modify_Real_Estate_Details";
		Map<String, String> dsRealState = fc.utobj().readTestData("infomgr", testCaseId);

		InfoMgrRealStatePage realStatePage = new InfoMgrMultiUnitEntityPage(driver).getRealStatePage();
		fc.utobj().clickLink(driver, "Modify");
		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().sendKeys(driver, realStatePage.drpOwnedLeased, dsRealState.get("ownedorLeased"));
				fc.utobj().sendKeys(driver, realStatePage.txtSiteAddress1, dsRealState.get("siteStreet1"));
				fc.utobj().sendKeys(driver, realStatePage.txtSiteAddress2, dsRealState.get("siteStreet2"));
				fc.utobj().sendKeys(driver, realStatePage.txtSiteCity, dsRealState.get("siteCity"));
				fc.utobj().sendKeys(driver, realStatePage.drpCountry, dsRealState.get("country"));
				fc.utobj().sleep();
				fc.utobj().sendKeys(driver, realStatePage.drpState, dsRealState.get("state/Province"));
				fc.utobj().sendKeys(driver, realStatePage.txtSquareFootage, dsRealState.get("buildingSize"));
				fc.utobj().sendKeys(driver, realStatePage.txtBuildingDimentionsX, dsRealState.get("buildingLength"));
				fc.utobj().sendKeys(driver, realStatePage.txtBuildingDimentionsY, dsRealState.get("buildingBreadth"));
				fc.utobj().sendKeys(driver, realStatePage.txtBuildingDimentionsZ, dsRealState.get("buildingHeight"));
				fc.utobj().sendKeys(driver, realStatePage.txtParkingSpaces, dsRealState.get("parkingSpaces"));
				fc.utobj().sendKeys(driver, realStatePage.txtDealType, dsRealState.get("dealType"));
				fc.utobj().sendKeys(driver, realStatePage.drpPremisesType, dsRealState.get("typeofPremises"));
				fc.utobj().sendKeys(driver, realStatePage.txtLoiSent, fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, realStatePage.txtLeaseSignedDate,
						fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, realStatePage.txtApprovalDate,
						fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, realStatePage.txtLeaseCommencementDate,
						fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, realStatePage.txtExpirationDate,
						fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, realStatePage.txtRentIncreases,
						fc.utobj().getFutureDateUSFormat(100));

				fc.utobj().sendKeys(driver, realStatePage.txtMonthlyRent, dsRealState.get("currentMonthlyRent"));
				fc.utobj().clickRadioButton(driver, realStatePage.rdoAcal, dsRealState.get("ACAL"));
				fc.utobj().sendKeys(driver, realStatePage.txtMultiUnitDocumentTitle, dsRealState.get("documentTitle"));
				fc.utobj().sendKeys(driver, realStatePage.txtMultiUnitDocumentAttachment, dsRealState.get("document"));

				// Term First
				fc.utobj().sendKeys(driver, realStatePage.txtRenewalTermFirst, dsRealState.get("termFirst(Years)"));
				fc.utobj().sendKeys(driver, realStatePage.txtRenewalDueDateFirst,
						fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, realStatePage.txtRenewalFeeFirst, dsRealState.get("Fee-First"));
				fc.utobj().sendKeys(driver, realStatePage.txtRenewalFeePaidFirst,
						fc.utobj().getFutureDateUSFormat(-10));

				// Term Second
				fc.utobj().sendKeys(driver, realStatePage.txtRenewalTermSecond, dsRealState.get("termSecond"));
				fc.utobj().sendKeys(driver, realStatePage.txtRenewalDueDateSecond,
						fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, realStatePage.txtRenewalFeeSecond, dsRealState.get("feeSecond"));
				fc.utobj().sendKeys(driver, realStatePage.txtRenewalFeePaidSecond,
						fc.utobj().getFutureDateUSFormat(-10));

				// Term Third
				fc.utobj().sendKeys(driver, realStatePage.txtRenewalTermThird, dsRealState.get("termThird"));
				fc.utobj().sendKeys(driver, realStatePage.txtRenewalDueDateThird,
						fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, realStatePage.txtRenewalFeeThird, dsRealState.get("feeThird"));
				fc.utobj().sendKeys(driver, realStatePage.txtRenewalFeePaidThird,
						fc.utobj().getFutureDateUSFormat(-10));

				fc.utobj().sendKeys(driver, realStatePage.txtRenewalOptions, dsRealState.get("optionTerm(Years)"));
				fc.utobj().clickRadioButton(driver, realStatePage.rdoPurchaseOption, dsRealState.get("purchaseOption"));
				fc.utobj().sendKeys(driver, realStatePage.txtProjectedOpeningDate,
						fc.utobj().getFutureDateUSFormat(-10));

				fc.utobj().clickRadioButton(driver, realStatePage.rdoGeneralContractorSelector,
						dsRealState.get("generalContractorSelected"));
				if (dsRealState.get("generalContractorSelected").equalsIgnoreCase("yes")) {
					fc.utobj().sendKeys(driver, realStatePage.txtNameGeneralContractor,
							dsRealState.get("generalContractorName"));
					fc.utobj().sendKeys(driver, realStatePage.txtAddressGeneralContractor,
							dsRealState.get("generalContractorAddress"));
				}

				fc.utobj().sendKeys(driver, realStatePage.txtPermitApplied,
						fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, realStatePage.txtPermitIssued,
						fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, realStatePage.txtCertificate,
						fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, realStatePage.txtTurnOverDate,
						fc.utobj().getFutureDateUSFormat(-10));
				fc.utobj().sendKeys(driver, realStatePage.txtGrandOpeningDate,
						fc.utobj().getFutureDateUSFormat(-10));

				// Lessor details
				fc.utobj().selectDropDownByVisibleText(driver, realStatePage.drpLessorTitle,
						dsRealState.get("lessor_title"));
				fc.utobj().sendKeys(driver, realStatePage.txtLessorFirstName, dsRealState.get("lessor_firstName"));
				fc.utobj().sendKeys(driver, realStatePage.txtLessorLastName, dsRealState.get("lessor_lastName"));
				fc.utobj().sendKeys(driver, realStatePage.txtLessorStreetAddress,
						dsRealState.get("lessor_streetAddress"));
				fc.utobj().sendKeys(driver, realStatePage.txtLessorCity, dsRealState.get("lessor_city"));
				fc.utobj().selectDropDownByVisibleText(driver, realStatePage.drpLessorCountry,
						dsRealState.get("lessor_country"));
				fc.utobj().sleep();
				fc.utobj().sendKeys(driver, realStatePage.txtLessorZipCode, dsRealState.get("lessor_zip"));
				fc.utobj().selectDropDownByVisibleText(driver, realStatePage.drpLessorState,
						dsRealState.get("lessor_state"));
				fc.utobj().sendKeys(driver, realStatePage.txtLessorPhoneNumbers, dsRealState.get("lessor_phone"));
				fc.utobj().sendKeys(driver, realStatePage.txtLessorExtension, dsRealState.get("lessor_phoneExtension"));
				fc.utobj().sendKeys(driver, realStatePage.txtLessorFaxNumbers, dsRealState.get("lessor_fax"));
				fc.utobj().sendKeys(driver, realStatePage.txtLessorMobileNumbers, dsRealState.get("lessor_mobile"));
				fc.utobj().sendKeys(driver, realStatePage.txtLessorEmailIds, dsRealState.get("lessor_email"));

				// Sub-lessor details
				fc.utobj().selectDropDownByVisibleText(driver, realStatePage.drpSubLessorTitle,
						dsRealState.get("sublessor_title"));
				fc.utobj().sendKeys(driver, realStatePage.txtSublessorFirstName,
						dsRealState.get("sublessor_firstName"));
				fc.utobj().sendKeys(driver, realStatePage.txtSublessorLastName, dsRealState.get("sublessor_lastName"));
				fc.utobj().sendKeys(driver, realStatePage.txtSubLessorStreetAddress,
						dsRealState.get("sublessor_streetAddress"));
				fc.utobj().sendKeys(driver, realStatePage.txtSubLessorCity, dsRealState.get("sublessor_city"));
				fc.utobj().selectDropDownByVisibleText(driver, realStatePage.drpSubLessorCountry,
						dsRealState.get("sublessor_country"));
				fc.utobj().sleep();
				fc.utobj().sendKeys(driver, realStatePage.txtSubLessorZipCode, dsRealState.get("sublessor_zip"));
				fc.utobj().selectDropDownByVisibleText(driver, realStatePage.drpSubLessorState,
						dsRealState.get("sublessor_state"));
				fc.utobj().sendKeys(driver, realStatePage.txtSubLessorPhoneNumbers, dsRealState.get("sublessor_phone"));
				fc.utobj().sendKeys(driver, realStatePage.txtSubLessorExtension,
						dsRealState.get("sublessor_phoneExtension"));
				fc.utobj().sendKeys(driver, realStatePage.txtSubLessorFaxNumbers, dsRealState.get("sublessor_fax"));
				fc.utobj().sendKeys(driver, realStatePage.txtSubLessorMobileNumbers,
						dsRealState.get("sublessor_mobile"));
				fc.utobj().sendKeys(driver, realStatePage.txtSubLessorEmailIds, dsRealState.get("sublessor_email"));

				// Tenant Details
				fc.utobj().selectDropDownByVisibleText(driver, realStatePage.drpTenantTitle,
						dsRealState.get("tenant_title"));
				fc.utobj().sendKeys(driver, realStatePage.txtTenantFirstName, dsRealState.get("tenant_firstName"));
				fc.utobj().sendKeys(driver, realStatePage.txtTenantLastName, dsRealState.get("tenant_lastName"));
				fc.utobj().sendKeys(driver, realStatePage.txtTenantStreetAddress,
						dsRealState.get("tenant_streetAddress"));
				fc.utobj().sendKeys(driver, realStatePage.txtTenantCity, dsRealState.get("tenant_city"));
				fc.utobj().selectDropDownByVisibleText(driver, realStatePage.drpTenantCountry,
						dsRealState.get("tenant_country"));
				fc.utobj().sleep();
				fc.utobj().sendKeys(driver, realStatePage.txtTenantZipCode, dsRealState.get("tenant_zip"));
				fc.utobj().selectDropDownByVisibleText(driver, realStatePage.drpTenantState,
						dsRealState.get("tenant_state"));
				fc.utobj().sendKeys(driver, realStatePage.txtTenantPhoneNumbers, dsRealState.get("tenant_phone"));
				fc.utobj().sendKeys(driver, realStatePage.txtTenantExtension, dsRealState.get("tenant_phoneExtension"));
				fc.utobj().sendKeys(driver, realStatePage.txtTenantFaxNumbers, dsRealState.get("tenant_fax"));
				fc.utobj().sendKeys(driver, realStatePage.txtTenantMobileNumbers, dsRealState.get("tenant_mobile"));
				fc.utobj().sendKeys(driver, realStatePage.txtTenantEmailIds, dsRealState.get("tenant_email"));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, realStatePage.btnAdd);
				if (fc.utobj().assertLinkText(driver, "Modify")) {
					Reporter.log("Real Estate details has been modifying successfully");
				} else {
					fc.utobj().throwsException("Real Estate Details couldn't be modified . Test couldn't continue !!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in modifying real estate details , please refer screenshot!");
		}
	}

	public String modifyTerritory(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("*********************** Modify Territory Details ****************************** ");

		String testCaseId = "TC_InFoMgr_MultiUnit_Modify_Territory_Details";
		Map<String, String> dsTerritory = fc.utobj().readTestData("infomgr", testCaseId);

		InfoMgrTerritoryPage territoryPage = new InfoMgrMultiUnitEntityPage(driver).getTerritoryPage();
		fc.utobj().clickLink(driver, "Modify");
		String txtNotes = fc.utobj().generateTestData(dsTerritory.get("notes"));

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().clickRadioButton(driver, territoryPage.rdoTypeTerritory, dsTerritory.get("typeofTerritory"));
				fc.utobj().sendKeys(driver, territoryPage.txtSalesRequirement,
						dsTerritory.get("minimumSalesRequirement"));
				fc.utobj().sendKeys(driver, territoryPage.txtRestrictions, dsTerritory.get("restrictionsonFranchisee"));
				fc.utobj().sendKeys(driver, territoryPage.txtMarketingObligation,
						dsTerritory.get("franchiseesMarketingObligations"));
				fc.utobj().sendKeys(driver, territoryPage.txtOwned, dsTerritory.get("otherExclusiveTerritoriesOwned"));
				fc.utobj().sendKeys(driver, territoryPage.txtJuridiction, dsTerritory.get("jurisdiction"));
				fc.utobj().sendKeys(driver, territoryPage.txtDisputes, dsTerritory.get("disputesifany"));
				fc.utobj().sendKeys(driver, territoryPage.txtNotes, txtNotes);
				fc.utobj().sendKeys(driver, territoryPage.txtGeoCoordinates, dsTerritory.get("geographicCoordinates"));
				fc.utobj().sendKeys(driver, territoryPage.txtLocation, dsTerritory.get("location"));
				fc.utobj().sendKeys(driver, territoryPage.txtCounty, dsTerritory.get("county"));
				fc.utobj().sendKeys(driver, territoryPage.txtZip, dsTerritory.get("zipOwnedExclusively"));
				fc.utobj().sendKeys(driver, territoryPage.txtLandBoundaries, dsTerritory.get("landBoundaries"));
				fc.utobj().sendKeys(driver, territoryPage.txtAraeSize, dsTerritory.get("areaSize"));
				fc.utobj().sendKeys(driver, territoryPage.txtNaturalHazards, dsTerritory.get("naturalHazards"));
				fc.utobj().sendKeys(driver, territoryPage.txtCompetitorsFranchise,
						dsTerritory.get("otherCompetitorsFranchiseTerritory"));
				fc.utobj().sendKeys(driver, territoryPage.txtProximity, dsTerritory.get("proximity"));
				fc.utobj().sendKeys(driver, territoryPage.txtCompetitionFive,
						dsTerritory.get("projectedCompetitionFiveYears"));
				fc.utobj().sendKeys(driver, territoryPage.txtCompetitionTen,
						dsTerritory.get("projectedCompetitionTenYear"));
				fc.utobj().sendKeys(driver, territoryPage.txtPopulation, dsTerritory.get("totalPopulation"));
				fc.utobj().sendKeys(driver, territoryPage.txtMedianIncome, dsTerritory.get("medianIncome"));
				fc.utobj().sendKeys(driver, territoryPage.txtPortsHarbors, dsTerritory.get("portsandHarbors"));
				fc.utobj().sendKeys(driver, territoryPage.txtAirport, dsTerritory.get("airports"));
				fc.utobj().sendKeys(driver, territoryPage.txtHeliports, dsTerritory.get("heliports"));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, territoryPage.btnSubmit);
				fc.utobj().sleep();
				if (fc.utobj().assertPageSource(driver, txtNotes)) {
					Reporter.log("Territory details has been modified successfully");
				} else {
					fc.utobj().throwsException("Territory details couldn't be modified . Test couldn't continue !!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in modifying territory details , please refer screenshot!");
		}
		return txtNotes;
	}

	public Map<String, String> modifyOwner(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("********************** Modifying Franchisee Owner ***********************");

		String testCaseId = "TC_InFoMgr_MultiUnit_Modify_Owner";
		Map<String, String> dsOwners = fc.utobj().readTestData("infomgr", testCaseId);

		Map<String, String> lstOwnerDetails = new HashMap<>();
		lstOwnerDetails.put("FirstName", fc.utobj().generateTestData(dsOwners.get("firstName")));
		lstOwnerDetails.put("LastName", dsOwners.get("lastName"));
		lstOwnerDetails.put("Email", dsOwners.get("email"));
		lstOwnerDetails.put("Country", dsOwners.get("country"));
		lstOwnerDetails.put("State", dsOwners.get("state"));
		lstOwnerDetails.put("SpouseFirstName", fc.utobj().generateTestData("SpouseFirstName1"));
		lstOwnerDetails.put("SpouseLastName", fc.utobj().generateTestData("SpouseLastName1"));

		InfoMgrOwnersPage objAddOwnersPage = new InfoMgrMultiUnitEntityPage(driver).getOwnersPage();
		if (fc.utobj().validate(testCaseId) == true) {
			try {
				// fc.utobj().sendKeys(driver, objAddOwnersPage.txtFirstName,
				// lstOwnerDetails.get("FirstName"));
				// fc.utobj().sendKeys(driver, objAddOwnersPage.txtLastName,
				// lstOwnerDetails.get("LastName"));

				fc.utobj().sendKeys(driver, objAddOwnersPage.txtEmail, lstOwnerDetails.get("Email"));
				fc.utobj().selectDropDownByVisibleText(driver, objAddOwnersPage.drpHomeCountry,
						lstOwnerDetails.get("Country"));
				fc.utobj().sleep();
				fc.utobj().selectDropDownByVisibleText(driver, objAddOwnersPage.drpHomeState,
						lstOwnerDetails.get("State"));
				fc.utobj().sendKeys(driver, objAddOwnersPage.txtSpouseFirstName,
						lstOwnerDetails.get("SpouseFirstName"));
				fc.utobj().sendKeys(driver, objAddOwnersPage.txtSpouseLastName, lstOwnerDetails.get("SpouseLastName"));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, objAddOwnersPage.btnAdd);
				boolean ownerAddedFlag = fc.utobj().assertPageSource(driver,
						fc.utobj().translateString("Owner Details"));
				if (ownerAddedFlag) {
					Reporter.log("Owner modified successfully !!!");
				} else {
					fc.utobj().throwsException("Error in modifying owner !!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in modifying owner , please refer screenshot!");
		}
		return lstOwnerDetails;
	}

	public void modifyTraining(WebDriver driver, Map<String, String> config) throws Exception {
		Reporter.log("********************** modify Training ****************** \n");

		String testCaseId = "TC_InFoMgr_MultiUnit_Training_Modify";
		Map<String, String> dsTraining = fc.utobj().readTestData("infomgr", testCaseId);

		InfoMgrTrainingPage objAddTrainingPage = new InfoMgrMultiUnitEntityPage(driver).getTrainingPage();
		String trainingProgram = fc.utobj().generateTestData(dsTraining.get("trainingProgram"));
		fc.utobj().clickLink(driver, "Modify");
		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().sendKeys(driver, objAddTrainingPage.txtTrainingProgName, trainingProgram);
				fc.utobj().selectDropDownByPartialText(driver, objAddTrainingPage.drpTrainingType,
						dsTraining.get("trainingType"));
				fc.utobj().sendKeys(driver, objAddTrainingPage.txtAttendee, dsTraining.get("attendee"));
				fc.utobj().sendKeys(driver, objAddTrainingPage.txtCompletionDate,
						fc.utobj().getFutureDateUSFormat(10));
				fc.utobj().sendKeys(driver, objAddTrainingPage.txtGrade, dsTraining.get("grade"));
				fc.utobj().sendKeys(driver, objAddTrainingPage.txtInstructor, dsTraining.get("instructor"));
				fc.utobj().sendKeys(driver, objAddTrainingPage.txtLocation, dsTraining.get("location"));
				fc.utobj().sendKeys(driver, objAddTrainingPage.txtAttendeeTitle, dsTraining.get("attendeeTitle"));
				fc.utobj().sendKeys(driver, objAddTrainingPage.txtScore, dsTraining.get("score"));
				fc.utobj().selectCustomManadatoryFieldsOfClient(driver, "fim");// applied
																				// for
																				// Custom
																				// Manadatory
																				// Fields
																				// Of
																				// Client
				fc.utobj().clickElement(driver, objAddTrainingPage.btnSubmit);
				fc.utobj().sleep();
				String txtTraining = fc.utobj().getText(driver, objAddTrainingPage.tblTrainings);
				if (txtTraining.contains(trainingProgram)) {
					Reporter.log("Training modified successfully");
				} else {
					fc.utobj().throwsException("Error - Training didn't got modified. Test Case failes !!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in modifying training , please refer screenshot!");
		}

	}

	// ********************************************************* Delete Section
	// *******************************

	public void deleteTabData(WebDriver driver, Map<String, String> config, WebElement element, String rowToDelete)
			throws Exception {
		String testCaseId = "TC_InFoMgr_MultiUnit_TabData_Delete";

		fc.utobj().clickElement(driver, element);
		fc.utobj().sleep();
		if (fc.utobj().validate(testCaseId) == true) {
			try {
				if (rowToDelete == null) {
					fc.utobj().clickLink(driver, "Delete");
				} else {
					fc.utobj().actionImgOption(driver, rowToDelete, "Delete");
				}
				driver.switchTo().alert().accept();
				fc.utobj().switchFrameToDefault(driver);
				fc.utobj().sleep();
				// Verify that there is no delete link displayed on the page
				boolean rowDeleted = true;

				if (rowToDelete == null) {
					rowDeleted = fc.utobj().assertLinkText(driver, "Delete");
				} else {
					rowDeleted = fc.utobj().assertPageSource(driver, rowToDelete);
				}
				if (!rowDeleted) {
					Reporter.log(element.getText() + " data has been deleted");
				} else {
					fc.utobj().throwsException("Some error occured while deleting the " + element.getText() + " data");
				}
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in deleting data , please refer screenshot!");
		}
	}

	public String createMultiUnit(WebDriver driver, Map<String, String> config) throws Exception {
		String testCaseId = "TC_InfoMgr_MultiUnit_Create";
		AdminInfoMgrCommonMethods adminInfoMgr = new AdminInfoMgrCommonMethods();
		FranchiseesCommonMethods franchise = new FranchiseesCommonMethods();
		InfoMgrCenterInfoPage centerInfo = new InfoMgrFranchiseesPage(driver).getCenterInfoPage();
		Map<String, String> lstOwnerDetails = null;

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				String franchiseID1 = adminInfoMgr.addFranchiseLocation(driver);

				franchise.searchFranchiseAndClick(driver, franchiseID1);

				fc.utobj().clickElement(driver, centerInfo.tabOwners);
				lstOwnerDetails = franchise.addOwner(driver);

				String franchiseID2 = adminInfoMgr.addFranchiseLocation(driver);

				franchise.searchFranchiseAndClick(driver, franchiseID2);
				fc.utobj().clickElement(driver, centerInfo.tabOwners);
				String multiUnitID = franchise.addExistingOwner(driver, lstOwnerDetails.get("FirstName"));

				searchMultiUnit(driver, lstOwnerDetails.get("FirstName") + " " + lstOwnerDetails.get("LastName"));
				if (fc.utobj().assertPageSource(driver,
						lstOwnerDetails.get("FirstName") + " " + lstOwnerDetails.get("LastName"))) {
					Reporter.log("Multi Unit created  -" + multiUnitID + "successfully");
				} else {
					fc.utobj().throwsException("Multi Unit creation failed ");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in creating multi unit , please refer screenshot!");
		}
		return (lstOwnerDetails.get("FirstName") + " " + lstOwnerDetails.get("LastName"));
	}

	public void search(WebDriver driver, Map<String, String> config, Map<String, String> dsMultiUnitInfo)
			throws Exception {
		Reporter.log("************************ Verify Search ***********");
		String testCaseId = "TC_InfoMgr_MultiUnit_Search";
		InfoMgrFranchiseeFilterPage franchiseeFilterPage = new InfoMgrMultiUnitEntityPage(driver)
				.getFranchiseeFilterPage();
		fc.utobj().sleep();
		if (fc.utobj().assertLinkText(driver, "Show Filters")) {
			fc.utobj().clickElement(driver, ".//*[@id='showFilter']");
		}

		if (fc.utobj().validate(testCaseId) == true) {
			try {

				//fc.utobj().setToDefault(driver, franchiseeFilterPage.drpOwnerId);
				fc.utobj().selectValFromMultiSelect(driver, franchiseeFilterPage.drpOwnerId,
						dsMultiUnitInfo.get("multiUnitOwnerName"));

				//fc.utobj().setToDefault(driver, franchiseeFilterPage.drpCity);
				fc.utobj().selectValFromMultiSelect(driver, franchiseeFilterPage.drpCity, dsMultiUnitInfo.get("city"));

				//fc.utobj().setToDefault(driver, franchiseeFilterPage.drpCountryId);
				fc.utobj().selectValFromMultiSelect(driver, franchiseeFilterPage.drpCountryId,
						dsMultiUnitInfo.get("country"));

				//fc.utobj().setToDefault(driver, franchiseeFilterPage.drpStateId);
				fc.utobj().selectValFromMultiSelect(driver, franchiseeFilterPage.drpStateId,
						dsMultiUnitInfo.get("state"));

				fc.utobj().clickElement(driver, franchiseeFilterPage.btnSearch);

				boolean isFranchiseeOnPage = fc.utobj().assertLinkText(driver,
						dsMultiUnitInfo.get("multiUnitOwnerName"));
				List<WebElement> lstchkbox = driver
						.findElements(By.xpath(".//*[@name='form1']//input[@type='checkbox']"));
				if (isFranchiseeOnPage && lstchkbox.size() == 2) {
					Reporter.log("Search filter is working fine !!!");
				} else {
					fc.utobj().throwsException("Error in searching multi unit !!!");
				}

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in searching multi unit, please refer screenshot!");
		}
	}

}
