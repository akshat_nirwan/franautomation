package com.builds.test.infomgr;

import org.openqa.selenium.WebDriver;
import org.testng.Reporter;

import com.builds.uimaps.infomgr.InfoMgrReportsCenterAddressReportPage;
import com.builds.utilities.FranconnectUtil;

public class ReportsCommonMethods extends FranconnectUtil {

	FranconnectUtil fc = new FranconnectUtil();

	public void navigateToCenterAddressReport(WebDriver driver) throws Exception {
		Reporter.log("**********************Navigae To Center Address Report ****************** \n");
		InfoMgrReportsCenterAddressReportPage pobj = new InfoMgrReportsCenterAddressReportPage(driver);
		utobj().clickElement(driver, pobj.reportsLnk);
		utobj().sleep();
		utobj().clickElement(driver, pobj.CenterAddressReport);

	}

	public void navigateToCenterSummaryReport(WebDriver driver) throws Exception {
		Reporter.log("**********************Navigae To Center Summary Report ****************** \n");
		InfoMgrReportsCenterAddressReportPage pobj = new InfoMgrReportsCenterAddressReportPage(driver);
		utobj().clickElement(driver, pobj.reportsLnk);
		utobj().sleep();
		utobj().clickElement(driver, pobj.CenterSumarysReport);

	}

	public void navigateToFranchiseeOwnerReport(WebDriver driver) throws Exception {
		Reporter.log("**********************Navigae To Franchisee To Owner Report ****************** \n");
		InfoMgrReportsCenterAddressReportPage pobj = new InfoMgrReportsCenterAddressReportPage(driver);
		utobj().clickElement(driver, pobj.reportsLnk);
		utobj().sleep();
		utobj().clickElement(driver, pobj.FranchiseeToOwnerReport);
	}

	public void navigateToOwnerDetailsReport(WebDriver driver) throws Exception {
		Reporter.log("**********************Navigae To Owner Details Report ****************** \n");
		InfoMgrReportsCenterAddressReportPage pobj = new InfoMgrReportsCenterAddressReportPage(driver);
		utobj().clickElement(driver, pobj.reportsLnk);
		utobj().sleep();
		utobj().clickElement(driver, pobj.OwnerDetailsReport);
	}

	public void navigateToEntityDetailsReport(WebDriver driver) throws Exception {
		Reporter.log("**********************Navigae To Entity Details Report ****************** \n");
		InfoMgrReportsCenterAddressReportPage pobj = new InfoMgrReportsCenterAddressReportPage(driver);
		utobj().clickElement(driver, pobj.reportsLnk);
		utobj().sleep();
		utobj().clickElement(driver, pobj.EntityDetailsReport);
	}

	public void navigateToEmailStatusReport(WebDriver driver) throws Exception {
		Reporter.log("**********************Navigae To Email Status Report ****************** \n");
		InfoMgrReportsCenterAddressReportPage pobj = new InfoMgrReportsCenterAddressReportPage(driver);
		utobj().clickElement(driver, pobj.reportsLnk);
		utobj().sleep();
		utobj().clickElement(driver, pobj.EmailStatusReport);
	}

	public void navigateToContactHistoryCallReport(WebDriver driver) throws Exception {
		Reporter.log("**********************Navigae To Contact History Call Report ****************** \n");
		InfoMgrReportsCenterAddressReportPage pobj = new InfoMgrReportsCenterAddressReportPage(driver);
		utobj().clickElement(driver, pobj.reportsLnk);
		utobj().sleep();
		utobj().clickElement(driver, pobj.ContactHistoryCallReport);
	}

	public void navigateToLegalViolationReport(WebDriver driver) throws Exception {
		Reporter.log("**********************Navigae To Legal Violation Report ****************** \n");
		InfoMgrReportsCenterAddressReportPage pobj = new InfoMgrReportsCenterAddressReportPage(driver);
		utobj().clickElement(driver, pobj.reportsLnk);
		utobj().sleep();
		utobj().clickElement(driver, pobj.LegalViolationReport);
	}

	public void navigateToDefaultandTerminationReport(WebDriver driver) throws Exception {
		Reporter.log("**********************Navigae To Defaultand Termination Report ****************** \n");
		InfoMgrReportsCenterAddressReportPage pobj = new InfoMgrReportsCenterAddressReportPage(driver);
		utobj().clickElement(driver, pobj.reportsLnk);
		utobj().sleep();
		utobj().clickElement(driver, pobj.DefaultandTerminationReport);
	}

	public void navigateToFranchiseStoreStatusReport(WebDriver driver) throws Exception {
		Reporter.log("**********************Navigae To Franchise Store Status Report ****************** \n");
		InfoMgrReportsCenterAddressReportPage pobj = new InfoMgrReportsCenterAddressReportPage(driver);
		utobj().clickElement(driver, pobj.reportsLnk);
		utobj().sleep();
		utobj().clickElement(driver, pobj.FranchiseStoreStatusReport);
	}

	public void navigateToFDDStoreFranchiseeListReport(WebDriver driver) throws Exception {
		Reporter.log("**********************Navigae To FDD Store Franchisee ListReport ****************** \n");
		InfoMgrReportsCenterAddressReportPage pobj = new InfoMgrReportsCenterAddressReportPage(driver);
		utobj().clickElement(driver, pobj.reportsLnk);
		utobj().sleep();
		utobj().clickElement(driver, pobj.FDDStoreFranchiseList);
	}

	public void navigateToBirthday_Report(WebDriver driver) throws Exception {
		Reporter.log("**********************Navigae To  Birthday Report   ****************** \n");
		InfoMgrReportsCenterAddressReportPage pobj = new InfoMgrReportsCenterAddressReportPage(driver);
		utobj().clickElement(driver, pobj.reportsLnk);
		utobj().sleep();
		utobj().clickElement(driver, pobj.Birthday_Report);
	}

	public void navigateToRenewalStatusReport(WebDriver driver) throws Exception {
		Reporter.log("**********************Navigae To  Renewal Status Report   ****************** \n");
		InfoMgrReportsCenterAddressReportPage pobj = new InfoMgrReportsCenterAddressReportPage(driver);
		utobj().clickElement(driver, pobj.reportsLnk);
		utobj().sleep();
		utobj().clickElement(driver, pobj.RenewalStatusReport);
	}

	public void navigateToRenewalDuesReport(WebDriver driver) throws Exception {
		Reporter.log("**********************Navigae To  Renewal Dues Report****************** \n");
		InfoMgrReportsCenterAddressReportPage pobj = new InfoMgrReportsCenterAddressReportPage(driver);
		utobj().clickElement(driver, pobj.reportsLnk);
		utobj().sleep();
		utobj().clickElement(driver, pobj.RenewalDuesReport);
	}

	public void navigateToLegalReport(WebDriver driver) throws Exception {
		Reporter.log("**********************Navigae To  Renewal Dues Report****************** \n");
		InfoMgrReportsCenterAddressReportPage pobj = new InfoMgrReportsCenterAddressReportPage(driver);
		utobj().clickElement(driver, pobj.reportsLnk);
		utobj().sleep();
		utobj().clickElement(driver, pobj.LegalReport);
	}

	public void navigateToTransferReport(WebDriver driver) throws Exception {
		Reporter.log("**********************Navigae To  Renewal Dues Report****************** \n");
		InfoMgrReportsCenterAddressReportPage pobj = new InfoMgrReportsCenterAddressReportPage(driver);
		utobj().clickElement(driver, pobj.reportsLnk);
		utobj().sleep();
		utobj().clickElement(driver, pobj.TransferReport);
	}

}
