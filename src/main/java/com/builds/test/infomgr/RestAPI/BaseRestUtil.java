package com.builds.test.infomgr.RestAPI;

import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Reporter;

import com.builds.utilities.FranconnectUtil;
import com.ibm.icu.text.SimpleDateFormat;

public class BaseRestUtil {
	
	FranconnectUtil fc = new FranconnectUtil();

	public String changeFormat(String date, String DateFromformat, String DateToformat) {
		Date date1;
		String mdy = null;
		try {
			date1 = new SimpleDateFormat(DateFromformat).parse(date);
			SimpleDateFormat sdm = new SimpleDateFormat(DateToformat);
			mdy = sdm.format(date1);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return mdy;
	}

	// public static Map<String, String> ReferenceStatus_keys = new
	// HashMap<String, String>();
	protected boolean ActivityPerform(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			Map<String, String> UniqueKey, String Module, String SubModule, String Operation, String Divtag) {
		String secureToken = GetSecureToken(driver, dataSet, config);
		String encToken = GetEcryptedToken(driver, dataSet, config, secureToken);
		String Query = GetQuery(driver, dataSet, config, encToken, null, Module, SubModule, Operation);
		String ResultantQuery = GetResultantQuery_R(Query, dataSet, UniqueKey);
		boolean CreateSubModule = CRUDByQuery(driver, dataSet, config, encToken, ResultantQuery, Module, SubModule,
				Operation, Divtag, DataServicePage.ReferenceStatus_keys);
		if (CreateSubModule) {
			System.out.println(" Contract Signing Sub Module created Successfully for Info Manager");
			return true;
		} else {
			System.out.println(" Contract Signing Sub Module Has not been created for Info Manager");
		}
		System.out.println("End of your Program");
		return false;
	}

	protected boolean ActivityPerform(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			Map<String, String> UniqueKey, String Module, String SubModule, String Operation, String Divtag,
			String testcaseid) throws Exception {

		driver.navigate().to(config.get("buildUrl") + DataServicePage.restURL);
		;
		String secureToken = GetSecureToken(driver, dataSet, config);
		;
		String encToken = GetEcryptedToken(driver, dataSet, config, secureToken);
		;
		String Query = GetQuery(driver, dataSet, config, encToken, null, Module, SubModule, Operation);
		;
		String ResultantQuery = GetResultantQuery_R(Query, dataSet, UniqueKey);
		;
		boolean CreateSubModule = CRUDByQuery(driver, dataSet, config, encToken, ResultantQuery, Module, SubModule,
				Operation, Divtag, DataServicePage.ReferenceStatus_keys, testcaseid);
		;
		if (CreateSubModule) {
			System.out.println(" successful");
			return true;
		} else {
			System.out.println(" Problem with rest api " + testcaseid);
		}
		System.out.println("End of your Program");
		return false;

	}

	public boolean CRUDByQuery(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String ecryptedToken, String resultantQuery, String Module, String SubModule, String Operation,
			String Divtag, Map<String, String> ReferenceStatus) {
		// TODO Auto-generated method stub createDiv
		try {
			Select DropDownServiceType = new Select(driver.findElement(By.id("serviceType")));
			DropDownServiceType.selectByVisibleText(Operation);
			Thread.sleep(2000);
			fc.utobj().sendKeys(driver, driver.findElement(By.xpath(".//*[@id='" + Divtag + "']//*[@id='key']")),
					ecryptedToken);
			Select CModule = new Select(driver.findElement(By.xpath("//*[@id='" + Divtag + "']//*[@name='module']")));
			CModule.selectByVisibleText(Module);
			Select CSubModule = new Select(
					driver.findElement(By.xpath("//*[@id='" + Divtag + "']//*[@name='subModule']")));
			CSubModule.selectByVisibleText(SubModule);
			if (Divtag.equals("retrieveDiv")) {
				fc.utobj().sendKeys(driver,
						driver.findElement(By.xpath(".//*[@id='" + Divtag + "']//*[@id='filterXML']")), resultantQuery);
			} else {
				fc.utobj().sendKeys(driver,
						driver.findElement(By.xpath(".//*[@id='" + Divtag + "']//*[@id='xmlString']")), resultantQuery);
			}
			clickElement(driver, driver.findElement(By.xpath(".//*[@id='" + Divtag + "']//*[@name='Submit']")));

			driver.switchTo().frame("resultIfr");
			Thread.sleep(3000);
			String FinalResult = driver.findElement(By.tagName("responseStatus")).getText();
			if (!FinalResult.equals("Error"))
				ReferenceStatus.put(SubModule.toString(), driver.findElement(By.tagName("referenceId")).getText());
			else
				ReferenceStatus.put(SubModule.toString(), "");
			driver.switchTo().defaultContent();
			if (FinalResult.equals("Success") || FinalResult.equals("Warning"))
				return true;
			else
				return false;

		} catch (Exception ex) {
			driver.switchTo().defaultContent();
			ex.printStackTrace();
			return false;
		}
	}

	public boolean CRUDByQuery(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String ecryptedToken, String resultantQuery, String Module, String SubModule, String Operation,
			String Divtag, Map<String, String> ReferenceStatus, String testcaseid) {
		// TODO Auto-generated method stub createDiv
		try {
			Select DropDownServiceType = new Select(driver.findElement(By.id("serviceType")));
			DropDownServiceType.selectByVisibleText(Operation);
			Thread.sleep(2000);
			fc.utobj().sendKeys(driver, driver.findElement(By.xpath(".//*[@id='" + Divtag + "']//*[@id='key']")),
					ecryptedToken);
			fc.utobj().sleep(1500);
			Select CModule = new Select(driver.findElement(By.xpath("//*[@id='" + Divtag + "']//*[@name='module']")));
			CModule.selectByVisibleText(Module);
			fc.utobj().sleep(1500);
			Select CSubModule = new Select(
					driver.findElement(By.xpath("//*[@id='" + Divtag + "']//*[@name='subModule']")));
			CSubModule.selectByVisibleText(SubModule);
			fc.utobj().sleep(1500);
			if (Divtag.equals("retrieveDiv")) {
				fc.utobj().sendKeys(driver,
						driver.findElement(By.xpath(".//*[@id='" + Divtag + "']//*[@id='filterXML']")), resultantQuery);
			} else {
				fc.utobj().sendKeys(driver,
						driver.findElement(By.xpath(".//*[@id='" + Divtag + "']//*[@id='xmlString']")), resultantQuery);
			}
			fc.utobj().sleep(1500);
			clickElement(driver, driver.findElement(By.xpath(".//*[@id='" + Divtag + "']//*[@name='Submit']")));
			Thread.sleep(1000);
			driver.switchTo().frame("resultIfr");
			Thread.sleep(3000);
			String FinalResult = driver.findElement(By.tagName("responseStatus")).getText();
			if (!FinalResult.equals("Error"))
				try {
					ReferenceStatus.put(testcaseid + SubModule.toString(),
							driver.findElement(By.tagName("referenceId")).getText());
				} catch (Exception ex) {

				}
			else
				ReferenceStatus.put(SubModule.toString(), "");
			driver.switchTo().defaultContent();
			if (FinalResult.equals("Success") || FinalResult.equals("Warning"))
				return true;
			else
				return false;

		} catch (Exception ex) {
			driver.switchTo().defaultContent();
			ex.printStackTrace();
			return false;
		}
	}

	public String GetResultantQuery_R(String query, Map<String, String> dataSet, Map<String, String> UniqueKey) {
		try {
			String format = query;
			HashMap<String, String> map = new HashMap<String, String>();
			Pattern p = Pattern.compile("<([^\\s>/]+)");
			Matcher m = p.matcher(format);
			int i = 0;
			while (m.find()) {
				String tag = m.group(1);
				map.put(String.valueOf(i), tag);
				i++;
			}
			int j = 2;
			while (i > 0) {

				// System.out.println(map.get(String.valueOf(j)) +
				// ">>>>>>>>>>>>>>>>>>>>>>"+
				// dataSet.get(map.get(String.valueOf(j))));
				if (dataSet.get(map.get(String.valueOf(j))) != null
						|| UniqueKey.get(map.get(String.valueOf(j))) != null) {
					// if (map.get(String.valueOf(j)).equals("userID"))
					if (UniqueKey.get(map.get(String.valueOf(j))) != null) {
						format = format.replaceAll(
								"(?<=<" + map.get(String.valueOf(j)) + ">).*?(?=</" + map.get(String.valueOf(j)) + ">)",
								UniqueKey.get(map.get(String.valueOf(j))));
						// System.out.println(format);
					} else {
						format = format.replaceAll(
								"(?<=<" + map.get(String.valueOf(j)) + ">).*?(?=</" + map.get(String.valueOf(j)) + ">)",
								dataSet.get(map.get(String.valueOf(j))));
					}
				} else {
					format = format.replaceAll(
							"(?<=<" + map.get(String.valueOf(j)) + ">).*?(?=</" + map.get(String.valueOf(j)) + ">)",
							"");
				}
				i--;
				j++;
			}

			// System.out.println(format);
			return format;
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}

	public String GetQuery(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String ecryptedToken, String roleType, String module, String subModule, String operation) {
		// TODO Auto-generated method stub
		try {
			Select DropDownServiceType = new Select(driver.findElement(By.id("serviceType")));
			DropDownServiceType.selectByVisibleText("Query");
			Thread.sleep(2000);
			fc.utobj().sendKeys(driver, driver.findElement(By.xpath(".//*[@id='queryDiv']//*[@id='key']")),
					ecryptedToken);
			fc.utobj().sleep(1500);
			Select Module = new Select(driver.findElement(By.xpath("//*[@id='queryDiv']//*[@name='module']")));
			Module.selectByVisibleText(module);
			fc.utobj().sleep(1500);
			Select SubModule = new Select(driver.findElement(By.xpath("//*[@id='queryDiv']//*[@name='subModule']")));
			SubModule.selectByVisibleText(subModule);
			fc.utobj().sleep(1500);
			Select Operation = new Select(driver.findElement(By.xpath("//*[@id='queryDiv']//*[@name='operation']")));
			Operation.selectByVisibleText(operation);
			fc.utobj().sleep(1500);
			if (roleType != null) {
				Select roleTypeForQuery = new Select(
						driver.findElement(By.xpath("//*[@id='queryDiv']//*[@name='roleTypeForQuery']")));
				roleTypeForQuery.selectByVisibleText(roleType);
			}
			clickElement(driver, driver.findElement(By.xpath(".//*[@id='queryDiv']//*[@id='Submit']")));

			driver.switchTo().frame("resultIfr");
			String responseData = driver.findElement(By.tagName("responseData")).getText();
			String str1 = driver.getPageSource();
			// System.out.println(str1);
			int firstOpenP = str1.indexOf("<fcRequest");
			int lastOpenP = str1.lastIndexOf("fcRequest>");
			String format = str1.substring(firstOpenP, lastOpenP + 10);
			driver.switchTo().defaultContent();
			// System.out.println(format);
			return format;
		} catch (Exception ex) {
			driver.switchTo().defaultContent();
			ex.printStackTrace();
		}
		return null;
	}

	public String GetEcryptedToken(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String SecureToken) {
		// TODO Auto-generated method stub
		try {
			Select DropDownServiceType = new Select(driver.findElement(By.id("serviceType")));
			DropDownServiceType.selectByVisibleText("Get Encrypted Token");
			Thread.sleep(2000);
			DataServicePage de = new DataServicePage(driver);
			fc.utobj().sendKeys(driver, de.SecureToken, SecureToken);
			fc.utobj().sleep(1500);
			fc.utobj().sendKeys(driver, driver.findElement(By.xpath(".//*[@id='getEncTokenDiv']//*[@id='clientCode']")),
					dataSet.get("clientcode"));
			fc.utobj().sleep(1500);
			clickElement(driver, driver.findElement(By.xpath(".//*[@id='getEncTokenDiv']//*[@name='Submit']")));
			fc.utobj().sleep(1500);
			driver.switchTo().frame("resultIfr");
			String enctext = driver.findElement(By.xpath(".//html/body")).getText();
			// System.out.println(enctext);
			int index = enctext.indexOf(":");
			String str = enctext.substring(index + 2, enctext.length());
			driver.switchTo().defaultContent();
			// System.out.println(str);
			return str;
		} catch (Exception ex) {
			driver.switchTo().defaultContent();
		}
		return null;
	}

	public String GetSecureToken(WebDriver driver, Map<String, String> dataSet, Map<String, String> config) {
		try {
			// driver = gotoDataServicePage(driver, config);
			DataServicePage de = new DataServicePage(driver);

			Select DropDownServiceType = new Select(driver.findElement(By.id("serviceType")));
			DropDownServiceType.selectByVisibleText("Login");
			Thread.sleep(2000);
			fc.utobj().sendKeys(driver, driver.findElement(By.xpath(".//*[@id='loginDiv']//*[@id='clientCode']")),
					dataSet.get("clientcode"));
			fc.utobj().sleep(1500);
			Select DropDownResponseType = new Select(driver.findElement(By.id("responseType")));
			DropDownResponseType.selectByVisibleText("XML");
			fc.utobj().sleep(1500);
			fc.utobj().clickElement(driver, driver.findElement(By.xpath(".//*[@id='loginDiv']//*[@id='Submit']")));
			Thread.sleep(1000);
			driver.switchTo().frame("resultIfr");
			fc.utobj().sleep(1500);
			String text = driver.findElement(By.tagName("secureToken")).getText();
			driver.switchTo().defaultContent();
			// System.out.println(text);
			return text;
		} catch (Exception e) {
			driver.switchTo().defaultContent();
			e.printStackTrace();
			return null;
		}

	}

	public void clickElement(WebDriver driver, WebElement element) throws Exception {

		fc.utobj().moveToElement(driver, element);
		try {

			element.click();
			// analyzeLog(driver);
		} catch (Exception e) {
			throwsException("Element not clickable : " + element);
		}
	}

	public String throwsException(String exceptionMsg) throws Exception {
		Reporter.log("");
		throw new Exception(exceptionMsg);
	}

}
