package com.builds.test.infomgr.RestAPI;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;
import org.xml.sax.SAXException;

import com.builds.test.admin.AdminAreaRegionAddAreaRegionPageTest;
import com.builds.test.admin.AdminConfigurationConfigureStoreTypePageTest;
import com.builds.uimaps.infomgr.InfoMgrCorporateLocationsPage;
import com.builds.uimaps.infomgr.InfoMgrExportPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class InfoMgrRestService extends BaseRestUtil {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = "infomgr_old")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-07-03", testCaseDescription = "Delete SubModule of InfoMgr  In Rest API ", testCaseId = "TC_07_Delete_Sub_Tab_Info_Mgr")
	public void DeleteAllSubTabInfo() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("infomgr", testCaseId);
		Map<String, String> UniqueKey_Franchisee = new HashMap<String, String>();
		Map<String, String> UniqueKey_CorpUser = new HashMap<String, String>();
		Map<String, String> UniqueKey_Employees = new HashMap<String, String>();
		Map<String, String> UniqueKey_Agreement = new HashMap<String, String>();
		Map<String, String> UniqueKey_Call = new HashMap<String, String>();
		Map<String, String> UniqueKey_CustomerComplain = new HashMap<String, String>();
		Map<String, String> UniqueKey_EntityDetails = new HashMap<String, String>();
		Map<String, String> UniqueKey_Events = new HashMap<String, String>();
		Map<String, String> UniqueKey_Financial = new HashMap<String, String>();
		Map<String, String> UniqueKey_Gaurantor = new HashMap<String, String>();
		Map<String, String> UniqueKey_Insurance = new HashMap<String, String>();
		Map<String, String> UniqueKey_LegalViolation = new HashMap<String, String>();
		Map<String, String> UniqueKey_Lenders = new HashMap<String, String>();
		Map<String, String> UniqueKey_ExternalMails = new HashMap<String, String>();
		Map<String, String> UniqueKey_Marketing = new HashMap<String, String>();
		Map<String, String> UniqueKey_MysteryReview = new HashMap<String, String>();
		Map<String, String> UniqueKey_Owners = new HashMap<String, String>();
		Map<String, String> UniqueKey_RealEstate = new HashMap<String, String>();
		Map<String, String> UniqueKey_Remark = new HashMap<String, String>();
		Map<String, String> UniqueKey_Renewal = new HashMap<String, String>();
		Map<String, String> UniqueKey_Task = new HashMap<String, String>();
		Map<String, String> UniqueKey_Territory = new HashMap<String, String>();
		Map<String, String> UniqueKey_Training = new HashMap<String, String>();
		Map<String, String> UniqueKey_ContractSigning = new HashMap<String, String>();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			UserCreation uc = new UserCreation();
			String Franchisee = null;
			boolean createUser = uc.createCorpUser(driver, config, dataSet, testCaseId, UniqueKey_CorpUser);
			if (createUser)
				Franchisee = AddLocation(driver, dataSet, config, UniqueKey_Franchisee, testCaseId);
			if (Franchisee == null) {
				fc.utobj().throwsException("Franchisee Location is not added " + testCaseId);
			}
			boolean fill = fillallSubTabInfo(driver, dataSet, config, testCaseId, UniqueKey_CorpUser,
					UniqueKey_Employees, UniqueKey_Agreement, UniqueKey_Call, UniqueKey_CustomerComplain,
					UniqueKey_EntityDetails, UniqueKey_Events, UniqueKey_Financial, UniqueKey_Gaurantor,
					UniqueKey_Insurance, UniqueKey_LegalViolation, UniqueKey_Lenders, UniqueKey_ExternalMails,
					UniqueKey_Marketing, UniqueKey_MysteryReview, UniqueKey_Owners, UniqueKey_RealEstate,
					UniqueKey_Remark, UniqueKey_Renewal, UniqueKey_Task, UniqueKey_Territory, UniqueKey_Training,
					UniqueKey_ContractSigning);

			if (!fill) {
				fc.utobj().throwsException("Sub Module is not created " + testCaseId);
			}

			boolean delete = deleteAllSubTabInfo(driver, dataSet, config, testCaseId);

			if (!delete) {
				fc.utobj().throwsException("Sub Module is not Deleted " + testCaseId);
			}
			if (Franchisee != null && fill && delete) {

				UniqueKey_Call.remove("referenceId");
				UniqueKey_Call.remove("parentReferenceId");
				UniqueKey_Call.remove("timeAdded");
				UniqueKey_Franchisee.put("calldate", UniqueKey_Call.get("date"));
				ArrayList<String> listItems = new ArrayList<String>(UniqueKey_Call.values());
				// Status =
				// fc.utobj().assertPageSourceWithMultipleRecords(driver,
				// listItems);
				boolean Status = ValidateForExportCall(driver, config, listItems, DataServicePage.Call,
						UniqueKey_Franchisee, UniqueKey_Call.get("subject"));
				if (Status)
					fc.utobj().throwsException("Call is not found on view page " + testCaseId);

				fc.loginpage().login(driver);
				fc.infomgr().infomgr_common().InfoMgrCorporateLocations(driver);

				InfoMgrCorporateLocationsPage objFranPage = new InfoMgrCorporateLocationsPage(driver);
				try {
					fc.utobj().clickElement(driver, objFranPage.lnkShowAll);
				} catch (Exception ex) {

				}

				boolean boolAddFranFlag = fc.utobj().assertPageSource(driver,
						UniqueKey_Franchisee.get("franchiseeName"));
				fc.utobj().clickPartialLinkText(driver, UniqueKey_Franchisee.get("franchiseeName"));
				if (boolAddFranFlag == true) {
					Reporter.log("Corporate Location addition successfull !!! ");
					// verifing contract signing
					fc.utobj().clickPartialLinkText(driver, "Contract Signing");
					UniqueKey_ContractSigning.remove("referenceId");
					UniqueKey_ContractSigning.remove("parentReferenceId");
					listItems = new ArrayList<String>(UniqueKey_ContractSigning.values());
					Status = fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);
					if (Status)
						fc.utobj().throwsException("Contact Signing is found on view page " + testCaseId);

					fc.utobj().clickPartialLinkText(driver, "Agreement");
					UniqueKey_Agreement.remove("referenceId");
					UniqueKey_Agreement.remove("parentReferenceId");
					listItems = new ArrayList<String>(UniqueKey_Agreement.values());

					Status = fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);
					if (Status)
						fc.utobj().throwsException("Agreement is found on view page " + testCaseId);

					fc.utobj().clickPartialLinkText(driver, "Customer Complaints");
					UniqueKey_CustomerComplain.remove("referenceId");
					UniqueKey_CustomerComplain.remove("parentReferenceId");
					listItems = new ArrayList<String>(UniqueKey_CustomerComplain.values());
					Status = fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);
					if (Status)
						fc.utobj().throwsException("Customer Complaints is found on view page " + testCaseId);

					fc.utobj().clickPartialLinkText(driver, "Entity Details");
					UniqueKey_EntityDetails.remove("referenceId");
					UniqueKey_EntityDetails.remove("parentReferenceId");
					listItems = new ArrayList<String>(UniqueKey_EntityDetails.values());
					Status = fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);
					if (Status)
						fc.utobj().throwsException("Entity Details is found on view page " + testCaseId);

					fc.utobj().clickElement(driver,
							driver.findElement(By.xpath(".//*[@id='fimUl']//a[contains(text(),'Training')]")));
					UniqueKey_Training.remove("referenceId");
					UniqueKey_Training.remove("parentReferenceId");
					listItems = new ArrayList<String>(UniqueKey_Training.values());
					Status = fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);
					if (Status)
						fc.utobj().throwsException("Training is found on view page " + testCaseId);

					fc.utobj().clickPartialLinkText(driver, "Territory");
					listItems = new ArrayList<String>(UniqueKey_Territory.values());
					Status = fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);
					if (Status)
						fc.utobj().throwsException("Territory is found on view page " + testCaseId);

					fc.utobj().clickPartialLinkText(driver, "Renewal");
					UniqueKey_Renewal.remove("referenceId");
					UniqueKey_Renewal.remove("parentReferenceId");
					listItems = new ArrayList<String>(UniqueKey_Renewal.values());
					Status = fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);
					if (Status)
						fc.utobj().throwsException("Renewal is found on view page " + testCaseId);

					fc.utobj().clickPartialLinkText(driver, "Real Estate");
					UniqueKey_RealEstate.remove("referenceId");
					UniqueKey_RealEstate.remove("parentReferenceId");
					listItems = new ArrayList<String>(UniqueKey_RealEstate.values());
					Status = fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);
					if (Status)
						fc.utobj().throwsException("Real Estate is  found on view page " + testCaseId);

					fc.utobj().clickPartialLinkText(driver, DataServicePage.CenterInfo);
					listItems = new ArrayList<String>();
					listItems.add(UniqueKey_Call.get("subject"));
					Status = fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);
					if (Status) {
						fc.utobj().clickPartialLinkText(driver, UniqueKey_Call.get("subject"));
						fc.commonMethods().switch_cboxIframe_frameId(driver);
						UniqueKey_Call.remove("referenceId");
						UniqueKey_Call.remove("parentReferenceId");
						listItems = new ArrayList<String>(UniqueKey_Call.values());
						Status = fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);
						if (Status)
							fc.utobj().throwsException("Call is  found on view page " + testCaseId);
					}
				} else {
					fc.utobj().throwsException("Corporate Location addition failed !!!");
				}
				fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			}
		} catch (Exception ex) {
			fc.utobj().quitBrowserOnCatch(driver, ex, testCaseId);
		}
	}

	private boolean deleteAllSubTabInfo(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId) throws Exception {
		try {

			boolean status = false;
			Map<String, String> UniqueKey_Employees = new HashMap<String, String>();
			Map<String, String> UniqueKey_Agreement = new HashMap<String, String>();
			Map<String, String> UniqueKey_Call = new HashMap<String, String>();
			Map<String, String> UniqueKey_CustomerComplain = new HashMap<String, String>();
			Map<String, String> UniqueKey_EntityDetails = new HashMap<String, String>();
			Map<String, String> UniqueKey_Events = new HashMap<String, String>();
			Map<String, String> UniqueKey_Financial = new HashMap<String, String>();
			Map<String, String> UniqueKey_Gaurantor = new HashMap<String, String>();
			Map<String, String> UniqueKey_Insurance = new HashMap<String, String>();
			Map<String, String> UniqueKey_LegalViolation = new HashMap<String, String>();
			Map<String, String> UniqueKey_Lenders = new HashMap<String, String>();
			Map<String, String> UniqueKey_ExternalMails = new HashMap<String, String>();
			Map<String, String> UniqueKey_Marketing = new HashMap<String, String>();
			Map<String, String> UniqueKey_MysteryReview = new HashMap<String, String>();
			Map<String, String> UniqueKey_Owners = new HashMap<String, String>();
			Map<String, String> UniqueKey_RealEstate = new HashMap<String, String>();
			Map<String, String> UniqueKey_Renewal = new HashMap<String, String>();
			Map<String, String> UniqueKey_Task = new HashMap<String, String>();
			Map<String, String> UniqueKey_Territory = new HashMap<String, String>();
			Map<String, String> UniqueKey_Training = new HashMap<String, String>();
			Map<String, String> UniqueKey_ContractSigning = new HashMap<String, String>();

			UniqueKey_ContractSigning.put("referenceId",
					DataServicePage.ReferenceStatus_keys.get(testCaseId + "Contract Signing"));
			UniqueKey_ContractSigning.put("fimDdContractRecievedSigned", "10/10/2014");
			UniqueKey_ContractSigning.put("fimDdComplaintDate", "02/12/2014");
			UniqueKey_ContractSigning.put("fimTtComplaint", "02/12/2014");
			UniqueKey_ContractSigning.put("fimTtComplaintBy", "02/12/2014");
			UniqueKey_ContractSigning.put("emailIds_includeInCampaignList", "02/12/2014");
			status = ActivityPerform(driver, dataSet, config, UniqueKey_ContractSigning, DataServicePage.InfoMgr,
					DataServicePage.ContractSigning, DataServicePage.Delete, DataServicePage.deleteDiv, testCaseId);
			if (!status) {
				fc.utobj().throwsException("Contract Signing Deletion is not performed in " + testCaseId);
			}

			UniqueKey_Agreement.put("referenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Agreement"));
			UniqueKey_Agreement.put("fimDdApprovedDate", fc.utobj().generateCurrentDatewithformat("yyyy-MM-dd"));
			status = ActivityPerform(driver, dataSet, config, UniqueKey_Agreement, DataServicePage.InfoMgr,
					DataServicePage.Agreement, DataServicePage.Delete, DataServicePage.deleteDiv, testCaseId);
			if (!status) {
				fc.utobj().throwsException("Agreement Deletion is not performed in " + testCaseId);
			}

			UniqueKey_Call.put("referenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Call"));
			UniqueKey_Call.put("date", fc.utobj().generateCurrentDatewithformat("yyyy-MM-dd"));
			UniqueKey_Call.put("timeAdded", "03:00Z");
			UniqueKey_Call.put("callStatus", "Contacted");
			UniqueKey_Call.put("callType", "Outbound Call");
			UniqueKey_Call.put("subject", "subjent" + fc.utobj().generateRandomNumber());
			status = ActivityPerform(driver, dataSet, config, UniqueKey_Call, DataServicePage.InfoMgr,
					DataServicePage.Call, DataServicePage.Delete, DataServicePage.deleteDiv, testCaseId);
			if (!status) {
				fc.utobj().throwsException("Call Deletion is not performed in " + testCaseId);
			}

			UniqueKey_CustomerComplain.put("referenceId",
					DataServicePage.ReferenceStatus_keys.get(testCaseId + "Customer Complaints"));
			UniqueKey_CustomerComplain.put("fimDdComplaintDate",
					fc.utobj().generateCurrentDatewithformat("yyyy-MM-dd"));
			UniqueKey_CustomerComplain.put("fimTtComplaint", "Complaint" + fc.utobj().generateRandomNumber());
			UniqueKey_CustomerComplain.put("fimDdIncidentDate", fc.utobj().generateCurrentDatewithformat("yyyy-MM-dd"));
			UniqueKey_CustomerComplain.put("fimTtComplaintBy", "complainId" + fc.utobj().generateRandomNumber());
			status = ActivityPerform(driver, dataSet, config, UniqueKey_CustomerComplain, DataServicePage.InfoMgr,
					DataServicePage.CustomerComplaints, DataServicePage.Delete, DataServicePage.deleteDiv, testCaseId);

			UniqueKey_Employees.put("referenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Employees"));
			UniqueKey_Employees.put("employeeType", "New");
			UniqueKey_Employees.put("firstName", "Fname" + fc.utobj().generateRandomNumber());
			UniqueKey_Employees.put("lastName", "Lname" + fc.utobj().generateRandomNumber());
			status = ActivityPerform(driver, dataSet, config, UniqueKey_Employees, DataServicePage.InfoMgr,
					DataServicePage.Employees, DataServicePage.Delete, DataServicePage.deleteDiv, testCaseId);
			if (!status) {
				fc.utobj().throwsException("Employees Deletion is not performed in " + testCaseId);
			}

			UniqueKey_EntityDetails.put("referenceId",
					DataServicePage.ReferenceStatus_keys.get(testCaseId + "Entity Details"));
			UniqueKey_EntityDetails.put("entityType", "New");
			UniqueKey_EntityDetails.put("fimTtEntityName", "Name" + fc.utobj().generateRandomNumber());
			status = ActivityPerform(driver, dataSet, config, UniqueKey_EntityDetails, DataServicePage.InfoMgr,
					DataServicePage.EntityDetails, DataServicePage.Delete, DataServicePage.deleteDiv, testCaseId);
			if (!status) {
				fc.utobj().throwsException("Entity Details Deletion is not performed in " + testCaseId);
			}

			UniqueKey_Events.put("referenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Events"));
			UniqueKey_Events.put("fimDdEventDate", fc.utobj().generateCurrentDatewithformat("yyyy-MM-dd"));
			UniqueKey_Events.put("fimTtEventAuthor", "Oraganizer" + fc.utobj().generateRandomNumber());
			status = ActivityPerform(driver, dataSet, config, UniqueKey_Events, DataServicePage.InfoMgr,
					DataServicePage.Events, DataServicePage.Delete, DataServicePage.deleteDiv, testCaseId);
			if (!status) {
				fc.utobj().throwsException("Events Deletion is not performed in " + testCaseId);
			}

			UniqueKey_Financial.put("referenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Financial"));
			UniqueKey_Financial.put("fimTtFirstName", "Fname" + fc.utobj().generateRandomNumber());
			status = ActivityPerform(driver, dataSet, config, UniqueKey_Financial, DataServicePage.InfoMgr,
					DataServicePage.Financial, DataServicePage.Delete, DataServicePage.deleteDiv, testCaseId);
			if (!status) {
				fc.utobj().throwsException("Financial Deletion is not performed in " + testCaseId);
			}

			UniqueKey_Gaurantor.put("referenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Guarantors"));
			UniqueKey_Gaurantor.put("fimTtFirstName", "Fname" + fc.utobj().generateRandomNumber());
			status = ActivityPerform(driver, dataSet, config, UniqueKey_Gaurantor, DataServicePage.InfoMgr,
					DataServicePage.Guarantors, DataServicePage.Delete, DataServicePage.deleteDiv, testCaseId);
			if (!status) {
				fc.utobj().throwsException("Gaurantor Deletion is not performed in " + testCaseId);
			}

			UniqueKey_Insurance.put("referenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Insurance"));
			UniqueKey_Insurance.put("fimTtInsuranceCompanyName", "CompanyName" + fc.utobj().generateRandomNumber());
			status = ActivityPerform(driver, dataSet, config, UniqueKey_Insurance, DataServicePage.InfoMgr,
					DataServicePage.Insurance, DataServicePage.Delete, DataServicePage.deleteDiv, testCaseId);
			if (!status) {
				fc.utobj().throwsException("Insurance Deletion is not performed in " + testCaseId);
			}

			UniqueKey_LegalViolation.put("referenceId",
					DataServicePage.ReferenceStatus_keys.get(testCaseId + "Legal Violation"));
			UniqueKey_LegalViolation.put("fimTaSummary",
					"Summary is not defined pls refer summary" + fc.utobj().generateRandomNumber());
			status = ActivityPerform(driver, dataSet, config, UniqueKey_LegalViolation, DataServicePage.InfoMgr,
					DataServicePage.LegalViolation, DataServicePage.Delete, DataServicePage.deleteDiv, testCaseId);
			if (!status) {
				fc.utobj().throwsException("Legal Violation Deletion is not performed in " + testCaseId);
			}

			UniqueKey_Lenders.put("referenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Lenders"));
			UniqueKey_Lenders.put("fimTtFirstName", "Fname" + fc.utobj().generateRandomNumber());
			status = ActivityPerform(driver, dataSet, config, UniqueKey_Lenders, DataServicePage.InfoMgr,
					DataServicePage.Lenders, DataServicePage.Delete, DataServicePage.deleteDiv, testCaseId);
			if (!status) {
				fc.utobj().throwsException("Lenders Deletion is not performed in " + testCaseId);
			}

			UniqueKey_ExternalMails.put("referenceId",
					DataServicePage.ReferenceStatus_keys.get(testCaseId + "External Mail"));
			UniqueKey_ExternalMails.put("mailFrom", "test@franconnect.net");
			UniqueKey_ExternalMails.put("mailCc", "test@franconnect.net");
			UniqueKey_ExternalMails.put("mailBcc", "test@franconnect.net");
			UniqueKey_ExternalMails.put("mailSubject", "Test Mail");
			UniqueKey_ExternalMails.put("mailMessage", "hello this is test");
			status = ActivityPerform(driver, dataSet, config, UniqueKey_ExternalMails, DataServicePage.InfoMgr,
					DataServicePage.ExternalMail, DataServicePage.Delete, DataServicePage.deleteDiv, testCaseId);
			if (!status) {
				fc.utobj().throwsException("External Mails Deletion is not performed in " + testCaseId);
			}

			UniqueKey_Marketing.put("referenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Marketing"));
			UniqueKey_Marketing.put("fimTtFirstName", "Fname" + fc.utobj().generateRandomNumber());
			status = ActivityPerform(driver, dataSet, config, UniqueKey_Marketing, DataServicePage.InfoMgr,
					DataServicePage.Marketing, DataServicePage.Delete, DataServicePage.deleteDiv, testCaseId);
			if (!status) {
				fc.utobj().throwsException("Marketing Deletion is not performed in " + testCaseId);
			}

			UniqueKey_MysteryReview.put("referenceId",
					DataServicePage.ReferenceStatus_keys.get(testCaseId + "Mystery Review"));
			UniqueKey_MysteryReview.put("fimDdInspectionDate", fc.utobj().generateCurrentDatewithformat("yyyy-MM-dd"));
			status = ActivityPerform(driver, dataSet, config, UniqueKey_MysteryReview, DataServicePage.InfoMgr,
					DataServicePage.MysteryReview, DataServicePage.Delete, DataServicePage.deleteDiv, testCaseId);
			if (!status) {
				fc.utobj().throwsException("Mystery Review Deletion is not performed in " + testCaseId);
			}

			UniqueKey_Owners.put("referenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Owners"));
			UniqueKey_Owners.put("ownerType", "New");
			UniqueKey_Owners.put("lastName", "Fname" + fc.utobj().generateRandomNumber());
			UniqueKey_Owners.put("firstName", "Fname" + fc.utobj().generateRandomNumber());
			status = ActivityPerform(driver, dataSet, config, UniqueKey_Owners, DataServicePage.InfoMgr,
					DataServicePage.Owners, DataServicePage.Delete, DataServicePage.deleteDiv, testCaseId);
			if (!status) {
				fc.utobj().throwsException("Owners Deletion is not performed in " + testCaseId);
			}

			UniqueKey_RealEstate.put("referenceId",
					DataServicePage.ReferenceStatus_keys.get(testCaseId + "Real Estate"));
			UniqueKey_RealEstate.put("fimTtFirstName", "Fname" + fc.utobj().generateRandomNumber());
			status = ActivityPerform(driver, dataSet, config, UniqueKey_RealEstate, DataServicePage.InfoMgr,
					DataServicePage.RealEstate, DataServicePage.Delete, DataServicePage.deleteDiv, testCaseId);
			if (!status) {
				fc.utobj().throwsException("Real Estate Deletion is not performed in " + testCaseId);
			}

			UniqueKey_Renewal.put("referenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Renewal"));
			UniqueKey_Renewal.put("fimTtFirstName", "Fname" + fc.utobj().generateRandomNumber());
			status = ActivityPerform(driver, dataSet, config, UniqueKey_Renewal, DataServicePage.InfoMgr,
					DataServicePage.Renewal, DataServicePage.Delete, DataServicePage.deleteDiv, testCaseId);
			if (!status) {
				fc.utobj().throwsException("Renewal Deletion is not performed in " + testCaseId);
			}

			UniqueKey_Task.put("referenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Task"));
			UniqueKey_Task.put("fimTtFirstName", "Fname" + fc.utobj().generateRandomNumber());
			status = ActivityPerform(driver, dataSet, config, UniqueKey_Task, DataServicePage.InfoMgr,
					DataServicePage.Task, DataServicePage.Delete, DataServicePage.deleteDiv, testCaseId);
			if (!status) {
				fc.utobj().throwsException("Task Deletion is not performed in " + testCaseId);
			}

			UniqueKey_Territory.put("referenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Territory"));
			UniqueKey_Territory.put("fimTtFirstName", "Fname" + fc.utobj().generateRandomNumber());
			status = ActivityPerform(driver, dataSet, config, UniqueKey_Territory, DataServicePage.InfoMgr,
					DataServicePage.Territory, DataServicePage.Delete, DataServicePage.deleteDiv, testCaseId);
			if (!status) {
				fc.utobj().throwsException("Territory Deletion is not performed in " + testCaseId);
			}

			UniqueKey_Training.put("referenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Training"));
			UniqueKey_Training.put("fimTtFirstName", "Fname" + fc.utobj().generateRandomNumber());
			status = ActivityPerform(driver, dataSet, config, UniqueKey_Training, DataServicePage.InfoMgr,
					DataServicePage.Training, DataServicePage.Delete, DataServicePage.deleteDiv, testCaseId);
			if (!status) {
				fc.utobj().throwsException("Training Deletion is not performed in " + testCaseId);
			}

			return true;

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
		return false;
	}

	@Test(groups = "infomgr_old")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-07-03", testCaseDescription = "Verify The UpdateAll SubModule of InfoMgr  In Rest API", testCaseId = "TC_06_Update_Sub_Tab_Info_Mgr")
	public void UpdateAndValidateAllSubTabInfo()
			throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("infomgr", testCaseId);
		Map<String, String> UniqueKey_Franchisee = new HashMap<String, String>();
		Map<String, String> UniqueKey_CorpUser = new HashMap<String, String>();
		Map<String, String> UniqueKey_Employees = new HashMap<String, String>();
		Map<String, String> UniqueKey_Agreement = new HashMap<String, String>();
		Map<String, String> UniqueKey_Call = new HashMap<String, String>();
		Map<String, String> UniqueKey_CustomerComplain = new HashMap<String, String>();
		Map<String, String> UniqueKey_EntityDetails = new HashMap<String, String>();
		Map<String, String> UniqueKey_Events = new HashMap<String, String>();
		Map<String, String> UniqueKey_Financial = new HashMap<String, String>();
		Map<String, String> UniqueKey_Gaurantor = new HashMap<String, String>();
		Map<String, String> UniqueKey_Insurance = new HashMap<String, String>();
		Map<String, String> UniqueKey_LegalViolation = new HashMap<String, String>();
		Map<String, String> UniqueKey_Lenders = new HashMap<String, String>();
		Map<String, String> UniqueKey_ExternalMails = new HashMap<String, String>();
		Map<String, String> UniqueKey_Marketing = new HashMap<String, String>();
		Map<String, String> UniqueKey_MysteryReview = new HashMap<String, String>();
		Map<String, String> UniqueKey_Owners = new HashMap<String, String>();
		Map<String, String> UniqueKey_RealEstate = new HashMap<String, String>();
		Map<String, String> UniqueKey_Remark = new HashMap<String, String>();
		Map<String, String> UniqueKey_Renewal = new HashMap<String, String>();
		Map<String, String> UniqueKey_Task = new HashMap<String, String>();
		Map<String, String> UniqueKey_Territory = new HashMap<String, String>();
		Map<String, String> UniqueKey_Training = new HashMap<String, String>();
		Map<String, String> UniqueKey_ContractSigning = new HashMap<String, String>();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			boolean createUser = false;
			String Franchisee = null;
			UserCreation uc = new UserCreation();
			createUser = uc.createCorpUser(driver, config, dataSet, testCaseId, UniqueKey_CorpUser);
			if (createUser)
				Franchisee = AddLocation(driver, dataSet, config, UniqueKey_Franchisee, testCaseId);
			boolean fill = fillallSubTabInfo(driver, dataSet, config, testCaseId, UniqueKey_CorpUser,
					UniqueKey_Employees, UniqueKey_Agreement, UniqueKey_Call, UniqueKey_CustomerComplain,
					UniqueKey_EntityDetails, UniqueKey_Events, UniqueKey_Financial, UniqueKey_Gaurantor,
					UniqueKey_Insurance, UniqueKey_LegalViolation, UniqueKey_Lenders, UniqueKey_ExternalMails,
					UniqueKey_Marketing, UniqueKey_MysteryReview, UniqueKey_Owners, UniqueKey_RealEstate,
					UniqueKey_Remark, UniqueKey_Renewal, UniqueKey_Task, UniqueKey_Territory, UniqueKey_Training,
					UniqueKey_ContractSigning);

			boolean update = updateAllSubTabInfo(driver, dataSet, config, testCaseId, UniqueKey_CorpUser,
					UniqueKey_Employees, UniqueKey_Agreement, UniqueKey_Call, UniqueKey_CustomerComplain,
					UniqueKey_EntityDetails, UniqueKey_Events, UniqueKey_Financial, UniqueKey_Gaurantor,
					UniqueKey_Insurance, UniqueKey_LegalViolation, UniqueKey_Lenders, UniqueKey_ExternalMails,
					UniqueKey_Marketing, UniqueKey_MysteryReview, UniqueKey_Owners, UniqueKey_RealEstate,
					UniqueKey_Remark, UniqueKey_Renewal, UniqueKey_Task, UniqueKey_Territory, UniqueKey_Training,
					UniqueKey_ContractSigning);
			if (Franchisee != null && fill && update) {

				UniqueKey_Call.remove("referenceId");
				UniqueKey_Call.remove("parentReferenceId");
				UniqueKey_Call.remove("timeAdded");
				UniqueKey_Franchisee.put("calldate", UniqueKey_Call.get("date"));
				ArrayList<String> listItems = new ArrayList<String>(UniqueKey_Call.values());
				// Status =
				// fc.utobj().assertPageSourceWithMultipleRecords(driver,
				// listItems);
				boolean Status = ValidateForExportCall(driver, config, listItems, DataServicePage.Call,
						UniqueKey_Franchisee, UniqueKey_Call.get("subject"));
				if (!Status)
					fc.utobj().throwsException("Call is not found on view page " + testCaseId);

				fc.loginpage().login(driver);
				fc.infomgr().infomgr_common().InfoMgrCorporateLocations(driver);

				InfoMgrCorporateLocationsPage objFranPage = new InfoMgrCorporateLocationsPage(driver);
				try {
					fc.utobj().clickElement(driver, objFranPage.lnkShowAll);
					fc.utobj().clickPartialLinkText(driver, UniqueKey_Franchisee.get("franchiseeName"));
				} catch (Exception ex) {
					fc.utobj().throwsException("Franchise Location not found in View Page" + testCaseId);
				}

				boolean boolAddFranFlag = fc.utobj().assertPageSource(driver,
						UniqueKey_Franchisee.get("franchiseeName"));
				if (boolAddFranFlag == true) {
					Reporter.log("Corporate Location addition successfull !!! ");
					// verifing contract signing
					fc.utobj().clickPartialLinkText(driver, "Contract Signing");
					UniqueKey_ContractSigning.remove("referenceId");
					UniqueKey_ContractSigning.remove("parentReferenceId");
					listItems = new ArrayList<String>(UniqueKey_ContractSigning.values());
					Status = fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);
					if (!Status)
						fc.utobj().throwsException("Contact Signing is not found on view page " + testCaseId);

					fc.utobj().clickPartialLinkText(driver, "Agreement");
					UniqueKey_Agreement.remove("referenceId");
					UniqueKey_Agreement.remove("parentReferenceId");
					listItems = new ArrayList<String>(UniqueKey_Agreement.values());

					Status = fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);
					if (!Status)
						fc.utobj().throwsException("Agreement is not found on view page " + testCaseId);

					fc.utobj().clickPartialLinkText(driver, "Customer Complaints");
					UniqueKey_CustomerComplain.remove("referenceId");
					UniqueKey_CustomerComplain.remove("parentReferenceId");
					listItems = new ArrayList<String>(UniqueKey_CustomerComplain.values());
					Status = fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);
					if (!Status)
						fc.utobj().throwsException("Customer Complaints is not found on view page " + testCaseId);

					fc.utobj().clickPartialLinkText(driver, "Entity Details");
					UniqueKey_EntityDetails.remove("referenceId");
					UniqueKey_EntityDetails.remove("parentReferenceId");
					listItems = new ArrayList<String>(UniqueKey_EntityDetails.values());
					Status = fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);
					if (!Status)
						fc.utobj().throwsException("Entity Details is not found on view page " + testCaseId);

					fc.utobj().clickElement(driver,
							driver.findElement(By.xpath(".//*[@id='fimUl']//a[contains(text(),'Training')]")));
					UniqueKey_Training.remove("referenceId");
					UniqueKey_Training.remove("parentReferenceId");
					listItems = new ArrayList<String>(UniqueKey_Training.values());
					Status = fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);
					if (!Status)
						fc.utobj().throwsException("Training is not found on view page " + testCaseId);

					fc.utobj().clickPartialLinkText(driver, "Territory");
					UniqueKey_Territory.remove("referenceId");
					UniqueKey_Territory.remove("parentReferenceId");
					listItems = new ArrayList<String>(UniqueKey_Territory.values());
					Status = fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);
					if (!Status)
						fc.utobj().throwsException("Territory is not found on view page " + testCaseId);

					fc.utobj().clickPartialLinkText(driver, "Renewal");
					UniqueKey_Renewal.remove("referenceId");
					UniqueKey_Renewal.remove("parentReferenceId");
					listItems = new ArrayList<String>(UniqueKey_Renewal.values());
					Status = fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);
					if (!Status)
						fc.utobj().throwsException("Renewal is not found on view page " + testCaseId);

					fc.utobj().clickPartialLinkText(driver, "Real Estate");
					UniqueKey_RealEstate.remove("referenceId");
					UniqueKey_RealEstate.remove("parentReferenceId");
					listItems = new ArrayList<String>(UniqueKey_RealEstate.values());
					Status = fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);
					if (!Status)
						fc.utobj().throwsException("Real Estate is not found on view page " + testCaseId);
					try {
						fc.utobj().clickPartialLinkText(driver, DataServicePage.CenterInfo);
						fc.utobj().clickPartialLinkText(driver, UniqueKey_Call.get("subject"));
					} catch (Exception e) {

						fc.utobj().throwsException("Call Subject Not Found on View Page");

					}
					fc.commonMethods().switch_cboxIframe_frameId(driver);

				} else {
					fc.utobj().throwsException("Corporate Location addition failed !!!");
				}
			}
		} catch (Exception ex) {
			fc.utobj().quitBrowserOnCatch(driver, ex, testCaseId);
		}
	}

	private boolean updateAllSubTabInfo(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> CorpUser, Map<String, String> UniqueKey_Employees,
			Map<String, String> UniqueKey_Agreement, Map<String, String> UniqueKey_Call,
			Map<String, String> UniqueKey_CustomerComplain, Map<String, String> UniqueKey_EntityDetails,
			Map<String, String> UniqueKey_Events, Map<String, String> UniqueKey_Financial,
			Map<String, String> UniqueKey_Gaurantor, Map<String, String> UniqueKey_Insurance,
			Map<String, String> UniqueKey_LegalViolation, Map<String, String> UniqueKey_Lenders,
			Map<String, String> UniqueKey_ExternalMails, Map<String, String> UniqueKey_Marketing,
			Map<String, String> UniqueKey_MysteryReview, Map<String, String> UniqueKey_Owners,
			Map<String, String> UniqueKey_RealEstate, Map<String, String> UniqueKey_Remark,
			Map<String, String> UniqueKey_Renewal, Map<String, String> UniqueKey_Task,
			Map<String, String> UniqueKey_Territory, Map<String, String> UniqueKey_Training,
			Map<String, String> UniqueKey_ContractSigning) throws Exception {

		boolean status = false;
		UniqueKey_ContractSigning.put("referenceId",
				DataServicePage.ReferenceStatus_keys.get(testCaseId + "Contract Signing"));
		UniqueKey_ContractSigning.put("fimDdContractRecievedSigned",
				fc.utobj().generateCurrentDatewithformat("yyyy-MM-dd"));
		status = ActivityPerform(driver, dataSet, config, UniqueKey_ContractSigning, DataServicePage.InfoMgr,
				DataServicePage.ContractSigning, DataServicePage.Update, DataServicePage.updateDiv, testCaseId);
		if (!status) {
			fc.utobj().throwsException("ContractSigning is not updated in " + testCaseId);
		}

		UniqueKey_Agreement.put("referenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Agreement"));
		UniqueKey_Agreement.put("fimDdApprovedDate", fc.utobj().generateCurrentDatewithformat("yyyy-MM-dd"));
		status = ActivityPerform(driver, dataSet, config, UniqueKey_Agreement, DataServicePage.InfoMgr,
				DataServicePage.Agreement, DataServicePage.Update, DataServicePage.updateDiv, testCaseId);
		if (!status) {
			fc.utobj().throwsException("Agreement is not updated in " + testCaseId);
		}

		UniqueKey_Call.put("referenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Call"));
		UniqueKey_Call.put("date", fc.utobj().generateCurrentDatewithformat("yyyy-MM-dd"));
		UniqueKey_Call.put("timeAdded", "03:00Z");
		UniqueKey_Call.put("callStatus", "Contacted");
		UniqueKey_Call.put("callType", "Outbound Call");
		UniqueKey_Call.put("subject", "subjent" + fc.utobj().generateRandomNumber());
		status = ActivityPerform(driver, dataSet, config, UniqueKey_Call, DataServicePage.InfoMgr, DataServicePage.Call,
				DataServicePage.Update, DataServicePage.updateDiv, testCaseId);
		if (!status) {
			fc.utobj().throwsException("Call is not updated in " + testCaseId);
		}

		UniqueKey_CustomerComplain.put("referenceId",
				DataServicePage.ReferenceStatus_keys.get(testCaseId + "Customer Complaints"));
		UniqueKey_CustomerComplain.put("fimDdComplaintDate", fc.utobj().generateCurrentDatewithformat("yyyy-MM-dd"));
		UniqueKey_CustomerComplain.put("fimTtComplaint", "Complaint" + fc.utobj().generateRandomNumber());
		UniqueKey_CustomerComplain.put("fimDdIncidentDate", fc.utobj().generateCurrentDatewithformat("yyyy-MM-dd"));
		UniqueKey_CustomerComplain.put("fimTtComplaintBy", "complainId" + fc.utobj().generateRandomNumber());
		status = ActivityPerform(driver, dataSet, config, UniqueKey_CustomerComplain, DataServicePage.InfoMgr,
				DataServicePage.CustomerComplaints, DataServicePage.Update, DataServicePage.updateDiv, testCaseId);
		if (!status) {
			fc.utobj().throwsException("Customer Complain is not updated in " + testCaseId);
		}

		UniqueKey_Employees.put("referenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Employees"));
		UniqueKey_Employees.put("firstName", "Fname" + fc.utobj().generateRandomNumber());
		UniqueKey_Employees.put("lastName", "Lname" + fc.utobj().generateRandomNumber());
		status = ActivityPerform(driver, dataSet, config, UniqueKey_Employees, DataServicePage.InfoMgr,
				DataServicePage.Employees, DataServicePage.Update, DataServicePage.updateDiv, testCaseId);
		if (!status) {
			fc.utobj().throwsException("Employees is not updated in " + testCaseId);
		}

		UniqueKey_EntityDetails.put("referenceId",
				DataServicePage.ReferenceStatus_keys.get(testCaseId + "Entity Details"));
		UniqueKey_EntityDetails.put("fimTtEntityName", "Name" + fc.utobj().generateRandomNumber());
		status = ActivityPerform(driver, dataSet, config, UniqueKey_EntityDetails, DataServicePage.InfoMgr,
				DataServicePage.EntityDetails, DataServicePage.Update, DataServicePage.updateDiv, testCaseId);
		if (!status) {
			fc.utobj().throwsException("Entity Details is not updated in " + testCaseId);
		}
		UniqueKey_Events.put("referenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Events"));
		UniqueKey_Events.put("fimDdEventDate", fc.utobj().generateCurrentDatewithformat("yyyy-MM-dd"));
		UniqueKey_Events.put("fimTtEventAuthor", "Oraganizer" + fc.utobj().generateRandomNumber());
		status = ActivityPerform(driver, dataSet, config, UniqueKey_Events, DataServicePage.InfoMgr,
				DataServicePage.Events, DataServicePage.Update, DataServicePage.updateDiv, testCaseId);
		if (!status) {
			fc.utobj().throwsException("Events is not updated in " + testCaseId);
		}
		UniqueKey_Financial.put("referenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Financial"));
		UniqueKey_Financial.put("fimTtFirstName", "Fname" + fc.utobj().generateRandomNumber());
		status = ActivityPerform(driver, dataSet, config, UniqueKey_Financial, DataServicePage.InfoMgr,
				DataServicePage.Financial, DataServicePage.Update, DataServicePage.updateDiv, testCaseId);
		if (!status) {
			fc.utobj().throwsException("Financial is not updated in " + testCaseId);
		}

		UniqueKey_Gaurantor.put("referenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Guarantors"));
		UniqueKey_Gaurantor.put("fimTtFirstName", "Fname" + fc.utobj().generateRandomNumber());
		status = ActivityPerform(driver, dataSet, config, UniqueKey_Gaurantor, DataServicePage.InfoMgr,
				DataServicePage.Guarantors, DataServicePage.Update, DataServicePage.updateDiv, testCaseId);
		if (!status) {
			fc.utobj().throwsException("Gaurantor is not updated in " + testCaseId);
		}
		UniqueKey_Insurance.put("referenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Insurance"));
		UniqueKey_Insurance.put("fimTtInsuranceCompanyName", "CompanyName" + fc.utobj().generateRandomNumber());
		status = ActivityPerform(driver, dataSet, config, UniqueKey_Insurance, DataServicePage.InfoMgr,
				DataServicePage.Insurance, DataServicePage.Update, DataServicePage.updateDiv, testCaseId);
		if (!status) {
			fc.utobj().throwsException("Insurance is not updated in " + testCaseId);
		}
		UniqueKey_LegalViolation.put("referenceId",
				DataServicePage.ReferenceStatus_keys.get(testCaseId + "Legal Violation"));
		UniqueKey_LegalViolation.put("fimTaSummary",
				"Summary is not defined pls refer summary" + fc.utobj().generateRandomNumber());
		status = ActivityPerform(driver, dataSet, config, UniqueKey_LegalViolation, DataServicePage.InfoMgr,
				DataServicePage.LegalViolation, DataServicePage.Update, DataServicePage.updateDiv, testCaseId);
		if (!status) {
			fc.utobj().throwsException("Legal Violation is not updated in " + testCaseId);
		}
		UniqueKey_Lenders.put("referenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Lenders"));
		UniqueKey_Lenders.put("fimTtFirstName", "Fname" + fc.utobj().generateRandomNumber());
		status = ActivityPerform(driver, dataSet, config, UniqueKey_Lenders, DataServicePage.InfoMgr,
				DataServicePage.Lenders, DataServicePage.Update, DataServicePage.updateDiv, testCaseId);
		if (!status) {
			fc.utobj().throwsException("Lenders is not updated in " + testCaseId);
		}

		UniqueKey_Marketing.put("referenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Marketing"));
		UniqueKey_Marketing.put("fimTtFirstName", "Fname" + fc.utobj().generateRandomNumber());
		status = ActivityPerform(driver, dataSet, config, UniqueKey_Marketing, DataServicePage.InfoMgr,
				DataServicePage.Marketing, DataServicePage.Update, DataServicePage.updateDiv, testCaseId);
		if (!status) {
			fc.utobj().throwsException("Marketing is not updated in " + testCaseId);
		}
		UniqueKey_MysteryReview.put("referenceId",
				DataServicePage.ReferenceStatus_keys.get(testCaseId + "Mystery Review"));
		UniqueKey_MysteryReview.put("fimDdInspectionDate", fc.utobj().generateCurrentDatewithformat("yyyy-MM-dd"));
		status = ActivityPerform(driver, dataSet, config, UniqueKey_MysteryReview, DataServicePage.InfoMgr,
				DataServicePage.MysteryReview, DataServicePage.Update, DataServicePage.updateDiv, testCaseId);
		if (!status) {
			fc.utobj().throwsException("Mystery Review is not updated in " + testCaseId);
		}
		UniqueKey_Owners.put("referenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Owners"));
		UniqueKey_Owners.put("ownerType", "New");
		UniqueKey_Owners.put("lastName", "Fname" + fc.utobj().generateRandomNumber());
		UniqueKey_Owners.put("firstName", "Fname" + fc.utobj().generateRandomNumber());
		status = ActivityPerform(driver, dataSet, config, UniqueKey_Owners, DataServicePage.InfoMgr,
				DataServicePage.Owners, DataServicePage.Update, DataServicePage.updateDiv, testCaseId);
		if (!status) {
			fc.utobj().throwsException("Owners is not updated in " + testCaseId);
		}
		UniqueKey_RealEstate.put("referenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Real Estate"));
		UniqueKey_RealEstate.put("fimTtFirstName", "Fname" + fc.utobj().generateRandomNumber());
		status = ActivityPerform(driver, dataSet, config, UniqueKey_RealEstate, DataServicePage.InfoMgr,
				DataServicePage.RealEstate, DataServicePage.Update, DataServicePage.updateDiv, testCaseId);
		if (!status) {
			fc.utobj().throwsException("Real Estate is not updated in " + testCaseId);
		}
		UniqueKey_Renewal.put("referenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Renewal"));
		String date = fc.utobj().generateCurrentDatewithformat("yyyy-MM-dd");
		UniqueKey_Renewal.put("fimDdAsOf", date);
		status = ActivityPerform(driver, dataSet, config, UniqueKey_Renewal, DataServicePage.InfoMgr,
				DataServicePage.Renewal, DataServicePage.Update, DataServicePage.updateDiv, testCaseId);
		if (!status) {
			fc.utobj().throwsException("Renewal is not updated in " + testCaseId);
		}
		UniqueKey_Task.put("referenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Task"));
		UniqueKey_Task.put("assignTo", CorpUser.get("firstname") + " " + CorpUser.get("lastname"));
		UniqueKey_Task.put("status", "Completed");
		UniqueKey_Task.put("taskType", "Default");
		UniqueKey_Task.put("subject", "Assigned Task" + fc.utobj().generateRandomNumber());
		UniqueKey_Task.put("timelessTask", "Y");
		UniqueKey_Task.put("date", date);
		status = ActivityPerform(driver, dataSet, config, UniqueKey_Task, DataServicePage.InfoMgr, DataServicePage.Task,
				DataServicePage.Update, DataServicePage.updateDiv, testCaseId);
		if (!status) {
			fc.utobj().throwsException("Task is not updated in " + testCaseId);
		}
		UniqueKey_Territory.put("referenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Territory"));
		UniqueKey_Territory.put("fimTtTypeTerritory", "Exclusive");
		status = ActivityPerform(driver, dataSet, config, UniqueKey_Territory, DataServicePage.InfoMgr,
				DataServicePage.Territory, DataServicePage.Update, DataServicePage.updateDiv, testCaseId);
		if (!status) {
			fc.utobj().throwsException("Territory is not updated in " + testCaseId);
		}
		UniqueKey_Training.put("referenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Training"));
		UniqueKey_Training.put("fimTtTrainingProgram", "Training Program" + fc.utobj().generateRandomNumber());
		status = ActivityPerform(driver, dataSet, config, UniqueKey_Training, DataServicePage.InfoMgr,
				DataServicePage.Training, DataServicePage.Update, DataServicePage.updateDiv, testCaseId);
		if (!status) {
			fc.utobj().throwsException("Traning is not updated in " + testCaseId);
		} else {
			return true;
		}
		return status;
	}

	@Test(groups = "infomgr_old")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-07-03", testCaseDescription = "Add all subModule for Info Mgr", testCaseId = "TC_05_Fill_Sub_Tab_Info_Mgr")
	public void FillAllSubTabInfo() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("infomgr", testCaseId);
		Map<String, String> UniqueKey_Franchisee = new HashMap<String, String>();
		Map<String, String> UniqueKey_CorpUser = new HashMap<String, String>();
		Map<String, String> UniqueKey_Employees = new HashMap<String, String>();
		Map<String, String> UniqueKey_Agreement = new HashMap<String, String>();
		Map<String, String> UniqueKey_Call = new HashMap<String, String>();
		Map<String, String> UniqueKey_CustomerComplain = new HashMap<String, String>();
		Map<String, String> UniqueKey_EntityDetails = new HashMap<String, String>();
		Map<String, String> UniqueKey_Events = new HashMap<String, String>();
		Map<String, String> UniqueKey_Financial = new HashMap<String, String>();
		Map<String, String> UniqueKey_Gaurantor = new HashMap<String, String>();
		Map<String, String> UniqueKey_Insurance = new HashMap<String, String>();
		Map<String, String> UniqueKey_LegalViolation = new HashMap<String, String>();
		Map<String, String> UniqueKey_Lenders = new HashMap<String, String>();
		Map<String, String> UniqueKey_ExternalMails = new HashMap<String, String>();
		Map<String, String> UniqueKey_Marketing = new HashMap<String, String>();
		Map<String, String> UniqueKey_MysteryReview = new HashMap<String, String>();
		Map<String, String> UniqueKey_Owners = new HashMap<String, String>();
		Map<String, String> UniqueKey_RealEstate = new HashMap<String, String>();
		Map<String, String> UniqueKey_Remark = new HashMap<String, String>();
		Map<String, String> UniqueKey_Renewal = new HashMap<String, String>();
		Map<String, String> UniqueKey_Task = new HashMap<String, String>();
		Map<String, String> UniqueKey_Territory = new HashMap<String, String>();
		Map<String, String> UniqueKey_Training = new HashMap<String, String>();
		Map<String, String> UniqueKey_ContractSigning = new HashMap<String, String>();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			UserCreation uc = new UserCreation();
			boolean createUser = uc.createCorpUser(driver, config, dataSet, testCaseId, UniqueKey_CorpUser);
			String Franchisee = AddLocation(driver, dataSet, config, UniqueKey_Franchisee, testCaseId);
			boolean fill = fillallSubTabInfo(driver, dataSet, config, testCaseId, UniqueKey_CorpUser,
					UniqueKey_Employees, UniqueKey_Agreement, UniqueKey_Call, UniqueKey_CustomerComplain,
					UniqueKey_EntityDetails, UniqueKey_Events, UniqueKey_Financial, UniqueKey_Gaurantor,
					UniqueKey_Insurance, UniqueKey_LegalViolation, UniqueKey_Lenders, UniqueKey_ExternalMails,
					UniqueKey_Marketing, UniqueKey_MysteryReview, UniqueKey_Owners, UniqueKey_RealEstate,
					UniqueKey_Remark, UniqueKey_Renewal, UniqueKey_Task, UniqueKey_Territory, UniqueKey_Training,
					UniqueKey_ContractSigning);

			if (Franchisee != null && fill) {

				UniqueKey_Call.remove("referenceId");
				UniqueKey_Call.remove("parentReferenceId");
				UniqueKey_Call.remove("timeAdded");
				UniqueKey_Franchisee.put("calldate", UniqueKey_Call.get("date"));
				ArrayList<String> listItems = new ArrayList<String>(UniqueKey_Call.values());
				// Status =
				// fc.utobj().assertPageSourceWithMultipleRecords(driver,
				// listItems);
				boolean Status = ValidateForExportCall(driver, config, listItems, DataServicePage.Call,
						UniqueKey_Franchisee, UniqueKey_Call.get("subject"));
				if (!Status)
					fc.utobj().throwsException("Call is not found on view page " + testCaseId);

				fc.loginpage().login(driver);
				fc.infomgr().infomgr_common().InfoMgrCorporateLocations(driver);

				InfoMgrCorporateLocationsPage objFranPage = new InfoMgrCorporateLocationsPage(driver);
				try {
					fc.utobj().clickElement(driver, objFranPage.lnkShowAll);
					fc.utobj().clickPartialLinkText(driver, UniqueKey_Franchisee.get("franchiseeName"));
				} catch (Exception ex) {
					fc.utobj().throwsException("Franchise Location not found in View Page" + testCaseId);
				}

				boolean boolAddFranFlag = fc.utobj().assertPageSource(driver,
						UniqueKey_Franchisee.get("franchiseeName"));
				if (boolAddFranFlag == true) {
					Reporter.log("Corporate Location addition successfull !!! ");
					// verifing contract signing
					fc.utobj().clickPartialLinkText(driver, "Contract Signing");
					UniqueKey_ContractSigning.remove("referenceId");
					UniqueKey_ContractSigning.remove("parentReferenceId");
					listItems = new ArrayList<String>(UniqueKey_ContractSigning.values());
					Status = fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);
					if (!Status)
						fc.utobj().throwsException("Contact Signing is not found on view page " + testCaseId);

					fc.utobj().clickPartialLinkText(driver, "Agreement");
					UniqueKey_Agreement.remove("referenceId");
					UniqueKey_Agreement.remove("parentReferenceId");
					listItems = new ArrayList<String>(UniqueKey_Agreement.values());

					Status = fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);
					if (!Status)
						fc.utobj().throwsException("Agreement is not found on view page " + testCaseId);

					fc.utobj().clickPartialLinkText(driver, "Customer Complaints");
					UniqueKey_CustomerComplain.remove("referenceId");
					UniqueKey_CustomerComplain.remove("parentReferenceId");
					listItems = new ArrayList<String>(UniqueKey_CustomerComplain.values());
					Status = fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);
					if (!Status)
						fc.utobj().throwsException("Customer Complaints is not found on view page " + testCaseId);

					fc.utobj().clickPartialLinkText(driver, "Entity Details");
					UniqueKey_EntityDetails.remove("referenceId");
					UniqueKey_EntityDetails.remove("parentReferenceId");
					listItems = new ArrayList<String>(UniqueKey_EntityDetails.values());
					Status = fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);
					if (!Status)
						fc.utobj().throwsException("Entity Details is not found on view page " + testCaseId);

					fc.utobj().clickElement(driver,
							driver.findElement(By.xpath(".//*[@id='fimUl']//a[contains(text(),'Training')]")));
					UniqueKey_Training.remove("referenceId");
					UniqueKey_Training.remove("parentReferenceId");
					listItems = new ArrayList<String>(UniqueKey_Training.values());
					Status = fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);
					if (!Status)
						fc.utobj().throwsException("Training is not found on view page " + testCaseId);

					fc.utobj().clickPartialLinkText(driver, "Territory");
					UniqueKey_Territory.remove("referenceId");
					UniqueKey_Territory.remove("parentReferenceId");
					listItems = new ArrayList<String>(UniqueKey_Territory.values());
					Status = fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);
					if (!Status)
						fc.utobj().throwsException("Territory is not found on view page " + testCaseId);

					fc.utobj().clickPartialLinkText(driver, "Renewal");
					UniqueKey_Renewal.remove("referenceId");
					UniqueKey_Renewal.remove("parentReferenceId");
					listItems = new ArrayList<String>(UniqueKey_Renewal.values());
					Status = fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);
					if (!Status)
						fc.utobj().throwsException("Renewal is not found on view page " + testCaseId);

					fc.utobj().clickPartialLinkText(driver, "Real Estate");
					UniqueKey_RealEstate.remove("referenceId");
					UniqueKey_RealEstate.remove("parentReferenceId");
					listItems = new ArrayList<String>(UniqueKey_RealEstate.values());
					Status = fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);
					if (!Status)
						fc.utobj().throwsException("Real Estate is not found on view page " + testCaseId);

				} else {
					fc.utobj().throwsException("Corporate Location addition failed !!!");
				}
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception ex) {
			fc.utobj().quitBrowserOnCatch(driver, ex, testCaseId);
		}
	}

	public boolean ValidateForExportCall(WebDriver driver, Map<String, String> config, List<String> listVI,
			String subModule, Map<String, String> uniqueKey, String string) throws Exception {
		try {
			boolean status = false;
			fc.loginpage().login(driver);
			fc.infomgr().infomgr_common().InfoMgrExport(driver);
			fc.utobj().clickPartialLinkText(driver,
					"Export Active, Terminated, Corporate Locations and In Development Franchisees Information");
			InfoMgrExportPage pobj = new InfoMgrExportPage(driver);
			fc.utobj().clickElement(driver, pobj.chkbxActHistoryCall);
			fc.utobj().clickElement(driver, pobj.srchDataBtn);
			fc.utobj().sendKeys(driver, pobj.FranchiseId, uniqueKey.get("franchiseeName"));
			fc.utobj().sendKeys(driver, pobj.CenterName, uniqueKey.get("centerName"));
			fc.utobj().clickElement(driver, pobj.viewdata);
			status = fc.utobj().assertPageSourceWithMultipleRecords(driver, listVI);
			return status;

		} catch (Exception ex) {
			fc.utobj().quitBrowserOnCatch(driver, ex, "validate failed for " + subModule);
			return false;
		}

		// TODO Auto-generated method stub

	}

	private boolean fillallSubTabInfo(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> CorpUser, Map<String, String> UniqueKey_Employees,
			Map<String, String> UniqueKey_Agreement, Map<String, String> UniqueKey_Call,
			Map<String, String> UniqueKey_CustomerComplain, Map<String, String> UniqueKey_EntityDetails,
			Map<String, String> UniqueKey_Events, Map<String, String> UniqueKey_Financial,
			Map<String, String> UniqueKey_Gaurantor, Map<String, String> UniqueKey_Insurance,
			Map<String, String> UniqueKey_LegalViolation, Map<String, String> UniqueKey_Lenders,
			Map<String, String> UniqueKey_ExternalMails, Map<String, String> UniqueKey_Marketing,
			Map<String, String> UniqueKey_MysteryReview, Map<String, String> UniqueKey_Owners,
			Map<String, String> UniqueKey_RealEstate, Map<String, String> UniqueKey_Remark,
			Map<String, String> UniqueKey_Renewal, Map<String, String> UniqueKey_Task,
			Map<String, String> UniqueKey_Territory, Map<String, String> UniqueKey_Training,
			Map<String, String> UniqueKey_ContractSigning) throws Exception {
		boolean status = false;
		UniqueKey_ContractSigning.put("parentReferenceId",
				DataServicePage.ReferenceStatus_keys.get(testCaseId + "Franchisee"));
		UniqueKey_ContractSigning.put("fimDdContractRecievedSigned", "10/10/2014");
		status = ActivityPerform(driver, dataSet, config, UniqueKey_ContractSigning, DataServicePage.InfoMgr,
				DataServicePage.ContractSigning, DataServicePage.Create, DataServicePage.createDiv, testCaseId);
		if (!status) {
			fc.utobj().throwsException("Contract Signing is not Created in RestAPI " + testCaseId);
		}

		UniqueKey_Agreement.put("parentReferenceId",
				DataServicePage.ReferenceStatus_keys.get(testCaseId + "Franchisee"));
		UniqueKey_Agreement.put("fimDdApprovedDate", fc.utobj().generateCurrentDatewithformat("yyyy-MM-dd"));
		UniqueKey_Agreement.put("_firstagree257535676", fc.utobj().generatefutureDatewithformat("MM/dd/yyyy", 90));
		UniqueKey_Agreement.put("_firstagreerenwal1407274656", "CH");
		UniqueKey_Agreement.put("fimDdDateExecuted", fc.utobj().generateCurrentDatewithformat("yyyy-MM-dd"));
		UniqueKey_Agreement.put("fimTtStateAddendum", "Addendum" + fc.utobj().generateRandomNumber());
		status = ActivityPerform(driver, dataSet, config, UniqueKey_Agreement, DataServicePage.InfoMgr,
				DataServicePage.Agreement, DataServicePage.Create, DataServicePage.createDiv, testCaseId);
		if (!status) {
			fc.utobj().throwsException("Agreement is not Created in RestAPI " + testCaseId);
		}
		// _firstagreerenwal1407274656

		UniqueKey_Call.put("parentReferenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Franchisee"));
		UniqueKey_Call.put("date", fc.utobj().generateCurrentDatewithformat("yyyy-MM-dd"));
		UniqueKey_Call.put("timeAdded", "03:00Z");
		UniqueKey_Call.put("callStatus", "Contacted");
		UniqueKey_Call.put("callType", "Outbound Call");
		UniqueKey_Call.put("subject", "subject" + fc.utobj().generateRandomNumber());
		status = ActivityPerform(driver, dataSet, config, UniqueKey_Call, DataServicePage.InfoMgr, DataServicePage.Call,
				DataServicePage.Create, DataServicePage.createDiv, testCaseId);
		if (!status) {
			fc.utobj().throwsException("Call is not Created in RestAPI " + testCaseId);
		}

		UniqueKey_CustomerComplain.put("parentReferenceId",
				DataServicePage.ReferenceStatus_keys.get(testCaseId + "Franchisee"));
		UniqueKey_CustomerComplain.put("fimDdComplaintDate", fc.utobj().generateCurrentDatewithformat("yyyy-MM-dd"));
		UniqueKey_CustomerComplain.put("fimTtComplaint", "Complaint" + fc.utobj().generateRandomNumber());
		UniqueKey_CustomerComplain.put("fimDdIncidentDate", fc.utobj().generateCurrentDatewithformat("yyyy-MM-dd"));
		UniqueKey_CustomerComplain.put("fimTtComplaintBy", "complainId" + fc.utobj().generateRandomNumber());
		status = ActivityPerform(driver, dataSet, config, UniqueKey_CustomerComplain, DataServicePage.InfoMgr,
				DataServicePage.CustomerComplaints, DataServicePage.Create, DataServicePage.createDiv, testCaseId);
		if (!status) {
			fc.utobj().throwsException("CustomerComplain is not Created in RestAPI " + testCaseId);
		}

		UniqueKey_Employees.put("parentReferenceId",
				DataServicePage.ReferenceStatus_keys.get(testCaseId + "Franchisee"));
		UniqueKey_Employees.put("firstName", "Fname" + fc.utobj().generateRandomNumber());
		UniqueKey_Employees.put("lastName", "Lname" + fc.utobj().generateRandomNumber());
		UniqueKey_Employees.put("employeeType", "New");
		status = ActivityPerform(driver, dataSet, config, UniqueKey_Employees, DataServicePage.InfoMgr,
				DataServicePage.Employees, DataServicePage.Create, DataServicePage.createDiv, testCaseId);
		if (!status) {
			fc.utobj().throwsException("Employee is not Created in RestAPI " + testCaseId);
		}

		UniqueKey_EntityDetails.put("parentReferenceId",
				DataServicePage.ReferenceStatus_keys.get(testCaseId + "Franchisee"));
		UniqueKey_EntityDetails.put("fimTtEntityName", "Name" + fc.utobj().generateRandomNumber());

		UniqueKey_EntityDetails.put("entityType", "New");
		status = ActivityPerform(driver, dataSet, config, UniqueKey_EntityDetails, DataServicePage.InfoMgr,
				DataServicePage.EntityDetails, DataServicePage.Create, DataServicePage.createDiv, testCaseId);
		if (!status) {
			fc.utobj().throwsException("Entity Details is not Created in RestAPI " + testCaseId);
		}
		UniqueKey_Events.put("parentReferenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Franchisee"));
		UniqueKey_Events.put("fimDdEventDate", fc.utobj().generateCurrentDatewithformat("yyyy-MM-dd"));
		UniqueKey_Events.put("fimTtEventAuthor", "Oraganizer" + fc.utobj().generateRandomNumber());
		status = ActivityPerform(driver, dataSet, config, UniqueKey_Events, DataServicePage.InfoMgr,
				DataServicePage.Events, DataServicePage.Create, DataServicePage.createDiv, testCaseId);
		if (!status) {
			fc.utobj().throwsException("Events is not Created in RestAPI " + testCaseId);
		}
		UniqueKey_Financial.put("parentReferenceId",
				DataServicePage.ReferenceStatus_keys.get(testCaseId + "Franchisee"));
		UniqueKey_Financial.put("fimTtFirstName", "Fname" + fc.utobj().generateRandomNumber());
		status = ActivityPerform(driver, dataSet, config, UniqueKey_Financial, DataServicePage.InfoMgr,
				DataServicePage.Financial, DataServicePage.Create, DataServicePage.createDiv, testCaseId);
		if (!status) {
			fc.utobj().throwsException("Financial is not Created in RestAPI " + testCaseId);
		}

		UniqueKey_Gaurantor.put("parentReferenceId",
				DataServicePage.ReferenceStatus_keys.get(testCaseId + "Franchisee"));
		UniqueKey_Gaurantor.put("fimTtFirstName", "Fname" + fc.utobj().generateRandomNumber());
		status = ActivityPerform(driver, dataSet, config, UniqueKey_Gaurantor, DataServicePage.InfoMgr,
				DataServicePage.Guarantors, DataServicePage.Create, DataServicePage.createDiv, testCaseId);
		if (!status) {
			fc.utobj().throwsException("Gaurantor is not Created in RestAPI " + testCaseId);
		}
		UniqueKey_Insurance.put("parentReferenceId",
				DataServicePage.ReferenceStatus_keys.get(testCaseId + "Franchisee"));
		UniqueKey_Insurance.put("fimTtInsuranceCompanyName", "CompanyName" + fc.utobj().generateRandomNumber());
		status = ActivityPerform(driver, dataSet, config, UniqueKey_Insurance, DataServicePage.InfoMgr,
				DataServicePage.Insurance, DataServicePage.Create, DataServicePage.createDiv, testCaseId);
		if (!status) {
			fc.utobj().throwsException("Insurance is not Created in RestAPI " + testCaseId);
		}
		UniqueKey_LegalViolation.put("parentReferenceId",
				DataServicePage.ReferenceStatus_keys.get(testCaseId + "Franchisee"));
		UniqueKey_LegalViolation.put("fimTaSummary",
				"Summary is not defined pls refer summary" + fc.utobj().generateRandomNumber());
		status = ActivityPerform(driver, dataSet, config, UniqueKey_LegalViolation, DataServicePage.InfoMgr,
				DataServicePage.LegalViolation, DataServicePage.Create, DataServicePage.createDiv, testCaseId);
		if (!status) {
			fc.utobj().throwsException("Leagal vioalation is not Created in RestAPI " + testCaseId);
		}
		UniqueKey_Lenders.put("parentReferenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Franchisee"));
		UniqueKey_Lenders.put("fimTtFirstName", "Fname" + fc.utobj().generateRandomNumber());
		status = ActivityPerform(driver, dataSet, config, UniqueKey_Lenders, DataServicePage.InfoMgr,
				DataServicePage.Lenders, DataServicePage.Create, DataServicePage.createDiv, testCaseId);
		if (!status) {
			fc.utobj().throwsException("Lenders is not Created in RestAPI " + testCaseId);
		}

		UniqueKey_ExternalMails.put("parentReferenceId",
				DataServicePage.ReferenceStatus_keys.get(testCaseId + "Franchisee"));
		UniqueKey_ExternalMails.put("mailFrom", "test@franconnect.net");
		UniqueKey_ExternalMails.put("mailCc", "test@franconnect.net");
		UniqueKey_ExternalMails.put("mailBcc", "test@franconnect.net");
		UniqueKey_ExternalMails.put("mailSubject", "Test Mail");
		UniqueKey_ExternalMails.put("mailMessage", "hello this is test");
		status = ActivityPerform(driver, dataSet, config, UniqueKey_ExternalMails, DataServicePage.InfoMgr,
				DataServicePage.ExternalMail, DataServicePage.Create, DataServicePage.createDiv, testCaseId);
		if (!status) {
			fc.utobj().throwsException("External Mails is not Created in RestAPI " + testCaseId);
		}
		UniqueKey_Marketing.put("parentReferenceId",
				DataServicePage.ReferenceStatus_keys.get(testCaseId + "Franchisee"));
		UniqueKey_Marketing.put("fimTtFirstName", "Fname" + fc.utobj().generateRandomNumber());
		status = ActivityPerform(driver, dataSet, config, UniqueKey_Marketing, DataServicePage.InfoMgr,
				DataServicePage.Marketing, DataServicePage.Create, DataServicePage.createDiv, testCaseId);
		if (!status) {
			fc.utobj().throwsException("Marketing is not Created in RestAPI " + testCaseId);
		}
		UniqueKey_MysteryReview.put("parentReferenceId",
				DataServicePage.ReferenceStatus_keys.get(testCaseId + "Franchisee"));
		UniqueKey_MysteryReview.put("fimDdInspectionDate", fc.utobj().generateCurrentDatewithformat("yyyy-MM-dd"));
		status = ActivityPerform(driver, dataSet, config, UniqueKey_MysteryReview, DataServicePage.InfoMgr,
				DataServicePage.MysteryReview, DataServicePage.Create, DataServicePage.createDiv, testCaseId);
		if (!status) {
			fc.utobj().throwsException("MysteryReview is not Created in RestAPI " + testCaseId);
		}
		UniqueKey_Owners.put("parentReferenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Franchisee"));
		UniqueKey_Owners.put("ownerType", "New");
		UniqueKey_Owners.put("lastName", "Fname" + fc.utobj().generateRandomNumber());
		UniqueKey_Owners.put("firstName", "Fname" + fc.utobj().generateRandomNumber());
		status = ActivityPerform(driver, dataSet, config, UniqueKey_Owners, DataServicePage.InfoMgr,
				DataServicePage.Owners, DataServicePage.Create, DataServicePage.createDiv, testCaseId);
		if (!status) {
			fc.utobj().throwsException("Owners is not Created in RestAPI " + testCaseId);
		}
		UniqueKey_RealEstate.put("parentReferenceId",
				DataServicePage.ReferenceStatus_keys.get(testCaseId + "Franchisee"));
		UniqueKey_RealEstate.put("fimTtFirstName", "Fname" + fc.utobj().generateRandomNumber());
		status = ActivityPerform(driver, dataSet, config, UniqueKey_RealEstate, DataServicePage.InfoMgr,
				DataServicePage.RealEstate, DataServicePage.Create, DataServicePage.createDiv, testCaseId);
		if (!status) {
			fc.utobj().throwsException("Real Estate is not Created in RestAPI " + testCaseId);
		}
		UniqueKey_Remark.put("parentReferenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Franchisee"));
		UniqueKey_Remark.put("remarks", "Remarks has been added");
		UniqueKey_Remark.put("fimTtFirstName", "Fname" + fc.utobj().generateRandomNumber());
		status = ActivityPerform(driver, dataSet, config, UniqueKey_Remark, DataServicePage.InfoMgr,
				DataServicePage.Remark, DataServicePage.Create, DataServicePage.createDiv, testCaseId);
		if (!status) {
			fc.utobj().throwsException("Remark is not Created in RestAPI " + testCaseId);
		}
		UniqueKey_Renewal.put("parentReferenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Franchisee"));
		String date = fc.utobj().generateCurrentDatewithformat("yyyy-MM-dd");
		UniqueKey_Renewal.put("fimDdAsOf", date);
		status = ActivityPerform(driver, dataSet, config, UniqueKey_Renewal, DataServicePage.InfoMgr,
				DataServicePage.Renewal, DataServicePage.Create, DataServicePage.createDiv, testCaseId);
		if (!status) {
			fc.utobj().throwsException("Renewal is not Created in RestAPI " + testCaseId);
		}
		UniqueKey_Task.put("parentReferenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Franchisee"));
		UniqueKey_Task.put("assignTo", CorpUser.get("firstname") + " " + CorpUser.get("lastname"));
		UniqueKey_Task.put("status", "Completed");
		UniqueKey_Task.put("startTime", "01:00Z");
		UniqueKey_Task.put("endTime", "11:00Z");
		UniqueKey_Task.put("taskType", "Default");
		UniqueKey_Task.put("subject", "Assigned Task" + fc.utobj().generateRandomNumber());
		UniqueKey_Task.put("timelessTask", "Y");
		UniqueKey_Task.put("date", date);
		status = ActivityPerform(driver, dataSet, config, UniqueKey_Task, DataServicePage.InfoMgr, DataServicePage.Task,
				DataServicePage.Create, DataServicePage.createDiv, testCaseId);
		if (!status) {
			fc.utobj().throwsException("Task is not Created in RestAPI " + testCaseId);
		}
		UniqueKey_Territory.put("parentReferenceId",
				DataServicePage.ReferenceStatus_keys.get(testCaseId + "Franchisee"));
		UniqueKey_Territory.put("fimTtTypeTerritory", "Exclusive");
		UniqueKey_Territory.put("fimTaNotes", "Territory");
		status = ActivityPerform(driver, dataSet, config, UniqueKey_Territory, DataServicePage.InfoMgr,
				DataServicePage.Territory, DataServicePage.Create, DataServicePage.createDiv, testCaseId);
		if (!status) {
			fc.utobj().throwsException("Territory is not Created in RestAPI " + testCaseId);
		}
		UniqueKey_Training.put("parentReferenceId",
				DataServicePage.ReferenceStatus_keys.get(testCaseId + "Franchisee"));
		UniqueKey_Training.put("fimTtTrainingProgram", "Training Program" + fc.utobj().generateRandomNumber());
		status = ActivityPerform(driver, dataSet, config, UniqueKey_Training, DataServicePage.InfoMgr,
				DataServicePage.Training, DataServicePage.Create, DataServicePage.createDiv, testCaseId);
		if (!status) {
			fc.utobj().throwsException("Training is not Created in RestAPI " + testCaseId);
		} else {
			return true;
		}
		return status;
	}

	public String AddLocation(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			Map<String, String> UniqueKey_Franchisee, String testCaseId) {
		try {
			driver = fc.loginpage().login(driver);
			String RegionName = "Reg" + fc.utobj().generateRandomNumber();
			String storeType = "store" + fc.utobj().generateRandomNumber();
			AdminAreaRegionAddAreaRegionPageTest p1 = new AdminAreaRegionAddAreaRegionPageTest();
			p1.addAreaRegion(driver, RegionName);
			AdminConfigurationConfigureStoreTypePageTest p2 = new AdminConfigurationConfigureStoreTypePageTest();
			storeType = p2.addStoreType(driver, storeType);
			UniqueKey_Franchisee.put("status", "Yes");
			UniqueKey_Franchisee.put("franchiseeName", "FIM" + fc.utobj().generateRandomNumber());
			UniqueKey_Franchisee.put("centerName", "Center" + fc.utobj().generateRandomNumber());
			UniqueKey_Franchisee.put("areaID", RegionName);
			UniqueKey_Franchisee.put("openingDate", fc.utobj().generateCurrentDatewithformat("yyyy-MM-dd"));
			UniqueKey_Franchisee.put("storeTypeId", storeType);
			UniqueKey_Franchisee.put("address", "address");
			UniqueKey_Franchisee.put("address2", "address2");
			UniqueKey_Franchisee.put("city", "NYC");
			UniqueKey_Franchisee.put("country", "USA");
			UniqueKey_Franchisee.put("storeEmail", "test@franconnect.net");
			UniqueKey_Franchisee.put("emailID", "ravi.pal@franconnect.com");
			UniqueKey_Franchisee.put("state", "Alaska");
			UniqueKey_Franchisee.put("storePhone", "1122334455");
			UniqueKey_Franchisee.put("entityDetail", "Individual");
			UniqueKey_Franchisee.put("ownerType", "New");
			UniqueKey_Franchisee.put("division", "div" + fc.utobj().generateRandomNumber());
			UniqueKey_Franchisee.put("firstName", "Fname" + fc.utobj().generateRandomNumber());
			UniqueKey_Franchisee.put("lastName", "Lname" + fc.utobj().generateRandomNumber());
			driver.navigate().to(config.get("buildUrl") + DataServicePage.restURL);
			boolean CreateFranchiseLocation = ActivityPerform(driver, dataSet, config, UniqueKey_Franchisee,
					DataServicePage.InfoMgr, DataServicePage.Franchisee, DataServicePage.Create,
					DataServicePage.createDiv, testCaseId);
			return DataServicePage.ReferenceStatus_keys.get(testCaseId + "Franchisee");
		} catch (Exception ex) {
			return null;
		}
	}

	public boolean CreateAgreementRestAPI(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey_Agreement) throws Exception {
		boolean status = false;

		try {
			UniqueKey_Agreement.put("parentReferenceId",
					DataServicePage.ReferenceStatus_keys.get(testCaseId + "Franchisee"));
			UniqueKey_Agreement.put("fimDdApprovedDate", fc.utobj().generateCurrentDatewithformat("yyyy-MM-dd"));
			UniqueKey_Agreement.put("_firstagree257535676", fc.utobj().generatefutureDatewithformat("yyyy-MM-dd", 90));
			UniqueKey_Agreement.put("_firstagreerenwal1407274656", "CH");
			UniqueKey_Agreement.put("fimDdDateExecuted", fc.utobj().generateCurrentDatewithformat("yyyy-MM-dd"));
			UniqueKey_Agreement.put("fimTtStateAddendum", "Addendum" + fc.utobj().generateRandomNumber());
			status = ActivityPerform(driver, dataSet, config, UniqueKey_Agreement, DataServicePage.InfoMgr,
					DataServicePage.Agreement, DataServicePage.Create, DataServicePage.createDiv, testCaseId);
			if (!status) {
				fc.utobj().throwsException("Agreement is not Created in RestAPI " + testCaseId);
			}
			return status;
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
		return status;
	}

	public boolean CreateCallRestAPI(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey_Call) throws Exception {
		boolean status = false;
		try {
			UniqueKey_Call.put("parentReferenceId",
					DataServicePage.ReferenceStatus_keys.get(testCaseId + "Franchisee"));
			UniqueKey_Call.put("date", fc.utobj().generateCurrentDatewithformat("yyyy-MM-dd"));
			UniqueKey_Call.put("timeAdded", "03:00Z");
			UniqueKey_Call.put("callStatus", "Contacted");
			UniqueKey_Call.put("callType", "Outbound Call");
			UniqueKey_Call.put("subject", "subject" + fc.utobj().generateRandomNumber());
			status = ActivityPerform(driver, dataSet, config, UniqueKey_Call, DataServicePage.InfoMgr,
					DataServicePage.Call, DataServicePage.Create, DataServicePage.createDiv, testCaseId);
			if (!status) {
				fc.utobj().throwsException("Call is not Created in RestAPI " + testCaseId);
				status = false;
			}
			return status;

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
		return status;
	}

	public boolean UpdateCallRestAPI(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey_Call) throws Exception {
		boolean status = false;
		try {
			UniqueKey_Call.put("referenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Call"));
			UniqueKey_Call.put("date", fc.utobj().generateCurrentDatewithformat("yyyy-MM-dd"));
			UniqueKey_Call.put("timeAdded", "03:00Z");
			UniqueKey_Call.put("callStatus", "Contacted");
			UniqueKey_Call.put("callType", "Outbound Call");
			UniqueKey_Call.put("subject", "subjent" + fc.utobj().generateRandomNumber());
			status = ActivityPerform(driver, dataSet, config, UniqueKey_Call, DataServicePage.InfoMgr,
					DataServicePage.Call, DataServicePage.Update, DataServicePage.updateDiv, testCaseId);
			if (!status) {
				fc.utobj().throwsException("Call is not updated in " + testCaseId);
				status = false;
			}
			return status;

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
		return status;
	}

	public boolean CreateCustomerComplainRestAPI(WebDriver driver, Map<String, String> dataSet,
			Map<String, String> config, String testCaseId, Map<String, String> UniqueKey_CustomerComplain)
			throws Exception {
		boolean status = false;
		try {
			UniqueKey_CustomerComplain.put("parentReferenceId",
					DataServicePage.ReferenceStatus_keys.get(testCaseId + "Franchisee"));
			UniqueKey_CustomerComplain.put("fimDdComplaintDate",
					fc.utobj().generateCurrentDatewithformat("yyyy-MM-dd"));
			UniqueKey_CustomerComplain.put("fimTtComplaint", "Complaint" + fc.utobj().generateRandomNumber());
			UniqueKey_CustomerComplain.put("fimDdIncidentDate", fc.utobj().generateCurrentDatewithformat("yyyy-MM-dd"));
			UniqueKey_CustomerComplain.put("fimTtComplaintBy", "complainId" + fc.utobj().generateRandomNumber());
			status = ActivityPerform(driver, dataSet, config, UniqueKey_CustomerComplain, DataServicePage.InfoMgr,
					DataServicePage.CustomerComplaints, DataServicePage.Create, DataServicePage.createDiv, testCaseId);
			if (!status) {
				fc.utobj().throwsException("CustomerComplain is not Created in RestAPI " + testCaseId);
			}
			return status;

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			status = false;
		}
		return status;
	}

	public boolean UpdateCustomerComplainRestAPI(WebDriver driver, Map<String, String> dataSet,
			Map<String, String> config, String testCaseId, Map<String, String> UniqueKey_CustomerComplain)
			throws Exception {
		boolean status = false;
		try {
			UniqueKey_CustomerComplain.put("referenceId",
					DataServicePage.ReferenceStatus_keys.get(testCaseId + "Customer Complaints"));
			UniqueKey_CustomerComplain.put("fimDdComplaintDate",
					fc.utobj().generateCurrentDatewithformat("yyyy-MM-dd"));
			UniqueKey_CustomerComplain.put("fimTtComplaint", "Complaint" + fc.utobj().generateRandomNumber());
			UniqueKey_CustomerComplain.put("fimDdIncidentDate", fc.utobj().generateCurrentDatewithformat("yyyy-MM-dd"));
			UniqueKey_CustomerComplain.put("fimTtComplaintBy", "complainId" + fc.utobj().generateRandomNumber());
			status = ActivityPerform(driver, dataSet, config, UniqueKey_CustomerComplain, DataServicePage.InfoMgr,
					DataServicePage.CustomerComplaints, DataServicePage.Update, DataServicePage.updateDiv, testCaseId);
			if (!status) {
				fc.utobj().throwsException("Customer Complain is not updated in " + testCaseId);
			}
			return status;

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			status = false;
		}
		return status;
	}

	public boolean CreateEntityDetailsRestAPI(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey_EntityDetails) throws Exception {
		boolean status = false;
		try {
			UniqueKey_EntityDetails.put("parentReferenceId",
					DataServicePage.ReferenceStatus_keys.get(testCaseId + "Franchisee"));
			UniqueKey_EntityDetails.put("fimTtEntityName", "Name" + fc.utobj().generateRandomNumber());

			UniqueKey_EntityDetails.put("entityType", "New");
			status = ActivityPerform(driver, dataSet, config, UniqueKey_EntityDetails, DataServicePage.InfoMgr,
					DataServicePage.EntityDetails, DataServicePage.Create, DataServicePage.createDiv, testCaseId);
			if (!status) {
				fc.utobj().throwsException("Entity Details is not Created in RestAPI " + testCaseId);
			}
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			status = false;
		}
		return status;
	}

	public boolean UpdateEntityDetailsRestAPI(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey_EntityDetails) throws Exception {
		boolean status = false;
		try {
			UniqueKey_EntityDetails.put("referenceId",
					DataServicePage.ReferenceStatus_keys.get(testCaseId + "Entity Details"));
			UniqueKey_EntityDetails.put("fimTtEntityName", "Name" + fc.utobj().generateRandomNumber());
			status = ActivityPerform(driver, dataSet, config, UniqueKey_EntityDetails, DataServicePage.InfoMgr,
					DataServicePage.EntityDetails, DataServicePage.Update, DataServicePage.updateDiv, testCaseId);
			if (!status) {
				fc.utobj().throwsException("Entity Details is not updated in " + testCaseId);
			}
			return status;
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			status = false;
		}
		return status;
	}

	public boolean CreateEmployeeRestAPI(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey_Employees) throws Exception {
		boolean status = false;
		try {
			UniqueKey_Employees.put("parentReferenceId",
					DataServicePage.ReferenceStatus_keys.get(testCaseId + "Franchisee"));
			UniqueKey_Employees.put("firstName", "Fname" + fc.utobj().generateRandomNumber());
			UniqueKey_Employees.put("lastName", "Lname" + fc.utobj().generateRandomNumber());
			UniqueKey_Employees.put("employeeType", "New");
			status = ActivityPerform(driver, dataSet, config, UniqueKey_Employees, DataServicePage.InfoMgr,
					DataServicePage.Employees, DataServicePage.Create, DataServicePage.createDiv, testCaseId);
			if (!status) {
				fc.utobj().throwsException("Employee is not Created in RestAPI " + testCaseId);
			}
			return status;
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			status = false;
		}
		return status;
	}

	public boolean UpdateEmployeeRestAPI(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey_Employees) throws Exception {
		boolean status = false;
		try {
			UniqueKey_Employees.put("referenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Employees"));
			UniqueKey_Employees.put("firstName", "Fname" + fc.utobj().generateRandomNumber());
			UniqueKey_Employees.put("lastName", "Lname" + fc.utobj().generateRandomNumber());
			status = ActivityPerform(driver, dataSet, config, UniqueKey_Employees, DataServicePage.InfoMgr,
					DataServicePage.Employees, DataServicePage.Update, DataServicePage.updateDiv, testCaseId);
			if (!status) {
				fc.utobj().throwsException("Employees is not updated in " + testCaseId);
			}
			return status;
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			status = false;
		}
		return status;
	}

	public boolean CreateTrainingRestAPI(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey_Training) throws Exception {
		boolean status = false;
		try {
			UniqueKey_Training.put("parentReferenceId",
					DataServicePage.ReferenceStatus_keys.get(testCaseId + "Franchisee"));
			UniqueKey_Training.put("fimTtTrainingProgram", "Training Program" + fc.utobj().generateRandomNumber());
			status = ActivityPerform(driver, dataSet, config, UniqueKey_Training, DataServicePage.InfoMgr,
					DataServicePage.Training, DataServicePage.Create, DataServicePage.createDiv, testCaseId);
			if (!status) {
				fc.utobj().throwsException("Training is not Created in RestAPI " + testCaseId);
			}
			return status;
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			status = false;
		}
		return status;
	}

	public boolean UpdateTrainingRestAPI(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey_Training) throws Exception {
		boolean status = false;
		try {
			UniqueKey_Training.put("referenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Training"));
			UniqueKey_Training.put("fimTtTrainingProgram", "Training Program" + fc.utobj().generateRandomNumber());
			status = ActivityPerform(driver, dataSet, config, UniqueKey_Training, DataServicePage.InfoMgr,
					DataServicePage.Training, DataServicePage.Update, DataServicePage.updateDiv, testCaseId);
			if (!status) {
				fc.utobj().throwsException("Traning is not updated in " + testCaseId);
			}
			return status;
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			status = false;
		}
		return status;
	}

	public boolean CreateEventsRestAPI(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey_Events) throws Exception {
		boolean status = false;
		try {
			UniqueKey_Events.put("parentReferenceId",
					DataServicePage.ReferenceStatus_keys.get(testCaseId + "Franchisee"));
			UniqueKey_Events.put("fimDdEventDate", fc.utobj().generateCurrentDatewithformat("yyyy-MM-dd"));
			UniqueKey_Events.put("fimTtEventAuthor", "Oraganizer" + fc.utobj().generateRandomNumber());
			status = ActivityPerform(driver, dataSet, config, UniqueKey_Events, DataServicePage.InfoMgr,
					DataServicePage.Events, DataServicePage.Create, DataServicePage.createDiv, testCaseId);
			if (!status) {
				fc.utobj().throwsException("Events is not Created in RestAPI " + testCaseId);
			}
			return status;
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			status = false;
		}
		return status;
	}

	public boolean UpdateEventsRestAPI(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey_Events) throws Exception {
		boolean status = false;
		try {
			UniqueKey_Events.put("referenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Events"));
			UniqueKey_Events.put("fimDdEventDate", fc.utobj().generateCurrentDatewithformat("yyyy-MM-dd"));
			UniqueKey_Events.put("fimTtEventAuthor", "Oraganizer" + fc.utobj().generateRandomNumber());
			status = ActivityPerform(driver, dataSet, config, UniqueKey_Events, DataServicePage.InfoMgr,
					DataServicePage.Events, DataServicePage.Update, DataServicePage.updateDiv, testCaseId);
			if (!status) {
				fc.utobj().throwsException("Events is not updated in " + testCaseId);
			}
			return status;
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			status = false;
		}
		return status;
	}

	public boolean CreateFinancialRestAPI(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey_Financial) throws Exception {
		boolean status = false;
		try {
			UniqueKey_Financial.put("parentReferenceId",
					DataServicePage.ReferenceStatus_keys.get(testCaseId + "Franchisee"));
			UniqueKey_Financial.put("fimTtFirstName", "Fname" + fc.utobj().generateRandomNumber());
			status = ActivityPerform(driver, dataSet, config, UniqueKey_Financial, DataServicePage.InfoMgr,
					DataServicePage.Financial, DataServicePage.Create, DataServicePage.createDiv, testCaseId);
			if (!status) {
				fc.utobj().throwsException("Financial is not Created in RestAPI " + testCaseId);
			}
			return status;
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			status = false;
		}
		return status;
	}

	public boolean UpdateFinancialRestAPI(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey_Financial) throws Exception {
		boolean status = false;
		try {
			UniqueKey_Financial.put("referenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Financial"));
			UniqueKey_Financial.put("fimTtFirstName", "Fname" + fc.utobj().generateRandomNumber());
			status = ActivityPerform(driver, dataSet, config, UniqueKey_Financial, DataServicePage.InfoMgr,
					DataServicePage.Financial, DataServicePage.Update, DataServicePage.updateDiv, testCaseId);
			if (!status) {
				fc.utobj().throwsException("Financial is not updated in " + testCaseId);
				status = false;
			}
			return status;
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			status = false;
		}
		return status;
	}

	public boolean CreateGaurantorRestAPI(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> uniqueKey_Gaurantor) throws Exception {
		boolean status = false;
		try {
			uniqueKey_Gaurantor.put("parentReferenceId",
					DataServicePage.ReferenceStatus_keys.get(testCaseId + "Franchisee"));
			uniqueKey_Gaurantor.put("fimTtFirstName", "Fname" + fc.utobj().generateRandomNumber());
			status = ActivityPerform(driver, dataSet, config, uniqueKey_Gaurantor, DataServicePage.InfoMgr,
					DataServicePage.Guarantors, DataServicePage.Create, DataServicePage.createDiv, testCaseId);
			if (!status) {
				fc.utobj().throwsException("Financial is not Created in RestAPI " + testCaseId);
			}
			return status;
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			status = false;
		}
		return status;
	}

	public boolean UpdateGaurantorRestAPI(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey_Gaurantor) throws Exception {
		boolean status = false;
		try {
			UniqueKey_Gaurantor.put("referenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Guarantors"));
			UniqueKey_Gaurantor.put("fimTtFirstName", "Fname" + fc.utobj().generateRandomNumber());
			status = ActivityPerform(driver, dataSet, config, UniqueKey_Gaurantor, DataServicePage.InfoMgr,
					DataServicePage.Guarantors, DataServicePage.Update, DataServicePage.updateDiv, testCaseId);
			if (!status) {
				fc.utobj().throwsException("Gaurantor is not updated in " + testCaseId);
			}
			return status;
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			status = false;
		}
		return status;
	}

	public boolean CreateInsuranceRestAPI(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey_Insurance) throws Exception {
		boolean status = false;
		try {
			UniqueKey_Insurance.put("parentReferenceId",
					DataServicePage.ReferenceStatus_keys.get(testCaseId + "Franchisee"));
			UniqueKey_Insurance.put("fimTtInsuranceCompanyName", "CompanyName" + fc.utobj().generateRandomNumber());
			status = ActivityPerform(driver, dataSet, config, UniqueKey_Insurance, DataServicePage.InfoMgr,
					DataServicePage.Insurance, DataServicePage.Create, DataServicePage.createDiv, testCaseId);
			if (!status) {
				fc.utobj().throwsException("Insurance is not Created in RestAPI " + testCaseId);
			}
			return status;
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			status = false;
		}
		return status;
	}

	public boolean UpdateInsuranceRestAPI(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey_Insurance) throws Exception {
		boolean status = false;
		try {
			UniqueKey_Insurance.put("referenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Insurance"));
			UniqueKey_Insurance.put("fimTtInsuranceCompanyName", "CompanyName" + fc.utobj().generateRandomNumber());
			status = ActivityPerform(driver, dataSet, config, UniqueKey_Insurance, DataServicePage.InfoMgr,
					DataServicePage.Insurance, DataServicePage.Update, DataServicePage.updateDiv, testCaseId);
			if (!status) {
				fc.utobj().throwsException("Insurance is not updated in " + testCaseId);
			}
			return status;
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			status = false;
		}
		return status;
	}

	public boolean CreateLegalViolationRestAPI(WebDriver driver, Map<String, String> dataSet,
			Map<String, String> config, String testCaseId, Map<String, String> UniqueKey_LegalViolation)
			throws Exception {
		boolean status = false;
		try {
			UniqueKey_LegalViolation.put("parentReferenceId",
					DataServicePage.ReferenceStatus_keys.get(testCaseId + "Franchisee"));
			UniqueKey_LegalViolation.put("fimTaSummary",
					"Summary is not defined pls refer summary" + fc.utobj().generateRandomNumber());
			status = ActivityPerform(driver, dataSet, config, UniqueKey_LegalViolation, DataServicePage.InfoMgr,
					DataServicePage.LegalViolation, DataServicePage.Create, DataServicePage.createDiv, testCaseId);
			if (!status) {
				fc.utobj().throwsException("Leagal vioalation is not Created in RestAPI " + testCaseId);
			}
			return status;
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			status = false;
		}
		return status;
	}

	public boolean UpdateLegalViolationRestAPI(WebDriver driver, Map<String, String> dataSet,
			Map<String, String> config, String testCaseId, Map<String, String> UniqueKey_LegalViolation)
			throws Exception {
		boolean status = false;
		try {
			UniqueKey_LegalViolation.put("referenceId",
					DataServicePage.ReferenceStatus_keys.get(testCaseId + "Legal Violation"));
			UniqueKey_LegalViolation.put("fimTaSummary",
					"Summary is not defined pls refer summary" + fc.utobj().generateRandomNumber());
			status = ActivityPerform(driver, dataSet, config, UniqueKey_LegalViolation, DataServicePage.InfoMgr,
					DataServicePage.LegalViolation, DataServicePage.Update, DataServicePage.updateDiv, testCaseId);
			if (!status) {
				fc.utobj().throwsException("Legal Violation is not updated in " + testCaseId);
			}
			return status;
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			status = false;
		}
		return status;
	}

	public boolean CreateLendersRestAPI(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey_Lenders) throws Exception {
		boolean status = false;
		try {
			UniqueKey_Lenders.put("parentReferenceId",
					DataServicePage.ReferenceStatus_keys.get(testCaseId + "Franchisee"));
			UniqueKey_Lenders.put("fimTtFirstName", "Fname" + fc.utobj().generateRandomNumber());
			status = ActivityPerform(driver, dataSet, config, UniqueKey_Lenders, DataServicePage.InfoMgr,
					DataServicePage.Lenders, DataServicePage.Create, DataServicePage.createDiv, testCaseId);
			if (!status) {
				fc.utobj().throwsException("Lenders is not Created in RestAPI " + testCaseId);
			}
			return status;
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			status = false;
		}
		return status;
	}

	public boolean UpdateLendersRestAPI(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey_Lenders) throws Exception {
		boolean status = false;
		try {
			UniqueKey_Lenders.put("referenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Lenders"));
			UniqueKey_Lenders.put("fimTtFirstName", "Fname" + fc.utobj().generateRandomNumber());
			status = ActivityPerform(driver, dataSet, config, UniqueKey_Lenders, DataServicePage.InfoMgr,
					DataServicePage.Lenders, DataServicePage.Update, DataServicePage.updateDiv, testCaseId);
			if (!status) {
				fc.utobj().throwsException("Lenders is not updated in " + testCaseId);
			}

			return status;
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			status = false;
		}
		return status;
	}

	public boolean CreateExternalMailsRestAPI(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey_ExternalMails) throws Exception {
		boolean status = false;
		try {
			UniqueKey_ExternalMails.put("parentReferenceId",
					DataServicePage.ReferenceStatus_keys.get(testCaseId + "Franchisee"));
			UniqueKey_ExternalMails.put("mailFrom", "test@franconnect.net");
			UniqueKey_ExternalMails.put("mailCc", "test@franconnect.net");
			UniqueKey_ExternalMails.put("mailBcc", "test@franconnect.net");
			UniqueKey_ExternalMails.put("mailSubject", "Test Mail");
			UniqueKey_ExternalMails.put("mailMessage", "hello this is test");
			status = ActivityPerform(driver, dataSet, config, UniqueKey_ExternalMails, DataServicePage.InfoMgr,
					DataServicePage.ExternalMail, DataServicePage.Create, DataServicePage.createDiv, testCaseId);
			if (!status) {
				fc.utobj().throwsException("External Mails is not Created in RestAPI " + testCaseId);
			}
			return status;
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			status = false;
		}
		return status;
	}

	public boolean UpdateExternalMailsRestAPI(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey_ExternalMails) throws Exception {
		boolean status = false;
		try {
			UniqueKey_ExternalMails.put("parentReferenceId",
					DataServicePage.ReferenceStatus_keys.get(testCaseId + "Franchisee"));
			UniqueKey_ExternalMails.put("mailFrom", "test@franconnect.net");
			UniqueKey_ExternalMails.put("mailCc", "test@franconnect.net");
			UniqueKey_ExternalMails.put("mailBcc", "test@franconnect.net");
			UniqueKey_ExternalMails.put("mailSubject", "Test Mail");
			UniqueKey_ExternalMails.put("mailMessage", "hello this is test");
			status = ActivityPerform(driver, dataSet, config, UniqueKey_ExternalMails, DataServicePage.InfoMgr,
					DataServicePage.ExternalMail, DataServicePage.Update, DataServicePage.updateDiv, testCaseId);
			if (status) {
				fc.utobj().throwsException("External Mails is Updated in RestAPI " + testCaseId);
			}
			return status;
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			status = false;
		}
		return status;
	}

	public boolean CreateMarketingRestAPI(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey_Marketing) throws Exception {
		boolean status = false;
		try {
			UniqueKey_Marketing.put("parentReferenceId",
					DataServicePage.ReferenceStatus_keys.get(testCaseId + "Franchisee"));
			UniqueKey_Marketing.put("fimTtFirstName", "Fname" + fc.utobj().generateRandomNumber());
			status = ActivityPerform(driver, dataSet, config, UniqueKey_Marketing, DataServicePage.InfoMgr,
					DataServicePage.Marketing, DataServicePage.Create, DataServicePage.createDiv, testCaseId);
			if (!status) {
				fc.utobj().throwsException("Marketing is not Created in RestAPI " + testCaseId);
			}

			return status;
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			status = false;
		}
		return status;
	}

	public boolean DeleteMarketingRestAPI(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey_Marketing) throws Exception {
		boolean status = false;
		try {
			UniqueKey_Marketing.put("referenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Marketing"));
			UniqueKey_Marketing.put("fimTtFirstName", "Fname" + fc.utobj().generateRandomNumber());
			status = ActivityPerform(driver, dataSet, config, UniqueKey_Marketing, DataServicePage.InfoMgr,
					DataServicePage.Marketing, DataServicePage.Delete, DataServicePage.deleteDiv, testCaseId);
			if (!status) {
				fc.utobj().throwsException("Marketing Deletion is not performed in " + testCaseId);
			}

			return status;
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			status = false;
		}
		return status;
	}

	public boolean UpdateMarketingRestAPI(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey_Marketing) throws Exception {
		boolean status = false;
		try {
			UniqueKey_Marketing.put("referenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Marketing"));
			UniqueKey_Marketing.put("fimTtFirstName", "Fname" + fc.utobj().generateRandomNumber());
			status = ActivityPerform(driver, dataSet, config, UniqueKey_Marketing, DataServicePage.InfoMgr,
					DataServicePage.Marketing, DataServicePage.Update, DataServicePage.updateDiv, testCaseId);
			if (!status) {
				fc.utobj().throwsException("Marketing is not updated in " + testCaseId);
			}

			return status;
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			status = false;
		}
		return status;
	}

	public boolean CreateMysteryReviewRestAPI(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey_MysteryReview) throws Exception {
		boolean status = false;
		try {
			UniqueKey_MysteryReview.put("parentReferenceId",
					DataServicePage.ReferenceStatus_keys.get(testCaseId + "Franchisee"));
			UniqueKey_MysteryReview.put("fimDdInspectionDate", fc.utobj().generateCurrentDatewithformat("yyyy-MM-dd"));
			status = ActivityPerform(driver, dataSet, config, UniqueKey_MysteryReview, DataServicePage.InfoMgr,
					DataServicePage.MysteryReview, DataServicePage.Create, DataServicePage.createDiv, testCaseId);
			if (!status) {
				fc.utobj().throwsException("MysteryReview is not Created in RestAPI " + testCaseId);
			}

			return status;
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			status = false;
		}
		return status;
	}

	public boolean UpdateMysteryReviewRestAPI(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey_MysteryReview) throws Exception {
		boolean status = false;
		try {
			UniqueKey_MysteryReview.put("referenceId",
					DataServicePage.ReferenceStatus_keys.get(testCaseId + "Mystery Review"));
			UniqueKey_MysteryReview.put("fimDdInspectionDate", fc.utobj().generateCurrentDatewithformat("yyyy-MM-dd"));
			status = ActivityPerform(driver, dataSet, config, UniqueKey_MysteryReview, DataServicePage.InfoMgr,
					DataServicePage.MysteryReview, DataServicePage.Update, DataServicePage.updateDiv, testCaseId);
			if (!status) {
				fc.utobj().throwsException("Mystery Review is not updated in " + testCaseId);
			}

			return status;
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			status = false;
		}
		return status;
	}

	public boolean CreateOwnersRestAPI(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey_Owners) throws Exception {
		boolean status = false;
		try {
			UniqueKey_Owners.put("parentReferenceId",
					DataServicePage.ReferenceStatus_keys.get(testCaseId + "Franchisee"));
			UniqueKey_Owners.put("ownerType", "New");
			UniqueKey_Owners.put("lastName", "Fname" + fc.utobj().generateRandomNumber());
			UniqueKey_Owners.put("firstName", "Fname" + fc.utobj().generateRandomNumber());
			status = ActivityPerform(driver, dataSet, config, UniqueKey_Owners, DataServicePage.InfoMgr,
					DataServicePage.Owners, DataServicePage.Create, DataServicePage.createDiv, testCaseId);
			if (!status) {
				fc.utobj().throwsException("Owners is not Created in RestAPI " + testCaseId);
			}

			return status;
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			status = false;
		}
		return status;
	}

	public boolean UpdateOwnersRestAPI(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey_Owners) throws Exception {
		boolean status = false;
		try {

			UniqueKey_Owners.put("referenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Owners"));
			UniqueKey_Owners.put("ownerType", "New");
			UniqueKey_Owners.put("lastName", "Fname" + fc.utobj().generateRandomNumber());
			UniqueKey_Owners.put("firstName", "Fname" + fc.utobj().generateRandomNumber());
			status = ActivityPerform(driver, dataSet, config, UniqueKey_Owners, DataServicePage.InfoMgr,
					DataServicePage.Owners, DataServicePage.Update, DataServicePage.updateDiv, testCaseId);
			if (!status) {
				fc.utobj().throwsException("Owners is not updated in " + testCaseId);
			}

			return status;
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			status = false;
		}
		return status;
	}

	public boolean CreateTerritoryRestAPI(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey_Territory) throws Exception {
		boolean status = false;
		try {
			UniqueKey_Territory.put("parentReferenceId",
					DataServicePage.ReferenceStatus_keys.get(testCaseId + "Franchisee"));
			UniqueKey_Territory.put("fimTtTypeTerritory", "Exclusive");
			UniqueKey_Territory.put("fimTaNotes", "Territory");
			status = ActivityPerform(driver, dataSet, config, UniqueKey_Territory, DataServicePage.InfoMgr,
					DataServicePage.Territory, DataServicePage.Create, DataServicePage.createDiv, testCaseId);
			if (!status) {
				fc.utobj().throwsException("Territory is not Created in RestAPI " + testCaseId);
			}

			return status;
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			status = false;
		}
		return status;
	}

	public boolean UpdateTerritoryRestAPI(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey_Territory) throws Exception {
		boolean status = false;
		try {
			UniqueKey_Territory.put("referenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Territory"));
			UniqueKey_Territory.put("fimTtTypeTerritory", "Exclusive");
			status = ActivityPerform(driver, dataSet, config, UniqueKey_Territory, DataServicePage.InfoMgr,
					DataServicePage.Territory, DataServicePage.Update, DataServicePage.updateDiv, testCaseId);
			if (!status) {
				fc.utobj().throwsException("Territory is not updated in " + testCaseId);
			}

			return status;
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			status = false;
		}
		return status;
	}

	public boolean CreateRenewalRestAPI(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey_Renewal) throws Exception {
		boolean status = false;
		try {
			UniqueKey_Renewal.put("parentReferenceId",
					DataServicePage.ReferenceStatus_keys.get(testCaseId + "Franchisee"));
			String date = fc.utobj().generateCurrentDatewithformat("yyyy-MM-dd");
			UniqueKey_Renewal.put("fimDdAsOf", date);
			status = ActivityPerform(driver, dataSet, config, UniqueKey_Renewal, DataServicePage.InfoMgr,
					DataServicePage.Renewal, DataServicePage.Create, DataServicePage.createDiv, testCaseId);
			if (!status) {
				fc.utobj().throwsException("Renewal is not Created in RestAPI " + testCaseId);
			}

			return status;
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			status = false;
		}
		return status;
	}

	public boolean DeleteRenewalRestAPI(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey_Renewal) throws Exception {
		boolean Status = false;
		try {
			UniqueKey_Renewal.remove("referenceId");
			UniqueKey_Renewal.remove("parentReferenceId");
			ArrayList<String> listItems = new ArrayList<String>(UniqueKey_Renewal.values());
			Status = fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);
			if (Status) {
				fc.utobj().throwsException("Renewal is found on view page " + testCaseId);
			}

			return Status;
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			Status = false;
		}
		return Status;
	}

	public boolean UpdateRenewalRestAPI(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey_Renewal) throws Exception {
		boolean status = false;
		try {
			UniqueKey_Renewal.put("parentReferenceId",
					DataServicePage.ReferenceStatus_keys.get(testCaseId + "Franchisee"));
			String date = fc.utobj().generateCurrentDatewithformat("yyyy-MM-dd");
			UniqueKey_Renewal.put("fimDdAsOf", date);
			status = ActivityPerform(driver, dataSet, config, UniqueKey_Renewal, DataServicePage.InfoMgr,
					DataServicePage.Renewal, DataServicePage.Create, DataServicePage.createDiv, testCaseId);
			if (!status) {
				fc.utobj().throwsException("Renewal is not Created in RestAPI " + testCaseId);
			}

			return status;
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			status = false;
		}
		return status;
	}

	public boolean CreateRealEstateRestAPI(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey_RealEstate) throws Exception {
		boolean status = false;
		try {
			UniqueKey_RealEstate.put("parentReferenceId",
					DataServicePage.ReferenceStatus_keys.get(testCaseId + "Franchisee"));
			UniqueKey_RealEstate.put("fimTtFirstName", "Fname" + fc.utobj().generateRandomNumber());
			status = ActivityPerform(driver, dataSet, config, UniqueKey_RealEstate, DataServicePage.InfoMgr,
					DataServicePage.RealEstate, DataServicePage.Create, DataServicePage.createDiv, testCaseId);
			if (!status) {
				fc.utobj().throwsException("Real Estate is not Created in RestAPI " + testCaseId);
			}

			return status;
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			status = false;
		}
		return status;
	}

	public boolean UpdateRealEstateRestAPI(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey_RealEstate) throws Exception {
		boolean status = false;
		try {
			UniqueKey_RealEstate.put("referenceId",
					DataServicePage.ReferenceStatus_keys.get(testCaseId + "Real Estate"));
			UniqueKey_RealEstate.put("fimTtFirstName", "Fname" + fc.utobj().generateRandomNumber());
			status = ActivityPerform(driver, dataSet, config, UniqueKey_RealEstate, DataServicePage.InfoMgr,
					DataServicePage.RealEstate, DataServicePage.Update, DataServicePage.updateDiv, testCaseId);
			if (!status) {
				fc.utobj().throwsException("Real Estate is not updated in " + testCaseId);
			}

			return status;
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			status = false;
		}
		return status;
	}

	public boolean CreateRemarkRestAPI(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey_Remark) throws Exception {
		boolean status = false;
		try {
			UniqueKey_Remark.put("parentReferenceId",
					DataServicePage.ReferenceStatus_keys.get(testCaseId + "Franchisee"));
			UniqueKey_Remark.put("remarks", "Remarks has been added");
			UniqueKey_Remark.put("fimTtFirstName", "Fname" + fc.utobj().generateRandomNumber());
			status = ActivityPerform(driver, dataSet, config, UniqueKey_Remark, DataServicePage.InfoMgr,
					DataServicePage.Remark, DataServicePage.Create, DataServicePage.createDiv, testCaseId);
			if (!status) {
				fc.utobj().throwsException("Remark is not Created in RestAPI " + testCaseId);
			}

			return status;
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			status = false;
		}
		return status;
	}

	public boolean UpdateRemarkRestAPI(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey_Remark) throws Exception {
		boolean status = false;
		try {
			UniqueKey_Remark.put("parentReferenceId",
					DataServicePage.ReferenceStatus_keys.get(testCaseId + "Franchisee"));
			UniqueKey_Remark.put("remarks", "Remarks has been added");
			UniqueKey_Remark.put("fimTtFirstName", "Fname" + fc.utobj().generateRandomNumber());
			status = ActivityPerform(driver, dataSet, config, UniqueKey_Remark, DataServicePage.InfoMgr,
					DataServicePage.Remark, DataServicePage.Create, DataServicePage.createDiv, testCaseId);
			if (!status) {
				fc.utobj().throwsException("Remark is not Created in RestAPI " + testCaseId);
			}

			return status;
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			status = false;
		}
		return status;
	}

	public boolean CreateTaskRestAPI(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey_Task, Map<String, String> CorpUser) throws Exception {
		boolean status = false;
		try {
			UniqueKey_Task.put("parentReferenceId",
					DataServicePage.ReferenceStatus_keys.get(testCaseId + "Franchisee"));
			UniqueKey_Task.put("assignTo", CorpUser.get("firstname") + " " + CorpUser.get("lastname"));
			UniqueKey_Task.put("status", "Completed");
			UniqueKey_Task.put("startTime", "01:00Z");
			UniqueKey_Task.put("endTime", "11:00Z");
			UniqueKey_Task.put("taskType", "Default");
			UniqueKey_Task.put("subject", "Assigned Task" + fc.utobj().generateRandomNumber());
			UniqueKey_Task.put("timelessTask", "Y");
			UniqueKey_Task.put("date", fc.utobj().generateCurrentDatewithformat("yyyy-MM-dd"));
			status = ActivityPerform(driver, dataSet, config, UniqueKey_Task, DataServicePage.InfoMgr,
					DataServicePage.Task, DataServicePage.Create, DataServicePage.createDiv, testCaseId);
			if (!status) {
				fc.utobj().throwsException("Task is not Created in RestAPI " + testCaseId);
			}

			return status;
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			status = false;
		}
		return status;
	}

	public boolean UpdateTaskRestAPI(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey_Task, Map<String, String> CorpUser) throws Exception {
		boolean status = false;
		try {
			UniqueKey_Task.put("referenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Task"));
			UniqueKey_Task.put("assignTo", CorpUser.get("firstname") + " " + CorpUser.get("lastname"));
			UniqueKey_Task.put("status", "Completed");
			UniqueKey_Task.put("taskType", "Default");
			UniqueKey_Task.put("subject", "Assigned Task" + fc.utobj().generateRandomNumber());
			UniqueKey_Task.put("timelessTask", "Y");
			UniqueKey_Task.put("date", fc.utobj().generateCurrentDatewithformat("yyyy-MM-dd"));
			status = ActivityPerform(driver, dataSet, config, UniqueKey_Task, DataServicePage.InfoMgr,
					DataServicePage.Task, DataServicePage.Update, DataServicePage.updateDiv, testCaseId);
			if (!status) {
				fc.utobj().throwsException("Task is not updated in " + testCaseId);
			}

			return status;
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			status = false;
		}
		return status;
	}

	public String UpdateLocation(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			Map<String, String> UniqueKey_Franchisee, String testCaseId) {
		try {
			driver = fc.loginpage().login(driver);
			String RegionName = "Reg" + fc.utobj().generateRandomNumber();
			String storeType = "store" + fc.utobj().generateRandomNumber();
			AdminAreaRegionAddAreaRegionPageTest p1 = new AdminAreaRegionAddAreaRegionPageTest();
			p1.addAreaRegion(driver, RegionName);
			AdminConfigurationConfigureStoreTypePageTest p2 = new AdminConfigurationConfigureStoreTypePageTest();
			storeType = p2.addStoreType(driver, storeType);
			UniqueKey_Franchisee.put("referenceId",
					DataServicePage.ReferenceStatus_keys.get(testCaseId + "Franchisee"));
			UniqueKey_Franchisee.put("status", "Yes");
			UniqueKey_Franchisee.put("franchiseeName", "FIM" + fc.utobj().generateRandomNumber());
			UniqueKey_Franchisee.put("centerName", "Center" + fc.utobj().generateRandomNumber());
			UniqueKey_Franchisee.put("areaID", RegionName);
			UniqueKey_Franchisee.put("openingDate", fc.utobj().generateCurrentDatewithformat("yyyy-MM-dd"));
			UniqueKey_Franchisee.put("storeTypeId", storeType);
			UniqueKey_Franchisee.put("city", "NYC");
			UniqueKey_Franchisee.put("country", "USA");
			UniqueKey_Franchisee.put("state", "Alaska");
			UniqueKey_Franchisee.put("storePhone", "1122334455");
			UniqueKey_Franchisee.put("entityType", "Existing");
			UniqueKey_Franchisee.put("ownerType", "New");
			UniqueKey_Franchisee.put("division", "div" + fc.utobj().generateRandomNumber());
			UniqueKey_Franchisee.put("fimTtEntityName", "New");
			UniqueKey_Franchisee.put("lastName", "Lname" + fc.utobj().generateRandomNumber());
			driver.navigate().to(config.get("buildUrl") + DataServicePage.restURL);
			boolean CreateFranchiseLocation = ActivityPerform(driver, dataSet, config, UniqueKey_Franchisee,
					DataServicePage.InfoMgr, DataServicePage.Franchisee, DataServicePage.Update,
					DataServicePage.updateDiv, testCaseId);
			return DataServicePage.ReferenceStatus_keys.get(testCaseId + "Franchisee");
		} catch (Exception ex) {
			return null;
		}
	}

	public boolean UpdateAgreementRestAPI(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey_Agreement) throws Exception {
		boolean status = false;

		try {
			UniqueKey_Agreement.put("referenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Agreement"));
			UniqueKey_Agreement.put("fimDdApprovedDate", fc.utobj().generateCurrentDatewithformat("yyyy-MM-dd"));
			status = ActivityPerform(driver, dataSet, config, UniqueKey_Agreement, DataServicePage.InfoMgr,
					DataServicePage.Agreement, DataServicePage.Update, DataServicePage.updateDiv, testCaseId);
			if (!status) {
				fc.utobj().throwsException("Agreement is not updated in " + testCaseId);
			}
			return status;
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
		return status;
	}

	public boolean DeleteAgreementRestAPI(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey_Agreement) throws Exception {
		boolean status = false;

		try {
			UniqueKey_Agreement.put("referenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Agreement"));
			UniqueKey_Agreement.put("fimDdApprovedDate", fc.utobj().generateCurrentDatewithformat("yyyy-MM-dd"));
			status = ActivityPerform(driver, dataSet, config, UniqueKey_Agreement, DataServicePage.InfoMgr,
					DataServicePage.Agreement, DataServicePage.Delete, DataServicePage.deleteDiv, testCaseId);
			if (!status) {
				fc.utobj().throwsException("Agreement Deletion is not performed in " + testCaseId);
			}
			return status;
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
		return status;
	}

	public boolean DeleteCustomerComplainRestAPI(WebDriver driver, Map<String, String> dataSet,
			Map<String, String> config, String testCaseId, Map<String, String> UniqueKey_CustomerComplain)
			throws Exception {
		boolean status = false;

		try {
			UniqueKey_CustomerComplain.put("referenceId",
					DataServicePage.ReferenceStatus_keys.get(testCaseId + "Customer Complaints"));
			status = ActivityPerform(driver, dataSet, config, UniqueKey_CustomerComplain, DataServicePage.InfoMgr,
					DataServicePage.CustomerComplaints, DataServicePage.Delete, DataServicePage.deleteDiv, testCaseId);
			if (!status) {
				fc.utobj().throwsException("Customer Complain Deletion is not performed in " + testCaseId);
			}
			return status;
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
		return status;
	}

	public boolean DeleteEntityDetailsRestAPI(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey_EntityDetails) throws Exception {
		boolean status = false;

		try {
			UniqueKey_EntityDetails.put("referenceId",
					DataServicePage.ReferenceStatus_keys.get(testCaseId + "Entity Details"));
			UniqueKey_EntityDetails.put("entityType", "New");
			UniqueKey_EntityDetails.put("fimTtEntityName", "Name" + fc.utobj().generateRandomNumber());
			status = ActivityPerform(driver, dataSet, config, UniqueKey_EntityDetails, DataServicePage.InfoMgr,
					DataServicePage.EntityDetails, DataServicePage.Delete, DataServicePage.deleteDiv, testCaseId);
			if (!status) {
				fc.utobj().throwsException("Entity Details Deletion is not performed in " + testCaseId);
			}
			return status;
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
		return status;
	}

	public boolean DeleteEmployeeRestAPI(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey_Employees) throws Exception {
		boolean status = false;

		try {
			UniqueKey_Employees.put("referenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Employees"));
			UniqueKey_Employees.put("employeeType", "New");
			UniqueKey_Employees.put("firstName", "Fname" + fc.utobj().generateRandomNumber());
			UniqueKey_Employees.put("lastName", "Lname" + fc.utobj().generateRandomNumber());
			status = ActivityPerform(driver, dataSet, config, UniqueKey_Employees, DataServicePage.InfoMgr,
					DataServicePage.Employees, DataServicePage.Delete, DataServicePage.deleteDiv, testCaseId);
			if (!status) {
				fc.utobj().throwsException("Employees Deletion is not performed in " + testCaseId);
			}
			return status;
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
		return status;
	}

	public boolean DeleteEventsRestAPI(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey_Events) throws Exception {
		boolean status = false;

		try {
			UniqueKey_Events.put("referenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Events"));
			UniqueKey_Events.put("fimDdEventDate", fc.utobj().generateCurrentDatewithformat("yyyy-MM-dd"));
			UniqueKey_Events.put("fimTtEventAuthor", "Oraganizer" + fc.utobj().generateRandomNumber());
			status = ActivityPerform(driver, dataSet, config, UniqueKey_Events, DataServicePage.InfoMgr,
					DataServicePage.Events, DataServicePage.Delete, DataServicePage.deleteDiv, testCaseId);
			if (!status) {
				fc.utobj().throwsException("Events Deletion is not performed in " + testCaseId);
			}

			return status;
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
		return status;
	}

	public boolean DeleteCallRestAPI(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey_Call) throws Exception {
		boolean status = false;

		try {
			UniqueKey_Call.put("referenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Call"));
			UniqueKey_Call.put("date", fc.utobj().generateCurrentDatewithformat("yyyy-MM-dd"));
			UniqueKey_Call.put("timeAdded", "03:00Z");
			UniqueKey_Call.put("callStatus", "Contacted");
			UniqueKey_Call.put("callType", "Outbound Call");
			UniqueKey_Call.put("subject", "subjent" + fc.utobj().generateRandomNumber());
			status = ActivityPerform(driver, dataSet, config, UniqueKey_Call, DataServicePage.InfoMgr,
					DataServicePage.Call, DataServicePage.Delete, DataServicePage.deleteDiv, testCaseId);
			if (!status) {
				fc.utobj().throwsException("Call Deletion is not performed in " + testCaseId);
			}

			return status;
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
		return status;
	}

	public boolean DeleteTrainingRestAPI(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey_Training) throws Exception {
		boolean status = false;

		try {
			UniqueKey_Training.put("referenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Training"));
			UniqueKey_Training.put("fimTtFirstName", "Fname" + fc.utobj().generateRandomNumber());
			status = ActivityPerform(driver, dataSet, config, UniqueKey_Training, DataServicePage.InfoMgr,
					DataServicePage.Training, DataServicePage.Delete, DataServicePage.deleteDiv, testCaseId);
			if (!status) {
				fc.utobj().throwsException("Training Deletion is not performed in " + testCaseId);
			}

			return status;
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
		return status;
	}

	public boolean DeleteFinancialRestAPI(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey_Financial) throws Exception {
		boolean status = false;

		try {
			UniqueKey_Financial.put("referenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Financial"));
			UniqueKey_Financial.put("fimTtFirstName", "Fname" + fc.utobj().generateRandomNumber());
			status = ActivityPerform(driver, dataSet, config, UniqueKey_Financial, DataServicePage.InfoMgr,
					DataServicePage.Financial, DataServicePage.Delete, DataServicePage.deleteDiv, testCaseId);
			if (!status) {
				fc.utobj().throwsException("Financial Deletion is not performed in " + testCaseId);
			}

			return status;
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
		return status;
	}

	public boolean DeleteGaurantorRestAPI(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey_Gaurantor) throws Exception {
		boolean status = false;

		try {
			UniqueKey_Gaurantor.put("referenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Guarantors"));
			UniqueKey_Gaurantor.put("fimTtFirstName", "Fname" + fc.utobj().generateRandomNumber());
			status = ActivityPerform(driver, dataSet, config, UniqueKey_Gaurantor, DataServicePage.InfoMgr,
					DataServicePage.Guarantors, DataServicePage.Delete, DataServicePage.deleteDiv, testCaseId);
			if (!status) {
				fc.utobj().throwsException("Gaurantor Deletion is not performed in " + testCaseId);
			}

			return status;
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
		return status;
	}

	public boolean DeleteInsuranceRestAPI(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey_Insurance) throws Exception {
		boolean status = false;

		try {
			UniqueKey_Insurance.put("referenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Insurance"));
			UniqueKey_Insurance.put("fimTtInsuranceCompanyName", "CompanyName" + fc.utobj().generateRandomNumber());
			status = ActivityPerform(driver, dataSet, config, UniqueKey_Insurance, DataServicePage.InfoMgr,
					DataServicePage.Insurance, DataServicePage.Delete, DataServicePage.deleteDiv, testCaseId);
			if (!status) {
				fc.utobj().throwsException("Insurance Deletion is not performed in " + testCaseId);
			}

			return status;
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
		return status;
	}

	public boolean DeleteLegalViolationRestAPI(WebDriver driver, Map<String, String> dataSet,
			Map<String, String> config, String testCaseId, Map<String, String> UniqueKey_LegalViolation)
			throws Exception {
		boolean status = false;

		try {
			UniqueKey_LegalViolation.put("referenceId",
					DataServicePage.ReferenceStatus_keys.get(testCaseId + "Insurance"));
			UniqueKey_LegalViolation.put("referenceId",
					DataServicePage.ReferenceStatus_keys.get(testCaseId + "Legal Violation"));
			UniqueKey_LegalViolation.put("fimTaSummary",
					"Summary is not defined pls refer summary" + fc.utobj().generateRandomNumber());
			status = ActivityPerform(driver, dataSet, config, UniqueKey_LegalViolation, DataServicePage.InfoMgr,
					DataServicePage.LegalViolation, DataServicePage.Delete, DataServicePage.deleteDiv, testCaseId);
			if (!status) {
				fc.utobj().throwsException("Legal Violation Deletion is not performed in " + testCaseId);
			}
			return status;
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
		return status;
	}

	public boolean DeleteLendersRestAPI(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey_Lenders) throws Exception {
		boolean status = false;

		try {
			UniqueKey_Lenders.put("referenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Lenders"));
			UniqueKey_Lenders.put("fimTtFirstName", "Fname" + fc.utobj().generateRandomNumber());
			status = ActivityPerform(driver, dataSet, config, UniqueKey_Lenders, DataServicePage.InfoMgr,
					DataServicePage.Lenders, DataServicePage.Delete, DataServicePage.deleteDiv, testCaseId);
			if (!status) {
				fc.utobj().throwsException("Lenders Deletion is not performed in " + testCaseId);
			}
			return status;
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
		return status;
	}

	public boolean DeleteMysteryReviewRestAPI(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey_MysteryReview) throws Exception {
		boolean status = false;

		try {
			UniqueKey_MysteryReview.put("referenceId",
					DataServicePage.ReferenceStatus_keys.get(testCaseId + "Mystery Review"));
			UniqueKey_MysteryReview.put("fimDdInspectionDate", fc.utobj().generateCurrentDatewithformat("yyyy-MM-dd"));
			status = ActivityPerform(driver, dataSet, config, UniqueKey_MysteryReview, DataServicePage.InfoMgr,
					DataServicePage.MysteryReview, DataServicePage.Delete, DataServicePage.deleteDiv, testCaseId);
			if (!status) {
				fc.utobj().throwsException("Mystery Review Deletion is not performed in " + testCaseId);
			}
			return status;
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
		return status;
	}

	public boolean DeleteOwnersRestAPI(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey_Owners) throws Exception {
		boolean status = false;

		try {
			UniqueKey_Owners.put("referenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Owners"));
			UniqueKey_Owners.put("ownerType", "New");
			UniqueKey_Owners.put("lastName", "Fname" + fc.utobj().generateRandomNumber());
			UniqueKey_Owners.put("firstName", "Fname" + fc.utobj().generateRandomNumber());
			status = ActivityPerform(driver, dataSet, config, UniqueKey_Owners, DataServicePage.InfoMgr,
					DataServicePage.Owners, DataServicePage.Delete, DataServicePage.deleteDiv, testCaseId);
			if (!status) {
				fc.utobj().throwsException("Owners Deletion is not performed in " + testCaseId);
			}
			return status;
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
		return status;
	}

	public boolean DeleteTerritoryRestAPI(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey_Territory) throws Exception {
		boolean status = false;

		try {
			UniqueKey_Territory.put("referenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Territory"));
			UniqueKey_Territory.put("fimTtFirstName", "Fname" + fc.utobj().generateRandomNumber());
			status = ActivityPerform(driver, dataSet, config, UniqueKey_Territory, DataServicePage.InfoMgr,
					DataServicePage.Territory, DataServicePage.Delete, DataServicePage.deleteDiv, testCaseId);
			if (!status) {
				fc.utobj().throwsException("Territory Deletion is not performed in " + testCaseId);
			}
			return status;
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
		return status;
	}

	public boolean DeleteRealEstateRestAPI(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String testCaseId, Map<String, String> UniqueKey_RealEstate) throws Exception {
		boolean status = false;

		try {
			UniqueKey_RealEstate.put("referenceId",
					DataServicePage.ReferenceStatus_keys.get(testCaseId + "Real Estate"));
			UniqueKey_RealEstate.put("fimTtFirstName", "Fname" + fc.utobj().generateRandomNumber());
			status = ActivityPerform(driver, dataSet, config, UniqueKey_RealEstate, DataServicePage.InfoMgr,
					DataServicePage.RealEstate, DataServicePage.Delete, DataServicePage.deleteDiv, testCaseId);
			if (!status) {
				fc.utobj().throwsException("Real Estate Deletion is not performed in " + testCaseId);
			}
			return status;
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
		return status;
	}

}
