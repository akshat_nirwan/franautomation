package com.builds.test.infomgr.RestAPI;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import org.xml.sax.SAXException;

import com.builds.uimaps.infomgr.InfoMgrCorporateLocationsPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class InfoMgrRestServiceDeleteSubModule extends BaseRestUtil {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "infomgrRestAPI", "FCRestAPI" })
	@TestCase(createdOn = "2017-06-16", updatedOn = "2017-06-16", testCaseDescription = "Delete Agreement subModule for Info Mgr In RestAPI", testCaseId = "TC_02_Fill_Agreement_Tab_Info_Mgr_Delete")
	public void DeleteAgreementInfoMgrRest() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("infomgrRestAPI", testCaseId);
		Map<String, String> UniqueKey_Franchisee = new HashMap<String, String>();
		Map<String, String> UniqueKey_CorpUser = new HashMap<String, String>();
		Map<String, String> UniqueKey_Agreement = new HashMap<String, String>();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			UserCreation uc = new UserCreation();
			InfoMgrRestService infomgrRest = new InfoMgrRestService();
			fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Corporate User");
			boolean createUser = uc.createCorpUser(driver, config, dataSet, testCaseId, UniqueKey_CorpUser);
			String Franchisee = null;
			if (createUser) {
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Franchisee Location");
				Franchisee = infomgrRest.AddLocation(driver, dataSet, config, UniqueKey_Franchisee, testCaseId);
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Agreement");
				boolean AgreementCreated = infomgrRest.CreateAgreementRestAPI(driver, dataSet, config, testCaseId,
						UniqueKey_Agreement);
				fc.utobj()
						.printTestStep("Navigating to InfoMgr > RestAPI > Deleting Agreement for  Franchisee Location");
				if (AgreementCreated) {
					infomgrRest.DeleteAgreementRestAPI(driver, dataSet, config, testCaseId, UniqueKey_Agreement);
				}
			}
			if (Franchisee != null) {
				fc.loginpage().login(driver);
				fc.infomgr().infomgr_common().InfoMgrCorporateLocations(driver);

				InfoMgrCorporateLocationsPage objFranPage = new InfoMgrCorporateLocationsPage(driver);
				try {
					try {
						fc.utobj().clickElement(driver, objFranPage.lnkShowAll);
					} catch (Exception ex) {
					}
					fc.utobj().clickPartialLinkText(driver, UniqueKey_Franchisee.get("franchiseeName"));
					fc.utobj().sleep(2500);
				} catch (Exception ex) {
					fc.utobj().throwsException("Franchise Location not found in View Page" + testCaseId);
				}
				fc.utobj().clickPartialLinkText(driver, "Agreement");
				UniqueKey_Agreement.remove("referenceId");
				UniqueKey_Agreement.remove("parentReferenceId");
				ArrayList<String> listItems = new ArrayList<String>(UniqueKey_Agreement.values());
				fc.utobj().printTestStep(
						"Navigating to InfoMgr > RestAPI > Validating Agreement for  Franchisee Location");
				boolean Status = fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);
				if (Status)
					fc.utobj().throwsException("Agreement found on view page " + testCaseId);
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgrRestAPI", "FCRestAPI" })
	@TestCase(createdOn = "2017-06-16", updatedOn = "2017-06-16", testCaseDescription = "Delete CustomerComplain subModule for Info Mgr In RestAPI", testCaseId = "TC_04_Fill_CustomerComplain_Tab_Info_Mgr_Delete")
	public void DeleteCustomerComplainInfoMgrRest()
			throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("infomgrRestAPI", testCaseId);
		Map<String, String> UniqueKey_Franchisee = new HashMap<String, String>();
		Map<String, String> UniqueKey_CorpUser = new HashMap<String, String>();
		Map<String, String> UniqueKey_CustomerComplain = new HashMap<String, String>();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			UserCreation uc = new UserCreation();
			InfoMgrRestService infomgrRest = new InfoMgrRestService();
			fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Corporate User");
			boolean createUser = uc.createCorpUser(driver, config, dataSet, testCaseId, UniqueKey_CorpUser);
			String Franchisee = null;
			if (createUser) {
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Franchisee Location");
				Franchisee = infomgrRest.AddLocation(driver, dataSet, config, UniqueKey_Franchisee, testCaseId);
				fc.utobj().sleep(2500);
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding CustomerComplain");
				boolean CustomerComplain = infomgrRest.CreateCustomerComplainRestAPI(driver, dataSet, config,
						testCaseId, UniqueKey_CustomerComplain);
				if (CustomerComplain) {
					fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Deleting CustomerComplain");
					fc.utobj().sleep(2500);
					infomgrRest.DeleteCustomerComplainRestAPI(driver, dataSet, config, testCaseId,
							UniqueKey_CustomerComplain);
				}
			}
			if (Franchisee != null) {
				fc.loginpage().login(driver);
				fc.infomgr().infomgr_common().InfoMgrCorporateLocations(driver);

				InfoMgrCorporateLocationsPage objFranPage = new InfoMgrCorporateLocationsPage(driver);
				try {
					try {
						fc.utobj().clickElement(driver, objFranPage.lnkShowAll);
					} catch (Exception ex) {
					}
					fc.utobj().clickPartialLinkText(driver, UniqueKey_Franchisee.get("franchiseeName"));
					fc.utobj().sleep(2500);
				} catch (Exception ex) {
					fc.utobj().throwsException("Franchise Location not found in View Page" + testCaseId);
				}
				fc.utobj().clickPartialLinkText(driver, "Customer Complaints");
				UniqueKey_CustomerComplain.remove("referenceId");
				UniqueKey_CustomerComplain.remove("parentReferenceId");
				ArrayList<String> listItems = new ArrayList<String>(UniqueKey_CustomerComplain.values());
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > validating CustomerComplain");
				boolean Status = fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);
				if (Status)
					fc.utobj().throwsException("Customer Complaints found on view page " + testCaseId);
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgrRestAPI", "FCRestAPI" })
	@TestCase(createdOn = "2017-06-16", updatedOn = "2017-06-16", testCaseDescription = "Delete EntityDetails subModule for Info Mgr In RestAPI", testCaseId = "TC_05_Fill_EntityDetails_Tab_Info_Mgr_Delete")
	public void DeleteEntityDetailsInfoMgrRest()
			throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("infomgrRestAPI", testCaseId);
		Map<String, String> UniqueKey_Franchisee = new HashMap<String, String>();
		Map<String, String> UniqueKey_CorpUser = new HashMap<String, String>();
		Map<String, String> UniqueKey_EntityDetails = new HashMap<String, String>();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			UserCreation uc = new UserCreation();
			InfoMgrRestService infomgrRest = new InfoMgrRestService();
			fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Corporate User");
			boolean createUser = uc.createCorpUser(driver, config, dataSet, testCaseId, UniqueKey_CorpUser);
			String Franchisee = null;
			if (createUser) {
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Franchisee Location");
				Franchisee = infomgrRest.AddLocation(driver, dataSet, config, UniqueKey_Franchisee, testCaseId);
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding EntityDetails");
				boolean EntityDetails = infomgrRest.CreateEntityDetailsRestAPI(driver, dataSet, config, testCaseId,
						UniqueKey_EntityDetails);
				if (EntityDetails) {
					fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Deleting EntityDetails");
					infomgrRest.DeleteEntityDetailsRestAPI(driver, dataSet, config, testCaseId,
							UniqueKey_EntityDetails);
				}
			}
			if (Franchisee != null) {
				fc.loginpage().login(driver);
				fc.infomgr().infomgr_common().InfoMgrCorporateLocations(driver);

				InfoMgrCorporateLocationsPage objFranPage = new InfoMgrCorporateLocationsPage(driver);
				try {
					try {
						fc.utobj().clickElement(driver, objFranPage.lnkShowAll);
					} catch (Exception ex) {
					}
					fc.utobj().clickPartialLinkText(driver, UniqueKey_Franchisee.get("franchiseeName"));
					fc.utobj().sleep(2500);
				} catch (Exception ex) {
					fc.utobj().throwsException("Franchise Location not found in View Page" + testCaseId);
				}
				fc.utobj().clickPartialLinkText(driver, "Entity Details");
				UniqueKey_EntityDetails.remove("referenceId");
				UniqueKey_EntityDetails.remove("parentReferenceId");
				ArrayList<String> listItems = new ArrayList<String>(UniqueKey_EntityDetails.values());
				boolean Status = fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);
				if (Status)
					fc.utobj().throwsException("Entity Details found on view page " + testCaseId);
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgrRestAPI", "FCRestAPI" })
	@TestCase(createdOn = "2017-06-16", updatedOn = "2017-06-16", testCaseDescription = "Delete Employees subModule for Info Mgr In RestAPI", testCaseId = "TC_06_Fill_Employees_Tab_Info_Mgr_Delete")
	public void DeleteEmployeesInfoMgrRest() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("infomgrRestAPI", testCaseId);
		Map<String, String> UniqueKey_Franchisee = new HashMap<String, String>();
		Map<String, String> UniqueKey_CorpUser = new HashMap<String, String>();
		Map<String, String> UniqueKey_Employees = new HashMap<String, String>();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			UserCreation uc = new UserCreation();
			InfoMgrRestService infomgrRest = new InfoMgrRestService();
			fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Corporate User");
			boolean createUser = uc.createCorpUser(driver, config, dataSet, testCaseId, UniqueKey_CorpUser);
			String Franchisee = null;
			if (createUser) {
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Franchisee Location");
				Franchisee = infomgrRest.AddLocation(driver, dataSet, config, UniqueKey_Franchisee, testCaseId);
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Employees");
				infomgrRest.CreateEmployeeRestAPI(driver, dataSet, config, testCaseId, UniqueKey_Employees);

				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Deleting Employees");
				infomgrRest.DeleteEmployeeRestAPI(driver, dataSet, config, testCaseId, UniqueKey_Employees);
			}
			if (Franchisee != null) {
				fc.loginpage().login(driver);
				fc.infomgr().infomgr_common().InfoMgrCorporateLocations(driver);

				InfoMgrCorporateLocationsPage objFranPage = new InfoMgrCorporateLocationsPage(driver);
				try {
					try {
						fc.utobj().clickElement(driver, objFranPage.lnkShowAll);
					} catch (Exception ex) {
					}
					fc.utobj().clickPartialLinkText(driver, UniqueKey_Franchisee.get("franchiseeName"));
					fc.utobj().sleep(2500);
				} catch (Exception ex) {
					fc.utobj().throwsException("Franchise Location not found in View Page" + testCaseId);
				}
				fc.utobj().clickPartialLinkText(driver, "Employees");
				UniqueKey_Employees.remove("referenceId");
				UniqueKey_Employees.remove("parentReferenceId");
				ArrayList<String> listItems = new ArrayList<String>(UniqueKey_Employees.values());
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Validating Employees");
				boolean Status = fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);
				if (Status)
					fc.utobj().throwsException("Employee found on view page " + testCaseId);
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgrRestAPI", "FCRestAPI" })
	@TestCase(createdOn = "2017-06-16", updatedOn = "2017-06-16", testCaseDescription = "Delete Events subModule for Info Mgr In RestAPI", testCaseId = "TC_08_Fill_Events_Tab_Info_Mgr_Delete")
	public void DeleteEventsInfoMgrRest() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("infomgrRestAPI", testCaseId);
		Map<String, String> UniqueKey_Franchisee = new HashMap<String, String>();
		Map<String, String> UniqueKey_CorpUser = new HashMap<String, String>();
		Map<String, String> UniqueKey_Events = new HashMap<String, String>();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			UserCreation uc = new UserCreation();
			InfoMgrRestService infomgrRest = new InfoMgrRestService();
			fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Corporate User");
			boolean createUser = uc.createCorpUser(driver, config, dataSet, testCaseId, UniqueKey_CorpUser);
			String Franchisee = null;
			if (createUser) {
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Franchisee Location");
				Franchisee = infomgrRest.AddLocation(driver, dataSet, config, UniqueKey_Franchisee, testCaseId);
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Events");
				infomgrRest.CreateEventsRestAPI(driver, dataSet, config, testCaseId, UniqueKey_Events);
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Deleting Events");
				infomgrRest.DeleteEventsRestAPI(driver, dataSet, config, testCaseId, UniqueKey_Events);
			}
			if (Franchisee != null) {
				fc.loginpage().login(driver);
				fc.infomgr().infomgr_common().InfoMgrCorporateLocations(driver);

				InfoMgrCorporateLocationsPage objFranPage = new InfoMgrCorporateLocationsPage(driver);
				try {
					try {
						fc.utobj().clickElement(driver, objFranPage.lnkShowAll);
					} catch (Exception ex) {
					}
					fc.utobj().clickPartialLinkText(driver, UniqueKey_Franchisee.get("franchiseeName"));
					fc.utobj().sleep(2500);
				} catch (Exception ex) {
					fc.utobj().throwsException("Franchise Location not found in View Page" + testCaseId);
				}
				fc.utobj().clickPartialLinkText(driver, "Events");
				UniqueKey_Events.remove("referenceId");
				UniqueKey_Events.remove("parentReferenceId");
				ArrayList<String> listItems = new ArrayList<String>(UniqueKey_Events.values());
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Valdiating Events");
				boolean Status = fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);
				if (Status)
					fc.utobj().throwsException("Events found on view page " + testCaseId);
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgrRestAPI", "FCRestAPI" })
	@TestCase(createdOn = "2017-06-16", updatedOn = "2017-06-16", testCaseDescription = "Delete Training subModule for Info Mgr In RestAPI", testCaseId = "TC_07_Fill_Training_Tab_Info_Mgr_Delete")
	public void DeleteTrainingInfoMgrRest() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("infomgrRestAPI", testCaseId);
		Map<String, String> UniqueKey_Franchisee = new HashMap<String, String>();
		Map<String, String> UniqueKey_CorpUser = new HashMap<String, String>();
		Map<String, String> UniqueKey_Training = new HashMap<String, String>();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			UserCreation uc = new UserCreation();
			InfoMgrRestService infomgrRest = new InfoMgrRestService();
			fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Corporate User");
			boolean createUser = uc.createCorpUser(driver, config, dataSet, testCaseId, UniqueKey_CorpUser);
			String Franchisee = null;
			if (createUser) {
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Franchisee Location");
				Franchisee = infomgrRest.AddLocation(driver, dataSet, config, UniqueKey_Franchisee, testCaseId);
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Training");
				infomgrRest.CreateTrainingRestAPI(driver, dataSet, config, testCaseId, UniqueKey_Training);
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Deleting Training");
				infomgrRest.DeleteTrainingRestAPI(driver, dataSet, config, testCaseId, UniqueKey_Training);
			}
			if (Franchisee != null) {
				fc.loginpage().login(driver);
				fc.infomgr().infomgr_common().InfoMgrCorporateLocations(driver);

				InfoMgrCorporateLocationsPage objFranPage = new InfoMgrCorporateLocationsPage(driver);
				try {
					try {
						fc.utobj().clickElement(driver, objFranPage.lnkShowAll);
					} catch (Exception ex) {
					}
					fc.utobj().clickPartialLinkText(driver, UniqueKey_Franchisee.get("franchiseeName"));
					fc.utobj().sleep(2500);
				} catch (Exception ex) {
					fc.utobj().throwsException("Franchise Location not found in View Page" + testCaseId);
				}
				fc.utobj().clickElement(driver,
						driver.findElement(By.xpath(".//*[@id='fimUl']//a[contains(text(),'Training')]")));
				UniqueKey_Training.remove("referenceId");
				UniqueKey_Training.remove("parentReferenceId");
				ArrayList<String> listItems = new ArrayList<String>(UniqueKey_Training.values());
				fc.utobj()
						.printTestStep("Navigating to InfoMgr > RestAPI > Validating Training for Franchise Location");
				boolean Status = fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);
				if (Status)
					fc.utobj().throwsException("Training is not found on view page " + testCaseId);
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgrRestAPI", "FCRestAPI" })
	@TestCase(createdOn = "2017-06-16", updatedOn = "2017-06-16", testCaseDescription = "Delete Call subModule for Info Mgr In RestAPI", testCaseId = "TC_03_Fill_Call_Tab_Info_Mgr_Delete")
	public void DeleteCallInfoMgrRest() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("infomgrRestAPI", testCaseId);
		Map<String, String> UniqueKey_Franchisee = new HashMap<String, String>();
		Map<String, String> UniqueKey_CorpUser = new HashMap<String, String>();
		Map<String, String> UniqueKey_Call = new HashMap<String, String>();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			UserCreation uc = new UserCreation();
			InfoMgrRestService infomgrRest = new InfoMgrRestService();
			fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Corporate User");
			boolean createUser = uc.createCorpUser(driver, config, dataSet, testCaseId, UniqueKey_CorpUser);
			String Franchisee = null;
			if (createUser) {
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Franchisee Location");
				Franchisee = infomgrRest.AddLocation(driver, dataSet, config, UniqueKey_Franchisee, testCaseId);
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Call");
				infomgrRest.CreateCallRestAPI(driver, dataSet, config, testCaseId, UniqueKey_Call);

				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Deleting Call");
				infomgrRest.DeleteCallRestAPI(driver, dataSet, config, testCaseId, UniqueKey_Call);
			}
			if (Franchisee != null) {
				fc.loginpage().login(driver);
				fc.infomgr().infomgr_common().InfoMgrCorporateLocations(driver);

				InfoMgrCorporateLocationsPage objFranPage = new InfoMgrCorporateLocationsPage(driver);
				try {
					try {
						fc.utobj().clickElement(driver, objFranPage.lnkShowAll);
					} catch (Exception ex) {
					}
					fc.utobj().clickPartialLinkText(driver, UniqueKey_Franchisee.get("franchiseeName"));
					fc.utobj().sleep(2500);
				} catch (Exception ex) {
					fc.utobj().throwsException("Franchise Location not found in View Page" + testCaseId);
				}
				UniqueKey_Call.remove("referenceId");
				UniqueKey_Call.remove("parentReferenceId");
				UniqueKey_Call.remove("timeAdded");
				UniqueKey_Franchisee.put("calldate", UniqueKey_Call.get("date"));
				ArrayList<String> listItems = new ArrayList<String>(UniqueKey_Call.values());
				boolean Status = infomgrRest.ValidateForExportCall(driver, config, listItems, DataServicePage.Call,
						UniqueKey_Franchisee, UniqueKey_Call.get("subject"));
				if (Status)
					fc.utobj().throwsException("Call  on view page " + testCaseId);
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgrRestAPI", "FCRestAPI" })
	@TestCase(createdOn = "2017-06-16", updatedOn = "2017-06-16", testCaseDescription = "Delete Financial subModule for Info Mgr In RestAPI", testCaseId = "TC_09_Fill_Financial_Tab_Info_Mgr_Delete")
	public void DeleteFinancialInfoMgrRest() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("infomgrRestAPI", testCaseId);
		Map<String, String> UniqueKey_Franchisee = new HashMap<String, String>();
		Map<String, String> UniqueKey_CorpUser = new HashMap<String, String>();
		Map<String, String> UniqueKey_Financial = new HashMap<String, String>();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			UserCreation uc = new UserCreation();
			InfoMgrRestService infomgrRest = new InfoMgrRestService();
			fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Corporate User");
			boolean createUser = uc.createCorpUser(driver, config, dataSet, testCaseId, UniqueKey_CorpUser);
			String Franchisee = null;
			if (createUser) {
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Franchisee Location");
				Franchisee = infomgrRest.AddLocation(driver, dataSet, config, UniqueKey_Franchisee, testCaseId);
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Financial");
				infomgrRest.CreateFinancialRestAPI(driver, dataSet, config, testCaseId, UniqueKey_Financial);

				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Deleting Financial");
				infomgrRest.DeleteFinancialRestAPI(driver, dataSet, config, testCaseId, UniqueKey_Financial);
			}
			if (Franchisee != null) {
				fc.loginpage().login(driver);
				fc.infomgr().infomgr_common().InfoMgrCorporateLocations(driver);

				InfoMgrCorporateLocationsPage objFranPage = new InfoMgrCorporateLocationsPage(driver);
				try {
					try {
						fc.utobj().clickElement(driver, objFranPage.lnkShowAll);
					} catch (Exception ex) {
					}
					fc.utobj().clickPartialLinkText(driver, UniqueKey_Franchisee.get("franchiseeName"));
					fc.utobj().sleep(2500);
				} catch (Exception ex) {
					fc.utobj().throwsException("Franchise Location not found in View Page" + testCaseId);
				}
				// fc.utobj().clickPartialLinkText(driver, "Events");
				// UniqueKey_Events.remove("referenceId");
				// UniqueKey_Events.remove("parentReferenceId");
				// ArrayList<String> listItems = new
				// ArrayList<String>(UniqueKey_Events.values());
				// boolean Status =
				// fc.utobj().assertPageSourceWithMultipleRecords(driver,
				// listItems);
				// if (Status)
				// fc.utobj().throwsException("Events on view page
				// " + testCaseId);
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgrRestAPI", "FCRestAPI" })
	@TestCase(createdOn = "2017-06-16", updatedOn = "2017-06-16", testCaseDescription = "Delete Gaurantor subModule for Info Mgr In RestAPI", testCaseId = "TC_10_Fill_Gaurantor_Tab_Info_Mgr_Delete")
	public void DeleteGaurantorInfoMgrRest() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("infomgrRestAPI", testCaseId);
		Map<String, String> UniqueKey_Franchisee = new HashMap<String, String>();
		Map<String, String> UniqueKey_CorpUser = new HashMap<String, String>();
		Map<String, String> UniqueKey_Gaurantor = new HashMap<String, String>();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			UserCreation uc = new UserCreation();
			InfoMgrRestService infomgrRest = new InfoMgrRestService();
			fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Corporate User");
			boolean createUser = uc.createCorpUser(driver, config, dataSet, testCaseId, UniqueKey_CorpUser);
			String Franchisee = null;
			if (createUser) {
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Franchisee Location");
				Franchisee = infomgrRest.AddLocation(driver, dataSet, config, UniqueKey_Franchisee, testCaseId);
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Guarantors");
				fc.utobj().sleep(2500);
				infomgrRest.CreateGaurantorRestAPI(driver, dataSet, config, testCaseId, UniqueKey_Gaurantor);
				fc.utobj().sleep(2500);

				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Deleting Guarantors");
				infomgrRest.DeleteGaurantorRestAPI(driver, dataSet, config, testCaseId, UniqueKey_Gaurantor);
			}
			if (Franchisee != null) {
				fc.loginpage().login(driver);
				fc.infomgr().infomgr_common().InfoMgrCorporateLocations(driver);

				InfoMgrCorporateLocationsPage objFranPage = new InfoMgrCorporateLocationsPage(driver);
				try {
					try {
						fc.utobj().clickElement(driver, objFranPage.lnkShowAll);
					} catch (Exception ex) {
					}
					fc.utobj().clickPartialLinkText(driver, UniqueKey_Franchisee.get("franchiseeName"));
					fc.utobj().sleep(2500);
				} catch (Exception ex) {
					fc.utobj().throwsException("Franchise Location not found in View Page" + testCaseId);
				}
				fc.utobj().clickPartialLinkText(driver, "Guarantors");
				UniqueKey_Gaurantor.remove("referenceId");
				UniqueKey_Gaurantor.remove("parentReferenceId");
				ArrayList<String> listItems = new ArrayList<String>(UniqueKey_Gaurantor.values());
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Validating Guarantors");
				boolean Status = fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);
				if (Status)
					fc.utobj().throwsException("Gaurantor found on view page " + testCaseId);
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgrRestAPI", "FCRestAPI" })
	@TestCase(createdOn = "2017-06-16", updatedOn = "2017-06-16", testCaseDescription = "Delete Insurance subModule for Info Mgr In RestAPI", testCaseId = "TC_11_Fill_Insurance_Tab_Info_Mgr_Delete")
	public void DeleteInsuranceInfoMgrRest() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("infomgrRestAPI", testCaseId);
		Map<String, String> UniqueKey_Franchisee = new HashMap<String, String>();
		Map<String, String> UniqueKey_CorpUser = new HashMap<String, String>();
		Map<String, String> UniqueKey_Insurance = new HashMap<String, String>();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			UserCreation uc = new UserCreation();
			InfoMgrRestService infomgrRest = new InfoMgrRestService();
			fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Corporate User");
			boolean createUser = uc.createCorpUser(driver, config, dataSet, testCaseId, UniqueKey_CorpUser);
			String Franchisee = null;
			if (createUser) {
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Franchisee Location");
				Franchisee = infomgrRest.AddLocation(driver, dataSet, config, UniqueKey_Franchisee, testCaseId);
				fc.utobj().sleep(2500);
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding INsurancce");
				infomgrRest.CreateInsuranceRestAPI(driver, dataSet, config, testCaseId, UniqueKey_Insurance);
				fc.utobj().sleep(2500);
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Deleting Insurance");
				infomgrRest.DeleteInsuranceRestAPI(driver, dataSet, config, testCaseId, UniqueKey_Insurance);
			}
			if (Franchisee != null) {
				fc.loginpage().login(driver);
				fc.infomgr().infomgr_common().InfoMgrCorporateLocations(driver);

				InfoMgrCorporateLocationsPage objFranPage = new InfoMgrCorporateLocationsPage(driver);
				try {
					try {
						fc.utobj().clickElement(driver, objFranPage.lnkShowAll);
					} catch (Exception ex) {
					}
					fc.utobj().clickPartialLinkText(driver, UniqueKey_Franchisee.get("franchiseeName"));
					fc.utobj().sleep(2500);
				} catch (Exception ex) {
					fc.utobj().throwsException("Franchise Location not found in View Page" + testCaseId);
				}
				fc.utobj().clickPartialLinkText(driver, "Insurance");
				UniqueKey_Insurance.remove("referenceId");
				UniqueKey_Insurance.remove("parentReferenceId");
				ArrayList<String> listItems = new ArrayList<String>(UniqueKey_Insurance.values());
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Validating Insurance");
				boolean Status = fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);
				if (Status)
					fc.utobj().throwsException("Insurance found on view page " + testCaseId);
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgrRestAPI", "FCRestAPI" })
	@TestCase(createdOn = "2017-06-16", updatedOn = "2017-06-16", testCaseDescription = "Delete LegalViolation subModule for Info Mgr In RestAPI", testCaseId = "TC_12_Fill_LeagalViolation_Tab_Info_Mgr_Delete")
	public void DeleteLegalViolationInfoMgrRest()
			throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("infomgrRestAPI", testCaseId);
		Map<String, String> UniqueKey_Franchisee = new HashMap<String, String>();
		Map<String, String> UniqueKey_CorpUser = new HashMap<String, String>();
		Map<String, String> UniqueKey_LegalViolation = new HashMap<String, String>();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			UserCreation uc = new UserCreation();
			InfoMgrRestService infomgrRest = new InfoMgrRestService();
			fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Corporate User");
			boolean createUser = uc.createCorpUser(driver, config, dataSet, testCaseId, UniqueKey_CorpUser);
			String Franchisee = null;
			if (createUser) {
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Franchisee Location");
				Franchisee = infomgrRest.AddLocation(driver, dataSet, config, UniqueKey_Franchisee, testCaseId);
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Legal Violation");
				fc.utobj().sleep(2500);
				infomgrRest.CreateLegalViolationRestAPI(driver, dataSet, config, testCaseId, UniqueKey_LegalViolation);
				fc.utobj().sleep(2500);
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Deleting Legal Violation");
				infomgrRest.DeleteLegalViolationRestAPI(driver, dataSet, config, testCaseId, UniqueKey_LegalViolation);
			}
			if (Franchisee != null) {
				fc.loginpage().login(driver);
				fc.infomgr().infomgr_common().InfoMgrCorporateLocations(driver);

				InfoMgrCorporateLocationsPage objFranPage = new InfoMgrCorporateLocationsPage(driver);
				try {
					try {
						fc.utobj().clickElement(driver, objFranPage.lnkShowAll);
					} catch (Exception ex) {
					}
					fc.utobj().clickPartialLinkText(driver, UniqueKey_Franchisee.get("franchiseeName"));
					fc.utobj().sleep(2500);
				} catch (Exception ex) {
					fc.utobj().throwsException("Franchise Location not found in View Page" + testCaseId);
				}
				fc.utobj().clickPartialLinkText(driver, "Legal Violation");
				UniqueKey_LegalViolation.remove("referenceId");
				UniqueKey_LegalViolation.remove("parentReferenceId");
				ArrayList<String> listItems = new ArrayList<String>(UniqueKey_LegalViolation.values());
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Validating Legal Violation");
				boolean Status = fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);
				if (Status)
					fc.utobj().throwsException("Legal Violation found on view page " + testCaseId);
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgrRestAPI", "FCRestAPI" })
	@TestCase(createdOn = "2017-06-16", updatedOn = "2017-06-16", testCaseDescription = "Delete Lenders subModule for Info Mgr In RestAPI", testCaseId = "TC_13_Fill_Lenders_Tab_Info_Mgr_Delete")
	public void DeleteLendersInfoMgrRest() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("infomgrRestAPI", testCaseId);
		Map<String, String> UniqueKey_Franchisee = new HashMap<String, String>();
		Map<String, String> UniqueKey_CorpUser = new HashMap<String, String>();
		Map<String, String> UniqueKey_Lenders = new HashMap<String, String>();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			UserCreation uc = new UserCreation();
			InfoMgrRestService infomgrRest = new InfoMgrRestService();
			fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Corporate User");
			boolean createUser = uc.createCorpUser(driver, config, dataSet, testCaseId, UniqueKey_CorpUser);
			String Franchisee = null;
			if (createUser) {
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Franchisee Location");
				fc.utobj().sleep(2500);
				Franchisee = infomgrRest.AddLocation(driver, dataSet, config, UniqueKey_Franchisee, testCaseId);
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Lenders");
				fc.utobj().sleep(2500);
				infomgrRest.CreateLendersRestAPI(driver, dataSet, config, testCaseId, UniqueKey_Lenders);
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Deleting Lenders");
				fc.utobj().sleep(2500);
				infomgrRest.DeleteLendersRestAPI(driver, dataSet, config, testCaseId, UniqueKey_Lenders);
			}
			if (Franchisee != null) {
				fc.loginpage().login(driver);
				fc.infomgr().infomgr_common().InfoMgrCorporateLocations(driver);

				InfoMgrCorporateLocationsPage objFranPage = new InfoMgrCorporateLocationsPage(driver);
				try {
					try {
						fc.utobj().clickElement(driver, objFranPage.lnkShowAll);
					} catch (Exception ex) {
					}
					fc.utobj().clickPartialLinkText(driver, UniqueKey_Franchisee.get("franchiseeName"));
					fc.utobj().sleep(2500);
				} catch (Exception ex) {
					fc.utobj().throwsException("Franchise Location not found in View Page" + testCaseId);
				}
				fc.utobj().clickPartialLinkText(driver, "Lenders");
				UniqueKey_Lenders.remove("referenceId");
				UniqueKey_Lenders.remove("parentReferenceId");
				ArrayList<String> listItems = new ArrayList<String>(UniqueKey_Lenders.values());
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Validating Lenders");
				boolean Status = fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);
				if (Status)
					fc.utobj().throwsException("Lenders found on view page " + testCaseId);
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgrRestAPI", "FCRestAPI" })
	@TestCase(createdOn = "2017-06-16", updatedOn = "2017-06-16", testCaseDescription = "Delete Lenders ExternalMail for Info Mgr In RestAPI", testCaseId = "TC_14_Fill_ExternalMail_Tab_Info_Mgr_Delete")
	public void DeleteExternalMailInfoMgrRest()
			throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("infomgrRestAPI", testCaseId);
		Map<String, String> UniqueKey_Franchisee = new HashMap<String, String>();
		Map<String, String> UniqueKey_CorpUser = new HashMap<String, String>();
		Map<String, String> UniqueKey_ExternalMails = new HashMap<String, String>();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			UserCreation uc = new UserCreation();
			InfoMgrRestService infomgrRest = new InfoMgrRestService();
			fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Corporate User");
			boolean createUser = uc.createCorpUser(driver, config, dataSet, testCaseId, UniqueKey_CorpUser);
			String Franchisee = null;
			if (createUser) {
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Franchisee Location");
				Franchisee = infomgrRest.AddLocation(driver, dataSet, config, UniqueKey_Franchisee, testCaseId);
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding External Mail");
				infomgrRest.CreateExternalMailsRestAPI(driver, dataSet, config, testCaseId, UniqueKey_ExternalMails);
			}
			if (Franchisee != null) {
				fc.loginpage().login(driver);
				fc.infomgr().infomgr_common().InfoMgrCorporateLocations(driver);

				InfoMgrCorporateLocationsPage objFranPage = new InfoMgrCorporateLocationsPage(driver);
				try {
					try {
						fc.utobj().clickElement(driver, objFranPage.lnkShowAll);
					} catch (Exception ex) {
					}
					fc.utobj().clickPartialLinkText(driver, UniqueKey_Franchisee.get("franchiseeName"));
					fc.utobj().sleep(2500);
				} catch (Exception ex) {
					fc.utobj().throwsException("Franchise Location not found in View Page" + testCaseId);
				}
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgrRestAPI", "FCRestAPI" })
	@TestCase(createdOn = "2017-06-16", updatedOn = "2017-06-16", testCaseDescription = "Delete Lenders Marketing for Info Mgr In RestAPI", testCaseId = "TC_15_Fill_Marketing_Tab_Info_Mgr_Delete")
	public void DeleteMarketingInfoMgrRest() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("infomgrRestAPI", testCaseId);
		Map<String, String> UniqueKey_Franchisee = new HashMap<String, String>();
		Map<String, String> UniqueKey_CorpUser = new HashMap<String, String>();
		Map<String, String> UniqueKey_Marketing = new HashMap<String, String>();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			UserCreation uc = new UserCreation();
			InfoMgrRestService infomgrRest = new InfoMgrRestService();
			fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Corporate User");
			boolean createUser = uc.createCorpUser(driver, config, dataSet, testCaseId, UniqueKey_CorpUser);
			String Franchisee = null;
			if (createUser) {
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Franchisee Location");
				Franchisee = infomgrRest.AddLocation(driver, dataSet, config, UniqueKey_Franchisee, testCaseId);
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Marketign");
				fc.utobj().sleep(2500);
				infomgrRest.CreateMarketingRestAPI(driver, dataSet, config, testCaseId, UniqueKey_Marketing);
				fc.utobj().sleep(2500);
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Deleting Marketing");
				infomgrRest.DeleteMarketingRestAPI(driver, dataSet, config, testCaseId, UniqueKey_Marketing);
			}
			if (Franchisee != null) {
				fc.loginpage().login(driver);
				fc.infomgr().infomgr_common().InfoMgrCorporateLocations(driver);

				InfoMgrCorporateLocationsPage objFranPage = new InfoMgrCorporateLocationsPage(driver);
				try {
					try {
						fc.utobj().clickElement(driver, objFranPage.lnkShowAll);
					} catch (Exception ex) {
					}
					fc.utobj().clickPartialLinkText(driver, UniqueKey_Franchisee.get("franchiseeName"));
					fc.utobj().sleep(2500);
				} catch (Exception ex) {
					fc.utobj().throwsException("Franchise Location not found in View Page" + testCaseId);
				}
				fc.utobj().clickPartialLinkText(driver, "Marketing");
				UniqueKey_Marketing.remove("referenceId");
				UniqueKey_Marketing.remove("parentReferenceId");
				ArrayList<String> listItems = new ArrayList<String>(UniqueKey_Marketing.values());
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Validating Marketing");
				boolean Status = fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);
				if (Status)
					fc.utobj().throwsException("Marketing found on view page " + testCaseId);
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgrRestAPI", "FCRestAPI" })
	@TestCase(createdOn = "2017-06-16", updatedOn = "2017-06-16", testCaseDescription = "Delete Mystery VIew for Info Mgr In RestAPI", testCaseId = "TC_16_Fill_MysteryReview_Tab_Info_Mgr_Delete")
	public void DeleteMysteryReviewInfoMgrRest()
			throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("infomgrRestAPI", testCaseId);
		Map<String, String> UniqueKey_Franchisee = new HashMap<String, String>();
		Map<String, String> UniqueKey_CorpUser = new HashMap<String, String>();
		Map<String, String> UniqueKey_MysteryReview = new HashMap<String, String>();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			UserCreation uc = new UserCreation();
			InfoMgrRestService infomgrRest = new InfoMgrRestService();
			fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Corporate User");
			boolean createUser = uc.createCorpUser(driver, config, dataSet, testCaseId, UniqueKey_CorpUser);
			String Franchisee = null;
			if (createUser) {
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Franchisee Location");
				Franchisee = infomgrRest.AddLocation(driver, dataSet, config, UniqueKey_Franchisee, testCaseId);
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding MysteryReview");
				fc.utobj().sleep(2500);
				infomgrRest.CreateMysteryReviewRestAPI(driver, dataSet, config, testCaseId, UniqueKey_MysteryReview);
				fc.utobj().sleep(2500);
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Deleting MysteryReview");
				infomgrRest.DeleteMysteryReviewRestAPI(driver, dataSet, config, testCaseId, UniqueKey_MysteryReview);
			}
			if (Franchisee != null) {
				fc.loginpage().login(driver);
				fc.infomgr().infomgr_common().InfoMgrCorporateLocations(driver);

				InfoMgrCorporateLocationsPage objFranPage = new InfoMgrCorporateLocationsPage(driver);
				try {
					try {
						fc.utobj().clickElement(driver, objFranPage.lnkShowAll);
					} catch (Exception ex) {
					}
					;
					fc.utobj().clickPartialLinkText(driver, UniqueKey_Franchisee.get("franchiseeName"));
					fc.utobj().sleep(2500);
				} catch (Exception ex) {
					fc.utobj().throwsException("Franchise Location not found in View Page" + testCaseId);
				}
				fc.utobj().clickPartialLinkText(driver, "Mystery Review");
				;
				UniqueKey_MysteryReview.remove("referenceId");
				UniqueKey_MysteryReview.remove("parentReferenceId");
				ArrayList<String> listItems = new ArrayList<String>(UniqueKey_MysteryReview.values());
				;
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Validating MysteryReview");
				boolean Status = fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);
				if (Status)
					fc.utobj().throwsException("Marketing found on view page " + testCaseId);
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgrRestAPI", "FCRestAPI" })
	@TestCase(createdOn = "2017-06-16", updatedOn = "2017-06-16", testCaseDescription = "Delete Owner for Info Mgr In RestAPI", testCaseId = "TC_17_Fill_Owner_Tab_Info_Mgr_Delete")
	public void DeleteOwnerInfoMgrRest() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("infomgrRestAPI", testCaseId);
		Map<String, String> UniqueKey_Franchisee = new HashMap<String, String>();
		Map<String, String> UniqueKey_CorpUser = new HashMap<String, String>();
		Map<String, String> UniqueKey_Owners = new HashMap<String, String>();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			UserCreation uc = new UserCreation();
			InfoMgrRestService infomgrRest = new InfoMgrRestService();
			fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Corporate User");
			boolean createUser = uc.createCorpUser(driver, config, dataSet, testCaseId, UniqueKey_CorpUser);
			String Franchisee = null;
			if (createUser) {
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Franchisee Location");
				Franchisee = infomgrRest.AddLocation(driver, dataSet, config, UniqueKey_Franchisee, testCaseId);
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Owners");
				fc.utobj().sleep(2500);
				infomgrRest.CreateOwnersRestAPI(driver, dataSet, config, testCaseId, UniqueKey_Owners);
				fc.utobj().sleep(2500);
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Deleting Owners");
				infomgrRest.DeleteOwnersRestAPI(driver, dataSet, config, testCaseId, UniqueKey_Owners);
			}
			if (Franchisee != null) {
				fc.loginpage().login(driver);
				fc.infomgr().infomgr_common().InfoMgrCorporateLocations(driver);

				InfoMgrCorporateLocationsPage objFranPage = new InfoMgrCorporateLocationsPage(driver);
				try {
					try {
						fc.utobj().clickElement(driver, objFranPage.lnkShowAll);
					} catch (Exception ex) {
					}
					fc.utobj().clickPartialLinkText(driver, UniqueKey_Franchisee.get("franchiseeName"));
					fc.utobj().sleep(2500);
				} catch (Exception ex) {
					fc.utobj().throwsException("Franchise Location not found in View Page" + testCaseId);
				}
				fc.utobj().clickPartialLinkText(driver, "Owners");
				UniqueKey_Owners.remove("referenceId");
				UniqueKey_Owners.remove("parentReferenceId");
				UniqueKey_Owners.remove("ownerType");
				ArrayList<String> listItems = new ArrayList<String>(UniqueKey_Owners.values());
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Validating Owners");
				boolean Status = fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);
				if (Status)
					fc.utobj().throwsException("Owner found on view page " + testCaseId);
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgrRestAPI", "FCRestAPI" })
	@TestCase(createdOn = "2017-06-16", updatedOn = "2017-06-16", testCaseDescription = "Delete Territory for Info Mgr In RestAPI", testCaseId = "TC_18_Fill_Territory_Tab_Info_Mgr_Delete")
	public void DeleteTerritoryInfoMgrRest() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("infomgrRestAPI", testCaseId);
		Map<String, String> UniqueKey_Franchisee = new HashMap<String, String>();
		Map<String, String> UniqueKey_CorpUser = new HashMap<String, String>();
		Map<String, String> UniqueKey_Territory = new HashMap<String, String>();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			UserCreation uc = new UserCreation();
			InfoMgrRestService infomgrRest = new InfoMgrRestService();
			fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Corporate User");
			boolean createUser = uc.createCorpUser(driver, config, dataSet, testCaseId, UniqueKey_CorpUser);
			String Franchisee = null;
			if (createUser) {
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Franchisee Location");
				Franchisee = infomgrRest.AddLocation(driver, dataSet, config, UniqueKey_Franchisee, testCaseId);
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Territory");
				fc.utobj().sleep(2500);
				infomgrRest.CreateTerritoryRestAPI(driver, dataSet, config, testCaseId, UniqueKey_Territory);
				fc.utobj().sleep(2500);
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Deleting Territory");
				infomgrRest.DeleteTerritoryRestAPI(driver, dataSet, config, testCaseId, UniqueKey_Territory);
			}
			if (Franchisee != null) {
				fc.loginpage().login(driver);
				fc.infomgr().infomgr_common().InfoMgrCorporateLocations(driver);

				InfoMgrCorporateLocationsPage objFranPage = new InfoMgrCorporateLocationsPage(driver);
				try {
					try {
						fc.utobj().clickElement(driver, objFranPage.lnkShowAll);
					} catch (Exception ex) {
					}
					fc.utobj().clickPartialLinkText(driver, UniqueKey_Franchisee.get("franchiseeName"));
					fc.utobj().sleep(2500);
				} catch (Exception ex) {
					fc.utobj().throwsException("Franchise Location not found in View Page" + testCaseId);
				}
				fc.utobj().clickPartialLinkText(driver, "Territory");
				UniqueKey_Territory.remove("referenceId");
				UniqueKey_Territory.remove("parentReferenceId");
				ArrayList<String> listItems = new ArrayList<String>(UniqueKey_Territory.values());
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Validating Territory");
				boolean Status = fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);
				if (Status)
					fc.utobj().throwsException("Territory found on view page " + testCaseId);
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgrRestAPI", "FCRestAPI" })
	@TestCase(createdOn = "2017-06-16", updatedOn = "2017-06-16", testCaseDescription = "Delete Renewal for Info Mgr In RestAPI", testCaseId = "TC_19_Fill_Renewal_Tab_Info_Mgr_Delete")
	public void DeleteRenewalInfoMgrRest() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("infomgrRestAPI", testCaseId);
		Map<String, String> UniqueKey_Franchisee = new HashMap<String, String>();
		Map<String, String> UniqueKey_CorpUser = new HashMap<String, String>();
		Map<String, String> UniqueKey_Renewal = new HashMap<String, String>();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			UserCreation uc = new UserCreation();
			InfoMgrRestService infomgrRest = new InfoMgrRestService();
			fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Corporate User");
			boolean createUser = uc.createCorpUser(driver, config, dataSet, testCaseId, UniqueKey_CorpUser);
			String Franchisee = null;
			if (createUser) {
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Franchisee Location");
				Franchisee = infomgrRest.AddLocation(driver, dataSet, config, UniqueKey_Franchisee, testCaseId);
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Renewal");
				infomgrRest.CreateRenewalRestAPI(driver, dataSet, config, testCaseId, UniqueKey_Renewal);
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Deleting Renewal");
				infomgrRest.DeleteRenewalRestAPI(driver, dataSet, config, testCaseId, UniqueKey_Renewal);
			}
			if (Franchisee != null) {
				fc.loginpage().login(driver);
				fc.infomgr().infomgr_common().InfoMgrCorporateLocations(driver);

				InfoMgrCorporateLocationsPage objFranPage = new InfoMgrCorporateLocationsPage(driver);
				try {
					try {
						fc.utobj().clickElement(driver, objFranPage.lnkShowAll);
					} catch (Exception ex) {
					}
					fc.utobj().clickPartialLinkText(driver, UniqueKey_Franchisee.get("franchiseeName"));
					fc.utobj().sleep(2500);
				} catch (Exception ex) {
					fc.utobj().throwsException("Franchise Location not found in View Page" + testCaseId);
				}
				fc.utobj().clickPartialLinkText(driver, "Renewal");
				UniqueKey_Renewal.remove("referenceId");
				UniqueKey_Renewal.remove("parentReferenceId");
				List<String> listItems = new ArrayList<String>(UniqueKey_Renewal.values());
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Validating Renewal");
				boolean Status = fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);
				if (Status)
					fc.utobj().throwsException("Renewal found on view page " + testCaseId);
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgrRestAPI", "FCRestAPI" })
	@TestCase(createdOn = "2017-06-16", updatedOn = "2017-06-16", testCaseDescription = "Delete RealEstate for Info Mgr In RestAPI", testCaseId = "TC_19_Fill_RealEstate_Tab_Info_Mgr_Delete")
	public void DeleteRealEstateInfoMgrRest()
			throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("infomgrRestAPI", testCaseId);
		Map<String, String> UniqueKey_Franchisee = new HashMap<String, String>();
		Map<String, String> UniqueKey_CorpUser = new HashMap<String, String>();
		Map<String, String> UniqueKey_RealEstate = new HashMap<String, String>();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			UserCreation uc = new UserCreation();
			InfoMgrRestService infomgrRest = new InfoMgrRestService();
			fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Corporate User");
			boolean createUser = uc.createCorpUser(driver, config, dataSet, testCaseId, UniqueKey_CorpUser);
			String Franchisee = null;
			if (createUser) {
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Franchisee Location");
				Franchisee = infomgrRest.AddLocation(driver, dataSet, config, UniqueKey_Franchisee, testCaseId);
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Real Estate");
				fc.utobj().sleep(2500);
				infomgrRest.CreateRealEstateRestAPI(driver, dataSet, config, testCaseId, UniqueKey_RealEstate);
				fc.utobj().sleep(2500);
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Deleting Real Estate");
				infomgrRest.DeleteRealEstateRestAPI(driver, dataSet, config, testCaseId, UniqueKey_RealEstate);
			}
			if (Franchisee != null) {
				fc.loginpage().login(driver);
				fc.infomgr().infomgr_common().InfoMgrCorporateLocations(driver);

				InfoMgrCorporateLocationsPage objFranPage = new InfoMgrCorporateLocationsPage(driver);
				try {
					try {
						fc.utobj().clickElement(driver, objFranPage.lnkShowAll);
					} catch (Exception ex) {
					}
					fc.utobj().clickPartialLinkText(driver, UniqueKey_Franchisee.get("franchiseeName"));
					fc.utobj().sleep(2500);
				} catch (Exception ex) {
					fc.utobj().throwsException("Franchise Location not found in View Page" + testCaseId);
				}
				fc.utobj().clickPartialLinkText(driver, "Real Estate");
				UniqueKey_RealEstate.remove("referenceId");
				UniqueKey_RealEstate.remove("parentReferenceId");
				ArrayList<String> listItems = new ArrayList<String>(UniqueKey_RealEstate.values());
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Validating Real Estate");
				boolean Status = fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);
				if (Status)
					fc.utobj().throwsException("Real Estate found on view page " + testCaseId);
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgrRestAPI", "FCRestAPI" })
	@TestCase(createdOn = "2017-06-16", updatedOn = "2017-06-16", testCaseDescription = "Delete Remark for Info Mgr In RestAPI", testCaseId = "TC_20_Fill_Remark_Tab_Info_Mgr_Delete")
	public void DeleteRemarkInfoMgrRest() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("infomgrRestAPI", testCaseId);
		Map<String, String> UniqueKey_Franchisee = new HashMap<String, String>();
		Map<String, String> UniqueKey_CorpUser = new HashMap<String, String>();
		Map<String, String> UniqueKey_Remark = new HashMap<String, String>();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			UserCreation uc = new UserCreation();
			InfoMgrRestService infomgrRest = new InfoMgrRestService();
			fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Corporate User");
			boolean createUser = uc.createCorpUser(driver, config, dataSet, testCaseId, UniqueKey_CorpUser);
			String Franchisee = null;
			if (createUser) {
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Franchisee Location");
				Franchisee = infomgrRest.AddLocation(driver, dataSet, config, UniqueKey_Franchisee, testCaseId);
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Remarks");
				infomgrRest.CreateRemarkRestAPI(driver, dataSet, config, testCaseId, UniqueKey_Remark);
			}
			if (Franchisee != null) {
				fc.loginpage().login(driver);
				fc.infomgr().infomgr_common().InfoMgrCorporateLocations(driver);
				fc.utobj().printTestStep("Navigating to InfoMgr > Validating Franchisee Location on VIew Page");
				InfoMgrCorporateLocationsPage objFranPage = new InfoMgrCorporateLocationsPage(driver);
				try {
					try {
						fc.utobj().clickElement(driver, objFranPage.lnkShowAll);
					} catch (Exception ex) {
					}
					fc.utobj().clickPartialLinkText(driver, UniqueKey_Franchisee.get("franchiseeName"));
					fc.utobj().sleep(2500);
				} catch (Exception ex) {
					fc.utobj().throwsException("Franchise Location not found in View Page" + testCaseId);
				}

			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgrRestAPI", "FCRestAPI" })
	@TestCase(createdOn = "2017-06-16", updatedOn = "2017-06-16", testCaseDescription = "Delete Task for Info Mgr In RestAPI", testCaseId = "TC_21_Fill_Task_Tab_Info_Mgr")
	public void DeleteTaskInfoMgrRest() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("infomgrRestAPI", testCaseId);
		Map<String, String> UniqueKey_Franchisee = new HashMap<String, String>();
		Map<String, String> UniqueKey_CorpUser = new HashMap<String, String>();
		Map<String, String> UniqueKey_Task = new HashMap<String, String>();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			UserCreation uc = new UserCreation();
			InfoMgrRestService infomgrRest = new InfoMgrRestService();
			fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Corporate User");
			boolean createUser = uc.createCorpUser(driver, config, dataSet, testCaseId, UniqueKey_CorpUser);
			String Franchisee = null;
			if (createUser) {
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Franchisee Location");
				Franchisee = infomgrRest.AddLocation(driver, dataSet, config, UniqueKey_Franchisee, testCaseId);
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Task");
				infomgrRest.CreateTaskRestAPI(driver, dataSet, config, testCaseId, UniqueKey_Task, UniqueKey_CorpUser);
			}
			if (Franchisee != null) {
				fc.loginpage().login(driver);
				fc.infomgr().infomgr_common().InfoMgrCorporateLocations(driver);

				InfoMgrCorporateLocationsPage objFranPage = new InfoMgrCorporateLocationsPage(driver);
				try {
					try {
						fc.utobj().clickElement(driver, objFranPage.lnkShowAll);
					} catch (Exception ex) {
					}
					fc.utobj().clickPartialLinkText(driver, UniqueKey_Franchisee.get("franchiseeName"));
					fc.utobj().sleep(2500);
				} catch (Exception ex) {
					fc.utobj().throwsException("Franchise Location not found in View Page" + testCaseId);
				}

			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

}
