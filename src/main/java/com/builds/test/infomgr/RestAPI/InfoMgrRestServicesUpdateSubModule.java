package com.builds.test.infomgr.RestAPI;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import org.xml.sax.SAXException;

import com.builds.uimaps.infomgr.InfoMgrCorporateLocationsPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class InfoMgrRestServicesUpdateSubModule {

	FranconnectUtil fc = new FranconnectUtil();
	BaseRestUtil bru = new BaseRestUtil();

	@Test(groups = { "infomgrRestAPI", "FCRestAPI" })
	@TestCase(createdOn = "2017-06-16", updatedOn = "2017-06-16", testCaseDescription = "Add Franchise subModule for Info Mgr In RestAPI", testCaseId = "TC_01_Fill_Franchisee_Tab_Info_Mgr_Update")
	public void AddFranchiseInfoMgrRest() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("infomgrRestAPI", testCaseId);
		Map<String, String> UniqueKey_Franchisee = new HashMap<String, String>();
		Map<String, String> UniqueKey_CorpUser = new HashMap<String, String>();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			UserCreation uc = new UserCreation();
			InfoMgrRestService infomgrRest = new InfoMgrRestService();
			fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Corporate User");
			boolean createUser = uc.createCorpUser(driver, config, dataSet, testCaseId, UniqueKey_CorpUser);
			String Franchisee = null;
			if (createUser) {
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Franchisee Location");
				Franchisee = infomgrRest.AddLocation(driver, dataSet, config, UniqueKey_Franchisee, testCaseId);
			}
			fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Updating Franchisee Location");
			Franchisee = infomgrRest.UpdateLocation(driver, dataSet, config, UniqueKey_Franchisee, testCaseId);
			if (Franchisee != null) {
				fc.utobj().printTestStep("Validating Franchisee Loaction in InfoMgr on View Page");
				fc.loginpage().login(driver);
				fc.infomgr().infomgr_common().InfoMgrCorporateLocations(driver);

				InfoMgrCorporateLocationsPage objFranPage = new InfoMgrCorporateLocationsPage(driver);
				try {
					try {
						fc.utobj().clickElement(driver, objFranPage.lnkShowAll);
					} catch (Exception ex) {

					}
					fc.utobj().clickPartialLinkText(driver, UniqueKey_Franchisee.get("franchiseeName"));
				} catch (Exception ex) {
					fc.utobj().throwsException("Franchise Location not found in View Page" + testCaseId);
				}
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgrRestAPI", "FCRestAPI" })
	@TestCase(createdOn = "2017-06-16", updatedOn = "2017-06-16", testCaseDescription = "Add Agreement subModule for Info Mgr In RestAPI", testCaseId = "TC_02_Fill_Agreement_Tab_Info_Mgr_Update")
	public void AddAgreementInfoMgrRest() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("infomgrRestAPI", testCaseId);
		Map<String, String> UniqueKey_Franchisee = new HashMap<String, String>();
		Map<String, String> UniqueKey_CorpUser = new HashMap<String, String>();
		Map<String, String> UniqueKey_Agreement = new HashMap<String, String>();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			UserCreation uc = new UserCreation();
			InfoMgrRestService infomgrRest = new InfoMgrRestService();
			fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Corporate User");
			boolean createUser = uc.createCorpUser(driver, config, dataSet, testCaseId, UniqueKey_CorpUser);
			String Franchisee = null;
			if (createUser) {
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Franchisee Location");
				Franchisee = infomgrRest.AddLocation(driver, dataSet, config, UniqueKey_Franchisee, testCaseId);
				fc.utobj().sleep(2500);
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Agreement for Franchisee Location");
				infomgrRest.CreateAgreementRestAPI(driver, dataSet, config, testCaseId, UniqueKey_Agreement);
				fc.utobj().sleep(2500);
				fc.utobj()
						.printTestStep("Navigating to InfoMgr > RestAPI > Updating Agreement for Franchisee Location");
				infomgrRest.UpdateAgreementRestAPI(driver, dataSet, config, testCaseId, UniqueKey_Agreement);
			}
			if (Franchisee != null) {
				fc.utobj().printTestStep("Validating Franchisee Loaction in InfoMgr on View Page");
				fc.loginpage().login(driver);
				fc.infomgr().infomgr_common().InfoMgrCorporateLocations(driver);

				InfoMgrCorporateLocationsPage objFranPage = new InfoMgrCorporateLocationsPage(driver);
				try {
					try {
						fc.utobj().clickElement(driver, objFranPage.lnkShowAll);
					} catch (Exception ex) {
					}
					fc.utobj().clickPartialLinkText(driver, UniqueKey_Franchisee.get("franchiseeName"));
					fc.utobj().sleep(2500);
				} catch (Exception ex) {
					fc.utobj().throwsException("Franchise Location not found in View Page" + testCaseId);
				}
				fc.utobj().clickPartialLinkText(driver, "Agreement");
				UniqueKey_Agreement.remove("referenceId");
				UniqueKey_Agreement.remove("parentReferenceId");
				ArrayList<String> listItems = new ArrayList<String>(UniqueKey_Agreement.values());
				fc.utobj().printTestStep(
						"Navigating to InfoMgr > RestAPI > validating Agreement for Franchisee Location on view page");
				boolean Status = fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);
				if (!Status)
					fc.utobj().throwsException("Agreement is not found on view page " + testCaseId);
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgrRestAPI", "FCRestAPI" })
	@TestCase(createdOn = "2017-06-16", updatedOn = "2017-06-16", testCaseDescription = "Add CustomerComplain subModule for Info Mgr In RestAPI", testCaseId = "TC_04_Fill_CustomerComplain_Tab_Info_Mgr_Update")
	public void AddCustomerComplainInfoMgrRest()
			throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("infomgrRestAPI", testCaseId);
		Map<String, String> UniqueKey_Franchisee = new HashMap<String, String>();
		Map<String, String> UniqueKey_CorpUser = new HashMap<String, String>();
		Map<String, String> UniqueKey_CustomerComplain = new HashMap<String, String>();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			UserCreation uc = new UserCreation();
			InfoMgrRestService infomgrRest = new InfoMgrRestService();
			fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Corporate User");
			boolean createUser = uc.createCorpUser(driver, config, dataSet, testCaseId, UniqueKey_CorpUser);
			String Franchisee = null;
			if (createUser) {
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Franchisee Location");
				Franchisee = infomgrRest.AddLocation(driver, dataSet, config, UniqueKey_Franchisee, testCaseId);
				fc.utobj().printTestStep(
						"Navigating to InfoMgr > RestAPI > Adding Customer Complaing for Franchisee Location");
				infomgrRest.CreateCustomerComplainRestAPI(driver, dataSet, config, testCaseId,
						UniqueKey_CustomerComplain);
				fc.utobj().printTestStep(
						"Navigating to InfoMgr > RestAPI > Updating Customer Complaing for Franchisee Location");
				infomgrRest.UpdateCustomerComplainRestAPI(driver, dataSet, config, testCaseId,
						UniqueKey_CustomerComplain);
			}
			if (Franchisee != null) {
				fc.utobj().printTestStep("Validating Franchisee Loaction in InfoMgr on View Page");
				fc.loginpage().login(driver);
				fc.infomgr().infomgr_common().InfoMgrCorporateLocations(driver);

				InfoMgrCorporateLocationsPage objFranPage = new InfoMgrCorporateLocationsPage(driver);
				try {
					try {
						fc.utobj().clickElement(driver, objFranPage.lnkShowAll);
					} catch (Exception ex) {

					}
					fc.utobj().clickPartialLinkText(driver, UniqueKey_Franchisee.get("franchiseeName"));
					fc.utobj().sleep(2500);
				} catch (Exception ex) {
					fc.utobj().throwsException("Franchise Location not found in View Page" + testCaseId);
				}
				fc.utobj().clickPartialLinkText(driver, "Customer Complaints");
				UniqueKey_CustomerComplain.remove("referenceId");
				UniqueKey_CustomerComplain.remove("parentReferenceId");
				UniqueKey_CustomerComplain.put("fimDdIncidentDate", bru
						.changeFormat(UniqueKey_CustomerComplain.get("fimDdIncidentDate"), "yyyy-MM-dd", "MM/dd/yyyy"));
				UniqueKey_CustomerComplain.put("fimDdComplaintDate", bru.changeFormat(
						UniqueKey_CustomerComplain.get("fimDdComplaintDate"), "yyyy-MM-dd", "MM/dd/yyyy"));
				ArrayList<String> listItems = new ArrayList<String>(UniqueKey_CustomerComplain.values());
				fc.utobj().printTestStep(
						"Navigating to InfoMgr > RestAPI > Validating Customer Complain for Franchisee Location");
				boolean Status = fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);
				if (!Status)
					fc.utobj().throwsException("Customer Complaints is not found on view page " + testCaseId);
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgrRestAPI", "FCRestAPI"})
	@TestCase(createdOn = "2017-06-16", updatedOn = "2017-06-16", testCaseDescription = "Add EntityDetails subModule for Info Mgr In RestAPI", testCaseId = "TC_05_Fill_EntityDetails_Tab_Info_Mgr_Update")
	public void AddEntityDetailsInfoMgrRest()
			throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("infomgrRestAPI", testCaseId);
		Map<String, String> UniqueKey_Franchisee = new HashMap<String, String>();
		Map<String, String> UniqueKey_CorpUser = new HashMap<String, String>();
		Map<String, String> UniqueKey_EntityDetails = new HashMap<String, String>();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			UserCreation uc = new UserCreation();
			InfoMgrRestService infomgrRest = new InfoMgrRestService();
			fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Corporate User");
			boolean createUser = uc.createCorpUser(driver, config, dataSet, testCaseId, UniqueKey_CorpUser);
			String Franchisee = null;
			if (createUser) {
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Franchisee Location");
				Franchisee = infomgrRest.AddLocation(driver, dataSet, config, UniqueKey_Franchisee, testCaseId);
				fc.utobj().printTestStep(
						"Navigating to InfoMgr > RestAPI > Adding Entity Details for Franchisee Location");
				infomgrRest.CreateEntityDetailsRestAPI(driver, dataSet, config, testCaseId, UniqueKey_EntityDetails);
				fc.utobj().printTestStep(
						"Navigating to InfoMgr > RestAPI > Updating Entity Details for Franchisee Location");
				infomgrRest.UpdateEntityDetailsRestAPI(driver, dataSet, config, testCaseId, UniqueKey_EntityDetails);
			}
			if (Franchisee != null) {
				fc.utobj().printTestStep("Validating Franchisee Loaction in InfoMgr on View Page");
				fc.loginpage().login(driver);
				fc.infomgr().infomgr_common().InfoMgrCorporateLocations(driver);

				InfoMgrCorporateLocationsPage objFranPage = new InfoMgrCorporateLocationsPage(driver);
				try {
					try {
						fc.utobj().clickElement(driver, objFranPage.lnkShowAll);
					} catch (Exception ex) {
					}
					fc.utobj().clickPartialLinkText(driver, UniqueKey_Franchisee.get("franchiseeName"));
					fc.utobj().sleep(2500);
				} catch (Exception ex) {
					fc.utobj().throwsException("Franchise Location not found in View Page" + testCaseId);
				}
				fc.utobj().clickPartialLinkText(driver, "Entity Details");
				UniqueKey_EntityDetails.remove("referenceId");
				UniqueKey_EntityDetails.remove("parentReferenceId");
				ArrayList<String> listItems = new ArrayList<String>(UniqueKey_EntityDetails.values());
				fc.utobj().printTestStep(
						"Navigating to InfoMgr > RestAPI > Validating Entity Details for Franchisee Location");
				boolean Status = fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);
				if (!Status)
					fc.utobj().throwsException("Entity Details is not found on view page " + testCaseId);
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgrRestAPI", "FCRestAPI" })
	@TestCase(createdOn = "2017-06-16", updatedOn = "2017-06-16", testCaseDescription = "Add Employees subModule for Info Mgr In RestAPI", testCaseId = "TC_06_Fill_Employees_Tab_Info_Mgr_Update")
	public void AddEmployeesInfoMgrRest() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("infomgrRestAPI", testCaseId);
		Map<String, String> UniqueKey_Franchisee = new HashMap<String, String>();
		Map<String, String> UniqueKey_CorpUser = new HashMap<String, String>();
		Map<String, String> UniqueKey_Employees = new HashMap<String, String>();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			UserCreation uc = new UserCreation();
			InfoMgrRestService infomgrRest = new InfoMgrRestService();
			fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Corporate User");
			boolean createUser = uc.createCorpUser(driver, config, dataSet, testCaseId, UniqueKey_CorpUser);
			String Franchisee = null;
			if (createUser) {
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Franchisee Location");
				Franchisee = infomgrRest.AddLocation(driver, dataSet, config, UniqueKey_Franchisee, testCaseId);
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Employee for Franchisee Location");
				infomgrRest.CreateEmployeeRestAPI(driver, dataSet, config, testCaseId, UniqueKey_Employees);
				fc.utobj()
						.printTestStep("Navigating to InfoMgr > RestAPI > Updating EMployees for Franchisee Location");
				infomgrRest.UpdateEmployeeRestAPI(driver, dataSet, config, testCaseId, UniqueKey_Employees);
			}
			if (Franchisee != null) {
				fc.utobj().printTestStep("Validating Franchisee Loaction in InfoMgr on View Page");
				fc.loginpage().login(driver);
				fc.infomgr().infomgr_common().InfoMgrCorporateLocations(driver);

				InfoMgrCorporateLocationsPage objFranPage = new InfoMgrCorporateLocationsPage(driver);
				try {
					try {
						fc.utobj().clickElement(driver, objFranPage.lnkShowAll);
					} catch (Exception ex) {
					}
					fc.utobj().clickPartialLinkText(driver, UniqueKey_Franchisee.get("franchiseeName"));
					fc.utobj().sleep(2500);
				} catch (Exception ex) {
					fc.utobj().throwsException("Franchise Location not found in View Page" + testCaseId);
				}
				fc.utobj().clickPartialLinkText(driver, "Employees");
				UniqueKey_Employees.remove("referenceId");
				UniqueKey_Employees.remove("parentReferenceId");
				ArrayList<String> listItems = new ArrayList<String>(UniqueKey_Employees.values());
				fc.utobj().printTestStep(
						"Navigating to InfoMgr > RestAPI > Validating Employees for Franchisee Location");
				boolean Status = fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);
				if (!Status)
					fc.utobj().throwsException("Employee is not found on view page " + testCaseId);
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgrRestAPI", "infomgrRestAPI123" })
	@TestCase(createdOn = "2017-06-16", updatedOn = "2017-06-16", testCaseDescription = "Add Events subModule for Info Mgr In RestAPI", testCaseId = "TC_08_Fill_Events_Tab_Info_Mgr_Update")
	public void AddEventsInfoMgrRest() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("infomgrRestAPI", testCaseId);
		Map<String, String> UniqueKey_Franchisee = new HashMap<String, String>();
		Map<String, String> UniqueKey_CorpUser = new HashMap<String, String>();
		Map<String, String> UniqueKey_Events = new HashMap<String, String>();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			UserCreation uc = new UserCreation();
			InfoMgrRestService infomgrRest = new InfoMgrRestService();
			fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Corporate User");
			boolean createUser = uc.createCorpUser(driver, config, dataSet, testCaseId, UniqueKey_CorpUser);
			String Franchisee = null;
			if (createUser) {
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Franchisee Location");
				Franchisee = infomgrRest.AddLocation(driver, dataSet, config, UniqueKey_Franchisee, testCaseId);
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Events for Franchisee Location");
				infomgrRest.CreateEventsRestAPI(driver, dataSet, config, testCaseId, UniqueKey_Events);
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Updating Events for Franchisee Location");
				infomgrRest.UpdateEventsRestAPI(driver, dataSet, config, testCaseId, UniqueKey_Events);
			}
			if (Franchisee != null) {
				fc.utobj().printTestStep("Validating Franchisee Loaction in InfoMgr on View Page");
				fc.loginpage().login(driver);
				fc.infomgr().infomgr_common().InfoMgrCorporateLocations(driver);

				InfoMgrCorporateLocationsPage objFranPage = new InfoMgrCorporateLocationsPage(driver);
				try {
					try {
						fc.utobj().clickElement(driver, objFranPage.lnkShowAll);
					} catch (Exception ex) {
					}
					fc.utobj().clickPartialLinkText(driver, UniqueKey_Franchisee.get("franchiseeName"));
					fc.utobj().sleep(2500);
				} catch (Exception ex) {
					fc.utobj().throwsException("Franchise Location not found in View Page" + testCaseId);
				}
				fc.utobj().clickPartialLinkText(driver, "Events");
				UniqueKey_Events.remove("referenceId");
				UniqueKey_Events.remove("parentReferenceId");
				UniqueKey_Events.put("fimDdEventDate",
						bru.changeFormat(UniqueKey_Events.get("fimDdEventDate"), "yyyy-MM-dd", "MM/dd/yyyy"));
				ArrayList<String> listItems = new ArrayList<String>(UniqueKey_Events.values());
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Validating Events for Franchisee Location");
				boolean Status = fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);
				if (!Status)
					fc.utobj().throwsException("Events is not found on view page " + testCaseId);
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgrRestAPI", "FCRestAPI" })
	@TestCase(createdOn = "2017-06-16", updatedOn = "2017-06-16", testCaseDescription = "Add Training subModule for Info Mgr In RestAPI", testCaseId = "TC_07_Fill_Training_Tab_Info_Mgr_Update")
	public void AddTrainingInfoMgrRest() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("infomgrRestAPI", testCaseId);
		Map<String, String> UniqueKey_Franchisee = new HashMap<String, String>();
		Map<String, String> UniqueKey_CorpUser = new HashMap<String, String>();
		Map<String, String> UniqueKey_Training = new HashMap<String, String>();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			UserCreation uc = new UserCreation();
			InfoMgrRestService infomgrRest = new InfoMgrRestService();
			fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Corporate User");
			boolean createUser = uc.createCorpUser(driver, config, dataSet, testCaseId, UniqueKey_CorpUser);
			String Franchisee = null;
			if (createUser) {
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Franchisee Location");
				Franchisee = infomgrRest.AddLocation(driver, dataSet, config, UniqueKey_Franchisee, testCaseId);
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Training for Franchisee Location");
				infomgrRest.CreateTrainingRestAPI(driver, dataSet, config, testCaseId, UniqueKey_Training);
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Updating Training for Franchisee Location");
				infomgrRest.UpdateTrainingRestAPI(driver, dataSet, config, testCaseId, UniqueKey_Training);
			}
			if (Franchisee != null) {
				fc.utobj().printTestStep("Validating Franchisee Loaction in InfoMgr on View Page");
				fc.loginpage().login(driver);
				fc.infomgr().infomgr_common().InfoMgrCorporateLocations(driver);

				InfoMgrCorporateLocationsPage objFranPage = new InfoMgrCorporateLocationsPage(driver);
				try {
					try {
						fc.utobj().clickElement(driver, objFranPage.lnkShowAll);
					} catch (Exception ex) {
					}
					fc.utobj().clickPartialLinkText(driver, UniqueKey_Franchisee.get("franchiseeName"));
					fc.utobj().sleep(2500);
				} catch (Exception ex) {
					fc.utobj().throwsException("Franchise Location not found in View Page" + testCaseId);
				}
				fc.utobj().clickElement(driver,
						driver.findElement(By.xpath(".//*[@id='fimUl']//a[contains(text(),'Training')]")));
				UniqueKey_Training.remove("referenceId");
				UniqueKey_Training.remove("parentReferenceId");
				ArrayList<String> listItems = new ArrayList<String>(UniqueKey_Training.values());
				fc.utobj()
						.printTestStep("Navigating to InfoMgr > RestAPI > Validating Training for Franchisee Location");
				boolean Status = fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);
				if (!Status)
					fc.utobj().throwsException("Training is not found on view page " + testCaseId);
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgrRestAPI", "FCRestAPI" })
	@TestCase(createdOn = "2017-06-16", updatedOn = "2017-06-16", testCaseDescription = "Add Call subModule for Info Mgr In RestAPI", testCaseId = "TC_03_Fill_Call_Tab_Info_Mgr_Update")
	public void AddCallInfoMgrRest() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("infomgrRestAPI", testCaseId);
		Map<String, String> UniqueKey_Franchisee = new HashMap<String, String>();
		Map<String, String> UniqueKey_CorpUser = new HashMap<String, String>();
		Map<String, String> UniqueKey_Call = new HashMap<String, String>();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			UserCreation uc = new UserCreation();
			InfoMgrRestService infomgrRest = new InfoMgrRestService();
			fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Corporate User");
			boolean createUser = uc.createCorpUser(driver, config, dataSet, testCaseId, UniqueKey_CorpUser);
			String Franchisee = null;
			if (createUser) {
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Franchisee Location");
				Franchisee = infomgrRest.AddLocation(driver, dataSet, config, UniqueKey_Franchisee, testCaseId);
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Call for Franchisee Location");
				infomgrRest.CreateCallRestAPI(driver, dataSet, config, testCaseId, UniqueKey_Call);
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Updating Call for Franchisee Location");
				infomgrRest.UpdateCallRestAPI(driver, dataSet, config, testCaseId, UniqueKey_Call);
			}
			if (Franchisee != null) {
				fc.utobj().printTestStep("Validating Franchisee Loaction in InfoMgr on View Page");
				fc.loginpage().login(driver);
				fc.infomgr().infomgr_common().InfoMgrCorporateLocations(driver);

				InfoMgrCorporateLocationsPage objFranPage = new InfoMgrCorporateLocationsPage(driver);
				try {
					try {
						fc.utobj().clickElement(driver, objFranPage.lnkShowAll);
					} catch (Exception ex) {
					}
					fc.utobj().clickPartialLinkText(driver, UniqueKey_Franchisee.get("franchiseeName"));
					fc.utobj().sleep(2500);
				} catch (Exception ex) {
					fc.utobj().throwsException("Franchise Location not found in View Page" + testCaseId);
				}
				UniqueKey_Call.remove("referenceId");
				UniqueKey_Call.remove("parentReferenceId");
				UniqueKey_Call.remove("timeAdded");
				UniqueKey_Franchisee.put("calldate", UniqueKey_Call.get("date"));
				ArrayList<String> listItems = new ArrayList<String>(UniqueKey_Call.values());
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Validating Call for Franchisee Location");
				boolean Status = infomgrRest.ValidateForExportCall(driver, config, listItems, DataServicePage.Call,
						UniqueKey_Franchisee, UniqueKey_Call.get("subject"));
				if (!Status)
					fc.utobj().throwsException("Call is not found on view page " + testCaseId);
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgrRestAPI", "FCRestAPI" })
	@TestCase(createdOn = "2017-06-16", updatedOn = "2017-06-16", testCaseDescription = "Add Financial subModule for Info Mgr In RestAPI", testCaseId = "TC_09_Fill_Financial_Tab_Info_Mgr_Update")
	public void AddFinancialInfoMgrRest() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("infomgrRestAPI", testCaseId);
		Map<String, String> UniqueKey_Franchisee = new HashMap<String, String>();
		Map<String, String> UniqueKey_CorpUser = new HashMap<String, String>();
		Map<String, String> UniqueKey_Financial = new HashMap<String, String>();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			UserCreation uc = new UserCreation();
			InfoMgrRestService infomgrRest = new InfoMgrRestService();
			fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Corporate User");
			boolean createUser = uc.createCorpUser(driver, config, dataSet, testCaseId, UniqueKey_CorpUser);
			String Franchisee = null;
			if (createUser) {
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Franchisee Location");
				Franchisee = infomgrRest.AddLocation(driver, dataSet, config, UniqueKey_Franchisee, testCaseId);
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Financial for Franchisee Location");
				infomgrRest.CreateFinancialRestAPI(driver, dataSet, config, testCaseId, UniqueKey_Financial);
				fc.utobj()
						.printTestStep("Navigating to InfoMgr > RestAPI > Updating Financial for Franchisee Location");
				infomgrRest.UpdateFinancialRestAPI(driver, dataSet, config, testCaseId, UniqueKey_Financial);
			}
			if (Franchisee != null) {
				fc.utobj().printTestStep("Validating Franchisee Loaction in InfoMgr on View Page");
				fc.loginpage().login(driver);
				fc.infomgr().infomgr_common().InfoMgrCorporateLocations(driver);

				InfoMgrCorporateLocationsPage objFranPage = new InfoMgrCorporateLocationsPage(driver);
				try {
					try {
						fc.utobj().clickElement(driver, objFranPage.lnkShowAll);
					} catch (Exception ex) {
					}
					fc.utobj().clickPartialLinkText(driver, UniqueKey_Franchisee.get("franchiseeName"));
					fc.utobj().sleep(2500);
				} catch (Exception ex) {
					fc.utobj().throwsException("Franchise Location not found in View Page" + testCaseId);
				}
				// fc.utobj().clickPartialLinkText(driver, "Events");
				// UniqueKey_Events.remove("referenceId");
				// UniqueKey_Events.remove("parentReferenceId");
				// ArrayList<String> listItems = new
				// ArrayList<String>(UniqueKey_Events.values());
				// boolean Status =
				// fc.utobj().printTestStep(testCaseId, "Navigating to InfoMgr >
				// RestAPI > Validating Financial for Franchisee Location");
				// fc.utobj().assertPageSourceWithMultipleRecords(driver,
				// listItems);
				// if (!Status)
				// fc.utobj().throwsException("Events is not found on view page
				// " + testCaseId);
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgrRestAPI", "FCRestAPI" })
	@TestCase(createdOn = "2017-06-16", updatedOn = "2017-06-16", testCaseDescription = "Add Gaurantor subModule for Info Mgr In RestAPI", testCaseId = "TC_10_Fill_Gaurantor_Tab_Info_Mgr_Update")
	public void AddGaurantorInfoMgrRest() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("infomgrRestAPI", testCaseId);
		Map<String, String> UniqueKey_Franchisee = new HashMap<String, String>();
		Map<String, String> UniqueKey_CorpUser = new HashMap<String, String>();
		Map<String, String> UniqueKey_Gaurantor = new HashMap<String, String>();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			UserCreation uc = new UserCreation();
			InfoMgrRestService infomgrRest = new InfoMgrRestService();
			fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Corporate User");
			boolean createUser = uc.createCorpUser(driver, config, dataSet, testCaseId, UniqueKey_CorpUser);
			String Franchisee = null;
			if (createUser) {
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Franchisee Location");
				Franchisee = infomgrRest.AddLocation(driver, dataSet, config, UniqueKey_Franchisee, testCaseId);
				fc.utobj().sleep(2500);
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Guarantor for Adding Franchisee Location");
				infomgrRest.CreateGaurantorRestAPI(driver, dataSet, config, testCaseId, UniqueKey_Gaurantor);
				fc.utobj().sleep(2500);
				fc.utobj()
						.printTestStep("Navigating to InfoMgr > RestAPI > Updating Guarantor for Franchisee Location");
				infomgrRest.UpdateGaurantorRestAPI(driver, dataSet, config, testCaseId, UniqueKey_Gaurantor);
			}
			if (Franchisee != null) {
				fc.utobj().printTestStep("Validating Franchisee Loaction in InfoMgr on View Page");
				fc.loginpage().login(driver);
				fc.infomgr().infomgr_common().InfoMgrCorporateLocations(driver);

				InfoMgrCorporateLocationsPage objFranPage = new InfoMgrCorporateLocationsPage(driver);
				try {
					try {
						fc.utobj().clickElement(driver, objFranPage.lnkShowAll);
					} catch (Exception ex) {
					}
					fc.utobj().clickPartialLinkText(driver, UniqueKey_Franchisee.get("franchiseeName"));
					fc.utobj().sleep(2500);
				} catch (Exception ex) {
					fc.utobj().throwsException("Franchise Location not found in View Page" + testCaseId);
				}
				fc.utobj().clickPartialLinkText(driver, "Guarantors");
				UniqueKey_Gaurantor.remove("referenceId");
				UniqueKey_Gaurantor.remove("parentReferenceId");
				ArrayList<String> listItems = new ArrayList<String>(UniqueKey_Gaurantor.values());
				fc.utobj().printTestStep(
						"Navigating to InfoMgr > RestAPI > Validating Guarantor for Franchisee Location");
				boolean Status = fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);
				if (!Status)
					fc.utobj().throwsException("Gaurantor is not found on view page " + testCaseId);
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgrRestAPI", "FCRestAPI" })
	@TestCase(createdOn = "2017-06-16", updatedOn = "2017-06-16", testCaseDescription = "Add Insurance subModule for Info Mgr In RestAPI", testCaseId = "TC_11_Fill_Insurance_Tab_Info_Mgr_Update")
	public void AddInsuranceInfoMgrRest() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("infomgrRestAPI", testCaseId);
		Map<String, String> UniqueKey_Franchisee = new HashMap<String, String>();
		Map<String, String> UniqueKey_CorpUser = new HashMap<String, String>();
		Map<String, String> UniqueKey_Insurance = new HashMap<String, String>();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			UserCreation uc = new UserCreation();
			InfoMgrRestService infomgrRest = new InfoMgrRestService();
			fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Corporate User");
			boolean createUser = uc.createCorpUser(driver, config, dataSet, testCaseId, UniqueKey_CorpUser);
			String Franchisee = null;
			if (createUser) {
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Franchisee Location");
				Franchisee = infomgrRest.AddLocation(driver, dataSet, config, UniqueKey_Franchisee, testCaseId);
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Insurance for Franchisee Location");
				infomgrRest.CreateInsuranceRestAPI(driver, dataSet, config, testCaseId, UniqueKey_Insurance);
				fc.utobj()
						.printTestStep("Navigating to InfoMgr > RestAPI > Updating Insurance for Franchisee Location");
				infomgrRest.UpdateInsuranceRestAPI(driver, dataSet, config, testCaseId, UniqueKey_Insurance);
			}
			if (Franchisee != null) {
				fc.utobj().printTestStep("Validating Franchisee Loaction in InfoMgr on View Page");
				fc.loginpage().login(driver);
				fc.infomgr().infomgr_common().InfoMgrCorporateLocations(driver);

				InfoMgrCorporateLocationsPage objFranPage = new InfoMgrCorporateLocationsPage(driver);
				try {
					try {
						fc.utobj().clickElement(driver, objFranPage.lnkShowAll);
					} catch (Exception ex) {
					}
					fc.utobj().clickPartialLinkText(driver, UniqueKey_Franchisee.get("franchiseeName"));
					fc.utobj().sleep(2500);
				} catch (Exception ex) {
					fc.utobj().throwsException("Franchise Location not found in View Page" + testCaseId);
				}
				fc.utobj().clickPartialLinkText(driver, "Insurance");
				UniqueKey_Insurance.remove("referenceId");
				UniqueKey_Insurance.remove("parentReferenceId");
				ArrayList<String> listItems = new ArrayList<String>(UniqueKey_Insurance.values());
				fc.utobj().printTestStep(
						"Navigating to InfoMgr > RestAPI > Validating Insurance for Franchisee Location");
				boolean Status = fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);
				if (!Status)
					fc.utobj().throwsException("Insurance is not found on view page " + testCaseId);
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgrRestAPI", "FCRestAPI" })
	@TestCase(createdOn = "2017-06-16", updatedOn = "2017-06-16", testCaseDescription = "Add LegalViolation subModule for Info Mgr In RestAPI", testCaseId = "TC_12_Fill_LeagalViolation_Tab_Info_Mgr_Update")
	public void AddLegalViolationInfoMgrRest()
			throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("infomgrRestAPI", testCaseId);
		Map<String, String> UniqueKey_Franchisee = new HashMap<String, String>();
		Map<String, String> UniqueKey_CorpUser = new HashMap<String, String>();
		Map<String, String> UniqueKey_LegalViolation = new HashMap<String, String>();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			UserCreation uc = new UserCreation();
			InfoMgrRestService infomgrRest = new InfoMgrRestService();
			fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Corporate User");
			boolean createUser = uc.createCorpUser(driver, config, dataSet, testCaseId, UniqueKey_CorpUser);
			String Franchisee = null;
			if (createUser) {
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Franchisee Location");
				Franchisee = infomgrRest.AddLocation(driver, dataSet, config, UniqueKey_Franchisee, testCaseId);
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Insurance for Franchisee Location");
				infomgrRest.CreateLegalViolationRestAPI(driver, dataSet, config, testCaseId, UniqueKey_LegalViolation);
				fc.utobj()
						.printTestStep("Navigating to InfoMgr > RestAPI > Updating Insurance for Franchisee Location");
				infomgrRest.UpdateLegalViolationRestAPI(driver, dataSet, config, testCaseId, UniqueKey_LegalViolation);
			}
			if (Franchisee != null) {
				fc.utobj().printTestStep("Validating Franchisee Loaction in InfoMgr on View Page");
				fc.loginpage().login(driver);
				fc.infomgr().infomgr_common().InfoMgrCorporateLocations(driver);

				InfoMgrCorporateLocationsPage objFranPage = new InfoMgrCorporateLocationsPage(driver);
				try {
					try {
						fc.utobj().clickElement(driver, objFranPage.lnkShowAll);
					} catch (Exception ex) {
					}
					fc.utobj().clickPartialLinkText(driver, UniqueKey_Franchisee.get("franchiseeName"));
					fc.utobj().sleep(2500);
				} catch (Exception ex) {
					fc.utobj().throwsException("Franchise Location not found in View Page" + testCaseId);
				}
				fc.utobj().clickPartialLinkText(driver, "Legal Violation");
				UniqueKey_LegalViolation.remove("referenceId");
				UniqueKey_LegalViolation.remove("parentReferenceId");
				ArrayList<String> listItems = new ArrayList<String>(UniqueKey_LegalViolation.values());
				fc.utobj().printTestStep(
						"Navigating to InfoMgr > RestAPI > Validating Legal Violation for Franchisee Location");
				boolean Status = fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);
				if (!Status)
					fc.utobj().throwsException("Legal Violation is not found on view page " + testCaseId);
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgrRestAPI", "FCRestAPI" })
	@TestCase(createdOn = "2017-06-16", updatedOn = "2017-06-16", testCaseDescription = "Add Lenders subModule for Info Mgr In RestAPI", testCaseId = "TC_13_Fill_Lenders_Tab_Info_Mgr_Update")
	public void AddLendersInfoMgrRest() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("infomgrRestAPI", testCaseId);
		Map<String, String> UniqueKey_Franchisee = new HashMap<String, String>();
		Map<String, String> UniqueKey_CorpUser = new HashMap<String, String>();
		Map<String, String> UniqueKey_Lenders = new HashMap<String, String>();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			UserCreation uc = new UserCreation();
			InfoMgrRestService infomgrRest = new InfoMgrRestService();
			fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Corporate User");
			boolean createUser = uc.createCorpUser(driver, config, dataSet, testCaseId, UniqueKey_CorpUser);
			String Franchisee = null;
			if (createUser) {
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Franchisee Location");
				Franchisee = infomgrRest.AddLocation(driver, dataSet, config, UniqueKey_Franchisee, testCaseId);
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Lenders for Franchisee Location");
				infomgrRest.CreateLendersRestAPI(driver, dataSet, config, testCaseId, UniqueKey_Lenders);
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Updating Lenders for Franchisee Location");
				infomgrRest.UpdateLendersRestAPI(driver, dataSet, config, testCaseId, UniqueKey_Lenders);
			}
			if (Franchisee != null) {
				fc.utobj().printTestStep("Validating Franchisee Loaction in InfoMgr on View Page");
				fc.loginpage().login(driver);
				fc.infomgr().infomgr_common().InfoMgrCorporateLocations(driver);

				InfoMgrCorporateLocationsPage objFranPage = new InfoMgrCorporateLocationsPage(driver);
				try {
					try {
						fc.utobj().clickElement(driver, objFranPage.lnkShowAll);
					} catch (Exception ex) {
					}
					fc.utobj().clickPartialLinkText(driver, UniqueKey_Franchisee.get("franchiseeName"));
					fc.utobj().sleep(2500);
				} catch (Exception ex) {
					fc.utobj().throwsException("Franchise Location not found in View Page" + testCaseId);
				}
				fc.utobj().clickPartialLinkText(driver, "Lenders");
				UniqueKey_Lenders.remove("referenceId");
				UniqueKey_Lenders.remove("parentReferenceId");
				ArrayList<String> listItems = new ArrayList<String>(UniqueKey_Lenders.values());
				fc.utobj()
						.printTestStep("Navigating to InfoMgr > RestAPI > Validating Lenders for Franchisee Location");
				boolean Status = fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);
				if (!Status)
					fc.utobj().throwsException("Lenders is not found on view page " + testCaseId);
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgrRestAPI", "FCRestAPI" })
	@TestCase(createdOn = "2017-06-16", updatedOn = "2017-06-16", testCaseDescription = "Add Lenders ExternalMail for Info Mgr In RestAPI", testCaseId = "TC_14_Fill_ExternalMail_Tab_Info_Mgr_Update")
	public void AddExternalMailInfoMgrRest() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("infomgrRestAPI", testCaseId);
		Map<String, String> UniqueKey_Franchisee = new HashMap<String, String>();
		Map<String, String> UniqueKey_CorpUser = new HashMap<String, String>();
		Map<String, String> UniqueKey_ExternalMails = new HashMap<String, String>();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			UserCreation uc = new UserCreation();
			InfoMgrRestService infomgrRest = new InfoMgrRestService();
			fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Corporate User");
			boolean createUser = uc.createCorpUser(driver, config, dataSet, testCaseId, UniqueKey_CorpUser);
			String Franchisee = null;
			if (createUser) {
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Franchisee Location");
				Franchisee = infomgrRest.AddLocation(driver, dataSet, config, UniqueKey_Franchisee, testCaseId);
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Lenders for Franchisee Location");
				infomgrRest.CreateExternalMailsRestAPI(driver, dataSet, config, testCaseId, UniqueKey_ExternalMails);
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Updating Lenders for Franchisee Location");
				infomgrRest.UpdateExternalMailsRestAPI(driver, dataSet, config, testCaseId, UniqueKey_ExternalMails);
			}
			if (Franchisee != null) {
				fc.utobj().printTestStep("Validating Franchisee Loaction in InfoMgr on View Page");
				fc.loginpage().login(driver);
				fc.infomgr().infomgr_common().InfoMgrCorporateLocations(driver);

				InfoMgrCorporateLocationsPage objFranPage = new InfoMgrCorporateLocationsPage(driver);
				try {
					try {
						fc.utobj().clickElement(driver, objFranPage.lnkShowAll);
					} catch (Exception ex) {
					}
					fc.utobj().clickPartialLinkText(driver, UniqueKey_Franchisee.get("franchiseeName"));
					fc.utobj().sleep(2500);
				} catch (Exception ex) {
					fc.utobj().throwsException("Franchise Location not found in View Page" + testCaseId);
				}
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgrRestAPI", "FCRestAPI" })
	@TestCase(createdOn = "2017-06-16", updatedOn = "2017-06-16", testCaseDescription = "Add Lenders Marketing for Info Mgr In RestAPI", testCaseId = "TC_15_Fill_Marketing_Tab_Info_Mgr_Update")
	public void AddMarketingInfoMgrRest() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("infomgrRestAPI", testCaseId);
		Map<String, String> UniqueKey_Franchisee = new HashMap<String, String>();
		Map<String, String> UniqueKey_CorpUser = new HashMap<String, String>();
		Map<String, String> UniqueKey_Marketing = new HashMap<String, String>();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			UserCreation uc = new UserCreation();
			InfoMgrRestService infomgrRest = new InfoMgrRestService();
			fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Corporate User");
			boolean createUser = uc.createCorpUser(driver, config, dataSet, testCaseId, UniqueKey_CorpUser);
			String Franchisee = null;
			if (createUser) {
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Franchisee Location");
				Franchisee = infomgrRest.AddLocation(driver, dataSet, config, UniqueKey_Franchisee, testCaseId);
				fc.utobj().sleep(2500);
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Marketing for Franchisee Location");
				infomgrRest.CreateMarketingRestAPI(driver, dataSet, config, testCaseId, UniqueKey_Marketing);
				fc.utobj().sleep(2500);
				fc.utobj()
						.printTestStep("Navigating to InfoMgr > RestAPI > Updating Marketing for Franchisee Location");
				infomgrRest.UpdateMarketingRestAPI(driver, dataSet, config, testCaseId, UniqueKey_Marketing);
				fc.utobj().sleep(2500);
			}
			if (Franchisee != null) {
				fc.utobj().printTestStep("Validating Franchisee Loaction in InfoMgr on View Page");
				fc.loginpage().login(driver);
				fc.infomgr().infomgr_common().InfoMgrCorporateLocations(driver);

				InfoMgrCorporateLocationsPage objFranPage = new InfoMgrCorporateLocationsPage(driver);
				try {
					try {
						fc.utobj().clickElement(driver, objFranPage.lnkShowAll);
					} catch (Exception ex) {
					}
					fc.utobj().clickPartialLinkText(driver, UniqueKey_Franchisee.get("franchiseeName"));
					fc.utobj().sleep(2500);
				} catch (Exception ex) {
					fc.utobj().throwsException("Franchise Location not found in View Page" + testCaseId);
				}
				fc.utobj().clickPartialLinkText(driver, "Marketing");
				UniqueKey_Marketing.remove("referenceId");
				UniqueKey_Marketing.remove("parentReferenceId");
				ArrayList<String> listItems = new ArrayList<String>(UniqueKey_Marketing.values());
				fc.utobj().printTestStep(
						"Navigating to InfoMgr > RestAPI > Validating Marketing for Franchisee Location");
				boolean Status = fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);
				if (!Status)
					fc.utobj().throwsException("Marketing is not found on view page " + testCaseId);
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgrRestAPI", "FCRestAPI" })
	@TestCase(createdOn = "2017-06-16", updatedOn = "2017-06-16", testCaseDescription = "Add Mystery VIew for Info Mgr In RestAPI", testCaseId = "TC_16_Fill_MysteryReview_Tab_Info_Mgr_Update")
	public void AddMysteryReviewInfoMgrRest()
			throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("infomgrRestAPI", testCaseId);
		Map<String, String> UniqueKey_Franchisee = new HashMap<String, String>();
		Map<String, String> UniqueKey_CorpUser = new HashMap<String, String>();
		Map<String, String> UniqueKey_MysteryReview = new HashMap<String, String>();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			UserCreation uc = new UserCreation();
			InfoMgrRestService infomgrRest = new InfoMgrRestService();
			fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Corporate User");
			boolean createUser = uc.createCorpUser(driver, config, dataSet, testCaseId, UniqueKey_CorpUser);
			String Franchisee = null;
			if (createUser) {
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Franchisee Location");
				Franchisee = infomgrRest.AddLocation(driver, dataSet, config, UniqueKey_Franchisee, testCaseId);
				fc.utobj().printTestStep(
						"Navigating to InfoMgr > RestAPI > Adding MysteryReview for Franchisee Location");
				infomgrRest.CreateMysteryReviewRestAPI(driver, dataSet, config, testCaseId, UniqueKey_MysteryReview);
				fc.utobj().printTestStep(
						"Navigating to InfoMgr > RestAPI > Updating MysteryReview for Franchisee Location");
				infomgrRest.UpdateMysteryReviewRestAPI(driver, dataSet, config, testCaseId, UniqueKey_MysteryReview);
			}
			if (Franchisee != null) {
				fc.utobj().printTestStep("Validating Franchisee Loaction in InfoMgr on View Page");
				fc.loginpage().login(driver);
				fc.infomgr().infomgr_common().InfoMgrCorporateLocations(driver);

				InfoMgrCorporateLocationsPage objFranPage = new InfoMgrCorporateLocationsPage(driver);
				try {
					try {
						fc.utobj().clickElement(driver, objFranPage.lnkShowAll);
					} catch (Exception ex) {
					}
					fc.utobj().clickPartialLinkText(driver, UniqueKey_Franchisee.get("franchiseeName"));
					fc.utobj().sleep(2500);
				} catch (Exception ex) {
					fc.utobj().throwsException("Franchise Location not found in View Page" + testCaseId);
				}
				fc.utobj().clickPartialLinkText(driver, "Mystery Review");
				UniqueKey_MysteryReview.remove("referenceId");
				UniqueKey_MysteryReview.remove("parentReferenceId");
				UniqueKey_MysteryReview.put("fimDdInspectionDate", bru
						.changeFormat(UniqueKey_MysteryReview.get("fimDdInspectionDate"), "yyyy-MM-dd", "MM/dd/yyyy"));
				ArrayList<String> listItems = new ArrayList<String>(UniqueKey_MysteryReview.values());
				fc.utobj().printTestStep(
						"Navigating to InfoMgr > RestAPI > validating MysteryReview for Franchisee Location");
				boolean Status = fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);
				if (!Status)
					fc.utobj().throwsException("Marketing is not found on view page " + testCaseId);
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgrRestAPI", "FCRestAPI" })
	@TestCase(createdOn = "2017-06-16", updatedOn = "2017-06-16", testCaseDescription = "Add Owner for Info Mgr In RestAPI", testCaseId = "TC_17_Fill_Owner_Tab_Info_Mgr_Update")
	public void AddOwnerInfoMgrRest() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("infomgrRestAPI", testCaseId);
		Map<String, String> UniqueKey_Franchisee = new HashMap<String, String>();
		Map<String, String> UniqueKey_CorpUser = new HashMap<String, String>();
		Map<String, String> UniqueKey_Owners = new HashMap<String, String>();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			UserCreation uc = new UserCreation();
			InfoMgrRestService infomgrRest = new InfoMgrRestService();
			fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Corporate User");
			boolean createUser = uc.createCorpUser(driver, config, dataSet, testCaseId, UniqueKey_CorpUser);
			String Franchisee = null;
			if (createUser) {
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Franchisee Location");
				Franchisee = infomgrRest.AddLocation(driver, dataSet, config, UniqueKey_Franchisee, testCaseId);
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Owners for Franchisee Location");
				infomgrRest.CreateOwnersRestAPI(driver, dataSet, config, testCaseId, UniqueKey_Owners);
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Updating Owners for Franchisee Location");
				infomgrRest.UpdateOwnersRestAPI(driver, dataSet, config, testCaseId, UniqueKey_Owners);
			}
			if (Franchisee != null) {
				fc.utobj().printTestStep("Validating Franchisee Loaction in InfoMgr on View Page");
				fc.loginpage().login(driver);
				fc.infomgr().infomgr_common().InfoMgrCorporateLocations(driver);

				InfoMgrCorporateLocationsPage objFranPage = new InfoMgrCorporateLocationsPage(driver);
				try {
					try {
						fc.utobj().clickElement(driver, objFranPage.lnkShowAll);
					} catch (Exception ex) {
					}
					fc.utobj().clickPartialLinkText(driver, UniqueKey_Franchisee.get("franchiseeName"));
					fc.utobj().sleep(2500);
				} catch (Exception ex) {
					fc.utobj().throwsException("Franchise Location not found in View Page" + testCaseId);
				}
				fc.utobj().clickPartialLinkText(driver, "Owners");
				UniqueKey_Owners.remove("referenceId");
				UniqueKey_Owners.remove("parentReferenceId");
				UniqueKey_Owners.remove("ownerType");
				ArrayList<String> listItems = new ArrayList<String>(UniqueKey_Owners.values());
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Validating Owners for Franchisee Location");
				boolean Status = fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);
				if (!Status)
					fc.utobj().throwsException("Owner is not found on view page " + testCaseId);
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgrRestAPI", "FCRestAPI" })
	@TestCase(createdOn = "2017-06-16", updatedOn = "2017-07-19", testCaseDescription = "Add Territory for Info Mgr In RestAPI", testCaseId = "TC_18_Fill_Territory_Tab_Info_Mgr_Update")
	public void AddTerritoryInfoMgrRest() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("infomgrRestAPI", testCaseId);
		Map<String, String> UniqueKey_Franchisee = new HashMap<String, String>();
		Map<String, String> UniqueKey_CorpUser = new HashMap<String, String>();
		Map<String, String> UniqueKey_Territory = new HashMap<String, String>();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			UserCreation uc = new UserCreation();
			InfoMgrRestService infomgrRest = new InfoMgrRestService();
			fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Corporate User");
			boolean createUser = uc.createCorpUser(driver, config, dataSet, testCaseId, UniqueKey_CorpUser);
			String Franchisee = null;
			if (createUser) {
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Franchisee Location");
				Franchisee = infomgrRest.AddLocation(driver, dataSet, config, UniqueKey_Franchisee, testCaseId);
				if (Franchisee == null) {
					fc.utobj().throwsException("Franchisee Location not added in infomgr from RestAPI");
				}
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Territory for Franchisee Location");
				infomgrRest.CreateTerritoryRestAPI(driver, dataSet, config, testCaseId, UniqueKey_Territory);
				fc.utobj()
						.printTestStep("Navigating to InfoMgr > RestAPI > Updating Territory for Franchisee Location");
				infomgrRest.UpdateTerritoryRestAPI(driver, dataSet, config, testCaseId, UniqueKey_Territory);
			}
			if (Franchisee != null) {
				fc.utobj().printTestStep("Validating Franchisee Loaction in InfoMgr on View Page");
				fc.loginpage().login(driver);
				fc.infomgr().infomgr_common().InfoMgrCorporateLocations(driver);

				InfoMgrCorporateLocationsPage objFranPage = new InfoMgrCorporateLocationsPage(driver);
				try {
					try {
						fc.utobj().clickElement(driver, objFranPage.lnkShowAll);
					} catch (Exception ex) {
					}
					fc.utobj().clickPartialLinkText(driver, UniqueKey_Franchisee.get("franchiseeName"));
					fc.utobj().sleep(2500);
				} catch (Exception ex) {
					fc.utobj().throwsException("Franchise Location not found in View Page" + testCaseId);
				}
				fc.utobj().clickPartialLinkText(driver, "Territory");
				UniqueKey_Territory.remove("referenceId");
				UniqueKey_Territory.remove("parentReferenceId");
				ArrayList<String> listItems = new ArrayList<String>(UniqueKey_Territory.values());
				fc.utobj().printTestStep(
						"Navigating to InfoMgr > RestAPI > Validating Territory for Franchisee Location");
				boolean Status = fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);
				if (!Status)
					fc.utobj().throwsException("Territory is not found on view page " + testCaseId);
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgrRestAPI", "FCRestAPI" })
	@TestCase(createdOn = "2017-06-16", updatedOn = "2017-06-16", testCaseDescription = "Add Renewal for Info Mgr In RestAPI", testCaseId = "TC_19_Fill_Renewal_Tab_Info_Mgr_Update")
	public void AddRenewalInfoMgrRest() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("infomgrRestAPI", testCaseId);
		Map<String, String> UniqueKey_Franchisee = new HashMap<String, String>();
		Map<String, String> UniqueKey_CorpUser = new HashMap<String, String>();
		Map<String, String> UniqueKey_Renewal = new HashMap<String, String>();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			UserCreation uc = new UserCreation();
			InfoMgrRestService infomgrRest = new InfoMgrRestService();
			fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Corporate User");
			boolean createUser = uc.createCorpUser(driver, config, dataSet, testCaseId, UniqueKey_CorpUser);
			String Franchisee = null;
			if (createUser) {
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Franchisee Location");
				Franchisee = infomgrRest.AddLocation(driver, dataSet, config, UniqueKey_Franchisee, testCaseId);
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Renewal for Franchisee Location");
				infomgrRest.CreateRenewalRestAPI(driver, dataSet, config, testCaseId, UniqueKey_Renewal);
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Updating Renewal for Franchisee Location");
				infomgrRest.UpdateRenewalRestAPI(driver, dataSet, config, testCaseId, UniqueKey_Renewal);
			}
			if (Franchisee != null) {
				fc.utobj().printTestStep("Validating Franchisee Loaction in InfoMgr on View Page");
				fc.loginpage().login(driver);
				fc.infomgr().infomgr_common().InfoMgrCorporateLocations(driver);

				InfoMgrCorporateLocationsPage objFranPage = new InfoMgrCorporateLocationsPage(driver);
				try {
					try {
						fc.utobj().clickElement(driver, objFranPage.lnkShowAll);
					} catch (Exception ex) {
					}
					fc.utobj().clickPartialLinkText(driver, UniqueKey_Franchisee.get("franchiseeName"));
					fc.utobj().sleep(2500);
				} catch (Exception ex) {
					fc.utobj().throwsException("Franchise Location not found in View Page" + testCaseId);
				}
				fc.utobj().clickPartialLinkText(driver, "Renewal");
				UniqueKey_Renewal.remove("referenceId");
				UniqueKey_Renewal.remove("parentReferenceId");
				List<String> listItems = new ArrayList<String>(UniqueKey_Renewal.values());
				fc.utobj()
						.printTestStep("Navigating to InfoMgr > RestAPI > Validating Renewal for Franchisee Location");
				boolean Status = fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);
				if (!Status)
					fc.utobj().throwsException("Renewal is not found on view page " + testCaseId);
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgrRestAPI", "FCRestAPI" })
	@TestCase(createdOn = "2017-06-16", updatedOn = "2017-06-16", testCaseDescription = "Add RealEstate for Info Mgr In RestAPI", testCaseId = "TC_19_Fill_RealEstate_Tab_Info_Mgr_Update")
	public void AddRealEstateInfoMgrRest() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("infomgrRestAPI", testCaseId);
		Map<String, String> UniqueKey_Franchisee = new HashMap<String, String>();
		Map<String, String> UniqueKey_CorpUser = new HashMap<String, String>();
		Map<String, String> UniqueKey_RealEstate = new HashMap<String, String>();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			UserCreation uc = new UserCreation();
			InfoMgrRestService infomgrRest = new InfoMgrRestService();
			fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Corporate User");
			boolean createUser = uc.createCorpUser(driver, config, dataSet, testCaseId, UniqueKey_CorpUser);
			String Franchisee = null;
			if (createUser) {
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Franchisee Location");
				Franchisee = infomgrRest.AddLocation(driver, dataSet, config, UniqueKey_Franchisee, testCaseId);
				fc.utobj().sleep(2500);
				fc.utobj()
						.printTestStep("Navigating to InfoMgr > RestAPI > Adding Real Estate for Franchisee Location");
				infomgrRest.CreateRealEstateRestAPI(driver, dataSet, config, testCaseId, UniqueKey_RealEstate);
				fc.utobj().sleep(2500);
				fc.utobj().printTestStep(
						"Navigating to InfoMgr > RestAPI > Updating Real Estate for Franchisee Location");
				infomgrRest.UpdateRealEstateRestAPI(driver, dataSet, config, testCaseId, UniqueKey_RealEstate);
			}
			if (Franchisee != null) {
				fc.utobj().printTestStep("Validating Franchisee Loaction in InfoMgr on View Page");
				fc.loginpage().login(driver);
				fc.infomgr().infomgr_common().InfoMgrCorporateLocations(driver);

				InfoMgrCorporateLocationsPage objFranPage = new InfoMgrCorporateLocationsPage(driver);
				try {
					try {
						fc.utobj().clickElement(driver, objFranPage.lnkShowAll);
					} catch (Exception ex) {
					}
					fc.utobj().clickPartialLinkText(driver, UniqueKey_Franchisee.get("franchiseeName"));
					fc.utobj().sleep(2500);
				} catch (Exception ex) {
					fc.utobj().throwsException("Franchise Location not found in View Page" + testCaseId);
				}
				fc.utobj().clickPartialLinkText(driver, "Real Estate");
				UniqueKey_RealEstate.remove("referenceId");
				UniqueKey_RealEstate.remove("parentReferenceId");
				ArrayList<String> listItems = new ArrayList<String>(UniqueKey_RealEstate.values());
				fc.utobj().printTestStep(
						"Navigating to InfoMgr > RestAPI > Validating Real Estate for Franchisee Location");
				boolean Status = fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);
				if (!Status)
					fc.utobj().throwsException("Real Estate is not found on view page " + testCaseId);
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgrRestAPI", "FCRestAPI" })
	@TestCase(createdOn = "2017-06-16", updatedOn = "2017-06-16", testCaseDescription = "Add Remark for Info Mgr In RestAPI", testCaseId = "TC_20_Fill_Remark_Tab_Info_Mgr_Update")
	public void AddRemarkInfoMgrRest() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("infomgrRestAPI", testCaseId);
		Map<String, String> UniqueKey_Franchisee = new HashMap<String, String>();
		Map<String, String> UniqueKey_CorpUser = new HashMap<String, String>();
		Map<String, String> UniqueKey_Remark = new HashMap<String, String>();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			UserCreation uc = new UserCreation();
			InfoMgrRestService infomgrRest = new InfoMgrRestService();
			fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Corporate User");
			boolean createUser = uc.createCorpUser(driver, config, dataSet, testCaseId, UniqueKey_CorpUser);
			String Franchisee = null;
			if (createUser) {
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Franchisee Location");
				Franchisee = infomgrRest.AddLocation(driver, dataSet, config, UniqueKey_Franchisee, testCaseId);
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Remark for Franchisee Location");
				infomgrRest.CreateRemarkRestAPI(driver, dataSet, config, testCaseId, UniqueKey_Remark);
				// infomgrRest.UpdateRemarkRestAPI(driver, dataSet, config,
				// testCaseId, UniqueKey_Remark);
			}
			if (Franchisee != null) {
				fc.utobj().printTestStep("Validating Franchisee Loaction in InfoMgr on View Page");
				fc.loginpage().login(driver);
				fc.infomgr().infomgr_common().InfoMgrCorporateLocations(driver);

				InfoMgrCorporateLocationsPage objFranPage = new InfoMgrCorporateLocationsPage(driver);
				try {
					try {
						fc.utobj().clickElement(driver, objFranPage.lnkShowAll);
					} catch (Exception ex) {
					}
					fc.utobj().clickPartialLinkText(driver, UniqueKey_Franchisee.get("franchiseeName"));
					fc.utobj().sleep(2500);
				} catch (Exception ex) {
					fc.utobj().throwsException("Franchise Location not found in View Page" + testCaseId);
				}

			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "infomgrRestAPI", "FCRestAPI" })
	@TestCase(createdOn = "2017-06-16", updatedOn = "2017-06-16", testCaseDescription = "Add Task for Info Mgr In RestAPI", testCaseId = "TC_21_Fill_Task_Tab_Info_Mgr_Update")
	public void AddTaskInfoMgrRest() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("infomgrRestAPI", testCaseId);
		Map<String, String> UniqueKey_Franchisee = new HashMap<String, String>();
		Map<String, String> UniqueKey_CorpUser = new HashMap<String, String>();
		Map<String, String> UniqueKey_Task = new HashMap<String, String>();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			UserCreation uc = new UserCreation();
			InfoMgrRestService infomgrRest = new InfoMgrRestService();
			fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Corporate User");
			boolean createUser = uc.createCorpUser(driver, config, dataSet, testCaseId, UniqueKey_CorpUser);
			String Franchisee = null;
			if (createUser) {
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Franchisee Location");
				Franchisee = infomgrRest.AddLocation(driver, dataSet, config, UniqueKey_Franchisee, testCaseId);
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Adding Tasl for Franchisee Location");
				infomgrRest.CreateTaskRestAPI(driver, dataSet, config, testCaseId, UniqueKey_Task, UniqueKey_CorpUser);
				fc.utobj().printTestStep("Navigating to InfoMgr > RestAPI > Validating Task for Franchisee Location");
				infomgrRest.UpdateTaskRestAPI(driver, dataSet, config, testCaseId, UniqueKey_Task, UniqueKey_CorpUser);
			}
			if (Franchisee != null) {
				fc.utobj().printTestStep("Validating Franchisee Loaction in InfoMgr on View Page");
				fc.loginpage().login(driver);
				fc.infomgr().infomgr_common().InfoMgrCorporateLocations(driver);

				InfoMgrCorporateLocationsPage objFranPage = new InfoMgrCorporateLocationsPage(driver);
				try {
					try {
						fc.utobj().clickElement(driver, objFranPage.lnkShowAll);
					} catch (Exception ex) {
					}
					fc.utobj().clickPartialLinkText(driver, UniqueKey_Franchisee.get("franchiseeName"));
					fc.utobj().sleep(2500);
				} catch (Exception ex) {
					fc.utobj().throwsException("Franchise Location not found in View Page" + testCaseId);
				}

			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

}
