package com.builds.test.infomgr;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Reporter;

import com.builds.uimaps.infomgr.InfoMgrSearchPage;
import com.builds.utilities.FranconnectUtil;

public class SearchCommonMethods {

	FranconnectUtil fc = new FranconnectUtil();

	public void searchPageShowAll(WebDriver driver) throws Exception {
		fc.infomgr().infomgr_common().InfoMgrSearch(driver);
		if (fc.utobj().assertLinkText(driver, "Show All")) {
			fc.utobj().clickLink(driver, "Show All");
		}
	}

	public void search(WebDriver driver, Map<String, String> config, InfoMgrFranchiseeFilter franchisee)
			throws Exception {
		Reporter.log("************************ Verify Search ***********");
		String testCaseId = "TC_InfoMgr_Search";
		InfoMgrSearchPage franchiseeFilterPage = new InfoMgrSearchPage(driver);

		if (fc.utobj().validate(testCaseId) == true) {
			try {

				fc.utobj().setToDefault(driver, franchiseeFilterPage.drpFranchiseeType);
				fc.utobj().selectValFromMultiSelect(driver, franchiseeFilterPage.drpFranchiseeType,
						franchisee.getStatusID());
				// fc.utobj().clickElement(driver,
				// franchiseeFilterPage.drpFranchiseeType);

				fc.utobj().sendKeys(driver, franchiseeFilterPage.txtFranchiseID, franchisee.getFranchiseeID());
				fc.utobj().sendKeys(driver, franchiseeFilterPage.txtCenterName, franchisee.getCenterName());
				fc.utobj().sendKeys(driver, franchiseeFilterPage.txtCity, franchisee.getCity());

				fc.utobj().setToDefault(driver, franchiseeFilterPage.drpCountry);
				fc.utobj().selectValFromMultiSelect(driver, franchiseeFilterPage.drpCountry, franchisee.getCountry());
				// fc.utobj().clickElement(driver,
				// franchiseeFilterPage.drpCountry);

				fc.utobj().sendKeys(driver, franchiseeFilterPage.txtZipCode, franchisee.getZipCode());
				fc.utobj().sleep();
				fc.utobj().setToDefault(driver, franchiseeFilterPage.drpState);
				fc.utobj().selectValFromMultiSelect(driver, franchiseeFilterPage.drpState, franchisee.getState());
				// fc.utobj().clickElement(driver,
				// franchiseeFilterPage.drpState);

				fc.utobj().sendKeys(driver, franchiseeFilterPage.txtPhoneNumbers, franchisee.getPhone());
				fc.utobj().sendKeys(driver, franchiseeFilterPage.txtEmailIds, franchisee.getEmailID());

				fc.utobj().sendKeys(driver, franchiseeFilterPage.txtFirstName, franchisee.getOwnerFirstName());
				fc.utobj().sendKeys(driver, franchiseeFilterPage.txtLastName, franchisee.getOwnerLastName());

				fc.utobj().sendKeys(driver, franchiseeFilterPage.txtSpouseFirstName, franchisee.getSpouseFirstName());
				fc.utobj().sendKeys(driver, franchiseeFilterPage.txtSpouseLastName, franchisee.getSpouseLastName());

				fc.utobj().setToDefault(driver, franchiseeFilterPage.drpDivision);
				fc.utobj().selectValFromMultiSelect(driver, franchiseeFilterPage.drpDivision,
						franchisee.getDivisionID());
				// fc.utobj().clickElement(driver,
				// franchiseeFilterPage.drpDivision);

				fc.utobj().clickElement(driver, franchiseeFilterPage.btnSearch);

				boolean isFranchiseeOnPage = fc.utobj().assertLinkText(driver, franchisee.getFranchiseeID());
				List<WebElement> lstchkbox = driver
						.findElements(By.xpath(".//*[@name='mailMergeForm']//following::input[@type='checkbox']"));
				if (isFranchiseeOnPage && lstchkbox.size() == 2) {
					Reporter.log("Search filter is working fine !!!");
				} else {
					fc.utobj().throwsException("Error in searching franchisee !!!");
				}

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in searching franchisee, please refer screenshot!");
		}
	}

}
