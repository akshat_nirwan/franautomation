package com.builds.test.infomgr;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;

import com.builds.uimaps.infomgr.InfoMgrLogATaskPage;
import com.builds.uimaps.infomgr.InfoMgrTasksPage;
import com.builds.utilities.FranconnectUtil;

public class TasksCommonMethods {

	FranconnectUtil fc = new FranconnectUtil();

	public String addTask(WebDriver driver, Map<String, String> config, String franchiseID, String Owner,
			String corporateUser) throws Exception {
		String testCaseId = "TC_InFoMgr_Tasks_AddTask";
		Map<String, String> dsLogATask = fc.utobj().readTestData("infomgr", testCaseId);

		InfoMgrTasksPage taskPage = new InfoMgrTasksPage(driver);

		String subject = fc.utobj().generateTestData(dsLogATask.get("subject"));
		WebDriverWait wait = new WebDriverWait(driver, 60);

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().clickLink(driver, "Add Task");
				fc.commonMethods().switch_cboxIframe_frameId(driver);

				fc.utobj().selectDropDownByVisibleText(driver, taskPage.drpCategory, "Franchise ID");
				fc.utobj().selectValFromMultiSelect(driver, taskPage.drpFranchisee, franchiseID);
				fc.utobj().selectValFromMultiSelect(driver, taskPage.drpOwner, Owner);
				fc.utobj().clickElement(driver, taskPage.btnContinue);

				InfoMgrLogATaskPage logaTaskPage = taskPage.getLogaTaskPage();
				
				fc.utobj().selectValFromMultiSelectWithoutReset(driver, logaTaskPage.multiCheckBoxCorporateUsers, corporateUser);
				fc.utobj().selectDropDownByPartialText(driver, logaTaskPage.drpStatus, dsLogATask.get("status"));
				fc.utobj().selectDropDownByVisibleText(driver, logaTaskPage.drpTaskType, dsLogATask.get("taskType"));
				fc.utobj().sendKeys(driver, logaTaskPage.txtSubject, subject);
				fc.utobj().sendKeys(driver, logaTaskPage.txtStartDate, fc.utobj().getFutureDateUSFormat(5));
				fc.utobj().clickElement(driver, logaTaskPage.btnAdd);
				fc.utobj().switchFrameToDefault(driver);
				searchTask(driver, subject);
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in adding a task , please refer screenshot!");
		}
		return subject;
	}

	public String modifyTask(WebDriver driver, Map<String, String> config, String taskSubject) throws Exception {
		String testCaseId = "TC_InFoMgr_Tasks_ModifyTask";
		Map<String, String> dsLogATask = fc.utobj().readTestData("infomgr", testCaseId);

		InfoMgrTasksPage taskPage = new InfoMgrTasksPage(driver);
		InfoMgrLogATaskPage logaTaskPage = taskPage.getLogaTaskPage();
		fc.utobj().actionImgOption(driver, taskSubject, "Modify");

		String subject = fc.utobj().generateTestData(dsLogATask.get("subject"));
		WebDriverWait wait = new WebDriverWait(driver, 60);

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().selectDropDownByPartialText(driver, logaTaskPage.drpStatus, dsLogATask.get("status"));
				fc.utobj().selectDropDownByVisibleText(driver, logaTaskPage.drpTaskType, dsLogATask.get("taskType"));
				fc.utobj().sendKeys(driver, logaTaskPage.txtSubject, subject);
				fc.utobj().sendKeys(driver, logaTaskPage.txtStartDate, fc.utobj().getFutureDateUSFormat(5));
				fc.utobj().clickElement(driver, logaTaskPage.btnAdd);
				fc.utobj().switchFrameToDefault(driver);
				searchTask(driver, subject);
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in modifying a task , please refer screenshot!");
		}
		return subject;
	}

	public void processTask(WebDriver driver, Map<String, String> config, String taskSubject) throws Exception {
		String testCaseId = "TC_InFoMgr_Tasks_ProcessTask";
		Map<String, String> dsLogATask = fc.utobj().readTestData("infomgr", testCaseId);

		InfoMgrTasksPage taskPage = new InfoMgrTasksPage(driver);
		InfoMgrLogATaskPage logaTaskPage = taskPage.getLogaTaskPage();
		fc.utobj().actionImgOption(driver, taskSubject, "Process");

		String status = dsLogATask.get("status");
		WebDriverWait wait = new WebDriverWait(driver, 60);

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().selectDropDownByPartialText(driver, logaTaskPage.drpStatus, status);
				fc.utobj().clickElement(driver, logaTaskPage.btnProcess);
				fc.utobj().switchFrameToDefault(driver);
				WebElement elementStatus = driver.findElement(By
						.xpath(".//*[text()='" + taskSubject + "']/following::*[contains(text() ,'" + status + "')]"));
				if (fc.utobj().getText(driver, elementStatus).contains(status)) {
					Reporter.log("Task has been processed successfully !");
				} else {
					fc.utobj().throwsException("Some problem occured while processing the task");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in processing the task , please refer screenshot!");
		}

	}

	public void viewTask(WebDriver driver, Map<String, String> config, String taskSubject) throws Exception {
		String testCaseId = "TC_InFoMgr_Tasks_ViewTask";
		InfoMgrLogATaskPage taskPage = new InfoMgrLogATaskPage(driver);

		WebDriverWait wait = new WebDriverWait(driver, 60);

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().getElement(driver, taskPage.lnkModify);
				if (fc.utobj().assertPageSource(driver, taskSubject)) {
					Reporter.log("Task is displayed properly");
				} else {
					fc.utobj().throwsException("Error in viewing the task");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
				fc.utobj().clickElement(driver, taskPage.btnClose);
				fc.utobj().switchFrameToDefault(driver);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in viewing a task , please refer screenshot!");
		}
	}

	public void deleteTask(WebDriver driver, Map<String, String> config, String taskSubject, int deleteLocation)
			throws Exception {
		String testCaseId = "TC_InFoMgr_Task_Delete";
		InfoMgrTasksPage taskPage = new InfoMgrTasksPage(driver);
		if (fc.utobj().validate(testCaseId) == true) {
			try {
				if (deleteLocation == 1) {
					checkLocation(driver, taskSubject);
					fc.utobj().selectActionMenuItemsWithTagInput(driver, "Delete");
				} else if (deleteLocation == 2) {
					fc.utobj().actionImgOption(driver, taskSubject, "Delete");
				} else {
					checkLocation(driver, taskSubject);
					fc.utobj().clickElement(driver, taskPage.btnDelete);
				}

				driver.switchTo().alert().accept();
				fc.utobj().switchFrameToDefault(driver);
				fc.utobj().sleep();
				if (fc.utobj().assertLinkText(driver, "Show Filters")) {
					fc.utobj().sleep();
					fc.utobj().clickElement(driver, ".//*[@id='showFilter']");
				}
				fc.utobj().sendKeys(driver, taskPage.txtSubject, taskSubject);
				fc.utobj().clickElement(driver, taskPage.btnSearch);
				if (!fc.utobj().assertLinkText(driver, taskSubject)) {
					Reporter.log(taskSubject + " task is deleted !!!");
				} else {
					fc.utobj().throwsException("Some problem occured while deleting the task " + taskSubject);
				}

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in deleting task , please refer screenshot!");
		}
	}

	public void VerifyPrintPreview(WebDriver driver, String textTobeVerified) throws Exception {
		Set<String> windows = new HashSet<String>();
		windows = driver.getWindowHandles();

		boolean isTextDisplayed = false;

		String currentWindow = driver.getWindowHandle();
		for (String window : windows) {
			if (!window.equals(currentWindow)) {

				driver.switchTo().window(window);
				if (driver.getCurrentUrl().contains("printcheck")) {
					isTextDisplayed = fc.utobj().assertPageSource(driver, textTobeVerified);
				}
			}
		}
		driver.close();

		if (isTextDisplayed) {
			Reporter.log(textTobeVerified + "  is displayed on the print preview page. !!! ");
		} else {
			fc.utobj().throwsException(textTobeVerified + "  is not displayed on the print preview page. !!! ");
		}

		driver.switchTo().window(currentWindow);
	}

	public void searchTask(WebDriver driver, String taskSubject) throws Exception {
		InfoMgrTasksPage taskPage = new InfoMgrTasksPage(driver);
		fc.utobj().sleep();
		if (fc.utobj().assertLinkText(driver, "Show Filters")) {
			fc.utobj().printTestStep("Show Filter Found Now going to click on It");
			fc.utobj().sleep();
			
			try {
				fc.utobj().clickElement(driver, ".//span[@id='showFilter']/a");
			}
			catch(Exception e)
			{
				fc.utobj().clickLink(driver, "Show Filters");
			}
			
			
			
			//fc.utobj().clickElement(driver, ".//*[@id='showFilter']");
		}
		fc.utobj().sendKeys(driver, taskPage.txtSubject, taskSubject);
		fc.utobj().clickElement(driver, taskPage.btnSearch);
		if (fc.utobj().assertLinkText(driver, taskSubject)) {
			Reporter.log(taskSubject + " is displayed on the page");
		} else {
			fc.utobj().throwsException("Task is not displayed on the page " + taskSubject);
		}
	}

	public void checkLocation(WebDriver driver, String location) throws Exception {
		String xpath = ".//*[@id='siteMainTable']//a[text()='" + location + "']/preceding::input[@type='checkbox'][1]";
		WebElement checkBox = fc.utobj().getElementByXpath(driver, xpath);
		fc.utobj().clickElement(driver, checkBox);
	}

	public Map<String, String> addTaskForFilter(WebDriver driver, Map<String, String> config, String franchiseID,
			String Owner, String corporateUser) throws Exception {
		String testCaseId = "TC_InFoMgr_Tasks_AddTask_Filter";
		Map<String, String> dsLogATask = fc.utobj().readTestData("infomgr", testCaseId);

		InfoMgrTasksPage taskPage = new InfoMgrTasksPage(driver);
		InfoMgrLogATaskPage logaTaskPage = taskPage.getLogaTaskPage();
		Map<String, String> taskDetails = new HashMap<String, String>();
		WebDriverWait wait = new WebDriverWait(driver, 60);

		taskDetails.put("category", "Franchise ID");
		taskDetails.put("subject", fc.utobj().generateTestData(dsLogATask.get("subject")));
		taskDetails.put("taskstatus", dsLogATask.get("status"));
		taskDetails.put("reminder", "No");
		taskDetails.put("corporateuser", corporateUser);
		taskDetails.put("tasktype", "Default");
		taskDetails.put("priority", dsLogATask.get("priority"));

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				fc.utobj().clickLink(driver, "Add Task");
				fc.commonMethods().switch_cboxIframe_frameId(driver);

				fc.utobj().selectDropDownByVisibleText(driver, taskPage.drpCategory, taskDetails.get("category"));

				fc.utobj().selectValFromMultiSelect(driver, taskPage.drpFranchisee, franchiseID);
				fc.utobj().selectValFromMultiSelect(driver, taskPage.drpOwner, Owner);
				fc.utobj().clickElement(driver, taskPage.btnContinue);

				fc.utobj().selectValFromMultiSelect(driver, logaTaskPage.multiCheckBoxCorporateUsers,
						taskDetails.get("corporateuser"));
				fc.utobj().clickElement(driver, logaTaskPage.multiCheckBoxCorporateUsers);

				fc.utobj().selectDropDownByPartialText(driver, logaTaskPage.drpStatus, taskDetails.get("taskstatus"));
				fc.utobj().selectDropDownByVisibleText(driver, logaTaskPage.drpTaskType, taskDetails.get("tasktype"));
				fc.utobj().sendKeys(driver, logaTaskPage.txtSubject, taskDetails.get("subject"));
				fc.utobj().selectDropDownByVisibleText(driver, logaTaskPage.drpPriority, taskDetails.get("priority"));
				fc.utobj().clickElement(driver, logaTaskPage.rdoNoReminder);
				fc.utobj().sendKeys(driver, logaTaskPage.txtStartDate, fc.utobj().getFutureDateUSFormat(5));
				fc.utobj().clickElement(driver, logaTaskPage.btnAdd);
				fc.utobj().switchFrameToDefault(driver);
				searchTask(driver, taskDetails.get("subject"));
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in adding a task , please refer screenshot!");
		}
		return taskDetails;
	}

	public void search(WebDriver driver, Map<String, String> config, Map<String, String> taskFilter) throws Exception {
		Reporter.log("************************ Verify Search ***********");
		String testCaseId = "TC_InfoMgr_Tasks_Search";
		InfoMgrTasksPage taskPage = new InfoMgrTasksPage(driver);
		if (fc.utobj().assertLinkText(driver, "Show Filters")) {
			fc.utobj().sleep();
			fc.utobj().clickLink(driver, "Show Filters");
		}

		if (fc.utobj().validate(testCaseId) == true) {
			try {
				//fc.utobj().selectDropDownByVisibleText(driver, taskPage.drpCategory, taskFilter.get("category"));
				fc.utobj().sendKeys(driver, taskPage.txtSubjectFilter, taskFilter.get("subject"));

				//fc.utobj().setToDefault(driver, taskPage.drpStatus);
				fc.utobj().selectValFromMultiSelect(driver, taskPage.drpStatus, taskFilter.get("taskstatus"));
				fc.utobj().selectDropDownByVisibleText(driver, taskPage.drpReminder, taskFilter.get("reminder"));

				//fc.utobj().setToDefault(driver, taskPage.drpPriority);
				fc.utobj().selectValFromMultiSelect(driver, taskPage.drpPriority, taskFilter.get("priority"));

				//fc.utobj().setToDefault(driver, taskPage.drpView);
				fc.utobj().selectValFromMultiSelect(driver, taskPage.drpView, taskFilter.get("corporateuser"));

				//fc.utobj().setToDefault(driver, taskPage.drpTaskType);
				fc.utobj().selectValFromMultiSelect(driver, taskPage.drpTaskType, taskFilter.get("tasktype"));

				fc.utobj().clickElement(driver, taskPage.btnSearch);

				boolean isFranchiseeOnPage = fc.utobj().assertLinkText(driver, taskFilter.get("subject"));
				if (isFranchiseeOnPage) {
					Reporter.log("Search filter is working fine !!!");
				} else {
					fc.utobj().throwsException("Error in searching multi unit !!!");
				}

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Error in searching multi unit, please refer screenshot!");
		}
	}

}
