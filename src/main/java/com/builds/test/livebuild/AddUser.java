package com.builds.test.livebuild;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.test.common.CorporateUser;
import com.builds.utilities.FranconnectUtil;

public class AddUser {
	
	
	FranconnectUtil fc = new FranconnectUtil();
	String url = "https://qa3.franconnectstg.net/fc/rest/dataservices/create";
	private static final String USER_AGENT = "Mozilla/5.0";
	private static final String POST_PARAMS = "userName=FRANC0NN3CT_API";
	
	@Test
	public void addUser() throws Exception
	{
		for(int x=0;x<1000;x++)
		{
			WebDriver driver = null;
			
			CorporateUser cu = fc.commonMethods().fillDefaultDataFor_CorporateUser(new CorporateUser());
			createDefaultCorporateUser_Through_WebService_AfterLogin(driver, cu);
		}
	}
	
	
	
	public CorporateUser createDefaultCorporateUser_Through_WebService_AfterLogin(WebDriver driver,
			CorporateUser corpUser) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();

		String responseMessage = null;

		String module = "admin";
		String subModule = "user";
		String isISODate = "no";

		fc.utobj().printTestStep("Adding User through Web Services.");

		String xmlString = "<fcRequest>" + "<adminUser>" + "<userID>" + corpUser.getUserName() + "</userID>"
				+ "<password>" + corpUser.getPassword() + "</password>" + "<userLevel>Corporate</userLevel>"
				+ "<timezone>" + corpUser.getTimezone() + "</timezone>" + "<type>" + corpUser.getUserType() + "</type>"
				+ "<expirationTime>" + corpUser.getExpiryDays() + "</expirationTime>" + "<role>" + corpUser.getRole()
				+ "</role>" + "<jobTitle>" + corpUser.getJobTitle() + "</jobTitle>" + "<firstname>"
				+ corpUser.getFirstName() + "</firstname>" + "<lastname>" + corpUser.getLastName() + "</lastname>"
				+ "<address>" + corpUser.getAddress() + "</address>" + "<city>" + corpUser.getCity() + "</city>"
				+ "<state>" + corpUser.getState() + "</state>" + "<zipCode>" + corpUser.getZipcode() + "</zipCode>"
				+ "<country>" + corpUser.getCountry() + "</country>" + "<phone1>" + corpUser.getPhone1() + "</phone1>"
				+ "<phoneExt1>" + corpUser.getPhoneExt1() + "</phoneExt1>" + "<phone2>" + corpUser.getPhone2()
				+ "</phone2>" + "<phoneExt2>" + corpUser.getPhoneExt2() + "</phoneExt2>" + "<fax>" + corpUser.getFax()
				+ "</fax>" + "<mobile>" + corpUser.getMobile() + "</mobile>" + "<email>" + corpUser.getEmail()
				+ "</email>" + "<sendNotification>" + corpUser.getSendNotification() + "</sendNotification>"
				+ "<loginUserIp>" + corpUser.getLoginUserIp() + "</loginUserIp>" + "<isBillable>"
				+ corpUser.getIsBillable() + "</isBillable>" + "<isDaylight>" + corpUser.getIsDaylight()
				+ "</isDaylight>" + "<isAuditor>" + corpUser.getConsultant() + "</isAuditor>" + "</adminUser>"
				+ "</fcRequest>";

		try {
			responseMessage = create(module, subModule, xmlString, isISODate);
		} catch (Exception e) {

		}
		
		//System.out.println(">>>>>>>>>>>>>>>>    "+responseMessage);

		return corpUser;
	}
	
	public String create(String module, String subModule, String xmlString, String isISODate) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();

		System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
		try {

			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			con.setRequestMethod("POST");
			con.setRequestProperty("User-Agent", USER_AGENT);

			String clientCode = "clientCode=FRANC0NN3CT_API&&key=pHNsc6x5mBksJDfo&&fromWhere=jsp&&responseType=xml&&environment=qa&&"
					+ "module=" + module + "&&subModule=" + subModule + "&&xmlString=" + xmlString
					+ "&&operation=create&&isISODate=no&&roleTypeForQuery=-1";

			System.out.println(clientCode);

			con.setDoOutput(true);
			OutputStream os = con.getOutputStream();
			os.write(clientCode.getBytes());
			os.write(POST_PARAMS.getBytes());

			os.flush();
			os.close();

			int responseCode = con.getResponseCode();

			if (responseCode == HttpURLConnection.HTTP_OK) { // success
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();
				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();
				return response.toString();
			} else {
				fc.utobj().throwsSkipException("WebService is not working." + "Response Code : " + responseCode);
			}
			if (responseCode == HttpURLConnection.HTTP_OK) { // success
			} else {
				fc.utobj().throwsSkipException("WebService is not working." + "Response Code : " + responseCode);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			fc.utobj().throwsSkipException("WebService is not working." + e.getMessage());
		}
		return "";

	}
}
