package com.builds.test.livebuild;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminPageTest;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class LiveSmokeTesting {
	FranconnectUtil fc = new FranconnectUtil();
	
	
	@Test(groups = { "smokelive" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FS_LeadManagement_ChangeStatus_001", testCaseDescription = "Verify Change Status of of Lead Through Batch")
	void changeStatusActionMenu_WithoutRemarks() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			
			AdminPageTest adminPage = fc.commonMethods().getModules().openAdminPage(driver);
			adminPage.openAddNewAreaRegion(driver);
			
			String regionName = fillRegionDetails(driver);
			selectFranchiseeAssignment(driver);
			searchAreaRegion(driver, regionName);
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	
	public String fillRegionDetails(WebDriver driver) throws Exception
	{
		String regionName = "AutomationRegion";
		
		fc.utobj().printTestStep("Fill Region Details");
		UIPage ui = new UIPage(driver);
		fc.utobj().selectDropDown(driver, ui.category, "Domestic");
		fc.utobj().sendKeys(driver, ui.regioname, "AutomationRegion");
		fc.utobj().selectDropDown(driver, ui.groupBy, "Zip Codes");
		fc.utobj().clickElement(driver, ui.allCheck);
		fc.utobj().sendKeys(driver, ui.ziplist, "1234556");
		fc.utobj().printTestStep("Submit Add Region Page");
		fc.utobj().clickElement(driver, ui.submit);
		return regionName;
		
	}
	
	public void selectFranchiseeAssignment(WebDriver driver) throws Exception
	{
		fc.utobj().printTestStep("Select Franchisee Assignment Area / Region");
		UIPage ui = new UIPage(driver);
		fc.utobj().clickElement(driver, ui.NextButton);
	}
	
	public void searchAreaRegion(WebDriver driver,String regionName) throws Exception
	{
		fc.utobj().printTestStep("Search Area Region");
		UIPage ui = new UIPage(driver);
		fc.utobj().selectValFromMultiSelect(driver, ui.areaRegionMulti, regionName);
		
		fc.utobj().assertSingleLinkText(driver, regionName);
	}
	
	
	
	
}
