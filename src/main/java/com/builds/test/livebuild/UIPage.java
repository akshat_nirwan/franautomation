package com.builds.test.livebuild;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class UIPage {

	//Fill Region Details
	
	@FindBy(id="category")
	public WebElement category;
	
	@FindBy(name="regioname")
	public WebElement regioname;
	
	@FindBy(id="groupBy")
	public WebElement groupBy;
	
	@FindBy(id="all")
	public WebElement allCheck;
	
	@FindBy(name="ziplist")
	public WebElement ziplist;
	
	@FindBy(name="button")
	public WebElement submit;
	
	
	
	
	
	//Select Franchisee Assignment Area / Region
	
	@FindBy(xpath=".//input[@class='cm_new_button' and @value='Next']")
	public WebElement NextButton;
	
	
	
	
	//Manage Area / Region
	
	
	@FindBy(xpath=".//*[@id='ms-parentregionKey']")
	public WebElement areaRegionMulti;
	
	
	
	
	
	
	
	
	public UIPage(WebDriver driver)
	{
		PageFactory.initElements(driver, this);
	}
	
}
