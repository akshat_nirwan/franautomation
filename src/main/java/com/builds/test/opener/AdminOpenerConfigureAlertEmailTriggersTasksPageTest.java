package com.builds.test.opener;

import java.util.Map;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminHiddenLinksConfigureNewHierarchyLevelPageTest;
import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.common.CorporateUser;
import com.builds.uimaps.opener.AdminOpenerConfigureAlertEmailTriggersTasksPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminOpenerConfigureAlertEmailTriggersTasksPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "opener" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Admin > Opener > Configure Alert Email Triggers for Tasks", testCaseId = "TC_85_Add_Alert")
	public void addAlert() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest addcorUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "openerautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			addcorUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Add Alert");
			AdminOpenerConfigureAlertEmailTriggersTasksPage pobj = new AdminOpenerConfigureAlertEmailTriggersTasksPage(
					driver);

			fc.opener().opener_common().adminFranchiseOpenerConfigureAlertEmailTriggersTasks(driver);

			fc.utobj().clickElement(driver, pobj.addAlertLnk);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String fromEmail = fc.utobj().generateRandomChar().concat("@gmail.com");
			fc.utobj().sendKeys(driver, pobj.fromEmail, fromEmail);

			Random random = new Random();
			int i = random.nextInt(100);
			String value = Integer.toString(i);
			System.out.println(value);

			fc.utobj().sendKeys(driver, pobj.frequencyAlertDays, value);
			fc.utobj().selectDropDown(driver, pobj.criticalLevel, dataSet.get("criticalLevel"));
			fc.utobj().sendKeys(driver, pobj.alertEmailSubject, dataSet.get("alertEmailSubject"));
			fc.utobj().sendKeys(driver, pobj.emailContent, dataSet.get("emailContent"));
			fc.utobj().clickElement(driver, pobj.sendAlertEmailBtn);
			fc.utobj().sendKeys(driver, pobj.sendAlertEmailTxBx, corpUser.getuserFullName());
			fc.utobj().clickElement(driver, pobj.sendEmailAlertEmailSelectAll);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			try {
				fc.utobj().clickElement(driver, pobj.showAllLink);
			} catch (Exception e) {

			}

			fc.utobj().printTestStep("Verify The Add Alert");
			if (!driver
					.findElement(By.xpath(".//*[contains(text () ,'" + fromEmail
							+ "')]/ancestor::tr/td[contains(text () ,'" + corpUser.getuserFullName() + "')]"))
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to Add Alert");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addAlert(WebDriver driver, Map<String, String> dataSet, String userName, Map<String, String> config)
			throws Exception {

		String testCaseId = "TC_Add_Alert_Opener_Configure_Alert_Email_Triggers";
		String fromEmail = null;

		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminOpenerConfigureAlertEmailTriggersTasksPage pobj = new AdminOpenerConfigureAlertEmailTriggersTasksPage(
						driver);

				fc.opener().opener_common().adminFranchiseOpenerConfigureAlertEmailTriggersTasks(driver);

				fc.utobj().clickElement(driver, pobj.addAlertLnk);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fromEmail = fc.utobj().generateRandomChar().concat("@gmail.com");
				fc.utobj().sendKeys(driver, pobj.fromEmail, fromEmail);

				Random random = new Random();
				int i = random.nextInt(100);
				String value = Integer.toString(i);

				fc.utobj().sendKeys(driver, pobj.frequencyAlertDays, value);
				fc.utobj().selectDropDown(driver, pobj.criticalLevel, dataSet.get("criticalLevel"));
				fc.utobj().sendKeys(driver, pobj.alertEmailSubject, dataSet.get("alertEmailSubject"));
				fc.utobj().sendKeys(driver, pobj.emailContent, dataSet.get("emailContent"));
				fc.utobj().clickElement(driver, pobj.sendAlertEmailBtn);
				fc.utobj().sendKeys(driver, pobj.sendAlertEmailTxBx, userName);
				fc.utobj().clickElement(driver, pobj.sendEmailAlertEmailSelectAll);
				fc.utobj().clickElement(driver, pobj.saveBtn);
				fc.utobj().clickElement(driver, pobj.closeBtn);
				driver.switchTo().defaultContent();

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to add Alert Opener");

		}

		return fromEmail;
	}

	@Test(groups = { "opener" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Modify Alert :: Admin > Opener > Configure Alert Email Triggers for Tasks", testCaseId = "TC_86_Modify_Alert")
	public void modifyAlert() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminOpenerConfigureAlertEmailTriggersTasksPage pobj = new AdminOpenerConfigureAlertEmailTriggersTasksPage(
					driver);

			fc.utobj().printTestStep("Enable Divisional Users");
			AdminHiddenLinksConfigureNewHierarchyLevelPageTest configure_level = new AdminHiddenLinksConfigureNewHierarchyLevelPageTest();
			configure_level.ConfigureNewHierarchyLevel(driver);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest addcorUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			/*
			 * String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String password=dataSet.get("password");
			 * addcorUser.addCorporateUser(driver, userName, password, config);
			 */

			String emailId = "openerautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			addcorUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Add Aler");
			String fromEmail = addAlert(driver, dataSet, corpUser.getuserFullName(), config);

			fc.utobj().showAll(driver);

			fc.utobj().printTestStep("Modify The Alert");
			// modify Alert Email
			String altertText = driver
					.findElement(By.xpath(
							".//*[contains(text () ,'" + fromEmail + "')]/ancestor::tr/td/div[@id='menuBar']/layer"))
					.getAttribute("id").trim();
			altertText = altertText.replace("Actions_dynamicmenu", "");
			altertText = altertText.replace("Bar", "");

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[contains(text () ,'" + fromEmail + "')]/ancestor::tr/td/div[@id='menuBar']/layer/a/img"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//div[@id='Actions_dynamicmenu" + altertText + "Menu']/span[contains(text () ,'Modify')]"));

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fromEmail = fc.utobj().generateRandomChar().concat("@gmail.com");
			fc.utobj().sendKeys(driver, pobj.fromEmail, fromEmail);

			Random random = new Random();
			int i = random.nextInt(100);
			String value = Integer.toString(i);
			System.out.println(value);

			fc.utobj().sendKeys(driver, pobj.frequencyAlertDays, value);
			fc.utobj().selectDropDown(driver, pobj.criticalLevel, dataSet.get("criticalLevel"));
			fc.utobj().sendKeys(driver, pobj.alertEmailSubject, dataSet.get("alertEmailSubject"));
			fc.utobj().sendKeys(driver, pobj.emailContent, dataSet.get("emailContent"));
			fc.utobj().clickElement(driver, pobj.sendAlertEmailBtn);
			fc.utobj().clickElement(driver, pobj.sendEmailAlertEmailSelectAll);
			fc.utobj().clickElement(driver, pobj.sendEmailAlertEmailSelectAll);
			fc.utobj().sendKeys(driver, pobj.sendAlertEmailTxBx, "Division");
			fc.utobj().clickElement(driver, pobj.sendEmailAlertEmailSelectAll);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().showAll(driver);

			fc.utobj().printTestStep("Verify The Modify The Alert");

			if (!fc.utobj().getElementByXpath(driver, ".//*[contains(text () ,'" + fromEmail
					+ "')]/ancestor::tr/td[contains(text () ,'Divisional Users')]").isDisplayed()) {
				fc.utobj().throwsException("was not able to modify Alert");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "opener" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Admin > Opener > Configure Alert Email Triggers for Tasks", testCaseId = "TC_87_Delete_Alert")
	public void deleteAlert() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest addcorUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			String emailId = "openerautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			addcorUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Add Alert");
			String fromEmail = addAlert(driver, dataSet, corpUser.getuserFullName(), config);

			fc.utobj().showAll(driver);

			fc.utobj().printTestStep("Delete The Alert");
			// Delete Alert Email
			String altertText = driver
					.findElement(By.xpath(
							".//*[contains(text () ,'" + fromEmail + "')]/ancestor::tr/td/div[@id='menuBar']/layer"))
					.getAttribute("id").trim();
			altertText = altertText.replace("Actions_dynamicmenu", "");
			altertText = altertText.replace("Bar", "");

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[contains(text () ,'" + fromEmail + "')]/ancestor::tr/td/div[@id='menuBar']/layer/a/img"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//div[@id='Actions_dynamicmenu" + altertText + "Menu']/span[contains(text () ,'Delete')]"));
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().showAll(driver);

			fc.utobj().printTestStep("Verify The Delete Alert");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, fromEmail);

			if (isTextPresent == true) {
				fc.utobj().throwsException("was not able to Delete Alert");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
}
