package com.builds.test.opener;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.uimaps.opener.AdminOpenerConfigureChecklistDisplaySettingPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminOpenerConfigureChecklistDisplaySettingPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "opener" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Configure Checklist Display Setting Based EOD :: Admin > Opener > Configure Checklist Display Setting", testCaseId = "TC_83_Based_On_EOD")
	public void basedOnEod() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("opener",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminOpenerConfigureChecklistDisplaySettingPage pobj = new AdminOpenerConfigureChecklistDisplaySettingPage(
					driver);

			fc.utobj().printTestStep("Configure Checklist Display Setting Based EOD");
			fc.opener().opener_common().adminFranchiseOpenerConfigureChecklistDisplaySetting(driver);

			if (!fc.utobj().isSelected(driver,pobj.startDay.get(0))) {

				fc.utobj().clickElement(driver, pobj.startDay.get(0));

			}
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("Verify The Configure Checklist Display Setting Based EOD");
			if (!fc.utobj().isSelected(driver,pobj.startDay.get(0))) {
				fc.utobj().throwsException("was not able to Configure Checklist Display Setting Base On EOD");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "opener" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Configure Checklist Display Setting Based On Project Start Date :: Admin > Opener > Configure Checklist Display Setting", testCaseId = "TC_84_Based_On_Project_Start_Date")
	public void basedOnProjectStartDate() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("opener",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminOpenerConfigureChecklistDisplaySettingPage pobj = new AdminOpenerConfigureChecklistDisplaySettingPage(
					driver);

			fc.utobj().printTestStep("Configure Checklist Display Setting Based On Project Start Date");
			fc.opener().opener_common().adminFranchiseOpenerConfigureChecklistDisplaySetting(driver);

			if (!fc.utobj().isSelected(driver,pobj.startDay.get(1))) {

				fc.utobj().clickElement(driver, pobj.startDay.get(1));

			}
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("Verify The Configure Checklist Display Setting Based On Project Start Date");
			if (!fc.utobj().isSelected(driver,pobj.startDay.get(1))) {
				fc.utobj().throwsException("was not able to Configure Checklist Display Setting Base On EOD");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
}
