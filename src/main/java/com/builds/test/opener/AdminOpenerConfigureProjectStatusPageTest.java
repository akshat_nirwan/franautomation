package com.builds.test.opener;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import com.builds.uimaps.opener.AdminOpenerConfigureProjectStatusPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminOpenerConfigureProjectStatusPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "opener" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Configure Project Status :: Admin > Opener > Configure Project Status", testCaseId = "TC_77_Add_Project_Status")
	public void addProjectStatus() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminOpenerConfigureProjectStatusPage pobj = new AdminOpenerConfigureProjectStatusPage(driver);

			fc.utobj().printTestStep("Add Project Status");
			fc.opener().opener_common().adminFranchiseOpenerConfigureProjectStatus(driver);
			fc.utobj().clickElement(driver, pobj.addProjectStatusBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String testData = fc.utobj().generateTestData(dataSet.get("projectStatus"));
			fc.utobj().sendKeys(driver, pobj.statusNameBtn, testData);
			fc.utobj().clickElement(driver, pobj.addBtn);

			fc.utobj().clickElement(driver, pobj.okBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Add Project Status");
			boolean isValPresent = fc.utobj().inSelectBox(driver, pobj.listing, testData);
			if (isValPresent == false) {
				fc.utobj().throwsException("Project Status Not added!");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}

	public void addProjectStatus(WebDriver driver, String projectStatus) throws Exception {

		String testCaseId = "TC_Add_Project_Status_Opener";

		if (fc.utobj().validate(testCaseId)) {
			try {

				AdminOpenerConfigureProjectStatusPage pobj = new AdminOpenerConfigureProjectStatusPage(driver);

				fc.opener().opener_common().adminFranchiseOpenerConfigureProjectStatus(driver);
				fc.utobj().clickElement(driver, pobj.addProjectStatusBtn);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().sendKeys(driver, pobj.statusNameBtn, projectStatus);
				fc.utobj().clickElement(driver, pobj.addBtn);

				fc.utobj().clickElement(driver, pobj.okBtn);
				fc.utobj().switchFrameToDefault(driver);
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Was not able to add Project Status");

		}

	}

	@Test(groups = { "opener" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Modify Project Status :: Admin > Opener > Configure Project Status ", testCaseId = "TC_78_modify_Project_Status")
	public void modifyProjectStatus() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminOpenerConfigureProjectStatusPage pobj = new AdminOpenerConfigureProjectStatusPage(driver);

			fc.utobj().printTestStep("Add Project Status");
			String projectStatus = fc.utobj().generateTestData(dataSet.get("projectStatus"));
			addProjectStatus(driver, projectStatus);

			fc.utobj().printTestStep("modify The Project Status");
			String value = fc.utobj().getTextTxtFld(driver, fc.utobj().getElementByXpath(driver,
					".//select[@name='projectOrder']/option[contains(text () , '" + projectStatus + "')]"));
			Select sl = new Select(pobj.listing);
			sl.selectByValue(value);

			// fc.utobj().selectDropDown(driver, pobj.listing, projectStatus);
			fc.utobj().clickElement(driver, pobj.modifyBtn);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			projectStatus = fc.utobj().generateTestData(dataSet.get("projectStatus"));
			fc.utobj().sendKeys(driver, pobj.statusNameBtn, projectStatus);
			fc.utobj().clickElement(driver, pobj.modifyBtn);

			fc.utobj().clickElement(driver, pobj.okBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify the Modify Project Status");
			boolean isValPresent = fc.utobj().inSelectBox(driver, pobj.listing, projectStatus);
			if (isValPresent == false) {
				fc.utobj().throwsException("was not able to modify Project Status!");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}

	@Test(groups = { "opener" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Delete Project Status :: Admin > Opener > Configure Project Status", testCaseId = "TC_79_delete_Project_Status")
	public void deleteProjectStatus() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminOpenerConfigureProjectStatusPage pobj = new AdminOpenerConfigureProjectStatusPage(driver);

			fc.utobj().printTestStep("Add Project Status");
			String projectStatus = fc.utobj().generateTestData(dataSet.get("projectStatus"));
			addProjectStatus(driver, projectStatus);

			String value = fc.utobj().getTextTxtFld(driver, fc.utobj().getElementByXpath(driver,
					".//select[@name='projectOrder']/option[contains(text () , '" + projectStatus + "')]"));
			Select sl = new Select(pobj.listing);
			sl.selectByValue(value);

			fc.utobj().printTestStep("Delete The Project Status");
			// fc.utobj().selectDropDown(driver, pobj.listing, projectStatus);
			fc.utobj().clickElement(driver, pobj.deleteBtn);
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Delete Project Status");
			boolean isValPresent = fc.utobj().inSelectBox(driver, pobj.listing, projectStatus);
			if (isValPresent == true) {
				fc.utobj().throwsException("was not able to delete Project Status!");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}
}