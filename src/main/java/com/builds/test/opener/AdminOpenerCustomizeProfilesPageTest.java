package com.builds.test.opener;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Reporter;

import com.builds.uimaps.opener.AdminOpenerCustomizeProfilesPage;
import com.builds.utilities.FranconnectUtil;

public class AdminOpenerCustomizeProfilesPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	// @Test(groups="adminopener")
	public void addCustomeProfile() throws Exception {

		Reporter.log("TC_71_Add_Custome_Profile :Add Customize Profiles:: Admin > Opener > Customize Profiles.\n");
		Reporter.log("###########################################################################################");

		String testCaseId = "TC_71_Add_Custome_Profile";

		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminOpenerCustomizeProfilesPage pobj = new AdminOpenerCustomizeProfilesPage(driver);
			driver = fc.loginpage().login(driver);
			boolean status = false;
			String fieldDisplayName = "";

			fc.opener().opener_common().adminFranchiseOpenerCustomizeProfiles(driver);

			List<WebElement> list = driver
					.findElements(By.xpath(".//tr[@class='bText12 grAltRw3' or @class='bText12']/td[2]"));
			System.out.println(list.size());

			for (int i = 1; i < list.size(); i++) {
				if (list.get(i).getText().trim().equalsIgnoreCase("")) {

					String text = driver
							.findElement(By
									.xpath(".//table[@class='summaryTblex']//tr/td[.='']//following-sibling::td/div/layer"))
							.getAttribute("id").trim();
					String alterText = text.replace("Actions_dynamicmenu", "");
					alterText = alterText.replace("Bar", "");

					fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
							".//table[@class='summaryTblex']//tr/td[.='']//following-sibling::td/div/layer/a/img"));
					fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
							".//div[@id='Actions_dynamicmenu" + alterText + "Menu']/span[contains(text () , 'Add')]"));

					fc.commonMethods().switch_cboxIframe_frameId(driver);
					fieldDisplayName = fc.utobj().generateTestData(dataSet.get("fieldDisplayName"));
					fc.utobj().sendKeys(driver, pobj.fieldDisplayName, fieldDisplayName);
					fc.utobj().clickElement(driver, pobj.addBtn);
					fc.utobj().clickElement(driver, pobj.closeBtn);
					fc.utobj().switchFrameToDefault(driver);
					status = true;
					break;
				}
			}

			if (status == true) {
				boolean isTextPresent = fc.utobj().assertPageSource(driver, fieldDisplayName);

				if (isTextPresent == false) {
					fc.utobj().throwsException("was not able to add cutomize profile");
				}

			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	// @Test(groups="adminopener")
	public void modifyCutomeProfile() throws Exception {

		Reporter.log(
				"TC_72_Modify_Custome_Profile :Modify Customize Profiles:: Admin > Opener > Customize Profiles.\n");
		Reporter.log("###########################################################################################");

		String testCaseId = "TC_72_Modify_Custome_Profile";

		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminOpenerCustomizeProfilesPage pobj = new AdminOpenerCustomizeProfilesPage(driver);
			driver = fc.loginpage().login(driver);
			boolean status = false;
			String fieldDisplayName = "";

			fc.opener().opener_common().adminFranchiseOpenerCustomizeProfiles(driver);

			List<WebElement> list = driver
					.findElements(By.xpath(".//tr[@class='bText12 grAltRw3' or @class='bText12']/td[2]"));
			System.out.println(list.size());

			for (int i = 1; i < list.size(); i++) {
				if (list.get(i).getText().trim().equalsIgnoreCase("")) {

					String text = driver
							.findElement(By
									.xpath(".//table[@class='summaryTblex']//tr/td[.='']//following-sibling::td/div/layer"))
							.getAttribute("id").trim();
					String alterText = text.replace("Actions_dynamicmenu", "");
					alterText = alterText.replace("Bar", "");

					fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
							".//table[@class='summaryTblex']//tr/td[.='']//following-sibling::td/div/layer/a/img"));
					fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
							".//div[@id='Actions_dynamicmenu" + alterText + "Menu']/span[contains(text () , 'Add')]"));

					fc.commonMethods().switch_cboxIframe_frameId(driver);
					fieldDisplayName = fc.utobj().generateTestData(dataSet.get("fieldDisplayName"));
					fc.utobj().sendKeys(driver, pobj.fieldDisplayName, fieldDisplayName);
					fc.utobj().clickElement(driver, pobj.addBtn);
					fc.utobj().clickElement(driver, pobj.closeBtn);
					fc.utobj().switchFrameToDefault(driver);
					status = true;
					break;
				}
			}

			if (status == true) {

				String text = driver
						.findElement(By
								.xpath(".//*[.='" + fieldDisplayName + "']//ancestor::tr/td/div[@id='menuBar']/layer"))
						.getAttribute("id").trim();
				String alterText = text.replace("Actions_dynamicmenu", "");
				alterText = alterText.replace("Bar", "");

				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[.='" + fieldDisplayName + "']//ancestor::tr/td/div[@id='menuBar']/layer/a/img"));
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//div[@id='Actions_dynamicmenu" + alterText + "Menu']/span[contains(text () , 'Modify')]"));

				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fieldDisplayName = fc.utobj().generateTestData(dataSet.get("fieldDisplayName"));
				fc.utobj().sendKeys(driver, pobj.fieldDisplayName, fieldDisplayName);
				fc.utobj().clickElement(driver, pobj.saveBtn);
				fc.utobj().clickElement(driver, pobj.closeBtn);
				fc.utobj().switchFrameToDefault(driver);

				boolean isTextPresent = fc.utobj().assertPageSource(driver, fieldDisplayName);

				if (isTextPresent == false) {
					fc.utobj().throwsException("was not able to modify cutomize profile");
				}
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	// @Test(groups="adminopener")
	public void deleteCustomeProfile() throws Exception {

		Reporter.log("TC_73_Delete_Cutome_Profile :Delete Customize Profiles:: Admin > Opener > Customize Profiles.\n");
		Reporter.log("###########################################################################################");

		String testCaseId = "TC_73_Delete_Cutome_Profile";

		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminOpenerCustomizeProfilesPage pobj = new AdminOpenerCustomizeProfilesPage(driver);
			driver = fc.loginpage().login(driver);
			boolean status = false;
			String fieldDisplayName = "";

			fc.opener().opener_common().adminFranchiseOpenerCustomizeProfiles(driver);

			List<WebElement> list = driver
					.findElements(By.xpath(".//tr[@class='bText12 grAltRw3' or @class='bText12']/td[2]"));
			System.out.println(list.size());

			for (int i = 1; i < list.size(); i++) {
				if (list.get(i).getText().trim().equalsIgnoreCase("")) {

					String text = driver
							.findElement(By
									.xpath(".//table[@class='summaryTblex']//tr/td[.='']//following-sibling::td/div/layer"))
							.getAttribute("id").trim();
					String alterText = text.replace("Actions_dynamicmenu", "");
					alterText = alterText.replace("Bar", "");

					fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
							".//table[@class='summaryTblex']//tr/td[.='']//following-sibling::td/div/layer/a/img"));
					fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
							".//div[@id='Actions_dynamicmenu" + alterText + "Menu']/span[contains(text () , 'Add')]"));

					fc.commonMethods().switch_cboxIframe_frameId(driver);
					fieldDisplayName = fc.utobj().generateTestData(dataSet.get("fieldDisplayName"));
					fc.utobj().sendKeys(driver, pobj.fieldDisplayName, fieldDisplayName);
					fc.utobj().clickElement(driver, pobj.addBtn);
					fc.utobj().clickElement(driver, pobj.closeBtn);
					fc.utobj().switchFrameToDefault(driver);
					status = true;
					break;
				}
			}

			if (status == true) {

				String text = driver
						.findElement(By
								.xpath(".//*[.='" + fieldDisplayName + "']//ancestor::tr/td/div[@id='menuBar']/layer"))
						.getAttribute("id").trim();
				String alterText = text.replace("Actions_dynamicmenu", "");
				alterText = alterText.replace("Bar", "");

				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[.='" + fieldDisplayName + "']//ancestor::tr/td/div[@id='menuBar']/layer/a/img"));
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//div[@id='Actions_dynamicmenu" + alterText + "Menu']/span[contains(text () , 'Delete')]"));
				fc.utobj().acceptAlertBox(driver);

				boolean isTextPresent = fc.utobj().assertNotInPageSource(driver, fieldDisplayName);

				if (isTextPresent == true) {
					fc.utobj().throwsException("was not able to delete cutomize profile");
				}
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
}
