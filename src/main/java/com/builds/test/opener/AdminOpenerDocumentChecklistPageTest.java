package com.builds.test.opener;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminConfigurationConfigureStoreTypePageTest;
import com.builds.test.admin.AdminHiddenLinksConfigureNewHierarchyLevelPageTest;
import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.common.CorporateUser;
import com.builds.uimaps.opener.AdminOpenerDocumentChecklistPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminOpenerDocumentChecklistPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "opener", "openeremail" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-29", testCaseDescription = "Add Document Check List  with upload file option:: Admin > Opener > Document Checklist", testCaseId = "TC_25_Add_Document_Checklist_01")
	public void addDocumentChecklistUploadFile() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminOpenerDocumentChecklistPage pobj = new AdminOpenerDocumentChecklistPage(driver);
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Responsible Department");
			fc.utobj().printTestStep("Add Responsible Department");

			AdminOpenerResponsibleDepartmentPageTest addDepartmentPage = new AdminOpenerResponsibleDepartmentPageTest();
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));
			addDepartmentPage.addDepartment(driver, responsibilityArea);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "openerautomation@staffex.com";

			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporatePage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Groups");
			fc.utobj().printTestStep("Add Group");

			fc.utobj().printTestStep("Navigate To Admin > Opener > Groups");
			fc.utobj().printTestStep("Add Group");

			AdminOpenerManageGroupsPageTest addGroupsPage = new AdminOpenerManageGroupsPageTest();
			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			addGroupsPage.addGroups(driver, groupName);

			// configureDefaultValue(driver, config);

			fc.utobj().printTestStep("Add Document Checklist");
			fc.opener().opener_common().adminFranchiseOpenerDocumentChecklist(driver);

			fc.utobj().clickElement(driver, pobj.addMoreBtn);

			String documentText = fc.utobj().generateTestData(dataSet.get("documentText"));
			fc.utobj().sendKeys(driver, pobj.documentNameTxtBox, documentText);

			/*
			 * fc.utobj().clickElement(driver,
			 * pobj.responsibilityAreaDropDownBtn); fc.utobj().sendKeys(driver,
			 * pobj.responsibilityAreaDropDownTxBx, responsibilityArea);
			 * fc.utobj().clickElement(driver,
			 * pobj.responsibilityAreaDropDownSelectAll);
			 * fc.utobj().clickElement(driver,
			 * pobj.responsibilityAreaDropDownBtn);
			 * 
			 * fc.utobj().clickElement(driver, pobj.ContactDropDown);
			 * fc.utobj().sendKeys(driver, pobj.ContactDropDownTxBx,
			 * userInfo.get("userFullName")); fc.utobj().clickElement(driver,
			 * pobj.ContactDropDownSelectAll);
			 * 
			 * fc.utobj().clickElement(driver, pobj.StoreTypeDropDown);
			 * fc.utobj().sendKeys(driver, pobj.StoreTypeDropDownTxBx,
			 * storeType); fc.utobj().clickElement(driver,
			 * pobj.StoreTypeDropDownSelectAll); fc.utobj().clickElement(driver,
			 * pobj.StoreTypeDropDown);
			 */

			fc.utobj().selectValFromMultiSelect(driver, pobj.responsibilitySelect, responsibilityArea);

			fc.utobj().selectValFromMultiSelect(driver, pobj.contactSelect, corpUser.getuserFullName());

			fc.utobj().selectValFromMultiSelect(driver, pobj.storeTypeSelect, storeType);

			fc.utobj().selectDropDown(driver, pobj.groupDropDown, groupName);
			fc.utobj().selectDropDown(driver, pobj.franchiseeAccessDropDown, dataSet.get("franchiseeAccess"));
			fc.utobj().selectDropDown(driver, pobj.priorityDropDown, dataSet.get("priority"));
			fc.utobj().selectDropDown(driver, pobj.criticalLevelDropDown, dataSet.get("criticalLevel"));

			String dependentOn = dataSet.get("dependentOn");
			String valueText = fc.utobj()
					.getElementByXpath(driver,
							".//select[@name='smDocumentChecklist_0referenceParent']/option[contains(text () , '"
									+ dependentOn + "')]")
					.getAttribute("value");
			fc.utobj().moveToElement(driver, pobj.dependentOnDropDown);
			Select sl = new Select(pobj.dependentOnDropDown);
			sl.selectByValue(valueText);

			fc.utobj().sendKeys(driver, pobj.startScheduleDateTxtBox, "1");
			fc.utobj().selectDropDown(driver, pobj.scheduleStartDropDown, "Days Prior");
			fc.utobj().sendKeys(driver, pobj.completionScheduleDateTxtBox, "1");
			fc.utobj().selectDropDown(driver, pobj.scheduleCompletionDropDown, "Days Prior");

			fc.utobj().sendKeys(driver, pobj.startReminderTxtBox, "2");
			fc.utobj().sendKeys(driver, pobj.completionReminderTxtBox, "2");

			if (fc.utobj().isSelected(driver, pobj.uploadFileRadioBtn)) {
				// do nothing
			} else {
				fc.utobj().clickElement(driver, pobj.uploadFileRadioBtn);
			}

			String file = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));

			System.out.println(file);

			fc.utobj().sendKeys(driver, pobj.attachmentBrowseBox, file);
			fc.utobj().clickElement(driver, pobj.addBtn);

			try {

				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().clickElement(driver, pobj.noBtn);
				fc.utobj().switchFrameToDefault(driver);
			} catch (Exception e) {

			}

			fc.utobj().printTestStep("Verify The Add Document Checklist");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, documentText);

			if (isTextPresent == false) {
				fc.utobj().throwsSkipException("was not able to add Document checklist");
			}

			fc.utobj().printTestStep("Verify The Mail After Document Check List Creation");

			Map<String, String> mailData = new HashMap<String, String>();

			mailData = fc.utobj().readMailBox("Checklist Item Creation Alert", documentText, emailId, "sdg@1a@Hfs");

			if (mailData.size() == 0 || !mailData.get("mailBody").contains("New Item Created")) {
				fc.utobj().throwsException("was not able to verify Document checklist creation mail");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addDocumentChecklist(WebDriver driver, Map<String, String> dataSet) throws Exception {

		String testCaseId = "TC_Add_Document_Checklist_Opener_Admin";
		String documentText = null;
		if (fc.utobj().validate(testCaseId)) {
			try {

				AdminOpenerDocumentChecklistPage pobj = new AdminOpenerDocumentChecklistPage(driver);

				AdminOpenerResponsibleDepartmentPageTest addDepartmentPage = new AdminOpenerResponsibleDepartmentPageTest();
				String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));
				addDepartmentPage.addDepartment(driver, responsibilityArea);

				AdminUsersManageCorporateUsersAddCorporateUserPageTest corporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
				String emailId = "openerautomation@staffex.com";

				CorporateUser corpUser = new CorporateUser();
				corpUser.setEmail(emailId);
				corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
				corporatePage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

				AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
				String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
				storeTypePage.addStoreType(driver, storeType);

				// configureDefaultValue(driver, config);

				fc.opener().opener_common().adminFranchiseOpenerDocumentChecklist(driver);

				fc.utobj().clickElement(driver, pobj.addMoreBtn);

				documentText = fc.utobj().generateTestData(dataSet.get("documentText"));
				fc.utobj().sendKeys(driver, pobj.documentNameTxtBox, documentText);

				/*
				 * fc.utobj().clickElement(driver,
				 * pobj.responsibilityAreaDropDownBtn);
				 * fc.utobj().sendKeys(driver,
				 * pobj.responsibilityAreaDropDownTxBx, responsibilityArea);
				 * fc.utobj().clickElement(driver,
				 * pobj.responsibilityAreaDropDownSelectAll);
				 * fc.utobj().clickElement(driver,
				 * pobj.responsibilityAreaDropDownBtn);
				 * 
				 * fc.utobj().clickElement(driver, pobj.ContactDropDown);
				 * fc.utobj().sendKeys(driver, pobj.ContactDropDownTxBx,
				 * corpUser.getuserFullName()); fc.utobj().clickElement(driver,
				 * pobj.ContactDropDownSelectAll);
				 * 
				 * fc.utobj().clickElement(driver, pobj.StoreTypeDropDown);
				 * fc.utobj().sendKeys(driver, pobj.StoreTypeDropDownTxBx,
				 * storeType); fc.utobj().clickElement(driver,
				 * pobj.StoreTypeDropDownSelectAll);
				 * fc.utobj().clickElement(driver, pobj.StoreTypeDropDown);
				 */

				fc.utobj().selectValFromMultiSelect(driver, pobj.responsibilitySelect, responsibilityArea);

				fc.utobj().selectValFromMultiSelect(driver, pobj.contactSelect, corpUser.getuserFullName());

				fc.utobj().selectValFromMultiSelect(driver, pobj.storeTypeSelect, storeType);

				/*
				 * fc.utobj().selectDropDown(driver, pobj.groupDropDown,
				 * groupName);
				 */
				fc.utobj().selectDropDown(driver, pobj.franchiseeAccessDropDown, dataSet.get("franchiseeAccess"));
				fc.utobj().selectDropDown(driver, pobj.priorityDropDown, dataSet.get("priority"));
				fc.utobj().selectDropDown(driver, pobj.criticalLevelDropDown, dataSet.get("criticalLevel"));

				String dependentOn = dataSet.get("dependentOn");
				String valueText = fc.utobj()
						.getElementByXpath(driver,
								".//select[@name='smDocumentChecklist_0referenceParent']/option[contains(text () , '"
										+ dependentOn + "')]")
						.getAttribute("value");
				fc.utobj().moveToElement(driver, pobj.dependentOnDropDown);
				Select sl = new Select(pobj.dependentOnDropDown);
				sl.selectByValue(valueText);

				if (dependentOn.equalsIgnoreCase("None (Timeless)")) {
					// do nothing
				} else if (dependentOn.equalsIgnoreCase("Expected Opening Date")) {

					fc.utobj().sendKeys(driver, pobj.startScheduleDateTxtBox, "1");
					fc.utobj().selectDropDown(driver, pobj.scheduleStartDropDown, "Days Prior");
					fc.utobj().sendKeys(driver, pobj.completionScheduleDateTxtBox, "1");
					fc.utobj().selectDropDown(driver, pobj.scheduleCompletionDropDown, "Days Prior");

					fc.utobj().sendKeys(driver, pobj.startReminderTxtBox, "2");
					fc.utobj().sendKeys(driver, pobj.completionReminderTxtBox, "2");

				} else if (dependentOn.equalsIgnoreCase("Multiple Checklists")) {

					fc.utobj().clickElement(driver, pobj.dependentOnDropDownBtn);
					// fc.utobj().sendKeys(driver, pobj.dependentOnDropDownTxBx,
					// txt);
					fc.utobj().clickElement(driver, pobj.dependentOnDropDownselectAll);

					fc.utobj().sendKeys(driver, pobj.startScheduleDateTxtBox, "1");
					fc.utobj().selectDropDown(driver, pobj.scheduleStartDropDown, "Days Prior");
					fc.utobj().sendKeys(driver, pobj.completionScheduleDateTxtBox, "1");
					fc.utobj().selectDropDown(driver, pobj.scheduleCompletionDropDown, "Days Prior");
					fc.utobj().sendKeys(driver, pobj.startReminderTxtBox, "2");
					fc.utobj().sendKeys(driver, pobj.completionReminderTxtBox, "2");

				}

				if (fc.utobj().isSelected(driver, pobj.uploadFileRadioBtn)) {
					// do nothing
				} else {
					fc.utobj().clickElement(driver, pobj.uploadFileRadioBtn);
				}

				String file = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));

				System.out.println(file);
				fc.utobj().sendKeys(driver, pobj.attachmentBrowseBox, file);
				fc.utobj().clickElement(driver, pobj.addBtn);

				try {
					fc.commonMethods().switch_cboxIframe_frameId(driver);
					fc.utobj().clickElement(driver, pobj.noBtn);
					fc.utobj().switchFrameToDefault(driver);
				} catch (Exception e) {

				}

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to add Document CHeckList");

		}

		return documentText;
	}

	public String addDocumentChecklist(WebDriver driver, Map<String, String> dataSet, String responsibilityArea,
			String userName, String storeType, File file, Map<String, String> config) throws Exception {

		String testCaseId = "TC_Add_Document_Checklist_Opener_Admin_1";
		String documentText = null;

		if (fc.utobj().validate(testCaseId)) {
			try {

				AdminOpenerDocumentChecklistPage pobj = new AdminOpenerDocumentChecklistPage(driver);

				AdminOpenerResponsibleDepartmentPageTest addDepartmentPage = new AdminOpenerResponsibleDepartmentPageTest();
				addDepartmentPage.addDepartment(driver, responsibilityArea);

				// configureDefaultValue(driver, config);

				fc.opener().opener_common().adminFranchiseOpenerDocumentChecklist(driver);

				fc.utobj().clickElement(driver, pobj.addMoreBtn);

				documentText = fc.utobj().generateTestData(dataSet.get("documentText"));
				fc.utobj().sendKeys(driver, pobj.documentNameTxtBox, documentText);

				/*
				 * fc.utobj().clickElement(driver,
				 * pobj.responsibilityAreaDropDownBtn);
				 * fc.utobj().sendKeys(driver,
				 * pobj.responsibilityAreaDropDownTxBx, responsibilityArea);
				 * fc.utobj().clickElement(driver,
				 * pobj.responsibilityAreaDropDownSelectAll);
				 * fc.utobj().clickElement(driver,
				 * pobj.responsibilityAreaDropDownBtn);
				 * 
				 * fc.utobj().clickElement(driver, pobj.ContactDropDown);
				 * fc.utobj().sendKeys(driver, pobj.ContactDropDownTxBx,
				 * userName); fc.utobj().clickElement(driver,
				 * pobj.ContactDropDownSelectAll);
				 * 
				 * fc.utobj().clickElement(driver, pobj.StoreTypeDropDown);
				 * fc.utobj().sendKeys(driver, pobj.StoreTypeDropDownTxBx,
				 * storeType); fc.utobj().clickElement(driver,
				 * pobj.StoreTypeDropDownSelectAll);
				 * fc.utobj().clickElement(driver, pobj.StoreTypeDropDown);
				 */

				fc.utobj().selectValFromMultiSelect(driver, pobj.responsibilitySelect, responsibilityArea);

				fc.utobj().selectValFromMultiSelect(driver, pobj.contactSelect, userName);

				fc.utobj().selectValFromMultiSelect(driver, pobj.storeTypeSelect, storeType);

				/*
				 * fc.utobj().selectDropDown(driver, pobj.groupDropDown,
				 * groupName);
				 */
				fc.utobj().selectDropDown(driver, pobj.franchiseeAccessDropDown, dataSet.get("franchiseeAccess"));
				fc.utobj().selectDropDown(driver, pobj.priorityDropDown, dataSet.get("priority"));
				fc.utobj().selectDropDown(driver, pobj.criticalLevelDropDown, dataSet.get("criticalLevel"));

				String dependentOn = dataSet.get("dependentOn");
				String valueText = fc.utobj()
						.getElementByXpath(driver,
								".//select[@name='smDocumentChecklist_0referenceParent']/option[contains(text () , '"
										+ dependentOn + "')]")
						.getAttribute("value");
				fc.utobj().moveToElement(driver, pobj.dependentOnDropDown);
				Select sl = new Select(pobj.dependentOnDropDown);
				sl.selectByValue(valueText);

				if (dependentOn.equalsIgnoreCase("None (Timeless)")) {
					// do nothing
				} else if (dependentOn.equalsIgnoreCase("Expected Opening Date")) {

					fc.utobj().sendKeys(driver, pobj.startScheduleDateTxtBox, "1");
					fc.utobj().selectDropDown(driver, pobj.scheduleStartDropDown, "Days Prior");
					fc.utobj().sendKeys(driver, pobj.completionScheduleDateTxtBox, "1");
					fc.utobj().selectDropDown(driver, pobj.scheduleCompletionDropDown, "Days Prior");

					fc.utobj().sendKeys(driver, pobj.startReminderTxtBox, "2");
					fc.utobj().sendKeys(driver, pobj.completionReminderTxtBox, "2");

				} else if (dependentOn.equalsIgnoreCase("Multiple Checklists")) {

					fc.utobj().clickElement(driver, pobj.dependentOnDropDownBtn);
					// fc.utobj().sendKeys(driver, pobj.dependentOnDropDownTxBx,
					// txt);
					fc.utobj().clickElement(driver, pobj.dependentOnDropDownselectAll);

					fc.utobj().sendKeys(driver, pobj.startScheduleDateTxtBox, "1");
					fc.utobj().selectDropDown(driver, pobj.scheduleStartDropDown, "Days Prior");
					fc.utobj().sendKeys(driver, pobj.completionScheduleDateTxtBox, "1");
					fc.utobj().selectDropDown(driver, pobj.scheduleCompletionDropDown, "Days Prior");
					fc.utobj().sendKeys(driver, pobj.startReminderTxtBox, "2");
					fc.utobj().sendKeys(driver, pobj.completionReminderTxtBox, "2");

				}

				if (fc.utobj().isSelected(driver, pobj.uploadFileRadioBtn)) {
					// do nothing
				} else {
					fc.utobj().clickElement(driver, pobj.uploadFileRadioBtn);
				}

				fc.utobj().sendKeys(driver, pobj.attachmentBrowseBox, file.getAbsolutePath());
				fc.utobj().clickElement(driver, pobj.addBtn);

				try {
					fc.commonMethods().switch_cboxIframe_frameId(driver);
					fc.utobj().clickElement(driver, pobj.yesBtn);
					fc.utobj().switchFrameToDefault(driver);
				} catch (Exception e) {

				}

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to add Document CheckList");

		}
		return documentText;
	}

	// with Group

	public String addDocumentChecklistWithGroup(WebDriver driver, Map<String, String> dataSet,
			String responsibilityArea, String userName, String groupName, String storeType, File file,
			Map<String, String> config) throws Exception {

		String testCaseId = "TC_Add_Document_Checklist_Opener_Admin_1";
		String documentText = null;

		if (fc.utobj().validate(testCaseId)) {
			try {

				AdminOpenerDocumentChecklistPage pobj = new AdminOpenerDocumentChecklistPage(driver);

				AdminOpenerResponsibleDepartmentPageTest addDepartmentPage = new AdminOpenerResponsibleDepartmentPageTest();
				addDepartmentPage.addDepartment(driver, responsibilityArea);

				AdminOpenerManageGroupsPageTest addGroupsPage = new AdminOpenerManageGroupsPageTest();
				addGroupsPage.addGroups(driver, groupName);

				// configureDefaultValue(driver, config);

				fc.opener().opener_common().adminFranchiseOpenerDocumentChecklist(driver);

				fc.utobj().clickElement(driver, pobj.addMoreBtn);

				documentText = fc.utobj().generateTestData(dataSet.get("documentText"));
				fc.utobj().sendKeys(driver, pobj.documentNameTxtBox, documentText);

				/*
				 * fc.utobj().clickElement(driver,
				 * pobj.responsibilityAreaDropDownBtn);
				 * fc.utobj().sendKeys(driver,
				 * pobj.responsibilityAreaDropDownTxBx, responsibilityArea);
				 * fc.utobj().clickElement(driver,
				 * pobj.responsibilityAreaDropDownSelectAll);
				 * fc.utobj().clickElement(driver,
				 * pobj.responsibilityAreaDropDownBtn);
				 * 
				 * fc.utobj().clickElement(driver, pobj.ContactDropDown);
				 * fc.utobj().sendKeys(driver, pobj.ContactDropDownTxBx,
				 * userName); fc.utobj().clickElement(driver,
				 * pobj.ContactDropDownSelectAll);
				 * 
				 * fc.utobj().clickElement(driver, pobj.StoreTypeDropDown);
				 * fc.utobj().sendKeys(driver, pobj.StoreTypeDropDownTxBx,
				 * storeType); fc.utobj().clickElement(driver,
				 * pobj.StoreTypeDropDownSelectAll);
				 * fc.utobj().clickElement(driver, pobj.StoreTypeDropDown);
				 */

				fc.utobj().selectValFromMultiSelect(driver, pobj.responsibilitySelect, responsibilityArea);
				fc.utobj().selectValFromMultiSelect(driver, pobj.contactSelect, userName);
				fc.utobj().selectValFromMultiSelect(driver, pobj.storeTypeSelect, storeType);

				fc.utobj().selectDropDown(driver, pobj.groupDropDown, groupName);
				fc.utobj().selectDropDown(driver, pobj.franchiseeAccessDropDown, dataSet.get("franchiseeAccess"));
				fc.utobj().selectDropDown(driver, pobj.priorityDropDown, dataSet.get("priority"));
				fc.utobj().selectDropDown(driver, pobj.criticalLevelDropDown, dataSet.get("criticalLevel"));

				String dependentOn = dataSet.get("dependentOn");
				String valueText = fc.utobj()
						.getElementByXpath(driver,
								".//select[@name='smDocumentChecklist_0referenceParent']/option[contains(text () , '"
										+ dependentOn + "')]")
						.getAttribute("value");
				fc.utobj().moveToElement(driver, pobj.dependentOnDropDown);
				Select sl = new Select(pobj.dependentOnDropDown);
				sl.selectByValue(valueText);

				if (dependentOn.equalsIgnoreCase("None (Timeless)")) {
					// do nothing
				} else if (dependentOn.equalsIgnoreCase("Expected Opening Date")) {

					fc.utobj().sendKeys(driver, pobj.startScheduleDateTxtBox, "1");
					fc.utobj().selectDropDown(driver, pobj.scheduleStartDropDown, "Days Prior");
					fc.utobj().sendKeys(driver, pobj.completionScheduleDateTxtBox, "1");
					fc.utobj().selectDropDown(driver, pobj.scheduleCompletionDropDown, "Days Prior");

					fc.utobj().sendKeys(driver, pobj.startReminderTxtBox, "2");
					fc.utobj().sendKeys(driver, pobj.completionReminderTxtBox, "2");

				} else if (dependentOn.equalsIgnoreCase("Multiple Checklists")) {

					fc.utobj().clickElement(driver, pobj.dependentOnDropDownBtn);
					// fc.utobj().sendKeys(driver, pobj.dependentOnDropDownTxBx,
					// txt);
					fc.utobj().clickElement(driver, pobj.dependentOnDropDownselectAll);

					fc.utobj().sendKeys(driver, pobj.startScheduleDateTxtBox, "1");
					fc.utobj().selectDropDown(driver, pobj.scheduleStartDropDown, "Days Prior");
					fc.utobj().sendKeys(driver, pobj.completionScheduleDateTxtBox, "1");
					fc.utobj().selectDropDown(driver, pobj.scheduleCompletionDropDown, "Days Prior");
					fc.utobj().sendKeys(driver, pobj.startReminderTxtBox, "2");
					fc.utobj().sendKeys(driver, pobj.completionReminderTxtBox, "2");

				}

				if (fc.utobj().isSelected(driver, pobj.uploadFileRadioBtn)) {
					// do nothing
				} else {
					fc.utobj().clickElement(driver, pobj.uploadFileRadioBtn);
				}

				fc.utobj().sendKeys(driver, pobj.attachmentBrowseBox, file.getAbsolutePath());
				fc.utobj().clickElement(driver, pobj.addBtn);

				try {
					fc.commonMethods().switch_cboxIframe_frameId(driver);
					fc.utobj().clickElement(driver, pobj.yesBtn);
					fc.utobj().switchFrameToDefault(driver);
				} catch (Exception e) {

				}

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to add Document CheckList");

		}
		return documentText;
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Add Document Check List with webLink option:: Admin > Opener > Document Checklist.", testCaseId = "TC_26_Add_Document_Checklist_02")
	public void addDocumentChecklistWebLink() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminOpenerDocumentChecklistPage pobj = new AdminOpenerDocumentChecklistPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Responsible Department");
			fc.utobj().printTestStep("Add Responsible Department");

			AdminOpenerResponsibleDepartmentPageTest addDepartmentPage = new AdminOpenerResponsibleDepartmentPageTest();
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));
			addDepartmentPage.addDepartment(driver, responsibilityArea);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			/*
			 * String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String password=dataSet.get("password");
			 * corporatePage.addCorporateUser(driver, userName, password,
			 * config);
			 */
			String emailId = "openerautomation@staffex.com";

			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporatePage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Groups");
			fc.utobj().printTestStep("Add Group");

			AdminOpenerManageGroupsPageTest addGroupsPage = new AdminOpenerManageGroupsPageTest();
			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			addGroupsPage.addGroups(driver, groupName);

			configureDefaultValue(driver);

			fc.utobj().printTestStep("Add Document Checklist With WebLink Option");
			fc.opener().opener_common().adminFranchiseOpenerDocumentChecklist(driver);

			fc.utobj().clickElement(driver, pobj.addMoreBtn);

			String documentText = fc.utobj().generateTestData(dataSet.get("documentText"));
			fc.utobj().sendKeys(driver, pobj.documentNameTxtBox, documentText);

			/*
			 * fc.utobj().clickElement(driver,
			 * pobj.responsibilityAreaDropDownBtn); fc.utobj().sendKeys(driver,
			 * pobj.responsibilityAreaDropDownTxBx, responsibilityArea);
			 * fc.utobj().clickElement(driver,
			 * pobj.responsibilityAreaDropDownSelectAll);
			 * fc.utobj().clickElement(driver,
			 * pobj.responsibilityAreaDropDownBtn);
			 * 
			 * fc.utobj().clickElement(driver, pobj.ContactDropDown);
			 * fc.utobj().sendKeys(driver, pobj.ContactDropDownTxBx,
			 * userInfo.get("userFullName")); fc.utobj().clickElement(driver,
			 * pobj.ContactDropDownSelectAll);
			 * 
			 * fc.utobj().clickElement(driver, pobj.StoreTypeDropDown);
			 * fc.utobj().sendKeys(driver, pobj.StoreTypeDropDownTxBx,
			 * storeType); fc.utobj().clickElement(driver,
			 * pobj.StoreTypeDropDownSelectAll); fc.utobj().clickElement(driver,
			 * pobj.StoreTypeDropDown);
			 */

			fc.utobj().selectValFromMultiSelect(driver, pobj.responsibilitySelect, responsibilityArea);
			fc.utobj().selectValFromMultiSelect(driver, pobj.contactSelect, corpUser.getuserFullName());
			fc.utobj().selectValFromMultiSelect(driver, pobj.storeTypeSelect, storeType);

			fc.utobj().selectDropDown(driver, pobj.groupDropDown, groupName);
			fc.utobj().selectDropDown(driver, pobj.franchiseeAccessDropDown, dataSet.get("franchiseeAccess"));
			fc.utobj().selectDropDown(driver, pobj.priorityDropDown, dataSet.get("priority"));
			fc.utobj().selectDropDown(driver, pobj.criticalLevelDropDown, dataSet.get("criticalLevel"));

			String dependentOn = dataSet.get("dependentOn");
			String valueText = fc.utobj()
					.getElementByXpath(driver,
							".//select[@name='smDocumentChecklist_0referenceParent']/option[contains(text () , '"
									+ dependentOn + "')]")
					.getAttribute("value");
			fc.utobj().moveToElement(driver, pobj.dependentOnDropDown);
			Select sl = new Select(pobj.dependentOnDropDown);
			sl.selectByValue(valueText);

			if (fc.utobj().isSelected(driver, pobj.webLinkRadioBtn)) {
				// do nothing
			} else {
				fc.utobj().clickElement(driver, pobj.webLinkRadioBtn);
			}

			fc.utobj().sendKeys(driver, pobj.webLinkTxBx, dataSet.get("webLinkUrl"));
			fc.utobj().clickElement(driver, pobj.addBtn);

			try {

				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().clickElement(driver, pobj.noBtn);
				fc.utobj().switchFrameToDefault(driver);
			} catch (Exception e) {

			}

			fc.utobj().printTestStep("Verify The Add Document Checklist");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, documentText);

			if (isTextPresent == false) {
				fc.utobj().throwsSkipException("was not able to add Document checklist");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Modify Document Check List  with upload file option at ActionImg:: Admin > Opener > Document Checklist", testCaseId = "TC_27_Modify_Document_Checklist")
	public void modifyDocumentChecklistActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminOpenerDocumentChecklistPage pobj = new AdminOpenerDocumentChecklistPage(driver);
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Add Document Checklist");
			String documentText = addDocumentChecklist(driver, dataSet);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Responsible Department");
			fc.utobj().printTestStep("Add Responsible Department");
			AdminOpenerResponsibleDepartmentPageTest addDepartmentPage = new AdminOpenerResponsibleDepartmentPageTest();
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));
			addDepartmentPage.addDepartment(driver, responsibilityArea);

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigate To Admin > Users > Manage Corporate Users");
			 * fc.utobj().printTestStep(testCaseId, "Add Corporate User");
			 * 
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * corporatePage=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest(); String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String password=dataSet.get("password");
			 * corporatePage.addCorporateUser(driver, userName, password,
			 * config); Map<String,String> userInfo =
			 * corporatePage.useExistingCorporateUser(driver, config);
			 */

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Groups");
			fc.utobj().printTestStep("Add Group");

			AdminOpenerManageGroupsPageTest addGroupsPage = new AdminOpenerManageGroupsPageTest();
			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			addGroupsPage.addGroups(driver, groupName);

			fc.opener().opener_common().adminFranchiseOpenerDocumentChecklist(driver);

			fc.utobj().printTestStep("Modify The Document Checklist");
			// modify Action Img
			String text = driver
					.findElement(By.xpath(".//*[.='" + documentText + "']/ancestor::tr/td/div[@id='menuBar']/layer"))
					.getAttribute("id").trim();
			String alterText = text.replace("Actions_dynamicmenu", "");
			alterText = alterText.replace("Bar", "");

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[.='" + documentText + "']/ancestor::tr/td/div[@id='menuBar']/layer/a/img"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//div[@id='Actions_dynamicmenu" + alterText + "Menu']/span[contains(text () , 'Modify')]"));

			documentText = fc.utobj().generateTestData(dataSet.get("documentText"));
			fc.utobj().sendKeys(driver, pobj.documentNameTxtBox, documentText);
			fc.utobj().selectValFromMultiSelect(driver, pobj.responsibilitySelect, responsibilityArea);
			fc.utobj().selectValFromMultiSelect(driver, pobj.storeTypeSelect, storeType);

			fc.utobj().selectDropDown(driver, pobj.groupDropDown, groupName);
			fc.utobj().selectDropDown(driver, pobj.franchiseeAccessDropDown, dataSet.get("franchiseeAccess"));
			fc.utobj().selectDropDown(driver, pobj.priorityDropDown, dataSet.get("priority"));
			fc.utobj().selectDropDown(driver, pobj.criticalLevelDropDown, dataSet.get("criticalLevel"));

			String dependentOn = dataSet.get("dependentOnModify");
			String valueText = fc.utobj()
					.getElementByXpath(driver,
							".//select[@name='smDocumentChecklist_0referenceParent']/option[contains(text () , '"
									+ dependentOn + "')]")
					.getAttribute("value");
			fc.utobj().moveToElement(driver, pobj.dependentOnDropDown);
			Select sl = new Select(pobj.dependentOnDropDown);
			sl.selectByValue(valueText);

			if (fc.utobj().isSelected(driver, pobj.webLinkRadioBtn)) {
				// do nothing
			} else {
				fc.utobj().clickElement(driver, pobj.webLinkRadioBtn);
			}

			fc.utobj().sendKeys(driver, pobj.webLinkTxBx, dataSet.get("webLinkUrl"));

			try {
				List<WebElement> listRadioButton = pobj.applyToAll;
				if (fc.utobj().isSelected(driver,listRadioButton.get(0))) {
					// do nothing
				} else {
					fc.utobj().clickElement(driver, listRadioButton.get(0));
				}
			} catch (Exception e) {

			}

			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("Verify The Modify Checklist");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, documentText);

			if (isTextPresent == false) {
				fc.utobj().throwsSkipException("was not able to modify Document checklist");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "opener", "openeremail" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-29", testCaseDescription = "Delete Document Check List  with upload file option at ActionImg:: Admin > Opener > Document Checklist", testCaseId = "TC_28_Delete_Document_Checklist")
	public void deleteDocumentChecklistActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminOpenerDocumentChecklistPage pobj = new AdminOpenerDocumentChecklistPage(driver);
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Add Document Checklist");
			String documentText = addDocumentChecklist(driver, dataSet);

			fc.utobj().printTestStep("Delete The Document Checklist By Action Image Option");
			String text = driver
					.findElement(By.xpath(".//*[.='" + documentText + "']/ancestor::tr/td/div[@id='menuBar']/layer"))
					.getAttribute("id").trim();
			String alterText = text.replace("Actions_dynamicmenu", "");
			alterText = alterText.replace("Bar", "");

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[.='" + documentText + "']/ancestor::tr/td/div[@id='menuBar']/layer/a/img"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//div[@id='Actions_dynamicmenu" + alterText + "Menu']/span[contains(text () , 'Delete')]"));

			fc.utobj().acceptAlertBox(driver);

			try {

				fc.commonMethods().switch_cboxIframe_frameId(driver);
				if (fc.utobj().isSelected(driver,pobj.applyToAll.get(0))) {
					// do nothing
				} else {
					fc.utobj().clickElement(driver, pobj.applyToAll.get(0));
				}

				fc.utobj().clickElement(driver, pobj.cboxDeleteBtn);
				fc.utobj().clickElement(driver, pobj.cboxCloseBtn);
				fc.utobj().switchFrameToDefault(driver);

			} catch (Exception e) {

			}

			fc.utobj().printTestStep("Verify The Delete Checklist");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, documentText);

			if (isTextPresent == true) {
				fc.utobj().throwsSkipException("Was not able to delete Document Checklist");
			}

			fc.utobj().printTestStep("Verify Email After Document Checklist Deletion");

			Map<String, String> mailData = new HashMap<String, String>();
			mailData = fc.utobj().readMailBox("Checklist Item Deletion Alert", documentText,
					"openerautomation@staffex.com", "sdg@1a@Hfs");

			if (mailData.size() == 0 || !mailData.get("mailBody").contains("Item Deleted")) {

				fc.utobj().throwsException("was not able to verify Document Checklist Deletion Mail");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Add description at Document Check List with upload file option at ActionImg:: Admin > Opener > Document Checklist", testCaseId = "TC_29_Add_Description_Document")
	public void addDescriptionDocumentActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminOpenerDocumentChecklistPage pobj = new AdminOpenerDocumentChecklistPage(driver);
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Add Document Checklist");
			String documentText = addDocumentChecklist(driver, dataSet);

			fc.utobj().printTestStep("Add Description");

			fc.utobj().actionImgOption(driver, documentText, "Add Description");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().sendKeys(driver, pobj.documentDescriptionArea, description);
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Add Description");

			fc.utobj().actionImgOption(driver, documentText, "Modify Description");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String isTextPresent = fc.utobj().getText(driver, pobj.documentDescriptionArea);

			if (isTextPresent.equalsIgnoreCase(description)) {
				// do nothing
			} else {
				fc.utobj().throwsSkipException("was not able to add description");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Modify description at Document Check List with upload file option at ActionImg:: Admin > Opener > Document Checklist", testCaseId = "TC_30_Modify_Description_Document")
	public void modifyDescriptionActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminOpenerDocumentChecklistPage pobj = new AdminOpenerDocumentChecklistPage(driver);
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Add Document Checklist");
			String documentText = addDocumentChecklist(driver, dataSet);

			fc.utobj().printTestStep("Add Description");
			fc.utobj().actionImgOption(driver, documentText, "Add Description");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().sendKeys(driver, pobj.documentDescriptionArea, description);
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Modify The Description");
			fc.utobj().actionImgOption(driver, documentText, "Modify Description");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().sendKeys(driver, pobj.documentDescriptionArea, description);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Modify Description");
			fc.utobj().actionImgOption(driver, documentText, "Modify Description");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String isTextPresent = fc.utobj().getText(driver, pobj.documentDescriptionArea);

			if (isTextPresent.equalsIgnoreCase(description)) {
				// do nothing
			} else {
				fc.utobj().throwsSkipException("was not able to modify description");
			}
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Modify Document Check List  with upload file option at Action Button:: Admin > Opener > Document Checklist.", testCaseId = "TC_31_Modfiy_Document_Checklist01")
	public void modifyDocumentChecklistActionButton() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminOpenerDocumentChecklistPage pobj = new AdminOpenerDocumentChecklistPage(driver);
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Add Document Checklist");
			String documentText = addDocumentChecklist(driver, dataSet);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Responsible Department");
			fc.utobj().printTestStep("Add Responsible Department");

			AdminOpenerResponsibleDepartmentPageTest addDepartmentPage = new AdminOpenerResponsibleDepartmentPageTest();
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));
			addDepartmentPage.addDepartment(driver, responsibilityArea);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Groups");
			fc.utobj().printTestStep("Add Group");

			AdminOpenerManageGroupsPageTest addGroupsPage = new AdminOpenerManageGroupsPageTest();
			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			addGroupsPage.addGroups(driver, groupName);

			fc.opener().opener_common().adminFranchiseOpenerDocumentChecklist(driver);

			fc.utobj().printTestStep("Modify The Document Checklist");

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[.='" + documentText
					+ "']//ancestor::tr[@class='bText12' or @class='bText12 grAltRw3']/td/input"));

			fc.utobj().selectActionMenuItems(driver, "Modify");

			documentText = fc.utobj().generateTestData(dataSet.get("documentText"));
			fc.utobj().sendKeys(driver, pobj.documentNameTxtBox, documentText);

			/*
			 * fc.utobj().clickElement(driver,
			 * pobj.responsibilityAreaDropDownBtn); fc.utobj().sendKeys(driver,
			 * pobj.responsibilityAreaDropDownTxBx, responsibilityArea);
			 * fc.utobj().clickElement(driver,
			 * pobj.responsibilityAreaDropDownSelectAll);
			 * fc.utobj().clickElement(driver,
			 * pobj.responsibilityAreaDropDownBtn);
			 * 
			 * fc.utobj().clickElement(driver, pobj.StoreTypeDropDown);
			 * fc.utobj().sendKeys(driver, pobj.StoreTypeDropDownTxBx,
			 * storeType); fc.utobj().clickElement(driver,
			 * pobj.StoreTypeDropDownSelectAll); fc.utobj().clickElement(driver,
			 * pobj.StoreTypeDropDown);
			 */

			fc.utobj().selectValFromMultiSelect(driver, pobj.responsibilitySelect, responsibilityArea);
			fc.utobj().selectValFromMultiSelect(driver, pobj.storeTypeSelect, storeType);

			fc.utobj().selectDropDown(driver, pobj.groupDropDown, groupName);
			fc.utobj().selectDropDown(driver, pobj.franchiseeAccessDropDown, dataSet.get("franchiseeAccess"));
			fc.utobj().selectDropDown(driver, pobj.priorityDropDown, dataSet.get("priority"));
			fc.utobj().selectDropDown(driver, pobj.criticalLevelDropDown, dataSet.get("criticalLevel"));

			String dependentOn = dataSet.get("dependentOnModify");
			String valueText = fc.utobj()
					.getElementByXpath(driver,
							".//select[@name='smDocumentChecklist_0referenceParent']/option[contains(text () , '"
									+ dependentOn + "')]")
					.getAttribute("value");
			fc.utobj().moveToElement(driver, pobj.dependentOnDropDown);
			Select sl = new Select(pobj.dependentOnDropDown);
			sl.selectByValue(valueText);

			if (fc.utobj().isSelected(driver, pobj.webLinkRadioBtn)) {
				// do nothing
			} else {
				fc.utobj().clickElement(driver, pobj.webLinkRadioBtn);
			}

			fc.utobj().sendKeys(driver, pobj.webLinkTxBx, dataSet.get("webLinkUrl"));

			try {
				List<WebElement> listRadioButton = pobj.applyToAll;
				if (fc.utobj().isSelected(driver,listRadioButton.get(0))) {
					// do nothing
				} else {
					fc.utobj().clickElement(driver, listRadioButton.get(0));
				}
			} catch (Exception e) {

			}

			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("Verify The Modify The Document Checklist");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, documentText);

			if (isTextPresent == false) {
				fc.utobj().throwsSkipException("was not able modify document checklist");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Delete Document Check List  with upload file option at Action Button:: Admin > Opener > Document Checklist", testCaseId = "TC_32_Delete_Document_Checklist01")
	public void deleteDocumentChecklistActionButton() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminOpenerDocumentChecklistPage pobj = new AdminOpenerDocumentChecklistPage(driver);
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Add Document Checklist");
			String documentText = addDocumentChecklist(driver, dataSet);

			// modify Action Button

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[.='" + documentText
					+ "']//ancestor::tr[@class='bText12' or @class='bText12 grAltRw3']/td/input"));

			fc.utobj().printTestStep("Delete The Document Checklist");
			fc.utobj().selectActionMenuItems(driver, "Delete");

			fc.utobj().acceptAlertBox(driver);

			try {
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				if (fc.utobj().isSelected(driver,pobj.applyToAll.get(1))) {
					// do nothing
				} else {
					fc.utobj().clickElement(driver, pobj.applyToAll.get(1));
				}

				fc.utobj().clickElement(driver, pobj.cboxDeleteBtn);
				fc.utobj().clickElement(driver, pobj.cboxCloseBtn);
				fc.utobj().switchFrameToDefault(driver);

			} catch (Exception e) {

			}

			fc.utobj().printTestStep("Verify The Delete The Checklist");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, documentText);

			if (isTextPresent == true) {
				fc.utobj().throwsSkipException("Was not able to delete Document Checklist");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Modify Contact(s) Document Check List  with upload file option at Action Button:: Admin > Opener > Document Checklist", testCaseId = "TC_33_Modify_Contact_Document")
	public void modifyContactActionButton() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Enable Divisional Users");
			AdminHiddenLinksConfigureNewHierarchyLevelPageTest configure_level = new AdminHiddenLinksConfigureNewHierarchyLevelPageTest();
			configure_level.ConfigureNewHierarchyLevel(driver);

			AdminOpenerDocumentChecklistPage pobj = new AdminOpenerDocumentChecklistPage(driver);
			fc.utobj().printTestStep("Add Document Checklist");
			String documentText = addDocumentChecklist(driver, dataSet);

			fc.utobj().printTestStep("Modify The Contact OF Checklist");
			// modify Action Button

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[.='" + documentText
					+ "']//ancestor::tr[@class='bText12' or @class='bText12 grAltRw3']/td/input"));

			fc.utobj().selectActionMenuItems(driver, "Modify Contact(s)");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.modifyContactSelectBtn);
			fc.utobj().sendKeys(driver, pobj.modifyContactSearchBox, "Divisional User");
			fc.utobj().clickElement(driver, pobj.modifyContactSelectAll);
			fc.utobj().clickElement(driver, pobj.modifyContactSelectBtn);

			try {

				if (fc.utobj().isSelected(driver,pobj.applyToAll.get(1))) {
					// do nothing
				} else {
					fc.utobj().clickElement(driver, pobj.applyToAll.get(1));
				}

			} catch (Exception e) {

			}

			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("Verify The Modify Contact");
			fc.utobj().switchFrameToDefault(driver);

			String textPresent = fc.utobj().getText(driver, fc.utobj().getElementByXpath(driver, ".//*[.='"
					+ documentText + "']//ancestor::tr[@class='bText12' or @class='bText12 grAltRw3']/td[5]"));

			if (!textPresent.contains("Divisional")) {
				fc.utobj().throwsSkipException("was not able to modify Contact");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Configure Default Value Document Check List  with upload file option at Action Button:: Admin > Opener > Document Checklist", testCaseId = "TC_34_Configure_Default_Value_Document")
	public void configureDefaultValueActionButton() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminOpenerDocumentChecklistPage pobj = new AdminOpenerDocumentChecklistPage(driver);
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Add Document Checklist");
			String documentText = addDocumentChecklist(driver, dataSet);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Responsible Department");
			fc.utobj().printTestStep("Add Responsible Department");

			AdminOpenerResponsibleDepartmentPageTest addDepartmentPage = new AdminOpenerResponsibleDepartmentPageTest();
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));
			addDepartmentPage.addDepartment(driver, responsibilityArea);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			/*
			 * String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String password=dataSet.get("password");
			 * corporatePage.addCorporateUser(driver, userName, password,
			 * config);
			 */
			String emailId = "openerautomation@staffex.com";

			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporatePage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Groups");
			fc.utobj().printTestStep("Add Group");

			AdminOpenerManageGroupsPageTest addGroupsPage = new AdminOpenerManageGroupsPageTest();
			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			addGroupsPage.addGroups(driver, groupName);

			fc.opener().opener_common().adminFranchiseOpenerDocumentChecklist(driver);

			fc.utobj().printTestStep("Configure Default Value");
			// modify Action Button

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[.='" + documentText
					+ "']//ancestor::tr[@class='bText12' or @class='bText12 grAltRw3']/td/input"));

			fc.utobj().selectActionMenuItems(driver, "Configure Default Values");

			/*
			 * fc.utobj().clickElement(driver,
			 * pobj.responsibilityAreaDropDownBtn); fc.utobj().sendKeys(driver,
			 * pobj.responsibilityAreaDropDownTxBx, responsibilityArea);
			 * fc.utobj().clickElement(driver,
			 * pobj.responsibilityAreaDropDownSelectAll);
			 * fc.utobj().clickElement(driver,
			 * pobj.responsibilityAreaDropDownBtn);
			 * 
			 * fc.utobj().clickElement(driver, pobj.ContactDropDown);
			 * fc.utobj().sendKeys(driver, pobj.ContactDropDownTxBx,
			 * userInfo.get("userFullName")); fc.utobj().clickElement(driver,
			 * pobj.ContactDropDownSelectAll);
			 * 
			 * fc.utobj().clickElement(driver, pobj.StoreTypeDropDown);
			 * fc.utobj().sendKeys(driver, pobj.StoreTypeDropDownTxBx,
			 * storeType); fc.utobj().clickElement(driver,
			 * pobj.StoreTypeDropDownSelectAll); fc.utobj().clickElement(driver,
			 * pobj.StoreTypeDropDown);
			 */

			fc.utobj().selectValFromMultiSelect(driver, pobj.responsibilitySelect, responsibilityArea);
			fc.utobj().selectValFromMultiSelect(driver, pobj.contactSelect, corpUser.getuserFullName());
			fc.utobj().selectValFromMultiSelect(driver, pobj.storeTypeSelect, storeType);

			fc.utobj().selectDropDown(driver, pobj.groupDropDown, groupName);
			fc.utobj().selectDropDown(driver, pobj.franchiseeAccessDropDown, dataSet.get("franchiseeAccess"));
			fc.utobj().selectDropDown(driver, pobj.priorityDropDown, dataSet.get("priority"));
			fc.utobj().selectDropDown(driver, pobj.criticalLevelDropDown, dataSet.get("criticalLevel"));

			String dependentOn = dataSet.get("dependentOn");
			String valueText = fc.utobj()
					.getElementByXpath(driver,
							".//select[@name='smDocumentChecklist_0referenceParent']/option[contains(text () , '"
									+ dependentOn + "')]")
					.getAttribute("value");
			fc.utobj().moveToElement(driver, pobj.dependentOnDropDown);
			Select sl = new Select(pobj.dependentOnDropDown);
			sl.selectByValue(valueText);

			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("Verify The Configure Default Value");
			String textResponsibility = driver
					.findElement(By.xpath(".//*[@id='ms-parentsmDocumentChecklist_0responsibilityArea']/button/span"))
					.getText().trim();
			String textContact = driver
					.findElement(By.xpath(".//*[@id='ms-parentsmDocumentChecklist_0contact']/button/span")).getText()
					.trim();
			String textStoreType = driver
					.findElement(By.xpath(".//*[@id='ms-parentsmDocumentChecklist_0storeTypeId']/button/span"))
					.getText().trim();

			if (textResponsibility.equalsIgnoreCase(responsibilityArea)
					&& textContact.contains(corpUser.getuserFullName()) && textStoreType.equalsIgnoreCase(storeType)) {
				// do nothing
			} else {
				fc.utobj().throwsException("was not able to Configure default value");
			}

			configureDefaultValue(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/**************** configure Default Value* *********************/

	public void configureDefaultValue(WebDriver driver) throws Exception {

		String testCaseId = "TC_Configure_Default_Equipment_Checklist_01";

		if (fc.utobj().configurationTestCaseToExecute(testCaseId)) {

			try {
				AdminOpenerDocumentChecklistPage pobj = new AdminOpenerDocumentChecklistPage(driver);
				fc.opener().opener_common().adminFranchiseOpenerDocumentChecklist(driver);
				fc.utobj().selectActionMenuItems(driver, "Configure Default Values");

				/*
				 * fc.utobj().clickElement(driver,
				 * pobj.responsibilityAreaDropDownBtn);
				 * fc.utobj().clickElement(driver,
				 * pobj.responsibilityAreaDropDownSelectAll);
				 * fc.utobj().clickElement(driver,
				 * pobj.responsibilityAreaDropDownSelectAll);
				 * fc.utobj().clickElement(driver,
				 * pobj.responsibilityAreaDropDownBtn);
				 * 
				 * fc.utobj().clickElement(driver, pobj.ContactDropDown);
				 * fc.utobj().clickElement(driver,
				 * pobj.ContactDropDownSelectAll);
				 * fc.utobj().clickElement(driver,
				 * pobj.ContactDropDownSelectAll);
				 * 
				 * fc.utobj().clickElement(driver, pobj.StoreTypeDropDown);
				 * fc.utobj().clickElement(driver,
				 * pobj.StoreTypeDropDownSelectAll);
				 * fc.utobj().clickElement(driver,
				 * pobj.StoreTypeDropDownSelectAll);
				 * fc.utobj().clickElement(driver, pobj.StoreTypeDropDown);
				 */

				fc.utobj().setToDefault(driver, pobj.responsibilitySelect);
				fc.utobj().setToDefault(driver, pobj.contactSelect);
				fc.utobj().setToDefault(driver, pobj.storeTypeSelect);

				fc.utobj().selectDropDown(driver, pobj.groupDropDown, "Select");
				fc.utobj().selectDropDown(driver, pobj.franchiseeAccessDropDown, "Select");
				fc.utobj().selectDropDown(driver, pobj.priorityDropDown, "Select");
				fc.utobj().selectDropDown(driver, pobj.criticalLevelDropDown, "Select");
				String dependentOn = "None (Timeless)";
				String valueText = fc.utobj()
						.getElementByXpath(driver,
								".//select[@name='smDocumentChecklist_0referenceParent']/option[contains(text () , '"
										+ dependentOn + "')]")
						.getAttribute("value");
				fc.utobj().moveToElement(driver, pobj.dependentOnDropDown);
				Select sl = new Select(pobj.dependentOnDropDown);
				sl.selectByValue(valueText);
				fc.utobj().clickElement(driver, pobj.saveBtn);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Modify Document Check List  with upload file option at Action Button Bottom:: Admin > Opener > Document Checklist", testCaseId = "TC_35_Modify_Document_Checklist02")
	public void modifyDocumentChecklistBottom() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminOpenerDocumentChecklistPage pobj = new AdminOpenerDocumentChecklistPage(driver);
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Add Document Checklist");
			String documentText = addDocumentChecklist(driver, dataSet);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Responsible Department");
			fc.utobj().printTestStep("Add Responsible Department");

			AdminOpenerResponsibleDepartmentPageTest addDepartmentPage = new AdminOpenerResponsibleDepartmentPageTest();
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));
			addDepartmentPage.addDepartment(driver, responsibilityArea);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Groups");
			fc.utobj().printTestStep("Add Group");

			AdminOpenerManageGroupsPageTest addGroupsPage = new AdminOpenerManageGroupsPageTest();
			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			addGroupsPage.addGroups(driver, groupName);

			fc.opener().opener_common().adminFranchiseOpenerDocumentChecklist(driver);

			fc.utobj().printTestStep("Modify The Document Checklist");
			// modify Action Btn Bottom

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[.='" + documentText
					+ "']//ancestor::tr[@class='bText12' or @class='bText12 grAltRw3']/td/input"));
			fc.utobj().clickElement(driver, pobj.bottomModifyBtn);

			documentText = fc.utobj().generateTestData(dataSet.get("documentText"));
			fc.utobj().sendKeys(driver, pobj.documentNameTxtBox, documentText);

			/*
			 * fc.utobj().clickElement(driver,
			 * pobj.responsibilityAreaDropDownBtn); fc.utobj().sendKeys(driver,
			 * pobj.responsibilityAreaDropDownTxBx, responsibilityArea);
			 * fc.utobj().clickElement(driver,
			 * pobj.responsibilityAreaDropDownSelectAll);
			 * fc.utobj().clickElement(driver,
			 * pobj.responsibilityAreaDropDownBtn);
			 */

			fc.utobj().selectValFromMultiSelect(driver, pobj.responsibilitySelect, responsibilityArea);
			fc.utobj().selectValFromMultiSelect(driver, pobj.storeTypeSelect, storeType);

			fc.utobj().selectDropDown(driver, pobj.groupDropDown, groupName);
			fc.utobj().selectDropDown(driver, pobj.franchiseeAccessDropDown, dataSet.get("franchiseeAccess"));
			fc.utobj().selectDropDown(driver, pobj.priorityDropDown, dataSet.get("priority"));
			fc.utobj().selectDropDown(driver, pobj.criticalLevelDropDown, dataSet.get("criticalLevel"));

			String dependentOn = dataSet.get("dependentOnModify");
			String valueText = fc.utobj()
					.getElementByXpath(driver,
							".//select[@name='smDocumentChecklist_0referenceParent']/option[contains(text () , '"
									+ dependentOn + "')]")
					.getAttribute("value");
			fc.utobj().moveToElement(driver, pobj.dependentOnDropDown);
			Select sl = new Select(pobj.dependentOnDropDown);
			sl.selectByValue(valueText);

			if (fc.utobj().isSelected(driver, pobj.webLinkRadioBtn)) {
				// do nothing
			} else {
				fc.utobj().clickElement(driver, pobj.webLinkRadioBtn);
			}

			fc.utobj().sendKeys(driver, pobj.webLinkTxBx, dataSet.get("webLinkUrl"));

			try {
				List<WebElement> listRadioButton = pobj.applyToAll;
				if (fc.utobj().isSelected(driver,listRadioButton.get(0))) {
					// do nothing
				} else {
					fc.utobj().clickElement(driver, listRadioButton.get(0));
				}
			} catch (Exception e) {

			}

			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("Verify The Modify of Checklist");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, documentText);

			if (isTextPresent == false) {
				fc.utobj().throwsSkipException("was not able to modify Document checklist At bottom btn");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Delete Document Check List  with upload file option at Bottom:: Admin > Opener > Document Checklist", testCaseId = "TC_36_Delete_Document_Checklist02")
	public void deleteDocumentChecklistBottom() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminOpenerDocumentChecklistPage pobj = new AdminOpenerDocumentChecklistPage(driver);
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Add Document Checklist");
			String documentText = addDocumentChecklist(driver, dataSet);

			fc.utobj().printTestStep("Delete The Document Checklist");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[.='" + documentText
					+ "']//ancestor::tr[@class='bText12' or @class='bText12 grAltRw3']/td/input"));
			fc.utobj().clickElement(driver, pobj.bottomDeleteBtn);

			fc.utobj().acceptAlertBox(driver);

			try {

				fc.commonMethods().switch_cboxIframe_frameId(driver);
				if (fc.utobj().isSelected(driver,pobj.applyToAll.get(0))) {
					// do nothing
				} else {
					fc.utobj().clickElement(driver, pobj.applyToAll.get(0));
				}

				fc.utobj().clickElement(driver, pobj.cboxDeleteBtn);
				fc.utobj().clickElement(driver, pobj.cboxCloseBtn);
				fc.utobj().switchFrameToDefault(driver);

			} catch (Exception e) {

			}

			fc.utobj().printTestStep("Verify The Delete Checklist");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, documentText);

			if (isTextPresent == true) {
				fc.utobj().throwsSkipException("Was not able to delete Document Checklist");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
}
