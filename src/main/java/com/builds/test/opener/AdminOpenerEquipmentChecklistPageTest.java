package com.builds.test.opener;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminConfigurationConfigureStoreTypePageTest;
import com.builds.test.admin.AdminHiddenLinksConfigureNewHierarchyLevelPageTest;
import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.admin.AdminUsersSupplierDetailsAddSupplierTest;
import com.builds.test.common.CorporateUser;
import com.builds.uimaps.opener.AdminOpenerEquipmentChecklistPage;
import com.builds.utilities.Browsers;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminOpenerEquipmentChecklistPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "opener", "openeremail" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-29", testCaseDescription = "Add Equipment Checklist with upload file:: Admin > Opener > Equipment Checklist.", testCaseId = "TC_13_Add_Equipment_Checklist01")
	public void addEquipmentChecklistUploadFile() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminOpenerEquipmentChecklistPage pobj = new AdminOpenerEquipmentChecklistPage(driver);
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > Users > Supplier Details");
			fc.utobj().printTestStep("Add Supplier");

			AdminUsersSupplierDetailsAddSupplierTest addSupplierPage = new AdminUsersSupplierDetailsAddSupplierTest();
			String supplierName = fc.utobj().generateTestData(dataSet.get("supplierName"));
			String emailId = "openerautomation@staffex.com";
			addSupplierPage.addSupplier(driver, supplierName, config, emailId);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Responsible Department");
			fc.utobj().printTestStep("Add Responsible Department");

			AdminOpenerResponsibleDepartmentPageTest addDepartmentPage = new AdminOpenerResponsibleDepartmentPageTest();
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));
			addDepartmentPage.addDepartment(driver, responsibilityArea);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporatePage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Groups");
			fc.utobj().printTestStep("Add Group");

			AdminOpenerManageGroupsPageTest addGroupsPage = new AdminOpenerManageGroupsPageTest();
			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			addGroupsPage.addGroups(driver, groupName);

			fc.utobj().printTestStep("Add Equipment Checklist");
			fc.opener().opener_common().adminFranchiseOpenerEquipmentChecklist(driver);
			fc.utobj().clickElement(driver, pobj.addMoreBtn);

			String equepmentText = fc.utobj().generateTestData(dataSet.get("equepmentText"));
			fc.utobj().sendKeys(driver, pobj.equipmentTextArea, equepmentText);
			fc.utobj().sendKeys(driver, pobj.quantityTextBx, dataSet.get("quantity"));
			fc.utobj().selectDropDown(driver, pobj.supplierSelectDropDown, supplierName);

			fc.utobj().selectValFromMultiSelect(driver, pobj.responsibilitySelect, responsibilityArea);
			fc.utobj().selectValFromMultiSelect(driver, pobj.contactSelect, corpUser.getuserFullName());
			fc.utobj().selectValFromMultiSelect(driver, pobj.storeTypeSelect, storeType);

			fc.utobj().selectDropDown(driver, pobj.groupSelectDropDown, groupName);
			fc.utobj().selectDropDown(driver, pobj.franchiseAccessDropDown, dataSet.get("franchiseeAccess"));
			fc.utobj().selectDropDown(driver, pobj.priorityDropDown, dataSet.get("priority"));
			fc.utobj().selectDropDown(driver, pobj.criticalLevelDropDown, dataSet.get("criticalLevel"));

			String dependentOn = dataSet.get("dependentOn");
			String valueText = fc.utobj()
					.getElementByXpath(driver,
							".//select[@name='smEquipmentChecklist_0referenceParent']/option[contains(text () , '"
									+ dependentOn + "')]")
					.getAttribute("value");
			fc.utobj().moveToElement(driver, pobj.dependentOnDropDown);
			Select sl = new Select(pobj.dependentOnDropDown);
			sl.selectByValue(valueText);

			if (fc.utobj().isSelected(driver, pobj.uploadFileRadioBtn)) {
				// do nothing
			} else {
				fc.utobj().clickElement(driver, pobj.uploadFileRadioBtn);
			}

			String file = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));

			System.out.println(file);
			fc.utobj().sendKeys(driver, pobj.attachedTxtBx, file);
			fc.utobj().clickElement(driver, pobj.addBtn);

			try {
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().clickElement(driver, pobj.noBtn);
				fc.utobj().switchFrameToDefault(driver);
			} catch (Exception e) {

			}

			fc.utobj().printTestStep("Verify The Add Equipment Checklist");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, equepmentText);

			if (isTextPresent == false) {
				fc.utobj().throwsSkipException("was not able add equipment checklist");
			}

			fc.utobj().printTestStep("Verify The Mail After Equipment Check List Creation");

			Map<String, String> mailData = new HashMap<String, String>();

			mailData = fc.utobj().readMailBox("Checklist Item Creation Alert", equepmentText, emailId, "sdg@1a@Hfs");

			if (mailData.size() == 0 || !mailData.get("mailBody").contains("New Item Created")) {
				fc.utobj().throwsException("was not able to verify Equipment checklist creation mail");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addEquipmentChecklistUploadFile(WebDriver driver, Map<String, String> dataSet,
			Map<String, String> config) throws Exception {

		String testCaseId = "TC_Add_Equipment_Checklist_Upload_File_Opener_Admin";
		String equepmentText = null;
		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminOpenerEquipmentChecklistPage pobj = new AdminOpenerEquipmentChecklistPage(driver);

				AdminOpenerResponsibleDepartmentPageTest addDepartmentPage = new AdminOpenerResponsibleDepartmentPageTest();
				String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));
				addDepartmentPage.addDepartment(driver, responsibilityArea);

				AdminUsersManageCorporateUsersAddCorporateUserPageTest corporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
				String emailId = "openerautomation@staffex.com";
				CorporateUser corpUser = new CorporateUser();
				corpUser.setEmail(emailId);
				corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
				corporatePage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

				AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
				String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
				storeTypePage.addStoreType(driver, storeType);

				fc.opener().opener_common().adminFranchiseOpenerEquipmentChecklist(driver);
				fc.utobj().clickElement(driver, pobj.addMoreBtn);

				equepmentText = fc.utobj().generateTestData(dataSet.get("equepmentText"));
				fc.utobj().sendKeys(driver, pobj.equipmentTextArea, equepmentText);
				fc.utobj().sendKeys(driver, pobj.quantityTextBx, dataSet.get("quantity"));

				fc.utobj().selectValFromMultiSelect(driver, pobj.responsibilitySelect, responsibilityArea);
				fc.utobj().selectValFromMultiSelect(driver, pobj.contactSelect, corpUser.getuserFullName());
				fc.utobj().selectValFromMultiSelect(driver, pobj.storeTypeSelect, storeType);

				fc.utobj().selectDropDown(driver, pobj.franchiseAccessDropDown, dataSet.get("franchiseeAccess"));
				fc.utobj().selectDropDown(driver, pobj.priorityDropDown, dataSet.get("priority"));
				fc.utobj().selectDropDown(driver, pobj.criticalLevelDropDown, dataSet.get("criticalLevel"));

				String dependentOn = dataSet.get("dependentOn");
				String valueText = fc.utobj()
						.getElementByXpath(driver,
								".//select[@name='smEquipmentChecklist_0referenceParent']/option[contains(text () , '"
										+ dependentOn + "')]")
						.getAttribute("value");
				fc.utobj().moveToElement(driver, pobj.dependentOnDropDown);
				Select sl = new Select(pobj.dependentOnDropDown);
				sl.selectByValue(valueText);

				if (dependentOn.equalsIgnoreCase("Multiple Checklists")) {

					fc.utobj().clickElement(driver, pobj.dependentSelectBtn);
					fc.utobj().clickElement(driver, pobj.dependentSelectAllChckBx);

					fc.utobj().sendKeys(driver, pobj.startScheduledTxBx, "1");
					fc.utobj().selectDropDown(driver, pobj.startScheduledSelect, "Days Prior");

					fc.utobj().sendKeys(driver, pobj.CompletionScheduledTxBx, "1");
					fc.utobj().selectDropDown(driver, pobj.CompletionScheduledSelect, "Days After");

					fc.utobj().sendKeys(driver, pobj.completiomReminderTxBx, "3");
					fc.utobj().sendKeys(driver, pobj.completiomReminderTxBx, "3");

				} else if (dependentOn.equalsIgnoreCase("Expected Opening Date")) {

					fc.utobj().sendKeys(driver, pobj.startScheduledTxBx, "1");
					fc.utobj().selectDropDown(driver, pobj.startScheduledSelect, "Days Prior");

					fc.utobj().sendKeys(driver, pobj.CompletionScheduledTxBx, "1");
					fc.utobj().selectDropDown(driver, pobj.CompletionScheduledSelect, "Days After");

					fc.utobj().sendKeys(driver, pobj.completiomReminderTxBx, "3");
					fc.utobj().sendKeys(driver, pobj.completiomReminderTxBx, "3");

				}

				if (fc.utobj().isSelected(driver, pobj.uploadFileRadioBtn)) {
					// do nothing
				} else {
					fc.utobj().clickElement(driver, pobj.uploadFileRadioBtn);
				}

				String file = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));

				System.out.println(file);
				fc.utobj().sendKeys(driver, pobj.attachedTxtBx, file);
				fc.utobj().clickElement(driver, pobj.addBtn);
				try {
					fc.commonMethods().switch_cboxIframe_frameId(driver);
					fc.utobj().clickElement(driver, pobj.noBtn);
					fc.utobj().switchFrameToDefault(driver);
				} catch (Exception e) {

				}

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to add Equipment Checklist");

		}

		return equepmentText;

	}

	public String addEquipmentChecklistUploadFile(WebDriver driver, Map<String, String> dataSet, File file,
			String responsibilityArea, String storeType, String userName, String quantity, Map<String, String> config)
			throws Exception {

		String testCaseId = "TC_Add_Equipment_Checklist_Upload_File_Admin_Opener_01";
		String equepmentText = null;
		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminOpenerEquipmentChecklistPage pobj = new AdminOpenerEquipmentChecklistPage(driver);

				AdminOpenerResponsibleDepartmentPageTest addDepartmentPage = new AdminOpenerResponsibleDepartmentPageTest();
				addDepartmentPage.addDepartment(driver, responsibilityArea);

				// configureDefaultValue(driver, config);

				fc.opener().opener_common().adminFranchiseOpenerEquipmentChecklist(driver);
				fc.utobj().clickElement(driver, pobj.addMoreBtn);

				equepmentText = fc.utobj().generateTestData(dataSet.get("equepmentText"));
				fc.utobj().sendKeys(driver, pobj.equipmentTextArea, equepmentText);
				fc.utobj().sendKeys(driver, pobj.quantityTextBx, quantity);

				/*
				 * fc.utobj().clickElement(driver,
				 * pobj.responsibilityAreaSelectBtn);
				 * fc.utobj().sendKeys(driver, pobj.responsibilityAreaSearchBx,
				 * responsibilityArea); fc.utobj().clickElement(driver,
				 * pobj.responsibilityAreaSelectAll);
				 * fc.utobj().clickElement(driver,
				 * pobj.responsibilityAreaSelectBtn);
				 * 
				 * fc.utobj().clickElement(driver, pobj.contactSelectBtn);
				 * fc.utobj().sendKeys(driver, pobj.contactSearchBx, userName);
				 * fc.utobj().clickElement(driver, pobj.contactSelectAll);
				 */

				fc.utobj().selectValFromMultiSelect(driver, pobj.responsibilitySelect, responsibilityArea);
				fc.utobj().selectValFromMultiSelect(driver, pobj.contactSelect, userName);

				if (storeType.equalsIgnoreCase("Default All selected")) {
					fc.utobj().clickElement(driver, pobj.storeTypeSelectBtn);
					fc.utobj().sendKeys(driver, pobj.storeTypeSearchBx, "All Stores");
					fc.utobj().clickElement(driver, pobj.storeTypeSelectAll);
					fc.utobj().clickElement(driver, pobj.storeTypeSelectBtn);

					fc.utobj().clickElement(driver, pobj.storeTypeSelectBtn);
					fc.utobj().sendKeys(driver, pobj.storeTypeSearchBx, "Default");
					fc.utobj().clickElement(driver, pobj.storeTypeSelectBtn);
				} else {
					fc.utobj().selectValFromMultiSelect(driver, pobj.storeTypeSelect, storeType);
				}

				fc.utobj().selectDropDown(driver, pobj.franchiseAccessDropDown, dataSet.get("franchiseeAccess"));
				fc.utobj().selectDropDown(driver, pobj.priorityDropDown, dataSet.get("priority"));
				fc.utobj().selectDropDown(driver, pobj.criticalLevelDropDown, dataSet.get("criticalLevel"));

				String dependentOn = dataSet.get("dependentOn");
				String valueText = fc.utobj()
						.getElementByXpath(driver,
								".//select[@name='smEquipmentChecklist_0referenceParent']/option[contains(text () , '"
										+ dependentOn + "')]")
						.getAttribute("value");
				fc.utobj().moveToElement(driver, pobj.dependentOnDropDown);
				Select sl = new Select(pobj.dependentOnDropDown);
				sl.selectByValue(valueText);

				if (dependentOn.equalsIgnoreCase("Multiple Checklists")) {

					fc.utobj().clickElement(driver, pobj.dependentSelectBtn);
					fc.utobj().clickElement(driver, pobj.dependentSelectAllChckBx);

					fc.utobj().sendKeys(driver, pobj.startScheduledTxBx, "1");
					fc.utobj().selectDropDown(driver, pobj.startScheduledSelect, "Days Prior");

					fc.utobj().sendKeys(driver, pobj.CompletionScheduledTxBx, "1");
					fc.utobj().selectDropDown(driver, pobj.CompletionScheduledSelect, "Days After");

					fc.utobj().sendKeys(driver, pobj.completiomReminderTxBx, "3");
					fc.utobj().sendKeys(driver, pobj.completiomReminderTxBx, "3");

				} else if (dependentOn.equalsIgnoreCase("Expected Opening Date")) {

					fc.utobj().sendKeys(driver, pobj.startScheduledTxBx, "1");
					fc.utobj().selectDropDown(driver, pobj.startScheduledSelect, "Days Prior");

					fc.utobj().sendKeys(driver, pobj.CompletionScheduledTxBx, "1");
					fc.utobj().selectDropDown(driver, pobj.CompletionScheduledSelect, "Days After");

					fc.utobj().sendKeys(driver, pobj.completiomReminderTxBx, "3");
					fc.utobj().sendKeys(driver, pobj.completiomReminderTxBx, "3");

				} else if (dependentOn.equalsIgnoreCase("None (Timeless)")) {
					// nothing
				}

				if (fc.utobj().isSelected(driver, pobj.uploadFileRadioBtn)) {
					// do nothing
				} else {
					fc.utobj().clickElement(driver, pobj.uploadFileRadioBtn);
				}

				fc.utobj().sendKeys(driver, pobj.attachedTxtBx, file.getAbsolutePath());
				fc.utobj().clickElement(driver, pobj.addBtn);

				try {
					fc.commonMethods().switch_cboxIframe_frameId(driver);
					fc.utobj().clickElement(driver, pobj.yesBtn);
					fc.utobj().switchFrameToDefault(driver);
				} catch (Exception e) {

				}

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to add Equipment Checklist FOr Upload File");

		}

		return equepmentText;

	}

	// With Group

	public String addEquipmentChecklistUploadFileWithGroup(WebDriver driver, Map<String, String> dataSet, File file,
			String responsibilityArea, String groupName, String storeType, String userName, String quantity,
			Map<String, String> config) throws Exception {

		String testCaseId = "TC_Add_Equipment_Checklist_Upload_File_Admin_Opener_01";
		String equepmentText = null;
		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminOpenerEquipmentChecklistPage pobj = new AdminOpenerEquipmentChecklistPage(driver);

				AdminOpenerResponsibleDepartmentPageTest addDepartmentPage = new AdminOpenerResponsibleDepartmentPageTest();
				addDepartmentPage.addDepartment(driver, responsibilityArea);

				AdminOpenerManageGroupsPageTest addGroupsPage = new AdminOpenerManageGroupsPageTest();
				addGroupsPage.addGroups(driver, groupName);

				fc.opener().opener_common().adminFranchiseOpenerEquipmentChecklist(driver);
				fc.utobj().clickElement(driver, pobj.addMoreBtn);

				equepmentText = fc.utobj().generateTestData(dataSet.get("equepmentText"));
				fc.utobj().sendKeys(driver, pobj.equipmentTextArea, equepmentText);
				fc.utobj().sendKeys(driver, pobj.quantityTextBx, quantity);

				fc.utobj().selectValFromMultiSelect(driver, pobj.responsibilitySelect, responsibilityArea);

				fc.utobj().selectValFromMultiSelect(driver, pobj.contactSelect, userName);

				if (storeType.equalsIgnoreCase("Default All selected")) {
					fc.utobj().clickElement(driver, pobj.storeTypeSelectBtn);
					fc.utobj().sendKeys(driver, pobj.storeTypeSearchBx, "All Stores");
					fc.utobj().clickElement(driver, pobj.storeTypeSelectAll);
					fc.utobj().clickElement(driver, pobj.storeTypeSelectBtn);

					fc.utobj().clickElement(driver, pobj.storeTypeSelectBtn);
					fc.utobj().sendKeys(driver, pobj.storeTypeSearchBx, "Default");
					fc.utobj().clickElement(driver, pobj.storeTypeSelectBtn);
				} else {
					fc.utobj().selectValFromMultiSelect(driver, pobj.storeTypeSelect, storeType);
				}

				fc.utobj().selectDropDown(driver, pobj.groupSelectDropDown, groupName);
				fc.utobj().selectDropDown(driver, pobj.franchiseAccessDropDown, dataSet.get("franchiseeAccess"));
				fc.utobj().selectDropDown(driver, pobj.priorityDropDown, dataSet.get("priority"));
				fc.utobj().selectDropDown(driver, pobj.criticalLevelDropDown, dataSet.get("criticalLevel"));

				String dependentOn = dataSet.get("dependentOn");
				String valueText = fc.utobj()
						.getElementByXpath(driver,
								".//select[@name='smEquipmentChecklist_0referenceParent']/option[contains(text () , '"
										+ dependentOn + "')]")
						.getAttribute("value");
				fc.utobj().moveToElement(driver, pobj.dependentOnDropDown);
				Select sl = new Select(pobj.dependentOnDropDown);
				sl.selectByValue(valueText);

				if (dependentOn.equalsIgnoreCase("Multiple Checklists")) {

					fc.utobj().clickElement(driver, pobj.dependentSelectBtn);
					fc.utobj().clickElement(driver, pobj.dependentSelectAllChckBx);

					fc.utobj().sendKeys(driver, pobj.startScheduledTxBx, "1");
					fc.utobj().selectDropDown(driver, pobj.startScheduledSelect, "Days Prior");

					fc.utobj().sendKeys(driver, pobj.CompletionScheduledTxBx, "1");
					fc.utobj().selectDropDown(driver, pobj.CompletionScheduledSelect, "Days After");

					fc.utobj().sendKeys(driver, pobj.completiomReminderTxBx, "3");
					fc.utobj().sendKeys(driver, pobj.completiomReminderTxBx, "3");

				} else if (dependentOn.equalsIgnoreCase("Expected Opening Date")) {

					fc.utobj().sendKeys(driver, pobj.startScheduledTxBx, "1");
					fc.utobj().selectDropDown(driver, pobj.startScheduledSelect, "Days Prior");

					fc.utobj().sendKeys(driver, pobj.CompletionScheduledTxBx, "1");
					fc.utobj().selectDropDown(driver, pobj.CompletionScheduledSelect, "Days After");

					fc.utobj().sendKeys(driver, pobj.completiomReminderTxBx, "3");
					fc.utobj().sendKeys(driver, pobj.completiomReminderTxBx, "3");

				} else if (dependentOn.equalsIgnoreCase("None (Timeless)")) {
					// nothing
				}

				if (fc.utobj().isSelected(driver, pobj.uploadFileRadioBtn)) {
					// do nothing
				} else {
					fc.utobj().clickElement(driver, pobj.uploadFileRadioBtn);
				}

				fc.utobj().sendKeys(driver, pobj.attachedTxtBx, file.getAbsolutePath());
				fc.utobj().clickElement(driver, pobj.addBtn);

				try {
					fc.commonMethods().switch_cboxIframe_frameId(driver);
					fc.utobj().clickElement(driver, pobj.yesBtn);
					fc.utobj().switchFrameToDefault(driver);
				} catch (Exception e) {

				}

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to add Equipment Checklist FOr Upload File");

		}
		return equepmentText;

	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Add Equipment Checklist with webLink:: Admin > Opener > Equipment Checklist", testCaseId = "TC_14_Add_Equipment_Checklist02")
	public void addEquipmentChecklistWebLink() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminOpenerEquipmentChecklistPage pobj = new AdminOpenerEquipmentChecklistPage(driver);
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > Users > Supplier Details");
			fc.utobj().printTestStep("Add Supplier");

			AdminUsersSupplierDetailsAddSupplierTest addSupplierPage = new AdminUsersSupplierDetailsAddSupplierTest();
			String supplierName = fc.utobj().generateTestData(dataSet.get("supplierName"));
			String emailId = "openerautomation@staffex.com";
			addSupplierPage.addSupplier(driver, supplierName, config, emailId);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Responsible Department");
			fc.utobj().printTestStep("Add Responsible Department");

			AdminOpenerResponsibleDepartmentPageTest addDepartmentPage = new AdminOpenerResponsibleDepartmentPageTest();
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));
			addDepartmentPage.addDepartment(driver, responsibilityArea);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporatePage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Groups");
			fc.utobj().printTestStep("Add Group");

			AdminOpenerManageGroupsPageTest addGroupsPage = new AdminOpenerManageGroupsPageTest();
			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			addGroupsPage.addGroups(driver, groupName);

			fc.utobj().printTestStep("Add Equipment Checklist With WebLink Option");
			fc.opener().opener_common().adminFranchiseOpenerEquipmentChecklist(driver);
			fc.utobj().clickElement(driver, pobj.addMoreBtn);

			String equepmentText = fc.utobj().generateTestData(dataSet.get("equepmentText"));
			fc.utobj().sendKeys(driver, pobj.equipmentTextArea, equepmentText);
			fc.utobj().sendKeys(driver, pobj.quantityTextBx, dataSet.get("quantity"));
			fc.utobj().selectDropDown(driver, pobj.supplierSelectDropDown, supplierName);

			fc.utobj().selectValFromMultiSelect(driver, pobj.responsibilitySelect, responsibilityArea);

			fc.utobj().selectValFromMultiSelect(driver, pobj.contactSelect, corpUser.getuserFullName());

			fc.utobj().selectValFromMultiSelect(driver, pobj.storeTypeSelect, storeType);

			fc.utobj().selectDropDown(driver, pobj.groupSelectDropDown, groupName);

			fc.utobj().selectDropDown(driver, pobj.franchiseAccessDropDown, dataSet.get("franchiseeAccess"));

			fc.utobj().selectDropDown(driver, pobj.priorityDropDown, dataSet.get("priority"));

			fc.utobj().selectDropDown(driver, pobj.criticalLevelDropDown, dataSet.get("criticalLevel"));

			String dependentOn = dataSet.get("dependentOn");
			String valueText = fc.utobj()
					.getElementByXpath(driver,
							".//select[@name='smEquipmentChecklist_0referenceParent']/option[contains(text () , '"
									+ dependentOn + "')]")
					.getAttribute("value");
			fc.utobj().moveToElement(driver, pobj.dependentOnDropDown);
			Select sl = new Select(pobj.dependentOnDropDown);
			sl.selectByValue(valueText);

			/*
			 * fc.utobj().clickElement(driver, pobj.dependentSelectBtn);
			 * fc.utobj().clickElement(driver, pobj.dependentSelectAllChckBx);
			 */

			fc.utobj().sendKeys(driver, pobj.startScheduledTxBx, "1");
			fc.utobj().selectDropDown(driver, pobj.startScheduledSelect, "Days Prior");

			fc.utobj().sendKeys(driver, pobj.CompletionScheduledTxBx, "1");
			fc.utobj().selectDropDown(driver, pobj.CompletionScheduledSelect, "Days After");

			fc.utobj().sendKeys(driver, pobj.completiomReminderTxBx, "3");
			fc.utobj().sendKeys(driver, pobj.completiomReminderTxBx, "3");

			if (fc.utobj().isSelected(driver, pobj.webLinkRadioBtn)) {
				// do nothing
			} else {
				fc.utobj().clickElement(driver, pobj.webLinkRadioBtn);
			}

			fc.utobj().sendKeys(driver, pobj.weblinkTxtBx, dataSet.get("webLinkUrl"));

			fc.utobj().clickElement(driver, pobj.addBtn);

			try {
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().clickElement(driver, pobj.noBtn);
				fc.utobj().switchFrameToDefault(driver);
			} catch (Exception e) {

			}

			fc.utobj().printTestStep("Verify The Add Equipment Checklist");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, equepmentText);

			if (isTextPresent == false) {
				fc.utobj().throwsSkipException("was not able add equipment checklist");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Modify Equipment Checklist with upload file At ActionImg:: Admin > Opener > Equipment Checklist", testCaseId = "TC_15_Modify_Equipment_Checklist")
	public void modifyEquipmentChecklistActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminOpenerEquipmentChecklistPage pobj = new AdminOpenerEquipmentChecklistPage(driver);
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > Users > Supplier Details");
			fc.utobj().printTestStep("Add Supplier");

			AdminUsersSupplierDetailsAddSupplierTest addSupplierPage = new AdminUsersSupplierDetailsAddSupplierTest();
			String supplierName = fc.utobj().generateTestData(dataSet.get("supplierName"));
			String emailId = "openerautomation@staffex.com";
			addSupplierPage.addSupplier(driver, supplierName, config, emailId);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Responsible Department");
			fc.utobj().printTestStep("Add Responsible Department");

			AdminOpenerResponsibleDepartmentPageTest addDepartmentPage = new AdminOpenerResponsibleDepartmentPageTest();
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));
			addDepartmentPage.addDepartment(driver, responsibilityArea);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Groups");
			fc.utobj().printTestStep("Add Group");

			AdminOpenerManageGroupsPageTest addGroupsPage = new AdminOpenerManageGroupsPageTest();
			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			addGroupsPage.addGroups(driver, groupName);

			fc.utobj().printTestStep("Add Equipment Checklist");
			// Add Equipment
			String equepmentText = addEquipmentChecklistUploadFile(driver, dataSet, config);

			fc.utobj().printTestStep("Modify Equipment Checklist");
			// modifyActionImg
			String text = driver
					.findElement(By.xpath(".//*[.='" + equepmentText + "']//ancestor::tr/td/div[@id='menuBar']/layer"))
					.getAttribute("id").trim();
			String alterText = text.replace("Actions_dynamicmenu", "");
			alterText = alterText.replace("Bar", "");

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[.='" + equepmentText + "']//ancestor::tr/td/div[@id='menuBar']/layer/a/img"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//div[@id='Actions_dynamicmenu" + alterText + "Menu']/span[contains(text () , 'Modify')]"));

			equepmentText = fc.utobj().generateTestData(dataSet.get("equepmentText"));
			fc.utobj().sendKeys(driver, pobj.equipmentTextArea, equepmentText);
			fc.utobj().sendKeys(driver, pobj.quantityTextBx, dataSet.get("quantity"));
			fc.utobj().selectDropDown(driver, pobj.supplierSelectDropDown, supplierName);

			fc.utobj().selectValFromMultiSelect(driver, pobj.responsibilitySelect, responsibilityArea);

			fc.utobj().selectValFromMultiSelect(driver, pobj.storeTypeSelect, storeType);

			fc.utobj().selectDropDown(driver, pobj.groupSelectDropDown, groupName);

			fc.utobj().selectDropDown(driver, pobj.franchiseAccessDropDown, dataSet.get("franchiseeAccess"));

			fc.utobj().selectDropDown(driver, pobj.priorityDropDown, dataSet.get("priority"));

			fc.utobj().selectDropDown(driver, pobj.criticalLevelDropDown, dataSet.get("criticalLevel"));

			String dependentOn = dataSet.get("dependentOnModify");
			String valueText = fc.utobj()
					.getElementByXpath(driver,
							".//select[@name='smEquipmentChecklist_0referenceParent']/option[contains(text () , '"
									+ dependentOn + "')]")
					.getAttribute("value");
			fc.utobj().moveToElement(driver, pobj.dependentOnDropDown);
			Select sl = new Select(pobj.dependentOnDropDown);
			sl.selectByValue(valueText);

			if (fc.utobj().isSelected(driver, pobj.webLinkRadioBtn)) {
				// do nothing
			} else {
				fc.utobj().clickElement(driver, pobj.webLinkRadioBtn);
			}

			fc.utobj().sendKeys(driver, pobj.weblinkTxtBx, dataSet.get("webLinkUrl"));

			try {
				fc.utobj().moveToElement(driver, fc.utobj().getElementByName(driver, "applyToAll"));
				List<WebElement> listRadioButton = pobj.applyToAll;
				if (fc.utobj().isSelected(driver,listRadioButton.get(0))) {
					// do nothing
				} else {
					fc.utobj().clickElement(driver, listRadioButton.get(0));
				}
			} catch (Exception e) {

			}

			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("Verify The Modify Equipment Checklist");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, equepmentText);

			if (isTextPresent == false) {
				fc.utobj().throwsSkipException("was not able modify equipment checklist");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "opener", "openeremail" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-29", testCaseDescription = "Delete Equipment Checklist with upload file At ActionImg:: Admin > Opener > Equipment Checklist", testCaseId = "TC_16_Delete_Equipment_Checklist")
	public void deleteEquipmentChecklistActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminOpenerEquipmentChecklistPage pobj = new AdminOpenerEquipmentChecklistPage(driver);
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Add Equipment Checklist");
			// Add Equipment
			String equepmentText = addEquipmentChecklistUploadFile(driver, dataSet, config);

			fc.utobj().printTestStep("Delete Equipment Checklist");
			// modifyActionImg
			String text = driver
					.findElement(By.xpath(".//*[.='" + equepmentText + "']//ancestor::tr/td/div[@id='menuBar']/layer"))
					.getAttribute("id").trim();
			String alterText = text.replace("Actions_dynamicmenu", "");
			alterText = alterText.replace("Bar", "");

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[.='" + equepmentText + "']//ancestor::tr/td/div[@id='menuBar']/layer/a/img"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//div[@id='Actions_dynamicmenu" + alterText + "Menu']/span[contains(text () , 'Delete')]"));

			fc.utobj().acceptAlertBox(driver);

			try {
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().moveToElement(driver, fc.utobj().getElementByName(driver, "applyToAll"));
				List<WebElement> listRadioButton = pobj.applyToAll;

				if (fc.utobj().isSelected(driver,listRadioButton.get(0))) {
					// do nothing
				} else {
					fc.utobj().clickElement(driver, listRadioButton.get(0));
				}

				fc.utobj().clickElement(driver, pobj.deleteFrameEquipmentBtn);
				fc.utobj().clickElement(driver, pobj.closeFrameEquipmentBtn);
				fc.utobj().switchFrameToDefault(driver);
			} catch (Exception e) {

			}

			fc.utobj().printTestStep("Verify The Delete Equipment Checklist");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, equepmentText);

			if (isTextPresent == true) {
				fc.utobj().throwsSkipException("was not able delete equipment checklist");
			}

			fc.utobj().printTestStep("Verify Email After Equipment Checklist Deletion");

			Map<String, String> mailData = new HashMap<String, String>();
			mailData = fc.utobj().readMailBox("Checklist Item Deletion Alert", equepmentText,
					"openerautomation@staffex.com", "sdg@1a@Hfs");

			if (mailData.size() == 0 || !mailData.get("mailBody").contains("Item Deleted")) {

				fc.utobj().throwsException("was not able to verify Equipment Checklist Deletion Mail");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Add Description in Equipment Checklist with upload file At ActionImg:: Admin > Opener > Equipment Checklist", testCaseId = "TC_17_Add_Description_Equipment")
	public void addDescriptionEquipmentActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminOpenerEquipmentChecklistPage pobj = new AdminOpenerEquipmentChecklistPage(driver);
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Add Equipment Checklist");
			String equepmentText = addEquipmentChecklistUploadFile(driver, dataSet, config);
			fc.utobj().printTestStep("Add Description");
			fc.utobj().actionImgOption(driver, equepmentText, "Add Description");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String desText = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().sendKeys(driver, pobj.descriptionArea, desText);
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Add Description");
			fc.utobj().actionImgOption(driver, equepmentText, "Modify Description");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String isTextPresent = fc.utobj().getElement(driver, pobj.descriptionArea).getText().trim();

			if (isTextPresent.equalsIgnoreCase(desText)) {
				// do nothing
			} else {
				fc.utobj().throwsException("was not able to add description");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Modify Description in Equipment Checklist with upload file At ActionImg:: Admin > Opener > Equipment Checklist", testCaseId = "TC_18_Modify_Description_Equipment")
	public void modifyDescriptionEquipmentActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminOpenerEquipmentChecklistPage pobj = new AdminOpenerEquipmentChecklistPage(driver);
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Add Equipment Checklist");
			// Add Equipment
			String equepmentText = addEquipmentChecklistUploadFile(driver, dataSet, config);

			fc.utobj().printTestStep("Add Description");

			fc.utobj().actionImgOption(driver, equepmentText, "Add Description");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String desText = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().sendKeys(driver, pobj.descriptionArea, desText);
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Modify The Description");

			fc.utobj().actionImgOption(driver, equepmentText, "Modify Description");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			desText = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().sendKeys(driver, pobj.descriptionArea, desText);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Modify Description");

			fc.utobj().actionImgOption(driver, equepmentText, "Modify Description");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String isTextPresent = fc.utobj().getElement(driver, pobj.descriptionArea).getText().trim();

			if (isTextPresent.equalsIgnoreCase(desText)) {
				// do nothing
			} else {
				fc.utobj().throwsException("was not able to Modify description");
			}
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Modify Equipment Checklist with upload file At Action Button:: Admin > Opener > Equipment Checklist", testCaseId = "TC_19_Modfiy_Equipment_Checklist01")
	public void modifyEquipmentChecklistActionButton() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminOpenerEquipmentChecklistPage pobj = new AdminOpenerEquipmentChecklistPage(driver);
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > Users > Supplier Details");
			fc.utobj().printTestStep("Add Supplier");

			AdminUsersSupplierDetailsAddSupplierTest addSupplierPage = new AdminUsersSupplierDetailsAddSupplierTest();
			String supplierName = fc.utobj().generateTestData(dataSet.get("supplierName"));
			String emailId = "openerautomation@staffex.com";
			addSupplierPage.addSupplier(driver, supplierName, config, emailId);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Responsible Department");
			fc.utobj().printTestStep("Add Responsible Department");

			AdminOpenerResponsibleDepartmentPageTest addDepartmentPage = new AdminOpenerResponsibleDepartmentPageTest();
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));
			addDepartmentPage.addDepartment(driver, responsibilityArea);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Groups");
			fc.utobj().printTestStep("Add Group");

			AdminOpenerManageGroupsPageTest addGroupsPage = new AdminOpenerManageGroupsPageTest();
			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			addGroupsPage.addGroups(driver, groupName);

			fc.utobj().printTestStep("Add Equipment Checklist");
			// Add Equipment
			String equepmentText = addEquipmentChecklistUploadFile(driver, dataSet, config);

			// modifyActionButton
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[.='" + equepmentText + "']//ancestor::tr/td/input"));

			fc.utobj().printTestStep("Modify The Equipment Checklist");
			fc.utobj().selectActionMenuItems(driver, "Modify");

			equepmentText = fc.utobj().generateTestData(dataSet.get("equepmentText"));
			fc.utobj().sendKeys(driver, pobj.equipmentTextArea, equepmentText);
			fc.utobj().sendKeys(driver, pobj.quantityTextBx, dataSet.get("quantity"));
			fc.utobj().selectDropDown(driver, pobj.supplierSelectDropDown, supplierName);

			fc.utobj().selectValFromMultiSelect(driver, pobj.responsibilitySelect, responsibilityArea);
			fc.utobj().selectValFromMultiSelect(driver, pobj.storeTypeSelect, storeType);

			fc.utobj().selectDropDown(driver, pobj.groupSelectDropDown, groupName);

			fc.utobj().selectDropDown(driver, pobj.franchiseAccessDropDown, dataSet.get("franchiseeAccess"));

			fc.utobj().selectDropDown(driver, pobj.priorityDropDown, dataSet.get("priority"));

			fc.utobj().selectDropDown(driver, pobj.criticalLevelDropDown, dataSet.get("criticalLevel"));

			String dependentOn = dataSet.get("dependentOnModify");
			String valueText = fc.utobj()
					.getElementByXpath(driver,
							".//select[@name='smEquipmentChecklist_0referenceParent']/option[contains(text () , '"
									+ dependentOn + "')]")
					.getAttribute("value");
			fc.utobj().moveToElement(driver, pobj.dependentOnDropDown);
			Select sl = new Select(pobj.dependentOnDropDown);
			sl.selectByValue(valueText);

			if (fc.utobj().isSelected(driver, pobj.webLinkRadioBtn)) {
				// do nothing
			} else {
				fc.utobj().clickElement(driver, pobj.webLinkRadioBtn);
			}

			fc.utobj().sendKeys(driver, pobj.weblinkTxtBx, dataSet.get("webLinkUrl"));

			try {
				fc.utobj().moveToElement(driver, fc.utobj().getElementByName(driver, "applyToAll"));
				List<WebElement> listRadioButton = pobj.applyToAll;
				if (fc.utobj().isSelected(driver,listRadioButton.get(0))) {
					// do nothing
				} else {
					fc.utobj().clickElement(driver, listRadioButton.get(0));
				}
			} catch (Exception e) {

			}
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("Verify The Modify Equipment Checklist");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, equepmentText);

			if (isTextPresent == false) {
				fc.utobj().throwsSkipException("was not able modify equipment checklist");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Delete Equipment Checklist with upload file At Action Button:: Admin > Opener > Equipment Checklist", testCaseId = "TC_20_Delete_Equipment_Checklist01")
	public void deleteEquipmentChecklistActionButton() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminOpenerEquipmentChecklistPage pobj = new AdminOpenerEquipmentChecklistPage(driver);
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Add Equipment Checklist");
			// Add Equipment
			String equepmentText = addEquipmentChecklistUploadFile(driver, dataSet, config);

			fc.utobj().printTestStep("Delete Equipment Checklist");
			// modifyActionButton
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[.='" + equepmentText + "']//ancestor::tr/td/input"));

			fc.utobj().selectActionMenuItems(driver, "Delete");

			fc.utobj().acceptAlertBox(driver);

			try {
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().moveToElement(driver, fc.utobj().getElementByName(driver, "applyToAll"));
				List<WebElement> listRadioButton = pobj.applyToAll;

				if (fc.utobj().isSelected(driver,listRadioButton.get(0))) {
					// do nothing
				} else {
					fc.utobj().clickElement(driver, listRadioButton.get(0));
				}

				fc.utobj().clickElement(driver, pobj.deleteFrameEquipmentBtn);
				fc.utobj().clickElement(driver, pobj.closeFrameEquipmentBtn);
				fc.utobj().switchFrameToDefault(driver);
			} catch (Exception e) {

			}

			fc.utobj().printTestStep("Verify The Delete Equipment Checklist");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, equepmentText);

			if (isTextPresent == true) {

				fc.utobj().throwsSkipException("was not able delete equipment checklist");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Modify Contact Equipment Checklist with upload file At Action Button:: Admin > Opener > Equipment Checklist", testCaseId = "TC_21_Modify_Contact_Equipment")
	public void modifyContactActionButton() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Enable Divisional Users");
			AdminHiddenLinksConfigureNewHierarchyLevelPageTest configure_level = new AdminHiddenLinksConfigureNewHierarchyLevelPageTest();
			configure_level.ConfigureNewHierarchyLevel(driver);

			AdminOpenerEquipmentChecklistPage pobj = new AdminOpenerEquipmentChecklistPage(driver);
			fc.utobj().printTestStep("Add Equipment Checklist");
			// Add Equipment
			String equepmentText = addEquipmentChecklistUploadFile(driver, dataSet, config);

			fc.utobj().printTestStep("Modify The Contact of Equipment Checklist");
			// modifyActionButton
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[.='" + equepmentText + "']//ancestor::tr/td/input"));

			fc.utobj().selectActionMenuItems(driver, "Modify Contact(s)");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.contactSelectModifyBtn);
			fc.utobj().sendKeys(driver, pobj.contactSelectModifyTxBx, "Division");
			fc.utobj().clickElement(driver, pobj.contactSelectModifySelectAll);
			fc.utobj().clickElement(driver, pobj.contactSelectModifyBtn);

			try {

				if (fc.utobj().isSelected(driver,pobj.applyToAll.get(0))) {
					// do nothing
				} else {
					fc.utobj().clickElement(driver, pobj.applyToAll.get(0));
				}

			} catch (Exception e) {

			}
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Modify Equipment Checklist");
			String isTextPresent = fc.utobj().getText(driver,
					fc.utobj().getElementByXpath(driver, ".//a[.='" + equepmentText + "']//ancestor::tr/td[7]"));

			if (isTextPresent.equalsIgnoreCase("Divisional Users")) {
				// do nothing
			} else {
				fc.utobj().throwsException("was not able to modify Contact");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Configure Default Value Equipment Checklist with upload file At Action Button:: Admin > Opener > Equipment Checklist", testCaseId = "TC_22_Configure_Default_Value_Equipment")
	public void configureDefaultValueActionButton() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminOpenerEquipmentChecklistPage pobj = new AdminOpenerEquipmentChecklistPage(driver);
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > Users > Supplier Details");
			fc.utobj().printTestStep("Add Supplier");

			AdminUsersSupplierDetailsAddSupplierTest addSupplierPage = new AdminUsersSupplierDetailsAddSupplierTest();
			String supplierName = fc.utobj().generateTestData(dataSet.get("supplierName"));
			String emailId = "openerautomation@staffex.com";
			addSupplierPage.addSupplier(driver, supplierName, config, emailId);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Responsible Department");
			fc.utobj().printTestStep("Add Responsible Department");

			AdminOpenerResponsibleDepartmentPageTest addDepartmentPage = new AdminOpenerResponsibleDepartmentPageTest();
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));
			addDepartmentPage.addDepartment(driver, responsibilityArea);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporatePage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Groups");
			fc.utobj().printTestStep("Add Group");

			AdminOpenerManageGroupsPageTest addGroupsPage = new AdminOpenerManageGroupsPageTest();
			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			addGroupsPage.addGroups(driver, groupName);

			fc.utobj().printTestStep("Configure Default Value");
			fc.opener().opener_common().adminFranchiseOpenerEquipmentChecklist(driver);

			fc.utobj().selectActionMenuItems(driver, "Configure Default Values");

			fc.utobj().sendKeys(driver, pobj.quantityTextBx, dataSet.get("quantity"));
			fc.utobj().selectDropDown(driver, pobj.supplierSelectDropDown, supplierName);

			fc.utobj().selectValFromMultiSelect(driver, pobj.responsibilitySelect, responsibilityArea);

			fc.utobj().selectValFromMultiSelect(driver, pobj.contactSelect, corpUser.getuserFullName());

			fc.utobj().selectValFromMultiSelect(driver, pobj.storeTypeSelect, storeType);

			fc.utobj().selectDropDown(driver, pobj.groupSelectDropDown, groupName);
			fc.utobj().selectDropDown(driver, pobj.franchiseAccessDropDown, dataSet.get("franchiseeAccess"));
			fc.utobj().selectDropDown(driver, pobj.priorityDropDown, dataSet.get("priority"));
			fc.utobj().selectDropDown(driver, pobj.criticalLevelDropDown, dataSet.get("criticalLevel"));
			String dependentOn = dataSet.get("dependentOn");
			String valueText = fc.utobj()
					.getElementByXpath(driver,
							".//select[@name='smEquipmentChecklist_0referenceParent']/option[contains(text () , '"
									+ dependentOn + "')]")
					.getAttribute("value");
			fc.utobj().moveToElement(driver, pobj.dependentOnDropDown);
			Select sl = new Select(pobj.dependentOnDropDown);
			sl.selectByValue(valueText);
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("Verify The Configure Default Value ");
			// verify
			String quantityText = fc.utobj().getElementByXpath(driver, ".//*[@id='smEquipmentChecklist_0quantity']")
					.getAttribute("value").trim();
			String textResponsibility = driver
					.findElement(By.xpath(".//*[@id='ms-parentsmEquipmentChecklist_0responsibilityArea']/button/span"))
					.getText().trim();
			String textContact = driver
					.findElement(By.xpath(".//*[@id='ms-parentsmEquipmentChecklist_0contact']/button/span")).getText()
					.trim();
			String textStoreType = driver
					.findElement(By.xpath(".//*[@id='ms-parentsmEquipmentChecklist_0storeTypeId']/button/span"))
					.getText().trim();

			if (textResponsibility.equalsIgnoreCase(responsibilityArea)
					&& textContact.contains(corpUser.getuserFullName()) && textStoreType.equalsIgnoreCase(storeType)
					&& quantityText.equalsIgnoreCase(dataSet.get("quantity"))) {
				// do nothing
			} else {
				fc.utobj().throwsException("was not able to Configure default value");
			}

			configureDefaultValue(driver, config);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void configureDefaultValue(WebDriver driver, Map<String, String> config) throws Exception {

		String testCaseId = "TC_Configure_Default_Equipment_Checklist_01";

		if (fc.utobj().configurationTestCaseToExecute(testCaseId)) {

			try {
				AdminOpenerEquipmentChecklistPage pobj = new AdminOpenerEquipmentChecklistPage(driver);
				fc.opener().opener_common().adminFranchiseOpenerEquipmentChecklist(driver);

				fc.utobj().selectActionMenuItems(driver, "Configure Default Values");
				fc.utobj().sendKeys(driver, pobj.quantityTextBx, "");
				fc.utobj().selectDropDown(driver, pobj.supplierSelectDropDown, "Select");

				fc.utobj().setToDefault(driver, pobj.responsibilitySelect);
				fc.utobj().setToDefault(driver, pobj.contactSelect);
				fc.utobj().setToDefault(driver, pobj.storeTypeSelect);

				fc.utobj().selectDropDown(driver, pobj.groupSelectDropDown, "Select");

				fc.utobj().selectDropDown(driver, pobj.franchiseAccessDropDown, "Select");
				fc.utobj().selectDropDown(driver, pobj.priorityDropDown, "Select");
				fc.utobj().selectDropDown(driver, pobj.criticalLevelDropDown, "Select");
				String dependentOn = "None (Timeless)";
				String valueText = fc.utobj()
						.getElementByXpath(driver,
								".//select[@name='smEquipmentChecklist_0referenceParent']/option[contains(text () , '"
										+ dependentOn + "')]")
						.getAttribute("value");
				fc.utobj().moveToElement(driver, pobj.dependentOnDropDown);
				Select sl = new Select(pobj.dependentOnDropDown);
				sl.selectByValue(valueText);
				fc.utobj().clickElement(driver, pobj.saveBtn);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Modify Equipment Checklist with upload file At Bottom:: Admin > Opener > Equipment Checklist", testCaseId = "TC_23_Modify_Equipment_Checklist02")
	public void modifyEquipmentChecklistBottom() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminOpenerEquipmentChecklistPage pobj = new AdminOpenerEquipmentChecklistPage(driver);
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > Users > Supplier Details");
			fc.utobj().printTestStep("Add Supplier");

			AdminUsersSupplierDetailsAddSupplierTest addSupplierPage = new AdminUsersSupplierDetailsAddSupplierTest();
			String supplierName = fc.utobj().generateTestData(dataSet.get("supplierName"));
			String emailId = "openerautomation@staffex.com";
			addSupplierPage.addSupplier(driver, supplierName, config, emailId);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Responsible Department");
			fc.utobj().printTestStep("Add Responsible Department");

			AdminOpenerResponsibleDepartmentPageTest addDepartmentPage = new AdminOpenerResponsibleDepartmentPageTest();
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));
			addDepartmentPage.addDepartment(driver, responsibilityArea);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Groups");
			fc.utobj().printTestStep("Add Group");

			AdminOpenerManageGroupsPageTest addGroupsPage = new AdminOpenerManageGroupsPageTest();
			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			addGroupsPage.addGroups(driver, groupName);

			fc.utobj().printTestStep("Add Equipment Checklist");
			// Add Equipment
			String equepmentText = addEquipmentChecklistUploadFile(driver, dataSet, config);

			fc.utobj().printTestStep("Modify The  Equipment Checklist By Bottom Button Option");
			// modifyBtn At Bottom
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[.='" + equepmentText + "']//ancestor::tr/td/input"));

			fc.utobj().clickElement(driver, pobj.modifyAtBottomBtn);

			equepmentText = fc.utobj().generateTestData(dataSet.get("equepmentText"));
			fc.utobj().sendKeys(driver, pobj.equipmentTextArea, equepmentText);
			fc.utobj().sendKeys(driver, pobj.quantityTextBx, dataSet.get("quantity"));
			fc.utobj().selectDropDown(driver, pobj.supplierSelectDropDown, supplierName);

			fc.utobj().selectValFromMultiSelect(driver, pobj.responsibilitySelect, responsibilityArea);
			fc.utobj().selectValFromMultiSelect(driver, pobj.storeTypeSelect, storeType);
			fc.utobj().selectDropDown(driver, pobj.groupSelectDropDown, groupName);
			fc.utobj().selectDropDown(driver, pobj.franchiseAccessDropDown, dataSet.get("franchiseeAccess"));
			fc.utobj().selectDropDown(driver, pobj.priorityDropDown, dataSet.get("priority"));
			fc.utobj().selectDropDown(driver, pobj.criticalLevelDropDown, dataSet.get("criticalLevel"));

			String dependentOn = dataSet.get("dependentOnModify");
			String valueText = fc.utobj()
					.getElementByXpath(driver,
							".//select[@name='smEquipmentChecklist_0referenceParent']/option[contains(text () , '"
									+ dependentOn + "')]")
					.getAttribute("value");
			fc.utobj().moveToElement(driver, pobj.dependentOnDropDown);
			Select sl = new Select(pobj.dependentOnDropDown);
			sl.selectByValue(valueText);

			if (fc.utobj().isSelected(driver, pobj.webLinkRadioBtn)) {
				// do nothing
			} else {
				fc.utobj().clickElement(driver, pobj.webLinkRadioBtn);
			}

			fc.utobj().sendKeys(driver, pobj.weblinkTxtBx, dataSet.get("webLinkUrl"));

			try {
				fc.utobj().moveToElement(driver, fc.utobj().getElementByName(driver, "applyToAll"));
				List<WebElement> listRadioButton = pobj.applyToAll;
				if (fc.utobj().isSelected(driver,listRadioButton.get(0))) {
					// do nothing
				} else {
					fc.utobj().clickElement(driver, listRadioButton.get(0));
				}
			} catch (Exception e) {

			}
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("Verify The Modify Equipment Checklist");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, equepmentText);

			if (isTextPresent == false) {
				fc.utobj().throwsSkipException("was not able modify equipment checklist");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Delete Equipment Checklist with upload file At Bottom:: Admin > Opener > Equipment Checklist", testCaseId = "TC_24_Delete_Equipment_Checklist02")
	public void deleteEquipmentChecklistBottom() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminOpenerEquipmentChecklistPage pobj = new AdminOpenerEquipmentChecklistPage(driver);
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Add Equipment Checklist");
			// Add Equipment
			String equepmentText = addEquipmentChecklistUploadFile(driver, dataSet, config);

			fc.utobj().printTestStep("Delete Equipment Checklist");
			// modifyActionButton
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[.='" + equepmentText + "']//ancestor::tr/td/input"));

			fc.utobj().clickElement(driver, pobj.deleteAtBottomBtn);
			fc.utobj().acceptAlertBox(driver);

			try {
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().moveToElement(driver, fc.utobj().getElementByName(driver, "applyToAll"));
				List<WebElement> listRadioButton = pobj.applyToAll;

				if (fc.utobj().isSelected(driver,listRadioButton.get(0))) {
					// do nothing
				} else {
					fc.utobj().clickElement(driver, listRadioButton.get(0));
				}

				fc.utobj().clickElement(driver, pobj.deleteFrameEquipmentBtn);
				fc.utobj().clickElement(driver, pobj.closeFrameEquipmentBtn);
				fc.utobj().switchFrameToDefault(driver);
			} catch (Exception e) {

			}

			fc.utobj().printTestStep("Verify The Delete Equipment Checklist");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, equepmentText);

			if (isTextPresent == true) {

				fc.utobj().throwsSkipException("was not able delete equipment checklist");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
}
