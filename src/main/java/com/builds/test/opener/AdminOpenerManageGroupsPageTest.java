package com.builds.test.opener;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import com.builds.uimaps.opener.AdminOpenerManageGroupsPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminOpenerManageGroupsPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "opener" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Add Groups :: Admin > Opener > Manage Groups", testCaseId = "TC_80_Add_Groups")
	public void addGroup() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Groups");
			fc.utobj().printTestStep("Add Groups");
			fc.opener().opener_common().adminFranchiseOpenerManageGroups(driver);
			AdminOpenerManageGroupsPage pobj = new AdminOpenerManageGroupsPage(driver);

			fc.utobj().clickElement(driver, pobj.addGroupsBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			fc.utobj().sendKeys(driver, pobj.groupNameBtn, groupName);
			fc.utobj().clickElement(driver, pobj.addBtn);

			fc.utobj().clickElement(driver, pobj.okBtn);

			fc.utobj().printTestStep("Verify The Add Groups");
			boolean isTextPresent = fc.utobj().inSelectBox(driver, pobj.listing, groupName);

			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to add groups");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "opener" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Modify Groups :: Admin > Opener > Manage Groups", testCaseId = "TC_81_Modify_Groups")
	public void modifyGroups() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminOpenerManageGroupsPage pobj = new AdminOpenerManageGroupsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Groups");
			fc.utobj().printTestStep("Add Group");

			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			addGroups(driver, groupName);

			String value = fc.utobj().getTextTxtFld(driver, fc.utobj().getElementByXpath(driver,
					".//select[@name='groupsOrder']/option[contains(text () , '" + groupName + "')]"));
			Select sl = new Select(pobj.listing);
			sl.selectByValue(value);

			fc.utobj().printTestStep("Modify The Groups");
			fc.utobj().clickElement(driver, pobj.modifyBtn);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			fc.utobj().sendKeys(driver, pobj.groupNameBtn, groupName);
			fc.utobj().clickElement(driver, pobj.modifyBtn);

			fc.utobj().clickElement(driver, pobj.okBtn);

			fc.utobj().printTestStep("Verify The Modify Groups");
			boolean isTextPresent = fc.utobj().inSelectBox(driver, pobj.listing, groupName);

			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to modify groups");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "opener" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Delete Groups :: Admin > Opener > Manage Groups", testCaseId = "TC_82_Delete_Groups")
	public void deleteGroups() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminOpenerManageGroupsPage pobj = new AdminOpenerManageGroupsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Groups");
			fc.utobj().printTestStep("Add Group");

			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			addGroups(driver, groupName);

			fc.utobj().printTestStep("Delete The Group");
			String value = fc.utobj().getTextTxtFld(driver, fc.utobj().getElementByXpath(driver,
					".//select[@name='groupsOrder']/option[contains(text () , '" + groupName + "')]"));
			Select sl = new Select(pobj.listing);
			sl.selectByValue(value);
			fc.utobj().clickElement(driver, pobj.deleteBtn);
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Delete Groups");
			boolean isTextPresent = fc.utobj().inSelectBox(driver, pobj.listing, groupName);

			if (isTextPresent == true) {
				fc.utobj().throwsException("was not able to delete groups");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void addGroups(WebDriver driver, String groupName) throws Exception {

		String testCaseId = "TC_Add_Groups_Opener_Admin";

		if (fc.utobj().validate(testCaseId)) {
			try {
				fc.opener().opener_common().adminFranchiseOpenerManageGroups(driver);

				AdminOpenerManageGroupsPage pobj = new AdminOpenerManageGroupsPage(driver);

				fc.utobj().clickElement(driver, pobj.addGroupsBtn);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().sendKeys(driver, pobj.groupNameBtn, groupName);
				fc.utobj().clickElement(driver, pobj.addBtn);

				fc.utobj().clickElement(driver, pobj.okBtn);

				fc.utobj().switchFrameToDefault(driver);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to add Group Opener");

		}

	}

}