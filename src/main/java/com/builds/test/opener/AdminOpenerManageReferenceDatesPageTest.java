package com.builds.test.opener;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.uimaps.opener.AdminOpenerManageReferenceDatesPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminOpenerManageReferenceDatesPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Add Reference Dates:: Admin > Opener > Manage Reference Dates", testCaseId = "TC_74_Add_Reference_Date")
	public void addReferenceDate() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			String referenceDate = dataSet.get("referenceDate");
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Add Refrences Date");
			fc.opener().opener_common().adminFranchiseOpenerManageReferenceDates(driver);
			AdminOpenerManageReferenceDatesPage pobj = new AdminOpenerManageReferenceDatesPage(driver);

			fc.utobj().clickElement(driver, pobj.addReferenceDateBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String testData = fc.utobj().generateTestData(referenceDate);
			fc.utobj().sendKeys(driver, pobj.AddReferenceDateTxtBox, testData);
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);

			fc.utobj().printTestStep("Verify The Add Reference Date");
			boolean isValPresent = fc.utobj().assertPageSource(driver, testData);

			if (isValPresent == false) {
				fc.utobj().throwsException("Was not able to add Reference Date!!!!!!!!!!!");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void addReferenceDate(WebDriver driver, String referenceDate) throws Exception {

		String testCaseId = "TC_Add_Reference_Date_Opener";

		if (fc.utobj().validate(testCaseId)) {
			try {
				fc.opener().opener_common().adminFranchiseOpenerManageReferenceDates(driver);
				AdminOpenerManageReferenceDatesPage pobj = new AdminOpenerManageReferenceDatesPage(driver);

				fc.utobj().clickElement(driver, pobj.addReferenceDateBtn);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().sendKeys(driver, pobj.AddReferenceDateTxtBox, referenceDate);
				fc.utobj().clickElement(driver, pobj.addBtn);
				fc.utobj().clickElement(driver, pobj.closeBtn);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to Add References At References Page");

		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Modify Reference Dates:: Admin > Opener > Manage Reference Dates", testCaseId = "TC_75_modify_Reference_Date")
	public void modifyReferenceDate() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Add Reference Date");
			AdminOpenerManageReferenceDatesPage pobj = new AdminOpenerManageReferenceDatesPage(driver);
			String referenceDate = fc.utobj().generateTestData(dataSet.get("referenceDate"));
			addReferenceDate(driver, referenceDate);

			fc.utobj().printTestStep("Modify The References Date");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[contains(text () , '"
					+ referenceDate + "')]/ancestor::tr[@class='groupItem']//img[@title='Edit Reference Date']"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.AddReferenceDateTxtBox, referenceDate);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);

			fc.utobj().printTestStep("Verify The Modify References Date");
			boolean isValPresent = fc.utobj().assertPageSource(driver, referenceDate);

			if (isValPresent == false) {
				fc.utobj().throwsException("Was not able to modify Reference Date!!!!!!!!!!!");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "opener" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Delete Reference Dates:: Admin > Opener > Manage Reference Dates", testCaseId = "TC_76_delete_Reference_Date")
	public void deleteReferenceDate() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Add References Date");
			String referenceDate = fc.utobj().generateTestData(dataSet.get("referenceDate"));
			addReferenceDate(driver, referenceDate);

			fc.utobj().printTestStep("Delete References Date");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[contains(text () , '"
					+ referenceDate + "')]/ancestor::tr[@class='groupItem']//img[@title='Delete Reference Date']"));
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Delete References Date");
			boolean isValPresent = fc.utobj().assertPageSource(driver, referenceDate);

			if (isValPresent == true) {
				fc.utobj().throwsException("Was not able to delete Reference Date!!!!!!!!!!!");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

}
