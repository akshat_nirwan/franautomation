package com.builds.test.opener;

import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.uimaps.opener.AdminOpenerMilestoneDateTriggersPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminOpenerMilestoneDateTriggersPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "opener" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Add Milestone Date Triggers :: Admin > Opener > Milestone Date Triggers", testCaseId = "TC_88_Add_Milestone_Date_Triggers")
	public void addMilestoneDateTriggers() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminOpenerMilestoneDateTriggersPage pobj = new AdminOpenerMilestoneDateTriggersPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Task Checklist");
			fc.utobj().printTestStep("Add Task Checklist");
			AdminOpenerTaskChecklistPageTest taskChecklistPage = new AdminOpenerTaskChecklistPageTest();
			String task = taskChecklistPage.addTaskChecklistUploadFile(driver, dataSet);

			fc.utobj().printTestStep("Add MileStone Date Trigger");
			fc.opener().opener_common().adminFranchiseOpenerConfigureMilestoneDateTriggers(driver);
			fc.utobj().clickElement(driver, pobj.addMilestoneDateTriggerLnk);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String triggerName = fc.utobj().generateTestData(dataSet.get("triggerName"));
			fc.utobj().sendKeys(driver, pobj.triggerName, triggerName);

			fc.utobj().selectDropDown(driver, pobj.milestoneDateId, "Expected Opening Date");
			fc.utobj().sendKeys(driver, pobj.scheduleDate, dataSet.get("scheduleDate"));
			fc.utobj().selectDropDown(driver, pobj.scheduleFlag, dataSet.get("scheduleFlag"));

			fc.utobj().clickElement(driver, pobj.actualCompletionBtn);
			fc.utobj().sendKeys(driver, pobj.actualCompletionTxBx, task);
			fc.utobj().clickElement(driver, pobj.actualCompletionSelectAll);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Add MileStone Date Trigger");
			if (fc.utobj().getElementByXpath(driver, ".//td[contains(text () ,'" + triggerName + "')]").isDisplayed()
					&& fc.utobj().getElementByXpath(driver, ".//td[contains(text () ,'" + task + "')]").isDisplayed()) {
				// do nothing
			} else {
				fc.utobj().throwsException("was not able to Configure MileStone Trigger");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addMilestoneDateTriggers(WebDriver driver, Map<String, String> dataSet) throws Exception {

		String testCaseId = "TC_Add_Milestone_Date_Triggers_Opner";
		String triggerName = null;
		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminOpenerMilestoneDateTriggersPage pobj = new AdminOpenerMilestoneDateTriggersPage(driver);
				AdminOpenerTaskChecklistPageTest taskChecklistPage = new AdminOpenerTaskChecklistPageTest();
				String task = taskChecklistPage.addTaskChecklistUploadFile(driver, dataSet);

				fc.opener().opener_common().adminFranchiseOpenerConfigureMilestoneDateTriggers(driver);
				fc.utobj().clickElement(driver, pobj.addMilestoneDateTriggerLnk);

				fc.commonMethods().switch_cboxIframe_frameId(driver);
				triggerName = fc.utobj().generateTestData(dataSet.get("triggerName"));
				fc.utobj().sendKeys(driver, pobj.triggerName, triggerName);

				fc.utobj().selectDropDown(driver, pobj.milestoneDateId, "Expected Opening Date");
				fc.utobj().sendKeys(driver, pobj.scheduleDate, dataSet.get("scheduleDate"));
				fc.utobj().selectDropDown(driver, pobj.scheduleFlag, dataSet.get("scheduleFlag"));

				fc.utobj().clickElement(driver, pobj.actualCompletionBtn);
				fc.utobj().sendKeys(driver, pobj.actualCompletionTxBx, task);
				fc.utobj().clickElement(driver, pobj.actualCompletionSelectAll);
				fc.utobj().clickElement(driver, pobj.saveBtn);
				fc.utobj().switchFrameToDefault(driver);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to Add Mile Stone Date Triggers");

		}
		return triggerName;
	}

	@Test(groups = { "opener" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Modify Milestone Date Triggers :: Admin > Opener > Milestone Date Triggers", testCaseId = "TC_89_Modify_Milestone_Date_Triggers")
	public void modifyMilestoneDateTriggers() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminOpenerMilestoneDateTriggersPage pobj = new AdminOpenerMilestoneDateTriggersPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Configure Milestone Date Triggers");
			fc.utobj().printTestStep("Add MileStone Dtae Triggers");
			String triggerName = addMilestoneDateTriggers(driver, dataSet);

			fc.utobj().printTestStep("Add TaskChecklist");
			AdminOpenerTaskChecklistPageTest taskChecklistPage = new AdminOpenerTaskChecklistPageTest();
			String task = taskChecklistPage.addTaskChecklistUploadFile(driver, dataSet);

			fc.opener().opener_common().adminFranchiseOpenerConfigureMilestoneDateTriggers(driver);

			fc.utobj().printTestStep("Modify the MileStone Date Triggers");
			String alterText = driver
					.findElement(By.xpath(
							".//td[contains(text() , '" + triggerName + "')]/ancestor::tr/td/div[@id='menuBar']/layer"))
					.getAttribute("id").trim();
			alterText = alterText.replace("Actions_dynamicmenu", "");
			alterText = alterText.replace("Bar", "");

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//td[contains(text() , '" + triggerName + "')]/ancestor::tr/td/div[@id='menuBar']/layer/a/img"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//div[@id='Actions_dynamicmenu" + alterText + "Menu']/span[contains(text () ,'Modify')]"));

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			triggerName = fc.utobj().generateTestData(dataSet.get("triggerName"));
			fc.utobj().sendKeys(driver, pobj.triggerName, triggerName);

			fc.utobj().selectDropDown(driver, pobj.milestoneDateId, "Expected Opening Date");
			fc.utobj().sendKeys(driver, pobj.scheduleDate, dataSet.get("scheduleDate"));
			fc.utobj().selectDropDown(driver, pobj.scheduleFlag, dataSet.get("scheduleFlag"));

			fc.utobj().setToDefault(driver, pobj.actualCompletionBtn1);
			fc.utobj().clickElement(driver, pobj.actualCompletionBtn);
			fc.utobj().sendKeys(driver, pobj.actualCompletionTxBx, task);
			fc.utobj().clickElement(driver, pobj.actualCompletionSelectAll);

			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The MileStone Date Triggers");
			if (fc.utobj().getElementByXpath(driver, ".//td[contains(text () ,'" + triggerName + "')]").isDisplayed()
					&& fc.utobj().getElementByXpath(driver, ".//td[contains(text () ,'" + task + "')]").isDisplayed()) {
			} else {
				fc.utobj().throwsException("was not able to modify MileStone Trigger");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "opener" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Delete Milestone Date Triggers :: Admin > Opener > Milestone Date Triggers", testCaseId = "TC_90_Delete_MileStone_Date_Triggers")
	public void deleteMilestoneDateTriggers() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Add MileStone Date Trigger");
			String triggerName = addMilestoneDateTriggers(driver, dataSet);

			AdminOpenerTaskChecklistPageTest taskChecklistPage = new AdminOpenerTaskChecklistPageTest();
			taskChecklistPage.addTaskChecklistUploadFile(driver, dataSet);

			fc.opener().opener_common().adminFranchiseOpenerConfigureMilestoneDateTriggers(driver);

			fc.utobj().printTestStep("Delete The MileStone Date Triggers");
			// modify Trigger
			String alterText = driver
					.findElement(By.xpath(
							".//td[contains(text() , '" + triggerName + "')]/ancestor::tr/td/div[@id='menuBar']/layer"))
					.getAttribute("id").trim();
			alterText = alterText.replace("Actions_dynamicmenu", "");
			alterText = alterText.replace("Bar", "");

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//td[contains(text() , '" + triggerName + "')]/ancestor::tr/td/div[@id='menuBar']/layer/a/img"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//div[@id='Actions_dynamicmenu" + alterText + "Menu']/span[contains(text () ,'Delete')]"));
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Delete MileStone Date Triggers");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, triggerName);

			if (isTextPresent == true) {
				fc.utobj().throwsException("was not able to delete Milestone Date Triggers");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
}
