package com.builds.test.opener;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.uimaps.opener.AdminOpenerModifyAlertEmailContentPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminOpenerModifyAlertEmailContentPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Alert Email Content:: Admin > Opener > Add Alert Email Content", testCaseId = "TC_70_Add_Alert_Email_Content")
	public void addAlertEmailContent() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminOpenerModifyAlertEmailContentPage pobj = new AdminOpenerModifyAlertEmailContentPage(driver);
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Alert Email Content");
			fc.utobj().printTestStep("Add Alert Email Content");
			fc.opener().opener_common().adminFranchiseOpenerAlertEmailContent(driver);

			String fromEmail = dataSet.get("fromEmail");
			fc.utobj().sendKeys(driver, pobj.fromEmailTextBox, fromEmail);
			fc.utobj().sendKeys(driver, pobj.scheduleStartReminderEmailContentTextBox,
					"Schedule Start Reminder Email Content");
			fc.utobj().sendKeys(driver, pobj.scheduleStartAlertEmailContentTextBox,
					"Schedule Start Alert Email Content");
			fc.utobj().sendKeys(driver, pobj.scheduleCompletionReminderEmailContentTextBox,
					"Schedule Completion Reminder Email Content");
			fc.utobj().sendKeys(driver, pobj.scheduleCompletionAlertEmailContentTextBox,
					"Schedule Completion Alert Email Content");
			fc.utobj().sendKeys(driver, pobj.overdueAlertEmailContentTextBox, "Overdue Alert Email Content");
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("Verify The Add Alert Email Content");
			fc.opener().opener_common().adminFranchiseOpenerAlertEmailContent(driver);
			String isTextPresent = fc.utobj().getTextTxtFld(driver, pobj.fromEmailTextBox);

			if (isTextPresent.equalsIgnoreCase(fromEmail)) {
				// do nothing
			} else {
				fc.utobj().throwsException("was not able to add Alert Email Content");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}

	public void addAlertEmailContent(WebDriver driver, String fromEmail, Map<String, String> config) throws Exception {

		String testCaseId = "TC_Add_Alert_Email_Content_Opener";

		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminOpenerModifyAlertEmailContentPage pobj = new AdminOpenerModifyAlertEmailContentPage(driver);

				fc.opener().opener_common().adminFranchiseOpenerAlertEmailContent(driver);

				fc.utobj().sendKeys(driver, pobj.fromEmailTextBox, fromEmail);
				fc.utobj().sendKeys(driver, pobj.scheduleStartReminderEmailContentTextBox,
						"Schedule Start Reminder Email Content");
				fc.utobj().sendKeys(driver, pobj.scheduleStartAlertEmailContentTextBox,
						"Schedule Start Alert Email Content");
				fc.utobj().sendKeys(driver, pobj.scheduleCompletionReminderEmailContentTextBox,
						"Schedule Completion Reminder Email Content");
				fc.utobj().sendKeys(driver, pobj.scheduleCompletionAlertEmailContentTextBox,
						"Schedule Completion Alert Email Content");
				fc.utobj().sendKeys(driver, pobj.overdueAlertEmailContentTextBox, "Overdue Alert Email Content");
				fc.utobj().clickElement(driver, pobj.saveBtn);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to Add Alert Email Containt Type");

		}

	}

}
