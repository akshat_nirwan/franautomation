package com.builds.test.opener;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.uimaps.opener.AdminOpenerOverdueAlertFrequencyPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminOpenerOverdueAlertFrequencyPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Set Overdue Alert Email Frequency:: Admin > Opener > Overdue Alert Frequency", testCaseId = "TC_69_Set_Overdue_Alert_Email_Frequency")
	public void setAlertEmailFrequency() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminOpenerOverdueAlertFrequencyPage pobj = new AdminOpenerOverdueAlertFrequencyPage(driver);

			fc.utobj().printTestStep("Set Overdue Alert Email Frequency");
			fc.opener().opener_common().adminFranchiseOpenerOverdueAlertFrequency(driver);

			String overdueAlert = dataSet.get("overdueAlert");
			fc.utobj().sendKeys(driver, pobj.overdueAlertFrequencyTextBox, overdueAlert);
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.opener().opener_common().adminFranchiseOpenerOverdueAlertFrequency(driver);

			fc.utobj().printTestStep("Verify The Set Overdue Alert Email Frequency");
			String isTextPresent = fc.utobj().getTextTxtFld(driver, pobj.overdueAlertFrequencyTextBox);
			if (isTextPresent.equalsIgnoreCase(overdueAlert)) {
				// do nothing
			} else {
				fc.utobj().throwsException("was not able to add overdule Alert Email Frequency");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void setAlertEmailFrequency(WebDriver driver, String overdueAlert, Map<String, String> config)
			throws Exception {

		String testCaseId = "TC_Set_Alert_Email_Frequency_Opener_Admin";

		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminOpenerOverdueAlertFrequencyPage pobj = new AdminOpenerOverdueAlertFrequencyPage(driver);
				fc.opener().opener_common().adminFranchiseOpenerOverdueAlertFrequency(driver);
				fc.utobj().sendKeys(driver, pobj.overdueAlertFrequencyTextBox, overdueAlert);
				fc.utobj().clickElement(driver, pobj.addBtn);
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to Send Aler Email Frequency");

		}
	}
}
