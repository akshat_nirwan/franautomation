package com.builds.test.opener;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminConfigurationConfigureStoreTypePageTest;
import com.builds.test.admin.AdminHiddenLinksConfigureNewHierarchyLevelPageTest;
import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.common.CorporateUser;
import com.builds.uimaps.opener.AdminOpenerPictureChecklistPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminOpenerPictureChecklistPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "opener", "openeremail" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-29", testCaseDescription = "Add Picture Check List  with upload file option:: Admin > Opener > Picture Checklist", testCaseId = "TC_37_Add_Picture_Checklist_01")
	public void addPictureChecklistUploadFile() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminOpenerPictureChecklistPage pobj = new AdminOpenerPictureChecklistPage(driver);
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Responsible Department");
			fc.utobj().printTestStep("Add Responsible Department");

			AdminOpenerResponsibleDepartmentPageTest addDepartmentPage = new AdminOpenerResponsibleDepartmentPageTest();
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));
			addDepartmentPage.addDepartment(driver, responsibilityArea);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			String emailId = "openerautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporatePage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Groups");
			fc.utobj().printTestStep("Add Group");

			AdminOpenerManageGroupsPageTest addGroupsPage = new AdminOpenerManageGroupsPageTest();
			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			addGroupsPage.addGroups(driver, groupName);

			// configureDefaultValue(driver, config);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Picture Checklist");
			fc.utobj().printTestStep("Add Picture Checklist");
			fc.opener().opener_common().adminFranchiseOpenerPictureChecklist(driver);

			fc.utobj().clickElement(driver, pobj.addMoreBtn);

			String pictureText = fc.utobj().generateTestData(dataSet.get("pictureText"));
			fc.utobj().sendKeys(driver, pobj.pictureTitleTxBx, pictureText);

			fc.utobj().selectValFromMultiSelect(driver, pobj.selectResponsibility, responsibilityArea);

			fc.utobj().selectValFromMultiSelect(driver, pobj.selectContact, corpUser.getuserFullName());

			fc.utobj().selectValFromMultiSelect(driver, pobj.selectStoreType, storeType);

			fc.utobj().selectDropDown(driver, pobj.groupDropDown, groupName);
			fc.utobj().selectDropDown(driver, pobj.franchiseeAccessDropDown, dataSet.get("franchiseeAccess"));
			fc.utobj().selectDropDown(driver, pobj.priorityDropDown, dataSet.get("priority"));
			fc.utobj().selectDropDown(driver, pobj.criticalLevelDropDown, dataSet.get("criticalLevel"));

			String dependentOn = dataSet.get("dependentOn");
			String valueText = fc.utobj()
					.getElementByXpath(driver,
							".//select[@name='smPictureChecklist_0referenceParent']/option[contains(text () , '"
									+ dependentOn + "')]")
					.getAttribute("value");
			fc.utobj().moveToElement(driver, pobj.dependentOnDropDown);
			Select sl = new Select(pobj.dependentOnDropDown);
			sl.selectByValue(valueText);

			fc.utobj().sendKeys(driver, pobj.startScheduleDateTxtBox, "1");
			fc.utobj().selectDropDown(driver, pobj.scheduleStartDropDown, "Days Prior");
			fc.utobj().sendKeys(driver, pobj.completionScheduleDateTxtBox, "1");
			fc.utobj().selectDropDown(driver, pobj.scheduleCompletionDropDown, "Days Prior");

			fc.utobj().sendKeys(driver, pobj.startReminderTxtBox, "2");
			fc.utobj().sendKeys(driver, pobj.completionReminderTxtBox, "2");

			if (fc.utobj().isSelected(driver, pobj.uploadFileRadioBtn)) {
				// do nothing
			} else {
				fc.utobj().clickElement(driver, pobj.uploadFileRadioBtn);
			}

			String file = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));

			System.out.println(file);
			fc.utobj().sendKeys(driver, pobj.attachmentBrowseBox, file);
			fc.utobj().clickElement(driver, pobj.addBtn);

			try {

				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().clickElement(driver, pobj.noBtn);
				fc.utobj().switchFrameToDefault(driver);
			} catch (Exception e) {

			}

			fc.utobj().printTestStep("Verify The Add Checklist");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, pictureText);

			if (isTextPresent == false) {
				fc.utobj().throwsSkipException("was not able to add Picture checklist");
			}

			fc.utobj().printTestStep("Verify The Mail After Picture Check List Creation");

			Map<String, String> mailData = new HashMap<String, String>();

			mailData = fc.utobj().readMailBox("Checklist Item Creation Alert", pictureText, emailId, "sdg@1a@Hfs");

			if (mailData.size() == 0 || !mailData.get("mailBody").contains("New Item Created")) {
				fc.utobj().throwsException("was not able to verify Picture checklist creation mail");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addPictureChecklist(WebDriver driver, Map<String, String> dataSet, Map<String, String> config)
			throws Exception {

		String testCaseId = "TC_Add_Picture_Checklist_Admin_Opener";
		String pictureText = null;
		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminOpenerPictureChecklistPage pobj = new AdminOpenerPictureChecklistPage(driver);

				AdminOpenerResponsibleDepartmentPageTest addDepartmentPage = new AdminOpenerResponsibleDepartmentPageTest();
				String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));
				addDepartmentPage.addDepartment(driver, responsibilityArea);

				AdminUsersManageCorporateUsersAddCorporateUserPageTest corporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

				String emailId = "openerautomation@staffex.com";
				CorporateUser corpUser = new CorporateUser();
				corpUser.setEmail(emailId);
				corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
				corporatePage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

				AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
				String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
				storeTypePage.addStoreType(driver, storeType);

				fc.opener().opener_common().adminFranchiseOpenerPictureChecklist(driver);

				fc.utobj().clickElement(driver, pobj.addMoreBtn);

				pictureText = fc.utobj().generateTestData(dataSet.get("pictureText"));
				fc.utobj().sendKeys(driver, pobj.pictureTitleTxBx, pictureText);

				fc.utobj().selectValFromMultiSelect(driver, pobj.selectResponsibility, responsibilityArea);

				fc.utobj().selectValFromMultiSelect(driver, pobj.selectContact, corpUser.getuserFullName());

				fc.utobj().selectValFromMultiSelect(driver, pobj.selectStoreType, storeType);

				fc.utobj().selectDropDown(driver, pobj.franchiseeAccessDropDown, dataSet.get("franchiseeAccess"));
				fc.utobj().selectDropDown(driver, pobj.priorityDropDown, dataSet.get("priority"));
				fc.utobj().selectDropDown(driver, pobj.criticalLevelDropDown, dataSet.get("criticalLevel"));

				String dependentOn = dataSet.get("dependentOn");
				String valueText = fc.utobj()
						.getElementByXpath(driver,
								".//select[@name='smPictureChecklist_0referenceParent']/option[contains(text () , '"
										+ dependentOn + "')]")
						.getAttribute("value");
				fc.utobj().moveToElement(driver, pobj.dependentOnDropDown);
				Select sl = new Select(pobj.dependentOnDropDown);
				sl.selectByValue(valueText);

				if (dependentOn.equalsIgnoreCase("None (Timeless)")) {
					// do nothing
				} else if (dependentOn.equalsIgnoreCase("Expected Opening Date")) {

					fc.utobj().sendKeys(driver, pobj.startScheduleDateTxtBox, "1");
					fc.utobj().selectDropDown(driver, pobj.scheduleStartDropDown, "Days Prior");
					fc.utobj().sendKeys(driver, pobj.completionScheduleDateTxtBox, "1");
					fc.utobj().selectDropDown(driver, pobj.scheduleCompletionDropDown, "Days Prior");

					fc.utobj().sendKeys(driver, pobj.startReminderTxtBox, "2");
					fc.utobj().sendKeys(driver, pobj.completionReminderTxtBox, "2");

				} else if (dependentOn.equalsIgnoreCase("Multiple Checklists")) {

					fc.utobj().clickElement(driver, pobj.dependentOnDropDownBtn);

					fc.utobj().clickElement(driver, pobj.dependentOnDropDownselectAll);

					fc.utobj().sendKeys(driver, pobj.startScheduleDateTxtBox, "1");
					fc.utobj().selectDropDown(driver, pobj.scheduleStartDropDown, "Days Prior");
					fc.utobj().sendKeys(driver, pobj.completionScheduleDateTxtBox, "1");
					fc.utobj().selectDropDown(driver, pobj.scheduleCompletionDropDown, "Days Prior");
					fc.utobj().sendKeys(driver, pobj.startReminderTxtBox, "2");
					fc.utobj().sendKeys(driver, pobj.completionReminderTxtBox, "2");

				}

				if (fc.utobj().isSelected(driver, pobj.uploadFileRadioBtn)) {
					// do nothing
				} else {
					fc.utobj().clickElement(driver, pobj.uploadFileRadioBtn);
				}

				String file = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));

				System.out.println(file);
				fc.utobj().sendKeys(driver, pobj.attachmentBrowseBox, file);
				fc.utobj().clickElement(driver, pobj.addBtn);

				try {
					fc.commonMethods().switch_cboxIframe_frameId(driver);
					fc.utobj().clickElement(driver, pobj.noBtn);
					fc.utobj().switchFrameToDefault(driver);
				} catch (Exception e) {

				}

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to add Picture Checklist");

		}
		return pictureText;
	}

	public String addPictureChecklist(WebDriver driver, Map<String, String> dataSet, String responsibilityArea,
			String userName, String storeType, File file, Map<String, String> config) throws Exception {

		String testCaseId = "TC_Add_Picture_Checklist_Admin_Opener_02";
		String pictureText = null;
		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminOpenerPictureChecklistPage pobj = new AdminOpenerPictureChecklistPage(driver);
				AdminOpenerResponsibleDepartmentPageTest addDepartmentPage = new AdminOpenerResponsibleDepartmentPageTest();
				addDepartmentPage.addDepartment(driver, responsibilityArea);

				fc.opener().opener_common().adminFranchiseOpenerPictureChecklist(driver);

				fc.utobj().clickElement(driver, pobj.addMoreBtn);

				pictureText = fc.utobj().generateTestData(dataSet.get("pictureText"));
				fc.utobj().sendKeys(driver, pobj.pictureTitleTxBx, pictureText);

				fc.utobj().selectValFromMultiSelect(driver, pobj.selectResponsibility, responsibilityArea);

				fc.utobj().selectValFromMultiSelect(driver, pobj.selectContact, userName);

				fc.utobj().selectValFromMultiSelect(driver, pobj.selectStoreType, storeType);

				fc.utobj().selectDropDown(driver, pobj.franchiseeAccessDropDown, dataSet.get("franchiseeAccess"));
				fc.utobj().selectDropDown(driver, pobj.priorityDropDown, dataSet.get("priority"));
				fc.utobj().selectDropDown(driver, pobj.criticalLevelDropDown, dataSet.get("criticalLevel"));

				String dependentOn = dataSet.get("dependentOn");
				String valueText = fc.utobj()
						.getElementByXpath(driver,
								".//select[@name='smPictureChecklist_0referenceParent']/option[contains(text () , '"
										+ dependentOn + "')]")
						.getAttribute("value");
				fc.utobj().moveToElement(driver, pobj.dependentOnDropDown);
				Select sl = new Select(pobj.dependentOnDropDown);
				sl.selectByValue(valueText);

				if (dependentOn.equalsIgnoreCase("None (Timeless)")) {
				} else if (dependentOn.equalsIgnoreCase("Expected Opening Date")) {

					fc.utobj().sendKeys(driver, pobj.startScheduleDateTxtBox, "1");
					fc.utobj().selectDropDown(driver, pobj.scheduleStartDropDown, "Days Prior");
					fc.utobj().sendKeys(driver, pobj.completionScheduleDateTxtBox, "1");
					fc.utobj().selectDropDown(driver, pobj.scheduleCompletionDropDown, "Days Prior");

					fc.utobj().sendKeys(driver, pobj.startReminderTxtBox, "2");
					fc.utobj().sendKeys(driver, pobj.completionReminderTxtBox, "2");

				} else if (dependentOn.equalsIgnoreCase("Multiple Checklists")) {

					fc.utobj().clickElement(driver, pobj.dependentOnDropDownBtn);
					fc.utobj().clickElement(driver, pobj.dependentOnDropDownselectAll);

					fc.utobj().sendKeys(driver, pobj.startScheduleDateTxtBox, "1");
					fc.utobj().selectDropDown(driver, pobj.scheduleStartDropDown, "Days Prior");
					fc.utobj().sendKeys(driver, pobj.completionScheduleDateTxtBox, "1");
					fc.utobj().selectDropDown(driver, pobj.scheduleCompletionDropDown, "Days Prior");
					fc.utobj().sendKeys(driver, pobj.startReminderTxtBox, "2");
					fc.utobj().sendKeys(driver, pobj.completionReminderTxtBox, "2");

				}

				if (fc.utobj().isSelected(driver, pobj.uploadFileRadioBtn)) {
				} else {
					fc.utobj().clickElement(driver, pobj.uploadFileRadioBtn);
				}

				fc.utobj().sendKeys(driver, pobj.attachmentBrowseBox, file.getAbsolutePath());
				fc.utobj().clickElement(driver, pobj.addBtn);

				try {
					fc.commonMethods().switch_cboxIframe_frameId(driver);
					fc.utobj().clickElement(driver, pobj.yesBtn);
					fc.utobj().switchFrameToDefault(driver);
				} catch (Exception e) {

				}

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to Picture CheckList");

		}
		return pictureText;
	}

	public String addPictureChecklistWithGroup(WebDriver driver, Map<String, String> dataSet, String responsibilityArea,
			String userName, String groupName, String storeType, File file, Map<String, String> config)
			throws Exception {

		String testCaseId = "TC_Add_Picture_Checklist_Admin_Opener_02";
		String pictureText = null;
		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminOpenerPictureChecklistPage pobj = new AdminOpenerPictureChecklistPage(driver);

				AdminOpenerResponsibleDepartmentPageTest addDepartmentPage = new AdminOpenerResponsibleDepartmentPageTest();
				addDepartmentPage.addDepartment(driver, responsibilityArea);

				AdminOpenerManageGroupsPageTest addGroupsPage = new AdminOpenerManageGroupsPageTest();
				addGroupsPage.addGroups(driver, groupName);

				fc.opener().opener_common().adminFranchiseOpenerPictureChecklist(driver);

				fc.utobj().clickElement(driver, pobj.addMoreBtn);

				pictureText = fc.utobj().generateTestData(dataSet.get("pictureText"));
				fc.utobj().sendKeys(driver, pobj.pictureTitleTxBx, pictureText);
				fc.utobj().selectValFromMultiSelect(driver, pobj.selectResponsibility, responsibilityArea);

				fc.utobj().selectValFromMultiSelect(driver, pobj.selectContact, userName);

				fc.utobj().selectValFromMultiSelect(driver, pobj.selectStoreType, storeType);

				fc.utobj().selectDropDown(driver, pobj.groupDropDown, groupName);
				fc.utobj().selectDropDown(driver, pobj.franchiseeAccessDropDown, dataSet.get("franchiseeAccess"));
				fc.utobj().selectDropDown(driver, pobj.priorityDropDown, dataSet.get("priority"));
				fc.utobj().selectDropDown(driver, pobj.criticalLevelDropDown, dataSet.get("criticalLevel"));

				String dependentOn = dataSet.get("dependentOn");
				String valueText = fc.utobj()
						.getElementByXpath(driver,
								".//select[@name='smPictureChecklist_0referenceParent']/option[contains(text () , '"
										+ dependentOn + "')]")
						.getAttribute("value");
				fc.utobj().moveToElement(driver, pobj.dependentOnDropDown);
				Select sl = new Select(pobj.dependentOnDropDown);
				sl.selectByValue(valueText);

				if (dependentOn.equalsIgnoreCase("None (Timeless)")) {
				} else if (dependentOn.equalsIgnoreCase("Expected Opening Date")) {

					fc.utobj().sendKeys(driver, pobj.startScheduleDateTxtBox, "1");
					fc.utobj().selectDropDown(driver, pobj.scheduleStartDropDown, "Days Prior");
					fc.utobj().sendKeys(driver, pobj.completionScheduleDateTxtBox, "1");
					fc.utobj().selectDropDown(driver, pobj.scheduleCompletionDropDown, "Days Prior");

					fc.utobj().sendKeys(driver, pobj.startReminderTxtBox, "2");
					fc.utobj().sendKeys(driver, pobj.completionReminderTxtBox, "2");

				} else if (dependentOn.equalsIgnoreCase("Multiple Checklists")) {

					fc.utobj().clickElement(driver, pobj.dependentOnDropDownBtn);
					fc.utobj().clickElement(driver, pobj.dependentOnDropDownselectAll);
					fc.utobj().sendKeys(driver, pobj.startScheduleDateTxtBox, "1");
					fc.utobj().selectDropDown(driver, pobj.scheduleStartDropDown, "Days Prior");
					fc.utobj().sendKeys(driver, pobj.completionScheduleDateTxtBox, "1");
					fc.utobj().selectDropDown(driver, pobj.scheduleCompletionDropDown, "Days Prior");
					fc.utobj().sendKeys(driver, pobj.startReminderTxtBox, "2");
					fc.utobj().sendKeys(driver, pobj.completionReminderTxtBox, "2");

				}

				if (fc.utobj().isSelected(driver, pobj.uploadFileRadioBtn)) {
				} else {
					fc.utobj().clickElement(driver, pobj.uploadFileRadioBtn);
				}

				fc.utobj().sendKeys(driver, pobj.attachmentBrowseBox, file.getAbsolutePath());
				fc.utobj().clickElement(driver, pobj.addBtn);

				try {
					fc.commonMethods().switch_cboxIframe_frameId(driver);
					fc.utobj().clickElement(driver, pobj.yesBtn);
					fc.utobj().switchFrameToDefault(driver);
				} catch (Exception e) {

				}

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to Picture CheckList");

		}
		return pictureText;
	}

	public void addPictureChecklist(WebDriver driver, Map<String, String> dataSet, String responsibilityArea,
			String userName, String storeType, String groupName, String pictureText, File file,
			Map<String, String> config) throws Exception {

		String testCaseId = "TC_Add_Picture_Checklist_Admin_Opener_03";

		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminOpenerPictureChecklistPage pobj = new AdminOpenerPictureChecklistPage(driver);

				AdminOpenerResponsibleDepartmentPageTest addDepartmentPage = new AdminOpenerResponsibleDepartmentPageTest();
				addDepartmentPage.addDepartment(driver, responsibilityArea);

				AdminOpenerManageGroupsPageTest addGroupsPage = new AdminOpenerManageGroupsPageTest();
				addGroupsPage.addGroups(driver, groupName);
				fc.opener().opener_common().adminFranchiseOpenerPictureChecklist(driver);

				fc.utobj().clickElement(driver, pobj.addMoreBtn);

				fc.utobj().sendKeys(driver, pobj.pictureTitleTxBx, pictureText);
				fc.utobj().selectValFromMultiSelect(driver, pobj.selectResponsibility, responsibilityArea);

				fc.utobj().selectValFromMultiSelect(driver, pobj.selectContact, userName);

				fc.utobj().selectValFromMultiSelect(driver, pobj.selectStoreType, storeType);

				fc.utobj().selectDropDown(driver, pobj.groupDropDown, groupName);
				fc.utobj().selectDropDown(driver, pobj.franchiseeAccessDropDown, dataSet.get("franchiseeAccess"));
				fc.utobj().selectDropDown(driver, pobj.priorityDropDown, dataSet.get("priority"));
				fc.utobj().selectDropDown(driver, pobj.criticalLevelDropDown, dataSet.get("criticalLevel"));

				String dependentOn = dataSet.get("dependentOn");
				fc.utobj().moveToElement(driver, pobj.dependentOnDropDown);
				String valueText = fc.utobj()
						.getElementByXpath(driver,
								".//select[@name='smPictureChecklist_0referenceParent']/option[contains(text () , '"
										+ dependentOn + "')]")
						.getAttribute("value");
				fc.utobj().moveToElement(driver, pobj.dependentOnDropDown);
				Select sl = new Select(pobj.dependentOnDropDown);
				sl.selectByValue(valueText);

				if (fc.utobj().isSelected(driver, pobj.uploadFileRadioBtn)) {
				} else {
					fc.utobj().clickElement(driver, pobj.uploadFileRadioBtn);
				}

				fc.utobj().sendKeys(driver, pobj.attachmentBrowseBox, file.getAbsolutePath());
				fc.utobj().clickElement(driver, pobj.addBtn);

				try {

					fc.commonMethods().switch_cboxIframe_frameId(driver);
					fc.utobj().clickElement(driver, pobj.yesBtn);
					fc.utobj().switchFrameToDefault(driver);
				} catch (Exception e) {

				}

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to add Contact :: CRM > Contacts");

		}

	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Add Picture Check List with webLink option:: Admin > Opener > Picture Checklist", testCaseId = "TC_38_Add_Picture_Checklist_02")
	public void addPictureChecklistWebLink() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminOpenerPictureChecklistPage pobj = new AdminOpenerPictureChecklistPage(driver);
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Responsible Department");
			fc.utobj().printTestStep("Add Responsible Department");
			AdminOpenerResponsibleDepartmentPageTest addDepartmentPage = new AdminOpenerResponsibleDepartmentPageTest();
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));
			addDepartmentPage.addDepartment(driver, responsibilityArea);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			String emailId = "openerautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporatePage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Groups");
			fc.utobj().printTestStep("Add Group");

			AdminOpenerManageGroupsPageTest addGroupsPage = new AdminOpenerManageGroupsPageTest();
			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			addGroupsPage.addGroups(driver, groupName);

			configureDefaultValue(driver, config);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Picture Checklist");
			fc.utobj().printTestStep("Add Picture Checklist With WebLink Option");
			fc.opener().opener_common().adminFranchiseOpenerPictureChecklist(driver);

			fc.utobj().clickElement(driver, pobj.addMoreBtn);

			String pictureText = fc.utobj().generateTestData(dataSet.get("pictureText"));
			fc.utobj().sendKeys(driver, pobj.pictureTitleTxBx, pictureText);

			fc.utobj().selectValFromMultiSelect(driver, pobj.selectResponsibility, responsibilityArea);

			fc.utobj().selectValFromMultiSelect(driver, pobj.selectContact, corpUser.getuserFullName());

			fc.utobj().selectValFromMultiSelect(driver, pobj.selectStoreType, storeType);

			fc.utobj().selectDropDown(driver, pobj.groupDropDown, groupName);
			fc.utobj().selectDropDown(driver, pobj.franchiseeAccessDropDown, dataSet.get("franchiseeAccess"));
			fc.utobj().selectDropDown(driver, pobj.priorityDropDown, dataSet.get("priority"));
			fc.utobj().selectDropDown(driver, pobj.criticalLevelDropDown, dataSet.get("criticalLevel"));

			String dependentOn = dataSet.get("dependentOn");
			String valueText = fc.utobj()
					.getElementByXpath(driver,
							".//select[@name='smPictureChecklist_0referenceParent']/option[contains(text () , '"
									+ dependentOn + "')]")
					.getAttribute("value");
			fc.utobj().moveToElement(driver, pobj.dependentOnDropDown);
			Select sl = new Select(pobj.dependentOnDropDown);
			sl.selectByValue(valueText);

			if (fc.utobj().isSelected(driver, pobj.webLinkRadioBtn)) {

			} else {
				fc.utobj().clickElement(driver, pobj.webLinkRadioBtn);
			}

			fc.utobj().sendKeys(driver, pobj.webLinkTxBx, dataSet.get("webLinkUrl"));
			fc.utobj().clickElement(driver, pobj.addBtn);

			try {

				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().clickElement(driver, pobj.noBtn);
				fc.utobj().switchFrameToDefault(driver);
			} catch (Exception e) {

			}

			fc.utobj().printTestStep("Verify The Add Picture Checklist");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, pictureText);

			if (isTextPresent == false) {
				fc.utobj().throwsSkipException("was not able to add Picture checklist");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Modify Picture Check List  with upload file option at ActionImg:: Admin > Opener > Picture Checklist", testCaseId = "TC_39_Modify_Picture_Checklist")
	public void modifyPictureChecklistActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminOpenerPictureChecklistPage pobj = new AdminOpenerPictureChecklistPage(driver);
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Picture Checklist");
			fc.utobj().printTestStep("Add Picture Checklist");
			String pictureText = addPictureChecklist(driver, dataSet, config);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Responsible Department");
			fc.utobj().printTestStep("Add Responsible Department");

			AdminOpenerResponsibleDepartmentPageTest addDepartmentPage = new AdminOpenerResponsibleDepartmentPageTest();
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));
			addDepartmentPage.addDepartment(driver, responsibilityArea);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Groups");
			fc.utobj().printTestStep("Add Group");

			AdminOpenerManageGroupsPageTest addGroupsPage = new AdminOpenerManageGroupsPageTest();
			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			addGroupsPage.addGroups(driver, groupName);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Picture Checklist");
			fc.opener().opener_common().adminFranchiseOpenerPictureChecklist(driver);

			fc.utobj().printTestStep("Modify The Picture Checklist");
			/*
			 * String
			 * text=fc.utobj().getElementByXpath(driver,".//*[.='"+pictureText+
			 * "']/ancestor::tr/td/div[@id='menuBar']/layer")).getAttribute("id"
			 * ).trim(); String alterText=text.replace("Actions_dynamicmenu",
			 * ""); alterText=alterText.replace("Bar", "");
			 * 
			 * fc.utobj().clickElement(driver,
			 * fc.utobj().getElementByXpath(driver,".//*[.='"+pictureText+
			 * "']/ancestor::tr/td/div[@id='menuBar']/layer/a/img")));
			 * fc.utobj().clickElement(driver,
			 * fc.utobj().getElementByXpath(driver,
			 * ".//div[@id='Actions_dynamicmenu"+ alterText+
			 * "Menu']/span[contains(text () , 'Modify')]")));
			 */

			fc.utobj().actionImgOption(driver, pictureText, "Modify");

			pictureText = fc.utobj().generateTestData(dataSet.get("pictureText"));
			fc.utobj().sendKeys(driver, pobj.pictureTitleTxBx, pictureText);

			fc.utobj().selectValFromMultiSelect(driver, pobj.selectResponsibility, responsibilityArea);
			fc.utobj().selectValFromMultiSelect(driver, pobj.selectStoreType, storeType);

			fc.utobj().selectDropDown(driver, pobj.groupDropDown, groupName);
			fc.utobj().selectDropDown(driver, pobj.franchiseeAccessDropDown, dataSet.get("franchiseeAccess"));
			fc.utobj().selectDropDown(driver, pobj.priorityDropDown, dataSet.get("priority"));
			fc.utobj().selectDropDown(driver, pobj.criticalLevelDropDown, dataSet.get("criticalLevel"));

			String dependentOn = dataSet.get("dependentOnModify");
			String valueText = fc.utobj()
					.getElementByXpath(driver,
							".//select[@name='smPictureChecklist_0referenceParent']/option[contains(text () , '"
									+ dependentOn + "')]")
					.getAttribute("value");
			fc.utobj().moveToElement(driver, pobj.dependentOnDropDown);
			Select sl = new Select(pobj.dependentOnDropDown);
			sl.selectByValue(valueText);

			if (fc.utobj().isSelected(driver, pobj.webLinkRadioBtn)) {
			} else {
				fc.utobj().clickElement(driver, pobj.webLinkRadioBtn);
			}

			fc.utobj().sendKeys(driver, pobj.webLinkTxBx, dataSet.get("webLinkUrl"));

			try {
				List<WebElement> listRadioButton = pobj.applyToAll;
				if (fc.utobj().isSelected(driver,listRadioButton.get(0))) {
				} else {
					fc.utobj().clickElement(driver, listRadioButton.get(0));
				}
			} catch (Exception e) {
			}

			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("Verify The Modify Picture Checklist");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, pictureText);

			if (isTextPresent == false) {
				fc.utobj().throwsSkipException("was not able to modify Picture checklist");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "opener", "openeremail" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-29", testCaseDescription = "Delete Picture Check List  with upload file option at ActionImg:: Admin > Opener > Picture Checklist", testCaseId = "TC_40_Delete_Picture_Checklist")
	public void deletePictureChecklistActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminOpenerPictureChecklistPage pobj = new AdminOpenerPictureChecklistPage(driver);
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Picture Checklist");
			fc.utobj().printTestStep("Add Picture Checklist");
			String pictureText = addPictureChecklist(driver, dataSet, config);

			fc.utobj().printTestStep("Delete Picture Checklist");
			String text = driver
					.findElement(By.xpath(".//*[.='" + pictureText + "']/ancestor::tr/td/div[@id='menuBar']/layer"))
					.getAttribute("id").trim();
			String alterText = text.replace("Actions_dynamicmenu", "");
			alterText = alterText.replace("Bar", "");

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[.='" + pictureText + "']/ancestor::tr/td/div[@id='menuBar']/layer/a/img"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//div[@id='Actions_dynamicmenu" + alterText + "Menu']/span[contains(text () , 'Delete')]"));

			fc.utobj().acceptAlertBox(driver);

			try {

				fc.commonMethods().switch_cboxIframe_frameId(driver);
				if (fc.utobj().isSelected(driver,pobj.applyToAll.get(0))) {
				} else {
					fc.utobj().clickElement(driver, pobj.applyToAll.get(0));
				}
				fc.utobj().clickElement(driver, pobj.cboxDeleteBtn);
				fc.utobj().clickElement(driver, pobj.cboxCloseBtn);
				fc.utobj().switchFrameToDefault(driver);
			} catch (Exception e) {

			}

			fc.utobj().printTestStep("Verify The Delete Picture Checklist");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, pictureText);

			if (isTextPresent == true) {
				fc.utobj().throwsSkipException("Was not able to delete Picture Checklist");
			}

			fc.utobj().printTestStep("Verify Email After Picture Checklist Deletion");

			Map<String, String> mailData = new HashMap<String, String>();
			mailData = fc.utobj().readMailBox("Checklist Item Deletion Alert", pictureText,
					"openerautomation@staffex.com", "sdg@1a@Hfs");

			if (mailData.size() == 0 || !mailData.get("mailBody").contains("Item Deleted")) {

				fc.utobj().throwsException("was not able to verify Picture Checklist Deletion Mail");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add Discription In Picture Checklist Task", testCaseId = "TC_41_Add_Description_Picture")
	public void addDescriptionPictureActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminOpenerPictureChecklistPage pobj = new AdminOpenerPictureChecklistPage(driver);
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Picture Checklist");
			fc.utobj().printTestStep("Add Picture Checklist");
			String pictureText = addPictureChecklist(driver, dataSet, config);

			fc.utobj().printTestStep("Add Description");
			fc.utobj().actionImgOption(driver, pictureText, "Add Description");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().sendKeys(driver, pobj.pictureDescriptionArea, description);
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify Add Description");
			fc.utobj().actionImgOption(driver, pictureText, "Modify Description");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String isTextPresent = fc.utobj().getText(driver, pobj.pictureDescriptionArea);

			if (isTextPresent.equalsIgnoreCase(description)) {
			} else {
				fc.utobj().throwsSkipException("was not able to add description");
			}
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = " Modify description at Picture Check List with upload file option at ActionImg:: Admin > Opener > Picture Checklist", testCaseId = "TC_42_Modify_Description_Picture")
	public void modifyDescriptionActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminOpenerPictureChecklistPage pobj = new AdminOpenerPictureChecklistPage(driver);
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Picture Checklist");
			fc.utobj().printTestStep("Add Picture Checklist");
			String pictureText = addPictureChecklist(driver, dataSet, config);

			fc.utobj().printTestStep("Add Description");

			fc.utobj().actionImgOption(driver, pictureText, "Add Description");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().sendKeys(driver, pobj.pictureDescriptionArea, description);
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Modify The Description");
			fc.utobj().actionImgOption(driver, pictureText, "Modify Description");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().sendKeys(driver, pobj.pictureDescriptionArea, description);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Modify Description");
			fc.utobj().actionImgOption(driver, pictureText, "Modify Description");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String isTextPresent = fc.utobj().getText(driver, pobj.pictureDescriptionArea);

			if (isTextPresent.equalsIgnoreCase(description)) {
				// do nothing
			} else {
				fc.utobj().throwsSkipException("was not able to modify description");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Modify Picture Check List  with upload file option at Action Button:: Admin > Opener > Picture Checklist", testCaseId = "TC_43_Modfiy_Picture_Checklist01")
	public void modifyPictureChecklistActionButton() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminOpenerPictureChecklistPage pobj = new AdminOpenerPictureChecklistPage(driver);
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Picture Checklist");
			fc.utobj().printTestStep("Add Picture Checklist");
			String pictureText = addPictureChecklist(driver, dataSet, config);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Responsible Department");
			fc.utobj().printTestStep("Add Responsible Department");

			AdminOpenerResponsibleDepartmentPageTest addDepartmentPage = new AdminOpenerResponsibleDepartmentPageTest();
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));
			addDepartmentPage.addDepartment(driver, responsibilityArea);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Groups");
			fc.utobj().printTestStep("Add Group");

			AdminOpenerManageGroupsPageTest addGroupsPage = new AdminOpenerManageGroupsPageTest();
			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			addGroupsPage.addGroups(driver, groupName);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Picture Checklist");
			fc.opener().opener_common().adminFranchiseOpenerPictureChecklist(driver);

			fc.utobj().printTestStep("Modify The Picture Checklist By Action Button Option");

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[.='" + pictureText
					+ "']//ancestor::tr[@class='bText12' or @class='bText12 grAltRw3']/td/input"));

			fc.utobj().selectActionMenuItems(driver, "Modify");

			pictureText = fc.utobj().generateTestData(dataSet.get("pictureText"));
			fc.utobj().sendKeys(driver, pobj.pictureTitleTxBx, pictureText);

			fc.utobj().selectValFromMultiSelect(driver, pobj.selectResponsibility, responsibilityArea);
			fc.utobj().selectValFromMultiSelect(driver, pobj.selectStoreType, storeType);

			fc.utobj().selectDropDown(driver, pobj.groupDropDown, groupName);
			fc.utobj().selectDropDown(driver, pobj.franchiseeAccessDropDown, dataSet.get("franchiseeAccess"));
			fc.utobj().selectDropDown(driver, pobj.priorityDropDown, dataSet.get("priority"));
			fc.utobj().selectDropDown(driver, pobj.criticalLevelDropDown, dataSet.get("criticalLevel"));

			String dependentOn = dataSet.get("dependentOnModify");
			String valueText = fc.utobj()
					.getElementByXpath(driver,
							".//select[@name='smPictureChecklist_0referenceParent']/option[contains(text () , '"
									+ dependentOn + "')]")
					.getAttribute("value");
			fc.utobj().moveToElement(driver, pobj.dependentOnDropDown);
			Select sl = new Select(pobj.dependentOnDropDown);
			sl.selectByValue(valueText);

			if (fc.utobj().isSelected(driver, pobj.webLinkRadioBtn)) {
			} else {
				fc.utobj().clickElement(driver, pobj.webLinkRadioBtn);
			}

			fc.utobj().sendKeys(driver, pobj.webLinkTxBx, dataSet.get("webLinkUrl"));
			try {
				List<WebElement> listRadioButton = pobj.applyToAll;
				if (fc.utobj().isSelected(driver,listRadioButton.get(0))) {
				} else {
					fc.utobj().clickElement(driver, listRadioButton.get(0));
				}
			} catch (Exception e) {

			}

			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("Verify The Modify Picture Checklist");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, pictureText);

			if (isTextPresent == false) {
				fc.utobj().throwsSkipException("was not able modify picture checklist");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Delete Picture Check List  with upload file option at Action Button:: Admin > Opener > Picture Checklist", testCaseId = "TC_44_Delete_Picture_Checklist01")
	public void deletePictureChecklistActionButton() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminOpenerPictureChecklistPage pobj = new AdminOpenerPictureChecklistPage(driver);
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Picture Checklist");
			fc.utobj().printTestStep("Add Picture Checklist");
			String pictureText = addPictureChecklist(driver, dataSet, config);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[.='" + pictureText
					+ "']//ancestor::tr[@class='bText12' or @class='bText12 grAltRw3']/td/input"));

			fc.utobj().printTestStep("Delete The Picture Checklist");
			fc.utobj().selectActionMenuItems(driver, "Delete");

			fc.utobj().acceptAlertBox(driver);

			try {
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				if (fc.utobj().isSelected(driver,pobj.applyToAll.get(1))) {
				} else {
					fc.utobj().clickElement(driver, pobj.applyToAll.get(1));
				}

				fc.utobj().clickElement(driver, pobj.cboxDeleteBtn);
				fc.utobj().clickElement(driver, pobj.cboxCloseBtn);
				fc.utobj().switchFrameToDefault(driver);

			} catch (Exception e) {

			}

			fc.utobj().printTestStep("Verify The Delete Picture Checklist");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, pictureText);

			if (isTextPresent == true) {
				fc.utobj().throwsSkipException("Was not able to delete Picture Checklist");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Modify Contact(s) Picture Check List  with upload file option at Action Button:: Admin > Opener > Picture Checklist", testCaseId = "TC_45_Modify_Contact_Picture")
	public void modifyContactActionButton() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminOpenerPictureChecklistPage pobj = new AdminOpenerPictureChecklistPage(driver);
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Enable Divisional Users");
			AdminHiddenLinksConfigureNewHierarchyLevelPageTest configure_level = new AdminHiddenLinksConfigureNewHierarchyLevelPageTest();
			configure_level.ConfigureNewHierarchyLevel(driver);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Picture Checklist");
			fc.utobj().printTestStep("Add Picture Checklist");
			String pictureText = addPictureChecklist(driver, dataSet, config);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[.='" + pictureText
					+ "']//ancestor::tr[@class='bText12' or @class='bText12 grAltRw3']/td/input"));

			fc.utobj().printTestStep("Modify The Contact Of Checklist");
			fc.utobj().selectActionMenuItems(driver, "Modify Contact(s)");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.modifyContactSelectBtn);
			fc.utobj().sendKeys(driver, pobj.modifyContactSearchBox, "Divisional User");
			fc.utobj().clickElement(driver, pobj.modifyContactSelectAll);
			fc.utobj().clickElement(driver, pobj.modifyContactSelectBtn);

			try {

				if (fc.utobj().isSelected(driver,pobj.applyToAll.get(1))) {
				} else {
					fc.utobj().clickElement(driver, pobj.applyToAll.get(1));
				}

			} catch (Exception e) {

			}

			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("Verify The Modify Contact");
			fc.utobj().switchFrameToDefault(driver);

			String textPresent = fc.utobj().getText(driver, fc.utobj().getElementByXpath(driver, ".//*[.='"
					+ pictureText + "']//ancestor::tr[@class='bText12' or @class='bText12 grAltRw3']/td[5]"));

			if (!textPresent.contains("Divisional")) {
				fc.utobj().throwsSkipException("was not able to modify Contact");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Configure Default Value Picture Check List  with upload file option at Action Button:: Admin > Opener > Picture Checklist", testCaseId = "TC_46_Configure_Default_Value_Picture")
	public void configureDefaultValueActionButton() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminOpenerPictureChecklistPage pobj = new AdminOpenerPictureChecklistPage(driver);
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Picture Checklist");
			fc.utobj().printTestStep("Add Picture Checklist");
			String pictureText = addPictureChecklist(driver, dataSet, config);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Responsible Department");
			fc.utobj().printTestStep("Add Responsible Department");

			AdminOpenerResponsibleDepartmentPageTest addDepartmentPage = new AdminOpenerResponsibleDepartmentPageTest();
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));
			addDepartmentPage.addDepartment(driver, responsibilityArea);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			String emailId = "openerautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporatePage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Groups");
			fc.utobj().printTestStep("Add Group");

			AdminOpenerManageGroupsPageTest addGroupsPage = new AdminOpenerManageGroupsPageTest();
			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			addGroupsPage.addGroups(driver, groupName);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Picture Checklist");
			fc.opener().opener_common().adminFranchiseOpenerPictureChecklist(driver);

			fc.utobj().printTestStep("Configure Default Value");

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[.='" + pictureText
					+ "']//ancestor::tr[@class='bText12' or @class='bText12 grAltRw3']/td/input"));

			fc.utobj().selectActionMenuItems(driver, "Configure Default Values");

			fc.utobj().selectValFromMultiSelect(driver, pobj.selectResponsibility, responsibilityArea);
			fc.utobj().selectValFromMultiSelect(driver, pobj.selectContact, corpUser.getuserFullName());
			fc.utobj().selectValFromMultiSelect(driver, pobj.selectStoreType, storeType);

			fc.utobj().selectDropDown(driver, pobj.groupDropDown, groupName);
			fc.utobj().selectDropDown(driver, pobj.franchiseeAccessDropDown, dataSet.get("franchiseeAccess"));
			fc.utobj().selectDropDown(driver, pobj.priorityDropDown, dataSet.get("priority"));
			fc.utobj().selectDropDown(driver, pobj.criticalLevelDropDown, dataSet.get("criticalLevel"));

			String dependentOn = dataSet.get("dependentOn");
			String valueText = fc.utobj()
					.getElementByXpath(driver,
							".//select[@name='smPictureChecklist_0referenceParent']/option[contains(text () , '"
									+ dependentOn + "')]")
					.getAttribute("value");
			fc.utobj().moveToElement(driver, pobj.dependentOnDropDown);
			Select sl = new Select(pobj.dependentOnDropDown);
			sl.selectByValue(valueText);

			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("Verify The Configure Default Value");
			String textResponsibility = driver
					.findElement(By.xpath(".//*[@id='ms-parentsmPictureChecklist_0responsibilityArea']/button/span"))
					.getText().trim();
			String textContact = driver
					.findElement(By.xpath(".//*[@id='ms-parentsmPictureChecklist_0contact']/button/span")).getText()
					.trim();
			String textStoreType = driver
					.findElement(By.xpath(".//*[@id='ms-parentsmPictureChecklist_0storeTypeId']/button/span")).getText()
					.trim();

			if (textResponsibility.equalsIgnoreCase(responsibilityArea)
					&& textContact.contains(corpUser.getuserFullName()) && textStoreType.equalsIgnoreCase(storeType)) {
			} else {
				fc.utobj().throwsException("was not able to Configure default value");
			}

			configureDefaultValue(driver, config);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void configureDefaultValue(WebDriver driver, Map<String, String> config) throws Exception {

		String testCaseId = "TC_Configure_Default_Picture_Checklist_01";

		if (fc.utobj().configurationTestCaseToExecute(testCaseId)) {

			try {
				AdminOpenerPictureChecklistPage pobj = new AdminOpenerPictureChecklistPage(driver);

				fc.opener().opener_common().adminFranchiseOpenerPictureChecklist(driver);
				fc.utobj().selectActionMenuItems(driver, "Configure Default Values");

				fc.utobj().setToDefault(driver, pobj.selectResponsibility);
				fc.utobj().setToDefault(driver, pobj.selectContact);
				fc.utobj().setToDefault(driver, pobj.selectStoreType);

				fc.utobj().selectDropDown(driver, pobj.groupDropDown, "Select");
				fc.utobj().selectDropDown(driver, pobj.franchiseeAccessDropDown, "Select");
				fc.utobj().selectDropDown(driver, pobj.priorityDropDown, "Select");
				fc.utobj().selectDropDown(driver, pobj.criticalLevelDropDown, "Select");
				String dependentOn = "None (Timeless)";
				String valueText = fc.utobj()
						.getElementByXpath(driver,
								".//select[@name='smPictureChecklist_0referenceParent']/option[contains(text () , '"
										+ dependentOn + "')]")
						.getAttribute("value");
				fc.utobj().moveToElement(driver, pobj.dependentOnDropDown);
				Select sl = new Select(pobj.dependentOnDropDown);
				sl.selectByValue(valueText);
				fc.utobj().clickElement(driver, pobj.saveBtn);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Modify Picture Check List  with upload file option at Action Button Bottom:: Admin > Opener > Picture Checklist", testCaseId = "TC_47_Modify_Picture_Checklist02")
	public void modifyPictureChecklistBottom() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminOpenerPictureChecklistPage pobj = new AdminOpenerPictureChecklistPage(driver);
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Picture Checklist");
			fc.utobj().printTestStep("Add Picture Checklist");
			String pictureText = addPictureChecklist(driver, dataSet, config);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Responsible Department");
			fc.utobj().printTestStep("Add Responsible Department");

			AdminOpenerResponsibleDepartmentPageTest addDepartmentPage = new AdminOpenerResponsibleDepartmentPageTest();
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));
			addDepartmentPage.addDepartment(driver, responsibilityArea);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Groups");
			fc.utobj().printTestStep("Add Group");

			AdminOpenerManageGroupsPageTest addGroupsPage = new AdminOpenerManageGroupsPageTest();
			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			addGroupsPage.addGroups(driver, groupName);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Picture Checklist");
			fc.opener().opener_common().adminFranchiseOpenerPictureChecklist(driver);

			fc.utobj().printTestStep("Modify The Picture Checklist By Action Button Bottom Option");

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[.='" + pictureText
					+ "']//ancestor::tr[@class='bText12' or @class='bText12 grAltRw3']/td/input"));
			fc.utobj().clickElement(driver, pobj.bottomModifyBtn);

			pictureText = fc.utobj().generateTestData(dataSet.get("pictureText"));
			fc.utobj().sendKeys(driver, pobj.pictureTitleTxBx, pictureText);

			fc.utobj().selectValFromMultiSelect(driver, pobj.selectResponsibility, responsibilityArea);

			fc.utobj().selectValFromMultiSelect(driver, pobj.selectStoreType, storeType);

			fc.utobj().selectDropDown(driver, pobj.groupDropDown, groupName);
			fc.utobj().selectDropDown(driver, pobj.franchiseeAccessDropDown, dataSet.get("franchiseeAccess"));
			fc.utobj().selectDropDown(driver, pobj.priorityDropDown, dataSet.get("priority"));
			fc.utobj().selectDropDown(driver, pobj.criticalLevelDropDown, dataSet.get("criticalLevel"));

			String dependentOn = dataSet.get("dependentOnModify");
			String valueText = fc.utobj()
					.getElementByXpath(driver,
							".//select[@name='smPictureChecklist_0referenceParent']/option[contains(text () , '"
									+ dependentOn + "')]")
					.getAttribute("value");
			fc.utobj().moveToElement(driver, pobj.dependentOnDropDown);
			Select sl = new Select(pobj.dependentOnDropDown);
			sl.selectByValue(valueText);

			if (fc.utobj().isSelected(driver, pobj.webLinkRadioBtn)) {
			} else {
				fc.utobj().clickElement(driver, pobj.webLinkRadioBtn);
			}

			fc.utobj().sendKeys(driver, pobj.webLinkTxBx, dataSet.get("webLinkUrl"));

			try {
				List<WebElement> listRadioButton = pobj.applyToAll;
				if (fc.utobj().isSelected(driver,listRadioButton.get(0))) {
				} else {
					fc.utobj().clickElement(driver, listRadioButton.get(0));
				}
			} catch (Exception e) {

			}

			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("Verify The Modify Checklist");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, pictureText);

			if (isTextPresent == false) {
				fc.utobj().throwsSkipException("was not able to modify Picture checklist At bottom btn");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Delete Picture Check List  with upload file option at Bottom:: Admin > Opener > Picture Checklist", testCaseId = "TC_48_Delete_Picture_Checklist02")
	public void deletePictureChecklistBottom() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminOpenerPictureChecklistPage pobj = new AdminOpenerPictureChecklistPage(driver);
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Picture Checklist");
			fc.utobj().printTestStep("Add Picture Checklist");
			String pictureText = addPictureChecklist(driver, dataSet, config);

			fc.utobj().printTestStep("Delete The Picture Checklist");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[.='" + pictureText
					+ "']//ancestor::tr[@class='bText12' or @class='bText12 grAltRw3']/td/input"));
			fc.utobj().clickElement(driver, pobj.bottomDeleteBtn);

			fc.utobj().acceptAlertBox(driver);

			try {

				fc.commonMethods().switch_cboxIframe_frameId(driver);
				if (fc.utobj().isSelected(driver,pobj.applyToAll.get(0))) {
				} else {
					fc.utobj().clickElement(driver, pobj.applyToAll.get(0));
				}

				fc.utobj().clickElement(driver, pobj.cboxDeleteBtn);
				fc.utobj().clickElement(driver, pobj.cboxCloseBtn);
				fc.utobj().switchFrameToDefault(driver);

			} catch (Exception e) {

			}

			fc.utobj().printTestStep("Verify The Delete Picture Checklist");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, pictureText);

			if (isTextPresent == true) {
				fc.utobj().throwsSkipException("Was not able to delete Picture Checklist");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
}
