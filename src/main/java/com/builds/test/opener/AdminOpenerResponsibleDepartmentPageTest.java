package com.builds.test.opener;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.uimaps.opener.AdminOpenerResponsibleDepartmentPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminOpenerResponsibleDepartmentPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Add Responsible Department :: Admin > Opner > Responsible Department", testCaseId = "TC_66_Add_Department")
	public void addDepartment() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminOpenerResponsibleDepartmentPage pobj = new AdminOpenerResponsibleDepartmentPage(driver);
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Add Responsibility Area Department");
			fc.opener().opener_common().adminFranchiseOpenerResponsibileDepartment(driver);

			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));
			fc.utobj().clickElement(driver, pobj.addMoreBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.responsibilityArea, responsibilityArea);
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Add Responsible Area Department");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, responsibilityArea);

			if (isTextPresent == false) {
				fc.utobj().throwsSkipException("was not able to add Responsible department");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Modify Responsible Department :: Admin > Opner > Responsible Department", testCaseId = "TC_67_Modify_Department")
	public void modifyDepartment() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminOpenerResponsibleDepartmentPage pobj = new AdminOpenerResponsibleDepartmentPage(driver);
			driver = fc.loginpage().login(driver);

			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));

			fc.utobj().printTestStep("Navigate To Admin > Opener > Responsible Department");
			fc.utobj().printTestStep("Add Responsible Department");

			addDepartment(driver, responsibilityArea);

			fc.utobj().printTestStep("Modify The Responsibility Area Departmnet");
			String text = fc.utobj().getElementByXpath(driver,
					".//*[contains(text () , '" + responsibilityArea + "')]//ancestor::tr/td/div[@id='menuBar']/layer")
					.getAttribute("id").trim();
			String alterText = text.replace("Actions_dynamicmenu", "");
			alterText = alterText.replace("Bar", "");

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[contains(text () , '"
					+ responsibilityArea + "')]//ancestor::tr/td/div[@id='menuBar']/layer/a/img"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//div[@id='Actions_dynamicmenu" + alterText + "Menu']/span[contains(text () , 'Modify')]"));

			responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.responsibilityArea, responsibilityArea);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Modify Responsibility Area Departmnet");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, responsibilityArea);

			if (isTextPresent == false) {
				fc.utobj().throwsSkipException("was not able to modify Responsible department");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Delete Responsible Department :: Admin > Opner > Responsible Department", testCaseId = "TC_68_Delete_Department")
	public void deleteDepartment() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Responsible Department");
			fc.utobj().printTestStep("Add Responsible Department");

			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));

			addDepartment(driver, responsibilityArea);

			fc.utobj().printTestStep("Delete Responsibility Area Departmnet");
			String text = fc.utobj().getElementByXpath(driver,
					".//*[contains(text () , '" + responsibilityArea + "')]//ancestor::tr/td/div[@id='menuBar']/layer")
					.getAttribute("id").trim();
			String alterText = text.replace("Actions_dynamicmenu", "");
			alterText = alterText.replace("Bar", "");

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[contains(text () , '"
					+ responsibilityArea + "')]//ancestor::tr/td/div[@id='menuBar']/layer/a/img"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//div[@id='Actions_dynamicmenu" + alterText + "Menu']/span[contains(text () , 'Delete')]"));
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Delete Responsibility Area Departmnet");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, responsibilityArea);

			if (isTextPresent == true) {
				fc.utobj().throwsSkipException("was not able to delete Responsible department");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void addDepartment(WebDriver driver, String responsibilityArea) throws Exception {

		String testCaseId = "TC_Add_Department_Opener";

		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminOpenerResponsibleDepartmentPage pobj = new AdminOpenerResponsibleDepartmentPage(driver);
				fc.opener().opener_common().adminFranchiseOpenerResponsibileDepartment(driver);

				fc.utobj().clickElement(driver, pobj.addMoreBtn);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().sendKeys(driver, pobj.responsibilityArea, responsibilityArea);
				fc.utobj().clickElement(driver, pobj.addBtn);
				fc.utobj().switchFrameToDefault(driver);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to Add DepartMent");

		}
	}
}