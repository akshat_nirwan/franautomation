package com.builds.test.opener;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminConfigurationConfigureStoreTypePageTest;
import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.common.CorporateUser;
import com.builds.uimaps.opener.AdminOpenerSecondaryChecklistPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminOpenerSecondaryChecklistPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Add Secondry Check List  :: Admin > Opener > Secondry Checklist", testCaseId = "TC_49_Add_Secondary_Checklist")
	public void addSecondaryChecklist() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminOpenerSecondaryChecklistPage pobj = new AdminOpenerSecondaryChecklistPage(driver);
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Add Secondary Checklist");
			fc.opener().opener_common().adminFranchiseOpenerSecondaryChecklist(driver);

			fc.utobj().clickElement(driver, pobj.addSecondaryBtn);
			String checklistName = fc.utobj().generateTestData(dataSet.get("checklistName"));
			fc.utobj().sendKeys(driver, pobj.checklistNameTxtBox, checklistName);

			fc.utobj().sendKeys(driver, pobj.checklistDescriptionTxtBox, dataSet.get("description"));
			fc.utobj().clickElement(driver, pobj.addBtn);

			fc.utobj().printTestStep("Verify The Add Secondary Checklist");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, checklistName);

			if (isTextPresent == false) {
				fc.utobj().throwsSkipException("was not able to add Secondary Checklist");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addSecondaryChecklist(WebDriver driver, Map<String, String> dataSet, Map<String, String> config)
			throws Exception {

		String testCaseId = "TC_Add_Secondary_Checklist_Opener";
		String checklistName = null;

		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminOpenerSecondaryChecklistPage pobj = new AdminOpenerSecondaryChecklistPage(driver);

				fc.opener().opener_common().adminFranchiseOpenerSecondaryChecklist(driver);

				fc.utobj().clickElement(driver, pobj.addSecondaryBtn);
				checklistName = fc.utobj().generateTestData(dataSet.get("checklistName"));
				fc.utobj().sendKeys(driver, pobj.checklistNameTxtBox, checklistName);

				fc.utobj().sendKeys(driver, pobj.checklistDescriptionTxtBox, dataSet.get("description"));
				fc.utobj().clickElement(driver, pobj.addBtn);
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to Add Secondary Checklist");

		}
		return checklistName;
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Modify Secondry Check List  :: Admin > Opener > Secondry Checklist", testCaseId = "TC_50_Modify_Secondary_Checklist")
	public void modifySecondaryChecklist() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminOpenerSecondaryChecklistPage pobj = new AdminOpenerSecondaryChecklistPage(driver);
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Add Secondary Checklist");
			String checklistName = addSecondaryChecklist(driver, dataSet, config);

			fc.utobj().printTestStep("Modify The Checklist");
			String text = driver
					.findElement(By.xpath(".//*[.='" + checklistName + "']/ancestor::tr/td/div[@id='menuBar']/layer"))
					.getAttribute("id").trim();
			String alterText = text.replace("Actions_dynamicmenu", "");
			alterText = alterText.replace("Bar", "");

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[.='" + checklistName + "']/ancestor::tr/td/div[@id='menuBar']/layer/a/img"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//div[@id='Actions_dynamicmenu" + alterText + "Menu']/span[contains(text () , 'Modify')]"));

			checklistName = fc.utobj().generateTestData(dataSet.get("checklistName"));
			fc.utobj().sendKeys(driver, pobj.checklistNameTxtBox, checklistName);

			fc.utobj().sendKeys(driver, pobj.checklistDescriptionTxtBox, dataSet.get("description"));
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("Verify The Modify Checklist");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, checklistName);

			if (isTextPresent == false) {
				fc.utobj().throwsSkipException("was not able to add Secondary Checklist");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Delete Secondry Check List  :: Admin > Opener > Secondry Checklist", testCaseId = "TC_51_Delete_Secondary_Checklist")
	public void deleteSecondaryChecklist() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Add Secondary Checklist");
			String checklistName = addSecondaryChecklist(driver, dataSet, config);

			fc.utobj().printTestStep("Delete The Checklist");
			String text = driver
					.findElement(By.xpath(".//*[.='" + checklistName + "']/ancestor::tr/td/div[@id='menuBar']/layer"))
					.getAttribute("id").trim();
			String alterText = text.replace("Actions_dynamicmenu", "");
			alterText = alterText.replace("Bar", "");

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[.='" + checklistName + "']/ancestor::tr/td/div[@id='menuBar']/layer/a/img"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//div[@id='Actions_dynamicmenu" + alterText + "Menu']/span[contains(text () , 'Delete')]"));
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Delete Checklist");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, checklistName);

			if (isTextPresent == true) {
				fc.utobj().throwsException("was not able to delete secondary checklist");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Add Task in Secondry Check List  :: Admin > Opener > Secondry Checklist", testCaseId = "TC_52_Add_Task_Secondary_Checklist")
	public void addTaskSecondaryChecklist() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminOpenerSecondaryChecklistPage pobj = new AdminOpenerSecondaryChecklistPage(driver);
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Responsible Department");
			fc.utobj().printTestStep("Add Responsible Department");

			AdminOpenerResponsibleDepartmentPageTest addDepartmentPage = new AdminOpenerResponsibleDepartmentPageTest();
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));
			addDepartmentPage.addDepartment(driver, responsibilityArea);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			/*
			 * String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String password=dataSet.get("password");
			 * corporatePage.addCorporateUser(driver, userName, password,
			 * config);
			 */

			String emailId = "openerautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporatePage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Groups");
			fc.utobj().printTestStep("Add Group");

			AdminOpenerManageGroupsPageTest addGroupsPage = new AdminOpenerManageGroupsPageTest();
			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			addGroupsPage.addGroups(driver, groupName);

			configureDefaultValue(driver, config);

			fc.utobj().printTestStep("Add Secondary Checklist");
			String checklistName = addSecondaryChecklist(driver, dataSet, config);

			fc.utobj().printTestStep("Add Task In Checklist");
			String text = driver
					.findElement(By.xpath(".//*[.='" + checklistName + "']/ancestor::tr/td/div[@id='menuBar']/layer"))
					.getAttribute("id").trim();
			String alterText = text.replace("Actions_dynamicmenu", "");
			alterText = alterText.replace("Bar", "");

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[.='" + checklistName + "']/ancestor::tr/td/div[@id='menuBar']/layer/a/img"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//div[@id='Actions_dynamicmenu" + alterText + "Menu']/span[contains(text () , 'Add Task')]"));

			String task = fc.utobj().generateTestData(dataSet.get("task"));
			fc.utobj().sendKeys(driver, pobj.itemNameTxtBox, task);

			fc.utobj().selectValFromMultiSelect(driver, pobj.selectResponsibilityArea, responsibilityArea);

			fc.utobj().selectValFromMultiSelect(driver, pobj.selectContact, corpUser.getuserFullName());

			fc.utobj().selectValFromMultiSelect(driver, pobj.selectStoreType, storeType);

			fc.utobj().selectDropDown(driver, pobj.groupDropDown, groupName);

			String franchiseeAccess = dataSet.get("franchiseeAccess");
			fc.utobj().selectDropDown(driver, pobj.franchiseeAccessDropDown, franchiseeAccess);

			String priority = dataSet.get("priority");
			fc.utobj().selectDropDown(driver, pobj.priorityDropDown, priority);

			String criticalLevel = dataSet.get("criticalLevel");
			fc.utobj().selectDropDown(driver, pobj.criticalLevelDropDown, criticalLevel);

			String dependentOn = dataSet.get("dependentOn");
			String valueText = fc.utobj()
					.getElementByXpath(driver,
							".//select[@name='smSecondryChecklist_0referenceParent']/option[contains(text () , '"
									+ dependentOn + "')]")
					.getAttribute("value");
			fc.utobj().moveToElement(driver, pobj.dependentOnDropDown);
			Select sl = new Select(pobj.dependentOnDropDown);
			sl.selectByValue(valueText);

			if (fc.utobj().isSelected(driver, pobj.uploadFileRadioBtn)) {
				// do nothing
			} else {
				fc.utobj().clickElement(driver, pobj.uploadFileRadioBtn);
			}

			String file = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));

			System.out.println(file);
			fc.utobj().sendKeys(driver, pobj.attachmentBrowseBox, file);
			fc.utobj().clickElement(driver, pobj.addBtn);

			try {

				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().clickElement(driver, pobj.noBtn);
				fc.utobj().switchFrameToDefault(driver);
			} catch (Exception e) {

			}

			fc.utobj().printTestStep("Verify The Add Secondary Checklist");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, task);

			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to add Task in secondary checklist");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Configure Default Value Secondry Check List  :: Admin > Opener > Secondry Checklist", testCaseId = "TC_53_Configure_Default_Value")
	public void configureDefaultValue() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminOpenerSecondaryChecklistPage pobj = new AdminOpenerSecondaryChecklistPage(driver);
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Responsible Department");
			fc.utobj().printTestStep("Add Responsible Department");

			AdminOpenerResponsibleDepartmentPageTest addDepartmentPage = new AdminOpenerResponsibleDepartmentPageTest();
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));
			addDepartmentPage.addDepartment(driver, responsibilityArea);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "openerautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporatePage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Groups");
			fc.utobj().printTestStep("Add Group");

			AdminOpenerManageGroupsPageTest addGroupsPage = new AdminOpenerManageGroupsPageTest();
			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			addGroupsPage.addGroups(driver, groupName);

			fc.utobj().printTestStep("Configure Default Vallue");
			fc.opener().opener_common().adminFranchiseOpenerSecondaryChecklist(driver);
			fc.utobj().clickElement(driver, pobj.configureDefaultValue);

			fc.utobj().selectValFromMultiSelect(driver, pobj.selectResponsibilityArea, responsibilityArea);

			fc.utobj().selectValFromMultiSelect(driver, pobj.selectContact, corpUser.getuserFullName());

			fc.utobj().selectValFromMultiSelect(driver, pobj.selectStoreType, storeType);

			fc.utobj().selectDropDown(driver, pobj.groupDropDown, groupName);

			String franchiseeAccess = dataSet.get("franchiseeAccess");
			fc.utobj().selectDropDown(driver, pobj.franchiseeAccessDropDown, franchiseeAccess);

			String priority = dataSet.get("priority");
			fc.utobj().selectDropDown(driver, pobj.priorityDropDown, priority);

			String criticalLevel = dataSet.get("criticalLevel");
			fc.utobj().selectDropDown(driver, pobj.criticalLevelDropDown, criticalLevel);

			String dependentOn = dataSet.get("dependentOn");
			String valueText = fc.utobj()
					.getElementByXpath(driver,
							".//select[@name='smSecondryChecklist_0referenceParent']/option[contains(text () , '"
									+ dependentOn + "')]")
					.getAttribute("value");
			fc.utobj().moveToElement(driver, pobj.dependentOnDropDown);
			Select sl = new Select(pobj.dependentOnDropDown);
			sl.selectByValue(valueText);

			if (fc.utobj().isSelected(driver, pobj.uploadFileRadioBtn)) {
				// do nothing
			} else {
				fc.utobj().clickElement(driver, pobj.uploadFileRadioBtn);
			}

			String file = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));

			System.out.println(file);
			fc.utobj().sendKeys(driver, pobj.attachmentBrowseBox, file);
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("Verify The Configure Default Value");

			String textResponsibility = driver
					.findElement(By.xpath(".//*[@id='ms-parentsmSecondryChecklist_0responsibilityArea']/button/span"))
					.getText().trim();
			String textContact = driver
					.findElement(By.xpath(".//*[@id='ms-parentsmSecondryChecklist_0contact']/button/span")).getText()
					.trim();
			String textStoreType = driver
					.findElement(By.xpath(".//*[@id='ms-parentsmSecondryChecklist_0storeTypeId']/button/span"))
					.getText().trim();

			if (textResponsibility.equalsIgnoreCase(responsibilityArea)
					&& textContact.contains(corpUser.getuserFullName()) && textStoreType.equalsIgnoreCase(storeType)) {
				// do nothing
			} else {
				fc.utobj().throwsException("was not able to Configure default value");
			}

			configureDefaultValue(driver, config);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Add Secondary Check List  with upload file option:: Admin > Opener > Secondary Checklist", testCaseId = "TC_54_Add_Secondary_Checklist_01")
	public void addSecondaryChecklistUploadFile() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminOpenerSecondaryChecklistPage pobj = new AdminOpenerSecondaryChecklistPage(driver);
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Responsible Department");
			fc.utobj().printTestStep("Add Responsible Department");

			AdminOpenerResponsibleDepartmentPageTest addDepartmentPage = new AdminOpenerResponsibleDepartmentPageTest();
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));
			addDepartmentPage.addDepartment(driver, responsibilityArea);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "openerautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporatePage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Groups");
			fc.utobj().printTestStep("Add Group");

			AdminOpenerManageGroupsPageTest addGroupsPage = new AdminOpenerManageGroupsPageTest();
			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			addGroupsPage.addGroups(driver, groupName);

			configureDefaultValue(driver, config);

			fc.utobj().printTestStep("Add Secondary Checklist");
			String checklistName = addSecondaryChecklist(driver, dataSet, config);

			fc.utobj().printTestStep("Add Task");
			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, checklistName));

			fc.utobj().clickElement(driver, pobj.addMoreBtn);

			String secondaryText = fc.utobj().generateTestData(dataSet.get("secondaryText"));
			fc.utobj().sendKeys(driver, pobj.itemNameTxtBox, secondaryText);

			fc.utobj().selectValFromMultiSelect(driver, pobj.selectResponsibilityArea, responsibilityArea);

			fc.utobj().selectValFromMultiSelect(driver, pobj.selectContact, corpUser.getuserFullName());

			fc.utobj().selectValFromMultiSelect(driver, pobj.selectStoreType, storeType);

			fc.utobj().selectDropDown(driver, pobj.groupDropDown, groupName);
			fc.utobj().selectDropDown(driver, pobj.franchiseeAccessDropDown, dataSet.get("franchiseeAccess"));
			fc.utobj().selectDropDown(driver, pobj.priorityDropDown, dataSet.get("priority"));
			fc.utobj().selectDropDown(driver, pobj.criticalLevelDropDown, dataSet.get("criticalLevel"));

			String dependentOn = dataSet.get("dependentOn");
			String valueText = fc.utobj()
					.getElementByXpath(driver,
							".//select[@name='smSecondryChecklist_0referenceParent']/option[contains(text () , '"
									+ dependentOn + "')]")
					.getAttribute("value");
			fc.utobj().moveToElement(driver, pobj.dependentOnDropDown);
			Select sl = new Select(pobj.dependentOnDropDown);
			sl.selectByValue(valueText);

			fc.utobj().sendKeys(driver, pobj.startScheduleDateTxtBox, "1");
			fc.utobj().selectDropDown(driver, pobj.scheduleStartDropDown, "Days Prior");
			fc.utobj().sendKeys(driver, pobj.completionScheduleDateTxtBox, "1");
			fc.utobj().selectDropDown(driver, pobj.scheduleCompletionDropDown, "Days Prior");

			fc.utobj().sendKeys(driver, pobj.startReminderTxtBox, "2");
			fc.utobj().sendKeys(driver, pobj.completionReminderTxtBox, "2");

			if (fc.utobj().isSelected(driver, pobj.uploadFileRadioBtn)) {
				// do nothing
			} else {
				fc.utobj().clickElement(driver, pobj.uploadFileRadioBtn);
			}

			String file = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			System.out.println(file);
			fc.utobj().sendKeys(driver, pobj.attachmentBrowseBox, file);
			fc.utobj().clickElement(driver, pobj.addBtn);

			try {

				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().clickElement(driver, pobj.noBtn);
				fc.utobj().switchFrameToDefault(driver);
			} catch (Exception e) {

			}

			fc.utobj().printTestStep("Verify The Add Task");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, secondaryText);

			if (isTextPresent == false) {
				fc.utobj().throwsSkipException("was not able to add Secondary checklist");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addSecondaryChecklist(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String checklistName) throws Exception {

		String testCaseId = "TC_Add_Secondary_Checklist_opener_03";
		String secondaryText = null;
		if (fc.utobj().validate(testCaseId)) {
			try {

				AdminOpenerSecondaryChecklistPage pobj = new AdminOpenerSecondaryChecklistPage(driver);
				AdminOpenerResponsibleDepartmentPageTest addDepartmentPage = new AdminOpenerResponsibleDepartmentPageTest();
				String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));
				addDepartmentPage.addDepartment(driver, responsibilityArea);

				AdminUsersManageCorporateUsersAddCorporateUserPageTest corporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

				String emailId = "openerautomation@staffex.com";
				CorporateUser corpUser = new CorporateUser();
				corpUser.setEmail(emailId);
				corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
				corporatePage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

				AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
				String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
				storeTypePage.addStoreType(driver, storeType);

				configureDefaultValue(driver, config);

				// add Secondary Checklist name
				fc.opener().opener_common().adminFranchiseOpenerSecondaryChecklist(driver);

				fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, checklistName));

				fc.utobj().clickElement(driver, pobj.addMoreBtn);

				secondaryText = fc.utobj().generateTestData(dataSet.get("secondaryText"));
				fc.utobj().sendKeys(driver, pobj.itemNameTxtBox, secondaryText);

				fc.utobj().selectValFromMultiSelect(driver, pobj.selectResponsibilityArea, responsibilityArea);

				fc.utobj().selectValFromMultiSelect(driver, pobj.selectContact, corpUser.getuserFullName());

				fc.utobj().selectValFromMultiSelect(driver, pobj.selectStoreType, storeType);

				fc.utobj().selectDropDown(driver, pobj.franchiseeAccessDropDown, dataSet.get("franchiseeAccess"));
				fc.utobj().selectDropDown(driver, pobj.priorityDropDown, dataSet.get("priority"));
				fc.utobj().selectDropDown(driver, pobj.criticalLevelDropDown, dataSet.get("criticalLevel"));

				String dependentOn = dataSet.get("dependentOn");
				String valueText = fc.utobj()
						.getElementByXpath(driver,
								".//select[@name='smSecondryChecklist_0referenceParent']/option[contains(text () , '"
										+ dependentOn + "')]")
						.getAttribute("value");
				fc.utobj().moveToElement(driver, pobj.dependentOnDropDown);
				Select sl = new Select(pobj.dependentOnDropDown);
				sl.selectByValue(valueText);

				if (dependentOn.equalsIgnoreCase("None (Timeless)")) {
					// do nothing
				} else if (dependentOn.equalsIgnoreCase("Expected Opening Date")) {

					fc.utobj().sendKeys(driver, pobj.startScheduleDateTxtBox, "1");
					fc.utobj().selectDropDown(driver, pobj.scheduleStartDropDown, "Days Prior");
					fc.utobj().sendKeys(driver, pobj.completionScheduleDateTxtBox, "1");
					fc.utobj().selectDropDown(driver, pobj.scheduleCompletionDropDown, "Days Prior");

					fc.utobj().sendKeys(driver, pobj.startReminderTxtBox, "2");
					fc.utobj().sendKeys(driver, pobj.completionReminderTxtBox, "2");

				} else if (dependentOn.equalsIgnoreCase("Multiple Checklists")) {

					fc.utobj().clickElement(driver, pobj.dependentOnDropDownBtn);
					// fc.utobj().sendKeys(driver, pobj.dependentOnDropDownTxBx,
					// txt);
					fc.utobj().clickElement(driver, pobj.dependentOnDropDownselectAll);

					fc.utobj().sendKeys(driver, pobj.startScheduleDateTxtBox, "1");
					fc.utobj().selectDropDown(driver, pobj.scheduleStartDropDown, "Days Prior");
					fc.utobj().sendKeys(driver, pobj.completionScheduleDateTxtBox, "1");
					fc.utobj().selectDropDown(driver, pobj.scheduleCompletionDropDown, "Days Prior");
					fc.utobj().sendKeys(driver, pobj.startReminderTxtBox, "2");
					fc.utobj().sendKeys(driver, pobj.completionReminderTxtBox, "2");

				}

				if (fc.utobj().isSelected(driver, pobj.uploadFileRadioBtn)) {
					// do nothing
				} else {
					fc.utobj().clickElement(driver, pobj.uploadFileRadioBtn);
				}

				String file = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
				System.out.println(file);
				fc.utobj().sendKeys(driver, pobj.attachmentBrowseBox, file);
				fc.utobj().clickElement(driver, pobj.addBtn);

				try {
					fc.commonMethods().switch_cboxIframe_frameId(driver);
					fc.utobj().clickElement(driver, pobj.noBtn);
					fc.utobj().switchFrameToDefault(driver);
				} catch (Exception e) {

				}

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to Add Secondary Checklist");

		}
		return secondaryText;
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Add Secondary Check List with webLink option:: Admin > Opener > Secondary Checklist", testCaseId = "TC_55_Add_Secondary_Checklist_02")
	public void addSecondaryChecklistWebLink() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminOpenerSecondaryChecklistPage pobj = new AdminOpenerSecondaryChecklistPage(driver);
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Responsible Department");
			fc.utobj().printTestStep("Add Responsible Department");

			AdminOpenerResponsibleDepartmentPageTest addDepartmentPage = new AdminOpenerResponsibleDepartmentPageTest();
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));
			addDepartmentPage.addDepartment(driver, responsibilityArea);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			String emailId = "openerautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporatePage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Groups");
			fc.utobj().printTestStep("Add Group");

			AdminOpenerManageGroupsPageTest addGroupsPage = new AdminOpenerManageGroupsPageTest();
			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			addGroupsPage.addGroups(driver, groupName);

			configureDefaultValue(driver, config);

			fc.utobj().printTestStep("Add Secondary Checklist");
			// add Secondary Checklist name
			String checklistName = addSecondaryChecklist(driver, dataSet, config);
			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, checklistName));

			fc.utobj().printTestStep("Add Task With WebLink Option");

			fc.utobj().clickElement(driver, pobj.addMoreBtn);

			String secondaryText = fc.utobj().generateTestData(dataSet.get("secondaryText"));
			fc.utobj().sendKeys(driver, pobj.itemNameTxtBox, secondaryText);

			fc.utobj().selectValFromMultiSelect(driver, pobj.selectResponsibilityArea, responsibilityArea);

			fc.utobj().selectValFromMultiSelect(driver, pobj.selectContact, corpUser.getuserFullName());

			fc.utobj().selectValFromMultiSelect(driver, pobj.selectStoreType, storeType);

			fc.utobj().selectDropDown(driver, pobj.groupDropDown, groupName);
			fc.utobj().selectDropDown(driver, pobj.franchiseeAccessDropDown, dataSet.get("franchiseeAccess"));
			fc.utobj().selectDropDown(driver, pobj.priorityDropDown, dataSet.get("priority"));
			fc.utobj().selectDropDown(driver, pobj.criticalLevelDropDown, dataSet.get("criticalLevel"));

			String dependentOn = dataSet.get("dependentOn");
			String valueText = fc.utobj()
					.getElementByXpath(driver,
							".//select[@name='smSecondryChecklist_0referenceParent']/option[contains(text () , '"
									+ dependentOn + "')]")
					.getAttribute("value");
			fc.utobj().moveToElement(driver, pobj.dependentOnDropDown);
			Select sl = new Select(pobj.dependentOnDropDown);
			sl.selectByValue(valueText);

			if (fc.utobj().isSelected(driver, pobj.webLinkRadioBtn)) {
				// do nothing
			} else {
				fc.utobj().clickElement(driver, pobj.webLinkRadioBtn);
			}

			fc.utobj().sendKeys(driver, pobj.webLinkTxBx, dataSet.get("webLinkUrl"));
			fc.utobj().clickElement(driver, pobj.addBtn);

			try {

				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().clickElement(driver, pobj.noBtn);
				fc.utobj().switchFrameToDefault(driver);
			} catch (Exception e) {

			}

			fc.utobj().printTestStep("Verify The Add Task");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, secondaryText);

			if (isTextPresent == false) {
				fc.utobj().throwsSkipException("was not able to add Secondary checklist");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Modify Secondary Check List  with upload file option at ActionImg:: Admin > Opener > Secondary Checklist", testCaseId = "TC_56_Modify_Secondary_Checklist")
	public void modifySecondaryChecklistActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminOpenerSecondaryChecklistPage pobj = new AdminOpenerSecondaryChecklistPage(driver);
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Add Secondary Checklist");
			String checklistName = addSecondaryChecklist(driver, dataSet, config);

			fc.utobj().printTestStep("Add Secondary Checklist With Task");
			String secondaryText = addSecondaryChecklist(driver, dataSet, config, checklistName);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Responsible Department");
			fc.utobj().printTestStep("Add Responsible Department");

			AdminOpenerResponsibleDepartmentPageTest addDepartmentPage = new AdminOpenerResponsibleDepartmentPageTest();
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));
			addDepartmentPage.addDepartment(driver, responsibilityArea);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Groups");
			fc.utobj().printTestStep("Add Group");

			AdminOpenerManageGroupsPageTest addGroupsPage = new AdminOpenerManageGroupsPageTest();
			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			addGroupsPage.addGroups(driver, groupName);

			fc.opener().opener_common().adminFranchiseOpenerSecondaryChecklist(driver);
			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, checklistName));

			fc.utobj().printTestStep("Modify The Task");

			/*
			 * //modify Action Img String
			 * text=fc.utobj().getElementByXpath(driver,".//*[.='"+
			 * secondaryText+
			 * "']/ancestor::tr/td/div[@id='menuBar']/layer")).getAttribute("id"
			 * ).trim(); String alterText=text.replace("Actions_dynamicmenu",
			 * ""); alterText=alterText.replace("Bar", "");
			 * 
			 * fc.utobj().clickElement(driver,
			 * fc.utobj().getElementByXpath(driver,".//*[.='"+secondaryText+
			 * "']/ancestor::tr/td/div[@id='menuBar']/layer/a/img")));
			 * fc.utobj().clickElement(driver,
			 * fc.utobj().getElementByXpath(driver,
			 * ".//div[@id='Actions_dynamicmenu"+ alterText+
			 * "Menu']/span[contains(text () , 'Modify')]")));
			 */

			fc.utobj().actionImgOption(driver, secondaryText, "Modify");

			secondaryText = fc.utobj().generateTestData(dataSet.get("secondaryText"));
			fc.utobj().sendKeys(driver, pobj.itemNameTxtBox, secondaryText);

			fc.utobj().selectValFromMultiSelect(driver, pobj.selectResponsibilityArea, responsibilityArea);
			fc.utobj().selectValFromMultiSelect(driver, pobj.selectStoreType, storeType);

			fc.utobj().selectDropDown(driver, pobj.groupDropDown, groupName);
			fc.utobj().selectDropDown(driver, pobj.franchiseeAccessDropDown, dataSet.get("franchiseeAccess"));
			fc.utobj().selectDropDown(driver, pobj.priorityDropDown, dataSet.get("priority"));
			fc.utobj().selectDropDown(driver, pobj.criticalLevelDropDown, dataSet.get("criticalLevel"));

			String dependentOn = dataSet.get("dependentOnModify");
			String valueText = fc.utobj()
					.getElementByXpath(driver,
							".//select[@name='smSecondryChecklist_0referenceParent']/option[contains(text () , '"
									+ dependentOn + "')]")
					.getAttribute("value");
			fc.utobj().moveToElement(driver, pobj.dependentOnDropDown);
			Select sl = new Select(pobj.dependentOnDropDown);
			sl.selectByValue(valueText);

			if (fc.utobj().isSelected(driver, pobj.webLinkRadioBtn)) {
				// do nothing
			} else {
				fc.utobj().clickElement(driver, pobj.webLinkRadioBtn);
			}

			fc.utobj().sendKeys(driver, pobj.webLinkTxBx, dataSet.get("webLinkUrl"));

			try {
				List<WebElement> listRadioButton = pobj.applyToAll;
				if (fc.utobj().isSelected(driver,listRadioButton.get(0))) {
					// do nothing
				} else {
					fc.utobj().clickElement(driver, listRadioButton.get(0));
				}
			} catch (Exception e) {

			}

			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("Verify The Modify Task");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, secondaryText);

			if (isTextPresent == false) {
				fc.utobj().throwsSkipException("was not able to modify Secondary checklist");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Delete Secondary Check List  with upload file option at ActionImg:: Admin > Opener > Secondary Checklist", testCaseId = "TC_57_Delete_Secondary_Checklist")
	public void deleteSecondaryChecklistActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminOpenerSecondaryChecklistPage pobj = new AdminOpenerSecondaryChecklistPage(driver);
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Add Secondary Checklist");
			String checklistName = addSecondaryChecklist(driver, dataSet, config);

			fc.utobj().printTestStep("Add Secondary Checklist With Task");
			String secondaryText = addSecondaryChecklist(driver, dataSet, config, checklistName);

			fc.utobj().printTestStep("Delete The Task");
			String text = driver
					.findElement(By.xpath(".//*[.='" + secondaryText + "']/ancestor::tr/td/div[@id='menuBar']/layer"))
					.getAttribute("id").trim();
			String alterText = text.replace("Actions_dynamicmenu", "");
			alterText = alterText.replace("Bar", "");

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,(".//*[.='" + secondaryText + "']/ancestor::tr/td/div[@id='menuBar']/layer/a/img")));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//div[@id='Actions_dynamicmenu" + alterText + "Menu']/span[contains(text () , 'Delete')]"));

			fc.utobj().acceptAlertBox(driver);

			try {

				fc.commonMethods().switch_cboxIframe_frameId(driver);
				if (fc.utobj().isSelected(driver,pobj.applyToAll.get(0))) {
					// do nothing
				} else {
					fc.utobj().clickElement(driver, pobj.applyToAll.get(0));
				}

				fc.utobj().clickElement(driver, pobj.cboxDeleteBtn);
				fc.utobj().clickElement(driver, pobj.cboxCloseBtn);
				fc.utobj().switchFrameToDefault(driver);

			} catch (Exception e) {

			}

			fc.utobj().printTestStep("Verify The Delete Task");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, secondaryText);

			if (isTextPresent == true) {
				fc.utobj().throwsSkipException("Was not able to delete Secondary Checklist");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Add description at Secondary Check List with upload file option at ActionImg:: Admin > Opener > Secondary Checklist", testCaseId = "TC_58_Add_Description_Secondary")
	public void addDescriptionSecondaryActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminOpenerSecondaryChecklistPage pobj = new AdminOpenerSecondaryChecklistPage(driver);
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Add Secondary Checklist");
			String checklistName = addSecondaryChecklist(driver, dataSet, config);

			fc.utobj().printTestStep("Add Secondary Checklist With Task");
			String secondaryText = addSecondaryChecklist(driver, dataSet, config, checklistName);

			fc.utobj().printTestStep("Add Description");
			String text = driver
					.findElement(By.xpath(".//*[.='" + secondaryText + "']/ancestor::tr/td/div[@id='menuBar']/layer"))
					.getAttribute("id").trim();
			String alterText = text.replace("Actions_dynamicmenu", "");
			alterText = alterText.replace("Bar", "");

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[.='" + secondaryText + "']/ancestor::tr/td/div[@id='menuBar']/layer/a/img"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//div[@id='Actions_dynamicmenu"
					+ alterText + "Menu']/span[contains(text () , 'Add Description')]"));

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().sendKeys(driver, pobj.secondaryDescriptionArea, description);
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Add Description");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[.='" + secondaryText + "']/ancestor::tr/td/div[@id='menuBar']/layer/a/img"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//div[@id='Actions_dynamicmenu"
					+ alterText + "Menu']/span[contains(text () , 'Modify Description')]"));

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String isTextPresent = fc.utobj().getText(driver, pobj.secondaryDescriptionArea);

			if (isTextPresent.equalsIgnoreCase(description)) {
				// do nothing
			} else {
				fc.utobj().throwsSkipException("was not able to add description");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Modify description at Secondary Check List with upload file option at ActionImg:: Admin > Opener > Secondary Checklist", testCaseId = "TC_59_Modify_Description_Secondary")
	public void modifyDescriptionActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminOpenerSecondaryChecklistPage pobj = new AdminOpenerSecondaryChecklistPage(driver);
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Add Secondary Checklist");
			String checklistName = addSecondaryChecklist(driver, dataSet, config);

			fc.utobj().printTestStep("Add Secondary Checklist With Task");
			String secondaryText = addSecondaryChecklist(driver, dataSet, config, checklistName);

			fc.utobj().printTestStep("Add Description");
			String text = driver
					.findElement(By.xpath(".//*[.='" + secondaryText + "']/ancestor::tr/td/div[@id='menuBar']/layer"))
					.getAttribute("id").trim();
			String alterText = text.replace("Actions_dynamicmenu", "");
			alterText = alterText.replace("Bar", "");

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[.='" + secondaryText + "']/ancestor::tr/td/div[@id='menuBar']/layer/a/img"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//div[@id='Actions_dynamicmenu"
					+ alterText + "Menu']/span[contains(text () , 'Add Description')]"));

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().sendKeys(driver, pobj.secondaryDescriptionArea, description);
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Modify The Description");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[.='" + secondaryText + "']/ancestor::tr/td/div[@id='menuBar']/layer/a/img"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//div[@id='Actions_dynamicmenu"
					+ alterText + "Menu']/span[contains(text () , 'Modify Description')]"));

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().sendKeys(driver, pobj.secondaryDescriptionArea, description);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Modify Description");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[.='" + secondaryText + "']/ancestor::tr/td/div[@id='menuBar']/layer/a/img"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//div[@id='Actions_dynamicmenu"
					+ alterText + "Menu']/span[contains(text () , 'Modify Description')]"));

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String isTextPresent = fc.utobj().getText(driver, pobj.secondaryDescriptionArea);

			if (isTextPresent.equalsIgnoreCase(description)) {
				// do nothing
			} else {
				fc.utobj().throwsSkipException("was not able to modify description");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "opener" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Modify Secondary Check List  with upload file option at Action Button:: Admin > Opener > Secondary Checklist", testCaseId = "TC_60_Modfiy_Secondary_Checklist01")
	public void modifySecondaryChecklistActionButton() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminOpenerSecondaryChecklistPage pobj = new AdminOpenerSecondaryChecklistPage(driver);
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Responsible Department");
			fc.utobj().printTestStep("Add Responsible Department");

			AdminOpenerResponsibleDepartmentPageTest addDepartmentPage = new AdminOpenerResponsibleDepartmentPageTest();
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));
			addDepartmentPage.addDepartment(driver, responsibilityArea);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Groups");
			fc.utobj().printTestStep("Add Group");

			AdminOpenerManageGroupsPageTest addGroupsPage = new AdminOpenerManageGroupsPageTest();
			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			addGroupsPage.addGroups(driver, groupName);

			fc.utobj().printTestStep("Add Secondary Checklist");
			String checklistName = addSecondaryChecklist(driver, dataSet, config);

			fc.utobj().printTestStep("Add Secondary Checklist With Task");
			String secondaryText = addSecondaryChecklist(driver, dataSet, config, checklistName);

			fc.utobj().printTestStep("Modify The Task");

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[.='" + secondaryText
					+ "']//ancestor::tr[@class='bText12' or @class='bText12 grAltRw3']/td/input"));
			fc.utobj().selectActionMenuItems(driver, "Modify");

			secondaryText = fc.utobj().generateTestData(dataSet.get("secondaryText"));
			fc.utobj().sendKeys(driver, pobj.itemNameTxtBox, secondaryText);

			fc.utobj().selectValFromMultiSelect(driver, pobj.selectResponsibilityArea, responsibilityArea);
			fc.utobj().selectValFromMultiSelect(driver, pobj.selectStoreType, storeType);

			fc.utobj().selectDropDown(driver, pobj.groupDropDown, groupName);
			fc.utobj().selectDropDown(driver, pobj.franchiseeAccessDropDown, dataSet.get("franchiseeAccess"));
			fc.utobj().selectDropDown(driver, pobj.priorityDropDown, dataSet.get("priority"));
			fc.utobj().selectDropDown(driver, pobj.criticalLevelDropDown, dataSet.get("criticalLevel"));

			String dependentOn = dataSet.get("dependentOnModify");
			String valueText = fc.utobj()
					.getElementByXpath(driver,
							".//select[@name='smSecondryChecklist_0referenceParent']/option[contains(text () , '"
									+ dependentOn + "')]")
					.getAttribute("value");
			fc.utobj().moveToElement(driver, pobj.dependentOnDropDown);
			Select sl = new Select(pobj.dependentOnDropDown);
			sl.selectByValue(valueText);

			if (fc.utobj().isSelected(driver, pobj.webLinkRadioBtn)) {
				// do nothing
			} else {
				fc.utobj().clickElement(driver, pobj.webLinkRadioBtn);
			}

			fc.utobj().sendKeys(driver, pobj.webLinkTxBx, dataSet.get("webLinkUrl"));

			try {
				List<WebElement> listRadioButton = pobj.applyToAll;
				if (fc.utobj().isSelected(driver,listRadioButton.get(0))) {
					// do nothing
				} else {
					fc.utobj().clickElement(driver, listRadioButton.get(0));
				}
			} catch (Exception e) {

			}

			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("Verify The Modify Task");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, secondaryText);

			if (isTextPresent == false) {
				fc.utobj().throwsSkipException("was not able modify Secondary checklist");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Delete Secondary Check List  with upload file option at Action Button:: Admin > Opener > Secondary Checklist", testCaseId = "TC_61_Delete_Secondary_Checklist01")
	public void deleteSecondaryChecklistActionButton() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminOpenerSecondaryChecklistPage pobj = new AdminOpenerSecondaryChecklistPage(driver);
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Add Secondary Checklist");
			String checklistName = addSecondaryChecklist(driver, dataSet, config);

			fc.utobj().printTestStep("Add Secondary Checklist With Task");
			String secondaryText = addSecondaryChecklist(driver, dataSet, config, checklistName);

			fc.utobj().printTestStep("Delete The Task");
			// modify Action Button

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[.='" + secondaryText
					+ "']//ancestor::tr[@class='bText12' or @class='bText12 grAltRw3']/td/input"));

			fc.utobj().selectActionMenuItems(driver, "Delete");

			fc.utobj().acceptAlertBox(driver);

			try {
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				if (fc.utobj().isSelected(driver,pobj.applyToAll.get(1))) {
					// do nothing
				} else {
					fc.utobj().clickElement(driver, pobj.applyToAll.get(1));
				}

				fc.utobj().clickElement(driver, pobj.cboxDeleteBtn);
				fc.utobj().clickElement(driver, pobj.cboxCloseBtn);
				fc.utobj().switchFrameToDefault(driver);

			} catch (Exception e) {

			}

			fc.utobj().printTestStep("Verify The Delete Task");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, secondaryText);

			if (isTextPresent == true) {
				fc.utobj().throwsSkipException("Was not able to delete Secondary Checklist");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Modify Contact(s) Secondary Check List  with upload file option at Action Button:: Admin > Opener > Secondary Checklist", testCaseId = "TC_62_Modify_Contact_Secondary")
	public void modifyContactActionButton() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminOpenerSecondaryChecklistPage pobj = new AdminOpenerSecondaryChecklistPage(driver);
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Add Secondary Checklist");
			String checklistName = addSecondaryChecklist(driver, dataSet, config);

			fc.utobj().printTestStep("Add Secondary Checklist With Task");
			String secondaryText = addSecondaryChecklist(driver, dataSet, config, checklistName);

			fc.utobj().printTestStep("Modify The Contact");
			// modify Action Button

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[.='" + secondaryText
					+ "']//ancestor::tr[@class='bText12' or @class='bText12 grAltRw3']/td/input"));

			fc.utobj().selectActionMenuItems(driver, "Modify Contact(s)");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.modifyContactSelectBtn);
			fc.utobj().sendKeys(driver, pobj.modifyContactSearchBox, "Regional User");
			fc.utobj().clickElement(driver, pobj.modifyContactSelectAll);
			fc.utobj().clickElement(driver, pobj.modifyContactSelectBtn);

			try {

				if (fc.utobj().isSelected(driver,pobj.applyToAll.get(1))) {
					// do nothing
				} else {
					fc.utobj().clickElement(driver, pobj.applyToAll.get(1));
				}
			} catch (Exception e) {

			}

			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Modify Contact");
			String textPresent = fc.utobj().getText(driver, fc.utobj().getElementByXpath(driver, ".//*[.='"
					+ secondaryText + "']//ancestor::tr[@class='bText12' or @class='bText12 grAltRw3']/td[5]"));

			if (!textPresent.contains("Regional")) {
				fc.utobj().throwsSkipException("was not able to modify Contact");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Modify Secondary Check List  with upload file option at Action Button Bottom:: Admin > Opener > Secondary Checklist", testCaseId = "TC_64_Modify_Secondary_Checklist02")
	public void modifySecondaryChecklistBottom() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminOpenerSecondaryChecklistPage pobj = new AdminOpenerSecondaryChecklistPage(driver);
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Responsible Department");
			fc.utobj().printTestStep("Add Responsible Department");

			AdminOpenerResponsibleDepartmentPageTest addDepartmentPage = new AdminOpenerResponsibleDepartmentPageTest();
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));
			addDepartmentPage.addDepartment(driver, responsibilityArea);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Groups");
			fc.utobj().printTestStep("Add Group");

			AdminOpenerManageGroupsPageTest addGroupsPage = new AdminOpenerManageGroupsPageTest();
			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			addGroupsPage.addGroups(driver, groupName);

			fc.utobj().printTestStep("Add Secondary Checklist");
			String checklistName = addSecondaryChecklist(driver, dataSet, config);

			fc.utobj().printTestStep("Add Secondary Checklist With Task");
			String secondaryText = addSecondaryChecklist(driver, dataSet, config, checklistName);

			fc.utobj().printTestStep("Modify The Secondary Checklist Task By Bottom Button Option");

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[.='" + secondaryText
					+ "']//ancestor::tr[@class='bText12' or @class='bText12 grAltRw3']/td/input"));
			fc.utobj().clickElement(driver, pobj.bottomModifyBtn);

			secondaryText = fc.utobj().generateTestData(dataSet.get("secondaryText"));
			fc.utobj().sendKeys(driver, pobj.itemNameTxtBox, secondaryText);

			fc.utobj().selectValFromMultiSelect(driver, pobj.selectResponsibilityArea, responsibilityArea);
			fc.utobj().selectValFromMultiSelect(driver, pobj.selectStoreType, storeType);
			fc.utobj().selectDropDown(driver, pobj.groupDropDown, groupName);
			fc.utobj().selectDropDown(driver, pobj.franchiseeAccessDropDown, dataSet.get("franchiseeAccess"));
			fc.utobj().selectDropDown(driver, pobj.priorityDropDown, dataSet.get("priority"));
			fc.utobj().selectDropDown(driver, pobj.criticalLevelDropDown, dataSet.get("criticalLevel"));

			String dependentOn = dataSet.get("dependentOnModify");
			String valueText = fc.utobj()
					.getElementByXpath(driver,
							".//select[@name='smSecondryChecklist_0referenceParent']/option[contains(text () , '"
									+ dependentOn + "')]")
					.getAttribute("value");
			fc.utobj().moveToElement(driver, pobj.dependentOnDropDown);
			Select sl = new Select(pobj.dependentOnDropDown);
			sl.selectByValue(valueText);

			if (fc.utobj().isSelected(driver, pobj.webLinkRadioBtn)) {
				// do nothing
			} else {
				fc.utobj().clickElement(driver, pobj.webLinkRadioBtn);
			}

			fc.utobj().sendKeys(driver, pobj.webLinkTxBx, dataSet.get("webLinkUrl"));

			try {
				List<WebElement> listRadioButton = pobj.applyToAll;
				if (fc.utobj().isSelected(driver,listRadioButton.get(0))) {
					// do nothing
				} else {
					fc.utobj().clickElement(driver, listRadioButton.get(0));
				}
			} catch (Exception e) {

			}

			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("Verify The Modify Task");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, secondaryText);

			if (isTextPresent == false) {
				fc.utobj().throwsSkipException("was not able to modify Secondary checklist At bottom btn");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Delete Secondary Check List  with upload file option at Bottom:: Admin > Opener > Secondary Checklist", testCaseId = "TC_65_Delete_Secondary_Checklist02")
	public void deleteSecondaryChecklistBottom() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminOpenerSecondaryChecklistPage pobj = new AdminOpenerSecondaryChecklistPage(driver);

			fc.utobj().printTestStep("Add Secondary Checklist");
			String checklistName = addSecondaryChecklist(driver, dataSet, config);

			fc.utobj().printTestStep("Add Secondary Checklist With Task");
			String secondaryText = addSecondaryChecklist(driver, dataSet, config, checklistName);

			fc.utobj().printTestStep("Delete Task");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[.='" + secondaryText
					+ "']//ancestor::tr[@class='bText12' or @class='bText12 grAltRw3']/td/input"));
			fc.utobj().clickElement(driver, pobj.bottomDeleteBtn);

			fc.utobj().acceptAlertBox(driver);

			try {

				fc.commonMethods().switch_cboxIframe_frameId(driver);
				if (fc.utobj().isSelected(driver,pobj.applyToAll.get(0))) {
					// do nothing
				} else {
					fc.utobj().clickElement(driver, pobj.applyToAll.get(0));
				}

				fc.utobj().clickElement(driver, pobj.cboxDeleteBtn);
				fc.utobj().clickElement(driver, pobj.cboxCloseBtn);
				fc.utobj().switchFrameToDefault(driver);

			} catch (Exception e) {

			}

			fc.utobj().printTestStep("Verify The Delete Task");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, secondaryText);

			if (isTextPresent == true) {
				fc.utobj().throwsSkipException("Was not able to delete Secondary Checklist");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void addSecondaryChecklistWithTask(WebDriver driver, Map<String, String> dataSet,
			String secondaryChecklistName, String description, String responsibilityArea, String userName,
			String storeType, File file, String referenceDate, String task, Map<String, String> config)
			throws Exception {

		String testCaseId = "TC_Add_Secondary_Checklist_With_Task_Opener_03";

		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminOpenerSecondaryChecklistPage pobj = new AdminOpenerSecondaryChecklistPage(driver);
				configureDefaultValue(driver, config);
				fc.opener().opener_common().adminFranchiseOpenerSecondaryChecklist(driver);
				// add secondary Checklist
				fc.utobj().clickElement(driver, pobj.addSecondaryBtn);
				fc.utobj().sendKeys(driver, pobj.checklistNameTxtBox, secondaryChecklistName);
				fc.utobj().sendKeys(driver, pobj.checklistDescriptionTxtBox, description);
				fc.utobj().clickElement(driver, pobj.addBtn);

				String text = driver
						.findElement(By.xpath(
								".//*[.='" + secondaryChecklistName + "']/ancestor::tr/td/div[@id='menuBar']/layer"))
						.getAttribute("id").trim();
				String alterText = text.replace("Actions_dynamicmenu", "");
				alterText = alterText.replace("Bar", "");

				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[.='" + secondaryChecklistName + "']/ancestor::tr/td/div[@id='menuBar']/layer/a/img"));
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//div[@id='Actions_dynamicmenu" + alterText + "Menu']/span[contains(text () , 'Add Task')]"));

				fc.utobj().sendKeys(driver, pobj.itemNameTxtBox, task);

				fc.utobj().selectValFromMultiSelect(driver, pobj.selectResponsibilityArea, responsibilityArea);

				fc.utobj().selectValFromMultiSelect(driver, pobj.selectContact, userName);

				fc.utobj().selectValFromMultiSelect(driver, pobj.selectStoreType, storeType);

				String franchiseeAccess = dataSet.get("franchiseeAccess");
				fc.utobj().selectDropDown(driver, pobj.franchiseeAccessDropDown, franchiseeAccess);

				String priority = dataSet.get("priority");
				fc.utobj().selectDropDown(driver, pobj.priorityDropDown, priority);

				String criticalLevel = dataSet.get("criticalLevel");
				fc.utobj().selectDropDown(driver, pobj.criticalLevelDropDown, criticalLevel);

				if (dataSet.get("dependentOn").equalsIgnoreCase("Multiple Checklists")) {

					String dependentOn = dataSet.get("dependentOn");
					String valueText = fc.utobj()
							.getElementByXpath(driver,
									".//select[@name='smSecondaryChecklist_0referenceParent']/option[contains(text () , '"
											+ dependentOn + "')]")
							.getAttribute("value");
					fc.utobj().moveToElement(driver, pobj.dependentOnDropDown);
					Select sl = new Select(pobj.dependentOnDropDown);
					sl.selectByValue(valueText);

					fc.utobj().clickElement(driver, pobj.selectDropDownBtn);
					fc.utobj().clickElement(driver, pobj.selectAll);

					fc.utobj().sendKeys(driver, pobj.startScheduleDateTxtBox, "1");
					fc.utobj().selectDropDown(driver, pobj.scheduleStartDropDown, "Days Prior");
					fc.utobj().sendKeys(driver, pobj.completionScheduleDateTxtBox, "1");
					fc.utobj().selectDropDown(driver, pobj.scheduleCompletionDropDown, "Days Prior");

					fc.utobj().sendKeys(driver, pobj.startReminderTxtBox, "2");
					fc.utobj().sendKeys(driver, pobj.completionReminderTxtBox, "2");

				} else if (dataSet.get("dependentOn").equalsIgnoreCase("Expected Opening Date")) {

					String dependentOn = dataSet.get("dependentOn");
					String valueText = fc.utobj()
							.getElementByXpath(driver,
									".//select[@name='smSecondryChecklist_0referenceParent']/option[contains(text () , '"
											+ dependentOn + "')]")
							.getAttribute("value");
					fc.utobj().moveToElement(driver, pobj.dependentOnDropDown);
					Select sl = new Select(pobj.dependentOnDropDown);
					sl.selectByValue(valueText);

					fc.utobj().sendKeys(driver, pobj.startScheduleDateTxtBox, "2");
					fc.utobj().selectDropDown(driver, pobj.scheduleStartDropDown, "Days Prior");
					fc.utobj().sendKeys(driver, pobj.completionScheduleDateTxtBox, "2");
					fc.utobj().selectDropDown(driver, pobj.scheduleCompletionDropDown, "Days Prior");

					fc.utobj().sendKeys(driver, pobj.startReminderTxtBox, "2");
					fc.utobj().sendKeys(driver, pobj.completionReminderTxtBox, "2");

				} else if (dataSet.get("dependentOn").equalsIgnoreCase("None (Timeless)")) {

					String dependentOn = dataSet.get("dependentOn");
					String valueText = fc.utobj()
							.getElementByXpath(driver,
									".//select[@name='smSecondryChecklist_0referenceParent']/option[contains(text () , '"
											+ dependentOn + "')]")
							.getAttribute("value");
					fc.utobj().moveToElement(driver, pobj.dependentOnDropDown);
					Select sl = new Select(pobj.dependentOnDropDown);
					sl.selectByValue(valueText);
				} else {

					String valueText = fc.utobj()
							.getElementByXpath(driver,
									".//select[@name='smSecondryChecklist_0referenceParent']/option[contains(text () , '"
											+ referenceDate + "')]")
							.getAttribute("value");
					fc.utobj().moveToElement(driver, pobj.dependentOnDropDown);
					Select sl = new Select(pobj.dependentOnDropDown);
					sl.selectByValue(valueText);

					fc.utobj().sendKeys(driver, pobj.startScheduleDateTxtBox, "1");
					fc.utobj().selectDropDown(driver, pobj.scheduleStartDropDown, "Days Prior");
					fc.utobj().sendKeys(driver, pobj.completionScheduleDateTxtBox, "1");
					fc.utobj().selectDropDown(driver, pobj.scheduleCompletionDropDown, "Days Prior");

					fc.utobj().sendKeys(driver, pobj.startReminderTxtBox, "2");
					fc.utobj().sendKeys(driver, pobj.completionReminderTxtBox, "2");

				}

				if (fc.utobj().isSelected(driver, pobj.uploadFileRadioBtn)) {
					// do nothing
				} else {
					fc.utobj().clickElement(driver, pobj.uploadFileRadioBtn);
				}

				fc.utobj().sendKeys(driver, pobj.attachmentBrowseBox, file.getAbsolutePath());
				fc.utobj().clickElement(driver, pobj.addBtn);

				try {
					fc.commonMethods().switch_cboxIframe_frameId(driver);
					fc.utobj().clickElement(driver, pobj.yesBtn);
					fc.utobj().switchFrameToDefault(driver);
				} catch (Exception e) {

				}

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to Add Secondary Checklist With Task");

		}

	}

	// with Group
	public void addSecondaryChecklistWithTaskWithGroup(WebDriver driver, Map<String, String> dataSet,
			String secondaryChecklistName, String description, String responsibilityArea, String userName,
			String storeType, String groupName, File file, String referenceDate, String task,
			Map<String, String> config) throws Exception {

		String testCaseId = "TC_Add_Secondary_Checklist_With_Task_Opener_03";

		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminOpenerSecondaryChecklistPage pobj = new AdminOpenerSecondaryChecklistPage(driver);
				configureDefaultValue(driver, config);
				fc.opener().opener_common().adminFranchiseOpenerSecondaryChecklist(driver);
				// add secondary Checklist
				fc.utobj().clickElement(driver, pobj.addSecondaryBtn);
				fc.utobj().sendKeys(driver, pobj.checklistNameTxtBox, secondaryChecklistName);
				fc.utobj().sendKeys(driver, pobj.checklistDescriptionTxtBox, description);
				fc.utobj().clickElement(driver, pobj.addBtn);

				String text = driver
						.findElement(By.xpath(
								".//*[.='" + secondaryChecklistName + "']/ancestor::tr/td/div[@id='menuBar']/layer"))
						.getAttribute("id").trim();
				String alterText = text.replace("Actions_dynamicmenu", "");
				alterText = alterText.replace("Bar", "");

				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[.='" + secondaryChecklistName + "']/ancestor::tr/td/div[@id='menuBar']/layer/a/img"));
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//div[@id='Actions_dynamicmenu" + alterText + "Menu']/span[contains(text () , 'Add Task')]"));

				fc.utobj().sendKeys(driver, pobj.itemNameTxtBox, task);
				fc.utobj().selectValFromMultiSelect(driver, pobj.selectResponsibilityArea, responsibilityArea);
				fc.utobj().selectValFromMultiSelect(driver, pobj.selectContact, userName);
				fc.utobj().selectValFromMultiSelect(driver, pobj.selectStoreType, storeType);

				fc.utobj().selectDropDown(driver, pobj.groupDropDown, groupName);
				String franchiseeAccess = dataSet.get("franchiseeAccess");
				fc.utobj().selectDropDown(driver, pobj.franchiseeAccessDropDown, franchiseeAccess);

				String priority = dataSet.get("priority");
				fc.utobj().selectDropDown(driver, pobj.priorityDropDown, priority);

				String criticalLevel = dataSet.get("criticalLevel");
				fc.utobj().selectDropDown(driver, pobj.criticalLevelDropDown, criticalLevel);

				if (dataSet.get("dependentOn").equalsIgnoreCase("Multiple Checklists")) {

					String dependentOn = dataSet.get("dependentOn");
					String valueText = fc.utobj()
							.getElementByXpath(driver,
									".//select[@name='smSecondaryChecklist_0referenceParent']/option[contains(text () , '"
											+ dependentOn + "')]")
							.getAttribute("value");
					fc.utobj().moveToElement(driver, pobj.dependentOnDropDown);
					Select sl = new Select(pobj.dependentOnDropDown);
					sl.selectByValue(valueText);

					fc.utobj().clickElement(driver, pobj.selectDropDownBtn);
					fc.utobj().clickElement(driver, pobj.selectAll);

					fc.utobj().sendKeys(driver, pobj.startScheduleDateTxtBox, "1");
					fc.utobj().selectDropDown(driver, pobj.scheduleStartDropDown, "Days Prior");
					fc.utobj().sendKeys(driver, pobj.completionScheduleDateTxtBox, "1");
					fc.utobj().selectDropDown(driver, pobj.scheduleCompletionDropDown, "Days Prior");

					fc.utobj().sendKeys(driver, pobj.startReminderTxtBox, "2");
					fc.utobj().sendKeys(driver, pobj.completionReminderTxtBox, "2");

				} else if (dataSet.get("dependentOn").equalsIgnoreCase("Expected Opening Date")) {

					String dependentOn = dataSet.get("dependentOn");
					String valueText = fc.utobj()
							.getElementByXpath(driver,
									".//select[@name='smSecondryChecklist_0referenceParent']/option[contains(text () , '"
											+ dependentOn + "')]")
							.getAttribute("value");
					fc.utobj().moveToElement(driver, pobj.dependentOnDropDown);
					Select sl = new Select(pobj.dependentOnDropDown);
					sl.selectByValue(valueText);

					fc.utobj().sendKeys(driver, pobj.startScheduleDateTxtBox, "2");
					fc.utobj().selectDropDown(driver, pobj.scheduleStartDropDown, "Days Prior");
					fc.utobj().sendKeys(driver, pobj.completionScheduleDateTxtBox, "2");
					fc.utobj().selectDropDown(driver, pobj.scheduleCompletionDropDown, "Days Prior");

					fc.utobj().sendKeys(driver, pobj.startReminderTxtBox, "2");
					fc.utobj().sendKeys(driver, pobj.completionReminderTxtBox, "2");

				} else if (dataSet.get("dependentOn").equalsIgnoreCase("None (Timeless)")) {

					String dependentOn = dataSet.get("dependentOn");
					String valueText = fc.utobj()
							.getElementByXpath(driver,
									".//select[@name='smSecondryChecklist_0referenceParent']/option[contains(text () , '"
											+ dependentOn + "')]")
							.getAttribute("value");
					fc.utobj().moveToElement(driver, pobj.dependentOnDropDown);
					Select sl = new Select(pobj.dependentOnDropDown);
					sl.selectByValue(valueText);
				} else {

					String valueText = fc.utobj()
							.getElementByXpath(driver,
									".//select[@name='smSecondryChecklist_0referenceParent']/option[contains(text () , '"
											+ referenceDate + "')]")
							.getAttribute("value");
					fc.utobj().moveToElement(driver, pobj.dependentOnDropDown);
					Select sl = new Select(pobj.dependentOnDropDown);
					sl.selectByValue(valueText);

					fc.utobj().sendKeys(driver, pobj.startScheduleDateTxtBox, "1");
					fc.utobj().selectDropDown(driver, pobj.scheduleStartDropDown, "Days Prior");
					fc.utobj().sendKeys(driver, pobj.completionScheduleDateTxtBox, "1");
					fc.utobj().selectDropDown(driver, pobj.scheduleCompletionDropDown, "Days Prior");

					fc.utobj().sendKeys(driver, pobj.startReminderTxtBox, "2");
					fc.utobj().sendKeys(driver, pobj.completionReminderTxtBox, "2");

				}

				if (fc.utobj().isSelected(driver, pobj.uploadFileRadioBtn)) {
					// do nothing
				} else {
					fc.utobj().clickElement(driver, pobj.uploadFileRadioBtn);
				}

				fc.utobj().sendKeys(driver, pobj.attachmentBrowseBox, file.getAbsolutePath());
				fc.utobj().clickElement(driver, pobj.addBtn);

				try {
					fc.commonMethods().switch_cboxIframe_frameId(driver);
					fc.utobj().clickElement(driver, pobj.yesBtn);
					fc.utobj().switchFrameToDefault(driver);
				} catch (Exception e) {

				}

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to Add Secondary Checklist With Task");

		}

	}

	public void configureDefaultValue(WebDriver driver, Map<String, String> config) throws Exception {

		String testCaseId = "TC_Configure_Default_Secondary_Checklist_01";

		if (fc.utobj().configurationTestCaseToExecute(testCaseId)) {

			try {
				AdminOpenerSecondaryChecklistPage pobj = new AdminOpenerSecondaryChecklistPage(driver);
				fc.opener().opener_common().adminFranchiseOpenerSecondaryChecklist(driver);
				fc.utobj().clickElement(driver, pobj.configureDefaultValueBtnAtHome);

				String text1 = fc.utobj().getText(driver, fc.utobj().getElementByXpath(driver,
						".//*[@id='ms-parentsmSecondryChecklist_0responsibilityArea']/button/span"));

				if (text1.equalsIgnoreCase("All Selected")) {
					fc.utobj().clickElement(driver, pobj.responsibilityAreaDropDownBtn);
					fc.utobj().clickElement(driver, pobj.responsibilityAreaDropDownSelectAll);
					fc.utobj().clickElement(driver, pobj.responsibilityAreaDropDownBtn);
				} else {
					fc.utobj().clickElement(driver, pobj.responsibilityAreaDropDownBtn);
					fc.utobj().clickElement(driver, pobj.responsibilityAreaDropDownSelectAll);
					fc.utobj().clickElement(driver, pobj.responsibilityAreaDropDownSelectAll);
					fc.utobj().clickElement(driver, pobj.responsibilityAreaDropDownBtn);
				}

				String text2 = fc.utobj().getText(driver, fc.utobj().getElementByXpath(driver,
						".//*[@id='ms-parentsmSecondryChecklist_0contact']/button/span"));

				if (text2.equalsIgnoreCase("All Selected")) {
					fc.utobj().clickElement(driver, pobj.ContactDropDown);
					fc.utobj().clickElement(driver, pobj.ContactDropDownSelectAll);
					fc.utobj().clickElement(driver, pobj.ContactDropDown);
				} else {
					fc.utobj().clickElement(driver, pobj.ContactDropDown);
					fc.utobj().clickElement(driver, pobj.ContactDropDownSelectAll);
					fc.utobj().clickElement(driver, pobj.ContactDropDownSelectAll);
					fc.utobj().clickElement(driver, pobj.ContactDropDown);
				}

				String text3 = fc.utobj().getText(driver, driver
						.findElement(By.xpath(".//*[@id='ms-parentsmSecondryChecklist_0storeTypeId']/button/span")));

				if (text3.equalsIgnoreCase("All Selected")) {
					fc.utobj().clickElement(driver, pobj.StoreTypeDropDown);
					fc.utobj().clickElement(driver, pobj.StoreTypeDropDownSelectAll);
					fc.utobj().clickElement(driver, pobj.StoreTypeDropDown);
				} else {
					fc.utobj().clickElement(driver, pobj.StoreTypeDropDown);
					fc.utobj().clickElement(driver, pobj.StoreTypeDropDownSelectAll);
					fc.utobj().clickElement(driver, pobj.StoreTypeDropDownSelectAll);
					fc.utobj().clickElement(driver, pobj.StoreTypeDropDown);
				}

				String dependentOn = "None (Timeless)";
				String valueText = fc.utobj()
						.getElementByXpath(driver,
								".//select[@name='smSecondryChecklist_0referenceParent']/option[contains(text () , '"
										+ dependentOn + "')]")
						.getAttribute("value");
				fc.utobj().moveToElement(driver, pobj.dependentOnDropDown);
				Select sl = new Select(pobj.dependentOnDropDown);
				sl.selectByValue(valueText);

				fc.utobj().clickElement(driver, pobj.saveBtn);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		}
	}
}
