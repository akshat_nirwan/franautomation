package com.builds.test.opener;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminConfigurationConfigureStoreTypePageTest;
import com.builds.test.admin.AdminHiddenLinksConfigureNewHierarchyLevelPageTest;
import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.admin.AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest;
import com.builds.test.common.CorporateUser;
import com.builds.uimaps.opener.AdminOpenerTaskChecklistPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminOpenerTaskChecklistPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "opener", "openeremail" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-29", testCaseDescription = "Add Task Check List :: Admin > Opener > Task Checklist", testCaseId = "TC_01_Add_Task_Checklist_01")
	public void addTaskChecklistUploadFile() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			AdminOpenerTaskChecklistPage pobj = new AdminOpenerTaskChecklistPage(driver);
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Responsible Department");
			fc.utobj().printTestStep("Add Responsible Department");

			AdminOpenerResponsibleDepartmentPageTest addDepartmentPage = new AdminOpenerResponsibleDepartmentPageTest();
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));
			addDepartmentPage.addDepartment(driver, responsibilityArea);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "openerautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporatePage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Groups");
			fc.utobj().printTestStep("Add Group");

			AdminOpenerManageGroupsPageTest addGroupsPage = new AdminOpenerManageGroupsPageTest();
			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			addGroupsPage.addGroups(driver, groupName);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Task Checklist");
			fc.opener().opener_common().adminFranchiseOpenerTaskChecklist(driver);

			fc.utobj().printTestStep("Add Task Checklist");
			fc.utobj().clickElement(driver, pobj.addMoreBtn);
			String task = fc.utobj().generateTestData(dataSet.get("task"));
			fc.utobj().sendKeys(driver, pobj.taskNameTxtBox, task);

			fc.utobj().selectValFromMultiSelect(driver, pobj.selectResponsibility, responsibilityArea);

			fc.utobj().selectValFromMultiSelect(driver, pobj.selectContact, corpUser.getuserFullName());

			fc.utobj().selectValFromMultiSelect(driver, pobj.selectStoreType, storeType);

			fc.utobj().selectDropDown(driver, pobj.groupDropDown, groupName);

			String franchiseeAccess = dataSet.get("franchiseeAccess");
			fc.utobj().selectDropDown(driver, pobj.franchiseeAccessDropDown, franchiseeAccess);

			String priority = dataSet.get("priority");
			fc.utobj().selectDropDown(driver, pobj.priorityDropDown, priority);

			String criticalLevel = dataSet.get("criticalLevel");
			fc.utobj().selectDropDown(driver, pobj.criticalLevelDropDown, criticalLevel);

			String dependentOn = dataSet.get("dependentOn");
			String valueText = fc.utobj()
					.getElementByXpath(driver,
							".//select[@name='smTaskChecklist_0referenceParent']/option[contains(text () , '"
									+ dependentOn + "')]")
					.getAttribute("value");
			fc.utobj().moveToElement(driver, pobj.dependentOnDropDown);
			Select sl = new Select(pobj.dependentOnDropDown);
			sl.selectByValue(valueText);

			fc.utobj().sendKeys(driver, pobj.startScheduleDateTxtBox, "1");
			fc.utobj().selectDropDown(driver, pobj.scheduleStartDropDown, "Days Prior");
			fc.utobj().sendKeys(driver, pobj.completionScheduleDateTxtBox, "1");
			fc.utobj().selectDropDown(driver, pobj.scheduleCompletionDropDown, "Days Prior");

			fc.utobj().sendKeys(driver, pobj.startReminderTxtBox, "2");
			fc.utobj().sendKeys(driver, pobj.completionReminderTxtBox, "2");

			if (fc.utobj().isSelected(driver, pobj.uploadFileRadioBtn)) {
				// do nothing
			} else {
				fc.utobj().clickElement(driver, pobj.uploadFileRadioBtn);
			}

			String file = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			System.out.println(file);
			fc.utobj().sendKeys(driver, pobj.attachmentBrowseBox, file);
			fc.utobj().clickElement(driver, pobj.addBtn);

			try {

				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().clickElement(driver, pobj.noBtn);
				fc.utobj().switchFrameToDefault(driver);
			} catch (Exception e) {

			}

			fc.utobj().printTestStep("Verify The Add Task Checklist");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, task);

			if (isTextPresent == false) {
				fc.utobj().throwsSkipException("was not able add task checklist");
			}

			fc.utobj().printTestStep("Verify The Mail After Task Check List Creation");

			Map<String, String> mailData = new HashMap<String, String>();

			mailData = fc.utobj().readMailBox("Checklist Item Creation Alert", task, emailId, "sdg@1a@Hfs");

			if (mailData.size() == 0 || !mailData.get("mailBody").contains("New Item Created")) {
				fc.utobj().throwsException("was not able to verify task checklist creation mail");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addTaskChecklistUploadFile(WebDriver driver, Map<String, String> dataSet) throws Exception {

		String testCaseId = "TC_Add_Task_Checklist_UploadFile_Opener";
		String task = null;
		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminOpenerTaskChecklistPage pobj = new AdminOpenerTaskChecklistPage(driver);

				AdminOpenerResponsibleDepartmentPageTest addDepartmentPage = new AdminOpenerResponsibleDepartmentPageTest();
				String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));
				addDepartmentPage.addDepartment(driver, responsibilityArea);

				AdminUsersManageCorporateUsersAddCorporateUserPageTest corporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
				String emailId = "openerautomation@staffex.com";
				CorporateUser corpUser = new CorporateUser();
				corpUser.setEmail(emailId);
				corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
				corporatePage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

				AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
				String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
				storeTypePage.addStoreType(driver, storeType);

				fc.opener().opener_common().adminFranchiseOpenerTaskChecklist(driver);
				fc.utobj().clickElement(driver, pobj.addMoreBtn);
				task = fc.utobj().generateTestData(dataSet.get("task"));
				fc.utobj().sendKeys(driver, pobj.taskNameTxtBox, task);

				fc.utobj().selectValFromMultiSelect(driver, pobj.selectResponsibility, responsibilityArea);

				fc.utobj().selectValFromMultiSelect(driver, pobj.selectContact, corpUser.getuserFullName());

				fc.utobj().selectValFromMultiSelect(driver, pobj.selectStoreType, storeType);

				String franchiseeAccess = dataSet.get("franchiseeAccess");
				fc.utobj().selectDropDown(driver, pobj.franchiseeAccessDropDown, franchiseeAccess);

				String priority = dataSet.get("priority");
				fc.utobj().selectDropDown(driver, pobj.priorityDropDown, priority);

				String criticalLevel = dataSet.get("criticalLevel");
				fc.utobj().selectDropDown(driver, pobj.criticalLevelDropDown, criticalLevel);

				if (dataSet.get("dependentOn").equalsIgnoreCase("Multiple Checklists")) {

					String dependentOn = dataSet.get("dependentOn");
					String valueText = fc.utobj()
							.getElementByXpath(driver,
									".//select[@name='smTaskChecklist_0referenceParent']/option[contains(text () , '"
											+ dependentOn + "')]")
							.getAttribute("value");
					fc.utobj().moveToElement(driver, pobj.dependentOnDropDown);
					Select sl = new Select(pobj.dependentOnDropDown);
					sl.selectByValue(valueText);

					fc.utobj().clickElement(driver, pobj.selectDropDownBtn);

					fc.utobj().clickElement(driver, pobj.selectAll);

					fc.utobj().sendKeys(driver, pobj.startScheduleDateTxtBox, "1");
					fc.utobj().selectDropDown(driver, pobj.scheduleStartDropDown, "Days Prior");
					fc.utobj().sendKeys(driver, pobj.completionScheduleDateTxtBox, "1");
					fc.utobj().selectDropDown(driver, pobj.scheduleCompletionDropDown, "Days Prior");

					fc.utobj().sendKeys(driver, pobj.startReminderTxtBox, "2");
					fc.utobj().sendKeys(driver, pobj.completionReminderTxtBox, "2");

				} else if (dataSet.get("dependentOn").equalsIgnoreCase("None (Timeless)")) {

					String dependentOn = dataSet.get("dependentOn");
					String valueText = fc.utobj()
							.getElementByXpath(driver,
									".//select[@name='smTaskChecklist_0referenceParent']/option[contains(text () , '"
											+ dependentOn + "')]")
							.getAttribute("value");
					fc.utobj().moveToElement(driver, pobj.dependentOnDropDown);
					Select sl = new Select(pobj.dependentOnDropDown);
					sl.selectByValue(valueText);
				} else if (dataSet.get("dependentOn").equalsIgnoreCase("Expected Opening Date")) {

					String dependentOn = dataSet.get("dependentOn");
					String valueText = fc.utobj()
							.getElementByXpath(driver,
									".//select[@name='smTaskChecklist_0referenceParent']/option[contains(text () , '"
											+ dependentOn + "')]")
							.getAttribute("value");
					fc.utobj().moveToElement(driver, pobj.dependentOnDropDown);
					Select sl = new Select(pobj.dependentOnDropDown);
					sl.selectByValue(valueText);

					fc.utobj().sendKeys(driver, pobj.startScheduleDateTxtBox, "1");
					fc.utobj().selectDropDown(driver, pobj.scheduleStartDropDown, "Days Prior");
					fc.utobj().sendKeys(driver, pobj.completionScheduleDateTxtBox, "1");
					fc.utobj().selectDropDown(driver, pobj.scheduleCompletionDropDown, "Days Prior");

					fc.utobj().sendKeys(driver, pobj.startReminderTxtBox, "2");
					fc.utobj().sendKeys(driver, pobj.completionReminderTxtBox, "2");

				}

				if (fc.utobj().isSelected(driver, pobj.uploadFileRadioBtn)) {
					// do nothing
				} else {
					fc.utobj().clickElement(driver, pobj.uploadFileRadioBtn);
				}

				String file = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
				System.out.println(file);
				fc.utobj().sendKeys(driver, pobj.attachmentBrowseBox, file);
				fc.utobj().clickElement(driver, pobj.addBtn);

				try {

					fc.commonMethods().switch_cboxIframe_frameId(driver);
					fc.utobj().clickElement(driver, pobj.noBtn);
					fc.utobj().switchFrameToDefault(driver);

				} catch (Exception e) {

				}

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to Add Task Checklist With Upload");

		}

		return task;

	}

	public String addTaskChecklistUploadFile(WebDriver driver, String storeType, String fileName,
			Map<String, String> dataSet, String referenceDate, Map<String, String> config) throws Exception {

		String testCaseId = "TC_Add_Task_Checklist_UploadFile_Opener_01";
		String task = null;
		if (fc.utobj().validate(testCaseId)) {
			try {
				String parantwindow = driver.getWindowHandle();
				AdminOpenerTaskChecklistPage pobj = new AdminOpenerTaskChecklistPage(driver);

				AdminOpenerResponsibleDepartmentPageTest addDepartmentPage = new AdminOpenerResponsibleDepartmentPageTest();
				String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));
				addDepartmentPage.addDepartment(driver, responsibilityArea);

				AdminUsersManageCorporateUsersAddCorporateUserPageTest corporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
				String emailId = "openerautomation@staffex.com";
				CorporateUser corpUser = new CorporateUser();
				corpUser.setEmail(emailId);
				corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
				corporatePage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

				fc.opener().opener_common().adminFranchiseOpenerTaskChecklist(driver);
				fc.utobj().clickElement(driver, pobj.addMoreBtn);
				task = fc.utobj().generateTestData(dataSet.get("task"));
				fc.utobj().sendKeys(driver, pobj.taskNameTxtBox, task);

				fc.utobj().selectValFromMultiSelect(driver, pobj.selectResponsibility, responsibilityArea);
				fc.utobj().selectValFromMultiSelect(driver, pobj.selectContact, corpUser.getuserFullName());
				fc.utobj().selectValFromMultiSelect(driver, pobj.selectStoreType, storeType);

				String franchiseeAccess = dataSet.get("franchiseeAccess");
				fc.utobj().selectDropDown(driver, pobj.franchiseeAccessDropDown, franchiseeAccess);

				String priority = dataSet.get("priority");
				fc.utobj().selectDropDown(driver, pobj.priorityDropDown, priority);

				String criticalLevel = dataSet.get("criticalLevel");
				fc.utobj().selectDropDown(driver, pobj.criticalLevelDropDown, criticalLevel);

				if (dataSet.get("dependentOn").equalsIgnoreCase("Multiple Checklists")) {

					String dependentOn = dataSet.get("dependentOn");
					String valueText = fc.utobj()
							.getElementByXpath(driver,
									".//select[@name='smTaskChecklist_0referenceParent']/option[contains(text () , '"
											+ dependentOn + "')]")
							.getAttribute("value");
					fc.utobj().moveToElement(driver, pobj.dependentOnDropDown);
					Select sl = new Select(pobj.dependentOnDropDown);
					sl.selectByValue(valueText);

					fc.utobj().clickElement(driver, pobj.selectDropDownBtn);
					fc.utobj().clickElement(driver, pobj.selectAll);

					fc.utobj().sendKeys(driver, pobj.startScheduleDateTxtBox, "1");
					fc.utobj().selectDropDown(driver, pobj.scheduleStartDropDown, "Days Prior");
					fc.utobj().sendKeys(driver, pobj.completionScheduleDateTxtBox, "1");
					fc.utobj().selectDropDown(driver, pobj.scheduleCompletionDropDown, "Days Prior");

					fc.utobj().sendKeys(driver, pobj.startReminderTxtBox, "2");
					fc.utobj().sendKeys(driver, pobj.completionReminderTxtBox, "2");

				} else if (dataSet.get("dependentOn").equalsIgnoreCase("Expected Opening Date")) {

					String dependentOn = dataSet.get("dependentOn");
					String valueText = fc.utobj()
							.getElementByXpath(driver,
									".//select[@name='smTaskChecklist_0referenceParent']/option[contains(text () , '"
											+ dependentOn + "')]")
							.getAttribute("value");
					fc.utobj().moveToElement(driver, pobj.dependentOnDropDown);
					Select sl = new Select(pobj.dependentOnDropDown);
					sl.selectByValue(valueText);

					fc.utobj().sendKeys(driver, pobj.startScheduleDateTxtBox, "2");
					fc.utobj().selectDropDown(driver, pobj.scheduleStartDropDown, "Days Prior");
					fc.utobj().sendKeys(driver, pobj.completionScheduleDateTxtBox, "2");
					fc.utobj().selectDropDown(driver, pobj.scheduleCompletionDropDown, "Days Prior");

					fc.utobj().sendKeys(driver, pobj.startReminderTxtBox, "2");
					fc.utobj().sendKeys(driver, pobj.completionReminderTxtBox, "2");

				} else if (dataSet.get("dependentOn").equalsIgnoreCase("None (Timeless)")) {

					String dependentOn = dataSet.get("dependentOn");
					String valueText = fc.utobj()
							.getElementByXpath(driver,
									".//select[@name='smTaskChecklist_0referenceParent']/option[contains(text () , '"
											+ dependentOn + "')]")
							.getAttribute("value");
					fc.utobj().moveToElement(driver, pobj.dependentOnDropDown);
					Select sl = new Select(pobj.dependentOnDropDown);
					sl.selectByValue(valueText);
				} else {

					String valueText = fc.utobj()
							.getElementByXpath(driver,
									".//select[@name='smTaskChecklist_0referenceParent']/option[contains(text () , '"
											+ referenceDate + "')]")
							.getAttribute("value");
					fc.utobj().moveToElement(driver, pobj.dependentOnDropDown);
					Select sl = new Select(pobj.dependentOnDropDown);
					sl.selectByValue(valueText);

					fc.utobj().sendKeys(driver, pobj.startScheduleDateTxtBox, "1");
					fc.utobj().selectDropDown(driver, pobj.scheduleStartDropDown, "Days Prior");
					fc.utobj().sendKeys(driver, pobj.completionScheduleDateTxtBox, "1");
					fc.utobj().selectDropDown(driver, pobj.scheduleCompletionDropDown, "Days Prior");

					fc.utobj().sendKeys(driver, pobj.startReminderTxtBox, "2");
					fc.utobj().sendKeys(driver, pobj.completionReminderTxtBox, "2");

				}

				if (fc.utobj().isSelected(driver, pobj.uploadFileRadioBtn)) {
					// do nothing
				} else {
					fc.utobj().clickElement(driver, pobj.uploadFileRadioBtn);
				}

				fc.utobj().sendKeys(driver, pobj.attachmentBrowseBox, fileName);
				fc.utobj().clickElement(driver, pobj.addBtn);

				try {

					fc.commonMethods().switch_cboxIframe_frameId(driver);
					fc.utobj().clickElement(driver, pobj.yesBtn);
					driver.switchTo().window(parantwindow);

				} catch (Exception e) {

				}

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to Add Task Cheklist with upload File");

		}

		return task;

	}

	// For Group
	public String addTaskChecklistUploadFileWithGroup(WebDriver driver, String storeType, File file,
			Map<String, String> dataSet, String groupName, String referenceDate, Map<String, String> config)
			throws Exception {

		String testCaseId = "TC_Add_Task_Checklist_UploadFile_Opener_01";
		String task = null;
		if (fc.utobj().validate(testCaseId)) {
			try {
				String parantwindow = driver.getWindowHandle();
				AdminOpenerTaskChecklistPage pobj = new AdminOpenerTaskChecklistPage(driver);
				AdminOpenerResponsibleDepartmentPageTest addDepartmentPage = new AdminOpenerResponsibleDepartmentPageTest();
				String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));
				addDepartmentPage.addDepartment(driver, responsibilityArea);

				AdminUsersManageCorporateUsersAddCorporateUserPageTest corporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
				String emailId = "openerautomation@staffex.com";
				CorporateUser corpUser = new CorporateUser();
				corpUser.setEmail(emailId);
				corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
				corporatePage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

				AdminOpenerManageGroupsPageTest addGroupsPage = new AdminOpenerManageGroupsPageTest();
				addGroupsPage.addGroups(driver, groupName);

				fc.opener().opener_common().adminFranchiseOpenerTaskChecklist(driver);

				fc.utobj().clickElement(driver, pobj.addMoreBtn);

				task = fc.utobj().generateTestData(dataSet.get("task"));
				fc.utobj().sendKeys(driver, pobj.taskNameTxtBox, task);

				fc.utobj().selectValFromMultiSelect(driver, pobj.selectResponsibility, responsibilityArea);

				fc.utobj().selectValFromMultiSelect(driver, pobj.selectContact, corpUser.getuserFullName());

				fc.utobj().selectValFromMultiSelect(driver, pobj.selectStoreType, storeType);

				fc.utobj().selectDropDown(driver, pobj.groupDropDown, groupName);

				String franchiseeAccess = dataSet.get("franchiseeAccess");
				fc.utobj().selectDropDown(driver, pobj.franchiseeAccessDropDown, franchiseeAccess);

				String priority = dataSet.get("priority");
				fc.utobj().selectDropDown(driver, pobj.priorityDropDown, priority);

				String criticalLevel = dataSet.get("criticalLevel");
				fc.utobj().selectDropDown(driver, pobj.criticalLevelDropDown, criticalLevel);

				if (dataSet.get("dependentOn").equalsIgnoreCase("Multiple Checklists")) {

					String dependentOn = dataSet.get("dependentOn");
					String valueText = fc.utobj()
							.getElementByXpath(driver,
									".//select[@name='smTaskChecklist_0referenceParent']/option[contains(text () , '"
											+ dependentOn + "')]")
							.getAttribute("value");
					fc.utobj().moveToElement(driver, pobj.dependentOnDropDown);
					Select sl = new Select(pobj.dependentOnDropDown);
					sl.selectByValue(valueText);

					fc.utobj().clickElement(driver, pobj.selectDropDownBtn);
					fc.utobj().clickElement(driver, pobj.selectAll);

					fc.utobj().sendKeys(driver, pobj.startScheduleDateTxtBox, "1");
					fc.utobj().selectDropDown(driver, pobj.scheduleStartDropDown, "Days Prior");
					fc.utobj().sendKeys(driver, pobj.completionScheduleDateTxtBox, "1");
					fc.utobj().selectDropDown(driver, pobj.scheduleCompletionDropDown, "Days Prior");

					fc.utobj().sendKeys(driver, pobj.startReminderTxtBox, "2");
					fc.utobj().sendKeys(driver, pobj.completionReminderTxtBox, "2");

				} else if (dataSet.get("dependentOn").equalsIgnoreCase("Expected Opening Date")) {

					String dependentOn = dataSet.get("dependentOn");
					String valueText = fc.utobj()
							.getElementByXpath(driver,
									".//select[@name='smTaskChecklist_0referenceParent']/option[contains(text () , '"
											+ dependentOn + "')]")
							.getAttribute("value");
					fc.utobj().moveToElement(driver, pobj.dependentOnDropDown);
					Select sl = new Select(pobj.dependentOnDropDown);
					sl.selectByValue(valueText);

					fc.utobj().sendKeys(driver, pobj.startScheduleDateTxtBox, "2");
					fc.utobj().selectDropDown(driver, pobj.scheduleStartDropDown, "Days Prior");
					fc.utobj().sendKeys(driver, pobj.completionScheduleDateTxtBox, "2");
					fc.utobj().selectDropDown(driver, pobj.scheduleCompletionDropDown, "Days Prior");

					fc.utobj().sendKeys(driver, pobj.startReminderTxtBox, "2");
					fc.utobj().sendKeys(driver, pobj.completionReminderTxtBox, "2");

				} else if (dataSet.get("dependentOn").equalsIgnoreCase("None (Timeless)")) {

					String dependentOn = dataSet.get("dependentOn");
					String valueText = fc.utobj()
							.getElementByXpath(driver,
									".//select[@name='smTaskChecklist_0referenceParent']/option[contains(text () , '"
											+ dependentOn + "')]")
							.getAttribute("value");
					fc.utobj().moveToElement(driver, pobj.dependentOnDropDown);
					Select sl = new Select(pobj.dependentOnDropDown);
					sl.selectByValue(valueText);
				} else {

					String valueText = fc.utobj()
							.getElementByXpath(driver,
									".//select[@name='smTaskChecklist_0referenceParent']/option[contains(text () , '"
											+ referenceDate + "')]")
							.getAttribute("value");
					fc.utobj().moveToElement(driver, pobj.dependentOnDropDown);
					Select sl = new Select(pobj.dependentOnDropDown);
					sl.selectByValue(valueText);

					fc.utobj().sendKeys(driver, pobj.startScheduleDateTxtBox, "1");
					fc.utobj().selectDropDown(driver, pobj.scheduleStartDropDown, "Days Prior");
					fc.utobj().sendKeys(driver, pobj.completionScheduleDateTxtBox, "1");
					fc.utobj().selectDropDown(driver, pobj.scheduleCompletionDropDown, "Days Prior");

					fc.utobj().sendKeys(driver, pobj.startReminderTxtBox, "2");
					fc.utobj().sendKeys(driver, pobj.completionReminderTxtBox, "2");

				}

				if (fc.utobj().isSelected(driver, pobj.uploadFileRadioBtn)) {
					// do nothing
				} else {
					fc.utobj().clickElement(driver, pobj.uploadFileRadioBtn);
				}

				System.out.println(file.getAbsolutePath());
				fc.utobj().sendKeys(driver, pobj.attachmentBrowseBox, file.getAbsolutePath());
				fc.utobj().clickElement(driver, pobj.addBtn);

				try {

					fc.commonMethods().switch_cboxIframe_frameId(driver);
					fc.utobj().clickElement(driver, pobj.yesBtn);
					driver.switchTo().window(parantwindow);

				} catch (Exception e) {

				}

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to Add Task Cheklist with upload File");

		}

		return task;

	}

	// task with divisional user
	public String addTaskChecklistUploadFile(WebDriver driver, String storeType, File file, Map<String, String> dataSet,
			String referenceDate, String userName, String divisionName, Map<String, String> config) throws Exception {

		String testCaseId = "TC_Add_TaskChecklist_Upload_File_02";
		String task = null;
		if (fc.utobj().validate(testCaseId)) {
			try {
				String parantwindow = driver.getWindowHandle();
				AdminOpenerTaskChecklistPage pobj = new AdminOpenerTaskChecklistPage(driver);

				AdminOpenerResponsibleDepartmentPageTest addDepartmentPage = new AdminOpenerResponsibleDepartmentPageTest();
				String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));
				addDepartmentPage.addDepartment(driver, responsibilityArea);

				String password = dataSet.get("password");
				AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest addDivUserPage = new AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest();
				String emailId = "openerautomation@staffex.com";
				addDivUserPage.addDivisionalUser(driver, userName, password, divisionName, emailId);

				fc.opener().opener_common().adminFranchiseOpenerTaskChecklist(driver);

				fc.utobj().clickElement(driver, pobj.addMoreBtn);

				task = fc.utobj().generateTestData(dataSet.get("task"));
				fc.utobj().sendKeys(driver, pobj.taskNameTxtBox, task);

				fc.utobj().selectValFromMultiSelect(driver, pobj.selectResponsibility, responsibilityArea);

				fc.utobj().selectValFromMultiSelect(driver, pobj.selectContact, "Division");

				fc.utobj().selectValFromMultiSelect(driver, pobj.selectStoreType, storeType);

				String franchiseeAccess = dataSet.get("franchiseeAccess");
				fc.utobj().selectDropDown(driver, pobj.franchiseeAccessDropDown, franchiseeAccess);

				String priority = dataSet.get("priority");
				fc.utobj().selectDropDown(driver, pobj.priorityDropDown, priority);

				String criticalLevel = dataSet.get("criticalLevel");
				fc.utobj().selectDropDown(driver, pobj.criticalLevelDropDown, criticalLevel);

				if (dataSet.get("dependentOn").equalsIgnoreCase("Multiple Checklists")) {

					String dependentOn = dataSet.get("dependentOn");
					String valueText = fc.utobj()
							.getElementByXpath(driver,
									".//select[@name='smTaskChecklist_0referenceParent']/option[contains(text () , '"
											+ dependentOn + "')]")
							.getAttribute("value");
					fc.utobj().moveToElement(driver, pobj.dependentOnDropDown);
					Select sl = new Select(pobj.dependentOnDropDown);
					sl.selectByValue(valueText);

					fc.utobj().clickElement(driver, pobj.selectDropDownBtn);
					fc.utobj().clickElement(driver, pobj.selectAll);

					fc.utobj().sendKeys(driver, pobj.startScheduleDateTxtBox, "1");
					fc.utobj().selectDropDown(driver, pobj.scheduleStartDropDown, "Days Prior");
					fc.utobj().sendKeys(driver, pobj.completionScheduleDateTxtBox, "1");
					fc.utobj().selectDropDown(driver, pobj.scheduleCompletionDropDown, "Days Prior");

					fc.utobj().sendKeys(driver, pobj.startReminderTxtBox, "2");
					fc.utobj().sendKeys(driver, pobj.completionReminderTxtBox, "2");

				} else if (dataSet.get("dependentOn").equalsIgnoreCase("Expected Opening Date")) {

					String dependentOn = dataSet.get("dependentOn");
					String valueText = fc.utobj()
							.getElementByXpath(driver,
									".//select[@name='smTaskChecklist_0referenceParent']/option[contains(text () , '"
											+ dependentOn + "')]")
							.getAttribute("value");
					fc.utobj().moveToElement(driver, pobj.dependentOnDropDown);
					Select sl = new Select(pobj.dependentOnDropDown);
					sl.selectByValue(valueText);

					fc.utobj().sendKeys(driver, pobj.startScheduleDateTxtBox, "2");
					fc.utobj().selectDropDown(driver, pobj.scheduleStartDropDown, "Days Prior");
					fc.utobj().sendKeys(driver, pobj.completionScheduleDateTxtBox, "2");
					fc.utobj().selectDropDown(driver, pobj.scheduleCompletionDropDown, "Days Prior");

					fc.utobj().sendKeys(driver, pobj.startReminderTxtBox, "2");
					fc.utobj().sendKeys(driver, pobj.completionReminderTxtBox, "2");

				} else if (dataSet.get("dependentOn").equalsIgnoreCase("None (Timeless)")) {

					String dependentOn = dataSet.get("dependentOn");
					String valueText = fc.utobj()
							.getElementByXpath(driver,
									".//select[@name='smTaskChecklist_0referenceParent']/option[contains(text () , '"
											+ dependentOn + "')]")
							.getAttribute("value");
					fc.utobj().moveToElement(driver, pobj.dependentOnDropDown);
					Select sl = new Select(pobj.dependentOnDropDown);
					sl.selectByValue(valueText);
				} else {

					String valueText = fc.utobj()
							.getElementByXpath(driver,
									".//select[@name='smTaskChecklist_0referenceParent']/option[contains(text () , '"
											+ referenceDate + "')]")
							.getAttribute("value");
					fc.utobj().moveToElement(driver, pobj.dependentOnDropDown);
					Select sl = new Select(pobj.dependentOnDropDown);
					sl.selectByValue(valueText);

					fc.utobj().sendKeys(driver, pobj.startScheduleDateTxtBox, "1");
					fc.utobj().selectDropDown(driver, pobj.scheduleStartDropDown, "Days Prior");
					fc.utobj().sendKeys(driver, pobj.completionScheduleDateTxtBox, "1");
					fc.utobj().selectDropDown(driver, pobj.scheduleCompletionDropDown, "Days Prior");

					fc.utobj().sendKeys(driver, pobj.startReminderTxtBox, "2");
					fc.utobj().sendKeys(driver, pobj.completionReminderTxtBox, "2");

				}

				if (fc.utobj().isSelected(driver, pobj.uploadFileRadioBtn)) {
					// do nothing
				} else {
					fc.utobj().clickElement(driver, pobj.uploadFileRadioBtn);
				}

				System.out.println(file.getAbsolutePath());
				fc.utobj().sendKeys(driver, pobj.attachmentBrowseBox, file.getAbsolutePath());
				fc.utobj().clickElement(driver, pobj.addBtn);

				try {

					fc.commonMethods().switch_cboxIframe_frameId(driver);
					fc.utobj().clickElement(driver, pobj.yesBtn);
					driver.switchTo().window(parantwindow);

				} catch (Exception e) {

				}

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to Task Checklist With Divisional USer");

		}

		return task;

	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Add Task Check List :: Admin > Opener > Task Checklist", testCaseId = "TC_02_Add_Task_Checklist_02")
	public void addTaskChecklistWebLink() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			AdminOpenerTaskChecklistPage pobj = new AdminOpenerTaskChecklistPage(driver);
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Responsible Department");
			fc.utobj().printTestStep("Add Responsible Department");

			AdminOpenerResponsibleDepartmentPageTest addDepartmentPage = new AdminOpenerResponsibleDepartmentPageTest();
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));
			addDepartmentPage.addDepartment(driver, responsibilityArea);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Groups");
			fc.utobj().printTestStep("Add Group");

			AdminOpenerManageGroupsPageTest addGroupsPage = new AdminOpenerManageGroupsPageTest();
			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			addGroupsPage.addGroups(driver, groupName);

			// configure Default
			configureDefaultValue(driver, FranconnectUtil.config);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Task Checklist");
			fc.utobj().printTestStep("Add Task Checklist With Web Link Option");
			fc.opener().opener_common().adminFranchiseOpenerTaskChecklist(driver);

			fc.utobj().clickElement(driver, pobj.addMoreBtn);

			String task = fc.utobj().generateTestData(dataSet.get("task"));
			fc.utobj().sendKeys(driver, pobj.taskNameTxtBox, task);
			fc.utobj().selectValFromMultiSelect(driver, pobj.selectResponsibility, responsibilityArea);
			fc.utobj().selectValFromMultiSelect(driver, pobj.selectContact, "Regional User");
			fc.utobj().selectValFromMultiSelect(driver, pobj.selectStoreType, storeType);

			fc.utobj().selectDropDown(driver, pobj.groupDropDown, groupName);

			String franchiseeAccess = dataSet.get("franchiseeAccess");
			fc.utobj().selectDropDown(driver, pobj.franchiseeAccessDropDown, franchiseeAccess);

			String priority = dataSet.get("priority");
			fc.utobj().selectDropDown(driver, pobj.priorityDropDown, priority);

			String criticalLevel = dataSet.get("criticalLevel");
			fc.utobj().selectDropDown(driver, pobj.criticalLevelDropDown, criticalLevel);

			String dependentOn = dataSet.get("dependentOn");
			String valueText = fc.utobj()
					.getElementByXpath(driver,
							".//select[@name='smTaskChecklist_0referenceParent']/option[contains(text () , '"
									+ dependentOn + "')]")
					.getAttribute("value");
			fc.utobj().moveToElement(driver, pobj.dependentOnDropDown);
			Select sl = new Select(pobj.dependentOnDropDown);
			sl.selectByValue(valueText);

			if (fc.utobj().isSelected(driver, pobj.webLinkRadioBtn)) {
				// do nothing
			} else {
				fc.utobj().clickElement(driver, pobj.webLinkRadioBtn);
			}

			fc.utobj().sendKeys(driver, pobj.webLinkAddressBx, dataSet.get("webLinkUrl"));

			fc.utobj().clickElement(driver, pobj.addBtn);

			try {

				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().clickElement(driver, pobj.noBtn);
				fc.utobj().switchFrameToDefault(driver);

			} catch (Exception e) {

			}

			fc.utobj().printTestStep("Verify The Add Task Checklist");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, task);

			if (isTextPresent == false) {
				fc.utobj().throwsSkipException("was not able add task checklist");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "opener" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Modify Task Check List :: Admin > Opener > Task Checklist", testCaseId = "TC_03_Modify_Task_Checklist")
	public void modifyTaskChecklistActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminOpenerTaskChecklistPage pobj = new AdminOpenerTaskChecklistPage(driver);
			driver = fc.loginpage().login(driver);
			String task = "";

			fc.utobj().printTestStep("Enable Divisional User");
			AdminHiddenLinksConfigureNewHierarchyLevelPageTest configure_level = new AdminHiddenLinksConfigureNewHierarchyLevelPageTest();
			configure_level.ConfigureNewHierarchyLevel(driver);

			fc.utobj().printTestStep("Add Task Checklist");
			task = addTaskChecklistUploadFile(driver, dataSet);

			fc.utobj().printTestStep("Modify The Task Checklist");
			String text = driver
					.findElement(By.xpath(".//*[.='" + task + "']/ancestor::tr/td/div[@id='menuBar']/layer"))
					.getAttribute("id").trim();
			String alterText = text.replace("Actions_dynamicmenu", "");
			alterText = alterText.replace("Bar", "");

			fc.utobj().clickElement(driver, driver
					.findElement(By.xpath(".//*[.='" + task + "']/ancestor::tr/td/div[@id='menuBar']/layer/a/img")));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//div[@id='Actions_dynamicmenu" + alterText + "Menu']/span[contains(text () , 'Modify')]"));

			task = fc.utobj().generateTestData(dataSet.get("task"));
			fc.utobj().sendKeys(driver, pobj.taskNameTxtBox, task);
			fc.utobj().selectValFromMultiSelect(driver, pobj.selectContact, "Division");

			// non timeless
			String dependentOn = dataSet.get("dependentOnModify");
			String valueText = fc.utobj()
					.getElementByXpath(driver,
							".//select[@name='smTaskChecklist_0referenceParent']/option[contains(text () , '"
									+ dependentOn + "')]")
					.getAttribute("value");
			fc.utobj().moveToElement(driver, pobj.dependentOnDropDown);
			Select sl = new Select(pobj.dependentOnDropDown);
			sl.selectByValue(valueText);

			if (fc.utobj().isSelected(driver, pobj.webLinkRadioBtn)) {
				// do nothing
			} else {
				fc.utobj().clickElement(driver, pobj.webLinkRadioBtn);
			}

			fc.utobj().sendKeys(driver, pobj.webLinkAddressBx, dataSet.get("webLinkUrl"));

			try {

				List<WebElement> listRadioButton = pobj.applyToAll;
				if (fc.utobj().isSelected(driver,listRadioButton.get(0))) {
					// do nothing
				} else {
					fc.utobj().clickElement(driver, listRadioButton.get(0));
				}
			} catch (Exception e) {

			}

			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("Verify The Modify Checklist");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, task);

			if (isTextPresent == false) {
				fc.utobj().throwsSkipException("Was not able to modify Task Checklist");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "opener", "openeremail" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-29", testCaseDescription = "Delete Task Check List :: Admin > Opener > Task Checklist.", testCaseId = "TC_04_Delete_Task_Checklist")
	public void deleteTaskChecklistActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminOpenerTaskChecklistPage pobj = new AdminOpenerTaskChecklistPage(driver);
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Add Task Checklist");
			String task = addTaskChecklistUploadFile(driver, dataSet);

			fc.utobj().printTestStep("Delete The Checklist");
			String text = driver
					.findElement(By.xpath(".//*[.='" + task + "']/ancestor::tr/td/div[@id='menuBar']/layer"))
					.getAttribute("id").trim();
			String alterText = text.replace("Actions_dynamicmenu", "");
			alterText = alterText.replace("Bar", "");

			fc.utobj().clickElement(driver, driver
					.findElement(By.xpath(".//*[.='" + task + "']/ancestor::tr/td/div[@id='menuBar']/layer/a/img")));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//div[@id='Actions_dynamicmenu" + alterText + "Menu']/span[contains(text () , 'Delete')]"));

			fc.utobj().acceptAlertBox(driver);

			try {

				fc.commonMethods().switch_cboxIframe_frameId(driver);
				if (fc.utobj().isSelected(driver,pobj.applyToAll.get(0))) {
					// do nothing
				} else {
					fc.utobj().clickElement(driver, pobj.applyToAll.get(0));
				}

				fc.utobj().clickElement(driver, pobj.cboxDeleteBtn);
				fc.utobj().clickElement(driver, pobj.cboxCloseBtn);
				fc.utobj().switchFrameToDefault(driver);
				fc.utobj().switchFrameToDefault(driver);
			} catch (Exception e) {

			}

			fc.utobj().printTestStep("Verify The Delete Checklist");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, task);

			if (isTextPresent == true) {
				fc.utobj().throwsSkipException("Was not able to delete Task Checklist");
			}

			fc.utobj().printTestStep("Verify Email After Task Checklist Deletion");

			Map<String, String> mailData = new HashMap<String, String>();
			mailData = fc.utobj().readMailBox("Checklist Item Deletion Alert", task, "openerautomation@staffex.com",
					"sdg@1a@Hfs");

			if (mailData.size() == 0 || !mailData.get("mailBody").contains("Item Deleted")) {

				fc.utobj().throwsException("was not able to verify Task Checklist Deletion Mail");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Add Description :: Admin > Opener > Task Checklist", testCaseId = "TC_05_Add_Description_Task")
	public void addDescriptionTaskActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminOpenerTaskChecklistPage pobj = new AdminOpenerTaskChecklistPage(driver);
			driver = fc.loginpage().login(driver);
			String task = "";

			fc.utobj().printTestStep("Add Task Checklist");
			task = addTaskChecklistUploadFile(driver, dataSet);

			fc.utobj().printTestStep("Add Description");
			fc.utobj().actionImgOption(driver, task, "Add Description");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().sendKeys(driver, pobj.descriptionArea, description);
			fc.utobj().clickElement(driver, pobj.addDescBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Add Description");
			fc.utobj().actionImgOption(driver, task, "Modify Description");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String descText = fc.utobj().getText(driver, pobj.descriptionArea);

			if (!descText.equalsIgnoreCase(description)) {
				fc.utobj().throwsSkipException("was not able to add description");
			}
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Modify Description :: Admin > Opener > Task Checklist", testCaseId = "TC_06_Modify_Description_Task")
	public void modifyDescriptionActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminOpenerTaskChecklistPage pobj = new AdminOpenerTaskChecklistPage(driver);
			driver = fc.loginpage().login(driver);
			String task = "";

			fc.utobj().printTestStep("Add Task Checklist");
			task = addTaskChecklistUploadFile(driver, dataSet);

			fc.utobj().printTestStep("Add Description");
			fc.utobj().actionImgOption(driver, task, "Add Description");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().sendKeys(driver, pobj.descriptionArea, description);
			fc.utobj().clickElement(driver, pobj.addDescBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Modify The Description");
			fc.utobj().actionImgOption(driver, task, "Modify Description");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().sendKeys(driver, pobj.descriptionArea, description);
			fc.utobj().clickElement(driver, pobj.saveDescModBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify Modify The Description");
			fc.utobj().actionImgOption(driver, task, "Modify Description");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String descText = fc.utobj().getText(driver, pobj.descriptionArea);

			if (!descText.equalsIgnoreCase(description)) {
				fc.utobj().throwsSkipException("was not able to add description");
			}

			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Modify Task Check List At Action Button :: Admin > Opener > Task Checklist", testCaseId = "TC_07_Modfiy_Task_Checklist01")
	public void modifyTaskChecklistActionButton() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminOpenerTaskChecklistPage pobj = new AdminOpenerTaskChecklistPage(driver);
			driver = fc.loginpage().login(driver);
			String task = "";

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");

			OpenerStoreSummaryStoreListPageTest storeListPage = new OpenerStoreSummaryStoreListPageTest();
			String emailId = "openerautomation@staffex.com";
			String franchiseID = storeListPage.addNewFranchiseLocation(driver, dataSet, emailId);

			fc.utobj().printTestStep("Add Task Checklist");
			task = addTaskChecklistUploadFile(driver, dataSet);

			fc.utobj().printTestStep("Modify The Checklist");

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[.='" + task + "']//ancestor::tr[@class='bText12' or @class='bText12 grAltRw3']/td/input"));

			fc.utobj().selectActionMenuItems(driver, "Modify");

			task = fc.utobj().generateTestData(dataSet.get("task"));
			fc.utobj().sendKeys(driver, pobj.taskNameTxtBox, task);
			fc.utobj().selectValFromMultiSelect(driver, pobj.selectContact, "Franchise User");

			String dependentOn = dataSet.get("dependentOnModify");
			String valueText = fc.utobj()
					.getElementByXpath(driver,
							".//select[@name='smTaskChecklist_0referenceParent']/option[contains(text () , '"
									+ dependentOn + "')]")
					.getAttribute("value");
			fc.utobj().moveToElement(driver, pobj.dependentOnDropDown);
			Select sl = new Select(pobj.dependentOnDropDown);
			sl.selectByValue(valueText);

			fc.utobj().sendKeys(driver, pobj.startScheduleDateTxtBox, "1");
			fc.utobj().selectDropDown(driver, pobj.scheduleStartDropDown, "Days Prior");
			fc.utobj().sendKeys(driver, pobj.completionScheduleDateTxtBox, "1");
			fc.utobj().selectDropDown(driver, pobj.scheduleCompletionDropDown, "Days Prior");
			fc.utobj().sendKeys(driver, pobj.startReminderTxtBox, "2");
			fc.utobj().sendKeys(driver, pobj.completionReminderTxtBox, "2");

			if (fc.utobj().isSelected(driver, pobj.webLinkRadioBtn)) {
				// do nothing
			} else {
				fc.utobj().clickElement(driver, pobj.webLinkRadioBtn);
			}

			fc.utobj().sendKeys(driver, pobj.webLinkAddressBx, dataSet.get("webLinkUrl"));

			try {
				List<WebElement> listRadioButton = pobj.applyToAll;

				if (fc.utobj().isSelected(driver,listRadioButton.get(2))) {
					// do nothing
				} else {
					fc.utobj().clickElement(driver, listRadioButton.get(2));
				}
				fc.utobj().selectValFromMultiSelect(driver, pobj.franchiseSelect, franchiseID);

			} catch (Exception e) {

			}
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("Verify The Modify Task Checklist");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, task);
			if (isTextPresent == false) {
				fc.utobj().throwsSkipException("Was not able to modify Task Checklist");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "opener" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = " Delete Task Check List At Action Button :: Admin > Opener > Task Checklist", testCaseId = "TC_08_Delete_Task_Checklist01")
	public void deleteTaskChecklistActionButton() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminOpenerTaskChecklistPage pobj = new AdminOpenerTaskChecklistPage(driver);
			driver = fc.loginpage().login(driver);
			String task = "";

			fc.utobj().printTestStep("Add Task Checklist");
			task = addTaskChecklistUploadFile(driver, dataSet);

			fc.utobj().printTestStep("Delete The Checklist");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[.='" + task + "']//ancestor::tr[@class='bText12' or @class='bText12 grAltRw3']/td/input"));

			fc.utobj().selectActionMenuItems(driver, "Delete");

			fc.utobj().acceptAlertBox(driver);

			try {

				fc.commonMethods().switch_cboxIframe_frameId(driver);
				if (fc.utobj().isSelected(driver,pobj.applyToAll.get(1))) {
					// do nothing
				} else {
					fc.utobj().clickElement(driver, pobj.applyToAll.get(1));
				}

				fc.utobj().clickElement(driver, pobj.cboxDeleteBtn);
				fc.utobj().clickElement(driver, pobj.cboxCloseBtn);
				fc.utobj().switchFrameToDefault(driver);
			} catch (Exception e) {

			}

			fc.utobj().printTestStep("Verift The Delete Checklist");

			boolean isTextPresent = fc.utobj().assertPageSource(driver, task);

			if (isTextPresent == true) {
				fc.utobj().throwsSkipException("Was not able to delete Task Checklist");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "opener" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Modify Contact(s) At Action Button :: Admin > Opener > Task Checklist", testCaseId = "TC_09_Modify_Contact_Task")
	public void modifyContactActionButton() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminOpenerTaskChecklistPage pobj = new AdminOpenerTaskChecklistPage(driver);
			driver = fc.loginpage().login(driver);
			String task = "";

			fc.utobj().printTestStep("Enable Divisional User");
			AdminHiddenLinksConfigureNewHierarchyLevelPageTest configure_Level = new AdminHiddenLinksConfigureNewHierarchyLevelPageTest();
			configure_Level.ConfigureNewHierarchyLevel(driver);

			fc.utobj().printTestStep("Add Task Checklist");
			task = addTaskChecklistUploadFile(driver, dataSet);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[.='" + task + "']//ancestor::tr[@class='bText12' or @class='bText12 grAltRw3']/td/input"));

			fc.utobj().printTestStep("Modify The Contact Of Checklist");
			fc.utobj().selectActionMenuItems(driver, "Modify Contact(s)");

			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().selectValFromMultiSelect(driver, pobj.contactSelectM, "Divisional");

			try {

				if (fc.utobj().isSelected(driver,pobj.applyToAll.get(1))) {
					// do nothing
				} else {
					fc.utobj().clickElement(driver, pobj.applyToAll.get(1));
				}
			} catch (Exception e) {

			}

			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verfy The Modify Contact");
			String textPresent = fc.utobj().getText(driver, fc.utobj().getElementByXpath(driver,
					".//*[.='" + task + "']//ancestor::tr[@class='bText12' or @class='bText12 grAltRw3']/td[5]"));

			if (!textPresent.contains("Divisional")) {
				fc.utobj().throwsSkipException("was not able to modify Contact");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Configure Default Value At Action Button :: Admin > Opener > Task Checklist", testCaseId = "TC_10_Configure_Default_Value_Task")
	public void configureDefaultValueActionButton() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminOpenerTaskChecklistPage pobj = new AdminOpenerTaskChecklistPage(driver);
			driver = fc.loginpage().login(driver);

			AdminOpenerManageReferenceDatesPageTest refDate = new AdminOpenerManageReferenceDatesPageTest();
			String referenceDate = fc.utobj().generateTestData(dataSet.get("referenceDate"));
			refDate.addReferenceDate(driver, referenceDate);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Responsible Department");
			fc.utobj().printTestStep("Add Responsible Department");

			AdminOpenerResponsibleDepartmentPageTest addDepartmentPage = new AdminOpenerResponsibleDepartmentPageTest();
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));
			addDepartmentPage.addDepartment(driver, responsibilityArea);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "openerautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporatePage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Groups");
			fc.utobj().printTestStep("Add Group");

			AdminOpenerManageGroupsPageTest addGroupsPage = new AdminOpenerManageGroupsPageTest();
			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			addGroupsPage.addGroups(driver, groupName);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Task Checklist");
			fc.opener().opener_common().adminFranchiseOpenerTaskChecklist(driver);

			fc.utobj().printTestStep("Configure Default Value");
			fc.utobj().selectActionMenuItems(driver, "Configure Default Values");

			fc.utobj().selectValFromMultiSelect(driver, pobj.selectResponsibility, responsibilityArea);
			fc.utobj().selectValFromMultiSelect(driver, pobj.selectContact, corpUser.getuserFullName());
			fc.utobj().selectValFromMultiSelect(driver, pobj.selectStoreType, storeType);

			fc.utobj().selectDropDown(driver, pobj.groupDropDown, groupName);

			String franchiseeAccess = dataSet.get("franchiseeAccess");
			fc.utobj().selectDropDown(driver, pobj.franchiseeAccessDropDown, franchiseeAccess);

			String priority = dataSet.get("priority");
			fc.utobj().selectDropDown(driver, pobj.priorityDropDown, priority);

			String criticalLevel = dataSet.get("criticalLevel");
			fc.utobj().selectDropDown(driver, pobj.criticalLevelDropDown, criticalLevel);

			String dependentOn = referenceDate;
			String valueText = fc.utobj()
					.getElementByXpath(driver,
							".//select[@name='smTaskChecklist_0referenceParent']/option[contains(text () , '"
									+ dependentOn + "')]")
					.getAttribute("value");
			fc.utobj().moveToElement(driver, pobj.dependentOnDropDown);
			Select sl = new Select(pobj.dependentOnDropDown);
			sl.selectByValue(valueText);

			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("Verify The Configure Default Value");
			String textResponsibility = driver
					.findElement(By.xpath(".//*[@id='ms-parentsmTaskChecklist_0responsibilityArea']/button/span"))
					.getText().trim();
			String textContact = driver
					.findElement(By.xpath(".//*[@id='ms-parentsmTaskChecklist_0contact']/button/span")).getText()
					.trim();
			String textStoreType = driver
					.findElement(By.xpath(".//*[@id='ms-parentsmTaskChecklist_0storeTypeId']/button/span")).getText()
					.trim();

			if (textResponsibility.equalsIgnoreCase(responsibilityArea)
					&& textContact.contains(corpUser.getuserFullName()) && textStoreType.equalsIgnoreCase(storeType)) {
			} else {
				fc.utobj().throwsException("was not able to Configure default value");
			}

			configureDefaultValue(driver, FranconnectUtil.config);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void configureDefaultValue(WebDriver driver, Map<String, String> config) throws Exception {

		String testCaseId = "TC_Configure_Default_Task_Checklist_01";

		if (fc.utobj().configurationTestCaseToExecute(testCaseId)) {

			try {
				AdminOpenerTaskChecklistPage pobj = new AdminOpenerTaskChecklistPage(driver);
				fc.opener().opener_common().adminFranchiseOpenerTaskChecklist(driver);
				fc.utobj().selectActionMenuItems(driver, "Configure Default Values");

				fc.utobj().setToDefault(driver, pobj.selectResponsibility);
				fc.utobj().setToDefault(driver, pobj.selectContact);
				fc.utobj().setToDefault(driver, pobj.selectStoreType);

				fc.utobj().selectDropDown(driver, pobj.groupDropDown, "Select");
				fc.utobj().selectDropDown(driver, pobj.franchiseeAccessDropDown, "Select");
				fc.utobj().selectDropDown(driver, pobj.priorityDropDown, "Select");
				fc.utobj().selectDropDown(driver, pobj.criticalLevelDropDown, "Select");
				String dependentOn = "None (Timeless)";
				String valueText = fc.utobj()
						.getElementByXpath(driver,
								".//select[@name='smTaskChecklist_0referenceParent']/option[contains(text () , '"
										+ dependentOn + "')]")
						.getAttribute("value");
				fc.utobj().moveToElement(driver, pobj.dependentOnDropDown);
				Select sl = new Select(pobj.dependentOnDropDown);
				sl.selectByValue(valueText);
				fc.utobj().clickElement(driver, pobj.saveBtn);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {

		}
	}

	@Test(groups = { "opener" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Modify Task Check List At Bottom:: Admin > Opener > Task Checklist", testCaseId = "TC_11_Modify_Task_Checklist02")
	public void modifyTaskChecklistBottom() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminOpenerTaskChecklistPage pobj = new AdminOpenerTaskChecklistPage(driver);
			driver = fc.loginpage().login(driver);
			String task = "";

			fc.utobj().printTestStep("Enable Divisional User");
			AdminHiddenLinksConfigureNewHierarchyLevelPageTest lirarchy_level = new AdminHiddenLinksConfigureNewHierarchyLevelPageTest();
			lirarchy_level.ConfigureNewHierarchyLevel(driver);

			fc.utobj().printTestStep("Add Task Checklist");
			task = addTaskChecklistUploadFile(driver, dataSet);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[.='" + task + "']//ancestor::tr[@class='bText12' or @class='bText12 grAltRw3']/td/input"));

			fc.utobj().printTestStep("Modify The Task Checklist");
			fc.utobj().clickElement(driver, pobj.bottomModifyBtn);

			task = fc.utobj().generateTestData(dataSet.get("task"));
			fc.utobj().sendKeys(driver, pobj.taskNameTxtBox, task);
			fc.utobj().selectValFromMultiSelect(driver, pobj.selectContact, "Division");

			// non timeless
			String dependentOn = dataSet.get("dependentOnModify");
			String valueText = fc.utobj()
					.getElementByXpath(driver,
							".//select[@name='smTaskChecklist_0referenceParent']/option[contains(text () , '"
									+ dependentOn + "')]")
					.getAttribute("value");
			fc.utobj().moveToElement(driver, pobj.dependentOnDropDown);
			Select sl = new Select(pobj.dependentOnDropDown);
			sl.selectByValue(valueText);

			if (fc.utobj().isSelected(driver, pobj.webLinkRadioBtn)) {
				// do nothing
			} else {
				fc.utobj().clickElement(driver, pobj.webLinkRadioBtn);
			}

			fc.utobj().sendKeys(driver, pobj.webLinkAddressBx, dataSet.get("webLinkUrl"));

			try {

				List<WebElement> listRadioButton = pobj.applyToAll;
				if (fc.utobj().isSelected(driver,listRadioButton.get(0))) {
					// do nothing
				} else {
					fc.utobj().clickElement(driver, listRadioButton.get(0));
				}

			} catch (Exception e) {

			}

			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("Verify The Modify Checklist");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, task);

			if (isTextPresent == false) {
				fc.utobj().throwsSkipException("Was not able to modify Task Checklist");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Delete Task Check List At Bottom :: Admin > Opener > Task Checklist", testCaseId = "TC_12_Delete_Task_Checklist02")
	public void deleteTaskChecklistBottom() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminOpenerTaskChecklistPage pobj = new AdminOpenerTaskChecklistPage(driver);
			driver = fc.loginpage().login(driver);
			String task = "";

			fc.utobj().printTestStep("Add Task Checklist");
			task = addTaskChecklistUploadFile(driver, dataSet);

			fc.utobj().printTestStep("Delete The Checklist");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[.='" + task + "']//ancestor::tr[@class='bText12' or @class='bText12 grAltRw3']/td/input"));
			fc.utobj().clickElement(driver, pobj.bottomDeleteBtn);

			fc.utobj().acceptAlertBox(driver);

			try {

				fc.commonMethods().switch_cboxIframe_frameId(driver);
				if (fc.utobj().isSelected(driver,pobj.applyToAll.get(0))) {
					// do nothing
				} else {
					fc.utobj().clickElement(driver, pobj.applyToAll.get(0));
				}

				fc.utobj().clickElement(driver, pobj.cboxDeleteBtn);
				fc.utobj().clickElement(driver, pobj.cboxCloseBtn);
				fc.utobj().switchFrameToDefault(driver);

			} catch (Exception e) {

			}

			fc.utobj().printTestStep("Verify The Delete Checklist");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, task);

			if (isTextPresent == true) {
				fc.utobj().throwsSkipException("Was not able to delete Task Checklist");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

}
