package com.builds.test.opener;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminAreaRegionAddAreaRegionPageTest;
import com.builds.uimaps.opener.OpenerArchivedStoreListPage;
import com.builds.uimaps.opener.OpenerStoreSummaryStoreListPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class OpenerArchivedStoreListPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Modify Store :: Opener >  Archived Store List", testCaseId = "TC_209_Modify_Store")
	public void modifyStoreActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			String emailId = "openerautomation@staffex.com";
			OpenerArchivedStoreListPage pobj = new OpenerArchivedStoreListPage(driver);

			String licenseNo = fc.utobj().generateRandomNumber();
			// String
			// userName=fc.utobj().generateTestData(dataSet.get("userName"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));

			fc.utobj().printTestStep("Navigate To Admin > Add Area/Region");
			fc.utobj().printTestStep("Add Region");

			AdminAreaRegionAddAreaRegionPageTest addRegionPage = new AdminAreaRegionAddAreaRegionPageTest();
			addRegionPage.addAreaRegion(driver, regionName);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");

			/*
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * corporatePage=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * corporatePage.addCorporateUser(driver, userName, config);
			 */

			fc.utobj().printTestStep("Add and Archive Store");
			String regionName1 = fc.utobj().generateTestData(dataSet.get("regionName"));
			OpenerStoreSummaryStoreListPageTest storeSummaryPage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = storeSummaryPage.archiveStore(driver, dataSet, regionName1, emailId);

			fc.opener().opener_common().openerArchived(driver);
			storeSummaryPage.setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			fc.utobj().printTestStep("Modify The Store");
			actionImgOption(driver, storeNoFranchiseID, "Modify");

			fc.utobj().selectDropDown(driver, pobj.areaRegionDrp, regionName);
			/*
			 * fc.utobj().selectDropDown(driver, pobj.divisionName,
			 * divisionName);
			 */
			fc.utobj().sendKeys(driver, pobj.licenseNo, licenseNo);
			String currentDate = fc.utobj().currentDate();
			fc.utobj().sendKeys(driver, pobj.expectedOpeningDate, currentDate);
			// fc.utobj().clickElement(driver, pobj.calanderClose);
			/*
			 * fc.utobj().selectDropDown(driver, pobj.projectStatus,
			 * projectStatus);
			 */
			fc.utobj().sendKeys(driver, pobj.streetAddressTextBox, dataSet.get("streetAddress"));
			fc.utobj().sendKeys(driver, pobj.address2TextBox, dataSet.get("address"));
			fc.utobj().sendKeys(driver, pobj.cityTextBox, dataSet.get("city"));
			fc.utobj().selectDropDown(driver, pobj.countryDropdown, "USA");
			fc.utobj().sendKeys(driver, pobj.zipcodeTextBox, "123456");
			fc.utobj().selectDropDown(driver, pobj.stateProvinceDropdown, "Alaska");
			fc.utobj().sendKeys(driver, pobj.PhoneTextBox, "1236547896");
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("Verify The Modify The Store");
			fc.opener().opener_common().openerArchived(driver);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			storeSummaryPage.setDefaultFilterOpener(driver, storeNoFranchiseID);

			if (!fc.utobj().getElementByXpath(driver, ".//a[.='" + storeNoFranchiseID + "']").isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to verify store name");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "opener" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Move to Info Mgr :: Opener >  Archived Store List", testCaseId = "TC_210_Move_To_Info_Mgr")
	public void moveToInfoMgrActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			String emailId = "openerautomation@staffex.com";
			OpenerArchivedStoreListPage pobj = new OpenerArchivedStoreListPage(driver);

			fc.utobj().printTestStep("Add and Archive Store");

			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));

			OpenerStoreSummaryStoreListPageTest storeSummaryPage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = storeSummaryPage.archiveStore(driver, dataSet, regionName, emailId);

			String parentWindow = driver.getWindowHandle();

			fc.utobj().printTestStep("Move To Info Mgr");
			fc.opener().opener_common().openerArchived(driver);
			storeSummaryPage.setDefaultFilterOpener(driver, storeNoFranchiseID);
			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			actionImgOption(driver, storeNoFranchiseID, "Move To Info Mgr");
			fc.utobj().acceptAlertBox(driver);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.okBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().printTestStep("Verify The Move To Info Mgr");
			// storeSummaryPage.setDefaultFilterOpener(driver,
			// storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.fimSearchString,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.fimSearchBtn);
			 */

			/*
			 * fc.utobj().clickElement(driver, pobj.showFilter);
			 * fc.utobj().selectValFromMultiSelect(driver, pobj.storeNumber,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchBtn);
			 */

			filterAtInfo(driver, pobj, storeNoFranchiseID);

			if (!fc.utobj().getElementByXpath(driver, ".//a[.='" + storeNoFranchiseID + "']").isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to was not able to verify store");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//a[.='" + regionName + "']").isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to verify regionName");
			}

			fc.opener().opener_common().openerArchived(driver);
			new OpenerStoreSummaryStoreListPageTest().setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			String alterText = driver
					.findElement(
							By.xpath(".//*[.='" + storeNoFranchiseID + "']/ancestor::tr/td/div[@id='menuBar']/layer"))
					.getAttribute("id").trim();
			alterText = alterText.replace("Actions_dynamicmenu", "");
			alterText = alterText.replace("Bar", "");

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[.='" + storeNoFranchiseID + "']/ancestor::tr/td/div[@id='menuBar']/layer/a/img"));

			List<WebElement> list = driver
					.findElements(By.xpath(".//div[@id='Actions_dynamicmenu" + alterText + "Menu']/span"));

			String text[] = new String[list.size()];

			for (int i = 0; i < list.size(); i++) {
				text[i] = list.get(i).getText().trim();
			}

			for (int j = 0; j < list.size(); j++) {

				System.out.println(text[j]);

				if (text[j].equalsIgnoreCase("Move To Info Mgr")) {
					fc.utobj().throwsException("was not able to verify Move to info Mgr Link at Action Img");
				}
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "opener" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Unarchive Store :: Opener >  Archived Store List", testCaseId = "TC_211_Unarchive_Store")
	public void unarchiveStoreActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			String emailId = "openerautomation@staffex.com";

			fc.utobj().printTestStep("Add and Archive Store");

			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			OpenerStoreSummaryStoreListPageTest storeSummaryPage = new OpenerStoreSummaryStoreListPageTest();

			String storeNoFranchiseID = storeSummaryPage.archiveStore(driver, dataSet, regionName, emailId);
			fc.opener().opener_common().openerArchived(driver);
			storeSummaryPage.setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			fc.utobj().printTestStep("Unarchive Store");
			actionImgOption(driver, storeNoFranchiseID, "Unarchive");

			fc.utobj().acceptAlertBox(driver);

			fc.opener().opener_common().openerStoreSummary(driver);
			storeSummaryPage.setDefaultFilterOpener(driver, storeNoFranchiseID);

			if (!fc.utobj().getElementByXpath(driver, ".//a[.='" + storeNoFranchiseID + "']").isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to was not able to verify store");
			}

			fc.utobj().printTestStep("Verify The Unarchive Store");
			fc.opener().opener_common().openerArchived(driver);

			boolean isTextPresent = fc.utobj().searchInSelectBoxSingleValue(driver,
					new OpenerStoreSummaryStoreListPage(driver).selectStoreNumber, storeNoFranchiseID);

			if (isTextPresent) {
				fc.utobj().throwsException("was not able to unacrchive a store");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "opener" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Move to Info Mgr :: Opener >  Archived Store List", testCaseId = "TC_212_Move_To_Info_Mgr")
	public void moveToInfoMgrActionBtn() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			String emailId = "openerautomation@staffex.com";
			OpenerArchivedStoreListPage pobj = new OpenerArchivedStoreListPage(driver);

			fc.utobj().printTestStep("Add and Archive Store");
			/*
			 * String projectStatus=fc.utobj().generateTestData(dataSet.get(
			 * "projectStatus"));
			 */
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			OpenerStoreSummaryStoreListPageTest storeSummaryPage = new OpenerStoreSummaryStoreListPageTest();
			/*
			 * String divisionName=fc.utobj().generateTestData(dataSet.get(
			 * "divisionName"));
			 */
			String storeNoFranchiseID = storeSummaryPage.archiveStore(driver, dataSet, regionName, emailId);

			String parentWindow = driver.getWindowHandle();

			fc.opener().opener_common().openerArchived(driver);

			storeSummaryPage.setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[.='" + storeNoFranchiseID + "']/ancestor::tr/td/input[@name='selectedItem']"));

			fc.utobj().printTestStep("Move To Info Mgr By Action Button Option");
			fc.utobj().selectActionMenuItems(driver, "Move to Info Mgr");

			fc.utobj().acceptAlertBox(driver);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.okBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().printTestStep("Verify The Move To info Mgr");
			// storeSummaryPage.setDefaultFilterOpener(driver,
			// storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.fimSearchString,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.fimSearchBtn);
			 */

			filterAtInfo(driver, pobj, storeNoFranchiseID);

			if (!fc.utobj().getElementByXpath(driver, ".//a[.='" + storeNoFranchiseID + "']").isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to was not able to verify store");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//a[.='" + regionName + "']").isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to verify regionName");
			}

			fc.opener().opener_common().openerArchived(driver);
			storeSummaryPage.setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			String alterText = driver
					.findElement(
							By.xpath(".//*[.='" + storeNoFranchiseID + "']/ancestor::tr/td/div[@id='menuBar']/layer"))
					.getAttribute("id").trim();
			alterText = alterText.replace("Actions_dynamicmenu", "");
			alterText = alterText.replace("Bar", "");

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[.='" + storeNoFranchiseID + "']/ancestor::tr/td/div[@id='menuBar']/layer/a/img"));

			List<WebElement> list = driver
					.findElements(By.xpath(".//div[@id='Actions_dynamicmenu" + alterText + "Menu']/span"));

			String text[] = new String[list.size()];

			for (int i = 0; i < list.size(); i++) {
				text[i] = list.get(i).getText().trim();
			}

			for (int j = 0; j < list.size(); j++) {

				System.out.println(text[j]);

				if (text[j].equalsIgnoreCase("Move To Info Mgr")) {
					fc.utobj().throwsException("was not able to verify Move to info Mgr Link at Action Img");
				}
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "opener" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Unarchive Store :: Opener >  Archived Store List", testCaseId = "TC_213_Unarchive_Store")
	public void unarchiveStoreActionBtn() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			String emailId = "openerautomation@staffex.com";
			fc.utobj().printTestStep("Add and Archive Store");

			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			OpenerStoreSummaryStoreListPageTest storeSummaryPage = new OpenerStoreSummaryStoreListPageTest();

			String storeNoFranchiseID = storeSummaryPage.archiveStore(driver, dataSet, regionName, emailId);

			fc.opener().opener_common().openerArchived(driver);
			storeSummaryPage.setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[.='" + storeNoFranchiseID + "']/ancestor::tr/td/input[@name='selectedItem']"));

			fc.utobj().printTestStep("Unarchive Store By Action Button Option");
			fc.utobj().selectActionMenuItems(driver, "Unarchive");

			fc.utobj().acceptAlertBox(driver);

			fc.opener().opener_common().openerStoreSummary(driver);

			storeSummaryPage.setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			if (!fc.utobj().getElementByXpath(driver, ".//a[.='" + storeNoFranchiseID + "']").isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to was not able to verify store");
			}

			fc.utobj().printTestStep("Verify The Unarchive Store");
			fc.opener().opener_common().openerArchived(driver);

			/*
			 * storeSummaryPage.setDefaultFilterOpener(driver,
			 * storeNoFranchiseID);
			 * 
			 * boolean isTextPresent=fc.utobj().assertNotInPageSource(driver,
			 * storeNoFranchiseID);
			 * 
			 * if (isTextPresent==true) { fc.utobj().throwsSkipException(
			 * "was not able to unacrchive a store"); }
			 */

			boolean isTextPresent = fc.utobj().searchInSelectBoxSingleValue(driver,
					new OpenerStoreSummaryStoreListPage(driver).selectStoreNumber, storeNoFranchiseID);

			if (isTextPresent) {
				fc.utobj().throwsException("was not able to unacrchive a store");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "opener" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Move to Info Mgr :: Opener >  Archived Store List", testCaseId = "TC_214_Move_To_Info_Mgr")
	public void moveToInfoMgrBottomBtn() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			OpenerArchivedStoreListPage pobj = new OpenerArchivedStoreListPage(driver);
			String emailId = "openerautomation@staffex.com";
			fc.utobj().printTestStep("Add and Archive Store");
			/*
			 * String projectStatus=fc.utobj().generateTestData(dataSet.get(
			 * "projectStatus"));
			 */
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			OpenerStoreSummaryStoreListPageTest storeSummaryPage = new OpenerStoreSummaryStoreListPageTest();
			/*
			 * String divisionName=fc.utobj().generateTestData(dataSet.get(
			 * "divisionName"));
			 */
			String storeNoFranchiseID = storeSummaryPage.archiveStore(driver, dataSet, regionName, emailId);

			String parentWindow = driver.getWindowHandle();

			fc.opener().opener_common().openerArchived(driver);
			storeSummaryPage.setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			fc.utobj().printTestStep("Move To Info Mgr By Bottom Button Option");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[.='" + storeNoFranchiseID + "']/ancestor::tr/td/input[@name='selectedItem']"));
			fc.utobj().clickElement(driver, pobj.moveToInfoMgrBottomBtn);

			fc.utobj().acceptAlertBox(driver);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.okBtn);
			driver.switchTo().window(parentWindow);

			// storeSummaryPage.setDefaultFilterOpener(driver,
			// storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.fimSearchString,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.fimSearchBtn);
			 */

			filterAtInfo(driver, pobj, storeNoFranchiseID);

			if (!fc.utobj().getElementByXpath(driver, ".//a[.='" + storeNoFranchiseID + "']").isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to was not able to verify store");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//a[.='" + regionName + "']").isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to verify regionName");
			}

			fc.utobj().printTestStep("Verify The Move To Info Mgr");
			fc.opener().opener_common().openerArchived(driver);

			storeSummaryPage.setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			String alterText = driver
					.findElement(
							By.xpath(".//*[.='" + storeNoFranchiseID + "']/ancestor::tr/td/div[@id='menuBar']/layer"))
					.getAttribute("id").trim();
			alterText = alterText.replace("Actions_dynamicmenu", "");
			alterText = alterText.replace("Bar", "");

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[.='" + storeNoFranchiseID + "']/ancestor::tr/td/div[@id='menuBar']/layer/a/img"));

			List<WebElement> list = driver
					.findElements(By.xpath(".//div[@id='Actions_dynamicmenu" + alterText + "Menu']/span"));

			String text[] = new String[list.size()];

			for (int i = 0; i < list.size(); i++) {
				text[i] = list.get(i).getText().trim();
			}

			for (int j = 0; j < list.size(); j++) {

				System.out.println(text[j]);

				if (text[j].equalsIgnoreCase("Move To Info Mgr")) {
					fc.utobj().throwsException("was not able to verify Move to info Mgr Link at Action Img");
				}
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "opener" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Unarchive Store Opener >  Archived Store List", testCaseId = "TC_215_Unarchive_Store")
	public void unarchiveStoreBottomBtn() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			OpenerArchivedStoreListPage pobj = new OpenerArchivedStoreListPage(driver);
			String emailId = "openerautomation@staffex.com";
			fc.utobj().printTestStep("Add and Archive Store");

			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			OpenerStoreSummaryStoreListPageTest storeSummaryPage = new OpenerStoreSummaryStoreListPageTest();

			String storeNoFranchiseID = storeSummaryPage.archiveStore(driver, dataSet, regionName, emailId);

			fc.opener().opener_common().openerArchived(driver);

			storeSummaryPage.setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			fc.utobj().printTestStep("Unarchive Store By Bottom Button option");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[.='" + storeNoFranchiseID + "']/ancestor::tr/td/input[@name='selectedItem']"));
			fc.utobj().clickElement(driver, pobj.unarchiveBottomBtn);

			fc.utobj().acceptAlertBox(driver);

			fc.opener().opener_common().openerStoreSummary(driver);
			storeSummaryPage.setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			if (!fc.utobj().getElementByXpath(driver, ".//a[.='" + storeNoFranchiseID + "']").isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to verify store");
			}

			fc.utobj().printTestStep("Verify The Unarchive Store");
			fc.opener().opener_common().openerArchived(driver);

			/*
			 * storeSummaryPage.setDefaultFilterOpener(driver,
			 * storeNoFranchiseID);
			 */

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			boolean isTextPresent = fc.utobj().searchInSelectBoxSingleValue(driver,
					new OpenerStoreSummaryStoreListPage(driver).selectStoreNumber, storeNoFranchiseID);

			if (isTextPresent) {
				fc.utobj().throwsException("was not able to unacrchive a store");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void actionImgOption(WebDriver driver, String task, String option) throws Exception {

		String alterText = driver
				.findElement(By.xpath(".//*[.='" + task + "']/ancestor::tr/td/div[@id='menuBar']/layer"))
				.getAttribute("id").trim();
		alterText = alterText.replace("Actions_dynamicmenu", "");
		alterText = alterText.replace("Bar", "");

		fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
				".//*[.='" + task + "']/ancestor::tr/td/div[@id='menuBar']/layer/a/img"));
		fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
				".//div[@id='Actions_dynamicmenu" + alterText + "Menu']/span[contains(text () , '" + option + "')]"));

	}

	public void filterAtInfo(WebDriver driver, OpenerArchivedStoreListPage pobj, String storeNoFranchiseID)
			throws Exception {
		fc.utobj().clickElement(driver, pobj.showFilter);
		fc.utobj().selectValFromMultiSelect(driver, pobj.storeNumber, storeNoFranchiseID);
		fc.utobj().clickElement(driver, pobj.searchBtn);
	}
}
