package com.builds.test.opener;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.opener.OpenerModulePage;
import com.builds.utilities.FranconnectUtil;

public class OpenerModulePageTest {
	FranconnectUtil fc = new FranconnectUtil();

	public void storeSummaryTab(WebDriver driver) throws Exception {

		fc.opener().opener_common().openerModule(driver);
		OpenerModulePage pobj = new OpenerModulePage(driver);
		fc.utobj().clickElement(driver, pobj.storeSummaryTab);
	}

	public void searchTab(WebDriver driver) throws Exception {

		fc.opener().opener_common().openerModule(driver);
		OpenerModulePage pobj = new OpenerModulePage(driver);
		fc.utobj().clickElement(driver, pobj.searchTab);
	}

	public void tasksTab(WebDriver driver) throws Exception {

		fc.opener().opener_common().openerModule(driver);
		OpenerModulePage pobj = new OpenerModulePage(driver);
		fc.utobj().clickElement(driver, pobj.taskTab);
	}

	public void archivedTab(WebDriver driver) throws Exception {

		fc.opener().opener_common().openerModule(driver);
		OpenerModulePage pobj = new OpenerModulePage(driver);
		fc.utobj().clickElement(driver, pobj.archivedTab);
	}

	public void reportsTab(WebDriver driver) throws Exception {

		fc.opener().opener_common().openerModule(driver);
		OpenerModulePage pobj = new OpenerModulePage(driver);
		fc.utobj().clickElement(driver, pobj.reportsTab);
	}

}
