package com.builds.test.opener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import org.xml.sax.SAXException;

import com.builds.uimaps.infomgr.InfoMgrPagination;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class OpenerPaginationTest {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "opener" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Store_Openerr_Paging_Report_Filter_StoreList", testCaseDescription = "This test case will verify StoreList Paging all the summary of  Store Opener Module ", reference = {
			"" })
	public void VerifyOpnerSummaryPagingStoreList() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("verify Paging for  Store List ");
			showPagingStoreOpnerModule(config, driver, "StoreList", 20);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "opener" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Store_Openerr_Paging_Report_Filter_ArchivedStoreList", testCaseDescription = "This test case will verify ArchivedStoreList Paging all the summary of  Store Opener Module ", reference = {
			"" })
	public void VerifyOpnerSummaryPagingArchivedStoreList() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("verify Paging for  Archived Store List");
			showPagingStoreOpnerModule(config, driver, "ArchivedStoreList", 20);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "opener" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Store_Openerr_Paging_Report_Filter_Task", testCaseDescription = "This test case will verify Task Paging all the summary of  Store Opener Module ", reference = {
			"" })
	public void VerifyOpnerSummaryPagingTask() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("verify Paging for  Tasks - Task Checklist ");
			showPagingStoreOpnerModule(config, driver, "Tasks", 10);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "opener" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Store_Openerr_Paging_Report_Filter_Equipment", testCaseDescription = "This test case will verify Equipment Paging all the summary of  Store Opener Module ", reference = {
			"" })
	public void VerifyOpnerSummaryPagingEquipment() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("verify Paging for  Tasks - Equipment  Checklist ");
			showPagingStoreOpnerModule(config, driver, "Equipment", 10);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "opener" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Store_Openerr_Paging_Report_Filter_Document", testCaseDescription = "This test case will verify Document Paging all the summary of  Store Opener Module ", reference = {
			"" })
	public void VerifyOpnerSummaryPagingDocument() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("verify Paging for  Tasks - Document   Checklist ");
			showPagingStoreOpnerModule(config, driver, "Document", 10);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "opener" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Store_Openerr_Paging_Report_Filter", testCaseDescription = "This test case will verify Paging all the summary of  Store Opener Module ", reference = {
			"" })
	public void VerifyOpnerSummaryPaging() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("verify Paging for  Tasks - Picture    Checklist ");
			showPagingStoreOpnerModule(config, driver, "Picture", 10);

			// End Lead Summary paging

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void showPagingStoreOpnerModule(Map<String, String> config, WebDriver driver, String subModuleName,
			int viewPerPage)
			throws ParserConfigurationException, SAXException, IOException, InterruptedException, Exception

	{
		String testCaseId = "TC_Store_Openerr_Paging_Report_Filter";
		InfoMgrPagination InfoMgrPagination = new InfoMgrPagination(driver);

		if (subModuleName == null)
			subModuleName = "StoreList";
		List<WebElement> records = null;
		fc.utobj().printTestStep("Search " + subModuleName + " and click");

		if (subModuleName != null && "StoreList".equals(subModuleName)) {
			fc.opener().opener_common().openerStoreSummary(driver);
			try {
				records = driver.findElements(By
						.xpath(".//*[@id='siteMainTable']/tbody/tr/td/form/table/tbody/tr/td/table/tbody/tr/td/table"));
				// Thread.sleep(1000);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (subModuleName != null && ("Tasks".equals(subModuleName) || "Equipment".equals(subModuleName)
				|| "Document".equals(subModuleName) || "Picture".equals(subModuleName))) {

			try {
				fc.opener().opener_common().openerTasks(driver);
				records = driver.findElements(By
						.xpath(".//*[@id='siteMainTable']/tbody/tr/td/form/table/tbody/tr/td/table/tbody/tr/td/table"));
				// Thread.sleep(1000);
			} catch (Exception e) {
				// e.printStackTrace();
				fc.utobj().printTestStep(subModuleName + " Not working in proper.");
			}
		} else if (subModuleName != null && "ArchivedStoreList".equals(subModuleName)) {
			fc.opener().opener_common().openerArchived(driver);
			try {
				records = driver.findElements(By
						.xpath(".//*[@id='siteMainTable']/tbody/tr/td/form/table/tbody/tr/td/table/tbody/tr/td/table"));
				// Thread.sleep(1000);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		try {
			records = driver.findElements(By.xpath(".//*[@id='siteMainTable']/tbody/tr/td/div"));
			// Thread.sleep(1000);
		} catch (Exception e) {
			// e.printStackTrace();
			fc.utobj().printTestStep(subModuleName + " Not opernational in proper.");
		}

		String header;
		int totalRecordCount = 0;
		int totalrecordPerPage = 0;
		String opnerPageingCheckList = "pageid";
		if (subModuleName != null && ("Tasks".equals(subModuleName))) {
			opnerPageingCheckList = "pageId1";
		} else if (subModuleName != null && ("Equipment".equals(subModuleName))) {
			opnerPageingCheckList = "pageId2";
		} else if (subModuleName != null && ("Document".equals(subModuleName))) {
			opnerPageingCheckList = "pageId3";
		} else if (subModuleName != null && ("Picture".equals(subModuleName))) {
			opnerPageingCheckList = "pageId4";
		}
		try {
			header = fc.utobj().getElementByXpath(driver, ".//*[@id='" + opnerPageingCheckList + "']").getText();
			String options[] = header.split(" ");
			if (options != null) {
				totalRecordCount = Integer.valueOf(options[5]);
				totalrecordPerPage = Integer.valueOf(options[3]);
			}
			fc.utobj().printTestStep(subModuleName + " ToTal Record " + totalRecordCount);

		} catch (Exception E) {
			fc.utobj().printTestStep("Pagination not found in " + subModuleName);
			// System.out.println("Pagination not found");
			totalRecordCount = 0;
		}

		if (totalRecordCount > 0 && totalRecordCount > viewPerPage) {
			fc.utobj().printTestStep("Pagination exists in " + subModuleName);

			int clickCount = totalRecordCount / viewPerPage;
			String optionsShoud[] = new String[clickCount + 1];
			;
			int inLastRerord = totalRecordCount - (viewPerPage * clickCount);
			for (int cnt = 0; cnt <= clickCount; cnt++) {
				optionsShoud[cnt] = viewPerPage + "";
				if (cnt == clickCount) {
					optionsShoud[cnt] = inLastRerord + "";
				}
			}
			int foundRecordCount = 0;
			int counter = 1;
			try {
				if (optionsShoud != null && optionsShoud.length > 0) {
					for (int i = 0; i < (optionsShoud.length); i++) {
						// System.out.println("===Val=="+lstOptions.get(i)
						// +"==New=="+lstOptions.get(i).getText()
						// +"===clickCount=="+clickCount+"optionsShoud=="+optionsShoud[i]);
						if (i != 0) {

							if (i == 1 && i != 0) {
								fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
										".//*[@id='" + opnerPageingCheckList + "']/a[" + (i) + "]/u"));
								counter = i;

							} else if (i > 1 && i != 0) {
								fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
										".//*[@id='" + opnerPageingCheckList + "']/a[" + (i + 1) + "]/u"));
								counter = i + 1;

							}
							foundRecordCount = pagingRecordsStoreOPner(driver, records, subModuleName).size();
							if (foundRecordCount > 0) {

								if (foundRecordCount == Integer.parseInt(optionsShoud[i])) {
									// System.out.println("found record
									// "+foundRecordCount+"= Test Case Pass for
									// the page "+(i));
									fc.utobj().printTestStep("" + foundRecordCount + " records for page = " + counter);
								} else {

									if (foundRecordCount <= Integer.parseInt(optionsShoud[i])) {
										// System.out.println("found record
										// "+foundRecordCount+"= Test Case Pass
										// for the Last page "+(i));
										fc.utobj().printTestStep(
												"" + foundRecordCount + " records for page = " + counter);
									} else {
										// System.out.println("found record
										// "+foundRecordCount+"= Test Case Fail
										// for the page "+(i));
										// fc.utobj().printTestStep(testCaseId,
										// "found record "+foundRecordCount+"=
										// Test Case Fail for the page "+(i));
										fc.utobj().throwsException("found record " + foundRecordCount
												+ "= Test Case Fail for the page Almost Last Page " + counter);
									}
								}
							} else {
								fc.utobj().throwsException(
										"" + foundRecordCount + " records for page " + counter + " Test case fails");
							}
							// Check for paging after sort function
							if (subModuleName != null
									&& (!"Tasks".equals(subModuleName) || !"Equipment".equals(subModuleName)
											|| !"Document".equals(subModuleName) || !"Picture".equals(subModuleName))) {
								fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
										".//table[@class='summaryTblex']/tbody/tr[1]/td[2]//a[not(*)]"));
								foundRecordCount = pagingRecordsStoreOPner(driver, records, subModuleName).size();
								if (foundRecordCount > 0) {

									if (foundRecordCount == Integer.parseInt(optionsShoud[i])) {
										// System.out.println("found record
										// "+foundRecordCount+"= Test Case Pass
										// for the page "+(i));
										fc.utobj().printTestStep(
												"After Sort record " + foundRecordCount + " for the page " + counter);
									} else {

										if (foundRecordCount <= Integer.parseInt(optionsShoud[i])) {
											// System.out.println("found record
											// "+foundRecordCount+"= Test Case
											// Pass for the Last page "+(i));
											fc.utobj().printTestStep("After Sort record " + foundRecordCount
													+ " for the page " + counter);
										} else {
											fc.utobj().throwsException("After Sort record " + foundRecordCount
													+ "= Test Case Fail for the page Almost Last Page " + (i + 1));
										}

									}
								} else {
									fc.utobj().throwsException("" + foundRecordCount + " records for page " + counter
											+ " Test case fails on click sort");
								}
								//
							}
						}
					}
				}

			} catch (Exception e) {
				e.printStackTrace();
			}

			try {
				// fc.utobj().printTestStep(testCaseId, "Reset our page to the
				// page 1 to check the results Per Page verification.");
				fc.utobj().clickElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='" + opnerPageingCheckList + "']/a[1]/u"));
				fc.utobj().selectDropDown(driver, fc.utobj().getElementByID(driver, "resultsPerPage"), "100");
				foundRecordCount = pagingRecordsStoreOPner(driver, records, subModuleName).size();
				if (foundRecordCount > 0) {

				}
				if (foundRecordCount <= 100) {
					// System.out.println("found record in Result Per page all
					// "+foundRecordCount+" test case passed at View Per Page");
					fc.utobj().printTestStep("View Per Page " + foundRecordCount + " records.");

				} else {
					if (foundRecordCount > 100) {
						fc.utobj().throwsException("View Per Page " + foundRecordCount + " records.");
					}
				}
				// Check for paging after click on Sort function
				fc.utobj().clickElement(driver,
						fc.utobj().getElementByXpath(driver, ".//table[@class='summaryTblex']/tbody/tr[1]/td[2]/a"));
				foundRecordCount = pagingRecordsStoreOPner(driver, records, subModuleName).size();
				if (foundRecordCount > 0) {

				}
				if (foundRecordCount <= 100) {
					// System.out.println("found record in Result Per page all
					// "+foundRecordCount+" test case passed at View Per Page");
					fc.utobj().printTestStep("After Sort found record in Result Per page  " + foundRecordCount
							+ " test case passed at View Per Page");

				} else {
					if (foundRecordCount > 100) {
						fc.utobj().throwsException("View Per Page " + foundRecordCount + " records.");
					}
				}

			} catch (Exception E) {
				// System.out.println("resultsPerPage Field Not Found!");
				fc.utobj().printTestStep("View Per Page  Field Not Found! in " + subModuleName);

				// totalRecordCount = 0;
			}
			try {
				if (subModuleName != null && ("Tasks".equals(subModuleName) || "Equipment".equals(subModuleName)
						|| "Document".equals(subModuleName) || "Picture".equals(subModuleName))) {
					if ("Tasks".equals(subModuleName))
						fc.utobj().moveToElement(driver, InfoMgrPagination.lnkTaskCheckListy);
					if ("Equipment".equals(subModuleName))
						fc.utobj().moveToElement(driver, InfoMgrPagination.lnkequipCheckListy);
					if ("Document".equals(subModuleName))
						fc.utobj().moveToElement(driver, InfoMgrPagination.lnkDocumentCheckListy);
					if ("Picture".equals(subModuleName))
						fc.utobj().moveToElement(driver, InfoMgrPagination.lnkpictureCheckListy);

					fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
							" .//*[@id='" + opnerPageingCheckList + "']/a/u[text()='Show All']"));
				} else {
					fc.utobj().clickLink(driver, "Show All");
				}
				// System.out.println("totalRecordCount===in show
				// all"+totalRecordCount);
				fc.utobj().printTestStep("totalRecordCount in show all " + subModuleName + " = " + totalRecordCount);
				foundRecordCount = pagingRecordsStoreOPner(driver, records, subModuleName).size();
				if (foundRecordCount > 0) {

				}
				if (totalRecordCount == foundRecordCount) {
					// System.out.println("found record in show all
					// "+foundRecordCount+" test case passed");
					fc.utobj().printTestStep("found record in show all " + foundRecordCount + " test case passed");
				} else {
					if (foundRecordCount > 0) {
						if (totalRecordCount != foundRecordCount) {
							fc.utobj()
									.throwsException("Show All " + foundRecordCount + " records not working correct.");
						}
					}
				}
				// System.out.println("found record "+foundRecordCount+" in
				// "+lstOptions.get(i).getText()+" th Page");

			} catch (Exception E) {
				// System.out.println("Show All Field Not Found!");
				fc.utobj().printTestStep("Show All field not found!");
				// totalRecordCount = 0;
			}

		} else {
			// System.out.println("Pagination Not Exists");
			fc.utobj().printTestStep("Pagination not exists in " + subModuleName);
		}

		// sortedList
		try {
			boolean isSorttable = sortedListOpnerModule(driver, config, records, subModuleName);
			if (isSorttable == true) {
				fc.utobj().printTestStep("Sorting function working for  " + subModuleName);
			} else {
				fc.utobj().printTestStep("Sorting function not Working for  " + subModuleName + " OR not applicable.");
			}

		} catch (Exception e) {
			// e.printStackTrace();
			fc.utobj().printTestStep("Eception in Sorting function which Working for  " + subModuleName);
		}

		// End sortedList

		fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
	}

	public boolean sortedListOpnerModule(WebDriver driver, Map<String, String> config, List<WebElement> records,
			String subModuleName) {

		boolean isSorttable = false;
		try {
			records = pagingRecordsStoreOPner(driver, records, subModuleName);
			ArrayList<String> obtainedList = new ArrayList<>();
			ArrayList<String> sortedList = new ArrayList<>();
			List<WebElement> elementList = records; // driver.findElements(By.xpath(YourLocator));
			for (WebElement we : elementList) {
				obtainedList.add(we.getText());
			}
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//table[@class='summaryTblex']/tbody/tr[1]/td[2]//a[not(*)]"));
			records = pagingRecordsStoreOPner(driver, records, subModuleName);
			for (WebElement we : records) {
				sortedList.add(we.getText());
			}
			try {
				if (sortedList != null) {
					for (int i = 0; i < sortedList.size(); i++) {
						if (sortedList.get(0) != obtainedList.get(0)) {
							isSorttable = true;
							break;
						}
					}
				}

			} catch (Exception e) {
				isSorttable = false;

			}

		} catch (Exception e) {
			isSorttable = false;
			// e.printStackTrace();
		}
		return isSorttable;
	}

	public List<WebElement> pagingRecordsStoreOPner(WebDriver driver, List<WebElement> records, String subModuleName) {

		if (subModuleName == null)
			subModuleName = "LeadSummary";
		List<WebElement> listofElements = null;
		int rc = 0;
		if (subModuleName != null && ("StoreList".equals(subModuleName) || "Visits".equals(subModuleName))) {
			listofElements = driver.findElements(
					By.xpath(".//table[@class='summaryTblex']/tbody/tr/td[@class='botBorder colPadding'][2]/a"));
		} else if (subModuleName != null && ("Tasks".equals(subModuleName) || "Equipment".equals(subModuleName)
				|| "Document".equals(subModuleName) || "Picture".equals(subModuleName))) {
			String hrefcontains = "smTask";
			if ("Equipment".equals(subModuleName))
				hrefcontains = "smEquip";
			if ("Document".equals(subModuleName))
				hrefcontains = "smDocu";
			if ("Picture".equals(subModuleName))
				hrefcontains = "smPict";

			listofElements = driver.findElements(By
					.xpath(".//table[@class='summaryTblex']/tbody/tr/td[@class='botBorder colPadding'][2]/a[contains(@href,'"
							+ hrefcontains + "')]"));
		} else if (subModuleName != null && "ArchivedStoreList".equals(subModuleName)) {
			listofElements = driver.findElements(
					By.xpath(".//table[@class='summaryTblex']/tbody/tr/td[@class='botBorder colPadding'][2]/a"));
		}

		rc = listofElements.size();
		return listofElements;

	}
}
