package com.builds.test.opener;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.test.support.SupportReportsPageTest;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class OpenerReportsPageTest extends FranconnectUtil {

	FranconnectUtil fc = new FranconnectUtil();
	SupportReportsPageTest rpReport = new SupportReportsPageTest();

	@Test(groups = { "openerReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click Over Status Report", testCaseId = "TC_Opener_Report_01")
	private void openerReports01() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("opener",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Opener > Reports Page > Status Report");
			fc.opener().opener_common().openReports(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'Status Report')]"));
			rpReport.setSelectAllMultiDropDown(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "openerReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click Over Master Tracking Report", testCaseId = "TC_Opener_Report_02")
	private void openerReports02() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("opener",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Opener > Reports Page > Master Tracking Report");
			fc.opener().opener_common().openReports(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'Master Tracking Report')]"));
			rpReport.setSelectAllMultiDropDown(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "openerReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click Over Task Contact Timeline Report", testCaseId = "TC_Opener_Report_03")
	private void openerReports03() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("opener",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Opener > Reports Page > Task Contact Timeline Report");
			fc.opener().opener_common().openReports(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'Task Contact Timeline Report')]"));
			rpReport.setSelectAllMultiDropDown(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "openerReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click Over Task Owner Details Report", testCaseId = "TC_Opener_Report_04")
	private void openerReports04() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("opener",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Opener > Reports Page > Task Owner Details Report");
			fc.opener().opener_common().openReports(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'Task Owner Details Report')]"));
			rpReport.setSelectAllMultiDropDown(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "openerReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click Over Overdue Task Details Report", testCaseId = "TC_Opener_Report_05")
	private void openerReports05() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("opener",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Opener > Reports Page > Overdue Task Details Report");
			fc.opener().opener_common().openReports(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'Overdue Task Details Report')]"));
			rpReport.setSelectAllMultiDropDown(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "openerReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click Over Problem Location Report", testCaseId = "TC_Opener_Report_06")
	private void openerReports06() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("opener",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Opener > Reports Page > Problem Location Report");
			fc.opener().opener_common().openReports(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'Problem Location Report')]"));
			rpReport.setSelectAllMultiDropDown(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "openerReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click Over Checklist Comment Report", testCaseId = "TC_Opener_Report_07")
	private void openerReports07() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("opener",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Opener > Reports Page > Checklist Comment Report");
			fc.opener().opener_common().openReports(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'Checklist Comment Report')]"));
			rpReport.setSelectAllMultiDropDown(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "openerReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click Over Store Opening Timeline Report", testCaseId = "TC_Opener_Report_08")
	private void openerReports08() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("opener",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Opener > Reports Page > Store Opening Timeline Report");
			fc.opener().opener_common().openReports(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'Store Opening Timeline Report')]"));
			rpReport.setSelectAllMultiDropDown(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
}
