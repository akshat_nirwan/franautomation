package com.builds.test.opener;

import java.io.File;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminConfigurationConfigureStoreTypePageTest;
import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.common.CorporateUser;
import com.builds.uimaps.opener.OpenerSearchPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class OpenerSearchPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Search :Picture Checklist : :: Opener > Search", testCaseId = "TC_222_Verify_Search")
	public void verifySearch() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			OpenerSearchPage pobj = new OpenerSearchPage(driver);
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");

			String emailId = "openerautomation@staffex.com";
			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));

			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));

			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			File file = new File(filePath);

			String pictureText = fc.utobj().generateTestData(dataSet.get("pictureText"));
			AdminOpenerPictureChecklistPageTest pictureCheckListPage = new AdminOpenerPictureChecklistPageTest();

			// Add Corporate User
			AdminUsersManageCorporateUsersAddCorporateUserPageTest cu = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			cu.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Add Picture Checklist");
			pictureCheckListPage.addPictureChecklist(driver, dataSet, responsibilityArea, corpUser.getuserFullName(),
					storeType, groupName, pictureText, file, config);

			fc.opener().opener_common().openerSearch(driver);
			fc.utobj().clickElement(driver, pobj.resetBtn);

			fc.utobj().printTestStep("Search The Checklist");
			// checklist
			fc.utobj().clickElement(driver, pobj.checklistSearchBtn);

			if (fc.utobj().isSelected(driver, pobj.checklistSearchSelectAll)) {
				fc.utobj().clickElement(driver, pobj.checklistSearchSelectAll);
			}

			fc.utobj().sendKeys(driver, pobj.checklistSearchTxBx, "Picture");
			fc.utobj().clickElement(driver, pobj.checklistSearchSelectAll);
			fc.utobj().clickElement(driver, pobj.checklistSearchBtn);

			// store number
			fc.utobj().sendKeys(driver, pobj.storeNumberTxBx, storeNoFranchiseID);

			// storeType
			fc.utobj().clickElement(driver, pobj.storeTypeSearchBtn);

			if (fc.utobj().isSelected(driver, pobj.storeTypeSearchSelectAll)) {
				fc.utobj().clickElement(driver, pobj.storeTypeSearchSelectAll);
			}
			fc.utobj().sendKeys(driver, pobj.storeTypeSearchTxBx, storeType);
			fc.utobj().clickElement(driver, pobj.storeTypeSearchSelectAll);
			fc.utobj().clickElement(driver, pobj.storeTypeSearchBtn);

			// status
			fc.utobj().clickElement(driver, pobj.statusSearchBtn);

			if (fc.utobj().isSelected(driver, pobj.statusSearchSelectAll)) {
				fc.utobj().clickElement(driver, pobj.statusSearchSelectAll);
			}

			fc.utobj().sendKeys(driver, pobj.statusSearchTxBx, "Pending");
			fc.utobj().clickElement(driver, pobj.statusSearchSelectAll);
			fc.utobj().clickElement(driver, pobj.statusSearchBtn);

			// item Name
			fc.utobj().sendKeys(driver, pobj.itemNameTxBx, pictureText);

			// Project Status
			fc.utobj().clickElement(driver, pobj.projectStatusSearchBtn);

			if (fc.utobj().isSelected(driver, pobj.projectStatusSearchSelectAll)) {
				fc.utobj().clickElement(driver, pobj.projectStatusSearchSelectAll);
			}
			fc.utobj().clickElement(driver, pobj.projectStatusSearchBtn);

			// contact
			fc.utobj().clickElement(driver, pobj.contactSearchBtn);

			if (fc.utobj().isSelected(driver, pobj.contactSearchSelectAll)) {
				fc.utobj().clickElement(driver, pobj.contactSearchSelectAll);
			}

			fc.utobj().sendKeys(driver, pobj.contactSearchTxBx, corpUser.getuserFullName());
			fc.utobj().clickElement(driver, pobj.contactSearchSelectAll);
			fc.utobj().clickElement(driver, pobj.contactSearchBtn);

			// responsibility
			fc.utobj().clickElement(driver, pobj.responsibilityAreaSearchBtn);

			if (fc.utobj().isSelected(driver, pobj.responsibilityAreaSearchSelectAll)) {
				fc.utobj().clickElement(driver, pobj.responsibilityAreaSearchSelectAll);
			}

			fc.utobj().sendKeys(driver, pobj.responsibilityAreaSearchTxBx, responsibilityArea);
			fc.utobj().clickElement(driver, pobj.responsibilityAreaSearchSelectAll);
			fc.utobj().clickElement(driver, pobj.responsibilityAreaSearchBtn);

			// group
			fc.utobj().clickElement(driver, pobj.groupSearchBtn);

			if (fc.utobj().isSelected(driver, pobj.groupSearchSelectAll)) {
				fc.utobj().clickElement(driver, pobj.groupSearchSelectAll);
			}

			fc.utobj().sendKeys(driver, pobj.groupSearchTxBx, groupName);
			fc.utobj().clickElement(driver, pobj.groupSearchSelectAll);
			fc.utobj().clickElement(driver, pobj.groupSearchBtn);

			fc.utobj().clickElement(driver, pobj.searchBtn);

			fc.utobj().printTestStep("Verify The Search Checklist");
			boolean isTextPresent1 = fc.utobj().assertPageSource(driver, pictureText);

			if (isTextPresent1 == false) {
				fc.utobj().throwsException("was not able to verify Item name");
			}

			boolean isTextPresent2 = fc.utobj().assertPageSource(driver, storeNoFranchiseID);

			if (isTextPresent2 == false) {
				fc.utobj().throwsException("was not able to verify Store Number");
			}

			boolean isTextPresent3 = fc.utobj().assertPageSource(driver, responsibilityArea);

			if (isTextPresent3 == false) {
				fc.utobj().throwsException("was not able to verify responsibility Area");
			}

			boolean isTextPresent4 = fc.utobj().assertPageSource(driver, corpUser.getuserFullName());

			if (isTextPresent4 == false) {
				fc.utobj().throwsException("was not able to verify userName");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
}
