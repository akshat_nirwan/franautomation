package com.builds.test.opener;

import java.io.File;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.uimaps.opener.OpenerStoreSummaryAddDocumentsPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class OpenerStoreSummaryAddDocumentsPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Add Document :: Opener > Add Document", testCaseId = "TC_100_Add_Document")
	public void addDocument() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			OpenerStoreSummaryAddDocumentsPage pobj = new OpenerStoreSummaryAddDocumentsPage(driver);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");
			String emailId = "openerautomation@staffex.com";
			OpenerStoreSummaryStoreListPageTest storeListPage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = storeListPage.addNewFranchiseLocation(driver, dataSet, emailId);

			fc.opener().opener_common().openerStoreSummary(driver);

			storeListPage.setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			fc.utobj().printTestStep("Add Document");
			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, storeNoFranchiseID));
			fc.utobj().clickElement(driver, pobj.documentsTab);
			String documentText = fc.utobj().generateTestData(dataSet.get("documentText"));
			fc.utobj().sendKeys(driver, pobj.documentTitleTxBx, documentText);

			/*
			 * File file = new
			 * File(config.get("testDataPath").concat("/").concat("document").
			 * concat("/")+dataSet.get("uploadFile"));
			 */
			String file = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));

			fc.utobj().sendKeys(driver, pobj.uploadDocumentBx, file);
			fc.utobj().clickElement(driver, pobj.addBtn);

			fc.utobj().printTestStep("Verify The Add Document");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, documentText);

			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to add document");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addDocument(WebDriver driver, String storeNoFranchiseID, String documentText, File file)
			throws Exception {

		String testCaseId = "TC_Add_Document_Opner_Store_Summary";

		if (fc.utobj().validate(testCaseId)) {
			try {
				OpenerStoreSummaryStoreListPageTest storeListPage = new OpenerStoreSummaryStoreListPageTest();
				OpenerStoreSummaryAddDocumentsPage pobj = new OpenerStoreSummaryAddDocumentsPage(driver);
				fc.opener().opener_common().openerStoreSummary(driver);

				storeListPage.setDefaultFilterOpener(driver, storeNoFranchiseID);

				/*
				 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
				 * storeNoFranchiseID); fc.utobj().clickElement(driver,
				 * pobj.searchImgBtn);
				 */

				fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, storeNoFranchiseID));
				fc.utobj().clickElement(driver, pobj.documentsTab);
				fc.utobj().sendKeys(driver, pobj.documentTitleTxBx, documentText);
				fc.utobj().sendKeys(driver, pobj.uploadDocumentBx, file.getAbsolutePath());
				fc.utobj().clickElement(driver, pobj.addBtn);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to Add Document Store Summary");

		}
		return documentText;
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Modify Document :: opener > Modify Document", testCaseId = "TC_101_Modify_Document")
	public void modifyDocument() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			OpenerStoreSummaryAddDocumentsPage pobj = new OpenerStoreSummaryAddDocumentsPage(driver);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");

			String emailId = "openerautomation@staffex.com";
			OpenerStoreSummaryStoreListPageTest storeListPage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = storeListPage.addNewFranchiseLocation(driver, dataSet, emailId);

			fc.utobj().printTestStep("Add Document");
			String documentText = fc.utobj().generateTestData(dataSet.get("documentText"));

			fc.utobj().printTestStep("Modify The Document");
			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			File file = new File(filePath);

			documentText = addDocument(driver, storeNoFranchiseID, documentText, file);

			String alterText = fc.utobj().getElementByXpath(driver,
					".//td[contains(text () ,'" + documentText + "')]//ancestor::tr/td/div[@id='menuBar']/layer")
					.getAttribute("id").trim();
			alterText = alterText.replace("Actions_dynamicmenu", "");
			alterText = alterText.replace("Bar", "");

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//td[contains(text () ,'"
					+ documentText + "')]//ancestor::tr/td/div[@id='menuBar']/layer/a/img"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//div[@id='Actions_dynamicmenu" + alterText + "Menu']/span[contains(text () ,'Modify')]"));

			documentText = fc.utobj().generateTestData(dataSet.get("documentText"));
			fc.utobj().sendKeys(driver, pobj.documentTitleTxBx, documentText);
			fc.utobj().sendKeys(driver, pobj.uploadDocumentBx, file.getAbsolutePath());
			fc.utobj().clickElement(driver, pobj.addBtn);

			fc.utobj().printTestStep("Verify The Modify Document");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, documentText);

			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to modify document");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Delete Document :: opener > Modify Document", testCaseId = "TC_102_Delete_Document")
	public void deleteDocument() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");

			OpenerStoreSummaryStoreListPageTest storeListPage = new OpenerStoreSummaryStoreListPageTest();
			String emailId = "openerautomation@staffex.com";
			String storeNoFranchiseID = storeListPage.addNewFranchiseLocation(driver, dataSet, emailId);

			fc.utobj().printTestStep("Add Document");
			String documentText = fc.utobj().generateTestData(dataSet.get("documentText"));

			fc.utobj().printTestStep("Delete The Document");

			/*
			 * File file = new
			 * File(config.get("testDataPath").concat("/").concat("document").
			 * concat("/")+dataSet.get("uploadFile"));
			 */
			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			File file = new File(filePath);

			documentText = addDocument(driver, storeNoFranchiseID, documentText, file.getAbsoluteFile());

			String alterText = fc.utobj().getElementByXpath(driver,
					".//td[contains(text () ,'" + documentText + "')]//ancestor::tr/td/div[@id='menuBar']/layer")
					.getAttribute("id").trim();
			alterText = alterText.replace("Actions_dynamicmenu", "");
			alterText = alterText.replace("Bar", "");

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//td[contains(text () ,'"
					+ documentText + "')]//ancestor::tr/td/div[@id='menuBar']/layer/a/img"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//div[@id='Actions_dynamicmenu" + alterText + "Menu']/span[contains(text () ,'Delete')]"));
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Delete Document");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, documentText);

			if (isTextPresent == true) {
				fc.utobj().throwsException("was not able to delete document");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Add Document by Add More Link :: Opener > Add Document", testCaseId = "TC_103_Add_Document_Add_More_Link")
	public void addDocumentAddMoreLnk() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			OpenerStoreSummaryAddDocumentsPage pobj = new OpenerStoreSummaryAddDocumentsPage(driver);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");

			String emailId = "openerautomation@staffex.com";
			OpenerStoreSummaryStoreListPageTest storeListPage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = storeListPage.addNewFranchiseLocation(driver, dataSet, emailId);

			fc.opener().opener_common().openerStoreSummary(driver);

			storeListPage.setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			fc.utobj().printTestStep("Add Document By Add More Link");
			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, storeNoFranchiseID));
			fc.utobj().clickElement(driver, pobj.documentsTab);
			// fc.utobj().clickElement(driver, pobj.addMoreLnk);
			String documentText = fc.utobj().generateTestData(dataSet.get("documentText"));
			fc.utobj().sendKeys(driver, pobj.documentTitleTxBx, documentText);

			/*
			 * File file = new
			 * File(config.get("testDataPath").concat("/").concat("document").
			 * concat("/")+dataSet.get("uploadFile"));
			 */
			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			File file = new File(filePath);

			fc.utobj().sendKeys(driver, pobj.uploadDocumentBx, file.getAbsolutePath());
			fc.utobj().clickElement(driver, pobj.addBtn);

			fc.utobj().printTestStep("Verify The Add Document");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, documentText);

			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to add document");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
}
