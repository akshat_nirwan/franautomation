package com.builds.test.opener;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminConfigurationConfigureStoreTypePageTest;
import com.builds.test.admin.AdminHiddenLinksConfigureNewHierarchyLevelPageTest;
import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.common.CorporateUser;
import com.builds.uimaps.opener.OpenerStoreSummaryEquipmentChecklistPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class OpenerStoreSummaryEquipmentChecklistPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "opener" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Veriy The Complete Equipment CheckList :: Opener > Store Summary >  Equipment Checklist", testCaseId = "TC_122_Complete_Equipment_Checklist")
	public void completeTaskActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			OpenerStoreSummaryEquipmentChecklistPage pobj = new OpenerStoreSummaryEquipmentChecklistPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");
			String emailId = "openerautomation@staffex.com";
			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			fc.utobj().printTestStep("Add Equipment Checklist");
			AdminOpenerEquipmentChecklistPageTest equipmentChecklistPage = new AdminOpenerEquipmentChecklistPageTest();

			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			File file = new File(filePath);

			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));

			// Add Corporate User
			AdminUsersManageCorporateUsersAddCorporateUserPageTest cu = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			cu.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			String quantity = /* fc.utobj().generateRandomNumber(); */fc.utobj().generateRandomNumber6Digit();
			String equepmentText = equipmentChecklistPage.addEquipmentChecklistUploadFile(driver, dataSet, file,
					responsibilityArea, storeType, corpUser.getuserFullName(), quantity, config);

			String parentWindow = driver.getWindowHandle();

			fc.opener().opener_common().openerStoreSummary(driver);
			addStorePage.setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, storeNoFranchiseID));
			fc.utobj().clickElement(driver, pobj.equipmentChecklistLnk);

			// update Filter
			searchByCheckList(driver, equepmentText);

			fc.utobj().printTestStep("Complete The Checklist");
			actionImgOption(driver, equepmentText, "Complete");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String curDate = fc.utobj().currentDate();
			fc.utobj().sendKeys(driver, pobj.completionDateTxBx, curDate);

			String comments = fc.utobj().generateTestData(dataSet.get("comments"));
			fc.utobj().sendKeys(driver, pobj.commentsTxBx, comments);
			fc.utobj().clickElement(driver, pobj.completeBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().printTestStep("Verify The Complete Checklist");

			searchByCheckList(driver, equepmentText);

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , 'Complete')]").isDisplayed() && !fc
					.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + curDate + "')]").isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to verify equipment checklist status");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + quantity + "')]").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify quantity");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + responsibilityArea + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify responsibilityArea");
			}

			boolean isContactPresent = fc.utobj().assertPageSource(driver, corpUser.getuserFullName());

			if (isContactPresent == false) {
				fc.utobj().throwsException("was not able to verify Contact(s)");
			}

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, equepmentText));
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + curDate + "')]").isDisplayed()
					&& !fc.utobj().getElementByXpath(driver, ".//td[contains(text () ,'" + comments + "')]")
							.isDisplayed()
					&& !fc.utobj().getElementByXpath(driver, ".//td[contains(text () , 'Complete')]").isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to verify completion date, comments and status");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "opener" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add Comments  :: Opener > Store Summary >  Equipment Checklist", testCaseId = "TC_123_Add_Comments")
	public void addCommentActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			OpenerStoreSummaryEquipmentChecklistPage pobj = new OpenerStoreSummaryEquipmentChecklistPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");
			String emailId = "openerautomation@staffex.com";
			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			fc.utobj().printTestStep("Add Equipment Checklist");
			AdminOpenerEquipmentChecklistPageTest equipmentChecklistPage = new AdminOpenerEquipmentChecklistPageTest();

			String fileName = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			File file = new File(fileName);// new
											// File(config.get("testDataPath").concat("/").concat("document").concat("/")+dataSet.get("uploadFile"));

			String quantity = fc.utobj().generateRandomNumber6Digit();
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));

			// Add Corporate User
			AdminUsersManageCorporateUsersAddCorporateUserPageTest cu = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			cu.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			String equepmentText = equipmentChecklistPage.addEquipmentChecklistUploadFile(driver, dataSet, file,
					responsibilityArea, storeType, corpUser.getuserFullName(), quantity, config);

			String parentWindow = driver.getWindowHandle();

			fc.opener().opener_common().openerStoreSummary(driver);

			addStorePage.setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */
			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, storeNoFranchiseID));
			fc.utobj().clickElement(driver, pobj.equipmentChecklistLnk);

			// update filter
			/*
			 * fc.utobj().sendKeys(driver, pobj.equipmentSearchBx,
			 * equepmentText); fc.utobj().clickElement(driver, pobj.searchBtn);
			 */
			searchByCheckList(driver, equepmentText);

			fc.utobj().printTestStep("Add Comments");
			actionImgOption(driver, equepmentText, "Add Comments");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			String comments = fc.utobj().generateTestData(dataSet.get("comments"));
			fc.utobj().sendKeys(driver, pobj.commentsTxBx, comments);
			fc.utobj().clickElement(driver, pobj.addBtn);
			driver.switchTo().window(parentWindow);

			/*
			 * fc.utobj().sendKeys(driver, pobj.equipmentSearchBx,
			 * equepmentText); fc.utobj().clickElement(driver, pobj.searchBtn);
			 */
			searchByCheckList(driver, equepmentText);

			fc.utobj().printTestStep("Verify The Add Comments");
			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, equepmentText));
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + fc.utobj().currentDate() + "')]")
					.isDisplayed()
					&& !fc.utobj().getElementByXpath(driver, ".//td[contains(text () ,'" + comments + "')]")
							.isDisplayed()
					&& !fc.utobj().getElementByXpath(driver, ".//td[contains(text () , 'Pending')]").isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to verify comments date, comments and task status");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + quantity + "')]").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify quantity");
			}

			/*
			 * if(!fc.utobj().getElementByXpath(driver,
			 * ".//td[contains(text () , '" +supplierName+"')]").isDisplayed()){
			 * fc.utobj().throwsException( "was not able to verify supplier"); }
			 */

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + responsibilityArea + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify responsibilityArea");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + corpUser.getuserFullName() + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Contact(s)");
			}

			/*
			 * if(!fc.utobj().getElementByXpath(driver,".//td[.='"+groupName+
			 * "']")). isDisplayed()){ fc.utobj().throwsException(
			 * "was not able to verify Group Name"); }
			 */

			fc.utobj().clickElement(driver, pobj.closeBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "opener" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Task Status In Progress :: Opener > Store Summary >  Equipment Checklist", testCaseId = "TC_124_Verify_Task_Status")
	public void verifyTaskStatusInProgress() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			OpenerStoreSummaryEquipmentChecklistPage pobj = new OpenerStoreSummaryEquipmentChecklistPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));

			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Add Equipment Checklist");
			AdminOpenerEquipmentChecklistPageTest equipmentChecklistPage = new AdminOpenerEquipmentChecklistPageTest();

			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			File file = new File(filePath);

			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));

			String quantity = fc.utobj().generateRandomNumber6Digit();
			AdminUsersManageCorporateUsersAddCorporateUserPageTest cu = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "openerautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			cu.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			String storeType1 = "Default All selected";
			String equepmentText = equipmentChecklistPage.addEquipmentChecklistUploadFile(driver, dataSet, file,
					responsibilityArea, storeType1, corpUser.getuserFullName(), quantity, config);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");

			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			String parentWindow = driver.getWindowHandle();

			fc.opener().opener_common().openerStoreSummary(driver);
			addStorePage.setDefaultFilterOpener(driver, storeNoFranchiseID);
			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, storeNoFranchiseID));
			fc.utobj().clickElement(driver, pobj.equipmentChecklistLnk);

			/*
			 * fc.utobj().sendKeys(driver, pobj.equipmentSearchBx,
			 * equepmentText); fc.utobj().clickElement(driver, pobj.searchBtn);
			 */
			// updateFilter
			searchByCheckList(driver, equepmentText);

			if (!fc.utobj().getElementByXpath(driver, ".//a[.='" + equepmentText + "']").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify task with all store type");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , 'Pending')]").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Pending status of Task");
			}

			fc.utobj().printTestStep("Change Status Of Checklist");
			actionImgOption(driver, equepmentText, "In Progress");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Status Of Checklist");
			/*
			 * fc.utobj().sendKeys(driver, pobj.equipmentSearchBx,
			 * equepmentText); fc.utobj().clickElement(driver, pobj.searchBtn);
			 */
			searchByCheckList(driver, equepmentText);

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , 'In Progress')]").isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to verify In Progress Status of task");
			}

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, equepmentText));
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + fc.utobj().currentDate() + "')]")
					.isDisplayed()
					&& !fc.utobj().getElementByXpath(driver, ".//td[contains(text () , 'In Progress')]")
							.isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to verify date and equipment checklist status");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + quantity + "')]").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify quantity");
			}

			/*
			 * if(!fc.utobj().getElementByXpath(driver,
			 * ".//td[contains(text () , '" +supplierName+"')]").isDisplayed()){
			 * fc.utobj().throwsException( "was not able to verify supplier"); }
			 */

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + responsibilityArea + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify responsibilityArea");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + corpUser.getuserFullName() + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Contact(s)");
			}
			fc.utobj().clickElement(driver, pobj.closeBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Modification of Task :: Opener > Store Summary >  Equipment Checklist", testCaseId = "TC_125_Modify_Task")
	public void modifyTaskActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			OpenerStoreSummaryEquipmentChecklistPage pobj = new OpenerStoreSummaryEquipmentChecklistPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");
			String emailId = "openerautomation@staffex.com";
			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			fc.utobj().printTestStep("Add Equipment Checklist");
			AdminOpenerEquipmentChecklistPageTest equipmentChecklistPage = new AdminOpenerEquipmentChecklistPageTest();

			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			File file = new File(filePath);

			/*
			 * String supplierName=fc.utobj().generateTestData(dataSet.get(
			 * "supplierName"));
			 */
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));

			String quantity = fc.utobj().generateRandomNumber();

			// Add Corporate User
			AdminUsersManageCorporateUsersAddCorporateUserPageTest cu = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			cu.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			String equepmentText = equipmentChecklistPage.addEquipmentChecklistUploadFile(driver, dataSet, file,
					responsibilityArea, storeType, corpUser.getuserFullName(), quantity, config);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Responsible Department");
			fc.utobj().printTestStep("Add Responsible Department");

			AdminOpenerResponsibleDepartmentPageTest addDepartmentPage = new AdminOpenerResponsibleDepartmentPageTest();
			responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));
			addDepartmentPage.addDepartment(driver, responsibilityArea);

			fc.opener().opener_common().openerStoreSummary(driver);
			addStorePage.setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */
			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, storeNoFranchiseID));
			fc.utobj().clickElement(driver, pobj.equipmentChecklistLnk);

			/*
			 * fc.utobj().sendKeys(driver, pobj.equipmentSearchBx,
			 * equepmentText); fc.utobj().clickElement(driver, pobj.searchBtn);
			 */
			// update Filter
			searchByCheckList(driver, equepmentText);

			fc.utobj().printTestStep("Modify The Checklist");
			actionImgOption(driver, equepmentText, "Modify");

			// 73448
			if (!fc.utobj().getElementByXpath(driver, ".//a[contains(text () , 'taskFile.pdf')]").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify upload file name while modifying task");
			}

			equepmentText = fc.utobj().generateTestData(dataSet.get("equepmentText"));
			fc.utobj().sendKeys(driver, pobj.equipmentNameTxtBox, equepmentText);

			quantity = fc.utobj().generateRandomNumber();
			fc.utobj().sendKeys(driver, pobj.quantityTxBx, quantity);

			/*
			 * fc.utobj().selectDropDown(driver, pobj.supplierDropDown,
			 * supplierName);
			 */

			fc.utobj().clickElement(driver, pobj.responsibilityAreaSelctBtn);
			fc.utobj().clickElement(driver, pobj.responsibilityAreaSelectAllBtn);
			fc.utobj().clickElement(driver, pobj.responsibilityAreaSelectAllBtn);
			fc.utobj().sendKeys(driver, pobj.responsibilityAreaTextBx, responsibilityArea);
			fc.utobj().clickElement(driver, pobj.responsibilityAreaSelectAllBtn);
			fc.utobj().clickElement(driver, pobj.responsibilityAreaSelctBtn);

			fc.utobj().clickElement(driver, pobj.ContactSelectBtn);
			fc.utobj().clickElement(driver, pobj.contactSelectAll);
			fc.utobj().clickElement(driver, pobj.contactSelectAll);
			fc.utobj().sendKeys(driver, pobj.contactTextBox, "Regional User");
			fc.utobj().clickElement(driver, pobj.contactSelectAll);

			/*
			 * fc.utobj().selectDropDown(driver, pobj.groupDropDown, groupName);
			 */

			String dependentOn = dataSet.get("dependentOnModify");
			String valueText = fc.utobj()
					.getElementByXpath(driver,
							".//select[@name='smUserEquipmentChecklist_0referenceParent']/option[contains(text () , '"
									+ dependentOn + "')]")
					.getAttribute("value");
			fc.utobj().moveToElement(driver, pobj.dependentOnDropDown);
			Select sl = new Select(pobj.dependentOnDropDown);
			sl.selectByValue(valueText);

			fc.utobj().sendKeys(driver, pobj.startScheduleDateTxtBox, "1");
			fc.utobj().selectDropDown(driver, pobj.scheduleStartDropDown, "Days Prior");
			fc.utobj().sendKeys(driver, pobj.completionScheduleDateTxtBox, "1");
			fc.utobj().selectDropDown(driver, pobj.scheduleCompletionDropDown, "Days Prior");
			fc.utobj().sendKeys(driver, pobj.startReminderTxtBox, "2");
			fc.utobj().sendKeys(driver, pobj.completionReminderTxtBox, "2");

			if (fc.utobj().isSelected(driver, pobj.webLinkRadioBtn)) {
				// do nothing
			} else {
				fc.utobj().clickElement(driver, pobj.webLinkRadioBtn);
			}

			fc.utobj().sendKeys(driver, pobj.webLinkAddressBx, dataSet.get("webLinkUrl"));

			try {
				List<WebElement> listRadioButton = pobj.applyToAll;

				if (fc.utobj().isSelected(driver,listRadioButton.get(2))) {
				} else {
					fc.utobj().clickElement(driver, listRadioButton.get(2));
				}

				// select
				fc.utobj().clickElement(driver, pobj.currentOpnerSelectBtn);
				fc.utobj().clickElement(driver, pobj.selectAllOpnerCheck);
			} catch (Exception e) {

			}
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("Verify The Modify Checklist");
			searchByCheckList(driver, equepmentText);

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , 'Overdue')]").isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to modify Equipment checklist task Status");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () ,'" + responsibilityArea + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to modify responsibility area");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () ,'Regional User')]").isDisplayed()) {
				fc.utobj().throwsException("was not able to modify Contact");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//a[.='" + equepmentText + "']").isDisplayed()) {
				fc.utobj().throwsException("was not able to modify equipment checklist task");
			}

			/*
			 * if(!fc.utobj().getElementByXpath(driver,
			 * ".//td[contains(text () ,'" +supplierName+"')]").isDisplayed()){
			 * fc.utobj().throwsException(
			 * "was not able to modify supplier in Equipment checklist task"); }
			 */

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () ,'" + quantity + "')]").isDisplayed()) {
				fc.utobj().throwsException("was not able to modify supplier in Equipment checklist task");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Deletion of Task :: Opener > Store Summary >  Equipment Checklist", testCaseId = "TC_126_Delete_Task")
	public void deleteTaskActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			OpenerStoreSummaryEquipmentChecklistPage pobj = new OpenerStoreSummaryEquipmentChecklistPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");
			String emailId = "openerautomation@staffex.com";
			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			fc.utobj().printTestStep("Add Equipment Checklist");
			AdminOpenerEquipmentChecklistPageTest equipmentChecklistPage = new AdminOpenerEquipmentChecklistPageTest();

			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			File file = new File(filePath);

			/*
			 * String supplierName=fc.utobj().generateTestData(dataSet.get(
			 * "supplierName"));
			 */
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));

			/*
			 * String
			 * groupName=fc.utobj().generateTestData(dataSet.get("groupName"));
			 */
			String quantity = fc.utobj().generateRandomNumber();

			// Add Corporate User
			AdminUsersManageCorporateUsersAddCorporateUserPageTest cu = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			cu.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			String equepmentText = equipmentChecklistPage.addEquipmentChecklistUploadFile(driver, dataSet, file,
					responsibilityArea, storeType, corpUser.getuserFullName(), quantity, config);

			String parentWindow = driver.getWindowHandle();

			fc.opener().opener_common().openerStoreSummary(driver);
			addStorePage.setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */
			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, storeNoFranchiseID));
			fc.utobj().clickElement(driver, pobj.equipmentChecklistLnk);

			/*
			 * fc.utobj().sendKeys(driver, pobj.equipmentSearchBx,
			 * equepmentText); fc.utobj().clickElement(driver, pobj.searchBtn);
			 */
			// update Filter
			searchByCheckList(driver, equepmentText);

			fc.utobj().printTestStep("Delete The Checklist Action Image Option");
			actionImgOption(driver, equepmentText, "Delete");
			fc.utobj().acceptAlertBox(driver);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().printTestStep("Verify The Delete Checklist");
			/*
			 * fc.utobj().sendKeys(driver, pobj.equipmentSearchBx,
			 * equepmentText); fc.utobj().clickElement(driver, pobj.searchBtn);
			 */
			searchByCheckList(driver, equepmentText);

			if (!fc.utobj().getElementByXpath(driver, ".//td[.='No records found.']").isDisplayed()) {
				fc.utobj().throwsException("was not able to delete task at Equipment Checklist page");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add Description :: Opener > Store Summary >  Equipment Checklist", testCaseId = "TC_127_Add_Description")
	public void addDescriptionActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			OpenerStoreSummaryEquipmentChecklistPage pobj = new OpenerStoreSummaryEquipmentChecklistPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");
			String emailId = "openerautomation@staffex.com";
			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			fc.utobj().printTestStep("Add Equipment Checklist");
			AdminOpenerEquipmentChecklistPageTest equipmentChecklistPage = new AdminOpenerEquipmentChecklistPageTest();

			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			File file = new File(filePath);

			/*
			 * String supplierName=fc.utobj().generateTestData(dataSet.get(
			 * "supplierName"));
			 */
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));

			/*
			 * String
			 * groupName=fc.utobj().generateTestData(dataSet.get("groupName"));
			 */
			String quantity = fc.utobj().generateRandomNumber();

			// Add Corporate User
			AdminUsersManageCorporateUsersAddCorporateUserPageTest cu = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			cu.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			String equepmentText = equipmentChecklistPage.addEquipmentChecklistUploadFile(driver, dataSet, file,
					responsibilityArea, storeType, corpUser.getuserFullName(), quantity, config);

			String parentWindow = driver.getWindowHandle();

			fc.opener().opener_common().openerStoreSummary(driver);
			addStorePage.setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */
			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, storeNoFranchiseID));
			fc.utobj().clickElement(driver, pobj.equipmentChecklistLnk);

			/*
			 * fc.utobj().sendKeys(driver, pobj.equipmentSearchBx,
			 * equepmentText); fc.utobj().clickElement(driver, pobj.searchBtn);
			 */
			// update Filter
			searchByCheckList(driver, equepmentText);

			fc.utobj().printTestStep("Add Description By Action Image Option");
			actionImgOption(driver, equepmentText, "Add Description");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().sendKeys(driver, pobj.descriptionTxBx, description);
			fc.utobj().clickElement(driver, pobj.addBtn);
			driver.switchTo().window(parentWindow);

			/*
			 * fc.utobj().sendKeys(driver, pobj.equipmentSearchBx,
			 * equepmentText); fc.utobj().clickElement(driver, pobj.searchBtn);
			 */
			searchByCheckList(driver, equepmentText);

			fc.utobj().printTestStep("Verify The Add Description");
			actionImgOption(driver, equepmentText, "Modify Description");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			String isTextPresent = pobj.descriptionTxBx.getText().trim();
			if (!isTextPresent.equalsIgnoreCase(description)) {
				fc.utobj().throwsException("was not able to add description");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Modification of Description :: Opener > Store Summary >  Equipment Checklist", testCaseId = "TC_128_Modify_Description")
	public void modifyDescriptionActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			OpenerStoreSummaryEquipmentChecklistPage pobj = new OpenerStoreSummaryEquipmentChecklistPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");
			String emailId = "openerautomation@staffex.com";
			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			fc.utobj().printTestStep("Add Equipment Checklist");
			AdminOpenerEquipmentChecklistPageTest equipmentChecklistPage = new AdminOpenerEquipmentChecklistPageTest();

			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			File file = new File(filePath);

			/*
			 * String supplierName=fc.utobj().generateTestData(dataSet.get(
			 * "supplierName"));
			 */
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));

			/*
			 * String
			 * groupName=fc.utobj().generateTestData(dataSet.get("groupName"));
			 */
			String quantity = fc.utobj().generateRandomNumber();

			// Add Corporate User
			AdminUsersManageCorporateUsersAddCorporateUserPageTest cu = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			cu.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			String equepmentText = equipmentChecklistPage.addEquipmentChecklistUploadFile(driver, dataSet, file,
					responsibilityArea, storeType, corpUser.getuserFullName(), quantity, config);

			String parentWindow = driver.getWindowHandle();

			fc.opener().opener_common().openerStoreSummary(driver);
			addStorePage.setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */
			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, storeNoFranchiseID));
			fc.utobj().clickElement(driver, pobj.equipmentChecklistLnk);

			/*
			 * fc.utobj().sendKeys(driver, pobj.equipmentSearchBx,
			 * equepmentText); fc.utobj().clickElement(driver, pobj.searchBtn);
			 */
			// update Filter
			searchByCheckList(driver, equepmentText);

			fc.utobj().printTestStep("Add Description");
			actionImgOption(driver, equepmentText, "Add Description");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().sendKeys(driver, pobj.descriptionTxBx, description);
			fc.utobj().clickElement(driver, pobj.addBtn);
			driver.switchTo().window(parentWindow);

			/*
			 * fc.utobj().sendKeys(driver, pobj.equipmentSearchBx,
			 * equepmentText); fc.utobj().clickElement(driver, pobj.searchBtn);
			 */
			searchByCheckList(driver, equepmentText);

			fc.utobj().printTestStep("Modify Description");
			actionImgOption(driver, equepmentText, "Modify Description");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().sendKeys(driver, pobj.descriptionTxBx, description);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().printTestStep("Verify The Modify The Description");
			actionImgOption(driver, equepmentText, "Modify Description");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			String isTextPresent = pobj.descriptionTxBx.getText().trim();
			if (!isTextPresent.equalsIgnoreCase(description)) {
				fc.utobj().throwsException("was not able to modify description");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Not Applicable :: Opener > Store Summary >  Equipment Checklist", testCaseId = "TC_129_Verify_Not_Applicable")
	public void verifyNotApplicable() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			OpenerStoreSummaryEquipmentChecklistPage pobj = new OpenerStoreSummaryEquipmentChecklistPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");
			String emailId = "openerautomation@staffex.com";
			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			fc.utobj().printTestStep("Add Equipment Checklist");
			AdminOpenerEquipmentChecklistPageTest equipmentChecklistPage = new AdminOpenerEquipmentChecklistPageTest();

			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			File file = new File(filePath);

			/*
			 * String supplierName=fc.utobj().generateTestData(dataSet.get(
			 * "supplierName"));
			 */
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));

			/*
			 * String
			 * groupName=fc.utobj().generateTestData(dataSet.get("groupName"));
			 */
			String quantity = fc.utobj().generateRandomNumber();

			// Add Corporate User
			AdminUsersManageCorporateUsersAddCorporateUserPageTest cu = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			cu.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			String equepmentText = equipmentChecklistPage.addEquipmentChecklistUploadFile(driver, dataSet, file,
					responsibilityArea, storeType, corpUser.getuserFullName(), quantity, config);

			fc.opener().opener_common().openerStoreSummary(driver);
			addStorePage.setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, storeNoFranchiseID));
			fc.utobj().clickElement(driver, pobj.equipmentChecklistLnk);

			/*
			 * fc.utobj().sendKeys(driver, pobj.equipmentSearchBx,
			 * equepmentText); fc.utobj().clickElement(driver, pobj.searchBtn);
			 */
			// update Filter
			searchByCheckList(driver, equepmentText);

			fc.utobj().printTestStep("Mark As A Not Applicable");
			actionImgOption(driver, equepmentText, "Not Applicable");
			fc.utobj().acceptAlertBox(driver);

			/*
			 * fc.utobj().sendKeys(driver, pobj.equipmentSearchBx,
			 * equepmentText); fc.utobj().clickElement(driver, pobj.searchBtn);
			 */
			searchByCheckList(driver, equepmentText);

			fc.utobj().printTestStep("Verify The Not Applicable");
			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , 'Not Applicable')]").isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to verify Not Applicable Status");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Mark As A In Complete Task :: Opener > Store Summary >  Equipment Checklist", testCaseId = "TC_130_Verify_Mark_In_Complete")
	public void verifyMarkAsInCompleteActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			OpenerStoreSummaryEquipmentChecklistPage pobj = new OpenerStoreSummaryEquipmentChecklistPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");
			String emailId = "openerautomation@staffex.com";
			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			fc.utobj().printTestStep("Add Equipment Checklist");
			AdminOpenerEquipmentChecklistPageTest equipmentChecklistPage = new AdminOpenerEquipmentChecklistPageTest();

			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			File file = new File(filePath);

			/*
			 * String supplierName=fc.utobj().generateTestData(dataSet.get(
			 * "supplierName"));
			 */
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));

			/*
			 * String
			 * groupName=fc.utobj().generateTestData(dataSet.get("groupName"));
			 */
			String quantity = fc.utobj().generateRandomNumber();

			// Add Corporate User
			AdminUsersManageCorporateUsersAddCorporateUserPageTest cu = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			cu.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			String equepmentText = equipmentChecklistPage.addEquipmentChecklistUploadFile(driver, dataSet, file,
					responsibilityArea, storeType, corpUser.getuserFullName(), quantity, config);

			String parentWindow = driver.getWindowHandle();

			fc.opener().opener_common().openerStoreSummary(driver);
			addStorePage.setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, storeNoFranchiseID));
			fc.utobj().clickElement(driver, pobj.equipmentChecklistLnk);

			// update Filter
			searchByCheckList(driver, equepmentText);

			fc.utobj().printTestStep("Complete The Checklist");
			actionImgOption(driver, equepmentText, "Complete");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String curDate = fc.utobj().currentDate();
			fc.utobj().sendKeys(driver, pobj.completionDateTxBx, curDate);
			// fc.utobj().clickElement(driver, pobj.calanderClose);
			String comments = fc.utobj().generateTestData(dataSet.get("comments"));
			fc.utobj().sendKeys(driver, pobj.commentsTxBx, comments);
			fc.utobj().clickElement(driver, pobj.completeBtn);
			driver.switchTo().window(parentWindow);

			searchByCheckList(driver, equepmentText);

			fc.utobj().printTestStep("Mark As Incomplete");
			actionImgOption(driver, equepmentText, "Mark as Incomplete");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Mark As Incomplete");
			searchByCheckList(driver, equepmentText);

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () ,'Pending')]").isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to verify Mark As InComplete status");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/*
	 * @Test(groups="opener") public void viewOverdueTaskActionsBtn() throws
	 * Exception {
	 * 
	 * Reporter.log(
	 * "TC_231_View_Overdue_Task : View OverDue Task :: Opener > Store Summary >  Equipment Checklist .\n"
	 * ); Reporter.log(
	 * "###########################################################################################"
	 * );
	 * 
	 * String testCaseId = "TC_231_View_Overdue_Task";
	 * 
	 * Map<String,String> config =
	 * fc.utobj().readConfigurationFile(FranconnectUtil.path);
	 * Map<String,String> dataSet = fc.utobj().readTestData("opener",
	 * testCaseId); WebDriver driver = fc.utobj().openDriver(config);
	 * 
	 * 
	 * try{ driver=fc.loginpage().login(driver, config);
	 * 
	 * 
	 * OpenerStoreSummaryEquipmentChecklistPage pobj=new
	 * OpenerStoreSummaryEquipmentChecklistPage(driver);
	 * 
	 * String storeType=fc.utobj().generateTestData(dataSet.get("storeType"));
	 * String regionName=fc.utobj().generateTestData(dataSet.get("regionName"));
	 * AdminConfigurationConfigureStoreTypePageTest storeTypePage=new
	 * AdminConfigurationConfigureStoreTypePageTest();
	 * storeTypePage.addStoreType(driver, storeType);
	 * 
	 * OpenerStoreSummaryStoreListPageTest addStorePage=new
	 * OpenerStoreSummaryStoreListPageTest(); String
	 * storeNoFranchiseID=addStorePage.addNewFranchiseLocation(driver,
	 * divisionName, regionName, storeType,dataSet);
	 * 
	 * AdminFOEquipmentChecklistPageTest equipmentChecklistPage=new
	 * AdminFOEquipmentChecklistPageTest(); File file = new
	 * File(config.get("testDataPath").concat("/").concat("document").concat("/"
	 * )+dataSet.get("uploadFile")); String
	 * supplierName=fc.utobj().generateTestData(dataSet.get("supplierName"));
	 * String responsibilityArea=fc.utobj().generateTestData(dataSet.get(
	 * "responsibilityArea")); String
	 * userName=fc.utobj().generateTestData(dataSet.get("userName")); String
	 * groupName=fc.utobj().generateTestData(dataSet.get("groupName")); String
	 * quantity=fc.utobj().generateRandomNumber(); String
	 * equepmentText=equipmentChecklistPage.addEquipmentChecklistUploadFile(
	 * driver, dataSet, file, supplierName, responsibilityArea, storeType,
	 * userName, groupName , quantity);
	 * 
	 * fc.nav().openerStoreSummary(driver); fc.utobj().sendKeys(driver,
	 * pobj.searchTextBx, storeNoFranchiseID); fc.utobj().clickElement(driver,
	 * pobj.searchImgBtn); fc.utobj().clickElement(driver,
	 * fc.utobj().getElementByLinkText(driver,storeNoFranchiseID)));
	 * fc.utobj().clickElement(driver, pobj.equipmentChecklistLnk);
	 * 
	 * fc.utobj().sendKeys(driver, pobj.equipmentSearchBx, equepmentText);
	 * fc.utobj().clickElement(driver, pobj.searchBtn);
	 * 
	 * fc.utobj().clickElement(driver,
	 * fc.utobj().getElementByXpath(driver,".//*[.='"+equepmentText+
	 * "']/ancestor::tr/td/input")));
	 * 
	 * fc.utobj().moveToElementThroughAction(driver, pobj.actionsBtn);
	 * fc.utobj().clickElement(driver, pobj.overdueEquipmentLink);
	 * 
	 * if(!fc.utobj().getElementByXpath(driver,
	 * ".//tr/td[contains(text () , 'Overdue')]" )).isDisplayed() &&
	 * !fc.utobj().getElementByXpath(driver,".//a[.='"+equepmentText+"']")).
	 * isDisplayed( )){ fc.utobj().throwsSkipException(
	 * "was not able to view overdue task"); }
	 * 
	 * fc.utobj().quitBrowser(driver, config);
	 * 
	 * }catch(Exception e){
	 * 
	 * fc.utobj().quitBrowserOnCatch(driver, config, e); } }
	 */

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verift The Complete Task At ActionBtn :: Opener > Store Summary >  Equipment Checklist", testCaseId = "TC_132_Complete_Task")
	public void completeTaskActionBtn() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			OpenerStoreSummaryEquipmentChecklistPage pobj = new OpenerStoreSummaryEquipmentChecklistPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");
			String emailId = "openerautomation@staffex.com";
			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			fc.utobj().printTestStep("Add Equipment Checklist");
			AdminOpenerEquipmentChecklistPageTest equipmentChecklistPage = new AdminOpenerEquipmentChecklistPageTest();

			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			File file = new File(filePath);

			/*
			 * String supplierName=fc.utobj().generateTestData(dataSet.get(
			 * "supplierName"));
			 */
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));

			/*
			 * String
			 * groupName=fc.utobj().generateTestData(dataSet.get("groupName"));
			 */
			String quantity = fc.utobj().generateRandomNumber();

			// Add Corporate User
			AdminUsersManageCorporateUsersAddCorporateUserPageTest cu = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			cu.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			String equepmentText = equipmentChecklistPage.addEquipmentChecklistUploadFile(driver, dataSet, file,
					responsibilityArea, storeType, corpUser.getuserFullName(), quantity, config);

			String parentWindow = driver.getWindowHandle();

			fc.opener().opener_common().openerStoreSummary(driver);
			addStorePage.setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, storeNoFranchiseID));
			fc.utobj().clickElement(driver, pobj.equipmentChecklistLnk);

			/*
			 * fc.utobj().sendKeys(driver, pobj.equipmentSearchBx,
			 * equepmentText); fc.utobj().clickElement(driver, pobj.searchBtn);
			 */
			// update Filter
			searchByCheckList(driver, equepmentText);

			fc.utobj().printTestStep("Complete The Checklist");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[.='" + equepmentText + "']/ancestor::tr/td/input"));

			fc.utobj().selectActionMenuItems(driver, "Complete");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String curDate = fc.utobj().currentDate();
			fc.utobj().sendKeys(driver, pobj.completionDateTxBx, curDate);
			// fc.utobj().clickElement(driver, pobj.calanderClose);
			String comments = fc.utobj().generateTestData(dataSet.get("comments"));
			fc.utobj().sendKeys(driver, pobj.commentsTxBx, comments);
			fc.utobj().clickElement(driver, pobj.completeBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().printTestStep("Verify The Complete Checklist");
			/*
			 * fc.utobj().sendKeys(driver, pobj.equipmentSearchBx,
			 * equepmentText); fc.utobj().clickElement(driver, pobj.searchBtn);
			 */
			searchByCheckList(driver, equepmentText);

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , 'Complete')]").isDisplayed() && !fc
					.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + curDate + "')]").isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to complete task");
			}

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, equepmentText));
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + curDate + "')]").isDisplayed()
					&& !fc.utobj().getElementByXpath(driver, ".//td[contains(text () ,'" + comments + "')]")
							.isDisplayed()
					&& !fc.utobj().getElementByXpath(driver, ".//td[contains(text () , 'Complete')]").isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to verify completion date, comments and status");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Deletion of Task :: Opener > Store Summary >  Equipment Checklist", testCaseId = "TC_133_Delete_Task")
	public void deleteTaskActionBtn() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			OpenerStoreSummaryEquipmentChecklistPage pobj = new OpenerStoreSummaryEquipmentChecklistPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");
			String emailId = "openerautomation@staffex.com";
			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			fc.utobj().printTestStep("Add Equipment Checklist");
			AdminOpenerEquipmentChecklistPageTest equipmentChecklistPage = new AdminOpenerEquipmentChecklistPageTest();

			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			File file = new File(filePath);

			/*
			 * String supplierName=fc.utobj().generateTestData(dataSet.get(
			 * "supplierName"));
			 */
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));

			/*
			 * String
			 * groupName=fc.utobj().generateTestData(dataSet.get("groupName"));
			 */
			String quantity = fc.utobj().generateRandomNumber();

			// Add Corporate User
			AdminUsersManageCorporateUsersAddCorporateUserPageTest cu = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			cu.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			String equepmentText = equipmentChecklistPage.addEquipmentChecklistUploadFile(driver, dataSet, file,
					responsibilityArea, storeType, corpUser.getuserFullName(), quantity, config);

			String parentWindow = driver.getWindowHandle();

			fc.opener().opener_common().openerStoreSummary(driver);
			addStorePage.setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, storeNoFranchiseID));
			fc.utobj().clickElement(driver, pobj.equipmentChecklistLnk);

			/*
			 * fc.utobj().sendKeys(driver, pobj.equipmentSearchBx,
			 * equepmentText); fc.utobj().clickElement(driver, pobj.searchBtn);
			 */
			// update Filter
			searchByCheckList(driver, equepmentText);

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[.='" + equepmentText + "']/ancestor::tr/td/input"));

			fc.utobj().printTestStep("Delete The Checklist");
			fc.utobj().selectActionMenuItems(driver, "Delete");

			fc.utobj().acceptAlertBox(driver);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().printTestStep("Verify The Delete Checklist");
			/*
			 * fc.utobj().sendKeys(driver, pobj.equipmentSearchBx,
			 * equepmentText); fc.utobj().clickElement(driver, pobj.searchBtn);
			 */
			searchByCheckList(driver, equepmentText);

			if (!fc.utobj().getElementByXpath(driver, ".//td[.='No records found.']").isDisplayed()) {
				fc.utobj().throwsException("was not able to delete task at Task Checklist page");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Modification Contact of Task :: Opener > Store Summary >  Equipment Checklist", testCaseId = "TC_134_Modify_Contact")
	public void modifyContactActionBtn() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			OpenerStoreSummaryEquipmentChecklistPage pobj = new OpenerStoreSummaryEquipmentChecklistPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");
			String emailId = "openerautomation@staffex.com";
			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			fc.utobj().printTestStep("Add Equipment Checklist");
			AdminOpenerEquipmentChecklistPageTest equipmentChecklistPage = new AdminOpenerEquipmentChecklistPageTest();

			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			File file = new File(filePath);

			/*
			 * String supplierName=fc.utobj().generateTestData(dataSet.get(
			 * "supplierName"));
			 */
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));

			/*
			 * String
			 * groupName=fc.utobj().generateTestData(dataSet.get("groupName"));
			 */
			String quantity = fc.utobj().generateRandomNumber();

			// Add Corporate User
			AdminUsersManageCorporateUsersAddCorporateUserPageTest cu = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			cu.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			String equepmentText = equipmentChecklistPage.addEquipmentChecklistUploadFile(driver, dataSet, file,
					responsibilityArea, storeType, corpUser.getuserFullName(), quantity, config);

			String parentWindow = driver.getWindowHandle();

			fc.opener().opener_common().openerStoreSummary(driver);
			addStorePage.setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, storeNoFranchiseID));
			fc.utobj().clickElement(driver, pobj.equipmentChecklistLnk);

			/*
			 * fc.utobj().sendKeys(driver, pobj.equipmentSearchBx,
			 * equepmentText); fc.utobj().clickElement(driver, pobj.searchBtn);
			 */
			// update Filter
			searchByCheckList(driver, equepmentText);

			fc.utobj().printTestStep("modify The Contact");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[.='" + equepmentText + "']/ancestor::tr/td/input"));

			fc.utobj().selectActionMenuItems(driver, "Modify Contact(s)");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.modifyContactSelectBtn);
			fc.utobj().clickElement(driver, pobj.modifyContactSelectAll);
			fc.utobj().clickElement(driver, pobj.modifyContactSelectAll);
			fc.utobj().sendKeys(driver, pobj.modifyContactSearchBx, "Regional User");
			fc.utobj().clickElement(driver, pobj.modifyContactSelectAll);
			fc.utobj().clickElement(driver, pobj.modifyContactSelectBtn);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().printTestStep("Verify The Modify The Contact");
			/*
			 * fc.utobj().sendKeys(driver, pobj.equipmentSearchBx,
			 * equepmentText); fc.utobj().clickElement(driver, pobj.searchBtn);
			 */
			searchByCheckList(driver, equepmentText);

			if (!driver
					.findElement(By.xpath(
							".//a[.='" + equepmentText + "']/ancestor::tr/td[contains(text () , 'Regional User')]"))
					.isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to modify Contact(s)");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Group View of Task :: Opener > Store Summary >  Equipment Checklist", testCaseId = "TC_135_Group_View")
	public void groupViewActionBtn() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			OpenerStoreSummaryEquipmentChecklistPage pobj = new OpenerStoreSummaryEquipmentChecklistPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");
			String emailId = "openerautomation@staffex.com";
			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			fc.utobj().printTestStep("Add Equipment Checklist");
			AdminOpenerEquipmentChecklistPageTest equipmentChecklistPage = new AdminOpenerEquipmentChecklistPageTest();

			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			File file = new File(filePath);

			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			String quantity = fc.utobj().generateRandomNumber();

			// Add Corporate User
			AdminUsersManageCorporateUsersAddCorporateUserPageTest cu = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			cu.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			/*
			 * String supplierName=fc.utobj().generateTestData(dataSet.get(
			 * "supplierName"));
			 */
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));

			String equepmentText = equipmentChecklistPage.addEquipmentChecklistUploadFileWithGroup(driver, dataSet,
					file, responsibilityArea, groupName, storeType, corpUser.getuserFullName(), quantity, config);

			fc.opener().opener_common().openerStoreSummary(driver);
			addStorePage.setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, storeNoFranchiseID));
			fc.utobj().clickElement(driver, pobj.equipmentChecklistLnk);

			/*
			 * fc.utobj().sendKeys(driver, pobj.equipmentSearchBx,
			 * equepmentText); fc.utobj().clickElement(driver, pobj.searchBtn);
			 */
			// update Filter
			searchByCheckList(driver, equepmentText);

			fc.utobj().printTestStep("Make The Group View Of Checklist");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[.='" + equepmentText + "']/ancestor::tr/td/input"));

			fc.utobj().printTestStep("Verify The Group View");
			fc.utobj().selectActionMenuItems(driver, "Group view");

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + groupName + "')]").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify group view");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Mark As A In Complete Task :: Opener > Store Summary >  Equipment Checklist", testCaseId = "TC_136_Complete_Task")
	public void completeTaskBottomBtn() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			OpenerStoreSummaryEquipmentChecklistPage pobj = new OpenerStoreSummaryEquipmentChecklistPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");
			String emailId = "openerautomation@staffex.com";
			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			fc.utobj().printTestStep("Add Equipment Checklist");
			AdminOpenerEquipmentChecklistPageTest equipmentChecklistPage = new AdminOpenerEquipmentChecklistPageTest();

			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			File file = new File(filePath);

			/*
			 * String supplierName=fc.utobj().generateTestData(dataSet.get(
			 * "supplierName"));
			 */
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));

			/*
			 * String
			 * groupName=fc.utobj().generateTestData(dataSet.get("groupName"));
			 */
			String quantity = fc.utobj().generateRandomNumber();

			// Add Corporate User
			AdminUsersManageCorporateUsersAddCorporateUserPageTest cu = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			cu.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			String equepmentText = equipmentChecklistPage.addEquipmentChecklistUploadFile(driver, dataSet, file,
					responsibilityArea, storeType, corpUser.getuserFullName(), quantity, config);

			String parentWindow = driver.getWindowHandle();

			fc.opener().opener_common().openerStoreSummary(driver);
			addStorePage.setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, storeNoFranchiseID));
			fc.utobj().clickElement(driver, pobj.equipmentChecklistLnk);

			/*
			 * fc.utobj().sendKeys(driver, pobj.equipmentSearchBx,
			 * equepmentText); fc.utobj().clickElement(driver, pobj.searchBtn);
			 */
			// update Filter
			searchByCheckList(driver, equepmentText);

			fc.utobj().printTestStep("Complete The Checklist By Bottom Button");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[.='" + equepmentText + "']/ancestor::tr/td/input"));
			fc.utobj().clickElement(driver, pobj.completeBtn);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String curDate = fc.utobj().currentDate();
			fc.utobj().sendKeys(driver, pobj.completionDateTxBx, curDate);
			// fc.utobj().clickElement(driver, pobj.calanderClose);
			String comments = fc.utobj().generateTestData(dataSet.get("comments"));
			fc.utobj().sendKeys(driver, pobj.commentsTxBx, comments);
			fc.utobj().clickElement(driver, pobj.completeBtn);
			driver.switchTo().window(parentWindow);

			/*
			 * fc.utobj().sendKeys(driver, pobj.equipmentSearchBx,
			 * equepmentText); fc.utobj().clickElement(driver, pobj.searchBtn);
			 */
			searchByCheckList(driver, equepmentText);

			fc.utobj().printTestStep("Verify The Complete Checklist");
			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , 'Complete')]").isDisplayed() && !fc
					.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + curDate + "')]").isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to complete task");
			}

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, equepmentText));
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + curDate + "')]").isDisplayed()
					&& !fc.utobj().getElementByXpath(driver, ".//td[contains(text () ,'" + comments + "')]")
							.isDisplayed()
					&& !fc.utobj().getElementByXpath(driver, ".//td[contains(text () , 'Complete')]").isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to verify completion date, comments and status");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Deletion Task at Bottom Btn:: Opener > Store Summary >  Equipment Checklist", testCaseId = "TC_137_Delete_Task")
	public void deleteTaskBottomBtn() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			OpenerStoreSummaryEquipmentChecklistPage pobj = new OpenerStoreSummaryEquipmentChecklistPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");
			String emailId = "openerautomation@staffex.com";
			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			fc.utobj().printTestStep("Add Equipment Checklist");
			AdminOpenerEquipmentChecklistPageTest equipmentChecklistPage = new AdminOpenerEquipmentChecklistPageTest();

			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			File file = new File(filePath);

			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));

			// Add Corporate User
			AdminUsersManageCorporateUsersAddCorporateUserPageTest cu = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			cu.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			String quantity = fc.utobj().generateRandomNumber();
			String equepmentText = equipmentChecklistPage.addEquipmentChecklistUploadFile(driver, dataSet, file,
					responsibilityArea, storeType, corpUser.getuserFullName(), quantity, config);

			String parentWindow = driver.getWindowHandle();

			fc.opener().opener_common().openerStoreSummary(driver);
			addStorePage.setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, storeNoFranchiseID));
			fc.utobj().clickElement(driver, pobj.equipmentChecklistLnk);

			/*
			 * fc.utobj().sendKeys(driver, pobj.equipmentSearchBx,
			 * equepmentText); fc.utobj().clickElement(driver, pobj.searchBtn);
			 */
			// update Filter
			searchByCheckList(driver, equepmentText);

			fc.utobj().printTestStep("Delete The Checklist By Bottom Button");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[.='" + equepmentText + "']/ancestor::tr/td/input"));
			fc.utobj().clickElement(driver, pobj.bottomDeleteBtn);

			fc.utobj().acceptAlertBox(driver);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().printTestStep("Verify The Delete Checklist");
			/*
			 * fc.utobj().sendKeys(driver, pobj.equipmentSearchBx,
			 * equepmentText); fc.utobj().clickElement(driver, pobj.searchBtn);
			 */
			searchByCheckList(driver, equepmentText);

			if (!fc.utobj().getElementByXpath(driver, ".//td[.='No records found.']").isDisplayed()) {
				fc.utobj().throwsException("was not able to delete task at Task Checklist page");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "opener" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Modification of Contact of Task At bottom Btn:: Opener > Store Summary >  Equipment Checklist", testCaseId = "TC_138_Modify_Contact")
	public void modifyContactBottomBtn() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Enable Divisional User");
			AdminHiddenLinksConfigureNewHierarchyLevelPageTest lirarchy_level = new AdminHiddenLinksConfigureNewHierarchyLevelPageTest();
			lirarchy_level.ConfigureNewHierarchyLevel(driver);

			String emailId = "openerautomation@staffex.com";
			OpenerStoreSummaryEquipmentChecklistPage pobj = new OpenerStoreSummaryEquipmentChecklistPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");

			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			fc.utobj().printTestStep("Add Equipment Checklist");
			AdminOpenerEquipmentChecklistPageTest equipmentChecklistPage = new AdminOpenerEquipmentChecklistPageTest();
			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			File file = new File(filePath);
			/*
			 * String supplierName=fc.utobj().generateTestData(dataSet.get(
			 * "supplierName"));
			 */
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));

			/*
			 * String
			 * groupName=fc.utobj().generateTestData(dataSet.get("groupName"));
			 */
			String quantity = fc.utobj().generateRandomNumber();

			// Add Corporate User
			AdminUsersManageCorporateUsersAddCorporateUserPageTest cu = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			cu.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			String equepmentText = equipmentChecklistPage.addEquipmentChecklistUploadFile(driver, dataSet, file,
					responsibilityArea, storeType, corpUser.getuserFullName(), quantity, config);

			String parentWindow = driver.getWindowHandle();

			fc.opener().opener_common().openerStoreSummary(driver);
			addStorePage.setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, storeNoFranchiseID));
			fc.utobj().clickElement(driver, pobj.equipmentChecklistLnk);

			/*
			 * fc.utobj().sendKeys(driver, pobj.equipmentSearchBx,
			 * equepmentText); fc.utobj().clickElement(driver, pobj.searchBtn);
			 */
			// update Filter
			searchByCheckList(driver, equepmentText);

			fc.utobj().printTestStep("modify The Contact");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[.='" + equepmentText + "']/ancestor::tr/td/input"));
			fc.utobj().clickElement(driver, pobj.modifyContactBoottomBtn);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.modifyContactSelectBtn);
			fc.utobj().clickElement(driver, pobj.modifyContactSelectAll);
			fc.utobj().clickElement(driver, pobj.modifyContactSelectAll);
			fc.utobj().sendKeys(driver, pobj.modifyContactSearchBx, "Division");
			fc.utobj().clickElement(driver, pobj.modifyContactSelectAll);
			fc.utobj().clickElement(driver, pobj.modifyContactSelectBtn);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			driver.switchTo().window(parentWindow);

			/*
			 * fc.utobj().sendKeys(driver, pobj.equipmentSearchBx,
			 * equepmentText); fc.utobj().clickElement(driver, pobj.searchBtn);
			 */
			searchByCheckList(driver, equepmentText);

			fc.utobj().printTestStep("Verify The Modify of Contact");
			if (!driver
					.findElement(By.xpath(
							".//a[.='" + equepmentText + "']/ancestor::tr/td[contains(text () , 'Divisional User')]"))
					.isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to modify Contact(s)");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "opener" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add More Link:: Opener > Store Summary >  Equipment Checklist", testCaseId = "TC_139_Add_More_Link")
	public void addMoreLink() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			String emailId = "openerautomation@staffex.com";
			OpenerStoreSummaryEquipmentChecklistPage pobj = new OpenerStoreSummaryEquipmentChecklistPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");

			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			fc.utobj().printTestStep("Add Equipment Checklist");
			AdminOpenerEquipmentChecklistPageTest equipmentChecklistPage = new AdminOpenerEquipmentChecklistPageTest();
			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			File file = new File(filePath);
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));

			String quantity = fc.utobj().generateRandomNumber();

			// Add Corporate User
			AdminUsersManageCorporateUsersAddCorporateUserPageTest cu = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			cu.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			String equepmentText = equipmentChecklistPage.addEquipmentChecklistUploadFile(driver, dataSet, file,
					responsibilityArea, storeType, corpUser.getuserFullName(), quantity, config);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Responsible Department");
			fc.utobj().printTestStep("Add Responsible Department");

			AdminOpenerResponsibleDepartmentPageTest addDepartmentPage = new AdminOpenerResponsibleDepartmentPageTest();
			responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));
			addDepartmentPage.addDepartment(driver, responsibilityArea);

			fc.utobj().printTestStep("Add Checklist By Add More Option");
			fc.opener().opener_common().openerStoreSummary(driver);
			addStorePage.setDefaultFilterOpener(driver, storeNoFranchiseID);

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, storeNoFranchiseID));
			fc.utobj().clickElement(driver, pobj.equipmentChecklistLnk);
			fc.utobj().clickElement(driver, pobj.addMoreLink);

			equepmentText = fc.utobj().generateTestData(dataSet.get("equepmentText"));
			fc.utobj().sendKeys(driver, pobj.equipmentNameTxtBox, equepmentText);

			quantity = fc.utobj().generateRandomNumber();
			fc.utobj().sendKeys(driver, pobj.quantityTxBx, quantity);

			fc.utobj().selectValFromMultiSelect(driver, pobj.selectResponsibilityArea, responsibilityArea);
			fc.utobj().selectValFromMultiSelect(driver, pobj.selectContact, "Regional User");

			String franchiseeAccess = dataSet.get("franchiseeAccess");
			fc.utobj().selectDropDown(driver, pobj.franchiseeAccessDropDown, franchiseeAccess);

			String dependentOn = dataSet.get("dependentOn");
			String valueText = fc.utobj()
					.getElementByXpath(driver,
							".//select[@name='smUserEquipmentChecklist_0referenceParent']/option[contains(text () , '"
									+ dependentOn + "')]")
					.getAttribute("value");
			fc.utobj().moveToElement(driver, pobj.dependentOnDropDown);
			Select sl = new Select(pobj.dependentOnDropDown);
			sl.selectByValue(valueText);

			if (fc.utobj().isSelected(driver,pobj.uploadFileRadioBtn)) {
				// do nothing
			} else {
				fc.utobj().clickElement(driver, pobj.uploadFileRadioBtn);
			}

			fc.utobj().sendKeys(driver, pobj.attachmentBrowseBox, file.getAbsolutePath());
			fc.utobj().clickElement(driver, pobj.addBtn);

			try {
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().clickElement(driver, pobj.noBtn);
				driver.switchTo().defaultContent();
			} catch (Exception e) {

			}

			fc.utobj().printTestStep("Verify The Add Checklist");
			searchByCheckList(driver, equepmentText);

			if (!fc.utobj().getElementByXpath(driver, ".//a[.='" + equepmentText + "']").isDisplayed()) {
				fc.utobj().throwsException("was not able to add equipment checkList");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () ,'" + quantity + "')]").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify quanity");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () ,'" + responsibilityArea + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify responsibility");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () ,'Regional User')]").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Contact");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () ,'Pending')]").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Status");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify status of dependent task after completion of tasks and before the completion of tasks:: Opener > Store Summary >  Equipment Checklist ", testCaseId = "TC_218_Verify_Initialize_Dependency")
	public void verifyInitializeDependency() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			OpenerStoreSummaryEquipmentChecklistPage pobj = new OpenerStoreSummaryEquipmentChecklistPage(driver);
			/**/

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");
			String emailId = "openerautomation@staffex.com";
			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			fc.utobj().printTestStep("Add Equipment Checklist");
			AdminOpenerEquipmentChecklistPageTest equipmentChecklistPage = new AdminOpenerEquipmentChecklistPageTest();
			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			File file = new File(filePath);
			/*
			 * String supplierName=fc.utobj().generateTestData(dataSet.get(
			 * "supplierName"));
			 */
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));
			/*
			 * String
			 * groupName=fc.utobj().generateTestData(dataSet.get("groupName"));
			 */
			String quantity = fc.utobj().generateRandomNumber();

			// Add Corporate User
			AdminUsersManageCorporateUsersAddCorporateUserPageTest cu = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			cu.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			String equepmentText1 = equipmentChecklistPage.addEquipmentChecklistUploadFile(driver, dataSet, file,
					responsibilityArea, storeType, corpUser.getuserFullName(), quantity, config);

			String parentWindow = driver.getWindowHandle();

			fc.utobj().printTestStep("Add Checklist By Add More Link And Make Dependent To Another Checklist");
			fc.opener().opener_common().openerStoreSummary(driver);

			addStorePage.setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, storeNoFranchiseID));
			fc.utobj().clickElement(driver, pobj.equipmentChecklistLnk);

			fc.utobj().clickElement(driver, pobj.addMoreLink);

			String equepmentText = fc.utobj().generateTestData(dataSet.get("equepmentText"));
			fc.utobj().sendKeys(driver, pobj.equipmentNameTxtBox, equepmentText);

			quantity = fc.utobj().generateRandomNumber();
			fc.utobj().sendKeys(driver, pobj.quantityTxBx, quantity);

			fc.utobj().selectValFromMultiSelect(driver, pobj.selectResponsibilityArea, responsibilityArea);
			fc.utobj().selectValFromMultiSelect(driver, pobj.selectContact, "Regional User");

			String franchiseeAccess = dataSet.get("franchiseeAccess");
			fc.utobj().selectDropDown(driver, pobj.franchiseeAccessDropDown, franchiseeAccess);

			fc.utobj().selectDropDownByPartialText(driver, pobj.dependentOnDropDown, "Equipment");
			fc.utobj().selectDropDown(driver,
					fc.utobj().getElementByID(driver, "smUserEquipmentChecklist_0referenceFlag"),
					"Actual Completion Date Of");
			fc.utobj().selectDropDown(driver,
					fc.utobj().getElementByID(driver, "smUserEquipmentChecklist_0referenceField_temp"), equepmentText1);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByID(driver, "smUserEquipmentChecklist_0dependencyFlag_temp"));

			fc.utobj().sendKeys(driver, pobj.startScheduleDateTxtBox, "1");
			fc.utobj().sendKeys(driver, pobj.completionScheduleDateTxtBox, "1");
			fc.utobj().sendKeys(driver, pobj.startReminderTxtBox, "2");
			fc.utobj().sendKeys(driver, pobj.completionReminderTxtBox, "2");
			fc.utobj().selectDropDown(driver, pobj.scheduleStartDropDown, "Days Prior");
			fc.utobj().selectDropDown(driver, pobj.scheduleCompletionDropDown, "Days Prior");

			if (fc.utobj().isSelected(driver,pobj.uploadFileRadioBtn)) {
			} else {
				fc.utobj().clickElement(driver, pobj.uploadFileRadioBtn);
			}

			fc.utobj().sendKeys(driver, pobj.attachmentBrowseBox, file.getAbsolutePath());
			fc.utobj().clickElement(driver, pobj.addBtn);

			try {
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().clickElement(driver, pobj.noBtn);
				driver.switchTo().defaultContent();
			} catch (Exception e) {

			}
			searchByCheckList(driver, equepmentText1);

			fc.utobj().printTestStep("Verify The Status of Checklist Before Completing The Dependent Checklist");
			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () ,'Pending')]").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Status");
			}
			searchByCheckList(driver, equepmentText);

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () ,'Unscheduled')]").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Status");
			}

			searchByCheckList(driver, equepmentText1);

			fc.utobj().printTestStep("Complete The Dependent Checklist");
			actionImgOption(driver, equepmentText1, "Complete");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String curDate = fc.utobj().currentDate();
			fc.utobj().sendKeys(driver, pobj.completionDateTxBx, curDate);
			String comments = fc.utobj().generateTestData(dataSet.get("comments"));
			fc.utobj().sendKeys(driver, pobj.commentsTxBx, comments);
			fc.utobj().clickElement(driver, pobj.completeBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().printTestStep("Verify The Checklist Status");
			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () ,'Complete')]").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Status");
			}
			searchByCheckList(driver, equepmentText);

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () ,'Overdue')]").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Status");
			}

			fc.utobj().printTestStep("Complete The Another Checklist");
			actionImgOption(driver, equepmentText, "Complete");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			curDate = fc.utobj().currentDate();
			fc.utobj().sendKeys(driver, pobj.completionDateTxBx, curDate);
			comments = fc.utobj().generateTestData(dataSet.get("comments"));
			fc.utobj().sendKeys(driver, pobj.commentsTxBx, comments);
			fc.utobj().clickElement(driver, pobj.completeBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().printTestStep("Verify the Status Of Checklist");
			searchByCheckList(driver, equepmentText);

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () ,'Complete')]").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Status");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void actionImgOption(WebDriver driver, String task, String option) throws Exception {

		String alterText = driver
				.findElement(By.xpath(".//*[.='" + task + "']/ancestor::tr/td/div[@id='menuBar']/layer"))
				.getAttribute("id").trim();
		alterText = alterText.replace("Actions_dynamicmenu", "");
		alterText = alterText.replace("Bar", "");

		fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
				".//*[.='" + task + "']/ancestor::tr/td/div[@id='menuBar']/layer/a/img"));
		fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
				".//div[@id='Actions_dynamicmenu" + alterText + "Menu']/span[contains(text () , '" + option + "')]"));
	}

	public void searchByCheckList(WebDriver driver, String equepmentText) throws Exception {

		OpenerStoreSummaryEquipmentChecklistPage pobj = new OpenerStoreSummaryEquipmentChecklistPage(driver);
		fc.utobj().sendKeys(driver, pobj.equipmentSearchBx, equepmentText);
		fc.utobj().setToDefault(driver, pobj.statusSelect);
		fc.utobj().setToDefault(driver, pobj.contactSelect);
		fc.utobj().setToDefault(driver, pobj.groupSelect);
		fc.utobj().selectDropDown(driver, pobj.scheduleStartDateDropDown, "All");
		fc.utobj().clickElement(driver, pobj.searchBtn);
	}
}
