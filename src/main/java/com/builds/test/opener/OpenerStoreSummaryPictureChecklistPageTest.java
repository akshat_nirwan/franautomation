package com.builds.test.opener;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminConfigurationConfigureStoreTypePageTest;
import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.common.CorporateUser;
import com.builds.uimaps.opener.OpenerStoreSummaryPictureChecklistPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class OpenerStoreSummaryPictureChecklistPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "opener", "openerfailed" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Picture Checklist Task Status In Progress :: Opener > Store Summary >  Picture Checklist", testCaseId = "TC_156_Verify_Task_Status_In_Progress")
	public void verifyTaskStatusInProgress() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			OpenerStoreSummaryPictureChecklistPage pobj = new OpenerStoreSummaryPictureChecklistPage(driver);

			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");
			String emailId = "openerautomation@staffex.com";
			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			fc.utobj().printTestStep("Add Picture Checklist");
			AdminOpenerPictureChecklistPageTest pictureChecklistPage = new AdminOpenerPictureChecklistPageTest();
			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			File file = new File(filePath);
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));

			// Add Corporate User
			AdminUsersManageCorporateUsersAddCorporateUserPageTest cu = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			cu.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			String pictureText = pictureChecklistPage.addPictureChecklist(driver, dataSet, responsibilityArea,
					corpUser.getuserFullName(), storeType, file, config);

			String parentWindow = driver.getWindowHandle();

			fc.opener().opener_common().openerStoreSummary(driver);

			addStorePage.setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, storeNoFranchiseID));
			fc.utobj().clickElement(driver, pobj.pictureChecklistLnk);

			fc.utobj().sendKeys(driver, pobj.pictureSearchBx, pictureText);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , 'Pending')]").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Pending status of Task");
			}

			fc.utobj().printTestStep("Change Status of Checklist By Action Image Option");
			actionImgOption(driver, pictureText, "In Progress");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Status of Checklist");
			fc.utobj().sendKeys(driver, pobj.pictureSearchBx, pictureText);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , 'In Progress')]").isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to verify In Progress Status of task");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + responsibilityArea + "')]")
					.isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to Responsibility Area");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + corpUser.getuserFullName() + "')]")
					.isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to Responsibility Area");
			}

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, pictureText));
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + fc.utobj().currentDate() + "')]")
					.isDisplayed()
					&& !fc.utobj().getElementByXpath(driver, ".//td[contains(text () , 'In Progress')]")
							.isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to verify date and checklist status");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + responsibilityArea + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify responsibilityArea");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + corpUser.getuserFullName() + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Contact(s)");
			}

			/*
			 * if(!fc.utobj().getElementByXpath(driver,".//td[.='"+groupName+
			 * "']")). isDisplayed()){ fc.utobj().throwsException(
			 * "was not able to verify Group Name"); }
			 */

			if (!fc.utobj().getElementByXpath(driver, ".//td[.='" + pictureText + "']").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Picture Name");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-30", testCaseDescription = "Verify The Upload Picture :: Opener > Store Summary >  Picture Checklist", testCaseId = "TC_157_Upload_Picture")
	public void uploadPictureActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			OpenerStoreSummaryPictureChecklistPage pobj = new OpenerStoreSummaryPictureChecklistPage(driver);

			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");
			String emailId = "openerautomation@staffex.com";
			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			fc.utobj().printTestStep("Add Picture Checklist");
			AdminOpenerPictureChecklistPageTest pictureChecklistPage = new AdminOpenerPictureChecklistPageTest();
			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			File file = new File(filePath);
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));

			// Add Corporate User
			AdminUsersManageCorporateUsersAddCorporateUserPageTest cu = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			cu.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			String pictureText = pictureChecklistPage.addPictureChecklist(driver, dataSet, responsibilityArea,
					corpUser.getuserFullName(), storeType, file, config);

			String parentWindow = driver.getWindowHandle();

			fc.opener().opener_common().openerStoreSummary(driver);

			addStorePage.setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, storeNoFranchiseID));
			fc.utobj().clickElement(driver, pobj.pictureChecklistLnk);

			fc.utobj().sendKeys(driver, pobj.pictureSearchBx, pictureText);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			fc.utobj().printTestStep("Upload The Picture By Action Image Option");
			actionImgOption(driver, pictureText, "Upload Picture");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String currentDate = fc.utobj().currentDate();
			fc.utobj().sendKeys(driver, pobj.completionDate, currentDate);
			String pictureTitle = fc.utobj().generateTestData(dataSet.get("pictureTitle"));
			fc.utobj().sendKeys(driver, pobj.pictureTitle, pictureTitle);

			/*
			 * File file1 = new
			 * File(config.get("testDataPath").concat("/").concat("document").
			 * concat("/")+dataSet.get("pictureUpload"));
			 * fc.utobj().sendKeys(driver, pobj.uploadPicAttachmentBx,
			 * file1.getAbsolutePath());
			 */

			String fileName = fc.utobj().getFilePathFromTestData(dataSet.get("pictureUpload"));
			fc.utobj().sendKeys(driver, pobj.uploadPicAttachmentBx, fileName);

			fc.utobj().clickElement(driver, pobj.completeUploadPicBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().printTestStep("Verify The Upload Picture Details");
			fc.utobj().sendKeys(driver, pobj.pictureSearchBx, pictureText);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			actionImgOption(driver, pictureText, "Upload Picture");

			fc.commonMethods().switch_cboxIframe_frameId(driver);

			String isTextPresent = pobj.completionDate.getAttribute("value").trim();
			if (!isTextPresent.equals(currentDate)) {
				fc.utobj().throwsException("was not able to verify Completion Date");
			}

			String isTextPresent1 = pobj.pictureTitle.getAttribute("value").trim();

			if (!isTextPresent1.equalsIgnoreCase(pictureTitle)) {
				fc.utobj().throwsSkipException("was not able to verify Picture Text");
			}

			if (!fc.utobj().getElementByLinkText(driver, dataSet.get("pictureUpload")).isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to verify Picture");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().sendKeys(driver, pobj.pictureSearchBx, pictureText);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , 'Complete')]").isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to verify Complete Status of task");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + responsibilityArea + "')]")
					.isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to verify Responsibility Area");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + corpUser.getuserFullName() + "')]")
					.isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to verify userName Area");
			}

			// 73777
			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + currentDate + "')]")
					.isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to verify userName Area");
			}

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, pictureText));
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + currentDate + "')]").isDisplayed()
					&& !fc.utobj().getElementByXpath(driver, ".//td[contains(text () , 'Complete')]").isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to verify date and checklist status");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + responsibilityArea + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify responsibilityArea");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + corpUser.getuserFullName() + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Contact(s)");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//td[.='" + pictureText + "']").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Picture Name");
			}

			if (!fc.utobj().getElementByLinkText(driver, dataSet.get("uploadFile")).isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to verify Picture");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "opener" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Mark As A Incomplete :: Opener > Store Summary >  Picture Checklist", testCaseId = "TC_158_Mark_As_Incomplete")
	public void markAsIncompleteActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			OpenerStoreSummaryPictureChecklistPage pobj = new OpenerStoreSummaryPictureChecklistPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));

			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");
			String emailId = "openerautomation@staffex.com";
			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			fc.utobj().printTestStep("Add Picture Checklist");
			AdminOpenerPictureChecklistPageTest pictureChecklistPage = new AdminOpenerPictureChecklistPageTest();
			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			File file = new File(filePath);
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));

			// Add Corporate User
			AdminUsersManageCorporateUsersAddCorporateUserPageTest cu = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			cu.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			String pictureText = pictureChecklistPage.addPictureChecklist(driver, dataSet, responsibilityArea,
					corpUser.getuserFullName(), storeType, file, config);

			String parentWindow = driver.getWindowHandle();

			fc.opener().opener_common().openerStoreSummary(driver);

			addStorePage.setDefaultFilterOpener(driver, storeNoFranchiseID);

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, storeNoFranchiseID));
			fc.utobj().clickElement(driver, pobj.pictureChecklistLnk);

			/*
			 * fc.utobj().sendKeys(driver, pobj.pictureSearchBx, pictureText);
			 * fc.utobj().clickElement(driver, pobj.searchBtn);
			 */

			fc.utobj().printTestStep("Upload Picture And Complete The Checklist");
			actionImgOption(driver, pictureText, "Upload Picture");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.completionDate, fc.utobj().currentDate());

			String pictureTitle = fc.utobj().generateTestData(dataSet.get("pictureTitle"));

			fc.utobj().sendKeys(driver, pobj.pictureTitle, pictureTitle);

			/*
			 * File file1 = new
			 * File(config.get("testDataPath").concat("/").concat("document").
			 * concat("/")+dataSet.get("pictureUpload"));
			 * System.out.println(file1.getAbsoluteFile());
			 */

			String file1 = fc.utobj().getFilePathFromTestData(dataSet.get("pictureUpload"));

			fc.utobj().sendKeys(driver, pobj.uploadPicAttachmentBx, file1);
			fc.utobj().clickElement(driver, pobj.completeUploadPicBtn);
			driver.switchTo().window(parentWindow);

			/*
			 * fc.utobj().sendKeys(driver, pobj.pictureSearchBx, pictureText);
			 * fc.utobj().clickElement(driver, pobj.searchBtn);
			 */

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , 'Complete')]").isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to verify Complete Status of task");
			}

			fc.utobj().printTestStep("Mark As A Incomplete");
			actionImgOption(driver, pictureText, "Mark as Incomplete");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Mark As Incomplte");

			/*
			 * fc.utobj().sendKeys(driver, pobj.pictureSearchBx, pictureText);
			 * fc.utobj().clickElement(driver, pobj.searchBtn);
			 */

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , 'Pending')]").isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to verify Complete Status of task");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + responsibilityArea + "')]")
					.isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to Responsibility Area");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + corpUser.getuserFullName() + "')]")
					.isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to Responsibility Area");
			}

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, pictureText));
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + fc.utobj().currentDate() + "')]")
					.isDisplayed()
					&& !fc.utobj().getElementByXpath(driver, ".//td[contains(text () , 'Pending')]").isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to verify date and checklist status");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + responsibilityArea + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify responsibilityArea");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + corpUser.getuserFullName() + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Contact(s)");
			}

			/*
			 * if(!fc.utobj().getElementByXpath(driver,".//td[.='"+groupName+
			 * "']")). isDisplayed()){ fc.utobj().throwsException(
			 * "was not able to verify Group Name"); }
			 */

			if (!fc.utobj().getElementByXpath(driver, ".//td[.='" + pictureText + "']").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Picture Name");
			}

			if (!fc.utobj().getElementByLinkText(driver, dataSet.get("uploadFile")).isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to verify Picture");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "opener", "openerfailed" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add Comment In Task At Opener > Store Summary >  Picture Checklist", testCaseId = "TC_159_Add_Comment")
	public void addCommentActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			OpenerStoreSummaryPictureChecklistPage pobj = new OpenerStoreSummaryPictureChecklistPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));

			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");
			String emailId = "openerautomation@staffex.com";
			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			fc.utobj().printTestStep("Add Picture Checklist");
			AdminOpenerPictureChecklistPageTest pictureChecklistPage = new AdminOpenerPictureChecklistPageTest();
			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			File file = new File(filePath);
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));

			// Add Corporate User
			AdminUsersManageCorporateUsersAddCorporateUserPageTest cu = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			cu.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			String pictureText = pictureChecklistPage.addPictureChecklist(driver, dataSet, responsibilityArea,
					corpUser.getuserFullName(), storeType, file, config);

			String parentWindow = driver.getWindowHandle();

			fc.opener().opener_common().openerStoreSummary(driver);

			addStorePage.setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, storeNoFranchiseID));
			fc.utobj().clickElement(driver, pobj.pictureChecklistLnk);

			fc.utobj().sendKeys(driver, pobj.pictureSearchBx, pictureText);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , 'Pending')]").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Pending status of Task");
			}

			fc.utobj().printTestStep("Add Comments");
			actionImgOption(driver, pictureText, "Add Comments");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String comments = fc.utobj().generateTestData(dataSet.get("comments"));
			fc.utobj().sendKeys(driver, pobj.commentsTxBx, comments);
			fc.utobj().clickElement(driver, pobj.addBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().printTestStep("Verify The Add Comments");
			fc.utobj().sendKeys(driver, pobj.pictureSearchBx, pictureText);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, pictureText));
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + fc.utobj().currentDate() + "')]")
					.isDisplayed()
					&& !fc.utobj().getElementByXpath(driver, ".//td[contains(text () , 'Pending')]").isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to verify date and checklist status");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + responsibilityArea + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify responsibilityArea");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + corpUser.getuserFullName() + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Contact(s)");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//td[.='" + pictureText + "']").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Picture Name");
			}

			if (!fc.utobj().getElementByLinkText(driver, dataSet.get("uploadFile")).isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to verify Picture");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + comments + "')]").isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to Comments");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Modification of Task :: Opener > Store Summary >  Picture Checklist", testCaseId = "TC_160_Modify_Task")
	public void modifyTaskActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			OpenerStoreSummaryPictureChecklistPage pobj = new OpenerStoreSummaryPictureChecklistPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));

			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");
			String emailId = "openerautomation@staffex.com";
			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			fc.utobj().printTestStep("Add Picture Checklist");
			AdminOpenerPictureChecklistPageTest pictureChecklistPage = new AdminOpenerPictureChecklistPageTest();
			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			File file = new File(filePath);
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));

			// Add Corporate User
			AdminUsersManageCorporateUsersAddCorporateUserPageTest cu = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			cu.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			String pictureText = pictureChecklistPage.addPictureChecklist(driver, dataSet, responsibilityArea,
					corpUser.getuserFullName(), storeType, file.getAbsoluteFile(), config);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Responsible Department");
			fc.utobj().printTestStep("Add Responsible Department");

			AdminOpenerResponsibleDepartmentPageTest addDepartmentPage = new AdminOpenerResponsibleDepartmentPageTest();
			responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));
			addDepartmentPage.addDepartment(driver, responsibilityArea);

			fc.opener().opener_common().openerStoreSummary(driver);
			addStorePage.setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, storeNoFranchiseID));
			fc.utobj().clickElement(driver, pobj.pictureChecklistLnk);

			fc.utobj().sendKeys(driver, pobj.pictureSearchBx, pictureText);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , 'Pending')]").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Pending status of Task");
			}

			fc.utobj().printTestStep("Modify The Checklist");
			actionImgOption(driver, pictureText, "Modify");

			pictureText = fc.utobj().generateTestData(dataSet.get("pictureText"));
			fc.utobj().sendKeys(driver, pobj.pictureNameTxtBox, pictureText);

			fc.utobj().clickElement(driver, pobj.responsibilityAreaSelctBtn);
			fc.utobj().clickElement(driver, pobj.responsibilityAreaSelectAllBtn);
			fc.utobj().clickElement(driver, pobj.responsibilityAreaSelectAllBtn);
			fc.utobj().sendKeys(driver, pobj.responsibilityAreaTextBx, responsibilityArea);
			fc.utobj().clickElement(driver, pobj.responsibilityAreaSelectAllBtn);
			fc.utobj().clickElement(driver, pobj.responsibilityAreaSelctBtn);

			fc.utobj().clickElement(driver, pobj.ContactSelectBtn);
			fc.utobj().clickElement(driver, pobj.contactSelectAll);
			fc.utobj().clickElement(driver, pobj.contactSelectAll);
			fc.utobj().sendKeys(driver, pobj.contactTextBox, "Regional User");
			fc.utobj().clickElement(driver, pobj.contactSelectAll);

			/*
			 * fc.utobj().selectDropDown(driver, pobj.groupDropDown, groupName);
			 */
			fc.utobj().selectDropDown(driver, pobj.franchiseeAccessDropDown, dataSet.get("franchiseeAccess"));

			String dependentOn = dataSet.get("dependentOnModify");
			String valueText = fc.utobj()
					.getElementByXpath(driver,
							".//select[@name='smUserPictureChecklist_0referenceParent']/option[contains(text () , '"
									+ dependentOn + "')]")
					.getAttribute("value");
			fc.utobj().moveToElement(driver, pobj.dependentOnDropDown);
			Select sl = new Select(pobj.dependentOnDropDown);
			sl.selectByValue(valueText);

			fc.utobj().sendKeys(driver, pobj.startScheduleDateTxtBox, "1");
			fc.utobj().selectDropDown(driver, pobj.scheduleStartDropDown, "Days Prior");
			fc.utobj().sendKeys(driver, pobj.completionScheduleDateTxtBox, "1");
			fc.utobj().selectDropDown(driver, pobj.scheduleCompletionDropDown, "Days Prior");
			fc.utobj().sendKeys(driver, pobj.startReminderTxtBox, "2");
			fc.utobj().sendKeys(driver, pobj.completionReminderTxtBox, "2");

			if (fc.utobj().isSelected(driver, pobj.webLinkRadioBtn)) {
			} else {
				fc.utobj().clickElement(driver, pobj.webLinkRadioBtn);
			}

			fc.utobj().sendKeys(driver, pobj.webLinkAddressBx, dataSet.get("webLinkUrl"));

			try {
				List<WebElement> listRadioButton = pobj.applyToAll;

				if (fc.utobj().isSelected(driver,listRadioButton.get(2))) {
					// do nothing
				} else {
					fc.utobj().clickElement(driver, listRadioButton.get(2));
				}

				// select
				fc.utobj().clickElement(driver, pobj.currentOpnerSelectBtn);
				fc.utobj().clickElement(driver, pobj.selectAllOpnerCheck);
			} catch (Exception e) {

			}
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("Verify The Modify Checklist");
			fc.utobj().sendKeys(driver, pobj.pictureSearchBx, pictureText);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , 'Overdue')]").isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to modify Picture checklist task Status");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () ,'" + responsibilityArea + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to modify responsibility area");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () ,'Regional User')]").isDisplayed()) {
				fc.utobj().throwsException("was not able to modify Contact");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//a[.='" + pictureText + "']").isDisplayed()) {
				fc.utobj().throwsException("was not able to modify equipment checklist task");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Deletion Task :: Opener > Store Summary >  Picture Checklist", testCaseId = "TC_161_Delete_Task")
	public void deleteTaskActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			OpenerStoreSummaryPictureChecklistPage pobj = new OpenerStoreSummaryPictureChecklistPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));

			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");
			String emailId = "openerautomation@staffex.com";
			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			fc.utobj().printTestStep("Add Picture Checklist");
			AdminOpenerPictureChecklistPageTest pictureChecklistPage = new AdminOpenerPictureChecklistPageTest();
			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			File file = new File(filePath);
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));

			// Add Corporate User
			AdminUsersManageCorporateUsersAddCorporateUserPageTest cu = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			cu.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			String pictureText = pictureChecklistPage.addPictureChecklist(driver, dataSet, responsibilityArea,
					corpUser.getuserFullName(), storeType, file, config);

			String parentWindow = driver.getWindowHandle();

			fc.opener().opener_common().openerStoreSummary(driver);

			addStorePage.setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, storeNoFranchiseID));
			fc.utobj().clickElement(driver, pobj.pictureChecklistLnk);

			fc.utobj().sendKeys(driver, pobj.pictureSearchBx, pictureText);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , 'Pending')]").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Pending status of Task");
			}

			fc.utobj().printTestStep("Delete Checklist");
			actionImgOption(driver, pictureText, "Delete");
			fc.utobj().acceptAlertBox(driver);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().printTestStep("Verify The Delete Checklist");
			fc.utobj().sendKeys(driver, pobj.pictureSearchBx, pictureText);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			if (!fc.utobj().getElementByXpath(driver, ".//td[.='No records found.']").isDisplayed()) {
				fc.utobj().throwsException("was not able to delete task at Document Checklist page");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add Description :: Opener > Store Summary >  Picture Checklist", testCaseId = "TC_162_Add_Description")
	public void addDescriptionActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			OpenerStoreSummaryPictureChecklistPage pobj = new OpenerStoreSummaryPictureChecklistPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));

			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");
			String emailId = "openerautomation@staffex.com";
			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			fc.utobj().printTestStep("Add Picture Checklist");
			AdminOpenerPictureChecklistPageTest pictureChecklistPage = new AdminOpenerPictureChecklistPageTest();
			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			File file = new File(filePath);
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));

			// Add Corporate User
			AdminUsersManageCorporateUsersAddCorporateUserPageTest cu = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			cu.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			String pictureText = pictureChecklistPage.addPictureChecklist(driver, dataSet, responsibilityArea,
					corpUser.getuserFullName(), storeType, file, config);

			String parentWindow = driver.getWindowHandle();

			fc.opener().opener_common().openerStoreSummary(driver);

			addStorePage.setDefaultFilterOpener(driver, storeNoFranchiseID);
			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, storeNoFranchiseID));
			fc.utobj().clickElement(driver, pobj.pictureChecklistLnk);

			fc.utobj().sendKeys(driver, pobj.pictureSearchBx, pictureText);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , 'Pending')]").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Pending status of Task");
			}

			fc.utobj().printTestStep("Add Description");
			actionImgOption(driver, pictureText, "Add Description");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().sendKeys(driver, pobj.descriptionTxBx, description);
			fc.utobj().clickElement(driver, pobj.addBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().printTestStep("Verify The Add Description");
			actionImgOption(driver, pictureText, "Modify Description");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String isTextPresent = fc.utobj().getText(driver, pobj.descriptionTxBx);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			driver.switchTo().window(parentWindow);

			if (!isTextPresent.equalsIgnoreCase(description)) {
				fc.utobj().throwsSkipException("was not able to add Description");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Modification of Description :: Opener > Store Summary >  Picture Checklist", testCaseId = "TC_163_Modify_Description")
	public void modifyDescriptionActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			OpenerStoreSummaryPictureChecklistPage pobj = new OpenerStoreSummaryPictureChecklistPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));

			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");
			String emailId = "openerautomation@staffex.com";
			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			fc.utobj().printTestStep("Add Picture Checklist");
			AdminOpenerPictureChecklistPageTest pictureChecklistPage = new AdminOpenerPictureChecklistPageTest();
			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			File file = new File(filePath);
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));

			// Add Corporate User
			AdminUsersManageCorporateUsersAddCorporateUserPageTest cu = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			cu.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			String pictureText = pictureChecklistPage.addPictureChecklist(driver, dataSet, responsibilityArea,
					corpUser.getuserFullName(), storeType, file, config);

			String parentWindow = driver.getWindowHandle();

			fc.opener().opener_common().openerStoreSummary(driver);

			addStorePage.setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, storeNoFranchiseID));
			fc.utobj().clickElement(driver, pobj.pictureChecklistLnk);

			fc.utobj().sendKeys(driver, pobj.pictureSearchBx, pictureText);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , 'Pending')]").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Pending status of Task");
			}

			fc.utobj().printTestStep("Add Description");
			actionImgOption(driver, pictureText, "Add Description");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().sendKeys(driver, pobj.descriptionTxBx, description);
			fc.utobj().clickElement(driver, pobj.addBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().printTestStep("Modify The Description");
			actionImgOption(driver, pictureText, "Modify Description");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().sendKeys(driver, pobj.descriptionTxBx, description);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().printTestStep("Verify The Modify Description");
			actionImgOption(driver, pictureText, "Modify Description");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			String isTextPresent = fc.utobj().getText(driver, pobj.descriptionTxBx);

			fc.utobj().clickElement(driver, pobj.closeBtn);
			driver.switchTo().window(parentWindow);

			if (!isTextPresent.equalsIgnoreCase(description)) {
				fc.utobj().throwsSkipException("was not able to modfiy Description");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Not Applicable Status :: Opener > Store Summary >  Picture Checklist", testCaseId = "TC_164_Verify_Not_Applicable")
	public void verifyNotApplicable() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			OpenerStoreSummaryPictureChecklistPage pobj = new OpenerStoreSummaryPictureChecklistPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));

			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");
			String emailId = "openerautomation@staffex.com";
			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			fc.utobj().printTestStep("Add Picture Checklist");
			AdminOpenerPictureChecklistPageTest pictureChecklistPage = new AdminOpenerPictureChecklistPageTest();
			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			File file = new File(filePath);
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));

			// Add Corporate User
			AdminUsersManageCorporateUsersAddCorporateUserPageTest cu = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			cu.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			String pictureText = pictureChecklistPage.addPictureChecklist(driver, dataSet, responsibilityArea,
					corpUser.getuserFullName(), storeType, file, config);

			fc.opener().opener_common().openerStoreSummary(driver);

			addStorePage.setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, storeNoFranchiseID));
			fc.utobj().clickElement(driver, pobj.pictureChecklistLnk);

			fc.utobj().sendKeys(driver, pobj.pictureSearchBx, pictureText);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , 'Pending')]").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Pending status of Task");
			}

			fc.utobj().printTestStep("Mark As A Not Applicable");
			actionImgOption(driver, pictureText, "Not Applicable");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Not Applicable");
			fc.utobj().sendKeys(driver, pobj.pictureSearchBx, pictureText);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , 'Not Applicable')]").isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to verify Not Applicable status of task");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Deletion of Task :: Opener > Store Summary >  Picture Checklist", testCaseId = "TC_165_Delete_Task")
	public void deleteTaskActionBtn() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			OpenerStoreSummaryPictureChecklistPage pobj = new OpenerStoreSummaryPictureChecklistPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));

			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");
			String emailId = "openerautomation@staffex.com";
			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			fc.utobj().printTestStep("Add Picture Checklist");
			AdminOpenerPictureChecklistPageTest pictureChecklistPage = new AdminOpenerPictureChecklistPageTest();
			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			File file = new File(filePath);
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));

			// Add Corporate User
			AdminUsersManageCorporateUsersAddCorporateUserPageTest cu = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			cu.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			String pictureText = pictureChecklistPage.addPictureChecklist(driver, dataSet, responsibilityArea,
					corpUser.getuserFullName(), storeType, file, config);

			String parentWindow = driver.getWindowHandle();

			fc.opener().opener_common().openerStoreSummary(driver);

			addStorePage.setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, storeNoFranchiseID));
			fc.utobj().clickElement(driver, pobj.pictureChecklistLnk);

			fc.utobj().sendKeys(driver, pobj.pictureSearchBx, pictureText);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , 'Pending')]").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Pending status of Task");
			}

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[.='" + pictureText + "']/ancestor::tr/td/input"));

			fc.utobj().printTestStep("Delete The Checklist");
			fc.utobj().selectActionMenuItems(driver, "Delete");

			fc.utobj().acceptAlertBox(driver);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().printTestStep("Verify The Delete Checklist");
			fc.utobj().sendKeys(driver, pobj.pictureSearchBx, pictureText);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			if (!fc.utobj().getElementByXpath(driver, ".//td[.='No records found.']").isDisplayed()) {
				fc.utobj().throwsException("was not able to delete task at Document Checklist page");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Modification of Contact(s) :: Opener > Store Summary >  Picture Checklist", testCaseId = "TC_166_Modify_Contact")
	public void modifyContactActionBtn() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			OpenerStoreSummaryPictureChecklistPage pobj = new OpenerStoreSummaryPictureChecklistPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));

			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");
			String emailId = "openerautomation@staffex.com";
			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			fc.utobj().printTestStep("Add Picture Checklist");
			AdminOpenerPictureChecklistPageTest pictureChecklistPage = new AdminOpenerPictureChecklistPageTest();
			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			File file = new File(filePath);
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));

			// Add Corporate User
			AdminUsersManageCorporateUsersAddCorporateUserPageTest cu = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			cu.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			String pictureText = pictureChecklistPage.addPictureChecklist(driver, dataSet, responsibilityArea,
					corpUser.getuserFullName(), storeType, file, config);

			String parentWindow = driver.getWindowHandle();

			fc.opener().opener_common().openerStoreSummary(driver);

			addStorePage.setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, storeNoFranchiseID));
			fc.utobj().clickElement(driver, pobj.pictureChecklistLnk);

			fc.utobj().sendKeys(driver, pobj.pictureSearchBx, pictureText);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , 'Pending')]").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Pending status of Task");
			}

			fc.utobj().printTestStep("Modify Contact");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[.='" + pictureText + "']/ancestor::tr/td/input"));

			fc.utobj().selectActionMenuItems(driver, "Modify Contact(s)");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.modifyContactSelectBtn);
			fc.utobj().sendKeys(driver, pobj.modifyContactSearchBx, "Regional User");
			fc.utobj().clickElement(driver, pobj.modifyContactSelectAll);
			fc.utobj().clickElement(driver, pobj.modifyContactSelectBtn);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().printTestStep("Verify The Modify Contact");
			fc.utobj().sendKeys(driver, pobj.pictureSearchBx, pictureText);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , 'Regional User')]").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify MOdify Contact(s)");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Group View  :: Opener > Store Summary >  Document Checklist", testCaseId = "TC_167_Verify_Group_View")
	public void verifyGroupViewActionBtn() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			OpenerStoreSummaryPictureChecklistPage pobj = new OpenerStoreSummaryPictureChecklistPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));

			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");
			String emailId = "openerautomation@staffex.com";
			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			fc.utobj().printTestStep("Add Picture Checklist");
			AdminOpenerPictureChecklistPageTest pictureChecklistPage = new AdminOpenerPictureChecklistPageTest();
			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			File file = new File(filePath);
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));

			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));

			// Add Corporate User
			AdminUsersManageCorporateUsersAddCorporateUserPageTest cu = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			cu.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			String pictureText = pictureChecklistPage.addPictureChecklistWithGroup(driver, dataSet, responsibilityArea,
					corpUser.getuserFullName(), groupName, storeType, file, config);

			fc.opener().opener_common().openerStoreSummary(driver);
			addStorePage.setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, storeNoFranchiseID));
			fc.utobj().clickElement(driver, pobj.pictureChecklistLnk);

			fc.utobj().sendKeys(driver, pobj.pictureSearchBx, pictureText);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , 'Pending')]").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Pending status of Task");
			}

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[.='" + pictureText + "']/ancestor::tr/td/input"));

			fc.utobj().printTestStep("make Group view Of Checklist");
			// need to modify
			fc.utobj().selectActionMenuItems(driver, "Group view");

			fc.utobj().printTestStep("Verify The Group view");
			fc.utobj().sendKeys(driver, pobj.pictureSearchBx, pictureText);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + groupName + "')]").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify MOdify Contact(s)");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Deletion of Task :: Opener > Store Summary >  Picture Checklist", testCaseId = "TC_168_Delete_Task")
	public void deleteTaskBottomBtn() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			OpenerStoreSummaryPictureChecklistPage pobj = new OpenerStoreSummaryPictureChecklistPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));

			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");
			String emailId = "openerautomation@staffex.com";
			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			fc.utobj().printTestStep("Add Picture Checklist");
			AdminOpenerPictureChecklistPageTest pictureChecklistPage = new AdminOpenerPictureChecklistPageTest();
			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			File file = new File(filePath);
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));

			// Add Corporate User
			AdminUsersManageCorporateUsersAddCorporateUserPageTest cu = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			cu.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			String pictureText = pictureChecklistPage.addPictureChecklist(driver, dataSet, responsibilityArea,
					corpUser.getuserFullName(), storeType, file, config);

			String parentWindow = driver.getWindowHandle();
			fc.opener().opener_common().openerStoreSummary(driver);
			addStorePage.setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, storeNoFranchiseID));
			fc.utobj().clickElement(driver, pobj.pictureChecklistLnk);

			fc.utobj().sendKeys(driver, pobj.pictureSearchBx, pictureText);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , 'Pending')]").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Pending status of Task");
			}

			fc.utobj().printTestStep("Delete Checklist bY Bottom Button");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[.='" + pictureText + "']/ancestor::tr/td/input"));
			fc.utobj().clickElement(driver, pobj.bottomDeleteBtn);
			fc.utobj().acceptAlertBox(driver);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().printTestStep("Verify The Delete Checklist");
			fc.utobj().sendKeys(driver, pobj.pictureSearchBx, pictureText);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			if (!fc.utobj().getElementByXpath(driver, ".//td[.='No records found.']").isDisplayed()) {
				fc.utobj().throwsException("was not able to delete task at Document Checklist page");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Modification of Contact(s) :: Opener > Store Summary >  Picture Checklist", testCaseId = "TC_169_Modify_Contact")
	public void modifyContactBottomBtn() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			OpenerStoreSummaryPictureChecklistPage pobj = new OpenerStoreSummaryPictureChecklistPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));

			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");
			String emailId = "openerautomation@staffex.com";
			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			fc.utobj().printTestStep("Add Picture Checklist");
			AdminOpenerPictureChecklistPageTest pictureChecklistPage = new AdminOpenerPictureChecklistPageTest();
			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			File file = new File(filePath);
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));

			// Add Corporate User
			AdminUsersManageCorporateUsersAddCorporateUserPageTest cu = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			cu.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			String pictureText = pictureChecklistPage.addPictureChecklist(driver, dataSet, responsibilityArea,
					corpUser.getuserFullName(), storeType, file, config);

			String parentWindow = driver.getWindowHandle();
			fc.opener().opener_common().openerStoreSummary(driver);
			addStorePage.setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, storeNoFranchiseID));
			fc.utobj().clickElement(driver, pobj.pictureChecklistLnk);

			fc.utobj().sendKeys(driver, pobj.pictureSearchBx, pictureText);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , 'Pending')]").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Pending status of Task");
			}

			fc.utobj().printTestStep("Modify The Contact By Bottom Button");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[.='" + pictureText + "']/ancestor::tr/td/input"));
			fc.utobj().clickElement(driver, pobj.modifyContactBoottomBtn);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.modifyContactSelectBtn);
			fc.utobj().sendKeys(driver, pobj.modifyContactSearchBx, "Regional User");
			fc.utobj().clickElement(driver, pobj.modifyContactSelectAll);
			fc.utobj().clickElement(driver, pobj.modifyContactSelectBtn);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().printTestStep("Verify The Modify The Contact");
			fc.utobj().sendKeys(driver, pobj.pictureSearchBx, pictureText);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , 'Regional User')]").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify MOdify Contact(s)");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "opener" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add More Link:: Opener > Store Summary >  Picture Checklist", testCaseId = "TC_170_Add_More_Link")
	public void addMoreLink() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			OpenerStoreSummaryPictureChecklistPage pobj = new OpenerStoreSummaryPictureChecklistPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));

			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");
			String emailId = "openerautomation@staffex.com";
			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			fc.utobj().printTestStep("Add Picture Checklist");
			AdminOpenerPictureChecklistPageTest pictureChecklistPage = new AdminOpenerPictureChecklistPageTest();
			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			File file = new File(filePath);
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));

			// Add Corporate User
			AdminUsersManageCorporateUsersAddCorporateUserPageTest cu = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			cu.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			String pictureText = pictureChecklistPage.addPictureChecklist(driver, dataSet, responsibilityArea,
					corpUser.getuserFullName(), storeType, file, config);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Responsible Department");
			fc.utobj().printTestStep("Add Responsible Department");

			AdminOpenerResponsibleDepartmentPageTest addDepartmentPage = new AdminOpenerResponsibleDepartmentPageTest();
			responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));
			addDepartmentPage.addDepartment(driver, responsibilityArea);

			fc.utobj().printTestStep("Add Checklist By Add More Link");
			fc.opener().opener_common().openerStoreSummary(driver);
			addStorePage.setDefaultFilterOpener(driver, storeNoFranchiseID);

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, storeNoFranchiseID));
			fc.utobj().clickElement(driver, pobj.pictureChecklistLnk);
			fc.utobj().clickElement(driver, pobj.addMoreLink);

			pictureText = fc.utobj().generateTestData(dataSet.get("pictureText"));
			fc.utobj().sendKeys(driver, pobj.pictureNameTxtBox, pictureText);

			fc.utobj().selectValFromMultiSelect(driver, pobj.selectResponsibility, responsibilityArea);
			fc.utobj().selectValFromMultiSelect(driver, pobj.selectContact, "Regional User");

			String franchiseeAccess = dataSet.get("franchiseeAccess");
			fc.utobj().selectDropDown(driver, pobj.franchiseeAccessDropDown, franchiseeAccess);

			String dependentOn = dataSet.get("dependentOn");
			String valueText = fc.utobj()
					.getElementByXpath(driver,
							".//select[@name='smUserPictureChecklist_0referenceParent']/option[contains(text () , '"
									+ dependentOn + "')]")
					.getAttribute("value");
			fc.utobj().moveToElement(driver, pobj.dependentOnDropDown);
			Select sl = new Select(pobj.dependentOnDropDown);
			sl.selectByValue(valueText);

			if (fc.utobj().isSelected(driver,pobj.uploadFileRadioBtn)) {
				// do nothing
			} else {
				fc.utobj().clickElement(driver, pobj.uploadFileRadioBtn);
			}

			fc.utobj().sendKeys(driver, pobj.attachmentBrowseBox, file.getAbsolutePath());
			fc.utobj().clickElement(driver, pobj.addBtn);

			try {
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().clickElement(driver, pobj.noBtn);
				driver.switchTo().defaultContent();
			} catch (Exception e) {

			}

			fc.utobj().printTestStep("Verify The Add Checklist");

			fc.utobj().sendKeys(driver, pobj.pictureSearchBx, pictureText);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			if (!fc.utobj().getElementByXpath(driver, ".//a[.='" + pictureText + "']").isDisplayed()) {
				fc.utobj().throwsException("was not able to add equipment checkList");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () ,'" + responsibilityArea + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify responsibility");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () ,'Regional User')]").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Contact");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () ,'Pending')]").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Status");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void actionImgOption(WebDriver driver, String task, String option) throws Exception {

		String alterText = driver
				.findElement(By.xpath(".//*[.='" + task + "']/ancestor::tr/td/div[@id='menuBar']/layer"))
				.getAttribute("id").trim();
		alterText = alterText.replace("Actions_dynamicmenu", "");
		alterText = alterText.replace("Bar", "");

		fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
				".//*[.='" + task + "']/ancestor::tr/td/div[@id='menuBar']/layer/a/img"));
		fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
				".//div[@id='Actions_dynamicmenu" + alterText + "Menu']/span[contains(text () , '" + option + "')]"));
	}

}
