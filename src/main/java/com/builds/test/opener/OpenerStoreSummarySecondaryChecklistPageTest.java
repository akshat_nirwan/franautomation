package com.builds.test.opener;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminConfigurationConfigureStoreTypePageTest;
import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.common.CorporateUser;
import com.builds.uimaps.opener.OpenerStoreSummarySecondaryChecklistPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class OpenerStoreSummarySecondaryChecklistPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "opener" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Delete Task :: Opener > Store Summary >  Task Checklist", testCaseId = "TC_171_Delete_Secondary_Checklist")
	public void deleteSecondaryChecklist() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			OpenerStoreSummarySecondaryChecklistPage pobj = new OpenerStoreSummarySecondaryChecklistPage(driver);
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			String secondaryChecklistName = fc.utobj().generateTestData(dataSet.get("secondaryChecklistName"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String task = fc.utobj().generateTestData(dataSet.get("task"));

			fc.utobj().printTestStep("Navigate To Admin > Opener > Responsible Department");
			fc.utobj().printTestStep("Add Responsible Department");

			AdminOpenerResponsibleDepartmentPageTest addDepartmentPage = new AdminOpenerResponsibleDepartmentPageTest();
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));
			addDepartmentPage.addDepartment(driver, responsibilityArea);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "openerautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporatePage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");

			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			fc.utobj().printTestStep("Add Secondary Checklist With Task");
			AdminOpenerSecondaryChecklistPageTest secondaryChecklistPage = new AdminOpenerSecondaryChecklistPageTest();
			String referenceDate = "";
			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			File file = new File(filePath);
			secondaryChecklistPage.addSecondaryChecklistWithTask(driver, dataSet, secondaryChecklistName, description,
					responsibilityArea, corpUser.getuserFullName(), storeType, file, referenceDate, task, config);

			fc.opener().opener_common().openerStoreSummary(driver);

			addStorePage.setDefaultFilterOpener(driver, storeNoFranchiseID);

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, storeNoFranchiseID));
			fc.utobj().clickElement(driver, pobj.secondaryChecklistLnk);

			if (fc.utobj().isSelected(driver, pobj.checkAllchckBx)) {
				fc.utobj().clickElement(driver, pobj.checkAllchckBx);
			}

			fc.utobj().printTestStep("Delete Secondary Checklist");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[.='" + secondaryChecklistName + "']/ancestor::tr/td/input"));
			fc.utobj().clickElement(driver, pobj.deleteBtn);
			fc.utobj().acceptAlertBox(driver);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			driver.switchTo().defaultContent();

			fc.utobj().printTestStep("Verify The Delete Secondary Checklist");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, secondaryChecklistName);

			if (isTextPresent == true) {
				fc.utobj().throwsException("was not able to delete secondary cheklist at Opener Page");
			}

			fc.utobj().clickElement(driver, pobj.storeInfoLnk);
			isTextPresent = fc.utobj().assertPageSource(driver, secondaryChecklistName);

			if (isTextPresent == true) {
				fc.utobj().throwsException("was not able to delete secondary cheklist at Opener > Strore Info Page");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Task Status In Progress:: Opener > Store Summary >  Secondary Checklist", testCaseId = "TC_172_Verify_Status_In_Progress")
	public void verifyTaskStatusInProgress() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			OpenerStoreSummarySecondaryChecklistPage pobj = new OpenerStoreSummarySecondaryChecklistPage(driver);

			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			String secondaryChecklistName = fc.utobj().generateTestData(dataSet.get("secondaryChecklistName"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String task = fc.utobj().generateTestData(dataSet.get("task"));

			fc.utobj().printTestStep("Navigate To Admin > Opener > Responsible Department");
			fc.utobj().printTestStep("Add Responsible Department");

			AdminOpenerResponsibleDepartmentPageTest addDepartmentPage = new AdminOpenerResponsibleDepartmentPageTest();
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));
			addDepartmentPage.addDepartment(driver, responsibilityArea);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "openerautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporatePage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");

			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			fc.utobj().printTestStep("Add Secondary Checklist With Task");
			AdminOpenerSecondaryChecklistPageTest secondaryChecklistPage = new AdminOpenerSecondaryChecklistPageTest();
			String referenceDate = "";
			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			File file = new File(filePath);
			secondaryChecklistPage.addSecondaryChecklistWithTask(driver, dataSet, secondaryChecklistName, description,
					responsibilityArea, corpUser.getuserFullName(), storeType, file, referenceDate, task, config);

			fc.opener().opener_common().openerStoreSummary(driver);
			addStorePage.setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, storeNoFranchiseID));
			fc.utobj().clickElement(driver, pobj.secondaryChecklistLnk);
			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, secondaryChecklistName));

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , 'Pending')]").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Pending status of Task");
			}

			fc.utobj().printTestStep("Make Checklist Status is In Progress");
			actionImgOption(driver, task, "In Progress");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify Checklist Status");
			fc.utobj().sendKeys(driver, pobj.secondarySearchBx, task);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , 'In Progress')]").isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to verify In Progress Status of task");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verif The Complete Task :: Opener > Store Summary >  Secondary Checklist", testCaseId = "TC_173_Complete_Task")
	public void completeTaskActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			OpenerStoreSummarySecondaryChecklistPage pobj = new OpenerStoreSummarySecondaryChecklistPage(driver);

			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			String secondaryChecklistName = fc.utobj().generateTestData(dataSet.get("secondaryChecklistName"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String task = fc.utobj().generateTestData(dataSet.get("task"));

			fc.utobj().printTestStep("Navigate To Admin > Opener > Responsible Department");
			fc.utobj().printTestStep("Add Responsible Department");

			AdminOpenerResponsibleDepartmentPageTest addDepartmentPage = new AdminOpenerResponsibleDepartmentPageTest();
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));
			addDepartmentPage.addDepartment(driver, responsibilityArea);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			/*
			 * String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String password=dataSet.get("password");
			 * corporatePage.addCorporateUser(driver, userName, password,
			 * config);
			 */

			String emailId = "openerautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporatePage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");

			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			fc.utobj().printTestStep("Add Secondary Checklist With Task");
			AdminOpenerSecondaryChecklistPageTest secondaryChecklistPage = new AdminOpenerSecondaryChecklistPageTest();
			String referenceDate = "";
			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			File file = new File(filePath);
			secondaryChecklistPage.addSecondaryChecklistWithTask(driver, dataSet, secondaryChecklistName, description,
					responsibilityArea, corpUser.getuserFullName(), storeType, file, referenceDate, task, config);

			fc.opener().opener_common().openerStoreSummary(driver);
			addStorePage.setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, storeNoFranchiseID));
			fc.utobj().clickElement(driver, pobj.secondaryChecklistLnk);
			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, secondaryChecklistName));

			String parentWindow = driver.getWindowHandle();

			fc.utobj().printTestStep("Complet The Checklist");
			actionImgOption(driver, task, "Complete");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String curDate = fc.utobj().currentDate();
			fc.utobj().sendKeys(driver, pobj.completionDateTxBx, curDate);
			// fc.utobj().clickElement(driver, pobj.calanderClose);
			String comments = fc.utobj().generateTestData(dataSet.get("comments"));
			fc.utobj().sendKeys(driver, pobj.commentsTxBx, comments);
			fc.utobj().clickElement(driver, pobj.completeBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().printTestStep("Verify The Complete Checklist");
			fc.utobj().sendKeys(driver, pobj.secondarySearchBx, task);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , 'Complete')]").isDisplayed() && !fc
					.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + curDate + "')]").isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to complete task");
			}

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, task));
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + curDate + "')]").isDisplayed()
					&& !fc.utobj().getElementByXpath(driver, ".//td[contains(text () ,'" + comments + "')]")
							.isDisplayed()
					&& !fc.utobj().getElementByXpath(driver, ".//td[contains(text () , 'Complete')]").isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to verify completion date, comments and status");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "opener" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add Comment :: Opener > Store Summary >  Secondary Checklist", testCaseId = "TC_174_Add_Comment")
	public void addCommentActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			OpenerStoreSummarySecondaryChecklistPage pobj = new OpenerStoreSummarySecondaryChecklistPage(driver);

			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			String secondaryChecklistName = fc.utobj().generateTestData(dataSet.get("secondaryChecklistName"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String task = fc.utobj().generateTestData(dataSet.get("task"));

			fc.utobj().printTestStep("Navigate To Admin > Opener > Responsible Department");
			fc.utobj().printTestStep("Add Responsible Department");

			AdminOpenerResponsibleDepartmentPageTest addDepartmentPage = new AdminOpenerResponsibleDepartmentPageTest();
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));
			addDepartmentPage.addDepartment(driver, responsibilityArea);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			/*
			 * String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String password=dataSet.get("password");
			 * corporatePage.addCorporateUser(driver, userName, password,
			 * config);
			 */

			String emailId = "openerautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporatePage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");

			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			fc.utobj().printTestStep("Add Secondary Checklist With Task");
			AdminOpenerSecondaryChecklistPageTest secondaryChecklistPage = new AdminOpenerSecondaryChecklistPageTest();
			String referenceDate = "";
			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			File file = new File(filePath);
			secondaryChecklistPage.addSecondaryChecklistWithTask(driver, dataSet, secondaryChecklistName, description,
					responsibilityArea, corpUser.getuserFullName(), storeType, file, referenceDate, task, config);

			fc.opener().opener_common().openerStoreSummary(driver);
			addStorePage.setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, storeNoFranchiseID));
			fc.utobj().clickElement(driver, pobj.secondaryChecklistLnk);
			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, secondaryChecklistName));

			String parentWindow = driver.getWindowHandle();

			fc.utobj().printTestStep("Add Commnets By Action Image Option");
			actionImgOption(driver, task, "Add Comments");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String comments = fc.utobj().generateTestData(dataSet.get("comments"));
			fc.utobj().sendKeys(driver, pobj.commentsTxBx, comments);
			fc.utobj().clickElement(driver, pobj.addBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().printTestStep("Verify The Comments");
			fc.utobj().sendKeys(driver, pobj.secondarySearchBx, task);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, task));
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + fc.utobj().currentDate() + "')]")
					.isDisplayed()
					&& !fc.utobj().getElementByXpath(driver, ".//td[contains(text () ,'" + comments + "')]")
							.isDisplayed()
					&& !fc.utobj().getElementByXpath(driver, ".//td[contains(text () , 'Pending')]").isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to verify comments date, comments and task status");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Modification of Task  At Opener > Store Summary >  Secondary Checklist", testCaseId = "TC_175_Modify_Task")
	public void modifyTaskActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			OpenerStoreSummarySecondaryChecklistPage pobj = new OpenerStoreSummarySecondaryChecklistPage(driver);

			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			String secondaryChecklistName = fc.utobj().generateTestData(dataSet.get("secondaryChecklistName"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String task = fc.utobj().generateTestData(dataSet.get("task"));

			fc.utobj().printTestStep("Navigate To Admin > Opener > Responsible Department");
			fc.utobj().printTestStep("Add Responsible Department");

			fc.utobj().printTestStep("Navigate To Admin > Opener > Responsible Department");
			fc.utobj().printTestStep("Add Responsible Department");

			AdminOpenerResponsibleDepartmentPageTest addDepartmentPage = new AdminOpenerResponsibleDepartmentPageTest();
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));
			addDepartmentPage.addDepartment(driver, responsibilityArea);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			/*
			 * String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String password=dataSet.get("password");
			 * corporatePage.addCorporateUser(driver, userName, password,
			 * config);
			 */

			String emailId = "openerautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporatePage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");

			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			fc.utobj().printTestStep("Add Secondary Checklist With Task");
			AdminOpenerSecondaryChecklistPageTest secondaryChecklistPage = new AdminOpenerSecondaryChecklistPageTest();
			String referenceDate = "";
			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			File file = new File(filePath);
			secondaryChecklistPage.addSecondaryChecklistWithTask(driver, dataSet, secondaryChecklistName, description,
					responsibilityArea, corpUser.getuserFullName(), storeType, file, referenceDate, task, config);

			responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));
			addDepartmentPage.addDepartment(driver, responsibilityArea);

			fc.opener().opener_common().openerStoreSummary(driver);

			addStorePage.setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, storeNoFranchiseID));
			fc.utobj().clickElement(driver, pobj.secondaryChecklistLnk);

			// configure Default
			configureDefaultValue(driver);
			fc.utobj().clickElement(driver, pobj.secondaryChecklistLnk);
			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, secondaryChecklistName));

			fc.utobj().sendKeys(driver, pobj.secondarySearchBx, task);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			fc.utobj().printTestStep("Modify The Checklist");
			actionImgOption(driver, task, "Modify");

			task = fc.utobj().generateTestData(dataSet.get("task"));
			fc.utobj().sendKeys(driver, pobj.secondaryNameTxtBox, task);

			fc.utobj().selectValFromMultiSelect(driver, pobj.selectResponsibilityArea, responsibilityArea);

			fc.utobj().selectValFromMultiSelect(driver, pobj.selectContact, "Regional User");

			// 74419

			String dependentOn = dataSet.get("dependentOnModify");
			String valueText = fc.utobj()
					.getElementByXpath(driver,
							".//select[@name='smUserSecondryChecklist_0referenceParent']/option[contains(text () , '"
									+ dependentOn + "')]")
					.getAttribute("value");
			fc.utobj().moveToElement(driver, pobj.dependentOnDropDown);
			Select sl = new Select(pobj.dependentOnDropDown);
			sl.selectByValue(valueText);

			String currentDate = fc.utobj().currentDate();

			fc.utobj().sendKeys(driver, pobj.scheduledStartDate, currentDate);
			// fc.utobj().clickElement(driver, pobj.calanderClose);

			String futureDate = fc.utobj().getFutureDateUSFormat(5);

			fc.utobj().sendKeys(driver, pobj.scheduledCompletionDate, futureDate);
			// fc.utobj().clickElement(driver, pobj.calanderClose);

			fc.utobj().sendKeys(driver, pobj.reminderStart, "1");
			fc.utobj().sendKeys(driver, pobj.reminderCompletion, "1");

			if (fc.utobj().isSelected(driver, pobj.webLinkRadioBtn)) {
				// do nothing
			} else {
				fc.utobj().clickElement(driver, pobj.webLinkRadioBtn);
			}

			fc.utobj().sendKeys(driver, pobj.webLinkAddressBx, dataSet.get("webLinkUrl"));

			try {
				List<WebElement> listRadioButton = pobj.applyToAll;

				if (fc.utobj().isSelected(driver,listRadioButton.get(2))) {
				} else {
					fc.utobj().clickElement(driver, listRadioButton.get(2));
				}
				fc.utobj().clickElement(driver, pobj.currentOpnerSelectBtn);
				fc.utobj().clickElement(driver, pobj.selectAllOpnerCheck);
			} catch (Exception e) {

			}
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("Verify The Modify Checklist");
			fc.utobj().sendKeys(driver, pobj.secondarySearchBx, task);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , 'Pending')]").isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to modify Task Status");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () ,'" + responsibilityArea + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to modify responsibility area");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () ,'Regional User')]").isDisplayed()) {
				fc.utobj().throwsException("was not able to modify Contact");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//a[.='" + task + "']").isDisplayed()) {
				fc.utobj().throwsException("was not able to modify task");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Deletion of Task :: Opener > Store Summary >  Secondary Checklist", testCaseId = "TC_178_Delete_Task")
	public void deleteTaskActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			OpenerStoreSummarySecondaryChecklistPage pobj = new OpenerStoreSummarySecondaryChecklistPage(driver);

			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			String secondaryChecklistName = fc.utobj().generateTestData(dataSet.get("secondaryChecklistName"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String task = fc.utobj().generateTestData(dataSet.get("task"));

			fc.utobj().printTestStep("Navigate To Admin > Opener > Responsible Department");
			fc.utobj().printTestStep("Add Responsible Department");

			AdminOpenerResponsibleDepartmentPageTest addDepartmentPage = new AdminOpenerResponsibleDepartmentPageTest();
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));
			addDepartmentPage.addDepartment(driver, responsibilityArea);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			/*
			 * String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String password=dataSet.get("password");
			 * corporatePage.addCorporateUser(driver, userName, password,
			 * config);
			 */

			String emailId = "openerautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporatePage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");

			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			fc.utobj().printTestStep("Add Secondary Checklist With Task");
			AdminOpenerSecondaryChecklistPageTest secondaryChecklistPage = new AdminOpenerSecondaryChecklistPageTest();
			String referenceDate = "";
			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			File file = new File(filePath);
			secondaryChecklistPage.addSecondaryChecklistWithTask(driver, dataSet, secondaryChecklistName, description,
					responsibilityArea, corpUser.getuserFullName(), storeType, file, referenceDate, task, config);

			fc.opener().opener_common().openerStoreSummary(driver);
			addStorePage.setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, storeNoFranchiseID));
			fc.utobj().clickElement(driver, pobj.secondaryChecklistLnk);
			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, secondaryChecklistName));

			String parentWindow = driver.getWindowHandle();

			fc.utobj().printTestStep("Delete The Checklist By Action Image Option");
			actionImgOption(driver, task, "Delete");
			fc.utobj().acceptAlertBox(driver);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().sendKeys(driver, pobj.secondarySearchBx, task);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			if (!fc.utobj().getElementByXpath(driver, ".//td[.='No records found.']").isDisplayed()) {
				fc.utobj().throwsException("was not able to delete task at Task Checklist page");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add Description :: Opener > Store Summary >  Secondary Checklist", testCaseId = "TC_179_Add_Description")
	public void addDescriptionActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			OpenerStoreSummarySecondaryChecklistPage pobj = new OpenerStoreSummarySecondaryChecklistPage(driver);

			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			String secondaryChecklistName = fc.utobj().generateTestData(dataSet.get("secondaryChecklistName"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String task = fc.utobj().generateTestData(dataSet.get("task"));

			fc.utobj().printTestStep("Navigate To Admin > Opener > Responsible Department");
			fc.utobj().printTestStep("Add Responsible Department");

			AdminOpenerResponsibleDepartmentPageTest addDepartmentPage = new AdminOpenerResponsibleDepartmentPageTest();
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));
			addDepartmentPage.addDepartment(driver, responsibilityArea);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			/*
			 * String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String password=dataSet.get("password");
			 * corporatePage.addCorporateUser(driver, userName, password,
			 * config);
			 */

			String emailId = "openerautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporatePage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");

			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			fc.utobj().printTestStep("Add Secondary Checklist With Task");
			AdminOpenerSecondaryChecklistPageTest secondaryChecklistPage = new AdminOpenerSecondaryChecklistPageTest();
			String referenceDate = "";
			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			File file = new File(filePath);
			secondaryChecklistPage.addSecondaryChecklistWithTask(driver, dataSet, secondaryChecklistName, description,
					responsibilityArea, corpUser.getuserFullName(), storeType, file, referenceDate, task, config);

			fc.opener().opener_common().openerStoreSummary(driver);
			addStorePage.setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, storeNoFranchiseID));
			fc.utobj().clickElement(driver, pobj.secondaryChecklistLnk);
			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, secondaryChecklistName));

			String parentWindow = driver.getWindowHandle();

			fc.utobj().printTestStep("Add Description");
			actionImgOption(driver, task, "Add Description");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().sendKeys(driver, pobj.descriptionTxBx, description);
			fc.utobj().clickElement(driver, pobj.addBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().sendKeys(driver, pobj.secondarySearchBx, task);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			fc.utobj().printTestStep("Verify The Add Description");
			actionImgOption(driver, task, "Modify Description");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			String isTextPresent = pobj.descriptionTxBx.getText().trim();
			if (!isTextPresent.equalsIgnoreCase(description)) {
				fc.utobj().throwsException("was not able to add description");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Modification of Description :: Opener > Store Summary >  Secondary Checklist", testCaseId = "TC_180_Modify_Description")
	public void modifyDescriptionActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			OpenerStoreSummarySecondaryChecklistPage pobj = new OpenerStoreSummarySecondaryChecklistPage(driver);

			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			String secondaryChecklistName = fc.utobj().generateTestData(dataSet.get("secondaryChecklistName"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String task = fc.utobj().generateTestData(dataSet.get("task"));

			fc.utobj().printTestStep("Navigate To Admin > Opener > Responsible Department");
			fc.utobj().printTestStep("Add Responsible Department");

			AdminOpenerResponsibleDepartmentPageTest addDepartmentPage = new AdminOpenerResponsibleDepartmentPageTest();
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));
			addDepartmentPage.addDepartment(driver, responsibilityArea);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			/*
			 * String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String password=dataSet.get("password");
			 * corporatePage.addCorporateUser(driver, userName, password,
			 * config);
			 */

			String emailId = "openerautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporatePage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");

			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			fc.utobj().printTestStep("Add Secondary Checklist With Task");
			AdminOpenerSecondaryChecklistPageTest secondaryChecklistPage = new AdminOpenerSecondaryChecklistPageTest();
			String referenceDate = "";
			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			File file = new File(filePath);
			secondaryChecklistPage.addSecondaryChecklistWithTask(driver, dataSet, secondaryChecklistName, description,
					responsibilityArea, corpUser.getuserFullName(), storeType, file, referenceDate, task, config);

			String parentWindow = driver.getWindowHandle();

			fc.opener().opener_common().openerStoreSummary(driver);
			addStorePage.setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, storeNoFranchiseID));
			fc.utobj().clickElement(driver, pobj.secondaryChecklistLnk);
			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, secondaryChecklistName));

			fc.utobj().printTestStep("Add Description");
			actionImgOption(driver, task, "Add Description");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().sendKeys(driver, pobj.descriptionTxBx, description);
			fc.utobj().clickElement(driver, pobj.addBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().sendKeys(driver, pobj.secondarySearchBx, task);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			fc.utobj().printTestStep("Modify The Description");
			actionImgOption(driver, task, "Modify Description");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().sendKeys(driver, pobj.descriptionTxBx, description);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().printTestStep("Verify The Description");

			actionImgOption(driver, task, "Modify Description");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			String isTextPresent = pobj.descriptionTxBx.getText().trim();
			if (!isTextPresent.equalsIgnoreCase(description)) {
				fc.utobj().throwsException("was not able to modify description");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "opener", "openerfailed" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Not Applicable :: Opener > Store Summary >  Secondary Checklist", testCaseId = "TC_181_Verify_Not_Applicable")
	public void verifyNotApplicable() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			OpenerStoreSummarySecondaryChecklistPage pobj = new OpenerStoreSummarySecondaryChecklistPage(driver);

			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			String secondaryChecklistName = fc.utobj().generateTestData(dataSet.get("secondaryChecklistName"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String task = fc.utobj().generateTestData(dataSet.get("task"));

			fc.utobj().printTestStep("Navigate To Admin > Opener > Responsible Department");
			fc.utobj().printTestStep("Add Responsible Department");

			AdminOpenerResponsibleDepartmentPageTest addDepartmentPage = new AdminOpenerResponsibleDepartmentPageTest();
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));
			addDepartmentPage.addDepartment(driver, responsibilityArea);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			/*
			 * String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String password=dataSet.get("password");
			 * corporatePage.addCorporateUser(driver, userName, password,
			 * config);
			 */
			String emailId = "openerautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporatePage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");

			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			fc.utobj().printTestStep("Add Secondary Checklist With Task");
			AdminOpenerSecondaryChecklistPageTest secondaryChecklistPage = new AdminOpenerSecondaryChecklistPageTest();
			String referenceDate = "";
			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			File file = new File(filePath);
			secondaryChecklistPage.addSecondaryChecklistWithTask(driver, dataSet, secondaryChecklistName, description,
					responsibilityArea, corpUser.getuserFullName(), storeType, file, referenceDate, task, config);

			fc.opener().opener_common().openerStoreSummary(driver);
			addStorePage.setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, storeNoFranchiseID));
			fc.utobj().clickElement(driver, pobj.secondaryChecklistLnk);
			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, secondaryChecklistName));

			fc.utobj().printTestStep("Mark As A Not Applicable By Action Image Opttion");
			actionImgOption(driver, task, "Not Applicable");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Not Applicable");
			fc.utobj().sendKeys(driver, pobj.secondarySearchBx, task);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , 'Not Applicable')]").isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to verify Not Applicable Status");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Mark As A In Complete Task :: Opener > Store Summary >  Secondary Checklist", testCaseId = "TC_182_Verify_Mark_As_InComplete")
	public void verifyMarkAsInCompleteActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			OpenerStoreSummarySecondaryChecklistPage pobj = new OpenerStoreSummarySecondaryChecklistPage(driver);

			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			String secondaryChecklistName = fc.utobj().generateTestData(dataSet.get("secondaryChecklistName"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String task = fc.utobj().generateTestData(dataSet.get("task"));

			fc.utobj().printTestStep("Navigate To Admin > Opener > Responsible Department");
			fc.utobj().printTestStep("Add Responsible Department");

			AdminOpenerResponsibleDepartmentPageTest addDepartmentPage = new AdminOpenerResponsibleDepartmentPageTest();
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));
			addDepartmentPage.addDepartment(driver, responsibilityArea);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			/*
			 * String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String password=dataSet.get("password");
			 * corporatePage.addCorporateUser(driver, userName, password,
			 * config);
			 */

			String emailId = "openerautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporatePage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");

			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			fc.utobj().printTestStep("Add Secondary Checklist With Task");
			AdminOpenerSecondaryChecklistPageTest secondaryChecklistPage = new AdminOpenerSecondaryChecklistPageTest();
			String referenceDate = "";
			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			File file = new File(filePath);
			secondaryChecklistPage.addSecondaryChecklistWithTask(driver, dataSet, secondaryChecklistName, description,
					responsibilityArea, corpUser.getuserFullName(), storeType, file, referenceDate, task, config);

			String parentWindow = driver.getWindowHandle();

			fc.opener().opener_common().openerStoreSummary(driver);
			addStorePage.setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, storeNoFranchiseID));
			fc.utobj().clickElement(driver, pobj.secondaryChecklistLnk);
			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, secondaryChecklistName));

			fc.utobj().printTestStep("Complte The Checklist");
			actionImgOption(driver, task, "Complete");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String curDate = fc.utobj().currentDate();
			fc.utobj().sendKeys(driver, pobj.completionDateTxBx, curDate);
			// fc.utobj().clickElement(driver, pobj.calanderClose);
			String comments = fc.utobj().generateTestData(dataSet.get("comments"));
			fc.utobj().sendKeys(driver, pobj.commentsTxBx, comments);
			fc.utobj().clickElement(driver, pobj.completeBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().printTestStep("Mark As A Incomplete");
			fc.utobj().sendKeys(driver, pobj.secondarySearchBx, task);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			actionImgOption(driver, task, "Mark as Incomplete");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Mark As A Incomplete");
			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () ,'Pending')]").isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to verify Mark As InComplete status");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Complete Task At ActionBtn :: Opener > Store Summary >  Secondary Checklist", testCaseId = "TC_183_Complete_Task")
	public void completeTaskActionBtn() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			OpenerStoreSummarySecondaryChecklistPage pobj = new OpenerStoreSummarySecondaryChecklistPage(driver);

			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			String secondaryChecklistName = fc.utobj().generateTestData(dataSet.get("secondaryChecklistName"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String task = fc.utobj().generateTestData(dataSet.get("task"));

			fc.utobj().printTestStep("Navigate To Admin > Opener > Responsible Department");
			fc.utobj().printTestStep("Add Responsible Department");

			AdminOpenerResponsibleDepartmentPageTest addDepartmentPage = new AdminOpenerResponsibleDepartmentPageTest();
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));
			addDepartmentPage.addDepartment(driver, responsibilityArea);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			/*
			 * String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String password=dataSet.get("password");
			 * corporatePage.addCorporateUser(driver, userName, password,
			 * config);
			 */

			String emailId = "openerautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporatePage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");

			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			fc.utobj().printTestStep("Add Secondary Checklist With Task");
			AdminOpenerSecondaryChecklistPageTest secondaryChecklistPage = new AdminOpenerSecondaryChecklistPageTest();
			String referenceDate = "";
			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			File file = new File(filePath);
			secondaryChecklistPage.addSecondaryChecklistWithTask(driver, dataSet, secondaryChecklistName, description,
					responsibilityArea, corpUser.getuserFullName(), storeType, file, referenceDate, task, config);

			String parentWindow = driver.getWindowHandle();

			fc.opener().opener_common().openerStoreSummary(driver);
			addStorePage.setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, storeNoFranchiseID));
			fc.utobj().clickElement(driver, pobj.secondaryChecklistLnk);
			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, secondaryChecklistName));

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[.='" + task + "']/ancestor::tr/td/input"));

			fc.utobj().printTestStep("Complete The Checklist");
			fc.utobj().selectActionMenuItems(driver, "Complete");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String curDate = fc.utobj().currentDate();
			fc.utobj().sendKeys(driver, pobj.completionDateTxBx, curDate);
			// fc.utobj().clickElement(driver, pobj.calanderClose);
			String comments = fc.utobj().generateTestData(dataSet.get("comments"));
			fc.utobj().sendKeys(driver, pobj.commentsTxBx, comments);
			fc.utobj().clickElement(driver, pobj.completeBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().printTestStep("Verify The Complete Checklist");
			fc.utobj().sendKeys(driver, pobj.secondarySearchBx, task);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , 'Complete')]").isDisplayed() && !fc
					.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + curDate + "')]").isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to complete task");
			}

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, task));
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + curDate + "')]").isDisplayed()
					&& !fc.utobj().getElementByXpath(driver, ".//td[contains(text () ,'" + comments + "')]")
							.isDisplayed()
					&& !fc.utobj().getElementByXpath(driver, ".//td[contains(text () , 'Complete')]").isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to verify completion date, comments and status");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Deletion of Task :: Opener > Store Summary >  Secondary Checklist", testCaseId = "TC_184_Delete_Task")
	public void deleteTaskActionBtn() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			OpenerStoreSummarySecondaryChecklistPage pobj = new OpenerStoreSummarySecondaryChecklistPage(driver);

			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			String secondaryChecklistName = fc.utobj().generateTestData(dataSet.get("secondaryChecklistName"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String task = fc.utobj().generateTestData(dataSet.get("task"));

			fc.utobj().printTestStep("Navigate To Admin > Opener > Responsible Department");
			fc.utobj().printTestStep("Add Responsible Department");

			AdminOpenerResponsibleDepartmentPageTest addDepartmentPage = new AdminOpenerResponsibleDepartmentPageTest();
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));
			addDepartmentPage.addDepartment(driver, responsibilityArea);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			/*
			 * String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String password=dataSet.get("password");
			 * corporatePage.addCorporateUser(driver, userName, password,
			 * config);
			 */

			String emailId = "openerautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporatePage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");

			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			fc.utobj().printTestStep("Add Secondary Checklist With Task");
			AdminOpenerSecondaryChecklistPageTest secondaryChecklistPage = new AdminOpenerSecondaryChecklistPageTest();
			String referenceDate = "";
			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			File file = new File(filePath);
			secondaryChecklistPage.addSecondaryChecklistWithTask(driver, dataSet, secondaryChecklistName, description,
					responsibilityArea, corpUser.getuserFullName(), storeType, file, referenceDate, task, config);

			String parentWindow = driver.getWindowHandle();

			fc.opener().opener_common().openerStoreSummary(driver);

			addStorePage.setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, storeNoFranchiseID));
			fc.utobj().clickElement(driver, pobj.secondaryChecklistLnk);
			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, secondaryChecklistName));

			fc.utobj().printTestStep("Delete The Checklist");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[.='" + task + "']/ancestor::tr/td/input"));

			fc.utobj().selectActionMenuItems(driver, "Delete");

			fc.utobj().acceptAlertBox(driver);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().printTestStep("Verify The Delete Checklist");
			fc.utobj().sendKeys(driver, pobj.secondarySearchBx, task);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			if (!fc.utobj().getElementByXpath(driver, ".//td[.='No records found.']").isDisplayed()) {
				fc.utobj().throwsException("was not able to delete task at Task Checklist page");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Modification of Contact of Task :: Opener > Store Summary >  Secondary Checklist", testCaseId = "TC_185_Modify_Contact")
	public void modifyContactActionBtn() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			OpenerStoreSummarySecondaryChecklistPage pobj = new OpenerStoreSummarySecondaryChecklistPage(driver);

			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			String secondaryChecklistName = fc.utobj().generateTestData(dataSet.get("secondaryChecklistName"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String task = fc.utobj().generateTestData(dataSet.get("task"));

			fc.utobj().printTestStep("Navigate To Admin > Opener > Responsible Department");
			fc.utobj().printTestStep("Add Responsible Department");

			AdminOpenerResponsibleDepartmentPageTest addDepartmentPage = new AdminOpenerResponsibleDepartmentPageTest();
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));
			addDepartmentPage.addDepartment(driver, responsibilityArea);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			/*
			 * String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String password=dataSet.get("password");
			 * corporatePage.addCorporateUser(driver, userName, password,
			 * config);
			 */

			String emailId = "openerautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporatePage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");

			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			fc.utobj().printTestStep("Add Secondary Checklist With Task");
			AdminOpenerSecondaryChecklistPageTest secondaryChecklistPage = new AdminOpenerSecondaryChecklistPageTest();
			String referenceDate = "";
			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			File file = new File(filePath);
			secondaryChecklistPage.addSecondaryChecklistWithTask(driver, dataSet, secondaryChecklistName, description,
					responsibilityArea, corpUser.getuserFullName(), storeType, file, referenceDate, task, config);

			String parentWindow = driver.getWindowHandle();

			fc.opener().opener_common().openerStoreSummary(driver);
			addStorePage.setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, storeNoFranchiseID));
			fc.utobj().clickElement(driver, pobj.secondaryChecklistLnk);
			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, secondaryChecklistName));

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[.='" + task + "']/ancestor::tr/td/input"));

			fc.utobj().printTestStep("Modify The Contact Of Checklist");
			fc.utobj().selectActionMenuItems(driver, "Modify Contact(s)");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.modifyContactSelectBtn);
			fc.utobj().clickElement(driver, pobj.modifyContactSelectAll);
			fc.utobj().clickElement(driver, pobj.modifyContactSelectAll);
			fc.utobj().sendKeys(driver, pobj.modifyContactSearchBx, "Regional User");
			fc.utobj().clickElement(driver, pobj.modifyContactSelectAll);
			fc.utobj().clickElement(driver, pobj.modifyContactSelectBtn);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().printTestStep("Verify The Modify Contact Of Checklist");
			fc.utobj().sendKeys(driver, pobj.secondarySearchBx, task);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			if (!driver
					.findElement(
							By.xpath(".//a[.='" + task + "']/ancestor::tr/td[contains(text () , 'Regional User')]"))
					.isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to modify Contact(s)");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Group View of Task :: Opener > Store Summary >  Secondary Checklist", testCaseId = "TC_186_Group_View")
	public void groupViewActionBtn() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			OpenerStoreSummarySecondaryChecklistPage pobj = new OpenerStoreSummarySecondaryChecklistPage(driver);

			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			String secondaryChecklistName = fc.utobj().generateTestData(dataSet.get("secondaryChecklistName"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String task = fc.utobj().generateTestData(dataSet.get("task"));

			fc.utobj().printTestStep("Navigate To Admin > Opener > Responsible Department");
			fc.utobj().printTestStep("Add Responsible Department");

			AdminOpenerResponsibleDepartmentPageTest addDepartmentPage = new AdminOpenerResponsibleDepartmentPageTest();
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));
			addDepartmentPage.addDepartment(driver, responsibilityArea);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			/*
			 * String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String password=dataSet.get("password");
			 * corporatePage.addCorporateUser(driver, userName, password,
			 * config);
			 */

			String emailId = "openerautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporatePage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Groups");
			fc.utobj().printTestStep("Add Group");

			AdminOpenerManageGroupsPageTest addGroupsPage = new AdminOpenerManageGroupsPageTest();
			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			addGroupsPage.addGroups(driver, groupName);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");

			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			fc.utobj().printTestStep("Add Secondary Checklist With Task");
			AdminOpenerSecondaryChecklistPageTest secondaryChecklistPage = new AdminOpenerSecondaryChecklistPageTest();
			String referenceDate = "";
			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			File file = new File(filePath);
			secondaryChecklistPage.addSecondaryChecklistWithTaskWithGroup(driver, dataSet, secondaryChecklistName,
					description, responsibilityArea, corpUser.getuserFullName(), storeType, groupName, file,
					referenceDate, task, config);

			fc.opener().opener_common().openerStoreSummary(driver);

			addStorePage.setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, storeNoFranchiseID));
			fc.utobj().clickElement(driver, pobj.secondaryChecklistLnk);
			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, secondaryChecklistName));

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[.='" + task + "']/ancestor::tr/td/input"));

			fc.utobj().printTestStep("Make Group View Of checklist By Action Button Option");
			// need to be changed
			fc.utobj().selectActionMenuItems(driver, "Group view");

			fc.utobj().printTestStep("Verify The Group view");
			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + groupName + "')]").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify group view");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Mark As A In Complete Task :: Opener > Store Summary >  Secondary Checklist", testCaseId = "TC_187_Complete_Task")
	public void completeTaskBottomBtn() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			OpenerStoreSummarySecondaryChecklistPage pobj = new OpenerStoreSummarySecondaryChecklistPage(driver);

			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			String secondaryChecklistName = fc.utobj().generateTestData(dataSet.get("secondaryChecklistName"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String task = fc.utobj().generateTestData(dataSet.get("task"));

			fc.utobj().printTestStep("Navigate To Admin > Opener > Responsible Department");
			fc.utobj().printTestStep("Add Responsible Department");

			AdminOpenerResponsibleDepartmentPageTest addDepartmentPage = new AdminOpenerResponsibleDepartmentPageTest();
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));
			addDepartmentPage.addDepartment(driver, responsibilityArea);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			/*
			 * String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String password=dataSet.get("password");
			 * corporatePage.addCorporateUser(driver, userName, password,
			 * config);
			 */

			String emailId = "openerautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporatePage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");

			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			fc.utobj().printTestStep("Add Secondary Checklist With Task");
			AdminOpenerSecondaryChecklistPageTest secondaryChecklistPage = new AdminOpenerSecondaryChecklistPageTest();
			String referenceDate = "";
			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			File file = new File(filePath);
			secondaryChecklistPage.addSecondaryChecklistWithTask(driver, dataSet, secondaryChecklistName, description,
					responsibilityArea, corpUser.getuserFullName(), storeType, file, referenceDate, task, config);

			String parentWindow = driver.getWindowHandle();

			fc.opener().opener_common().openerStoreSummary(driver);

			addStorePage.setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, storeNoFranchiseID));
			fc.utobj().clickElement(driver, pobj.secondaryChecklistLnk);
			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, secondaryChecklistName));

			fc.utobj().printTestStep("Complete The Checklist By Bottom Option Option");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[.='" + task + "']/ancestor::tr/td/input"));
			fc.utobj().clickElement(driver, pobj.completeBtn);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String curDate = fc.utobj().currentDate();
			fc.utobj().sendKeys(driver, pobj.completionDateTxBx, curDate);
			// fc.utobj().clickElement(driver, pobj.calanderClose);
			String comments = fc.utobj().generateTestData(dataSet.get("comments"));
			fc.utobj().sendKeys(driver, pobj.commentsTxBx, comments);
			fc.utobj().clickElement(driver, pobj.completeBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().printTestStep("Verify The Complete Status Of Checklist");
			fc.utobj().sendKeys(driver, pobj.secondarySearchBx, task);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , 'Complete')]").isDisplayed() && !fc
					.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + curDate + "')]").isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to complete task");
			}

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, task));
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + curDate + "')]").isDisplayed()
					&& !fc.utobj().getElementByXpath(driver, ".//td[contains(text () ,'" + comments + "')]")
							.isDisplayed()
					&& !fc.utobj().getElementByXpath(driver, ".//td[contains(text () , 'Complete')]").isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to verify completion date, comments and status");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Deletion of Task at Bottom Btn:: Opener > Store Summary >  Secondary Checklist", testCaseId = "TC_188_Delete_Task")
	public void deleteTaskBottomBtn() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			OpenerStoreSummarySecondaryChecklistPage pobj = new OpenerStoreSummarySecondaryChecklistPage(driver);

			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			String secondaryChecklistName = fc.utobj().generateTestData(dataSet.get("secondaryChecklistName"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String task = fc.utobj().generateTestData(dataSet.get("task"));

			fc.utobj().printTestStep("Navigate To Admin > Opener > Responsible Department");
			fc.utobj().printTestStep("Add Responsible Department");

			AdminOpenerResponsibleDepartmentPageTest addDepartmentPage = new AdminOpenerResponsibleDepartmentPageTest();
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));
			addDepartmentPage.addDepartment(driver, responsibilityArea);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			/*
			 * String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String password=dataSet.get("password");
			 * corporatePage.addCorporateUser(driver, userName, password,
			 * config);
			 */

			String emailId = "openerautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporatePage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");

			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			fc.utobj().printTestStep("Add Secondary Checklist With Task");
			AdminOpenerSecondaryChecklistPageTest secondaryChecklistPage = new AdminOpenerSecondaryChecklistPageTest();
			String referenceDate = "";
			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			File file = new File(filePath);
			secondaryChecklistPage.addSecondaryChecklistWithTask(driver, dataSet, secondaryChecklistName, description,
					responsibilityArea, corpUser.getuserFullName(), storeType, file, referenceDate, task, config);

			String parentWindow = driver.getWindowHandle();

			fc.opener().opener_common().openerStoreSummary(driver);

			addStorePage.setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, storeNoFranchiseID));
			fc.utobj().clickElement(driver, pobj.secondaryChecklistLnk);
			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, secondaryChecklistName));

			fc.utobj().printTestStep("Delete The Checklist By Bottom Button");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[.='" + task + "']/ancestor::tr/td/input"));
			fc.utobj().clickElement(driver, pobj.bottomDeleteBtn);

			fc.utobj().acceptAlertBox(driver);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().printTestStep("Verify The Delete Checklist");
			fc.utobj().sendKeys(driver, pobj.secondarySearchBx, task);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			if (!fc.utobj().getElementByXpath(driver, ".//td[.='No records found.']").isDisplayed()) {
				fc.utobj().throwsException("was not able to delete task at Task Checklist page");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Modification Contact of Task At bottom Btn:: Opener > Store Summary >  Secondary Checklist", testCaseId = "TC_189_Modify_Contact")
	public void modifyContactBottomBtn() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			OpenerStoreSummarySecondaryChecklistPage pobj = new OpenerStoreSummarySecondaryChecklistPage(driver);

			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			String secondaryChecklistName = fc.utobj().generateTestData(dataSet.get("secondaryChecklistName"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String task = fc.utobj().generateTestData(dataSet.get("task"));

			fc.utobj().printTestStep("Navigate To Admin > Opener > Responsible Department");
			fc.utobj().printTestStep("Add Responsible Department");

			AdminOpenerResponsibleDepartmentPageTest addDepartmentPage = new AdminOpenerResponsibleDepartmentPageTest();
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));
			addDepartmentPage.addDepartment(driver, responsibilityArea);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			/*
			 * String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String password=dataSet.get("password");
			 * corporatePage.addCorporateUser(driver, userName, password,
			 * config);
			 */

			String emailId = "openerautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporatePage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");

			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			fc.utobj().printTestStep("Add Secondary Checklist With Task");
			AdminOpenerSecondaryChecklistPageTest secondaryChecklistPage = new AdminOpenerSecondaryChecklistPageTest();
			String referenceDate = "";
			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			File file = new File(filePath);
			secondaryChecklistPage.addSecondaryChecklistWithTask(driver, dataSet, secondaryChecklistName, description,
					responsibilityArea, corpUser.getuserFullName(), storeType, file, referenceDate, task, config);

			String parentWindow = driver.getWindowHandle();

			fc.utobj().printTestStep("Modify The Contact By Bottom Button");
			fc.opener().opener_common().openerStoreSummary(driver);

			addStorePage.setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, storeNoFranchiseID));
			fc.utobj().clickElement(driver, pobj.secondaryChecklistLnk);
			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, secondaryChecklistName));

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[.='" + task + "']/ancestor::tr/td/input"));
			fc.utobj().clickElement(driver, pobj.modifyContactBoottomBtn);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.modifyContactSelectBtn);
			fc.utobj().clickElement(driver, pobj.modifyContactSelectAll);
			fc.utobj().clickElement(driver, pobj.modifyContactSelectAll);
			fc.utobj().sendKeys(driver, pobj.modifyContactSearchBx, "Division");
			fc.utobj().clickElement(driver, pobj.modifyContactSelectAll);
			fc.utobj().clickElement(driver, pobj.modifyContactSelectBtn);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().printTestStep("Verify The Modify Contact");
			fc.utobj().sendKeys(driver, pobj.secondarySearchBx, task);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			if (!driver
					.findElement(
							By.xpath(".//a[.='" + task + "']/ancestor::tr/td[contains(text () , 'Divisional User')]"))
					.isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to modify Contact(s)");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Add More Link:: Opener > Store Summary >  Secondary Checklist", testCaseId = "TC_190_Add_More_Link")
	public void addMoreLink() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			OpenerStoreSummarySecondaryChecklistPage pobj = new OpenerStoreSummarySecondaryChecklistPage(driver);

			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			String secondaryChecklistName = fc.utobj().generateTestData(dataSet.get("secondaryChecklistName"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String task = fc.utobj().generateTestData(dataSet.get("task"));

			fc.utobj().printTestStep("Navigate To Admin > Opener > Responsible Department");
			fc.utobj().printTestStep("Add Responsible Department");

			AdminOpenerResponsibleDepartmentPageTest addDepartmentPage = new AdminOpenerResponsibleDepartmentPageTest();
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));
			addDepartmentPage.addDepartment(driver, responsibilityArea);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			/*
			 * String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String password=dataSet.get("password");
			 * corporatePage.addCorporateUser(driver, userName, password,
			 * config);
			 */

			String emailId = "openerautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporatePage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");

			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			fc.utobj().printTestStep("Add Secondary Checklist With Task");
			AdminOpenerSecondaryChecklistPageTest secondaryChecklistPage = new AdminOpenerSecondaryChecklistPageTest();
			String referenceDate = "";
			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			File file = new File(filePath);
			secondaryChecklistPage.addSecondaryChecklistWithTask(driver, dataSet, secondaryChecklistName, description,
					responsibilityArea, corpUser.getuserFullName(), storeType, file, referenceDate, task, config);

			fc.utobj().printTestStep("Add Checklist By More Add Option");
			fc.opener().opener_common().openerStoreSummary(driver);
			addStorePage.setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, storeNoFranchiseID));
			fc.utobj().clickElement(driver, pobj.secondaryChecklistLnk);
			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, secondaryChecklistName));

			fc.utobj().clickElement(driver, pobj.addMoreLink);

			task = fc.utobj().generateTestData(dataSet.get("task"));
			fc.utobj().sendKeys(driver, pobj.secondaryNameTxtBox, task);

			fc.utobj().clickElement(driver, pobj.responsibilityAreaSelctBtn);
			fc.utobj().sendKeys(driver, pobj.responsibilityAreaTextBx, responsibilityArea);
			fc.utobj().clickElement(driver, pobj.responsibilityAreaSelectAllBtn);
			fc.utobj().clickElement(driver, pobj.responsibilityAreaSelctBtn);

			fc.utobj().clickElement(driver, pobj.ContactSelectBtn);
			fc.utobj().sendKeys(driver, pobj.contactTextBox, corpUser.getuserFullName());
			fc.utobj().clickElement(driver, pobj.contactSelectAll);

			/*
			 * fc.utobj().selectDropDown(driver, pobj.groupDropDown, groupName);
			 */

			String franchiseeAccess = dataSet.get("franchiseeAccess");
			fc.utobj().selectDropDown(driver, pobj.franchiseeAccessDropDown, franchiseeAccess);

			String dependentOn = dataSet.get("dependentOn");
			String valueText = fc.utobj()
					.getElementByXpath(driver,
							".//select[@name='smUserSecondryChecklist_0referenceParent']/option[contains(text () , '"
									+ dependentOn + "')]")
					.getAttribute("value");
			fc.utobj().moveToElement(driver, pobj.dependentOnDropDown);
			Select sl = new Select(pobj.dependentOnDropDown);
			sl.selectByValue(valueText);

			if (fc.utobj().isSelected(driver,pobj.uploadFileRadioBtn)) {
				// do nothing
			} else {
				fc.utobj().clickElement(driver, pobj.uploadFileRadioBtn);
			}

			System.out.println(file.getAbsolutePath());
			fc.utobj().sendKeys(driver, pobj.attachmentBrowseBox, file.getAbsolutePath());
			fc.utobj().clickElement(driver, pobj.addBtn);

			try {
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().clickElement(driver, pobj.noBtn);
				driver.switchTo().defaultContent();
			} catch (Exception e) {

			}

			fc.utobj().printTestStep("Verify The Checklist");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, task);

			if (isTextPresent == false) {
				fc.utobj().throwsSkipException("was not able add task checklist");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void actionImgOption(WebDriver driver, String task, String option) throws Exception {

		String alterText = driver
				.findElement(By.xpath(".//*[.='" + task + "']/ancestor::tr/td/div[@id='menuBar']/layer"))
				.getAttribute("id").trim();
		alterText = alterText.replace("Actions_dynamicmenu", "");
		alterText = alterText.replace("Bar", "");

		fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
				".//*[.='" + task + "']/ancestor::tr/td/div[@id='menuBar']/layer/a/img"));
		fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
				".//div[@id='Actions_dynamicmenu" + alterText + "Menu']/span[contains(text () , '" + option + "')]"));

	}

	public void configureDefaultValue(WebDriver driver) throws Exception {

		OpenerStoreSummarySecondaryChecklistPage pobj = new OpenerStoreSummarySecondaryChecklistPage(driver);

		fc.utobj().clickElement(driver, pobj.configureDefaultValueBtn);

		String text = fc.utobj().getText(driver, driver
				.findElement(By.xpath(".//*[@id='ms-parentsmUserSecondryChecklist_0responsibilityArea']/button/span")));
		if (text.equalsIgnoreCase("All Selected")) {
			fc.utobj().clickElement(driver, pobj.responsibilityAreaSelctBtn);
			fc.utobj().clickElement(driver, pobj.responsibilityAreaSelectAllBtn);
			fc.utobj().clickElement(driver, pobj.responsibilityAreaSelctBtn);
		} else {

			fc.utobj().clickElement(driver, pobj.responsibilityAreaSelctBtn);
			fc.utobj().clickElement(driver, pobj.responsibilityAreaSelectAllBtn);
			fc.utobj().clickElement(driver, pobj.responsibilityAreaSelectAllBtn);
			fc.utobj().clickElement(driver, pobj.responsibilityAreaSelctBtn);

		}

		String text2 = fc.utobj().getText(driver, fc.utobj().getElementByXpath(driver,
				".//*[@id='ms-parentsmUserSecondryChecklist_0contact']/button/span"));

		if (text2.equalsIgnoreCase("All Selected")) {
			fc.utobj().clickElement(driver, pobj.ContactSelectBtn);
			fc.utobj().clickElement(driver, pobj.contactSelectAll);
			fc.utobj().clickElement(driver, pobj.ContactSelectBtn);
		} else {

			fc.utobj().clickElement(driver, pobj.ContactSelectBtn);
			fc.utobj().clickElement(driver, pobj.contactSelectAll);
			fc.utobj().clickElement(driver, pobj.contactSelectAll);
			fc.utobj().clickElement(driver, pobj.ContactSelectBtn);
		}
		String dependentOn = "None (Timeless)";
		String valueText = fc.utobj()
				.getElementByXpath(driver,
						".//select[@name='smUserSecondryChecklist_0referenceParent']/option[contains(text () , '"
								+ dependentOn + "')]")
				.getAttribute("value");
		fc.utobj().moveToElement(driver, pobj.dependentOnDropDown);
		Select sl = new Select(pobj.dependentOnDropDown);
		sl.selectByValue(valueText);

		fc.utobj().clickElement(driver, pobj.saveBtn);
	}

}
