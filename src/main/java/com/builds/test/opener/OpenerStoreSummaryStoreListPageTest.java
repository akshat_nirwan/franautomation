package com.builds.test.opener;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminAreaRegionAddAreaRegionPageTest;
import com.builds.test.admin.AdminConfigurationConfigureStoreTypePageTest;
import com.builds.test.admin.AdminDivisionAddDivisionPageTest;
import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.admin.AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest;
import com.builds.test.common.CorporateUser;
import com.builds.uimaps.admin.AdminConfigurationConfigureStoreTypePage;
import com.builds.uimaps.fs.SearchUI;
import com.builds.uimaps.opener.OpenerArchivedStoreListPage;
import com.builds.uimaps.opener.OpenerStoreSummaryStoreListPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class OpenerStoreSummaryStoreListPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "opener" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add New Franchise Locaton :: Opener > Store Summary > Store List.", testCaseId = "TC_91_Add _New_Franchise_Location")
	public void addNewFranchiseLocation() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			OpenerStoreSummaryStoreListPage pobj = new OpenerStoreSummaryStoreListPage(driver);
			SearchUI search_page = new SearchUI(driver);
			String storeNoFranchiseID = fc.utobj().generateTestData(dataSet.get("storeNoFranchiseID"));
			String centerName = fc.utobj().generateTestData(dataSet.get("centerName"));
			String licenseNo = fc.utobj().generateRandomNumber();

			fc.utobj().printTestStep("Navigate To Admin > Add Area/Region");
			fc.utobj().printTestStep("Add Region");

			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			AdminAreaRegionAddAreaRegionPageTest addRegionPage = new AdminAreaRegionAddAreaRegionPageTest();
			addRegionPage.addAreaRegion(driver, regionName);

			String divisionName = fc.utobj().generateTestData(dataSet.get("divisionName"));
			AdminDivisionAddDivisionPageTest divisionPage = new AdminDivisionAddDivisionPageTest();
			divisionPage.addDivision(driver, divisionName);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "openerautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corPage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			String projectStatus = fc.utobj().generateTestData(dataSet.get("projectStatus"));
			AdminOpenerConfigureProjectStatusPageTest projectstausPage = new AdminOpenerConfigureProjectStatusPageTest();
			projectstausPage.addProjectStatus(driver, projectStatus);

			String referenceDate = fc.utobj().generateTestData(dataSet.get("referenceDate"));
			AdminOpenerManageReferenceDatesPageTest refDatePage = new AdminOpenerManageReferenceDatesPageTest();
			refDatePage.addReferenceDate(driver, referenceDate);

			fc.utobj().printTestStep("Add New Franchise Location");
			fc.opener().opener_common().openerStoreSummary(driver);
			fc.utobj().clickElement(driver, pobj.addNewFranchiseLocation);
			fc.utobj().sendKeys(driver, pobj.StoreNoFranchiseID, storeNoFranchiseID);
			fc.utobj().sendKeys(driver, pobj.centerName, centerName);
			fc.utobj().selectDropDown(driver, pobj.areaRegionDrp, regionName);
			fc.utobj().selectDropDown(driver, pobj.divisionName, divisionName);
			fc.utobj().sendKeys(driver, pobj.licenseNo, licenseNo);
			fc.utobj().selectDropDown(driver, pobj.storeType, storeType);
			fc.utobj().selectDropDown(driver, pobj.corporateLocation, "No");

			String currentDate = fc.utobj().currentDate();
			fc.utobj().sendKeys(driver, pobj.royaltyReportingStartDate, currentDate);

			fc.utobj().selectDropDown(driver, pobj.fbc, corpUser.getuserFullName());
			fc.utobj().sendKeys(driver, pobj.expectedOpeningDate, currentDate);

			fc.utobj().selectDropDown(driver, pobj.projectStatus, projectStatus);
			fc.utobj().sendKeys(driver,
					fc.utobj().getElementByXpath(driver,
							".//td[contains(text () ,'" + referenceDate + "')]/following-sibling::td[1]//input"),
					currentDate);

			try {
				List<WebElement> list = driver.findElements(
						By.xpath(".//select[contains(@id,'franchiseeS') and contains(@name,'franchiseeS') ]"));
				System.out.println(list.size());

				for (int i = 0; i < list.size(); i++) {
					String id = list.get(i).getAttribute("id").trim();
					fc.utobj().selectDropDown(driver, fc.utobj().getElementByID(driver, id),
							"FranConnect Administrator");
				}

			} catch (Exception e) {
			}

			fc.utobj().sendKeys(driver, pobj.streetAddressTextBox, dataSet.get("streetAddress"));
			fc.utobj().sendKeys(driver, pobj.address2TextBox, dataSet.get("address"));
			fc.utobj().sendKeys(driver, pobj.cityTextBox, dataSet.get("city"));
			fc.utobj().selectDropDown(driver, pobj.countryDropdown, "USA");
			fc.utobj().sendKeys(driver, pobj.zipcodeTextBox, "123456");
			fc.utobj().selectDropDown(driver, pobj.stateProvinceDropdown, "Alaska");
			fc.utobj().sendKeys(driver, pobj.PhoneTextBox, "1236547896");
			fc.utobj().sendKeys(driver, pobj.ownerFirstName, "firstName");
			fc.utobj().sendKeys(driver, pobj.ownerLastName, "lastName");
			fc.utobj().clickElement(driver, pobj.addBtn);

			boolean isSearchTrue = false;
			for (int i = 0; i < 3; i++) {
				if (isSearchTrue == false) {
					fc.utobj().sendKeys(driver, search_page.topSearchField, storeNoFranchiseID);
					Thread.sleep(2000);
					if (i == 2) {
						Thread.sleep(2000);
					}
					fc.utobj().clickEnterOnElement(driver, search_page.topSearchField);

					try {
						isSearchTrue = fc.utobj().isElementPresent(driver
								.findElements(By.xpath(".//custom[contains(text () , '" + storeNoFranchiseID + "')]")));
					} catch (Exception e) {
						Reporter.log(e.getMessage());
						System.out.println(e.getMessage());
					}
				}
			}

			if (isSearchTrue == false) {
				fc.utobj().throwsException("Not able to search Store through Top Solar Search");
			}
			fc.utobj().refresh(driver);

			fc.opener().opener_common().openerStoreSummary(driver);
			setDefaultFilterOpener(driver, storeNoFranchiseID);

			fc.utobj().printTestStep("Verify The Add New Franchise Location");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, storeNoFranchiseID);

			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to add new franchise Location");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addNewFranchiseLocation(WebDriver driver, Map<String, String> dataSet, String emailId)
			throws Exception {

		String testCaseId = "TC_Add_New_Franchise_Location_Opener_Store_List";
		String storeNoFranchiseID = null;
		if (fc.utobj().validate(testCaseId)) {
			try {
				OpenerStoreSummaryStoreListPage pobj = new OpenerStoreSummaryStoreListPage(driver);
				String centerName = fc.utobj().generateTestData(dataSet.get("centerName"));
				storeNoFranchiseID = fc.utobj().generateTestData(dataSet.get("storeNoFranchiseID"));
				String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
				String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));

				AdminAreaRegionAddAreaRegionPageTest addRegionPage = new AdminAreaRegionAddAreaRegionPageTest();
				addRegionPage.addAreaRegion(driver, regionName);

				AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
				storeTypePage.addStoreType(driver, storeType);

				AdminUsersManageCorporateUsersAddCorporateUserPageTest corporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
				CorporateUser corpUser = new CorporateUser();
				corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
				corpUser.setEmail(emailId);
				corpUser = corporatePage.createDefaultUser(driver, corpUser);

				fc.opener().opener_common().openerStoreSummary(driver);
				fc.utobj().clickElement(driver, pobj.addNewFranchiseLocation);
				fc.utobj().sendKeys(driver, pobj.StoreNoFranchiseID, storeNoFranchiseID);
				fc.utobj().sendKeys(driver, pobj.centerName, centerName);
				fc.utobj().selectDropDown(driver, pobj.areaRegionDrp, regionName);
				/*
				 * fc.utobj().selectDropDown(driver, pobj.divisionName,
				 * divisionName);
				 */
				fc.utobj().selectDropDown(driver, pobj.storeType, storeType);
				String currentDate = fc.utobj().currentDate();
				fc.utobj().sendKeys(driver, pobj.expectedOpeningDate, currentDate);
				// fc.utobj().clickElement(driver, pobj.calanderClose);
				/*
				 * fc.utobj().selectDropDown(driver, pobj.projectStatus,
				 * projectStatus);
				 */

				try {
					List<WebElement> list = driver.findElements(
							By.xpath(".//select[contains(@id,'franchiseeS') and contains(@name,'franchiseeS') ]"));
					System.out.println(list.size());
					for (int i = 0; i < list.size(); i++) {
						String id = list.get(i).getAttribute("id").trim();
						fc.utobj().selectDropDown(driver, fc.utobj().getElementByID(driver, id),
								"FranConnect Administrator");
					}
				} catch (Exception e) {

				}
				fc.utobj().sendKeys(driver, pobj.cityTextBox, dataSet.get("city"));
				fc.utobj().selectDropDown(driver, pobj.countryDropdown, "USA");
				fc.utobj().selectDropDown(driver, pobj.stateProvinceDropdown, "Alaska");
				fc.utobj().sendKeys(driver, pobj.PhoneTextBox, "1236547896");
				fc.utobj().sendKeys(driver, pobj.ownerFirstName, "firstName");
				fc.utobj().sendKeys(driver, pobj.ownerLastName, "lastName");
				fc.utobj().clickElement(driver, pobj.addBtn);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to add Franchise Location At Opener");

		}

		return storeNoFranchiseID;
	}

	public String addNewFranchiseLocation(WebDriver driver, String regionName, String storeType,
			Map<String, String> dataSet, Map<String, String> config, String emailId) throws Exception {

		String testCaseId = "TC_Add_New_Franchise_Location_Opner_01";
		String storeNoFranchiseID = null;

		if (fc.utobj().validate(testCaseId)) {
			try {
				OpenerStoreSummaryStoreListPage pobj = new OpenerStoreSummaryStoreListPage(driver);

				String centerName = fc.utobj().generateTestData(dataSet.get("centerName"));
				storeNoFranchiseID = fc.utobj().generateTestData(dataSet.get("storeNoFranchiseID"));

				AdminAreaRegionAddAreaRegionPageTest addRegionPage = new AdminAreaRegionAddAreaRegionPageTest();
				addRegionPage.addAreaRegion(driver, regionName);

				AdminUsersManageCorporateUsersAddCorporateUserPageTest corporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
				CorporateUser corpUser = new CorporateUser();
				corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
				corpUser.setEmail(emailId);
				corpUser = corporatePage.createDefaultUser(driver, corpUser);

				fc.opener().opener_common().openerStoreSummary(driver);
				fc.utobj().clickElement(driver, pobj.addNewFranchiseLocation);
				fc.utobj().sendKeys(driver, pobj.StoreNoFranchiseID, storeNoFranchiseID);
				fc.utobj().sendKeys(driver, pobj.centerName, centerName);
				fc.utobj().selectDropDown(driver, pobj.areaRegionDrp, regionName);
				fc.utobj().selectDropDown(driver, pobj.storeType, storeType);

				String currentDate = fc.utobj().currentDate();
				fc.utobj().sendKeys(driver, pobj.expectedOpeningDate, currentDate);

				try {
					List<WebElement> list = driver.findElements(
							By.xpath(".//select[contains(@id,'franchiseeS') and contains(@name,'franchiseeS') ]"));
					System.out.println(list.size());

					for (int i = 0; i < list.size(); i++) {

						String id = list.get(i).getAttribute("id").trim();
						fc.utobj().selectDropDown(driver, fc.utobj().getElementByID(driver, id),
								"FranConnect Administrator");
					}

				} catch (Exception e) {

				}

				fc.utobj().sendKeys(driver, pobj.cityTextBox, dataSet.get("city"));
				fc.utobj().selectDropDown(driver, pobj.countryDropdown, "USA");
				fc.utobj().sendKeys(driver, pobj.zipcodeTextBox, "123456");
				fc.utobj().selectDropDown(driver, pobj.stateProvinceDropdown, "Alaska");
				fc.utobj().sendKeys(driver, pobj.PhoneTextBox, "1236547896");
				fc.utobj().sendKeys(driver, pobj.ownerFirstName, "firstName");
				fc.utobj().sendKeys(driver, pobj.ownerLastName, "lastName");
				fc.utobj().clickElement(driver, pobj.addBtn);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to add Franchise Location");

		}

		return storeNoFranchiseID;
	}

	public String addNewFranchiseLocation(WebDriver driver, Map<String, String> dataSet, String regionName,
			String emailId) throws Exception {

		String testCaseId = "TC_Add_New_Franchise_Location_2";
		String storeNoFranchiseID = null;
		if (fc.utobj().validate(testCaseId)) {
			try {
				OpenerStoreSummaryStoreListPage pobj = new OpenerStoreSummaryStoreListPage(driver);

				String centerName = fc.utobj().generateTestData(dataSet.get("centerName"));
				storeNoFranchiseID = fc.utobj().generateTestData(dataSet.get("storeNoFranchiseID"));
				String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));

				AdminAreaRegionAddAreaRegionPageTest addRegionPage = new AdminAreaRegionAddAreaRegionPageTest();
				addRegionPage.addAreaRegion(driver, regionName);

				AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
				storeTypePage.addStoreType(driver, storeType);

				AdminUsersManageCorporateUsersAddCorporateUserPageTest corporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
				CorporateUser corpUser = new CorporateUser();
				corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
				corpUser.setEmail(emailId);
				corpUser = corporatePage.createDefaultUser(driver, corpUser);

				fc.opener().opener_common().openerStoreSummary(driver);

				fc.utobj().clickElement(driver, pobj.addNewFranchiseLocation);
				fc.utobj().sendKeys(driver, pobj.StoreNoFranchiseID, storeNoFranchiseID);
				fc.utobj().sendKeys(driver, pobj.centerName, centerName);
				fc.utobj().selectDropDown(driver, pobj.areaRegionDrp, regionName);
				fc.utobj().selectDropDown(driver, pobj.storeType, storeType);

				String currentDate = fc.utobj().currentDate();

				fc.utobj().sendKeys(driver, pobj.expectedOpeningDate, currentDate);
				// fc.utobj().clickElement(driver, pobj.calanderClose);

				try {
					List<WebElement> list = driver.findElements(
							By.xpath(".//select[contains(@id,'franchiseeS') and contains(@name,'franchiseeS') ]"));
					System.out.println(list.size());

					for (int i = 0; i < list.size(); i++) {

						String id = list.get(i).getAttribute("id").trim();
						fc.utobj().selectDropDown(driver, fc.utobj().getElementByID(driver, id),
								"FranConnect Administrator");
					}

				} catch (Exception e) {

				}

				// fc.utobj().sendKeys(driver, pobj.streetAddressTextBox,
				// dataSet.get("streetAddress"));
				// fc.utobj().sendKeys(driver, pobj.address2TextBox,
				// dataSet.get("address"));
				fc.utobj().sendKeys(driver, pobj.cityTextBox, dataSet.get("city"));
				fc.utobj().selectDropDown(driver, pobj.countryDropdown, "USA");
				// fc.utobj().sendKeys(driver, pobj.zipcodeTextBox, "123456");
				fc.utobj().selectDropDown(driver, pobj.stateProvinceDropdown, "Alaska");
				fc.utobj().sendKeys(driver, pobj.PhoneTextBox, "1236547896");
				fc.utobj().sendKeys(driver, pobj.ownerFirstName, "firstName");
				fc.utobj().sendKeys(driver, pobj.ownerLastName, "lastName");
				fc.utobj().clickElement(driver, pobj.addBtn);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to add Franchise Location At Opener");

		}

		return storeNoFranchiseID;
	}

	// withDivision

	public String addNewFranchiseLocationWithDivision(WebDriver driver, Map<String, String> dataSet,
			String divisionName, String regionName, Map<String, String> config, String emailId) throws Exception {

		String testCaseId = "TC_Add_New_Franchise_Location_2";
		String storeNoFranchiseID = null;
		if (fc.utobj().validate(testCaseId)) {
			try {
				OpenerStoreSummaryStoreListPage pobj = new OpenerStoreSummaryStoreListPage(driver);

				String centerName = fc.utobj().generateTestData(dataSet.get("centerName"));
				storeNoFranchiseID = fc.utobj().generateTestData(dataSet.get("storeNoFranchiseID"));
				String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));

				AdminAreaRegionAddAreaRegionPageTest addRegionPage = new AdminAreaRegionAddAreaRegionPageTest();
				addRegionPage.addAreaRegion(driver, regionName);

				AdminDivisionAddDivisionPageTest divisionPage = new AdminDivisionAddDivisionPageTest();
				divisionPage.addDivision(driver, divisionName);

				AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
				storeTypePage.addStoreType(driver, storeType);

				AdminUsersManageCorporateUsersAddCorporateUserPageTest corporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
				CorporateUser corpUser = new CorporateUser();
				corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
				corpUser.setEmail(emailId);
				corpUser = corporatePage.createDefaultUser(driver, corpUser);

				fc.opener().opener_common().openerStoreSummary(driver);

				fc.utobj().clickElement(driver, pobj.addNewFranchiseLocation);
				fc.utobj().sendKeys(driver, pobj.StoreNoFranchiseID, storeNoFranchiseID);
				fc.utobj().sendKeys(driver, pobj.centerName, centerName);
				fc.utobj().selectDropDown(driver, pobj.areaRegionDrp, regionName);
				fc.utobj().selectDropDown(driver, pobj.divisionName, divisionName);
				// fc.utobj().sendKeys(driver, pobj.licenseNo, licenseNo);
				fc.utobj().selectDropDown(driver, pobj.storeType, storeType);
				// fc.utobj().selectDropDown(driver, pobj.corporateLocation,
				// "No");

				String currentDate = fc.utobj().currentDate();
				// fc.utobj().sendKeys(driver, pobj.royaltyReportingStartDate,
				// currentDate);
				// fc.utobj().clickElement(driver, pobj.calanderClose);

				// fc.utobj().selectDropDown(driver, pobj.fbc, userName);
				fc.utobj().sendKeys(driver, pobj.expectedOpeningDate, currentDate);
				// fc.utobj().clickElement(driver, pobj.calanderClose);
				/*
				 * fc.utobj().selectDropDown(driver, pobj.projectStatus,
				 * projectStatus);
				 */

				try {
					List<WebElement> list = driver.findElements(
							By.xpath(".//select[contains(@id,'franchiseeS') and contains(@name,'franchiseeS') ]"));
					System.out.println(list.size());

					for (int i = 0; i < list.size(); i++) {

						String id = list.get(i).getAttribute("id").trim();
						fc.utobj().selectDropDown(driver, fc.utobj().getElementByID(driver, id),
								"FranConnect Administrator");
					}

				} catch (Exception e) {

				}

				// fc.utobj().sendKeys(driver, pobj.streetAddressTextBox,
				// dataSet.get("streetAddress"));
				// fc.utobj().sendKeys(driver, pobj.address2TextBox,
				// dataSet.get("address"));
				fc.utobj().sendKeys(driver, pobj.cityTextBox, dataSet.get("city"));
				fc.utobj().selectDropDown(driver, pobj.countryDropdown, "USA");
				// fc.utobj().sendKeys(driver, pobj.zipcodeTextBox, "123456");
				fc.utobj().selectDropDown(driver, pobj.stateProvinceDropdown, "Alaska");
				fc.utobj().sendKeys(driver, pobj.PhoneTextBox, "1236547896");
				fc.utobj().sendKeys(driver, pobj.ownerFirstName, "firstName");
				fc.utobj().sendKeys(driver, pobj.ownerLastName, "lastName");
				fc.utobj().clickElement(driver, pobj.addBtn);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to add Franchise Location At Opener");

		}

		return storeNoFranchiseID;
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Modification of Store Info :: Opener > Store Summary > Store List", testCaseId = "TC_92_Modify_Store_Info")
	public void modifyStoreInfo() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			OpenerStoreSummaryStoreListPage pobj = new OpenerStoreSummaryStoreListPage(driver);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");
			String emailId = "openerautomation@staffex.com";
			String storeNoFranchiseID = addNewFranchiseLocation(driver, dataSet, emailId);

			fc.utobj().printTestStep("Navigate To Admin > Add Area/Region");
			fc.utobj().printTestStep("Add Region");

			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			AdminAreaRegionAddAreaRegionPageTest addRegionPage = new AdminAreaRegionAddAreaRegionPageTest();
			addRegionPage.addAreaRegion(driver, regionName);

			String divisionName = fc.utobj().generateTestData(dataSet.get("divisionName"));
			AdminDivisionAddDivisionPageTest divisionPage = new AdminDivisionAddDivisionPageTest();
			divisionPage.addDivision(driver, divisionName);

			String projectStatus = fc.utobj().generateTestData(dataSet.get("projectStatus"));
			AdminOpenerConfigureProjectStatusPageTest projectstausPage = new AdminOpenerConfigureProjectStatusPageTest();
			projectstausPage.addProjectStatus(driver, projectStatus);

			fc.utobj().printTestStep("Modify The Store Info");
			String parentWindow = driver.getWindowHandle();

			fc.opener().opener_common().openerStoreSummary(driver);
			setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			String alterText = driver
					.findElement(
							By.xpath(".//a[.='" + storeNoFranchiseID + "']/ancestor::tr/td/div[@id='menuBar']/layer"))
					.getAttribute("id").trim();
			alterText = alterText.replace("Actions_dynamicmenu", "");
			alterText = alterText.replace("Bar", "");

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[.='" + storeNoFranchiseID + "']/ancestor::tr/td/div[@id='menuBar']/layer/a/img"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//div[@id='Actions_dynamicmenu" + alterText + "Menu']/span[contains(text () , 'Modify')]"));

			fc.utobj().selectDropDown(driver, pobj.areaRegionDrp, regionName);
			fc.utobj().selectDropDown(driver, pobj.divisionName, divisionName);
			fc.utobj().sendKeys(driver, pobj.licenseNo, fc.utobj().generateRandomNumber());
			fc.utobj().selectDropDown(driver, pobj.licenseType, "New License");

			// 73436
			String mDate = fc.utobj().getFutureDateUSFormat(2);

			System.out.println("mdate : >>>>>>>>>>>>>>>>>> " + mDate);

			fc.utobj().sendKeys(driver, pobj.expectedOpeningDate1, mDate);

			fc.utobj().selectDropDown(driver, pobj.projectStatus, projectStatus);
			fc.utobj().selectDropDown(driver, pobj.storeStatus, "Non-Operational");
			fc.utobj().sendKeys(driver, pobj.streetAddressTextBox, dataSet.get("streetAddress"));
			fc.utobj().sendKeys(driver, pobj.address2TextBox, dataSet.get("address"));
			fc.utobj().sendKeys(driver, pobj.cityTextBox, dataSet.get("city"));
			fc.utobj().selectDropDown(driver, pobj.countryDropdown, "USA");
			fc.utobj().sendKeys(driver, pobj.zipcodeTextBox, "123456");
			fc.utobj().selectDropDown(driver, pobj.stateProvinceDropdown, "Alaska");
			fc.utobj().sendKeys(driver, pobj.PhoneTextBox, "1236547896");
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("Verify The Modify Store Info");
			if (fc.utobj().getElementByXpath(driver, ".//*[.='" + regionName + "']").isDisplayed()
					&& fc.utobj().getElementByXpath(driver, ".//*[.='" + divisionName + "']").isDisplayed()
					&& fc.utobj().getElementByXpath(driver, ".//*[.='" + projectStatus + "']").isDisplayed()
					&& fc.utobj().getElementByXpath(driver, ".//*[.='Non-Operational']").isDisplayed()
					&& fc.utobj().getElementByXpath(driver, ".//*[.='New License']").isDisplayed()) {
				// do nothing
			} else {
				fc.utobj().throwsException("was not able to modify Store Info");
			}

			// 73436

			WebElement elm = fc.utobj().getElementByXpath(driver,
					".//td[.='Expected Opening Date']/following-sibling::td/a[.='" + mDate + "']");
			fc.utobj().clickElement(driver, elm);

			fc.commonMethods().switch_cboxIframe_frameId(driver);

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + mDate + "')]").isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to verify EOD after modification");
			}
			driver.switchTo().window(parentWindow);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "opener" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Move To Info Mgr :: Opener > Store Summary > Store List", testCaseId = "TC_93_Move_To_Info_Mgr")
	public void moveToInfoMgr() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			OpenerStoreSummaryStoreListPage pobj = new OpenerStoreSummaryStoreListPage(driver);
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");
			String emailId = "openerautomation@staffex.com";
			String storeNoFranchiseID = addNewFranchiseLocation(driver, dataSet, emailId);

			fc.opener().opener_common().openerStoreSummary(driver);
			setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			fc.utobj().printTestStep("Move To Info Mgr");
			String alterText = driver
					.findElement(
							By.xpath(".//a[.='" + storeNoFranchiseID + "']/ancestor::tr/td/div[@id='menuBar']/layer"))
					.getAttribute("id").trim();
			alterText = alterText.replace("Actions_dynamicmenu", "");
			alterText = alterText.replace("Bar", "");

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[.='" + storeNoFranchiseID + "']/ancestor::tr/td/div[@id='menuBar']/layer/a/img"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//div[@id='Actions_dynamicmenu"
					+ alterText + "Menu']/span[contains(text () , 'Move To Info Mgr')]"));
			fc.utobj().acceptAlertBox(driver);

			String parantWindow = driver.getWindowHandle();

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.okBtn);

			driver.switchTo().window(parantWindow);

			fc.utobj().printTestStep("Verify The Move To Info Mgr");
			// setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * JavascriptExecutor myExecutor = ((JavascriptExecutor) driver);
			 * myExecutor.executeScript("arguments[0].value='"+
			 * storeNoFranchiseID+"';", pobj.fimSearchString);
			 * fc.utobj().clickElement(driver, pobj.fimSearchBtn);
			 */

			new OpenerArchivedStoreListPageTest().filterAtInfo(driver, new OpenerArchivedStoreListPage(driver),
					storeNoFranchiseID);

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, storeNoFranchiseID));

			if (fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + storeNoFranchiseID + "')]")
					.isDisplayed()) {

			} else {
				fc.utobj().throwsException("was not able to move to Info Mgr");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "opener" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Archive Store :: Opener > Store Summary > Store List", testCaseId = "TC_94_Archive_Store")
	public void archiveStore() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			OpenerStoreSummaryStoreListPage pobj = new OpenerStoreSummaryStoreListPage(driver);
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");
			String emailId = "openerautomation@staffex.com";
			String storeNoFranchiseID = addNewFranchiseLocation(driver, dataSet, emailId);

			fc.opener().opener_common().openerStoreSummary(driver);
			setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			fc.utobj().printTestStep("Archive Store");
			/*
			 * String alterText=fc.utobj().getElementByXpath(driver,".//a[.='"+
			 * storeNoFranchiseID+"']/ancestor::tr/td/div[@id='menuBar']/layer")
			 * ).getAttribute("id").trim();
			 * alterText=alterText.replace("Actions_dynamicmenu", "");
			 * alterText=alterText.replace("Bar", "");
			 * 
			 * fc.utobj().clickElement(driver,
			 * fc.utobj().getElementByXpath(driver,".//a[.='"+
			 * storeNoFranchiseID+
			 * "']/ancestor::tr/td/div[@id='menuBar']/layer/a/img")));
			 * fc.utobj().clickElement(driver,
			 * fc.utobj().getElementByXpath(driver,
			 * ".//div[@id='Actions_dynamicmenu"+ alterText+
			 * "Menu']/span[contains(text () , 'Archive')]")));
			 * fc.utobj().acceptAlertBox(driver);
			 */
			fc.utobj().actionImgOption(driver, storeNoFranchiseID, "Archive");
			fc.utobj().acceptAlertBox(driver);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			/*
			 * fc.utobj().printTestStep(testCaseId, "Verify The Archive Store");
			 * boolean isTextPresent=fc.utobj().getElementByXpath(driver,
			 * ".//td[.='No records found.']")).isDisplayed();
			 */

			fc.utobj().printTestStep("Verify The Archive Store At Store List Page");
			// fc.utobj().clickElement(driver, pobj.showFilter);
			boolean isTextPresent = fc.utobj().searchInSelectBoxSingleValue(driver, pobj.selectStoreNumber,
					storeNoFranchiseID);

			if (isTextPresent) {
				fc.utobj().throwsException("Not able to verify the Archived Store At Store List Page");
			}

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, "Archived"));

			setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			boolean isTextPresent1 = fc.utobj().assertPageSource(driver, storeNoFranchiseID);

			if (isTextPresent1 == false) {
				fc.utobj().throwsException("Not able to verify archived store archived page");
			}

			fc.opener().opener_common().openerTasks(driver);

			try {
				fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, "Show Filters"));
			} catch (Exception e) {

			}

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='ms-parentstoreNameCombo']/button"));
			fc.utobj().sendKeys(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='ms-parentstoreNameCombo']/div/div/input"),
					storeNoFranchiseID);

			if (!fc.utobj()
					.getElementByXpath(driver, ".//*[@id='ms-parentstoreNameCombo']/div/ul/li[.='No matches found']")
					.isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to verify store at opener tasks page in store no filter");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String archiveStore(WebDriver driver, Map<String, String> dataSet, String regionName, String emailId)
			throws Exception {

		String testCaseId = "TC_Archive_Store_Opner_Store_List";
		String storeNoFranchiseID = null;

		if (fc.utobj().validate(testCaseId)) {
			try {

				storeNoFranchiseID = addNewFranchiseLocation(driver, dataSet, regionName, emailId);

				fc.opener().opener_common().openerStoreSummary(driver);
				setDefaultFilterOpener(driver, storeNoFranchiseID);

				/*
				 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
				 * storeNoFranchiseID); fc.utobj().clickElement(driver,
				 * pobj.searchImgBtn);
				 */

				String alterText = driver
						.findElement(By
								.xpath(".//a[.='" + storeNoFranchiseID + "']/ancestor::tr/td/div[@id='menuBar']/layer"))
						.getAttribute("id").trim();
				alterText = alterText.replace("Actions_dynamicmenu", "");
				alterText = alterText.replace("Bar", "");

				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//a[.='" + storeNoFranchiseID + "']/ancestor::tr/td/div[@id='menuBar']/layer/a/img"));
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//div[@id='Actions_dynamicmenu" + alterText + "Menu']/span[contains(text () , 'Archive')]"));

				fc.utobj().acceptAlertBox(driver);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to Archive Store");

		}

		return storeNoFranchiseID;
	}

	@Test(groups = { "opener" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Move To Info Mgr By Action Btn:: Opener > Store Summary > Store List", testCaseId = "TC_95_Move_To_Info_Mgr01")
	public void moveToInfoMgrActionBtn() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			OpenerStoreSummaryStoreListPage pobj = new OpenerStoreSummaryStoreListPage(driver);
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");
			String emailId = "openerautomation@staffex.com";
			String storeNoFranchiseID = addNewFranchiseLocation(driver, dataSet, emailId);

			fc.opener().opener_common().openerStoreSummary(driver);

			setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[.='" + storeNoFranchiseID + "']/ancestor::tr/td/input[@name='selectedItem']"));

			fc.utobj().printTestStep("Move To Info Mgr By Action Button Option");
			fc.utobj().selectActionMenuItems(driver, "Move to Info Mgr");

			fc.utobj().acceptAlertBox(driver);

			String parantWindow = driver.getWindowHandle();

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.okBtn);

			driver.switchTo().window(parantWindow);

			// setDefaultFilterOpener(driver, storeNoFranchiseID);

			new OpenerArchivedStoreListPageTest().filterAtInfo(driver, new OpenerArchivedStoreListPage(driver),
					storeNoFranchiseID);

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Verify The Move To Info Mgr"); JavascriptExecutor myExecutor =
			 * ((JavascriptExecutor) driver);
			 * myExecutor.executeScript("arguments[0].value='"+
			 * storeNoFranchiseID+"';", pobj.fimSearchString);
			 * fc.utobj().clickElement(driver, pobj.fimSearchBtn);
			 */

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, storeNoFranchiseID));

			if (fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + storeNoFranchiseID + "')]")
					.isDisplayed()) {

			} else {
				fc.utobj().throwsException("was not able to move to Info Mgr");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "opener" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Archive Store at Action Btn :: Opener > Store Summary > Store List.", testCaseId = "TC_96_Archive01")
	public void archiveStoreActionBtn() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			OpenerStoreSummaryStoreListPage pobj = new OpenerStoreSummaryStoreListPage(driver);
			driver = fc.loginpage().login(driver);

			// 73363
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");
			String emailId = "openerautomation@staffex.com";
			String storeNoFranchiseID = addNewFranchiseLocation(driver, regionName, storeType, dataSet, config,
					emailId);

			AdminOpenerTaskChecklistPageTest addTaskPage = new AdminOpenerTaskChecklistPageTest();
			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			String referenceDate = "";

			fc.utobj().printTestStep("Add Task Checklist");
			String task = addTaskPage.addTaskChecklistUploadFile(driver, storeType, filePath, dataSet, referenceDate,
					config);

			fc.opener().opener_common().openerStoreSummary(driver);
			setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '"
					+ storeNoFranchiseID + "')]/ancestor::tr/td/input[@name='selectedItem']"));

			fc.utobj().printTestStep("Archive Location");
			fc.utobj().selectActionMenuItems(driver, "Archive");
			fc.utobj().acceptAlertBox(driver);

			// setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			// boolean
			// isTextPresent=fc.utobj().getElementByXpath(driver,".//td[.='No
			// records found.']")).isDisplayed();

			fc.utobj().printTestStep("Verify The Archive Store At Store List Page");
			// fc.utobj().clickElement(driver, pobj.showFilter);
			boolean isTextPresent = fc.utobj().searchInSelectBoxSingleValue(driver, pobj.selectStoreNumber,
					storeNoFranchiseID);

			if (isTextPresent) {
				fc.utobj().throwsException("Not able to verify the Archived Store At Store List Page");
			}

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, "Archived"));
			setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			boolean isTextPresent1 = fc.utobj().assertPageSource(driver, storeNoFranchiseID);

			if (isTextPresent1) {

			} else {
				fc.utobj().throwsException("was not able to archived store");
			}

			String parentWindow = driver.getWindowHandle();

			fc.utobj().printTestStep("Complete The Task Checklist");
			// 73363
			fc.opener().opener_common().openerArchived(driver);

			setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='1']"));

			actionImgOption(driver, task, "Complete");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String curDate = fc.utobj().currentDate();
			fc.utobj().sendKeys(driver, pobj.completionDateTxBx, curDate);
			// fc.utobj().clickElement(driver, pobj.calanderClose);
			String comments = fc.utobj().generateTestData(dataSet.get("comments"));
			fc.utobj().sendKeys(driver, pobj.commentsTxBx, comments);
			fc.utobj().clickElement(driver, pobj.completeBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().printTestStep("Verify The Complete Checklist");
			boolean isText = fc.utobj().assertPageSource(driver, task);

			if (isText == true) {
				fc.utobj().throwsException("was not able to verify archive overdue task");
			}

			// first unArchive the store and verify in store list page
			fc.opener().opener_common().openerArchived(driver);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			setDefaultFilterOpener(driver, storeNoFranchiseID);

			actionImgOption(driver, storeNoFranchiseID, "Unarchive");

			fc.utobj().acceptAlertBox(driver);

			fc.opener().opener_common().openerStoreSummary(driver);

			setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			fc.utobj().clickElement(driver, fc.utobj().getElementByPartialLinkText(driver, storeNoFranchiseID));
			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, "Task Checklist"));

			if (!driver
					.findElement(By.xpath(
							".//*[contains(text () , '" + task + "')]/ancestor::tr/td[contains(text () , 'Complete')]"))
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify overdue task completion status");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "opener" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Move To Info Mgr By Bottom Btn:: Opener > Store Summary > Store List", testCaseId = "TC_97_Move_To_Info_Mgr02")
	public void moveToInfoMgrBottomBtn() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			OpenerStoreSummaryStoreListPage pobj = new OpenerStoreSummaryStoreListPage(driver);
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");
			String emailId = "openerautomation@staffex.com";
			String storeNoFranchiseID = addNewFranchiseLocation(driver, dataSet, emailId);

			fc.utobj().printTestStep("Move To Info Mgr By Bottom Button Option");
			fc.opener().opener_common().openerStoreSummary(driver);
			setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[.='" + storeNoFranchiseID + "']/ancestor::tr/td/input[@name='selectedItem']"));
			fc.utobj().clickElement(driver, pobj.moveToInfoMgrBottomBtn);

			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify Move To Info Mgr");
			String parantWindow = driver.getWindowHandle();

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.okBtn);

			driver.switchTo().window(parantWindow);

			// setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * JavascriptExecutor myExecutor = ((JavascriptExecutor) driver);
			 * myExecutor.executeScript("arguments[0].value='"+
			 * storeNoFranchiseID+"';", pobj.fimSearchString);
			 * fc.utobj().clickElement(driver, pobj.fimSearchBtn);
			 */

			new OpenerArchivedStoreListPageTest().filterAtInfo(driver, new OpenerArchivedStoreListPage(driver),
					storeNoFranchiseID);

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, storeNoFranchiseID));

			if (fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + storeNoFranchiseID + "')]")
					.isDisplayed()) {

			} else {
				fc.utobj().throwsException("was not able to move to Info Mgr");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "opener" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Archive Store at Bottom Btn :: Opener > Store Summary > Store List.", testCaseId = "TC_98_Archived02")
	public void archiveStoreBottomBtn() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			OpenerStoreSummaryStoreListPage pobj = new OpenerStoreSummaryStoreListPage(driver);
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");
			String emailId = "openerautomation@staffex.com";
			String storeNoFranchiseID = addNewFranchiseLocation(driver, dataSet, emailId);

			fc.opener().opener_common().openerStoreSummary(driver);
			setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			fc.utobj().printTestStep("Archive Store By Bottom Button");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '"
					+ storeNoFranchiseID + "')]/ancestor::tr/td/input[@name='selectedItem']"));
			fc.utobj().clickElement(driver, pobj.archiveBottomBtn);
			fc.utobj().acceptAlertBox(driver);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			fc.utobj().printTestStep("Verify The Archive Store At Store List Page");
			// fc.utobj().clickElement(driver, pobj.showFilter);
			boolean isTextPresent = fc.utobj().searchInSelectBoxSingleValue(driver, pobj.selectStoreNumber,
					storeNoFranchiseID);

			if (isTextPresent) {
				fc.utobj().throwsException("Not able to verify the Archived Store At Store List Page");
			}

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, "Archived"));

			setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			boolean isTextPresent1 = fc.utobj().assertPageSource(driver, storeNoFranchiseID);
			if (isTextPresent1) {

			} else {
				fc.utobj().throwsException("was not able to archived store");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "opener" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Modify Store Info At Store Info Page :: Opener > Store Summary > Store List", testCaseId = "TC_99_Modify_Store_Info_01")
	public void modifyStoreInfo_01() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			OpenerStoreSummaryStoreListPage pobj = new OpenerStoreSummaryStoreListPage(driver);
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");
			String emailId = "openerautomation@staffex.com";
			String storeNoFranchiseID = addNewFranchiseLocation(driver, dataSet, emailId);

			fc.utobj().printTestStep("Navigate To Admin > Add Area/Region");
			fc.utobj().printTestStep("Add Region");

			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			AdminAreaRegionAddAreaRegionPageTest addRegionPage = new AdminAreaRegionAddAreaRegionPageTest();
			addRegionPage.addAreaRegion(driver, regionName);

			fc.opener().opener_common().openerStoreSummary(driver);
			setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			fc.utobj().printTestStep("Modify The Store");
			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, storeNoFranchiseID));
			fc.utobj().clickElement(driver, pobj.modifyLink);

			fc.utobj().selectDropDown(driver, pobj.areaRegionDrp, regionName);
			/*
			 * fc.utobj().selectDropDown(driver, pobj.divisionName,
			 * divisionName);
			 */
			fc.utobj().sendKeys(driver, pobj.licenseNo, fc.utobj().generateRandomNumber());
			fc.utobj().selectDropDown(driver, pobj.licenseType, "New License");
			/*
			 * fc.utobj().selectDropDown(driver, pobj.projectStatus,
			 * projectStatus);
			 */
			fc.utobj().selectDropDown(driver, pobj.storeStatus, "Non-Operational");
			fc.utobj().sendKeys(driver, pobj.streetAddressTextBox, dataSet.get("streetAddress"));
			fc.utobj().sendKeys(driver, pobj.address2TextBox, dataSet.get("address"));
			fc.utobj().sendKeys(driver, pobj.cityTextBox, dataSet.get("city"));
			fc.utobj().selectDropDown(driver, pobj.countryDropdown, "USA");
			fc.utobj().sendKeys(driver, pobj.zipcodeTextBox, "123456");
			fc.utobj().selectDropDown(driver, pobj.stateProvinceDropdown, "Alaska");
			fc.utobj().sendKeys(driver, pobj.PhoneTextBox, "1236547896");

			// 73495

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , 'Equipment Checklist')]/ancestor::tr/td/input[@name='checkForPrimary1']"));

			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("Verify The Modify Store Info");
			if (fc.utobj().getElementByXpath(driver, ".//*[.='" + regionName + "']").isDisplayed()
					&& fc.utobj().getElementByXpath(driver, ".//*[.='Non-Operational']").isDisplayed()
					&& fc.utobj().getElementByXpath(driver, ".//*[.='New License']").isDisplayed()) {
				// do nothing
			} else {
				fc.utobj().throwsException("was not able to modify Store Info");
			}

			fc.utobj().clickElement(driver, pobj.primaryChecklistLink);

			if (!fc.utobj().getElementByXpath(driver, ".//input[@value='Add More']").isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to verify Primary checklist Add More Btn");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//input[@value='Configure Default Values']").isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to verify Primary checklist Configure Default Values Btn");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Store with Divisional user Login :: Opener > Store Summary > Store List", testCaseId = "TC_216_Verify_Store_Divisional_User")
	public void verifyStoreDivisionalUser() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);

			String divisionName = fc.utobj().generateTestData(dataSet.get("divisionName"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			/*
			 * String projectStatus=fc.utobj().generateTestData(dataSet.get(
			 * "projectStatus"));
			 */
			String password = dataSet.get("password");
			String emailId = "openerautomation@staffex.com";
			fc.utobj().printTestStep("Add New Franchise Location");
			String storeNoFranchiseID = addNewFranchiseLocationWithDivision(driver, dataSet, divisionName, regionName,
					config, emailId);

			fc.utobj().printTestStep("Add Divisional User");
			AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest addDivUserPage = new AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest();
			String userName = fc.utobj().generateTestData(dataSet.get("divUserName"));
			addDivUserPage.addDivisionalUser(driver, userName, password, divisionName, emailId);

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With Divisional User");
			fc.loginpage().loginWithParameter(driver, userName, password);

			fc.utobj().printTestStep("Verify Store");
			fc.opener().opener_common().openerStoreSummary(driver);

			setDefaultFilterOpener(driver, storeNoFranchiseID);
			boolean isTextPresent = fc.utobj().assertPageSource(driver, storeNoFranchiseID);

			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to verify store At divisional user");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Reference Date History :: Opener > Store Summary > Store List", testCaseId = "TC_217_Verify_Reference_Field_History")
	public void verifyReferenceFieldHistory() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			OpenerStoreSummaryStoreListPage pobj = new OpenerStoreSummaryStoreListPage(driver);

			String parentWindow = driver.getWindowHandle();
			String storeNoFranchiseID = fc.utobj().generateTestData(dataSet.get("storeNoFranchiseID"));
			String centerName = fc.utobj().generateTestData(dataSet.get("centerName"));
			String licenseNo = fc.utobj().generateRandomNumber();

			fc.utobj().printTestStep("Navigate To Admin > Add Area/Region");
			fc.utobj().printTestStep("Add Region");

			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			AdminAreaRegionAddAreaRegionPageTest addRegionPage = new AdminAreaRegionAddAreaRegionPageTest();
			addRegionPage.addAreaRegion(driver, regionName);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "openerautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = corporatePage.createDefaultUser(driver, corpUser);

			String referenceDate = fc.utobj().generateTestData(dataSet.get("referenceDate"));
			AdminOpenerManageReferenceDatesPageTest refDatePage = new AdminOpenerManageReferenceDatesPageTest();
			refDatePage.addReferenceDate(driver, referenceDate);

			fc.utobj().printTestStep("Add New Franchise Location");
			fc.opener().opener_common().openerStoreSummary(driver);

			fc.utobj().clickElement(driver, pobj.addNewFranchiseLocation);
			fc.utobj().sendKeys(driver, pobj.StoreNoFranchiseID, storeNoFranchiseID);
			fc.utobj().sendKeys(driver, pobj.centerName, centerName);
			fc.utobj().selectDropDown(driver, pobj.areaRegionDrp, regionName);
			fc.utobj().sendKeys(driver, pobj.licenseNo, licenseNo);
			fc.utobj().selectDropDown(driver, pobj.storeType, storeType);
			fc.utobj().selectDropDown(driver, pobj.corporateLocation, "No");

			String currentDate = fc.utobj().currentDate();
			fc.utobj().sendKeys(driver, pobj.royaltyReportingStartDate, currentDate);

			fc.utobj().selectDropDown(driver, pobj.fbc, corpUser.getuserFullName());
			fc.utobj().sendKeys(driver, pobj.expectedOpeningDate, currentDate);
			fc.utobj().sendKeys(driver,
					fc.utobj().getElementByXpath(driver,
							".//td[contains(text () ,'" + referenceDate + "')]/following-sibling::td[1]//input"),
					currentDate);
			fc.utobj().sendKeys(driver, pobj.streetAddressTextBox, dataSet.get("streetAddress"));
			fc.utobj().sendKeys(driver, pobj.address2TextBox, dataSet.get("address"));
			fc.utobj().sendKeys(driver, pobj.cityTextBox, dataSet.get("city"));
			fc.utobj().selectDropDown(driver, pobj.countryDropdown, "USA");
			fc.utobj().sendKeys(driver, pobj.zipcodeTextBox, "123456");
			fc.utobj().selectDropDown(driver, pobj.stateProvinceDropdown, "Alaska");
			fc.utobj().sendKeys(driver, pobj.PhoneTextBox, "1236547896");
			fc.utobj().sendKeys(driver, pobj.ownerFirstName, "firstName");
			fc.utobj().sendKeys(driver, pobj.ownerLastName, "lastName");
			fc.utobj().clickElement(driver, pobj.addBtn);

			fc.opener().opener_common().openerStoreSummary(driver);
			setDefaultFilterOpener(driver, storeNoFranchiseID);
			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, storeNoFranchiseID));
			fc.utobj().clickElement(driver, pobj.modifyLink);

			String textDate = driver
					.findElement(By
							.xpath(".//td[contains(text () ,'" + referenceDate + "')]/following-sibling::td[1]//input"))
					.getAttribute("value").trim();

			if (!textDate.equalsIgnoreCase(currentDate)) {
				fc.utobj().throwsSkipException("was not able to verify reference date");
			}

			String futureDate = fc.utobj().getFutureDateUSFormat(5);
			fc.utobj().sendKeys(driver,
					fc.utobj().getElementByXpath(driver,
							".//td[contains(text () ,'" + referenceDate + "')]/following-sibling::td[1]//input"),
					futureDate);
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("Verify The Reference Field History");
			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, futureDate));
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			String modificationDate = fc.utobj().getText(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='storeGOD']/tbody/tr[2]/td/table/tbody/tr[2]/td/table/tbody/tr[2]/td[1]"));
			if (!modificationDate.equalsIgnoreCase(currentDate)) {
				fc.utobj().throwsSkipException("was not able to verify modication Date");
			}

			String beforeModificationDate = fc.utobj().getText(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='storeGOD']/tbody/tr[2]/td/table/tbody/tr[2]/td/table/tbody/tr[2]/td[2]"));
			if (!beforeModificationDate.equalsIgnoreCase(currentDate)) {
				fc.utobj().throwsSkipException("was not able to verify Before modifcation Date");
			}

			String afterModficationDate = fc.utobj().getText(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='storeGOD']/tbody/tr[2]/td/table/tbody/tr[2]/td/table/tbody/tr[2]/td[3]"));
			if (!afterModficationDate.equalsIgnoreCase(futureDate)) {
				fc.utobj().throwsSkipException("was not able to verify After modifcation Date");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void actionImgOption(WebDriver driver, String task, String option) throws Exception {

		String alterText = driver
				.findElement(
						By.xpath(".//a[contains(text () , '" + task + "')]/ancestor::tr/td/div[@id='menuBar']/layer"))
				.getAttribute("id").trim();
		alterText = alterText.replace("Actions_dynamicmenu", "");
		alterText = alterText.replace("Bar", "");

		fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
				".//a[contains(text () , '" + task + "')]/ancestor::tr/td/div[@id='menuBar']/layer/a/img"));
		fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
				".//div[@id='Actions_dynamicmenu" + alterText + "Menu']/span[contains(text () , '" + option + "')]"));
	}

	// saveView
	public void setDefaultFilterOpener(WebDriver driver, String storeNoFranchiseID) throws Exception {

		try {
			OpenerStoreSummaryStoreListPage pobj = new OpenerStoreSummaryStoreListPage(driver);

			fc.utobj().clickElement(driver, pobj.showFilter);

			fc.utobj().selectValFromMultiSelect(driver, pobj.storeNumber, storeNoFranchiseID);

			fc.utobj().setToDefault(driver, pobj.countrySelect);
			fc.utobj().setToDefault(driver, pobj.stateSelect);
			fc.utobj().setToDefault(driver, pobj.entitySelect);
			fc.utobj().setToDefault(driver, pobj.ownerNameSelect);
			// fc.utobj().selectDropDown(driver,
			// pobj.expectedOpeningDateDropDown, "All");
			fc.utobj().clickElement(driver, pobj.searchBtn);

			fc.utobj().clickElement(driver, pobj.hideFilter);
		} catch (Exception e) {

		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-11-02", updatedOn = "2017-11-02", testCaseDescription = "Verify the store type is not getting delete when it is associated with store", testCaseId = "TC_Opener_StoreType_002")
	public void verifyTheStoreType() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");
			String storeType = fc.utobj().generateTestData("TestR");
			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");
			String emailId = "openerautomation@staffex.com";
			String regionName = fc.utobj().generateTestData("TestR");
			String storeNoFranchiseID = addNewFranchiseLocation(driver, regionName, storeType, dataSet, config,
					emailId);

			fc.opener().opener_common().openerStoreSummary(driver);
			setDefaultFilterOpener(driver, storeNoFranchiseID);

			fc.utobj().printTestStep("Verify The Add New Franchise Location");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, storeNoFranchiseID);

			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to add new Store");
			}

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.adminpage().adminConfigurationConfigureStoreType(driver);
			fc.utobj().printTestStep("Verify That Store type should not get deleted");

			boolean isPresnt = fc.utobj().selectDropDownByVisibleTextTrimed(driver,
					new AdminConfigurationConfigureStoreTypePage(driver).listing, storeType);
			if (isPresnt == false) {
				String temp = "*";
				temp = temp.concat(storeType);
				fc.utobj().selectDropDownByVisibleTextTrimed(driver,
						new AdminConfigurationConfigureStoreTypePage(driver).listing, temp);
			}
			fc.utobj().clickElement(driver, new AdminConfigurationConfigureStoreTypePage(driver).deleteBtn);

			String isAlertTextPresent = fc.utobj().acceptAlertBox(driver);

			if (!isAlertTextPresent.contains("Store Type Marked with * cannot be deleted")) {
				fc.utobj().throwsException("Not able to verify that Used Store Type in Store should not get deleted");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
}
