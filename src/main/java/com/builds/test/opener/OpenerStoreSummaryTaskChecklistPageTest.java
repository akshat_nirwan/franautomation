package com.builds.test.opener;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminConfigurationConfigureStoreTypePageTest;
import com.builds.test.admin.AdminDivisionAddDivisionPageTest;
import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.common.CorporateUser;
import com.builds.uimaps.opener.OpenerStoreSummaryTaskChecklistPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class OpenerStoreSummaryTaskChecklistPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "opener", "openerfailed" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Complete Task :: Opener > Store Summary >  Task Checklist", testCaseId = "TC_104_Complete_Task01")
	public void completeTaskActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			OpenerStoreSummaryTaskChecklistPage pobj = new OpenerStoreSummaryTaskChecklistPage(driver);
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");

			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String emailId = "openerautomation@staffex.com";
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			AdminOpenerTaskChecklistPageTest addTaskPage = new AdminOpenerTaskChecklistPageTest();
			String fileName = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			String referenceDate = "";

			fc.utobj().printTestStep("Add Task Checklist");
			String task = addTaskPage.addTaskChecklistUploadFile(driver, storeType, fileName, dataSet, referenceDate,
					config);

			String parentWindow = driver.getWindowHandle();

			fc.opener().opener_common().openerStoreSummary(driver);
			addStorePage.setDefaultFilterOpener(driver, storeNoFranchiseID);

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, storeNoFranchiseID));
			fc.utobj().clickElement(driver, pobj.taskChecklistLnk);

			fc.utobj().sendKeys(driver, pobj.taskSearchBx, task);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			fc.utobj().printTestStep("Complete The Task Checklist");
			actionImgOption(driver, task, "Complete");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String curDate = fc.utobj().currentDate();
			fc.utobj().sendKeys(driver, pobj.completionDateTxBx, curDate);
			String comments = fc.utobj().generateTestData(dataSet.get("comments"));
			fc.utobj().sendKeys(driver, pobj.commentsTxBx, comments);
			fc.utobj().clickElement(driver, pobj.completeBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().printTestStep("Verify The Complete Checklist");
			fc.utobj().sendKeys(driver, pobj.taskSearchBx, task);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , 'Complete')]").isDisplayed() && !fc
					.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + curDate + "')]").isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to complete task");
			}

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, task));
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + curDate + "')]").isDisplayed()
					&& !fc.utobj().getElementByXpath(driver, ".//td[contains(text () ,'" + comments + "')]")
							.isDisplayed()
					&& !fc.utobj().getElementByXpath(driver, ".//td[contains(text () , 'Complete')]").isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to verify completion date, comments and status");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add Comment In Task CheckList:: Opener > Store Summary >  Task Checklist", testCaseId = "TC_105_Add_C0mment")
	public void addCommentActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			OpenerStoreSummaryTaskChecklistPage pobj = new OpenerStoreSummaryTaskChecklistPage(driver);
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");
			String emailId = "openerautomation@staffex.com";
			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			AdminOpenerTaskChecklistPageTest addTaskPage = new AdminOpenerTaskChecklistPageTest();
			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			String referenceDate = "";

			fc.utobj().printTestStep("Add Task Checklist");
			String task = addTaskPage.addTaskChecklistUploadFile(driver, storeType, filePath, dataSet, referenceDate,
					config);

			String parentWindow = driver.getWindowHandle();

			fc.opener().opener_common().openerStoreSummary(driver);
			addStorePage.setDefaultFilterOpener(driver, storeNoFranchiseID);
			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, storeNoFranchiseID));
			fc.utobj().clickElement(driver, pobj.taskChecklistLnk);

			fc.utobj().sendKeys(driver, pobj.taskSearchBx, task);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			fc.utobj().printTestStep("Add Comments");
			actionImgOption(driver, task, "Add Comments");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String comments = fc.utobj().generateTestData(dataSet.get("comments"));
			fc.utobj().sendKeys(driver, pobj.commentsTxBx, comments);
			fc.utobj().clickElement(driver, pobj.addBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().printTestStep("Verify The Add Comments");
			fc.utobj().sendKeys(driver, pobj.taskSearchBx, task);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, task));
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + fc.utobj().currentDate() + "')]")
					.isDisplayed()
					&& !fc.utobj().getElementByXpath(driver, ".//td[contains(text () ,'" + comments + "')]")
							.isDisplayed()
					&& !fc.utobj().getElementByXpath(driver, ".//td[contains(text () , 'Pending')]").isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to verify comments date, comments and task status");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Task Status In Progress:: Opener > Store Summary >  Task Checklist", testCaseId = "TC_106_Verify_Task_Status_In_Progress")
	public void verifyTaskStatusInProgress() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			OpenerStoreSummaryTaskChecklistPage pobj = new OpenerStoreSummaryTaskChecklistPage(driver);

			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");

			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String emailId = "openerautomation@staffex.com";
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			AdminOpenerTaskChecklistPageTest addTaskPage = new AdminOpenerTaskChecklistPageTest();
			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			String referenceDate = "";

			fc.utobj().printTestStep("Add Task Checklist");
			String task = addTaskPage.addTaskChecklistUploadFile(driver, storeType, filePath, dataSet, referenceDate,
					config);

			fc.opener().opener_common().openerStoreSummary(driver);

			addStorePage.setDefaultFilterOpener(driver, storeNoFranchiseID);
			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, storeNoFranchiseID));
			fc.utobj().clickElement(driver, pobj.taskChecklistLnk);

			fc.utobj().sendKeys(driver, pobj.taskSearchBx, task);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , 'Pending')]").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Pending status of Task");
			}

			fc.utobj().printTestStep("Mark The Status Of The Task is In Process");
			actionImgOption(driver, task, "In Progress");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().sendKeys(driver, pobj.taskSearchBx, task);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			fc.utobj().printTestStep("Verify The Status of Task Checklist");
			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , 'In Progress')]").isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to verify In Progress Status of task");
			}

			fc.utobj().clickElement(driver, pobj.taskChecklistLnk);

			fc.utobj().selectActionMenuItems(driver, "Configure Default Values");

			fc.utobj().clickElement(driver, pobj.saveBtn);

			if (!fc.utobj().getElementByXpath(driver, ".//*[@id='displayMsg']/td/span").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify default configuration msg");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Modification of Task :: Opener > Store Summary >  Task Checklist ", testCaseId = "TC_107_Modify_Task")
	public void modifyTaskActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Responsible Department");
			fc.utobj().printTestStep("Add Responsible Department");

			AdminOpenerResponsibleDepartmentPageTest addDepartmentPage = new AdminOpenerResponsibleDepartmentPageTest();
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));
			addDepartmentPage.addDepartment(driver, responsibilityArea);

			OpenerStoreSummaryTaskChecklistPage pobj = new OpenerStoreSummaryTaskChecklistPage(driver);

			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");
			String emailId = "openerautomation@staffex.com";
			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			AdminOpenerTaskChecklistPageTest addTaskPage = new AdminOpenerTaskChecklistPageTest();
			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			String referenceDate = "";

			fc.utobj().printTestStep("Add Task Checklist");
			String task = addTaskPage.addTaskChecklistUploadFile(driver, storeType, filePath, dataSet, referenceDate,
					config);

			fc.opener().opener_common().openerStoreSummary(driver);
			addStorePage.setDefaultFilterOpener(driver, storeNoFranchiseID);

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, storeNoFranchiseID));
			fc.utobj().clickElement(driver, pobj.taskChecklistLnk);

			fc.utobj().sendKeys(driver, pobj.taskSearchBx, task);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			fc.utobj().printTestStep("Modify The Task Checklist By Action Image Option");
			actionImgOption(driver, task, "Modify");

			task = fc.utobj().generateTestData(dataSet.get("task"));
			fc.utobj().sendKeys(driver, pobj.taskNameTxtBox, task);

			fc.utobj().selectValFromMultiSelect(driver, pobj.selectResponsibilityArea, responsibilityArea);
			fc.utobj().selectValFromMultiSelect(driver, pobj.selectContact, "Regional User");

			String dependentOn = dataSet.get("dependentOnModify");
			String valueText = fc.utobj()
					.getElementByXpath(driver,
							".//select[@name='smUserTaskChecklist_0referenceParent']/option[contains(text () , '"
									+ dependentOn + "')]")
					.getAttribute("value");
			fc.utobj().moveToElement(driver, pobj.dependentOnDropDown);
			Select sl = new Select(pobj.dependentOnDropDown);
			sl.selectByValue(valueText);

			fc.utobj().sendKeys(driver, pobj.startScheduleDateTxtBox, "1");
			fc.utobj().selectDropDown(driver, pobj.scheduleStartDropDown, "Days Prior");
			fc.utobj().sendKeys(driver, pobj.completionScheduleDateTxtBox, "1");
			fc.utobj().selectDropDown(driver, pobj.scheduleCompletionDropDown, "Days Prior");
			fc.utobj().sendKeys(driver, pobj.startReminderTxtBox, "2");
			fc.utobj().sendKeys(driver, pobj.completionReminderTxtBox, "2");

			if (fc.utobj().isSelected(driver, pobj.webLinkRadioBtn)) {
				// do nothing
			} else {
				fc.utobj().clickElement(driver, pobj.webLinkRadioBtn);
			}

			fc.utobj().sendKeys(driver, pobj.webLinkAddressBx, dataSet.get("webLinkUrl"));

			try {
				List<WebElement> listRadioButton = pobj.applyToAll;

				if (fc.utobj().isSelected(driver,listRadioButton.get(2))) {
					// do nothing
				} else {
					fc.utobj().clickElement(driver, listRadioButton.get(2));
				}

				// select
				fc.utobj().clickElement(driver, pobj.currentOpnerSelectBtn);
				fc.utobj().clickElement(driver, pobj.selectAllOpnerCheck);
			} catch (Exception e) {

			}
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("Verify The Modify Task Checklist");
			fc.utobj().clickElement(driver, pobj.storeInfoLnk);

			if (!fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + task + "')]").isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to verify at Store Info Page");
			}
			fc.utobj().clickElement(driver, pobj.taskChecklistLnk);

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , 'Overdue')]").isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to modify Task Status");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () ,'" + responsibilityArea + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to modify responsibility area");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () ,'Regional User')]").isDisplayed()) {
				fc.utobj().throwsException("was not able to modify Contact");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//a[.='" + task + "']").isDisplayed()) {
				fc.utobj().throwsException("was not able to modify task");
			}

			fc.opener().opener_common().openerStoreSummary(driver);

			addStorePage.setDefaultFilterOpener(driver, storeNoFranchiseID);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='1']"));

			if (!fc.utobj().getElementByXpath(driver, ".//a[.='" + storeNoFranchiseID + "']").isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to verify overdue task page");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Deletion of Task :: Opener > Store Summary >  Task Checklist", testCaseId = "TC_108_Delete_Task")
	public void deleteTaskActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			OpenerStoreSummaryTaskChecklistPage pobj = new OpenerStoreSummaryTaskChecklistPage(driver);

			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");

			String emailId = "openerautomation@staffex.com";
			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			AdminOpenerTaskChecklistPageTest addTaskPage = new AdminOpenerTaskChecklistPageTest();
			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			String referenceDate = "";

			fc.utobj().printTestStep("Add Task Checklist");
			String task = addTaskPage.addTaskChecklistUploadFile(driver, storeType, filePath, dataSet, referenceDate,
					config);

			String parentWindow = driver.getWindowHandle();

			fc.opener().opener_common().openerStoreSummary(driver);
			addStorePage.setDefaultFilterOpener(driver, storeNoFranchiseID);

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, storeNoFranchiseID));
			fc.utobj().clickElement(driver, pobj.taskChecklistLnk);

			fc.utobj().sendKeys(driver, pobj.taskSearchBx, task);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			fc.utobj().printTestStep("Delete Task Checklist By Action Image Option");
			actionImgOption(driver, task, "Delete");
			fc.utobj().acceptAlertBox(driver);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().printTestStep("Verify The Delete Task Checklist");
			// Verify At Store Info Page
			fc.utobj().clickElement(driver, pobj.storeInfoLnk);

			boolean isTextPresentAtStoreInfoPage = fc.utobj().assertPageSource(driver, task);

			if (isTextPresentAtStoreInfoPage == true) {
				fc.utobj().throwsException("was not able to verify deleted task at store info page");
			}

			fc.utobj().clickElement(driver, pobj.taskChecklistLnk);
			fc.utobj().sendKeys(driver, pobj.taskSearchBx, task);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			if (!fc.utobj().getElementByXpath(driver, ".//td[.='No records found.']").isDisplayed()) {
				fc.utobj().throwsException("was not able to delete task at Task Checklist page");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Add Description in TaskCheklist :: Opener > Store Summary >  Task Checklist", testCaseId = "TC_109_Add_Description")
	public void addDescriptionActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			OpenerStoreSummaryTaskChecklistPage pobj = new OpenerStoreSummaryTaskChecklistPage(driver);

			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");

			String emailId = "openerautomation@staffex.com";
			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			AdminOpenerTaskChecklistPageTest addTaskPage = new AdminOpenerTaskChecklistPageTest();
			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			// File file=new File(filePath);
			/*
			 * String
			 * groupName=fc.utobj().generateTestData(dataSet.get("groupName"));
			 */
			String referenceDate = "";

			fc.utobj().printTestStep("Add Task Checklist");
			String task = addTaskPage.addTaskChecklistUploadFile(driver, storeType, filePath, dataSet, referenceDate,
					config);

			String parentWindow = driver.getWindowHandle();

			fc.opener().opener_common().openerStoreSummary(driver);

			addStorePage.setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, storeNoFranchiseID));
			fc.utobj().clickElement(driver, pobj.taskChecklistLnk);

			fc.utobj().sendKeys(driver, pobj.taskSearchBx, task);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			fc.utobj().printTestStep("Add Description");
			actionImgOption(driver, task, "Add Description");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().sendKeys(driver, pobj.descriptionTxBx, description);
			fc.utobj().clickElement(driver, pobj.addBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().sendKeys(driver, pobj.taskSearchBx, task);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			fc.utobj().printTestStep("Verify The Description");
			actionImgOption(driver, task, "Modify Description");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			String isTextPresent = pobj.descriptionTxBx.getText().trim();
			if (!isTextPresent.equalsIgnoreCase(description)) {
				fc.utobj().throwsException("was not able to add description");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Modification of Description of TaskChecklist :: Opener > Store Summary >  Task Checklist", testCaseId = "TC_110_Modify_Description")
	public void modifyDescriptionActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			OpenerStoreSummaryTaskChecklistPage pobj = new OpenerStoreSummaryTaskChecklistPage(driver);

			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");

			String emailId = "openerautomation@staffex.com";
			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			AdminOpenerTaskChecklistPageTest addTaskPage = new AdminOpenerTaskChecklistPageTest();
			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			// File file=new File(filePath);
			/*
			 * String
			 * groupName=fc.utobj().generateTestData(dataSet.get("groupName"));
			 */
			String referenceDate = "";

			fc.utobj().printTestStep("Add Task Checklist");
			String task = addTaskPage.addTaskChecklistUploadFile(driver, storeType, filePath, dataSet, referenceDate,
					config);

			String parentWindow = driver.getWindowHandle();

			fc.opener().opener_common().openerStoreSummary(driver);
			addStorePage.setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, storeNoFranchiseID));
			fc.utobj().clickElement(driver, pobj.taskChecklistLnk);

			fc.utobj().sendKeys(driver, pobj.taskSearchBx, task);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			fc.utobj().printTestStep("Add Description");
			actionImgOption(driver, task, "Add Description");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().sendKeys(driver, pobj.descriptionTxBx, description);
			fc.utobj().clickElement(driver, pobj.addBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().sendKeys(driver, pobj.taskSearchBx, task);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			fc.utobj().printTestStep("Modify The Description");
			actionImgOption(driver, task, "Modify Description");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().sendKeys(driver, pobj.descriptionTxBx, description);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().printTestStep("Verify The Modify Description");
			actionImgOption(driver, task, "Modify Description");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			String isTextPresent = pobj.descriptionTxBx.getText().trim();
			if (!isTextPresent.equalsIgnoreCase(description)) {
				fc.utobj().throwsException("was not able to modify description");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Not Applicable :: Opener > Store Summary >  Task Checklist", testCaseId = "TC_111_Not_Applicable")
	public void verifyNotApplicable() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			OpenerStoreSummaryTaskChecklistPage pobj = new OpenerStoreSummaryTaskChecklistPage(driver);

			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");

			String emailId = "openerautomation@staffex.com";
			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			AdminOpenerTaskChecklistPageTest addTaskPage = new AdminOpenerTaskChecklistPageTest();
			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			// File file=new File(filePath);
			/*
			 * String
			 * groupName=fc.utobj().generateTestData(dataSet.get("groupName"));
			 */
			String referenceDate = "";

			fc.utobj().printTestStep("Add Task Checklist");
			String task = addTaskPage.addTaskChecklistUploadFile(driver, storeType, filePath, dataSet, referenceDate,
					config);

			fc.opener().opener_common().openerStoreSummary(driver);

			addStorePage.setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, storeNoFranchiseID));
			fc.utobj().clickElement(driver, pobj.taskChecklistLnk);

			fc.utobj().sendKeys(driver, pobj.taskSearchBx, task);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			fc.utobj().printTestStep("Mark Task Checklist As A Not Applicable");
			actionImgOption(driver, task, "Not Applicable");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().sendKeys(driver, pobj.taskSearchBx, task);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			fc.utobj().printTestStep("Verify The Task Checklist");
			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , 'Not Applicable')]").isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to verify Not Applicable Status");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Mark As A In Complete Task :: Opener > Store Summary >  Task Checklist", testCaseId = "TC_112_Mark_As_InComplete")
	public void verifyMarkAsInCompleteActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			OpenerStoreSummaryTaskChecklistPage pobj = new OpenerStoreSummaryTaskChecklistPage(driver);

			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");

			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String emailId = "openerautomation@staffex.com";
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			AdminOpenerTaskChecklistPageTest addTaskPage = new AdminOpenerTaskChecklistPageTest();
			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			// File file=new File(filePath);
			/*
			 * String
			 * groupName=fc.utobj().generateTestData(dataSet.get("groupName"));
			 */
			String referenceDate = "";

			fc.utobj().printTestStep("Add Task Checklist");
			String task = addTaskPage.addTaskChecklistUploadFile(driver, storeType, filePath, dataSet, referenceDate,
					config);

			String parentWindow = driver.getWindowHandle();

			fc.opener().opener_common().openerStoreSummary(driver);
			addStorePage.setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, storeNoFranchiseID));
			fc.utobj().clickElement(driver, pobj.taskChecklistLnk);

			fc.utobj().sendKeys(driver, pobj.taskSearchBx, task);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			fc.utobj().printTestStep("Complete Task Checklist");
			actionImgOption(driver, task, "Complete");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String curDate = fc.utobj().currentDate();
			fc.utobj().sendKeys(driver, pobj.completionDateTxBx, curDate);
			// fc.utobj().clickElement(driver, pobj.calanderClose);
			String comments = fc.utobj().generateTestData(dataSet.get("comments"));
			fc.utobj().sendKeys(driver, pobj.commentsTxBx, comments);
			fc.utobj().clickElement(driver, pobj.completeBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().printTestStep("Mark As A Incomplete");
			fc.utobj().sendKeys(driver, pobj.taskSearchBx, task);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			actionImgOption(driver, task, "Mark as Incomplete");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Task Checklist");
			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () ,'Pending')]").isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to verify Mark As InComplete status");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the View OverDue Task :: Opener > Store Summary >  Task Checklist", testCaseId = "TC_113_View_OverDue_Task")
	public void viewOverdueTaskActionsBtn() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			OpenerStoreSummaryTaskChecklistPage pobj = new OpenerStoreSummaryTaskChecklistPage(driver);

			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");

			String emailId = "openerautomation@staffex.com";
			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			AdminOpenerTaskChecklistPageTest addTaskPage = new AdminOpenerTaskChecklistPageTest();
			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			// File file=new File(filePath);
			/*
			 * String
			 * groupName=fc.utobj().generateTestData(dataSet.get("groupName"));
			 */
			String referenceDate = "";

			fc.utobj().printTestStep("Add Task Checklist");
			String task = addTaskPage.addTaskChecklistUploadFile(driver, storeType, filePath, dataSet, referenceDate,
					config);

			fc.opener().opener_common().openerStoreSummary(driver);
			addStorePage.setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, storeNoFranchiseID));
			fc.utobj().clickElement(driver, pobj.taskChecklistLnk);

			fc.utobj().sendKeys(driver, pobj.taskSearchBx, task);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[.='" + task + "']/ancestor::tr/td/input"));

			fc.utobj().printTestStep("Verify The Overdue Task");
			fc.utobj().moveToElementThroughAction(driver, pobj.actionsBtn);
			fc.utobj().clickElement(driver, pobj.overdueTaskLink);

			if (!fc.utobj().getElementByXpath(driver, ".//tr/td[contains(text () , 'Overdue')]").isDisplayed()
					&& !fc.utobj().getElementByXpath(driver, ".//a[.='" + task + "']").isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to view overdue task");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Complete Task At ActionBtn :: Opener > Store Summary >  Task Checklist", testCaseId = "TC_114_Complete_Task")
	public void completeTaskActionBtn() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			OpenerStoreSummaryTaskChecklistPage pobj = new OpenerStoreSummaryTaskChecklistPage(driver);

			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");

			String emailId = "openerautomation@staffex.com";
			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			AdminOpenerTaskChecklistPageTest addTaskPage = new AdminOpenerTaskChecklistPageTest();
			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			/*
			 * String
			 * groupName=fc.utobj().generateTestData(dataSet.get("groupName"));
			 */
			String referenceDate = "";

			fc.utobj().printTestStep("Add Task Checklist");
			String task = addTaskPage.addTaskChecklistUploadFile(driver, storeType, filePath, dataSet, referenceDate,
					config);

			String parentWindow = driver.getWindowHandle();

			fc.opener().opener_common().openerStoreSummary(driver);
			addStorePage.setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, storeNoFranchiseID));
			fc.utobj().clickElement(driver, pobj.taskChecklistLnk);

			fc.utobj().sendKeys(driver, pobj.taskSearchBx, task);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[.='" + task + "']/ancestor::tr/td/input"));

			fc.utobj().printTestStep("Complete The Task Checklist");
			fc.utobj().selectActionMenuItems(driver, "Complete");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String curDate = fc.utobj().currentDate();
			fc.utobj().sendKeys(driver, pobj.completionDateTxBx, curDate);
			// fc.utobj().clickElement(driver, pobj.calanderClose);
			String comments = fc.utobj().generateTestData(dataSet.get("comments"));
			fc.utobj().sendKeys(driver, pobj.commentsTxBx, comments);
			fc.utobj().clickElement(driver, pobj.completeBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().sendKeys(driver, pobj.taskSearchBx, task);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			fc.utobj().printTestStep("Verify The Complete Task Checklist");
			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , 'Complete')]").isDisplayed() && !fc
					.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + curDate + "')]").isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to complete task");
			}

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, task));
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + curDate + "')]").isDisplayed()
					&& !fc.utobj().getElementByXpath(driver, ".//td[contains(text () ,'" + comments + "')]")
							.isDisplayed()
					&& !fc.utobj().getElementByXpath(driver, ".//td[contains(text () , 'Complete')]").isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to verify completion date, comments and status");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Deletion of Task :: Opener > Store Summary >  Task Checklist", testCaseId = "TC_115_Delete_Task")
	public void deleteTaskActionBtn() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			OpenerStoreSummaryTaskChecklistPage pobj = new OpenerStoreSummaryTaskChecklistPage(driver);

			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");

			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String emailId = "openerautomation@staffex.com";
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			AdminOpenerTaskChecklistPageTest addTaskPage = new AdminOpenerTaskChecklistPageTest();
			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			String referenceDate = "";

			fc.utobj().printTestStep("Add Task Checklist");
			String task = addTaskPage.addTaskChecklistUploadFile(driver, storeType, filePath, dataSet, referenceDate,
					config);

			String parentWindow = driver.getWindowHandle();

			fc.opener().opener_common().openerStoreSummary(driver);
			addStorePage.setDefaultFilterOpener(driver, storeNoFranchiseID);

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, storeNoFranchiseID));
			fc.utobj().clickElement(driver, pobj.taskChecklistLnk);

			fc.utobj().sendKeys(driver, pobj.taskSearchBx, task);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[.='" + task + "']/ancestor::tr/td/input"));

			fc.utobj().printTestStep("Delete The Task Checklist By Action Image Option");
			fc.utobj().selectActionMenuItems(driver, "Delete");

			fc.utobj().acceptAlertBox(driver);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().printTestStep("Verify The Complete Task Checklist");
			// Verify At Store Info Page
			fc.utobj().clickElement(driver, pobj.storeInfoLnk);

			boolean isTextPresentAtStoreInfoPage = fc.utobj().assertPageSource(driver, task);

			if (isTextPresentAtStoreInfoPage == true) {
				fc.utobj().throwsException("was not able to verify deleted task at store info page");
			}

			fc.utobj().clickElement(driver, pobj.taskChecklistLnk);
			fc.utobj().sendKeys(driver, pobj.taskSearchBx, task);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			if (!fc.utobj().getElementByXpath(driver, ".//td[.='No records found.']").isDisplayed()) {
				fc.utobj().throwsException("was not able to delete task at Task Checklist page");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify modification of Contact of Task :: Opener > Store Summary >  Task Checklist", testCaseId = "TC_116_Modify_Contacts")
	public void modifyContactActionBtn() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			OpenerStoreSummaryTaskChecklistPage pobj = new OpenerStoreSummaryTaskChecklistPage(driver);

			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");

			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String emailId = "openerautomation@staffex.com";
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			AdminOpenerTaskChecklistPageTest addTaskPage = new AdminOpenerTaskChecklistPageTest();
			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			// File file=new File(filePath);
			/*
			 * String
			 * groupName=fc.utobj().generateTestData(dataSet.get("groupName"));
			 */
			String referenceDate = "";

			fc.utobj().printTestStep("Add Task Checklist");
			String task = addTaskPage.addTaskChecklistUploadFile(driver, storeType, filePath, dataSet, referenceDate,
					config);

			String parentWindow = driver.getWindowHandle();

			fc.opener().opener_common().openerStoreSummary(driver);
			addStorePage.setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, storeNoFranchiseID));
			fc.utobj().clickElement(driver, pobj.taskChecklistLnk);

			fc.utobj().sendKeys(driver, pobj.taskSearchBx, task);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			fc.utobj().printTestStep("Modify The Contact Of Task Checklist");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[.='" + task + "']/ancestor::tr/td/input"));

			fc.utobj().selectActionMenuItems(driver, "Modify Contact(s)");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.modifyContactSelectBtn);
			fc.utobj().clickElement(driver, pobj.modifyContactSelectAll);
			fc.utobj().clickElement(driver, pobj.modifyContactSelectAll);
			fc.utobj().sendKeys(driver, pobj.modifyContactSearchBx, "Regional User");
			fc.utobj().clickElement(driver, pobj.modifyContactSelectAll);
			fc.utobj().clickElement(driver, pobj.modifyContactSelectBtn);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().sendKeys(driver, pobj.taskSearchBx, task);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			if (!driver
					.findElement(
							By.xpath(".//a[.='" + task + "']/ancestor::tr/td[contains(text () , 'Regional User')]"))
					.isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to modify Contact(s)");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Group View of Task :: Opener > Store Summary >  Task Checklist", testCaseId = "TC_117_Group_View")
	public void groupViewActionBtn() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			OpenerStoreSummaryTaskChecklistPage pobj = new OpenerStoreSummaryTaskChecklistPage(driver);

			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");

			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String emailId = "openerautomation@staffex.com";
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			AdminOpenerTaskChecklistPageTest addTaskPage = new AdminOpenerTaskChecklistPageTest();
			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			File file = new File(filePath);
			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			String referenceDate = "";

			fc.utobj().printTestStep("Add Task Checklist");
			String task = addTaskPage.addTaskChecklistUploadFileWithGroup(driver, storeType, file, dataSet, groupName,
					referenceDate, config);

			fc.opener().opener_common().openerStoreSummary(driver);
			addStorePage.setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, storeNoFranchiseID));
			fc.utobj().clickElement(driver, pobj.taskChecklistLnk);

			fc.utobj().sendKeys(driver, pobj.taskSearchBx, task);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			fc.utobj().printTestStep("Make Group View Of Task Checklist By Action Button Option");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[.='" + task + "']/ancestor::tr/td/input"));

			fc.utobj().selectActionMenuItems(driver, "Group view");

			fc.utobj().printTestStep("Verify The Group View");
			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + groupName + "')]").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify group view");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Mark As A In Complete Task :: Opener > Store Summary >  Task Checklist", testCaseId = "TC_118_Complete_Task")
	public void completeTaskBottomBtn() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			OpenerStoreSummaryTaskChecklistPage pobj = new OpenerStoreSummaryTaskChecklistPage(driver);

			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");

			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String emailId = "openerautomation@staffex.com";
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			AdminOpenerTaskChecklistPageTest addTaskPage = new AdminOpenerTaskChecklistPageTest();
			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			/*
			 * String
			 * groupName=fc.utobj().generateTestData(dataSet.get("groupName"));
			 */
			String referenceDate = "";

			fc.utobj().printTestStep("Add Task Checklist");
			String task = addTaskPage.addTaskChecklistUploadFile(driver, storeType, filePath, dataSet, referenceDate,
					config);

			String parentWindow = driver.getWindowHandle();

			fc.opener().opener_common().openerStoreSummary(driver);
			addStorePage.setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, storeNoFranchiseID));
			fc.utobj().clickElement(driver, pobj.taskChecklistLnk);

			fc.utobj().sendKeys(driver, pobj.taskSearchBx, task);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			fc.utobj().printTestStep("Complete Task Checklist By Bottom Button Option");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[.='" + task + "']/ancestor::tr/td/input"));
			fc.utobj().clickElement(driver, pobj.completeBtn);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String curDate = fc.utobj().currentDate();
			fc.utobj().sendKeys(driver, pobj.completionDateTxBx, curDate);
			// fc.utobj().clickElement(driver, pobj.calanderClose);
			String comments = fc.utobj().generateTestData(dataSet.get("comments"));
			fc.utobj().sendKeys(driver, pobj.commentsTxBx, comments);
			fc.utobj().clickElement(driver, pobj.completeBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().sendKeys(driver, pobj.taskSearchBx, task);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			fc.utobj().printTestStep("Verify The Complete Task Checklist");

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , 'Complete')]").isDisplayed() && !fc
					.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + curDate + "')]").isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to complete task");
			}

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, task));
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + curDate + "')]").isDisplayed()
					&& !fc.utobj().getElementByXpath(driver, ".//td[contains(text () ,'" + comments + "')]")
							.isDisplayed()
					&& !fc.utobj().getElementByXpath(driver, ".//td[contains(text () , 'Complete')]").isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to verify completion date, comments and status");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Deletion of Task at Bottom Btn:: Opener > Store Summary >  Task Checklist ", testCaseId = "TC_119_Delete_Task")
	public void deleteTaskBottomBtn() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			OpenerStoreSummaryTaskChecklistPage pobj = new OpenerStoreSummaryTaskChecklistPage(driver);

			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");

			String emailId = "openerautomation@staffex.com";
			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			AdminOpenerTaskChecklistPageTest addTaskPage = new AdminOpenerTaskChecklistPageTest();
			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			// File file=new File(filePath);
			String referenceDate = "";
			fc.utobj().printTestStep("Add Task Checklist");
			String task = addTaskPage.addTaskChecklistUploadFile(driver, storeType, filePath, dataSet, referenceDate,
					config);

			String parentWindow = driver.getWindowHandle();

			fc.opener().opener_common().openerStoreSummary(driver);
			addStorePage.setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, storeNoFranchiseID));
			fc.utobj().clickElement(driver, pobj.taskChecklistLnk);

			fc.utobj().sendKeys(driver, pobj.taskSearchBx, task);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			fc.utobj().printTestStep("Delete Task Cheklist By Bottom Button Option ");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[.='" + task + "']/ancestor::tr/td/input"));
			fc.utobj().clickElement(driver, pobj.bottomDeleteBtn);

			fc.utobj().acceptAlertBox(driver);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().printTestStep("Verify The Delete Task Checklist");
			// Verify At Store Info Page
			fc.utobj().clickElement(driver, pobj.storeInfoLnk);

			boolean isTextPresentAtStoreInfoPage = fc.utobj().assertPageSource(driver, task);

			if (isTextPresentAtStoreInfoPage == true) {
				fc.utobj().throwsException("was not able to verify deleted task at store info page");
			}

			fc.utobj().clickElement(driver, pobj.taskChecklistLnk);
			fc.utobj().sendKeys(driver, pobj.taskSearchBx, task);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			if (!fc.utobj().getElementByXpath(driver, ".//td[.='No records found.']").isDisplayed()) {
				fc.utobj().throwsException("was not able to delete task at Task Checklist page");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Modification of Contact of Task At bottom Btn:: Opener > Store Summary >  Task Checklist", testCaseId = "TC_120_Modify_Contact")
	public void modifyContactBottomBtn() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			OpenerStoreSummaryTaskChecklistPage pobj = new OpenerStoreSummaryTaskChecklistPage(driver);

			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");

			String emailId = "openerautomation@staffex.com";
			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			AdminOpenerTaskChecklistPageTest addTaskPage = new AdminOpenerTaskChecklistPageTest();
			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			// File file=new File(filePath);
			/*
			 * String
			 * groupName=fc.utobj().generateTestData(dataSet.get("groupName"));
			 */
			String referenceDate = "";

			fc.utobj().printTestStep("Add Task Checklist");
			String task = addTaskPage.addTaskChecklistUploadFile(driver, storeType, filePath, dataSet, referenceDate,
					config);

			String parentWindow = driver.getWindowHandle();

			fc.opener().opener_common().openerStoreSummary(driver);
			addStorePage.setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, storeNoFranchiseID));
			fc.utobj().clickElement(driver, pobj.taskChecklistLnk);

			fc.utobj().sendKeys(driver, pobj.taskSearchBx, task);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			fc.utobj().printTestStep("Modify Task Checklist Contact By Bottom Button");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[.='" + task + "']/ancestor::tr/td/input"));
			fc.utobj().clickElement(driver, pobj.modifyContactBoottomBtn);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.modifyContactSelectBtn);
			fc.utobj().clickElement(driver, pobj.modifyContactSelectAll);
			fc.utobj().clickElement(driver, pobj.modifyContactSelectAll);
			fc.utobj().sendKeys(driver, pobj.modifyContactSearchBx, "Division");
			fc.utobj().clickElement(driver, pobj.modifyContactSelectAll);
			fc.utobj().clickElement(driver, pobj.modifyContactSelectBtn);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().printTestStep("Verify The Task Checklist Modify Contact");
			fc.utobj().sendKeys(driver, pobj.taskSearchBx, task);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			if (!driver
					.findElement(
							By.xpath(".//a[.='" + task + "']/ancestor::tr/td[contains(text () , 'Divisional User')]"))
					.isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to modify Contact(s)");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "opener" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-30", testCaseDescription = "Verify the Add More Task Link:: Opener > Store Summary >  Task Checklist", testCaseId = "TC_121_Add_More_Link")
	public void addMoreLink() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Responsible Department");
			fc.utobj().printTestStep("Add Responsible Department");

			AdminOpenerResponsibleDepartmentPageTest addDepartmentPage = new AdminOpenerResponsibleDepartmentPageTest();
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));
			addDepartmentPage.addDepartment(driver, responsibilityArea);

			OpenerStoreSummaryTaskChecklistPage pobj = new OpenerStoreSummaryTaskChecklistPage(driver);

			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));

			fc.utobj().printTestStep("Navigate To Admin > Opener > Groups");
			fc.utobj().printTestStep("Add Group");

			AdminOpenerManageGroupsPageTest addGroupsPage = new AdminOpenerManageGroupsPageTest();
			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			addGroupsPage.addGroups(driver, groupName);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");

			String emailId = "openerautomation@staffex.com";
			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			fc.utobj().printTestStep("Add Task Checklist By Add More Button Option");
			fc.opener().opener_common().openerStoreSummary(driver);
			addStorePage.setDefaultFilterOpener(driver, storeNoFranchiseID);

			fc.utobj().printTestStep("Configure Default Value");

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, storeNoFranchiseID));
			fc.utobj().clickElement(driver, pobj.taskChecklistLnk);
			fc.utobj().clickElement(driver, pobj.addMoreLink);
			String task = fc.utobj().generateTestData(dataSet.get("task"));
			fc.utobj().sendKeys(driver, pobj.taskNameTxtBox, task);
			fc.utobj().selectValFromMultiSelect(driver, pobj.selectResponsibilityArea, responsibilityArea);
			fc.utobj().selectValFromMultiSelect(driver, pobj.selectContact, "Division");
			fc.utobj().selectDropDown(driver, pobj.groupDropDown, groupName);

			String franchiseeAccess = dataSet.get("franchiseeAccess");
			fc.utobj().selectDropDown(driver, pobj.franchiseeAccessDropDown, franchiseeAccess);

			String dependentOn = dataSet.get("dependentOn");
			String valueText = fc.utobj()
					.getElementByXpath(driver,
							".//select[@name='smUserTaskChecklist_0referenceParent']/option[contains(text () , '"
									+ dependentOn + "')]")
					.getAttribute("value");
			fc.utobj().moveToElement(driver, pobj.dependentOnDropDown);
			Select sl = new Select(pobj.dependentOnDropDown);
			sl.selectByValue(valueText);

			if (fc.utobj().isSelected(driver,pobj.uploadFileRadioBtn)) {
			} else {
				fc.utobj().clickElement(driver, pobj.uploadFileRadioBtn);
			}

			String fileName = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			fc.utobj().sendKeys(driver, pobj.attachmentBrowseBox, fileName);
			fc.utobj().clickElement(driver, pobj.addBtn);

			try {
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().clickElement(driver, pobj.noBtn);
				driver.switchTo().defaultContent();
			} catch (Exception e) {

			}

			fc.utobj().printTestStep("Verify The Add Task Checklist");
			fc.utobj().sendKeys(driver, pobj.taskSearchBx, task);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , 'Divisional Users')]").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify user");
			}

			boolean isTextPresent = fc.utobj().assertPageSource(driver, task);

			if (isTextPresent == false) {
				fc.utobj().throwsSkipException("was not able add task checklist");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/*
	 * @Test(groups="opener1") public void verifyDependencyEndDateOf() throws
	 * Exception {
	 * 
	 * Reporter.log(
	 * "TC_219_Verify_Dependency_End_Date_Of : Verify Dependency of Task with option End Date of:: Opener > Store Summary >  Task Checklist .\n"
	 * ); Reporter.log(
	 * "###########################################################################################"
	 * );
	 * 
	 * String testCaseId = "TC_219_Verify_Dependency_End_Date_Of";
	 * 
	 * Map<String,String> config =
	 * fc.utobj().readConfigurationFile(FranconnectUtil.path);
	 * Map<String,String> dataSet = fc.utobj().readTestData("opener",
	 * testCaseId); WebDriver driver = fc.utobj().openDriver(config);
	 * 
	 * 
	 * try{ driver=fc.loginpage().login(driver, config);
	 * 
	 * 
	 * AdminFOResponsibleDepartmentPageTest addDepartmentPage=new
	 * AdminFOResponsibleDepartmentPageTest(); String
	 * responsibilityArea=fc.utobj().generateTestData(dataSet.get(
	 * "responsibilityArea")); addDepartmentPage.addDepartment(driver,
	 * responsibilityArea);
	 * 
	 * OpenerStoreSummaryTaskChecklistPage pobj=new
	 * OpenerStoreSummaryTaskChecklistPage(driver);
	 * 
	 * String regionName=fc.utobj().generateTestData(dataSet.get("regionName"));
	 * 
	 * String storeType=fc.utobj().generateTestData(dataSet.get("storeType"));
	 * AdminConfigurationConfigureStoreTypePageTest storeTypePage=new
	 * AdminConfigurationConfigureStoreTypePageTest();
	 * storeTypePage.addStoreType(driver, storeType);
	 * 
	 * OpenerStoreSummaryStoreListPageTest addStorePage=new
	 * OpenerStoreSummaryStoreListPageTest(); String
	 * storeNoFranchiseID=addStorePage.addNewFranchiseLocation(driver,
	 * divisionName, regionName, storeType,dataSet);
	 * 
	 * AdminFOTaskChecklistPageTest addTaskPage=new
	 * AdminFOTaskChecklistPageTest(); File file = new
	 * File(config.get("testDataPath").concat("/").concat("document").concat("/"
	 * )+dataSet.get("uploadFile")); String
	 * groupName=fc.utobj().generateTestData(dataSet.get("groupName")); String
	 * referenceDate=""; String
	 * task1=addTaskPage.addTaskChecklistUploadFile(driver, storeType,
	 * file.getAbsoluteFile(), dataSet, groupName, referenceDate);
	 * 
	 * fc.nav().openerStoreSummary(driver); fc.utobj().sendKeys(driver,
	 * pobj.searchTextBx, storeNoFranchiseID); fc.utobj().clickElement(driver,
	 * pobj.searchImgBtn); fc.utobj().clickElement(driver,
	 * fc.utobj().getElementByLinkText(driver,storeNoFranchiseID)));
	 * fc.utobj().clickElement(driver, pobj.taskChecklistLnk);
	 * 
	 * fc.utobj().clickElement(driver, pobj.addMoreLink);
	 * 
	 * String task=fc.utobj().generateTestData(dataSet.get("task"));
	 * fc.utobj().sendKeys(driver,pobj.taskNameTxtBox, task);
	 * 
	 * fc.utobj().clickElement(driver, pobj.responsibilityAreaSelctBtn);
	 * fc.utobj().sendKeys(driver, pobj.responsibilityAreaTextBx,
	 * responsibilityArea); fc.utobj().clickElement(driver,
	 * pobj.responsibilityAreaSelectAllBtn); fc.utobj().clickElement(driver,
	 * pobj.responsibilityAreaSelctBtn);
	 * 
	 * fc.utobj().clickElement(driver, pobj.ContactSelectBtn);
	 * fc.utobj().sendKeys(driver, pobj.contactTextBox, "Division");
	 * fc.utobj().clickElement(driver, pobj.contactSelectAll);
	 * 
	 * fc.utobj().selectDropDown(driver, pobj.groupDropDown, groupName);
	 * 
	 * String franchiseeAccess=dataSet.get("franchiseeAccess");
	 * fc.utobj().selectDropDown(driver,pobj.franchiseeAccessDropDown,
	 * franchiseeAccess);
	 * 
	 * String dependentOn=dataSet.get("dependentOn1"); String
	 * valueText=fc.utobj().getElementByXpath(driver,
	 * ".//select[@name='smUserTaskChecklist_0referenceParent']/option[contains(text () , '"
	 * +dependentOn+"')]")).getAttribute("value");
	 * fc.utobj().moveToElement(driver, pobj.dependentOnDropDown); Select sl=new
	 * Select(pobj.dependentOnDropDown); sl.selectByValue(valueText);
	 * 
	 * fc.utobj().selectDropDownByPartialText(driver, pobj.dependentOnDropDown,
	 * "Task");
	 * 
	 * //by value fc.utobj().selectDropDown(driver,
	 * fc.utobj().getElementByID(driver,"smUserTaskChecklist_0referenceFlag")),
	 * "End Date Of"); fc.utobj().selectDropDown(driver,
	 * fc.utobj().getElementByID(driver,
	 * "smUserTaskChecklist_0referenceField_temp")), task1);
	 * fc.utobj().clickElement(driver, fc.utobj().getElementByID(driver,
	 * "smUserTaskChecklist_0dependencyFlag_temp")));
	 * 
	 * fc.utobj().sendKeys(driver, pobj.startScheduleDateTxtBox, "1");
	 * fc.utobj().sendKeys(driver, pobj.completionScheduleDateTxtBox, "1");
	 * fc.utobj().sendKeys(driver, pobj.startReminderTxtBox, "2");
	 * fc.utobj().sendKeys(driver, pobj.completionReminderTxtBox, "2");
	 * fc.utobj().selectDropDown(driver, pobj.scheduleStartDropDown,
	 * "Days Prior"); fc.utobj().selectDropDown(driver,
	 * pobj.scheduleCompletionDropDown, "Days Prior");
	 * 
	 * 
	 * if(fc.utobj().getElement(driver,pobj.uploadFileRadioBtn).
	 * isSelected()){ // do nothing }else{ fc.utobj().clickElement(driver,
	 * pobj.uploadFileRadioBtn); }
	 * 
	 * File file = new
	 * File(config.get("testDataPath").concat("/").concat("document").concat("/"
	 * )+dataSet.get("uploadFile")); System.out.println(file.getAbsolutePath());
	 * fc.utobj().sendKeys(driver, pobj.attachmentBrowseBox,
	 * file.getAbsolutePath()); fc.utobj().clickElement(driver, pobj.addBtn);
	 * 
	 * try{ fc.commonMethods().switch_cboxIframe_frameId(driver);
	 * fc.utobj().clickElement(driver, pobj.noBtn);
	 * driver.switchTo().defaultContent(); }catch(Exception e){
	 * 
	 * }
	 * 
	 * fc.utobj().quitBrowser(driver, config); }catch(Exception e){
	 * 
	 * fc.utobj().quitBrowserOnCatch(driver, config, e); } }
	 */

	@Test(groups = { "opener", "openerfailed" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify mail after create task :: Opener > Store Summary >  Task Checklist", testCaseId = "TC_220_Verify_Task_Creation_Mail")
	public void verifyTaskCreationMail() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Responsible Department");
			fc.utobj().printTestStep("Add Responsible Department");

			AdminOpenerResponsibleDepartmentPageTest addDepartmentPage = new AdminOpenerResponsibleDepartmentPageTest();
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));
			addDepartmentPage.addDepartment(driver, responsibilityArea);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			/*
			 * String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String password=dataSet.get("password");
			 * corporatePage.addCorporateUser(driver, userName, password,
			 * config);
			 */
			String emailId = "openerautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporatePage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			OpenerStoreSummaryTaskChecklistPage pobj = new OpenerStoreSummaryTaskChecklistPage(driver);

			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));

			fc.utobj().printTestStep("Navigate To Admin > Opener > Groups");
			fc.utobj().printTestStep("Add Group");
			AdminOpenerManageGroupsPageTest addGroupsPage = new AdminOpenerManageGroupsPageTest();
			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			addGroupsPage.addGroups(driver, groupName);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");

			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			fc.utobj().printTestStep("Add Task Checklist");
			fc.opener().opener_common().openerStoreSummary(driver);
			addStorePage.setDefaultFilterOpener(driver, storeNoFranchiseID);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchTextBx,
			 * storeNoFranchiseID); fc.utobj().clickElement(driver,
			 * pobj.searchImgBtn);
			 */

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, storeNoFranchiseID));
			fc.utobj().clickElement(driver, pobj.taskChecklistLnk);
			fc.utobj().clickElement(driver, pobj.addMoreLink);

			String task = fc.utobj().generateTestData(dataSet.get("task"));
			fc.utobj().sendKeys(driver, pobj.taskNameTxtBox, task);
			fc.utobj().selectValFromMultiSelect(driver, pobj.selectResponsibilityArea, responsibilityArea);
			fc.utobj().selectValFromMultiSelect(driver, pobj.selectContact, corpUser.getuserFullName());
			fc.utobj().selectDropDown(driver, pobj.groupDropDown, groupName);

			String franchiseeAccess = dataSet.get("franchiseeAccess");
			fc.utobj().selectDropDown(driver, pobj.franchiseeAccessDropDown, franchiseeAccess);

			String dependentOn = dataSet.get("dependentOn");
			String valueText = fc.utobj()
					.getElementByXpath(driver,
							".//select[@name='smUserTaskChecklist_0referenceParent']/option[contains(text () , '"
									+ dependentOn + "')]")
					.getAttribute("value");
			fc.utobj().moveToElement(driver, pobj.dependentOnDropDown);
			Select sl = new Select(pobj.dependentOnDropDown);
			sl.selectByValue(valueText);

			if (fc.utobj().isSelected(driver,pobj.uploadFileRadioBtn)) {
			} else {
				fc.utobj().clickElement(driver, pobj.uploadFileRadioBtn);
			}

			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			File file = new File(filePath);
			System.out.println(file.getAbsolutePath());
			fc.utobj().sendKeys(driver, pobj.attachmentBrowseBox, file.getAbsolutePath());
			fc.utobj().clickElement(driver, pobj.addBtn);

			try {
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().clickElement(driver, pobj.noBtn);
				driver.switchTo().defaultContent();
			} catch (Exception e) {

			}

			fc.utobj().printTestStep("Verify The Task Checklist Creation mail");

			String expectedSubject = "Checklist Item Creation Alert";
			;
			String expectedMessageBody = task;
			Map<String, String> mailData = fc.utobj().readMailBox(expectedSubject, expectedMessageBody, emailId,
					"sdg@1a@Hfs");

			if (mailData.size() == 0 || !mailData.get("mailBody").contains(task)) {
				fc.utobj().throwsException("was not able to verify task at mail");
			}

			if (mailData.size() == 0 || !mailData.get("mailBody").contains(corpUser.getuserFullName())) {
				fc.utobj().throwsException("was not able to verify userName");
			}

			if (mailData.size() == 0 || !mailData.get("mailBody").contains(storeNoFranchiseID)) {
				fc.utobj().throwsException("was not able to verify Store number");
			}

			if (mailData.size() == 0 || !mailData.get("mailBody").contains(fc.utobj().getCurrentDateUSFormat())) {
				fc.utobj().throwsException("was not able to verify Expected Opening Date");
			}

			/*
			 * //Verify Mail For Consultant
			 * 
			 * String host = "192.168.9.2";// change accordingly String
			 * mailStoreType = "pop3"; String user =
			 * "openerautomation@staffex.com";// change accordingly String
			 * password1 = "sdg@1a@Hfs";// change accordingly String fromEmail =
			 * "openerautomation@staffex.com"; String returnPath =
			 * "rohit.kant@franconnect.net"; String[] emailRecepient =
			 * {"rohit.kant@franconnect.net","inzamam.haq@franconnect.net"};
			 * String mailBody = task; String subject=
			 * "Checklist Item Creation Alert"; String[] attachmentName =
			 * {"attachment1.txt"};
			 * 
			 * ReadLocalEmail readEmail=new ReadLocalEmail(); Map<String,
			 * String> mailData=readEmail.emailReceivedValidator(host,
			 * mailStoreType, user, password1, fromEmail, returnPath,
			 * emailRecepient, subject, mailBody, attachmentName);
			 * 
			 * if(mailData!=null){
			 * 
			 * if (!mailData.get("EmailBody").contains(task)) {
			 * fc.utobj().throwsException("was not able to verify task at mail"
			 * ); }
			 * 
			 * if
			 * (!mailData.get("EmailBody").contains(corpUser.getuserFullName()
			 * )) { fc.utobj().throwsException("was not able to verify userName"
			 * ); }
			 * 
			 * if (!mailData.get("EmailBody").contains(storeNoFranchiseID)) {
			 * fc.utobj().throwsException("was not able to verify Store number"
			 * ); }
			 * 
			 * if (!mailData.get("EmailBody").contains(fc.utobj().
			 * getCurrentDateUSFormat())) { fc.utobj().throwsException(
			 * "was not able to verify Expected Opening Date"); }
			 * 
			 * }
			 */

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "opener" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify mail after delete the task :: Opener > Store Summary >  Task Checklist .", testCaseId = "TC_221_Verify_Task_Deletion_Mail")
	public void verifyTaskDeletionMail() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			OpenerStoreSummaryTaskChecklistPage pobj = new OpenerStoreSummaryTaskChecklistPage(driver);
			String divisionName = fc.utobj().generateTestData(dataSet.get("divisionName"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To  Admin > Division > Manage Division");
			fc.utobj().printTestStep("Add Division");

			AdminDivisionAddDivisionPageTest divisionPage = new AdminDivisionAddDivisionPageTest();
			divisionPage.addDivision(driver, divisionName);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");

			String emailId = "openerautomation@staffex.com";
			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			AdminOpenerTaskChecklistPageTest addTaskPage = new AdminOpenerTaskChecklistPageTest();
			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			File file = new File(filePath);

			String referenceDate = "";

			fc.utobj().printTestStep("Add Task Checklist");
			String userName = fc.utobj().generateTestData(dataSet.get("userName"));
			String task = addTaskPage.addTaskChecklistUploadFile(driver, storeType, file, dataSet, referenceDate,
					userName, divisionName, config);

			String parentWindow = driver.getWindowHandle();

			fc.opener().opener_common().openerStoreSummary(driver);
			addStorePage.setDefaultFilterOpener(driver, storeNoFranchiseID);

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, storeNoFranchiseID));
			fc.utobj().clickElement(driver, pobj.taskChecklistLnk);

			fc.utobj().sendKeys(driver, pobj.taskSearchBx, task);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			fc.utobj().printTestStep("Delete The Task By Action Image Icon");

			actionImgOption(driver, task, "Delete");
			fc.utobj().acceptAlertBox(driver);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().printTestStep("Verify The Task Detail In Mail");

			String expectedSubject = "Checklist Item Deletion Alert";
			String expectedMessageBody = task;
			Map<String, String> mailData = fc.utobj().readMailBox(expectedSubject, expectedMessageBody, emailId,
					"sdg@1a@Hfs");

			if (mailData.size() == 0 || !mailData.get("mailBody").contains(task)) {
				fc.utobj().throwsException("was not able to verify task at mail");
			}

			if (mailData.size() == 0 || !mailData.get("mailBody").contains(userName)) {
				fc.utobj().throwsException("was not able to verify userName");
			}

			if (mailData.size() == 0 || !mailData.get("mailBody").contains(storeNoFranchiseID)) {
				fc.utobj().throwsException("was not able to verify Store number");
			}

			if (mailData.size() == 0 || !mailData.get("mailBody").contains(fc.utobj().getCurrentDateUSFormat())) {
				fc.utobj().throwsException("was not able to verify Expected Opening Date");
			}

			if (mailData.size() == 0
					|| !mailData.get("mailBody").contains("Following Item has been deleted from UserTask Checklist")) {
				fc.utobj().throwsException("was not able to verify Expected Opening Date");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void actionImgOption(WebDriver driver, String task, String option) throws Exception {

		String alterText = driver
				.findElement(By.xpath(".//*[.='" + task + "']/ancestor::tr/td/div[@id='menuBar']/layer"))
				.getAttribute("id").trim();
		alterText = alterText.replace("Actions_dynamicmenu", "");
		alterText = alterText.replace("Bar", "");

		fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
				".//*[.='" + task + "']/ancestor::tr/td/div[@id='menuBar']/layer/a/img"));
		fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
				".//div[@id='Actions_dynamicmenu" + alterText + "Menu']/span[contains(text () , '" + option + "')]"));

	}
}
