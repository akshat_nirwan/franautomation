package com.builds.test.opener;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminConfigurationConfigureStoreTypePageTest;
import com.builds.test.admin.AdminDivisionAddDivisionPageTest;
import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.common.CorporateUser;
import com.builds.uimaps.opener.OpenerStoreSummaryDocumentChecklistPage;
import com.builds.uimaps.opener.OpenerStoreSummaryEquipmentChecklistPage;
import com.builds.uimaps.opener.OpenerStoreSummaryPictureChecklistPage;
import com.builds.uimaps.opener.OpenerStoreSummarySecondaryChecklistPage;
import com.builds.uimaps.opener.OpenerStoreSummaryTaskChecklistPage;
import com.builds.uimaps.opener.OpenerTasksPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class OpenerTasksPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "opener" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Task Status In Progress:: Opener > Tasks", testCaseId = "TC_191_Verify_In_Progress_Status")
	public void verifyInProgressStatus() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			String divisionName = fc.utobj().generateTestData(dataSet.get("divisionName"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To  Admin > Division > Manage Division");
			fc.utobj().printTestStep("Add Division");

			AdminDivisionAddDivisionPageTest divisionPage = new AdminDivisionAddDivisionPageTest();
			divisionPage.addDivision(driver, divisionName);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");

			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String emailId = "openerautomation@staffex.com";
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			AdminOpenerTaskChecklistPageTest addTaskPage = new AdminOpenerTaskChecklistPageTest();
			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			File file = new File(filePath);
			String referenceDate = "";

			fc.utobj().printTestStep("Add Task Checklist");
			String divUserName = fc.utobj().generateTestData(dataSet.get("divUserName"));
			String task = addTaskPage.addTaskChecklistUploadFile(driver, storeType, file, dataSet, referenceDate,
					divUserName, divisionName, config);

			fc.opener().opener_common().openerTasks(driver);

			setDefaultViewOpnerTaskPage(driver);

			fc.utobj().printTestStep("Verify Status Of Task");
			// modify by Inzi "11/03/2016"
			/* String userNameDiv=divUserName+" "+divUserName; */

			filterByTaskStoreNumber(driver, task, storeNoFranchiseID);

			fc.utobj().isTextDisplayed(driver, task, "was not able to verify task at Task Page");

			if (!fc.utobj().getElementByXpath(driver,
					".//*[contains(text () , '" + task + "')]/ancestor::tr/td/a[.='" + storeNoFranchiseID + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Store number at Task Page");
			}
			if (!driver
					.findElement(By.xpath(
							".//*[contains(text () , '" + task + "')]/ancestor::tr/td[contains(text () ,'Division')]"))
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify User Name");
			}
			WebElement statusText = fc.utobj().getElementByXpath(driver,
					".//*[contains(text () , '" + task + "')]/ancestor::tr/td[contains(text () ,'Pending')]");
			if (!statusText.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify status of Task");
			}

			actionImgOption(driver, task, "In Progress");
			fc.utobj().acceptAlertBox(driver);

			WebElement statusText1 = fc.utobj().getElementByXpath(driver,
					".//*[contains(text () , '" + task + "')]/ancestor::tr/td[contains(text () ,'In Progress')]");
			if (!statusText1.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify status of Task after Modify Task 'In Progress'");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "opener" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Add Comments in Task  :: Opener > Tasks", testCaseId = "TC_192_Add_Comment")
	public void addCommentActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			OpenerStoreSummaryEquipmentChecklistPage pobj = new OpenerStoreSummaryEquipmentChecklistPage(driver);
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");
			String emailId = "openerautomation@staffex.com";
			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			AdminOpenerEquipmentChecklistPageTest equipmentChecklistPage = new AdminOpenerEquipmentChecklistPageTest();
			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			File file = new File(filePath);
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));

			String quantity = fc.utobj().generateRandomNumber6Digit();

			// Add Corporate User
			AdminUsersManageCorporateUsersAddCorporateUserPageTest cu = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			cu.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Add Equipments Checklist");
			String equepmentText = equipmentChecklistPage.addEquipmentChecklistUploadFile(driver, dataSet, file,
					responsibilityArea, storeType, corpUser.getuserFullName(), quantity, config);

			String parentWindow = driver.getWindowHandle();

			fc.opener().opener_common().openerTasks(driver);
			setDefaultViewOpnerTaskPage(driver);
			filterByTaskStoreNumber(driver, equepmentText, storeNoFranchiseID);

			fc.utobj().printTestStep("Add Comments");
			actionImgOption(driver, equepmentText, "Add Comments");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String comments = fc.utobj().generateTestData(dataSet.get("comments"));
			fc.utobj().sendKeys(driver, pobj.commentsTxBx, comments);
			fc.utobj().clickElement(driver, pobj.addBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().printTestStep("Verify The Comments");
			filterByTaskStoreNumber(driver, equepmentText, storeNoFranchiseID);
			WebElement eqText = fc.utobj().getElementByXpath(driver,
					".//a[contains(text () , '" + equepmentText + "')]");
			fc.utobj().clickElement(driver, eqText);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			WebElement element2 = driver
					.findElement(By.xpath(".//td[contains(text () , '" + fc.utobj().currentDate() + "')]"));
			fc.utobj().moveToElement(driver, element2);

			if (!element2.isDisplayed()
					&& !fc.utobj().getElementByXpath(driver, ".//td[contains(text () ,'" + comments + "')]")
							.isDisplayed()
					&& !fc.utobj().getElementByXpath(driver, ".//td[contains(text () , 'Pending')]").isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to verify comments date, comments and task status");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + quantity + "')]").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify quantity");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + responsibilityArea + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify responsibilityArea");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + corpUser.getuserFullName() + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Contact(s)");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Upload Document :: Opener > Tasks", testCaseId = "TC_193_Upload_Document")
	public void uploadDocumentActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			OpenerStoreSummaryDocumentChecklistPage pobj = new OpenerStoreSummaryDocumentChecklistPage(driver);
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");
			String emailId = "openerautomation@staffex.com";
			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			AdminOpenerDocumentChecklistPageTest documentChecklistPage = new AdminOpenerDocumentChecklistPageTest();
			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			File file = new File(filePath);
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporatePage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Add Document Cheklist");
			String documentText = documentChecklistPage.addDocumentChecklist(driver, dataSet, responsibilityArea,
					corpUser.getuserFullName(), storeType, file, config);
			String parentWindow = driver.getWindowHandle();

			fc.opener().opener_common().openerTasks(driver);
			setDefaultViewOpnerTaskPage(driver);

			fc.utobj().printTestStep("Upload Document");
			filterByTaskStoreNumber(driver, documentText, storeNoFranchiseID);
			actionImgOption(driver, documentText, "Upload Document");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.completionDate, fc.utobj().currentDate());
			// fc.utobj().clickElement(driver, pobj.calanderClose);
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTitle"));
			fc.utobj().sendKeys(driver, pobj.documentTitle, documentTitle);
			fc.utobj().sendKeys(driver, pobj.uploadDocAttachmentBx, file.getAbsolutePath());
			fc.utobj().clickElement(driver, pobj.completeUploadDocBtn);
			driver.switchTo().window(parentWindow);

			filterByTaskStoreNumber(driver, documentText, storeNoFranchiseID);
			actionImgOption(driver, documentText, "Upload Document");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String isTextPresent = pobj.completionDate.getAttribute("value").trim();

			fc.utobj().printTestStep("Verify The Upload Documents");
			if (!isTextPresent.equals(fc.utobj().currentDate())) {
				fc.utobj().throwsException("was not able to verify Completion Date");
			}

			String isTextPresent1 = pobj.documentTitle.getAttribute("value").trim();

			if (!isTextPresent1.equalsIgnoreCase(documentTitle)) {
				fc.utobj().throwsSkipException("was not able to verify Document Text");
			}

			if (!fc.utobj().getElementByLinkText(driver, dataSet.get("uploadFile")).isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to verify Document");
			}
			fc.utobj().clickElement(driver, pobj.closeBtn);
			driver.switchTo().window(parentWindow);

			filterByTaskStoreNumber(driver, documentText, storeNoFranchiseID);

			fc.utobj().isTextDisplayed(driver, documentText, "was not able to verify doument Text");
			if (!fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + documentText + "')]/ancestor::tr/td[contains(text () , 'Complete')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Status 'complete'");
			}
			if (!fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + documentText
					+ "')]/ancestor::tr/td[contains(text () , '" + responsibilityArea + "')]").isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to Responsibility Area");
			}
			if (!driver
					.findElement(By.xpath(".//a[contains(text () ,'" + documentText
							+ "')]/ancestor::tr/td[contains(text () , '" + corpUser.getuserFullName() + "')]"))
					.isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to userName Area");
			}

			WebElement elementDoc = fc.utobj().getElementByXpath(driver,
					".//a[contains(text () , '" + documentText + "')]");
			fc.utobj().clickElement(driver, elementDoc);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + fc.utobj().currentDate() + "')]")
					.isDisplayed()
					&& !fc.utobj().getElementByXpath(driver, ".//td[contains(text () , 'Complete')]").isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to verify date and checklist status");
			}
			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + responsibilityArea + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify responsibilityArea");
			}
			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + corpUser.getuserFullName() + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Contact(s)");
			}
			/*
			 * if(!fc.utobj().getElementByXpath(driver,".//td[.='"+groupName+
			 * "']")). isDisplayed()){ fc.utobj().throwsException(
			 * "was not able to verify Group Name"); }
			 */
			if (!fc.utobj().getElementByXpath(driver, ".//td[.='" + documentText + "']").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document Name");
			}
			if (!fc.utobj().getElementByLinkText(driver, dataSet.get("uploadFile")).isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to verify Document");
			}
			fc.utobj().clickElement(driver, pobj.closeBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "opener" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Upload Picture :: Opener > Tasks", testCaseId = "TC_194_Upload_Picture")
	public void uploadPictureActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			OpenerStoreSummaryPictureChecklistPage pobj = new OpenerStoreSummaryPictureChecklistPage(driver);
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");
			String emailId = "openerautomation@staffex.com";
			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			AdminOpenerPictureChecklistPageTest pictureChecklistPage = new AdminOpenerPictureChecklistPageTest();
			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			File file = new File(filePath);
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));

			AdminUsersManageCorporateUsersAddCorporateUserPageTest cu = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			cu.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Add Picture Checklist");
			String pictureText = pictureChecklistPage.addPictureChecklist(driver, dataSet, responsibilityArea,
					corpUser.getuserFullName(), storeType, file, config);

			fc.utobj().printTestStep("Upload Picture");
			fc.opener().opener_common().openerTasks(driver);

			setDefaultViewOpnerTaskPage(driver);
			filterByTaskStoreNumber(driver, pictureText, storeNoFranchiseID);
			fc.utobj().isTextDisplayed(driver, pictureText, "was not able to verify Picture Task Text");
			if (!driver
					.findElement(By.xpath(".//a[contains(text () , '" + pictureText
							+ "')]/ancestor::tr/td/a[contains(text () ,'" + storeNoFranchiseID + "')]"))
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Store Number");
			}

			actionImgOption(driver, pictureText, "Upload Picture");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.completionDate, fc.utobj().currentDate());
			String pictureTitle = fc.utobj().generateTestData(dataSet.get("pictureTitle"));
			fc.utobj().sendKeys(driver, pobj.pictureTitle, pictureTitle);

			String file1 = fc.utobj().getFilePathFromTestData(dataSet.get("pictureUpload"));

			fc.utobj().sendKeys(driver, pobj.uploadPicAttachmentBx, file1);
			fc.utobj().clickElement(driver, pobj.completeUploadPicBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify Upload Picture Details");

			filterByTaskStoreNumber(driver, pictureText, storeNoFranchiseID);

			actionImgOption(driver, pictureText, "Upload Picture");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String isTextPresent = pobj.completionDate.getAttribute("value").trim();

			if (!isTextPresent.equals(fc.utobj().currentDate())) {
				fc.utobj().throwsException("was not able to verify Completion Date");
			}

			String isTextPresent1 = pobj.pictureTitle.getAttribute("value").trim();

			if (!isTextPresent1.equalsIgnoreCase(pictureTitle)) {
				fc.utobj().throwsException("was not able to verify Picture Text");
			}

			if (!fc.utobj().getElementByLinkText(driver, dataSet.get("pictureUpload")).isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Picture");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);

			fc.utobj().switchFrameToDefault(driver);

			filterByTaskStoreNumber(driver, pictureText, storeNoFranchiseID);

			if (!fc.utobj().getElementByXpath(driver,
					".//a[contains(text () , '" + pictureText + "')]/ancestor::tr/td[contains(text () ,'Complete')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Status of Picture Checklist Task");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + pictureText
					+ "')]/ancestor::tr/td/a[contains(text () ,'" + pictureTitle + "')]").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Title of Picture document");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + pictureText
					+ "')]/ancestor::tr/td[contains(text () ,'" + responsibilityArea + "')]").isDisplayed()) {
				fc.utobj().throwsException("was not able to Responsibility Area");
			}

			if (!driver
					.findElement(By.xpath(".//a[contains(text () , '" + pictureText
							+ "')]/ancestor::tr/td[contains(text () ,'" + corpUser.getuserFullName() + "')]"))
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to userName");
			}

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + pictureText + "')]"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + fc.utobj().currentDate() + "')]")
					.isDisplayed()
					&& !fc.utobj().getElementByXpath(driver, ".//td[contains(text () , 'Complete')]").isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to verify date and checklist status");
			}
			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + responsibilityArea + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify responsibilityArea");
			}
			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + corpUser.getuserFullName() + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Contact(s)");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//td[.='" + pictureText + "']").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Picture Name");
			}
			if (!fc.utobj().getElementByLinkText(driver, dataSet.get("uploadFile")).isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to verify Picture");
			}
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Complete Task :: Opener > Tasks", testCaseId = "TC_195_Complete_Task")
	public void completeTaskActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			OpenerStoreSummarySecondaryChecklistPage pobj = new OpenerStoreSummarySecondaryChecklistPage(driver);
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			String secondaryChecklistName = fc.utobj().generateTestData(dataSet.get("secondaryChecklistName"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String task = fc.utobj().generateTestData(dataSet.get("task"));

			fc.utobj().printTestStep("Navigate To Admin > Opener > Responsible Department");
			fc.utobj().printTestStep("Add Responsible Department");

			AdminOpenerResponsibleDepartmentPageTest addDepartmentPage = new AdminOpenerResponsibleDepartmentPageTest();
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));
			addDepartmentPage.addDepartment(driver, responsibilityArea);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			/*
			 * String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String password=dataSet.get("password");
			 * corporatePage.addCorporateUser(driver, userName, password,
			 * config);
			 */

			String emailId = "openerautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporatePage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");

			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			fc.utobj().printTestStep("Add Secondary Checklist With Task");
			AdminOpenerSecondaryChecklistPageTest secondaryChecklistPage = new AdminOpenerSecondaryChecklistPageTest();
			String referenceDate = "";
			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			File file = new File(filePath);
			secondaryChecklistPage.addSecondaryChecklistWithTask(driver, dataSet, secondaryChecklistName, description,
					responsibilityArea, corpUser.getuserFullName(), storeType, file, referenceDate, task, config);

			String parentWindow = driver.getWindowHandle();

			fc.utobj().printTestStep("Verify Secondary Checklist And Task Associate With Checklist");
			fc.opener().opener_common().openerTasks(driver);

			setDefaultViewOpnerTaskPage(driver);

			fc.utobj().isTextDisplayed(driver, secondaryChecklistName,
					"was not able to verify secondary checklist Name");
			filterByTaskStoreNumber(driver, task, storeNoFranchiseID);

			if (!driver
					.findElement(By.xpath(".//a[contains(text () ,'" + task
							+ "')]/ancestor::tr/td/a[contains(text () ,'" + storeNoFranchiseID + "')]"))
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify store number");
			}

			actionImgOption(driver, task, "Complete");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String curDate = fc.utobj().currentDate();
			fc.utobj().sendKeys(driver, pobj.completionDateTxBx, curDate);
			// fc.utobj().clickElement(driver, pobj.calanderClose);
			String comments = fc.utobj().generateTestData(dataSet.get("comments"));
			fc.utobj().sendKeys(driver, pobj.commentsTxBx, comments);
			fc.utobj().clickElement(driver, pobj.completeBtn);
			driver.switchTo().window(parentWindow);

			filterByTaskStoreNumber(driver, task, storeNoFranchiseID);
			if (!driver
					.findElement(By.xpath(
							".//a[contains(text () ,'" + task + "')]/ancestor::tr/td[contains(text () ,'Complete')]"))
					.isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to complete task");
			}

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + task + "')]"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + curDate + "')]").isDisplayed()
					&& !fc.utobj().getElementByXpath(driver, ".//td[contains(text () ,'" + comments + "')]")
							.isDisplayed()
					&& !fc.utobj().getElementByXpath(driver, ".//td[contains(text () , 'Complete')]").isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to verify completion date, comments and status");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Modification of Task :: Opener >  Tasks", testCaseId = "TC_196_Modify_Task")
	public void modifyTaskActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > Opener > Responsible Department");
			fc.utobj().printTestStep("Add Responsible Department");

			AdminOpenerResponsibleDepartmentPageTest addDepartmentPage = new AdminOpenerResponsibleDepartmentPageTest();
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));
			addDepartmentPage.addDepartment(driver, responsibilityArea);

			OpenerStoreSummaryTaskChecklistPage pobj = new OpenerStoreSummaryTaskChecklistPage(driver);
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");
			String emailId = "openerautomation@staffex.com";
			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			AdminOpenerTaskChecklistPageTest addTaskPage = new AdminOpenerTaskChecklistPageTest();
			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			// File file=new File(filePath);
			/*
			 * String
			 * groupName=fc.utobj().generateTestData(dataSet.get("groupName"));
			 */
			String referenceDate = "";

			fc.utobj().printTestStep("Add Task Checklist");
			String task = addTaskPage.addTaskChecklistUploadFile(driver, storeType, filePath, dataSet, referenceDate,
					config);

			fc.opener().opener_common().openerTasks(driver);

			setDefaultViewOpnerTaskPage(driver);
			filterByTaskStoreNumber(driver, task, storeNoFranchiseID);

			fc.utobj().printTestStep("Modify Task Checklist");
			actionImgOption(driver, task, "Modify");
			task = fc.utobj().generateTestData(dataSet.get("task"));
			fc.utobj().sendKeys(driver, pobj.taskNameTxtBox, task);

			fc.utobj().clickElement(driver, pobj.responsibilityAreaSelctBtn);
			fc.utobj().clickElement(driver, pobj.responsibilityAreaSelectAllBtn);
			fc.utobj().clickElement(driver, pobj.responsibilityAreaSelectAllBtn);
			fc.utobj().sendKeys(driver, pobj.responsibilityAreaTextBx, responsibilityArea);
			fc.utobj().clickElement(driver, pobj.responsibilityAreaSelectAllBtn);
			fc.utobj().clickElement(driver, pobj.responsibilityAreaSelctBtn);

			fc.utobj().clickElement(driver, pobj.ContactSelectBtn);
			fc.utobj().clickElement(driver, pobj.contactSelectAll);
			fc.utobj().clickElement(driver, pobj.contactSelectAll);
			fc.utobj().sendKeys(driver, pobj.contactTextBox, "Regional User");
			fc.utobj().clickElement(driver, pobj.contactSelectAll);

			String dependentOn = dataSet.get("dependentOnModify");
			String valueText = fc.utobj()
					.getElementByXpath(driver,
							".//select[@name='smUserTaskChecklist_0referenceParent']/option[contains(text () , '"
									+ dependentOn + "')]")
					.getAttribute("value");
			fc.utobj().moveToElement(driver, pobj.dependentOnDropDown);
			Select sl = new Select(pobj.dependentOnDropDown);
			sl.selectByValue(valueText);

			fc.utobj().sendKeys(driver, pobj.startScheduleDateTxtBox, "1");
			fc.utobj().selectDropDown(driver, pobj.scheduleStartDropDown, "Days Prior");
			fc.utobj().sendKeys(driver, pobj.completionScheduleDateTxtBox, "1");
			fc.utobj().selectDropDown(driver, pobj.scheduleCompletionDropDown, "Days Prior");
			fc.utobj().sendKeys(driver, pobj.startReminderTxtBox, "2");
			fc.utobj().sendKeys(driver, pobj.completionReminderTxtBox, "2");

			if (fc.utobj().isSelected(driver, pobj.webLinkRadioBtn)) {
				// do nothing
			} else {
				fc.utobj().clickElement(driver, pobj.webLinkRadioBtn);
			}

			fc.utobj().sendKeys(driver, pobj.webLinkAddressBx, dataSet.get("webLinkUrl"));

			try {
				List<WebElement> listRadioButton = pobj.applyToAll;

				if (fc.utobj().isSelected(driver,listRadioButton.get(2))) {
					// do nothing
				} else {
					fc.utobj().clickElement(driver, listRadioButton.get(2));
				}

				// select
				fc.utobj().clickElement(driver, pobj.currentOpnerSelectBtn);
				fc.utobj().clickElement(driver, pobj.selectAllOpnerCheck);
			} catch (Exception e) {

			}
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("Verify The Modify Task Checklist Detail");
			filterByTaskStoreNumber(driver, task, storeNoFranchiseID);

			if (!fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + task + "')]").isDisplayed()) {
				fc.utobj().throwsException("was not able to modify task Text");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + task
					+ "')]/ancestor::tr/td[contains(text () , '" + responsibilityArea + "')]").isDisplayed()) {
				fc.utobj().throwsException("was not able to modify responsibility area");
			}

			if (!fc.utobj().getElementByXpath(driver,
					".//a[contains(text () , '" + task + "')]/ancestor::tr/td[contains(text () , 'Regional User')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to modify Contact");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Deletion of Task :: Opener > Tasks", testCaseId = "TC_197_Delete_Task")
	public void deleteTaskActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			OpenerStoreSummaryEquipmentChecklistPage pobj = new OpenerStoreSummaryEquipmentChecklistPage(driver);
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");
			String emailId = "openerautomation@staffex.com";
			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			AdminOpenerEquipmentChecklistPageTest equipmentChecklistPage = new AdminOpenerEquipmentChecklistPageTest();
			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			File file = new File(filePath);
			/*
			 * String supplierName=fc.utobj().generateTestData(dataSet.get(
			 * "supplierName"));
			 */
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));

			/*
			 * String
			 * groupName=fc.utobj().generateTestData(dataSet.get("groupName"));
			 */
			String quantity = fc.utobj().generateRandomNumber();

			// Add Corporate User
			AdminUsersManageCorporateUsersAddCorporateUserPageTest cu = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			cu.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Add Equipment Checklist");
			String equepmentText = equipmentChecklistPage.addEquipmentChecklistUploadFile(driver, dataSet, file,
					responsibilityArea, storeType, corpUser.getuserFullName(), quantity, config);

			String parentWindow = driver.getWindowHandle();

			fc.opener().opener_common().openerTasks(driver);
			setDefaultViewOpnerTaskPage(driver);
			filterByTaskStoreNumber(driver, equepmentText, storeNoFranchiseID);
			fc.utobj().isTextDisplayed(driver, equepmentText, "was not able to verify Equipment Task Text");
			if (!fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + equepmentText
					+ "')]/ancestor::tr/td/a[.='" + storeNoFranchiseID + "']").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify store Number");
			}

			fc.utobj().printTestStep("Delete Equipment Checklist");
			actionImgOption(driver, equepmentText, "Delete");
			fc.utobj().acceptAlertBox(driver);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().printTestStep("Verify The Delete Equipment Checklist");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, equepmentText);
			if (isTextPresent == true) {
				fc.utobj().throwsSkipException("was not able to delete equipment checklist Task");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Mark As A Incomplete Task :: Opener > Tasks ", testCaseId = "TC_198_Mark_As_InComplete")
	public void markAsIncompleteActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			OpenerStoreSummaryDocumentChecklistPage pobj = new OpenerStoreSummaryDocumentChecklistPage(driver);
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");
			String emailId = "openerautomation@staffex.com";
			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			AdminOpenerDocumentChecklistPageTest documentChecklistPage = new AdminOpenerDocumentChecklistPageTest();
			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			File file = new File(filePath);
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporatePage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Add Document Checklist");
			String documentText = documentChecklistPage.addDocumentChecklist(driver, dataSet, responsibilityArea,
					corpUser.getuserFullName(), storeType, file, config);

			String parentWindow = driver.getWindowHandle();

			fc.opener().opener_common().openerTasks(driver);
			setDefaultViewOpnerTaskPage(driver);
			filterByTaskStoreNumber(driver, documentText, storeNoFranchiseID);

			fc.utobj().isTextDisplayed(driver, documentText, "was not able to verify document Text");

			fc.utobj().printTestStep("Upload Document");
			actionImgOption(driver, documentText, "Upload Document");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.completionDate, fc.utobj().currentDate());
			// fc.utobj().clickElement(driver, pobj.calanderClose);
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTitle"));
			fc.utobj().sendKeys(driver, pobj.documentTitle, documentTitle);
			fc.utobj().sendKeys(driver, pobj.uploadDocAttachmentBx, file.getAbsolutePath());
			fc.utobj().clickElement(driver, pobj.completeUploadDocBtn);
			driver.switchTo().window(parentWindow);

			filterByTaskStoreNumber(driver, documentText, storeNoFranchiseID);

			if (!fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + documentText + "')]/ancestor::tr/td[contains(text () ,'Complete')]")
					.isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to verify Complete Status of task");
			}

			fc.utobj().printTestStep("Mark As A Incomplete");
			actionImgOption(driver, documentText, "Mark as Incomplete");
			fc.utobj().acceptAlertBox(driver);

			filterByTaskStoreNumber(driver, documentText, storeNoFranchiseID);

			fc.utobj().printTestStep("Verify Mark As InComplete");
			if (!fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + documentText + "')]/ancestor::tr/td[contains(text () ,'Pending')]")
					.isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to verify Complete Status of task");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + documentText
					+ "')]/ancestor::tr/td[contains(text () ,'" + responsibilityArea + "')]").isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to Responsibility Area");
			}

			if (!driver
					.findElement(By.xpath(".//a[contains(text () ,'" + documentText
							+ "')]/ancestor::tr/td[contains(text () ,'" + corpUser.getuserFullName() + "')]"))
					.isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to userName Area");
			}

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + documentText + "')]"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + fc.utobj().currentDate() + "')]")
					.isDisplayed()
					&& !fc.utobj().getElementByXpath(driver, ".//td[contains(text () , 'Pending')]").isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to verify date and checklist status");
			}
			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + responsibilityArea + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify responsibilityArea");
			}
			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + corpUser.getuserFullName() + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Contact(s)");
			}
			/*
			 * if(!fc.utobj().getElementByXpath(driver,".//td[.='"+groupName+
			 * "']")). isDisplayed()){ fc.utobj().throwsException(
			 * "was not able to verify Group Name"); }
			 */
			if (!fc.utobj().getElementByXpath(driver, ".//td[.='" + documentText + "']").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document Name");
			}
			if (!fc.utobj().getElementByLinkText(driver, dataSet.get("uploadFile")).isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to verify Document");
			}
			fc.utobj().clickElement(driver, pobj.closeBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add Description :: Opener > Tasks", testCaseId = "TC_199_Add_Description")
	public void addDescriptionActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			OpenerStoreSummaryPictureChecklistPage pobj = new OpenerStoreSummaryPictureChecklistPage(driver);
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");
			String emailId = "openerautomation@staffex.com";
			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			AdminOpenerPictureChecklistPageTest pictureChecklistPage = new AdminOpenerPictureChecklistPageTest();
			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			File file = new File(filePath);
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));

			// Add Corporate User
			AdminUsersManageCorporateUsersAddCorporateUserPageTest cu = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			cu.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Add Picture Checklist");
			String pictureText = pictureChecklistPage.addPictureChecklist(driver, dataSet, responsibilityArea,
					corpUser.getuserFullName(), storeType, file, config);

			String parentWindow = driver.getWindowHandle();
			fc.opener().opener_common().openerTasks(driver);
			setDefaultViewOpnerTaskPage(driver);
			filterByTaskStoreNumber(driver, pictureText, storeNoFranchiseID);

			fc.utobj().isTextDisplayed(driver, pictureText, "was not able to verify Picture Checklist Task");

			if (!fc.utobj().getElementByXpath(driver,
					".//a[contains(text () , '" + pictureText + "')]/ancestor::tr/td[contains(text () ,'Pending')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Pending status of Task");
			}

			fc.utobj().printTestStep("Add Description");
			actionImgOption(driver, pictureText, "Add Description");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().sendKeys(driver, pobj.descriptionTxBx, description);
			fc.utobj().clickElement(driver, pobj.addBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().printTestStep("Verify The Add Description");
			filterByTaskStoreNumber(driver, pictureText, storeNoFranchiseID);
			actionImgOption(driver, pictureText, "Modify Description");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String isTextPresent = fc.utobj().getText(driver, pobj.descriptionTxBx);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			driver.switchTo().window(parentWindow);

			if (!isTextPresent.equalsIgnoreCase(description)) {
				fc.utobj().throwsSkipException("was not able to add Description");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Modification Description :: Opener > Tasks", testCaseId = "TC_200_Modify_Description")
	public void modifyDescriptionActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			OpenerStoreSummaryTaskChecklistPage pobj = new OpenerStoreSummaryTaskChecklistPage(driver);
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");
			String emailId = "openerautomation@staffex.com";
			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			AdminOpenerTaskChecklistPageTest addTaskPage = new AdminOpenerTaskChecklistPageTest();
			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			// File file=new File(filePath);
			/*
			 * String
			 * groupName=fc.utobj().generateTestData(dataSet.get("groupName"));
			 */
			String referenceDate = "";

			fc.utobj().printTestStep("Add Task Checklist");
			String task = addTaskPage.addTaskChecklistUploadFile(driver, storeType, filePath, dataSet, referenceDate,
					config);

			String parentWindow = driver.getWindowHandle();

			fc.opener().opener_common().openerTasks(driver);

			setDefaultViewOpnerTaskPage(driver);
			filterByTaskStoreNumber(driver, task, storeNoFranchiseID);

			fc.utobj().isTextDisplayed(driver, task, "was not able to verify Task Checklist Task");

			if (!driver
					.findElement(By.xpath(
							".//a[contains(text () , '" + task + "')]/ancestor::tr/td[contains(text () ,'Pending')]"))
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Pending status of Task");
			}

			fc.utobj().printTestStep("Add Description");
			actionImgOption(driver, task, "Add Description");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().sendKeys(driver, pobj.descriptionTxBx, description);
			fc.utobj().clickElement(driver, pobj.addBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().printTestStep("Modify Description");
			filterByTaskStoreNumber(driver, task, storeNoFranchiseID);
			actionImgOption(driver, task, "Modify Description");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().sendKeys(driver, pobj.descriptionTxBx, description);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().printTestStep("Verify The Modify Description");
			filterByTaskStoreNumber(driver, task, storeNoFranchiseID);
			actionImgOption(driver, task, "Modify Description");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			String isTextPresent = pobj.descriptionTxBx.getText().trim();
			if (!isTextPresent.equalsIgnoreCase(description)) {
				fc.utobj().throwsException("was not able to modify description");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Not Applicable :: Opener > Tasks", testCaseId = "TC_201_Verify_Not_Applicable")
	public void verifyNotApplicable() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");
			String emailId = "openerautomation@staffex.com";
			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			AdminOpenerEquipmentChecklistPageTest equipmentChecklistPage = new AdminOpenerEquipmentChecklistPageTest();
			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			File file = new File(filePath);
			/*
			 * String supplierName=fc.utobj().generateTestData(dataSet.get(
			 * "supplierName"));
			 */
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));

			/*
			 * String
			 * groupName=fc.utobj().generateTestData(dataSet.get("groupName"));
			 */
			String quantity = fc.utobj().generateRandomNumber();

			// Add Corporate User
			AdminUsersManageCorporateUsersAddCorporateUserPageTest cu = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			cu.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Add Equipment Checklist");
			String equepmentText = equipmentChecklistPage.addEquipmentChecklistUploadFile(driver, dataSet, file,
					responsibilityArea, storeType, corpUser.getuserFullName(), quantity, config);

			fc.opener().opener_common().openerTasks(driver);
			setDefaultViewOpnerTaskPage(driver);
			filterByTaskStoreNumber(driver, equepmentText, storeNoFranchiseID);

			fc.utobj().isTextDisplayed(driver, equepmentText, "was not able to verify Equipment Checklist Task");

			if (!fc.utobj().getElementByXpath(driver,
					".//a[contains(text () , '" + equepmentText + "')]/ancestor::tr/td[contains(text () ,'Pending')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Pending status of Task");
			}

			fc.utobj().printTestStep("Mark As A Not Applicable");
			actionImgOption(driver, equepmentText, "Not Applicable");
			fc.utobj().acceptAlertBox(driver);
			filterByTaskStoreNumber(driver, equepmentText, storeNoFranchiseID);

			fc.utobj().printTestStep("Verify Mark As A Not Applicable");
			if (!fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + equepmentText
					+ "')]/ancestor::tr/td[contains(text () ,'Not Applicable')]").isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to verify Not Applicable Status");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "opener", "opener22" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Complete Task At ActionBtn :: Opener >  Tasks", testCaseId = "TC_202_Complete_Task")
	public void completeTaskActionBtn() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			OpenerStoreSummaryTaskChecklistPage pobj = new OpenerStoreSummaryTaskChecklistPage(driver);
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");
			String emailId = "openerautomation@staffex.com";
			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			AdminOpenerTaskChecklistPageTest addTaskPage = new AdminOpenerTaskChecklistPageTest();
			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			String referenceDate = "";

			fc.utobj().printTestStep("Add Task Checklist");
			String task = addTaskPage.addTaskChecklistUploadFile(driver, storeType, filePath, dataSet, referenceDate,
					config);

			String parentWindow = driver.getWindowHandle();

			fc.opener().opener_common().openerTasks(driver);

			setDefaultViewOpnerTaskPage(driver);
			filterByTaskStoreNumber(driver, task, storeNoFranchiseID);

			fc.utobj().isTextDisplayed(driver, task, "was not able to verify Task Checklist Task");

			if (!driver
					.findElement(By.xpath(
							".//a[contains(text () , '" + task + "')]/ancestor::tr/td[contains(text () ,'Pending')]"))
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Pending status of Task");
			}

			fc.utobj().printTestStep("Complete Task Checklist");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + task + "')]/ancestor::tr/td/input"));
			fc.utobj().selectActionMenuItems(driver, "Complete");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String curDate = fc.utobj().currentDate();
			fc.utobj().sendKeys(driver, pobj.completionDateTxBx, curDate);
			String comments = fc.utobj().generateTestData(dataSet.get("comments"));
			fc.utobj().sendKeys(driver, pobj.commentsTxBx, comments);
			fc.utobj().clickElement(driver, pobj.completeBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().printTestStep("Verify The Complete Checklist");

			filterByTaskStoreNumber(driver, task, storeNoFranchiseID);
			if (!driver
					.findElement(By.xpath(
							".//a[contains(text () ,'" + task + "')]/ancestor::tr/td[contains(text () ,'Complete')]"))
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify status of task");
			}

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + task + "')]"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + curDate + "')]").isDisplayed()
					&& !fc.utobj().getElementByXpath(driver, ".//td[contains(text () ,'" + comments + "')]")
							.isDisplayed()
					&& !fc.utobj().getElementByXpath(driver, ".//td[contains(text () , 'Complete')]").isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to verify completion date, comments and status");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Deletion of Task :: Opener > Tasks", testCaseId = "TC_203_Delete_Task")
	public void deleteTaskActionBtn() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			OpenerStoreSummaryDocumentChecklistPage pobj = new OpenerStoreSummaryDocumentChecklistPage(driver);

			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");
			String emailId = "openerautomation@staffex.com";
			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			AdminOpenerDocumentChecklistPageTest documentChecklistPage = new AdminOpenerDocumentChecklistPageTest();
			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			File file = new File(filePath);
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporatePage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Add Document Checklist");
			String documentText = documentChecklistPage.addDocumentChecklist(driver, dataSet, responsibilityArea,
					corpUser.getuserFullName(), storeType, file, config);

			String parentWindow = driver.getWindowHandle();

			fc.opener().opener_common().openerTasks(driver);

			setDefaultViewOpnerTaskPage(driver);
			filterByTaskStoreNumber(driver, documentText, storeNoFranchiseID);

			fc.utobj().isTextDisplayed(driver, documentText, "was not able to verify Document Checklist Task");

			if (!fc.utobj().getElementByXpath(driver,
					".//a[contains(text () , '" + documentText + "')]/ancestor::tr/td[contains(text () ,'Pending')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Pending status of Task");
			}

			fc.utobj().printTestStep("Delete Task Checklist By Action Button Option");
			fc.utobj().clickElement(driver, driver
					.findElement(By.xpath(".//a[contains(text () ,'" + documentText + "')]/ancestor::tr/td/input")));
			fc.utobj().selectActionMenuItems(driver, "Delete");
			fc.utobj().acceptAlertBox(driver);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().printTestStep("Verify The Delete Task Checklist");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, documentText);
			if (isTextPresent == true) {
				fc.utobj().throwsSkipException("was not able to delete a task");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Modification of Contact(s) :: Opener > Tasks ", testCaseId = "TC_204_Modify_Contact")
	public void modifyContactActionBtn() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			OpenerStoreSummaryPictureChecklistPage pobj = new OpenerStoreSummaryPictureChecklistPage(driver);

			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");
			String emailId = "openerautomation@staffex.com";
			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			AdminOpenerPictureChecklistPageTest pictureChecklistPage = new AdminOpenerPictureChecklistPageTest();
			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			File file = new File(filePath);
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));

			// Add Corporate User
			AdminUsersManageCorporateUsersAddCorporateUserPageTest cu = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			cu.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Add Picture Checklist");
			String pictureText = pictureChecklistPage.addPictureChecklist(driver, dataSet, responsibilityArea,
					corpUser.getuserFullName(), storeType, file, config);

			String parentWindow = driver.getWindowHandle();
			fc.opener().opener_common().openerTasks(driver);

			setDefaultViewOpnerTaskPage(driver);
			filterByTaskStoreNumber(driver, pictureText, storeNoFranchiseID);

			fc.utobj().isTextDisplayed(driver, pictureText, "was not able to verify Picture Checklist Task");
			if (!fc.utobj().getElementByXpath(driver,
					".//a[contains(text () , '" + pictureText + "')]/ancestor::tr/td[contains(text () ,'Pending')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Pending status of Task");
			}

			fc.utobj().printTestStep("Modify The Contact Of The Checklist");
			fc.utobj().clickElement(driver, driver
					.findElement(By.xpath(".//a[contains(text () ,'" + pictureText + "')]/ancestor::tr/td/input")));
			fc.utobj().selectActionMenuItems(driver, "Modify Contact(s)");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.modifyContactSelectBtn);
			fc.utobj().sendKeys(driver, pobj.modifyContactSearchBx, "Regional User");
			fc.utobj().clickElement(driver, pobj.modifyContactSelectAll);
			fc.utobj().clickElement(driver, pobj.modifyContactSelectBtn);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().printTestStep("Verify The Modify of Contact");
			filterByTaskStoreNumber(driver, pictureText, storeNoFranchiseID);
			if (!fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + pictureText
					+ "')]/ancestor::tr/td[contains(text () , 'Regional User')]").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify MOdify Contact(s)");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Group View of Task :: Opener > Tasks", testCaseId = "TC_205_Verify_Group_View")
	public void groupViewActionBtn() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			OpenerStoreSummaryEquipmentChecklistPage pobj = new OpenerStoreSummaryEquipmentChecklistPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");
			String emailId = "openerautomation@staffex.com";
			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			AdminOpenerEquipmentChecklistPageTest equipmentChecklistPage = new AdminOpenerEquipmentChecklistPageTest();
			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			File file = new File(filePath);

			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));

			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			String quantity = fc.utobj().generateRandomNumber();

			AdminUsersManageCorporateUsersAddCorporateUserPageTest cu = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			cu.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Add Equipment Checklist");
			String equepmentText = equipmentChecklistPage.addEquipmentChecklistUploadFileWithGroup(driver, dataSet,
					file, responsibilityArea, groupName, storeType, corpUser.getuserFullName(), quantity, config);

			fc.utobj().printTestStep("Navigate To Opener > Tasks Page");
			fc.opener().opener_common().openerTasks(driver);

			setDefaultViewOpnerTaskPage(driver);
			filterByTaskStoreNumber(driver, equepmentText, storeNoFranchiseID);

			fc.utobj().isTextDisplayed(driver, equepmentText, "was not able to verify Picture Checklist Task");
			if (!fc.utobj().getElementByXpath(driver,
					".//a[contains(text () , '" + equepmentText + "')]/ancestor::tr/td[contains(text () ,'Pending')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Pending status of Task");
			}

			fc.utobj().printTestStep("Make Group View Of Checklist");
			fc.utobj().clickElement(driver, driver
					.findElement(By.xpath(".//a[contains(text () ,'" + equepmentText + "')]/ancestor::tr/td/input")));
			fc.utobj().selectActionMenuItems(driver, "Group view");

			fc.utobj().printTestStep("Verify The Group view Of Checklist");
			filterByTaskStoreNumber(driver, equepmentText, storeNoFranchiseID);
			if (!fc.utobj()
					.getElementByXpath(driver,
							".//*[.='Equipment Checklist']/ancestor::tr[2]/following-sibling::tr//tr/td[contains(text () , '"
									+ groupName + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify group view");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Deletion Task :: Opener > Tasks", testCaseId = "TC_206_Delete_Task")
	public void deleteTaskBottomBtn() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			OpenerStoreSummaryDocumentChecklistPage pobj = new OpenerStoreSummaryDocumentChecklistPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));

			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");
			String emailId = "openerautomation@staffex.com";
			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			AdminOpenerDocumentChecklistPageTest documentChecklistPage = new AdminOpenerDocumentChecklistPageTest();
			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			File file = new File(filePath);
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporatePage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Add Document Checklist");
			String documentText = documentChecklistPage.addDocumentChecklist(driver, dataSet, responsibilityArea,
					corpUser.getuserFullName(), storeType, file, config);

			String parentWindow = driver.getWindowHandle();

			fc.opener().opener_common().openerTasks(driver);

			setDefaultViewOpnerTaskPage(driver);
			filterByTaskStoreNumber(driver, documentText, storeNoFranchiseID);

			fc.utobj().isTextDisplayed(driver, documentText, "was not able to verify Picture Checklist Task");
			if (!fc.utobj().getElementByXpath(driver,
					".//a[contains(text () , '" + documentText + "')]/ancestor::tr/td[contains(text () ,'Pending')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Pending status of Task");
			}

			fc.utobj().printTestStep("Delete Checklist By Bottom Button Option");
			fc.utobj().clickElement(driver, driver
					.findElement(By.xpath(".//a[contains(text () ,'" + documentText + "')]/ancestor::tr/td/input")));
			fc.utobj().clickElement(driver, pobj.bottomDeleteBtn);
			fc.utobj().acceptAlertBox(driver);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().printTestStep("Verify The Delete Checklist By Bottom Button");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, documentText);
			if (isTextPresent == true) {
				fc.utobj().throwsSkipException("was not able to delete task");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Modification of Contact(s) :: Opener > Tasks", testCaseId = "TC_207_Modify_Contact")
	public void modifyContactBottomBtn() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			OpenerStoreSummaryPictureChecklistPage pobj = new OpenerStoreSummaryPictureChecklistPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));

			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");
			String emailId = "openerautomation@staffex.com";
			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			AdminOpenerPictureChecklistPageTest pictureChecklistPage = new AdminOpenerPictureChecklistPageTest();
			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			File file = new File(filePath);
			String responsibilityArea = fc.utobj().generateTestData(dataSet.get("responsibilityArea"));

			// Add Corporate User
			AdminUsersManageCorporateUsersAddCorporateUserPageTest cu = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			cu.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Add Picture Checklist");
			String pictureText = pictureChecklistPage.addPictureChecklist(driver, dataSet, responsibilityArea,
					corpUser.getuserFullName(), storeType, file, config);

			String parentWindow = driver.getWindowHandle();

			fc.opener().opener_common().openerTasks(driver);

			setDefaultViewOpnerTaskPage(driver);
			filterByTaskStoreNumber(driver, pictureText, storeNoFranchiseID);

			fc.utobj().isTextDisplayed(driver, pictureText, "was not able to verify Picture Checklist Task");
			if (!fc.utobj().getElementByXpath(driver,
					".//a[contains(text () , '" + pictureText + "')]/ancestor::tr/td[contains(text () ,'Pending')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Pending status of Task");
			}

			fc.utobj().printTestStep("Modify The Contact By Bottom Button Option");
			fc.utobj().clickElement(driver, driver
					.findElement(By.xpath(".//a[contains(text () ,'" + pictureText + "')]/ancestor::tr/td/input")));
			fc.utobj().clickElement(driver, pobj.modifyContactBoottomBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.modifyContactSelectBtn);
			fc.utobj().sendKeys(driver, pobj.modifyContactSearchBx, "Regional User");
			fc.utobj().clickElement(driver, pobj.modifyContactSelectAll);
			fc.utobj().clickElement(driver, pobj.modifyContactSelectBtn);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			driver.switchTo().window(parentWindow);

			filterByTaskStoreNumber(driver, pictureText, storeNoFranchiseID);

			fc.utobj().printTestStep("Veriyf The Modify of Contact");
			if (!fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + pictureText
					+ "')]/ancestor::tr/td[contains(text () , 'Regional User')]").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify MOdify Contact(s)");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "opener")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Mark As A In Complete Task At Opener > Tasks", testCaseId = "TC_208_Complete_Task")
	public void completeTaskBottomBtn() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("opener", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			OpenerStoreSummaryTaskChecklistPage pobj = new OpenerStoreSummaryTaskChecklistPage(driver);

			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary");
			fc.utobj().printTestStep("Add New Franchise Location");
			String emailId = "openerautomation@staffex.com";
			OpenerStoreSummaryStoreListPageTest addStorePage = new OpenerStoreSummaryStoreListPageTest();
			String storeNoFranchiseID = addStorePage.addNewFranchiseLocation(driver, regionName, storeType, dataSet,
					config, emailId);

			AdminOpenerTaskChecklistPageTest addTaskPage = new AdminOpenerTaskChecklistPageTest();
			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			// File file=new File(filePath);
			/*
			 * String
			 * groupName=fc.utobj().generateTestData(dataSet.get("groupName"));
			 */
			String referenceDate = "";

			fc.utobj().printTestStep("Add Task Checklist");
			String task = addTaskPage.addTaskChecklistUploadFile(driver, storeType, filePath, dataSet, referenceDate,
					config);

			String parentWindow = driver.getWindowHandle();

			fc.opener().opener_common().openerTasks(driver);

			setDefaultViewOpnerTaskPage(driver);
			filterByTaskStoreNumber(driver, task, storeNoFranchiseID);

			fc.utobj().isTextDisplayed(driver, task, "was not able to verify Picture Checklist Task");
			if (!driver
					.findElement(By.xpath(
							".//a[contains(text () , '" + task + "')]/ancestor::tr/td[contains(text () ,'Pending')]"))
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Pending status of Task");
			}

			fc.utobj().printTestStep("Complete The Checklist");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + task + "')]/ancestor::tr/td/input"));
			fc.utobj().clickElement(driver, pobj.completeBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String curDate = fc.utobj().currentDate();
			fc.utobj().sendKeys(driver, pobj.completionDateTxBx, curDate);
			// fc.utobj().clickElement(driver, pobj.calanderClose);
			String comments = fc.utobj().generateTestData(dataSet.get("comments"));
			fc.utobj().sendKeys(driver, pobj.commentsTxBx, comments);
			fc.utobj().clickElement(driver, pobj.completeBtn);
			driver.switchTo().window(parentWindow);

			fc.utobj().printTestStep("Verify The Complete Checklist");
			filterByTaskStoreNumber(driver, task, storeNoFranchiseID);

			if (!driver
					.findElement(By.xpath(
							".//a[contains(text () ,'" + task + "')]/ancestor::tr/td[contains(text () ,'Complete')]"))
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify status of task");
			}

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + task + "')]"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + curDate + "')]").isDisplayed()
					&& !fc.utobj().getElementByXpath(driver, ".//td[contains(text () ,'" + comments + "')]")
							.isDisplayed()
					&& !fc.utobj().getElementByXpath(driver, ".//td[contains(text () , 'Complete')]").isDisplayed()) {
				fc.utobj().throwsSkipException("was not able to verify completion date, comments and status");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			driver.switchTo().window(parentWindow);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void setDefaultViewOpnerTaskPage(WebDriver driver) throws Exception {

		OpenerTasksPage pobj = new OpenerTasksPage(driver);
		fc.utobj().clickElement(driver, pobj.showFilter);
		fc.utobj().setToDefault(driver, pobj.storeNumberSelect);
		fc.utobj().setToDefault(driver, pobj.checkListSelect);
		fc.utobj().sendKeys(driver, pobj.taskTxBx, "123");
		fc.utobj().getElement(driver, pobj.taskTxBx).clear();
		fc.utobj().setToDefault(driver, pobj.statusSelect);
		fc.utobj().setToDefault(driver, pobj.contactSelect);
		fc.utobj().setToDefault(driver, pobj.groupSelect);
		fc.utobj().selectDropDown(driver, pobj.scheduleStartDateDropDown, "All");
		fc.utobj().clickElement(driver, pobj.saveViewBtn);
		fc.utobj().clickElement(driver, pobj.hideFilter);
	}

	// updated for opener > task page
	public void actionImgOption(WebDriver driver, String task, String option) throws Exception {

		String alterText = driver
				.findElement(
						By.xpath(".//a[contains(text () , '" + task + "')]/ancestor::tr/td/div[@id='menuBar']/layer"))
				.getAttribute("id").trim();
		alterText = alterText.replace("Actions_dynamicmenu", "");
		alterText = alterText.replace("Bar", "");

		fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
				".//a[contains(text () , '" + task + "')]/ancestor::tr/td/div[@id='menuBar']/layer/a/img"));
		fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
				".//div[@id='Actions_dynamicmenu" + alterText + "Menu']/span[contains(text () , '" + option + "')]"));

	}

	public void showAll(WebDriver driver, String CheckListName) {
		try {

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[.='" + CheckListName + "']/following-sibling::td/a[@title='Show All']"));
		} catch (Exception e) {

		}
	}

	public void filterByTaskStoreNumber(WebDriver driver, String task, String storeNumber) throws Exception {

		OpenerTasksPage pobj = new OpenerTasksPage(driver);
		fc.utobj().clickElement(driver, pobj.showFilter);
		fc.utobj().selectValFromMultiSelect(driver, pobj.storeNumberSelect, storeNumber);
		fc.utobj().sendKeys(driver, pobj.taskTxBx, task);
		fc.utobj().clickElement(driver, pobj.searchBtn);
		fc.utobj().clickElement(driver, pobj.hideFilter);
	}
}
