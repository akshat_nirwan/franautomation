package com.builds.test.opener;

import org.openqa.selenium.WebDriver;
import org.testng.Reporter;

import com.builds.test.admin.AdminPageTest;
import com.builds.test.common.FCHomePageTest;
import com.builds.uimaps.common.FCHomePage;
import com.builds.utilities.FranconnectUtil;

public class Opener_Common extends FranconnectUtil {

	public void openerModule(WebDriver driver) throws Exception {
		Reporter.log("Navigating to openerModule...");
		Thread.sleep(2000);

		home_page().openOpenerModule(driver);
	}

	public void adminFranchiseOpenerTaskChecklist(WebDriver driver) throws Exception {
		Reporter.log("Navigating to adminFranchiseOpenerTaskChecklist...");
		adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openTaskChecklistLnk(driver);
	}

	public void adminFranchiseOpenerEquipmentChecklist(WebDriver driver) throws Exception {
		Reporter.log("Navigating to adminFranchiseOpenerEquipmentChecklist...");
		adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openEquipmentChecklistLnk(driver);
	}

	public void adminFranchiseOpenerDocumentChecklist(WebDriver driver) throws Exception {
		Reporter.log("Navigating to adminFranchiseOpenerDocumentChecklist...");
		adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openDocumentChecklistLnk(driver);
	}

	public void adminFranchiseOpenerPictureChecklist(WebDriver driver) throws Exception {
		Reporter.log("Navigating to adminFranchiseOpenerPictureChecklist...");
		adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openPictureChecklistLnk(driver);
	}

	public void adminFranchiseOpenerSecondaryChecklist(WebDriver driver) throws Exception {
		Reporter.log("Navigating to adminFranchiseOpenerSecondaryChecklist...");
		adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openSecondaryChecklistLnk(driver);
	}

	public void adminFranchiseOpenerResponsibileDepartment(WebDriver driver) throws Exception {
		Reporter.log("Navigating to adminFranchiseOpenerResponsibileDepartment...");
		adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openResponsibleDepartmentLnk(driver);
	}

	public void adminFranchiseOpenerOverdueAlertFrequency(WebDriver driver) throws Exception {
		Reporter.log("Navigating to adminFranchiseOpenerOverdueAlertFrequency...");
		adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openOverdueAlertFrequencyLnk(driver);
	}

	public void adminFranchiseOpenerAlertEmailContent(WebDriver driver) throws Exception {
		Reporter.log("Navigating to adminFranchiseOpenerAlertEmailContent...");
		adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openAlertEmailContentLnk(driver);
	}

	public void adminFranchiseOpenerCustomizeProfiles(WebDriver driver) throws Exception {
		Reporter.log("Navigating to adminFranchiseOpenerCustomizeProfiles...");
		adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openCustomizeProfilesLnk(driver);
	}

	public void adminFranchiseOpenerManageReferenceDates(WebDriver driver) throws Exception {
		Reporter.log("Navigating to adminFranchiseOpenerManageReferenceDates...");
		adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openManageReferenceDatesLnk(driver);
	}

	public void adminFranchiseOpenerConfigureProjectStatus(WebDriver driver) throws Exception {
		Reporter.log("Navigating to adminFranchiseOpenerConfigureProjectStatus...");
		adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openConfigureProjectStatusLnk(driver);
	}

	public void adminFranchiseOpenerManageGroups(WebDriver driver) throws Exception {
		Reporter.log("Navigating to adminFranchiseOpenerManageGroups...");
		adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openManageGroupsLnk(driver);
	}

	public void adminFranchiseOpenerConfigureChecklistDisplaySetting(WebDriver driver) throws Exception {
		Reporter.log("Navigating to adminFranchiseOpenerManageGroups...");
		adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openConfigureChecklistDisplaySettingLnk(driver);

	}

	public void adminFranchiseOpenerConfigureAlertEmailTriggersTasks(WebDriver driver) throws Exception {
		Reporter.log("Navigating to adminFranchiseOpenerConfigureAlertEmailTriggersTasks...");
		adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openConfigureAlertEmailTriggersTasksLnk(driver);
	}

	public void adminFranchiseOpenerConfigureMilestoneDateTriggers(WebDriver driver) throws Exception {
		Reporter.log("Navigating to adminFranchiseOpenerMilestoneDateTriggers...");
		adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openConfigureMilestoneDateTriggersLnk(driver);

	}

	public void openerStoreSummary(WebDriver driver) throws Exception {
		Reporter.log("Navigating to openerStoreSummary...");
		openerModule(driver);

		opener().opener_common().openOpnerStoreSummary(home_page(), driver);
	}

	public void openerSearch(WebDriver driver) throws Exception {
		Reporter.log("Navigating to openerSearch...");
		openerModule(driver);

		opener().opener_common().openOpenerSearch(home_page(), driver);
	}

	public void openerTasks(WebDriver driver) throws Exception {
		Reporter.log("Navigating to openerTasks...");
		openerModule(driver);

		opener().opener_common().openOpnerTasks(home_page(), driver);

	}

	public void openerArchived(WebDriver driver) throws Exception {
		Reporter.log("Navigating to openerArchived...");
		openerModule(driver);
		opener().opener_common().openOpnerArchived(home_page(), driver);
	}

	public void openReports(WebDriver driver) throws Exception {
		Reporter.log("Navigating to OpenerReports...");
		openerModule(driver);
		opener().opener_common().openOpenerReports(home_page(), driver);
	}

	public void openOpnerArchived(FCHomePageTest fcHomePageTest, WebDriver driver) throws Exception {

		FCHomePage pobj = new FCHomePage(driver);
		fcHomePageTest.utobj().sleep(5000);
		fcHomePageTest.utobj().clickElement(driver, pobj.openerArchived);
		Reporter.log("Navigating to Opner Archived Page...");
	}

	public void openOpenerSearch(FCHomePageTest fcHomePageTest, WebDriver driver) throws Exception {

		FCHomePage pobj = new FCHomePage(driver);
		fcHomePageTest.utobj().sleep(5000);
		fcHomePageTest.utobj().clickElement(driver, pobj.openerSearch);
		Reporter.log("Navigating to Opner Search Page...");
	}

	public void openOpnerStoreSummary(FCHomePageTest fcHomePageTest, WebDriver driver) throws Exception {

		FCHomePage pobj = new FCHomePage(driver);
		fcHomePageTest.utobj().sleep(5000);
		fcHomePageTest.utobj().clickElement(driver, pobj.storeSummary);
		Reporter.log("Navigating to Opner Store Summary Page...");
	}

	public void openOpnerTasks(FCHomePageTest fcHomePageTest, WebDriver driver) throws Exception {

		FCHomePage pobj = new FCHomePage(driver);
		fcHomePageTest.utobj().sleep(5000);
		fcHomePageTest.utobj().clickElement(driver, pobj.openerTasks);
		Reporter.log("Navigating to Opner Tasks Page...");
	}

	public void openOpenerReports(FCHomePageTest fcHomePageTest, WebDriver driver) throws Exception {

		FCHomePage pobj = new FCHomePage(driver);
		fcHomePageTest.utobj().sleep(5000);
		fcHomePageTest.utobj().clickElement(driver, pobj.openerReports);
		Reporter.log("Navigating to Opner Reports Page...");
	}

}
