package com.builds.test.salesTest;

import java.util.ArrayList;

class AdminCorporateUser {
	
	// User Details
		private String loginID;
		private String password;
		private String confirmPassword;
		private String userType;
		private String expirationTime;
		ArrayList<String> role = new ArrayList<String>();
		private String timeZone;
		private String allowDST;
		
	// Personal Details
		private String jobTitle;
		private String language;
		private String firstName;
		private String lastName;
		private String address;
		private String city;
		private String country;
		private String zipPostalCode;
		private String stateProvince;
		private String phone1;
		private String phone1Extension;
		private String phone2;
		private String phone2Extension;
		private String fax;
		private String mobile;
		private String email;
		private String ipAddress;
		private String isBillable;
		private String consultant;
		private String uploadUserPicture;
		private String sendNotification;
		
		public String userFullName = firstName + " " + lastName ;
		
	//	public String fullname = firstName.concat(" ").concat(lastName);
	
	public String getLoginID() {
		return loginID;
	}
	public void setLoginID(String loginID) {
		this.loginID = loginID;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getConfirmPassword() {
		return confirmPassword;
	}
	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	public String getExpirationTime() {
		return expirationTime;
	}
	public void setExpirationTime(String expirationTime) {
		this.expirationTime = expirationTime;
	}
	
	public ArrayList<String> getRole() {
		return role;
	}
	public void setRole(ArrayList<String> role) {
		this.role = role;
	}
	
	public String getTimeZone() {
		return timeZone;
	}
	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}
	public String getAllowDST() {
		return allowDST;
	}
	public void setAllowDST(String allowDST) {
		this.allowDST = allowDST;
	}
	public String getJobTitle() {
		return jobTitle;
	}
	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getZipPostalCode() {
		return zipPostalCode;
	}
	public void setZipPostalCode(String zipPostalCode) {
		this.zipPostalCode = zipPostalCode;
	}
	public String getStateProvince() {
		return stateProvince;
	}
	public void setStateProvince(String stateProvince) {
		this.stateProvince = stateProvince;
	}
	public String getPhone1() {
		return phone1;
	}
	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}
	public String getPhone1Extension() {
		return phone1Extension;
	}
	public void setPhone1Extension(String phone1Extension) {
		this.phone1Extension = phone1Extension;
	}
	public String getPhone2() {
		return phone2;
	}
	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}
	public String getPhone2Extension() {
		return phone2Extension;
	}
	public void setPhone2Extension(String phone2Extension) {
		this.phone2Extension = phone2Extension;
	}
	public String getFax() {
		return fax;
	}
	public void setFax(String fax) {
		this.fax = fax;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public String getIsBillable() {
		return isBillable;
	}
	public void setIsBillable(String isBillable) {
		this.isBillable = isBillable;
	}
	public String getConsultant() {
		return consultant;
	}
	public void setConsultant(String consultant) {
		this.consultant = consultant;
	}
	public String getUploadUserPicture() {
		return uploadUserPicture;
	}
	public void setUploadUserPicture(String uploadUserPicture) {
		this.uploadUserPicture = uploadUserPicture;
	}
	public String getSendNotification() {
		return sendNotification;
	}
	public void setSendNotification(String sendNotification) {
		this.sendNotification = sendNotification;
	}
	


}
