package com.builds.test.salesTest;

import org.openqa.selenium.WebDriver;

import com.builds.test.admin.AdminPageTest;
import com.builds.test.admin.AdminUsersManageCorporateUsersPageTest;
import com.builds.test.common.CorporateUser;
import com.builds.test.common.WebService;
import com.builds.uimaps.common.CorporateUserUI;
import com.builds.uimaps.fs.AdminCorporateUserUI;
import com.builds.utilities.FranconnectUtil;

class AdminCorporateUserTest {
	
	WebDriver driver;
	FranconnectUtil fc = new FranconnectUtil();
	
	AdminCorporateUserTest(WebDriver driver) {
		this.driver = driver;
	}
	
	AdminCorporateUserTest addCorporateUser(AdminCorporateUser adminCorporateUser) throws Exception {
		AdminCorporateUserUI adminCorporateUserUI = new AdminCorporateUserUI(driver);
		
		fc.utobj().printTestStep("Adding Corporate User");
		
		fc.utobj().clickElement(driver, adminCorporateUserUI.addCorporateUserBtn);
		fill_addCorporateUser(adminCorporateUser);
		fc.utobj().clickElement(driver, adminCorporateUserUI.addBtn);
		
		return new AdminCorporateUserTest(driver);
		
	}
	
	void fill_addCorporateUser(AdminCorporateUser adminCorporateUser) throws Exception {
		AdminCorporateUserUI adminCorporateUserUI = new AdminCorporateUserUI(driver);

		fc.utobj().printTestStep("Fill details in Add Corporate User");
		
		if(adminCorporateUser.getLoginID()!=null)
		{
			fc.utobj().sendKeys(driver, adminCorporateUserUI.loginID, adminCorporateUser.getLoginID());
		}
		
		if(adminCorporateUser.getPassword()!=null)
		{
			fc.utobj().sendKeys(driver, adminCorporateUserUI.password, adminCorporateUser.getPassword());
		}	
		
		if(adminCorporateUser.getConfirmPassword()!=null)
		{
			fc.utobj().sendKeys(driver, adminCorporateUserUI.confirmPassword, adminCorporateUser.getConfirmPassword());
		}
		
		if(adminCorporateUser.getUserType()!=null)
		{
			fc.utobj().selectDropDown(driver, adminCorporateUserUI.userType, adminCorporateUser.getUserType());
		}
		
		if(adminCorporateUser.getExpirationTime()!=null)
		{
			fc.utobj().selectDropDown(driver, adminCorporateUserUI.expirationTime, adminCorporateUser.getExpirationTime());
		}
		
		if(adminCorporateUser.getRole()!=null)
		{
			fc.utobj().selectMultipleValFromMultiSelect(driver, adminCorporateUserUI.role, adminCorporateUser.getRole());
		}
		
		if(adminCorporateUser.getTimeZone()!=null)
		{
			fc.utobj().selectDropDown(driver, adminCorporateUserUI.timeZone, adminCorporateUser.getTimeZone());
		}
		
		if(adminCorporateUser.getAllowDST()!=null)
		{
			if(adminCorporateUser.getAllowDST().equalsIgnoreCase("Yes"))
			{
				if(!fc.utobj().isSelected(driver, adminCorporateUserUI.allowDST)) 
				{
					fc.utobj().clickElement(driver, adminCorporateUserUI.allowDST);
				}
			}
			
			if(adminCorporateUser.getAllowDST().equalsIgnoreCase("no"))
			{
				if(fc.utobj().isSelected(driver, adminCorporateUserUI.allowDST))
				{
					fc.utobj().clickElement(driver, adminCorporateUserUI.allowDST);
				}
			}
		}
		
		// PERSONAL DETAILS
		
		if(adminCorporateUser.getJobTitle()!=null)
		{
			fc.utobj().sendKeys(driver, adminCorporateUserUI.jobTitle, adminCorporateUser.getJobTitle());
		}
		
		if(adminCorporateUser.getLanguage()!=null)
		{
			fc.utobj().selectDropDown(driver, adminCorporateUserUI.language, adminCorporateUser.getLanguage());
		}
		
		if(adminCorporateUser.getFirstName()!=null)
		{
			fc.utobj().sendKeys(driver, adminCorporateUserUI.firstName, adminCorporateUser.getFirstName());
		}
		
		if(adminCorporateUser.getLastName()!=null)
		{
			fc.utobj().sendKeys(driver, adminCorporateUserUI.lastName, adminCorporateUser.getLastName());
		}
		
		if(adminCorporateUser.getAddress()!=null)
		{
			fc.utobj().sendKeys(driver, adminCorporateUserUI.address, adminCorporateUser.getAddress());
		}
		
		if(adminCorporateUser.getCity()!=null)
		{
			fc.utobj().sendKeys(driver, adminCorporateUserUI.city, adminCorporateUser.getCity());
		}
		
		if(adminCorporateUser.getCountry()!=null)
		{
			fc.utobj().selectDropDown(driver, adminCorporateUserUI.country, adminCorporateUser.getCountry());
		}
		
		if(adminCorporateUser.getZipPostalCode()!=null)
		{
			fc.utobj().sendKeys(driver, adminCorporateUserUI.zipPostalCode, adminCorporateUser.getZipPostalCode());
		}
		
		if(adminCorporateUser.getStateProvince()!=null)
		{
			fc.utobj().selectDropDown(driver, adminCorporateUserUI.stateProvince, adminCorporateUser.getStateProvince());
		}
		
		if(adminCorporateUser.getPhone1()!=null)
		{
			fc.utobj().sendKeys(driver, adminCorporateUserUI.phone1, adminCorporateUser.getPhone1());
		}
		
		if(adminCorporateUser.getPhone1Extension()!=null)
		{
			fc.utobj().sendKeys(driver, adminCorporateUserUI.phone1Extension, adminCorporateUser.getPhone1Extension());
		}
		
		if(adminCorporateUser.getPhone2()!=null)
		{
			fc.utobj().sendKeys(driver, adminCorporateUserUI.phone2, adminCorporateUser.getPhone2());
		}
		
		if(adminCorporateUser.getPhone2Extension()!=null)
		{
			fc.utobj().sendKeys(driver, adminCorporateUserUI.phone2Extension, adminCorporateUser.getPhone2Extension());
		}
		
		if(adminCorporateUser.getFax()!=null)
		{
			fc.utobj().sendKeys(driver, adminCorporateUserUI.fax, adminCorporateUser.getFax());
		}
		
		if(adminCorporateUser.getMobile()!=null)
		{
			fc.utobj().sendKeys(driver, adminCorporateUserUI.mobile, adminCorporateUser.getMobile());
		}
		
		if(adminCorporateUser.getEmail()!=null)
		{
			fc.utobj().sendKeys(driver, adminCorporateUserUI.email, adminCorporateUser.getEmail());
		}
		
		if(adminCorporateUser.getIpAddress()!=null)
		{
			fc.utobj().sendKeys(driver, adminCorporateUserUI.ipAddress, adminCorporateUser.getIpAddress());
		}
		
		if(adminCorporateUser.getIsBillable()!=null)
		{
			
			if(adminCorporateUser.getIsBillable().equalsIgnoreCase("Yes"))
			{
				if(!fc.utobj().isSelected(driver, adminCorporateUserUI.isBillable)) 
				{
					fc.utobj().clickElement(driver, adminCorporateUserUI.isBillable);
				}
			}
			
			if(adminCorporateUser.getIsBillable().equalsIgnoreCase("no"))
			{
				if(fc.utobj().isSelected(driver, adminCorporateUserUI.isBillable))
				{
					fc.utobj().clickElement(driver, adminCorporateUserUI.isBillable);
				}
			}
		}
		
		if(adminCorporateUser.getConsultant()!=null)
		{
			
			if(adminCorporateUser.getConsultant().equalsIgnoreCase("Yes"))
			{
				if(!fc.utobj().isSelected(driver, adminCorporateUserUI.consultant)) 
				{
					fc.utobj().clickElement(driver, adminCorporateUserUI.consultant);
				}
			}
			
			if(adminCorporateUser.getConsultant().equalsIgnoreCase("no"))
			{
				if(fc.utobj().isSelected(driver, adminCorporateUserUI.consultant))
				{
					fc.utobj().clickElement(driver, adminCorporateUserUI.consultant);
				}
			}
		}
			
		if(adminCorporateUser.getUploadUserPicture()!=null)
		{
			fc.utobj().sendKeys(driver, adminCorporateUserUI.uploadUserPicture, adminCorporateUser.getUploadUserPicture());
		}
		
		if(adminCorporateUser.getSendNotification()!=null)
		{
			
			if(adminCorporateUser.getSendNotification().equalsIgnoreCase("Yes"))
			{
				if(!fc.utobj().isSelected(driver, adminCorporateUserUI.sendNotification)) 
				{
					fc.utobj().clickElement(driver, adminCorporateUserUI.sendNotification);
				}
			}
			
			if(adminCorporateUser.getSendNotification().equalsIgnoreCase("no"))
			{
				if(fc.utobj().isSelected(driver, adminCorporateUserUI.sendNotification))
				{
					fc.utobj().clickElement(driver, adminCorporateUserUI.sendNotification);
				}
			}
		}
		
	} 
	
	void verify_addCorporateUser(AdminCorporateUser adminCorporateUser) throws Exception
	{
		AdminCorporateUserUI adminCorporateUserUI = new AdminCorporateUserUI(driver);
		fc.utobj().printTestStep("Verify the Coporate User in  (Users >  Manage Corporate Users)");
		fc.utobj().sendKeys(driver, adminCorporateUserUI.searchByUserName_TextBox, adminCorporateUser.getFirstName().concat(" ").concat(adminCorporateUser.getLastName()));
		fc.utobj().clickElement(driver, adminCorporateUserUI.searchBtn);
		if(! fc.utobj().assertLinkText(driver, adminCorporateUser.getFirstName().concat(" ").concat(adminCorporateUser.getLastName())))
		{
			fc.utobj().throwsException("Add Corporate User Verification FAILED");
		}
	}
	
	private void addUser() throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		AdminCorporateUser adminCorporateUser = new AdminCorporateUser();
		Sales_Common_New sales_Common = new Sales_Common_New();
		adminCorporateUser = sales_Common.fillDefaultDataFor_CorporateUser(adminCorporateUser);

		System.out.println(adminCorporateUser.getLoginID());
		System.out.println(adminCorporateUser.getFirstName());
		System.out.println(adminCorporateUser.getLastName());

		WebDriver driver = null;
		try {
			createDefaultCorporateUser_Through_WebService_AfterLogin(driver, adminCorporateUser);
		} catch (Exception e) {
			System.out.println("Error");
			// fc.utobj().quitBrowserOnCatch(driver, config, e, testCaseId);
		}
	}

	AdminCorporateUser createDefaultCorporateUser_Through_WebService_AfterLogin(WebDriver driver, AdminCorporateUser adminCorporateUser) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();

		String responseMessage = null;

		String module = "admin";
		String subModule = "user";
		String isISODate = "no";

		fc.utobj().printTestStep("Adding User through Web Services.");
		WebService rest = new WebService();

		String xmlString = "<fcRequest>" + "<adminUser>" + "<userID>" + adminCorporateUser.getLoginID() + "</userID>"
				+ "<password>" + adminCorporateUser.getPassword() + "</password>" + "<userLevel>Corporate</userLevel>"
				+ "<timezone>" + adminCorporateUser.getTimeZone() + "</timezone>" + "<type>" + adminCorporateUser.getUserType() + "</type>"
				+ "<expirationTime>" + adminCorporateUser.getExpirationTime() + "</expirationTime>" + "<role>" + adminCorporateUser.getRole()
				+ "</role>" + "<jobTitle>" + adminCorporateUser.getJobTitle() + "</jobTitle>" + "<firstname>"
				+ adminCorporateUser.getFirstName() + "</firstname>" + "<lastname>" + adminCorporateUser.getLastName() + "</lastname>"
				+ "<address>" + adminCorporateUser.getAddress() + "</address>" + "<city>" + adminCorporateUser.getCity() + "</city>"
				+ "<state>" + adminCorporateUser.getStateProvince() + "</state>" + "<zipCode>" + adminCorporateUser.getZipPostalCode() + "</zipCode>"
				+ "<country>" + adminCorporateUser.getCountry() + "</country>" + "<phone1>" + adminCorporateUser.getPhone1() + "</phone1>"
				+ "<phoneExt1>" + adminCorporateUser.getPhone1Extension() + "</phoneExt1>" + "<phone2>" + adminCorporateUser.getPhone2()
				+ "</phone2>" + "<phoneExt2>" + adminCorporateUser.getPhone2Extension() + "</phoneExt2>" + "<fax>" + adminCorporateUser.getFax()
				+ "</fax>" + "<mobile>" + adminCorporateUser.getMobile() + "</mobile>" + "<email>" + adminCorporateUser.getEmail()
				+ "</email>" + "<sendNotification>" + adminCorporateUser.getSendNotification() + "</sendNotification>"
				+ "<loginUserIp>" + adminCorporateUser.getIpAddress() + "</loginUserIp>" + "<isBillable>"
				+ adminCorporateUser.getIsBillable() + "</isBillable>" + "<isDaylight>" + adminCorporateUser.getAllowDST()
				+ "</isDaylight>" + "<isAuditor>" + adminCorporateUser.getConsultant() + "</isAuditor>" + "</adminUser>"
				+ "</fcRequest>";

		try {
			responseMessage = rest.create(module, subModule, xmlString, isISODate);
		} catch (Exception e) {

		}

		responseMessage = "success";/*
									 * verifyCorporateUser_ThroughWebService(
									 * corpUser);
									 */

		if (!responseMessage.contains("success")) {

			AdminUsersManageCorporateUsersPageTest manageCorporate = new AdminUsersManageCorporateUsersPageTest();
			AdminPageTest admin = new AdminPageTest();
			admin.adminPage(driver);
			admin.openCorporateUserPage(driver);

			manageCorporate.addCorporateUsers(driver);

			CorporateUserUI ui = new CorporateUserUI(driver);

			if (adminCorporateUser.getLoginID() != null) {
				fc.utobj().sendKeys(driver, ui.userName, adminCorporateUser.getLoginID() + "1");
			}

			if (adminCorporateUser.getPassword() != null) {
				fc.utobj().sendKeys(driver, ui.password, adminCorporateUser.getPassword());
			}

			if (adminCorporateUser.getConfirmPassword() != null) {
				fc.utobj().sendKeys(driver, ui.confirmPassword, adminCorporateUser.getConfirmPassword());
			}

			if (adminCorporateUser.getConfirmPassword() != null) {
				fc.utobj().sendKeys(driver, ui.confirmPassword, adminCorporateUser.getConfirmPassword());
			}

			if (adminCorporateUser.getUserType() != null) {
				fc.utobj().selectDropDownByVisibleText(driver, ui.userType, adminCorporateUser.getUserType());
			}

			if (adminCorporateUser.getRole() != null) {
				fc.utobj().selectMultipleValFromMultiSelect(driver, ui.role_MultiSelect, adminCorporateUser.getRole());
			}

			if (adminCorporateUser.getTimeZone() != null) {
				fc.utobj().selectDropDownByVisibleText(driver, ui.timezone_Select, adminCorporateUser.getTimeZone());
			}

			if (adminCorporateUser.getAllowDST() != null) {
				fc.utobj().check(ui.isDaylight_Check, adminCorporateUser.getAllowDST());
			}

			if (adminCorporateUser.getJobTitle() != null) {
				fc.utobj().sendKeys(driver, ui.jobTitle, adminCorporateUser.getJobTitle());
			}

			if (adminCorporateUser.getLanguage() != null) {
				fc.utobj().selectDropDownByVisibleText(driver, ui.userLanguage, adminCorporateUser.getLanguage());
			}

			if (adminCorporateUser.getFirstName() != null) {
				fc.utobj().sendKeys(driver, ui.firstName, adminCorporateUser.getFirstName());
			}

			if (adminCorporateUser.getLastName() != null) {
				fc.utobj().sendKeys(driver, ui.lastName, adminCorporateUser.getLastName());
			}

			if (adminCorporateUser.getCity() != null) {
				fc.utobj().sendKeys(driver, ui.city, adminCorporateUser.getCity());
			}

			if (adminCorporateUser.getCountry() != null) {
				fc.utobj().selectDropDownByVisibleText(driver, ui.country, adminCorporateUser.getCountry());
			}

			if (adminCorporateUser.getStateProvince() != null) {
				fc.utobj().selectDropDownByVisibleText(driver, ui.state, adminCorporateUser.getStateProvince());
			}

			if (adminCorporateUser.getPhone1() != null) {
				fc.utobj().sendKeys(driver, ui.phone1, adminCorporateUser.getPhone1());
			}

			if (adminCorporateUser.getEmail() != null) {
				fc.utobj().sendKeys(driver, ui.phone1, adminCorporateUser.getEmail());
			}

			fc.utobj().clickElement(driver, ui.submit);

		}

		return adminCorporateUser;
	}

	String verifyCorporateUser_ThroughWebService(AdminCorporateUser adminCorporateUser) throws Exception {
		String module = "admin";
		String subModule = "user";
		String isISODate = "no";

		String xmlString = "<fcRequest>" + "<filter>" + "<referenceId></referenceId>" + "<userID>"
				+ adminCorporateUser.getLoginID() + "</userID>" + "<firstname>" + adminCorporateUser.getFirstName() + "</firstname>"
				+ "<lastname>" + adminCorporateUser.getLastName() + "</lastname>" + "</filter>" + "</fcRequest>";

		WebService service = new WebService();
		String responseMessage = service.retrieve(module, subModule, xmlString, isISODate);

		return responseMessage;

	}
	
	void delete_CorpUser(String userName) throws Exception
	{
		AdminCorporateUserUI adminCorporateUserUI = new AdminCorporateUserUI(driver);
		FranconnectUtil fc = new FranconnectUtil();
		
		search_CorpUser(userName);
		fc.utobj().singleActionIcon(driver, "Delete");
		if(fc.utobj().isAlertPresent(driver))
		{
		fc.utobj().acceptAlertBox(driver);
		}
		
		fc.utobj().switchFrameById(driver, "cboxIframe");
		
		if(fc.utobj().isElementPresent(driver, adminCorporateUserUI.delete_Btn))
		{
			fc.utobj().clickElement(driver, adminCorporateUserUI.delete_Btn);
		}
				
		// fc.utobj().switchFrameById(driver, "progressIframe");
		fc.utobj().clickElement(driver, adminCorporateUserUI.closeBtn_onConfirmation);

	}
	
	void search_CorpUser(String userName) throws Exception
	{
		AdminCorporateUserUI adminCorporateUserUI = new AdminCorporateUserUI(driver);
		fc.utobj().sendKeys(driver, adminCorporateUserUI.searchByUserName_TextBox, userName);
		fc.utobj().clickElement(driver, adminCorporateUserUI.searchBtn);
	}
	

}
