package com.builds.test.salesTest;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.admin.AdminUI;
import com.builds.utilities.FranconnectUtil;

class AdminSales {
	private FranconnectUtil fc = new FranconnectUtil();
	private WebDriver driver;

	AdminSales(WebDriver driver) throws Exception {
		this.driver = driver;
	}

	AdminSalesLeadStatusTest click_AdminSalesLeadStatus() throws Exception {
		fc.utobj().printTestStep("Click AdminSalesLeadStatus");
		AdminUI adminUI = new AdminUI(driver);
		fc.utobj().clickElement(driver, adminUI.statusLink);
		return new AdminSalesLeadStatusTest(driver);
	}

	ManageFormGeneratorTest click_ManageFormGenerator() throws Exception {
		fc.utobj().printTestStep("Click Manage Form Generator");
		AdminUI adminUI = new AdminUI(driver);
		fc.utobj().clickElement(driver, adminUI.manageFormGenerator);
		return new ManageFormGeneratorTest(driver);
	}

	ManageWebformGeneratorTest click_ManageWebFormGenerator() throws Exception {
		fc.utobj().printTestStep("Click Manage Web Form Generator");
		AdminUI adminUI = new AdminUI(driver);
		fc.utobj().clickElement(driver, adminUI.manageWebFormGeneratorLink);
		return new ManageWebformGeneratorTest(driver);
	}

	ConfigureWebFormSettingsTest click_ConfigureWebFormSettings() throws Exception {
		fc.utobj().printTestStep("Click Configure Web Form Settings");
		AdminUI adminUI = new AdminUI(driver);
		fc.utobj().clickElement(driver, adminUI.configureWebformSettings);
		return new ConfigureWebFormSettingsTest(driver);
	}

	ConfigureServiceListsTest click_ConfigureServiceListsTest() throws Exception {
		fc.utobj().printTestStep("Click Configure Service Lists");
		AdminUI adminUI = new AdminUI(driver);
		fc.utobj().clickElement(driver, adminUI.configureServiceListsLink);
		return new ConfigureServiceListsTest(driver);
	}

	ManageSalesTerritoriesPage click_SalesTerritoriesTest() throws Exception {
		fc.utobj().printTestStep("Click on Sales Terriotories");
		AdminUI adminUI = new AdminUI(driver);
		fc.utobj().clickElement(driver, adminUI.salesTerritories);
		return new ManageSalesTerritoriesPage(driver);
	}

	DefineUnregisteredStatesProvincesTest click_DefineUnregisteredStatesProvincesTest() throws Exception {
		fc.utobj().printTestStep("Click on Define Unregistered States Provinces");
		AdminUI adminUI = new AdminUI(driver);
		fc.utobj().clickElement(driver, adminUI.defineUnregisteredStatesProvinces);
		return new DefineUnregisteredStatesProvincesTest(driver);
	}

	SetupSalesLeadOwnersTest click_SetupSalesLeadOwnersTest() throws Exception {
		fc.utobj().printTestStep("Click Setup Sales Lead Owners");
		AdminUI adminUI = new AdminUI(driver);
		fc.utobj().clickElement(driver, adminUI.setupFranchiseSalesLeadOwners);
		return new SetupSalesLeadOwnersTest(driver);
	}

	AssignLeadOwnersTest click_AssignLeadOwnersTest() throws Exception {
		fc.utobj().printTestStep("Click Assign Lead Owners");
		AdminUI adminUI = new AdminUI(driver);
		fc.utobj().clickElement(driver, adminUI.assignLeadOwners);
		return new AssignLeadOwnersTest(driver);
	}

	ConfigureDuplicateCriteriaTest click_ConfigureDuplicateCriteriaTest() throws Exception {
		fc.utobj().printTestStep("Click Configure Duplicate Criteria");
		AdminUI adminUI = new AdminUI(driver);
		fc.utobj().clickElement(driver, adminUI.configureDuplicateCriteria);
		return new ConfigureDuplicateCriteriaTest(driver);
	}

	ConfigureCoApplicantRelationshipTest click_ConfigureCoApplicantRelationshipTest() throws Exception {
		fc.utobj().printTestStep("Click Configure Co-Applicant Relationship");
		AdminUI adminUI = new AdminUI(driver);
		fc.utobj().clickElement(driver, adminUI.configureCoApplicantRelationshipLink);
		return new ConfigureCoApplicantRelationshipTest(driver);
	}

	// Configure Content for Default Emails and Web Form

	ConfigureEmailContentSentToLeadOwnerTest click_ConfigureEmailContentSentToLeadOwnerTest() throws Exception {
		fc.utobj().printTestStep("Click Configure Email Content Sent To Lead Owner");
		AdminUI adminUI = new AdminUI(driver);
		fc.utobj().clickElement(driver, adminUI.salesConfigureEmailContent);
		return new ConfigureEmailContentSentToLeadOwnerTest(driver);
	}

	ConfigureWebFormEmailContentTest click_ConfigureWebFormEmailContentTest() throws Exception {
		fc.utobj().printTestStep("Click Configure Web Form Email Content");
		AdminUI adminUI = new AdminUI(driver);
		fc.utobj().clickElement(driver, adminUI.configureWebFormEmailContent);
		return new ConfigureWebFormEmailContentTest(driver);
	}

	ConfigureWeeklyUpdatesEmailTest click_ConfigureWeeklyUpdatesEmailTest() throws Exception {
		fc.utobj().printTestStep("Click Configure Weekly Updates Email");
		AdminUI adminUI = new AdminUI(driver);
		fc.utobj().clickElement(driver, adminUI.configureWeeklyUpdatesEmail);
		return new ConfigureWeeklyUpdatesEmailTest(driver);
	}

	// Configure Sales Process Steps

	AdminSalesLeadStatusTest click_AdminSalesLeadStatusTest() throws Exception {
		fc.utobj().printTestStep("Click Admin Sales Lead Status");
		AdminUI adminUI = new AdminUI(driver);
		fc.utobj().clickElement(driver, adminUI.configureStatus);
		return new AdminSalesLeadStatusTest(driver);
	}

	ConfigureOptOutStatusTest click_ConfigureOptOutStatusTest() throws Exception {
		fc.utobj().printTestStep("Click Configure Opt Out Status");
		AdminUI adminUI = new AdminUI(driver);
		fc.utobj().clickElement(driver, adminUI.configureOptOutStatus);
		return new ConfigureOptOutStatusTest(driver);
	}

	// Define Lead Sources

	SourceTest click_SourceTest() throws Exception {
		fc.utobj().printTestStep("Click Source");
		AdminUI adminUI = new AdminUI(driver);
		fc.utobj().clickElement(driver, adminUI.sourceLink);
		return new SourceTest(driver);
	}

	// Broker's Settings

	BrokersTypeConfigurationTest click_BrokersTypeConfigurationTest() throws Exception {
		fc.utobj().printTestStep("Click Brokers Type Configuration");
		AdminUI adminUI = new AdminUI(driver);
		fc.utobj().clickElement(driver, adminUI.brokersTypeConfigurationLink);
		return new BrokersTypeConfigurationTest(driver);
	}

	BrokersAgencyTest click_BrokersAgencyTest() throws Exception {
		fc.utobj().printTestStep("Click Brokers Agency");
		AdminUI adminUI = new AdminUI(driver);
		fc.utobj().clickElement(driver, adminUI.brokersAgencyLink);
		return new BrokersAgencyTest(driver);
	}

	ConfigureBrokersEmailCampaignSettingTest click_ConfigureBrokersEmailCampaignSettingTest() throws Exception {
		fc.utobj().printTestStep("Click Configure Brokers Email Campaign Setting");
		AdminUI adminUI = new AdminUI(driver);
		fc.utobj().clickElement(driver, adminUI.configureBrokersEmailCampaignSetting);
		return new ConfigureBrokersEmailCampaignSettingTest(driver);
	}

	// Define Lead Attributes

	ForecastRatingTest click_ForecastRatingTest() throws Exception {
		fc.utobj().printTestStep("Click Forecast Rating");
		AdminUI adminUI = new AdminUI(driver);
		fc.utobj().clickElement(driver, adminUI.forecastRatingLink);
		return new ForecastRatingTest(driver);
	}

	ConfigureLeadRatingTest click_ConfigureLeadRatingTest() throws Exception {
		fc.utobj().printTestStep("Click Configure Lead Rating");
		AdminUI adminUI = new AdminUI(driver);
		fc.utobj().clickElement(driver, adminUI.configureLeadRatingLink);
		return new ConfigureLeadRatingTest(driver);
	}

	ConfigureLeadKilledReasonTest click_ConfigureLeadKilledReasonTest() throws Exception {
		fc.utobj().printTestStep("Click Configure Lead Killed Reason");
		AdminUI adminUI = new AdminUI(driver);
		fc.utobj().clickElement(driver, adminUI.configureLeadKilledReasonLink);
		return new ConfigureLeadKilledReasonTest(driver);
	}

	ConfigureQualificationChecklistsTest click_ConfigureQualificationChecklistsTest() throws Exception {
		fc.utobj().printTestStep("Click Configure Qualification Checklists");
		AdminUI adminUI = new AdminUI(driver);
		fc.utobj().clickElement(driver, adminUI.configureQualificationChecklists);
		return new ConfigureQualificationChecklistsTest(driver);
	}

	ConfigureHeatMeterTest click_ConfigureHeatMeterTest() throws Exception {
		fc.utobj().printTestStep("Click Configure Heat Meter");
		AdminUI adminUI = new AdminUI(driver);
		fc.utobj().clickElement(driver, adminUI.configureHeatMeter);
		return new ConfigureHeatMeterTest(driver);
	}

	// Define Marketing and Advertising Costs

	ConfigureMarketingCostsTest click_ConfigureMarketingCostsTest() throws Exception {
		fc.utobj().printTestStep("Click Configure Marketing Costs");
		AdminUI adminUI = new AdminUI(driver);
		fc.utobj().clickElement(driver, adminUI.configureMarketingCosts);
		return new ConfigureMarketingCostsTest(driver);
	}

	MarketingCodeTest click_MarketingCodeTest() throws Exception {
		fc.utobj().printTestStep("Click Marketing Code");
		AdminUI adminUI = new AdminUI(driver);
		fc.utobj().clickElement(driver, adminUI.marketingCodeLink);
		return new MarketingCodeTest(driver);
	}

	// Virtual Brochure Settings

	ConfigureVirtualBrochureCampaignSettingsTest click_ConfigureVirtualBrochureCampaignSettingsTest() throws Exception {
		fc.utobj().printTestStep("Click Configure Virtual Brochure Campaign Settings");
		AdminUI adminUI = new AdminUI(driver);
		fc.utobj().clickElement(driver, adminUI.configureVirtualBrochureCampaignSettings);
		return new ConfigureVirtualBrochureCampaignSettingsTest(driver);
	}

	ManageCandidatePortalTest click_ManageCandidatePortalTest() throws Exception {
		fc.utobj().printTestStep("Click Manage Candidate Portal");
		AdminUI adminUI = new AdminUI(driver);
		fc.utobj().clickElement(driver, adminUI.manageCandidatePortal);
		return new ManageCandidatePortalTest(driver);
	}

	// Integrations

	ConfigureDocuSignSettingsTest click_ConfigureDocuSignSettingsTest() throws Exception {
		fc.utobj().printTestStep("Click Configure Docu Sign Settings");
		AdminUI adminUI = new AdminUI(driver);
		fc.utobj().clickElement(driver, adminUI.configureDocuSignSettings);
		return new ConfigureDocuSignSettingsTest(driver);
	}

	ConfigureBoeflyIntegrationDetailsTest click_ConfigureBoeflyIntegrationDetailsTest() throws Exception {
		fc.utobj().printTestStep("Click Configure Boefly Integration Details");
		AdminUI adminUI = new AdminUI(driver);
		fc.utobj().clickElement(driver, adminUI.configureBoeflyIntegrationDetails);
		return new ConfigureBoeflyIntegrationDetailsTest(driver);
	}

	ConfigureNathanProfilerIntegrationDetailsTest click_ConfigureNathanProfilerIntegrationDetailsTest()
			throws Exception {
		fc.utobj().printTestStep("Click Configure Nathan Profiler Integration Details");
		AdminUI adminUI = new AdminUI(driver);
		fc.utobj().clickElement(driver, adminUI.ConfigureNathanProfilerIntegrationDetails);
		return new ConfigureNathanProfilerIntegrationDetailsTest(driver);
	}

	ConfigureProvenMatchIntegrationDetailsTest click_ConfigureProvenMatchIntegrationDetailsTest() throws Exception {
		fc.utobj().printTestStep("Click Configure Proven Match Integration Details");
		AdminUI adminUI = new AdminUI(driver);
		fc.utobj().clickElement(driver, adminUI.ConfigureProvenMatchIntegrationDetails);
		return new ConfigureProvenMatchIntegrationDetailsTest(driver);
	}

	ConfigureSMSTest click_ConfigureSMSTest() throws Exception {
		fc.utobj().printTestStep("Click Configure SMS");
		AdminUI adminUI = new AdminUI(driver);
		fc.utobj().clickElement(driver, adminUI.configureSMS);
		return new ConfigureSMSTest(driver);
	}

	ConfigureSearchCriteriaForApiPluginsTest click_ConfigureSearchCriteriaForApiPluginsTest() throws Exception {
		fc.utobj().printTestStep("Click Configure Search Criteria For Api Plugins");
		AdminUI adminUI = new AdminUI(driver);
		fc.utobj().clickElement(driver, adminUI.configureSearchCriteriaForAPIPlugins);
		return new ConfigureSearchCriteriaForApiPluginsTest(driver);
	}

	ConfigureEmailCaptureTest click_ConfigureEmailCaptureTest() throws Exception {
		fc.utobj().printTestStep("Click Configure Email Capture");
		AdminUI adminUI = new AdminUI(driver);
		fc.utobj().clickElement(driver, adminUI.configureEmailCapture);
		return new ConfigureEmailCaptureTest(driver);
	}

	// FDD Management

	FDDManagementTest click_FDDManagement() throws Exception {
		fc.utobj().printTestStep("Click FDD Management");
		AdminUI adminUI = new AdminUI(driver);
		fc.utobj().clickElement(driver, adminUI.fddManagementLink);
		return new FDDManagementTest(driver);
	}

	LogOnCredentialsDurationTest click_LogOnCredentialsDurationTest() throws Exception {
		fc.utobj().printTestStep("Click Log On Credentials Duration");
		AdminUI adminUI = new AdminUI(driver);
		fc.utobj().clickElement(driver, adminUI.logOnCredentialsDurationLink);
		return new LogOnCredentialsDurationTest(driver);
	}

	FDDEmailTemplateSummaryTest click_FDDEmailTemplateSummary() throws Exception {
		fc.utobj().printTestStep("Click FDD Email Template Summary");
		AdminUI adminUI = new AdminUI(driver);
		fc.utobj().clickElement(driver, adminUI.fddEmailTemplateSummaryLink);
		return new FDDEmailTemplateSummaryTest(driver);
	}

	Item23ReceiptSummaryTest click_Item23ReceiptSummary() throws Exception {
		fc.utobj().printTestStep("Click Item 23 Receipt Summary");
		AdminUI adminUI = new AdminUI(driver);
		fc.utobj().clickElement(driver, adminUI.item23ReceiptSummaryLink);
		return new Item23ReceiptSummaryTest(driver);
	}

	ConfigureEmailSentPriorToFDDEmailTest click_ConfigureEmailSentPriorToFDDEmail() throws Exception {
		fc.utobj().printTestStep("Click Configure Email Sent Prior To FDD Email");
		AdminUI adminUI = new AdminUI(driver);
		fc.utobj().clickElement(driver, adminUI.configureEmailSentPriorToFDDEmailLink);
		return new ConfigureEmailSentPriorToFDDEmailTest(driver);
	}

	ConfigureEmailSentPriorToFDDExpirationTest click_ConfigureEmailSentPriorToFDDExpiration() throws Exception {
		fc.utobj().printTestStep("Click Configure Email Sent Prior To FDD Expiration");
		AdminUI adminUI = new AdminUI(driver);
		fc.utobj().clickElement(driver, adminUI.configureEmailsentPriortoFDDExpirationLink);
		return new ConfigureEmailSentPriorToFDDExpirationTest(driver);
	}

	// Site Clearance

	ConfigureSiteSubmissionFormTest click_ConfigureSiteSubmissionFormTest() throws Exception {
		fc.utobj().printTestStep("Click Configure Site Submission Form");
		AdminUI adminUI = new AdminUI(driver);
		fc.utobj().clickElement(driver, adminUI.configureSiteClearance);
		return new ConfigureSiteSubmissionFormTest(driver);
	}

}
