package com.builds.test.salesTest;

public class AdminSalesLeadStatus {

	String statusName;
	String statusFor;
	String excludeFromHeatMeter;

	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	public String getStatusFor() {
		return statusFor;
	}

	public void setStatusFor(String statusFor) {
		this.statusFor = statusFor;
	}

	public String getExcludeFromHeatMeter() {
		return excludeFromHeatMeter;
	}

	public void setExcludeFromHeatMeter(String excludeFromHeatMeter) {
		this.excludeFromHeatMeter = excludeFromHeatMeter;
	}

}
