package com.builds.test.salesTest;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.builds.uimaps.fs.AdminSalesLeadStatusUI;
import com.builds.utilities.FranconnectUtil;

class AdminSalesLeadStatusTest {

	WebDriver driver;
	FranconnectUtil fc = new FranconnectUtil();

	AdminSalesLeadStatusTest(WebDriver driver) {
		this.driver = driver;
	}

	void addLeadStatus(WebDriver driver) throws Exception {
		AdminSalesLeadStatusUI adminSalesLeadStatusUI = new AdminSalesLeadStatusUI(driver);
		fc.utobj().clickElement(driver, adminSalesLeadStatusUI.addLeadStatusBtn);
		fc.commonMethods().switch_cboxIframe_frameId(driver);
		AdminSalesLeadStatusTest.fill_admin_addStatusInfo();
		fc.utobj().clickElement(driver, adminSalesLeadStatusUI.addBtn);
	}

	void modifyLeadStatus(WebDriver driver) throws Exception {
		AdminSalesLeadStatusUI adminSalesLeadStatusUI = new AdminSalesLeadStatusUI(driver);

		WebElement selectElement = driver.findElement(By.name("templateOrder"));
		Select select = new Select(selectElement);
		List<WebElement> allStatus = select.getOptions();

		for (WebElement status : allStatus) {
			System.out.println(status.getText());
		}

	}

	static void fill_admin_addStatusInfo() {
		AdminSalesLeadStatus adminSalesLeadStatus = new AdminSalesLeadStatus();
		adminSalesLeadStatus.setStatusName("statusName");
		adminSalesLeadStatus.setStatusFor("Active Lead");
		adminSalesLeadStatus.setExcludeFromHeatMeter("Yes");
	}

}
