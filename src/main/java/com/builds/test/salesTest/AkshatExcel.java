package com.builds.test.salesTest;

import java.io.File;
import java.io.FileOutputStream;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class AkshatExcel {

	public static void main() {

		// Blank Workbook
		XSSFWorkbook workbook = new XSSFWorkbook();

		// Create a blank sheet
		XSSFSheet sheet = workbook.createSheet("myTestSheet");

		// Create new Row in sheet
		Row row = sheet.createRow(5);

		// Create a Cell
		Cell cell = row.createCell(1);

		try {

			// Write to workbook
			FileOutputStream out = new FileOutputStream(new File("myTestFile"));
			workbook.write(out);
			out.close();
			System.out.println("File written successfully");
		}

		catch (Exception e) {

		}
	}
}
