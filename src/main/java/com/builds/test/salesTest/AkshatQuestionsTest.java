package com.builds.test.salesTest;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AkshatQuestionsTest {
	
	@Test(groups = { "checkboxesOnPage" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "85786897976", testCaseDescription = "If a user is deleted/deactivated then user should not appear in assignment")
	private void Execute2() throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		try {
				driver.get("https://www.amazon.com/gp/goldbox/ref=nav_cs_gb/141-7075108-2072931");
				
				List<WebElement> countOfCheckBoxes = driver.findElements(By.xpath(".//input[@type='checkbox']"));
			
				System.out.println(countOfCheckBoxes.size());
				int visibleCheckboxes = 0;

				for(int x=0; x<countOfCheckBoxes.size();x++)
				{
					if(countOfCheckBoxes.get(x).isDisplayed())
					{
						visibleCheckboxes++;
					}
					System.out.println(countOfCheckBoxes.get(x));
				}
				
				System.out.println(visibleCheckboxes);
				
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(groups = { "imageSize" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "7845845tiuhtjkregh", testCaseDescription = "If a user is deleted/deactivated then user should not appear in assignment")
	private void Execute3() throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		try {
				driver.get("https://www.geeksforgeeks.org/java/");
				
				WebElement imageElement = fc.utobj().getElementByXpath(driver, ".//div//img[@class='img_ad']");
				System.out.println(imageElement.getAttribute("width"));
				System.out.println(imageElement.getAttribute("height"));

				System.out.println(" Image Dimensions = " +imageElement.getAttribute("width")+ "by" +imageElement.getAttribute("height"));
				
				
				
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

}
