package com.builds.test.salesTest;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import com.builds.test.common.AddDivision;
import com.builds.test.common.ConfigureNewHierarchyLevel;
import com.builds.test.common.ConfigureNewHierarchyLevelTest;
import com.builds.test.common.CorporateUser;
import com.builds.test.common.LoginPageTest;
import com.builds.test.common.ManageAreaRegion;
import com.builds.test.common.ManageAreaRegionTest;
import com.builds.test.common.ManageDivisionTest;
import com.builds.test.fs.FSLeadSummaryPageTest;
import com.builds.test.fs.FSSearchPageTest;
import com.builds.uimaps.fs.FSLeadSummaryMergeLeadsPage;
import com.builds.uimaps.fs.LeadSummaryUI;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AkshatTest {

	@Test(groups = { "sales" , "Sales_FDD_FullDetails" }) // Fixed : Verified // Akshat // new ui fixed
	@TestCase(createdOn = "2018-04-17", updatedOn = "2018-04-17", testCaseId = "Sales_FDD_FullDetails", testCaseDescription = "Go to Sales > Admin > FDD Management > Configure FDD Management , Log on credentials durations , FDD Email Template summary , ITEM 23 - RECEIPT Summary , Configure Email sent Prior to FDD Email , Configure Email sent Prior to FDD Expiration > Add Lead > Send FDD to Lead > Verify FDD Email in Activity History")
	private void Execute() throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Item23ReceiptSummary item23ReceiptSummary = new Item23ReceiptSummary();

		FDDManagement fddManagement = new FDDManagement();
		FDDEmailTemplateSummary fddEmailTemplateSummary = new FDDEmailTemplateSummary();
		LogOnCredentialsDuration logOnCredentialsDuration = new LogOnCredentialsDuration();
		ConfigureEmailSentPriorToFDDEmail configureEmailSentPriorToFDDEmail = new ConfigureEmailSentPriorToFDDEmail();
		ConfigureEmailSentPriorToFDDExpiration configureEmailSentPriorToFDDExpiration = new ConfigureEmailSentPriorToFDDExpiration();
		Sales_Common_New common = new Sales_Common_New();
		SendFDD sendFDD = new SendFDD();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Lead ld = new Lead();
		AdminSales adminSales = new AdminSales(driver);
		Sales sales = new Sales(driver);

		try {
			
			CorporateUser corpUser2 = new CorporateUser();
			fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser2);
			corpUser2.setFirstName("CorpUser");
			corpUser2.setLastName("FDD" + fc.utobj().generateRandomNumber());
			corpUser2.setUserName("user" + fc.utobj().generateRandomChar());
			corpUser2.setEmail("frantest2017@gmail.com");
			fc.commonMethods().addCorporateUser(driver, corpUser2);
			System.out.println(corpUser2.getFirstName() + " " + corpUser2.getLastName());

			driver = fc.loginpage().login(driver);
			fc.commonMethods().getModules().clickSalesModule(driver);

			fc.commonMethods().getModules().openAdminPage(driver);
			common.set_LogOnCredentialsDuration(logOnCredentialsDuration);
			adminSales.click_LogOnCredentialsDurationTest().set_LogOnCredentialsDuration(driver,
					logOnCredentialsDuration);
			adminSales.click_LogOnCredentialsDurationTest().verify_LogOnCredentialsDuration(logOnCredentialsDuration);

			fc.commonMethods().getModules().openAdminPage(driver);
			common.set_addItem23Receipt(item23ReceiptSummary);
			adminSales.click_Item23ReceiptSummary().addItem23Receipt(item23ReceiptSummary);
			fc.commonMethods().getModules().openAdminPage(driver);
			adminSales.click_Item23ReceiptSummary().verify_addItem23Receipt(item23ReceiptSummary);

			fc.commonMethods().getModules().openAdminPage(driver);
			common.set_uploadFDD(fddManagement, item23ReceiptSummary);
			adminSales.click_FDDManagement().uploadFDD(fddManagement).verify_uploadFDD(fddManagement);

			fc.commonMethods().getModules().openAdminPage(driver);
			common.set_FDDEmailTemplateSummary(fddEmailTemplateSummary);
			adminSales.click_FDDEmailTemplateSummary().addTemplate(driver, fddEmailTemplateSummary)
					.verify_addTemplate(fddEmailTemplateSummary);

			fc.commonMethods().getModules().openAdminPage(driver);
			common.fillDefaultValue_EmailSentPriorToFDDEmail(configureEmailSentPriorToFDDEmail);
			adminSales.click_ConfigureEmailSentPriorToFDDEmail().configureEmailSentPriorToFDDEmail(driver,
					configureEmailSentPriorToFDDEmail);

			fc.commonMethods().getModules().openAdminPage(driver);
			common.set_ConfigureEmailSentPriorToFDDExpiration(configureEmailSentPriorToFDDExpiration);
			adminSales.click_ConfigureEmailSentPriorToFDDExpiration()
					.configureEmail(configureEmailSentPriorToFDDExpiration);

			ld.setFirstName("FDD Lead" + fc.utobj().generateRandomNumber());
			ld.setLastName("FDD Lead" + fc.utobj().generateRandomNumber());
			ld.setEmail("salesautomation@staffex.com");
			ld.setLeadOwner(corpUser2.getuserFullName());
			ld.setLeadSourceCategory("Friends");
			ld.setLeadSourceDetails("Friends");
			ld.setLeadOwner(corpUser2.getuserFullName());

			fc.commonMethods().getModules().clickSalesModule(driver);
			common.set_sendFDD(sendFDD, fddEmailTemplateSummary, fddManagement);
			sales.clickLeadManagement().clickAddLeadFillLeadDetailsAndSave(ld).click_RightActionMenu_Then_SendFDDEmail()
					.SendFDD(fddManagement, sendFDD, ld).verify_SendFDD(sendFDD, fddEmailTemplateSummary);
		 	fc.utobj().readMailBox(fddEmailTemplateSummary.getSubject(), "Franchise Disclosure Document (FDD)" , ld.getEmail(),"sdg@1a@Hfs");
			
		//	fc.utobj().readMailBox("subject13141826" , "Franchise Disclosure Document (FDD)" , "salesautomation@staffex.com" ,"sdg@1a@Hfs");

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			// e.printStackTrace();
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(invocationCount = 10, groups = { "mergelead_akshat" })
	@TestCase(createdOn = "2018-02-27", updatedOn = "2018-02-27", testCaseId = "Sales_Test", testCaseDescription = "Test merge lead issue")
	private void Execute1() throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Sales_Common_New common = new Sales_Common_New();
		LeadManagementTest leadManagementPage = new LeadManagementTest(driver);

		Sales sales = new Sales(driver);
		Lead ld = new Lead();
		int recordCount = 0;
		FSLeadSummaryPageTest fsLeadSummaryPageTest = new FSLeadSummaryPageTest();

		try {

			driver = fc.loginpage().login(driver);
			fc.commonMethods().getModules().clickSalesModule(driver);

			// common.fillDefaultValue_LeadDetails(ld);
			ld.setFirstName("Lead");
			ld.setLastName("myBugTest" + fc.utobj().generateRandomNumber());

			common.addLeadThroughWebServices(driver, ld);
			common.addLeadThroughWebServices(driver, ld);

			String leadNameSearch = ld.getLastName();

			FSSearchPageTest p3 = new FSSearchPageTest();
			LeadSummaryUI pobj = new LeadSummaryUI(driver);
			FSLeadSummaryMergeLeadsPage pobj2 = new FSLeadSummaryMergeLeadsPage(driver);

			p3.searchByLeadName(driver, ld.getFirstName(), leadNameSearch);
			fc.utobj().clickElement(driver, pobj.checkBoxAll);

			fc.utobj().printTestStep("Merge these two leads");
			fc.utobj().selectActionMenuItems(driver, "Merge Leads");
			fc.utobj().clickElement(driver, ".//html//tr[7]/td[3]/input[1]");
			fc.utobj().clickElement(driver,
					"./html/body/div[8]/table[1]/tbody/tr/td/form[2]/table/tbody/tr[8]/td[3]/input");
			/*
			 * fc.utobj().clickElement(driver,
			 * "./html/body/div[8]/table[1]/tbody/tr/td/form[2]/table/tbody/tr[10]/td[3]/input"
			 * );
			 */ fc.utobj().clickElement(driver,
					"./html/body/div[8]/table[1]/tbody/tr/td/form[2]/table/tbody/tr[10]/td[3]/table/tbody/tr/td[1]/input");
			fc.utobj().clickElement(driver, pobj2.mergeBtn);

			fc.utobj().assertPageSource(driver, "Lead added through merging of two leads");
			p3.searchByLeadName(driver, ld.getFirstName(), ld.getLastName());

			fc.utobj().printTestStep("Search if the leads are merged or not");
			recordCount = fc.utobj().findSingleLinkPartialText(driver, ld.getLastName());
			if (recordCount > 1) {
				fc.utobj().throwsException("More than one lead found after merge - record mismatch!");
			} else if (recordCount < 1) {
				fc.utobj().throwsException("No leads found after merge - record mismatch!");
			}

			p3.archivedLeadSearchByName(driver, ld.getFirstName(), ld.getLastName());
			boolean isLeadMerged = fc.utobj().assertLinkPartialText(driver, ld.getLastName());
			if (isLeadMerged == false) {
				fc.utobj().throwsException("Leads not found in archived section!");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "restAkshat" })
	@TestCase(createdOn = "2018-04-17", updatedOn = "2018-04-17", testCaseId = "restAkshat", testCaseDescription = "Go to Sales > Admin > FDD Management > Configure FDD Management , Log on credentials durations , FDD Email Template summary , ITEM 23 - RECEIPT Summary , Configure Email sent Prior to FDD Email , Configure Email sent Prior to FDD Expiration > Add Lead > Send FDD to Lead > Verify FDD Email in Activity History")
	private void Execute4() throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Sales_Common_New common = new Sales_Common_New();
		Lead lead = new Lead();

		try {

			lead.setFirstName("Khubaib");
			lead.setLastName("Ali");
			/*
			 * lead.setEmail("frantest2017@gmail.com");
			 * lead.setLeadOwner("FranConnect Administrator");
			 * lead.setLeadSourceCategory("Friends");
			 * lead.setLeadSourceDetails("Friends");
			 */
			System.out.println(common.getLeadDetailsThroughWebServices_MajorFields(driver, lead));
			String myResponse = common.getLeadDetailsThroughWebServices_MajorFields(driver, lead);

			HashMap<String, String> map = new HashMap<String, String>();
			String replacedString = myResponse.replace("</", "#");
			String replacedString1 = replacedString.replace("<", "$");

			System.out.println(replacedString);
			System.out.println(replacedString1);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}

	@Test(groups = { "addCorpUser2018" })
	@TestCase(createdOn = "2018-04-18", updatedOn = "2018-04-18", testCaseId = "Sales_AddCorporateUser", testCaseDescription = "test case description")
	private void Execute123() throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		AdminCorporateUser adminCorporateUser = new AdminCorporateUser();
		AdminCorporateUserTest adminCorporateUserTest = new AdminCorporateUserTest(driver);
		Sales_Common_New common = new Sales_Common_New();

		try {
			
			driver = fc.loginpage().login(driver);
			// fc.commonMethods().getModules().adminpage().openCorporateUserPage(driver);
			
			common.set_addCorporateUser(adminCorporateUser);
			adminCorporateUserTest.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, adminCorporateUser);
			adminCorporateUserTest.verifyCorporateUser_ThroughWebService(adminCorporateUser);
			
			/*fc.commonMethods().getModules().openAdminPage(driver).openCorporateUserPage(driver);
			common.set_addCorporateUser(adminCorporateUser);
			adminCorporateUserTest.addCorporateUser(adminCorporateUser).verify_addCorporateUser(adminCorporateUser);*/
			
			
			
			
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}
	
	
	@Test(groups = { "salesterritory2018" })
	@TestCase(createdOn = "2018-04-27", updatedOn = "2018-04-27", testCaseId = "Sales_addTerritory", testCaseDescription = "test case description territory")
	private void Execute1234() throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Sales_Common_New common = new Sales_Common_New();
		SalesTerritories salesTerritories = new SalesTerritories();
		SalesTerritoriesTest salesTerritoriesTest = new SalesTerritoriesTest(driver);
		

		try {
			
			driver = fc.loginpage().login(driver);
			
			fc.commonMethods().getModules().openAdminPage(driver).openSalesTerritoryPage(driver);
			common.setGroupByCounty_Domestic_filladdNewSalesTerritory(salesTerritories);
			salesTerritoriesTest.addNewSalesTerritory(salesTerritories);
			
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}
	
	@Test(groups = { "webform2018" })
	@TestCase(createdOn = "2018-04-27", updatedOn = "2018-04-27", testCaseId = "Sales_OwnerAssignment", testCaseDescription = "test case description Owner Assignment")
	private void Execute12345() throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Sales_Common_New common = new Sales_Common_New();
		ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
		ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();
		
		try {
			
			driver = fc.loginpage().login(driver);
			
			fc.commonMethods().getModules().openAdminPage(driver).openFranchiseSalesManageWebFormGenerator(driver);
			common.set_ManageWebform(manageWebFormGenerator);
			manageWebFormGenerator.setFormTitle("formtitle" +fc.utobj().generateRandomNumber());
			manageWebFormGenerator.setFormName("formname" +fc.utobj().generateRandomNumber());
			manageWebFormGenerator.setFormURL("URL" + fc.utobj().generateRandomNumber());
			manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);
			
			
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}
	
	@Test(groups = { "AddSalesTerritory" })
	@TestCase(createdOn = "2018-05-04", updatedOn = "2018-05-04", testCaseId = "Sales_OwnerAssignment", testCaseDescription = "test case description Add Sales Territories + Enable New Hierarchy Level + Add Division + Assign User By Sales Territory")
	private void Execute123456() throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Sales_Common_New common = new Sales_Common_New();
		ManageAreaRegionTest manageAreaRegionTest = new ManageAreaRegionTest(driver);
		ManageAreaRegion manageAreaRegion = new ManageAreaRegion();
		SalesTerritories salesTerritories = new SalesTerritories(); 
		SalesTerritoriesTest salesTerritoriesTest = new SalesTerritoriesTest(driver);
		AddDivision addDivision = new AddDivision();
		ManageDivisionTest manageDivisionTest = new ManageDivisionTest(driver);
		ConfigureNewHierarchyLevelTest configureNewHierarchyLevelTest = new ConfigureNewHierarchyLevelTest(driver);
		ConfigureNewHierarchyLevel configureNewHierarchyLevel = new ConfigureNewHierarchyLevel();
		
		AssignLeadOwners assignLeadOwners = new AssignLeadOwners();
		AssignLeadOwnerbySalesTerritoriesTest assignLeadOwnerbySalesTerritoriesTest = new AssignLeadOwnerbySalesTerritoriesTest(driver);
		AssignLeadOwnersTest assignLeadOwnersTest = new AssignLeadOwnersTest(driver);
		
		try {
			
			driver = fc.loginpage().login(driver);
			common.setGroupByStates_Domestic_filladdNewSalesTerritory(salesTerritories);
			fc.commonMethods().getModules().openAdminPage(driver).openSalesTerritoryPage(driver);
			salesTerritoriesTest.addNewSalesTerritory(salesTerritories);
			
			configureNewHierarchyLevel.setEnableNewHierarchyLevel("Yes");
			fc.commonMethods().getModules().openAdminPage(driver).openConfigureNewHierarchyLevel(driver);
			configureNewHierarchyLevelTest.EnableNewHierarchyLevel(configureNewHierarchyLevel);
			
			common.set_AddDivision(addDivision);
			fc.commonMethods().getModules().openAdminPage(driver).openAddDivision(driver);
			manageDivisionTest.addDivision(addDivision);
			
			fc.commonMethods().getModules().openAdminPage(driver).openAssignLeadOwnersPage(driver);
			assignLeadOwnersTest.set_AssignLeadOwnerbySalesTerritories();
			
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}
	
	@Test(groups = { "gmailplugin" })
	@TestCase(createdOn = "2018-05-04", updatedOn = "2018-05-04", testCaseId = "Sales_OwnerAssignment", testCaseDescription = "Add Lead through Gmail plugin")
	private void Execute1234567() throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		GmailPluginTest gmailPluginTest = new GmailPluginTest(driver);
		Lead lead = new Lead();
		Sales_Common_New common = new Sales_Common_New();
		
		try {
			driver.get("https://www.gmail.com");
			lead.setFirstName("firstname");
			lead.setLastName("lastname");
			lead.setLeadSourceCategory("Friends");
			lead.setLeadSourceDetails("Friends");
			lead.setAddress1("address1");
			lead.setEmail("abc@abc.com");
			lead.setCountry("USA");
			lead.setStateProvince("Alabama");
			lead.setCity("city");
			lead.setZipPostalCode("201301");
			
			
			GmailPlugin gmailPlugin = new GmailPlugin();
			gmailPlugin.setGmailURL("");
			gmailPlugin.setEmailID("frantest2017@gmail.com");
			gmailPlugin.setPassword("sdg@1a@Hfs45");
			gmailPlugin.setLoginID_plugin("adm");
			gmailPlugin.setPassword_plugin("t0n1ght");
			gmailPlugin.setCompanyCode_plugin("qa1");
			
			gmailPluginTest.loginToGmail(gmailPlugin).loginToGmailPlugin(gmailPlugin).addLeadThroughGmailPlugin(lead);
			
			
			
			
			
			
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}
	
	@Test(groups = { "1000corpuseradd" })
	@TestCase(createdOn = "2018-05-04", updatedOn = "2018-05-04", testCaseId = "Sales_OwnerAssignment", testCaseDescription = "1000 corp user add through api")
	private void Execute12345678() throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Sales_Common_New common = new Sales_Common_New();
		CorporateUser corpUser = new CorporateUser();

		
		try {
			
			ArrayList<String> corpUsers = new ArrayList<String>();
			for (int i = 0; i <= 1000; i++) {
				fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
				corpUser.setFirstName("CorpUser" + i);
				corpUser.setLastName("Automation" + fc.utobj().generateRandomNumber());
				corpUser.setUserName("user" + fc.utobj().generateRandomChar());
				fc.commonMethods().addCorporateUser(driver, corpUser);
				System.out.println(corpUser.getFirstName() + " " + corpUser.getLastName());
				// corpUsers.add(corpUser.getFirstName() + " " + corpUser.getLastName());
			}
			
			
			
			
			
			
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}
	
	@Test(groups = { "importLead" })
	@TestCase(createdOn = "2018-05-04", updatedOn = "2018-05-04", testCaseId = "Sales_OwnerAssignment", testCaseDescription = "1000 corp user add through api")
	private void Execute123456789() throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		Import importlead = new Import();
		ImportTest importTest = new ImportTest(driver);
		
		try {
		
			driver = fc.loginpage().login(driver);
			fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);
			
			importlead.setImportType("Leads");
			importlead.setSpecifyFileFormat("CSV");
			importlead.setSalesDataFile(fc.utobj().getFilePathFromTestData("Import Testing.csv"));
			
			importlead.setLeadStatus("New Lead");
			importlead.setLeadSourceCategory("Friends");
			importlead.setLeadSourceDetails("Friends");
			
			importTest.importLeadsCSV(importlead);
			
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(groups = { "readFromExcel" })
	
	public List<String> getFullNameOfLeadsFromCSV() throws Exception
		{
			FranconnectUtil fc = new FranconnectUtil();		
			List<String> LeadNames = new ArrayList<String>();
			 String csvFile = fc.utobj().getFilePathFromTestData("Import Testing.csv");
		     String line = "";
		     String cvsSplitBy = ",";

		        try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {

		            while ((line = br.readLine()) != null)
		            {
		            	// use comma as separator
		                String[] name = line.split(cvsSplitBy);
		                System.out.println("FirstName : " +name[1]+ " , LastName : " +name[2]+"]");
		                LeadNames.add(name[1]+" "+name[2]);
		            }
		            	return LeadNames;
		        }

		   }
	
	
	
	@Test(groups = { "readFromExcel123" })
	
	
	public void getFullNameOfLeadsFromCSV123() throws Exception
	{
		FranconnectUtil fc = new FranconnectUtil();
		Sales_Common_New common = new Sales_Common_New();
		
		String csvFile = fc.utobj().getFilePathFromTestData("AsgmntByDivPrioritySalesTerritories_GroupByStatesIntl.csv");
		Map<String,List<String>> map = common.getFullNameOfLeadsFromCSV(csvFile);
		
	Lead x = new Lead();
	List<String> leadsFirstNames = map.get("FN");
	List<String> leadsLastNames = map.get("LN");
	
	for(int a=1,b=1; a<leadsFirstNames.size() & b<leadsLastNames.size(); a++,b++)
	{
		
		System.out.println(leadsFirstNames.get(a)+ " " +leadsLastNames.get(b));
		
	}
	
	/*for(String firstName : leadsFirstNames)
	{
		for(String lastName : leadsLastNames)
		{
			System.out.println(firstName+ " " +lastName);
			x.setFirstName(firstName);
			x.setLastName(lastName);*/
			
			/*String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
			String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
			System.out.println("Owner Assigned to Lead "+x.getLeadFullName()+" >>>>>>>>" + ownerAssignedToLeadx);*/
	
		}
	
	
	@Test(groups = { "Add_Lead_WebForm_Test" })
	@TestCase(createdOn = "2018-05-28", updatedOn = "2018-05-10", testCaseId = "Owner_Assignment_1234", testCaseDescription = "Add user and check owner assignment")
	private void Execute45() throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		ManageWebformGeneratorTest ManageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
		ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();
		AddDivision addDivision1 = new AddDivision();
		ManageDivisionTest manageDivisionTest = new ManageDivisionTest(driver);
		Sales_Common_New common = new Sales_Common_New();
		CorporateUser corpUser = new CorporateUser();

	
		try {
			
			driver = fc.loginpage().login(driver);

			
			// ADD Corporate Users - through API 
						ArrayList<String> corpUsers = new ArrayList<String>();
						for (int i = 0; i <= 2; i++) {											// (Adding 3 Corporate Users)
							fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
							corpUser.setFirstName("CorpUser" + i);
							corpUser.setLastName("SalesTerritory" + fc.utobj().generateRandomNumber());
							corpUser.setUserName("user" + fc.utobj().generateRandomChar());
							fc.commonMethods().addCorporateUser(driver, corpUser);
							System.out.println(corpUser.getFirstName() + " " + corpUser.getLastName());
							corpUsers.add(corpUser.getFirstName() + " " + corpUser.getLastName());
						}
			
			// Add Division #1
			common.set_AddDivision(addDivision1);
			fc.commonMethods().getModules().openAdminPage(driver).openAddDivision(driver);
			manageDivisionTest.addDivision(addDivision1);
			
			// Create WebForm 
						fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
						manageWebFormGenerator.setDivision(addDivision1.getDivisionName());
						common.set_ManageWebform(manageWebFormGenerator);
						ManageWebformGeneratorTest.createWebForm(manageWebFormGenerator);
						
						// Add leads from WebForm and verify Division RoundRobin
						Lead lead2 = new Lead();
						for (int j = 0, k = 0; j <= 5; j++, k++) {
							lead2.setFirstName("Lead" + j);
							lead2.setLastName("RoundRobin" + fc.utobj().generateRandomNumber());
							lead2.setEmail("frantest2017@gmail.com");

							ManageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, lead2); // ADD LEAD THROUGH WEBFORM
																											
							fc.utobj().sleep();
							String xml2 = common.getLeadDetailsThroughWebServices_MajorFields(driver, lead2);
							//System.out.println(xml2);
							String ownerAssignedToLead2 = common.getValFromXML(xml2, "leadOwnerID");
							System.out.println("Owner Assigned to Lead "+lead2.getLeadFullName()+" >>>>>>>>" + ownerAssignedToLead2);							
							// Validate
							if (k > 2)
								k = 0;
							if (! corpUsers.get(k).equalsIgnoreCase(ownerAssignedToLead2)) {
								fc.utobj().throwsException("Lead Owner Mismatch - Issue in Division RoundRobin Lead Owner Assignment");
							}
						}
						
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
		}
	
	
	
	@Test(groups = { "csvCreateWriteSave" })
	 void csv()
	 {
		String fileName = FranconnectUtil.config.get("testLogFolderPath")+"/student1.csv";
		//String fileName = System.getProperty("user.home")+"/student123.csv";
		System.out.println(fileName);
		
		Lead lead4 = new Lead();
		lead4.setFirstName("LeadDivision");
		lead4.setLastName("PriorityTerritory(County)");
		lead4.setEmail("frantest2017@gmail.com");
		lead4.setDivision("div1");
		lead4.setCountry("USA");
		lead4.setStateProvince("Oregon");
		lead4.setCounty("Morrow");
		lead4.setBasedonAssignmentRules("yes");
		lead4.setLeadSourceCategory("cat1");
		lead4.setLeadSourceDetails("None");
		lead4.setBestTimeToContact("9");
		
		Lead lead5 = new Lead();
		lead5.setFirstName("LeadDivision111111");
		lead5.setLastName("PriorityTerritory(County)0000000");
		lead5.setEmail("frantest2017@gmail.com");
		lead5.setDivision("div1");
		lead5.setCountry("USA");
		lead5.setStateProvince("Oregon");
		lead5.setCounty("Morrow");
		lead5.setBasedonAssignmentRules("yes");
		lead5.setLeadSourceCategory("cat1");
		lead5.setLeadSourceDetails("None");
		lead5.setBestTimeToContact("9");
		
		List<Lead> leadList = new ArrayList<Lead>(); 
		leadList.add(lead4);
		leadList.add(lead5);
		
		Sales_Common_New common = new Sales_Common_New();
		common.writeCsvFile(fileName, leadList);
		System.out.println("Write CSV file:");

	 } 
	
	@Test(groups = { "leadthroughwebform" })
	// Through WebForm ----------------------------------------------------------------------------------------------------------------------------------------------------------
	void leadthroughwebform() throws Exception
	{
		FranconnectUtil fc = new FranconnectUtil();
		
		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			driver = fc.loginpage().login(driver);


	ManageWebformGeneratorTest ManageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
	ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();
	CorporateUser corpUser = new CorporateUser();
	Sales_Common_New common = new Sales_Common_New();
	
			try {	// Create WebForm
				fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
				//manageWebFormGenerator.setDivision(addDivision1.getDivisionName());
				common.set_ManageWebform(manageWebFormGenerator);
				//manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory1.getLeadSourceCategory());
				manageWebFormGenerator.setLeadSourceDetails("None");
				ManageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

				// Add Lead - WebForm
				Lead webformLead = new Lead();
				for (int j = 0; j <= 2; j++) {
					webformLead.setFirstName("LeadSource_WebForm" + j);
					webformLead.setLastName("RR" + fc.utobj().generateRandomNumber());
					webformLead.setEmail("frantest2017@gmail.com");
					// webformLead.setDivision(addDivision1.getDivisionName());
					//webformLead.setCountry("USA");
					//webformLead.setStateProvince("Florida");
					//webformLead.setCounty("Gulf");
					//webformLead.setZipPostalCode(salesTerritories2.getZipPostalCodeText());
					//webformLead.setLeadSourceCategory(leadSourceCategory1.getLeadSourceCategory());
					webformLead.setLeadSourceDetails("None");

					// Add Lead through WebForm
					ManageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, webformLead);

					// fc.utobj().sleep();
					String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, webformLead);
					// System.out.println(xml2);
					String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
					System.out.println("Owner Assigned to Lead " + webformLead.getLeadFullName() + " >>>>>>>>"
							+ ownerAssignedTowebformLead);
					// Validate
				/*	if (!corpUsersForSource.contains(ownerAssignedTowebformLead)) {
						fc.utobj().throwsException("Assign Lead Owner by Lead Source - Priority(Sales Territories) - RR - WebForm - FAILED");
					}
				}*/
}
	}catch(Exception e)
			{
		}
			}
	
	@Test(groups= { "test123apiUpdate" } )
	void updateLeadThroughApi() throws Exception
	{
		FranconnectUtil fc = new FranconnectUtil();
		Sales_Common_New common = new Sales_Common_New();
		
		// Add Lead 1
		Lead lead1 = new Lead();
		lead1.setFirstName("Duplicatetest123" + fc.utobj().generateRandomNumber6Digit());
		lead1.setLastName("Lead" + fc.utobj().generateRandomNumber());
		lead1.setEmail("frantest2017@gmail.com");
		lead1.setBasedonAssignmentRules("yes");
		lead1.setLeadSourceCategory("Friends");
		lead1.setLeadSourceDetails("Friends");
		common.addLeadThroughWebServices(null, lead1);
		
		// Fetch Owner assigned to Lead through Rest API
		String xml1 = common.getLeadDetailsThroughWebServices_MajorFields(null, lead1);
		String ownerAssignedToLead1 = common.getValFromXML(xml1, "leadOwnerID");
		String leadReferenceId = common.getValFromXML(xml1, "referenceId");
		System.out.println("Owner Assigned to Lead >>>>>>>>" + lead1.getLeadFullName() + ">>>" + ownerAssignedToLead1);
		
		lead1.setLastName("newupdatedlastName");
		common.updateLeadThroughWebServices(null, lead1, leadReferenceId);
	}

	@Test(groups={"18072018"})
	void exec() throws Exception
	{
		FranconnectUtil fc = new FranconnectUtil();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		driver = fc.loginpage().login(driver);
		
		CorporateUser corporateUser = new CorporateUser();
		fc.commonMethods().fillDefaultDataFor_CorporateUser(corporateUser);
		corporateUser.setFirstName("Corp" + fc.utobj().generateRandomNumber());
		corporateUser.setLastName("User" + fc.utobj().generateRandomNumber());
		fc.commonMethods().addCorporateUser(driver, corporateUser);
								
		fc.home_page().logout(driver);

		LoginPageTest loginPageTest = new LoginPageTest();
		loginPageTest.loginWithUserNameAndPassword(driver, corporateUser.getUserName(), corporateUser.getPassword());
		
		
		fc.commonMethods().getModules().clickSalesModule(driver); // click on Sales
		
		Sales salesModule = new Sales(driver);
		salesModule.clickGroups();	// click on Groups
		
		GroupTest groupTest = new GroupTest();
		Group group = new Group();
		groupTest.clickCreateGroupBtnAndSwitchToFrame(driver);
		
		try {
			driver.switchTo().frame(1);

		} catch (Exception ex) {
			try {
				driver.switchTo().frame(2);
			} catch (Exception e1) {
				fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);

			}

		}
		}
	
	
	@Test(groups = { "AddLeadThroughWebForm"})
	@TestCase(createdOn = "2018-07-17", updatedOn = "2018-07-17", testCaseId = "AddLeadThroughWebForm", testCaseDescription = "AddLeadThroughWebForm")
	void createLeadFromWebForm() throws Exception
	{
		FranconnectUtil fc = new FranconnectUtil();

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		
WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try
		{
			driver = fc.loginpage().login(driver);
			
			ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();
			Sales_Common_New common = new Sales_Common_New();
			
			ManageDivisionTest manageDivisionTest = new ManageDivisionTest(driver);
			AddDivision addDivision1 = new AddDivision();
			// Add Division #1
						//common.set_AddDivision(addDivision1); Random Dynamic Division Name
						addDivision1.setDivisionName("Division" +fc.utobj().generateRandomNumber());
						fc.commonMethods().getModules().openAdminPage(driver).openAddDivision(driver);
						manageDivisionTest.addDivision(addDivision1);
					
	// Through WebForm -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	// Create WebForm 
	ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
	fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
	manageWebFormGenerator.setDivision(addDivision1.getDivisionName());
	common.set_ManageWebform(manageWebFormGenerator);
	//manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory1.getLeadSourceCategory());
	//manageWebFormGenerator.setLeadSourceDetails("None");
	manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);
	
	// Add Lead - WebForm
	Lead webformLead1 = new Lead();
	for (int j = 0, k = 0; j <= 5; j++, k++) {
			webformLead1.setFirstName("LeadDivWebform" + j);
			webformLead1.setLastName("RoundRobin" + fc.utobj().generateRandomNumber());
			webformLead1.setEmail("frantest2017@gmail.com");
			webformLead1.setDivision(addDivision1.getDivisionName());
			webformLead1.setLeadSourceCategory("Friends");
			webformLead1.setLeadSourceDetails("Friends");

		manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, webformLead1); // ADD LEAD THROUGH WEBFORM
	}
		
		fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		
	} catch (Exception e) {
		fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
	}
	}
	
	@Test(groups= {"Sales_OwnerAssignment_44-WebForm_TestBench"})
	void test() throws Exception
	{
		FranconnectUtil fc = new FranconnectUtil();
		Sales_Common_New common = new Sales_Common_New();
		
		LeadSourceCategory leadSourceCategory2 = new LeadSourceCategory();
		leadSourceCategory2.setLeadSourceCategory("Lead Source 14005334");
		
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			driver = fc.loginpage().login(driver);


			// Through WebForm
			// ----------------------------------------------------------------------------------------------------------------------------------------------------------
			// Create WebForm
			ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
			ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

			fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
			common.set_ManageWebform(manageWebFormGenerator);
			manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
			manageWebFormGenerator.setLeadSourceDetails("None");
			// manageWebFormGenerator.setDivision(addDivision1.getDivisionName());
			manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

			// Add Lead - WebForm
				Lead leadWebForm = new Lead();
				leadWebForm.setFirstName("LeadSource_WebForm_33930_TEST");
				leadWebForm.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadWebForm.setEmail("frantest2017@gmail.com");
				// leadApi.setDivision(addDivision1.getDivisionName()); // BLANK
				leadWebForm.setCountry("USA");
				leadWebForm.setStateProvince("Florida"); // BLANK
				leadWebForm.setCounty("Gulf"); // BLANK
				leadWebForm.setZipPostalCode("364675");
				// leadWebForm.setBasedonAssignmentRules("yes");
				leadWebForm.setLeadSourceCategory("Friends");
				leadWebForm.setLeadSourceDetails("Friends");

				// Add Lead through WebForm
				manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm);

				// fc.utobj().sleep();
				String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
				String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
				System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>"
						+ ownerAssignedTowebformLead);
				// Validation
				if (!ownerAssignedTowebformLead.equalsIgnoreCase("FranConnect Administrator")) {
					fc.utobj().throwsException(
							"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33930 - WebForm - FAILED");
				fc.utobj().logoutAndQuitBrowser(driver, null);

	}
		}catch (Exception e) {
		fc.utobj().quitBrowserOnCatch(driver, e, null);
	}
	
}
	
	@Test(groups = {"sales_OwnerAssignment_failed_TESTTEST"})
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_97", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source , DefaultOwner")
	private void OwnerAssignmentByLeadSource_DefaultOwner() throws Exception {

		FranconnectUtil fc = new FranconnectUtil();
		Sales_Common_New common = new Sales_Common_New();
		
		AddDivision addDivision2 = new AddDivision();
		addDivision2.setDivisionName("Division a14005302");
		
		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			try {
				
				 driver = fc.loginpage().login(driver);
				 
			// Default Owner Assignment
			// Through API
			Lead leadApi = new Lead();
			leadApi.setFirstName("DefaultOwner_Api_TEST");
			leadApi.setLastName("LeadSource" + fc.utobj().generateRandomNumber());
			leadApi.setEmail("frantest2017@gmail.com");
			leadApi.setBasedonAssignmentRules("yes");
			leadApi.setLeadSourceCategory("Friends");
			leadApi.setLeadSourceDetails("Friends");
			common.addLeadThroughWebServices(driver, leadApi);
			// Fetch Owner assigned to Lead through Rest API
			String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadApi);
			// System.out.println(xml14);
			String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
			System.out.println("Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
			// Validation
			if (!ownerAssignedToLeadApi.equalsIgnoreCase("FranConnect Administrator")) {
				fc.utobj().throwsException("Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - API - FAILED");
			}
			
			// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
			// Create CSV File with Lead Details
			String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_Division_DefaultOwner.csv";
			System.out.println(filePath);
			List<Lead> leadList = new ArrayList<Lead>();

			for (int j = 0; j <= 2; j++) {
				Lead importLead = new Lead();
				importLead.setFirstName("DefaultOwner_Import_TEST" + j);
				importLead.setLastName("LeadSource" + fc.utobj().generateRandomNumber());
				importLead.setEmail("frantest2017@gmail.com");
				//importLead.setDivision(addDivision1.getDivisionName());
				//importLead.setCountry("United Kingdom");
				//importLead.setStateProvince("Berkshire");
				// importLead.setCounty("Gulf");
				// importLead.setZipPostalCode(salesTerritories2.getZipPostalCodeText());
				importLead.setBasedonAssignmentRules("yes");
				importLead.setLeadSourceCategory("Friends");
				importLead.setLeadSourceDetails("Friends");

				leadList.add(importLead);
			}

			common.writeCsvFile(filePath, leadList);

			// Import the CSV file
			Import importlead = new Import();
			ImportTest importTest = new ImportTest(driver);

			// Navigate to Import Tab
			fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

			importlead.setImportType("Leads");
			importlead.setSpecifyFileFormat("CSV");
			importlead.setSalesDataFile(filePath);
			// importlead.setSalesDataFile(fc.utobj().getFilePathFromTestData("AsgmntByDivPrioritySalesTerritories_GroupByStatesIntl.csv"));
			importlead.setLeadStatus("New Lead");
			importlead.setLeadSourceCategory("Friends");
			importlead.setLeadSourceDetails("Friends");

			importTest.importLeadsCSV(importlead);

			// Validation
			Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

			Lead x = new Lead();
			List<String> leadsFirstNames = map.get("FN");
			List<String> leadsLastNames = map.get("LN");

			for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
				System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
				x.setFirstName(leadsFirstNames.get(a));
				x.setLastName(leadsLastNames.get(b));

				String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
				String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
				System.out.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

				// Validate Owner Assignment
				if (!ownerAssignedToLeadx.equalsIgnoreCase("FranConnect Administrator")) {
					fc.utobj().throwsException("Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - Import - FAILED");
				}
				
				// 
			}

			// Through WebForm
			// Create WebForm
			ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
			ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

			fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
			common.set_ManageWebform(manageWebFormGenerator);
			manageWebFormGenerator.setDivision(addDivision2.getDivisionName());
			manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

			// Add lead from WebForm
			Lead leadWebForm = new Lead();
			leadWebForm.setFirstName("DefaultOwner_TEST");
			leadWebForm.setLastName("LeadWebForm" + fc.utobj().generateRandomNumber());
			leadWebForm.setEmail("frantest2017@gmail.com");

			// ADD LEAD THROUGH WEBFORM
			manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm); 																					
			fc.utobj().sleep();
			String xmlWebForm = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
			// System.out.println(xmlWebForm);
			String ownerAssignedToLeadWebForm = common.getValFromXML(xmlWebForm, "leadOwnerID");
			System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadWebForm);
			// Validate
			if (!ownerAssignedToLeadWebForm.equalsIgnoreCase("FranConnect Administrator")) {
				fc.utobj().throwsException("Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - WebForm - FAILED");
			}

			fc.utobj().logoutAndQuitBrowser(driver, null);
			
			} catch (Exception e) {
				e.printStackTrace();
			 // fc.utobj().quitBrowserOnCatch(driver, e, null);
			
		}
	}
}
	

	



