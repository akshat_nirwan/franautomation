package com.builds.test.salesTest;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.test.fs.Sales_Common;
import com.builds.utilities.FranconnectUtil;

public class AkshatTestTest {
	
	static WebDriver driver;

		
	@Test(groups = { "rest2018akshat"})
	public void execute() throws Exception
	{
		StrtoXML_AkshatTest akshatTest = new StrtoXML_AkshatTest();
		Lead lead = new Lead();
		lead.setFirstName("System");
		lead.setLastName("Lead");
		Sales_Common_New common = new Sales_Common_New();
		common.getLeadDetailsThroughWebServices_MajorFields(driver, lead);
		String fetchedVal = akshatTest.getValFromXML(common.getLeadDetailsThroughWebServices_MajorFields(driver, lead), "homePhone");
		System.out.println(fetchedVal);
		
	}
	
	@Test(groups = { "restupdate2018" })
	public void execute1() throws Exception
	{
		// Lead lead = new Lead();
		Sales_Common_New common = new Sales_Common_New();
		common.updateLeadDetailsThroughWebServices_EmailOptIn(driver, "1325937883");
				
	}
	
	@Test(groups = {"createlead2018"})
	public void createLeadTest() throws Exception
	{
		Sales_Common_New sales_Common_New = new Sales_Common_New();
		Lead lead = sales_Common_New.fillDefaultValue_LeadDetails();
		
		RestAssured_API api = new RestAssured_API();
		api.createLead_restAssured(lead);
	}
	
	@Test(groups = {"updatelead2018"})
	public void updateLeadTest()  throws Exception
	{
		Sales_Common_New sales_Common_New = new Sales_Common_New();
		Lead lead = sales_Common_New.fillDefaultValue_LeadDetails();
		
		RestAssured_API api = new RestAssured_API();
		api.updateLead_restAssured(1364547435, lead);
		
	}
	
	@Test(groups = {"retrievelead2018"})
	public void retrieveLeadTest() throws Exception
	{
		Lead lead = new Lead();
		lead.setFirstName("Fnamem28180383");
		lead.setLastName("Lnamef28180376");

		RestAssured_API api = new RestAssured_API();
		api.retrieveLead_restAssured(lead);
	}
	
	
}



