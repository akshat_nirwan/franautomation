package com.builds.test.salesTest;

import org.testng.IAnnotationTransformer;
import org.testng.annotations.ITestAnnotation;
 
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
 
/**
 * Created by Akshat 23/11/2018
 */
public class AnnotationTransformer implements IAnnotationTransformer {
 
    @Override
    public void transform(ITestAnnotation annotation, Class testClass, Constructor testConstructor, Method testMethod) {
        annotation.setRetryAnalyzer(Retry.class);
    }
    
    
}