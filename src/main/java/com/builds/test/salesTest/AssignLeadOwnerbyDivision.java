package com.builds.test.salesTest;

import java.util.ArrayList;
import java.util.List;

class AssignLeadOwnerbyDivision {
	
	private List<String> defaultOwners = new ArrayList<String>();
	private String divisionName;

	
	public List<String> getDefaultOwners() {
		return defaultOwners;
	}
	public void setDefaultOwners(List<String> defaultOwners) {
		this.defaultOwners = defaultOwners;
	}
	public String getDivisionName() {
		return divisionName;
	}
	public void setDivisionName(String divisionName) {
		this.divisionName = divisionName;
	}

}
