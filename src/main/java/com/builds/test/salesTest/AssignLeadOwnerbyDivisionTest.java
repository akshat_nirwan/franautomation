package com.builds.test.salesTest;

import java.util.ArrayList;
import java.util.Iterator;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

import com.builds.test.common.AddDivision;
import com.builds.uimaps.fs.AssignLeadOwnerbyDivisionUI;
import com.builds.uimaps.fs.AssignLeadOwnerbyLeadSourceUI;
import com.builds.uimaps.fs.AssignLeadOwnerbySalesTerritoriesUI;
import com.builds.utilities.FranconnectUtil;

public class AssignLeadOwnerbyDivisionTest {

	private WebDriver driver;
	FranconnectUtil fc = new FranconnectUtil();

	public AssignLeadOwnerbyDivisionTest(WebDriver driver) {
		this.driver = driver;
	}

	AssignLeadOwnerbyDivisionTest Configure_AssignLeadOwnerbyDivision(String division, ArrayList<String> corpUsers) throws Exception {
		AssignLeadOwnerbyDivisionUI ui = new AssignLeadOwnerbyDivisionUI(driver);
		
		fc.utobj().selectMultipleValFromMultiSelect(driver, ui.DefaultOwner_Against_DivisionName(driver, division), corpUsers);
		fc.utobj().clickElement(driver, ui.updateBtn);

		return new AssignLeadOwnerbyDivisionTest(driver);
/*		fc.utobj().selectMultipleFromMultiList(driver, byId_AttributeValue, textOptions);
*/	

		/*for (String user : corpUsers) {

			if (division.getDivisionName() != null && 	user != null) {
				fc.utobj().selectDropDown(driver, ui.DefaultOwner_Against_DivisionName(driver, division.getDivisionName()),user);
			}

			fc.utobj().clickElement(driver, ui.updateBtn);
		}*/
	}
	
	AssignLeadOwnerbyDivisionTest AssociateDifferentOwnerFor_SalesTerritories(ArrayList<String> corpUsers, String divisionName, String territoryName) throws Exception
	{		
		AssignLeadOwnerbyDivisionUI ui = new AssignLeadOwnerbyDivisionUI(driver);
		
		fc.utobj().actionImgOption(driver, divisionName, "Associate different owner for Sales Territories");
		fc.utobj().selectMultipleValFromMultiSelect(driver, ui.DefaultOwner_Against_TerritoryName(driver, territoryName), corpUsers );
		fc.utobj().clickElement(driver, ui.saveBtn_AssignFor_Any);
		
		return new AssignLeadOwnerbyDivisionTest(driver);
	}
	
	void setPriority_SalesTerritory() throws Exception
	{
		AssignLeadOwnerbyLeadSourceUI ui = new AssignLeadOwnerbyLeadSourceUI(driver);

		fc.utobj().printTestStep("Setting priority for Owner Assignment as - Sales Territory");
		fc.utobj().clickElement(driver, ui.setPriority_link);
		Select prioritylist = new Select(ui.priorityList);
		prioritylist.selectByVisibleText("Sales Territory");
		fc.utobj().clickElement(driver, ui.upArrow_Btn);
		fc.utobj().clickElement(driver, ui.saveBtn);
	}
	
	void setPriority_Source() throws Exception
	{
		AssignLeadOwnerbyLeadSourceUI ui = new AssignLeadOwnerbyLeadSourceUI(driver);

		fc.utobj().printTestStep("Setting priority for Owner Assignment as - Source");
		Select prioritylist = new Select(ui.priorityList);
		prioritylist.selectByVisibleText("Source");
		fc.utobj().clickElement(driver, ui.upArrow_Btn);
		fc.utobj().clickElement(driver, ui.saveBtn);
	}
	
	void click_setPriority() throws Exception
	{
		AssignLeadOwnerbyDivisionUI assignLeadOwnerbyDivisionUI = new AssignLeadOwnerbyDivisionUI(driver);
		fc.utobj().clickElement(driver, assignLeadOwnerbyDivisionUI.setPriority_link);
	}


	/*
	 * void configure_AssignLeadOwnerbySalesTerritories(ArrayList<String>
	 * corpUsers , String divisionName) throws Exception {
	 * AssignLeadOwnerbyDivisionUI ui = new AssignLeadOwnerbyDivisionUI(driver);
	 * 
	 * 
	 * Iterator<String> user = corpUsers.iterator(); Iterator<String> territory
	 * = SalesTerritories.iterator(); while(user.hasNext() &&
	 * territory.hasNext()) { String value1 = user.next(); String value2 =
	 * territory.next();
	 * 
	 * //do stuff fc.utobj().selectValFromMultiSelect(driver,
	 * ui.DefaultOwner_Against_SalesTerritories(driver, value2), value1);
	 * 
	 * }
	 * 
	 * fc.utobj().clickElement(driver, ui.updateBtn); }
	 */

	AssignLeadOwnerbyDivisionTest AssociateDifferentOwnerFor_Source(ArrayList<String> corpUsers, String divisionName, String sourceName) throws Exception
		{		
		AssignLeadOwnerbyDivisionUI ui = new AssignLeadOwnerbyDivisionUI(driver);
	
		fc.utobj().actionImgOption(driver, divisionName, "Associate different owner for Source");
		fc.utobj().selectMultipleValFromMultiSelect(driver, ui.DefaultOwner_Against_SourceName(driver, sourceName), corpUsers );
		fc.utobj().clickElement(driver, ui.saveBtn_AssignFor_Any);
	
		return new AssignLeadOwnerbyDivisionTest(driver);
		}

}

