package com.builds.test.salesTest;

import java.util.ArrayList;
import java.util.List;

public class AssignLeadOwnerbyLeadSource {
	
	private List<String> defaultOwners = new ArrayList<String>();
	private String leadSourceName;

	public List<String> getDefaultOwners() {
		return defaultOwners;
	}
	public void setDefaultOwners(List<String> defaultOwners) {
		this.defaultOwners = defaultOwners;
	}
	public String getLeadSourceName() {
		return leadSourceName;
	}
	public void setLeadSourceName(String leadSourceName) {
		this.leadSourceName = leadSourceName;
	}

}
