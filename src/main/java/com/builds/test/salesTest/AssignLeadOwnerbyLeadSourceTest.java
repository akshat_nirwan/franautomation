package com.builds.test.salesTest;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.builds.uimaps.fs.AssignLeadOwnerbyDivisionUI;
import com.builds.uimaps.fs.AssignLeadOwnerbyLeadSourceUI;
import com.builds.utilities.FranconnectUtil;

 class AssignLeadOwnerbyLeadSourceTest {
	
	private WebDriver driver;
	FranconnectUtil fc = new FranconnectUtil();
	
	AssignLeadOwnerbyLeadSourceTest(WebDriver driver) {
		this.driver = driver;
	}
	
	
	AssignLeadOwnerbyLeadSourceTest Configure_AssignLeadOwnerbyLeadSource(String leadSource , ArrayList<String> corpUsers) throws Exception
	{
		AssignLeadOwnerbyLeadSourceUI ui = new AssignLeadOwnerbyLeadSourceUI(driver);
		
		fc.utobj().selectMultipleValFromMultiSelect(driver, ui.DefaultOwner_Against_LeadSourceName(driver, leadSource), corpUsers);
		fc.utobj().clickElement(driver, ui.updateBtn);
		
		return new AssignLeadOwnerbyLeadSourceTest(driver);
	}
	
	void setPriority_SalesTerritory() throws Exception
	{
		AssignLeadOwnerbyLeadSourceUI ui = new AssignLeadOwnerbyLeadSourceUI(driver);

		fc.utobj().printTestStep("Setting priority for Owner Assignment as - Sales Territory");
		fc.utobj().clickElement(driver, ui.setPriority_link);
		Select prioritylist = new Select(ui.priorityList);
		prioritylist.selectByVisibleText("Sales Territory");
		fc.utobj().clickElement(driver, ui.upArrow_Btn);
		fc.utobj().clickElement(driver, ui.saveBtn);
	}
	
	void setPriority_Division() throws Exception
	{
		AssignLeadOwnerbyLeadSourceUI ui = new AssignLeadOwnerbyLeadSourceUI(driver);

		fc.utobj().printTestStep("Setting priority for Owner Assignment as - Division");
		fc.utobj().clickElement(driver, ui.setPriority_link);
		Select prioritylist = new Select(ui.priorityList);
		prioritylist.selectByVisibleText("Division");
		fc.utobj().clickElement(driver, ui.upArrow_Btn);
		fc.utobj().clickElement(driver, ui.saveBtn);
	}
	
	AssignLeadOwnerbyLeadSourceTest AssociateDifferentOwnerFor_SalesTerritories(ArrayList<String> corpUsers, String leadSource, String territoryName) throws Exception
	{		
		AssignLeadOwnerbyLeadSourceUI ui = new AssignLeadOwnerbyLeadSourceUI(driver);
		
		fc.utobj().actionImgOption(driver, leadSource, "Associate different owner for Sales Territories");
	//	click_AssociateDifferentOwnerFor_SalesTerritories(leadSource);
		fc.utobj().selectMultipleValFromMultiSelect(driver, ui.DefaultOwner_Against_TerritoryName(driver, territoryName), corpUsers );
		fc.utobj().clickElement(driver, ui.saveBtn_AssignFor_Any);
		
		return new AssignLeadOwnerbyLeadSourceTest(driver);
	}
	
	AssignLeadOwnerbyLeadSourceTest AssociateDifferentOwnerFor_Division(ArrayList<String> corpUsers, String leadSource, String division) throws Exception
	{		
		AssignLeadOwnerbyLeadSourceUI ui = new AssignLeadOwnerbyLeadSourceUI(driver);
		
		fc.utobj().actionImgOption(driver, leadSource, "Associate different owner for Division");
	//	click_AssociateDifferentOwnerFor_Division(leadSource);
		fc.utobj().selectMultipleValFromMultiSelect(driver, ui.DefaultOwner_Against_Division(driver, division), corpUsers );
		fc.utobj().clickElement(driver, ui.saveBtn_AssignFor_Any);
		
		return new AssignLeadOwnerbyLeadSourceTest(driver);
	}
	
	// -----------------
	
	
	 void click_AssociateDifferentOwnerFor_SalesTerritories(String leadSource) throws Exception
	 	{
		AssignLeadOwnerbyLeadSourceUI ui = new AssignLeadOwnerbyLeadSourceUI(driver);
		
		fc.utobj().clickElement(driver, ui.actionBtn_Against_LeadSourceName(driver, leadSource));
		fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//div/span[contains(text(),'Associate different owner for Sales Territories')]"));

	 	}
	
	void click_AssociateDifferentOwnerFor_Division(String leadSource) throws Exception
		{
		AssignLeadOwnerbyLeadSourceUI ui = new AssignLeadOwnerbyLeadSourceUI(driver);
		
		fc.utobj().clickElement(driver, ui.actionBtn_Against_LeadSourceName(driver, leadSource));
		fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//div/span[contains(text(),'Associate different owner for Division')]"));

		}
	
	// ----------------------------
	
	void click_AssociateDifferentOwnerFor_Source(String divisionName) throws Exception
	{
	AssignLeadOwnerbyLeadSourceUI ui = new AssignLeadOwnerbyLeadSourceUI(driver);
	
	fc.utobj().clickElement(driver, ui.actionBtn_Against_DivisionName(driver, divisionName));
	fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//div/span[contains(text(),'Associate different owner for Source')]"));

	}
	
}
