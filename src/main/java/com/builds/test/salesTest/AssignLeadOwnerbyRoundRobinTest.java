package com.builds.test.salesTest;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.fs.AssignLeadOwnerbyRoundRobinUI;
import com.builds.utilities.FranconnectUtil;

class AssignLeadOwnerbyRoundRobinTest {

	private FranconnectUtil fc = new FranconnectUtil();
	private WebDriver driver;

	AssignLeadOwnerbyRoundRobinTest(WebDriver driver) {
		this.driver = driver;
	}
	
	AssignLeadOwnerbyRoundRobinTest moveUserTo_ConfiguredLeadOwners(String userName) throws Exception
	{
		fc.utobj().printTestStep("Move User to Configured Lead Owners List");
		AssignLeadOwnerbyRoundRobinUI ui = new AssignLeadOwnerbyRoundRobinUI(driver);
		fc.utobj().selectDropDown(driver, ui.availableLeadOwners_List, userName);
		fc.utobj().clickElement(driver, ui.moveToConfiguredLeadOwnersList);
		return this;
	}
	
	void moveAllUserTo_AvailableLeadOwners() throws Exception
	{
		fc.utobj().printTestStep("Move User to Available Lead Owners List");
		AssignLeadOwnerbyRoundRobinUI ui = new AssignLeadOwnerbyRoundRobinUI(driver);
		if(fc.utobj().listBoxSelectAllValues(ui.configuredLeadOwners_List) != 0)
		{
		fc.utobj().clickElement(driver, ui.moveToAvailableLeadOwners);
		}
	}
	
	void updateButton() throws Exception{
		fc.utobj().printTestStep("Click on Update Button");
		AssignLeadOwnerbyRoundRobinUI ui = new AssignLeadOwnerbyRoundRobinUI(driver);
		fc.utobj().clickElement(driver, ui.updateBtn);
	}
}
