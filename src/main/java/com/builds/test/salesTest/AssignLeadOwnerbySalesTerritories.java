package com.builds.test.salesTest;

import java.util.ArrayList;
import java.util.List;

public class AssignLeadOwnerbySalesTerritories {
	
	private String salesTerritory;
	private List<String> defaultOwners = new ArrayList<String>();

	
	public String getSalesTerritory() {
		return salesTerritory;
	}
	public void setSalesTerritory(String salesTerritory) {
		this.salesTerritory = salesTerritory;
	}
	public List<String> getDefaultOwners() {
		return defaultOwners;
	}
	public void setDefaultOwners(List<String> defaultOwners) {
		this.defaultOwners = defaultOwners;
	}

}
