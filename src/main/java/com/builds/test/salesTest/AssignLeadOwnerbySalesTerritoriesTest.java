package com.builds.test.salesTest;

import java.util.ArrayList;
import java.util.Iterator;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.fs.AssignLeadOwnerbyLeadSourceUI;
import com.builds.uimaps.fs.AssignLeadOwnerbySalesTerritoriesUI;
import com.builds.utilities.FranconnectUtil;

class AssignLeadOwnerbySalesTerritoriesTest {
	
	private WebDriver driver;
	FranconnectUtil fc = new FranconnectUtil();
	
	AssignLeadOwnerbySalesTerritoriesTest(WebDriver driver) {
		this.driver = driver;
	}
	
	
	
	AssignLeadOwnerbySalesTerritoriesTest configure1_AssignLeadOwnerbySalesTerritories(ArrayList<String> corpUsers , ArrayList<String> SalesTerritories ) throws Exception
	{
		AssignLeadOwnerbySalesTerritoriesUI ui = new AssignLeadOwnerbySalesTerritoriesUI(driver);
		
		
			Iterator<String> user = corpUsers.iterator();
			Iterator<String> territory = SalesTerritories.iterator();
			while(user.hasNext() && territory.hasNext()) 
			{
			   String value1 = user.next();
			   System.out.println(user.next());
			   String value2 = territory.next();
			   
			   //do stuff
			   fc.utobj().selectValFromMultiSelect(driver, ui.DefaultOwner_Against_SalesTerritories(driver, value2), value1);
			   
			}
		
		fc.utobj().clickElement(driver, ui.updateBtn);
		
		return new AssignLeadOwnerbySalesTerritoriesTest(driver);
	}
	
	// Correct
	AssignLeadOwnerbySalesTerritoriesTest configure_AssignLeadOwnerbySalesTerritories(ArrayList<String> corpUsercorpUsersSalesTerritories , SalesTerritories salesTerritories) throws Exception
	{
		AssignLeadOwnerbySalesTerritoriesUI ui = new AssignLeadOwnerbySalesTerritoriesUI(driver);

		/*Iterator<String> users = corpUsercorpUsersSalesTerritories.iterator();
		
		while(users.hasNext())
		{
			String user = users.next();
			System.out.println(user);
			 fc.utobj().selectValFromMultiSelect(driver, ui.DefaultOwner_Against_SalesTerritories(driver, salesTerritories.getSalesTerritoryName()), user);*/
			 fc.utobj().selectMultipleValFromMultiSelect(driver,  ui.DefaultOwner_Against_SalesTerritories(driver, salesTerritories.getSalesTerritoryName()), corpUsercorpUsersSalesTerritories);
		// }
		
		fc.utobj().clickElement(driver, ui.updateBtn);
		
		return new AssignLeadOwnerbySalesTerritoriesTest(driver);
	}
	
	/*void setOwnerToSalesTerritories_AssignLeadOwnerbySalesTerritories(ArrayList<String> corpUsers , ArrayList<String> SalesTerritories ) throws Exception
	{
		AssignLeadOwnerbySalesTerritoriesUI ui = new AssignLeadOwnerbySalesTerritoriesUI(driver);
		fc.utobj().printTestStep("Assign Lead Owners to Sales Territories");
		
		for(String user : corpUsers)
		{
				fc.utobj().selectDropDown(driver, ui.DefaultOwner_Against_SalesTerritories(driver, SalesTerritories), user);
		}		

	}*/
	
	
	AssignLeadOwnerbySalesTerritoriesTest AssociateDifferentOwnerFor_Source(ArrayList<String> corpUsers, String leadSource, String territoryName) throws Exception
	{		
		AssignLeadOwnerbySalesTerritoriesUI assignLeadOwnerbySalesTerritoriesUI = new AssignLeadOwnerbySalesTerritoriesUI(driver);
		
		fc.utobj().actionImgOption(driver, territoryName, "Associate different owner for Source");
		fc.utobj().selectMultipleValFromMultiSelect(driver, assignLeadOwnerbySalesTerritoriesUI.DefaultOwner_Against_SourceName(driver, leadSource), corpUsers );
		fc.utobj().clickElement(driver, assignLeadOwnerbySalesTerritoriesUI.saveBtn_AssignFor_Any);
		
		return new AssignLeadOwnerbySalesTerritoriesTest(driver);
	}
	
	AssignLeadOwnerbySalesTerritoriesTest AssociateDifferentOwnerFor_Division(ArrayList<String> corpUsers, String division , String territoryName) throws Exception
	{				
		AssignLeadOwnerbySalesTerritoriesUI assignLeadOwnerbySalesTerritoriesUI = new AssignLeadOwnerbySalesTerritoriesUI(driver);
		
		fc.utobj().actionImgOption(driver, territoryName, "Associate different owner for Division");
		fc.utobj().selectMultipleValFromMultiSelect(driver, assignLeadOwnerbySalesTerritoriesUI.DefaultOwner_Against_Division(driver, division), corpUsers );
		fc.utobj().clickElement(driver, assignLeadOwnerbySalesTerritoriesUI.saveBtn_AssignFor_Any);
		
		return new AssignLeadOwnerbySalesTerritoriesTest(driver);
	}
	
	
	

}
