package com.builds.test.salesTest;

public class AssignLeadOwners {
	
	private String defaultOwner;
	private String selectLeadOwnerAssignmentScheme;

	
	public String getDefaultOwner() {
		return defaultOwner;
	}
	public void setDefaultOwner(String defaultOwner) {
		this.defaultOwner = defaultOwner;
	}
	public String getSelectLeadOwnerAssignmentScheme() {
		return selectLeadOwnerAssignmentScheme;
	}
	public void setSelectLeadOwnerAssignmentScheme(String selectLeadOwnerAssignmentScheme) {
		this.selectLeadOwnerAssignmentScheme = selectLeadOwnerAssignmentScheme;
	}
	
	

}
