package com.builds.test.salesTest;

import java.util.ArrayList;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.fs.AssignLeadOwnerbySalesTerritoriesUI;
import com.builds.uimaps.fs.AssignLeadOwnersUI;
import com.builds.utilities.FranconnectUtil;

class AssignLeadOwnersTest {

	private FranconnectUtil fc = new FranconnectUtil();
	private WebDriver driver;

	AssignLeadOwnersTest(WebDriver driver) {
		this.driver = driver;
	}

	AssignLeadOwnerbyRoundRobinTest setAssignLeadOwnerbyRoundRobin() throws Exception
	{
		fc.utobj().printTestStep("Select Lead Owner Assignment Scheme : Assign Lead Owner by Round Robin");
		AssignLeadOwnersUI ui = new AssignLeadOwnersUI(driver);
		fc.utobj().clickElement(driver, ui.assignLeadOwnerbyRoundRobin_Radio);
		fc.utobj().clickElement(driver, ui.continue_btn);
		
		return new AssignLeadOwnerbyRoundRobinTest(driver);
	}
	
	AssignLeadOwnerbySalesTerritoriesTest set_AssignLeadOwnerbySalesTerritories() throws Exception
	{
		fc.utobj().printTestStep("Select Lead Owner Assignment Scheme : Assign Lead Owner by Sales Territories");
		AssignLeadOwnersUI ui = new AssignLeadOwnersUI(driver);
		fc.utobj().clickElement(driver, ui.assignLeadOwnerbySalesTerritories_Radio);
		fc.utobj().clickElement(driver, ui.continue_btn);
		
		return new AssignLeadOwnerbySalesTerritoriesTest(driver);
	}
	
	
	AssignLeadOwnerbyLeadSourceTest set_assignLeadOwnerbyLeadSource() throws Exception
	{
		fc.utobj().printTestStep("Select Lead Owner Assignment Scheme : Assign Lead Owner by Lead Source");
		AssignLeadOwnersUI ui = new AssignLeadOwnersUI(driver);
		fc.utobj().clickElement(driver, ui.assignLeadOwnerbyLeadSource_Radio);
		fc.utobj().clickElement(driver, ui.continue_btn);		
		
		return new AssignLeadOwnerbyLeadSourceTest(driver);
	}
	
	AssignLeadOwnerbyDivisionTest set_assignLeadOwnerbyDivision() throws Exception
	{
		fc.utobj().printTestStep("Select Lead Owner Assignment Scheme : Assign Lead Owner by Division");
		AssignLeadOwnersUI ui = new AssignLeadOwnersUI(driver);
		fc.utobj().clickElement(driver, ui.assignLeadOwnerbyDivision_Radio);
		fc.utobj().clickElement(driver, ui.continue_btn);		
		
		return new AssignLeadOwnerbyDivisionTest(driver);
	}
	
	void set_defaultOwner(String ownerName) throws Exception
	{
		fc.utobj().printTestStep("Assign Default Lead Owner");
		AssignLeadOwnersUI ui = new AssignLeadOwnersUI(driver);
		fc.utobj().selectDropDown(driver, ui.DefaultOwner_Select, ownerName);
		fc.utobj().clickElement(driver, ui.udpate_btn);
	}
	
	
	
}
