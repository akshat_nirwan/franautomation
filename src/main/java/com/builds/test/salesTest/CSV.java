package com.builds.test.salesTest;

	import java.io.FileWriter;
	import java.io.IOException;
	import java.util.ArrayList;
	import java.util.List;

import com.builds.utilities.FranconnectUtil;

	public class CSV {
		
		
		//Delimiter used in CSV file
		//private static final String COMMA_DELIMITER = ",";
		private static final String NEW_LINE_SEPARATOR = "\n";
		
		//CSV file header
		private static final String FILE_HEADER = "Inquiry Date,First Name,Last Name,Email,Company Name,Address1,Address2,City,State / Province,Country,Zip / Postal Code,Work Phone,Work Phone Extension,Home Phone,Home Phone Extension,Fax,Comments,Mobile,Best Time To Contact,Lead Status,Lead Source Category,Lead Source Details,Assign To,Preferred Mode of Contact,Current Net Worth,Cash Available for Investment,Investment Timeframe,Source Of Investment,Background,Division,Brand";

		public static void writeCsvFile(String fileName , List<Lead> lead ) {
			
			List<String> leadsObj = new ArrayList<String>();
			for(Lead y : lead)
			{
			CsvLead csvLead = new CsvLead();
			csvLead.getCSVLeadInfo(y);
			
			
			//Create a new list of lead objects
			leadsObj.add(csvLead.getCSVLeadInfo(y));
			
			}
			
			FileWriter fileWriter = null;
					
			try {
				fileWriter = new FileWriter(fileName);

				//Write the CSV file header
				fileWriter.append(FILE_HEADER.toString());
				
				//Add a new line separator after the header
				fileWriter.append(NEW_LINE_SEPARATOR);
				
				//Write a new lead object list to the CSV file
				for (String x : leadsObj) {
					
					fileWriter.append(x);
					fileWriter.append(NEW_LINE_SEPARATOR);
				}

		
				System.out.println("CSV file was created successfully !!!");
				
			} catch (Exception e) {
				System.out.println("Error in CsvFileWriter !!!");
				e.printStackTrace();
			} finally {
				
				try {
					fileWriter.flush();
					fileWriter.close();
				} catch (IOException e) {
					System.out.println("Error while flushing/closing fileWriter !!!");
	                e.printStackTrace();
				}
			
			}
		}
	}


