package com.builds.test.salesTest;

import java.util.ArrayList;
import java.util.List;

import com.builds.test.fs.testravi.franconnectListener;
import com.builds.utilities.FranconnectUtil;

public class CSVTest {

		
		public static void main(String[] args) {
					
			//String fileName = FranconnectUtil.config.get("testLogFolderPath")+"/student1.csv";
			String fileName = System.getProperty("user.home")+"/student123.csv";
			System.out.println(fileName);
			
			Lead lead4 = new Lead();
			lead4.setFirstName("LeadDivision");
			lead4.setLastName("PriorityTerritory(County)");
			lead4.setEmail("frantest2017@gmail.com");
			lead4.setDivision("div1");
			lead4.setCountry("USA");
			lead4.setStateProvince("Oregon");
			lead4.setCounty("Morrow");
			lead4.setBasedonAssignmentRules("yes");
			lead4.setLeadSourceCategory("cat1");
			lead4.setLeadSourceDetails("None");
			lead4.setBestTimeToContact("9");
			
			Lead lead5 = new Lead();
			lead5.setFirstName("LeadDivision111111");
			lead5.setLastName("PriorityTerritory(County)0000000");
			lead5.setEmail("frantest2017@gmail.com");
			lead5.setDivision("div1");
			lead5.setCountry("USA");
			lead5.setStateProvince("Oregon");
			lead5.setCounty("Morrow");
			lead5.setBasedonAssignmentRules("yes");
			lead5.setLeadSourceCategory("cat1");
			lead5.setLeadSourceDetails("None");
			lead5.setBestTimeToContact("9");
			
			List<Lead> leadList = new ArrayList<Lead>(); 
			leadList.add(lead4);
			leadList.add(lead5);
			
			Sales_Common_New common = new Sales_Common_New();
			common.writeCsvFile(fileName, leadList);
			System.out.println("Write CSV file:");

		}

	}


