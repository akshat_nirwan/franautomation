package com.builds.test.salesTest;

class Call {
	private String Subject;
	private String Date;
	private String TimeHH;
	private String TimeMM;
	private String TimeAMPM;
	private String callStatus;
	private String CommunicationType;
	private String Comments;

	public String getSubject() {
		return Subject;
	}

	public void setSubject(String subject) {
		Subject = subject;
	}

	public String getDate() {
		return Date;
	}

	public void setDate(String date) {
		Date = date;
	}

	public String getTimeHH() {
		return TimeHH;
	}

	public void setTimeHH(String timeHH) {
		TimeHH = timeHH;
	}

	public String getTimeMM() {
		return TimeMM;
	}

	public void setTimeMM(String timeMM) {
		TimeMM = timeMM;
	}

	public String getTimeAMPM() {
		return TimeAMPM;
	}

	public void setTimeAMPM(String timeAMPM) {
		TimeAMPM = timeAMPM;
	}

	public String getCallStatus() {
		return callStatus;
	}

	public void setCallStatus(String callStatus) {
		this.callStatus = callStatus;
	}

	public String getCommunicationType() {
		return CommunicationType;
	}

	public void setCommunicationType(String communicationType) {
		CommunicationType = communicationType;
	}

	public String getComments() {
		return Comments;
	}

	public void setComments(String comments) {
		Comments = comments;
	}

}
