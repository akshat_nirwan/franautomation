package com.builds.test.salesTest;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.fs.CallUI;
import com.builds.utilities.FranconnectUtil;

public class CallTest {
	
	FranconnectUtil fc = new FranconnectUtil();
	WebDriver driver;
	
	public CallTest(WebDriver driver) {
		this.driver = driver;
	}
	
	void fillNewCall(Call call) throws Exception {
		CallUI ui = new CallUI(driver);
		
		if(call.getSubject()!=null)
		{
			fc.utobj().sendKeys(driver, ui.subject, call.getSubject());
		}
		
		if(call.getDate()!=null)
		{
			fc.utobj().sendKeys(driver, ui.date, call.getDate());
		}
		
		if(call.getTimeHH()!=null)
		{
			fc.utobj().sendKeys(driver, ui.timeHH, call.getTimeHH());
		}
		
		if(call.getTimeMM()!=null)
		{
			fc.utobj().sendKeys(driver, ui.timeMM, call.getTimeMM());
		}
		
		if(call.getTimeAMPM()!=null)
		{
			fc.utobj().sendKeys(driver, ui.timeAMPM, call.getTimeAMPM());
		}
		
		if(call.getCallStatus()!=null)
		{
			fc.utobj().sendKeys(driver, ui.callStatus, call.getCallStatus());
		}
		
		if(call.getCommunicationType()!=null)
		{
			fc.utobj().sendKeys(driver, ui.communicationtype, call.getCommunicationType());
		}
		
		if(call.getComments()!=null)
		{
			fc.utobj().sendKeys(driver, ui.comments, call.getComments());
		}
		
	}
	
	void click_Add() throws Exception {
		CallUI ui = new CallUI(driver);
		fc.utobj().clickElement(driver, ui.addButton);
	}

	void click_No_ConfirmSchedule() throws Exception {
		CallUI ui = new CallUI(driver);
		fc.utobj().clickElement(driver, ui.No_ConfirmScheduleTask);
	}

	void click_Yes_ConfirmSchedule() throws Exception {
		CallUI ui = new CallUI(driver);
		fc.utobj().clickElement(driver, ui.Yes_ConfirmScheduleTask);
	}
	
	void verify_CallUnderActivityHistory_OnPrimaryInfo(Call call) throws Exception {
		if(! fc.utobj().assertLinkText(driver, call.getSubject()))
		{
			fc.utobj().throwsException("Call not present in Activity History");
		}
	}
	
	void clickAndOpen_CallUnderActivityHistory_AndSwitchToFrame(Call call) throws Exception {
		fc.utobj().clickElement(driver, ".//*[contains(text(),'"+call.getSubject()+"')]");
		fc.commonMethods().switch_cboxIframe_frameId(driver);
	}
	
	void verify_callDetailsOniFrame(Call call) throws Exception {
		CallUI ui = new CallUI(driver);
		if (!fc.utobj().assertPageSource(driver, call.getSubject())) {
			fc.utobj().throwsException("Call Details not matching on call detail iframe");
		}
		fc.utobj().clickElement(driver, ui.closeBtn_callDetailsIframe);
	}
	
	void verify_CallDetailsinDetailedHistory_RemarksAndCallHistoryTab(Call call) throws Exception {
		PrimaryInfoTest primaryInfoTest = new PrimaryInfoTest(driver);
		primaryInfoTest.click_DetailedHistory_ActivityHistory().verify_RemarksAndCallHistory_In_DetailedHistory(call.getSubject());
	}


}
