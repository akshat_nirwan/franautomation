package com.builds.test.salesTest;

class Campaign {

	private String CampaignName;
	private String Description;
	private String Accessibility;
	private String Services;
	private String SenderName;
	private String ReplytoAddress;
	private String CampaignStartDate;
	private String CampaignTimeZone;
	private String SendFirstEmail;
	
	private String subscriptionType;
	private String campaignType;
	
	public String getSubscriptionType() {
		return subscriptionType;
	}

	public void setSubscriptionType(String subscriptionType) {
		this.subscriptionType = subscriptionType;
	}

	public String getCampaignType() {
		return campaignType;
	}

	public void setCampaignType(String campaignType) {
		this.campaignType = campaignType;
	}

	public String getSenderName() {
		return SenderName;
	}

	public void setSenderName(String senderName) {
		SenderName = senderName;
	}

	public String getReplytoAddress() {
		return ReplytoAddress;
	}

	public void setReplytoAddress(String replytoAddress) {
		ReplytoAddress = replytoAddress;
	}

	public String getCampaignStartDate() {
		return CampaignStartDate;
	}

	public void setCampaignStartDate(String campaignStartDate) {
		CampaignStartDate = campaignStartDate;
	}

	public String getCampaignTimeZone() {
		return CampaignTimeZone;
	}

	public void setCampaignTimeZone(String campaignTimeZone) {
		CampaignTimeZone = campaignTimeZone;
	}

	public String getSendFirstEmail() {
		return SendFirstEmail;
	}

	public void setSendFirstEmail(String sendFirstEmail) {
		SendFirstEmail = sendFirstEmail;
	}

	public String getCampaignName() {
		return CampaignName;
	}

	public void setCampaignName(String campaignName) {
		CampaignName = campaignName;
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		Description = description;
	}

	public String getAccessibility() {
		return Accessibility;
	}

	public void setAccessibility(String accessibility) {
		Accessibility = accessibility;
	}

	public String getServices() {
		return Services;
	}

	public void setServices(String services) {
		Services = services;
	}

}
