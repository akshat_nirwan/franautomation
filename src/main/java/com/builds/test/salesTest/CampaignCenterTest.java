package com.builds.test.salesTest;

import org.openqa.selenium.WebDriver;

import com.builds.test.common.CommonMethods;
import com.builds.uimaps.fs.CampaignCenterUI;
import com.builds.uimaps.fs.CreateCampaignUI;
import com.builds.utilities.FranconnectUtil;

class CampaignCenterTest {
	private WebDriver driver;
	private FranconnectUtil fc = new FranconnectUtil();

	CampaignCenterTest(WebDriver driver) {
		this.driver = driver;
	}

	CampaignCenterTest clickCreateButton() throws Exception {
		fc.utobj().printTestStep("Click on Create Button");

		return this;
	}

	CampaignCenterTest openMenu() throws Exception {
		fc.utobj().printTestStep("Open Menu");

		return this;
	}
	
	void click_manageCampaigns() throws Exception
	{
		fc.utobj().printTestStep("Click on Manage Campaigns button");
		CampaignCenterUI ui = new CampaignCenterUI(driver);
		fc.utobj().clickElement(driver, ui.manageCampaigns_Btn);
	}
	
	void open_ManageCampaignsFromTopLeftCornerMenu() throws Exception
	{
		fc.utobj().printTestStep("Click on Campaigns button from top left corner menu");
		CampaignCenterUI ui = new CampaignCenterUI(driver);
		fc.utobj().clickElement(driver, ui.menuBtn_leftTopCorner);
		fc.utobj().clickElement(driver, ui.campaignsBtn_TopLeftMenu);
	}

	void createCampaign(Campaign campaign) throws Exception  {
		
		CampaignCenterUI campaignCenterUI = new CampaignCenterUI(driver);
		CreateCampaignUI ui = new CreateCampaignUI(driver);
		fc.utobj().clickElement(driver, campaignCenterUI.clickCreate_IconXpath);
		fc.utobj().clickElement(driver, campaignCenterUI.campaign_Btn);
		CommonMethods commonMethods = new CommonMethods();
		commonMethods.switch_frameClass_newLayoutcboxIframe(driver);
		fill_CreateCampaign(campaign);
		fc.utobj().clickElement(driver, ui.startCampaign_Btn);
	
	}
	
	Campaign fill_CreateCampaign(Campaign campaign) throws Exception
	{
		CreateCampaignUI ui = new CreateCampaignUI(driver);
		
		if(campaign.getCampaignName() != null)
		{
			fc.utobj().sendKeys(driver, ui.campaignName_text, campaign.getCampaignName());
		}
		
		if(campaign.getDescription() != null )
		{
			fc.utobj().sendKeys(driver, ui.description_text, campaign.getDescription());
		}
		
		if(campaign.getAccessibility() != null)
		{
			fc.utobj().selectDropDown(driver, ui.accessibility_dropDown, campaign.getAccessibility());
		}
		
		if(campaign.getSubscriptionType() != null)
		{
			fc.utobj().selectDropDown(driver, ui.subscriptionType_dropDown	, campaign.getSubscriptionType());
		}
		
		if(campaign.getCampaignType() != null)
		{
			if(campaign.getCampaignType().equalsIgnoreCase("Promotional"))
			{
				fc.utobj().clickElement(driver, ui.Promotional_CampaignType_RadioBtn);
			}
			if(campaign.getCampaignType().equalsIgnoreCase("Status Driven"))
			{
				fc.utobj().clickElement(driver, ui.StatusDriven_CampaignType_RadioBtn);
			}
		}		
		return  campaign;
	}

	CreateTemplateTest clickCreateTemplate() throws Exception {
		fc.utobj().printTestStep("Click Create Template");

		return new CreateTemplateTest(driver);
	}

}
