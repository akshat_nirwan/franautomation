package com.builds.test.salesTest;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.fs.CampaignCenter_SelectRecipientsUI;
import com.builds.utilities.FranconnectUtil;

public class CampaignCenter_SelectRecipientsTest {
	
	WebDriver driver;
	FranconnectUtil fc = new FranconnectUtil();
	
	public CampaignCenter_SelectRecipientsTest(WebDriver driver) {
		this.driver=driver;
	} 
	
	void click_ContinueBtn() throws Exception
	{
		CampaignCenter_SelectRecipientsUI ui = new CampaignCenter_SelectRecipientsUI(driver);
		fc.utobj().printTestStep("Click on Continue Button");
		fc.utobj().clickElement(driver, ui.continue_Btn);
	}

}
