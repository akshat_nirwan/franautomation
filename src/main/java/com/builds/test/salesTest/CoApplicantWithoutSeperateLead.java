package com.builds.test.salesTest;

class CoApplicantWithoutSeperateLead {

	private String coApplicantType;
	private String firstName;
	private String lastName;
	private String coApplicantRelationship;
	private String phone;
	private String ext;
	private String fax;
	private String emailID;
	private String address;
	private String city;
	private String country;
	private String stateID;
	private String zip;
	private String socialSecurityNumber;
	private String date;

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getCoApplicantType() {
		return coApplicantType;
	}

	public void setCoApplicantType(String coApplicantType) {
		this.coApplicantType = coApplicantType;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getCoApplicantRelationship() {
		return coApplicantRelationship;
	}

	public void setCoApplicantRelationship(String coApplicantRelationship) {
		this.coApplicantRelationship = coApplicantRelationship;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getExt() {
		return ext;
	}

	public void setExt(String ext) {
		this.ext = ext;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getEmailID() {
		return emailID;
	}

	public void setEmailID(String emailID) {
		this.emailID = emailID;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getStateID() {
		return stateID;
	}

	public void setStateID(String stateID) {
		this.stateID = stateID;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getSocialSecurityNumber() {
		return socialSecurityNumber;
	}

	public void setSocialSecurityNumber(String socialSecurityNumber) {
		this.socialSecurityNumber = socialSecurityNumber;
	}

	public String getCoapplicantFullName() {
		String firstName = getFirstName();
		String lastName = getLastName();

		if (firstName == null) {
			firstName = "";
		}
		if (lastName == null) {
			lastName = "";
		}
		return (firstName + " " + lastName).trim();
	}

}
