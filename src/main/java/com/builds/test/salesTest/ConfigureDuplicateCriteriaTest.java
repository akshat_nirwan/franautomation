package com.builds.test.salesTest;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.builds.uimaps.fs.ConfigureDuplicateCriteriaUI;
import com.builds.utilities.FranconnectUtil;

public class ConfigureDuplicateCriteriaTest {

	FranconnectUtil fc = new FranconnectUtil();
	WebDriver driver;

	public ConfigureDuplicateCriteriaTest(WebDriver driver) {
		this.driver = driver;
	}

	void click_ConfigureDuplicateLeadOwnerAssignment() throws Exception {
		ConfigureDuplicateCriteriaUI configureDuplicateCriteriaUI = new ConfigureDuplicateCriteriaUI(driver);
		fc.utobj().clickElement(driver, configureDuplicateCriteriaUI.configureDuplicateLeadOwnerAssignment_Link);
	}

	void set_DuplicateCriteria1(ArrayList<String> fields) throws Exception {
		ConfigureDuplicateCriteriaUI configureDuplicateCriteriaUI = new ConfigureDuplicateCriteriaUI(driver);

		Select availableFields = new Select(configureDuplicateCriteriaUI.availableFields_DuplicateCriteria1);
		Select configuredCriteria = new Select(configureDuplicateCriteriaUI.configuredCriteria_DuplicateCriteria1);

		List<WebElement> configuredCriteria_options = configuredCriteria.getOptions();
		if (configuredCriteria_options.size() > 0) {
			for (int i = 0; i < configuredCriteria_options.size(); i++) {
				configuredCriteria.selectByIndex(0);
				fc.utobj().clickElement(driver, configureDuplicateCriteriaUI.moveLeft_ArrowButton_DuplicateCriteria1);
			}
		}

		for (String field : fields) {
			/*
			 * Iterator<WebElement> itr = options.iterator();
			 * while(itr.hasNext()) { fc.utobj().clickElement(driver,
			 * itr.next()); }
			 */
			availableFields.selectByVisibleText(field);
			fc.utobj().clickElement(driver, configureDuplicateCriteriaUI.moveRight_ArrowButton_DuplicateCriteria1);
		}

		fc.utobj().clickElement(driver, configureDuplicateCriteriaUI.submitBtn);

	}

	void set_DuplicateCriteria2(ArrayList<String> fields) throws Exception {
		ConfigureDuplicateCriteriaUI configureDuplicateCriteriaUI = new ConfigureDuplicateCriteriaUI(driver);

		Select availableFields = new Select(configureDuplicateCriteriaUI.availableFields_DuplicateCriteria2);
		Select configuredCriteria = new Select(configureDuplicateCriteriaUI.configuredCriteria_DuplicateCriteria2);

		List<WebElement> configuredCriteria_options = configuredCriteria.getOptions();
		if (configuredCriteria_options.size() > 0) {
			for (int i = 0; i < configuredCriteria_options.size(); i++) {
				configuredCriteria.selectByIndex(0);
				fc.utobj().clickElement(driver, configureDuplicateCriteriaUI.moveLeft_ArrowButton_DuplicateCriteria2);
			}
		}

		for (String field : fields) {
			/*
			 * Iterator<WebElement> itr = options.iterator();
			 * while(itr.hasNext()) { fc.utobj().clickElement(driver,
			 * itr.next()); }
			 */
			availableFields.selectByVisibleText(field);
			fc.utobj().clickElement(driver, configureDuplicateCriteriaUI.moveRight_ArrowButton_DuplicateCriteria2);
		}

		fc.utobj().clickElement(driver, configureDuplicateCriteriaUI.submitBtn);
	}
}
