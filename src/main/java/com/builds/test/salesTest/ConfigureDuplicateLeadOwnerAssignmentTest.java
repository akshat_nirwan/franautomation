package com.builds.test.salesTest;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.fs.ConfigureDuplicateLeadOwnerAssignmentUI;
import com.builds.utilities.FranconnectUtil;

class ConfigureDuplicateLeadOwnerAssignmentTest {
	
	WebDriver driver;
	
	ConfigureDuplicateLeadOwnerAssignmentTest(WebDriver driver) {
		this.driver = driver;
}

	
	 FranconnectUtil fc = new FranconnectUtil();
	 
	
	void set_ReassignTheExistingLeadToTheOwnerOfTheNewLead() throws Exception
	{
		ConfigureDuplicateLeadOwnerAssignmentUI assignmentUI = new ConfigureDuplicateLeadOwnerAssignmentUI(driver);
			 fc.utobj().clickElement(driver, assignmentUI.reassignExistingLeadToOwnerOfNewLead_radioBtn);
			 fc.utobj().clickElement(driver, assignmentUI.submitBtn);
	}
	
	void set_assignNewLeadToOwnerOfExistingLead() throws Exception
	{
		ConfigureDuplicateLeadOwnerAssignmentUI assignmentUI = new ConfigureDuplicateLeadOwnerAssignmentUI(driver);
			fc.utobj().clickElement(driver, assignmentUI.assignNewLeadToOwnerOfExistingLead_radioBtn);
			 fc.utobj().clickElement(driver, assignmentUI.submitBtn);
	}
	
	void set_assignNewLeadToOwnerBasedOnLeadAssignmentRules() throws Exception
	{
		ConfigureDuplicateLeadOwnerAssignmentUI assignmentUI = new ConfigureDuplicateLeadOwnerAssignmentUI(driver);
		fc.utobj().clickElement(driver, assignmentUI.assignNewLeadToOwnerBasedOnLeadAssignmentRules_radioBtn);
		 fc.utobj().clickElement(driver, assignmentUI.submitBtn);
	}
	
	void set_onlyAssignNewLeadToSameOwnerAsTheExistingLeadIfTheyAreAssignedToSameDivision() throws Exception
	{
		ConfigureDuplicateLeadOwnerAssignmentUI assignmentUI = new ConfigureDuplicateLeadOwnerAssignmentUI(driver);
		fc.utobj().clickElement(driver, assignmentUI.onlyAssignNewLeadToSameOwnerAsTheExistingLeadIfTheyAreAssignedToSameDivision_radioBtn);
		 fc.utobj().clickElement(driver, assignmentUI.submitBtn);
	}
	
	void selectRadioButton_assignNewLeadToOwnerOfExistingLead() throws Exception
	{
		ConfigureDuplicateLeadOwnerAssignmentUI assignmentUI = new ConfigureDuplicateLeadOwnerAssignmentUI(driver);
		fc.utobj().clickElement(driver, assignmentUI.assignNewLeadToOwnerOfExistingLead_radioBtn);
	}

	void click_CancelBtn() throws Exception
	{
		ConfigureDuplicateLeadOwnerAssignmentUI assignmentUI = new ConfigureDuplicateLeadOwnerAssignmentUI(driver);
		fc.utobj().clickElement(driver, assignmentUI.cancelBtn);
	}
	
}
