package com.builds.test.salesTest;

public class ConfigureEmailSentPriorToFDDEmail {

	private String sendEmailNotification;
	private String emailSubject;
	private String timeIntervalMinutes;
	private String emailContentPriorToFDDEmail;
	private String keywordsForReplacement;

	public String getSendEmailNotification() {
		return sendEmailNotification;
	}

	public void setSendEmailNotification(String sendEmailNotification) {
		this.sendEmailNotification = sendEmailNotification;
	}

	public String getEmailSubject() {
		return emailSubject;
	}

	public void setEmailSubject(String emailSubject) {
		this.emailSubject = emailSubject;
	}

	public String getTimeIntervalMinutes() {
		return timeIntervalMinutes;
	}

	public void setTimeIntervalMinutes(String timeIntervalMinutes) {
		this.timeIntervalMinutes = timeIntervalMinutes;
	}

	public String getEmailContentPriorToFDDEmail() {
		return emailContentPriorToFDDEmail;
	}

	public void setEmailContentPriorToFDDEmail(String emailContentPriorToFDDEmail) {
		this.emailContentPriorToFDDEmail = emailContentPriorToFDDEmail;
	}

	public String getKeywordsForReplacement() {
		return keywordsForReplacement;
	}

	public void setKeywordsForReplacement(String keywordsForReplacement) {
		this.keywordsForReplacement = keywordsForReplacement;
	}

}
