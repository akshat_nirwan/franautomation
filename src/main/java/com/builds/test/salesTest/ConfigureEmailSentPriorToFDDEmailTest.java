package com.builds.test.salesTest;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.fs.ConfigureEmailSentPriorToFDDEmailUI;
import com.builds.utilities.FranconnectUtil;

public class ConfigureEmailSentPriorToFDDEmailTest {

	private FranconnectUtil fc = new FranconnectUtil();
	private WebDriver driver;

	ConfigureEmailSentPriorToFDDEmailTest configureEmailSentPriorToFDDEmail(WebDriver driver,
			ConfigureEmailSentPriorToFDDEmail configureEmailSentPriorToFDDEmail) throws Exception {
		ConfigureEmailSentPriorToFDDEmailUI configureEmailSentPriorToFDDEmailUI = new ConfigureEmailSentPriorToFDDEmailUI(
				driver);
		fc.utobj().printTestStep("Fill and Configure Email Sent Prior To FDD Email");
		fill_ConfigureEmailSentPriorToFDDEmail(configureEmailSentPriorToFDDEmail);
		fc.utobj().clickElement(driver, configureEmailSentPriorToFDDEmailUI.saveBtn);
		return new ConfigureEmailSentPriorToFDDEmailTest(driver);
	}

	void fill_ConfigureEmailSentPriorToFDDEmail(ConfigureEmailSentPriorToFDDEmail configureEmailSentPriorToFDDEmail)
			throws Exception {
		ConfigureEmailSentPriorToFDDEmailUI configureEmailSentPriorToFDDEmailUI = new ConfigureEmailSentPriorToFDDEmailUI(
				driver);

		if (configureEmailSentPriorToFDDEmail.getSendEmailNotification() != null) {
			if (configureEmailSentPriorToFDDEmail.getSendEmailNotification().equalsIgnoreCase("yes")) {
				fc.utobj().clickElement(driver, configureEmailSentPriorToFDDEmailUI.sendEmailNotificationYes);
			}
			if (configureEmailSentPriorToFDDEmail.getSendEmailNotification().equalsIgnoreCase("no")) {
				fc.utobj().clickElement(driver, configureEmailSentPriorToFDDEmailUI.sendEmailNotificationNo);
			}
		}

		if (configureEmailSentPriorToFDDEmail.getEmailSubject() != null) {
			fc.utobj().sendKeys(driver, configureEmailSentPriorToFDDEmailUI.emailSubject,
					configureEmailSentPriorToFDDEmail.getEmailSubject());
		}

		if (configureEmailSentPriorToFDDEmail.getTimeIntervalMinutes() != null) {
			fc.utobj().sendKeys(driver, configureEmailSentPriorToFDDEmailUI.timeIntervalMinutes,
					configureEmailSentPriorToFDDEmail.getTimeIntervalMinutes());
		}

		if (configureEmailSentPriorToFDDEmail.getEmailContentPriorToFDDEmail() != null) {
			fc.utobj().switchFrameById(driver, "templateText_ifr");
			fc.utobj().sendKeys(driver, configureEmailSentPriorToFDDEmailUI.emailContentPriorToFDDEmail,
					configureEmailSentPriorToFDDEmail.getEmailContentPriorToFDDEmail());
			fc.utobj().switchFrameToDefault(driver);
		}

	}

	ConfigureEmailSentPriorToFDDEmailTest(WebDriver driver) {
		this.driver = driver;
	}

}
