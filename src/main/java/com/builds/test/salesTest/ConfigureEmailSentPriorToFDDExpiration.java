package com.builds.test.salesTest;

import java.util.ArrayList;
import java.util.List;

public class ConfigureEmailSentPriorToFDDExpiration {
	
	private String sendEmailNotification;
	private String emailSubject;
	private String daysPrior;
	private String fromEmail;
	private List<String> recipients = new ArrayList<String>();
	private String emailContentPriorToFDDExpiration;
	
	
	public String getSendEmailNotification() {
		return sendEmailNotification;
	}
	public void setSendEmailNotification(String sendEmailNotification) {
		this.sendEmailNotification = sendEmailNotification;
	}
	public String getEmailSubject() {
		return emailSubject;
	}
	public void setEmailSubject(String emailSubject) {
		this.emailSubject = emailSubject;
	}
	public String getDaysPrior() {
		return daysPrior;
	}
	public void setDaysPrior(String daysPrior) {
		this.daysPrior = daysPrior;
	}
	public String getFromEmail() {
		return fromEmail;
	}
	public void setFromEmail(String fromEmail) {
		this.fromEmail = fromEmail;
	}
	
	public List<String> getRecipients() {
		return recipients;
	}
	public void setRecipients(List<String> recipients) {
		this.recipients = recipients;
	}
	
	
	
	public String getEmailContentPriorToFDDExpiration() {
		return emailContentPriorToFDDExpiration;
	}
	public void setEmailContentPriorToFDDExpiration(String emailContentPriorToFDDExpiration) {
		this.emailContentPriorToFDDExpiration = emailContentPriorToFDDExpiration;
	}
	
	

}
