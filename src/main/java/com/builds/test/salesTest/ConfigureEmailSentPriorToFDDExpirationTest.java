package com.builds.test.salesTest;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.fs.ConfigureEmailSentPriorToFDDExpirationUI;
import com.builds.utilities.FranconnectUtil;

public class ConfigureEmailSentPriorToFDDExpirationTest {

	private FranconnectUtil fc = new FranconnectUtil();
	private WebDriver driver;
	
	ConfigureEmailSentPriorToFDDExpirationTest configureEmail(ConfigureEmailSentPriorToFDDExpiration configureEmailSentPriorToFDDExpiration) throws Exception
	{
		ConfigureEmailSentPriorToFDDExpirationUI configureEmailSentPriorToFDDExpirationUI = new ConfigureEmailSentPriorToFDDExpirationUI(driver);
		fc.utobj().printTestStep("Configuring Email content of the email notification that will go out to the Users prior to FDD Expiration");
		fill_ConfigureEmailSentPriorToFDDExpiration(configureEmailSentPriorToFDDExpiration);
		fc.utobj().clickElement(driver, configureEmailSentPriorToFDDExpirationUI.configureBtn);
		return new ConfigureEmailSentPriorToFDDExpirationTest(driver);
	}
	
	void fill_ConfigureEmailSentPriorToFDDExpiration(ConfigureEmailSentPriorToFDDExpiration configureEmailSentPriorToFDDExpiration) throws Exception
	{
		ConfigureEmailSentPriorToFDDExpirationUI configureEmailSentPriorToFDDExpirationUI = new ConfigureEmailSentPriorToFDDExpirationUI(driver);
		
		if(configureEmailSentPriorToFDDExpiration.getSendEmailNotification()!=null)
		{
			if(configureEmailSentPriorToFDDExpiration.getSendEmailNotification().equalsIgnoreCase("yes"))
			{
				fc.utobj().clickElement(driver, configureEmailSentPriorToFDDExpirationUI.sendEmailNotification_Yes);
				if(configureEmailSentPriorToFDDExpiration.getEmailSubject()!=null)
				{
					fc.utobj().sendKeys(driver, configureEmailSentPriorToFDDExpirationUI.emailSubject, configureEmailSentPriorToFDDExpiration.getEmailSubject());
				}
				
				if(configureEmailSentPriorToFDDExpiration.getDaysPrior()!=null)
				{
					fc.utobj().sendKeys(driver, configureEmailSentPriorToFDDExpirationUI.daysPrior, configureEmailSentPriorToFDDExpiration.getDaysPrior());
				}
				
				if(configureEmailSentPriorToFDDExpiration.getFromEmail()!=null)
				{
					fc.utobj().sendKeys(driver, configureEmailSentPriorToFDDExpirationUI.fromEmail, configureEmailSentPriorToFDDExpiration.getFromEmail());
				}
				
				if(configureEmailSentPriorToFDDExpiration.getRecipients()!=null)
				{
					fc.utobj().selectMultipleValFromMultiSelect(driver, configureEmailSentPriorToFDDExpirationUI.recipients_dropdown, configureEmailSentPriorToFDDExpiration.getRecipients());
				}
				
				if(configureEmailSentPriorToFDDExpiration.getEmailContentPriorToFDDExpiration()!=null)
				{
					fc.utobj().switchFrameById(driver, configureEmailSentPriorToFDDExpirationUI.email_iframeID);
					if(configureEmailSentPriorToFDDExpiration.getEmailContentPriorToFDDExpiration()!=null)
					{
						fc.utobj().sendKeys(driver, configureEmailSentPriorToFDDExpirationUI.emailBody, configureEmailSentPriorToFDDExpiration.getEmailContentPriorToFDDExpiration());
					}
					fc.utobj().switchFrameToDefault(driver);
				}		
			}
			if(configureEmailSentPriorToFDDExpiration.getSendEmailNotification().equalsIgnoreCase("no"))
			{
				fc.utobj().clickElement(driver, configureEmailSentPriorToFDDExpirationUI.sendEmailNotification_No);
			}
		}
		
		
		
	}

	ConfigureEmailSentPriorToFDDExpirationTest(WebDriver driver) {
		this.driver = driver;
	}

}
