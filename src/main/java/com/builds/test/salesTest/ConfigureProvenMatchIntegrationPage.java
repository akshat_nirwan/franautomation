package com.builds.test.salesTest;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.fs.ConfigureProvenMatchIntegrationPageUI;
import com.builds.utilities.FranconnectUtil;

class ConfigureProvenMatchIntegrationPage {
	private FranconnectUtil fc = new FranconnectUtil();
	private WebDriver driver;
	
	ConfigureProvenMatchIntegrationPage(WebDriver driver) {
		this.driver = driver;
	}
	
	ConfigureProvenMatchIntegrationPage enableProvenMatchIntegration_ON() throws Exception
	{
		ConfigureProvenMatchIntegrationPageUI ui = new ConfigureProvenMatchIntegrationPageUI(driver);
		fc.utobj().printTestStep("Enable Proven Match Integration : On");
		fc.utobj().clickElement(driver, ui.on);
		fc.commonMethods().Click_Save_Input_ByValue(driver);
		return this;
	}
	
	ConfigureProvenMatchIntegrationPage enableProvenMatchIntegration_OFF() throws Exception
	{
		ConfigureProvenMatchIntegrationPageUI ui = new ConfigureProvenMatchIntegrationPageUI(driver);
		fc.utobj().printTestStep("Enable Proven Match Integration : Off");
		fc.utobj().clickElement(driver, ui.off);
		fc.commonMethods().Click_Save_Input_ByValue(driver);
		return this;
	}
	
}
