package com.builds.test.salesTest;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.fs.ConfigureSalesEmailParsingServerUI;
import com.builds.utilities.FranconnectUtil;

public class ConfigureSalesEmailParsingServerTest {
	
	WebDriver driver;
	FranconnectUtil fc = new FranconnectUtil();
	
	public ConfigureSalesEmailParsingServerTest(WebDriver driver) {
		this.driver = driver;
}
	
	void configureSalesEmailParsingServer() throws Exception
	{
		ConfigureSalesEmailParsingServerUI configureSalesEmailParsingServerUI = new ConfigureSalesEmailParsingServerUI(driver);
		fc.utobj().clickElement(driver, configureSalesEmailParsingServerUI.addServer_Btn);
		fc.utobj().sendKeys(driver, configureSalesEmailParsingServerUI.serverName_TextBox, "mail.franconnect.com");
		fc.utobj().sendKeys(driver, configureSalesEmailParsingServerUI.user_TextBox, "uatparsing@franconnect.com");
		fc.utobj().sendKeys(driver, configureSalesEmailParsingServerUI.password_TextBox, "sdg@1a@Hfs");
		fc.utobj().clickElement(driver, configureSalesEmailParsingServerUI.add_Btn);
	}

}
