package com.builds.test.salesTest;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.fs.ConfigureServiceListsUI;
import com.builds.utilities.FranconnectUtil;

class ConfigureServiceListsTest {

	FranconnectUtil fc = new FranconnectUtil();
	WebDriver driver;

	public ConfigureServiceListsTest(WebDriver driver) {
		this.driver = driver;
	}

	void addService(WebDriver driver) throws Exception {
		ConfigureServiceLists configureServiceLists = new ConfigureServiceLists();
		ConfigureServiceListsUI configureServiceListsUI = new ConfigureServiceListsUI(driver);
		fc.utobj().clickElement(driver, configureServiceListsUI.addServiceBtn);
		fc.utobj().switchFrameToDefault(driver);
		fc.utobj().sendKeys(driver, configureServiceListsUI.serviceName, configureServiceLists.getServiceName());
		fc.utobj().clickElement(driver, configureServiceListsUI.addBtn);
		fc.utobj().clickElement(driver, configureServiceListsUI.closeBtn);
	}

	void modifyService() {

	}

	void deleteService() {

	}

	void printServiceSummary() {

	}

}
