package com.builds.test.salesTest;

class ConfigureSiteSubmissionForm {

	private String displayName;
	private String isExportableAndSearchable;
	private String fieldType;
	private String maximumLength;
	private String validation;

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getIsExportableAndSearchable() {
		return isExportableAndSearchable;
	}

	public void setIsExportableAndSearchable(String isExportableAndSearchable) {
		this.isExportableAndSearchable = isExportableAndSearchable;
	}

	public String getFieldType() {
		return fieldType;
	}

	public void setFieldType(String fieldType) {
		this.fieldType = fieldType;
	}

	public String getMaximumLength() {
		return maximumLength;
	}

	public void setMaximumLength(String maximumLength) {
		this.maximumLength = maximumLength;
	}

	public String getValidation() {
		return validation;
	}

	public void setValidation(String validation) {
		this.validation = validation;
	}

}
