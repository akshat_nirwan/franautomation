package com.builds.test.salesTest;

class ConfigureWebFormSettings {

	String leadSourceCategory;
	String leadSourceDetails;

	public String getLeadSourceCategory() {
		return leadSourceCategory;
	}

	public void setLeadSourceCategory(String leadSourceCategory) {
		this.leadSourceCategory = leadSourceCategory;
	}

	public String getLeadSourceDetails() {
		return leadSourceDetails;
	}

	public void setLeadSourceDetails(String leadSourceDetails) {
		this.leadSourceDetails = leadSourceDetails;
	}

}
