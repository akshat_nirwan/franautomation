package com.builds.test.salesTest;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.fs.ConfigureWebFormSettingsUI;
import com.builds.utilities.FranconnectUtil;

public class ConfigureWebFormSettingsTest {

	WebDriver driver;
	FranconnectUtil fc = new FranconnectUtil();

	ConfigureWebFormSettingsTest(WebDriver driver) {
		this.driver = driver;
	}

	void fill_WebFormSettings() throws Exception {
		ConfigureWebFormSettingsUI configureWebFormSettingsTestUI = new ConfigureWebFormSettingsUI(driver);
		ConfigureWebFormSettings configureWebFormSettings = new ConfigureWebFormSettings();

		if (configureWebFormSettings.getLeadSourceCategory() != null) {
			fc.utobj().selectDropDown(driver, configureWebFormSettingsTestUI.leadSourceCategory,
					configureWebFormSettings.getLeadSourceCategory());
		}
	}

}
