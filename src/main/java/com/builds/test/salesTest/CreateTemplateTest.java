package com.builds.test.salesTest;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.fs.CreateTemplateUI;
import com.builds.uimaps.fs.TemplateUI;
import com.builds.utilities.FranconnectUtil;

class CreateTemplateTest {
	private WebDriver driver;
	private FranconnectUtil fc = new FranconnectUtil();

	CreateTemplateTest(WebDriver driver) {
		this.driver = driver;
	}

	void clickSave() throws Exception {
		fc.utobj().printTestStep("Click Save Button");
		fc.commonMethods().Click_Save_Button_ByValue(driver);
	}

	CreateTemplateTest clickCodeYourOwn() throws Exception {
		fc.utobj().printTestStep("Click - Code your own");
		CreateTemplateUI ui = new CreateTemplateUI(driver);
		fc.utobj().clickElement(driver, ui.codeYourOwn);
		return this;
	}
	
	void click_SaveAndContinue() throws Exception
	{
		fc.utobj().printTestStep("Click on Save and Continue button");
		CreateTemplateUI createTemplateUI = new CreateTemplateUI(driver);
		fc.utobj().clickElement(driver, createTemplateUI.saveAndContinue_Btn);
	}

	void fillTemplateDetails(WebDriver driver, Template template) throws Exception {
		TemplateUI ui = new TemplateUI(driver);

		fc.utobj().printTestStep("Fill Template Details");

		if (template.getTemplateName() != null) {
			fc.utobj().sendKeys(driver, ui.mailTitle, template.getTemplateName());
		}

		if (template.getEmailSubject() != null) {
			fc.utobj().sendKeys(driver, ui.mailSubject, template.getEmailSubject());
		}

		if (template.getAccessibility() != null) {
			if (template.getAccessibility().equalsIgnoreCase("Public")) {
				fc.utobj().clickElement(driver, ui.public_Accessibility);
			} else {
				fc.utobj().clickElement(driver, ui.private_Accessibility);
			}
		}

		if (template.getEmailType() != null) {
			if (template.getEmailType().equalsIgnoreCase("Graphical")) {
				fc.utobj().clickElement(driver, ui.graphicalTemplate);
			} else if (template.getEmailType().contains("Plain")) {
				fc.utobj().clickElement(driver, ui.graphicalTemplate);
			} else if (template.getEmailType().toLowerCase().contains("html")
					|| template.getEmailType().toLowerCase().contains("zip")) {
				fc.utobj().clickElement(driver, ui.uploadHTML_ZIPTemplate);
			}
		}

		if (template.getFolderName() != null) {
			fc.utobj().sendKeys(driver, ui.mailSubject, template.getFolderName());
		}

		if (template.getBody() != null) {
			fc.utobj().switchFrameById(driver, "ta_ifr");
			fc.utobj().sendKeys(driver, ui.emailBody, template.getBody());
			fc.utobj().switchFrameToDefault(driver);
		}

		if (template.getSetAsEmailType() != null) {
			fc.utobj().selectDropDown(driver, ui.SetAsEmailType_Select, template.getSetAsEmailType());
		}
	}

}
