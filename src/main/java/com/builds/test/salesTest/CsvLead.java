package com.builds.test.salesTest;

class CsvLead {

	private static final String COMMA_DELIMITER = ",";
	
	String getCSVLeadInfo(Lead lead) {
		
	String csvStr="";
		
		if(lead.getInquiryDate()!=null)
		{
			csvStr = csvStr+lead.getInquiryDate();
			csvStr = csvStr+COMMA_DELIMITER;
		}else
		{
			//csvStr = csvStr+"";
			csvStr = csvStr+COMMA_DELIMITER;
		}
		
		if(lead.getFirstName()!=null)
		{
			csvStr = csvStr+lead.getFirstName();
			csvStr = csvStr+COMMA_DELIMITER;
		}
		else 
		{
			csvStr = csvStr+COMMA_DELIMITER;
		}
		
		if(lead.getLastName()!=null)
		{
			csvStr = csvStr+lead.getLastName();
			csvStr = csvStr+COMMA_DELIMITER;
		}else
		{
			//csvStr = csvStr+"";
			csvStr = csvStr+COMMA_DELIMITER;
		}
		
		if(lead.getEmail()!=null)
		{
			csvStr = csvStr+lead.getEmail();
			csvStr = csvStr+COMMA_DELIMITER;
		}else
		{
			//csvStr = csvStr+"";
			csvStr = csvStr+COMMA_DELIMITER;
		}
		
		if(lead.getCompanyName()!=null)
		{
			csvStr = csvStr+lead.getCompanyName();
			csvStr = csvStr+COMMA_DELIMITER;
		}else
		{
			//csvStr = csvStr+"";
			csvStr = csvStr+COMMA_DELIMITER;
		}
		
		if(lead.getAddress1()!=null)
		{
			csvStr = csvStr+lead.getAddress1();
			csvStr = csvStr+COMMA_DELIMITER;
		}else
		{
			//csvStr = csvStr+"";
			csvStr = csvStr+COMMA_DELIMITER;
		}
		
		if(lead.getAddress2()!=null)
		{
			csvStr = csvStr+lead.getAddress2();
			csvStr = csvStr+COMMA_DELIMITER;
		}else
		{
			//csvStr = csvStr+"";
			csvStr = csvStr+COMMA_DELIMITER;
		}
		
		if(lead.getCity()!=null)
		{
			csvStr = csvStr+lead.getCity();
			csvStr = csvStr+COMMA_DELIMITER;
		}else
		{
			//csvStr = csvStr+"";
			csvStr = csvStr+COMMA_DELIMITER;
		}
		
		if(lead.getStateProvince()!=null)
		{
			csvStr = csvStr+lead.getStateProvince();
			csvStr = csvStr+COMMA_DELIMITER;
		}else
		{
			//csvStr = csvStr+"";
			csvStr = csvStr+COMMA_DELIMITER;
		}
		
		if(lead.getCountry()!=null)
		{
			csvStr = csvStr+lead.getCountry();
			csvStr = csvStr+COMMA_DELIMITER;
		}else
		{
			//csvStr = csvStr+"";
			csvStr = csvStr+COMMA_DELIMITER;
		}
		
		if(lead.getZipPostalCode()!=null)
		{
			csvStr = csvStr+lead.getZipPostalCode();
			csvStr = csvStr+COMMA_DELIMITER;
		}else
		{
			//csvStr = csvStr+"";
			csvStr = csvStr+COMMA_DELIMITER;
		}
		
		if(lead.getWorkPhone()!=null)
		{
			csvStr = csvStr+lead.getWorkPhone();
			csvStr = csvStr+COMMA_DELIMITER;
		}else
		{
			//csvStr = csvStr+"";
			csvStr = csvStr+COMMA_DELIMITER;
		}
		
		if(lead.getWorkPhoneExtension()!=null)
		{
			csvStr = csvStr+lead.getWorkPhoneExtension();
			csvStr = csvStr+COMMA_DELIMITER;
		}else
		{
			//csvStr = csvStr+"";
			csvStr = csvStr+COMMA_DELIMITER;
		}
		
		if(lead.getHomePhone()!=null)
		{
			csvStr = csvStr+lead.getHomePhone();
			csvStr = csvStr+COMMA_DELIMITER;
		}else
		{
			//csvStr = csvStr+"";
			csvStr = csvStr+COMMA_DELIMITER;
		}
		
		if(lead.getHomePhoneExtension()!=null)
		{
			csvStr = csvStr+lead.getHomePhoneExtension();
			csvStr = csvStr+COMMA_DELIMITER;
		}else
		{
			//csvStr = csvStr+"";
			csvStr = csvStr+COMMA_DELIMITER;
		}
		
		if(lead.getFax()!=null)
		{
			csvStr = csvStr+lead.getFax();
			csvStr = csvStr+COMMA_DELIMITER;
		}else
		{
			//csvStr = csvStr+"";
			csvStr = csvStr+COMMA_DELIMITER;
		}
		
		if(lead.getComment()!=null)
		{
			csvStr = csvStr+lead.getComment();
			csvStr = csvStr+COMMA_DELIMITER;
		}else
		{
			//csvStr = csvStr+"";
			csvStr = csvStr+COMMA_DELIMITER;
		}
		
		if(lead.getMobile()!=null)
		{
			csvStr = csvStr+lead.getMobile();
			csvStr = csvStr+COMMA_DELIMITER;
		}else
		{
			//csvStr = csvStr+"";
			csvStr = csvStr+COMMA_DELIMITER;
		}
		
		if(lead.getBestTimeToContact()!=null)
		{
			csvStr = csvStr+lead.getBestTimeToContact();
			csvStr = csvStr+COMMA_DELIMITER;
		}else
		{
			//csvStr = csvStr+"";
			csvStr = csvStr+COMMA_DELIMITER;
		}
		
		if(lead.getLeadStatus()!=null)
		{
			csvStr = csvStr+lead.getLeadStatus();
			csvStr = csvStr+COMMA_DELIMITER;
		}else
		{
			//csvStr = csvStr+"";
			csvStr = csvStr+COMMA_DELIMITER;
		}
		
		if(lead.getLeadSourceCategory()!=null)
		{
			csvStr = csvStr+lead.getLeadSourceCategory();
			csvStr = csvStr+COMMA_DELIMITER;
		}else
		{
			//csvStr = csvStr+"";
			csvStr = csvStr+COMMA_DELIMITER;
		}
		
		if(lead.getLeadSourceDetails()!=null)
		{
			csvStr = csvStr+lead.getLeadSourceDetails();
			csvStr = csvStr+COMMA_DELIMITER;
		}else
		{
			//csvStr = csvStr+"";
			csvStr = csvStr+COMMA_DELIMITER;
		}
		
		if(lead.getBasedonAssignmentRules()!=null)
		{
			csvStr = csvStr+lead.getBasedonAssignmentRules();
			csvStr = csvStr+COMMA_DELIMITER;
		}else
		{
			//csvStr = csvStr+"";
			csvStr = csvStr+COMMA_DELIMITER;
		}
		
		if(lead.getPreferredModeofContact()!=null)
		{
			csvStr = csvStr+lead.getPreferredModeofContact();
			csvStr = csvStr+COMMA_DELIMITER;
		}else
		{
			//csvStr = csvStr+"";
			csvStr = csvStr+COMMA_DELIMITER;
		}
		
		if(lead.getCurrentNetWorth()!=null)
		{
			csvStr = csvStr+lead.getCurrentNetWorth();
			csvStr = csvStr+COMMA_DELIMITER;
		}else
		{
			//csvStr = csvStr+"";
			csvStr = csvStr+COMMA_DELIMITER;
		}
		
		if(lead.getCashAvailableforInvestment()!=null)
		{
			csvStr = csvStr+lead.getCashAvailableforInvestment();
			csvStr = csvStr+COMMA_DELIMITER;
		}else
		{
			//csvStr = csvStr+"";
			csvStr = csvStr+COMMA_DELIMITER;
		}
		
		if(lead.getInvestmentTimeframe()!=null)
		{
			csvStr = csvStr+lead.getInvestmentTimeframe();
			csvStr = csvStr+COMMA_DELIMITER;
		}else
		{
			//csvStr = csvStr+"";
			csvStr = csvStr+COMMA_DELIMITER;
		}
		
		if(lead.getSourceOfInvestment()!=null)
		{
			csvStr = csvStr+lead.getSourceOfInvestment();
			csvStr = csvStr+COMMA_DELIMITER;
		}else
		{
			//csvStr = csvStr+"";
			csvStr = csvStr+COMMA_DELIMITER;
		}
		
		if(lead.getBackground()!=null)
		{
			csvStr = csvStr+lead.getBackground();
			csvStr = csvStr+COMMA_DELIMITER;
		}else
		{
			//csvStr = csvStr+"";
			csvStr = csvStr+COMMA_DELIMITER;
		}
		
		if(lead.getDivision()!=null)
		{
			csvStr = csvStr+lead.getDivision();
			csvStr = csvStr+COMMA_DELIMITER;
		}else
		{
			//csvStr = csvStr+"";
			csvStr = csvStr+COMMA_DELIMITER;
		}
			
				
		return csvStr;
	
	}
}
