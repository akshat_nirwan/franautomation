package com.builds.test.salesTest;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.fs.DetailedHistoryUI;
import com.builds.utilities.FranconnectUtil;

class DetailedHistoryTest {
	private WebDriver driver;
	private FranconnectUtil fc = new FranconnectUtil();

	DetailedHistoryTest(WebDriver driver) {
		this.driver = driver;
	}

	public void clickStatusChangeHistory_DetailedHistory() throws Exception {

		PrimaryInfoUI ui = new PrimaryInfoUI(driver);
		fc.utobj().printTestStep("Click Status Change History");
		fc.utobj().clickElement(driver, ui.StatusChangeHistory_link);
	}

	public void verify_RemarksAndCallHistory_In_DetailedHistory(String subject) throws Exception {
		DetailedHistoryUI ui = new DetailedHistoryUI(driver);
		fc.utobj().printTestStep("Click Remarks and Call History");
		fc.utobj().clickElement(driver, ui.RemarksandCallHistory_link);
		fc.utobj().printTestStep("Verify that Activity is available in Remarks and Call History");
		verifyTextInDetailedHistoryWindow(subject);
		fc.utobj().switchFrameToDefault(driver);
		new Sales(driver).commonMethods().closeColorBox(driver);
	}

	public void verify_ActivityHistory_In_DetailedHistory(String subject) throws Exception {
		DetailedHistoryUI ui = new DetailedHistoryUI(driver);
		fc.utobj().printTestStep("Click Activity History");
		fc.utobj().sleep(2500);
		fc.utobj().clickElement(driver, ui.ActivityHistory_link);
		fc.utobj().printTestStep("Verify that Activity is available in Activity History");
		verifyTextInDetailedHistoryWindow(subject);
		fc.utobj().switchFrameToDefault(driver);
		new Sales(driver).commonMethods().closeColorBox(driver);
	}

	public void verifyTextInDetailedHistoryWindow(String text) throws Exception {

		if (fc.utobj().assertPageSource(driver, text) == false) {
			fc.utobj().throwsException("Remarks history not found.");
		}
	}

	public void verify_StatusChangeHistory_In_DetailedHistory(Status status) throws Exception {
		DetailedHistoryUI ui = new DetailedHistoryUI(driver);
		fc.utobj().printTestStep("Click Status Change History");
		fc.utobj().clickElement(driver, ui.StatusChangeHistory_link);
		fc.utobj().printTestStep("Verify that Activity is available in Status Change History");
		verifyTextInDetailedHistoryWindow(status.getLeadStatus());
		new Sales(driver).commonMethods().closeColorBox(driver);
		fc.utobj().switchFrameToDefault(driver);
	}
	
	public void clickTasks_DetailedHistory() throws Exception
	{
		DetailedHistoryUI ui = new DetailedHistoryUI(driver);
		fc.utobj().printTestStep("Click Tasks");
		fc.utobj().clickElement(driver, ui.Tasks_link);
	}

}
