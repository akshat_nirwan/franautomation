package com.builds.test.salesTest;

public class Email {

	private String FromReply;
	private String ExistingEmailTemplates;
	private String To;
	private String CC;
	private String BCC;
	private String Subject;
	private String EmailFooter;
	private String CcandBccshouldreceivesingleEmail;
	private String SendEmailtoCoApplicants;
	private String Signature;
	private String body;

	public String getFromReply() {
		return FromReply;
	}

	public void setFromReply(String fromReply) { // Fill with these values only
													// Lead Owner ID | Logged
													// User ID | Or fill the
													// Custom Email
		FromReply = fromReply;
	}

	public String getExistingEmailTemplates() {
		return ExistingEmailTemplates;
	}

	public void setExistingEmailTemplates(String existingEmailTemplates) {
		ExistingEmailTemplates = existingEmailTemplates;
	}

	public String getTo() {
		return To;
	}

	public void setTo(String to) {
		To = to;
	}

	public String getCC() {
		return CC;
	}

	public void setCC(String cC) {
		CC = cC;
	}

	public String getBCC() {
		return BCC;
	}

	public void setBCC(String bCC) {
		BCC = bCC;
	}

	public String getSubject() {
		return Subject;
	}

	public void setSubject(String subject) {
		Subject = subject;
	}

	public String getEmailFooter() {
		return EmailFooter;
	}

	public void setEmailFooter(String emailFooter) {
		EmailFooter = emailFooter;
	}

	public String getCcandBccshouldreceivesingleEmail() {
		return CcandBccshouldreceivesingleEmail;
	}

	public void setCcandBccshouldreceivesingleEmail(String ccandBccshouldreceivesingleEmail) {
		CcandBccshouldreceivesingleEmail = ccandBccshouldreceivesingleEmail;
	}

	public String getSendEmailtoCoApplicants() {
		return SendEmailtoCoApplicants;
	}

	public void setSendEmailtoCoApplicants(String sendEmailtoCoApplicants) {
		SendEmailtoCoApplicants = sendEmailtoCoApplicants;
	}

	public String getSignature() {
		return Signature;
	}

	public void setSignature(String signature) {
		Signature = signature;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

}
