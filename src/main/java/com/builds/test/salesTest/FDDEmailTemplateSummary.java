package com.builds.test.salesTest;

public class FDDEmailTemplateSummary {

	private String title;
	private String subject;
	private String setAsDefaultTemplate;
	private String emailType;
	private String emailBody;
	private String attachment;
	private String keywordsForReplacement;
	private String htmlZipFileAttachment;

	public String getHtmlZipFileAttachment() {
		return htmlZipFileAttachment;
	}

	public void setHtmlZipFileAttachment(String htmlZipFileAttachment) {
		this.htmlZipFileAttachment = htmlZipFileAttachment;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getSetAsDefaultTemplate() {
		return setAsDefaultTemplate;
	}

	public void setSetAsDefaultTemplate(String setAsDefaultTemplate) {
		this.setAsDefaultTemplate = setAsDefaultTemplate;
	}

	public String getEmailType() {
		return emailType;
	}

	public void setEmailType(String emailType) {
		this.emailType = emailType;
	}

	public String getEmailBody() {
		return emailBody;
	}

	public void setEmailBody(String emailBody) {
		this.emailBody = emailBody;
	}

	public String getAttachment() {
		return attachment;
	}

	public void setAttachment(String attachment) {
		this.attachment = attachment;
	}

	public String getKeywordsForReplacement() {
		return keywordsForReplacement;
	}

	public void setKeywordsForReplacement(String keywordsForReplacement) {
		this.keywordsForReplacement = keywordsForReplacement;
	}

}
