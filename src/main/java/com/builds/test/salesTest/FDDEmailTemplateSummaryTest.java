package com.builds.test.salesTest;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.fs.FDDEmailTemplateSummaryUI;
import com.builds.utilities.FranconnectUtil;

class FDDEmailTemplateSummaryTest {

	private FranconnectUtil fc = new FranconnectUtil();
	private WebDriver driver;

	FDDEmailTemplateSummaryTest addTemplate(WebDriver driver, FDDEmailTemplateSummary fddEmailTemplateSummary)
			throws Exception {
		FDDEmailTemplateSummaryUI fddEmailTemplateSummaryUI = new FDDEmailTemplateSummaryUI(driver);
		fc.utobj().printTestStep("Add template in FDD Email Template Summary");
		fc.utobj().clickElement(driver, fddEmailTemplateSummaryUI.addTemplateBtn);
		fill_emailTemplate(fddEmailTemplateSummary);
		fc.utobj().clickElement(driver, fddEmailTemplateSummaryUI.addBtn);
		
		if(fc.utobj().isAlertPresent(driver))
		{
			fc.utobj().acceptAlertBox(driver);
			fc.utobj().clickElement(driver, fddEmailTemplateSummaryUI.addBtn);
			
		}
		
		
		
		return new FDDEmailTemplateSummaryTest(driver);
	}

	void verify_addTemplate(FDDEmailTemplateSummary fddEmailTemplateSummary) throws Exception {
		FDDEmailTemplateSummaryUI fddEmailTemplateSummaryUI = new FDDEmailTemplateSummaryUI(driver);
		fc.utobj().printTestStep("Verify added template presence on FDD Email Template Summary page");
		if(fc.utobj().isElementPresent(driver, fddEmailTemplateSummaryUI.showAllLink))
			{fc.utobj().clickElement(driver, fddEmailTemplateSummaryUI.showAllLink);
			fc.utobj().printTestStep("Added template successfully found on FDD Email Template Summary template page");}
		if ( ! fc.utobj().assertPageSource(driver, fddEmailTemplateSummary.getTitle())) {
			fc.utobj().throwsException("Added template not found on FDD Email Template Summary template page");
		}

	}

	void fill_emailTemplate(FDDEmailTemplateSummary fddEmailTemplateSummary) throws Exception {
		fc.utobj().printTestStep("Entering data in FDD Email Template Summary - Add Template");
		FDDEmailTemplateSummaryUI fddEmailTemplateSummaryUI = new FDDEmailTemplateSummaryUI(driver);

		if (fddEmailTemplateSummary.getEmailType() != null) {
			if (fddEmailTemplateSummary.getEmailType().equalsIgnoreCase("graphical")) {
				fc.utobj().clickElement(driver, fddEmailTemplateSummaryUI.emailType_Graphical);

				if (fddEmailTemplateSummary.getKeywordsForReplacement() != null) {
					fc.utobj().switchFrameById(driver, fddEmailTemplateSummaryUI.iframeID);
					// code to input
				}

				if (fddEmailTemplateSummary.getAttachment() != null) {
					Sales_Common_New common = new Sales_Common_New();
					common.addAttachment(fddEmailTemplateSummary.getAttachment(), driver);
				}
			}
			if (fddEmailTemplateSummary.getEmailType().equalsIgnoreCase("plain text")) {
				fc.utobj().clickElement(driver, fddEmailTemplateSummaryUI.emailType_plainText);

				if (fddEmailTemplateSummary.getKeywordsForReplacement() != null) {
					fc.utobj().sendKeys(driver, fddEmailTemplateSummaryUI.plainTextBody,
							fddEmailTemplateSummary.getKeywordsForReplacement());
				}

				if (fddEmailTemplateSummary.getAttachment() != null) {
					Sales_Common_New common = new Sales_Common_New();
					common.addAttachment(fddEmailTemplateSummary.getAttachment(), driver);
				}
			}
			if (fddEmailTemplateSummary.getEmailType().toUpperCase().contains("UPLOAD")) {
				fc.utobj().clickElement(driver, fddEmailTemplateSummaryUI.emailType_uploadHtmlZipFile);
				fc.utobj().sendKeys(driver, fddEmailTemplateSummaryUI.htmlZipFileAttachment,
						fddEmailTemplateSummary.getHtmlZipFileAttachment());
			}
		}

		if (fddEmailTemplateSummary.getTitle() != null) {
			fc.utobj().sendKeys(driver, fddEmailTemplateSummaryUI.title, fddEmailTemplateSummary.getTitle());
		}

		if (fddEmailTemplateSummary.getSubject() != null) {
			fc.utobj().sendKeys(driver, fddEmailTemplateSummaryUI.subject, fddEmailTemplateSummary.getSubject());
		}

		if (fddEmailTemplateSummary.getSetAsDefaultTemplate() != null) {
			if (fddEmailTemplateSummary.getSetAsDefaultTemplate().equalsIgnoreCase("Yes")) {
				fc.utobj().clickElement(driver, fddEmailTemplateSummaryUI.setAsDefaultTemplate_Checkbox);
			}
		}

	}

	FDDEmailTemplateSummaryTest(WebDriver driver) {
		this.driver = driver;
	}

}
