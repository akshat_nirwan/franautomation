package com.builds.test.salesTest;

import java.util.ArrayList;
import java.util.List;

public class FDDManagement {

	private String fddName;
	private String version;
	private String dateOfExpiry;
	private String item23Receipt_sales;
	private List<String> countries = new ArrayList<String>();
	private String item23Receipt_infoMgr;
	private String division;
	private List<String> associateWith = new ArrayList<String>();
	// private String associateWith;
	private String uploadFile;
	private String comments;

	public String getFddName() {
		return fddName;
	}

	public void setFddName(String fddName) {
		this.fddName = fddName;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getDateOfExpiry() {
		return dateOfExpiry;
	}

	public void setDateOfExpiry(String dateOfExpiry) {
		this.dateOfExpiry = dateOfExpiry;
	}

	public String getItem23Receipt_sales() {
		return item23Receipt_sales;
	}

	public void setItem23Receipt_sales(String item23Receipt_sales) {
		this.item23Receipt_sales = item23Receipt_sales;
	}

	public List<String> getCountries() {
		return countries;
	}

	public void setCountries(List<String> countries) {
		this.countries = countries;
	}

	/*
	 * public String getCountries() { return countries; } public void
	 * setCountries(String countries) { this.countries = countries; }
	 */
	public String getItem23Receipt_infoMgr() {
		return item23Receipt_infoMgr;
	}

	public void setItem23Receipt_infoMgr(String item23Receipt_infoMgr) {
		this.item23Receipt_infoMgr = item23Receipt_infoMgr;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public List<String> getAssociateWith() {
		return associateWith;
	}

	public void setAssociateWith(List<String> associateWith) {
		this.associateWith = associateWith;
	}

	/*
	 * public String getAssociateWith() { return associateWith; } public void
	 * setAssociateWith(String associateWith) { this.associateWith =
	 * associateWith; }
	 */
	public String getUploadFile() {
		return uploadFile;
	}

	public void setUploadFile(String uploadFile) {
		this.uploadFile = uploadFile;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

}
