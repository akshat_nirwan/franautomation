package com.builds.test.salesTest;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.fs.FDDManagementUI;
import com.builds.utilities.FranconnectUtil;

public class FDDManagementTest {

	private FranconnectUtil fc = new FranconnectUtil();
	private WebDriver driver;

	FDDManagementTest(WebDriver driver) {
		this.driver = driver;
	}

	FDDManagementTest uploadFDD(FDDManagement fddManagement) throws Exception {
		FDDManagementUI fddManagementUI = new FDDManagementUI(driver);
		fc.utobj().clickElement(driver, fddManagementUI.uploadFDDBtn);
		fill_uploadFDD(fddManagement);
		fc.utobj().clickElement(driver, fddManagementUI.addFDDBtn);
		return new FDDManagementTest(driver);
	}

	void verify_uploadFDD(FDDManagement fddManagement) throws Exception {
		fc.utobj().printTestStep("Verify Uploaded FDD on FDD Management page");
		if (!fc.utobj().assertLinkText(driver, fddManagement.getFddName())) {
			fc.utobj().throwsException("Uploaded FDD not visible on FDD Management page");
		} fc.utobj().printTestStep("FDD successfully uploaded : "+fddManagement.getFddName()); System.out.println("FDD successfully uploaded : "+fddManagement.getFddName());
	}

	private void fill_uploadFDD(FDDManagement fddManagement) throws Exception {
		FDDManagementUI fddManagementUI = new FDDManagementUI(driver);

		if (fddManagement.getFddName() != null) {
			fc.utobj().sendKeys(driver, fddManagementUI.fddName, fddManagement.getFddName());
		}

		if (fddManagement.getVersion() != null) {
			fc.utobj().sendKeys(driver, fddManagementUI.version, fddManagement.getVersion());
		}

		if (fddManagement.getDateOfExpiry() != null) {
			fc.utobj().sendKeys(driver, fddManagementUI.dateOfExpiry, fddManagement.getDateOfExpiry());
		}

		if (fddManagement.getItem23Receipt_sales() != null) {
			fc.utobj().selectDropDown(driver, fddManagementUI.item23ReceiptSales,
					fddManagement.getItem23Receipt_sales());
		}

		if (fddManagement.getCountries() != null) {
			String byId_AttributeValue = "ufocAction_countryID";
			fc.utobj().selectMultipleFromMultiList(driver, byId_AttributeValue, fddManagement.getCountries());
		}

		if (fddManagement.getItem23Receipt_infoMgr() != null) {
			fc.utobj().selectDropDown(driver, fddManagementUI.item23ReceiptFim,
					fddManagement.getItem23Receipt_infoMgr());
		}

		if (fddManagement.getDivision() != null) {
			fc.utobj().selectDropDown(driver, fddManagementUI.division, fddManagement.getDivision());
		}

		if (fddManagement.getAssociateWith() != null) {
			for (String element : fddManagement.getAssociateWith()) {
				fc.utobj().clickElement(driver, fddManagementUI.associateWith(element, driver));
				if (fc.utobj().isAlertPresent(driver)) {
					fc.utobj().acceptAlertBox(driver);
				}
			}

		}

		if (fddManagement.getUploadFile() != null) {
			fc.utobj().sendKeys(driver, fddManagementUI.uploadFile, fddManagement.getUploadFile());
		}

		if (fddManagement.getComments() != null) {
			fc.utobj().sendKeys(driver, fddManagementUI.comments, fddManagement.getComments());
		}

	}

}
