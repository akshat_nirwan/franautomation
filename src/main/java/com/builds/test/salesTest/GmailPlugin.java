package com.builds.test.salesTest;

public class GmailPlugin {
	
	// Gmail 
	private String gmailURL;
	private String emailID;
	private String password;
	
	public String getGmailURL() {
		return gmailURL;
	}
	public void setGmailURL(String gmailURL) {
		this.gmailURL = gmailURL;
	}
	public String getEmailID() {
		return emailID;
	}
	public void setEmailID(String emailID) {
		this.emailID = emailID;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	// Gmail Plugin
	private String loginID_plugin;
	private String password_plugin;
	private String companyCode_plugin;
	
	public String getLoginID_plugin() {
		return loginID_plugin;
	}
	public void setLoginID_plugin(String loginID_plugin) {
		this.loginID_plugin = loginID_plugin;
	}
	public String getPassword_plugin() {
		return password_plugin;
	}
	public void setPassword_plugin(String password_plugin) {
		this.password_plugin = password_plugin;
	}
	public String getCompanyCode_plugin() {
		return companyCode_plugin;
	}
	public void setCompanyCode_plugin(String companyCode_plugin) {
		this.companyCode_plugin = companyCode_plugin;
	}

	

}
