package com.builds.test.salesTest;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.fs.GmailPluginUI;
import com.builds.utilities.FranconnectUtil;

public class GmailPluginTest {
	
	WebDriver driver;
	FranconnectUtil fc = new FranconnectUtil();
	
	public GmailPluginTest(WebDriver driver) {
		this.driver = driver;
	}
	
	void addLeadThroughGmailPlugin(Lead lead) throws Exception
	{
		GmailPluginUI ui = new GmailPluginUI(driver);
		fc.utobj().sleep();
		fc.utobj().clickElement(driver, ui.newBtn);
		fc.utobj().clickElement(driver, ui.salesLead);
		fc.utobj().sleep();

		fill_addLeadThroughGmailPlugin(lead);
		fc.utobj().clickElement(driver, ui.saveBtn_plugin);
		
	}
	
	void fill_addLeadThroughGmailPlugin(Lead lead) throws Exception
	{
		GmailPluginUI ui = new GmailPluginUI(driver);
		
		if(lead.getLeadSourceCategory() != null)
		{
			fc.utobj().selectDropDown(driver, ui.sourceCategory_dropdown, lead.getLeadSourceCategory());
		}
		
		if(lead.getLeadSourceDetails() != null)
		{
			fc.utobj().selectDropDown(driver, ui.sourceDetail_dropdown, lead.getLeadSourceDetails());
		}
		
		if(lead.getFirstName() != null)
		{
			fc.utobj().sendKeys(driver, ui.firstName, lead.getFirstName());
		}
		
		if(lead.getLastName() != null)
		{
			fc.utobj().sendKeys(driver, ui.lastName, lead.getLastName());
		}
		
		if(lead.getEmail() != null)
		{
			fc.utobj().sendKeys(driver, ui.emailID, lead.getEmail());
		}
		
		if(lead.getAddress1() != null)
		{
			fc.utobj().sendKeys(driver, ui.addressLine1, lead.getAddress1());
		}
		
		if(lead.getCity() != null)
		{
			fc.utobj().sendKeys(driver, ui.city, lead.getCity());
		}
		
		if(lead.getCountry() != null)
		{
			fc.utobj().selectDropDown(driver, ui.country, lead.getCountry());
		}
		
		if(lead.getStateProvince() != null)
		{
			fc.utobj().selectDropDown(driver, ui.state, lead.getStateProvince());
		}
		
		if(lead.getZipPostalCode() != null)
		{
			fc.utobj().selectDropDown(driver, ui.zipCode, lead.getZipPostalCode());
		}
		
		
		
	}
	
	GmailPluginTest loginToGmail(GmailPlugin gmailPlugin) throws Exception
	{
		GmailPluginUI ui = new GmailPluginUI(driver);

		if(gmailPlugin.getEmailID() != null)
		{
			fc.utobj().sendKeys(driver, ui.userName_textBox, gmailPlugin.getEmailID());
		}
		
		fc.utobj().clickElement(driver, ui.nextBtn);
		
		if(gmailPlugin.getPassword() != null)
		{
			fc.utobj().sendKeys(driver, ui.password_textBox, gmailPlugin.getPassword());
		}
		
		fc.utobj().clickElement(driver, ui.nextBtn);
		
		return new GmailPluginTest(driver);
		
	}
	
	GmailPluginTest loginToGmailPlugin(GmailPlugin gmailPlugin) throws Exception
	{
		GmailPluginUI ui = new GmailPluginUI(driver);

		fc.utobj().clickElement(driver, ui.continueBtn);
		
		if(gmailPlugin.getLoginID_plugin() != null)
		{
			fc.utobj().sendKeys(driver, ui.pluginLoginID_textBox, gmailPlugin.getLoginID_plugin());
		}
		
		if(gmailPlugin.getPassword_plugin() != null)
		{
			fc.utobj().sendKeys(driver, ui.pluginPassword_textBox, gmailPlugin.getPassword_plugin());
		}
		
		if(gmailPlugin.getCompanyCode_plugin() != null)
		{
			fc.utobj().sendKeys(driver, ui.companyCode_textBox, gmailPlugin.getCompanyCode_plugin());
		}
		
		fc.utobj().clickElement(driver, ui.plugin_loginBtn);
		
		return new GmailPluginTest(driver);
		
	}
}
