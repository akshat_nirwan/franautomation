package com.builds.test.salesTest;

public class Group {

	private String name;
	private String description;
	private String visibility;
	private String groupType;
	private String creationDate;
	private String createdBy;
	private String currentEmailCampaign;

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCurrentEmailCampaign() {
		return currentEmailCampaign;
	}

	public void setCurrentEmailCampaign(String currentEmailCampaign) {
		this.currentEmailCampaign = currentEmailCampaign;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getVisibility() {
		return visibility;
	}

	public void setVisibility(String visibility) {
		this.visibility = visibility;
	}

	public String getGroupType() {
		return groupType;
	}

	public void setGroupType(String smartGroup) {
		this.groupType = smartGroup;
	}

}
