package com.builds.test.salesTest;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.builds.uimaps.fs.GroupsUI;
import com.builds.utilities.FranconnectUtil;

class GroupTest {

	void clickCreateGroupBtnAndSwitchToFrame(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		GroupsUI gui = new GroupsUI(driver);
		fc.utobj().printTestStep("Click Create Group Button");
		fc.utobj().clickElement(driver, gui.createGroupBtn);
		fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
	}

	void clickSaveBtn(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		GroupsUI gui = new GroupsUI(driver);
		fc.utobj().printTestStep("Click Save Button");
		fc.utobj().clickElement(driver, gui.Save_Button_ByValue);
	}

	void clickCancelBtn(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		GroupsUI gui = new GroupsUI(driver);
		fc.utobj().printTestStep("Click Cancel Button");
		fc.utobj().clickElement(driver, gui.Cancel_Button_ByValue);
	}

	void acceptAlertAfterSavingFilters(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		fc.utobj().printTestStep("Accept Confirmation for filters");
		fc.utobj().acceptAlertBox(driver);
	}

	void addGroupDetails_AndCreate(WebDriver driver, Group group) throws Exception {
		clickCreateGroupBtnAndSwitchToFrame(driver);
		fillGroupDetails(driver, group);
		clickCreateBtn(driver);
	}
	
	void verify_addedGroup(WebDriver driver ,String groupName) throws Exception
	{
		FranconnectUtil fc = new FranconnectUtil();
		fc.utobj().printTestStep("Verifying added group");
		fc.utobj().assertLinkText(driver, groupName);
	}

	void fillGroupDetails(WebDriver driver, Group group) throws Exception {

		GroupsUI gui = new GroupsUI(driver);
		FranconnectUtil fc = new FranconnectUtil();
		fc.utobj().printTestStep("Fill Group Details");

		if (group.getName() != null) {
			fc.utobj().printTestStep("Enter Group Name");
			fc.utobj().sendKeys(driver, gui.groupName, group.getName());
		}
		if (group.getDescription() != null) {
			fc.utobj().printTestStep("Enter Group Description");
			fc.utobj().sendKeys(driver, gui.groupDescription, group.getDescription());
		}
		if (group.getVisibility() != null) {
			fc.utobj().printTestStep("Select Visibility : " + group.getVisibility());
			fc.utobj().selectDropDown(driver, gui.visibility, group.getVisibility());
		}
		if (group.getGroupType() != null) {
			boolean isSmartGroup = fc.utobj().isSelected(driver, gui.groupChoice_Input_ById);
			if (isSmartGroup == false && group.getGroupType().equalsIgnoreCase("Smart")) {
				fc.utobj().printTestStep("Select Group Type : Smart");
				gui.smartGroup.click();
			}
			if (isSmartGroup == true && group.getGroupType().equalsIgnoreCase("Regular")) {
				fc.utobj().printTestStep("Select Group Type : Regular");
				gui.smartGroup.click();
			}
		}
	}

	void openSearchFilter(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		try {
			GroupsUI gui = new GroupsUI(driver);
			if (gui.Search_Button_ByID.isDisplayed() == false) {
				fc.utobj().printTestStep("Open Search Filter");
				fc.utobj().clickElement(driver, gui.ico_Filter_XPATH_ByDataRole);
			}
		} catch (Exception e) {
			fc.utobj().throwsException("Not able to open the search Filter");
		}
	}

	void closeSearchFilter(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		try {
			GroupsUI gui = new GroupsUI(driver);
			if (gui.Search_Button_ByID.isDisplayed() == true) {
				fc.utobj().printTestStep("Close Search Filter");
				fc.utobj().clickElement(driver, gui.ico_Filter_XPATH_ByDataRole);
			}
		} catch (Exception e) {
			fc.utobj().throwsException("Not able to close the search Filter");
		}
	}

	void salesGroupsSearch(WebDriver driver, Group group) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		GroupsUI gui = new GroupsUI(driver);

		openSearchFilter(driver);

		if (group.getName() != null) {
			fc.utobj().printTestStep("Enter Group Name in Search Box");
			fc.utobj().sendKeys(driver, gui.searchTemplate_Input_ByID, group.getName());
		}

		if (group.getCreationDate() != null) {
			//
		}

		if (group.getVisibility() != null) {
			fc.utobj().selectRadioValueInDropDown(driver, gui.dropdown_Div_ById, gui.textField_dropdown_Div_ById,
					group.getVisibility());
		}

		clickSearch(driver);

	}

	void clickCreateBtn(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		GroupsUI gui = new GroupsUI(driver);
		fc.utobj().printTestStep("Click Create Button on Add Group page");
		fc.utobj().clickElement(driver, gui.Create_Button_ByXPATH);
	}

	void clickSearch(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		GroupsUI gui = new GroupsUI(driver);
		fc.utobj().printTestStep("Click Search");
		fc.utobj().clickElement(driver, gui.Search_Button_ByID);

	}

	void modifyRegularGroup(WebDriver driver, Group group) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		GroupsUI gui = new GroupsUI(driver);

		fc.utobj().printTestStep("Modify Group : " + group.getName());
		click_Ico_ThreeDots_ForGroup(driver, group);
		fc.utobj().clickElement(driver, driver.findElement(By.xpath(gui.selectMenuFromRightActionDiv("Modify"))));
		fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);

	}

	private void click_Ico_ThreeDots_ForGroup(WebDriver driver, Group group) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		fc.utobj().printTestStep("Click on Top Right Action");
		GroupsUI gui = new GroupsUI(driver);
		fc.utobj().clickElement(driver, driver.findElement(By.xpath(gui.clickGroup_ico_ThreeDots(group.getName()))));
	}

	void clickAssociateWithLead(WebDriver driver, Group group) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		GroupsUI gui = new GroupsUI(driver);

		fc.utobj().printTestStep("Associate Group with lead");
		click_Ico_ThreeDots_ForGroup(driver, group);
		fc.utobj().clickElement(driver,
				driver.findElement(By.xpath(gui.selectMenuFromRightActionDiv("Associate with Leads"))));
		fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);

	}

	void fillAndSearchLeadInfoInGroupFilterPage(WebDriver driver, Lead lead) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		GroupsUI gui = new GroupsUI(driver);

		fc.utobj().printTestStep("Fill the lead info to search");

		if (lead.getFirstName() != null) {
			fc.utobj().sendKeys(driver, gui.firstName, lead.getFirstName());
		}

		if (lead.getLastName() != null) {
			fc.utobj().sendKeys(driver, gui.lastName, lead.getLastName());
		}

		if (lead.getEmail() != null) {
			fc.utobj().sendKeys(driver, gui.emailID, lead.getEmail());
		}

		fc.utobj().clickElement(driver, gui.Search_Input_ByValue);

		fc.utobj().clickElement(driver, gui.yes_Input_ByValue);
		fc.utobj().clickElement(driver, gui.Close_Input_ByValue);

	}

	void clickLeadCountForGroup(WebDriver driver, Group group) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		GroupsUI gui = new GroupsUI(driver);

		String position = "5";

		fc.utobj().printTestStep("Click on the lead count");
		fc.utobj().clickElement(driver,
				driver.findElement(By.xpath(gui.getXPathOfLeadCount(group.getName(), position))));

	}

	int getRowCountOfGroup(WebDriver driver) {
		FranconnectUtil fc = new FranconnectUtil();
		GroupsUI gui = new GroupsUI(driver);

		List<WebElement> list = driver.findElements(By.xpath(gui.getRowXPathOfGroup()));

		if (list != null && !list.isEmpty()) {
			return list.size() - 1;
		} else {
			return 0;
		}
	}

	void setViewPerPage(WebDriver driver, String count) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		GroupsUI gui = new GroupsUI(driver);
		fc.utobj().printTestStep("Click View Per Page");
		fc.utobj().clickElement(driver, gui.clickViewPerPage);

		fc.utobj().clickElement(driver, driver.findElement(By.xpath(gui.getXPathOfViewPerPageCount("+count+"))));
	}

	void clickGroupName(WebDriver driver, Group group) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		GroupsUI gui = new GroupsUI(driver);

		fc.utobj().printTestStep("Click on group name");
		fc.utobj().clickLink(driver, group.getName());
		fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
	}

	Group validateGroupInfoInFrame(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		GroupsUI gui = new GroupsUI(driver);

		Group group = new Group();

		// List<WebElement> groupInfo =
		// driver.findElements(By.xpath(".//div[@class='grid-row']"));

		// closeGroupDetailsFrame(driver);

		return group;
	}

	void closeGroupDetailsFrame(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		GroupsUI gui = new GroupsUI(driver);

		fc.utobj().printTestStep("Close the Group Details frame.");
		fc.utobj().clickElement(driver, gui.Close_Button_ByValue);
	}

	void archiveGroup(WebDriver driver, Group group) throws Exception {

		FranconnectUtil fc = new FranconnectUtil();
		GroupsUI gui = new GroupsUI(driver);

		fc.utobj().printTestStep("Archive Group : " + group.getName());
		click_Ico_ThreeDots_ForGroup(driver, group);
		fc.utobj().clickElement(driver, driver.findElement(By.xpath(gui.selectMenuFromRightActionDiv("Archive"))));
		fc.utobj().acceptAlertBox(driver);
	}

	void archiveGroup_Batch_Top(WebDriver driver, Group group) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		GroupsUI gui = new GroupsUI(driver);

		fc.utobj().printTestStep("Click on Top Action for Batch Selection");
		fc.utobj().clickElement(driver, gui.top_Action_Batch_ByXpath);
		fc.utobj().clickElement(driver, driver.findElement(By.xpath(gui.selectMenuFromTopActionDiv("Archive"))));
		fc.utobj().acceptAlertBox(driver);
	}

	void archiveGroup_Batch_Bottom(WebDriver driver, Group group) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		GroupsUI gui = new GroupsUI(driver);

		fc.utobj().printTestStep("Archive Group through bottom button : " + group.getName());
		fc.utobj().clickElement(driver, gui.Archive_Button_ByValue);
		fc.utobj().acceptAlertBox(driver);
	}

	void deleteGroup(WebDriver driver, Group group) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		GroupsUI gui = new GroupsUI(driver);

		fc.utobj().printTestStep("Delete Group : " + group.getName());
		click_Ico_ThreeDots_ForGroup(driver, group);
		fc.utobj().clickElement(driver, driver.findElement(By.xpath(gui.selectMenuFromRightActionDiv("Delete"))));
		fc.utobj().acceptAlertBox(driver);
	}

	void deleteGroup_Batch_Top(WebDriver driver, Group group) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		GroupsUI gui = new GroupsUI(driver);

		fc.utobj().printTestStep("Click on Top Action for Batch Selection");
		fc.utobj().clickElement(driver, gui.top_Action_Batch_ByXpath);
		fc.utobj().clickElement(driver, driver.findElement(By.xpath(gui.selectMenuFromTopActionDiv("Delete"))));
		fc.utobj().acceptAlertBox(driver);

	}

	void deleteGroup_Batch_Bottom(WebDriver driver, Group group) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		GroupsUI gui = new GroupsUI(driver);

		fc.utobj().printTestStep("Delete Group through bottom button : " + group.getName());
		fc.utobj().clickElement(driver, gui.Delete_Button_ByValue);
		fc.utobj().acceptAlertBox(driver);

	}

	void goToArchiveGroupTab(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		GroupsUI gui = new GroupsUI(driver);

		fc.utobj().printTestStep("Go to Archived Groups");
		fc.utobj().clickElement(driver, gui.archivedGroupsTab);
	}

	void goToGroupTab(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		GroupsUI gui = new GroupsUI(driver);

		fc.utobj().printTestStep("Go to Groups");
		fc.utobj().clickElement(driver, gui.groupsTab);
	}

	void restoreGroup(WebDriver driver, Group group) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		GroupsUI gui = new GroupsUI(driver);

		fc.utobj().printTestStep("Restore Group : " + group.getName());
		click_Ico_ThreeDots_ForGroup(driver, group);
		fc.utobj().clickElement(driver, driver.findElement(By.xpath(gui.selectMenuFromRightActionDiv("Restore"))));
		fc.utobj().acceptAlertBox(driver);

	}

	void checkGroupName(WebDriver driver, Group group) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		GroupsUI gui = new GroupsUI(driver);

		fc.utobj().clickElement(driver, driver.findElement(By.xpath(gui.checkboxAgainstGroup(group.getName()))));
	}

	void clickModifyButton(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		GroupsUI gui = new GroupsUI(driver);

		fc.utobj().clickElement(driver, gui.modify_Button_ByValue);
	}

	void clickFiltersForAvailableFields(WebDriver driver, String SubModuleName, String fieldName)
			throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		GroupsUI gui = new GroupsUI(driver);

		fc.utobj().clickElement(driver, gui.AvailableFields_Button_ByClass);
		fc.utobj().clickElement(driver,
				driver.findElement(By.xpath(gui.clickFieldNameInFilter(SubModuleName, fieldName))));
		fc.utobj().clickElement(driver, gui.AvailableFields_Button_ByClass);

	}

	void selectFieldsValueInFilters(WebDriver driver, String SubModuleName, String fieldName, String fieldType,
			String position, String fieldValue) throws Exception {

		FranconnectUtil fc = new FranconnectUtil();
		GroupsUI gui = new GroupsUI(driver);

		if (fieldType.equalsIgnoreCase("select")) {
			fc.utobj().selectDropDownByVisibleText(driver,
					gui.getFieldXpathInFilters(SubModuleName + " > " + fieldName, fieldType, position), fieldValue);
		}

		if (fieldType.equalsIgnoreCase("input")) {
			fc.utobj().sendKeys(driver,
					driver.findElement(By
							.xpath(gui.getFieldXpathInFilters(SubModuleName + " > " + fieldName, fieldType, position))),
					fieldValue);
		}
	}

	void clickDeleteFilter(WebDriver driver, String SubModuleName, String fieldName) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		GroupsUI gui = new GroupsUI(driver);

		fc.utobj().printTestStep("Delete filter : " + SubModuleName + " > " + fieldName);
		fc.utobj().clickElement(driver, gui.getdeleteFieldXpath(SubModuleName + " > " + fieldName));
	}
}
