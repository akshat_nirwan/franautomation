package com.builds.test.salesTest;

import org.openqa.selenium.WebDriver;

import com.builds.utilities.FranconnectUtil;

public class Groups {

	private WebDriver driver;
	private FranconnectUtil fc = new FranconnectUtil();

	Groups(WebDriver driver) {
		this.driver = driver;
	}

	Group clickCreateGroup(Group group) throws Exception {
		fc.utobj().printTestStep("Click Create Group");

		return group;
	}

}
