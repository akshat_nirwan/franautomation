package com.builds.test.salesTest;

import org.openqa.selenium.WebDriver;

import com.builds.utilities.FranconnectUtil;

class Home {
	WebDriver driver;
	FranconnectUtil fc = new FranconnectUtil();

	Home(WebDriver driver) {
		this.driver = driver;
	}

	Lead clickAddLead_FillLeadInfo_Submit(Lead lead) {

		return lead;
	}

	LeadManagementTest clickMore() throws Exception {
		fc.utobj().printTestStep("Click More");

		return new LeadManagementTest(driver);
	}

}
