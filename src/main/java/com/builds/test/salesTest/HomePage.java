package com.builds.test.salesTest;

import com.builds.utilities.FranconnectUtil;

class HomePage {
	FranconnectUtil fc = new FranconnectUtil();

	void salesFunnel() throws Exception {
		fc.utobj().printTestStep("Sales Funnel");
	}

	void fifteenMostRecentLeads() throws Exception {
		fc.utobj().printTestStep("15 Most Recent Leads");
	}

	void recentlyViewedLeads() throws Exception {
		fc.utobj().printTestStep("Recently Viewed Leads");
	}

	void dashboard() throws Exception {
		fc.utobj().printTestStep("Dashboard");
	}

	void taskView() throws Exception {
		fc.utobj().printTestStep("Task View");
	}

	void myForecast() throws Exception {
		fc.utobj().printTestStep("My Forecast");
	}

}
