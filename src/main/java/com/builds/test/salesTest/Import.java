package com.builds.test.salesTest;

class Import {
	
	private String importType;
	private String specifyFileFormat;
	private String salesDataFile;
	
	// Default values for Fields
	private String inquiryDate;
	private String leadOwner;
	private String leadStatus;
	private String leadSourceCategory;
	private String leadSourceDetails;
	private String emailSubscriptionStatus;
	
	public String getImportType() {
		return importType;
	}
	public void setImportType(String importType) {
		this.importType = importType;
	}
	public String getSpecifyFileFormat() {
		return specifyFileFormat;
	}
	public void setSpecifyFileFormat(String specifyFileFormat) {
		this.specifyFileFormat = specifyFileFormat;
	}
	public String getSalesDataFile() {
		return salesDataFile;
	}
	public void setSalesDataFile(String salesDataFile) {
		this.salesDataFile = salesDataFile;
	}
	
	// Default values for Fields
	
	public String getInquiryDate() {
		return inquiryDate;
	}
	public void setInquiryDate(String inquiryDate) {
		this.inquiryDate = inquiryDate;
	}
	public String getLeadOwner() {
		return leadOwner;
	}
	public void setLeadOwner(String leadOwner) {
		this.leadOwner = leadOwner;
	}
	public String getLeadStatus() {
		return leadStatus;
	}
	public void setLeadStatus(String leadStatus) {
		this.leadStatus = leadStatus;
	}
	public String getLeadSourceCategory() {
		return leadSourceCategory;
	}
	public void setLeadSourceCategory(String leadSourceCategory) {
		this.leadSourceCategory = leadSourceCategory;
	}
	public String getLeadSourceDetails() {
		return leadSourceDetails;
	}
	public void setLeadSourceDetails(String leadSourceDetails) {
		this.leadSourceDetails = leadSourceDetails;
	}
	public String getEmailSubscriptionStatus() {
		return emailSubscriptionStatus;
	}
	public void setEmailSubscriptionStatus(String emailSubscriptionStatus) {
		this.emailSubscriptionStatus = emailSubscriptionStatus;
	}
	
	
}
