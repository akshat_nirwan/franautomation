package com.builds.test.salesTest;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.fs.ImportUI;
import com.builds.utilities.FranconnectUtil;

public class ImportTest {
	
	WebDriver driver;
	FranconnectUtil fc = new FranconnectUtil();

	public ImportTest(WebDriver driver) {
		this.driver = driver;
	}
	
	void importLeadsCSV(Import importLeads) throws Exception
	{
		ImportUI importUI = new ImportUI(driver);

		fc.utobj().printTestStep("Importing Leads through CSV");
		fill_importLeadsCSV(importLeads);
		fc.utobj().clickElement(driver, importUI.continueBtn);
		fill_defaultValuesForFields(importLeads);
		fc.utobj().clickElement(driver, importUI.disclaimer_checkbox); // Disclaimer CheckBox
		fc.utobj().clickElement(driver, importUI.continueBtn);
		fc.utobj().clickElement(driver, importUI.continueBtn);
		// Handling alert while navigating to some other page
		fc.utobj().sleep();
		fc.commonMethods().getModules().clickSalesModule(driver);
		if(fc.utobj().isAlertPresent(driver))
		{
			fc.utobj().acceptAlertBox(driver);
		}
	}
	
	private void fill_importLeadsCSV(Import importLeads) throws Exception
	{
		ImportUI importUI = new ImportUI(driver);
		
		if(importLeads.getImportType() != null)
		{
			if(importLeads.getImportType().equalsIgnoreCase("Leads"))
			{
				fc.utobj().clickElement(driver, importUI.leads_RadioBtn);
			}
			else if(importLeads.getImportType().equalsIgnoreCase("Brokers"))
			{
				fc.utobj().clickElement(driver, importUI.broker_RadioBtn);
			}
		}
		
		if(importLeads.getSpecifyFileFormat() != null)
		{
			if(importLeads.getSpecifyFileFormat().equalsIgnoreCase("CSV"))
			{
				fc.utobj().clickElement(driver, importUI.csv_RadioBtn);
			}
			else if(importLeads.getSpecifyFileFormat().equalsIgnoreCase("Tab"))
			{
				fc.utobj().clickElement(driver, importUI.tab_RadioBtn);
			}
		}
		
		if(importLeads.getSalesDataFile() != null)
		{
			fc.utobj().sendKeys(driver, importUI.chooseFile, importLeads.getSalesDataFile());
		}
	}
	
	private void fill_defaultValuesForFields(Import importLeads) throws Exception
	{
		ImportUI importUI = new ImportUI(driver);

		if(importLeads.getInquiryDate() != null)
		{
			fc.utobj().sendKeys(driver, importUI.inquiryDate, importLeads.getInquiryDate());
		}
		
		if(importLeads.getLeadOwner() == null)
		{
			fc.utobj().clickElement(driver, importUI.leadOwner_basedOnAssignmentRules_RadioBtn);
		}
		else if(importLeads.getLeadOwner() != null)
		{
			fc.utobj().clickElement(driver, importUI.leadOwner_select_RadioBtn);
			fc.utobj().selectDropDown(driver, importUI.leadOwner_select_DropDown, importLeads.getLeadOwner());
		}
		
		if(importLeads.getLeadStatus() != null)
		{
			fc.utobj().selectDropDown(driver, importUI.leadStatus_DropDown, importLeads.getLeadStatus());
		}
		
		if(importLeads.getLeadSourceCategory() != null)
		{
			fc.utobj().selectDropDown(driver, importUI.leadSourceCategory_DropDown, importLeads.getLeadSourceCategory());
		}
		
		if(importLeads.getLeadSourceDetails() != null)
		{
			fc.utobj().selectDropDown(driver, importUI.leadSourceDetails_DropDown, importLeads.getLeadSourceDetails());
		}
		
		if(importLeads.getEmailSubscriptionStatus() != null)
		{
			fc.utobj().selectDropDown(driver, importUI.emailSubscriptionStatus_DropDown, importLeads.getEmailSubscriptionStatus());
		}
		
		
		
		
	}

}
