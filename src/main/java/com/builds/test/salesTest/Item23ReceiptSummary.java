package com.builds.test.salesTest;

public class Item23ReceiptSummary {

	private String title;
	private String item23Receipt;
	private String businessName;
	private String address;
	private String country;
	private String state;
	private String phone;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getItem23Receipt() {
		return item23Receipt;
	}

	public void setItem23Receipt(String item23Receipt) {
		this.item23Receipt = item23Receipt;
	}

	public String getBusinessName() {
		return businessName;
	}

	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	/*
	 * @Override public String toString(){
	 * 
	 * return "::title::"+title +"::item23Receipt::"+ item23Receipt
	 * +"::businessName::"+ businessName +"::address::"+address +"::country::"+
	 * country +"::state::"+ state +"::phone::"+ phone; }
	 */

}
