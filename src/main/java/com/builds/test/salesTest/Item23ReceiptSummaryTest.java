package com.builds.test.salesTest;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.fs.Item23ReceiptSummaryUI;
import com.builds.utilities.FranconnectUtil;

public class Item23ReceiptSummaryTest {

	private FranconnectUtil fc = new FranconnectUtil();
	private WebDriver driver;

	Item23ReceiptSummaryTest(WebDriver driver) {
		this.driver = driver;
	}

	Item23ReceiptSummaryTest(WebDriver driver, Item23ReceiptSummary item23ReceiptSummary) {
		this.driver = driver;
	}

	Item23ReceiptSummaryTest addItem23Receipt(Item23ReceiptSummary item23ReceiptSummary) throws Exception {
		Item23ReceiptSummaryUI item23ReceiptSummaryUI = new Item23ReceiptSummaryUI(driver);
		fc.utobj().clickElement(driver, item23ReceiptSummaryUI.addItem23ReceiptBtn);
		fill_addItem23Receipt(driver, item23ReceiptSummary);
		fc.utobj().clickElement(driver, item23ReceiptSummaryUI.previewBtn);
		fc.utobj().clickElement(driver, item23ReceiptSummaryUI.saveBtn);
		return new Item23ReceiptSummaryTest(driver, item23ReceiptSummary);
	}

	void verify_addItem23Receipt(Item23ReceiptSummary item23ReceiptSummary) throws Exception {
		fc.utobj().printTestStep("Verify Add Item 23 Receipt");

		if (!fc.utobj().assertLinkText(driver, item23ReceiptSummary.getTitle())) {
			fc.utobj().throwsException("Added Item 23 Receipt 'Title' is not present on Summary page");
		} else {
			System.out.println("Receipt added successfully");
		}

	}

	void fill_addItem23Receipt(WebDriver driver, Item23ReceiptSummary item23ReceiptSummary) throws Exception {
		Item23ReceiptSummaryUI item23ReceiptSummaryUI = new Item23ReceiptSummaryUI(driver);

		if (item23ReceiptSummary.getTitle() != null) {
			fc.utobj().sendKeys(driver, item23ReceiptSummaryUI.title, item23ReceiptSummary.getTitle());
		}

		if (item23ReceiptSummary.getItem23Receipt() != null) {
			fc.utobj().sendKeys(driver, item23ReceiptSummaryUI.item23Receipt, item23ReceiptSummary.getItem23Receipt());
		}

		if (item23ReceiptSummary.getBusinessName() != null) {
			fc.utobj().sendKeys(driver, item23ReceiptSummaryUI.businessName, item23ReceiptSummary.getBusinessName());
		}

		if (item23ReceiptSummary.getAddress() != null) {
			fc.utobj().sendKeys(driver, item23ReceiptSummaryUI.address, item23ReceiptSummary.getAddress());
		}

		if (item23ReceiptSummary.getCountry() != null) {
			fc.utobj().selectDropDown(driver, item23ReceiptSummaryUI.country, item23ReceiptSummary.getCountry());
		}

		if (item23ReceiptSummary.getState() != null) {
			;
			fc.utobj().selectDropDown(driver, item23ReceiptSummaryUI.state, item23ReceiptSummary.getState());
		}

		if (item23ReceiptSummary.getPhone() != null) {
			fc.utobj().sendKeys(driver, item23ReceiptSummaryUI.phone, item23ReceiptSummary.getPhone());
		}

	}

}
