package com.builds.test.salesTest;

public class LeadAsCoApplicant extends Lead {

	private String CoApplicantRelationship;

	public String getCoApplicantRelationship() {
		return CoApplicantRelationship;
	}

	public void setCoApplicantRelationship(String coApplicantRelationship) {
		CoApplicantRelationship = coApplicantRelationship;
	}

}
