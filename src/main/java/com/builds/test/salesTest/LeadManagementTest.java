package com.builds.test.salesTest;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.builds.uimaps.fs.LeadManagementUI;
import com.builds.utilities.FranconnectUtil;

class LeadManagementTest {
	private WebDriver driver;
	FranconnectUtil fc = new FranconnectUtil();
	private Sales sales;

	LeadManagementTest(WebDriver driver) throws Exception {
		this.driver = driver;
		this.sales = new Sales(driver);
	}

	PrimaryInfoTest clickAddLead() throws Exception {
		LeadManagementUI ui = new LeadManagementUI(driver);
		fc.utobj().printTestStep("Click Add Lead at Lead Management Page");
		fc.utobj().clickElement(driver, ui.addLead);
		return new PrimaryInfoTest(driver);
	}

	void showFilters() throws Exception
	{
		LeadManagementUI ui = new LeadManagementUI(driver);
		fc.utobj().printTestStep("Click on Show Filters");
		fc.utobj().clickElement(driver, ui.showFilters_linkTextBtn);
	}
	
	void setViewPerPage(int viewPerPage) throws Exception
	{
		LeadManagementUI ui = new LeadManagementUI(driver);
		fc.utobj().selectDropDown(driver, ui.viewPerPage_dropDown, viewPerPage);
		
	}
	
	void selectALL_leadOwnersFilter() throws Exception
	{
		LeadManagementUI ui = new LeadManagementUI(driver);
		fc.utobj().clickElement(driver, ui.leadOwners_filter_dropDown_divID);
		WebElement selectAllCheck = fc.utobj().getElementByXpath(driver, ".//*[@id='selectAll']");
		fc.utobj().clickElement(driver, selectAllCheck);
		fc.utobj().clickElement(driver, ui.leadOwners_filter_dropDown_divID);
		
	}
	
	/*ArrayList<String> fetchLeadOwnersFilterDropDown_label() throws Exception
	{		
		ArrayList<String> leadOwnersList = new ArrayList<String>();
		List<WebElement> leadOwnersList_WebElements = new ArrayList<WebElement>();
		
		leadOwnersList_WebElements =  fc.utobj().getElementListByXpath(driver, ".//div[@id='ms-parentmyLeads']/div/ul/li");
		
		for(WebElement leadOwnerWebElement : leadOwnersList_WebElements )
		{
			leadOwnersList.add(leadOwnerWebElement.getText());
		}
		
		return leadOwnersList;
	}*/
	
	ArrayList<String> fetchLeadOwnersFilterDropDown_label() throws Exception {
		ArrayList<String> leadOwnersList = new ArrayList<String>();
		List<WebElement> leadOwnersList_WebElements = new ArrayList<WebElement>();

		fc.utobj().clickElement(driver, fc.utobj().getElementByID(driver, "ms-parentmyLeads"));

		// Select leadOwners_WebElements_Select = new Select(driver.findElement(By.id("myLeads")));
		// leadOwnersList_WebElements = driver.findElements(By.xpath(".//select[@id='myLeads']/option"));

		// leadOwnersList_WebElements = driver.findElements(By.xpath(".//div[@id='ms-parentmyLeads']/div/ul/li/label"));
		leadOwnersList_WebElements = driver.findElements(By.xpath(".//select[@id='myLeads']/option"));

		// leadOwnersList_WebElements = leadOwners_WebElements_Select.getOptions();

		for (WebElement el : leadOwnersList_WebElements) {
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			// System.out.println(">>>>>>>>>>>>>>>>>>>>>>>"+jse.executeScript("return arguments[0].text;",el));
			String tempString = (jse.executeScript("return arguments[0].text;", el)).toString();
			if (tempString.contains("(")) {
				tempString = tempString.substring(0, tempString.indexOf('('));
				tempString = tempString.trim();
				leadOwnersList.add(tempString);
			}
		}
		fc.utobj().clickElement(driver, fc.utobj().getElementByID(driver, "ms-parentmyLeads"));
		return leadOwnersList;
	}
	
	void select_leadOwnersFilter(ArrayList<String> leadOwners) throws Exception
	{
		LeadManagementUI ui = new LeadManagementUI(driver);
		fc.utobj().printTestStep("Select Lead Owners Filter");
		fc.utobj().selectMultipleValFromMultiSelect(driver, ui.leadOwners_filter_dropDown_divID, leadOwners);
	}
	
	void click_SearchBtn_Filters() throws Exception
	{
		LeadManagementUI ui = new LeadManagementUI(driver);
		fc.utobj().printTestStep("Click on Search button on Filters");
		fc.utobj().clickElement(driver, ui.Search_Button_ByID);
	}

	PrimaryInfoTest openLead_LeadManagementPage(Lead lead) throws Exception {
		LeadManagementUI ui = new LeadManagementUI(driver);
		fc.utobj().printTestStep("Click on Lead Name to view lead details.");
		fc.utobj().clickElement(driver, ui.getXpathOfTheLead(lead.getLeadFullName()));
		return new PrimaryInfoTest(driver);
	}

	PrimaryInfoTest clickAddLeadFillLeadDetailsAndSave(Lead lead) throws Exception {
		clickAddLead();
		PrimaryInfoTest pinfo = new PrimaryInfoTest(driver);
		return pinfo.fillLeadInfo_ClickSave(lead);

	}
	
	void click_saveView_filter() throws Exception
	{
		LeadManagementUI ui = new LeadManagementUI(driver);
		fc.utobj().clickElement(driver, ui.saveView_Btn);
	}
	
	void leadSearch(Lead lead) throws Exception
	{
		LeadManagementUI ui = new LeadManagementUI(driver);
		fc.utobj().sendKeys(driver, ui.leadSearch_TextBox, lead.getLeadFullName());
		fc.utobj().clickEnterOnElement(driver, ui.leadSearch_TextBox);
	}

	void removeFromGroup() throws Exception {
		LeadManagementUI ui = new LeadManagementUI(driver);
		fc.utobj().printTestStep("Remove from group");
		;
		fc.utobj().clickElement(driver, ui.removeFromGroup);
		fc.utobj().acceptAlertBox(driver);
	}

	void clickCheckBoxForLead(Lead ld) throws Exception {

		LeadManagementUI ui = new LeadManagementUI(driver);
		fc.utobj().printTestStep("Select the lead.");
		fc.utobj().clickElement(driver,
				driver.findElement(By.xpath(ui.getXpathOfTheLeadCheckBox(ld.getLeadFullName()))));
	}
	
	void clickCheckBoxForLead_multiple(Lead ld, Lead ld2) throws Exception {
		
		LeadManagementUI ui = new LeadManagementUI(driver);
		fc.utobj().printTestStep("Select the lead.");
		fc.utobj().clickElement(driver,
				driver.findElement(By.xpath(ui.getXpathOfTheLeadCheckBox(ld.getLeadFullName()))));
		fc.utobj().clickElement(driver,
				driver.findElement(By.xpath(ui.getXpathOfTheLeadCheckBox(ld2.getLeadFullName()))));
	}

	LeadManagementTest clickCheckBox_All() throws Exception {

		LeadManagementUI ui = new LeadManagementUI(driver);
		fc.utobj().printTestStep("Select all the lead.");
		fc.utobj().clickElement(driver, ui.checkAll);
		return new LeadManagementTest(driver);
	}

	void clickRightAction_AndClick_DetailedHistory(String leadName) throws Exception {

		clickRightAction_AndClick_selectActionsFromRightActionMenu(leadName, "Detailed History");
		fc.commonMethods().switch_cboxIframe_frameId(driver);
	}

	void clickRightAction_AndClick_SendEmail(String leadName) throws Exception {
		clickRightAction_AndClick_selectActionsFromRightActionMenu(leadName, "Send Email");
	}

	void clickRightAction_AndClick_LogACall(String leadName) throws Exception {

		clickRightAction_AndClick_selectActionsFromRightActionMenu(leadName, "Log a Call");
		fc.commonMethods().switch_cboxIframe_frameId(driver);
	}

	void clickRightAction_AndClick_LogATask(String leadName) throws Exception {

		clickRightAction_AndClick_selectActionsFromRightActionMenu(leadName, "Log a Task");
		fc.commonMethods().switch_cboxIframe_frameId(driver);
	}

	void clickRightAction_AndClick_AddRemarks(String leadName) throws Exception {

		clickRightAction_AndClick_selectActionsFromRightActionMenu(leadName, "Add Remarks");
		fc.commonMethods().switch_cboxIframe_frameId(driver);
	}

	void clickRightAction_AndClick_Delete(String leadName) throws Exception {

		clickRightAction_AndClick_selectActionsFromRightActionMenu(leadName, "Delete");
		fc.commonMethods().switch_cboxIframe_frameId(driver);
		fc.commonMethods().Click_OK_Input_ByValue(driver);
	}

	void clickRightAction_AndClick_MovetoInfoMgr(String leadName) throws Exception {

		clickRightAction_AndClick_selectActionsFromRightActionMenu(leadName, "Move to Info Mgr");
		fc.commonMethods().switch_cboxIframe_frameId(driver);
	}

	void clickRightAction_AndClick_MovetoOpener(String leadName) throws Exception {

		clickRightAction_AndClick_selectActionsFromRightActionMenu(leadName, "Move to Opener");
		fc.commonMethods().switch_cboxIframe_frameId(driver);
	}

	void clickRightAction_AndClick_OptOutfromEmailList(String leadName) throws Exception {

		clickRightAction_AndClick_selectActionsFromRightActionMenu(leadName, "Opt Out from Email List");
		fc.commonMethods().switch_cboxIframe_frameId(driver);
	}

	void clickRightAction_AndClick_AssociateCoApplicant(String leadName) throws Exception {

		clickRightAction_AndClick_selectActionsFromRightActionMenu(leadName, "Associate Co-Applicant");
		fc.commonMethods().switch_cboxIframe_frameId(driver);
	}

	private void clickRightAction_AndClick_selectActionsFromRightActionMenu(String leadName, String actionName)
			throws Exception {
		clickRightAction(leadName);
		selectActionsFromRightActionMenu(actionName);
	}

	private void selectActionsFromRightActionMenu(String actionName) throws Exception {

		LeadManagementUI ui = new LeadManagementUI(driver);

		fc.utobj().printTestStep("Select : " + actionName + ", from Right Action Menu");
		List<WebElement> actionMenu = ui.actionsInRigthActionMenu_ByXpath;

		for (WebElement menu : actionMenu) {
			if (actionName.equalsIgnoreCase(menu.getText().trim())) {
				fc.utobj().clickElement(driver, menu);
				break;
			}
		}
	}

	private void clickRightAction(String leadName) throws Exception {

		LeadManagementUI ui = new LeadManagementUI(driver);
		fc.utobj().printTestStep("Click Right Actions");
		fc.utobj().clickElement(driver, ui.getXpathOfTheLeads_RigthActionIcon(leadName));

	}

	private void clickLeftAction() throws Exception {

		LeadManagementUI ui = new LeadManagementUI(driver);
		fc.utobj().printTestStep("Click Left Actions");
		fc.utobj().clickElement(driver, ui.ActionsMenu_Left_Input_ByValue);

	}

	void clickLeftActionsMenu_Click_Delete_ClickOK() throws Exception {

		selectActionsFromLeftActionMenu("Delete");
		fc.commonMethods().switch_cboxIframe_frameId(driver);
		fc.commonMethods().Click_OK_Input_ByValue(driver);
	}

	void clickLeftActionsMenu_Click_ChangeStatus() throws Exception {

		selectActionsFromLeftActionMenu("Change Status");
		fc.commonMethods().switch_cboxIframe_frameId(driver);
	}

	void clickLeftActionsMenu_Click_logACall_AndSwitchToiFrame() throws Exception {

		selectActionsFromLeftActionMenu("Log a Call");
		fc.commonMethods().switch_cboxIframe_frameId(driver);
	}

	void clickLeftActionsMenu_Click_SendEmail() throws Exception {
		selectActionsFromLeftActionMenu("Send Email");
	}

	void clickLeftActionsMenu_Click_logATask() throws Exception {

		selectActionsFromLeftActionMenu("Log a Task");
		fc.commonMethods().switch_cboxIframe_frameId(driver);
	}

	void clickLeftActionsMenu_Click_Archive(String notes) throws Exception {

		selectActionsFromLeftActionMenu("Archive Leads");

		fc.utobj().acceptAlertBox(driver);

		ArchiveWindowTest arvhiveTest = new ArchiveWindowTest();
		arvhiveTest.addNotes_ClickArchive(driver, notes);
	}

	void clickLeftActionsMenu_Click_Add_To_Group() throws Exception {

		selectActionsFromLeftActionMenu("Add To Group");
		fc.commonMethods().switch_cboxIframe_frameId(driver);
	}

	void clickLeftActionsMenu_Click_ChangeOwner() throws Exception {

		selectActionsFromLeftActionMenu("Change Owner");
		fc.commonMethods().switch_cboxIframe_frameId(driver);
	}

	private void checkGroupNameOnGroupWindow(Group group) throws Exception {

		LeadManagementUI ui = new LeadManagementUI(driver);
		fc.utobj().printTestStep("Check the Group Name");
		fc.utobj().clickElement(driver, ui.getXpathOfCheckBoxOfGroup(group.getName()));
	}

	private void clickAddToGroupButtonOnGroupWindow() throws Exception {

		LeadManagementUI ui = new LeadManagementUI(driver);
		fc.utobj().printTestStep("Click Add To Group Button");
		fc.utobj().clickElement(driver, ui.addToGroup_Button);
		fc.utobj().printTestStep("Click OK on confirmation");
		fc.utobj().clickElement(driver, ui.ok_Input_ByValue);
		fc.utobj().switchFrameToDefault(driver);
	}

	void checkGroupName_clickAddToGroupButtonOnGroupWindow(Group group) throws Exception {
		checkGroupNameOnGroupWindow(group);
		clickAddToGroupButtonOnGroupWindow();
	}

	private void selectActionsFromLeftActionMenu(String actionName) throws Exception {

		LeadManagementUI ui = new LeadManagementUI(driver);

		clickLeftAction();

		fc.utobj().printTestStep("Select : " + actionName + ", from Left Action Menu");
		List<WebElement> actionMenu = ui.actionsIn_LeftActionMenu_ByXpath;

		for (WebElement menu : actionMenu) {
			if (actionName.equalsIgnoreCase(menu.getText().trim())) {
				fc.utobj().clickElement(driver, menu);
				break;
			}
		}
	}
	
	boolean VerifyActionIsPresent_FromLeftActionMenu(String actionNameToBeVerified) throws Exception {

		LeadManagementUI ui = new LeadManagementUI(driver);
		boolean isActionPresent = false;
		clickLeftAction();

		fc.utobj().printTestStep("Verify : " + actionNameToBeVerified + " is present in list, from Left Action Menu");
		List<WebElement> actionMenu = ui.actionsIn_LeftActionMenu_ByXpath;
		
		for (WebElement menu : actionMenu) {
			if ((actionNameToBeVerified.equalsIgnoreCase(menu.getText().trim()))) {
				isActionPresent = true;
				break;
			}
		}
		return isActionPresent;
	}
	

	void clickChangeStatus_BottomButton() throws Exception {

		LeadManagementUI ui = new LeadManagementUI(driver);

		fc.utobj().printTestStep("Click Change Status");
		fc.utobj().clickElement(driver, ui.changeStatus);
		fc.commonMethods().switch_cboxIframe_frameId(driver);
	}

	void clickChangeOwner_BottomButton() throws Exception {

		LeadManagementUI ui = new LeadManagementUI(driver);

		fc.utobj().printTestStep("Click Change Owner");
		fc.utobj().clickElement(driver, ui.changeOwner);
	}

	void clickSendEmail_BottomButton() throws Exception {

		LeadManagementUI ui = new LeadManagementUI(driver);

		fc.utobj().printTestStep("Click Send Email");
		fc.utobj().clickElement(driver, ui.sendMail);
	}

	void clickAddToGroup_BottomButton() throws Exception {

		LeadManagementUI ui = new LeadManagementUI(driver);

		fc.utobj().printTestStep("Click Add to Group");
		fc.utobj().clickElement(driver, ui.addToGroup);
		fc.commonMethods().switch_cboxIframe_frameId(driver);
	}

	void clickAssociateWithCampaign_BottomButton() throws Exception {

		LeadManagementUI ui = new LeadManagementUI(driver);

		fc.utobj().printTestStep("Click Associate With Campaign");
		fc.utobj().clickElement(driver, ui.associateCampaign);
	}

	void clickMailMerge_BottomButton() throws Exception {

		LeadManagementUI ui = new LeadManagementUI(driver);

		fc.utobj().printTestStep("Click Mail Merge");
		fc.utobj().clickElement(driver, ui.mailMerge);
	}

	void clickMergeLeads_BottomButton() throws Exception {

		LeadManagementUI ui = new LeadManagementUI(driver);

		fc.utobj().printTestStep("Click Merge Leads");
		fc.utobj().clickElement(driver, ui.mergeLeads);
	}

	void clickArchiveLeads_BottomButton() throws Exception {

		LeadManagementUI ui = new LeadManagementUI(driver);

		fc.utobj().printTestStep("Click Archive Leads");
		fc.utobj().clickElement(driver, ui.archiveLeads);
	}

	void clickDelete_BottomButton_ClickOK() throws Exception {

		LeadManagementUI ui = new LeadManagementUI(driver);
		fc.utobj().printTestStep("Click Delete");
		fc.utobj().clickElement(driver, ui.delete);
		fc.commonMethods().switch_cboxIframe_frameId(driver);
		fc.commonMethods().Click_OK_Input_ByValue(driver);
	}

	void clickPrint_BottomButton() throws Exception {

		LeadManagementUI ui = new LeadManagementUI(driver);

		fc.utobj().printTestStep("Click Print");
		fc.utobj().clickElement(driver, ui.Print);
	}

}
