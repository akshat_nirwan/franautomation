package com.builds.test.salesTest;

public class LeadSourceCategory {
	
	private String leadSourceCategory;
	private String includeInWebPage;

	
	public String getLeadSourceCategory() {
		return leadSourceCategory;
	}
	public void setLeadSourceCategory(String leadSourceCategory) {
		this.leadSourceCategory = leadSourceCategory;
	}
	public String getIncludeInWebPage() {
		return includeInWebPage;
	}
	public void setIncludeInWebPage(String includeInWebPage) {
		this.includeInWebPage = includeInWebPage;
	}
}
