package com.builds.test.salesTest;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.fs.LeadSourceCategoryUI;
import com.builds.utilities.FranconnectUtil;

public class LeadSourceCategoryTest {
	
	private FranconnectUtil fc = new FranconnectUtil();
	private WebDriver driver;
	
	LeadSourceCategoryTest(WebDriver driver) {
		this.driver = driver;
	}

	LeadSourceCategoryTest addLeadSourceCategory(LeadSourceCategory leadSourceCategory) throws Exception
	{
		fc.utobj().printTestStep("Adding Lead Source Category");
		LeadSourceCategoryUI ui = new LeadSourceCategoryUI(driver);
		fc.utobj().clickElement(driver, ui.addLeadSourceCategory_Btn);
		fc.utobj().switchFrameById(driver, "cboxIframe");
		fill_addLeadSourceCategory(leadSourceCategory);
		fc.utobj().clickElement(driver, ui.addBtn);
		fc.utobj().clickElement(driver, ui.close_Btn);
		fc.utobj().switchFrameToDefault(driver);
		
		return new LeadSourceCategoryTest(driver);
		
	}

	private void fill_addLeadSourceCategory(LeadSourceCategory leadSourceCategory) throws Exception
	{
		LeadSourceCategoryUI ui = new LeadSourceCategoryUI(driver);

		if(leadSourceCategory.getLeadSourceCategory() != null)
		{
			fc.utobj().sendKeys(driver, ui.enterLeadSourceCategory_Text, leadSourceCategory.getLeadSourceCategory());
		}
		
		if(leadSourceCategory.getIncludeInWebPage() != null)
		{
			if(leadSourceCategory.getIncludeInWebPage().equalsIgnoreCase("yes"))
			{
				if(! fc.utobj().isSelected(driver, ui.includeInWebPage_checkBox))
				{
					fc.utobj().clickElement(driver, ui.includeInWebPage_checkBox);
				}
			}
			
			else if(leadSourceCategory.getIncludeInWebPage().equalsIgnoreCase("no"))
			{
				if(fc.utobj().isSelected(driver, ui.includeInWebPage_checkBox))
				{
					fc.utobj().clickElement(driver, ui.includeInWebPage_checkBox);
				}
			}
		}
		
	}
	
	void verify_addLeadSourceCategory(LeadSourceCategory leadSourceCategory) throws Exception
	{
		fc.utobj().printTestStep("Verify Lead Source Category Addition");
		
		if(! fc.utobj().assertPageSource(driver, leadSourceCategory.getLeadSourceCategory()))
		{
			fc.utobj().throwsException("Verify Lead Source Category - Failed");
		}
	}
}
