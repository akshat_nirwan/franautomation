package com.builds.test.salesTest;

import java.util.ArrayList;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.fs.LeadStatusReportUI;
import com.builds.utilities.FranconnectUtil;

public class LeadStatusReportTest {
	
	WebDriver driver;
	
	public LeadStatusReportTest(WebDriver driver) {
	this.driver = driver;
	}
	
	void select_ViewLeadsBelongingTo_Filter(ArrayList<String> regionalUserName) throws Exception
	{
		LeadStatusReportUI leadStatusReportUI = new LeadStatusReportUI(driver);
		FranconnectUtil franconnectUtil = new FranconnectUtil();
		franconnectUtil.utobj().selectMultipleValFromMultiSelect(driver, leadStatusReportUI.viewLeadsBelongingTo_divID, regionalUserName);
		franconnectUtil.utobj().clickElement(driver, leadStatusReportUI.viewReportBtn);
	}

}
