package com.builds.test.salesTest;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.fs.LogOnCredentialsDurationUI;
import com.builds.utilities.FranconnectUtil;

class LogOnCredentialsDurationTest {

	private FranconnectUtil fc = new FranconnectUtil();
	private WebDriver driver;

	LogOnCredentialsDurationTest set_LogOnCredentialsDuration(WebDriver driver,
			LogOnCredentialsDuration logOnCredentialsDuration) throws Exception {
		fc.utobj().printTestStep("Setting Log on credentials duration");
		LogOnCredentialsDurationUI logOnCredentialsDurationUI = new LogOnCredentialsDurationUI(driver);

		if (logOnCredentialsDuration.getLogOnCredentialsDuration() != null) {
			fc.utobj().selectDropDown(driver, logOnCredentialsDurationUI.selectLogOnCredentialsDuration,
					logOnCredentialsDuration.getLogOnCredentialsDuration());
		}

		fc.utobj().clickElement(driver, logOnCredentialsDurationUI.saveBtn);

		return new LogOnCredentialsDurationTest(driver);
	}

	void verify_LogOnCredentialsDuration(LogOnCredentialsDuration logOnCredentialsDuration)
			throws Exception {
		LogOnCredentialsDurationUI logOnCredentialsDurationUI = new LogOnCredentialsDurationUI(driver);
		String value = fc.utobj().getSelectedDropDownValue(driver,
				logOnCredentialsDurationUI.selectLogOnCredentialsDuration);
		if (!value.equalsIgnoreCase(logOnCredentialsDuration.getLogOnCredentialsDuration())) {
			fc.utobj().throwsException("Log on credentials mismatch");
		}
	}

	LogOnCredentialsDurationTest(WebDriver driver) {
		this.driver = driver;
	}

}
