package com.builds.test.salesTest;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

class Login {

	WebDriver loginTest() {
		System.setProperty("webdriver.chrome.driver", "c:\\Selenium_Test_Input\\exe\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://qaautomation.franconnectqa.net/fc/");
		driver.manage().window().maximize();
		driver.findElement(By.id("user_id")).sendKeys("adm");
		driver.findElement(By.id("password")).sendKeys("t0n1ght");
		driver.findElement(By.id("ulogin")).click();
		return driver;

	}

}
