package com.builds.test.salesTest;

class ManageWebFormGenerator {
	
	private String formName;
	private String formTitle;
	private String displayFormTitle;
	private String formDescription;
	private String formFormat;
	private String noOfColumns;
	private String defaultFieldLabelAlignment;	
	private String formLanguage;
	private String iframeWidth;
	private String iframeHeight;
	private String formURL;
	
	public String getFormName() {
		return formName;
	}
	public void setFormName(String formName) {
		this.formName = formName;
	}
	public String getFormTitle() {
		return formTitle;
	}
	public void setFormTitle(String formTitle) {
		this.formTitle = formTitle;
	}
	public String getDisplayFormTitle() {
		return displayFormTitle;
	}
	public void setDisplayFormTitle(String displayFormTitle) {
		this.displayFormTitle = displayFormTitle;
	}
	public String getFormDescription() {
		return formDescription;
	}
	public void setFormDescription(String formDescription) {
		this.formDescription = formDescription;
	}
	public String getFormFormat() {
		return formFormat;
	}
	public void setFormFormat(String formFormat) {
		this.formFormat = formFormat;
	}
	public String getNoOfColumns() {
		return noOfColumns;
	}
	public void setNoOfColumns(String noOfColumns) {
		this.noOfColumns = noOfColumns;
	}
	public String getDefaultFieldLabelAlignment() {
		return defaultFieldLabelAlignment;
	}
	public void setDefaultFieldLabelAlignment(String defaultFieldLabelAlignment) {
		this.defaultFieldLabelAlignment = defaultFieldLabelAlignment;
	}
	public String getFormLanguage() {
		return formLanguage;
	}
	public void setFormLanguage(String formLanguage) {
		this.formLanguage = formLanguage;
	}
	public String getIframeWidth() {
		return iframeWidth;
	}
	public void setIframeWidth(String iframeWidth) {
		this.iframeWidth = iframeWidth;
	}
	public String getIframeHeight() {
		return iframeHeight;
	}
	public void setIframeHeight(String iframeHeight) {
		this.iframeHeight = iframeHeight;
	}
	public String getFormURL() {
		return formURL;
	}
	public void setFormURL(String formURL) {
		this.formURL = formURL;
	}
	
		
	//  Manage Web Form Generator  > Settings
	
	private String leadStatus;
	private String leadOwner;
	private String campaignName;
	private String leadSourceCategory;
	private String leadSourceDetails;
	private String division;
	private String afterSubmission;
	private String bodyText;
	
	
	public String getLeadStatus() {
		return leadStatus;
	}
	public void setLeadStatus(String leadStatus) {
		this.leadStatus = leadStatus;
	}
	public String getLeadOwner() {
		return leadOwner;
	}
	public void setLeadOwner(String leadOwner) {
		this.leadOwner = leadOwner;
	}
	public String getCampaignName() {
		return campaignName;
	}
	public void setCampaignName(String campaignName) {
		this.campaignName = campaignName;
	}
	public String getLeadSourceCategory() {
		return leadSourceCategory;
	}
	public void setLeadSourceCategory(String leadSourceCategory) {
		this.leadSourceCategory = leadSourceCategory;
	}
	public String getLeadSourceDetails() {
		return leadSourceDetails;
	}
	public void setLeadSourceDetails(String leadSourceDetails) {
		this.leadSourceDetails = leadSourceDetails;
	}
	public String getAfterSubmission() {
		return afterSubmission;
	}
	public void setAfterSubmission(String afterSubmission) {
		this.afterSubmission = afterSubmission;
	}
	public String getBodyText() {
		return bodyText;
	}
	public void setBodyText(String bodyText) {
		this.bodyText = bodyText;
	}
	
	public String getDivision() {
		return division;
	}
	public void setDivision(String division) {
		this.division = division;
	}




	

}
