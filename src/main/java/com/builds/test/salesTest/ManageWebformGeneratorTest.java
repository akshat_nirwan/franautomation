package com.builds.test.salesTest;

import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import com.builds.uimaps.fs.LeadUI;
import com.builds.uimaps.fs.ManageWebFormGeneratorBasicDetailsUI;
import com.builds.uimaps.fs.ManageWebFormGeneratorConfirmationUI;
import com.builds.uimaps.fs.ManageWebFormGeneratorDesignUI;
import com.builds.uimaps.fs.ManageWebFormGeneratorSettingsUI;
import com.builds.uimaps.fs.ManageWebFormGeneratorUI;
import com.builds.utilities.FranconnectUtil;

class ManageWebformGeneratorTest {
	
	WebDriver driver;
	FranconnectUtil fc = new FranconnectUtil();

	ManageWebformGeneratorTest(WebDriver driver) {
		this.driver = driver;
	}
	
	void createWebForm(ManageWebFormGenerator manageWebFormGenerator) throws Exception
	{
		ManageWebFormGeneratorUI manageWebFormGeneratorUI = new ManageWebFormGeneratorUI(driver);
		ManageWebFormGeneratorBasicDetailsUI detailsUI = new ManageWebFormGeneratorBasicDetailsUI(driver);
		ManageWebFormGeneratorDesignUI manageWebFormGeneratorDesignUI = new ManageWebFormGeneratorDesignUI(driver);
		ManageWebFormGeneratorSettingsUI manageWebFormGeneratorSettingsUI = new ManageWebFormGeneratorSettingsUI(driver);
		ManageWebFormGeneratorConfirmationUI manageWebFormGeneratorConfirmationUI = new ManageWebFormGeneratorConfirmationUI(driver);
		
		fc.utobj().clickElement(driver, manageWebFormGeneratorUI.createNewForm_btn);
		fill_ManageWebForm_Details(manageWebFormGenerator);			
		fc.utobj().clickElement(driver, detailsUI.saveAndNext_Btn);
		fc.utobj().clickElement(driver, manageWebFormGeneratorDesignUI.primaryInfo_availableField);
		moveAllFieldsFromAvailableToSection();
		// 	moveFieldFromAvailableToSection("city");
		fc.utobj().clickElement(driver, manageWebFormGeneratorDesignUI.saveAndNext_btn);
		fill_Webform_Settings(manageWebFormGenerator);
		fc.utobj().clickElement(driver, manageWebFormGeneratorSettingsUI.finishBtn);
		fc.utobj().clickElement(driver, manageWebFormGeneratorConfirmationUI.ok_btn);
	}
	
	private void fill_ManageWebForm_Details(ManageWebFormGenerator manageWebFormGenerator) throws Exception
	{
		ManageWebFormGeneratorBasicDetailsUI ui = new ManageWebFormGeneratorBasicDetailsUI(driver);
		if(manageWebFormGenerator.getFormName() != null)
		{
			fc.utobj().sendKeys(driver, ui.formName, manageWebFormGenerator.getFormName());
		}
		
		if(manageWebFormGenerator.getFormTitle() != null)
		{
			fc.utobj().sendKeys(driver, ui.formTitle, manageWebFormGenerator.getFormTitle());
		}
		
		if(manageWebFormGenerator.getDisplayFormTitle() != null)
		{
			if(manageWebFormGenerator.getDisplayFormTitle().equalsIgnoreCase("yes"))
			{	
				if(! fc.utobj().isSelected(driver, ui.displayFormTitle_checkbox))
				{ fc.utobj().clickElement(driver, ui.displayFormTitle_checkbox); }
			}
			if(manageWebFormGenerator.getDisplayFormTitle().equalsIgnoreCase("no"))
			{
				if(fc.utobj().isSelected(driver, ui.displayFormTitle_checkbox))
				{ fc.utobj().clickElement(driver, ui.displayFormTitle_checkbox); }
			}
		}
		
		if(manageWebFormGenerator.getFormDescription() != null)
		{
			fc.utobj().sendKeys(driver, ui.formDescription , manageWebFormGenerator.getFormDescription());
		}
		
		if(manageWebFormGenerator.getFormFormat() != null)
		{
			fc.utobj().selectDropDown(driver, ui.formFormat, manageWebFormGenerator.getFormFormat());
		}
		
		if(manageWebFormGenerator.getNoOfColumns() != null)
		{
			fc.utobj().selectDropDown(driver, ui.noOfColumns, manageWebFormGenerator.getNoOfColumns());
		}	
		
		if(manageWebFormGenerator.getDefaultFieldLabelAlignment() != null)
		{
			fc.utobj().selectDropDown(driver, ui.defaulFieldLabelAlignment, manageWebFormGenerator.getDefaultFieldLabelAlignment());
		}
		
		if(manageWebFormGenerator.getFormLanguage() != null)
		{
			fc.utobj().selectDropDown(driver, ui.formLanguage, manageWebFormGenerator.getFormLanguage());
		}
		
		if(manageWebFormGenerator.getIframeWidth() != null)
		{
			fc.utobj().sendKeys(driver, ui.iframeWidth, manageWebFormGenerator.getIframeWidth());
		}
		
		if(manageWebFormGenerator.getIframeHeight() != null)
		{
			fc.utobj().sendKeys(driver, ui.iframeWidth, manageWebFormGenerator.getIframeHeight());
		}
		
		if(manageWebFormGenerator.getFormURL() != null)
		{
			fc.utobj().sendKeys(driver, ui.formURL, manageWebFormGenerator.getFormURL());
		}
		
		
	}
	
	
	void moveFieldFromAvailableToSection(String fieldName) throws Exception
	{
		
		ManageWebFormGeneratorDesignUI ui = new ManageWebFormGeneratorDesignUI(driver);
		fc.utobj().sendKeys(driver, ui.searchFields_primaryInfo, fieldName);
		fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//li[@id='fsLeadDetails_fld']//a[(.='fieldName')]"), fc.utobj().getElementByXpath(driver,".//div[@class='drop-cnt' and @xpath='1']/p[contains(text(),'Drop here.')]"));
	}
	
	void moveAllFieldsFromAvailableToSection() throws Exception 
	{

	fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//li[@id='fsLeadDetails_fld']"));
	List<WebElement> listOfFields = driver.findElements(By.xpath(".//*[@id='fsLeadDetails_fld']//li[@data-fieldname]//a"));
		
		for(int x=0;x<listOfFields.size();x++)
		{
			try
			{
				//System.out.println(listOfFields.get(x).getText());
				moveFieldsToForm(listOfFields.get(x).getText());
				listOfFields = driver.findElements(By.xpath(".//*[@id='fsLeadDetails_fld']//li[@data-fieldname]//a"));
				Thread.sleep(2000);
				listOfFields.remove(listOfFields.get(x));
			}catch(Exception e)
			{
				System.out.println(e.getMessage());
			}
			
		}
		Thread.sleep(5000);
	}
	
	void moveFieldsToForm(String keywrd) throws Exception
	{
		//System.out.println(keywrd);
		driver.findElement(By.xpath(".//input[@data-searchid='fsLeadDetails']")).clear();
		driver.findElement(By.xpath(".//input[@data-searchid='fsLeadDetails']")).sendKeys(keywrd);
		
		List<WebElement> listOfElement = driver.findElements(By.xpath(".//li[@id='fsLeadDetails_fld']//a"));
		
		for(int x=0;x<listOfElement.size();x++)
		{
			if(listOfElement.get(x).getText().equalsIgnoreCase(keywrd)){
				WebElement dest = fc.utobj().getElementByXpath(driver, ".//ul[contains(@id,'_defaultSection') and contains(@id,'Flds')]");
				Actions act = new Actions(driver);
				act.dragAndDrop(listOfElement.get(x), dest).build().perform();
				//driver.findElement(By.xpath(".//input[@data-searchid='fsLeadDetails']")).clear();
				WebElement toClear = driver.findElement(By.xpath(".//input[@data-searchid='fsLeadDetails']"));
				toClear.sendKeys(Keys.CONTROL + "a");
				toClear.sendKeys(Keys.DELETE);
				
				break;
			}
		}
	}
	
	void fill_Webform_Settings(ManageWebFormGenerator manageWebFormGenerator) throws Exception
	{
		ManageWebFormGeneratorSettingsUI ui = new ManageWebFormGeneratorSettingsUI(driver);
		if(manageWebFormGenerator.getLeadStatus() != null)
		{
			fc.utobj().selectDropDown(driver, ui.leadStatus, manageWebFormGenerator.getLeadStatus());
		}
		
		if(manageWebFormGenerator.getLeadOwner() != null)
		{
			if(manageWebFormGenerator.getLeadOwner().equalsIgnoreCase("Based On Assignment Rules"))
			{
				fc.utobj().clickElement(driver, ui.LeadOwner_basedOnAssignmentRules_radioBtn);
			}
			else 
			{
				fc.utobj().clickElement(driver, ui.LeadOwner_selectRadioBtn);
				fc.utobj().selectDropDown(driver, ui.leadOwner_dropDown, manageWebFormGenerator.getLeadOwner());
			}
		}
		
		if(manageWebFormGenerator.getCampaignName() != null)
		{
			fc.utobj().selectDropDown(driver, ui.campaignName_dropdown, manageWebFormGenerator.getCampaignName());
		}
		
		if(manageWebFormGenerator.getLeadSourceCategory() != null)
		{
			fc.utobj().selectDropDown(driver, ui.leadSourceCategory_dropdown, manageWebFormGenerator.getLeadSourceCategory());
		}
		
		if(manageWebFormGenerator.getLeadSourceDetails() != null)
		{
			fc.utobj().selectDropDown(driver, ui.leadSourceDetails_dropdown, manageWebFormGenerator.getLeadSourceDetails());
		}
		
		if(manageWebFormGenerator.getDivision() != null)
		{
			fc.utobj().selectDropDown(driver, ui.division_dropdown, manageWebFormGenerator.getDivision());
		}
		
		if(manageWebFormGenerator.getAfterSubmission() != null)
		{
			if(manageWebFormGenerator.getAfterSubmission().equalsIgnoreCase("Confirmation Message"));
			{
				fc.utobj().clickElement(driver, ui.afterSubmission_ConfirmationMessage_radioBtn);
			}
			if(manageWebFormGenerator.getAfterSubmission().equalsIgnoreCase("Redirect URL"))
			{
				fc.utobj().clickElement(driver, ui.afterSubmission_RedirectURL_radioBtn);
			}
		}
		
		if(manageWebFormGenerator.getBodyText() != null)
		{
			fc.utobj().switchFrameById(driver, "textBlock_ifr");
			fc.utobj().sendKeys(driver, ui.bodyTextArea, manageWebFormGenerator.getBodyText());
			fc.utobj().switchFrameToDefault(driver);	
		}
	}
	
	void launchAndTest_Webform(ManageWebFormGenerator manageWebFormGenerator , Lead lead ) throws Exception
	{
		PrimaryInfoTest primaryInfoTest = new PrimaryInfoTest(driver);
		ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
		// fc.utobj().sleep(1000);
		searchForm(manageWebFormGenerator);
		String parentWindowHandle = driver.getWindowHandle();
		fc.utobj().singleActionIcon(driver, "Launch & Test");
		fc.utobj().sleep();
		
		Set<String> drivers  = driver.getWindowHandles();
		
		
		for (String i : drivers) {
			if (! i.equalsIgnoreCase(parentWindowHandle)) {
				driver.switchTo().window(i);
				String windowTitle = driver.getTitle();
				if (windowTitle.contains("WebFormTitle")) {

					// if(driver.getTitle().equalsIgnoreCase(manageWebFormGenerator.getFormTitle()))
					manageWebformGeneratorTest.fillWebFormLead(lead);
					fc.utobj().clickElement(driver,
							fc.utobj().getElementByXpath(driver, ".//input[@type='button' and @id='submitButton']"));
					driver.close();
					driver.switchTo().window(parentWindowHandle);
				} else {
					driver.switchTo().window(parentWindowHandle);
				}
			}
		}
	}
	
	void searchForm(ManageWebFormGenerator manageWebFormGenerator) throws Exception
	{
		ManageWebFormGeneratorUI ui = new ManageWebFormGeneratorUI(driver);
		fc.utobj().sendKeys(driver, ui.search_TextBox, manageWebFormGenerator.getFormName());
		fc.utobj().clickEnterOnElement(driver, ui.search_TextBox);
	}
	
	Lead fillWebFormLead(Lead lead) throws Exception {

		LeadUI ui = new LeadUI(driver);

		fc.utobj().printTestStep("Fill Lead Info");

		if (lead.getSelectLeadType() != null) {
			fc.utobj().selectDropDown(driver, ui.SelectLeadType, lead.getSelectLeadType());
		}

		if (lead.getExistingLead() != null) {
			fc.utobj().sendKeys(driver, ui.ExistingLead, lead.getExistingLead());
		}

		if (lead.getExistingOwner() != null) {
			fc.utobj().sendKeys(driver, ui.ExistingOwner, lead.getExistingOwner());
		}

		if (lead.getSalutation() != null) {
			fc.utobj().selectDropDownByValue(driver, ui.Salutation, lead.getSalutation());
		}

		if (lead.getFirstName() != null) {
			fc.utobj().sendKeys(driver, ui.firstName, lead.getFirstName());
		}

		if (lead.getLastName() != null) {
			fc.utobj().sendKeys(driver, ui.lastName, lead.getLastName());
		}

		if (lead.getAddress1() != null) {
			fc.utobj().sendKeys(driver, ui.address1, lead.getAddress1());
		}

		if (lead.getAddress2() != null) {
			fc.utobj().sendKeys(driver, ui.address2, lead.getAddress2());
		}

		if (lead.getCity() != null) {
			fc.utobj().sendKeys(driver, ui.city, lead.getCity());
		}

		if (lead.getCountry() != null) {
			fc.utobj().selectDropDown(driver, ui.country, lead.getCountry());
		}

		if (lead.getStateProvince() != null) {
			fc.utobj().selectDropDown(driver, ui.stateID, lead.getStateProvince());
		}

		if (lead.getZipPostalCode() != null) {
			fc.utobj().sendKeys(driver, ui.zip, lead.getZipPostalCode());
		}

		if (lead.getCounty() != null) {
			fc.utobj().selectDropDown(driver, ui.countyID, lead.getCounty());
		}

		if (lead.getPreferredModeofContact() != null) {
			fc.utobj().selectDropDown(driver, ui.PreferredModeofContact, lead.getPreferredModeofContact());
		}

		if (lead.getBestTimeToContact() != null) {
			fc.utobj().sendKeys(driver, ui.bestTimeToContact, lead.getBestTimeToContact());
		}

		if (lead.getWorkPhone() != null) {
			fc.utobj().sendKeys(driver, ui.workPhone, lead.getWorkPhone());
		}

		if (lead.getWorkPhoneExtension() != null) {
			fc.utobj().sendKeys(driver, ui.workPhoneExt, lead.getWorkPhoneExtension());
		}

		if (lead.getHomePhone() != null) {
			fc.utobj().sendKeys(driver, ui.homePhone, lead.getHomePhone());
		}

		if (lead.getHomePhoneExtension() != null) {
			fc.utobj().sendKeys(driver, ui.homePhoneExt, lead.getHomePhoneExtension());
		}

		if (lead.getFax() != null) {
			fc.utobj().sendKeys(driver, ui.fax, lead.getFax());
		}

		if (lead.getMobile() != null) {
			fc.utobj().sendKeys(driver, ui.mobile, lead.getMobile());
		}

		if (lead.getEmail() != null) {
			fc.utobj().sendKeys(driver, ui.emailID, lead.getEmail());
		}

		if (lead.getCompanyName() != null) {
			fc.utobj().sendKeys(driver, ui.companyName, lead.getCompanyName());
		}

		if (lead.getComment() != null) {
			fc.utobj().sendKeys(driver, ui.comments, lead.getComment());
		}

		if (lead.getLeadOwner() != null) {
			fc.utobj().selectDropDown(driver, ui.leadOwnerID, lead.getLeadOwner());
		}

		/*if (lead.getBasedonAssignmentRules() != null) {
			if (lead.getBasedonAssignmentRules().equalsIgnoreCase("yes")) {
				fc.utobj().clickElement(driver, ui.basedonAssignmentRules);
			}
		}*/

		if (lead.getLeadRating() != null) {
			fc.utobj().selectDropDown(driver, ui.leadRatingID, lead.getLeadRating());
		}

		if (lead.getMarketingCode() != null) {
			fc.utobj().selectDropDown(driver, ui.marketingCodeId, lead.getMarketingCode());
		}

		if (lead.getLeadSourceCategory() != null) {
			fc.utobj().selectDropDownByVisibleTextTrimed(driver, ui.leadSource2ID_Category, lead.getLeadSourceCategory());
		}

		if (lead.getLeadSourceDetails() != null) {
			fc.utobj().selectDropDownByVisibleTextTrimed(driver, ui.leadSource3ID_Details, lead.getLeadSourceDetails());
		}

		if (lead.getOtherLeadSources() != null) {
			fc.utobj().sleep();
			fc.utobj().sendKeys(driver, ui.otherLeadSourceDetail, lead.getOtherLeadSources());
		}

		if (lead.getCurrentNetWorth() != null) {
			fc.utobj().sendKeys(driver, ui.CurrentNetWorth, lead.getCurrentNetWorth());
		}

		if (lead.getCashAvailableforInvestment() != null) {
			fc.utobj().sendKeys(driver, ui.CashAvailableforInvestment, lead.getCashAvailableforInvestment());
		}

		if (lead.getInvestmentTimeframe() != null) {
			fc.utobj().sendKeys(driver, ui.investTimeframe, lead.getInvestmentTimeframe());
		}

		if (lead.getBackground() != null) {
			fc.utobj().sendKeys(driver, ui.background, lead.getBackground());
		}

		if (lead.getSourceOfInvestment() != null) {
			fc.utobj().sendKeys(driver, ui.sourceOfFunding, lead.getSourceOfInvestment());
		}

		if (lead.getNextCallDate() != null) {
			fc.utobj().sendKeys(driver, ui.nextCallDate, lead.getNextCallDate());
		}

		if (lead.getNoOfUnitsLocationsRequested() != null) {
			fc.utobj().sendKeys(driver, ui.noOfUnitReq, lead.getNoOfUnitsLocationsRequested());
		}

		/*if (lead.getDivision() != null) {
			fc.utobj().selectDropDown(driver, ui.brandMapping_0brandID, lead.getDivision());
		}*/

		if (lead.getPreferredCity1() != null) {
			fc.utobj().sendKeys(driver, ui.temppreferredCity1, lead.getPreferredCity1());
		}

		if (lead.getPreferredCountry1() != null) {
			fc.utobj().selectDropDown(driver, ui.temppreferredCountry1, lead.getPreferredCountry1());
		}

		if (lead.getPreferredStateProvince1() != null) {
			fc.utobj().selectDropDown(driver, ui.temppreferredStateId1, lead.getPreferredStateProvince1());
		}

		if (lead.getPreferredCity2() != null) {
			fc.utobj().sendKeys(driver, ui.temppreferredCity2, lead.getPreferredCity2());
		}

		if (lead.getPreferredCountry2() != null) {
			fc.utobj().selectDropDown(driver, ui.temppreferredCountry2, lead.getPreferredCountry2());
		}

		if (lead.getPreferredStateProvince2() != null) {
			fc.utobj().selectDropDown(driver, ui.temppreferredStateId2, lead.getPreferredStateProvince2());
		}

		if (lead.getNewAvailableSites() != null) {
			fc.utobj().selectDropDown(driver, ui.AvailableSites, lead.getNewAvailableSites());
		}

		if (lead.getExistingSites() != null) {
			fc.utobj().selectDropDown(driver, ui.ExistingSites, lead.getExistingSites());
		}

		if (lead.getResaleSites() != null) {
			fc.utobj().selectDropDown(driver, ui.ResaleSites, lead.getResaleSites());
		}

		if (lead.getForecastClosureDate() != null) {
			fc.utobj().sendKeys(driver, ui.forecastClosureDate, lead.getForecastClosureDate());
		}

		if (lead.getProbability() != null) {
			fc.utobj().sendKeys(driver, ui.probability, lead.getProbability());
		}

		if (lead.getForecastRating() != null) {
			fc.utobj().sendKeys(driver, ui.forecastRating, lead.getForecastRating());
		}

		if (lead.getForecastRevenue() != null) {
			fc.utobj().sendKeys(driver, ui.forecastRevenue, lead.getForecastRevenue());
		}

		if (lead.getCampaignName() != null) {
			if ("Based on Workflow Assignment Rules".equalsIgnoreCase(lead.getCampaignName())) {
				fc.utobj().clickElement(driver, ui.BasedonWorkflowAssignmentRules);
			} else {
				fc.utobj().clickElement(driver, ui.campaignName);
				fc.utobj().selectDropDown(driver, ui.campaignID, lead.getCampaignName());
			}
		}
		return lead;
	}

	
	

}
