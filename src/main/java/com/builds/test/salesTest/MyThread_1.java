package com.builds.test.salesTest;

public class MyThread_1 extends Thread {

	public void run() {
		for (int i = 1; i < 5; i++) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				System.out.println(e);
			}
			System.out.println(Thread.currentThread().getName());
		}

	}

	public static void main(String arg[]) {
		MyThread_1 myThread_1 = new MyThread_1();
		MyThread_1 myThread_2 = new MyThread_1();
		MyThread_1 myThread_3 = new MyThread_1();

		myThread_1.start();
		try{
		myThread_1.join();
		} catch(Exception e) {}
		
		myThread_2.start();
		myThread_3.start();

	}

}
