package com.builds.test.salesTest;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.builds.uimaps.fs.LeadUI;
import com.builds.uimaps.fs.PrimaryInfoUI;
import com.builds.utilities.FranconnectUtil;

public class PrimaryInfoTest {
	FranconnectUtil fc = new FranconnectUtil();
	WebDriver driver;
	Sales sales;

	public PrimaryInfoTest(WebDriver driver) throws Exception {
		this.driver = driver;
		this.sales = new Sales(driver);
	}

	public String getCurrentLeadStatusFromDropDown() throws Exception {
		PrimaryInfoUI ui = new PrimaryInfoUI(driver);
		return fc.utobj().getSelectedDropDownValue(driver, ui.LeadStatus_Select).trim();
	}

	public void click_AddRemarks_TopLink() throws Exception {
		PrimaryInfoUI ui = new PrimaryInfoUI(driver);
		fc.utobj().printTestStep("Click Add Remarks - Top Link");
		fc.utobj().clickElement(driver, ui.AddRemarks_Link);
		fc.commonMethods().switch_cboxIframe_frameId(driver);
	}

	public void click_Modify_TopLink() throws Exception {
		PrimaryInfoUI ui = new PrimaryInfoUI(driver);
		fc.utobj().printTestStep("Click Modify - Top Link");
		fc.utobj().clickElement(driver, ui.modify_Link);
	}

	public void click_Log_A_Call_TopLink() throws Exception {

		PrimaryInfoUI ui = new PrimaryInfoUI(driver);

		fc.utobj().printTestStep("Click Log A Call - Top Link");
		fc.utobj().clickElement(driver, ui.LogaCall_Link);
		fc.commonMethods().switch_cboxIframe_frameId(driver);
	}

	public void click_Log_A_Task_TopLink() throws Exception {

		PrimaryInfoUI ui = new PrimaryInfoUI(driver);

		fc.utobj().printTestStep("Click Log A Task - Top Link");
		fc.utobj().clickElement(driver, ui.LogaTask_Link);
		fc.commonMethods().switch_cboxIframe_frameId(driver);
	}

	public void click_AddRemarks_ActivityHistory() throws Exception {

		PrimaryInfoUI ui = new PrimaryInfoUI(driver);

		fc.utobj().printTestStep("Click Add Remarks - Activity History");
		switchToActivityHistoryFrame();
		fc.utobj().clickElement(driver, ui.AddRemarks_Link_ActivityHistory);
		fc.utobj().switchFrameToDefault(driver);
		fc.commonMethods().switch_cboxIframe_frameId(driver);
	}

	public DetailedHistoryTest click_DetailedHistory_ActivityHistory() throws Exception {
		PrimaryInfoUI ui = new PrimaryInfoUI(driver);
		fc.utobj().printTestStep("Go to Activity History in Lead's Primary Info");
		
		switchToActivityHistoryFrame();
		fc.utobj().printTestStep("Click Detailed History");
		
		fc.utobj().clickElementByJS(driver, ui.DetailedHistory_Link_ActivityHistory);
		fc.utobj().switchFrameToDefault(driver);
		
		fc.commonMethods().switch_cboxIframe_frameId(driver);

		return new DetailedHistoryTest(driver);
	}

	public void logACall_TopLink(Call call) throws Exception {

		fc.utobj().printTestStep("Verify log a Call");

		// TODO

	}

	public void logATask_TopLink(Task task) throws Exception {

		fc.utobj().printTestStep("Verify log a Task");

		// TODO

	}

	public void ViewElectronicDetails_TopLink() throws Exception {

		fc.utobj().printTestStep("Verify View Electronic Details");

		// TODO This code is incomplete
	}

	private void switchToActivityHistoryFrame() throws Exception {
		PrimaryInfoUI ui = new PrimaryInfoUI(driver);
		fc.utobj().switchFrameById(driver, ui.leadLogCallSummary_IFrame_ById);
	}

	public void verify_TextUnder_ActivityHistory(String text) throws Exception {
		fc.utobj().printTestStep("Verify the Subject/Remarks is present");

		switchToActivityHistoryFrame();
		if (!fc.utobj().assertPageSource(driver, text)) {
			fc.utobj().throwsException("Remarks not found in the Activity History.");
		}
		fc.utobj().switchFrameToDefault(driver);
	}

	public void verifyCallInfo_ActivityHistory(Call call) throws Exception {

		switchToActivityHistoryFrame();
		fc.utobj().printTestStep("Verify Call is present in Activity History.");
		if (fc.utobj().assertPageSource(driver, call.getSubject()) == false) {
			fc.utobj().throwsException("Call not present in Activity History");
		}

		// TODO: This method is incomplete

		fc.utobj().switchFrameToDefault(driver);

	}

	public void verifyTaskInfo_ActivityHistory(Task task) throws Exception {

		PrimaryInfoUI ui = new PrimaryInfoUI(driver);

		fc.utobj().printTestStep("Verify Task in Contact History > Open Task");
		fc.utobj().switchFrameById(driver, ui.leadOpenActivitesSummary_IFrame_ById);

		// TODO: This method is incomplete

		if (!fc.utobj().assertPageSource(driver, task.getSubject())) {
			fc.utobj().throwsException("Task not found in Open Task Section");
		}

		fc.utobj().switchFrameToDefault(driver);

	}

	public void modify_CallInfo_ActivityHistory(Call call, Call callNew) throws Exception {

		switchToActivityHistoryFrame();
		fc.utobj().printTestStep("Modify Call in Activity History.");
		fc.utobj().actionImgOption(driver, call.getSubject(), "Modify");
		fc.utobj().switchFrameToDefault(driver);		
		fc.commonMethods().switch_cboxIframe_frameId(driver);
		CallTest callTest = new CallTest(driver);
		callTest.fillNewCall(callNew);
		callTest.click_Add();
		callTest.click_No_ConfirmSchedule();
		fc.utobj().switchFrameToDefault(driver);

	}
	
	public void delete_CallInfo_ActivityHistory(Call call) throws Exception {

		switchToActivityHistoryFrame();
		fc.utobj().printTestStep("Modify Call in Activity History.");
		fc.utobj().actionImgOption(driver, call.getSubject(), "Delete");
		fc.utobj().acceptAlertBox(driver);
		fc.utobj().switchFrameToDefault(driver);		

	} 

	public void clickPrev() throws Exception {

		PrimaryInfoUI ui = new PrimaryInfoUI(driver);

		fc.utobj().printTestStep("Click Prev link");
		fc.utobj().clickElement(driver, ui.prev_Xpath);
	}

	public void clickNext() throws Exception {

		PrimaryInfoUI ui = new PrimaryInfoUI(driver);

		fc.utobj().printTestStep("Click Next link");
		fc.utobj().clickElement(driver, ui.next_Xpath);
	}

	public void clickNextOrPrev() throws Exception {

		PrimaryInfoUI ui = new PrimaryInfoUI(driver);

		if (fc.utobj().isElementPresent(driver, ui.next_Xpath) == true) {
			fc.utobj().printTestStep("Click Next Link");
			fc.utobj().clickElement(driver, ui.next_Xpath);
		} else if (fc.utobj().isElementPresent(driver, ui.prev_Xpath) == true) {
			fc.utobj().printTestStep("Click Prev Link");
			fc.utobj().clickElement(driver, ui.prev_Xpath);
		}
	}

	private void clickActivityTimeline() throws Exception {

		PrimaryInfoUI ui = new PrimaryInfoUI(driver);

		fc.utobj().printTestStep("Click Activity Timeline");
		fc.utobj().clickElement(driver, ui.ActivityTimeline);
	}

	private void clickHeatMeter() throws Exception {

		PrimaryInfoUI ui = new PrimaryInfoUI(driver);

		fc.utobj().printTestStep("Click Heat Meter");
		fc.utobj().clickElement(driver, ui.HeatMeter);
	}

	public void verifyUnderActivityTimeline(String comments) throws Exception {

		PrimaryInfoUI ui = new PrimaryInfoUI(driver);

		clickActivityTimeline();

		fc.utobj().printTestStep("Verify Comments/Remarks under Activity Timeline : " + comments);
		boolean isTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ui.checkValueUnderRightMeter(comments));

		if (isTextPresent == false) {
			fc.utobj().throwsException("Comments/Remarks not found under Activity Timeline");
		}
	}

	public void verifyUnderHeatMeter(String comments) throws Exception {

		PrimaryInfoUI ui = new PrimaryInfoUI(driver);

		clickHeatMeter();

		fc.utobj().printTestStep("Verify Comments/Remarks under Right Meter");

		fc.utobj().moveToElement(driver, ui.checkValueUnderRightMeter(comments));
	}

	public void clickCommentsUnderActivityTimeline(String comments) throws Exception {

		PrimaryInfoUI ui = new PrimaryInfoUI(driver);

		clickActivityTimeline();

		fc.utobj().printTestStep("Click Comments/Remarks under Right Meter");

		fc.utobj().clickElement(driver, ui.checkValueUnderRightMeter(comments));
	}

	public void verifyCommentsUnderHeatMeter(String comments) throws Exception {

		PrimaryInfoUI ui = new PrimaryInfoUI(driver);

		clickHeatMeter();

		fc.utobj().printTestStep("Click Comments/Remarks under Right Meter");

		fc.utobj().clickElement(driver, ui.checkValueUnderRightMeter(comments));
	}

	public void clickMoreActionRightMenu() throws Exception {

		PrimaryInfoUI ui = new PrimaryInfoUI(driver);

		fc.utobj().printTestStep("Click on More Action Menu");
		fc.utobj().moveToElementThroughAction(driver, ui.moreAction_RigthMenu_Xpath);
		//fc.utobj().clickElement(driver, ui.moreAction_RigthMenu_Xpath);
	}

	public void clickOnPrimaryInfoTab() throws Exception {

		PrimaryInfoUI ui = new PrimaryInfoUI(driver);

		fc.utobj().printTestStep("Click on Primary Info tab");
		fc.utobj().clickElement(driver, ui.PrimaryInfo_tab_link);
	}

	public void clickOnCo_ApplicantTab() throws Exception {

		PrimaryInfoUI ui = new PrimaryInfoUI(driver);

		fc.utobj().printTestStep("Click on Co_Applicant tab");
		fc.utobj().clickElement(driver, ui.CoApplicants_tab_link);
	}

	public void clickOnComplianceTab() throws Exception {

		PrimaryInfoUI ui = new PrimaryInfoUI(driver);

		fc.utobj().printTestStep("Click on Compliance tab");
		fc.utobj().clickElement(driver, ui.Compliance_tab_link);
	}

	public void clickOnDocumentsTab() throws Exception {

		PrimaryInfoUI ui = new PrimaryInfoUI(driver);

		fc.utobj().printTestStep("Click on Documents tab");
		fc.utobj().clickElement(driver, ui.Documents_tab_link);

	}

	public void clickOnPersonalProfileTab() throws Exception {

		PrimaryInfoUI ui = new PrimaryInfoUI(driver);

		fc.utobj().printTestStep("Click on Personal Profile tab");
		fc.utobj().clickElement(driver, ui.PersonalProfile_tab_link);

	}

	public void clickOnQualificationDetailsTab() throws Exception {

		PrimaryInfoUI ui = new PrimaryInfoUI(driver);

		fc.utobj().printTestStep("Click on Qualification Details tab");
		fc.utobj().clickElement(driver, ui.QualificationDetails_tab_link);

	}

	public void clickOnRealEstateTab() throws Exception {

		PrimaryInfoUI ui = new PrimaryInfoUI(driver);

		fc.utobj().printTestStep("Click on Real Estate tab");
		fc.utobj().clickElement(driver, ui.RealEstate_tab_link);

	}

	public void clickOnVirtualBrochureTab() throws Exception {

		PrimaryInfoUI ui = new PrimaryInfoUI(driver);

		fc.utobj().printTestStep("Click on Virtual Brochure tab");
		fc.utobj().clickElement(driver, ui.VirtualBrochure_tab_link);

	}

	public void clickOnVisitTab() throws Exception {

		PrimaryInfoUI ui = new PrimaryInfoUI(driver);

		fc.utobj().printTestStep("Click on Visit tab");
		fc.utobj().clickElement(driver, ui.Visit_tab_link);

	}

	public void clickOnbQualTab() throws Exception {

		PrimaryInfoUI ui = new PrimaryInfoUI(driver);

		fc.utobj().printTestStep("Click on bQual tab");
		fc.utobj().clickElement(driver, ui.bQual_tab_link);

	}

	public void clickOnProvenMatchAssessmentTab() throws Exception {

		PrimaryInfoUI ui = new PrimaryInfoUI(driver);

		fc.utobj().printTestStep("Click on Proven Match Assessment tab");
		fc.utobj().clickElement(driver, ui.ProvenMatchAssessment_tab_link);

	}

	public void clickProvenMatchAssessment_AndClose(String successMessage) throws Exception {

		PrimaryInfoUI ui = new PrimaryInfoUI(driver);

		fc.utobj().printTestStep("Click on Proven Match Assessment tab");
		fc.utobj().clickElement(driver, ui.provenMatchLink);
		fc.commonMethods().switch_cboxIframe_frameId(driver);

		if (!fc.utobj().assertPageSource(driver, successMessage)) {
			fc.utobj().throwsException("Proven Match Invite not successful.");
		}
		fc.commonMethods().Click_Close_Input_ByValue(driver);

		fc.utobj().switchFrameToDefault(driver);

		if (fc.utobj().isElementPresent(driver, ui.provenMatchLink) == true) {
			fc.utobj().throwsException("Proven Match Invite Link is still present");
		}
	}

	private void selectStatusInDropDown_OnPrimaryInfoViewPage(String status) throws Exception {

		PrimaryInfoUI ui = new PrimaryInfoUI(driver);

		fc.utobj().printTestStep("Select New Status In Status Drop Down");
		fc.utobj().clickElement(driver, ui.LeadStatus_Select);
	}

	public void changeStatusInDropDownOnPrimaryInfoViewPage(String status) throws Exception {

		PrimaryInfoUI ui = new PrimaryInfoUI(driver);

		selectStatusInDropDown_OnPrimaryInfoViewPage(status);
		fc.utobj().printTestStep("Click Change Status Button on View Page");
		fc.utobj().clickElement(driver, ui.changeStatus_Button);
	}

	public void click_RightActionMenu_Then_ChangeOwner() throws Exception {
		clickMoreActions_SelectOption("Change Owner");
	}

	public SendFDDTest click_RightActionMenu_Then_SendFDDEmail() throws Exception {
		clickMoreActions_SelectOption("Send FDD Email");
		return new SendFDDTest(driver);
	}

	public void click_RightActionMenu_Then_SendVirtualBrochureEmail() throws Exception {
		clickMoreActions_SelectOption("Send Virtual Brochure Email");
	}

	public void click_RightActionMenu_Then_AddToGroup() throws Exception {

		clickMoreActions_SelectOption("Add To Group");
		fc.commonMethods().switch_cboxIframe_frameId(driver);
	}

	public void click_RightActionMenu_Then_MailMerge() throws Exception {
		clickMoreActions_SelectOption("Mail Merge");
	}

	public void click_RightActionMenu_Then_MoveToInfoMgr() throws Exception {
		clickMoreActions_SelectOption("Move To Info Mgr");
	}

	public void click_RightActionMenu_Then_MoveToOpener() throws Exception {
		clickMoreActions_SelectOption("Move To Opener");
	}

	public void click_RightActionMenu_Then_AddAnotherLead() throws Exception {
		clickMoreActions_SelectOption("Add Another Lead");
	}

	public void click_RightActionMenu_Then_AssociateWithCampaign() throws Exception {
		clickMoreActions_SelectOption("Associate With Campaign");
	}

	public void click_RightActionMenu_Then_ViewDetailedHistory() throws Exception {
		clickMoreActions_SelectOption("View Detailed History");
	}

	public void click_RightActionMenu_Then_ViewinMap() throws Exception {
		clickMoreActions_SelectOption("View in Map");
	}

	public void click_RightActionMenu_Then_KillLead() throws Exception {
		clickMoreActions_SelectOption("Kill Lead");
	}

	public void click_RightActionMenu_Then_DeleteLead() throws Exception {

		clickMoreActions_SelectOption("Delete Lead");
		fc.commonMethods().switch_cboxIframe_frameId(driver);
		fc.commonMethods().Click_OK_Input_ByValue(driver);

	}

	public void click_RightActionMenu_Then_OptOutfromEmailList() throws Exception {
		clickMoreActions_SelectOption("Opt Out from Email List");
	}

	public void click_RightActionMenu_Then_AssociateCoApplicant() throws Exception {
		clickMoreActions_SelectOption("Associate Co-Applicant");
	}

	public void click_RightActionMenu_Then_ViewTabsonTop() throws Exception {
		clickMoreActions_SelectOption("View Tabs on Top");
	}

	private void clickMoreActions_SelectOption(String action) throws Exception {

		clickMoreActionRightMenu();

		PrimaryInfoUI ui = new PrimaryInfoUI(driver);
		fc.utobj().printTestStep("Click on More-Actions : " + action);

		for (WebElement el : ui.list_RightActionsMenu) {
			if (action.trim().equalsIgnoreCase(el.getText().trim())) {
				el.click();
				break;
			}
		}
	}

	public void checkGroupName_clickAddToGroupButtonOnGroupWindow(Group group) throws Exception {
		LeadManagementTest lm = new LeadManagementTest(driver);
		lm.checkGroupName_clickAddToGroupButtonOnGroupWindow(group);
	}

	PrimaryInfoTest clickSave(Lead lead) throws Exception {
		LeadUI ui = new LeadUI(driver);
		fc.utobj().printTestStep("Click Save");
		fc.utobj().clickElement(driver, ui.Save);

		return new PrimaryInfoTest(driver);
	}

	PrimaryInfoTest fillLeadInfo_ClickSave(Lead lead) throws Exception {
		fillLeadInfo(lead);
		clickSave(lead);
		return new PrimaryInfoTest(driver);
	}

	Lead fillLeadInfo(Lead lead) throws Exception {

		LeadUI ui = new LeadUI(driver);

		fc.utobj().printTestStep("Fill Lead Info");

		if (lead.getSelectLeadType() != null) {
			fc.utobj().selectDropDown(driver, ui.SelectLeadType, lead.getSelectLeadType());
		}

		if (lead.getExistingLead() != null) {
			fc.utobj().sendKeys(driver, ui.ExistingLead, lead.getExistingLead());
		}

		if (lead.getExistingOwner() != null) {
			fc.utobj().sendKeys(driver, ui.ExistingOwner, lead.getExistingOwner());
		}

		if (lead.getSalutation() != null) {
			fc.utobj().selectDropDownByValue(driver, ui.Salutation, lead.getSalutation());
		}

		if (lead.getFirstName() != null) {
			fc.utobj().sendKeys(driver, ui.firstName, lead.getFirstName());
		}

		if (lead.getLastName() != null) {
			fc.utobj().sendKeys(driver, ui.lastName, lead.getLastName());
		}

		if (lead.getAddress1() != null) {
			fc.utobj().sendKeys(driver, ui.address1, lead.getAddress1());
		}

		if (lead.getAddress2() != null) {
			fc.utobj().sendKeys(driver, ui.address2, lead.getAddress2());
		}

		if (lead.getCity() != null) {
			fc.utobj().sendKeys(driver, ui.city, lead.getCity());
		}

		if (lead.getCountry() != null) {
			fc.utobj().selectDropDown(driver, ui.country, lead.getCountry());
		}

		if (lead.getStateProvince() != null) {
			fc.utobj().selectDropDown(driver, ui.stateID, lead.getStateProvince());
		}

		if (lead.getZipPostalCode() != null) {
			fc.utobj().sendKeys(driver, ui.zip, lead.getZipPostalCode());
		}

		if (lead.getCounty() != null) {
			fc.utobj().selectDropDown(driver, ui.countyID, lead.getCounty());
		}

		if (lead.getPreferredModeofContact() != null) {
			fc.utobj().selectDropDown(driver, ui.PreferredModeofContact, lead.getPreferredModeofContact());
		}

		if (lead.getBestTimeToContact() != null) {
			fc.utobj().sendKeys(driver, ui.bestTimeToContact, lead.getBestTimeToContact());
		}

		if (lead.getWorkPhone() != null) {
			fc.utobj().sendKeys(driver, ui.workPhone, lead.getWorkPhone());
		}

		if (lead.getWorkPhoneExtension() != null) {
			fc.utobj().sendKeys(driver, ui.workPhoneExt, lead.getWorkPhoneExtension());
		}

		if (lead.getHomePhone() != null) {
			fc.utobj().sendKeys(driver, ui.homePhone, lead.getHomePhone());
		}

		if (lead.getHomePhoneExtension() != null) {
			fc.utobj().sendKeys(driver, ui.homePhoneExt, lead.getHomePhoneExtension());
		}

		if (lead.getFax() != null) {
			fc.utobj().sendKeys(driver, ui.fax, lead.getFax());
		}

		if (lead.getMobile() != null) {
			fc.utobj().sendKeys(driver, ui.mobile, lead.getMobile());
		}

		if (lead.getEmail() != null) {
			fc.utobj().sendKeys(driver, ui.emailID, lead.getEmail());
		}

		if (lead.getCompanyName() != null) {
			fc.utobj().sendKeys(driver, ui.companyName, lead.getCompanyName());
		}

		if (lead.getComment() != null) {
			fc.utobj().sendKeys(driver, ui.comments, lead.getComment());
		}

		if (lead.getLeadOwner() != null) {
			fc.utobj().selectDropDown(driver, ui.leadOwnerID, lead.getLeadOwner());
		}

		if (lead.getBasedonAssignmentRules() != null) {
			if (lead.getBasedonAssignmentRules().equalsIgnoreCase("yes")) {
				fc.utobj().clickElement(driver, ui.basedonAssignmentRules);
			}
		}

		if (lead.getLeadRating() != null) {
			fc.utobj().selectDropDown(driver, ui.leadRatingID, lead.getLeadRating());
		}

		if (lead.getMarketingCode() != null) {
			fc.utobj().selectDropDown(driver, ui.marketingCodeId, lead.getMarketingCode());
		}

		if (lead.getLeadSourceCategory() != null) {
			fc.utobj().selectDropDown(driver, ui.leadSource2ID_Category, lead.getLeadSourceCategory());
		}

		if (lead.getLeadSourceDetails() != null) {
			fc.utobj().selectDropDown(driver, ui.leadSource3ID_Details, lead.getLeadSourceDetails());
		}

		if (lead.getOtherLeadSources() != null) {
			fc.utobj().sendKeys(driver, ui.otherLeadSourceDetail, lead.getOtherLeadSources());
		}

		if (lead.getCurrentNetWorth() != null) {
			fc.utobj().sendKeys(driver, ui.CurrentNetWorth, lead.getCurrentNetWorth());
		}

		if (lead.getCashAvailableforInvestment() != null) {
			fc.utobj().sendKeys(driver, ui.CashAvailableforInvestment, lead.getCashAvailableforInvestment());
		}

		if (lead.getInvestmentTimeframe() != null) {
			fc.utobj().sendKeys(driver, ui.investTimeframe, lead.getInvestmentTimeframe());
		}

		if (lead.getBackground() != null) {
			fc.utobj().sendKeys(driver, ui.background, lead.getBackground());
		}

		if (lead.getSourceOfInvestment() != null) {
			fc.utobj().sendKeys(driver, ui.sourceOfFunding, lead.getSourceOfInvestment());
		}

		if (lead.getNextCallDate() != null) {
			fc.utobj().sendKeys(driver, ui.nextCallDate, lead.getNextCallDate());
		}

		if (lead.getNoOfUnitsLocationsRequested() != null) {
			fc.utobj().sendKeys(driver, ui.noOfUnitReq, lead.getNoOfUnitsLocationsRequested());
		}

		if (lead.getDivision() != null) {
			fc.utobj().selectDropDown(driver, ui.brandMapping_0brandID, lead.getDivision());
		}

		if (lead.getPreferredCity1() != null) {
			fc.utobj().sendKeys(driver, ui.temppreferredCity1, lead.getPreferredCity1());
		}

		if (lead.getPreferredCountry1() != null) {
			fc.utobj().selectDropDown(driver, ui.temppreferredCountry1, lead.getPreferredCountry1());
		}

		if (lead.getPreferredStateProvince1() != null) {
			fc.utobj().selectDropDown(driver, ui.temppreferredStateId1, lead.getPreferredStateProvince1());
		}

		if (lead.getPreferredCity2() != null) {
			fc.utobj().sendKeys(driver, ui.temppreferredCity2, lead.getPreferredCity2());
		}

		if (lead.getPreferredCountry2() != null) {
			fc.utobj().selectDropDown(driver, ui.temppreferredCountry2, lead.getPreferredCountry2());
		}

		if (lead.getPreferredStateProvince2() != null) {
			fc.utobj().selectDropDown(driver, ui.temppreferredStateId2, lead.getPreferredStateProvince2());
		}

		if (lead.getNewAvailableSites() != null) {
			fc.utobj().selectDropDown(driver, ui.AvailableSites, lead.getNewAvailableSites());
		}

		if (lead.getExistingSites() != null) {
			fc.utobj().selectDropDown(driver, ui.ExistingSites, lead.getExistingSites());
		}

		if (lead.getResaleSites() != null) {
			fc.utobj().selectDropDown(driver, ui.ResaleSites, lead.getResaleSites());
		}

		if (lead.getForecastClosureDate() != null) {
			fc.utobj().sendKeys(driver, ui.forecastClosureDate, lead.getForecastClosureDate());
		}

		if (lead.getProbability() != null) {
			fc.utobj().sendKeys(driver, ui.probability, lead.getProbability());
		}

		if (lead.getForecastRating() != null) {
			fc.utobj().sendKeys(driver, ui.forecastRating, lead.getForecastRating());
		}

		if (lead.getForecastRevenue() != null) {
			fc.utobj().sendKeys(driver, ui.forecastRevenue, lead.getForecastRevenue());
		}

		if (lead.getCampaignName() != null) {
			if ("Based on Workflow Assignment Rules".equalsIgnoreCase(lead.getCampaignName())) {
				fc.utobj().clickElement(driver, ui.BasedonWorkflowAssignmentRules);
			} else {
				fc.utobj().clickElement(driver, ui.campaignName);
				fc.utobj().selectDropDown(driver, ui.campaignID, lead.getCampaignName());
			}
		}
		return lead;
	}

	Lead addCoApplicant(WebDriver driver, Lead lead, LeadAsCoApplicant leadAsCoapplicant) throws Exception {

		LeadUI ui = new LeadUI(driver);
		FranconnectUtil fc = new FranconnectUtil();

		if (lead.getSalutation() != null) {
			fc.utobj().selectDropDownByValue(driver, ui.Salutation, lead.getSalutation());
		}

		if (lead.getFirstName() != null) {
			fc.utobj().sendKeys(driver, ui.firstName, lead.getFirstName());
		}

		if (lead.getLastName() != null) {
			fc.utobj().sendKeys(driver, ui.lastName, lead.getLastName());
		}

		if (leadAsCoapplicant.getCoApplicantRelationship() != null) {
			fc.utobj().selectDropDown(driver, ui.CoApplicantRelationship,
					leadAsCoapplicant.getCoApplicantRelationship());
		}

		if (lead.getAddress1() != null) {
			fc.utobj().sendKeys(driver, ui.address1, lead.getAddress1());
		}

		if (lead.getAddress2() != null) {
			fc.utobj().sendKeys(driver, ui.address2, lead.getAddress2());
		}

		if (lead.getCity() != null) {
			fc.utobj().sendKeys(driver, ui.city, lead.getCity());
		}

		if (lead.getCountry() != null) {
			fc.utobj().selectDropDown(driver, ui.country, lead.getCountry());
		}

		if (lead.getStateProvince() != null) {
			fc.utobj().selectDropDown(driver, ui.stateID, lead.getStateProvince());
		}

		if (lead.getZipPostalCode() != null) {
			fc.utobj().sendKeys(driver, ui.zip, lead.getZipPostalCode());
		}

		if (lead.getCounty() != null) {
			fc.utobj().selectDropDown(driver, ui.countyID, lead.getCounty());
		}

		if (lead.getPreferredModeofContact() != null) {
			fc.utobj().selectDropDown(driver, ui.PreferredModeofContact, lead.getPreferredModeofContact());
		}

		if (lead.getBestTimeToContact() != null) {
			fc.utobj().sendKeys(driver, ui.bestTimeToContact, lead.getBestTimeToContact());
		}

		if (lead.getWorkPhone() != null) {
			fc.utobj().sendKeys(driver, ui.workPhone, lead.getWorkPhone());
		}

		if (lead.getWorkPhoneExtension() != null) {
			fc.utobj().sendKeys(driver, ui.workPhoneExt, lead.getWorkPhoneExtension());
		}

		if (lead.getHomePhone() != null) {
			fc.utobj().sendKeys(driver, ui.homePhone, lead.getHomePhone());
		}

		if (lead.getHomePhoneExtension() != null) {
			fc.utobj().sendKeys(driver, ui.homePhoneExt, lead.getHomePhoneExtension());
		}

		if (lead.getFax() != null) {
			fc.utobj().sendKeys(driver, ui.fax, lead.getFax());
		}

		if (lead.getMobile() != null) {
			fc.utobj().sendKeys(driver, ui.mobile, lead.getMobile());
		}

		if (lead.getEmail() != null) {
			fc.utobj().sendKeys(driver, ui.emailID, lead.getEmail());
		}

		if (lead.getCompanyName() != null) {
			fc.utobj().sendKeys(driver, ui.companyName, lead.getCompanyName());
		}

		if (lead.getComment() != null) {
			fc.utobj().sendKeys(driver, ui.comments, lead.getComment());
		}

		if (lead.getLeadOwner() != null) {
			fc.utobj().selectDropDown(driver, ui.leadOwnerID, lead.getLeadOwner());
		}

		if (lead.getBasedonAssignmentRules() != null) {
			if (lead.getBasedonAssignmentRules().equalsIgnoreCase("yes")) {
				fc.utobj().clickElement(driver, ui.basedonAssignmentRules);
			}
		}

		if (lead.getLeadRating() != null) {
			fc.utobj().selectDropDown(driver, ui.leadRatingID, lead.getLeadRating());
		}

		if (lead.getMarketingCode() != null) {
			fc.utobj().selectDropDown(driver, ui.marketingCodeId, lead.getMarketingCode());
		}

		if (lead.getLeadSourceCategory() != null) {
			fc.utobj().selectDropDown(driver, ui.leadSource2ID_Category, lead.getLeadSourceCategory());
		}

		if (lead.getLeadSourceDetails() != null) {
			fc.utobj().selectDropDown(driver, ui.leadSource3ID_Details, lead.getLeadSourceDetails());
		}

		if (lead.getOtherLeadSources() != null) {
			fc.utobj().sendKeys(driver, ui.otherLeadSourceDetail, lead.getOtherLeadSources());
		}

		if (lead.getCurrentNetWorth() != null) {
			fc.utobj().sendKeys(driver, ui.CurrentNetWorth, lead.getCurrentNetWorth());
		}

		if (lead.getCashAvailableforInvestment() != null) {
			fc.utobj().sendKeys(driver, ui.CashAvailableforInvestment, lead.getCashAvailableforInvestment());
		}

		if (lead.getInvestmentTimeframe() != null) {
			fc.utobj().sendKeys(driver, ui.investTimeframe, lead.getInvestmentTimeframe());
		}

		if (lead.getBackground() != null) {
			fc.utobj().sendKeys(driver, ui.background, lead.getBackground());
		}

		if (lead.getSourceOfInvestment() != null) {
			fc.utobj().sendKeys(driver, ui.sourceOfFunding, lead.getSourceOfInvestment());
		}

		if (lead.getNextCallDate() != null) {
			fc.utobj().sendKeys(driver, ui.nextCallDate, lead.getNextCallDate());
		}

		if (lead.getNoOfUnitsLocationsRequested() != null) {
			fc.utobj().sendKeys(driver, ui.noOfUnitReq, lead.getNoOfUnitsLocationsRequested());
		}

		if (lead.getDivision() != null) {
			fc.utobj().selectDropDown(driver, ui.brandMapping_0brandID, lead.getDivision());
		}

		if (lead.getPreferredCity1() != null) {
			fc.utobj().sendKeys(driver, ui.temppreferredCity1, lead.getPreferredCity1());
		}

		if (lead.getPreferredCountry1() != null) {
			fc.utobj().selectDropDown(driver, ui.temppreferredCountry1, lead.getPreferredCountry1());
		}

		if (lead.getPreferredStateProvince1() != null) {
			fc.utobj().selectDropDown(driver, ui.temppreferredStateId1, lead.getPreferredStateProvince1());
		}

		if (lead.getPreferredCity2() != null) {
			fc.utobj().sendKeys(driver, ui.temppreferredCity2, lead.getPreferredCity2());
		}

		if (lead.getPreferredCountry2() != null) {
			fc.utobj().selectDropDown(driver, ui.temppreferredCountry2, lead.getPreferredCountry2());
		}

		if (lead.getPreferredStateProvince2() != null) {
			fc.utobj().selectDropDown(driver, ui.temppreferredStateId2, lead.getPreferredStateProvince2());
		}

		if (lead.getNewAvailableSites() != null) {
			fc.utobj().selectDropDown(driver, ui.AvailableSites, lead.getNewAvailableSites());
		}

		if (lead.getExistingSites() != null) {
			fc.utobj().selectDropDown(driver, ui.ExistingSites, lead.getExistingSites());
		}

		if (lead.getResaleSites() != null) {
			fc.utobj().selectDropDown(driver, ui.ResaleSites, lead.getResaleSites());
		}

		if (lead.getForecastClosureDate() != null) {
			fc.utobj().sendKeys(driver, ui.forecastClosureDate, lead.getForecastClosureDate());
		}

		if (lead.getProbability() != null) {
			fc.utobj().sendKeys(driver, ui.probability, lead.getProbability());
		}

		if (lead.getForecastRating() != null) {
			fc.utobj().sendKeys(driver, ui.forecastRating, lead.getForecastRating());
		}

		if (lead.getForecastRevenue() != null) {
			fc.utobj().sendKeys(driver, ui.forecastRevenue, lead.getForecastRevenue());
		}

		if (lead.getCampaignName() != null) {
			fc.utobj().printTestStep("Select Campaign : " + lead.getCampaignName());
			if ("Based on Workflow Assignment Rules".equalsIgnoreCase(lead.getCampaignName())) {
				fc.utobj().clickElement(driver, ui.BasedonWorkflowAssignmentRules);
			} else {
				fc.utobj().clickElement(driver, ui.campaignName);
				fc.utobj().selectDropDown(driver, ui.campaignID, lead.getCampaignName());
			}
		}

		return lead;
	}

	Lead modifyLead(WebDriver driver, Lead lead) throws Exception {
		LeadUI ui = new LeadUI(driver);

		Lead ld = new Lead();

		if (ui.firstName.getAttribute("value") != null) {
			ld.setFirstName(ui.firstName.getAttribute("value"));
		}

		if (ui.lastName.getAttribute("value") != null) {
			ld.setLastName(ui.lastName.getAttribute("value"));
		}

		if (ui.address1.getAttribute("value") != null) {
			ld.setAddress1(ui.address1.getAttribute("value"));
		}

		if (ui.address2.getAttribute("value") != null) {
			ld.setAddress2(ui.address2.getAttribute("null"));
		}

		if (ui.city.getAttribute("value") != null) {
			ld.setCity(ui.city.getAttribute("value"));
		}

		if (ui.country.getAttribute("value") != null) {
			ld.setCountry(ui.country.getAttribute("value"));
		}

		if (ui.stateID.getAttribute("value") != null) {
			ld.setStateProvince(ui.stateID.getAttribute("value"));
		}

		if (ui.zip.getAttribute("value") != null) {
			ld.setZipPostalCode(ui.zip.getAttribute("value"));
		}

		if (ui.countyID.getAttribute("value") != null) {
			ld.setCounty(ui.countyID.getAttribute("value"));
		}

		if (ui.PreferredModeofContact.getAttribute("value") != null) {
			ld.setPreferredModeofContact(ui.PreferredModeofContact.getAttribute("value"));
		}

		if (ui.bestTimeToContact.getAttribute("value") != null) {
			ld.setBestTimeToContact(ui.bestTimeToContact.getAttribute("value"));
		}

		if (ui.workPhone.getAttribute("value") != null) {
			ld.setHomePhone(ui.workPhone.getAttribute("value"));
		}

		if (ui.workPhoneExt.getAttribute("value") != null) {
			ld.setWorkPhoneExtension(ui.workPhoneExt.getAttribute("value"));
		}

		if (ui.homePhone.getAttribute("value") != null) {
			ld.setHomePhone(ui.homePhone.getAttribute("value"));
		}

		if (ui.homePhoneExt.getAttribute("value") != null) {
			ld.setHomePhoneExtension(ui.homePhoneExt.getAttribute("null"));
		}

		if (ui.fax.getAttribute("value") != null) {
			ld.setFax(ui.fax.getAttribute("value"));
		}

		if (ui.mobile.getAttribute("value") != null) {
			ld.setMobile(ui.mobile.getAttribute("value"));
		}

		if (ui.emailID.getAttribute("value") != null) {
			ld.setEmail(ui.emailID.getAttribute("value"));
		}

		if (ui.companyName.getAttribute("value") != null) {
			ld.setCompanyName(ui.companyName.getAttribute("value"));
		}

		if (ui.comments.getAttribute("value") != null) {
			ld.setComment(ui.comments.getAttribute("value"));
		}

		if (ui.leadOwnerID.getAttribute("value") != null) {
			ld.setLeadOwner(ui.leadOwnerID.getAttribute("value"));
		}

		if (ui.leadRatingID.getAttribute("value") != null) {
			ld.setLeadRating(ui.leadRatingID.getAttribute("value"));
		}

		if (ui.marketingCodeId.getAttribute("value") != null) {
			ld.setMarketingCode(ui.marketingCodeId.getAttribute("value"));
		}

		if (ui.leadSource2ID_Category.getAttribute("value") != null) {
			ld.setLeadSourceCategory(ui.leadSource2ID_Category.getAttribute("value"));
		}

		if (ui.leadSource3ID_Details.getAttribute("value") != null) {
			ld.setLeadSourceDetails(ui.leadSource3ID_Details.getAttribute("value"));
		}

		if (ui.otherLeadSourceDetail.getAttribute("value") != null) {
			ld.setOtherLeadSources(ui.otherLeadSourceDetail.getAttribute("value"));
		}

		if (ui.CurrentNetWorth.getAttribute("value") != null) {
			ld.setCurrentNetWorth(ui.CurrentNetWorth.getAttribute("value"));
		}

		if (ui.CashAvailableforInvestment.getAttribute("value") != null) {
			ld.setCashAvailableforInvestment(ui.CashAvailableforInvestment.getAttribute("value"));
		}

		if (ui.investTimeframe.getAttribute("value") != null) {
			ld.setInvestmentTimeframe(ui.investTimeframe.getAttribute("value"));
		}

		if (ui.background.getAttribute("value") != null) {
			ld.setBackground(ui.background.getAttribute("value"));
		}

		if (ui.sourceOfFunding.getAttribute("value") != null) {
			ld.setSourceOfInvestment(ui.sourceOfFunding.getAttribute("value"));
		}

		if (ui.nextCallDate.getAttribute("value") != null) {
			ld.setNextCallDate(ui.nextCallDate.getAttribute("value"));
		}

		if (ui.noOfUnitReq.getAttribute("value") != null) {
			ld.setNoOfUnitsLocationsRequested(ui.noOfUnitReq.getAttribute("value"));
		}

		if (ui.brandMapping_0brandID.getAttribute("value") != null) {
			ld.setDivision(ui.brandMapping_0brandID.getAttribute("value"));
		}

		if (ui.temppreferredCity1.getAttribute("value") != null) {
			ld.setPreferredCity1(ui.temppreferredCity1.getAttribute("value"));
		}

		if (ui.temppreferredCountry1.getAttribute("value") != null) {
			ld.setPreferredCountry1(ui.temppreferredCountry1.getAttribute("value"));
		}

		if (ui.temppreferredStateId1.getAttribute("value") != null) {
			ld.setPreferredStateProvince1(ui.temppreferredStateId1.getAttribute("value"));
		}

		if (ui.temppreferredCity2.getAttribute("value") != null) {
			ld.setPreferredCity2(ui.temppreferredCity2.getAttribute("value"));
		}

		if (ui.temppreferredCity2.getAttribute("value") != null) {
			ld.setPreferredCity2(ui.temppreferredCity2.getAttribute("value"));
		}

		if (ui.temppreferredCountry2.getAttribute("value") != null) {
			ld.setPreferredCountry2(ui.temppreferredCountry2.getAttribute("value"));
		}

		if (ui.temppreferredStateId2.getAttribute("value") != null) {
			ld.setPreferredStateProvince2(ui.temppreferredStateId2.getAttribute("value"));
		}

		if (ui.AvailableSites.getAttribute("value") != null) {
			ld.setNewAvailableSites(ui.AvailableSites.getAttribute("value"));
		}

		if (ui.ExistingSites.getAttribute("value") != null) {
			ld.setExistingSites(ui.ExistingSites.getAttribute("value"));
		}

		if (ui.ResaleSites.getAttribute("value") != null) {
			ld.setResaleSites(ui.ResaleSites.getAttribute("value"));
		}

		if (ui.forecastClosureDate.getAttribute("value") != null) {
			ld.setForecastClosureDate(ui.forecastClosureDate.getAttribute("value"));
		}

		if (ui.probability.getAttribute("value") != null) {
			ld.setProbability(ui.probability.getAttribute("value"));
		}

		if (ui.forecastRating.getAttribute("value") != null) {
			ld.setForecastRating(ui.forecastRating.getAttribute("value"));
		}

		if (ui.forecastRevenue.getAttribute("value") != null) {
			ld.setForecastRevenue(ui.forecastRevenue.getAttribute("value"));
		}

		if (ui.campaignName.getAttribute("value") != null) {
			ld.setCampaignName(ui.campaignName.getAttribute("value"));
		}

		return ld;
	}

}
