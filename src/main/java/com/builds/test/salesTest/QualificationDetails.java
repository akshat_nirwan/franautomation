package com.builds.test.salesTest;

class QualificationDetails {

	// Personal Information
	private String name;
	private String gender;
	private String presentAddress;
	private String yearsAtThisAddress;
	private String City;
	private String Country;
	private String stateProvince;
	private String zipPostalCode;
	private String workPhone;
	private String workPhoneExt;
	private String homePhone;
	private String homePhoneExt;
	private String email;
	private String usCitizen;
	private String socialSecurity;
	private String previousAddress;
	private String previousCity;
	private String previousCountry;
	private String previousStateProvince;
	private String previousZipPostalCode;
	private String birthDate;
	private String birthMonth;
	private String bestTimeToCall;
	private String homeOwnership;
	private String maritalStatus;
	private String spouseName;
	private String spouseSocialSecurity;
	private String spouseUSCitizen;
	private String spouseBirthDate;
	private String spouseBirthMonth;

	// Assets & Liabilities
	private String cashOnHandInBanks;
	private String mortgages;
	private String marketableSecurities;
	private String accountsPayable;
	private String accountsNotesReceivable;
	private String notesPayable;
	private String retirementPlans;
	private String loansOnLifeInsurance;
	private String realEstate;
	private String creditCardsTotalBalance;
	private String personalProperty;
	private String unpaidTaxes;
	private String businessHoldings;
	private String lifeInsurance;
	private String otherAssets;
	private String otherLiabilities;
	private String descriptionLiabilities;
	private String descriptionAssests;
	private String totalAssets;
	private String totalLiabilities;
	private String totalNetWorth;

	// Real Estate Owned
	private String realEstateAddress1;
	private String realEstateDatePurchased1;
	private String realEstateOriginalCost1;
	private String realEstatePresentValue1;
	private String realEstateMortgageBalance1;
	private String realEstateAddress2;
	private String realEstateDatePurchased2;
	private String realEstateOriginalCost2;
	private String realEstatePresentValue2;
	private String realEstateMortgageBalance2;
	private String RealEstateAddress3;
	private String realEstateDatePurchased3;
	private String realEstateOriginalCost3;
	private String realEstatePresentValue3;
	private String realEstateMortgageBalance3;

	// Annual Sources Of Income
	private String salary;
	private String investment;
	private String realEstateIncome;
	private String other;
	private String description;
	private String totalAnnualSource;

	// Total Contingent Liabilities
	private String loanCoSignature;
	private String legalJudgement;
	private String incomeTaxes;
	private String otherSpecialDebt;
	private String totalContingent;

	// Specific Data
	private String whenReadyIfApproved;
	private String skillsExperience;
	private String enableReachGoals;
	private String responsibleForDailyOperations;
	private String cashAvailableForInvestment;
	private String approvedForFinancing;
	private String amountApprovedForFinance;
	private String soleIncomeSource;
	private String contingentliabilites;
	private String lawsuit;
	private String convicted;
	private String convictedOfFelony;
	private String explainConviction;
	private String filedBankruptcy;
	private String dateFiledBankruptcy;
	private String dateDischargedBankruptcy;
	private String locationPreference1;
	private String locationPreference2;
	private String locationPreference3;
	private String businessQuestion1;
	private String businessQuestion2;
	private String businessQuestion3;

	// Internal Analysis of Applicant
	private String firstNameApplicant;
	private String lastNameApplicant;
	private String addressApplicant;
	private String cityApplicant;
	private String workPhoneApplicant;
	private String countryApplicant;
	private String stateProvinceApplicant;
	private String workPhoneExtApplicant;
	private String calledAtOffice;

	// Heat Index Components
	private String currentNetWorth_liquidCapitalMin;
	private String cashAvailableForInvestment_liquidCapitalMax;
	private String investmentTimeframe;
	private String employmentBackground;
	private String backgroundCheckApproval;

	// Qualification Checklist
	private String lendableNetWorth_value;
	private String lendableNetWorth_completed;
	private String lendableNetWorth_completionDate;
	private String lendableNetWorth_associatedDocument;
	private String lendableNetWorth_verifiedBy;
	private String lendableNetWorth_date;

	private String backgroundandCriminalCheck_value;
	private String backgroundandCriminalCheck_completed;
	private String backgroundandCriminalCheck_CompletionDate;
	private String backgroundandCriminalCheck_associatedDocument;
	private String backgroundandCriminalCheck_VerifiedBy;
	private String backgroundandCriminalCheck_Date;

	private String creditCheck_value;
	private String creditCheck_completed;
	private String creditCheck_CompletionDate;
	private String creditCheck_associatedDocument;
	private String creditCheck_VerifiedBy;
	private String creditCheck_Date;

	private String territoryApproved_value;
	private String territoryApproved_completed;
	private String territoryApproved_CompletionDate;
	private String territoryApproved_associatedDocument;
	private String territoryApproved_VerifiedBy;
	private String territoryApproved_Date;

	private String franchiseAgreementonFile_value;
	private String franchiseAgreementonFile_completed;
	private String franchiseAgreementonFile_CompletionDate;
	private String franchiseAgreementonFile_associatedDocument;
	private String franchiseAgreementonFile_VerifiedBy;
	private String franchiseAgreementonFile_Date;

	private String fddReceiptonFile_value;
	private String fddReceiptonFile_completed;
	private String fddReceiptonFile_CompletionDate;
	private String fddReceiptonFile_associatedDocument;
	private String fddReceiptonFile_VerifiedBy;
	private String fddReceiptonFile_Date;

	private String opener_value;
	private String opener_completed;
	private String opener_CompletionDate;
	private String opener_associatedDocument;
	private String opener_VerifiedBy;
	private String opener_Date;

	private String date;

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getPresentAddress() {
		return presentAddress;
	}

	public void setPresentAddress(String presentAddress) {
		this.presentAddress = presentAddress;
	}

	public String getYearsAtThisAddress() {
		return yearsAtThisAddress;
	}

	public void setYearsAtThisAddress(String yearsAtThisAddress) {
		this.yearsAtThisAddress = yearsAtThisAddress;
	}

	public String getCity() {
		return City;
	}

	public void setCity(String city) {
		City = city;
	}

	public String getCountry() {
		return Country;
	}

	public void setCountry(String country) {
		Country = country;
	}

	public String getStateProvince() {
		return stateProvince;
	}

	public void setStateProvince(String stateProvince) {
		this.stateProvince = stateProvince;
	}

	public String getZipPostalCode() {
		return zipPostalCode;
	}

	public void setZipPostalCode(String zipPostalCode) {
		this.zipPostalCode = zipPostalCode;
	}

	public String getWorkPhone() {
		return workPhone;
	}

	public void setWorkPhone(String workPhone) {
		this.workPhone = workPhone;
	}

	public String getWorkPhoneExt() {
		return workPhoneExt;
	}

	public void setWorkPhoneExt(String workPhoneExt) {
		this.workPhoneExt = workPhoneExt;
	}

	public String getHomePhone() {
		return homePhone;
	}

	public void setHomePhone(String homePhone) {
		this.homePhone = homePhone;
	}

	public String getHomePhoneExt() {
		return homePhoneExt;
	}

	public void setHomePhoneExt(String homePhoneExt) {
		this.homePhoneExt = homePhoneExt;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsCitizen() {
		return usCitizen;
	}

	public void setUsCitizen(String usCitizen) {
		this.usCitizen = usCitizen;
	}

	public String getSocialSecurity() {
		return socialSecurity;
	}

	public void setSocialSecurity(String socialSecurity) {
		this.socialSecurity = socialSecurity;
	}

	public String getPreviousAddress() {
		return previousAddress;
	}

	public void setPreviousAddress(String previousAddress) {
		this.previousAddress = previousAddress;
	}

	public String getPreviousCity() {
		return previousCity;
	}

	public void setPreviousCity(String previousCity) {
		this.previousCity = previousCity;
	}

	public String getPreviousCountry() {
		return previousCountry;
	}

	public void setPreviousCountry(String previousCountry) {
		this.previousCountry = previousCountry;
	}

	public String getPreviousStateProvince() {
		return previousStateProvince;
	}

	public void setPreviousStateProvince(String previousStateProvince) {
		this.previousStateProvince = previousStateProvince;
	}

	public String getPreviousZipPostalCode() {
		return previousZipPostalCode;
	}

	public void setPreviousZipPostalCode(String previousZipPostalCode) {
		this.previousZipPostalCode = previousZipPostalCode;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	public String getBirthMonth() {
		return birthMonth;
	}

	public void setBirthMonth(String birthMonth) {
		this.birthMonth = birthMonth;
	}

	public String getBestTimeToCall() {
		return bestTimeToCall;
	}

	public void setBestTimeToCall(String bestTimeToCall) {
		this.bestTimeToCall = bestTimeToCall;
	}

	public String getHomeOwnership() {
		return homeOwnership;
	}

	public void setHomeOwnership(String homeOwnership) {
		this.homeOwnership = homeOwnership;
	}

	public String getMaritalStatus() {
		return maritalStatus;
	}

	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public String getSpouseName() {
		return spouseName;
	}

	public void setSpouseName(String spouseName) {
		this.spouseName = spouseName;
	}

	public String getSpouseSocialSecurity() {
		return spouseSocialSecurity;
	}

	public void setSpouseSocialSecurity(String spouseSocialSecurity) {
		this.spouseSocialSecurity = spouseSocialSecurity;
	}

	public String getSpouseUSCitizen() {
		return spouseUSCitizen;
	}

	public void setSpouseUSCitizen(String spouseUSCitizen) {
		this.spouseUSCitizen = spouseUSCitizen;
	}

	public String getSpouseBirthDate() {
		return spouseBirthDate;
	}

	public void setSpouseBirthDate(String spouseBirthDate) {
		this.spouseBirthDate = spouseBirthDate;
	}

	public String getSpouseBirthMonth() {
		return spouseBirthMonth;
	}

	public void setSpouseBirthMonth(String spouseBirthMonth) {
		this.spouseBirthMonth = spouseBirthMonth;
	}

	public String getCashOnHandInBanks() {
		return cashOnHandInBanks;
	}

	public void setCashOnHandInBanks(String cashOnHandInBanks) {
		this.cashOnHandInBanks = cashOnHandInBanks;
	}

	public String getMortgages() {
		return mortgages;
	}

	public void setMortgages(String mortgages) {
		this.mortgages = mortgages;
	}

	public String getMarketableSecurities() {
		return marketableSecurities;
	}

	public void setMarketableSecurities(String marketableSecurities) {
		this.marketableSecurities = marketableSecurities;
	}

	public String getAccountsPayable() {
		return accountsPayable;
	}

	public void setAccountsPayable(String accountsPayable) {
		this.accountsPayable = accountsPayable;
	}

	public String getAccountsNotesReceivable() {
		return accountsNotesReceivable;
	}

	public void setAccountsNotesReceivable(String accountsNotesReceivable) {
		this.accountsNotesReceivable = accountsNotesReceivable;
	}

	public String getNotesPayable() {
		return notesPayable;
	}

	public void setNotesPayable(String notesPayable) {
		this.notesPayable = notesPayable;
	}

	public String getRetirementPlans() {
		return retirementPlans;
	}

	public void setRetirementPlans(String retirementPlans) {
		this.retirementPlans = retirementPlans;
	}

	public String getLoansOnLifeInsurance() {
		return loansOnLifeInsurance;
	}

	public void setLoansOnLifeInsurance(String loansOnLifeInsurance) {
		this.loansOnLifeInsurance = loansOnLifeInsurance;
	}

	public String getRealEstate() {
		return realEstate;
	}

	public void setRealEstate(String realEstate) {
		this.realEstate = realEstate;
	}

	public String getCreditCardsTotalBalance() {
		return creditCardsTotalBalance;
	}

	public void setCreditCardsTotalBalance(String creditCardsTotalBalance) {
		this.creditCardsTotalBalance = creditCardsTotalBalance;
	}

	public String getPersonalProperty() {
		return personalProperty;
	}

	public void setPersonalProperty(String personalProperty) {
		this.personalProperty = personalProperty;
	}

	public String getUnpaidTaxes() {
		return unpaidTaxes;
	}

	public void setUnpaidTaxes(String unpaidTaxes) {
		this.unpaidTaxes = unpaidTaxes;
	}

	public String getBusinessHoldings() {
		return businessHoldings;
	}

	public void setBusinessHoldings(String businessHoldings) {
		this.businessHoldings = businessHoldings;
	}

	public String getLifeInsurance() {
		return lifeInsurance;
	}

	public void setLifeInsurance(String lifeInsurance) {
		this.lifeInsurance = lifeInsurance;
	}

	public String getOtherAssets() {
		return otherAssets;
	}

	public void setOtherAssets(String otherAssets) {
		this.otherAssets = otherAssets;
	}

	public String getOtherLiabilities() {
		return otherLiabilities;
	}

	public void setOtherLiabilities(String otherLiabilities) {
		this.otherLiabilities = otherLiabilities;
	}

	public String getDescriptionLiabilities() {
		return descriptionLiabilities;
	}

	public void setDescriptionLiabilities(String descriptionLiabilities) {
		this.descriptionLiabilities = descriptionLiabilities;
	}

	public String getDescriptionAssests() {
		return descriptionAssests;
	}

	public void setDescriptionAssests(String descriptionAssests) {
		this.descriptionAssests = descriptionAssests;
	}

	public String getTotalAssets() {
		return totalAssets;
	}

	public void setTotalAssets(String totalAssets) {
		this.totalAssets = totalAssets;
	}

	public String getTotalLiabilities() {
		return totalLiabilities;
	}

	public void setTotalLiabilities(String totalLiabilities) {
		this.totalLiabilities = totalLiabilities;
	}

	public String getTotalNetWorth() {
		return totalNetWorth;
	}

	public void setTotalNetWorth(String totalNetWorth) {
		this.totalNetWorth = totalNetWorth;
	}

	public String getRealEstateAddress1() {
		return realEstateAddress1;
	}

	public void setRealEstateAddress1(String realEstateAddress1) {
		this.realEstateAddress1 = realEstateAddress1;
	}

	public String getRealEstateDatePurchased1() {
		return realEstateDatePurchased1;
	}

	public void setRealEstateDatePurchased1(String realEstateDatePurchased1) {
		this.realEstateDatePurchased1 = realEstateDatePurchased1;
	}

	public String getRealEstateOriginalCost1() {
		return realEstateOriginalCost1;
	}

	public void setRealEstateOriginalCost1(String realEstateOriginalCost1) {
		this.realEstateOriginalCost1 = realEstateOriginalCost1;
	}

	public String getRealEstatePresentValue1() {
		return realEstatePresentValue1;
	}

	public void setRealEstatePresentValue1(String realEstatePresentValue1) {
		this.realEstatePresentValue1 = realEstatePresentValue1;
	}

	public String getRealEstateMortgageBalance1() {
		return realEstateMortgageBalance1;
	}

	public void setRealEstateMortgageBalance1(String realEstateMortgageBalance1) {
		this.realEstateMortgageBalance1 = realEstateMortgageBalance1;
	}

	public String getRealEstateAddress2() {
		return realEstateAddress2;
	}

	public void setRealEstateAddress2(String realEstateAddress2) {
		this.realEstateAddress2 = realEstateAddress2;
	}

	public String getRealEstateDatePurchased2() {
		return realEstateDatePurchased2;
	}

	public void setRealEstateDatePurchased2(String realEstateDatePurchased2) {
		this.realEstateDatePurchased2 = realEstateDatePurchased2;
	}

	public String getRealEstateOriginalCost2() {
		return realEstateOriginalCost2;
	}

	public void setRealEstateOriginalCost2(String realEstateOriginalCost2) {
		this.realEstateOriginalCost2 = realEstateOriginalCost2;
	}

	public String getRealEstatePresentValue2() {
		return realEstatePresentValue2;
	}

	public void setRealEstatePresentValue2(String realEstatePresentValue2) {
		this.realEstatePresentValue2 = realEstatePresentValue2;
	}

	public String getRealEstateMortgageBalance2() {
		return realEstateMortgageBalance2;
	}

	public void setRealEstateMortgageBalance2(String realEstateMortgageBalance2) {
		this.realEstateMortgageBalance2 = realEstateMortgageBalance2;
	}

	public String getRealEstateAddress3() {
		return RealEstateAddress3;
	}

	public void setRealEstateAddress3(String realEstateAddress3) {
		RealEstateAddress3 = realEstateAddress3;
	}

	public String getRealEstateDatePurchased3() {
		return realEstateDatePurchased3;
	}

	public void setRealEstateDatePurchased3(String realEstateDatePurchased3) {
		this.realEstateDatePurchased3 = realEstateDatePurchased3;
	}

	public String getRealEstateOriginalCost3() {
		return realEstateOriginalCost3;
	}

	public void setRealEstateOriginalCost3(String realEstateOriginalCost3) {
		this.realEstateOriginalCost3 = realEstateOriginalCost3;
	}

	public String getRealEstatePresentValue3() {
		return realEstatePresentValue3;
	}

	public void setRealEstatePresentValue3(String realEstatePresentValue3) {
		this.realEstatePresentValue3 = realEstatePresentValue3;
	}

	public String getRealEstateMortgageBalance3() {
		return realEstateMortgageBalance3;
	}

	public void setRealEstateMortgageBalance3(String realEstateMortgageBalance3) {
		this.realEstateMortgageBalance3 = realEstateMortgageBalance3;
	}

	public String getSalary() {
		return salary;
	}

	public void setSalary(String salary) {
		this.salary = salary;
	}

	public String getInvestment() {
		return investment;
	}

	public void setInvestment(String investment) {
		this.investment = investment;
	}

	public String getRealEstateIncome() {
		return realEstateIncome;
	}

	public void setRealEstateIncome(String realEstateIncome) {
		this.realEstateIncome = realEstateIncome;
	}

	public String getOther() {
		return other;
	}

	public void setOther(String other) {
		this.other = other;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTotalAnnualSource() {
		return totalAnnualSource;
	}

	public void setTotalAnnualSource(String totalAnnualSource) {
		this.totalAnnualSource = totalAnnualSource;
	}

	public String getLoanCoSignature() {
		return loanCoSignature;
	}

	public void setLoanCoSignature(String loanCoSignature) {
		this.loanCoSignature = loanCoSignature;
	}

	public String getLegalJudgement() {
		return legalJudgement;
	}

	public void setLegalJudgement(String legalJudgement) {
		this.legalJudgement = legalJudgement;
	}

	public String getIncomeTaxes() {
		return incomeTaxes;
	}

	public void setIncomeTaxes(String incomeTaxes) {
		this.incomeTaxes = incomeTaxes;
	}

	public String getOtherSpecialDebt() {
		return otherSpecialDebt;
	}

	public void setOtherSpecialDebt(String otherSpecialDebt) {
		this.otherSpecialDebt = otherSpecialDebt;
	}

	public String getTotalContingent() {
		return totalContingent;
	}

	public void setTotalContingent(String totalContingent) {
		this.totalContingent = totalContingent;
	}

	public String getWhenReadyIfApproved() {
		return whenReadyIfApproved;
	}

	public void setWhenReadyIfApproved(String whenReadyIfApproved) {
		this.whenReadyIfApproved = whenReadyIfApproved;
	}

	public String getSkillsExperience() {
		return skillsExperience;
	}

	public void setSkillsExperience(String skillsExperience) {
		this.skillsExperience = skillsExperience;
	}

	public String getEnableReachGoals() {
		return enableReachGoals;
	}

	public void setEnableReachGoals(String enableReachGoals) {
		this.enableReachGoals = enableReachGoals;
	}

	public String getResponsibleForDailyOperations() {
		return responsibleForDailyOperations;
	}

	public void setResponsibleForDailyOperations(String responsibleForDailyOperations) {
		this.responsibleForDailyOperations = responsibleForDailyOperations;
	}

	public String getCashAvailableForInvestment() {
		return cashAvailableForInvestment;
	}

	public void setCashAvailableForInvestment(String cashAvailableForInvestment) {
		this.cashAvailableForInvestment = cashAvailableForInvestment;
	}

	public String getApprovedForFinancing() {
		return approvedForFinancing;
	}

	public void setApprovedForFinancing(String approvedForFinancing) {
		this.approvedForFinancing = approvedForFinancing;
	}

	public String getAmountApprovedForFinance() {
		return amountApprovedForFinance;
	}

	public void setAmountApprovedForFinance(String amountApprovedForFinance) {
		this.amountApprovedForFinance = amountApprovedForFinance;
	}

	public String getSoleIncomeSource() {
		return soleIncomeSource;
	}

	public void setSoleIncomeSource(String soleIncomeSource) {
		this.soleIncomeSource = soleIncomeSource;
	}

	public String getContingentliabilites() {
		return contingentliabilites;
	}

	public void setContingentliabilites(String contingentliabilites) {
		this.contingentliabilites = contingentliabilites;
	}

	public String getLawsuit() {
		return lawsuit;
	}

	public void setLawsuit(String lawsuit) {
		this.lawsuit = lawsuit;
	}

	public String getConvicted() {
		return convicted;
	}

	public void setConvicted(String convicted) {
		this.convicted = convicted;
	}

	public String getConvictedOfFelony() {
		return convictedOfFelony;
	}

	public void setConvictedOfFelony(String convictedOfFelony) {
		this.convictedOfFelony = convictedOfFelony;
	}

	public String getExplainConviction() {
		return explainConviction;
	}

	public void setExplainConviction(String explainConviction) {
		this.explainConviction = explainConviction;
	}

	public String getFiledBankruptcy() {
		return filedBankruptcy;
	}

	public void setFiledBankruptcy(String filedBankruptcy) {
		this.filedBankruptcy = filedBankruptcy;
	}

	public String getDateFiledBankruptcy() {
		return dateFiledBankruptcy;
	}

	public void setDateFiledBankruptcy(String dateFiledBankruptcy) {
		this.dateFiledBankruptcy = dateFiledBankruptcy;
	}

	public String getDateDischargedBankruptcy() {
		return dateDischargedBankruptcy;
	}

	public void setDateDischargedBankruptcy(String dateDischargedBankruptcy) {
		this.dateDischargedBankruptcy = dateDischargedBankruptcy;
	}

	public String getLocationPreference1() {
		return locationPreference1;
	}

	public void setLocationPreference1(String locationPreference1) {
		this.locationPreference1 = locationPreference1;
	}

	public String getLocationPreference2() {
		return locationPreference2;
	}

	public void setLocationPreference2(String locationPreference2) {
		this.locationPreference2 = locationPreference2;
	}

	public String getLocationPreference3() {
		return locationPreference3;
	}

	public void setLocationPreference3(String locationPreference3) {
		this.locationPreference3 = locationPreference3;
	}

	public String getBusinessQuestion1() {
		return businessQuestion1;
	}

	public void setBusinessQuestion1(String businessQuestion1) {
		this.businessQuestion1 = businessQuestion1;
	}

	public String getBusinessQuestion2() {
		return businessQuestion2;
	}

	public void setBusinessQuestion2(String businessQuestion2) {
		this.businessQuestion2 = businessQuestion2;
	}

	public String getBusinessQuestion3() {
		return businessQuestion3;
	}

	public void setBusinessQuestion3(String businessQuestion3) {
		this.businessQuestion3 = businessQuestion3;
	}

	public String getFirstNameApplicant() {
		return firstNameApplicant;
	}

	public void setFirstNameApplicant(String firstNameApplicant) {
		this.firstNameApplicant = firstNameApplicant;
	}

	public String getLastNameApplicant() {
		return lastNameApplicant;
	}

	public void setLastNameApplicant(String lastNameApplicant) {
		this.lastNameApplicant = lastNameApplicant;
	}

	public String getAddressApplicant() {
		return addressApplicant;
	}

	public void setAddressApplicant(String addressApplicant) {
		this.addressApplicant = addressApplicant;
	}

	public String getCityApplicant() {
		return cityApplicant;
	}

	public void setCityApplicant(String cityApplicant) {
		this.cityApplicant = cityApplicant;
	}

	public String getWorkPhoneApplicant() {
		return workPhoneApplicant;
	}

	public void setWorkPhoneApplicant(String workPhoneApplicant) {
		this.workPhoneApplicant = workPhoneApplicant;
	}

	public String getCountryApplicant() {
		return countryApplicant;
	}

	public void setCountryApplicant(String countryApplicant) {
		this.countryApplicant = countryApplicant;
	}

	public String getStateProvinceApplicant() {
		return stateProvinceApplicant;
	}

	public void setStateProvinceApplicant(String stateProvinceApplicant) {
		this.stateProvinceApplicant = stateProvinceApplicant;
	}

	public String getWorkPhoneExtApplicant() {
		return workPhoneExtApplicant;
	}

	public void setWorkPhoneExtApplicant(String workPhoneExtApplicant) {
		this.workPhoneExtApplicant = workPhoneExtApplicant;
	}

	public String getCalledAtOffice() {
		return calledAtOffice;
	}

	public void setCalledAtOffice(String calledAtOffice) {
		this.calledAtOffice = calledAtOffice;
	}

	public String getCurrentNetWorth_liquidCapitalMin() {
		return currentNetWorth_liquidCapitalMin;
	}

	public void setCurrentNetWorth_liquidCapitalMin(String currentNetWorth_liquidCapitalMin) {
		this.currentNetWorth_liquidCapitalMin = currentNetWorth_liquidCapitalMin;
	}

	public String getCashAvailableForInvestment_liquidCapitalMax() {
		return cashAvailableForInvestment_liquidCapitalMax;
	}

	public void setCashAvailableForInvestment_liquidCapitalMax(String cashAvailableForInvestment_liquidCapitalMax) {
		this.cashAvailableForInvestment_liquidCapitalMax = cashAvailableForInvestment_liquidCapitalMax;
	}

	public String getInvestmentTimeframe() {
		return investmentTimeframe;
	}

	public void setInvestmentTimeframe(String investmentTimeframe) {
		this.investmentTimeframe = investmentTimeframe;
	}

	public String getEmploymentBackground() {
		return employmentBackground;
	}

	public void setEmploymentBackground(String employmentBackground) {
		this.employmentBackground = employmentBackground;
	}

	public String getBackgroundCheckApproval() {
		return backgroundCheckApproval;
	}

	public void setBackgroundCheckApproval(String backgroundCheckApproval) {
		this.backgroundCheckApproval = backgroundCheckApproval;
	}

	public String getLendableNetWorth_value() {
		return lendableNetWorth_value;
	}

	public void setLendableNetWorth_value(String lendableNetWorth_value) {
		this.lendableNetWorth_value = lendableNetWorth_value;
	}

	public String getLendableNetWorth_completed() {
		return lendableNetWorth_completed;
	}

	public void setLendableNetWorth_completed(String lendableNetWorth_completed) {
		this.lendableNetWorth_completed = lendableNetWorth_completed;
	}

	public String getLendableNetWorth_completionDate() {
		return lendableNetWorth_completionDate;
	}

	public void setLendableNetWorth_completionDate(String lendableNetWorth_completionDate) {
		this.lendableNetWorth_completionDate = lendableNetWorth_completionDate;
	}

	public String getLendableNetWorth_associatedDocument() {
		return lendableNetWorth_associatedDocument;
	}

	public void setLendableNetWorth_associatedDocument(String lendableNetWorth_associatedDocument) {
		this.lendableNetWorth_associatedDocument = lendableNetWorth_associatedDocument;
	}

	public String getLendableNetWorth_verifiedBy() {
		return lendableNetWorth_verifiedBy;
	}

	public void setLendableNetWorth_verifiedBy(String lendableNetWorth_verifiedBy) {
		this.lendableNetWorth_verifiedBy = lendableNetWorth_verifiedBy;
	}

	public String getLendableNetWorth_date() {
		return lendableNetWorth_date;
	}

	public void setLendableNetWorth_date(String lendableNetWorth_date) {
		this.lendableNetWorth_date = lendableNetWorth_date;
	}

	public String getBackgroundandCriminalCheck_value() {
		return backgroundandCriminalCheck_value;
	}

	public void setBackgroundandCriminalCheck_value(String backgroundandCriminalCheck_value) {
		this.backgroundandCriminalCheck_value = backgroundandCriminalCheck_value;
	}

	public String getBackgroundandCriminalCheck_completed() {
		return backgroundandCriminalCheck_completed;
	}

	public void setBackgroundandCriminalCheck_completed(String backgroundandCriminalCheck_completed) {
		this.backgroundandCriminalCheck_completed = backgroundandCriminalCheck_completed;
	}

	public String getBackgroundandCriminalCheck_CompletionDate() {
		return backgroundandCriminalCheck_CompletionDate;
	}

	public void setBackgroundandCriminalCheck_CompletionDate(String backgroundandCriminalCheck_CompletionDate) {
		this.backgroundandCriminalCheck_CompletionDate = backgroundandCriminalCheck_CompletionDate;
	}

	public String getBackgroundandCriminalCheck_associatedDocument() {
		return backgroundandCriminalCheck_associatedDocument;
	}

	public void setBackgroundandCriminalCheck_associatedDocument(String backgroundandCriminalCheck_associatedDocument) {
		this.backgroundandCriminalCheck_associatedDocument = backgroundandCriminalCheck_associatedDocument;
	}

	public String getBackgroundandCriminalCheck_VerifiedBy() {
		return backgroundandCriminalCheck_VerifiedBy;
	}

	public void setBackgroundandCriminalCheck_VerifiedBy(String backgroundandCriminalCheck_VerifiedBy) {
		this.backgroundandCriminalCheck_VerifiedBy = backgroundandCriminalCheck_VerifiedBy;
	}

	public String getBackgroundandCriminalCheck_Date() {
		return backgroundandCriminalCheck_Date;
	}

	public void setBackgroundandCriminalCheck_Date(String backgroundandCriminalCheck_Date) {
		this.backgroundandCriminalCheck_Date = backgroundandCriminalCheck_Date;
	}

	public String getCreditCheck_value() {
		return creditCheck_value;
	}

	public void setCreditCheck_value(String creditCheck_value) {
		this.creditCheck_value = creditCheck_value;
	}

	public String getCreditCheck_completed() {
		return creditCheck_completed;
	}

	public void setCreditCheck_completed(String creditCheck_completed) {
		this.creditCheck_completed = creditCheck_completed;
	}

	public String getCreditCheck_CompletionDate() {
		return creditCheck_CompletionDate;
	}

	public void setCreditCheck_CompletionDate(String creditCheck_CompletionDate) {
		this.creditCheck_CompletionDate = creditCheck_CompletionDate;
	}

	public String getCreditCheck_associatedDocument() {
		return creditCheck_associatedDocument;
	}

	public void setCreditCheck_associatedDocument(String creditCheck_associatedDocument) {
		this.creditCheck_associatedDocument = creditCheck_associatedDocument;
	}

	public String getCreditCheck_VerifiedBy() {
		return creditCheck_VerifiedBy;
	}

	public void setCreditCheck_VerifiedBy(String creditCheck_VerifiedBy) {
		this.creditCheck_VerifiedBy = creditCheck_VerifiedBy;
	}

	public String getCreditCheck_Date() {
		return creditCheck_Date;
	}

	public void setCreditCheck_Date(String creditCheck_Date) {
		this.creditCheck_Date = creditCheck_Date;
	}

	public String getTerritoryApproved_value() {
		return territoryApproved_value;
	}

	public void setTerritoryApproved_value(String territoryApproved_value) {
		this.territoryApproved_value = territoryApproved_value;
	}

	public String getTerritoryApproved_completed() {
		return territoryApproved_completed;
	}

	public void setTerritoryApproved_completed(String territoryApproved_completed) {
		this.territoryApproved_completed = territoryApproved_completed;
	}

	public String getTerritoryApproved_CompletionDate() {
		return territoryApproved_CompletionDate;
	}

	public void setTerritoryApproved_CompletionDate(String territoryApproved_CompletionDate) {
		this.territoryApproved_CompletionDate = territoryApproved_CompletionDate;
	}

	public String getTerritoryApproved_associatedDocument() {
		return territoryApproved_associatedDocument;
	}

	public void setTerritoryApproved_associatedDocument(String territoryApproved_associatedDocument) {
		this.territoryApproved_associatedDocument = territoryApproved_associatedDocument;
	}

	public String getTerritoryApproved_VerifiedBy() {
		return territoryApproved_VerifiedBy;
	}

	public void setTerritoryApproved_VerifiedBy(String territoryApproved_VerifiedBy) {
		this.territoryApproved_VerifiedBy = territoryApproved_VerifiedBy;
	}

	public String getTerritoryApproved_Date() {
		return territoryApproved_Date;
	}

	public void setTerritoryApproved_Date(String territoryApproved_Date) {
		this.territoryApproved_Date = territoryApproved_Date;
	}

	public String getFranchiseAgreementonFile_value() {
		return franchiseAgreementonFile_value;
	}

	public void setFranchiseAgreementonFile_value(String franchiseAgreementonFile_value) {
		this.franchiseAgreementonFile_value = franchiseAgreementonFile_value;
	}

	public String getFranchiseAgreementonFile_completed() {
		return franchiseAgreementonFile_completed;
	}

	public void setFranchiseAgreementonFile_completed(String franchiseAgreementonFile_completed) {
		this.franchiseAgreementonFile_completed = franchiseAgreementonFile_completed;
	}

	public String getFranchiseAgreementonFile_CompletionDate() {
		return franchiseAgreementonFile_CompletionDate;
	}

	public void setFranchiseAgreementonFile_CompletionDate(String franchiseAgreementonFile_CompletionDate) {
		this.franchiseAgreementonFile_CompletionDate = franchiseAgreementonFile_CompletionDate;
	}

	public String getFranchiseAgreementonFile_associatedDocument() {
		return franchiseAgreementonFile_associatedDocument;
	}

	public void setFranchiseAgreementonFile_associatedDocument(String franchiseAgreementonFile_associatedDocument) {
		this.franchiseAgreementonFile_associatedDocument = franchiseAgreementonFile_associatedDocument;
	}

	public String getFranchiseAgreementonFile_VerifiedBy() {
		return franchiseAgreementonFile_VerifiedBy;
	}

	public void setFranchiseAgreementonFile_VerifiedBy(String franchiseAgreementonFile_VerifiedBy) {
		this.franchiseAgreementonFile_VerifiedBy = franchiseAgreementonFile_VerifiedBy;
	}

	public String getFranchiseAgreementonFile_Date() {
		return franchiseAgreementonFile_Date;
	}

	public void setFranchiseAgreementonFile_Date(String franchiseAgreementonFile_Date) {
		this.franchiseAgreementonFile_Date = franchiseAgreementonFile_Date;
	}

	public String getFddReceiptonFile_value() {
		return fddReceiptonFile_value;
	}

	public void setFddReceiptonFile_value(String fddReceiptonFile_value) {
		this.fddReceiptonFile_value = fddReceiptonFile_value;
	}

	public String getFddReceiptonFile_completed() {
		return fddReceiptonFile_completed;
	}

	public void setFddReceiptonFile_completed(String fddReceiptonFile_completed) {
		this.fddReceiptonFile_completed = fddReceiptonFile_completed;
	}

	public String getFddReceiptonFile_CompletionDate() {
		return fddReceiptonFile_CompletionDate;
	}

	public void setFddReceiptonFile_CompletionDate(String fddReceiptonFile_CompletionDate) {
		this.fddReceiptonFile_CompletionDate = fddReceiptonFile_CompletionDate;
	}

	public String getFddReceiptonFile_associatedDocument() {
		return fddReceiptonFile_associatedDocument;
	}

	public void setFddReceiptonFile_associatedDocument(String fddReceiptonFile_associatedDocument) {
		this.fddReceiptonFile_associatedDocument = fddReceiptonFile_associatedDocument;
	}

	public String getFddReceiptonFile_VerifiedBy() {
		return fddReceiptonFile_VerifiedBy;
	}

	public void setFddReceiptonFile_VerifiedBy(String fddReceiptonFile_VerifiedBy) {
		this.fddReceiptonFile_VerifiedBy = fddReceiptonFile_VerifiedBy;
	}

	public String getFddReceiptonFile_Date() {
		return fddReceiptonFile_Date;
	}

	public void setFddReceiptonFile_Date(String fddReceiptonFile_Date) {
		this.fddReceiptonFile_Date = fddReceiptonFile_Date;
	}

	public String getOpener_value() {
		return opener_value;
	}

	public void setOpener_value(String opener_value) {
		this.opener_value = opener_value;
	}

	public String getOpener_completed() {
		return opener_completed;
	}

	public void setOpener_completed(String opener_completed) {
		this.opener_completed = opener_completed;
	}

	public String getOpener_CompletionDate() {
		return opener_CompletionDate;
	}

	public void setOpener_CompletionDate(String opener_CompletionDate) {
		this.opener_CompletionDate = opener_CompletionDate;
	}

	public String getOpener_associatedDocument() {
		return opener_associatedDocument;
	}

	public void setOpener_associatedDocument(String opener_associatedDocument) {
		this.opener_associatedDocument = opener_associatedDocument;
	}

	public String getOpener_VerifiedBy() {
		return opener_VerifiedBy;
	}

	public void setOpener_VerifiedBy(String opener_VerifiedBy) {
		this.opener_VerifiedBy = opener_VerifiedBy;
	}

	public String getOpener_Date() {
		return opener_Date;
	}

	public void setOpener_Date(String opener_Date) {
		this.opener_Date = opener_Date;
	}

}
