package com.builds.test.salesTest;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.fs.ReportsUI;
import com.builds.utilities.FranconnectUtil;

class ReportsTest {
	
	WebDriver driver;
	FranconnectUtil fc = new FranconnectUtil();

	public ReportsTest(WebDriver driver) {
		this.driver = driver;
	}
	
	
	void clickOn_LeadStatusReport() throws Exception
	{
		ReportsUI reportsUI = new ReportsUI(driver);
		fc.utobj().printTestStep("Click on Lead Status Report");
		fc.utobj().clickElement(driver, reportsUI.leadStatusReport);
	}

	
	
}
