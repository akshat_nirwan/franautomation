package com.builds.test.salesTest;

import com.builds.utilities.FranconnectUtil;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

/**
 * @author Akshat
 * 
 *         Created on 28-11-2018
 * 
 */
 
final class RestAssured_API {
	
	FranconnectUtil fc = new FranconnectUtil();

	void createLead_restAssured(Lead lead) throws Exception
	{
		// Getting Build URL
		String buildUrl = FranconnectUtil.config.get("buildUrl");
		System.out.println(buildUrl);
		if (buildUrl.endsWith("//") || buildUrl.endsWith("/") || buildUrl.endsWith("\\")) {
			// Do Nothing
		} else {
			buildUrl = buildUrl + "/";
		}
		
		String xmlString = "<fcRequest>" + 
				"<fsLead>" + 
				"<firstName>"+lead.getFirstName()+"</firstName>" + 
				"<lastName>"+lead.getLastName()+"</lastName>" + 
				"<companyName>"+lead.getCompanyName()+"</companyName>" + 
				"<city>"+lead.getCity()+"</city>" + 
				"<phone>"+lead.getHomePhone()+"</phone>" + 
				"<stateID>"+lead.getStateProvince()+"</stateID>" + 
				"<leadSource2ID>"+lead.getLeadSourceCategory()+"</leadSource2ID>" + 
				"<leadSource3ID>"+lead.getLeadSourceDetails()+"</leadSource3ID>" + 
				"<emailID>"+lead.getEmail()+"</emailID>" + 
				"<basedOnAssignmentRule>"+lead.getBasedonAssignmentRules()+"</basedOnAssignmentRule>" + 
				"</fsLead>" + 
				"</fcRequest>";
		
		RestAssured.baseURI = buildUrl;
		RequestSpecification httpRequest = RestAssured.given();
		
		httpRequest.given().contentType("application/x-www-form-urlencoded");
		
		httpRequest.given().param("responseType", "json");
		httpRequest.given().param("key", "pHNsc6x5mBksJDfo");
		httpRequest.given().param("clientCode", "FRANC0NN3CT_API");
		httpRequest.given().param("module", "fs");
		httpRequest.given().param("subModule", "lead");
		httpRequest.given().param("xmlString", xmlString);
		
		Response response = httpRequest.post("rest/dataservices/create");
		System.out.println("Response Body :: " +response.getBody().asString());
		
		if(response.getStatusCode()!=200)
		{
			fc.utobj().throwsException("API IS DOWN ==> " +response.getStatusCode());
		} else 	System.out.println("Success: Response Code ==> " +response.getStatusCode());

		if(response.getTime()<1000)
		{
			System.out.println("Response Time OK :: " +response.getTime());
		} else System.out.println("Response Time NOT OK :: " +response.getTime());
		
	}
	
	// =================================================================================================================================================================================
	
	 String retrieveLead_restAssured(Lead lead) throws Exception 
	{	
		// Getting Build URL
		String buildUrl = FranconnectUtil.config.get("buildUrl");
		System.out.println(buildUrl);
		if (buildUrl.endsWith("//") || buildUrl.endsWith("/") || buildUrl.endsWith("\\")) {
			// Do Nothing
		} else {
			buildUrl = buildUrl + "/";
		}
		
		String filterXML = "<fcRequest>" + 
				"<filter>" + 
				"<firstName>"+lead.getFirstName()+"</firstName>" + 
				"<lastName>"+lead.getLastName()+"</lastName>" + 
				"<companyName>"+lead.getCompanyName()+"</companyName>" + 
				"<city>"+lead.getCity()+"</city>" + 
				"<phone>"+lead.getHomePhone()+"</phone>" + 
				"<stateID>"+lead.getStateProvince()+"</stateID>" + 
				"<leadSource2ID>"+lead.getLeadSourceCategory()+"</leadSource2ID>" + 
				"<leadSource3ID>"+lead.getLeadSourceDetails()+"</leadSource3ID>" + 
				"<emailID>"+lead.getEmail()+"</emailID>" + 
				"</filter>" + 
				"</fcRequest>";
		
		RestAssured.baseURI = buildUrl;
		RequestSpecification httpRequest = RestAssured.given();
		
		httpRequest.given().contentType("application/x-www-form-urlencoded");
		
		httpRequest.given().param("responseType", "json");
		httpRequest.given().param("key", "pHNsc6x5mBksJDfo");
		httpRequest.given().param("clientCode", "FRANC0NN3CT_API");
		httpRequest.given().param("module", "fs");
		httpRequest.given().param("subModule", "lead");
		httpRequest.given().param("filterXML", filterXML);
		httpRequest.given().param("roleTypeForQuery", "-1");
		httpRequest.given().param("fromWhere", "jsp");
		
		Response response = httpRequest.post("rest/dataservices/retrieve");
		System.out.println("Response Body :: " +response.getBody().asString());
		
		if(response.getStatusCode()!=200)
		{
			fc.utobj().throwsException("API IS DOWN ==> " +response.getStatusCode());
		} else 	System.out.println("Success: Response Code ==> " +response.getStatusCode());

		if(response.getTime()<1000)
		{
			System.out.println("Response Time OK :: " +response.getTime());
		} else System.out.println("Response Time NOT OK :: " +response.getTime());
		
		return response.getBody().asString();
		
	}
	
	
	// =================================================================================================================================================================================
	
	 void updateLead_restAssured(int leadReferenceID , Lead lead) throws Exception
	{
		// Getting Build URL
		String buildUrl = FranconnectUtil.config.get("buildUrl");
		System.out.println(buildUrl);
		if (buildUrl.endsWith("//") || buildUrl.endsWith("/") || buildUrl.endsWith("\\")) {
			// Do Nothing
		} else {
			buildUrl = buildUrl + "/";
		}
		
		String xmlString = "<fcRequest>" + "<fsLead>" + "<referenceId>"+leadReferenceID+"</referenceId>"+"<salutation>" + lead.getSalutation() + "</salutation>"
				+ "<firstName>" + lead.getFirstName() + "</firstName>" + "<lastName>" + lead.getLastName()
				+ "</lastName>" + "<address>" + lead.getAddress1() + "</address>" + "<address2>" + lead.getAddress2()
				+ "</address2>" + "<city>" + lead.getCity() + "</city>" + "<country>" + lead.getCountry() + "</country>"
				+ "<stateID>" + lead.getStateProvince() + "</stateID>" + "<sendAutomaticMail>"
				+ lead.getSendAutomaticMail() + "</sendAutomaticMail>" + "<zip>" + lead.getZipPostalCode() + "</zip>"
				+ "<countyID>" + lead.getCounty() + "</countyID>" + "<primaryPhoneToCall>"
				+ lead.getPreferredModeofContact() + "</primaryPhoneToCall>" + "<bestTimeToContact>"
				+ lead.getBestTimeToContact() + "</bestTimeToContact>" + "<phone>" + lead.getWorkPhone() + "</phone>"
				+ "<phoneExt>" + lead.getWorkPhoneExtension() + "</phoneExt>" + "<homePhone>" + lead.getHomePhone()
				+ "</homePhone>" + "<homePhoneExt>" + lead.getHomePhoneExtension() + "</homePhoneExt>" + "<fax>"
				+ lead.getFax() + "</fax>" + "<mobile>" + lead.getMobile() + "</mobile>" + "<emailID>" + lead.getEmail()
				+ "</emailID>" + "<companyName>" + lead.getCompanyName() + "</companyName>" + "<comments>"
				+ lead.getComment() + "</comments>" + "<basedOnAssignmentRule>" + lead.getBasedonAssignmentRules()
				+ "</basedOnAssignmentRule>" + "<leadOwnerID>" + lead.getLeadOwner() + "</leadOwnerID>"
				+ "<leadOwnerReferenceId>" + "</leadOwnerReferenceId>" + "<leadRatingID>" + lead.getLeadRating()
				+ "</leadRatingID>" + "<marketingCodeId>" + lead.getMarketingCode() + "</marketingCodeId>"
				+ "<leadSource2ID>" + lead.getLeadSourceCategory() + "</leadSource2ID>" + "<leadSource3ID>"
				+ lead.getLeadSourceDetails() + "</leadSource3ID>" + "<otherLeadSourceDetail>"
				+ lead.getOtherLeadSources() + "</otherLeadSourceDetail>" + "<liquidCapitalMin>"
				+ lead.getCashAvailableforInvestment() + "</liquidCapitalMin>" + "<liquidCapitalMax>"
				+ lead.getCurrentNetWorth() + "</liquidCapitalMax>" + "<investTimeframe>"
				+ lead.getInvestmentTimeframe() + "</investTimeframe>" + "<background>" + lead.getBackground()
				+ "</background>" + "<sourceOfFunding>" + lead.getSourceOfInvestment() + "</sourceOfFunding>"
				+ "<nextCallDate>" + lead.getNextCallDate() + "</nextCallDate>" + "<divisionReferenceId>"
				+lead.getDivision()+ "</divisionReferenceId>" + "<division>" + lead.getDivision() + "</division>"
				+ "<noOfUnitReq>" + lead.getNoOfUnitsLocationsRequested() + "</noOfUnitReq>" + "<locationId1>"
				+ lead.getNewAvailableSites() + "</locationId1>" + "<locationId1b>" + lead.getExistingSites()
				+ "</locationId1b>" + "<locationId2>" + lead.getResaleSites() + "</locationId2>" + "<preferredCity1>"
				+ lead.getPreferredCity1() + "</preferredCity1>" + "<preferredCountry1>" + lead.getPreferredCountry1()
				+ "</preferredCountry1>" + "<preferredStateId1>" + lead.getPreferredStateProvince1()
				+ "</preferredStateId1>" + "<preferredCity2>" + lead.getPreferredCity2() + "</preferredCity2>"
				+ "<preferredCountry2>" + lead.getPreferredCountry2() + "</preferredCountry2>" + "<preferredStateId2>"
				+ lead.getPreferredStateProvince2() + "</preferredStateId2>" + "<forecastClosureDate>"
				+ lead.getForecastClosureDate() + "</forecastClosureDate>" + "<probability>" + lead.getProbability()
				+ "</probability>" + "<forecastRating>" + lead.getForecastRating() + "</forecastRating>"
				+ "<forecastRevenue>" + lead.getForecastRevenue() + "</forecastRevenue>"
				+ "<basedOnWorkflowAssignmentRule>" + "</basedOnWorkflowAssignmentRule>" + "<campaignID>"
				+ lead.getCampaignName() + "</campaignID>" + "</fsLead>" + "</fcRequest>";
		
		RestAssured.baseURI = buildUrl;
		RequestSpecification httpRequest = RestAssured.given();
		
		httpRequest.given().contentType("application/x-www-form-urlencoded");
		
		httpRequest.given().param("responseType", "json");
		httpRequest.given().param("key", "pHNsc6x5mBksJDfo");
		httpRequest.given().param("clientCode", "FRANC0NN3CT_API");
		httpRequest.given().param("module", "fs");
		httpRequest.given().param("subModule", "lead");
		httpRequest.given().param("xmlString", xmlString);
		
		Response response = httpRequest.post("rest/dataservices/update");
		System.out.println("Response Body :: " +response.getBody().asString());
		
		if(response.getStatusCode()!=200)
		{
			fc.utobj().throwsException("API IS DOWN ==> " +response.getStatusCode());
		} else 	System.out.println("Success: Response Code ==> " +response.getStatusCode());

		if(response.getTime()<1000)
		{
			System.out.println("Response Time OK :: " +response.getTime());
		} else System.out.println("Response Time NOT OK :: " +response.getTime());
	}
	
	// =================================================================================================================================================================================
	

	

}
