package com.builds.test.salesTest;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.common.FCHomePage;
import com.builds.uimaps.fs.SalesUI;
import com.builds.utilities.FranconnectUtil;

public class Sales {
	private FranconnectUtil fc = new FranconnectUtil();
	private WebDriver driver;

	public Sales(WebDriver driver) throws Exception {
		this.driver = driver;
	}

	Sales clickSales() throws Exception
	{
		FCHomePage pobj = new FCHomePage(driver);

		try {
			fc.utobj().clickElement(driver, pobj.fsModule);
		} catch (Exception e) {
			try
			{
				fc.utobj().clickElement(driver, pobj.moreLink);
			}catch(Exception e2)
			{
				
			}
			fc.utobj().clickElement(driver, pobj.fsModule);
		}
		return this;
	}
	
	Home clickHome() throws Exception {
		fc.utobj().printTestStep("Click on Home Tab");
		SalesUI ui = new SalesUI(driver);
		fc.utobj().clickElement(driver, ui.salesHomePageLnk);
		return new Home(driver);
	}

	LeadManagementTest clickLeadManagement() throws Exception {
		fc.utobj().printTestStep("Click on Lead Management Tab");
		SalesUI ui = new SalesUI(driver);
		fc.utobj().clickElement(driver, ui.leadManagementLnk);
		return new LeadManagementTest(driver);
	}

	public Groups clickGroups() throws Exception {
		fc.utobj().printTestStep("Click on Groups Tab");
		SalesUI ui = new SalesUI(driver);
		fc.utobj().clickElement(driver, ui.groupsPageLnk);
		return new Groups(driver);
	}

	SearchPage clickSearch() throws Exception {
		fc.utobj().printTestStep("Click on Search Tab");
		SalesUI ui = new SalesUI(driver);
		fc.utobj().clickElement(driver, ui.search);

		return new SearchPage(driver);
	}

	CampaignCenterTest clickCampaignCenter() throws Exception {
		fc.utobj().printTestStep("Click on Campaign Center Tab");
		SalesUI ui = new SalesUI(driver);
		fc.utobj().clickElement(driver, ui.campaign);
		return new CampaignCenterTest(driver);
	}

	WorkflowsPage clickWorkflows() throws Exception {
		fc.utobj().printTestStep("Click on Workflows Tab");
		SalesUI ui = new SalesUI(driver);
		fc.utobj().clickElement(driver, ui.Workflows);
		return new WorkflowsPage(driver);
	}

	FDD clickFDD() throws Exception {
		fc.utobj().printTestStep("Click on FDD Tab");
		SalesUI ui = new SalesUI(driver);
		fc.utobj().clickElement(driver, ui.FDD);
		return new FDD(driver);
	}

	TasksTab clickTasks() throws Exception {
		fc.utobj().printTestStep("Click on Tasks Tab");
		SalesUI ui = new SalesUI(driver);
		fc.utobj().clickElement(driver, ui.Tasks);
		return new TasksTab(driver);
	}

	Calendar clickCalendar() throws Exception {
		fc.utobj().printTestStep("Click on Calendar Tab");
		SalesUI ui = new SalesUI(driver);
		fc.utobj().clickElement(driver, ui.Calendar);
		return new Calendar(driver);
	}

	ImportTest clickImport() throws Exception {
		fc.utobj().printTestStep("Click on Import Tab");
		SalesUI ui = new SalesUI(driver);
		fc.utobj().clickElement(driver, ui.Import);
		return new ImportTest(driver);
	}

	Export clickExport() throws Exception {
		fc.utobj().printTestStep("Click on Export Tab");
		SalesUI ui = new SalesUI(driver);
		fc.utobj().clickElement(driver, ui.Export);
		return new Export(driver);
	}

	SitesPage clickSites() throws Exception {
		fc.utobj().printTestStep("Click on Sites Tab");
		SalesUI ui = new SalesUI(driver);
		fc.utobj().clickElement(driver, ui.Sites);
		return new SitesPage(driver);
	}

	BrokersTab clickBrokers() throws Exception {
		fc.utobj().printTestStep("Click on Brokers Tab");
		SalesUI ui = new SalesUI(driver);
		fc.utobj().clickElement(driver, ui.Brokers);
		return new BrokersTab(driver);
	}

	Reports clickReports() throws Exception {
		fc.utobj().printTestStep("Click on Reports Tab");
		SalesUI ui = new SalesUI(driver);
		fc.utobj().clickElement(driver, ui.Reports);
		return new Reports(driver);
	}

	Sales_Common_New commonMethods() {
		return new Sales_Common_New();
	}

}
