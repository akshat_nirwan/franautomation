package com.builds.test.salesTest;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.test.common.FCHomePageTest;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

class SalesGroupsPage_Test {

	@Test(groups = {"admin451455"})
	@TestCase(createdOn = "2018-02-06", updatedOn = "2018-02-06", testCaseId = "Sales_Regular_Groups_AdvancedSearch_Export", testCaseDescription = "Verify the lead associated with group is getting searched on basis of group at search / advanced search and export.")
	private void Sales_Regular_Groups_AdvancedSearch_Export() throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			
			FCHomePageTest  fcHomePageTest = new FCHomePageTest();
			fc.commonMethods().getModules().openFinanceModule(driver);
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

}
