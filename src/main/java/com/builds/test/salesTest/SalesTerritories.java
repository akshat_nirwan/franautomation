package com.builds.test.salesTest;

import java.util.ArrayList;

class SalesTerritories {

	private String category;
	private String salesTerritoryName;
	private String groupBy;
	private ArrayList<String> countries = new ArrayList<String>();
	private String zipPostalCode; // zip / postal code OR Upload (.csv) file
	private String zipPostalCodeText; // zip codes
	private String zipPostalCode_csvFile;
	private String country;
	private ArrayList<String> states = new ArrayList<String>();
	private ArrayList<String> counties = new ArrayList<String>();

	
	public ArrayList<String> getCounties() {
		return counties;
	}

	public void setCounties(ArrayList<String> counties) {
		this.counties = counties;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public ArrayList<String> getStates() {
		return states;
	}

	public void setStates(ArrayList<String> states) {
		this.states = states;
	}

	
	public String getZipPostalCode_csvFile() {
		return zipPostalCode_csvFile;
	}

	public void setZipPostalCode_csvFile(String zipPostalCode_csvFile) {
		this.zipPostalCode_csvFile = zipPostalCode_csvFile;
	}

	public String getZipPostalCode() {
		return zipPostalCode;
	}

	public void setZipPostalCode(String zipPostalCode) {
		this.zipPostalCode = zipPostalCode;
	}

	public String getZipPostalCodeText() {
		return zipPostalCodeText;
	}

	public void setZipPostalCodeText(String zipPostalCodeText) {
		this.zipPostalCodeText = zipPostalCodeText;
	}
	
	public void setCountries(ArrayList<String> countries) {
		this.countries = countries;
	}
	
	public ArrayList<String> getCountries() {
		return countries;
	}
	
	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getSalesTerritoryName() {
		return salesTerritoryName;
	}

	public void setSalesTerritoryName(String salesTerritoryName) {
		this.salesTerritoryName = salesTerritoryName;
	}

	public String getGroupBy() {
		return groupBy;
	}

	public void setGroupBy(String groupBy) {
		this.groupBy = groupBy;
	}
}
