package com.builds.test.salesTest;

import org.openqa.selenium.WebDriver;

import com.builds.test.common.ManageAreaRegion;
import com.builds.uimaps.fs.SalesTerritoriesUI;
import com.builds.utilities.FranconnectUtil;

class SalesTerritoriesTest {

	WebDriver driver;
	FranconnectUtil fc = new FranconnectUtil();

	public SalesTerritoriesTest(WebDriver driver) {
		this.driver = driver;
	}

	SalesTerritoriesTest addNewSalesTerritory(SalesTerritories salesTerritories) throws Exception {
		fc.utobj().printTestStep("Adding New Sales Territory");
		SalesTerritoriesUI salesTerritoriesUI = new SalesTerritoriesUI(driver);

		fc.utobj().clickElement(driver, salesTerritoriesUI.addNewSalesTerritory_Btn);
		fill_addNewSalesTerritory(salesTerritories);
		fc.utobj().clickElement(driver, salesTerritoriesUI.submitBtn);

		if (salesTerritories.getGroupBy().equalsIgnoreCase("County")) {
			if (salesTerritories.getCounties() != null) {
				for (String county : salesTerritories.getCounties()) {
					fc.utobj().clickElement(driver, salesTerritoriesUI.selectCounty(driver, county));
				}

			}
			fc.utobj().clickElement(driver, salesTerritoriesUI.NextBtn);
		}
		
		if (salesTerritories.getCategory().equalsIgnoreCase("International")) {
			if (salesTerritories.getStates() != null) {
				for (String state : salesTerritories.getStates()) {
					fc.utobj().clickElement(driver, salesTerritoriesUI.selectState(driver, state));
				}

			}
			fc.utobj().clickElement(driver, salesTerritoriesUI.NextBtn2);
		}

		/*if (salesTerritories.getGroupBy().equalsIgnoreCase("States")) {
			if (salesTerritories.getStates() != null) {
				for (String state : salesTerritories.getStates()) {
					fc.utobj().clickElement(driver, salesTerritoriesUI.selectState(driver, state));
				}

			}
			fc.utobj().clickElement(driver, salesTerritoriesUI.NextBtn2);
		}*/

		return new SalesTerritoriesTest(driver);
	}

	private void fill_addNewSalesTerritory(SalesTerritories salesTerritories) throws Exception {
		fc.utobj().printTestStep("Fill data in addNewSalesTerritory");
		SalesTerritoriesUI salesTerritoriesUI = new SalesTerritoriesUI(driver);

		if (salesTerritories.getCategory() != null) {
			fc.utobj().selectDropDown(driver, salesTerritoriesUI.category, salesTerritories.getCategory());
		}

		if (salesTerritories.getSalesTerritoryName() != null) {
			fc.utobj().sendKeys(driver, salesTerritoriesUI.salesTerritoryName,
					salesTerritories.getSalesTerritoryName());
		}

		if (salesTerritories.getGroupBy() != null) {
			fc.utobj().selectDropDown(driver, salesTerritoriesUI.groupBy, salesTerritories.getGroupBy());

			if (salesTerritories.getGroupBy().equalsIgnoreCase("zip / postal code")) {
				if (salesTerritories.getZipPostalCode().equalsIgnoreCase("Enter a comma separated list of Zip / Postal Code manually")) {
					fc.utobj().clickElement(driver, salesTerritoriesUI.zipPostalCodeManually_radioBtn);
					fc.utobj().sendKeys(driver, salesTerritoriesUI.zipPostalCodeManually_textBox,
							salesTerritories.getZipPostalCodeText());
				}
				if (salesTerritories.getZipPostalCode().equalsIgnoreCase("Upload (.csv) file")) {
					fc.utobj().clickElement(driver, salesTerritoriesUI.uploadCsvFile_radioBtn);
					fc.utobj().sendKeys(driver, salesTerritoriesUI.csvFileToUpload,
							salesTerritories.getZipPostalCode_csvFile());
				}

				if (salesTerritories.getCountries() != null) {
					for (String country : salesTerritories.getCountries()) {
						if (country.equalsIgnoreCase("USA")) {
							fc.utobj().clickElement(driver, salesTerritoriesUI.countryUSA);
						}
					}
				}
			}

			else if (salesTerritories.getGroupBy().equalsIgnoreCase("County")) {
				fc.utobj().selectDropDown(driver, salesTerritoriesUI.groupBy, salesTerritories.getGroupBy());

				if (salesTerritories.getCountries() != null) {
					for (String country : salesTerritories.getCountries()) {
						if (country.equalsIgnoreCase("USA")) {
							fc.utobj().clickElement(driver, salesTerritoriesUI.countryUSA);
						}
					}
				}

				/*
				 * if(salesTerritories.getCountry() != null &&
				 * salesTerritories.getCountry().equalsIgnoreCase("USA")) {
				 * fc.utobj().clickElement(driver,
				 * salesTerritoriesUI.countryUSA); }
				 */

				if (salesTerritories.getStates() != null) {
					for (String state : salesTerritories.getStates()) {
						fc.utobj().clickElement(driver, salesTerritoriesUI.selectState(driver, state));
					}
				}

			}

			else if (salesTerritories.getGroupBy().equalsIgnoreCase("States")) {
				fc.utobj().selectDropDown(driver, salesTerritoriesUI.groupBy, salesTerritories.getGroupBy());

				if (salesTerritories.getCountries() != null) {
					for (String country : salesTerritories.getCountries()) {
						if (country.equalsIgnoreCase("USA")) {
							fc.utobj().clickElement(driver, salesTerritoriesUI.countryUSA);
						} else {
							fc.utobj().clickElement(driver,
									salesTerritoriesUI.select_Country_GroupByCounty(driver, country));
						}
					}
				}

				/*
				 * if(salesTerritories.getCountry() != null &&
				 * salesTerritories.getCountry().equalsIgnoreCase("USA")) {
				 * fc.utobj().clickElement(driver,
				 * salesTerritoriesUI.countryUSA); }
				 */
				
				if (salesTerritories.getCategory().equalsIgnoreCase("Domestic")) {
					if (salesTerritories.getStates() != null) {
						for (String state : salesTerritories.getStates()) {
							fc.utobj().clickElement(driver, salesTerritoriesUI.selectState(driver, state));
						}
					}
				}
			}

		}
	}

	public void verify_addNewSalesTerritory(SalesTerritories salesTerritories) throws Exception {
		fc.utobj().printTestStep("Verify Add New Sales Territory");
		if (!fc.utobj().assertLinkText(driver, salesTerritories.getSalesTerritoryName())) {
			fc.utobj().throwsException("Verify Add New Sales Territory : Failed");
		}
	}

}
