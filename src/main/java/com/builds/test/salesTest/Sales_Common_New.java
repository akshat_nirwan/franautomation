package com.builds.test.salesTest;

import java.io.BufferedReader;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Reporter;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.builds.test.common.AddDivision;
import com.builds.test.common.CorporateUser;
import com.builds.test.common.ManageAreaRegion;
import com.builds.test.common.WebService;
import com.builds.test.fs.Sales;
import com.builds.uimaps.common.CommonUI;
import com.builds.uimaps.fs.SalesUI;
import com.builds.utilities.FranconnectUtil;


/**
 * 
 * @author Akshat
 *
 */

final public class Sales_Common_New extends FranconnectUtil {
	private FranconnectUtil fc = new FranconnectUtil();

	public Sales fsModule(WebDriver driver) throws Exception {
		utobj().printTestStep("Go to Sales Module");
		home_page().clickSalesModule(driver);
		return new Sales();
	}
	
	public void minimizeNotificationFooter_ifMaximised(WebDriver driver) throws Exception
	{
		WebElement ele = fc.utobj().getElementByID(driver, "footer");
		String styleAttributeValues = ele.getAttribute("display");
		
		CommonUI coi = new CommonUI(driver);
		boolean isDisplayed = coi.showQuickLinks.isDisplayed();

        if (isDisplayed) {
            fc.utobj().clickElement(driver, coi.hideNotificationBar);
        }
		
		/*if(styleAttributeValues.contains("block"))
		{
			fc.utobj().clickElement(driver, fc.utobj().getElementByID(driver, "hideBar"));
		}*/
	}

	Email fillDefaultValue_Email() throws InterruptedException {
		Email email = new Email();
		email.setFromReply("customsalesemail@xyz.com");
		email.setSubject(fc.utobj().generateTestData("Email Subject"));
		email.setBody(fc.utobj().generateTestData("Email Body"));
		return email;
	}

	Lead fillDefaultValue_LeadDetails() throws InterruptedException {
		Lead ld = new Lead();
		ld.setFirstName(fc.utobj().generateTestData("Fname"));
		ld.setLastName(fc.utobj().generateTestData("Lname"));
		ld.setSendAutomaticMail("Yes");
		ld.setEmail(fc.utobj().generateTestData("e").toLowerCase() + "@xyz.com");
		ld.setLeadOwner("FranConnect Administrator");
		ld.setLeadSourceCategory("Advertisement");
		ld.setLeadSourceDetails("Newspaper");
		ld.setCompanyName(fc.utobj().generateTestData("Company"));
		ld.setCampaignName("Select");
		return ld;
	}

	Group fillDefaultValue_RegularGroupDetails(Group group) throws InterruptedException {
		group.setName(fc.utobj().generateTestData("Group"));
		group.setDescription(fc.utobj().generateTestData("Description"));
		group.setGroupType("Regular");
		group.setVisibility("Public");
		return group;
	}

	Group fillDefaultValue_SmartGroupDetails(Group group) throws InterruptedException {
		group.setName(fc.utobj().generateTestData("Group"));
		group.setDescription(fc.utobj().generateTestData("Description"));
		group.setGroupType("Smart");
		group.setVisibility("Public");
		return group;
	}

	Call fillDefaultValue_CallDetails(Call call) throws InterruptedException {
		call.setSubject(fc.utobj().generateTestData("Call"));
		call.setDate("01/23/2019");
		call.setTimeHH("6");
		call.setTimeMM("30 Min");
		call.setTimeAMPM("AM");
		call.setCallStatus("Busy");
		call.setCommunicationType("Inbound Call");
		call.setComments("Test Comments added for Call.");
		return call;
	}

	Remark fillDefaultValue_Remark(Remark remark) throws InterruptedException {
		remark.setRemark(fc.utobj().generateTestData("Remarks"));
		return remark;
	}

	Task set_DefaultValue_TaskDetails(Task task) throws InterruptedException {
		task.setAssignTo("Lead Owner");
		task.setStatus("Not Started");
		task.setTaskType("Default");
		task.setSubject(fc.utobj().generateTestData("Task Subject"));
		task.setTimelessTask("no");
		task.setPriority("Medium");
		task.setStartDate("06/06/2019");
		task.setAddToCalendar("yes");
		task.setStartTimeHH("10");
		task.setStartTimeMM("45 Min");
		task.setStartTimeAMPM("AM");
		task.setEndTimeHH("11");
		task.setEndTimeMM("45 Min");
		task.setEndTimeAMPM("AM");
		task.setTaskDescription(fc.utobj().generateTestData("Task Description"));
		task.setSetReminder("15 Minutes Prior");
		task.setTaskEmailReminder("yes");

		return task;
	}

	Campaign fillDefaultValue_Campaign(Campaign campaign) throws InterruptedException {
		campaign.setCampaignName(fc.utobj().generateTestData("Campaign"));
		campaign.setDescription(fc.utobj().generateTestData("Desc"));
		campaign.setAccessibility("Public");
		return campaign;
	}

	void closeColorBox(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Test Steps# : Close Color Box / Overlay");
		try {
			;
			List<WebElement> elements = driver
					.findElements(By.xpath(".//div[@id='cboxClose' and contains(text(),'close')]"));
			try {
				for (WebElement e : elements) {
					fc.utobj().clickElement(driver, e);
				}
			} catch (Exception e) {
			}
		} catch (Exception e) {
			Reporter.log("Method closeColorBox : " + e.getMessage());
			fc.utobj().throwsException("Was not able to close the overlay / colorbox");
		}
	}

	FieldsToBeExported fillDefaultValue_ExportFields(WebDriver driver, FieldsToBeExported fields) throws Exception {
		fields.setFirstName("yes");
		fields.setLastName("yes");
		fields.setEmailID("yes");
		return fields;
	}

	Status fillDefaultValue_LeadStatus(WebDriver driver, Status status, String statusName) throws InterruptedException {
		if (statusName != null) {
			status.setLeadStatus(statusName);
		}

		status.setFranchiseAwardedDate("01/01/2019");
		status.setDatetoOpen("01/01/2020");
		status.setNoOfLocation("2");
		status.setRemarks(fc.utobj().generateTestData("Remarks"));
		return status;
	}

	String getUniqueEmail() throws InterruptedException {
		String email = fc.utobj().generateTestData("e").toLowerCase();
		email = email + "@xyz.com";
		return email;
	}

	void topSearch_EnterText_clickTopSearchImage(WebDriver driver, String searchString) throws Exception {
		SalesUI ui = new SalesUI(driver);
		fc.utobj().printTestStep("Search from top right search" + searchString);
		fc.utobj().sendKeys(driver, ui.topSearchRight_TextBox, searchString);
		clickTopSearchImage(driver);
	}

	void clickTopSearchImage(WebDriver driver) throws Exception {
		SalesUI ui = new SalesUI(driver);
		fc.utobj().printTestStep("Click on Search Image");
		fc.utobj().clickElement(driver, ui.searchImg);
	}

	CoApplicantWithoutSeperateLead fillDefaultValue_Coapplicant_AdditionalContactWithoutAddingSeparateLead_FullInfo(
			CoApplicantWithoutSeperateLead additionalContact) throws Exception {
		fc.utobj().printTestStep("Fill Coapplicant Data");

		additionalContact.setFirstName("firstName");
		additionalContact.setLastName("lastName");
		additionalContact.setCoApplicantRelationship("Spouse");
		additionalContact.setPhone("654654354");
		additionalContact.setExt("6546");
		additionalContact.setFax("54564654");
		additionalContact.setEmailID("frantest2017@gmail.com");
		additionalContact.setAddress("address");
		additionalContact.setCity("city");
		additionalContact.setCountry("USA");
		additionalContact.setStateID("Iowa");
		additionalContact.setZip("656445");
		return additionalContact;
	}

	CoApplicantWithAdditionLead fillDefaultValue_Coapplicant_AdditionalContactWithAdditionOf_ExistingLead(
			CoApplicantWithAdditionLead coApplicant) throws Exception {
		fc.utobj().printTestStep("Fill Coapplicant Data Coapplicant Additional Contact With Addition Of_ExistingLead");

		coApplicant.setLead("Existing Lead");
		coApplicant.setCoApplicantRelationship("Spouse");

		return coApplicant;
	}

	CoApplicantWithAdditionLead fillDefaultValue_Coapplicant_AdditionalContactWithAdditionOf_NewLead(
			CoApplicantWithAdditionLead coApplicant) throws Exception {
		fc.utobj().printTestStep("Fill Coapplicant Data Coapplicant Additional Contact With Addition Of_NewLead");

		coApplicant.setLead("New Lead");
		coApplicant.setCoApplicantRelationship("Relative");

		return coApplicant;
	}

	Compliance fillDefaultValue_Compliance_FullInfo(Compliance compliance) throws Exception {
		fc.utobj().printTestStep("Fill Compliance");

		compliance.setDateOfFDD("02/24/2018");
		compliance.setDateFDDReceivedByFranchisee("02/27/2018");
		compliance.setDateHoldingPeriodRequirementsExpireForFDD("02/28/2018");
		compliance.setVersionOfFDD("8");
		compliance.setIpAddress("192.168.1.2");
		compliance.setBrowserType("CHROME");
		compliance.setDateOfFirstFranchiseePayment("02/14/2018");
		compliance.setStateProvinceRegistrationRequired("YES");
		compliance.setDateOfSecondFranchiseePayment("02/14/2018");
		compliance.setStateProvinceAddendumRequired("na");
		compliance.setFranchiseCommitteeApproval("YES");

		// Franchise Agreement
		compliance.setFaRequestedDate("02/21/2018");
		compliance.setFaReceivedDate("02/21/2018");
		compliance.setDateFranchiseeReceivedAgreements("02/21/2018");
		compliance.setDateHoldingPeriodRequirementsAreMet("02/21/2018");
		compliance.setDateAgreementSignedByFranchisee("02/21/2018");
		compliance.setDateHoldingPeriodRuleOnCheckMet("02/21/2018");
		compliance.setDateHoldingPeriodRuleOnAgreementsMet("02/21/2018");
		compliance.setVersionOfFranchiseeAgreement("9");

		// Franchise Fee and Signed Agreements Received
		compliance.setAmountFranchiseFee("02/21/2018");
		compliance.setDateFranchiseFee("02/24/2018");

		// Area Development Fee and Signed Agreements Received
		compliance.setAmountAreaDevelopmentFee("02/24/2018");
		compliance.setDateAreaDevelopmentFee("02/21/2018");
		compliance.setAdaExecutionDate("02/26/2018");
		compliance.setFaExecutionDate("02/02/2018");

		// Contract Signing Details
		compliance.setContractReceivedSigned("YES");
		compliance.setLeaseRiderSigned("NO");
		compliance.setLicenseAgreementSigned("NA");
		compliance.setPromissoryNoteSigned("NO");
		compliance.setPersonalCovenantsAgreementSigned("YES");
		compliance.setFddReceiptSigned("NO");
		compliance.setGuaranteeSigned("NA");
		compliance.setOtherDocumentsSigned("NA");
		compliance.setStateProvinceRequiredAddendumSigned("YES");
		compliance.setHandWrittenChanges("NO");
		compliance.setOtherAddendumSigned("NO");
		compliance.setProofOfControlOverRealEstate("YES");

		return compliance;
	}

	RealEstate fillDefaultValue_RealEstate_FullInfo(RealEstate realEstate) throws Exception {
		fc.utobj().printTestStep("Fill Real Estate");

		realEstate.setSiteAddress1("siteAddress1");
		realEstate.setSiteAddress2("siteAddress2");
		realEstate.setSiteCity("siteCity");
		realEstate.setSiteCountry("USA");
		realEstate.setSiteStateProvince("Alaska");
		realEstate.setBuildingSize("10000");
		realEstate.setBuildingLengthX("500");
		realEstate.setBuildingBreadthY("300");
		realEstate.setBuildingHeightZ("200");
		realEstate.setParkingSpaces("5000");
		realEstate.setDealType("dealType");
		realEstate.setLoiSent("01/02/2019");
		realEstate.setLoiSigned("01/03/2019");
		realEstate.setApprovalDate("02/09/2019");
		realEstate.setLeaseCommencement("10/07/2019");
		realEstate.setLeaseExpiration("02/07/2020");
		realEstate.setInitialTerm("10");
		realEstate.setOptionTerm("5");
		realEstate.setPurchaseOption("Yes");
		realEstate.setProjectedOpeningDate("02/08/2019");
		realEstate.setGeneralContractorSelected("Yes");
		realEstate.setNameGeneralContractor("nameGeneralContractor");
		realEstate.setAddressGeneralContractor("addressGeneralContractor");
		realEstate.setPermitAppliedFor("01/03/2018");
		realEstate.setPermitIssuedDate("01/03/2018");
		realEstate.setCertificateOfOccupancy("01/03/2018");
		realEstate.setTurnoverDate("01/03/2018");
		realEstate.setExpectedOpeningDate("01/03/2018");

		return realEstate;

	}

	PersonalProfile fillDefaultValue_PersonalProfile_FullInfo(PersonalProfile personalProfile) throws Exception {
		fc.utobj().printTestStep("Fill Personal Profile");

		// Personal Information
		personalProfile.setFirstName("firstName");
		personalProfile.setLastName("lastName");
		personalProfile.setGender("Male");
		personalProfile.setHomeAddress("homeAddress");
		personalProfile.setHowLongAtAddress("10");
		personalProfile.setHomeCity("homeCity");
		personalProfile.setHomeCountry("USA");
		personalProfile.setHomeStateProvince("Maine");
		personalProfile.setHomeZipPostalCode("645665");
		personalProfile.setBirthMonth("Jan");
		personalProfile.setBirthDate("11");
		personalProfile.setHomePhone("6456565");
		personalProfile.setHomePhoneExt("5454");
		personalProfile.setTimeToCall("9");
		personalProfile.setHomeOwnership("Own");
		personalProfile.setEmail("frantest2017@gmail.com");
		personalProfile.setMaritalStatus("Married");
		personalProfile.setSpouseName("spouseName");
		personalProfile.setHeardProformaFrom("heardProformaFrom");
		personalProfile.setSeekingOwnBusiness("seekingOwnBusiness");
		personalProfile.setFullTimeBusiness("No");
		personalProfile.setOtherInvestigation("otherInvestigation");

		// Employment
		personalProfile.setPresentEmployer("presentEmployer");
		personalProfile.setPercentOwn("50");
		personalProfile.setTitle("title");
		personalProfile.setDateStarted("02/08/2018");
		personalProfile.setEmployerAddress("employerAddress");
		personalProfile.setEmployerCity("employerCity");
		personalProfile.setEmployerCountry("USA");
		personalProfile.setEmployerState("Maine");
		personalProfile.setEmployerZip("201301");
		personalProfile.setBusinessPhone("654654");
		personalProfile.setCallAtWork("Yes");
		personalProfile.setHourPerWeek("0-20");
		personalProfile.setSalary("2155565");
		personalProfile.setResponsibility("responsibility");
		personalProfile.setSelfEmployed("Yes");
		personalProfile.setLimitProforma("No");
		personalProfile.setSimilarWork("Yes");

		// Finance
		personalProfile.setFinanceProforma("FinancePerfoma");
		personalProfile.setPartner("Yes");
		personalProfile.setSupportHowLong("1");
		personalProfile.setIncome("5415454");
		personalProfile.setOtherSalary("999999");
		personalProfile.setOtherIncomeExplaination("otherIncomeExplaination");

		// Specified Data
		personalProfile.setSoleSource("Yes");
		personalProfile.setHowSoon("howSoon");
		personalProfile.setRunYourself("Yes");
		personalProfile.setResponsibleForOperation("responsibleForOperation");
		personalProfile.setConvictedForFelony("Yes");
		personalProfile.setLiabilites("No");
		personalProfile.setBankruptcy("Yes");
		personalProfile.setLawsuit("No");
		personalProfile.setConvicted("Yes");

		// Other Comments
		personalProfile.setFamilyFeelings("familyFeelings");
		personalProfile.setOtherFacts("otherFacts");

		// Prospect Summary
		personalProfile.setPersonalPersonalityStyle("personalPersonalityStyle");
		personalProfile.setBackgroundOverview("backgroundOverview");
		personalProfile.setGoalDream("goalDream");
		personalProfile.setConcerns("concerns");
		personalProfile.setTiming("timing");
		personalProfile.setHotButtons("hotButtons");
		personalProfile.setOtherOptions("otherOptions");

		return personalProfile;

	}

	Visit fillDefaultValue_Visit_FullInfo(Visit visit) throws Exception {
		fc.utobj().printTestStep("Fill Visit Details");

		visit.setVisitSchedule("01/01/2019");
		visit.setVisitdate("02/01/2019");
		visit.setType("Individual");
		visit.setAssignTo("Logged in User");
		visit.setVisitor1Name("Visitor1");
		visit.setVisitor2Name("visitor2");
		visit.setVisitor3Name("visitor3");
		visit.setRelationship1("relationship1");
		visit.setRelationship2("relationship2");
		visit.setRelationship3("relationship3");
		visit.setAgreedReimbursement("100");
		visit.setActualReimbursement("200");
		visit.setPaymentSentDate("02/06/2018");
		visit.setVisitConfirmed("VisitConfirmedBy");
		visit.setComments("comments");

		return visit;
	}

	QualificationDetails fillDefaultValue_QualificationDetails_FullInfo(QualificationDetails qualificationDetails)
			throws Exception {
		fc.utobj().printTestStep("Fill Qualification Details");

		qualificationDetails.setDate("01/02/2018");
		qualificationDetails.setName("name");
		qualificationDetails.setGender("Male");
		qualificationDetails.setPresentAddress("presentAddress");
		qualificationDetails.setYearsAtThisAddress("10");
		qualificationDetails.setCity("city");
		qualificationDetails.setCountry("USA");
		qualificationDetails.setStateProvince("Virginia");
		qualificationDetails.setZipPostalCode("201301");
		qualificationDetails.setWorkPhone("6546548");
		qualificationDetails.setWorkPhoneExt("65496");
		qualificationDetails.setHomePhone("68596565");
		qualificationDetails.setHomePhoneExt("656");
		qualificationDetails.setEmail("frantest2017@gmail.com");
		qualificationDetails.setUsCitizen("Yes");
		qualificationDetails.setSocialSecurity("65465");
		qualificationDetails.setPreviousAddress("previousAddress");
		qualificationDetails.setPreviousCity("previousCity");
		qualificationDetails.setPreviousCountry("India");
		qualificationDetails.setPreviousStateProvince("Delhi");
		qualificationDetails.setPreviousZipPostalCode("201103");
		qualificationDetails.setBirthDate("11");
		qualificationDetails.setBirthMonth("Feb");
		qualificationDetails.setBestTimeToCall("01.00");
		qualificationDetails.setHomeOwnership("Own");
		qualificationDetails.setMaritalStatus("Married");
		qualificationDetails.setSpouseName("spouseName");
		qualificationDetails.setSpouseSocialSecurity("654654564");
		qualificationDetails.setSpouseUSCitizen("No");
		qualificationDetails.setSpouseBirthMonth("Jan");
		qualificationDetails.setSpouseBirthDate("12");

		// Assets & Liabilities
		qualificationDetails.setCashOnHandInBanks("100000");
		qualificationDetails.setMortgages("10000");
		qualificationDetails.setMarketableSecurities("20000");
		qualificationDetails.setAccountsPayable("3000");
		qualificationDetails.setAccountsNotesReceivable("4000");
		qualificationDetails.setNotesPayable("500000");
		qualificationDetails.setRetirementPlans("60000");
		qualificationDetails.setLoansOnLifeInsurance("74000");
		qualificationDetails.setRealEstate("99999999");
		qualificationDetails.setCreditCardsTotalBalance("888888");
		qualificationDetails.setPersonalProperty("555555");
		qualificationDetails.setUnpaidTaxes("444444");
		qualificationDetails.setBusinessHoldings("222222");
		qualificationDetails.setLifeInsurance("5959595");
		qualificationDetails.setOtherAssets("4242424");
		qualificationDetails.setOtherLiabilities("9494");
		qualificationDetails.setDescriptionLiabilities("descriptionLiabilities");
		qualificationDetails.setDescriptionAssests("descriptionAssests");
		// qd.setTotalAssets("999999999");
		// qd.setTotalLiabilities("1000000");
		// qd.setTotalNetWorth("9999999999");

		// Real Estate Owned
		qualificationDetails.setRealEstateAddress1("realEstateAddress1");
		qualificationDetails.setRealEstateDatePurchased1("01/18/2019");
		qualificationDetails.setRealEstateOriginalCost1("realEstateOriginalCost1");
		qualificationDetails.setRealEstatePresentValue1("realEstatePresentValue1");
		qualificationDetails.setRealEstateMortgageBalance1("realEstateMortgageBalance1");

		qualificationDetails.setRealEstateAddress2("realEstateAddress2");
		qualificationDetails.setRealEstateDatePurchased2("01/18/2019");
		qualificationDetails.setRealEstateOriginalCost2("realEstateOriginalCost2");
		qualificationDetails.setRealEstatePresentValue2("realEstatePresentValue2");
		qualificationDetails.setRealEstateMortgageBalance2("realEstateMortgageBalance2");

		qualificationDetails.setRealEstateAddress3("realEstateAddress3");
		qualificationDetails.setRealEstateDatePurchased3("01/18/2019");
		qualificationDetails.setRealEstateOriginalCost3("realEstateOriginalCost3");
		qualificationDetails.setRealEstatePresentValue3("realEstatePresentValue3");
		qualificationDetails.setRealEstateMortgageBalance3("realEstateMortgageBalance3");

		// Annual Sources Of Income
		qualificationDetails.setSalary("54654654");
		qualificationDetails.setInvestment("3546541");
		qualificationDetails.setRealEstateIncome("54165465");
		qualificationDetails.setOther("5465465");
		qualificationDetails.setDescription("description");
		// qd.setTotalAnnualSource("9000000");

		// Total Contingent Liabilities
		qualificationDetails.setLoanCoSignature("564654");
		qualificationDetails.setLegalJudgement("45654654");
		qualificationDetails.setIncomeTaxes("54654654");
		qualificationDetails.setOtherSpecialDebt("654654");
		// qd.setTotalLiabilities("9999999");

		// Specific Data
		qualificationDetails.setWhenReadyIfApproved("whenReadyIfApproved");
		qualificationDetails.setSkillsExperience("skillsExperience");
		qualificationDetails.setEnableReachGoals("enableReachGoals");
		qualificationDetails.setResponsibleForDailyOperations("responsibleForDailyOperations");
		qualificationDetails.setCashAvailableForInvestment("responsibleForDailyOperations");
		qualificationDetails.setApprovedForFinancing("Yes");
		qualificationDetails.setAmountApprovedForFinance("999999999");
		qualificationDetails.setSoleIncomeSource("Yes");
		qualificationDetails.setContingentliabilites("No");
		qualificationDetails.setLawsuit("No");
		qualificationDetails.setConvicted("No");
		qualificationDetails.setConvictedOfFelony("No");
		qualificationDetails.setExplainConviction("explainConviction");

		qualificationDetails.setFiledBankruptcy("Yes");
		qualificationDetails.setDateFiledBankruptcy("01/10/2018");
		qualificationDetails.setDateDischargedBankruptcy("01/10/2019");

		qualificationDetails.setLocationPreference1("locationPreference1");
		qualificationDetails.setLocationPreference2("locationPreference2");
		qualificationDetails.setLocationPreference3("locationPreference3");

		qualificationDetails.setBusinessQuestion1("businessQuestion1");
		qualificationDetails.setBusinessQuestion2("businessQuestion2");
		qualificationDetails.setBusinessQuestion3("businessQuestion3");

		// Internal Analysis of Applicant
		qualificationDetails.setFirstNameApplicant("firstNameApplicant");
		qualificationDetails.setLastNameApplicant("lastNameApplicant");
		qualificationDetails.setWorkPhoneApplicant("654564654");
		qualificationDetails.setWorkPhoneExtApplicant("55454");
		qualificationDetails.setAddressApplicant("addressApplicant");
		qualificationDetails.setCityApplicant("cityApplicant");
		qualificationDetails.setCountryApplicant("USA");
		qualificationDetails.setStateProvinceApplicant("Alaska");
		qualificationDetails.setCalledAtOffice("No");

		// Heat Index Components
		qualificationDetails.setCurrentNetWorth_liquidCapitalMin("Under $499999");
		qualificationDetails.setCashAvailableForInvestment_liquidCapitalMax("Under $199999");
		qualificationDetails.setInvestmentTimeframe("Under 1 Month");
		qualificationDetails.setEmploymentBackground("Investor");
		qualificationDetails.setBackgroundCheckApproval("Yes");

		// Qualification Checklists
		qualificationDetails.setLendableNetWorth_value("lendableNetWorth_value");
		qualificationDetails.setLendableNetWorth_completed("Yes");
		qualificationDetails.setLendableNetWorth_completionDate("01/09/2019");
		qualificationDetails
				.setLendableNetWorth_associatedDocument(fc.utobj().getFilePathFromTestData("samplepdffile.pdf"));
		qualificationDetails.setLendableNetWorth_verifiedBy("lendableNetWorth_verifiedBy");
		qualificationDetails.setLendableNetWorth_date("01/21/2019");

		qualificationDetails.setBackgroundandCriminalCheck_value("backgroundandCriminalCheck_value");
		qualificationDetails.setBackgroundandCriminalCheck_completed("Yes");
		qualificationDetails.setBackgroundandCriminalCheck_CompletionDate("01/09/2019");
		qualificationDetails.setBackgroundandCriminalCheck_associatedDocument(
				fc.utobj().getFilePathFromTestData("samplepdffile.pdf"));
		qualificationDetails.setBackgroundandCriminalCheck_VerifiedBy("lendableNetWorth_verifiedBy");
		qualificationDetails.setBackgroundandCriminalCheck_Date("01/21/2019");

		qualificationDetails.setCreditCheck_value("creditCheck_value");
		qualificationDetails.setCreditCheck_completed("No");
		qualificationDetails.setCreditCheck_CompletionDate("01/09/2019");
		qualificationDetails.setCreditCheck_associatedDocument(fc.utobj().getFilePathFromTestData("samplepdffile.pdf"));
		qualificationDetails.setCreditCheck_VerifiedBy("lendableNetWorth_verifiedBy");
		qualificationDetails.setCreditCheck_Date("01/21/2019");

		qualificationDetails.setTerritoryApproved_value("territoryApproved_value");
		qualificationDetails.setTerritoryApproved_completed("No");
		qualificationDetails.setTerritoryApproved_CompletionDate("01/09/2019");
		qualificationDetails
				.setTerritoryApproved_associatedDocument(fc.utobj().getFilePathFromTestData("samplepdffile.pdf"));
		qualificationDetails.setTerritoryApproved_VerifiedBy("lendableNetWorth_verifiedBy");
		qualificationDetails.setTerritoryApproved_Date("01/21/2019");

		qualificationDetails.setFranchiseAgreementonFile_value("franchiseAgreementonFile_value");
		qualificationDetails.setFranchiseAgreementonFile_completed("lendableNetWorth_completed");
		qualificationDetails.setFranchiseAgreementonFile_CompletionDate("01/09/2019");
		qualificationDetails.setFranchiseAgreementonFile_associatedDocument(
				fc.utobj().getFilePathFromTestData("samplepdffile.pdf"));
		qualificationDetails.setFranchiseAgreementonFile_VerifiedBy("lendableNetWorth_verifiedBy");
		qualificationDetails.setFranchiseAgreementonFile_Date("01/21/2019");

		qualificationDetails.setFddReceiptonFile_value("fddReceiptonFile_value");
		qualificationDetails.setFddReceiptonFile_completed("lendableNetWorth_completed");
		qualificationDetails.setFddReceiptonFile_CompletionDate("01/09/2019");
		qualificationDetails
				.setFddReceiptonFile_associatedDocument(fc.utobj().getFilePathFromTestData("samplepdffile.pdf"));
		qualificationDetails.setFddReceiptonFile_VerifiedBy("lendableNetWorth_verifiedBy");
		qualificationDetails.setFddReceiptonFile_Date("01/21/2019");

		/*
		 * qualificationDetails.setOpener_value("opener_value");
		 * qualificationDetails.setOpener_completed("lendableNetWorth_completed"
		 * ); qualificationDetails.setOpener_CompletionDate("01/09/2019");
		 * qualificationDetails.setOpener_associatedDocument(fc.utobj().
		 * getFilePathFromTestData(config, "samplepdffile.pdf"));
		 * qualificationDetails.setOpener_VerifiedBy(
		 * "lendableNetWorth_verifiedBy");
		 * qualificationDetails.setOpener_Date("01/21/2019");
		 */

		return qualificationDetails;
	}
	
	
	/* 
	/	SALES FDD ADMIN ======================================================================================================================================
	*/

	Item23ReceiptSummary set_addItem23Receipt(Item23ReceiptSummary item23ReceiptSummary) {
		item23ReceiptSummary.setTitle("Title" + fc.utobj().generateRandomNumber());
		item23ReceiptSummary.setItem23Receipt(
				"Broker Name - $BROKER_NAME$ \n, Broker Agency - $BROKER_AGENCY$ \n, Broker Address1 - $BROKER_ADDRESS1$ \n, Broker Address2 - $BROKER_ADDRESS2$ \n, Broker City - $BROKER_CITY$ \n, Broker State - $BROKER_STATE$ \n, Broker ZipCode - $BROKER_ZIPCODE$ \n, Broker Home Phone - $BROKER_HOME_PHONE$ \n, Broker Work Phone - $BROKER_WORK_PHONE$ \n, Broker Mobile - $BROKER_MOBILE$ \n, Lead Name - $LEAD_NAME$ \n, Lead Address1 - $LEAD_ADDRESS1$ \n, Lead Address2 - $LEAD_ADDRESS2$ \n, Lead City - $CITY$ \n, Lead State - $LEAD_STATE$ \n, Lead Zip/ Postal Code - $ZIP$ \n, Lead Home Phone - $HOME_PHONE$ \n, Lead Work Phone - $WORK_PHONE$ \n, Lead Mobile - $MOBILE$ \n, Seller Name - $FRANCHISE_SELLER_NAME$ \n, Business Name - $BUSINESS_NAME$ \n, Address - $ADDRESS$ \n, State - $STATE$ \n, Country - $COUNTRY$ \n, Phone - $PHONE$, Date - $DATE$ \n");
		item23ReceiptSummary.setBusinessName("Business Name");
		item23ReceiptSummary.setAddress("Address");
		item23ReceiptSummary.setCountry("France");
		item23ReceiptSummary.setState("France");
		item23ReceiptSummary.setPhone("9015463035");
		return item23ReceiptSummary;
	}

	FDDManagement set_uploadFDD(FDDManagement fddManagement, Item23ReceiptSummary item23ReceiptSummary) {
		fddManagement.setFddName("FDD Name" + fc.utobj().generateRandomNumber());
		fddManagement.setVersion("vsersion 1");
		fddManagement.setDateOfExpiry(fc.utobj().getFutureDateUSFormat(25));
		fddManagement.setItem23Receipt_sales(item23ReceiptSummary.getTitle());
		fddManagement.getCountries().add("USA");
		fddManagement.getCountries().add("Thailand");
		fddManagement.getCountries().add("United Kingdom");
		fddManagement.setItem23Receipt_infoMgr(item23ReceiptSummary.getTitle());
		// fddManagement.setDivision("div1");
		fddManagement.getAssociateWith().add("Florida");
		fddManagement.getAssociateWith().add("Iowa");
		fddManagement.setUploadFile(fc.utobj().getFilePathFromTestData("CoursePdf.pdf"));
		fddManagement.setComments("comments");
		return fddManagement;

	}

	FDDEmailTemplateSummary set_FDDEmailTemplateSummary(FDDEmailTemplateSummary fddEmailTemplateSummary) {
		fddEmailTemplateSummary.setTitle("title" + fc.utobj().generateRandomNumber());
		fddEmailTemplateSummary.setSubject("subject" + fc.utobj().generateRandomNumber());
		fddEmailTemplateSummary.setSetAsDefaultTemplate("Yes");
		fddEmailTemplateSummary.setEmailType("upload");
		fddEmailTemplateSummary.setHtmlZipFileAttachment(fc.utobj().getFilePathFromTestData("Franchise System999"));
		fddEmailTemplateSummary.setKeywordsForReplacement(
				"Primary Info \n, Contact Information \n, First Name - $FIRST_NAME_33_1$ \n, Last Name - $LAST_NAME_33_1$ \n, Address1 - $ADDRESS1_33_1$ \n, Address2 - $ADDRESS2_33_1$ \n, City - $CITY_33_1$ \n, Country - $COUNTRY_33_1$ \n, State / Province $STATE_PROVINCE_33_1$ \n, Info Mgr \n, Center Info \n, Center Details \n, Center Name - $CENTER_NAME_1_1$ \n, Street Address - $STREET_ADDRESS_1_1$ \n, City - $CITY_1_1$ \n, State / Province - $STATE_PROVINCE_1_1$ \n, Zip / Postal Code - $ZIP_POSTAL_CODE_1_1$, Email - $EMAIL_1_1$ \n, Fax - $FAX_1_1$ \n, Phone - $PHONE_1_1$ \n, Mobile - $MOBILE_1_1$ \n, Center Contact Details \n, First Name - $FIRST_NAME_1_2$ \n, Last Name - $LAST_NAME_1_2$ \n, Others \n, Lead Owners First Name - $OWNERS_FIRST_NAME$ \n, Lead Owners Last Name - $OWNERS_LAST_NAME$ \n, Lead Owners Title - $OWNERS_TITLE$ \n, Lead Owners Address - $OWNERS_ADDRESS$ \n, Lead Owners City - $OWNERS_CITY$ \n, Lead Owners State - $OWNERS_STATE$ \n, Lead Owners Zip/Postal Code - $OWNERS_ZIP$ \n, Lead Owners Email - $OWNERS_EMAIL$ \n, Lead Owners Phone - $OWNERS_PHONE$ \n, Lead Owners Phone Extension - $OWNERS_PHONE_EXTENSION$ \n, Lead Owners Mobile - $OWNERS_MOBILE$ \n, Lead Owners Fax - $OWNERS_FAX$ \n, Lead Owner Signature - $OWNER_SIGNATURE$ \n, Sender's Name - $SENDER_NAME$ \n, User's Name - $USERID$ \n, User's Password - $PASSWORD$ \n, URL to Download FDD - $URL_TO_DOWNLOAD_FDD$ \n, New Line - $NEXT_LINE$");
		fddEmailTemplateSummary.setAttachment(fc.utobj().getFilePathFromTestData("CoursePdf.pdf"));
		return fddEmailTemplateSummary;
	}
	
	ConfigureEmailSentPriorToFDDExpiration set_ConfigureEmailSentPriorToFDDExpiration(ConfigureEmailSentPriorToFDDExpiration configureEmailSentPriorToFDDExpiration)
	{
		configureEmailSentPriorToFDDExpiration.setSendEmailNotification("yes");
		configureEmailSentPriorToFDDExpiration.setEmailSubject("email subject" +fc.utobj().generateRandomNumber());
		configureEmailSentPriorToFDDExpiration.setDaysPrior("2");
		configureEmailSentPriorToFDDExpiration.setFromEmail("support@franconnect.com");
		configureEmailSentPriorToFDDExpiration.getRecipients().add("FranConnect Administrator");
		//configureEmailSentPriorToFDDExpiration.getRecipients().add("");
		configureEmailSentPriorToFDDExpiration.setEmailContentPriorToFDDExpiration("This is body of email $FDD_NAME$ , $EXPIRY_DATE$, $VERSION$, $UPLOADING_DATE$");
		return configureEmailSentPriorToFDDExpiration;
	}

	Broker fillDefaultValue_Brokers_FullInfo(Broker broker) throws Exception {
		fc.utobj().printTestStep("Fill Broker");

		broker.setBrokerType("Brokers");
		broker.setFirstName(fc.utobj().generateTestData("Bfn"));
		broker.setLastName(fc.utobj().generateTestData("Bln"));
		broker.setAddress1("address1");
		broker.setAddress2("address2");
		broker.setCity("city");
		broker.setCountry("USA");
		broker.setZipPostalCode("201306");
		broker.setStateProvince("Alaska");
		broker.setHomePhone("13165485");
		broker.setWorkPhone("35154411");
		broker.setFax("356253");
		broker.setMobile("63456324");
		broker.setBestTimeToContact("9");
		broker.setPrimaryPhoneToCall("Mobile");
		broker.setEmail("frantest2017@gmail.com");
		broker.setAgency("None");
		broker.setPriority("Hot");
		broker.setComments("comments");
		return broker;
	}

	ConfigureEmailSentPriorToFDDEmail fillDefaultValue_EmailSentPriorToFDDEmail(
			ConfigureEmailSentPriorToFDDEmail configureEmailSentPriorToFDDEmail) {
		configureEmailSentPriorToFDDEmail.setSendEmailNotification("yes");
		configureEmailSentPriorToFDDEmail.setEmailSubject("Email Subject 123");
		configureEmailSentPriorToFDDEmail.setTimeIntervalMinutes("0");
		configureEmailSentPriorToFDDEmail.setKeywordsForReplacement(
				"Sales prior to FDD sent email First Name $FIRST_NAME_33_1$ Last Name $LAST_NAME_33_1$ Address1 $ADDRESS1_33_1$ Address2 $ADDRESS2_33_1$ Lead City $CITY_33_1$ Country $COUNTRY_33_1$ State / Province $STATE_PROVINCE_33_1$Lead Owners First Name $OWNERS_FIRST_NAME$ Lead Owners Last Name $OWNERS_LAST_NAME$ Lead Owners Title $OWNERS_TITLE$ Lead Owners Address $OWNERS_ADDRESS$Lead Owners City $OWNERS_CITY$ Lead Owners State $OWNERS_STATE$ Lead Owners Zip/Postal Code $OWNERS_ZIP$Lead Owners Email $OWNERS_EMAIL$ Lead Owners Phone $OWNERS_PHONE$ Lead Owners Phone Extension $OWNERS_PHONE_EXTENSION$Lead Owners Mobile $OWNERS_MOBILE$ Lead Owners Fax $OWNERS_FAX$ Lead Owner Signature $OWNER_SIGNATURE$ Sender's Name $SENDER_NAME$ New Line $NEXT_LINE$ Username: $USERID$ Password: $PASSWORD$ Website: $URL_TO_DOWNLOAD_FDD$");
		configureEmailSentPriorToFDDEmail
				.setEmailContentPriorToFDDEmail(configureEmailSentPriorToFDDEmail.getKeywordsForReplacement());
		return configureEmailSentPriorToFDDEmail;
	}

	void addAttachment(String fileName, WebDriver driver) throws Exception {
		fc.utobj().clickElement(driver, driver.findElement(By.xpath(".//a[contains(text(),'Attachment')]")));
		fc.utobj().switchFrameById(driver, "cboxIframe");
		fc.utobj().sendKeys(driver, driver.findElement(By.name("attachmentName")), fileName);
		fc.utobj().clickElement(driver, driver.findElement(By.xpath(".//input[@value='Attach']")));
		fc.utobj().clickElement(driver, driver.findElement(By.xpath(".//input[@value='Done']")));
	}
	
	SendFDD set_sendFDD(SendFDD sendFDD , FDDEmailTemplateSummary fddEmailTemplateSummary , FDDManagement fddManagement)
	{
		sendFDD.setFromReply("Custom"); //email address
		sendFDD.setCustomText_fromReply("frantest2017@gmail.com");
		sendFDD.setCountry("USA");
		sendFDD.setState("Alaska");
		sendFDD.setFddEmailTemplates(fddEmailTemplateSummary.getTitle());
		sendFDD.setVersionOfFDDShouldBeSent("Let me select");
		sendFDD.setSelectFDD(fddManagement.getFddName());
		sendFDD.setDefaultSeller("Default Seller");
		sendFDD.getSellerNames().add("FranConnect Administrator");
	//	sendFDD.getSellerNames().add("reg 1");
	//	sendFDD.setSubject("Send FDD Subject" + fc.utobj().generateRandomNumber());
		sendFDD.setAttachment(fc.utobj().getFilePathFromTestData("keywords.html"));
		sendFDD.setSendFDDtoCoApplicants("yes");
		sendFDD.setEmailBody("Dear $FIRST_NAME$ , We are pleased to send you a copy of our Franchise Disclosure Document (FDD) , Set forth below is a Username, Password and Website information for accessing and downloading our Franchise Disclosure Document. The FDD is an important document and provides you with some basic information about our company, the franchise that we offer, and our franchise system Please carefully read the entire FDD (including all of the exhibits). The FDD is in “PDF” format. You will need a copy of “Adobe Reader”. You probably already have this on your computer, but in case you don't, You can download a free version of Adobe Acrobat Reader by going to www.adobe.com.) Username: $USERID$ Password: $PASSWORD$ Website: $URL_TO_DOWNLOAD_FDD$ When you open the FDD, please click on the link on the first page. That page is the receipt, which just confirms that you have received the document. Alternatively, you can receive the FDD (a) as an Adobe Portable Document Format (PDF) File on a CD ROM disc, (b) as a PDF attachment to an e-mail message, or (c) on paper as a two-sided hardcopy. If you prefer to have the FDD delivered in one of these other ways, please reply to this e-mail indicating which method you prefer. If you choose to download the PDF or receive it via email, you will need internet access and a valid e-mail account, in addition to Adobe Acrobat Reader. You will also need access to a printer to print the Receipt exhibit, and also to print a paper copy for yourself. The FDD is confidential, subject to the mutual confidentiality agreement you have signed, and contains proprietary information, including trade secrets. Neither the FDD nor any of the information contained in the FDD may be reproduced or disclosed to any person under any circumstances without our written permission. $SENDER_NAME$");
		return sendFDD;
	}
	
	void set_LogOnCredentialsDuration(LogOnCredentialsDuration logOnCredentialsDuration)
	{
		logOnCredentialsDuration.setLogOnCredentialsDuration("15 Days");
	}

	void addLeadThroughWebServices(WebDriver driver, Lead lead) throws Exception {
		fc.utobj().printTestStep("Add a Lead Through WebServices");

		String module = "fs";
		String subModule = "lead";
		String isISODate = "no";
		
		        /*StringBuilder divisionString = new StringBuilder();
		        for(String div : lead.getDivisionList())
		        {
		            divisionString.append(div);
		            divisionString.append(',');
		        }*/

		String xmlString = "<fcRequest>" + "<fsLead>" + "<salutation>" + lead.getSalutation() + "</salutation>"
				+ "<firstName>" + lead.getFirstName() + "</firstName>" + "<lastName>" + lead.getLastName()
				+ "</lastName>" + "<address>" + lead.getAddress1() + "</address>" + "<address2>" + lead.getAddress2()
				+ "</address2>" + "<city>" + lead.getCity() + "</city>" + "<country>" + lead.getCountry() + "</country>"
				+ "<stateID>" + lead.getStateProvince() + "</stateID>" + "<sendAutomaticMail>"
				+ lead.getSendAutomaticMail() + "</sendAutomaticMail>" + "<zip>" + lead.getZipPostalCode() + "</zip>"
				+ "<countyID>" + lead.getCounty() + "</countyID>" + "<primaryPhoneToCall>"
				+ lead.getPreferredModeofContact() + "</primaryPhoneToCall>" + "<bestTimeToContact>"
				+ lead.getBestTimeToContact() + "</bestTimeToContact>" + "<phone>" + lead.getWorkPhone() + "</phone>"
				+ "<phoneExt>" + lead.getWorkPhoneExtension() + "</phoneExt>" + "<homePhone>" + lead.getHomePhone()
				+ "</homePhone>" + "<homePhoneExt>" + lead.getHomePhoneExtension() + "</homePhoneExt>" + "<fax>"
				+ lead.getFax() + "</fax>" + "<mobile>" + lead.getMobile() + "</mobile>" + "<emailID>" + lead.getEmail()
				+ "</emailID>" + "<companyName>" + lead.getCompanyName() + "</companyName>" + "<comments>"
				+ lead.getComment() + "</comments>" + "<basedOnAssignmentRule>" + lead.getBasedonAssignmentRules()
				+ "</basedOnAssignmentRule>" + "<leadOwnerID>" + lead.getLeadOwner() + "</leadOwnerID>"
				+ "<leadOwnerReferenceId>" + "</leadOwnerReferenceId>" + "<leadRatingID>" + lead.getLeadRating()
				+ "</leadRatingID>" + "<marketingCodeId>" + lead.getMarketingCode() + "</marketingCodeId>"
				+ "<leadSource2ID>" + lead.getLeadSourceCategory() + "</leadSource2ID>" + "<leadSource3ID>"
				+ lead.getLeadSourceDetails() + "</leadSource3ID>" + "<otherLeadSourceDetail>"
				+ lead.getOtherLeadSources() + "</otherLeadSourceDetail>" + "<liquidCapitalMin>"
				+ lead.getCashAvailableforInvestment() + "</liquidCapitalMin>" + "<liquidCapitalMax>"
				+ lead.getCurrentNetWorth() + "</liquidCapitalMax>" + "<investTimeframe>"
				+ lead.getInvestmentTimeframe() + "</investTimeframe>" + "<background>" + lead.getBackground()
				+ "</background>" + "<sourceOfFunding>" + lead.getSourceOfInvestment() + "</sourceOfFunding>"
				+ "<nextCallDate>" + lead.getNextCallDate() + "</nextCallDate>" + "<divisionReferenceId>"
				+lead.getDivision()+ "</divisionReferenceId>" + "<division>" + lead.getDivision() + "</division>"
				+ "<noOfUnitReq>" + lead.getNoOfUnitsLocationsRequested() + "</noOfUnitReq>" + "<locationId1>"
				+ lead.getNewAvailableSites() + "</locationId1>" + "<locationId1b>" + lead.getExistingSites()
				+ "</locationId1b>" + "<locationId2>" + lead.getResaleSites() + "</locationId2>" + "<preferredCity1>"
				+ lead.getPreferredCity1() + "</preferredCity1>" + "<preferredCountry1>" + lead.getPreferredCountry1()
				+ "</preferredCountry1>" + "<preferredStateId1>" + lead.getPreferredStateProvince1()
				+ "</preferredStateId1>" + "<preferredCity2>" + lead.getPreferredCity2() + "</preferredCity2>"
				+ "<preferredCountry2>" + lead.getPreferredCountry2() + "</preferredCountry2>" + "<preferredStateId2>"
				+ lead.getPreferredStateProvince2() + "</preferredStateId2>" + "<forecastClosureDate>"
				+ lead.getForecastClosureDate() + "</forecastClosureDate>" + "<probability>" + lead.getProbability()
				+ "</probability>" + "<forecastRating>" + lead.getForecastRating() + "</forecastRating>"
				+ "<forecastRevenue>" + lead.getForecastRevenue() + "</forecastRevenue>"
				+ "<basedOnWorkflowAssignmentRule>" + "</basedOnWorkflowAssignmentRule>" + "<campaignID>"
				+ lead.getCampaignName() + "</campaignID>" + "</fsLead>" + "</fcRequest>";

		WebService rest = new WebService();
		rest.create(module, subModule, xmlString, isISODate);
		// System.out.println(rest.create(module, subModule, xmlString, isISODate));
	}
	
	void logACallThroughWebServices(String referenceID , Call call) throws Exception {
		String module = "fs";
		String subModule = "call";
		String isISODate = "no";
		
		String xmlString = "<fcRequest>" + "<fsCall>"
		+ "<parentReferenceId>" + referenceID + "</parentReferenceId>"
		+ "<subject>" + call.getSubject() + "</subject>"
		+ "<date>" + call.getDate() + "</date>"
		+ "<callStatus>" + call.getCallStatus() + "</callStatus>"
		+ "<callType>" + call.getCommunicationType()+ "</callType>"
		+ "<comments>" + call.getComments() + "</comments>"
		+ "<timeAdded>" + call.getTimeHH() + ":" + call.getTimeMM()+ "Z" + "</timeAdded>"
		+ "</fsCall>" + "</fcRequest>" ;
		
		System.out.println(xmlString);
		
		WebService rest = new WebService();
		rest.create(module, subModule, xmlString, isISODate);
	}
	
	void updateLeadThroughWebServices(WebDriver driver, Lead lead , String leadReferenceId) throws Exception {
		fc.utobj().printTestStep("Add a Lead Through WebServices");

		String module = "fs";
		String subModule = "lead";
		String isISODate = "no";
		
		        /*StringBuilder divisionString = new StringBuilder();
		        for(String div : lead.getDivisionList())
		        {
		            divisionString.append(div);
		            divisionString.append(',');
		        }*/

		String xmlString = "<fcRequest>" + "<fsLead>" + "<referenceId>"+leadReferenceId+"</referenceId>"+"<salutation>" + lead.getSalutation() + "</salutation>"
				+ "<firstName>" + lead.getFirstName() + "</firstName>" + "<lastName>" + lead.getLastName()
				+ "</lastName>" + "<address>" + lead.getAddress1() + "</address>" + "<address2>" + lead.getAddress2()
				+ "</address2>" + "<city>" + lead.getCity() + "</city>" + "<country>" + lead.getCountry() + "</country>"
				+ "<stateID>" + lead.getStateProvince() + "</stateID>" + "<sendAutomaticMail>"
				+ lead.getSendAutomaticMail() + "</sendAutomaticMail>" + "<zip>" + lead.getZipPostalCode() + "</zip>"
				+ "<countyID>" + lead.getCounty() + "</countyID>" + "<primaryPhoneToCall>"
				+ lead.getPreferredModeofContact() + "</primaryPhoneToCall>" + "<bestTimeToContact>"
				+ lead.getBestTimeToContact() + "</bestTimeToContact>" + "<phone>" + lead.getWorkPhone() + "</phone>"
				+ "<phoneExt>" + lead.getWorkPhoneExtension() + "</phoneExt>" + "<homePhone>" + lead.getHomePhone()
				+ "</homePhone>" + "<homePhoneExt>" + lead.getHomePhoneExtension() + "</homePhoneExt>" + "<fax>"
				+ lead.getFax() + "</fax>" + "<mobile>" + lead.getMobile() + "</mobile>" + "<emailID>" + lead.getEmail()
				+ "</emailID>" + "<companyName>" + lead.getCompanyName() + "</companyName>" + "<comments>"
				+ lead.getComment() + "</comments>" + "<basedOnAssignmentRule>" + lead.getBasedonAssignmentRules()
				+ "</basedOnAssignmentRule>" + "<leadOwnerID>" + lead.getLeadOwner() + "</leadOwnerID>"
				+ "<leadOwnerReferenceId>" + "</leadOwnerReferenceId>" + "<leadRatingID>" + lead.getLeadRating()
				+ "</leadRatingID>" + "<marketingCodeId>" + lead.getMarketingCode() + "</marketingCodeId>"
				+ "<leadSource2ID>" + lead.getLeadSourceCategory() + "</leadSource2ID>" + "<leadSource3ID>"
				+ lead.getLeadSourceDetails() + "</leadSource3ID>" + "<otherLeadSourceDetail>"
				+ lead.getOtherLeadSources() + "</otherLeadSourceDetail>" + "<liquidCapitalMin>"
				+ lead.getCashAvailableforInvestment() + "</liquidCapitalMin>" + "<liquidCapitalMax>"
				+ lead.getCurrentNetWorth() + "</liquidCapitalMax>" + "<investTimeframe>"
				+ lead.getInvestmentTimeframe() + "</investTimeframe>" + "<background>" + lead.getBackground()
				+ "</background>" + "<sourceOfFunding>" + lead.getSourceOfInvestment() + "</sourceOfFunding>"
				+ "<nextCallDate>" + lead.getNextCallDate() + "</nextCallDate>" + "<divisionReferenceId>"
				+lead.getDivision()+ "</divisionReferenceId>" + "<division>" + lead.getDivision() + "</division>"
				+ "<noOfUnitReq>" + lead.getNoOfUnitsLocationsRequested() + "</noOfUnitReq>" + "<locationId1>"
				+ lead.getNewAvailableSites() + "</locationId1>" + "<locationId1b>" + lead.getExistingSites()
				+ "</locationId1b>" + "<locationId2>" + lead.getResaleSites() + "</locationId2>" + "<preferredCity1>"
				+ lead.getPreferredCity1() + "</preferredCity1>" + "<preferredCountry1>" + lead.getPreferredCountry1()
				+ "</preferredCountry1>" + "<preferredStateId1>" + lead.getPreferredStateProvince1()
				+ "</preferredStateId1>" + "<preferredCity2>" + lead.getPreferredCity2() + "</preferredCity2>"
				+ "<preferredCountry2>" + lead.getPreferredCountry2() + "</preferredCountry2>" + "<preferredStateId2>"
				+ lead.getPreferredStateProvince2() + "</preferredStateId2>" + "<forecastClosureDate>"
				+ lead.getForecastClosureDate() + "</forecastClosureDate>" + "<probability>" + lead.getProbability()
				+ "</probability>" + "<forecastRating>" + lead.getForecastRating() + "</forecastRating>"
				+ "<forecastRevenue>" + lead.getForecastRevenue() + "</forecastRevenue>"
				+ "<basedOnWorkflowAssignmentRule>" + "</basedOnWorkflowAssignmentRule>" + "<campaignID>"
				+ lead.getCampaignName() + "</campaignID>" + "</fsLead>" + "</fcRequest>";

				WebService rest = new WebService();
				rest.update(module, subModule, xmlString, isISODate);
	}
	
	void getLeadDetailsThroughWebServices_FullForm(WebDriver driver, Lead lead) throws Exception
	{
		fc.utobj().printTestStep("Retrieve Lead Info Through WebServices");

		String module = "fs";
		String subModule = "lead";
		String isISODate = "no";

		String xmlString = 
				"<fcRequest>"
				+"<filter>"
				+"<referenceId>"+"</referenceId>"
				+"<requestDateFrom>"+"</requestDateFrom>"
				+"<requestDateTo>"+"</requestDateTo>"
				+"<salutation>"+lead.getSalutation()+"</salutation>"
				+"<firstName>"+lead.getFirstName()+"</firstName>"
				+"<lastName>"+lead.getLastName()+"</lastName>"
				+"<address>"+lead.getAddress1()+"</address>"
				+"<address2>"+lead.getAddress2()+"</address2>"
				+"<city>"+lead.getCity()+"</city>"
				+"<country>"+lead.getCountry()+"</country>"
				+"<stateID>"+lead.getStateProvince()+"</stateID>"
				+"<zip>"+lead.getZipPostalCode()+"</zip>"
				+"<countyID>"+lead.getCountry()+"</countyID>"
				+"<primaryPhoneToCall>"+lead.getPreferredModeofContact()+"</primaryPhoneToCall>"
				+"<bestTimeToContact>"+lead.getBestTimeToContact()+"</bestTimeToContact>"
				+"<phone>"+lead.getWorkPhone()+"</phone>"
				+"<phoneExt>"+lead.getWorkPhoneExtension()+"</phoneExt>"
				+"<homePhone>"+lead.getHomePhone()+"</homePhone>"
				+"<homePhoneExt>"+lead.getHomePhoneExtension()+"</homePhoneExt>"
				+"<fax>"+lead.getFax()+"</fax>"
				+"<mobile>"+lead.getMobile()+"</mobile>"
				+"<emailID>"+lead.getEmail()+"</emailID>"
				+"<companyName>"+lead.getCompanyName()+"</companyName>"
				+"<comments>"+lead.getComment()+"</comments>"
				+"<leadStatusID>"+lead.getLeadStatus()+"</leadStatusID>"
				+"<leadOwnerID>"+lead.getLeadOwner()+"</leadOwnerID>"
				+"<leadRatingID>"+lead.getLeadRating()+"</leadRatingID>"
				+"<leadKilledReason>"+"</leadKilledReason>"
				+"<leadSource2ID>"+"</leadSource2ID>"
				+"<leadSource3ID>"+"</leadSource3ID>"
				+"<otherLeadSourceDetail>"+"</otherLeadSourceDetail>"
				+"<marketingCodeId>"+"</marketingCodeId>"
				+"<liquidCapitalMin>"+"</liquidCapitalMin>"
				+"<liquidCapitalMax>"+"</liquidCapitalMax>"
				+"<investTimeframe>"+"</investTimeframe>"
				+"<background>"+"</background>"
				+"<sourceOfFunding>"+"</sourceOfFunding>"
				+"<nextCallDateFrom>"+"</nextCallDateFrom>"
				+"<nextCallDateTo>"+"</nextCallDateTo>"
				+"<noOfUnitReq>"+"</noOfUnitReq>"
				+"<franchiseAwarded>"+"</franchiseAwarded>"
				+"<noOfFieldSold>"+"</noOfFieldSold>"
				+"<dateOfOpenFrom>"+"</dateOfOpenFrom>"
				+"<dateOfOpenTo>"+"</dateOfOpenTo>"
				+"<statusChangeDateFrom>"+"</statusChangeDateFrom>"
				+"<statusChangeDateTo>"+"</statusChangeDateTo>"
				+"<webSiteLead>"+"</webSiteLead>"
				+"<forecastClosureDateFrom>"+"</forecastClosureDateFrom>"
				+"<forecastClosureDateTo>"+"</forecastClosureDateTo>"
				+"<probability>"+"</probability>"
				+"<forecastRating>"+"</forecastRating>"
				+"<forecastRevenue>"+"</forecastRevenue>"
				+"<coApplicantRelationshipID>"+"</coApplicantRelationshipID>"
				+"<ipAddress>"+"</ipAddress>"
				+"<browserType>"+"</browserType>"
				+"<preferredCountry1>"+"</preferredCountry1>"
				+"<preferredCity1>"+"</preferredCity1>"
				+"<preferredStateId1>"+"</preferredStateId1>"
				+"<preferredCountry2>"+"</preferredCountry2>"
				+"<preferredStateId2>"+"</preferredStateId2>"
				+"<preferredCity2>"+"</preferredCity2>"
				+"<lastUpdateFrom>"+"</lastUpdateFrom>"
				+"<lastUpdateTo>"+"</lastUpdateTo>"
				+"<lastAttendedFrom>"+"</lastAttendedFrom>"
				+"<lastAttendedTo>"+"</lastAttendedTo>"
				+"<bounceEmailStatus>"+"</bounceEmailStatus>"
				+"<spamEmailStatus>"+"</spamEmailStatus>"
				+"<bounceReason>"+"</bounceReason>"
				+"<leadOwnerReferenceId>"+"</leadOwnerReferenceId>"
				+"<divisionReferenceId>"+"</divisionReferenceId>"
				+"<division>"+"</division>"
				+"</filter>"
				+"</fcRequest>";

		WebService rest = new WebService();
		System.out.println(rest.retrieve(module, subModule, xmlString, isISODate));
	}
	
	String getLeadDetailsThroughWebServices_MajorFields(WebDriver driver, Lead lead) throws Exception
	{
		fc.utobj().printTestStep("Retrieve Lead Info Through WebServices");
		
		String module = "fs";
		String subModule = "lead";
		String isISODate = "no";
		
		String xmlString = 
				"<fcRequest>"
						+"<filter>"
						/*+"<referenceId>"+"</referenceId>"
						+"<requestDateFrom>"+"</requestDateFrom>"
						+"<requestDateTo>"+"</requestDateTo>"*/
						+"<firstName>"+lead.getFirstName()+"</firstName>"
						+"<lastName>"+lead.getLastName()+"</lastName>"
						/*+"<emailID>"+lead.getEmail()+"</emailID>"
						+"<leadStatusID>"+lead.getLeadStatus()+"</leadStatusID>"
						+"<leadOwnerID>"+lead.getLeadOwner()+"</leadOwnerID>"
						+"<leadRatingID>"+lead.getLeadRating()+"</leadRatingID>"*/
						+"</filter>"
						+"</fcRequest>";
		
		WebService rest = new WebService();
		
		String response = rest.retrieve(module, subModule, xmlString, isISODate);
		
		// System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>\n\n\n\n\n\n"+response);
		return response;
	}
	
	String getLeadDetailsThroughWebServices_MajorFields(String firstName , String lastName) throws Exception
	{
		fc.utobj().printTestStep("Retrieve Lead Info Through WebServices");
		
		String module = "fs";
		String subModule = "lead";
		String isISODate = "no";
		
		String xmlString = 
				"<fcRequest>"
						+"<filter>"
						/*+"<referenceId>"+"</referenceId>"
						+"<requestDateFrom>"+"</requestDateFrom>"
						+"<requestDateTo>"+"</requestDateTo>"*/
						+"<firstName>"+firstName+"</firstName>"
						+"<lastName>"+lastName+"</lastName>"
						/*+"<emailID>"+lead.getEmail()+"</emailID>"
						+"<leadStatusID>"+lead.getLeadStatus()+"</leadStatusID>"
						+"<leadOwnerID>"+lead.getLeadOwner()+"</leadOwnerID>"
						+"<leadRatingID>"+lead.getLeadRating()+"</leadRatingID>"*/
						+"</filter>"
						+"</fcRequest>";
		
		WebService rest = new WebService();
		
		String response = rest.retrieve(module, subModule, xmlString, isISODate);
		
		// System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>\n\n\n\n\n\n"+response);
		return response;
	}
	
	String updateLeadDetailsThroughWebServices_EmailOptIn(WebDriver driver , String referenceID) throws Exception
	{
		fc.utobj().printTestStep("Update Lead Info Through WebServices");
		
		String module = "fs";
		String subModule = "lead";
		String isISODate = "no";
		
		String xmlString = 
							"<fcRequest>"
							+"<fsLead>"
							+"<referenceId>"+referenceID+"</referenceId>"
							/*+"<requestDateFrom>"+"</requestDateFrom>"
							+"<requestDateTo>"+"</requestDateTo>"*/
							/*+"<firstName>"+lead.getFirstName()+"</firstName>"
							+"<lastName>"+lead.getLastName()+"</lastName>"*/
							+"<unsubscribe>"+"Opt-in"+"</unsubscribe>"
							/*+"<emailID>"+lead.getEmail()+"</emailID>"
							+"<leadStatusID>"+lead.getLeadStatus()+"</leadStatusID>"
							+"<leadOwnerID>"+lead.getLeadOwner()+"</leadOwnerID>"
							+"<leadRatingID>"+lead.getLeadRating()+"</leadRatingID>"*/
							+"</fsLead>"
							+"</fcRequest>";
		
		System.out.println(xmlString);
		
		WebService rest = new WebService();
		
		String response = rest.update(module, subModule, xmlString, isISODate );
		
		System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>\n\n\n\n\n\n"+response);
		return response;
				
	}
	
	boolean verifyDatainWebServiceResponse(WebDriver driver,String response,String value) throws Exception
	{
		System.out.println("Response : "+response);
		System.out.println("Value : "+value);
		fc.utobj().printTestStep("Validate that the lead is assigned to : "+value);
		if(response.toLowerCase().contains(value.toLowerCase()))
		{
			fc.utobj().printTestStep("Assigned to "+value);
			return true;
		}else
		{
			fc.utobj().printTestStep("Not Assigned to "+value);
			return false;
		}
	}
	
	// Akshat 
	private static Document loadXMLFromString(String xml) throws Exception {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		InputSource is = new InputSource(new StringReader(xml));
		return builder.parse(is);
	}

	 String getValFromXML(String xmlStr , String tagName) throws Exception {

		String value="";
		Document doc = StrtoXML_AkshatTest.loadXMLFromString(xmlStr);
		doc.getDocumentElement().normalize();
		NodeList ag = doc.getElementsByTagName("fcResponse");

		for (int temp = 0; temp < ag.getLength(); temp++) {

			org.w3c.dom.Node nNode = ag.item(temp);

			// System.out.println("\nCurrent Element :" + nNode.getNodeName());

			if (nNode.getNodeType() == org.w3c.dom.Node.ELEMENT_NODE) {
				Element eElement = (Element) nNode;
				value = eElement.getElementsByTagName(tagName).item(0).getTextContent();
			}
		}
		
		return value;
	}
	
	
	
	 AdminCorporateUser set_addCorporateUser(AdminCorporateUser adminCorporateUser)
	{
		// User Details
		adminCorporateUser.setLoginID("corp"+fc.utobj().generateRandomNumber());
		adminCorporateUser.setPassword("sdg@1a@Hfs45");
		adminCorporateUser.setConfirmPassword("sdg@1a@Hfs45");
		adminCorporateUser.setUserType("Normal User");
	//	adminCorporateUser.setExpirationTime();
		adminCorporateUser.getRole().add("Corporate Administrator");
		adminCorporateUser.getRole().add("Default Corporate Role");
		adminCorporateUser.setTimeZone("GMT +05:30 India");
		adminCorporateUser.setAllowDST("no");
		// Personal Details
		adminCorporateUser.setJobTitle("corp user title");
		adminCorporateUser.setLanguage("English");
		adminCorporateUser.setFirstName(adminCorporateUser.getLoginID());
		adminCorporateUser.setLastName("lastName"+fc.utobj().generateRandomNumber());
		adminCorporateUser.setAddress("address");
		adminCorporateUser.setCity("city");
		adminCorporateUser.setCountry("USA");
		adminCorporateUser.setZipPostalCode("561454");
		adminCorporateUser.setStateProvince("Alaska");
		adminCorporateUser.setPhone1("123456");
		adminCorporateUser.setPhone1Extension("123");
		adminCorporateUser.setPhone2("456789");
		adminCorporateUser.setPhone2Extension("789");
		adminCorporateUser.setFax("987654321");
		adminCorporateUser.setMobile("123456789");
		adminCorporateUser.setEmail("xyz@gmail.com");
	//	adminCorporateUser.setIpAddress("");
		adminCorporateUser.setIsBillable("yes");
		adminCorporateUser.setConsultant("no");
		adminCorporateUser.setUploadUserPicture(fc.utobj().getFilePathFromTestData("001_Fish-Wallpaper-HD.jpg"));
		adminCorporateUser.setSendNotification("yes");
		return adminCorporateUser;
	}
	
	AdminCorporateUser fillDefaultDataFor_CorporateUser(AdminCorporateUser adminCorporateUser) throws Exception {

		if (adminCorporateUser.getLoginID() == null) {
			adminCorporateUser.setLoginID(fc.utobj().generateTestData("c"));
		}
		if (adminCorporateUser.getPassword() == null) {
			adminCorporateUser.setPassword("123456789");
		}
		if (adminCorporateUser.getConfirmPassword() == null) {
			adminCorporateUser.setConfirmPassword("123456789");
		}
		if (adminCorporateUser.getUserType() == null) {
			adminCorporateUser.setUserType("Normal User");
		}
		if (adminCorporateUser.getRole() == null) {
			adminCorporateUser.getRole().add("Corporate Administrator");
		}
		if (adminCorporateUser.getTimeZone() == null) {
			adminCorporateUser.setTimeZone("GMT -05:00 US/Canada/Eastern");
		}
		if (adminCorporateUser.getFirstName() == null) {
			adminCorporateUser.setFirstName(fc.utobj().generateTestData("cu"));
		}
		if (adminCorporateUser.getLastName() == null) {
			adminCorporateUser.setLastName(fc.utobj().generateTestData("cu"));
		}
		if (adminCorporateUser.getCity() == null) {
			adminCorporateUser.setCity("Reston");
		}
		if (adminCorporateUser.getCountry() == null) {
			adminCorporateUser.setCountry("USA");
		}
		if (adminCorporateUser.getStateProvince() == null) {
			adminCorporateUser.setStateProvince("Virginia");
		}
		if (adminCorporateUser.getPhone1() == null) {
			adminCorporateUser.setPhone1(fc.utobj().generateRandomNumber6Digit() + 3333);
		}
		if (adminCorporateUser.getEmail() == null) {
			adminCorporateUser.setEmail(fc.utobj().generateTestData("e") + "@fran.com");
		}
		if (adminCorporateUser.getSendNotification() == null) {
			adminCorporateUser.setSendNotification("N");
		}
		if (adminCorporateUser.getIsBillable() == null) {
			adminCorporateUser.setIsBillable("N");
		}
		if (adminCorporateUser.getAllowDST() == null) {
			adminCorporateUser.setAllowDST("Y");
		}

		return adminCorporateUser;
	}
	
	// SALES TERRITORY SETTERS
	
	SalesTerritories setGroupByZipPostalCode_Domestic_filladdNewSalesTerritory(SalesTerritories salesTerritories)
	{
		salesTerritories.setCategory("Domestic");
		salesTerritories.setSalesTerritoryName("Territory_Zip_Dom "+fc.utobj().generateRandomNumber());
		salesTerritories.setGroupBy("Zip / Postal Code");
		salesTerritories.getCountries().add("USA");
		salesTerritories.setZipPostalCode("Enter a comma separated list of Zip / Postal Code manually");
		salesTerritories.setZipPostalCodeText(fc.utobj().generateRandomNumber6Digit());
		return salesTerritories;
	}
	
	SalesTerritories setGroupByStates_Domestic_filladdNewSalesTerritory(SalesTerritories salesTerritories)
	{
		salesTerritories.setCategory("Domestic");
		salesTerritories.setSalesTerritoryName("Territory_States_Dom "+fc.utobj().generateRandomNumber());
		salesTerritories.setGroupBy("States");
		salesTerritories.getCountries().add("USA");
		salesTerritories.getStates().add("Arizona");
		salesTerritories.setZipPostalCodeText(fc.utobj().generateRandomNumber6Digit());
		return salesTerritories;
	}
	
	SalesTerritories setGroupByStates_International_filladdNewSalesTerritory(SalesTerritories salesTerritories)
	{
		salesTerritories.setCategory("International");
		salesTerritories.setSalesTerritoryName("Territory_States_Int "+fc.utobj().generateRandomNumber());
		salesTerritories.setGroupBy("States");
		salesTerritories.getCountries().add("United Kingdom");
		salesTerritories.getStates().add("Berkshire");
		salesTerritories.setZipPostalCodeText(fc.utobj().generateRandomNumber6Digit());
		return salesTerritories;
	}
	
	SalesTerritories setGroupByCounty_Domestic_filladdNewSalesTerritory(SalesTerritories salesTerritories)
	{
		salesTerritories.setCategory("Domestic");
		salesTerritories.setSalesTerritoryName("Territory_County_Dom "+fc.utobj().generateRandomNumber());
		salesTerritories.setGroupBy("County");
		// salesTerritories.setCountry("USA");
		salesTerritories.getCountries().add("USA");
		salesTerritories.getStates().add("Florida");
		salesTerritories.getCounties().add("Gulf");
		salesTerritories.setZipPostalCodeText(fc.utobj().generateRandomNumber6Digit());
		return salesTerritories;
	}
	
	
	
	void selectAllUsersInList_SelectTag(WebDriver driver, String selectTag_id) throws Exception {
		Select select = new Select(fc.utobj().getElementByID(driver, selectTag_id));
		List<WebElement> options = select.getOptions();

		try {
			for (int i = 0; i < options.size(); i++)

			{
				select.selectByIndex(i);
			}
			
			
		} catch (Exception e) {
			Reporter.log(e.getMessage());
			fc.utobj().throwsException("Was not able to select all the values in the list box");
		}
	}
	
	ManageWebFormGenerator set_ManageWebform(ManageWebFormGenerator manageWebFormGenerator) throws Exception
	{
		manageWebFormGenerator.setFormName("WebForm" +fc.utobj().generateRandomNumber());
		manageWebFormGenerator.setFormTitle("WebFormTitle " + manageWebFormGenerator.getFormName());
		manageWebFormGenerator.setDisplayFormTitle("Yes");
		manageWebFormGenerator.setFormDescription("WebForm Description" +fc.utobj());
		manageWebFormGenerator.setFormURL(manageWebFormGenerator.getFormName());
		
		manageWebFormGenerator.setLeadStatus("New Lead");
		manageWebFormGenerator.setLeadOwner("Based on Assignment Rules");
		manageWebFormGenerator.setLeadSourceCategory("Internet");
		manageWebFormGenerator.setLeadSourceDetails("BISON");
		manageWebFormGenerator.setAfterSubmission("Confirmation Message");
		manageWebFormGenerator.setBodyText("Thank you for submitting your information, we will get back to you shortly.");
		
		return manageWebFormGenerator;
	}
	
	ManageAreaRegion set_addAreaRegion(ManageAreaRegion manageAreaRegion) throws Exception
	{
		manageAreaRegion.setCategory("Domestic");
		manageAreaRegion.setAreaRegionName("Area Region" + fc.utobj().generateRandomNumber());
		manageAreaRegion.setGroupBy("States");
		manageAreaRegion.getCountries().add("USA");
		manageAreaRegion.getStates().add("Utah");
		manageAreaRegion.setSelectFranchiseeAssignment("Assign Franchisees later");
		return manageAreaRegion;
	}
	
	AddDivision set_AddDivision(AddDivision addDivision) throws Exception
	{
		addDivision.setDivisionName("Division a" + fc.utobj().generateRandomNumber());
		return addDivision;
	}
	
	Map<String,List<String>> getFullNameOfLeadsFromCSV(String csvFile) throws Exception
	{
		List<String> LeadFirstNames = new ArrayList<String>();
		List<String> LeadLastNames = new ArrayList<String>();
		
		Map<String,List<String>> map = new HashMap<String,List<String>>();

	     String line = "";
	     String cvsSplitBy = ",";

	        try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {

	            while ((line = br.readLine()) != null)
	            {
	            	// use comma as separator
	                String[] name = line.split(cvsSplitBy);
	                //System.out.println("FirstName : " +name[1]+ " , LastName : " +name[2]+"]");
	                LeadFirstNames.add(name[1]);
	                LeadLastNames.add(name[2]);
	            }
	            
	            map.put("FN" ,LeadFirstNames);
	            map.put("LN",LeadLastNames);
	              
	        }
	        return map;
	}
	
	
	
	
	void writeCsvFile(String fileName, List<Lead> lead) {

		// Delimiter used in CSV file
		// private static final String COMMA_DELIMITER = ",";
		final String NEW_LINE_SEPARATOR = "\n";

		// CSV file header
		final String FILE_HEADER = "Inquiry Date,First Name,Last Name,Email,Company Name,Address1,Address2,City,State / Province,Country,Zip / Postal Code,Work Phone,Work Phone Extension,Home Phone,Home Phone Extension,Fax,Comments,Mobile,Best Time To Contact,Lead Status,Lead Source Category,Lead Source Details,Assign To,Preferred Mode of Contact,Current Net Worth,Cash Available for Investment,Investment Timeframe,Source Of Investment,Background,Division,Brand";

		List<String> leadsObj = new ArrayList<String>();
		for (Lead y : lead) {
			getCSVLeadInfo(y);
			
			// Create a new list of lead objects
			leadsObj.add(getCSVLeadInfo(y));
		}

		FileWriter fileWriter = null;

		try {
			fileWriter = new FileWriter(fileName);

			// Write the CSV file header
			fileWriter.append(FILE_HEADER.toString());

			// Add a new line separator after the header
			fileWriter.append(NEW_LINE_SEPARATOR);

			// Write a new lead object list to the CSV file
			for (String x : leadsObj) {

				fileWriter.append(x);
				fileWriter.append(NEW_LINE_SEPARATOR);
			}

			System.out.println("CSV file was created successfully !!!");

		} catch (Exception e) {
			System.out.println("Error in CsvFileWriter !!!");
			e.printStackTrace();
		} finally {

			try {
				fileWriter.flush();
				fileWriter.close();
			} catch (IOException e) {
				System.out.println("Error while flushing/closing fileWriter !!!");
				e.printStackTrace();
			}

		}
	}
	
	
	String getCSVLeadInfo(Lead lead) {
		final String COMMA_DELIMITER = ",";
		String csvStr = "";

		if (lead.getInquiryDate() != null) {
			csvStr = csvStr + lead.getInquiryDate();
			csvStr = csvStr + COMMA_DELIMITER;
		} else {
			// csvStr = csvStr+"";
			csvStr = csvStr + COMMA_DELIMITER;
		}

		if (lead.getFirstName() != null) {
			csvStr = csvStr + lead.getFirstName();
			csvStr = csvStr + COMMA_DELIMITER;
		} else {
			csvStr = csvStr + COMMA_DELIMITER;
		}

		if (lead.getLastName() != null) {
			csvStr = csvStr + lead.getLastName();
			csvStr = csvStr + COMMA_DELIMITER;
		} else {
			// csvStr = csvStr+"";
			csvStr = csvStr + COMMA_DELIMITER;
		}

		if (lead.getEmail() != null) {
			csvStr = csvStr + lead.getEmail();
			csvStr = csvStr + COMMA_DELIMITER;
		} else {
			// csvStr = csvStr+"";
			csvStr = csvStr + COMMA_DELIMITER;
		}

		if (lead.getCompanyName() != null) {
			csvStr = csvStr + lead.getCompanyName();
			csvStr = csvStr + COMMA_DELIMITER;
		} else {
			// csvStr = csvStr+"";
			csvStr = csvStr + COMMA_DELIMITER;
		}

		if (lead.getAddress1() != null) {
			csvStr = csvStr + lead.getAddress1();
			csvStr = csvStr + COMMA_DELIMITER;
		} else {
			// csvStr = csvStr+"";
			csvStr = csvStr + COMMA_DELIMITER;
		}

		if (lead.getAddress2() != null) {
			csvStr = csvStr + lead.getAddress2();
			csvStr = csvStr + COMMA_DELIMITER;
		} else {
			// csvStr = csvStr+"";
			csvStr = csvStr + COMMA_DELIMITER;
		}

		if (lead.getCity() != null) {
			csvStr = csvStr + lead.getCity();
			csvStr = csvStr + COMMA_DELIMITER;
		} else {
			// csvStr = csvStr+"";
			csvStr = csvStr + COMMA_DELIMITER;
		}

		if (lead.getStateProvince() != null) {
			csvStr = csvStr + lead.getStateProvince();
			csvStr = csvStr + COMMA_DELIMITER;
		} else {
			// csvStr = csvStr+"";
			csvStr = csvStr + COMMA_DELIMITER;
		}

		if (lead.getCountry() != null) {
			csvStr = csvStr + lead.getCountry();
			csvStr = csvStr + COMMA_DELIMITER;
		} else {
			// csvStr = csvStr+"";
			csvStr = csvStr + COMMA_DELIMITER;
		}
		
		/*if (lead.getCounty() != null) {
			csvStr = csvStr + lead.getCounty();
			csvStr = csvStr + COMMA_DELIMITER;
		} else {
			// csvStr = csvStr+"";
			csvStr = csvStr + COMMA_DELIMITER;
		}*/

		if (lead.getZipPostalCode() != null) {
			csvStr = csvStr + lead.getZipPostalCode();
			csvStr = csvStr + COMMA_DELIMITER;
		} else {
			// csvStr = csvStr+"";
			csvStr = csvStr + COMMA_DELIMITER;
		}

		if (lead.getWorkPhone() != null) {
			csvStr = csvStr + lead.getWorkPhone();
			csvStr = csvStr + COMMA_DELIMITER;
		} else {
			// csvStr = csvStr+"";
			csvStr = csvStr + COMMA_DELIMITER;
		}

		if (lead.getWorkPhoneExtension() != null) {
			csvStr = csvStr + lead.getWorkPhoneExtension();
			csvStr = csvStr + COMMA_DELIMITER;
		} else {
			// csvStr = csvStr+"";
			csvStr = csvStr + COMMA_DELIMITER;
		}

		if (lead.getHomePhone() != null) {
			csvStr = csvStr + lead.getHomePhone();
			csvStr = csvStr + COMMA_DELIMITER;
		} else {
			// csvStr = csvStr+"";
			csvStr = csvStr + COMMA_DELIMITER;
		}

		if (lead.getHomePhoneExtension() != null) {
			csvStr = csvStr + lead.getHomePhoneExtension();
			csvStr = csvStr + COMMA_DELIMITER;
		} else {
			// csvStr = csvStr+"";
			csvStr = csvStr + COMMA_DELIMITER;
		}

		if (lead.getFax() != null) {
			csvStr = csvStr + lead.getFax();
			csvStr = csvStr + COMMA_DELIMITER;
		} else {
			// csvStr = csvStr+"";
			csvStr = csvStr + COMMA_DELIMITER;
		}

		if (lead.getComment() != null) {
			csvStr = csvStr + lead.getComment();
			csvStr = csvStr + COMMA_DELIMITER;
		} else {
			// csvStr = csvStr+"";
			csvStr = csvStr + COMMA_DELIMITER;
		}

		if (lead.getMobile() != null) {
			csvStr = csvStr + lead.getMobile();
			csvStr = csvStr + COMMA_DELIMITER;
		} else {
			// csvStr = csvStr+"";
			csvStr = csvStr + COMMA_DELIMITER;
		}

		if (lead.getBestTimeToContact() != null) {
			csvStr = csvStr + lead.getBestTimeToContact();
			csvStr = csvStr + COMMA_DELIMITER;
		} else {
			// csvStr = csvStr+"";
			csvStr = csvStr + COMMA_DELIMITER;
		}

		if (lead.getLeadStatus() != null) {
			csvStr = csvStr + lead.getLeadStatus();
			csvStr = csvStr + COMMA_DELIMITER;
		} else {
			// csvStr = csvStr+"";
			csvStr = csvStr + COMMA_DELIMITER;
		}

		if (lead.getLeadSourceCategory() != null) {
			csvStr = csvStr + lead.getLeadSourceCategory();
			csvStr = csvStr + COMMA_DELIMITER;
		} else {
			// csvStr = csvStr+"";
			csvStr = csvStr + COMMA_DELIMITER;
		}

		if (lead.getLeadSourceDetails() != null) {
			csvStr = csvStr + lead.getLeadSourceDetails();
			csvStr = csvStr + COMMA_DELIMITER;
		} else {
			// csvStr = csvStr+"";
			csvStr = csvStr + COMMA_DELIMITER;
		}

		if (lead.getBasedonAssignmentRules() != null) {
			csvStr = csvStr + lead.getBasedonAssignmentRules();
			csvStr = csvStr + COMMA_DELIMITER;
		} else {
			// csvStr = csvStr+"";
			csvStr = csvStr + COMMA_DELIMITER;
		}

		if (lead.getPreferredModeofContact() != null) {
			csvStr = csvStr + lead.getPreferredModeofContact();
			csvStr = csvStr + COMMA_DELIMITER;
		} else {
			// csvStr = csvStr+"";
			csvStr = csvStr + COMMA_DELIMITER;
		}

		if (lead.getCurrentNetWorth() != null) {
			csvStr = csvStr + lead.getCurrentNetWorth();
			csvStr = csvStr + COMMA_DELIMITER;
		} else {
			// csvStr = csvStr+"";
			csvStr = csvStr + COMMA_DELIMITER;
		}

		if (lead.getCashAvailableforInvestment() != null) {
			csvStr = csvStr + lead.getCashAvailableforInvestment();
			csvStr = csvStr + COMMA_DELIMITER;
		} else {
			// csvStr = csvStr+"";
			csvStr = csvStr + COMMA_DELIMITER;
		}

		if (lead.getInvestmentTimeframe() != null) {
			csvStr = csvStr + lead.getInvestmentTimeframe();
			csvStr = csvStr + COMMA_DELIMITER;
		} else {
			// csvStr = csvStr+"";
			csvStr = csvStr + COMMA_DELIMITER;
		}

		if (lead.getSourceOfInvestment() != null) {
			csvStr = csvStr + lead.getSourceOfInvestment();
			csvStr = csvStr + COMMA_DELIMITER;
		} else {
			// csvStr = csvStr+"";
			csvStr = csvStr + COMMA_DELIMITER;
		}

		if (lead.getBackground() != null) {
			csvStr = csvStr + lead.getBackground();
			csvStr = csvStr + COMMA_DELIMITER;
		} else {
			// csvStr = csvStr+"";
			csvStr = csvStr + COMMA_DELIMITER;
		}

		if (lead.getDivision() != null) {
			csvStr = csvStr + lead.getDivision();
			csvStr = csvStr + COMMA_DELIMITER;
		} else {
			// csvStr = csvStr+"";
			csvStr = csvStr + COMMA_DELIMITER;
		}

		return csvStr;
	}

}
