package com.builds.test.salesTest;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.fs.SearchUI;
import com.builds.utilities.FranconnectUtil;

public class SearchPage {

	private WebDriver driver;
	FranconnectUtil fc = new FranconnectUtil();

	SearchPage(WebDriver driver) {
		this.driver = driver;
	}

	public LeadManagementTest fill_And_Search_PrimaryInfoBasedSearch(Search search) throws Exception {
		fillDetailsInPrimaryInfoBasedSearch(search);
		clickSearchPrimaryInfoBasedSearch();
		return new LeadManagementTest(driver);
	}

	public void fill_And_Search_PrimaryInfoBasedSearch_ArchivedLeads(Search search) throws Exception {
		SearchUI ui = new SearchUI(driver);
		fc.utobj().printTestStep("Click on Archive lead Option");
		fc.utobj().clickElement(driver, ui.archivedleadRadio);

		fillDetailsInPrimaryInfoBasedSearch(search);
		clickSearchPrimaryInfoBasedSearch();
	}

	private void clickSearchPrimaryInfoBasedSearch() throws Exception {
		SearchUI ui = new SearchUI(driver);
		fc.utobj().printTestStep("Click on Search Button");
		fc.utobj().clickElement(driver, ui.search);
	}

	private void fillDetailsInPrimaryInfoBasedSearch(Search search) throws Exception {
		SearchUI ui = new SearchUI(driver);

		if (search.getFirstName() != null) {
			fc.utobj().sendKeys(driver, ui.firstName, search.getFirstName());
		}

		if (search.getLastName() != null) {
			fc.utobj().sendKeys(driver, ui.lastName, search.getLastName());
		}

		if (search.getEmail() != null) {
			fc.utobj().sendKeys(driver, ui.emailID, search.getEmail());
		}
	}

}
