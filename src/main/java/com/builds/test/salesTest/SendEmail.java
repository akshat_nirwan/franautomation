package com.builds.test.salesTest;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.fs.SendEmailUI;
import com.builds.utilities.FranconnectUtil;

class SendEmail {
	private WebDriver driver;
	FranconnectUtil fc = new FranconnectUtil();

	SendEmail(WebDriver driver) {
		this.driver = driver;
	}

	LeadManagementTest fillEmailDetails_ClickTopSendButton(Email email) throws Exception {
		// Lead Owner ID | Logged User ID | Or fill the Custom Email

		fc.utobj().printTestStep("Fill Email Details");

		SendEmailUI ui = new SendEmailUI(driver);

		if (email.getFromReply() != null) {
			if (email.getFromReply().toLowerCase().contains("Lead Owner ID")) {
				fc.utobj().clickElement(driver, ui.Lead_Owner_ID_Radio);
			} else if (email.getFromReply().toLowerCase().contains("Logged User ID")) {
				fc.utobj().clickElement(driver, ui.Logged_User_ID_Radio);
			} else {
				fc.utobj().clickElement(driver, ui.Custom_Radio);
				fc.utobj().sendKeys(driver, ui.customID, email.getFromReply());
			}
		}

		if (email.getExistingEmailTemplates() != null) {
			fc.utobj().selectDropDown(driver, ui.mailTemplateSelect, email.getExistingEmailTemplates());
		}

		if (email.getSubject() != null) {
			fc.utobj().sendKeys(driver, ui.subject, email.getSubject());
		}

		if (email.getBody() != null) {
			fc.utobj().switchFrameById(driver, ui.frame_forbody);
			fc.utobj().sendKeys(driver, ui.body, email.getBody());
			fc.utobj().switchFrameToDefault(driver);
		}

		clickTopSendBtn();

		return new LeadManagementTest(driver);
	}

	void clickTopSendBtn() throws Exception {
		SendEmailUI ui = new SendEmailUI(driver);
		fc.utobj().clickElement(driver, ui.sendEmail_Top_Button);
		fc.utobj().printTestStep("Click Top Send Button");
	}

	void clickResetBtn() {

	}

	void clickCancelBtn() {

	}
}
