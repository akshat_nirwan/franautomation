package com.builds.test.salesTest;

import java.util.ArrayList;
import java.util.List;

public class SendFDD {
	
	private String fromReply;
	private String fddEmailTemplates;
	private String versionOfFDDShouldBeSent;
	private List<String> sellerNames = new ArrayList<String>();  
	private String defaultSeller;
	private String toEmail;
	private String subject;
	private String attachment;
	private String sendFDDtoCoApplicants;
	private String emailBody;
	private String customText_fromReply;
	private String country;
	private String state;
	
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	
	public String getCustomText_fromReply() {
		return customText_fromReply;
	}
	public void setCustomText_fromReply(String customText_fromReply) {
		this.customText_fromReply = customText_fromReply;
	}
	
	public String getFromReply() {
		return fromReply;
	}
	public void setFromReply(String fromReply) {
		this.fromReply = fromReply;
	}
	public String getFddEmailTemplates() {
		return fddEmailTemplates;
	}
	public void setFddEmailTemplates(String fddEmailTemplates) {
		this.fddEmailTemplates = fddEmailTemplates;
	}
	public String getVersionOfFDDShouldBeSent() {
		return versionOfFDDShouldBeSent;
	}
	public void setVersionOfFDDShouldBeSent(String versionOfFDDShouldBeSent) {
		this.versionOfFDDShouldBeSent = versionOfFDDShouldBeSent;
	}
	
	public List<String> getSellerNames() {
		return sellerNames;
	}
	public void setSellerNames(List<String> sellerNames) {
		this.sellerNames = sellerNames;
	}
	
	public String getDefaultSeller() {
		return defaultSeller;
	}
	public void setDefaultSeller(String defaultSeller) {
		this.defaultSeller = defaultSeller;
	}
	public String getToEmail() {
		return toEmail;
	}
	
	public String getSelectFDD() {
		return selectFDD;
	}
	public void setSelectFDD(String selectFDD) {
		this.selectFDD = selectFDD;
	}
	private String selectFDD;

	public void setToEmail(String toEmail) {
		this.toEmail = toEmail;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getAttachment() {
		return attachment;
	}
	public void setAttachment(String attachment) {
		this.attachment = attachment;
	}
	public String getSendFDDtoCoApplicants() {
		return sendFDDtoCoApplicants;
	}
	public void setSendFDDtoCoApplicants(String sendFDDtoCoApplicants) {
		this.sendFDDtoCoApplicants = sendFDDtoCoApplicants;
	}
	public String getEmailBody() {
		return emailBody;
	}
	public void setEmailBody(String emailBody) {
		this.emailBody = emailBody;
	}
	
	

}
