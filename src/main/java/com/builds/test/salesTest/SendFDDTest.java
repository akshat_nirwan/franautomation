package com.builds.test.salesTest;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.builds.uimaps.fs.SendFDDUI;
import com.builds.utilities.FranconnectUtil;

public class SendFDDTest {

	private FranconnectUtil fc = new FranconnectUtil();
	private WebDriver driver;

	SendFDDTest(WebDriver driver) {
		this.driver = driver;
	}

	SendFDDTest SendFDD(FDDManagement fddManagement, SendFDD sendFDD, Lead lead) throws Exception {
		fc.utobj().printTestStep("Send FDD to Lead");
		SendFDDUI sendFDDUI = new SendFDDUI(driver);
		fill_SendFDD(fddManagement, sendFDD);
		
		try 
		{
			fc.utobj().clickElement(driver, sendFDDUI.sendBtn);
		}
		catch(Exception e)
		{
			StrtoXML_AkshatTest akshatTest = new StrtoXML_AkshatTest();
			Sales_Common_New common = new Sales_Common_New();
			common.getLeadDetailsThroughWebServices_MajorFields(driver, lead);
			String fetchedVal = akshatTest.getValFromXML(common.getLeadDetailsThroughWebServices_MajorFields(driver, lead), "referenceId");
			
			common.updateLeadDetailsThroughWebServices_EmailOptIn(driver, fetchedVal ); // update lead to --> Email Opt-in
		}
		
		fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver); // switch to frame
		fc.utobj().sleep();
		System.out.println(fc.utobj().getElement(driver, sendFDDUI.noOfLeadsWhomFDDwillBeSent).getText()); // print value after fetching text of the element 
		System.out.println(fc.utobj().getText(driver, sendFDDUI.noOfLeadsWhomFDDwillBeSent)); // print value after fetching text of the element 
		
		if (!(fc.utobj().getElement(driver, sendFDDUI.noOfLeadsWhomFDDwillBeSent).getText().equalsIgnoreCase("1"))) {
			fc.utobj().throwsException("Total No. of Leads to whom default State FDD will be sent :"
					+ fc.utobj().getElement(driver, sendFDDUI.noOfLeadsWhomFDDwillBeSent).getText());
		}
		
		fc.utobj().clickElement(driver, sendFDDUI.confirmBtn);
		return new SendFDDTest(driver);
	}

	void fill_SendFDD(FDDManagement fddManagement, SendFDD sendFDD) throws Exception {
		SendFDDUI sendFDDUI = new SendFDDUI(driver);

		if (sendFDD.getFromReply() != null) {
			if (sendFDD.getFromReply().equalsIgnoreCase("Logged User ID")) {
				fc.utobj().clickElement(driver, sendFDDUI.LoggedUserID_RadioBtn);
			}
			if (sendFDD.getFromReply().equalsIgnoreCase("Lead OwnerID")) {
				fc.utobj().clickElement(driver, sendFDDUI.LeadOwnerID_RadioBtn);
			}
			if (sendFDD.getFromReply().equalsIgnoreCase("Custom")) {
				fc.utobj().clickElement(driver, sendFDDUI.custom_RadioBtn);
				fc.utobj().sendKeys(driver, sendFDDUI.customText_fromReply, sendFDD.getCustomText_fromReply());
			}
		}

		if (sendFDD.getFddEmailTemplates() != null) {
			fc.utobj().selectDropDown(driver, sendFDDUI.fddEmailTemplates, sendFDD.getFddEmailTemplates());
		}

		if (sendFDD.getVersionOfFDDShouldBeSent() != null) {
			if (sendFDD.getVersionOfFDDShouldBeSent().equalsIgnoreCase("FDD registered with Lead")) {
				fc.utobj().clickElement(driver, sendFDDUI.FDDRegisteredWithLead_RadioBtn);
			}
			if (sendFDD.getVersionOfFDDShouldBeSent().equalsIgnoreCase("Alternate Division State")) {
				fc.utobj().clickElement(driver, sendFDDUI.alternateDivisionState_RadioBtn);

				if (sendFDD.getCountry() != null) {
					fc.utobj().selectDropDown(driver, sendFDDUI.Country_AlternateState, sendFDD.getCountry());
				}
				if (sendFDD.getState() != null) {
					fc.utobj().selectDropDown(driver, sendFDDUI.State_AlternateState, sendFDD.getState());
				}
			}
			if (sendFDD.getVersionOfFDDShouldBeSent().equalsIgnoreCase("Let me select")) {
				fc.utobj().clickElement(driver, sendFDDUI.letMeSelect_RadioBtn);

				if (sendFDD.getSelectFDD() != null) {
					fc.utobj().selectDropDown(driver, sendFDDUI.selectFDD, sendFDD.getSelectFDD());
				}
			}
		}

		if (sendFDD.getDefaultSeller() != null) {
			fc.utobj().sendKeys(driver, sendFDDUI.defaultSeller, sendFDD.getDefaultSeller());
		}

		if (sendFDD.getSellerNames() != null) {
			fc.utobj().selectMultipleValFromMultiSelect(driver, sendFDDUI.selectSellers, sendFDD.getSellerNames());
		}

		if (sendFDD.getSubject() != null) {
			fc.utobj().sendKeys(driver, sendFDDUI.subject, sendFDD.getSubject());
		}

		if (sendFDD.getAttachment() != null) {
			fc.utobj().sendKeys(driver, sendFDDUI.attachment, sendFDD.getAttachment());
		}

		if (sendFDD.getSendFDDtoCoApplicants() != null) {
			if (sendFDD.getSendFDDtoCoApplicants().equalsIgnoreCase("Yes")) {
				fc.utobj().clickElement(driver, sendFDDUI.sendFDDtoCoApplicants_checkBox);
			}
		}

		if (sendFDD.getEmailBody() != null) {
			fc.utobj().switchFrameById(driver, "mailText_ifr"); // switch to frame
			// System.out.println("switched to frame - mailText_ifr");
			fc.utobj().sendKeys(driver, sendFDDUI.emailBody, sendFDD.getEmailBody());
			fc.utobj().switchFrameToDefault(driver); // switch to default
		}
	}

	void verify_SendFDD(SendFDD sendFDD, FDDEmailTemplateSummary fddEmailTemplateSummary) throws Exception {
		SendFDDUI sendFDDUI = new SendFDDUI(driver);
		fc.utobj().printTestStep("Verify FDD Email in Activity History in Primary Info page of a Lead");
		fc.utobj().switchFrameById(driver, "leadLogCallSummary"); // switch to frame
	//	driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		if (fc.utobj().assertLinkText(driver, fddEmailTemplateSummary.getSubject())) {
		} else {
			fc.utobj().throwsException("FDD Email not found in Activity History in Primary Info page of a Lead");
		}

		fc.utobj().printTestStep("Verify if FDD email subject is clickable with data in pop-up");
		fc.utobj().clickElementByJS(driver, fc.utobj().getElementByLinkText(driver,fddEmailTemplateSummary.getSubject())); // click on FDD Email subject in Activity History tab of Primary Info Page of a Lead
		fc.utobj().switchFrameToDefault(driver); // switch to default frame
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.id("cboxIframe")));
		
		//fc.commonMethods().switch_cboxIframe_frameId(driver); // switch to frame
	
		fc.utobj().clickElement(driver, sendFDDUI.closeBtn_EmailInformation); 
	}

}
