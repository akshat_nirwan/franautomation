package com.builds.test.salesTest;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.builds.utilities.FranconnectUtil;

public class Sequence {

	// @BeforeClass()
	//@BeforeGroups(/*groups= "annotations"*/)
	@Test(priority = 1 , groups= "annotations")
	public void config() throws Exception {
		
		
		for(int x=1;x<=5;x++)
		{
			
			System.out.println("In Config");
			Thread.sleep(5000);
		}
		
		
		FranconnectUtil fc = new FranconnectUtil();
		fc.utobj().throwsException("Exception");

	}	
	
	@Test(priority = 2 , dependsOnMethods="config" , groups="annotations")
	public void m1() throws InterruptedException
	{
		for(int x=1;x<=5;x++)
		{
			
			System.out.println("m1");
			Thread.sleep(5000);
		}
		
	}
	

	@Test(priority = 3 , dependsOnMethods="config" ,  groups="annotations")
	public void m2() throws Exception
	{
		for(int x=1;x<=5;x++)
		{
			
			System.out.println("m2");
			Thread.sleep(5000);
		}
	}
	
	@Test(priority = 4 , dependsOnMethods="config" , groups="annotations")
	public void m3() throws Exception
	{
		/*for(int x=1;x<=5;x++)
		{
			
			System.out.println("m4");
			Thread.sleep(5000);
		}*/
		throw new Exception();
	}

	
	/*@AfterClass()
	public void m4() throws Exception
	{
		System.out.println("m4");
	}
*/
	
	
}
