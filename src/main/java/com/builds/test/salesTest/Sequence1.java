package com.builds.test.salesTest;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Sequence1 {

	@Test(priority = 5 , dependsOnMethods="m3"  ,  groups="annotations" )
	public void m4() {
		System.out.println("m4");
	}
	
	@Test(priority = 6 , dependsOnMethods = "m4" , groups="annotations")
	public void m5() throws Exception
	{
		System.out.println("m5");
		// throw new Exception();
	}
	
	@Test(priority = 7 , dependsOnMethods = "m4" , groups="annotations" )
	public void m6() throws Exception
	{
		System.out.println("m6");
	}
}
