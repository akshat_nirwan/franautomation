package com.builds.test.salesTest;

public class SetupPriorityForOwnerAssignment {
	
	private String priorityForOwnerAssignment;

	
	public String getPriorityForOwnerAssignment() {
		return priorityForOwnerAssignment;
	}

	public void setPriorityForOwnerAssignment(String priorityForOwnerAssignment) {
		this.priorityForOwnerAssignment = priorityForOwnerAssignment;
	}


}
