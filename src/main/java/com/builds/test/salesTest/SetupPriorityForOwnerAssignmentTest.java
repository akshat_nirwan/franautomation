package com.builds.test.salesTest;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

import com.builds.uimaps.fs.SetupPriorityForOwnerAssignmentUI;
import com.builds.utilities.FranconnectUtil;

public class SetupPriorityForOwnerAssignmentTest {
	
	WebDriver driver;
	FranconnectUtil fc = new FranconnectUtil();

	public SetupPriorityForOwnerAssignmentTest(WebDriver driver) {
		this.driver = driver;
	} 
	
	void SetPriorityForOwnerAssignment(SetupPriorityForOwnerAssignment setupPriorityForOwnerAssignment) throws Exception
	{
		fc.utobj().printTestStep(" Setup priority for Owner Assignment");
		SetupPriorityForOwnerAssignmentUI ui = new SetupPriorityForOwnerAssignmentUI(driver);
		
		if(setupPriorityForOwnerAssignment.getPriorityForOwnerAssignment() != null)
		{
			if(setupPriorityForOwnerAssignment.getPriorityForOwnerAssignment().equalsIgnoreCase("Source"))
			{
				Select priority = new Select(ui.select_byClass);
				priority.selectByVisibleText("Source");
				fc.utobj().clickElement(driver, ui.upBtn);
			}
			else if(setupPriorityForOwnerAssignment.getPriorityForOwnerAssignment().equalsIgnoreCase("Division"))
			{
				Select priority = new Select(ui.select_byClass);
				priority.selectByVisibleText("Division");
				fc.utobj().clickElement(driver, ui.upBtn);
			}
		}
		
		fc.utobj().clickElement(driver, ui.saveBtn);
		
	}
}
