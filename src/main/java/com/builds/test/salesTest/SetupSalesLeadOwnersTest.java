package com.builds.test.salesTest;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.fs.SetupSalesLeadOwnersUI;
import com.builds.utilities.FranconnectUtil;

class SetupSalesLeadOwnersTest {

	private FranconnectUtil fc = new FranconnectUtil();
	private WebDriver driver;

	SetupSalesLeadOwnersTest(WebDriver driver) {
		this.driver = driver;
	}
	
	void setAllusersAsSalesLeadOwners_AndSubmit() throws Exception
	{
		fc.utobj().printTestStep("Set Setup Sales Lead Owners as : 'All users as Sales Lead Owners'");
		SetupSalesLeadOwnersUI ui = new SetupSalesLeadOwnersUI(driver);
		fc.utobj().clickElement(driver, ui.allusersasSalesLeadOwners_Radio);
		fc.commonMethods().Click_Submit_Input_ByValue(driver);
	}

	void setSelectedusersAsSalesLeadOwners_AndSubmit() throws Exception
	{
		fc.utobj().printTestStep("Set Setup Sales Lead Owners as : 'Selected users as Sales Lead Owners'");
		fc.commonMethods().Click_Submit_Input_ByValue(driver);
	}
	
	
	
}

