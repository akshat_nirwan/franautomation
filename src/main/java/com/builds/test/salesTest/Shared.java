package com.builds.test.salesTest;

public class Shared {
	
	// First synchronized method
	synchronized void test1(Shared s2)
	{
		try {
			System.out.println("Test 1 Begins");
			Thread.sleep(1000);
			
			// Taking object lock of s2
			// Enters into test2 method 
			s2.test2(this);
			System.out.println("Test 1 Ends");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	// Second synchronized method
	synchronized void test2(Shared s1)
	{
		try {
		System.out.println("Test 2 Begins");
		Thread.sleep(1000);
		
		// Taking object lock of s1
		// Enters into test1 method
		s1.test1(this);
		System.out.println("Test 2 Ends");
	}catch(Exception e)
		{
		e.printStackTrace();
		}
		}
	
	
	
}
