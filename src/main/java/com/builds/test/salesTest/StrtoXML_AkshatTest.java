package com.builds.test.salesTest;

import java.io.IOException;
import java.io.StringReader;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.graphbuilder.struc.LinkedList.Node;

public class StrtoXML_AkshatTest {

	static Document loadXMLFromString(String xml) throws Exception {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		InputSource is = new InputSource(new StringReader(xml));
		return builder.parse(is);
	}

	public String getValFromXML(String xmlStr , String tagName) throws Exception {

		String value="";
		Document doc = StrtoXML_AkshatTest.loadXMLFromString(xmlStr);
		doc.getDocumentElement().normalize();
		NodeList ag = doc.getElementsByTagName("fcResponse");

		for (int temp = 0; temp < ag.getLength(); temp++) {

			org.w3c.dom.Node nNode = ag.item(temp);

			// System.out.println("\nCurrent Element :" + nNode.getNodeName());

			if (nNode.getNodeType() == org.w3c.dom.Node.ELEMENT_NODE) {
				Element eElement = (Element) nNode;
				value = eElement.getElementsByTagName(tagName).item(0).getTextContent();
			}
		}
		
		return value;
	}
	
	
	/* public static Document getXMLFromString(String xml) throws Exception {
	        Document doc = null;
	        DocumentBuilderFactory factory = DocumentBuilderFactory
	            .newInstance();
	        factory.setNamespaceAware(true);
	        DocumentBuilder builder;
	        builder = factory.newDocumentBuilder();
	        doc = builder.parse(new InputSource(new StringReader(xml)));
	        return doc;
	    }*/
	
	
	public static void main(String[] args) throws Exception {
		
		String xmlStr = "<fcResponse><responseStatus>Success</responseStatus><responseId>1873518390</responseId><responseData><fsLead><referenceId>461986687</referenceId><requestDate>2018-04-16</requestDate><salutation></salutation><firstName>Khubaib</firstName><lastName>Ali</lastName><address></address><address2></address2><city></city><country>USA</country><stateID>Alaska</stateID><zip></zip><countyID></countyID><primaryPhoneToCall></primaryPhoneToCall><bestTimeToContact></bestTimeToContact><phone>09074678683</phone><phoneExt></phoneExt><homePhone></homePhone><homePhoneExt></homePhoneExt><fax></fax><mobile></mobile><emailID>frandevices@gmail.com</emailID><companyName></companyName><comments></comments><leadStatusID>Test - status123</leadStatusID><leadOwnerID>Khubaib C1 Ali C1</leadOwnerID><leadRatingID></leadRatingID><leadKilledReason></leadKilledReason><leadKilledReasonComment></leadKilledReasonComment><leadSource2ID>Advertisement</leadSource2ID><leadSource3ID>Magazine</leadSource3ID><otherLeadSourceDetail></otherLeadSourceDetail><marketingCodeId></marketingCodeId><liquidCapitalMin></liquidCapitalMin><liquidCapitalMax></liquidCapitalMax><investTimeframe></investTimeframe><background></background><sourceOfFunding></sourceOfFunding><nextCallDate></nextCallDate><noOfUnitReq></noOfUnitReq><franchiseAwarded></franchiseAwarded><noOfFieldSold></noOfFieldSold><dateOfOpen></dateOfOpen><statusChangeDate></statusChangeDate><webSiteLead>No</webSiteLead><forecastClosureDate></forecastClosureDate><probability>0</probability><forecastRating></forecastRating><forecastRevenue></forecastRevenue><coApplicantRelationshipID></coApplicantRelationshipID><ipAddress></ipAddress><browserType></browserType><locationId1></locationId1><locationId1b></locationId1b><locationId2></locationId2><preferredCountry1>USA</preferredCountry1><preferredCity1></preferredCity1><preferredStateId1></preferredStateId1><preferredCountry2>USA</preferredCountry2><preferredStateId2></preferredStateId2><preferredCity2></preferredCity2><lastUpdate>2018-04-17T09:32Z</lastUpdate><lastAttended>2018-04-17T09:32Z</lastAttended><bounceEmailStatus>No</bounceEmailStatus><spamEmailStatus>No</spamEmailStatus><bounceReason></bounceReason><_akshatSync1TextValphone1587905297></_akshatSync1TextValphone1587905297><_jkj7HorizintalAkshat438704671></_jkj7HorizintalAkshat438704671><_verticalAkshatField1082244156></_verticalAkshatField1082244156><_jhgyjg267Akshat1907893116></_jhgyjg267Akshat1907893116><_cstmCntry1128729005>USA</_cstmCntry1128729005><_cstmState1370072658></_cstmState1370072658><_nsTempcountry559103107>USA</_nsTempcountry559103107><_nsTempstate435148040></_nsTempstate435148040><_test55874046663>USA</_test55874046663><_test661897621114></_test661897621114><_whatSkillsAndExperienceDoYouHaveThatWil618799858></_whatSkillsAndExperienceDoYouHaveThatWil618799858><_test1122307020086></_test1122307020086><_test112233613716891></_test112233613716891><_q1q1q11646669361></_q1q1q11646669361><_a2a2a2a2a2a863684044></_a2a2a2a2a2a863684044><_uuuuuuuuuuu1526082668></_uuuuuuuuuuu1526082668><_qqqqqqqqqqqq1170758668></_qqqqqqqqqqqq1170758668><_testDdField162887969></_testDdField162887969><divisionReferenceId></divisionReferenceId><division></division><leadOwnerReferenceId>635318489</leadOwnerReferenceId></fsLead></responseData></fcResponse>"; 
		StrtoXML_AkshatTest obj = new StrtoXML_AkshatTest();
		System.out.println(obj.getValFromXML(xmlStr, "preferredCountry2"));	
		
		
		
		
	}

}


