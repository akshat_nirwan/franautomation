package com.builds.test.salesTest;

import org.openqa.selenium.WebDriver;

import com.builds.utilities.FranconnectUtil;

class TasksTab {
	private WebDriver driver;
	private FranconnectUtil fc = new FranconnectUtil();

	TasksTab(WebDriver driver) {
		this.driver = driver;
	}
	
	void clickAddTasksLink() throws Exception
	{
		fc.utobj().printTestStep("Click on Add Task Link on the top");
	}
	
	void  associateLeadWithTaskAndSearch(String leadName) throws Exception
	{
		fc.utobj().printTestStep("Enter lead name and click on Search : "+ leadName);
		
	}

	
}
