package com.builds.test.salesTest;

import org.openqa.selenium.WebDriver;

import com.builds.utilities.FranconnectUtil;

class TemplatesPage {

	private FranconnectUtil fc = new FranconnectUtil();
	private WebDriver driver;

	TemplatesPage(WebDriver driver) {
		this.driver = driver;
	}

	TemplatesPage clickEmailTemplate() throws Exception {
		fc.utobj().printTestStep("Click on Email Template Link");

		return this;
	}

	TemplatesPage clickTemplateFolders() throws Exception {
		fc.utobj().printTestStep("Click on Template Folders Link");

		return this;
	}

	TemplatesPage clickInfomercials() throws Exception {
		fc.utobj().printTestStep("Click on Infomercials Link");

		return this;
	}

	TemplatesPage deleteTemplate() throws Exception {
		fc.utobj().printTestStep("Click on Delete Template");

		return this;
	}

}
