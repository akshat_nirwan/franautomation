package com.builds.test.salesTest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminPageTest;
import com.builds.test.common.AddDivision;
import com.builds.test.common.ConfigureNewHierarchyLevel;
import com.builds.test.common.ConfigureNewHierarchyLevelTest;
import com.builds.test.common.CorporateUser;
import com.builds.test.common.ManageDivisionTest;
import com.builds.uimaps.common.ConfigureNewHierarchyLevelUI;
import com.builds.uimaps.fs.ConfigureDuplicateLeadOwnerAssignmentUI;
import com.builds.uimaps.fs.LeadSummaryUI;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class TestCases_DuplicateCriteria {
	
	FranconnectUtil fc = new FranconnectUtil();
	Sales_Common_New common = new Sales_Common_New();
	ArrayList<String> corpUsersList = new ArrayList<String>();
	
	@Test(priority = 1 , groups = { "DuplicateCriteria" , "DuplicateCriteria_01"})
	@TestCase(createdOn = "2018-04-27", updatedOn = "2018-05-27", testCaseId = "Sales_DuplicateCriteria_01", testCaseDescription = "To verify duplicate lead owner assignment when Divisions are OFF.")
	void DuplicateCriteria_01() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);

			ConfigureNewHierarchyLevelUI configureNewHierarchyLevelUI = new ConfigureNewHierarchyLevelUI(driver);
			fc.commonMethods().getModules().openAdminPage(driver).openConfigureNewHierarchyLevel(driver);
			
			if (configureNewHierarchyLevelUI.enableNewHierarchyLevel_No.isSelected()) {
				fc.commonMethods().getModules().openAdminPage(driver).openConfigureDuplicateCriteriaPage(driver);

				ConfigureDuplicateCriteriaTest configureDuplicateCriteriaTest = new ConfigureDuplicateCriteriaTest(driver);
				configureDuplicateCriteriaTest.click_ConfigureDuplicateLeadOwnerAssignment();

				ConfigureDuplicateLeadOwnerAssignmentUI assignmentUI = new ConfigureDuplicateLeadOwnerAssignmentUI(
						driver);
				if (fc.utobj().isElementPresent(driver, assignmentUI.onlyAssignNewLeadToSameOwnerAsTheExistingLeadIfTheyAreAssignedToSameDivision_radioBtn)) {
					fc.utobj().throwsException("Settings by Brand is visible in duplicate lead owner assignment despite brands being OFF in admin settings - FAILED");
				}

			} else if ( configureNewHierarchyLevelUI.enableNewHierarchyLevel_Yes.isSelected())
			{
				fc.commonMethods().getModules().openAdminPage(driver).openConfigureDuplicateCriteriaPage(driver);

				ConfigureDuplicateCriteriaTest configureDuplicateCriteriaTest = new ConfigureDuplicateCriteriaTest(driver);
				configureDuplicateCriteriaTest.click_ConfigureDuplicateLeadOwnerAssignment();

				ConfigureDuplicateLeadOwnerAssignmentUI assignmentUI = new ConfigureDuplicateLeadOwnerAssignmentUI(driver);
				if (! fc.utobj().isElementPresent(driver, assignmentUI.onlyAssignNewLeadToSameOwnerAsTheExistingLeadIfTheyAreAssignedToSameDivision_radioBtn)) {
					fc.utobj().throwsException("Settings by Brand is not visible in duplicate lead owner assignment despite brands being ON in admin settings - FAILED");
			}
			
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
}
	
	@Test( priority = 2 , groups = { "DuplicateCriteria" , "DuplicateCriteria_02"})
	@TestCase(createdOn = "2018-04-27", updatedOn = "2018-05-27", testCaseId = "Sales_DuplicateCriteria_02", testCaseDescription = "Verify configuration of Duplicate Criteria")
	void DuplicateCriteria_02() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

	WebDriver driver = fc.commonMethods().browsers().openBrowser();
	CorporateUser corpUser = new CorporateUser();
		
		try { 
			
		driver = fc.loginpage().login(driver);
		
		// Turn ON Division
		ConfigureNewHierarchyLevel configureNewHierarchyLevel = new ConfigureNewHierarchyLevel();
		ConfigureNewHierarchyLevelTest configureNewHierarchyLevelTest = new ConfigureNewHierarchyLevelTest(driver);
		configureNewHierarchyLevel.setEnableNewHierarchyLevel("Yes");
		fc.commonMethods().getModules().openAdminPage(driver).openConfigureNewHierarchyLevel(driver);
		configureNewHierarchyLevelTest.EnableNewHierarchyLevel(configureNewHierarchyLevel);
		
		fc.commonMethods().getModules().openAdminPage(driver).openConfigureDuplicateCriteriaPage(driver);
		
		ConfigureDuplicateCriteriaTest configureDuplicateCriteriaTest = new ConfigureDuplicateCriteriaTest(driver);
		
		ArrayList<String> DuplicateCriteria1_Fields = new ArrayList<String>();
		DuplicateCriteria1_Fields.add("First Name");
		DuplicateCriteria1_Fields.add("Email");
		configureDuplicateCriteriaTest.set_DuplicateCriteria1(DuplicateCriteria1_Fields);
		
		ArrayList<String> DuplicateCriteria2_Fields = new ArrayList<String>();
		DuplicateCriteria2_Fields.add("Last Name");
		DuplicateCriteria2_Fields.add("Company Name");
		configureDuplicateCriteriaTest.set_DuplicateCriteria2(DuplicateCriteria2_Fields);
		
		 configureDuplicateCriteriaTest.click_ConfigureDuplicateLeadOwnerAssignment();
		 
		 ConfigureDuplicateLeadOwnerAssignmentTest configureDuplicateLeadOwnerAssignmentTest = new ConfigureDuplicateLeadOwnerAssignmentTest(driver);
		 
		 //  Assign the new lead to the owner of the existing lead
		 configureDuplicateLeadOwnerAssignmentTest.set_assignNewLeadToOwnerOfExistingLead();
		 
		 for (int i = 0; i <= 2; i++) {
				fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
				corpUser.setFirstName("CorpUser" + i);
				corpUser.setLastName("RoundRobin" + fc.utobj().generateRandomNumber());
				corpUser.setUserName("user" + fc.utobj().generateRandomChar());
				fc.commonMethods().addCorporateUser(driver, corpUser);
				corpUsersList.add(corpUser.getFirstName() + " " + corpUser.getLastName());
			}
		 
			SetupSalesLeadOwnersTest setupSalesLeadOwnersTest = new SetupSalesLeadOwnersTest(driver);
			fc.commonMethods().getModules().openAdminPage(driver).openSetupFranchiseSalesLeadOwnersPage(driver);
			setupSalesLeadOwnersTest.setAllusersAsSalesLeadOwners_AndSubmit();
		 
			fc.commonMethods().getModules().openAdminPage(driver).openAssignLeadOwnersPage(driver);
			
			AssignLeadOwnersTest assignLeadOwnersTest = new AssignLeadOwnersTest(driver);
			AssignLeadOwnerbyRoundRobinTest assignLeadOwnerbyRoundRobinTest = new AssignLeadOwnerbyRoundRobinTest(driver);

			assignLeadOwnersTest.set_defaultOwner("FranConnect Administrator");
			assignLeadOwnersTest.setAssignLeadOwnerbyRoundRobin();
			assignLeadOwnerbyRoundRobinTest.moveAllUserTo_AvailableLeadOwners();
			// System.out.println(corpUsersList);
			for (String temp : corpUsersList) {
				assignLeadOwnerbyRoundRobinTest.moveUserTo_ConfiguredLeadOwners(temp);
			}
			assignLeadOwnerbyRoundRobinTest.updateButton();
			
			// Add Lead 1
			Lead lead1 = new Lead();
			lead1.setFirstName("Duplicate1 " +fc.utobj().generateRandomChar());
			lead1.setLastName("Lead" +fc.utobj().generateRandomNumber());
			lead1.setEmail("frantest2017@gmail.com");
			lead1.setBasedonAssignmentRules("yes");
			lead1.setLeadSourceCategory("Friends");
			lead1.setLeadSourceDetails("Friends");
			common.addLeadThroughWebServices(null, lead1);

			// Fetch Owner assigned to Lead through Rest API
			String xml1 = common.getLeadDetailsThroughWebServices_MajorFields(null, lead1);
			String ownerAssignedToLead1 = common.getValFromXML(xml1, "leadOwnerID");
			System.out.println("Owner Assigned to Lead >>>>>>>>" + lead1.getLeadFullName() + ">>>" + ownerAssignedToLead1);
			System.out.println("Lead should be assigned to >>>> " + corpUsersList.get(0));
			
			// Validation - The newly created lead should be assigned a lead owner as per the assignment rule
			if(! corpUsersList.get(0).equalsIgnoreCase(ownerAssignedToLead1))
			{
				fc.utobj().throwsException("Newly created lead is not assigned as per assignment rule - FAILED");
			}
											
			// Add Lead 2
			Lead lead2 = new Lead();
			lead2.setFirstName(lead1.getFirstName());
			lead2.setLastName(lead1.getLastName());
			lead2.setEmail("frantest2017@gmail.com");
			lead2.setBasedonAssignmentRules("yes");
			lead2.setLeadSourceCategory("Friends");
			lead2.setLeadSourceDetails("Friends");
			common.addLeadThroughWebServices(null, lead2);
			
			// Fetch Owner assigned to Lead through Rest API
			String xml2 = common.getLeadDetailsThroughWebServices_MajorFields(null, lead2);
			String ownerAssignedToLead2 = common.getValFromXML(xml2, "leadOwnerID");
			System.out.println("Owner Assigned to Lead >>>>>>>>" + lead2.getLeadFullName() + ">>>" + ownerAssignedToLead2);
										
			// Validation - > The Lead Owner of the Duplicate Lead should be assigned same as that of Existing Lead's Lead Owner
			if (! (ownerAssignedToLead1.equalsIgnoreCase(ownerAssignedToLead2))) {
				fc.utobj().throwsException("Lead Owner of the Duplicate Lead is not  assigned same as that of Existing Lead's Lead Owner - FAILED");
			}
			
			// Validation - > Existing lead's Lead Owner should remain unchanged
			if(! corpUsersList.get(0).equalsIgnoreCase(ownerAssignedToLead1))
			{
				fc.utobj().throwsException("Existing lead's Lead Owner has been changed - FAILED");
			}
		 
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}
	
	@Test( priority = 3 , groups = { "DuplicateCriteria" , "DuplicateCriteria_03"})
	@TestCase(createdOn = "2018-04-27", updatedOn = "2018-05-27", testCaseId = "Sales_DuplicateCriteria_03", testCaseDescription = "To verify Assign Lead Owner of Existing Lead Same as of New Lead")
	void DuplicateCriteria_03() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {	
			
			driver = fc.loginpage().login(driver);

			fc.commonMethods().getModules().openAdminPage(driver).openConfigureDuplicateCriteriaPage(driver);
			
			ConfigureDuplicateCriteriaTest configureDuplicateCriteriaTest = new ConfigureDuplicateCriteriaTest(driver);
			 configureDuplicateCriteriaTest.click_ConfigureDuplicateLeadOwnerAssignment();
			 
			 ConfigureDuplicateLeadOwnerAssignmentTest configureDuplicateLeadOwnerAssignmentTest = new ConfigureDuplicateLeadOwnerAssignmentTest(driver);
			 // configureDuplicateLeadOwnerAssignmentTest.set_assignNewLeadToOwnerOfExistingLead();
			 configureDuplicateLeadOwnerAssignmentTest.set_ReassignTheExistingLeadToTheOwnerOfTheNewLead();
	
				// Add Lead 1
				Lead lead1 = new Lead();
				lead1.setFirstName("Duplicate " +fc.utobj().generateRandomChar());
				lead1.setLastName("LeadOne");
				lead1.setEmail(""+fc.utobj().generateRandomChar()+"@gmail.com");
				lead1.setBasedonAssignmentRules("yes");
				lead1.setLeadSourceCategory("Friends");
				lead1.setLeadSourceDetails("Friends");
				common.addLeadThroughWebServices(null, lead1);

				// Fetch Owner assigned to Lead through Rest API
				String xml1 = common.getLeadDetailsThroughWebServices_MajorFields(null, lead1);
				String ownerAssignedToLead1 = common.getValFromXML(xml1, "leadOwnerID");
				System.out.println("Owner Assigned to Lead >>>>>>>>" + lead1.getLeadFullName() + ">>>" + ownerAssignedToLead1);
				String OwnerOfLead1 = ownerAssignedToLead1;
				
				// Validation 
				if(! (corpUsersList.contains(ownerAssignedToLead1)))
				{
					fc.utobj().throwsException("Lead Owner for lead-1 is not assigned as per Assignment Rule - FAILED");
				}
				
				// Add Lead 2
				Lead lead2 = new Lead();
				lead2.setFirstName(lead1.getFirstName());
				lead2.setLastName("LeadTwo");
				lead2.setEmail(lead1.getEmail());
				lead2.setBasedonAssignmentRules("yes");
				lead2.setLeadSourceCategory("Friends");
				lead2.setLeadSourceDetails("Friends");
				common.addLeadThroughWebServices(null, lead2);
				
				// Fetch Owner assigned to Lead through Rest API
				String xml2 = common.getLeadDetailsThroughWebServices_MajorFields(null, lead2);
				String ownerAssignedToLead2 = common.getValFromXML(xml2, "leadOwnerID");
				System.out.println("Owner Assigned to Lead >>>>>>>>" + lead2.getLeadFullName() + ">>>" + ownerAssignedToLead2);
							
				// Validation 
				if(! (corpUsersList.contains(ownerAssignedToLead2)))
				{
					fc.utobj().throwsException("Lead Owner for lead-2 is not assigned as per Assignment Rule - FAILED");
				}
				
				// Validation 
				String xmlNew1 = common.getLeadDetailsThroughWebServices_MajorFields(null, lead1);
				String ownerAssignedToLeadNew1 = common.getValFromXML(xmlNew1, "leadOwnerID");
				System.out.println("Owner Assigned to Lead >>>>>>>>" + lead1.getLeadFullName() + ">>>" + ownerAssignedToLeadNew1);
				if(OwnerOfLead1.equalsIgnoreCase(ownerAssignedToLeadNew1))
				{
					fc.utobj().throwsException("Lead Owner of Existing lead is same even after adding duplicate lead associated to differentg lead owner - FAILED");
				}
				
			 // Validation
				if(! (ownerAssignedToLeadNew1.equalsIgnoreCase(ownerAssignedToLead2)))
				{
					fc.utobj().throwsException("Lead Owner of Existing duplicate Lead is not Same as of New duplicate Lead - FAILED");
				}
	
				fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}
	
	@Test( priority = 4 ,  groups = { "DuplicateCriteria"})
	@TestCase(createdOn = "2018-04-27", updatedOn = "2018-05-27", testCaseId = "Sales_DuplicateCriteria_04", testCaseDescription = "To verify switching through options 'Configure Duplicate Lead Owner Assignment' in Admin >> Sales .")
	void DuplicateCriteria_04() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {	
			
			driver = fc.loginpage().login(driver);
		
			fc.commonMethods().getModules().openAdminPage(driver).openConfigureDuplicateCriteriaPage(driver);
			
			ConfigureDuplicateCriteriaTest configureDuplicateCriteriaTest = new ConfigureDuplicateCriteriaTest(driver);
			 configureDuplicateCriteriaTest.click_ConfigureDuplicateLeadOwnerAssignment();
			 
			 ConfigureDuplicateLeadOwnerAssignmentTest configureDuplicateLeadOwnerAssignmentTest = new ConfigureDuplicateLeadOwnerAssignmentTest(driver);
			 configureDuplicateLeadOwnerAssignmentTest.set_onlyAssignNewLeadToSameOwnerAsTheExistingLeadIfTheyAreAssignedToSameDivision();
			 
				fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}
	
	@Test( priority = 5 , groups = { "DuplicateCriteria" })
	@TestCase(createdOn = "2018-04-27", updatedOn = "2018-05-27", testCaseId = "Sales_DuplicateCriteria_05", testCaseDescription = "Verify Duplicate Lead Icon at primary Info of lead")
	void DuplicateCriteria_05() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);

			// Add Lead 1
			Lead lead1 = new Lead();
			lead1.setFirstName("Duplicate" + fc.utobj().generateRandomNumber6Digit());
			lead1.setLastName("Lead" + fc.utobj().generateRandomNumber());
			lead1.setEmail("frantest2017@gmail.com");
			lead1.setBasedonAssignmentRules("yes");
			lead1.setLeadSourceCategory("Friends");
			lead1.setLeadSourceDetails("Friends");
			common.addLeadThroughWebServices(null, lead1);

			// Add Lead 2
			Lead lead2 = new Lead();
			lead2.setFirstName(lead1.getFirstName());
			lead2.setLastName(lead1.getLastName());
			lead2.setEmail("frantest2017@gmail.com");
			lead2.setBasedonAssignmentRules("yes");
			lead2.setLeadSourceCategory("Friends");
			lead2.setLeadSourceDetails("Friends");
			common.addLeadThroughWebServices(null, lead2);

			fc.commonMethods().getModules().clickSalesModule(driver);

			Sales sales = new Sales(driver);
			sales.clickLeadManagement();

			LeadManagementTest leadManagementTest = new LeadManagementTest(driver);
			leadManagementTest.leadSearch(lead1);

			List<WebElement> duplicateIconList = fc.utobj().getElementListByXpath(driver,".//*[contains(.,'"+lead1.getLeadFullName()+"')]/following-sibling::img[@id='Image' and contains(@src,'dup_icon_w.gif')]");
			for (WebElement webElement : duplicateIconList) {
				// System.out.println(duplicateIcon.getAttribute("onmouseover"));
				// System.out.println(duplicateIcon.getAttribute("onmouseover").contains("Indicates duplicate leads"));
				if (! webElement.getAttribute("onmouseover").contains("Indicates duplicate leads")) {
					fc.utobj().throwsException("Duplicate Lead Icon not found - FAILED");
				}
			}
			
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[contains(.,'"+lead1.getLeadFullName()+"')]/following-sibling::img[@id='Image' and contains(@src,'dup_icon_w.gif')]"));  // click on duplicate icon
			
			boolean check1 = fc.utobj().assertPageSource(driver, lead1.getLeadFullName());
			if (!check1) {
				fc.utobj().throwsException("duplicate lead 1 not present on clicking duplicate icon");
			}

			boolean check2 = fc.utobj().assertPageSource(driver, lead2.getLeadFullName());
			if (!check2) {
				fc.utobj().throwsException("duplicate lead 2 not present on clicking duplicate icon");
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(priority = 6 , groups = { "DuplicateCriteria" })
	@TestCase(createdOn = "2018-04-27", updatedOn = "2018-05-27", testCaseId = "Sales_DuplicateCriteria_06", testCaseDescription = "To verify Duplicate Lead Owner Assignment based on Assignment Rules")
	void DuplicateCriteria_06() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			
			fc.commonMethods().getModules().openAdminPage(driver).openConfigureDuplicateCriteriaPage(driver);
			
			ConfigureDuplicateCriteriaTest configureDuplicateCriteriaTest = new ConfigureDuplicateCriteriaTest(driver);
			 configureDuplicateCriteriaTest.click_ConfigureDuplicateLeadOwnerAssignment();
			 
			 ConfigureDuplicateLeadOwnerAssignmentTest configureDuplicateLeadOwnerAssignmentTest = new ConfigureDuplicateLeadOwnerAssignmentTest(driver);
			 configureDuplicateLeadOwnerAssignmentTest.set_assignNewLeadToOwnerBasedOnLeadAssignmentRules();

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(priority = 7 , groups = { "DuplicateCriteria" })
	@TestCase(createdOn = "2018-04-27", updatedOn = "2018-05-27", testCaseId = "Sales_DuplicateCriteria_07", testCaseDescription = "To verify that on clicking Cancel button , any changes done in Select Duplicate Lead Owner Assignment Scheme gets discarded")
	void DuplicateCriteria_07() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			
			fc.commonMethods().getModules().openAdminPage(driver).openConfigureDuplicateCriteriaPage(driver);
			
			ConfigureDuplicateCriteriaTest configureDuplicateCriteriaTest = new ConfigureDuplicateCriteriaTest(driver);
			 configureDuplicateCriteriaTest.click_ConfigureDuplicateLeadOwnerAssignment();
			 
			 ConfigureDuplicateLeadOwnerAssignmentTest configureDuplicateLeadOwnerAssignmentTest = new ConfigureDuplicateLeadOwnerAssignmentTest(driver);
			 configureDuplicateLeadOwnerAssignmentTest.set_assignNewLeadToOwnerBasedOnLeadAssignmentRules();
			 
			 configureDuplicateLeadOwnerAssignmentTest.selectRadioButton_assignNewLeadToOwnerOfExistingLead();
			 configureDuplicateLeadOwnerAssignmentTest.click_CancelBtn();
			 
			fc.commonMethods().getModules().openAdminPage(driver).openConfigureDuplicateCriteriaPage(driver);
			configureDuplicateCriteriaTest.click_ConfigureDuplicateLeadOwnerAssignment();
			
			ConfigureDuplicateLeadOwnerAssignmentUI assignmentUI = new ConfigureDuplicateLeadOwnerAssignmentUI(driver);
			// System.out.println(assignmentUI.assignNewLeadToOwnerBasedOnLeadAssignmentRules_radioBtn.isSelected());
			if(! assignmentUI.assignNewLeadToOwnerBasedOnLeadAssignmentRules_radioBtn.isSelected())
			{
				fc.utobj().throwsException("Cancel button not working on 'Configure Duplicate Lead Owner Assignment' - FAILED");
			}
			 
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(priority = 8 , groups = { "DuplicateCriteria" })
	@TestCase(createdOn = "2018-04-27", updatedOn = "2018-05-27", testCaseId = "Sales_DuplicateCriteria_08", testCaseDescription = "To verify when a Lead Owner for a duplicate lead is changed.Assign Lead Owner of New Lead same as of Existing Lead, if both leads are of same Division")
	void DuplicateCriteria_08() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			
			
			AssignLeadOwnersTest assignLeadOwnersTest = new AssignLeadOwnersTest(driver);

			CorporateUser corpUserForDivision = new CorporateUser();
			ArrayList<String> corpUsersListForDivision = new ArrayList<String>();
			 for (int i = 0; i <2; i++) {
					fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUserForDivision);
					corpUserForDivision.setFirstName("CorpUser" + i);
					corpUserForDivision.setLastName("Division" + fc.utobj().generateRandomNumber());
					corpUserForDivision.setUserName("user" + fc.utobj().generateRandomChar());
					fc.commonMethods().addCorporateUser(driver, corpUserForDivision);
					corpUsersListForDivision.add(corpUserForDivision.getFirstName() + " " + corpUserForDivision.getLastName());
				}
			
			// Add Division #1
			AddDivision addDivision1 = new AddDivision();
			ManageDivisionTest manageDivisionTest = new ManageDivisionTest(driver);
			common.set_AddDivision(addDivision1);
			fc.commonMethods().getModules().openAdminPage(driver).openAddDivision(driver);
			manageDivisionTest.addDivision(addDivision1);
			
			// Set -> Assign Lead Owner by RoundRobin
			AssignLeadOwnerbyRoundRobinTest assignLeadOwnerbyRoundRobinTest = new AssignLeadOwnerbyRoundRobinTest(driver);
			fc.commonMethods().getModules().openAdminPage(driver).openAssignLeadOwnersPage(driver);
			assignLeadOwnersTest.set_defaultOwner("FranConnect Administrator");
			assignLeadOwnersTest.setAssignLeadOwnerbyRoundRobin();
			assignLeadOwnerbyRoundRobinTest.moveAllUserTo_AvailableLeadOwners();

			for (String temp : corpUsersListForDivision) {
				assignLeadOwnerbyRoundRobinTest.moveUserTo_ConfiguredLeadOwners(temp);
			}
			assignLeadOwnerbyRoundRobinTest.updateButton();
			
			fc.commonMethods().getModules().openAdminPage(driver).openConfigureDuplicateCriteriaPage(driver);
			ConfigureDuplicateCriteriaTest configureDuplicateCriteriaTest = new ConfigureDuplicateCriteriaTest(driver);
			
			ArrayList<String> DuplicateCriteria1_Fields = new ArrayList<String>();
		//	DuplicateCriteria1_Fields.add("First Name");
			DuplicateCriteria1_Fields.add("Email");
			configureDuplicateCriteriaTest.set_DuplicateCriteria1(DuplicateCriteria1_Fields);
			
			ArrayList<String> DuplicateCriteria2_Fields = new ArrayList<String>();
			DuplicateCriteria2_Fields.add("Last Name");
			DuplicateCriteria2_Fields.add("Company Name");
			configureDuplicateCriteriaTest.set_DuplicateCriteria2(DuplicateCriteria2_Fields);
			
			configureDuplicateCriteriaTest.click_ConfigureDuplicateLeadOwnerAssignment();

			 ConfigureDuplicateLeadOwnerAssignmentTest configureDuplicateLeadOwnerAssignmentTest = new ConfigureDuplicateLeadOwnerAssignmentTest(driver);
			 configureDuplicateLeadOwnerAssignmentTest.set_onlyAssignNewLeadToSameOwnerAsTheExistingLeadIfTheyAreAssignedToSameDivision();
			 
			// Add Lead 1
				Lead lead1 = new Lead();
				lead1.setFirstName("Duplicate" + fc.utobj().generateRandomNumber6Digit());
				lead1.setLastName("Lead" + fc.utobj().generateRandomNumber());
				lead1.setEmail(""+fc.utobj().generateRandomChar()+"@gmail.com");
				lead1.setBasedonAssignmentRules("yes");
				lead1.setDivision(addDivision1.getDivisionName());
				lead1.setLeadSourceCategory("Friends");
				lead1.setLeadSourceDetails("Friends");
				common.addLeadThroughWebServices(null, lead1);

				// Add Lead 2
				Lead lead2 = new Lead();
				lead2.setFirstName("Duplicate" + fc.utobj().generateRandomNumber6Digit());
				lead2.setLastName("Lead" + fc.utobj().generateRandomNumber());
				lead2.setEmail(lead1.getEmail());
				lead2.setBasedonAssignmentRules("yes");
				lead2.setDivision(addDivision1.getDivisionName());
				lead2.setLeadSourceCategory("Friends");
				lead2.setLeadSourceDetails("Friends");
				common.addLeadThroughWebServices(null, lead2);
						
				// Fetch Owner assigned to Lead through Rest API
				String xml1 = common.getLeadDetailsThroughWebServices_MajorFields(null, lead1);
				String ownerAssignedToLead1 = common.getValFromXML(xml1, "leadOwnerID");
				System.out.println("Owner Assigned to Lead 1 >>>>>>>>" + lead1.getLeadFullName() + ">>>" + ownerAssignedToLead1);
				
				// Fetch Owner assigned to Lead through Rest API
				String xml2 = common.getLeadDetailsThroughWebServices_MajorFields(null, lead2);
				String ownerAssignedToLead2 = common.getValFromXML(xml2, "leadOwnerID");
				System.out.println("Owner Assigned to Lead 2 >>>>>>>>" + lead2.getLeadFullName() + ">>>" + ownerAssignedToLead2);
				
				// Validation
				if(! ownerAssignedToLead1.equalsIgnoreCase(ownerAssignedToLead2))
				{
					fc.utobj().throwsException("Lead Owner of duplicate lead2 is not same as of lead1 despite linked to same Division - FAILED");
				}
				
				fc.commonMethods().getModules().clickSalesModule(driver);

				Sales sales = new Sales(driver);
				sales.clickLeadManagement();

				LeadManagementTest leadManagementTest = new LeadManagementTest(driver);
				leadManagementTest.leadSearch(lead2);
				
				PrimaryInfoTest primaryInfoTest = new PrimaryInfoTest(driver);
				leadManagementTest.clickCheckBoxForLead(lead2);
				leadManagementTest.clickChangeOwner_BottomButton();
				fc.utobj().switchFrameById(driver, "cboxIframe");
				fc.utobj().selectDropDown(driver, fc.utobj().getElementByID(driver, "leadOwnerID"), "FranConnect Administrator");
				fc.utobj().clickElement(driver, fc.utobj().getElementByName(driver, "change_status"));
				fc.utobj().acceptAlertBox(driver);
				fc.utobj().clickElement(driver, fc.utobj().getElementByName(driver, "Close"));
				fc.utobj().switchFrameToDefault(driver);
				
				leadManagementTest.leadSearch(lead1);
				
				// Fetch Owner assigned to Lead through Rest API
				String xmlNew1 = common.getLeadDetailsThroughWebServices_MajorFields(null, lead1);
				String ownerAssignedToLeadNew1 = common.getValFromXML(xmlNew1, "leadOwnerID");
				System.out.println("Owner Assigned to Lead 1 After Owner Change >>>>>>>>" + lead1.getLeadFullName() + ">>>" + ownerAssignedToLeadNew1);
				
				// Fetch Owner assigned to Lead through Rest API
				String xmlNew2 = common.getLeadDetailsThroughWebServices_MajorFields(null, lead2);
				String ownerAssignedToLeadNew2 = common.getValFromXML(xmlNew2, "leadOwnerID");
				System.out.println("Owner Assigned to Lead 2 After Owner Change >>>>>>>>" + lead2.getLeadFullName() + ">>>" + ownerAssignedToLeadNew2);
				
				// Validation
				if(! ownerAssignedToLeadNew2.equalsIgnoreCase("FranConnect Administrator"))
				{
					fc.utobj().throwsException("Lead Owner of Lead2 is not changed - FAILED");
				}
				
				// Validation
				if(ownerAssignedToLeadNew1.equalsIgnoreCase("ownerAssignedToLeadNew2"))
				{
					fc.utobj().throwsException("Owner of duplicate lead1 has also changed - FAILED");
				}
				
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(priority = 9 , groups = { "DuplicateCriteria" })
	@TestCase(createdOn = "2018-04-27", updatedOn = "2018-05-27", testCaseId = "Sales_DuplicateCriteria_09", testCaseDescription = "All previous duplicate lead's Lead Owner get changed to new manually edited duplicate leads's Lead Owner. (Assign Lead Owner of Existing Lead same as of New Lead)")
	void DuplicateCriteria_09() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			
			fc.commonMethods().getModules().openAdminPage(driver).openConfigureDuplicateCriteriaPage(driver);
			
			ConfigureDuplicateCriteriaTest configureDuplicateCriteriaTest = new ConfigureDuplicateCriteriaTest(driver);
			
			ArrayList<String> DuplicateCriteria1_Fields = new ArrayList<String>();
		//	DuplicateCriteria1_Fields.add("First Name");
			DuplicateCriteria1_Fields.add("Email");
			configureDuplicateCriteriaTest.set_DuplicateCriteria1(DuplicateCriteria1_Fields);
			
			ArrayList<String> DuplicateCriteria2_Fields = new ArrayList<String>();
			DuplicateCriteria2_Fields.add("Last Name");
			DuplicateCriteria2_Fields.add("Company Name");
			configureDuplicateCriteriaTest.set_DuplicateCriteria2(DuplicateCriteria2_Fields);
			 configureDuplicateCriteriaTest.click_ConfigureDuplicateLeadOwnerAssignment();
			 
			 ConfigureDuplicateLeadOwnerAssignmentTest configureDuplicateLeadOwnerAssignmentTest = new ConfigureDuplicateLeadOwnerAssignmentTest(driver);
			 configureDuplicateLeadOwnerAssignmentTest.set_ReassignTheExistingLeadToTheOwnerOfTheNewLead();
	
				// Add Lead 1
				Lead lead1 = new Lead();
				lead1.setFirstName("Duplicate" + fc.utobj().generateRandomNumber6Digit());
				lead1.setLastName("Lead" + fc.utobj().generateRandomNumber());
				lead1.setEmail(""+fc.utobj().generateRandomChar()+"@gmail.com");
				lead1.setBasedonAssignmentRules("yes");
				lead1.setLeadSourceCategory("Friends");
				lead1.setLeadSourceDetails("Friends");
				common.addLeadThroughWebServices(null, lead1);
				
				// Fetch Owner assigned to Lead through Rest API
				String xml1 = common.getLeadDetailsThroughWebServices_MajorFields(null, lead1);
				String ownerAssignedToLead1 = common.getValFromXML(xml1, "leadOwnerID");
				System.out.println("Owner Assigned to Lead >>>>>>>>" + lead1.getLeadFullName() + ">>>" + ownerAssignedToLead1);

				// Add Lead 2
				Lead lead2 = new Lead();
				lead2.setFirstName("Duplicate" + fc.utobj().generateRandomNumber6Digit());
				lead2.setLastName("Lead" + fc.utobj().generateRandomNumber());
				lead2.setEmail(lead1.getEmail());
				lead2.setBasedonAssignmentRules("yes");
				lead2.setLeadSourceCategory("Friends");
				lead2.setLeadSourceDetails("Friends");
				common.addLeadThroughWebServices(null, lead2);

				// Fetch Owner assigned to Lead through Rest API
				String xmlNew1 = common.getLeadDetailsThroughWebServices_MajorFields(null, lead1);
				String ownerAssignedToLeadNew1 = common.getValFromXML(xmlNew1, "leadOwnerID");
				System.out.println("Owner Assigned to Lead >>>>>>>>" + lead1.getLeadFullName() + ">>>" + ownerAssignedToLeadNew1);
				
				// Fetch Owner assigned to Lead through Rest API
				String xml2 = common.getLeadDetailsThroughWebServices_MajorFields(null, lead2);
				String ownerAssignedToLead2 = common.getValFromXML(xml2, "leadOwnerID");
				System.out.println("Owner Assigned to Lead >>>>>>>>" + lead2.getLeadFullName() + ">>>" + ownerAssignedToLead2);
				
				// Validation  - Lead Owner for duplicate lead 1 should change to same lead owner as that of duplicate lead 2 (Same Lead Owners)
				if(! ownerAssignedToLeadNew1.equalsIgnoreCase(ownerAssignedToLead2))
				{
					fc.utobj().throwsException("Lead Owner of Duplicate Lead 1 has not changed to the lead owner of Duplicate Lead 2 - FAILED");
				}
				
				// Manually change the Lead Owner of Lead 2 to "FRANCONNECT ADMINISTRATOR"
				fc.commonMethods().getModules().clickSalesModule(driver);

				Sales sales = new Sales(driver);
				sales.clickLeadManagement();

				LeadManagementTest leadManagementTest = new LeadManagementTest(driver);
				leadManagementTest.leadSearch(lead2);
				
				PrimaryInfoTest primaryInfoTest = new PrimaryInfoTest(driver);
				leadManagementTest.clickCheckBoxForLead(lead2);
				leadManagementTest.clickChangeOwner_BottomButton();
				fc.utobj().switchFrameById(driver, "cboxIframe");
				fc.utobj().selectDropDown(driver, fc.utobj().getElementByID(driver, "leadOwnerID"), "FranConnect Administrator");
				fc.utobj().clickElement(driver, fc.utobj().getElementByName(driver, "change_status"));
				fc.utobj().acceptAlertBox(driver);
				fc.utobj().clickElement(driver, fc.utobj().getElementByName(driver, "Close"));
				fc.utobj().switchFrameToDefault(driver);
				
				leadManagementTest.leadSearch(lead1);
				
				// Fetch Owner assigned to Lead through Rest API
				String xmlNewNew1 = common.getLeadDetailsThroughWebServices_MajorFields(null, lead1);
				String ownerAssignedToLeadNewNew1 = common.getValFromXML(xmlNewNew1, "leadOwnerID");
				System.out.println("Owner Assigned to Lead >>>>>>>>" + lead1.getLeadFullName() + ">>>" + ownerAssignedToLeadNewNew1);
				
				// Validation
				if(ownerAssignedToLeadNewNew1.equalsIgnoreCase("FranConnect Administrator"))
				{
					fc.utobj().throwsException("Lead Owner of Duplicate Lead1 changed after changing the Lead Owner of Duplicate Lead2 - FAILED");
				}
	
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			}
		}
	
	@Test(priority = 10 , groups = { "DuplicateCriteria" })
	@TestCase(createdOn = "2018-04-27", updatedOn = "2018-05-27", testCaseId = "Sales_DuplicateCriteria_10", testCaseDescription = "To verify that on clicking duplicate Icon, all duplicate leads should come")
	void DuplicateCriteria_10() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			
			fc.commonMethods().getModules().clickSalesModule(driver);

			Sales sales = new Sales(driver);
			sales.clickLeadManagement();
			
			LeadSummaryUI leadSummaryUI = new LeadSummaryUI(driver);
			fc.utobj().clickElement(driver, leadSummaryUI.showFilter);
			fc.utobj().clickElement(driver, leadSummaryUI.showOnlyDuplicates_Yes_RadioBtn);
			fc.utobj().clickElement(driver, leadSummaryUI.filterSearchBtn);
			
			List<WebElement> duplicateIconsAgainstLeadNames = fc.utobj().getElementListByXpath(driver, ".//tbody/tr[@class='bText12' or @class='bText12 grAltRw3']//*[contains(.,'')]/following-sibling::img[@id='Image' and contains(@src,'dup_icon_w.gif')]");
			List<WebElement> duplicateLeads = fc.utobj().getElementListByXpath(driver, ".//tbody/tr[@class='bText12' or @class='bText12 grAltRw3']/td[2]//span/a");
			
			if(! (duplicateIconsAgainstLeadNames.size() == duplicateLeads.size()))
			{
				fc.utobj().throwsException("On selecting duplicate filter , Some non duplicate leads are also visible - FAILED");
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(priority = 11 , groups = { "DuplicateCriteria" })
	@TestCase(createdOn = "2018-04-27", updatedOn = "2018-05-27", testCaseId = "Sales_DuplicateCriteria_11", testCaseDescription = "To verify that the Lead Owner for existing duplicate leads does not change when 'Configure duplicate Lead Owner Assignment' settings are changed")
	void DuplicateCriteria_11() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			
			driver = fc.loginpage().login(driver);
			
			Map<String, String> leadAndLeadOwners = new HashMap<String, String>();
			
			for(int i=1 ; i<=5 ; i++)
			{
			Lead lead1 = new Lead();
			lead1.setFirstName("Duplicate" + fc.utobj().generateRandomNumber6Digit());
			lead1.setLastName("Lead" + fc.utobj().generateRandomNumber());
			lead1.setEmail(""+fc.utobj().generateRandomChar()+"@gmail.com");
			lead1.setBasedonAssignmentRules("yes");
			lead1.setLeadSourceCategory("Friends");
			lead1.setLeadSourceDetails("Friends");
			common.addLeadThroughWebServices(null, lead1);
			
			// Fetch Owner assigned to Lead through Rest API
			String xml1 = common.getLeadDetailsThroughWebServices_MajorFields(null, lead1);
			String ownerAssignedToLead1 = common.getValFromXML(xml1, "leadOwnerID");
			System.out.println("Owner Assigned to Lead >>>>>>>>" + lead1.getLeadFullName() + ">>>" + ownerAssignedToLead1);
			
			leadAndLeadOwners.put(lead1.getLeadFullName(), ownerAssignedToLead1);
			}
			
			// Change the duplicate owner settings
			fc.commonMethods().getModules().openAdminPage(driver).openConfigureDuplicateCriteriaPage(driver);
			ConfigureDuplicateCriteriaTest configureDuplicateCriteriaTest = new ConfigureDuplicateCriteriaTest(driver);
			 configureDuplicateCriteriaTest.click_ConfigureDuplicateLeadOwnerAssignment();
			 ConfigureDuplicateLeadOwnerAssignmentTest configureDuplicateLeadOwnerAssignmentTest = new ConfigureDuplicateLeadOwnerAssignmentTest(driver);
			 configureDuplicateLeadOwnerAssignmentTest.set_assignNewLeadToOwnerOfExistingLead();
			 
				for(Map.Entry<String, String> entry : leadAndLeadOwners.entrySet())
				{
					System.out.println(entry.getKey() + " >>>" + entry.getValue());
					String leadFullName = entry.getKey();
					String leadNameArray[] = leadFullName.split(" ");
					String leadFirstName = leadNameArray[0];
					System.out.println(leadFirstName);
					String leadLastName = leadNameArray[1];
					System.out.println(leadLastName);
					// Fetch Owner assigned to Lead through Rest API
					String xml1 = common.getLeadDetailsThroughWebServices_MajorFields(leadFirstName	, leadLastName);
					String ownerAssignedToLead1 = common.getValFromXML(xml1, "leadOwnerID");
					System.out.println("Owner Assigned to Lead >>>>>>>>" + leadFullName + ">>>" + ownerAssignedToLead1);
					
					if(! (entry.getValue().equalsIgnoreCase(ownerAssignedToLead1)))
					{
						fc.utobj().throwsException("Lead Owner for leads have changed after changing duplicate owner settings");
					}
				}
				
				 configureDuplicateLeadOwnerAssignmentTest.set_assignNewLeadToOwnerBasedOnLeadAssignmentRules();
				 
				 for(Map.Entry<String, String> entry : leadAndLeadOwners.entrySet())
					{
						String leadFullName = entry.getKey();
						String leadNameArray[] = leadFullName.split(" ");
						String leadFirstName = leadNameArray[0];
						String leadLastName = leadNameArray[1];
						// Fetch Owner assigned to Lead through Rest API
						String xml1 = common.getLeadDetailsThroughWebServices_MajorFields(leadFirstName	, leadLastName);
						String ownerAssignedToLead1 = common.getValFromXML(xml1, "leadOwnerID");
						System.out.println("Owner Assigned to Lead >>>>>>>>" + leadFullName + ">>>" + ownerAssignedToLead1);
						
						if(! (entry.getValue().equalsIgnoreCase(ownerAssignedToLead1)))
						{
							fc.utobj().throwsException("Lead Owner for leads have changed after changing duplicate owner settings");
						}
					}
				 
				 configureDuplicateLeadOwnerAssignmentTest.set_ReassignTheExistingLeadToTheOwnerOfTheNewLead();

				 for(Map.Entry<String, String> entry : leadAndLeadOwners.entrySet())
					{
						String leadFullName = entry.getKey();
						String leadNameArray[] = leadFullName.split(" ");
						String leadFirstName = leadNameArray[0];
						String leadLastName = leadNameArray[1];
						// Fetch Owner assigned to Lead through Rest API
						String xml1 = common.getLeadDetailsThroughWebServices_MajorFields(leadFirstName	, leadLastName);
						String ownerAssignedToLead1 = common.getValFromXML(xml1, "leadOwnerID");
						System.out.println("Owner Assigned to Lead >>>>>>>>" + leadFullName + ">>>" + ownerAssignedToLead1);
						
						if(! (entry.getValue().equalsIgnoreCase(ownerAssignedToLead1)))
						{
							fc.utobj().throwsException("Lead Owner for leads have changed after changing duplicate owner settings");
						}
					}
				
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
		}
	
	@Test(priority = 12 , groups = { "DuplicateCriteria" })
	@TestCase(createdOn = "2018-04-27", updatedOn = "2018-05-27", testCaseId = "Sales_DuplicateCriteria_12", testCaseDescription = "All previous duplicate lead's Lead Owner get changed to new manually edited duplicate leads's Lead Owner. (Assign Lead Owner of Existing Lead same as of New Lead)")
	void DuplicateCriteria_12() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			
			fc.commonMethods().getModules().openAdminPage(driver).openConfigureDuplicateCriteriaPage(driver);
			
			ConfigureDuplicateCriteriaTest configureDuplicateCriteriaTest = new ConfigureDuplicateCriteriaTest(driver);
			
			ArrayList<String> DuplicateCriteria1_Fields = new ArrayList<String>();
		//	DuplicateCriteria1_Fields.add("First Name");
			DuplicateCriteria1_Fields.add("Email");
			configureDuplicateCriteriaTest.set_DuplicateCriteria1(DuplicateCriteria1_Fields);
			
			ArrayList<String> DuplicateCriteria2_Fields = new ArrayList<String>();
			DuplicateCriteria2_Fields.add("Last Name");
			DuplicateCriteria2_Fields.add("Company Name");
			configureDuplicateCriteriaTest.set_DuplicateCriteria2(DuplicateCriteria2_Fields);
			 configureDuplicateCriteriaTest.click_ConfigureDuplicateLeadOwnerAssignment();
			 
			 ConfigureDuplicateLeadOwnerAssignmentTest configureDuplicateLeadOwnerAssignmentTest = new ConfigureDuplicateLeadOwnerAssignmentTest(driver);
			 configureDuplicateLeadOwnerAssignmentTest.set_assignNewLeadToOwnerOfExistingLead();
	
				// Add Lead 1
				Lead lead1 = new Lead();
				lead1.setFirstName("Duplicate" + fc.utobj().generateRandomNumber6Digit());
				lead1.setLastName("Lead" + fc.utobj().generateRandomNumber());
				lead1.setEmail(""+fc.utobj().generateRandomChar()+"@gmail.com");
				lead1.setBasedonAssignmentRules("yes");
				lead1.setLeadSourceCategory("Friends");
				lead1.setLeadSourceDetails("Friends");
				common.addLeadThroughWebServices(null, lead1);
				
				// Fetch Owner assigned to Lead through Rest API
				String xml1 = common.getLeadDetailsThroughWebServices_MajorFields(null, lead1);
				String ownerAssignedToLead1 = common.getValFromXML(xml1, "leadOwnerID");
				System.out.println("Owner Assigned to Lead >>>>>>>>" + lead1.getLeadFullName() + ">>>" + ownerAssignedToLead1);

				// Add Lead 2
				Lead lead2 = new Lead();
				lead2.setFirstName("Duplicate" + fc.utobj().generateRandomNumber6Digit());
				lead2.setLastName("Lead" + fc.utobj().generateRandomNumber());
				lead2.setEmail(lead1.getEmail());
				lead2.setBasedonAssignmentRules("yes");
				lead2.setLeadSourceCategory("Friends");
				lead2.setLeadSourceDetails("Friends");
				common.addLeadThroughWebServices(null, lead2);

				// Fetch Owner assigned to Lead through Rest API
				String xmlNew1 = common.getLeadDetailsThroughWebServices_MajorFields(null, lead1);
				String ownerAssignedToLeadNew1 = common.getValFromXML(xmlNew1, "leadOwnerID");
				System.out.println("Owner Assigned to Lead >>>>>>>>" + lead1.getLeadFullName() + ">>>" + ownerAssignedToLeadNew1);
				
				// Fetch Owner assigned to Lead through Rest API
				String xml2 = common.getLeadDetailsThroughWebServices_MajorFields(null, lead2);
				String ownerAssignedToLead2 = common.getValFromXML(xml2, "leadOwnerID");
				System.out.println("Owner Assigned to Lead >>>>>>>>" + lead2.getLeadFullName() + ">>>" + ownerAssignedToLead2);
				
				// Validation - Lead Owner of existing duplicate lead should remain same and should not change
				if(! ownerAssignedToLead1.equalsIgnoreCase(ownerAssignedToLeadNew1))
				{
					fc.utobj().throwsException("Lead Owner of Existing Duplicate Lead has changed after adding New Duplicate Lead - FAILED");
				}
				
				// Validation  - Lead Owner of New duplicate lead should be same as that of previous duplicate lead
				if(! ownerAssignedToLeadNew1.equalsIgnoreCase(ownerAssignedToLead2))
				{
					fc.utobj().throwsException("Lead Owner of New Duplicate lead is not same as that of Existing duplicate lead - FAILED");
				}
				
				// Manually change the Lead Owner of Lead 2 to "FRANCONNECT ADMINISTRATOR"
				fc.commonMethods().getModules().clickSalesModule(driver);

				Sales sales = new Sales(driver);
				sales.clickLeadManagement();

				LeadManagementTest leadManagementTest = new LeadManagementTest(driver);
				leadManagementTest.leadSearch(lead2);
				
				PrimaryInfoTest primaryInfoTest = new PrimaryInfoTest(driver);
				leadManagementTest.clickCheckBoxForLead(lead2);
				leadManagementTest.clickChangeOwner_BottomButton();
				fc.utobj().switchFrameById(driver, "cboxIframe");
				fc.utobj().selectDropDown(driver, fc.utobj().getElementByID(driver, "leadOwnerID"), "FranConnect Administrator");
				fc.utobj().clickElement(driver, fc.utobj().getElementByName(driver, "change_status"));
				fc.utobj().acceptAlertBox(driver);
				fc.utobj().clickElement(driver, fc.utobj().getElementByName(driver, "Close"));
				fc.utobj().switchFrameToDefault(driver);
				
				leadManagementTest.leadSearch(lead1);
				
				// Fetch Owner assigned to Lead through Rest API
				String xmlNewNew1 = common.getLeadDetailsThroughWebServices_MajorFields(null, lead1);
				String ownerAssignedToLeadNewNew1 = common.getValFromXML(xmlNewNew1, "leadOwnerID");
				System.out.println("Owner Assigned to Lead >>>>>>>>" + lead1.getLeadFullName() + ">>>" + ownerAssignedToLeadNewNew1);
				
				// Validation
				if(ownerAssignedToLeadNewNew1.equalsIgnoreCase("FranConnect Administrator"))
				{
					fc.utobj().throwsException("Lead Owner of Duplicate Lead1 changed after changing the Lead Owner of Duplicate Lead2 - FAILED");
				}
	
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			}
		}
	
	@Test(priority = 13 , groups = { "DuplicateCriteria" , "DuplicateCriteria_13" })
	@TestCase(createdOn = "2018-04-27", updatedOn = "2018-05-27", testCaseId = "Sales_DuplicateCriteria_13", testCaseDescription = "To verify assigned lead when a duplicate lead is added with Lead Owner selected. When Select Duplicate Lead Owner Assignment Scheme --> Assign Lead Owner of New Lead Same as of Existing Lead, if both leads are of same Division")
	void DuplicateCriteria_13() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			
			// Add Division #1
			AddDivision addDivision1 = new AddDivision();
			ManageDivisionTest manageDivisionTest = new ManageDivisionTest(driver);
			common.set_AddDivision(addDivision1);
			fc.commonMethods().getModules().openAdminPage(driver).openAddDivision(driver);
			manageDivisionTest.addDivision(addDivision1);
		
			// Add User
			CorporateUser corpUser = new CorporateUser();
			
					fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
					corpUser.setFirstName("CorpUser");
					corpUser.setLastName("RoundRobin" + fc.utobj().generateRandomNumber());
					corpUser.setUserName("user" + fc.utobj().generateRandomChar());
					fc.commonMethods().addCorporateUser(driver, corpUser);
					// String myCorpUser = (corpUser.getFirstName() + " " + corpUser.getLastName());
					
			 
			// Configuring Duplicate Criteria
			fc.commonMethods().getModules().openAdminPage(driver).openConfigureDuplicateCriteriaPage(driver);
			ConfigureDuplicateCriteriaTest configureDuplicateCriteriaTest = new ConfigureDuplicateCriteriaTest(driver);
			
			ArrayList<String> DuplicateCriteria1_Fields = new ArrayList<String>();
		//	DuplicateCriteria1_Fields.add("First Name");
			DuplicateCriteria1_Fields.add("Email");
			configureDuplicateCriteriaTest.set_DuplicateCriteria1(DuplicateCriteria1_Fields);
			
			ArrayList<String> DuplicateCriteria2_Fields = new ArrayList<String>();
			DuplicateCriteria2_Fields.add("Last Name");
			DuplicateCriteria2_Fields.add("Company Name");
			configureDuplicateCriteriaTest.set_DuplicateCriteria2(DuplicateCriteria2_Fields);
			 configureDuplicateCriteriaTest.click_ConfigureDuplicateLeadOwnerAssignment();
			 
	
			 ConfigureDuplicateLeadOwnerAssignmentTest configureDuplicateLeadOwnerAssignmentTest = new ConfigureDuplicateLeadOwnerAssignmentTest(driver);
			 configureDuplicateLeadOwnerAssignmentTest.set_onlyAssignNewLeadToSameOwnerAsTheExistingLeadIfTheyAreAssignedToSameDivision();
	
				// Add Lead 1
				Lead lead1 = new Lead();
				lead1.setFirstName("Duplicate" + fc.utobj().generateRandomNumber6Digit());
				lead1.setLastName("Lead" + fc.utobj().generateRandomNumber());
				lead1.setEmail(""+fc.utobj().generateRandomChar()+"@gmail.com");
				lead1.setBasedonAssignmentRules("yes");
				lead1.setLeadSourceCategory("Friends");
				lead1.setLeadSourceDetails("Friends");
				lead1.setDivision(addDivision1.getDivisionName());
				common.addLeadThroughWebServices(null, lead1);
				
				// Fetch Owner assigned to Lead through Rest API
				String xml1 = common.getLeadDetailsThroughWebServices_MajorFields(null, lead1);
				String ownerAssignedToLead1 = common.getValFromXML(xml1, "leadOwnerID");
				System.out.println("Owner Assigned to Lead >>>>>>>>" + lead1.getLeadFullName() + ">>>" + ownerAssignedToLead1);

				// Add Lead 2
				Lead lead2 = new Lead();
				lead2.setFirstName("Duplicate" + fc.utobj().generateRandomNumber6Digit());
				lead2.setLastName("Lead" + fc.utobj().generateRandomNumber());
				lead2.setEmail(lead1.getEmail());
				lead2.setBasedonAssignmentRules("no");
				lead2.setLeadOwner(corpUser.getuserFullName());
				lead2.setLeadSourceCategory("Friends");
				lead2.setLeadSourceDetails("Friends");
				lead2.setDivision(addDivision1.getDivisionName());

				common.addLeadThroughWebServices(null, lead2);

				// Fetch Owner assigned to Lead through Rest API
				String xmlNew1 = common.getLeadDetailsThroughWebServices_MajorFields(null, lead1);
				String ownerAssignedToLeadNew1 = common.getValFromXML(xmlNew1, "leadOwnerID");
				System.out.println("Owner Assigned to Lead >>>>>>>>" + lead1.getLeadFullName() + ">>>" + ownerAssignedToLeadNew1);
				
				// Fetch Owner assigned to Lead through Rest API
				String xml2 = common.getLeadDetailsThroughWebServices_MajorFields(null, lead2);
				String ownerAssignedToLead2 = common.getValFromXML(xml2, "leadOwnerID");
				System.out.println("Owner Assigned to Lead >>>>>>>>" + lead2.getLeadFullName() + ">>>" + ownerAssignedToLead2);
				
				// Validation - The duplicate lead Two's Lead Owner should not be same as that of assigned while creation of the duplicate lead One
				if(ownerAssignedToLead2.equalsIgnoreCase(ownerAssignedToLeadNew1))
				{
					fc.utobj().throwsException("The duplicate lead Two's Lead Owner is same as that of assigned while creation of the duplicate lead One");
				}
				
				if(ownerAssignedToLead2.equalsIgnoreCase(ownerAssignedToLead1))
				{
					fc.utobj().throwsException("The duplicate lead Two's Lead Owner is same as that of assigned while creation of the duplicate lead One");
				}
				
				if(ownerAssignedToLead2.equalsIgnoreCase(corpUser.getuserFullName()))
				{
					
				}
				
				// Validation - The existing lead's Lead Owner should remain unchanged
				if(! ownerAssignedToLead1.equalsIgnoreCase(ownerAssignedToLeadNew1))
				{
					fc.utobj().throwsException("The existing lead's Lead Owner has changed - FAILED");
				}
				
	
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			}
	}
	
	@Test(priority = 14 , groups = { "DuplicateCriteria" , "Sales_DuplicateCriteria_14"})
	@TestCase(createdOn = "2018-04-27", updatedOn = "2018-05-27", testCaseId = "Sales_DuplicateCriteria_14", testCaseDescription = "To verify assigned lead when a duplicate lead is added with Lead Owner selected. When Select Duplicate Lead Owner Assignment Scheme --> Assign Lead Owners based on Assignment Rules")
	void DuplicateCriteria_14() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			
			/*// Add Division #1
			AddDivision addDivision1 = new AddDivision();
			ManageDivisionTest manageDivisionTest = new ManageDivisionTest(driver);
			common.set_AddDivision(addDivision1);
			fc.commonMethods().getModules().openAdminPage(driver).openAddDivision(driver);
			manageDivisionTest.addDivision(addDivision1);*/
		
			// Add User
			CorporateUser corpUser = new CorporateUser();
			
					fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
					corpUser.setFirstName("CorpUser");
					corpUser.setLastName("RoundRobin" + fc.utobj().generateRandomNumber());
					corpUser.setUserName("user" + fc.utobj().generateRandomChar());
					fc.commonMethods().addCorporateUser(driver, corpUser);
				
			 
			// Configuring Duplicate Criteria
			fc.commonMethods().getModules().openAdminPage(driver).openConfigureDuplicateCriteriaPage(driver);
			ConfigureDuplicateCriteriaTest configureDuplicateCriteriaTest = new ConfigureDuplicateCriteriaTest(driver);
			
			ArrayList<String> DuplicateCriteria1_Fields = new ArrayList<String>();
		//	DuplicateCriteria1_Fields.add("First Name");
			DuplicateCriteria1_Fields.add("Email");
			configureDuplicateCriteriaTest.set_DuplicateCriteria1(DuplicateCriteria1_Fields);
			
			ArrayList<String> DuplicateCriteria2_Fields = new ArrayList<String>();
			DuplicateCriteria2_Fields.add("Last Name");
			DuplicateCriteria2_Fields.add("Company Name");
			configureDuplicateCriteriaTest.set_DuplicateCriteria2(DuplicateCriteria2_Fields);
			 configureDuplicateCriteriaTest.click_ConfigureDuplicateLeadOwnerAssignment();
			 
	
			 ConfigureDuplicateLeadOwnerAssignmentTest configureDuplicateLeadOwnerAssignmentTest = new ConfigureDuplicateLeadOwnerAssignmentTest(driver);
			 configureDuplicateLeadOwnerAssignmentTest.set_assignNewLeadToOwnerBasedOnLeadAssignmentRules();
	
				// Add Lead 1
				Lead lead1 = new Lead();
				lead1.setFirstName("Duplicate" + fc.utobj().generateRandomNumber6Digit());
				lead1.setLastName("Lead" + fc.utobj().generateRandomNumber());
				lead1.setEmail(""+fc.utobj().generateRandomChar()+"@gmail.com");
				lead1.setBasedonAssignmentRules("yes");
				lead1.setLeadSourceCategory("Friends");
				lead1.setLeadSourceDetails("Friends");
				common.addLeadThroughWebServices(null, lead1);
				
				// Fetch Owner assigned to Lead through Rest API
				String xml1 = common.getLeadDetailsThroughWebServices_MajorFields(null, lead1);
				String ownerAssignedToLead1 = common.getValFromXML(xml1, "leadOwnerID");
				System.out.println("Owner Assigned to Lead >>>>>>>>" + lead1.getLeadFullName() + ">>>" + ownerAssignedToLead1);

				// Add Lead 2
				Lead lead2 = new Lead();
				lead2.setFirstName("Duplicate" + fc.utobj().generateRandomNumber6Digit());
				lead2.setLastName("Lead" + fc.utobj().generateRandomNumber());
				lead2.setEmail(lead1.getEmail());
				lead2.setBasedonAssignmentRules("no");
				lead2.setLeadOwner(corpUser.getuserFullName());
				lead2.setLeadSourceCategory("Friends");
				lead2.setLeadSourceDetails("Friends");
				common.addLeadThroughWebServices(null, lead2);

				// Fetch Owner assigned to Lead through Rest API
				String xmlNew1 = common.getLeadDetailsThroughWebServices_MajorFields(null, lead1);
				String ownerAssignedToLeadNew1 = common.getValFromXML(xmlNew1, "leadOwnerID");
				System.out.println("Owner Assigned to Lead >>>>>>>>" + lead1.getLeadFullName() + ">>>" + ownerAssignedToLeadNew1);
				
				// Fetch Owner assigned to Lead through Rest API
				String xml2 = common.getLeadDetailsThroughWebServices_MajorFields(null, lead2);
				String ownerAssignedToLead2 = common.getValFromXML(xml2, "leadOwnerID");
				System.out.println("Owner Assigned to Lead >>>>>>>>" + lead2.getLeadFullName() + ">>>" + ownerAssignedToLead2);
				
				// Validation - The duplicate lead's Lead Owner should be same as that of assigned while creation of the duplicate lead
				if(! ownerAssignedToLead2.equalsIgnoreCase(corpUser.getuserFullName()))
				{
					fc.utobj().throwsException("The duplicate lead's Lead Owner is not same as that of assigned while creation of the duplicate lead");
				}
				
				// Validation - The existing lead's Lead Owner should remain unchanged
				if(! ownerAssignedToLead1.equalsIgnoreCase(ownerAssignedToLeadNew1))
				{
					fc.utobj().throwsException("The existing lead's Lead Owner has changed - FAILED");
				}
				
				if(ownerAssignedToLead1.equalsIgnoreCase(ownerAssignedToLead2))
				{
					fc.utobj().throwsException("The existing duplicate lead's Lead Owner is same as that of new duplicate lead's Lead Owner - FAILED");
				}
				
	
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			}
	}
	
	@Test(priority = 15 , groups = { "DuplicateCriteria" , "Sales_DuplicateCriteria_15" })
	@TestCase(createdOn = "2018-04-27", updatedOn = "2018-05-27", testCaseId = "Sales_DuplicateCriteria_15", testCaseDescription = "To verify assigned lead when a duplicate lead is added with Lead Owner selected. When Select Duplicate Lead Owner Assignment Scheme --> Assign Lead Owner of New Lead same as of Existing Lead")
	void DuplicateCriteria_15() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			
			/*// Add Division #1
			AddDivision addDivision1 = new AddDivision();
			ManageDivisionTest manageDivisionTest = new ManageDivisionTest(driver);
			common.set_AddDivision(addDivision1);
			fc.commonMethods().getModules().openAdminPage(driver).openAddDivision(driver);
			manageDivisionTest.addDivision(addDivision1);*/
		
			// Add User
			CorporateUser corpUser = new CorporateUser();
			
					fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
					corpUser.setFirstName("CorpUser");
					corpUser.setLastName("RoundRobin" + fc.utobj().generateRandomNumber());
					corpUser.setUserName("user" + fc.utobj().generateRandomChar());
					fc.commonMethods().addCorporateUser(driver, corpUser);
					// String myCorpUser = (corpUser.getFirstName() + " " + corpUser.getLastName());
				
			 
			// Configuring Duplicate Criteria
			fc.commonMethods().getModules().openAdminPage(driver).openConfigureDuplicateCriteriaPage(driver);
			ConfigureDuplicateCriteriaTest configureDuplicateCriteriaTest = new ConfigureDuplicateCriteriaTest(driver);
			
			ArrayList<String> DuplicateCriteria1_Fields = new ArrayList<String>();
		//	DuplicateCriteria1_Fields.add("First Name");
			DuplicateCriteria1_Fields.add("Email");
			configureDuplicateCriteriaTest.set_DuplicateCriteria1(DuplicateCriteria1_Fields);
			
			ArrayList<String> DuplicateCriteria2_Fields = new ArrayList<String>();
			DuplicateCriteria2_Fields.add("Last Name");
			DuplicateCriteria2_Fields.add("Company Name");
			configureDuplicateCriteriaTest.set_DuplicateCriteria2(DuplicateCriteria2_Fields);
			 configureDuplicateCriteriaTest.click_ConfigureDuplicateLeadOwnerAssignment();
			 
	
			 ConfigureDuplicateLeadOwnerAssignmentTest configureDuplicateLeadOwnerAssignmentTest = new ConfigureDuplicateLeadOwnerAssignmentTest(driver);
			 configureDuplicateLeadOwnerAssignmentTest.selectRadioButton_assignNewLeadToOwnerOfExistingLead();
	
				// Add Lead 1
				Lead lead1 = new Lead();
				lead1.setFirstName("Duplicate" + fc.utobj().generateRandomNumber6Digit());
				lead1.setLastName("Lead" + fc.utobj().generateRandomNumber());
				lead1.setEmail(""+fc.utobj().generateRandomChar()+"@gmail.com");
				lead1.setBasedonAssignmentRules("yes");
				lead1.setLeadSourceCategory("Friends");
				lead1.setLeadSourceDetails("Friends");
				common.addLeadThroughWebServices(null, lead1);
				
				// Fetch Owner assigned to Lead through Rest API
				String xml1 = common.getLeadDetailsThroughWebServices_MajorFields(null, lead1);
				String ownerAssignedToLead1 = common.getValFromXML(xml1, "leadOwnerID");
				System.out.println("Owner Assigned to Lead >>>>>>>>" + lead1.getLeadFullName() + ">>>" + ownerAssignedToLead1);

				// Add Lead 2
				Lead lead2 = new Lead();
				lead2.setFirstName("Duplicate" + fc.utobj().generateRandomNumber6Digit());
				lead2.setLastName("Lead" + fc.utobj().generateRandomNumber());
				lead2.setEmail(lead1.getEmail());
				lead2.setBasedonAssignmentRules("no");
				lead2.setLeadOwner(corpUser.getuserFullName());
				lead2.setLeadSourceCategory("Friends");
				lead2.setLeadSourceDetails("Friends");
				common.addLeadThroughWebServices(null, lead2);

				// Fetch Owner assigned to Lead through Rest API
				String xmlNew1 = common.getLeadDetailsThroughWebServices_MajorFields(null, lead1);
				String ownerAssignedToLeadNew1 = common.getValFromXML(xmlNew1, "leadOwnerID");
				System.out.println("Owner Assigned to Lead >>>>>>>>" + lead1.getLeadFullName() + ">>>" + ownerAssignedToLeadNew1);
				
				// Fetch Owner assigned to Lead through Rest API
				String xml2 = common.getLeadDetailsThroughWebServices_MajorFields(null, lead2);
				String ownerAssignedToLead2 = common.getValFromXML(xml2, "leadOwnerID");
				System.out.println("Owner Assigned to Lead >>>>>>>>" + lead2.getLeadFullName() + ">>>" + ownerAssignedToLead2);
				
				// Validation - The duplicate lead's Lead Owner should be same as that of assigned while creation of the duplicate lead
				if(! ownerAssignedToLead2.equalsIgnoreCase(corpUser.getuserFullName()))
				{
					fc.utobj().throwsException("The duplicate lead's Lead Owner is not same as that of assigned while creation of the duplicate lead");
				}
				
				// Validation - The existing lead's Lead Owner should remain unchanged
				if(! ownerAssignedToLead1.equalsIgnoreCase(ownerAssignedToLeadNew1))
				{
					fc.utobj().throwsException("The existing lead's Lead Owner has changed - FAILED");
				}
				
				if(ownerAssignedToLead1.equalsIgnoreCase(ownerAssignedToLead2))
				{
					fc.utobj().throwsException("The existing duplicate lead's Lead Owner is same as that of new duplicate lead's Lead Owner - FAILED");
				}
				
				
	
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			}
	}
	
	@Test(priority = 16 , groups = { "DuplicateCriteria" , "Sales_DuplicateCriteria_16"})
	@TestCase(createdOn = "2018-04-27", updatedOn = "2018-05-27", testCaseId = "Sales_DuplicateCriteria_16", testCaseDescription = "To verify assigned lead when a duplicate lead is added with Lead Owner selected. When Select Duplicate Lead Owner Assignment Scheme --> Assign Lead Owner of Existing Lead same as of New Lead")
	void DuplicateCriteria_16() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			
			// Add User
			CorporateUser corpUser = new CorporateUser();
			
					fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
					corpUser.setFirstName("CorpUser");
					corpUser.setLastName("RoundRobin" + fc.utobj().generateRandomNumber());
					corpUser.setUserName("user" + fc.utobj().generateRandomChar());
					fc.commonMethods().addCorporateUser(driver, corpUser);
					// String myCorpUser = (corpUser.getFirstName() + " " + corpUser.getLastName());
				
			 
			// Configuring Duplicate Criteria
			fc.commonMethods().getModules().openAdminPage(driver).openConfigureDuplicateCriteriaPage(driver);
			ConfigureDuplicateCriteriaTest configureDuplicateCriteriaTest = new ConfigureDuplicateCriteriaTest(driver);
			
			ArrayList<String> DuplicateCriteria1_Fields = new ArrayList<String>();
		//	DuplicateCriteria1_Fields.add("First Name");
			DuplicateCriteria1_Fields.add("Email");
			configureDuplicateCriteriaTest.set_DuplicateCriteria1(DuplicateCriteria1_Fields);
			
			ArrayList<String> DuplicateCriteria2_Fields = new ArrayList<String>();
			DuplicateCriteria2_Fields.add("Last Name");
			DuplicateCriteria2_Fields.add("Company Name");
			configureDuplicateCriteriaTest.set_DuplicateCriteria2(DuplicateCriteria2_Fields);
			 configureDuplicateCriteriaTest.click_ConfigureDuplicateLeadOwnerAssignment();
			 
	
			 ConfigureDuplicateLeadOwnerAssignmentTest configureDuplicateLeadOwnerAssignmentTest = new ConfigureDuplicateLeadOwnerAssignmentTest(driver);
			 configureDuplicateLeadOwnerAssignmentTest.set_ReassignTheExistingLeadToTheOwnerOfTheNewLead();
	
				// Add Lead 1
				Lead lead1 = new Lead();
				lead1.setFirstName("Duplicate" + fc.utobj().generateRandomNumber6Digit());
				lead1.setLastName("Lead" + fc.utobj().generateRandomNumber());
				lead1.setEmail(""+fc.utobj().generateRandomChar()+"@gmail.com");
				lead1.setBasedonAssignmentRules("yes");
				lead1.setLeadSourceCategory("Friends");
				lead1.setLeadSourceDetails("Friends");
				common.addLeadThroughWebServices(null, lead1);
				
				// Fetch Owner assigned to Lead through Rest API
				String xml1 = common.getLeadDetailsThroughWebServices_MajorFields(null, lead1);
				String ownerAssignedToLead1 = common.getValFromXML(xml1, "leadOwnerID");
				System.out.println("Owner Assigned to Lead >>>>>>>>" + lead1.getLeadFullName() + ">>>" + ownerAssignedToLead1);

				// Add Lead 2
				Lead lead2 = new Lead();
				lead2.setFirstName("Duplicate" + fc.utobj().generateRandomNumber6Digit());
				lead2.setLastName("Lead" + fc.utobj().generateRandomNumber());
				lead2.setEmail(lead1.getEmail());
				lead2.setBasedonAssignmentRules("no");
				lead2.setLeadOwner(corpUser.getuserFullName());
				lead2.setLeadSourceCategory("Friends");
				lead2.setLeadSourceDetails("Friends");
				common.addLeadThroughWebServices(null, lead2);

				// Fetch Owner assigned to Lead through Rest API
				String xmlNew1 = common.getLeadDetailsThroughWebServices_MajorFields(null, lead1);
				String ownerAssignedToLeadNew1 = common.getValFromXML(xmlNew1, "leadOwnerID");
				System.out.println("Owner Assigned to Lead >>>>>>>>" + lead1.getLeadFullName() + ">>>" + ownerAssignedToLeadNew1);
				
				// Fetch Owner assigned to Lead through Rest API
				String xml2 = common.getLeadDetailsThroughWebServices_MajorFields(null, lead2);
				String ownerAssignedToLead2 = common.getValFromXML(xml2, "leadOwnerID");
				System.out.println("Owner Assigned to Lead >>>>>>>>" + lead2.getLeadFullName() + ">>>" + ownerAssignedToLead2);
				
				// Validation - The duplicate lead's Lead Owner should be same as that of assigned while creation of the duplicate lead
				if(! ownerAssignedToLead2.equalsIgnoreCase(corpUser.getuserFullName()))
				{
					fc.utobj().throwsException("The duplicate lead's Lead Owner is not same as that of assigned while creation of the duplicate lead");
				}
				
				// Validation - The existing lead's Lead Owner is updated/changed to Duplicate Lead's Lead Owner
				if(! ownerAssignedToLead2.equalsIgnoreCase(ownerAssignedToLeadNew1))
				{
					fc.utobj().throwsException("The existing lead's Lead Owner is not updated/changed to Duplicate Lead's Lead Owner - FAILED");
				}
				
				if(ownerAssignedToLead1.equalsIgnoreCase(ownerAssignedToLeadNew1))
				{
					fc.utobj().throwsException("The duplicate lead One's Lead Owner is still same even after creation of the duplicate lead two with different  assigned ead owner");
				}
				
	
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			}
	}
	
	@Test(priority = 17 , groups = { "DuplicateCriteria"  , "DuplicateCriteria_17"})
	@TestCase(createdOn = "2018-04-27", updatedOn = "2018-05-27", testCaseId = "Sales_DuplicateCriteria_17", testCaseDescription = "To verify the Lead Owner assigned to a duplicate lead in case where Duplicate Criteria matches but Division is different > Configure Duplicate Lead Owner Assignment ---> * Assign Lead Owner of New Lead same as of Existing Lead, if both leads are of sam...")
	void DuplicateCriteria_17() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			
			// Turn ON Division
			ConfigureNewHierarchyLevel configureNewHierarchyLevel = new ConfigureNewHierarchyLevel();
			ConfigureNewHierarchyLevelTest configureNewHierarchyLevelTest = new ConfigureNewHierarchyLevelTest(driver);
			configureNewHierarchyLevel.setEnableNewHierarchyLevel("Yes");
			fc.commonMethods().getModules().openAdminPage(driver).openConfigureNewHierarchyLevel(driver);
			configureNewHierarchyLevelTest.EnableNewHierarchyLevel(configureNewHierarchyLevel);
			
			ArrayList<String> corpUsersListx = new ArrayList<String>();
			CorporateUser corpUser = new CorporateUser();
			 for (int i = 0; i <= 2; i++) {
					fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
					corpUser.setFirstName("CorpUser" + i);
					corpUser.setLastName("RoundRobin" + fc.utobj().generateRandomNumber());
					corpUser.setUserName("user" + fc.utobj().generateRandomChar());
					fc.commonMethods().addCorporateUser(driver, corpUser);
					corpUsersListx.add(corpUser.getFirstName() + " " + corpUser.getLastName());
				}
			 
				SetupSalesLeadOwnersTest setupSalesLeadOwnersTest = new SetupSalesLeadOwnersTest(driver);
				fc.commonMethods().getModules().openAdminPage(driver).openSetupFranchiseSalesLeadOwnersPage(driver);
				setupSalesLeadOwnersTest.setAllusersAsSalesLeadOwners_AndSubmit();
			 
				fc.commonMethods().getModules().openAdminPage(driver).openAssignLeadOwnersPage(driver);
				
				AssignLeadOwnersTest assignLeadOwnersTest = new AssignLeadOwnersTest(driver);
				AssignLeadOwnerbyRoundRobinTest assignLeadOwnerbyRoundRobinTest = new AssignLeadOwnerbyRoundRobinTest(driver);

				assignLeadOwnersTest.set_defaultOwner("FranConnect Administrator");
				assignLeadOwnersTest.setAssignLeadOwnerbyRoundRobin();
				assignLeadOwnerbyRoundRobinTest.moveAllUserTo_AvailableLeadOwners();
				// System.out.println(corpUsersList);
				for (String temp : corpUsersListx) {
					assignLeadOwnerbyRoundRobinTest.moveUserTo_ConfiguredLeadOwners(temp);
				}
				assignLeadOwnerbyRoundRobinTest.updateButton();
			
			// Add Division #1
			AddDivision addDivision1 = new AddDivision();
			ManageDivisionTest manageDivisionTest = new ManageDivisionTest(driver);
			common.set_AddDivision(addDivision1);
			fc.commonMethods().getModules().openAdminPage(driver).openAddDivision(driver);
			manageDivisionTest.addDivision(addDivision1);
			
			// Add Division #2
			AddDivision addDivision2 = new AddDivision();
			common.set_AddDivision(addDivision2);
			fc.commonMethods().getModules().openAdminPage(driver).openAddDivision(driver);
			manageDivisionTest.addDivision(addDivision2);
			
			// Configuring Duplicate Criteria
			fc.commonMethods().getModules().openAdminPage(driver).openConfigureDuplicateCriteriaPage(driver);
			ConfigureDuplicateCriteriaTest configureDuplicateCriteriaTest = new ConfigureDuplicateCriteriaTest(driver);
			
			ArrayList<String> DuplicateCriteria1_Fields = new ArrayList<String>();
			DuplicateCriteria1_Fields.add("First Name");
			DuplicateCriteria1_Fields.add("Email");
			configureDuplicateCriteriaTest.set_DuplicateCriteria1(DuplicateCriteria1_Fields);
			
			ArrayList<String> DuplicateCriteria2_Fields = new ArrayList<String>();
			DuplicateCriteria2_Fields.add("Last Name");
			DuplicateCriteria2_Fields.add("Company Name");
			configureDuplicateCriteriaTest.set_DuplicateCriteria2(DuplicateCriteria2_Fields);
			 configureDuplicateCriteriaTest.click_ConfigureDuplicateLeadOwnerAssignment();
			 
	
			 ConfigureDuplicateLeadOwnerAssignmentTest configureDuplicateLeadOwnerAssignmentTest = new ConfigureDuplicateLeadOwnerAssignmentTest(driver);
			 configureDuplicateLeadOwnerAssignmentTest.set_onlyAssignNewLeadToSameOwnerAsTheExistingLeadIfTheyAreAssignedToSameDivision();
	
			
			// Add Lead 1
			Lead lead1 = new Lead();
			lead1.setFirstName("Duplicate" + fc.utobj().generateRandomNumber6Digit());
			lead1.setLastName("Lead" + fc.utobj().generateRandomNumber());
			lead1.setEmail("frantest2017@gmail.com");
			lead1.setBasedonAssignmentRules("yes");
			lead1.setDivision(addDivision1.getDivisionName());
			lead1.setLeadSourceCategory("Friends");
			lead1.setLeadSourceDetails("Friends");
			common.addLeadThroughWebServices(null, lead1);
			
			// Fetch Owner assigned to Lead through Rest API
			String xml1 = common.getLeadDetailsThroughWebServices_MajorFields(null, lead1);
			String ownerAssignedToLead1 = common.getValFromXML(xml1, "leadOwnerID");
			System.out.println("Owner Assigned to Lead >>>>>>>>" + lead1.getLeadFullName() + ">>>" + ownerAssignedToLead1);
			
			// Add Lead 2
			Lead lead2 = new Lead();
			lead2.setFirstName(lead1.getFirstName());
			lead2.setLastName("Lead" + fc.utobj().generateRandomNumber());
			lead2.setEmail(lead1.getEmail());
			lead2.setBasedonAssignmentRules("yes");
			lead2.setDivision(addDivision2.getDivisionName());
			lead2.setLeadSourceCategory("Friends");
			lead2.setLeadSourceDetails("Friends");
			common.addLeadThroughWebServices(null, lead2);
			
			// Fetch Owner assigned to Lead through Rest API
			String xml2 = common.getLeadDetailsThroughWebServices_MajorFields(null, lead2);
			String ownerAssignedToLead2 = common.getValFromXML(xml2, "leadOwnerID");
			System.out.println("Owner Assigned to Lead >>>>>>>>" + lead2.getLeadFullName() + ">>>" + ownerAssignedToLead2);
			
			//  Validation
			if(! corpUsersListx.get(0).equalsIgnoreCase(ownerAssignedToLead1) & corpUsersListx.get(1).equalsIgnoreCase(ownerAssignedToLead2) )
			{
				fc.utobj().throwsException("Lead Owners not assigned properly - FAILED");
			}
			
			// Validation
			if(ownerAssignedToLead1.equalsIgnoreCase(ownerAssignedToLead2))
			{
				fc.utobj().throwsException("Same Lead Owners assigned despite leads belonging to different divisions - FAILED");
			}
	
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
	
			}
	}
	
	@Test(priority = 18 , groups = { "DuplicateCriteria"  , "DuplicateCriteria_18"})
	@TestCase(createdOn = "2018-04-27", updatedOn = "2018-05-27", testCaseId = "Sales_DuplicateCriteria_18", testCaseDescription = "To verify Lead Owner Assignment for Existing and Duplicate Lead in case of multi-select for Lead Divisions. Duplicate Criteria set to -->  Assign Lead Owner of New Lead same as of Existing Lead, if both leads are of same Division ")
	void DuplicateCriteria_18() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);

			// Add Division #1
			AddDivision addDivision1 = new AddDivision();
			ManageDivisionTest manageDivisionTest = new ManageDivisionTest(driver);
			common.set_AddDivision(addDivision1);
			fc.commonMethods().getModules().openAdminPage(driver).openAddDivision(driver);
			manageDivisionTest.addDivision(addDivision1);
			
			// Add Division #2
			AddDivision addDivision2 = new AddDivision();
			common.set_AddDivision(addDivision2);
			fc.commonMethods().getModules().openAdminPage(driver).openAddDivision(driver);
			manageDivisionTest.addDivision(addDivision2);
			
			// Add Division #3
			AddDivision addDivision3 = new AddDivision();
			common.set_AddDivision(addDivision3);
			fc.commonMethods().getModules().openAdminPage(driver).openAddDivision(driver);
			manageDivisionTest.addDivision(addDivision3);
			
			// Add Division #4
			AddDivision addDivision4 = new AddDivision();
			common.set_AddDivision(addDivision4);
			fc.commonMethods().getModules().openAdminPage(driver).openAddDivision(driver);
			manageDivisionTest.addDivision(addDivision4);
				
			// Add Division #5
			AddDivision addDivision5 = new AddDivision();
			common.set_AddDivision(addDivision5);
			fc.commonMethods().getModules().openAdminPage(driver).openAddDivision(driver);
			manageDivisionTest.addDivision(addDivision5);
			
			// Turn ON Division
			ConfigureNewHierarchyLevel configureNewHierarchyLevel = new ConfigureNewHierarchyLevel();
			ConfigureNewHierarchyLevelTest configureNewHierarchyLevelTest = new ConfigureNewHierarchyLevelTest(driver);
			configureNewHierarchyLevel.setEnableNewHierarchyLevel("Yes");
			configureNewHierarchyLevel.setLeadsExclusiveToNewHierarchyLevel("no");
			fc.commonMethods().getModules().openAdminPage(driver).openConfigureNewHierarchyLevel(driver);
			configureNewHierarchyLevelTest.EnableNewHierarchyLevel(configureNewHierarchyLevel);
			
			// Add Lead 1
			Lead lead1 = new Lead();
			lead1.setFirstName("Duplicate" + fc.utobj().generateRandomNumber6Digit());
			lead1.setLastName("Lead" + fc.utobj().generateRandomNumber());
			lead1.setEmail("frantest2017@gmail.com");
			lead1.setBasedonAssignmentRules("yes");
			lead1.setDivision(""+addDivision1.getDivisionName()+", "+addDivision2.getDivisionName()+" , "+addDivision3.getDivisionName()+"");
			lead1.setLeadSourceCategory("Friends");
			lead1.setLeadSourceDetails("Friends");
			common.addLeadThroughWebServices(null, lead1);
			
			// Fetch Owner assigned to Lead through Rest API
			String xml1 = common.getLeadDetailsThroughWebServices_MajorFields(null, lead1);
			String ownerAssignedToLead1 = common.getValFromXML(xml1, "leadOwnerID");
			System.out.println("Owner Assigned to Lead >>>>>>>>" + lead1.getLeadFullName() + ">>>" + ownerAssignedToLead1);
			
			// Add Lead 2
			Lead lead2 = new Lead();
			lead2.setFirstName(lead1.getFirstName());
			lead2.setLastName("Lead" + fc.utobj().generateRandomNumber());
			lead2.setEmail(lead1.getEmail());
			lead2.setBasedonAssignmentRules("yes");
			lead2.setDivision(""+addDivision3.getDivisionName()+" , "+addDivision4.getDivisionName()+" , "+addDivision5.getDivisionName()+"");
			lead2.setLeadSourceCategory("Friends");
			lead2.setLeadSourceDetails("Friends");
			common.addLeadThroughWebServices(null, lead2);
			
			// Fetch Owner assigned to Lead through Rest API
			String xmlNew1 = common.getLeadDetailsThroughWebServices_MajorFields(null, lead1);
			String ownerAssignedToLeadNew1 = common.getValFromXML(xmlNew1, "leadOwnerID");
			System.out.println("Owner Assigned to Lead >>>>>>>>" + lead1.getLeadFullName() + ">>>" + ownerAssignedToLeadNew1);
			
			// Fetch Owner assigned to Lead through Rest API
			String xml2 = common.getLeadDetailsThroughWebServices_MajorFields(null, lead2);
			String ownerAssignedToLead2 = common.getValFromXML(xml2, "leadOwnerID");
			System.out.println("Owner Assigned to Lead >>>>>>>>" + lead2.getLeadFullName() + ">>>" + ownerAssignedToLead2);
			
			// Validation - The Lead Owner of the Duplicate lead should be assigned same as that of Existing lead or ( latest duplicate existing lead -> in case of many duplicate leads ) 
			if(! ownerAssignedToLead2.equalsIgnoreCase(ownerAssignedToLead1)){
					fc.utobj().throwsException("Lead Owner of duplicate lead is not same as of existing duplicate lead - FAILED");
			}
			
			// Validation - The Lead Owner of the Existing lead should remain as it was earlier ( as per assignment rule set in build )
			if(! ownerAssignedToLeadNew1.equalsIgnoreCase(ownerAssignedToLead1)){
				fc.utobj().throwsException("Lead Owner of Existing duplicate lead has changed after addition of duplicate lead 2 - FAILED");
			}
		
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
	
			}
	}
	
	
	@Test(priority = 19 , groups = { "DuplicateCriteria"  , "DuplicateCriteria_19"})
	@TestCase(createdOn = "2018-04-27", updatedOn = "2018-05-27", testCaseId = "Sales_DuplicateCriteria_19", testCaseDescription = "To verify Lead Owner Assignment for Existing and Duplicate Lead in case of single-select for Lead Divisions (DIVISIONS DIFFERENT). Duplicate Criteria set to -->  Assign Lead Owner of New Lead same as of Existing Lead, if both leads are of same Division")
	void DuplicateCriteria_19() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);

			// Add Division #1
			AddDivision addDivision1 = new AddDivision();
			ManageDivisionTest manageDivisionTest = new ManageDivisionTest(driver);
			common.set_AddDivision(addDivision1);
			fc.commonMethods().getModules().openAdminPage(driver).openAddDivision(driver);
			manageDivisionTest.addDivision(addDivision1);
			
			// Add Division #2
			AddDivision addDivision2 = new AddDivision();
			common.set_AddDivision(addDivision2);
			fc.commonMethods().getModules().openAdminPage(driver).openAddDivision(driver);
			manageDivisionTest.addDivision(addDivision2);
			
			// Configuring Duplicate Criteria
			fc.commonMethods().getModules().openAdminPage(driver).openConfigureDuplicateCriteriaPage(driver);
			ConfigureDuplicateCriteriaTest configureDuplicateCriteriaTest = new ConfigureDuplicateCriteriaTest(driver);
			 configureDuplicateCriteriaTest.click_ConfigureDuplicateLeadOwnerAssignment();
	
			 ConfigureDuplicateLeadOwnerAssignmentTest configureDuplicateLeadOwnerAssignmentTest = new ConfigureDuplicateLeadOwnerAssignmentTest(driver);
			 configureDuplicateLeadOwnerAssignmentTest.set_onlyAssignNewLeadToSameOwnerAsTheExistingLeadIfTheyAreAssignedToSameDivision();
	
			
			// Add Lead 1
						Lead lead1 = new Lead();
						lead1.setFirstName("Duplicate" + fc.utobj().generateRandomNumber6Digit());
						lead1.setLastName("Lead" + fc.utobj().generateRandomNumber());
						lead1.setEmail("frantest2017@gmail.com");
						lead1.setBasedonAssignmentRules("yes");
						lead1.setDivision(addDivision1.getDivisionName());
						lead1.setLeadSourceCategory("Friends");
						lead1.setLeadSourceDetails("Friends");
						common.addLeadThroughWebServices(null, lead1);
						
						// Fetch Owner assigned to Lead through Rest API
						String xml1 = common.getLeadDetailsThroughWebServices_MajorFields(null, lead1);
						String ownerAssignedToLead1 = common.getValFromXML(xml1, "leadOwnerID");
						System.out.println("Owner Assigned to Lead >>>>>>>>" + lead1.getLeadFullName() + ">>>" + ownerAssignedToLead1);
						
						// Add Lead 2
						Lead lead2 = new Lead();
						lead2.setFirstName(lead1.getFirstName());
						lead2.setLastName("Lead" + fc.utobj().generateRandomNumber());
						lead2.setEmail(lead1.getEmail());
						lead2.setBasedonAssignmentRules("yes");
						lead2.setDivision(addDivision2.getDivisionName());
						lead2.setLeadSourceCategory("Friends");
						lead2.setLeadSourceDetails("Friends");
						common.addLeadThroughWebServices(null, lead2);
						
						// Fetch Owner assigned to Lead through Rest API
						String xml2 = common.getLeadDetailsThroughWebServices_MajorFields(null, lead2);
						String ownerAssignedToLead2 = common.getValFromXML(xml2, "leadOwnerID");
						System.out.println("Owner Assigned to Lead >>>>>>>>" + lead2.getLeadFullName() + ">>>" + ownerAssignedToLead2);
		
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
	
			}
	}
	
	@Test(priority = 20 , groups = { "DuplicateCriteria"  , "DuplicateCriteria_20"})
	@TestCase(createdOn = "2018-04-27", updatedOn = "2018-05-27", testCaseId = "Sales_DuplicateCriteria_20", testCaseDescription = "To verify configuring Duplicate Criteria 1 and 2, Duplicate leads will be marked if they match one or both criterias. (by Company Name, First Name, Last Name, Phone, Email) When above criterias are matching")
	void DuplicateCriteria_20() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			
			// Configuring Duplicate Criteria
			fc.commonMethods().getModules().openAdminPage(driver).openConfigureDuplicateCriteriaPage(driver);
			ConfigureDuplicateCriteriaTest configureDuplicateCriteriaTest = new ConfigureDuplicateCriteriaTest(driver);
			
			ArrayList<String> DuplicateCriteria1_Fields = new ArrayList<String>();
			DuplicateCriteria1_Fields.add("First Name");
			DuplicateCriteria1_Fields.add("Email");
			configureDuplicateCriteriaTest.set_DuplicateCriteria1(DuplicateCriteria1_Fields);
			
			// Add Lead 1
			Lead lead1 = new Lead();
			lead1.setFirstName("Duplicate" + fc.utobj().generateRandomNumber6Digit());
			lead1.setLastName("Lead" + fc.utobj().generateRandomNumber());
			lead1.setEmail("frantest2017@gmail.com");
			lead1.setBasedonAssignmentRules("yes");
			lead1.setLeadSourceCategory("Friends");
			lead1.setLeadSourceDetails("Friends");
			common.addLeadThroughWebServices(null, lead1);
			
			// Fetch Owner assigned to Lead through Rest API
			String xml1 = common.getLeadDetailsThroughWebServices_MajorFields(null, lead1);
			String ownerAssignedToLead1 = common.getValFromXML(xml1, "leadOwnerID");
			System.out.println("Owner Assigned to Lead >>>>>>>>" + lead1.getLeadFullName() + ">>>" + ownerAssignedToLead1);
			
			// Add Lead 2
			Lead lead2 = new Lead();
			lead2.setFirstName(lead1.getFirstName());
			lead2.setLastName("Lead" + fc.utobj().generateRandomNumber());
			lead2.setEmail(lead1.getEmail());
			lead2.setBasedonAssignmentRules("yes");
			lead2.setLeadSourceCategory("Friends");
			lead2.setLeadSourceDetails("Friends");
			common.addLeadThroughWebServices(null, lead2);
			
			// Fetch Owner assigned to Lead through Rest API
			String xml2 = common.getLeadDetailsThroughWebServices_MajorFields(null, lead2);
			String ownerAssignedToLead2 = common.getValFromXML(xml2, "leadOwnerID");
			System.out.println("Owner Assigned to Lead >>>>>>>>" + lead2.getLeadFullName() + ">>>" + ownerAssignedToLead2);

			fc.commonMethods().getModules().clickSalesModule(driver);

			Sales sales = new Sales(driver);
			sales.clickLeadManagement();

			LeadManagementTest leadManagementTest = new LeadManagementTest(driver);
			leadManagementTest.leadSearch(lead2); 
			
			List<WebElement> duplicateIconList = fc.utobj().getElementListByXpath(driver,".//*[contains(.,'"+lead2.getLeadFullName()+"')]/following-sibling::img[@id='Image' and contains(@src,'dup_icon_w.gif')]");
			for (WebElement webElement : duplicateIconList) {
				if (! webElement.getAttribute("onmouseover").contains("Indicates duplicate leads")) {
					fc.utobj().throwsException("Created Duplicate lead is not marked as duplicate - FAILED");
				}
			}
			
			leadManagementTest.leadSearch(lead1); 
			
			List<WebElement> duplicateIconList1 = fc.utobj().getElementListByXpath(driver,".//*[contains(.,'"+lead2.getLeadFullName()+"')]/following-sibling::img[@id='Image' and contains(@src,'dup_icon_w.gif')]");
			for (WebElement webElement : duplicateIconList1) {
				if (! webElement.getAttribute("onmouseover").contains("Indicates duplicate leads")) {
					fc.utobj().throwsException("Created Duplicate lead is not marked as duplicate - FAILED");
				}
			}
				
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(priority = 21 , groups = { "DuplicateCriteria"  , "DuplicateCriteria_21"})
	@TestCase(createdOn = "2018-04-27", updatedOn = "2018-05-27", testCaseId = "Sales_DuplicateCriteria_21", testCaseDescription = "To verify owner assignment when duplicate criteria is matched after modifying a lead. - ReassignTheExistingLeadToTheOwnerOfTheNewLead")
	void DuplicateCriteria_21() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			
						ArrayList<String> corpUsersListx = new ArrayList<String>();
						CorporateUser corpUser = new CorporateUser();
						 for (int i = 0; i <= 2; i++) {
								fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
								corpUser.setFirstName("CorpUser" + i);
								corpUser.setLastName("RoundRobin" + fc.utobj().generateRandomNumber());
								corpUser.setUserName("user" + fc.utobj().generateRandomChar());
								fc.commonMethods().addCorporateUser(driver, corpUser);
								corpUsersListx.add(corpUser.getFirstName() + " " + corpUser.getLastName());
							}
						 
							SetupSalesLeadOwnersTest setupSalesLeadOwnersTest = new SetupSalesLeadOwnersTest(driver);
							fc.commonMethods().getModules().openAdminPage(driver).openSetupFranchiseSalesLeadOwnersPage(driver);
							setupSalesLeadOwnersTest.setAllusersAsSalesLeadOwners_AndSubmit();
						 
							fc.commonMethods().getModules().openAdminPage(driver).openAssignLeadOwnersPage(driver);
							
							AssignLeadOwnersTest assignLeadOwnersTest = new AssignLeadOwnersTest(driver);
							AssignLeadOwnerbyRoundRobinTest assignLeadOwnerbyRoundRobinTest = new AssignLeadOwnerbyRoundRobinTest(driver);

							assignLeadOwnersTest.set_defaultOwner("FranConnect Administrator");
							assignLeadOwnersTest.setAssignLeadOwnerbyRoundRobin();
							assignLeadOwnerbyRoundRobinTest.moveAllUserTo_AvailableLeadOwners();
							for (String temp : corpUsersListx) {
								assignLeadOwnerbyRoundRobinTest.moveUserTo_ConfiguredLeadOwners(temp);
							}
							assignLeadOwnerbyRoundRobinTest.updateButton();
							
							// Configuring Duplicate Criteria
							fc.commonMethods().getModules().openAdminPage(driver).openConfigureDuplicateCriteriaPage(driver);
							ConfigureDuplicateCriteriaTest configureDuplicateCriteriaTest = new ConfigureDuplicateCriteriaTest(driver);
							 configureDuplicateCriteriaTest.click_ConfigureDuplicateLeadOwnerAssignment();
					
							 ConfigureDuplicateLeadOwnerAssignmentTest configureDuplicateLeadOwnerAssignmentTest = new ConfigureDuplicateLeadOwnerAssignmentTest(driver);
							 configureDuplicateLeadOwnerAssignmentTest.set_ReassignTheExistingLeadToTheOwnerOfTheNewLead();
						
			
			// Add Lead 1
			Lead lead1 = new Lead();
			lead1.setFirstName("Duplicate" + fc.utobj().generateRandomNumber6Digit());
			lead1.setLastName("Lead" + fc.utobj().generateRandomNumber());
			lead1.setEmail("frantest2017@gmail.com");
			lead1.setBasedonAssignmentRules("yes");
			lead1.setLeadSourceCategory("Friends");
			lead1.setLeadSourceDetails("Friends");
			common.addLeadThroughWebServices(null, lead1);
			
			// Fetch Owner assigned to Lead through Rest API
			String xml1 = common.getLeadDetailsThroughWebServices_MajorFields(null, lead1);
			String ownerAssignedToLead1 = common.getValFromXML(xml1, "leadOwnerID");
			System.out.println("Owner Assigned to Lead >>>>>>>>" + lead1.getLeadFullName() + ">>>" + ownerAssignedToLead1);
			
			// Add Lead 2
			Lead lead2 = new Lead();
			lead2.setFirstName(lead1.getFirstName());
			lead2.setLastName(lead1.getLastName());
			lead2.setEmail("test2017@gmail.com"); // Non-Duplicate 
			lead2.setBasedonAssignmentRules("yes");
			lead2.setLeadSourceCategory("Friends");
			lead2.setLeadSourceDetails("Friends");
			common.addLeadThroughWebServices(null, lead2);
			
			// Fetch Owner assigned to Lead through Rest API
			String xml2 = common.getLeadDetailsThroughWebServices_MajorFields(null, lead2);
			String ownerAssignedToLead2 = common.getValFromXML(xml2, "leadOwnerID");
			System.out.println("Owner Assigned to Lead >>>>>>>>" + lead2.getLeadFullName() + ">>>" + ownerAssignedToLead2);
			
			// Validation - lead owners should be different as leads are not duplicate leads
			if(ownerAssignedToLead1.equalsIgnoreCase(ownerAssignedToLead2))
			{
				fc.utobj().throwsException("Lead Owners are same despite being non duplicates - FAILED");
			}
			
			// Validation - 
			if(! ownerAssignedToLead1.equalsIgnoreCase(corpUsersListx.get(0)))
			{
				fc.utobj().throwsException("Owner not assigned as per assignment round robin to duplicate lead 1 - FAILED");
			}
			
			// Validation - 
			if(! ownerAssignedToLead2.equalsIgnoreCase(corpUsersListx.get(1)))
			{
				fc.utobj().throwsException("Owner not assigned as per assignment round robin to duplicate lead 2  - FAILED");
			}
			
			// Modify lead 2 email same as that of lead 1 and make them duplicate - through REST Api
			String leadReferenceId = common.getValFromXML(xml2, "referenceId");
			lead2.setEmail(lead1.getEmail());
			common.updateLeadThroughWebServices(driver, lead2, leadReferenceId);
			
			// Fetch lead owner assigned to Lead 2 after modifying and making it duplicate
			String xmlNew1 = common.getLeadDetailsThroughWebServices_MajorFields(null, lead1);
			String ownerAssignedToLeadNew1= common.getValFromXML(xmlNew1, "leadOwnerID");
			System.out.println("ownerAssignedToLead1 after modifying Lead2 and making them duplicates  >>>>>>> "+ownerAssignedToLeadNew1);
			
			// Validate - Lead Owner of existing duplicate lead should update and change
			if(ownerAssignedToLead1.equalsIgnoreCase(ownerAssignedToLeadNew1))
			{
				fc.utobj().throwsException("Lead owner of existing lead has not updated - FAILED");
				// fc.utobj().throwsException("Lead Owner of duplicate lead not same as of  existing lead even after modifying the duplicate lead and making the division same as of existing duplicate lead - FAILED");
			}
			
			// Validation - Lead Owner of Existing duplicate lead should update and match with the owner of  (modified and made duplicate) lead 
			if(! ownerAssignedToLead2.equalsIgnoreCase(ownerAssignedToLeadNew1))
			{
				fc.utobj().throwsException("Lead owner of  lead 1 has not updated to match with the modified and made duplicate lead - FAILED");
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(priority = 22 , groups = { "DuplicateCriteria"  , "DuplicateCriteria_22"})
	@TestCase(createdOn = "2018-04-27", updatedOn = "2018-05-27", testCaseId = "Sales_DuplicateCriteria_22", testCaseDescription = "To verify owner assignment when duplicate criteria is matched after modifying a lead. - onlyAssignNewLeadToSameOwnerAsTheExistingLeadIfTheyAreAssignedToSameDivision")
	void DuplicateCriteria_22() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			
			/*// Turn ON Division
						ConfigureNewHierarchyLevel configureNewHierarchyLevel = new ConfigureNewHierarchyLevel();
						ConfigureNewHierarchyLevelTest configureNewHierarchyLevelTest = new ConfigureNewHierarchyLevelTest(driver);
						configureNewHierarchyLevel.setEnableNewHierarchyLevel("Yes");
						fc.commonMethods().getModules().openAdminPage(driver).openConfigureNewHierarchyLevel(driver);
						configureNewHierarchyLevelTest.EnableNewHierarchyLevel(configureNewHierarchyLevel);*/
						
						// Add Division #1
						AddDivision addDivision1 = new AddDivision();
						ManageDivisionTest manageDivisionTest = new ManageDivisionTest(driver);
						common.set_AddDivision(addDivision1);
						fc.commonMethods().getModules().openAdminPage(driver).openAddDivision(driver);
						manageDivisionTest.addDivision(addDivision1);
						
						// Add Division #2
						AddDivision addDivision2 = new AddDivision();
						common.set_AddDivision(addDivision2);
						fc.commonMethods().getModules().openAdminPage(driver).openAddDivision(driver);
						manageDivisionTest.addDivision(addDivision2);
						
						ArrayList<String> corpUsersListx = new ArrayList<String>();
						CorporateUser corpUser = new CorporateUser();
						 for (int i = 0; i <= 2; i++) {
								fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
								corpUser.setFirstName("CorpUser" + i);
								corpUser.setLastName("RoundRobin" + fc.utobj().generateRandomNumber());
								corpUser.setUserName("user" + fc.utobj().generateRandomChar());
								fc.commonMethods().addCorporateUser(driver, corpUser);
								corpUsersListx.add(corpUser.getFirstName() + " " + corpUser.getLastName());
							}
						 
							SetupSalesLeadOwnersTest setupSalesLeadOwnersTest = new SetupSalesLeadOwnersTest(driver);
							fc.commonMethods().getModules().openAdminPage(driver).openSetupFranchiseSalesLeadOwnersPage(driver);
							setupSalesLeadOwnersTest.setAllusersAsSalesLeadOwners_AndSubmit();
						 
							fc.commonMethods().getModules().openAdminPage(driver).openAssignLeadOwnersPage(driver);
							
							AssignLeadOwnersTest assignLeadOwnersTest = new AssignLeadOwnersTest(driver);
							AssignLeadOwnerbyRoundRobinTest assignLeadOwnerbyRoundRobinTest = new AssignLeadOwnerbyRoundRobinTest(driver);

							assignLeadOwnersTest.set_defaultOwner("FranConnect Administrator");
							assignLeadOwnersTest.setAssignLeadOwnerbyRoundRobin();
							assignLeadOwnerbyRoundRobinTest.moveAllUserTo_AvailableLeadOwners();
							for (String temp : corpUsersListx) {
								assignLeadOwnerbyRoundRobinTest.moveUserTo_ConfiguredLeadOwners(temp);
							}
							assignLeadOwnerbyRoundRobinTest.updateButton();
							
							// Configuring Duplicate Criteria
							fc.commonMethods().getModules().openAdminPage(driver).openConfigureDuplicateCriteriaPage(driver);
							ConfigureDuplicateCriteriaTest configureDuplicateCriteriaTest = new ConfigureDuplicateCriteriaTest(driver);
							 configureDuplicateCriteriaTest.click_ConfigureDuplicateLeadOwnerAssignment();
					
							 ConfigureDuplicateLeadOwnerAssignmentTest configureDuplicateLeadOwnerAssignmentTest = new ConfigureDuplicateLeadOwnerAssignmentTest(driver);
							 configureDuplicateLeadOwnerAssignmentTest.set_onlyAssignNewLeadToSameOwnerAsTheExistingLeadIfTheyAreAssignedToSameDivision();
						
			
			// Add Lead 1
			Lead lead1 = new Lead();
			lead1.setFirstName("Duplicate" + fc.utobj().generateRandomNumber6Digit());
			lead1.setLastName("LeadOne");
			lead1.setEmail(""+fc.utobj().generateRandomChar()+"@gmail.com");
			lead1.setDivision(addDivision1.getDivisionName());
			lead1.setBasedonAssignmentRules("yes");
			lead1.setLeadSourceCategory("Friends");
			lead1.setLeadSourceDetails("Friends");
			common.addLeadThroughWebServices(null, lead1);
			
			// Fetch Owner assigned to Lead through Rest API
			String xml1 = common.getLeadDetailsThroughWebServices_MajorFields(null, lead1);
			String ownerAssignedToLead1 = common.getValFromXML(xml1, "leadOwnerID");
			System.out.println("Owner Assigned to Lead >>>>>>>>" + lead1.getLeadFullName() + ">>>" + ownerAssignedToLead1);
			
			// Add Lead 2
			Lead lead2 = new Lead();
			lead2.setFirstName(lead1.getFirstName());
			lead2.setLastName("LeadTwo");
			lead2.setEmail(lead1.getEmail());
			lead2.setDivision(addDivision2.getDivisionName());
			lead2.setBasedonAssignmentRules("yes");
			lead2.setLeadSourceCategory("Friends");
			lead2.setLeadSourceDetails("Friends");
			common.addLeadThroughWebServices(null, lead2);
			
			// Fetch Owner assigned to Lead through Rest API
			String xml2 = common.getLeadDetailsThroughWebServices_MajorFields(null, lead2);
			String ownerAssignedToLead2 = common.getValFromXML(xml2, "leadOwnerID");
			System.out.println("Owner Assigned to Lead >>>>>>>>" + lead2.getLeadFullName() + ">>>" + ownerAssignedToLead2);
			
			// Validation - lead owners should be different as divisions are different
			if(ownerAssignedToLead1.equalsIgnoreCase(ownerAssignedToLead2))
			{
				fc.utobj().throwsException("Lead Owners are same despite having different divisions - FAILED");
			}
			
			// Validation - 
			if(! ownerAssignedToLead1.equalsIgnoreCase(corpUsersListx.get(0)))
			{
				fc.utobj().throwsException("Owner not assigned as per assignment round robin to duplicate lead 1 - FAILED");
			}
			
			// Validation - 
			if(! ownerAssignedToLead2.equalsIgnoreCase(corpUsersListx.get(1)))
			{
				fc.utobj().throwsException("Owner not assigned as per assignment round robin to duplicate lead 2  - FAILED");
			}
			
			// Modify lead 2 division same as that of lead 1 - through REST Api
			String leadReferenceId = common.getValFromXML(xml2, "referenceId");
			lead2.setDivision(addDivision1.getDivisionName());
			common.updateLeadThroughWebServices(driver, lead2, leadReferenceId);
			
			System.out.println("After modifying LeadTwo division to match LeadOne duolicate");
			
			// Fetch lead owner assigned to Lead 1 after updating duplicate lead with same division
			String xmlNew1 = common.getLeadDetailsThroughWebServices_MajorFields(null, lead1);
			String ownerAssignedToLeadNew1= common.getValFromXML(xmlNew1, "leadOwnerID");
			System.out.println("ownerAssignedToLeadNew1" + lead1.getLeadFullName() + ">>>>>>> " +ownerAssignedToLeadNew1);
			
			// Fetch lead owner assigned to Lead 2 after updating duplicate lead with same division
			String xmlNew2 = common.getLeadDetailsThroughWebServices_MajorFields(null, lead2);
			String ownerAssignedToLeadNew2= common.getValFromXML(xmlNew2, "leadOwnerID");
			System.out.println("ownerAssignedToLeadNew2" +lead2.getLeadFullName()+ ">>>>>>> "+ownerAssignedToLeadNew2);
			
			// Validate - Lead Owner of existing duplicate lead should not update and change
			if( ! (ownerAssignedToLead1.equalsIgnoreCase(ownerAssignedToLeadNew1)))
			{
				fc.utobj().throwsException("Lead Owner of existing duplicate lead has changed after modifying the new duplicate lead and making the division same as of existing duplicate lead - FAILED");
			}
			
			// Validate - Lead Owner of  new duplicate lead should not update and change
			if(! (ownerAssignedToLead2.equalsIgnoreCase(ownerAssignedToLeadNew2)))
			{
				fc.utobj().throwsException("Lead Owner of new duplicate lead has changed after modifying the new duplicate lead and making the division same as of existing duplicate lead - FAILED");
			}
			
			// Validation - Lead Owner of Existing duplicate lead should NOTupdate and match with the owner of  (modified and made duplicate) lead 
			if(ownerAssignedToLeadNew1.equalsIgnoreCase(ownerAssignedToLeadNew2))
			{
				fc.utobj().throwsException("Lead owner of  existing duplicate lead has updated to match with the modified (now same division) new duplicate lead - FAILED");
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}
	
}