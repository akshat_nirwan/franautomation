package com.builds.test.salesTest;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class TestCases_LeadManagement {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "sales", "sales_restructured" , "Sales_Lead_LeadManagement_SendEmail_001"}) // Fixed : Verified // Akshat
	@TestCase(createdOn = "2018-03-07", updatedOn = "2018-03-07", testCaseId = "Sales_Lead_LeadManagement_SendEmail_001", testCaseDescription = "Verify Send Email (Left Action Menu) in batch functionality \nfrom lead summary page")
	private void Sales_Lead_LeadManagement_SendEmail_001() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String basicFirstName = fc.utobj().generateTestData("lead").toLowerCase();

		Sales sales = new Sales(driver);
		Lead lead = sales.commonMethods().fillDefaultValue_LeadDetails();
		Lead lead2 = sales.commonMethods().fillDefaultValue_LeadDetails();
		Email email = sales.commonMethods().fillDefaultValue_Email();

		lead.setFirstName(basicFirstName);
		lead.setEmail("salesautomation@staffex.com");
		lead2.setFirstName(basicFirstName);
		lead2.setEmail("salesautomation@staffex.com");

		Search search = new Search();
		search.setFirstName(lead.getFirstName());

		try {
			driver = fc.loginpage().login(driver);

			fc.commonMethods().getModules().clickSalesModule(driver);
			
			sales.clickLeadManagement().clickAddLeadFillLeadDetailsAndSave(lead);
			sales.clickLeadManagement().clickAddLeadFillLeadDetailsAndSave(lead2);

			sales.clickSearch().fill_And_Search_PrimaryInfoBasedSearch(search).clickCheckBox_All()
					.clickLeftActionsMenu_Click_SendEmail();

			PrimaryInfoTest pinfo = new SendEmail(driver).fillEmailDetails_ClickTopSendButton(email)
					.openLead_LeadManagementPage(lead);

			pinfo.verify_TextUnder_ActivityHistory(email.getSubject());
			pinfo.click_DetailedHistory_ActivityHistory().verify_ActivityHistory_In_DetailedHistory(email.getSubject());

			fc.utobj().readMailBox(email.getSubject(), email.getBody(), "salesautomation@staffex.com", "sdg@1a@Hfs");

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			// e.printStackTrace();
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales", "sales_restructured","sales_failed" , "Sales_LeadManagement_SendEmail_002" }) // Fixed : Verified // Akshat
	@TestCase(createdOn = "2018-03-07", updatedOn = "2018-03-07", testCaseId = "Sales_LeadManagement_SendEmail_002", testCaseDescription = "Verify Send Email (Right Action) from lead summary page")
	private void Sales_Lead_LeadManagement_SendEmail_002() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		Sales sales = new Sales(driver);
		Lead lead = sales.commonMethods().fillDefaultValue_LeadDetails();
		lead.setEmail("salesautomation@staffex.com");

		Email email = sales.commonMethods().fillDefaultValue_Email();
		Search search = new Search();
		search.setFirstName(lead.getFirstName());

		try {
			driver = fc.loginpage().login(driver);

			fc.commonMethods().getModules().clickSalesModule(driver);
			sales.clickLeadManagement().clickAddLeadFillLeadDetailsAndSave(lead);

			sales.clickSearch().fill_And_Search_PrimaryInfoBasedSearch(search)
					.clickRightAction_AndClick_SendEmail(lead.getLeadFullName());

			PrimaryInfoTest pinfo = new SendEmail(driver).fillEmailDetails_ClickTopSendButton(email)
					.openLead_LeadManagementPage(lead);

			pinfo.verify_TextUnder_ActivityHistory(email.getSubject());
			pinfo.click_DetailedHistory_ActivityHistory().verify_ActivityHistory_In_DetailedHistory(email.getSubject());

			fc.utobj().readMailBox(email.getSubject(), email.getBody(), "salesautomation@staffex.com", "sdg@1a@Hfs");

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales031215", "sales_restructured" })
	@TestCase(createdOn = "2018-03-08", updatedOn = "2018-03-08", testCaseId = "Sales_Lead_LeadManagement_SendEmail_003", testCaseDescription = "Verify Send Email (Bottom Button) in batch functionality \nfrom lead summary page")
	private void Sales_Lead_LeadManagement_SendEmail_003() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String basicFirstName = fc.utobj().generateTestData("lead").toLowerCase();

		Sales sales = new Sales(driver);
		Lead lead = sales.commonMethods().fillDefaultValue_LeadDetails();
		Lead lead2 = sales.commonMethods().fillDefaultValue_LeadDetails();
		Email email = sales.commonMethods().fillDefaultValue_Email();

		lead.setFirstName(basicFirstName);
		lead.setEmail("salesautomation@staffex.com");
		lead2.setFirstName(basicFirstName);
		lead2.setEmail("salesautomation@staffex.com");

		Search search = new Search();
		search.setFirstName(lead.getFirstName());

		try {
			driver = fc.loginpage().login(driver);

			sales.clickLeadManagement().clickAddLeadFillLeadDetailsAndSave(lead);
			sales.clickLeadManagement().clickAddLeadFillLeadDetailsAndSave(lead2);

			sales.clickSearch().fill_And_Search_PrimaryInfoBasedSearch(search).clickCheckBox_All()
					.clickSendEmail_BottomButton();

			PrimaryInfoTest pinfo = new SendEmail(driver).fillEmailDetails_ClickTopSendButton(email)
					.openLead_LeadManagementPage(lead);

			pinfo.verify_TextUnder_ActivityHistory(email.getSubject());
			pinfo.click_DetailedHistory_ActivityHistory().verify_ActivityHistory_In_DetailedHistory(email.getSubject());

			fc.utobj().readMailBox(email.getSubject(), email.getBody(), "salesautomation@staffex.com", "sdg@1a@Hfs");

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

}
