package com.builds.test.salesTest;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import com.builds.test.fs.Sales;
import com.builds.uimaps.fs.CallUI;
import com.builds.uimaps.fs.PrimaryInfoUI;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class TestCases_LogACall {
	
	FranconnectUtil fc = new FranconnectUtil();
	
	@Test(groups = { "LogACall" , "LogACall_01" })
	@TestCase(createdOn = "2018-10-18", updatedOn = "2018-10-18", testCaseId = "Sales_LeadManagement_LogACall_01", testCaseDescription = "Verify all fields are there for Log A Call")
	void logACall_01() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try{
			
			driver = fc.loginpage().login(driver);
			Sales_Common_New sales_Common = new Sales_Common_New();
			Lead lead = sales_Common.fillDefaultValue_LeadDetails();
			sales_Common.addLeadThroughWebServices(driver, lead);
			
			fc.commonMethods().getModules().clickSalesModule(driver);
			
			Sales sales = new Sales();
			sales.leadManagement(driver);
			
			LeadManagementTest leadManagementTest = new LeadManagementTest(driver);
			leadManagementTest.leadSearch(lead);
			leadManagementTest.clickCheckBoxForLead(lead);
			leadManagementTest.clickLeftActionsMenu_Click_logACall_AndSwitchToiFrame();
			
			// Validation
			boolean isLeadNameFieldPresent = fc.utobj().isElementPresent(driver, ".//table[@id='logACallTable']/tbody/tr[2]/td[contains(text(),'Lead Name')]/following-sibling::td[contains(text(),'"+lead.getLeadFullName()+"')]");
			if(! isLeadNameFieldPresent)
			{
				fc.utobj().throwsException("Lead Name field not found");
			}
			
			boolean isSubjectFieldPresent = fc.utobj().isElementPresent(driver, ".//table[@id='logACallTable']/tbody/tr[3]/td[contains(text(),'Subject')]/following-sibling::td/input[@type='text']");
			if(! isSubjectFieldPresent)
			{
				fc.utobj().throwsException("Subject field not found");
			}
			
			boolean isDateFieldPresent = fc.utobj().isElementPresent(driver, ".//table[@id='logACallTable']/tbody/tr[4]/td[contains(text(),'Date')]/following-sibling::td");
			if(! isDateFieldPresent)
			{
				fc.utobj().throwsException("Date field not found");
			}
			
			boolean isTimeHourFieldPresent = fc.utobj().isElementPresent(driver, ".//table[@id='logACallTable']/tbody/tr[4]/td[contains(text(),'Time')]/following-sibling::td/select[1]");
			if(! isTimeHourFieldPresent)
			{
				fc.utobj().throwsException("Time Hour drop-down field not found");
			}
			
			boolean isTimeMinFieldPresent = fc.utobj().isElementPresent(driver, ".//table[@id='logACallTable']/tbody/tr[4]/td[contains(text(),'Time')]/following-sibling::td/select[2]");
			if(! isTimeMinFieldPresent)
			{
				fc.utobj().throwsException("Time Min drop-down field not found");
			}
			
			boolean isTimeMeridiemFieldPresent = fc.utobj().isElementPresent(driver, ".//table[@id='logACallTable']/tbody/tr[4]/td[contains(text(),'Time')]/following-sibling::td/select[3]");
			if(! isTimeMeridiemFieldPresent)
			{
				fc.utobj().throwsException("Time Meridiem drop-down field not found");
			}
			
			boolean isCallStatusFieldPresent = fc.utobj().isElementPresent(driver, ".//table[@id='logACallTable']/tbody/tr[5]/td[contains(text(),'Call Status')]/following-sibling::td/select[@id='callStatus']");
			if(! isCallStatusFieldPresent)
			{
				fc.utobj().throwsException("Call Status dropdown Field not found");
			}
			
			boolean isCommunicationTypeFieldPresent = fc.utobj().isElementPresent(driver, ".//table[@id='logACallTable']/tbody/tr[5]/td[contains(text(),'Communication Type')]/following-sibling::td/select[@id='callType']");
			if(! isCommunicationTypeFieldPresent)
			{
				fc.utobj().throwsException("Communication Type dropdown field not found");
			}
			
			boolean isCommentsFieldPresent = fc.utobj().isElementPresent(driver, ".//table[@id='logACallTable']/tbody/tr[6]/td[contains(text(),'Comments ')]/following-sibling::td/label/textarea[@id='comments']");
			if(! isCommentsFieldPresent)
			{
				fc.utobj().throwsException("Comments text Field not found");
			}
			
			boolean isAddButtonPresent = fc.utobj().isElementPresent(driver, ".//td/input[@type='button' and @value='Add']");
			if(! isAddButtonPresent)
			{
				fc.utobj().throwsException("Add button not found ");
			}
			
			boolean isResetButtonPresent = fc.utobj().isElementPresent(driver, ".//td/input[@type='button' and @value='Reset']");
			if(! isResetButtonPresent)
			{
				fc.utobj().throwsException("Reset button not found");
			}
			
			boolean isCloseButtonPresent = fc.utobj().isElementPresent(driver, ".//td/input[@type='button' and @value='Close']");
			if(! isCloseButtonPresent)
			{
				fc.utobj().throwsException("Close button not found");
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	
	@Test(groups = { "LogACall" , "LogACall_02" })
	@TestCase(createdOn = "2018-10-18", updatedOn = "2018-10-18", testCaseId = "Sales_LeadManagement_LogACall_02", testCaseDescription = "To verify Log-A-Call from Lead Management Page through top-left Actions button menu")
	void logACall_02() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try{
			
			driver = fc.loginpage().login(driver);
			Sales_Common_New sales_Common = new Sales_Common_New();
			Lead lead = sales_Common.fillDefaultValue_LeadDetails();
			sales_Common.addLeadThroughWebServices(driver, lead);
			
			FranconnectUtil fc = new FranconnectUtil();
			fc.commonMethods().getModules().clickSalesModule(driver);
			
			Sales sales = new Sales();
			sales.leadManagement(driver);
			
			LeadManagementTest leadManagementTest = new LeadManagementTest(driver);
			leadManagementTest.leadSearch(lead);
			leadManagementTest.clickCheckBoxForLead(lead);
			leadManagementTest.clickLeftActionsMenu_Click_logACall_AndSwitchToiFrame();
						
			Call call = new Call();
			sales_Common.fillDefaultValue_CallDetails(call);
			
			CallTest callTest = new CallTest(driver);
			callTest.fillNewCall(call);
			callTest.click_Add();
			callTest.click_No_ConfirmSchedule();
			
			fc.utobj().switchFrameToDefault(driver);
			leadManagementTest.leadSearch(lead);
			leadManagementTest.openLead_LeadManagementPage(lead);
			
			//Validation #1
			callTest.verify_CallUnderActivityHistory_OnPrimaryInfo(call);
			
			callTest.clickAndOpen_CallUnderActivityHistory_AndSwitchToFrame(call);
			
			//Validation #2
			callTest.verify_callDetailsOniFrame(call);
			
			fc.utobj().switchFrameToDefault(driver);
			
			//Validation #3
			callTest.verify_CallDetailsinDetailedHistory_RemarksAndCallHistoryTab(call);
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
						
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(groups = { "LogACall" , "LogACall_03" })
	@TestCase(createdOn = "2018-10-18", updatedOn = "2018-10-18", testCaseId = "Sales_LeadManagement_LogACall_03", testCaseDescription = "Verify option exists at: Sales > Lead Summary > Actions (single/batch); Sales > Lead Summary > Action Menu of a Lead(Single); Sales > Lead Summary > Lead Primay Info Page (Top and Bottom - Single)")
	void logACall_03() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try{
			driver = fc.loginpage().login(driver);
			
			fc.commonMethods().getModules().clickSalesModule(driver);
			
			Sales sales = new Sales();
			sales.leadManagement(driver);
			
			LeadManagementTest leadManagementTest = new LeadManagementTest(driver);
			
			// Validation
			if(! leadManagementTest.VerifyActionIsPresent_FromLeftActionMenu("Log a Call"))
			{
				fc.utobj().throwsException("Log-a-Call not found on Lead Management Page under top-left actions list");
			}
			
			// Adding a Lead
			Sales_Common_New sales_Common = new Sales_Common_New();
			Lead lead = sales_Common.fillDefaultValue_LeadDetails();
			sales_Common.addLeadThroughWebServices(driver, lead);
			
			sales.leadManagement(driver); 	
			leadManagementTest.leadSearch(lead);
			leadManagementTest.openLead_LeadManagementPage(lead);
			
			// Validation
			PrimaryInfoUI ui = new PrimaryInfoUI(driver);
			boolean isLogACallPresent_Top = fc.utobj().isElementPresent(driver, ui.LogaCall_Link);
			if(! isLogACallPresent_Top)
			{
				fc.utobj().throwsException("Log-a-Call link not found on primary info page of a lead");
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	
}
	
	@Test(groups = { "LogACall" , "LogACall_04" })
	@TestCase(createdOn = "2018-10-18", updatedOn = "2018-10-18", testCaseId = "Sales_LeadManagement_LogACall_04", testCaseDescription = "Verify Activity History/Remarks are created against each change")
	void logACall_04() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try{
			driver = fc.loginpage().login(driver);
			
			fc.commonMethods().getModules().clickSalesModule(driver);
			
			// Adding a Lead
			Sales_Common_New sales_Common = new Sales_Common_New();
			Lead lead = sales_Common.fillDefaultValue_LeadDetails();
			sales_Common.addLeadThroughWebServices(driver, lead);

			Sales sales = new Sales();
			sales.leadManagement(driver);
			LeadManagementTest leadManagementTest = new LeadManagementTest(driver);
			leadManagementTest.leadSearch(lead);
			leadManagementTest.openLead_LeadManagementPage(lead);
			
			PrimaryInfoTest primaryInfoTest = new PrimaryInfoTest(driver);
			primaryInfoTest.click_Log_A_Call_TopLink();
			Call call = new Call();
			sales_Common.fillDefaultValue_CallDetails(call);
			CallTest callTest = new CallTest(driver);
			callTest.fillNewCall(call);
			callTest.click_Add();
			callTest.click_No_ConfirmSchedule();
			fc.utobj().switchFrameToDefault(driver);
			
			// Validation
			if(! fc.utobj().isElementPresent(driver, fc.utobj().getElementByLinkText(driver, call.getSubject())))
			{
				fc.utobj().throwsException("Logged call details are not present on primary info page under Activity History");
			}
			
			// Modify the call details 
			Call callNew = new Call();
			callNew.setSubject("NewCallSubject");
			primaryInfoTest.modify_CallInfo_ActivityHistory(call, callNew);
			
			//Validation #1
			callTest.verify_CallUnderActivityHistory_OnPrimaryInfo(callNew);
			
			callTest.clickAndOpen_CallUnderActivityHistory_AndSwitchToFrame(callNew);
			
			//Validation #2
			callTest.verify_callDetailsOniFrame(callNew);
			
			fc.utobj().switchFrameToDefault(driver);
			
			//Validation #3
			callTest.verify_CallDetailsinDetailedHistory_RemarksAndCallHistoryTab(callNew);
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(groups = { "LogACall" , "LogACall_05" })
	@TestCase(createdOn = "2018-10-18", updatedOn = "2018-10-18", testCaseId = "Sales_LeadManagement_LogACall_05", testCaseDescription = "Verify Log-a-Call from Rest API")
	void logACall_05() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try{
			
			driver = fc.loginpage().login(driver);
			
			fc.commonMethods().getModules().clickSalesModule(driver);

			// Adding a Lead
			Sales_Common_New sales_Common = new Sales_Common_New();
			Lead lead = sales_Common.fillDefaultValue_LeadDetails();
			sales_Common.addLeadThroughWebServices(driver, lead);
			
			Call call = new Call();
			sales_Common.fillDefaultValue_CallDetails(call);
			call.setTimeMM("30");
			call.setDate("2018-12-25");
			
			// Logging a call through Api
			String xml = sales_Common.getLeadDetailsThroughWebServices_MajorFields(driver, lead);
			String referenceID = sales_Common.getValFromXML(xml, "referenceId");
			System.out.println(referenceID);
			sales_Common.logACallThroughWebServices(referenceID, call);
			
			Sales sales = new Sales();
			sales.leadManagement(driver);
			LeadManagementTest leadManagementTest = new LeadManagementTest(driver);
			leadManagementTest.leadSearch(lead);
			leadManagementTest.openLead_LeadManagementPage(lead);
			
			CallTest callTest = new CallTest(driver);

			//Validation #1
			callTest.verify_CallUnderActivityHistory_OnPrimaryInfo(call);
			
			callTest.clickAndOpen_CallUnderActivityHistory_AndSwitchToFrame(call);
			
			//Validation #2
			callTest.verify_callDetailsOniFrame(call);
			
			fc.utobj().switchFrameToDefault(driver);
			
			//Validation #3
			callTest.verify_CallDetailsinDetailedHistory_RemarksAndCallHistoryTab(call);
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(groups = { "LogACall" , "LogACall_06" })
	@TestCase(createdOn = "2018-10-18", updatedOn = "2018-10-18", testCaseId = "Sales_LeadManagement_LogACall_06", testCaseDescription = "To Verify click on 'Yes' when prompted to log a task after logging call in batch")
	void logACall_06() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try{
			
			driver = fc.loginpage().login(driver);
			
			fc.commonMethods().getModules().clickSalesModule(driver);
			Sales_Common_New sales_Common = new Sales_Common_New();

			// Adding 100 Leads
			for(int i=1 ; i<=100 ; i++)
			{
			Lead lead = sales_Common.fillDefaultValue_LeadDetails();
			sales_Common.addLeadThroughWebServices(driver, lead);
			}
			
			Sales sales = new Sales();
			sales.leadManagement(driver);
			LeadManagementTest leadManagementTest = new LeadManagementTest(driver);
			leadManagementTest.showFilters();
			leadManagementTest.selectALL_leadOwnersFilter();	
			leadManagementTest.click_saveView_filter();
			
			leadManagementTest.setViewPerPage(500);
			fc.utobj().sleep();
			leadManagementTest.clickCheckBox_All();
			
			leadManagementTest.clickLeftActionsMenu_Click_logACall_AndSwitchToiFrame();
			
			Call call = new Call();
			sales_Common.fillDefaultValue_CallDetails(call);
			
			CallTest callTest = new CallTest(driver);
			callTest.fillNewCall(call);
			callTest.click_Add();
			callTest.click_No_ConfirmSchedule();
			
			// Validation
			if(! fc.utobj().assertPageSource(driver, "LOG A TASK"))
			{
				fc.utobj().throwsException("Log-a-Task window issue on selecting 'Yes' on confirmation page after Log-a-Call");
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(groups = { "LogACall" , "LogACall_07" })
	@TestCase(createdOn = "2018-10-18", updatedOn = "2018-10-18", testCaseId = "Sales_LeadManagement_LogACall_07", testCaseDescription = "To verify the cancle button while logging a call.")
	void logACall_07() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try{
			
			driver = fc.loginpage().login(driver);
			
			fc.commonMethods().getModules().clickSalesModule(driver);
			Sales_Common_New sales_Common = new Sales_Common_New();
			Lead lead = sales_Common.fillDefaultValue_LeadDetails();
			sales_Common.addLeadThroughWebServices(driver, lead);
			
			FranconnectUtil fc = new FranconnectUtil();
			fc.commonMethods().getModules().clickSalesModule(driver);
			
			Sales sales = new Sales();
			sales.leadManagement(driver);
			
			LeadManagementTest leadManagementTest = new LeadManagementTest(driver);
			leadManagementTest.leadSearch(lead);
			leadManagementTest.clickCheckBoxForLead(lead);
			leadManagementTest.clickLeftActionsMenu_Click_logACall_AndSwitchToiFrame();
			
			CallUI callUI = new CallUI(driver);
			fc.utobj().clickElement(driver, callUI.closeBtn_callDetailsIframe);
			fc.utobj().switchFrameToDefault(driver);
			sales.leadManagement(driver);
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(groups = { "LogACall" , "LogACall_08" })
	@TestCase(createdOn = "2018-10-18", updatedOn = "2018-10-18", testCaseId = "Sales_LeadManagement_LogACall_08", testCaseDescription = "User should be able to modify/delete Call logged which has Email as Communication Type.")
	void logACall_08() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try{
			
			driver = fc.loginpage().login(driver);
			
			fc.commonMethods().getModules().clickSalesModule(driver);
			
			// Adding a Lead
			Sales_Common_New sales_Common = new Sales_Common_New();
			Lead lead = sales_Common.fillDefaultValue_LeadDetails();
			sales_Common.addLeadThroughWebServices(driver, lead);

			Sales sales = new Sales();
			sales.leadManagement(driver);
			LeadManagementTest leadManagementTest = new LeadManagementTest(driver);
			leadManagementTest.leadSearch(lead);
			leadManagementTest.openLead_LeadManagementPage(lead);

			PrimaryInfoTest primaryInfoTest = new PrimaryInfoTest(driver);
			primaryInfoTest.click_Log_A_Call_TopLink();
			Call call = new Call();
			sales_Common.fillDefaultValue_CallDetails(call);
			call.setCommunicationType("Email");
			CallTest callTest = new CallTest(driver);
			callTest.fillNewCall(call);
			callTest.click_Add();
			callTest.click_No_ConfirmSchedule();
			fc.utobj().switchFrameToDefault(driver);
			
			primaryInfoTest.delete_CallInfo_ActivityHistory(call);
			
			// Validation
			if (fc.utobj().assertLinkText(driver, call.getSubject())) {
				fc.utobj().throwsException("Call details are still present in 'Activity History' even after deleting it");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(groups = { "LogACall" , "LogACall_09" })
	@TestCase(createdOn = "2018-10-18", updatedOn = "2018-10-18", testCaseId = "Sales_LeadManagement_LogACall_09", testCaseDescription = "To verify that Call can be logged for multiple lead from lead summary page")
	void logACall_09() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			
			fc.commonMethods().getModules().clickSalesModule(driver);
			Sales_Common_New sales_Common = new Sales_Common_New();

			// Adding 10 Leads
			// Lead 1
			Lead lead1 = sales_Common.fillDefaultValue_LeadDetails();
			sales_Common.addLeadThroughWebServices(driver, lead1);
			
			// Lead 2
			Lead lead2 = sales_Common.fillDefaultValue_LeadDetails();
			sales_Common.addLeadThroughWebServices(driver, lead2);

			// Lead 3
			Lead lead3 = sales_Common.fillDefaultValue_LeadDetails();
			sales_Common.addLeadThroughWebServices(driver, lead3);

			// Lead 4
			Lead lead4 = sales_Common.fillDefaultValue_LeadDetails();
			sales_Common.addLeadThroughWebServices(driver, lead4);

			// Lead 5
			Lead lead5 = sales_Common.fillDefaultValue_LeadDetails();
			sales_Common.addLeadThroughWebServices(driver, lead5);

			// Lead 6
			Lead lead6 = sales_Common.fillDefaultValue_LeadDetails();
			sales_Common.addLeadThroughWebServices(driver, lead6);
			
			// Lead 7
			Lead lead7 = sales_Common.fillDefaultValue_LeadDetails();
			sales_Common.addLeadThroughWebServices(driver, lead7);

			// Lead 8
			Lead lead8 = sales_Common.fillDefaultValue_LeadDetails();
			sales_Common.addLeadThroughWebServices(driver, lead8);
			
			// Lead 9
			Lead lead9 = sales_Common.fillDefaultValue_LeadDetails();
			sales_Common.addLeadThroughWebServices(driver, lead9);
			
			// Lead 10
			Lead lead10 = sales_Common.fillDefaultValue_LeadDetails();
			sales_Common.addLeadThroughWebServices(driver, lead10);
			
			Sales sales = new Sales();
			sales.leadManagement(driver);
			LeadManagementTest leadManagementTest = new LeadManagementTest(driver);
			leadManagementTest.showFilters();
			leadManagementTest.selectALL_leadOwnersFilter();	
			leadManagementTest.click_saveView_filter();
			
			leadManagementTest.setViewPerPage(20);
			fc.utobj().sleep();
			leadManagementTest.clickCheckBox_All();
			
			leadManagementTest.clickLeftActionsMenu_Click_logACall_AndSwitchToiFrame();
			
			Call call = new Call();
			sales_Common.fillDefaultValue_CallDetails(call);
			
			CallTest callTest = new CallTest(driver);
			callTest.fillNewCall(call);
			callTest.click_Add();
			callTest.click_No_ConfirmSchedule();
			
			fc.utobj().switchFrameToDefault(driver);

			// Validations
	
			//Validation #1
			leadManagementTest.leadSearch(lead1);
			leadManagementTest.openLead_LeadManagementPage(lead1);
			callTest.verify_CallUnderActivityHistory_OnPrimaryInfo(call);
			
			sales.leadManagement(driver);

			//Validation #2
			leadManagementTest.leadSearch(lead2);
			leadManagementTest.openLead_LeadManagementPage(lead2);
			callTest.verify_CallUnderActivityHistory_OnPrimaryInfo(call);
			
			

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(groups = { "LogACall", "LogACall_10" })
	@TestCase(createdOn = "2018-10-18", updatedOn = "2018-10-18", testCaseId = "Sales_LeadManagement_LogACall_10", testCaseDescription = "To test that call can be logged through 'Log a call' option of 'Action' menu given next to each lead")
	void logACall_10() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			
			fc.commonMethods().getModules().clickSalesModule(driver);
			
			// Adding a Lead
			Sales_Common_New sales_Common = new Sales_Common_New();
			Lead lead = sales_Common.fillDefaultValue_LeadDetails();
			sales_Common.addLeadThroughWebServices(driver, lead);

			Sales sales = new Sales();
			sales.leadManagement(driver);
			LeadManagementTest leadManagementTest = new LeadManagementTest(driver);
			leadManagementTest.leadSearch(lead);
			
			leadManagementTest.clickRightAction_AndClick_LogACall(lead.getLeadFullName());
			
			Call call = new Call();
			sales_Common.fillDefaultValue_CallDetails(call);
			
			CallTest callTest = new CallTest(driver);
			callTest.fillNewCall(call);
			callTest.click_Add();
			callTest.click_No_ConfirmSchedule();
			
			fc.utobj().switchFrameToDefault(driver);

			// Validation 
			leadManagementTest.leadSearch(lead);
			leadManagementTest.openLead_LeadManagementPage(lead);
			callTest.verify_CallUnderActivityHistory_OnPrimaryInfo(call);
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
}
