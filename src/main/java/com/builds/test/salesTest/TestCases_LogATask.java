package com.builds.test.salesTest;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.test.fs.Sales;
import com.builds.uimaps.fs.PrimaryInfoUI;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class TestCases_LogATask {
	
	FranconnectUtil fc = new FranconnectUtil();
	
	@Test(groups = { "LogATask" , "LogATask_01" })
	@TestCase(createdOn = "2018-10-18", updatedOn = "2018-10-18", testCaseId = "Sales_LeadManagement_LogATask_01", testCaseDescription = "Verify Log a Task from top-left action menu")
	void LogATask_01() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try{
			
			driver = fc.loginpage().login(driver);
			
			Sales_Common_New sales_Common = new Sales_Common_New();
			Lead lead = sales_Common.fillDefaultValue_LeadDetails();
			sales_Common.addLeadThroughWebServices(driver, lead);
			
			FranconnectUtil fc = new FranconnectUtil();
			fc.commonMethods().getModules().clickSalesModule(driver);
			
			Sales sales = new Sales();
			sales.leadManagement(driver);
			
			LeadManagementTest leadManagementTest = new LeadManagementTest(driver);
			leadManagementTest.leadSearch(lead);
			leadManagementTest.clickCheckBoxForLead(lead);
			
			leadManagementTest.clickLeftActionsMenu_Click_logATask();
			Task task = new Task();
			sales_Common.set_DefaultValue_TaskDetails(task);
			
			TaskTest taskTest = new TaskTest(driver);
			taskTest.fillTaskAndClickCreate(task);
			
			sales.leadManagement(driver); 	
			leadManagementTest.leadSearch(lead);
			leadManagementTest.openLead_LeadManagementPage(lead);
			
			// Validation
			PrimaryInfoUI ui = new PrimaryInfoUI(driver);
			fc.utobj().switchFrameById(driver, ui.leadOpenActivitesSummary_IFrame_ById);
			
			if(!fc.utobj().assertLinkText(driver, task.getSubject()))
			{
				fc.utobj().throwsException("Task not logged under Activity History tab on Primary Info page");
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(groups = { "LogATask" , "LogATask_02" })
	@TestCase(createdOn = "2018-10-18", updatedOn = "2018-10-18", testCaseId = "Sales_LeadManagement_LogATask_02", testCaseDescription = "Verify Log-a-Task from top-link on Primary Info page")
	void LogATask_02() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try{
			
			driver = fc.loginpage().login(driver);
			
			Sales_Common_New sales_Common = new Sales_Common_New();
			Lead lead = sales_Common.fillDefaultValue_LeadDetails();
			sales_Common.addLeadThroughWebServices(driver, lead);
			
			FranconnectUtil fc = new FranconnectUtil();
			fc.commonMethods().getModules().clickSalesModule(driver);
			
			Sales sales = new Sales();
			sales.leadManagement(driver);
			
			LeadManagementTest leadManagementTest = new LeadManagementTest(driver);
			leadManagementTest.leadSearch(lead);
			leadManagementTest.openLead_LeadManagementPage(lead);
	
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	
	@Test(groups = { "LogATask" , "LogATask_03" })
	@TestCase(createdOn = "2018-10-18", updatedOn = "2018-10-18", testCaseId = "Sales_LeadManagement_LogATask_03", testCaseDescription = "Verify Log-a-Task all the fields are present on frame")
	void LogATask_03() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try{
			
			driver = fc.loginpage().login(driver);	
			
			Sales_Common_New sales_Common = new Sales_Common_New();
			Lead lead = sales_Common.fillDefaultValue_LeadDetails();
			sales_Common.addLeadThroughWebServices(driver, lead);
			
			FranconnectUtil fc = new FranconnectUtil();
			fc.commonMethods().getModules().clickSalesModule(driver);
			
			Sales sales = new Sales();
			sales.leadManagement(driver);
			
			LeadManagementTest leadManagementTest = new LeadManagementTest(driver);
			leadManagementTest.leadSearch(lead);
			leadManagementTest.clickCheckBoxForLead(lead);
			
			leadManagementTest.clickLeftActionsMenu_Click_logATask();
			
			// Validation
			boolean isLeadOwner_RadioBtn_Present = fc.utobj().isElementPresent(driver, ".//input[@type='radio' and @id='radioOwner']");
			if(! isLeadOwner_RadioBtn_Present)
			{
				fc.utobj().throwsException("Lead Owner radio button not found");
			}
			
			boolean isOtherUser_RadioBtn_Present = fc.utobj().isElementPresent(driver, ".//input[@type='radio' and @id='radioUser']");
			if(! isOtherUser_RadioBtn_Present)
			{
				fc.utobj().throwsException("Other User radio button not found");
			}
			
			// Validation (Select drop-down active , only when Other_User radio is selected)
			
			// click on lead owner radio button
			fc.utobj().clickElement(driver, ".//input[@type='radio' and @id='radioOwner']");
			// verify - drop-down should be de-active
			if(! fc.utobj().getElementByXpath(driver, ".//*[@id='ms-parentassignTo']/button").getAttribute("class").contains("disabled"))
			{
				fc.utobj().throwsException("Other User select drop-down is active , despite lead-owner radio selected");
			}
			
			// click on Other User radio button
			fc.utobj().clickElement(driver, ".//input[@type='radio' and @id='radioUser']");
			// verify - drop-down should be active
			if (fc.utobj().getElementByXpath(driver, ".//*[@id='ms-parentassignTo']/button").getAttribute("class").contains("disabled")) {
				fc.utobj().throwsException("Other User select drop-down is de-active , despite Other-User radio selected");
			}
			
			boolean isStatus_DropDown_Present = fc.utobj().isElementPresent(driver, ".//select[@id='status' and @class='multiList']");
			if(! isStatus_DropDown_Present)
			{
				fc.utobj().throwsException("Status drop-down not found");
			}
			
			boolean isTaskType_DropDown_Present = fc.utobj().isElementPresent(driver, ".//select[@id='taskType' and @class='multiList']");
			if(! isTaskType_DropDown_Present)
			{
				fc.utobj().throwsException("Task Type drop-down not found");
			}
			
			boolean isSubject_TextBox_Present = fc.utobj().isElementPresent(driver, ".//input[@id='subject' and @type='text']");
			if(! isSubject_TextBox_Present)
			{
				fc.utobj().throwsException("Subject text-box not found");
			}	
			
			boolean isTimelessTask_CheckBox_Present = fc.utobj().isElementPresent(driver, ".//input[@name='timelessTaskId' and @type='checkbox']");
			if(! isTimelessTask_CheckBox_Present)
			{
				fc.utobj().throwsException("Timeless Task checkbox is not found");
			}
			
			boolean isPriority_DropDown_Present = fc.utobj().isElementPresent(driver, ".//select[@id='priority' and @class='multiList']");
			if(! isPriority_DropDown_Present)
			{
				fc.utobj().throwsException("Priority dropdown not found");
			}
			
			boolean isStartDate_InputBox_Present = fc.utobj().isElementPresent(driver, ".//input[@id='startDate' and @type='text']");
			if(! isStartDate_InputBox_Present)
			{
				fc.utobj().throwsException("Start Date input box not found");
			}
			
			boolean isAddToCalendar_checkBox_Present = fc.utobj().isElementPresent(driver, ".//input[@name='calendarTaskCheckBox' and @type='checkbox']");
			if(! isAddToCalendar_checkBox_Present)
			{
				fc.utobj().throwsException("Add to Calendar checkbox not found");
			}
			
			boolean isStartTime_Hour_Present = fc.utobj().isElementPresent(driver, ".//select[@name='sTime' and @class='multiList']");
			if(! isStartTime_Hour_Present)
			{
				fc.utobj().throwsException("StartTime Hour dropdown not found");
			}
			
			boolean isStartTime_Min_Present = fc.utobj().isElementPresent(driver, ".//select[@name='sMinute' and @class='multiList']");
			if(! isStartTime_Min_Present)
			{
				fc.utobj().throwsException("StartTime Minute dropdown not found");
			}
			
			boolean isStartTime_AMPM_Present = fc.utobj().isElementPresent(driver, ".//select[@name='APM' and @class='multiList']");
			if(! isStartTime_AMPM_Present)
			{
				fc.utobj().throwsException("StartTime am-pm dropdown not found");
			}
			
			//---
			
			boolean isEndTime_Hour_Present = fc.utobj().isElementPresent(driver, ".//select[@name='eTime' and @class='multiList']");
			if(! isEndTime_Hour_Present)
			{
				fc.utobj().throwsException("EndTime Hour dropdown not found");
			}
			
			boolean isEndTime_Min_Present = fc.utobj().isElementPresent(driver, ".//select[@name='eMinute' and @class='multiList']");
			if(! isEndTime_Min_Present)
			{
				fc.utobj().throwsException("EndTime Minute dropdown not found");
			}
			
			boolean isEndTime_AMPM_Present = fc.utobj().isElementPresent(driver, ".//select[@name='MPM' and @class='multiList']");
			if(! isEndTime_AMPM_Present)
			{
				fc.utobj().throwsException("EndTime am-pm dropdown not found");
			}
			
			boolean isTaskDescription_TextArea_Present = fc.utobj().isElementPresent(driver, ".//textarea[@id='comments' and @class='fTextBox']");
			if(! isTaskDescription_TextArea_Present)
			{
				fc.utobj().throwsException("Task Description Text Area not found");
			}
			
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(groups = { "LogATask" , "LogATask_04" })
	@TestCase(createdOn = "2018-12-06", updatedOn = "2018-12-06", testCaseId = "Sales_LeadManagement_LogATask_04", testCaseDescription = "To verify that Task can be logged through 'Log a task' option of Top 'Action' menu")
	void LogATask_04() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// open browser
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try{
			
			// login
			driver = fc.loginpage().login(driver);	
			
			// fill lead details in lead object
			Sales_Common_New sales_Common = new Sales_Common_New();
			Lead lead = sales_Common.fillDefaultValue_LeadDetails();
			
			// add lead through restAssured api
			RestAssured_API api = new RestAssured_API();
			api.createLead_restAssured(lead);
			
			// go to sales module
			FranconnectUtil fc = new FranconnectUtil();
			fc.commonMethods().getModules().clickSalesModule(driver);
			
			// go to lead management
			Sales sales = new Sales();
			sales.leadManagement(driver);
			
			// search the lead and click checkbox against the lead
			LeadManagementTest leadManagementTest = new LeadManagementTest(driver);
			leadManagementTest.leadSearch(lead);
			leadManagementTest.clickCheckBoxForLead(lead);
			
			// click left top actions menu and click on log a task
			leadManagementTest.clickLeftActionsMenu_Click_logATask();
			
			// set the task details in task object
			Task task = new Task();
			sales_Common.set_DefaultValue_TaskDetails(task);
			
			// fill task details in task pop-up frame
			TaskTest taskTest = new TaskTest(driver);
			taskTest.fillTaskAndClickCreate(task);
			
			// search the lead
			leadManagementTest.leadSearch(lead);
			
			// open the lead primary info
			leadManagementTest.openLead_LeadManagementPage(lead);
			
			// verify task is listed in lead's primary info > contact history > open tasks
			taskTest.verifyTask_leadPrimaryInfo_contactHistory(task);
			
			// verify task is listed in Activity History > Detailed History > Tasks
		    taskTest.verifyTask_detailedHistory(task);
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(groups = { "LogATask" , "LogATask_05" })
	@TestCase(createdOn = "2018-12-06", updatedOn = "2018-12-06", testCaseId = "Sales_LeadManagement_LogATask_05", testCaseDescription = " ")
	void LogATask_05() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// open browser
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try{
			
			// login
			driver = fc.loginpage().login(driver);	
			
			
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
}
