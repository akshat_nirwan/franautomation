package com.builds.test.salesTest;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterGroups;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.builds.test.common.AddDivision;
import com.builds.test.common.ConfigureNewHierarchyLevel;
import com.builds.test.common.ConfigureNewHierarchyLevelTest;
import com.builds.test.common.CorporateUser;
import com.builds.test.common.ManageAreaRegion;
import com.builds.test.common.ManageAreaRegionTest;
import com.builds.test.common.ManageDivisionTest;
import com.builds.uimaps.fs.AssignLeadOwnerbyDivisionUI;
import com.builds.uimaps.fs.AssignLeadOwnersUI;
import com.builds.uimaps.fs.ManageWebFormGeneratorUI;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

import gridpackage.DependentClass;

public class TestCases_OwnerAssignment {
	
	FranconnectUtil fc = new FranconnectUtil();
	Sales_Common_New common = new Sales_Common_New();
	CorporateUser corpUser = new CorporateUser();
	
	ArrayList<String> corpUsers = new ArrayList<String>();
	ArrayList<String> corpUsersForSalesTerritories1 = new ArrayList<String>();
	ArrayList<String> corpUsersForSalesTerritories2 = new ArrayList<String>();
	ArrayList<String> corpUsersForSalesTerritories3 = new ArrayList<String>();
	ArrayList<String> corpUsersForSalesTerritories4 = new ArrayList<String>();
	ArrayList<String> corpUsersForSource = new ArrayList<String>();
	
	SalesTerritories salesTerritories2 = new SalesTerritories(); 
	LeadSourceCategory leadSourceCategory1 = new LeadSourceCategory();
	AddDivision addDivision1 = new AddDivision();
	ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();
	
	// ====================================================================================================================
	
	CorporateUser corpUser2 = new CorporateUser();

	ArrayList<String> corpUsersForSource2 = new ArrayList<String>();
	ArrayList<String> corpUsersForSalesTerritoriesCounty = new ArrayList<String>();
	ArrayList<String> corpUsersForSalesTerritoriesZip = new ArrayList<String>();
	ArrayList<String> corpUsersForSalesTerritoriesStatesDomestic = new ArrayList<String>();
	ArrayList<String> corpUsersForSalesTerritoriesStatesInternational = new ArrayList<String>();
	ArrayList<String> corpUsersForDivision = new ArrayList<String>();
	
	SalesTerritories salesTerritoriesByCounty = new SalesTerritories(); 
	SalesTerritories salesTerritoriesByZip = new SalesTerritories(); 
	SalesTerritories salesTerritoriesByStatesDomestic = new SalesTerritories(); 
	SalesTerritories salesTerritoriesByStatesInternational = new SalesTerritories(); 

	LeadSourceCategory leadSourceCategory2 = new LeadSourceCategory();
	AddDivision addDivision2 = new AddDivision();
	AddDivision addDivision3 = new AddDivision();
	
	// ================================================================================================================
	
	CorporateUser corpUser3 = new CorporateUser();
	ArrayList<String> corpUsersList = new ArrayList<String>();
	
	// ======================================================================================================================
	
	// @BeforeClass(groups = {"sales_OwnerAssignment"})
	@Test( priority = 1 , groups = { "Division_OwnerAssignment" , "sales_OwnerAssignment", "123456" , "sales_OwnerAssignment_failed" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_TC-01", testCaseDescription = "Configure Admin content required for Owner Assignment By Division")
	private void ConfigurationForOwnerAssignmentByDivision() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			
			driver = fc.loginpage().login(driver);
				
		// ADD Corporate Users - through API 
				for (int i = 0; i <= 2; i++) {											// (Adding 3 Corporate Users)
					fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
					corpUser.setFirstName("CorpUser" + i);
					corpUser.setLastName("DivisionRR" + fc.utobj().generateRandomNumber());
					corpUser.setUserName("user" + fc.utobj().generateRandomChar());
					fc.commonMethods().addCorporateUser(null, corpUser);
					System.out.println(corpUser.getFirstName() + " " + corpUser.getLastName());
					corpUsers.add(corpUser.getFirstName() + " " + corpUser.getLastName());
							}
				
				// ADD Corporate Users - through API (Associate different Owner for Sales Territories) - GroupBy(County)
							for (int i = 0; i <= 1; i++) {											// (Adding 2 Corporate Users)
								fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
								corpUser.setFirstName("CorpAssignByDiv" + i);
								corpUser.setLastName("STbyCounty" + fc.utobj().generateRandomNumber());
								corpUser.setUserName("user" + fc.utobj().generateRandomChar());
								fc.commonMethods().addCorporateUser(null, corpUser);
								System.out.println(corpUser.getFirstName() + " " + corpUser.getLastName());
								corpUsersForSalesTerritories1.add(corpUser.getFirstName() + " " + corpUser.getLastName());
							}

							// ADD Corporate Users - through API (Associate different Owner for Sales Territories) - GroupBy(ZIP/Postal Code)
							for (int i = 0; i <= 1; i++) {											// (Adding 2 Corporate Users)
								fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
								corpUser.setFirstName("CorpAssignByDiv" + i);
								corpUser.setLastName("STbyZIP" + fc.utobj().generateRandomNumber());
								corpUser.setUserName("user" + fc.utobj().generateRandomChar());
								fc.commonMethods().addCorporateUser(null, corpUser);
								System.out.println(corpUser.getFirstName() + " " + corpUser.getLastName());
								corpUsersForSalesTerritories2.add(corpUser.getFirstName() + " " + corpUser.getLastName());
							}
							// ADD Corporate Users - through API (Associate different Owner for Sales Territories) - GroupBy(States) Domestic
							for (int i = 0; i <= 1; i++) {											// (Adding 2 Corporate Users)
								fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
								corpUser.setFirstName("CorpAssignByDiv" + i);
								corpUser.setLastName("STbyStatesDomestic" + fc.utobj().generateRandomNumber());
								corpUser.setUserName("user" + fc.utobj().generateRandomChar());
								fc.commonMethods().addCorporateUser(null, corpUser);
								System.out.println(corpUser.getFirstName() + " " + corpUser.getLastName());
								corpUsersForSalesTerritories3.add(corpUser.getFirstName() + " " + corpUser.getLastName());
							}
							// ADD Corporate Users - through API (Associate different Owner for Sales Territories) - GroupBy(States) International
							for (int i = 0; i <= 1; i++) {											// (Adding 2 Corporate Users)
								fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
								corpUser.setFirstName("CorpAssignByDiv" + i);
								corpUser.setLastName("STbyStatesIntl" + fc.utobj().generateRandomNumber());
								corpUser.setUserName("user" + fc.utobj().generateRandomChar());
								fc.commonMethods().addCorporateUser(null, corpUser);
								System.out.println(corpUser.getFirstName() + " " + corpUser.getLastName());
								corpUsersForSalesTerritories4.add(corpUser.getFirstName() + " " + corpUser.getLastName());
							}
							
							// ADD Corporate Users - through API (Associate different Owner for : Source)
										for (int i = 0; i <= 2; i++) {											// (Adding 3 Corporate Users)
											fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
											corpUser.setFirstName("CorpAssignByDiv" + i);
											corpUser.setLastName("Source" + fc.utobj().generateRandomNumber());
											corpUser.setUserName("user" + fc.utobj().generateRandomChar());
											fc.commonMethods().addCorporateUser(null, corpUser);
											System.out.println(corpUser.getFirstName() + " " + corpUser.getLastName());
											corpUsersForSource.add(corpUser.getFirstName() + " " + corpUser.getLastName());
										}

		SetupSalesLeadOwnersTest setupSalesLeadOwnersTest = new SetupSalesLeadOwnersTest(driver);
		AssignLeadOwnersTest assignLeadOwnersTest = new AssignLeadOwnersTest(driver);

		ManageDivisionTest manageDivisionTest = new ManageDivisionTest(driver);
		
		SalesTerritoriesTest salesTerritoriesTest = new SalesTerritoriesTest(driver);
		
		ConfigureNewHierarchyLevelTest configureNewHierarchyLevelTest = new ConfigureNewHierarchyLevelTest(driver);
		ConfigureNewHierarchyLevel configureNewHierarchyLevel = new ConfigureNewHierarchyLevel();
		
		// Lead Source
		LeadSourceCategoryTest leadSourceCategoryTest = new LeadSourceCategoryTest(driver);

			// Turn ON Division
			configureNewHierarchyLevel.setEnableNewHierarchyLevel("Yes");
			configureNewHierarchyLevel.setLeadsExclusiveToNewHierarchyLevel("Yes");
			fc.commonMethods().getModules().openAdminPage(driver).openConfigureNewHierarchyLevel(driver);
			configureNewHierarchyLevelTest.EnableNewHierarchyLevel(configureNewHierarchyLevel);

			// Add Division #1
			//common.set_AddDivision(addDivision1); Random Dynamic Division Name
			addDivision1.setDivisionName("Division" +fc.utobj().generateRandomNumber());
			fc.commonMethods().getModules().openAdminPage(driver).openAddDivision(driver);
			manageDivisionTest.addDivision(addDivision1);
			
			// Adding Lead Source Category #1
			fc.commonMethods().getModules().openAdminPage(driver).openFranchiseSourcePage(driver);
			leadSourceCategory1.setLeadSourceCategory("Lead Source " + fc.utobj().generateRandomNumber());
			leadSourceCategory1.setIncludeInWebPage("yes");
			leadSourceCategoryTest.addLeadSourceCategory(leadSourceCategory1).verify_addLeadSourceCategory(leadSourceCategory1);

			// Setup Sales Lead Owners as ALL
			fc.commonMethods().getModules().openAdminPage(driver).openSetupFranchiseSalesLeadOwnersPage(driver);
			setupSalesLeadOwnersTest.setAllusersAsSalesLeadOwners_AndSubmit();
			
			// ADD Sales Territory #1 - GroupBy(County)
			ArrayList<String> SalesTerritories = new ArrayList<String>();
			SalesTerritories salesTerritories1 = new SalesTerritories(); 
			common.setGroupByCounty_Domestic_filladdNewSalesTerritory(salesTerritories1); // County Domestic
			salesTerritories1.getStates().clear();
			salesTerritories1.getCounties().clear();
			salesTerritories1.getStates().add("Oregon");
			salesTerritories1.getCounties().add("Morrow");
			fc.commonMethods().getModules().openAdminPage(driver).openSalesTerritoryPage(driver);
			salesTerritoriesTest.addNewSalesTerritory(salesTerritories1);
			SalesTerritories.add(salesTerritories1.getSalesTerritoryName()); 
			
			// ADD Sales Territory #2 - GroupBy(ZIP/Postal Code)
			common.setGroupByZipPostalCode_Domestic_filladdNewSalesTerritory(salesTerritories2);
			fc.commonMethods().getModules().openAdminPage(driver).openSalesTerritoryPage(driver);
			salesTerritoriesTest.addNewSalesTerritory(salesTerritories2);
			SalesTerritories.add(salesTerritories2.getSalesTerritoryName()); 
			
			// ADD Sales Territory #3 - GroupBy(States) Domestic
			SalesTerritories salesTerritories3 = new SalesTerritories(); 
			common.setGroupByStates_Domestic_filladdNewSalesTerritory(salesTerritories3);
			salesTerritories3.getStates().clear();
			salesTerritories3.getStates().add("Ohio");
			fc.commonMethods().getModules().openAdminPage(driver).openSalesTerritoryPage(driver);
			salesTerritoriesTest.addNewSalesTerritory(salesTerritories3);
			SalesTerritories.add(salesTerritories3.getSalesTerritoryName()); 

			// ADD Sales Territory #4 - GroupBy(States) International
			SalesTerritories salesTerritories4 = new SalesTerritories(); 
			common.setGroupByStates_International_filladdNewSalesTerritory(salesTerritories4);
			salesTerritories4.getCountries().clear();
			salesTerritories4.getStates().clear();
			salesTerritories4.getCountries().add("Germany");
			salesTerritories4.getStates().add("Hessen");
			fc.commonMethods().getModules().openAdminPage(driver).openSalesTerritoryPage(driver);
			salesTerritoriesTest.addNewSalesTerritory(salesTerritories4);
			SalesTerritories.add(salesTerritories4.getSalesTerritoryName()); 
			
			// Set -> Assign Lead Owner by Division
			fc.commonMethods().getModules().openAdminPage(driver).openAssignLeadOwnersPage(driver);
			assignLeadOwnersTest.set_defaultOwner("FranConnect Administrator");
			assignLeadOwnersTest.set_assignLeadOwnerbyDivision().Configure_AssignLeadOwnerbyDivision(addDivision1.getDivisionName(), corpUsers).AssociateDifferentOwnerFor_Source(corpUsersForSource, addDivision1.getDivisionName(), leadSourceCategory1.getLeadSourceCategory()).AssociateDifferentOwnerFor_SalesTerritories(corpUsersForSalesTerritories1, addDivision1.getDivisionName(),salesTerritories1.getSalesTerritoryName()).AssociateDifferentOwnerFor_SalesTerritories(corpUsersForSalesTerritories2, addDivision1.getDivisionName(), salesTerritories2.getSalesTerritoryName()).AssociateDifferentOwnerFor_SalesTerritories(corpUsersForSalesTerritories3, addDivision1.getDivisionName(), salesTerritories3.getSalesTerritoryName()).AssociateDifferentOwnerFor_SalesTerritories(corpUsersForSalesTerritories4, addDivision1.getDivisionName(), salesTerritories4.getSalesTerritoryName()).setPriority_SalesTerritory();
			
		fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		
		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			// fc.utobj().throwsException(e.toString());
			// System.out.println("Exception =" + e +" "+ "TestCaseID =" +testCaseId);
		}
	}
	
	@Test(priority = 2 , dependsOnMethods="ConfigurationForOwnerAssignmentByDivision" , groups = { "DivPrioritySource123" , "Division_OwnerAssignment" , "sales_OwnerAssignment" , "123456" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_TC-02_Api", testCaseDescription = "Owner Assignment By Division - RoundRobin - Priority(Source) - Api")
	private void OwnerAssignmentByDivision_TC_02_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
				
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		try {
			
			driver = fc.loginpage().login(driver);
			
			// Verify Division RoundRobin
			fc.utobj().printTestStep("Check Division RoundRobin Lead Owner Assignment through API - Adding 6 Leads");
			
			// Through API  ---------------------------------------------------------------------------------------------------------------------------------------------------------------
			Lead lead3 = new Lead();
			for (int j = 0, k = 0; j <= 5; j++, k++) {
				lead3.setFirstName("DivisionLead" + j);
				lead3.setLastName("Api_RoundRobin" + fc.utobj().generateRandomNumber());
				lead3.setEmail("frantest2017@gmail.com");
				lead3.setDivision(addDivision1.getDivisionName());
				lead3.setBasedonAssignmentRules("yes");
				lead3.setLeadSourceCategory("Friends");
				lead3.setLeadSourceDetails("Friends");
				common.addLeadThroughWebServices(driver, lead3);

				
				String xml3 = common.getLeadDetailsThroughWebServices_MajorFields(driver, lead3);
				String ownerAssignedToLead3 = common.getValFromXML(xml3, "leadOwnerID");
				System.out.println("Owner Assigned to Lead "+lead3.getLeadFullName()+" >>>>>>>>" + ownerAssignedToLead3);
				if (k > 2)
					k = 0;
				if (!corpUsers.get(k).equalsIgnoreCase(ownerAssignedToLead3)) {
					System.out.println("Owner should be ::: " + corpUsers.get(k));
					fc.utobj().throwsException("Division RoundRobin Lead Owner Assignment - API  - FAILED");
				}
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			}
	}
	
			@Test(priority = 2 , dependsOnMethods="ConfigurationForOwnerAssignmentByDivision" , groups = { "DivPrioritySource123" , "Division_OwnerAssignment" , "sales_OwnerAssignment" , "123456" })
			@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_TC-02_Import", testCaseDescription = "Owner Assignment By Division - RoundRobin - Priority(Source) - Import")
			private void OwnerAssignmentByDivision_TC_02_Import() throws Exception {
				String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
				}.getClass().getEnclosingMethod().getName());
						
				WebDriver driver = fc.commonMethods().browsers().openBrowser();
				
				try {
					
					driver = fc.loginpage().login(driver);
					
			
			// Through Sales > IMPORT --------------------------------------------------------------------------------------------------------------------------------------------------------
						// Create CSV File with Lead Details
						String filePath = FranconnectUtil.config.get("testLogFolderPath")+"/ImportLead_TC_02.csv";
						System.out.println(filePath);
						List<Lead> leadList = new ArrayList<Lead>(); 

						for (int j = 0; j <= 5; j++) {
						Lead importLead1 = new Lead();
						importLead1.setFirstName("DivisionLead" +j);
						importLead1.setLastName("Import_RoundRobin" + fc.utobj().generateRandomNumber());
						importLead1.setEmail("frantest2017@gmail.com");
						importLead1.setDivision(addDivision1.getDivisionName());
						importLead1.setBasedonAssignmentRules("yes");
						importLead1.setLeadSourceCategory("Friends");
						importLead1.setLeadSourceDetails("Friends");
						
						leadList.add(importLead1);
						}
						
						common.writeCsvFile(filePath, leadList); // Create file in Selenium_Test_Output 
						
						// Import the CSV file 
						Import importlead = new Import();
						ImportTest importTest = new ImportTest(driver);
						
						fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver); // Navigate to Import Tab
						
						importlead.setImportType("Leads");
						importlead.setSpecifyFileFormat("CSV");
						importlead.setSalesDataFile(filePath);
						//importlead.setSalesDataFile(fc.utobj().getFilePathFromTestData("AsgmntByDivPrioritySalesTerritories_GroupByStatesIntl.csv"));
						
						importlead.setLeadStatus("New Lead");
						importlead.setLeadSourceCategory("Friends");
						importlead.setLeadSourceDetails("Friends");
						
						importTest.importLeadsCSV(importlead);
						
						// Validation
						//String csvFile = fc.utobj().getFilePathFromTestData("AsgmntByDivPrioritySalesTerritories_GroupByStatesIntl.csv");
						Map<String,List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);
						
						Lead x = new Lead();
						List<String> leadsFirstNames = map.get("FN");
						List<String> leadsLastNames = map.get("LN");
						

						for(int a=1,b=1,k=0; a<leadsFirstNames.size() & b<leadsLastNames.size(); a++,b++, k++)
						{
							System.out.println(leadsFirstNames.get(a)+ " " +leadsLastNames.get(b));
							x.setFirstName(leadsFirstNames.get(a));
							x.setLastName(leadsLastNames.get(b));
							
							String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
							String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
							System.out.println("Owner Assigned to Lead "+x.getLeadFullName()+" >>>>>>>>" + ownerAssignedToLeadx);
							
							// Validate Owner Assignment
							if (k > 2)
								k = 0;
							if (! (corpUsers.get(k).equalsIgnoreCase(ownerAssignedToLeadx))) {

								System.out.println("Owner should be ::: " + corpUsers.get(k));
								fc.utobj().throwsException("Assign Lead Owner By Division - Division RoundRobin Lead Owner Assignment - Import - FAILED");
							}
						}
						
						fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
						
				} catch (Exception e) {
					fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
					
				}
			
			}
			
			@Test(priority = 2 , dependsOnMethods="ConfigurationForOwnerAssignmentByDivision" , groups = { "DivPrioritySource123" , "Division_OwnerAssignment" , "sales_OwnerAssignment" , "123456" })
			@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_TC-02_WebForm", testCaseDescription = "Owner Assignment By Division - RoundRobin - Priority(Source) - WebForm")
			private void OwnerAssignmentByDivision_TC_02_WebForm() throws Exception {
				String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
				}.getClass().getEnclosingMethod().getName());
						
				WebDriver driver = fc.commonMethods().browsers().openBrowser();
				
				try {
					
					driver = fc.loginpage().login(driver);
			
			// Through WebForm -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
			// Create WebForm 
			ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
			fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
			manageWebFormGenerator.setDivision(addDivision1.getDivisionName());
			common.set_ManageWebform(manageWebFormGenerator);
			//manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory1.getLeadSourceCategory());
			//manageWebFormGenerator.setLeadSourceDetails("None");
			manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);
			
			// Add Lead - WebForm
			Lead webformLead1 = new Lead();
			for (int j = 0, k = 0; j <= 5; j++, k++) {
					webformLead1.setFirstName("LeadDivWebform" + j);
					webformLead1.setLastName("RoundRobin" + fc.utobj().generateRandomNumber());
					webformLead1.setEmail("frantest2017@gmail.com");
					webformLead1.setDivision(addDivision1.getDivisionName());
					webformLead1.setLeadSourceCategory("Friends");
					webformLead1.setLeadSourceDetails("Friends");

				manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, webformLead1); // ADD LEAD THROUGH WEBFORM
																								
				// fc.utobj().sleep();
				String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, webformLead1);
				//System.out.println(xml2);
				String ownerAssignedTowebformLead1 = common.getValFromXML(xmlWebForm1, "leadOwnerID");
				System.out.println("Owner Assigned to Lead "+webformLead1.getLeadFullName()+" >>>>>>>>" + ownerAssignedTowebformLead1);							
				// Validate
				if (k > 2)
					k = 0;
				if (!corpUsers.get(k).equalsIgnoreCase(ownerAssignedTowebformLead1)) {

					System.out.println("Owner should be ::: " + corpUsers.get(k));
					fc.utobj().throwsException("Lead Owner Mismatch - Issue in Division RoundRobin Lead Owner Assignment - WebForm");
				}
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	
	}
	
	@Test(priority = 3 , dependsOnMethods="ConfigurationForOwnerAssignmentByDivision" , groups = { "Division_OwnerAssignment" , "sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_TC-03_Api", testCaseDescription = "Owner Assignment By division - PriorityST_County - API")
	private void OwnerAssignmentByDivision_TC_03_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		try {
			
			driver = fc.loginpage().login(driver);
			
			// Validate Owner Assignment by Division - Priority(Sales Territories) - Sales Territory #1 - GroupBy(County) 
			// Through API ------------------------------------------------------------------------------------------------------------------------------------------------------------------
			Lead lead1 = new Lead();
			for (int j = 0; j <= 2; j++) {
				lead1.setFirstName("LeadDivision_Api" + j);
				lead1.setLastName("PriorityTerritory(County)" + fc.utobj().generateRandomNumber());
				lead1.setEmail("frantest2017@gmail.com");
				lead1.setDivision(addDivision1.getDivisionName());
				lead1.setCountry("USA");
				lead1.setStateProvince("Oregon");
				lead1.setCounty("Morrow");
				lead1.setBasedonAssignmentRules("yes");
				lead1.setLeadSourceCategory(leadSourceCategory1.getLeadSourceCategory());
				lead1.setLeadSourceDetails("None");
							
				common.addLeadThroughWebServices(driver, lead1);
				String xml4 = common.getLeadDetailsThroughWebServices_MajorFields(driver, lead1);
				//System.out.println(xml1);
				String ownerAssignedToLead4 = common.getValFromXML(xml4, "leadOwnerID");
				System.out.println("Owner Assigned to Lead "+lead1.getLeadFullName()+"  >>>>>>>>" + ownerAssignedToLead4);
				
				// Validate
				if (! corpUsersForSalesTerritories1.contains(ownerAssignedToLead4)) {
					System.out.println("Owner should be among ::: " + corpUsersForSalesTerritories1);
								fc.utobj().throwsException("Assign Lead Owner By Division - Priority(Sales Territories - GroupBy(County)) - API - FAILED");
							}
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			
			} catch (Exception e) {
				 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			}
			}
			
			
		// Import does not have "County" as any field , so owner assignment based on county will not work 	
		/*	@Test(priority = 3 , dependsOnMethods="ConfigurationForOwnerAssignmentByDivision" , groups = { "Division_OwnerAssignment" , "sales_OwnerAssignment" })
			@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Division_OwnerAssignment_TC-03_Import", testCaseDescription = "Owner Assignment By division - PriorityST_County - Import")
			private void OwnerAssignmentByDivision_TC_03_Import() throws Exception {
				String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
				}.getClass().getEnclosingMethod().getName());
				
				WebDriver driver = fc.commonMethods().browsers().openBrowser();
				
				try {
					
					driver = fc.loginpage().login(driver);
								
			
			
						// Through Sales > IMPORT -----------------------------------------------------------------------------------------------------------------------------------------------------------------
						// Create CSV File with Lead Details
							String filePath = FranconnectUtil.config.get("testLogFolderPath")+"/ImportLead_TC_03.csv";
							System.out.println(filePath);
							List<Lead> leadList = new ArrayList<Lead>(); 

							for (int j = 0; j <= 2; j++) {
							Lead importLead1 = new Lead();
							importLead1.setFirstName("LeadDivision_Import" +j);
							importLead1.setLastName("PriorityTerritory(County)" + fc.utobj().generateRandomNumber());
							importLead1.setEmail("frantest2017@gmail.com");
							importLead1.setDivision(addDivision1.getDivisionName());
							importLead1.setCountry("USA");
							importLead1.setStateProvince("Oregon");
							importLead1.setCounty("Morrow");
							importLead1.setBasedonAssignmentRules("yes");
							importLead1.setLeadSourceCategory(leadSourceCategory1.getLeadSourceCategory());
							importLead1.setLeadSourceDetails("None");
							
							leadList.add(importLead1);
							}
							
							common.writeCsvFile(filePath, leadList);
							
							// Import the CSV file 
							Import importlead = new Import();
							ImportTest importTest = new ImportTest(driver);
							
							fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver); // Navigate to Import Tab
							
							importlead.setImportType("Leads");
							importlead.setSpecifyFileFormat("CSV");
							importlead.setSalesDataFile(filePath);
							
							importlead.setLeadStatus("New Lead");
							importlead.setLeadSourceCategory(leadSourceCategory1.getLeadSourceCategory());
							importlead.setLeadSourceDetails("None");
							
							importTest.importLeadsCSV(importlead);
							
							// Validation
							Map<String,List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);
							
							Lead x = new Lead();
							List<String> leadsFirstNames = map.get("FN");
							List<String> leadsLastNames = map.get("LN");
							

							for(int a=1,b=1; a<leadsFirstNames.size() & b<leadsLastNames.size(); a++,b++)
							{
								System.out.println(leadsFirstNames.get(a)+ " " +leadsLastNames.get(b));
								x.setFirstName(leadsFirstNames.get(a));
								x.setLastName(leadsLastNames.get(b));
								
								String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
								String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
								System.out.println("Owner Assigned to Lead "+x.getLeadFullName()+" >>>>>>>>" + ownerAssignedToLeadx);
								
								// Validate Owner Assignment
								if (! corpUsersForSalesTerritories1.contains(ownerAssignedToLeadx)) {
									System.out.println("Owner Assigned to Lead "+x.getLeadFullName()+" >>>>>>>>" + ownerAssignedToLeadx);
									fc.utobj().throwsException("Assign Lead Owner By Division - Priority(Sales Territories - GroupBy(County)) - Import - FAILED");
								}
							}
							
							fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
							
				} catch (Exception e) {
					 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
				}
				}*/
			
			@Test(priority = 3 , dependsOnMethods="ConfigurationForOwnerAssignmentByDivision" , groups = { "Division_OwnerAssignment" , "sales_OwnerAssignment" })
			@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_TC-03_WebForm", testCaseDescription = "Owner Assignment By division - PriorityST_County - WebForm")
			private void OwnerAssignmentByDivision_TC_03_WebForm() throws Exception {
				String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
				}.getClass().getEnclosingMethod().getName());
				
				WebDriver driver = fc.commonMethods().browsers().openBrowser();
				
				try {
					
					driver = fc.loginpage().login(driver);
						
							// Through WebForm ----------------------------------------------------------------------------------------------------------------------------------------------------------------
							// Create WebForm 
							ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
							fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
							manageWebFormGenerator.setDivision(addDivision1.getDivisionName());
							common.set_ManageWebform(manageWebFormGenerator);
							manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory1.getLeadSourceCategory());
							manageWebFormGenerator.setLeadSourceDetails("None");
							manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);
							
							// Add Lead - WebForm
							Lead webformLead = new Lead();
							for (int j = 0; j <= 2; j++) {
								webformLead.setFirstName("LeadDivWebform" + j);
								webformLead.setLastName("PriorityTerritory(County)" + fc.utobj().generateRandomNumber());
								webformLead.setEmail("frantest2017@gmail.com");
								webformLead.setDivision(addDivision1.getDivisionName());
								webformLead.setCountry("USA");
								webformLead.setStateProvince("Oregon");
								webformLead.setCounty("Morrow");
								webformLead.setDivision(addDivision1.getDivisionName());
								webformLead.setLeadSourceCategory(leadSourceCategory1.getLeadSourceCategory());
								webformLead.setLeadSourceDetails("None");

								manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, webformLead); // ADD LEAD THROUGH WEBFORM
																												
								// fc.utobj().sleep();
								String xmlWebForm = common.getLeadDetailsThroughWebServices_MajorFields(driver, webformLead);
								//System.out.println(xmlWebForm);
								String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm, "leadOwnerID");
								System.out.println("Owner Assigned to Lead "+webformLead.getLeadFullName()+" >>>>>>>>" + ownerAssignedTowebformLead);							
								// Validate
								if (! corpUsersForSalesTerritories1.contains(ownerAssignedTowebformLead)) {
									System.out.println("Owner should be among ::: " + corpUsersForSalesTerritories1);
									fc.utobj().throwsException("Assign Lead Owner By Division - Priority(Sales Territories - GroupBy(County)) - WebForm - FAILED");
								}
							}
							
							fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			
		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
		}
		

	@Test(priority = 4 , dependsOnMethods="ConfigurationForOwnerAssignmentByDivision" , groups = { "Division_OwnerAssignment" , "sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_TC-04_Api", testCaseDescription = "Verify Lead Owner Assignment By Division with Priority as Sales Territories Group By ZIP - API")
	private void OwnerAssignmentByDivision_TC_04_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		try {
			
			driver = fc.loginpage().login(driver);
			
			// Validate Owner Assignment by Division - Priority(Sales Territories) - Sales Territory #2 - GroupBy(ZIP/Postal Code)
			
			// Through API ------------------------------------------------------------------------------------------------------------------------------------------------------------------------
			Lead lead7 = new Lead();
			for (int j = 0; j <= 2; j++) {
				lead7.setFirstName("LeadDivision_Api" + j);
				lead7.setLastName("PriorityTerritory(ZIP/PostalCode)" + fc.utobj().generateRandomNumber());
				lead7.setEmail("frantest2017@gmail.com");
				lead7.setDivision(addDivision1.getDivisionName());
				lead7.setCountry("USA");
				lead7.setStateProvince("Oregon");
				lead7.setCounty("Morrow");
				lead7.setZipPostalCode(salesTerritories2.getZipPostalCodeText());
				lead7.setBasedonAssignmentRules("yes");
				lead7.setLeadSourceCategory(leadSourceCategory1.getLeadSourceCategory());
				lead7.setLeadSourceDetails("None");
							
				common.addLeadThroughWebServices(driver, lead7);
				String xml7 = common.getLeadDetailsThroughWebServices_MajorFields(driver, lead7);
				//System.out.println(xml1);
				String ownerAssignedToLead7 = common.getValFromXML(xml7, "leadOwnerID");
				System.out.println("Owner Assigned to Lead "+lead7.getLeadFullName()+"  >>>>>>>>" + ownerAssignedToLead7);
				
				// Validate
				if (! corpUsersForSalesTerritories2.contains(ownerAssignedToLead7)) {
					System.out.println("Owner should be among ::: " + corpUsersForSalesTerritories2);
								fc.utobj().throwsException("Assign Lead Owner By Division - Priority(Sales Territories - GroupBy(ZIP/Postal Code)) - API - FAILED");
							}
						}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			
			}
	}
		
		@Test(priority = 4 , dependsOnMethods="ConfigurationForOwnerAssignmentByDivision" , groups = { "Division_OwnerAssignment" , "sales_OwnerAssignment" })
		@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_TC-04_Import", testCaseDescription = "Verify Lead Owner Assignment By Division with Priority as Sales Territories Group By ZIP - Import")
		private void OwnerAssignmentByDivision_TC_04_Import() throws Exception {
			String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
			}.getClass().getEnclosingMethod().getName());
			
			WebDriver driver = fc.commonMethods().browsers().openBrowser();
			
			try {
				
				driver = fc.loginpage().login(driver);
			
			// Through Sales > IMPORT -----------------------------------------------------------------------------------------------------------------------------------------------------------------
			// Create CSV File with Lead Details
				String filePath = FranconnectUtil.config.get("testLogFolderPath")+"/ImportLead_TC_04.csv";
				System.out.println(filePath);
				List<Lead> leadList = new ArrayList<Lead>(); 

				for (int j = 0; j <= 2; j++) {
				Lead importLead1 = new Lead();
				importLead1.setFirstName("LeadDivision_Import" +j);
				importLead1.setLastName("PriorityTerritory(ZIP/PostalCode)" + fc.utobj().generateRandomNumber());
				importLead1.setEmail("frantest2017@gmail.com");
				importLead1.setDivision(addDivision1.getDivisionName());
				importLead1.setCountry("USA");
				importLead1.setStateProvince("Oregon");
				importLead1.setCounty("Morrow");
				importLead1.setZipPostalCode(salesTerritories2.getZipPostalCodeText());
				importLead1.setBasedonAssignmentRules("yes");
				importLead1.setLeadSourceCategory(leadSourceCategory1.getLeadSourceCategory());
				importLead1.setLeadSourceDetails("None");
				
				leadList.add(importLead1);
				}
				
				common.writeCsvFile(filePath, leadList);
				
				// Import the CSV file 
				Import importlead = new Import();
				ImportTest importTest = new ImportTest(driver);
				
				fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver); // Navigate to Import Tab
				
				importlead.setImportType("Leads");
				importlead.setSpecifyFileFormat("CSV");
				importlead.setSalesDataFile(filePath);
				//importlead.setSalesDataFile(fc.utobj().getFilePathFromTestData("AsgmntByDivPrioritySalesTerritories_GroupByStatesIntl.csv"));
				
				importlead.setLeadStatus("New Lead");
				importlead.setLeadSourceCategory(leadSourceCategory1.getLeadSourceCategory());
				importlead.setLeadSourceDetails("None");
				
				importTest.importLeadsCSV(importlead);
				
				// Validation
				//String csvFile = fc.utobj().getFilePathFromTestData("AsgmntByDivPrioritySalesTerritories_GroupByStatesIntl.csv");
				Map<String,List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);
				
				Lead x = new Lead();
				List<String> leadsFirstNames = map.get("FN");
				List<String> leadsLastNames = map.get("LN");
				

				for(int a=1,b=1; a<leadsFirstNames.size() & b<leadsLastNames.size(); a++,b++)
				{
					System.out.println(leadsFirstNames.get(a)+ " " +leadsLastNames.get(b));
					x.setFirstName(leadsFirstNames.get(a));
					x.setLastName(leadsLastNames.get(b));
					
					String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
					String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
					System.out.println("Owner Assigned to Lead "+x.getLeadFullName()+" >>>>>>>>" + ownerAssignedToLeadx);
					
					// Validate Owner Assignment
					if (! corpUsersForSalesTerritories2.contains(ownerAssignedToLeadx)) {
						System.out.println("Owner should be among ::: " + corpUsersForSalesTerritories2);
						fc.utobj().throwsException("Assign Lead Owner By Division - Priority(Sales Territories - GroupBy(ZIP/Postal Code)) - Import - FAILED");
					}
				}
				
				fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
				
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			
			}
	}
		
		@Test(priority = 4 , dependsOnMethods="ConfigurationForOwnerAssignmentByDivision" , groups = { "Division_OwnerAssignment" , "sales_OwnerAssignment" })
		@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_TC-04_WebForm", testCaseDescription = "Verify Lead Owner Assignment By Division with Priority as Sales Territories Group By ZIP - WebForm")
		private void OwnerAssignmentByDivision_TC_04_WebForm() throws Exception {
			String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
			}.getClass().getEnclosingMethod().getName());
			
			WebDriver driver = fc.commonMethods().browsers().openBrowser();
			
			try {
				
				driver = fc.loginpage().login(driver);
				
				// Through WebForm ----------------------------------------------------------------------------------------------------------------------------------------------------------------
				// Create WebForm 
				ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);	
				fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
				manageWebFormGenerator.setDivision(addDivision1.getDivisionName());
				common.set_ManageWebform(manageWebFormGenerator);
				manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory1.getLeadSourceCategory());
				manageWebFormGenerator.setLeadSourceDetails("None");
				manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);
				
				// Add Lead - WebForm
				Lead webformLead1 = new Lead();
				for (int j = 0; j <= 2; j++) {
						webformLead1.setFirstName("LeadDivWebform" + j);
						webformLead1.setLastName("PriorityTerritory(ZIP/PostalCode)" + fc.utobj().generateRandomNumber());
						webformLead1.setEmail("frantest2017@gmail.com");
						webformLead1.setDivision(addDivision1.getDivisionName());
						webformLead1.setCountry("USA");
						webformLead1.setStateProvince("Oregon");
						webformLead1.setCounty("Morrow");
						webformLead1.setZipPostalCode(salesTerritories2.getZipPostalCodeText());
						webformLead1.setDivision(addDivision1.getDivisionName());
						webformLead1.setLeadSourceCategory(leadSourceCategory1.getLeadSourceCategory());
						webformLead1.setLeadSourceDetails("None");

					manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, webformLead1); // ADD LEAD THROUGH WEBFORM
																									
					// fc.utobj().sleep();
					String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, webformLead1);
					//System.out.println(xml2);
					String ownerAssignedTowebformLead1 = common.getValFromXML(xmlWebForm1, "leadOwnerID");
					System.out.println("Owner Assigned to Lead "+webformLead1.getLeadFullName()+" >>>>>>>>" + ownerAssignedTowebformLead1);							
					// Validate
					if (! corpUsersForSalesTerritories2.contains(ownerAssignedTowebformLead1)) {
						System.out.println("Owner should be among ::: " + corpUsersForSalesTerritories2);
						fc.utobj().throwsException("Assign Lead Owner By Division - Priority(Sales Territories - GroupBy(ZIP/Postal Code)) - WebForm - FAILED");
					}
				}
								
				fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
		}
		
	@Test(priority = 5 , dependsOnMethods="ConfigurationForOwnerAssignmentByDivision" , groups = { "Division_OwnerAssignment" , "sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_TC-05_Api", testCaseDescription = "Verify Lead Owner Assignment By Division with Priority as Sales Territories Group By (States)Domestic - API")
	private void OwnerAssignmentByDivision_TC_05_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		try {
			
			driver = fc.loginpage().login(driver);
				
			// Validate Owner Assignment by Division - Priority(Sales Territories) - Sales Territory #3 - GroupBy(States) Domestic
			
			// Through API -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
			Lead lead5 = new Lead();
			for (int j = 0; j <= 2; j++) {
				lead5.setFirstName("LeadDivision_Api" + j);
				lead5.setLastName("PriorityTerritory(States)Domestic" + fc.utobj().generateRandomNumber());
				lead5.setEmail("frantest2017@gmail.com");
				lead5.setDivision(addDivision1.getDivisionName());
				lead5.setCountry("USA");
				lead5.setStateProvince("Ohio");
				lead5.setBasedonAssignmentRules("yes");
				lead5.setLeadSourceCategory(leadSourceCategory1.getLeadSourceCategory());
				lead5.setLeadSourceDetails("None");
							
				common.addLeadThroughWebServices(driver, lead5);
				String xml5 = common.getLeadDetailsThroughWebServices_MajorFields(driver, lead5);
				//System.out.println(xml5);
				String ownerAssignedToLead5 = common.getValFromXML(xml5, "leadOwnerID");
				System.out.println("Owner Assigned to Lead "+lead5.getLeadFullName()+" >>>>>>>>" + ownerAssignedToLead5);
				
				// Validate
				if (! corpUsersForSalesTerritories3.contains(ownerAssignedToLead5)) {
					System.out.println("Owner should be among ::: " + corpUsersForSalesTerritories3);
								fc.utobj().throwsException("Assign Lead Owner By Division - Priority(Sales Territories - GroupBy(States) Domestic) - API - FAILED");
							}
						}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			}
			}
	
	@Test(priority = 5 , dependsOnMethods="ConfigurationForOwnerAssignmentByDivision" , groups = { "Division_OwnerAssignment" , "sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_TC-05_Import", testCaseDescription = "Verify Lead Owner Assignment By Division with Priority as Sales Territories Group By (States)Domestic - Import")
	private void OwnerAssignmentByDivision_TC_05_Import() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		try {
			
			driver = fc.loginpage().login(driver);
			
				// Through Sales > IMPORT ----------------------------------------------------------------------------------------------------------------------------------------------------------------
					// Create CSV File with Lead Details
						String filePath = FranconnectUtil.config.get("testLogFolderPath")+"/ImportLead_TC_05.csv";
						System.out.println(filePath);
						List<Lead> leadList = new ArrayList<Lead>(); 

						for (int j = 0; j <= 2; j++) {
						Lead importLead1 = new Lead();
						importLead1.setFirstName("LeadDivision_Import" +j);
						importLead1.setLastName("PriorityTerritory(States)Domestic" + fc.utobj().generateRandomNumber());
						importLead1.setEmail("frantest2017@gmail.com");
						importLead1.setDivision(addDivision1.getDivisionName());
						importLead1.setCountry("USA");
						importLead1.setStateProvince("Ohio");
						importLead1.setBasedonAssignmentRules("yes");
						importLead1.setLeadSourceCategory(leadSourceCategory1.getLeadSourceCategory());
						importLead1.setLeadSourceDetails("None");
						
						leadList.add(importLead1);
						}
						
						common.writeCsvFile(filePath, leadList);
						
						// Import the CSV file 
						Import importlead = new Import();
						ImportTest importTest = new ImportTest(driver);
						
						fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver); // Navigate to Import Tab
						
						importlead.setImportType("Leads");
						importlead.setSpecifyFileFormat("CSV");
						importlead.setSalesDataFile(filePath);
						//importlead.setSalesDataFile(fc.utobj().getFilePathFromTestData("AsgmntByDivPrioritySalesTerritories_GroupByStatesIntl.csv"));
						
						importlead.setLeadStatus("New Lead");
						importlead.setLeadSourceCategory(leadSourceCategory1.getLeadSourceCategory());
						importlead.setLeadSourceDetails("None");
						
						importTest.importLeadsCSV(importlead);
						
						// Validation
						//String csvFile = fc.utobj().getFilePathFromTestData("AsgmntByDivPrioritySalesTerritories_GroupByStatesIntl.csv");
						Map<String,List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);
						
						Lead x = new Lead();
						List<String> leadsFirstNames = map.get("FN");
						List<String> leadsLastNames = map.get("LN");
						

						for(int a=1,b=1; a<leadsFirstNames.size() & b<leadsLastNames.size(); a++,b++)
						{
							System.out.println(leadsFirstNames.get(a)+ " " +leadsLastNames.get(b));
							x.setFirstName(leadsFirstNames.get(a));
							x.setLastName(leadsLastNames.get(b));
							
							String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
							String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
							System.out.println("Owner Assigned to Lead "+x.getLeadFullName()+" >>>>>>>>" + ownerAssignedToLeadx);
							
							// Validate Owner Assignment
							if (! corpUsersForSalesTerritories3.contains(ownerAssignedToLeadx)) {
								System.out.println("Owner should be among ::: " + corpUsersForSalesTerritories3);
								fc.utobj().throwsException("Assign Lead Owner By Division - Priority(Sales Territories - GroupBy(States) Domestic) - Import - FAILED");
							}
						}
						
						fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
						
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
		}

	@Test(priority = 5 , dependsOnMethods="ConfigurationForOwnerAssignmentByDivision" , groups = { "Division_OwnerAssignment" , "sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_TC-05_WebForm", testCaseDescription = "Verify Lead Owner Assignment By Division with Priority as Sales Territories Group By (States)Domestic - WebForm")
	private void OwnerAssignmentByDivision_TC_05_WebForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		try {
			
			driver = fc.loginpage().login(driver);
						
						// Through WebForm ---------------------------------------------------------------------------------------------------------------------------------------------------------
						// Create WebForm 
						ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
						fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
						manageWebFormGenerator.setDivision(addDivision1.getDivisionName());
						common.set_ManageWebform(manageWebFormGenerator);
						manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory1.getLeadSourceCategory());
						manageWebFormGenerator.setLeadSourceDetails("None");
						manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);
						
						// Add Lead - WebForm
						Lead webformLead1 = new Lead();
						for (int j = 0; j <= 2; j++) {
								webformLead1.setFirstName("LeadDivWebform" + j);
								webformLead1.setLastName("PriorityTerritory(States)Domestic" + fc.utobj().generateRandomNumber());
								webformLead1.setEmail("frantest2017@gmail.com");
								webformLead1.setDivision(addDivision1.getDivisionName());
								webformLead1.setCountry("USA");
								webformLead1.setStateProvince("Ohio");
								webformLead1.setDivision(addDivision1.getDivisionName());
								webformLead1.setLeadSourceCategory(leadSourceCategory1.getLeadSourceCategory());
								webformLead1.setLeadSourceDetails("None");

							manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, webformLead1); // ADD LEAD THROUGH WEBFORM
																											
							// fc.utobj().sleep();
							String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, webformLead1);
							//System.out.println(xml2);
							String ownerAssignedTowebformLead1 = common.getValFromXML(xmlWebForm1, "leadOwnerID");
							System.out.println("Owner Assigned to Lead "+webformLead1.getLeadFullName()+" >>>>>>>>" + ownerAssignedTowebformLead1);							
							// Validate
							if (! corpUsersForSalesTerritories3.contains(ownerAssignedTowebformLead1)) {
								System.out.println("Owner should be among ::: " + corpUsersForSalesTerritories3);
								fc.utobj().throwsException("Assign Lead Owner By Division - Priority(Sales Territories - GroupBy(States) Domestic) - WebForm - FAILED");
							}
						}
						
						fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			
		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
		}
		
	@Test(priority = 6 , dependsOnMethods="ConfigurationForOwnerAssignmentByDivision" , groups = { "Division_OwnerAssignment" , "sales_OwnerAssignment" , "test20180601"})
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_TC-06_Api", testCaseDescription = "Verify Lead Owner Assignment By Division with Priority as Sales Territories Group By (States)International - API")
	private void OwnerAssignmentByDivision_TC_06_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		try {
			
			driver = fc.loginpage().login(driver);	
			
			// Add Leads(3) and Validate Owner Assignment by Division - Priority(Sales Territories) - Sales Territory #4 - GroupBy(States) International
			// Through API ----------------------------------------------------------------------------------------------------------------------------------------------------------------------
			Lead lead6 = new Lead();
			for (int j = 0; j <= 2; j++) {
				lead6.setFirstName("LeadDivision" + j);
				lead6.setLastName("PriorityTerritory(States)International" + fc.utobj().generateRandomNumber());
				lead6.setEmail("frantest2017@gmail.com");
				lead6.setDivision(addDivision1.getDivisionName());
				lead6.setCountry("Germany");
				lead6.setStateProvince("Hessen");
				lead6.setBasedonAssignmentRules("yes");
				lead6.setLeadSourceCategory(leadSourceCategory1.getLeadSourceCategory());
				lead6.setLeadSourceDetails("None");
										
				common.addLeadThroughWebServices(driver, lead6);
				String xml6 = common.getLeadDetailsThroughWebServices_MajorFields(driver, lead6);
				//System.out.println(xml1);
				String ownerAssignedToLead6 = common.getValFromXML(xml6, "leadOwnerID");
				System.out.println("Owner Assigned to Lead "+lead6.getLeadFullName()+" >>>>>>>>" + ownerAssignedToLead6);
							
			// Validate
				if (! corpUsersForSalesTerritories4.contains(ownerAssignedToLead6)) {
					System.out.println("Owner should be among ::: " + corpUsersForSalesTerritories4);
							fc.utobj().throwsException("Assign Lead Owner By Division - Priority(Sales Territories - GroupBy(States) International) - API - FAILED");
						}
				}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			
			} catch (Exception e) {
				 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			}
			}
	
	@Test(priority = 6 , dependsOnMethods="ConfigurationForOwnerAssignmentByDivision" , groups = { "Division_OwnerAssignment" , "sales_OwnerAssignment" , "test20180601"})
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_TC-06_Import", testCaseDescription = "Verify Lead Owner Assignment By Division with Priority as Sales Territories Group By (States)International - Import")
	private void OwnerAssignmentByDivision_TC_06_Import() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		try {
			
			driver = fc.loginpage().login(driver);	
			
			// Through Sales > IMPORT ----------------------------------------------------------------------------------------------------------------------------------------------------------------
			// Create CSV File with Lead Details
			String filePath = FranconnectUtil.config.get("testLogFolderPath")+"/ImportLead_TC_06.csv";
			System.out.println(filePath);
			List<Lead> leadList = new ArrayList<Lead>(); 

			for (int j = 0; j <= 2; j++) {
			Lead importLead1 = new Lead();
			importLead1.setFirstName("LeadDivision" +j);
			importLead1.setLastName("PriorityTerritory(States)International" + fc.utobj().generateRandomNumber());
			importLead1.setEmail("frantest2017@gmail.com");
			importLead1.setDivision(addDivision1.getDivisionName());
			importLead1.setCountry("Germany");
			importLead1.setStateProvince("Hessen");
			importLead1.setBasedonAssignmentRules("yes");
			importLead1.setLeadSourceCategory(leadSourceCategory1.getLeadSourceCategory());
			importLead1.setLeadSourceDetails("None");
			
			leadList.add(importLead1);
			}
			
			common.writeCsvFile(filePath, leadList);
			
			// Import the CSV file 
			Import importlead = new Import();
			ImportTest importTest = new ImportTest(driver);
			
			fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver); // Navigate to Import Tab
			
			importlead.setImportType("Leads");
			importlead.setSpecifyFileFormat("CSV");
			importlead.setSalesDataFile(filePath);
			//importlead.setSalesDataFile(fc.utobj().getFilePathFromTestData("AsgmntByDivPrioritySalesTerritories_GroupByStatesIntl.csv"));
			
			importlead.setLeadStatus("New Lead");
			importlead.setLeadSourceCategory(leadSourceCategory1.getLeadSourceCategory());
			importlead.setLeadSourceDetails("None");
			
			importTest.importLeadsCSV(importlead);
			
			// Validation
			//String csvFile = fc.utobj().getFilePathFromTestData("AsgmntByDivPrioritySalesTerritories_GroupByStatesIntl.csv");
			Map<String,List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);
			
			Lead x = new Lead();
			List<String> leadsFirstNames = map.get("FN");
			List<String> leadsLastNames = map.get("LN");
			

			for(int a=1,b=1; a<leadsFirstNames.size() & b<leadsLastNames.size(); a++,b++)
			{
				System.out.println(leadsFirstNames.get(a)+ " " +leadsLastNames.get(b));
				x.setFirstName(leadsFirstNames.get(a));
				x.setLastName(leadsLastNames.get(b));
				
				String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
				String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
				System.out.println("Owner Assigned to Lead "+x.getLeadFullName()+" >>>>>>>>" + ownerAssignedToLeadx);
				
				// Validate Owner Assignment
				if (! corpUsersForSalesTerritories4.contains(ownerAssignedToLeadx)) {
					System.out.println("Owner should be among ::: " + corpUsersForSalesTerritories4);
					fc.utobj().throwsException("Assign Lead Owner By Division - Priority(Source) - Import - FAILED");
				}
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			
		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
		}
	
	@Test(priority = 6 , dependsOnMethods="ConfigurationForOwnerAssignmentByDivision" , groups = { "Division_OwnerAssignment" , "sales_OwnerAssignment" , "test20180601"})
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_TC-06_WebForm", testCaseDescription = "Verify Lead Owner Assignment By Division with Priority as Sales Territories Group By (States)International - WebForm")
	private void OwnerAssignmentByDivision_TC_06_WebForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		try {
			
			driver = fc.loginpage().login(driver);	
			
			// Through WebForm --------------------------------------------------------------------------------------------------------------------------------------------------------------------------
						// Create WebForm 
						ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
						fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
						manageWebFormGenerator.setDivision(addDivision1.getDivisionName());
						common.set_ManageWebform(manageWebFormGenerator);
						//manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory1.getLeadSourceCategory());
						//manageWebFormGenerator.setLeadSourceDetails("None");
						manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);
						
						// Add Lead - WebForm
						Lead webformLead1 = new Lead();
						for (int j = 0, k = 0; j <= 2; j++, k++) {
								webformLead1.setFirstName("LeadDivWebform" + j);
								webformLead1.setLastName("PriorityTerritory(States)International" + fc.utobj().generateRandomNumber());
								webformLead1.setEmail("frantest2017@gmail.com");
								webformLead1.setCountry("Germany");
								webformLead1.setStateProvince("Hessen");
								webformLead1.setDivision(addDivision1.getDivisionName());
								webformLead1.setLeadSourceCategory("Friends");
								webformLead1.setLeadSourceDetails("Friends");

							manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, webformLead1); // ADD LEAD THROUGH WEBFORM
																											
							// fc.utobj().sleep();
							String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, webformLead1);
							//System.out.println(xml2);
							String ownerAssignedTowebformLead1 = common.getValFromXML(xmlWebForm1, "leadOwnerID");
							System.out.println("Owner Assigned to Lead "+webformLead1.getLeadFullName()+" >>>>>>>>" + ownerAssignedTowebformLead1);							
							// Validate
							if (! corpUsersForSalesTerritories4.contains(ownerAssignedTowebformLead1)) {
								System.out.println("Owner should be among ::: " + corpUsersForSalesTerritories4);
								fc.utobj().throwsException("Assign Lead Owner By Division - Priority(Source)- WebForm - FAILED");
							}
						}
						
						fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
		}
	
	
	
	
	@Test(priority = 7 , dependsOnMethods="ConfigurationForOwnerAssignmentByDivision" , groups = { "Division_OwnerAssignment" , "sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_TC-07_Api", testCaseDescription = "Owner Assignment By Division - Priority(Source) - API")
	private void OwnerAssignmentByDivision_TC_07_Api() throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		try {
			
			driver = fc.loginpage().login(driver);
			
			
		  //  Setup priority for Owner Assignment : Source
					AssignLeadOwnerbyDivisionTest assignLeadOwnerbyDivisionTest = new AssignLeadOwnerbyDivisionTest(driver);
					fc.commonMethods().getModules().openAdminPage(driver).openAssignLeadOwnersPage(driver);
					AssignLeadOwnersUI assignLeadOwnersUI = new AssignLeadOwnersUI(driver);
					fc.utobj().clickElement(driver, assignLeadOwnersUI.continue_btn);	
					assignLeadOwnerbyDivisionTest.click_setPriority();
					assignLeadOwnerbyDivisionTest.setPriority_Source();
					
					
					// Owner Assignment - Check Owner assignment by Division - Priority(Source) 
					
					// Through API -------------------------------------------------------------------------------------------------------------------------------------------------------------------
					Lead lead1 = new Lead();
					for (int j = 0; j <= 2; j++) {
						lead1.setFirstName("LeadDivApi" + j);
						lead1.setLastName("Api_PrioritySource" + fc.utobj().generateRandomNumber());
						lead1.setEmail("frantest2017@gmail.com");
						lead1.setDivision(addDivision1.getDivisionName());
						lead1.setCountry("USA");
						lead1.setStateProvince("Oregon");
						lead1.setCounty("Morrow");
						lead1.setBasedonAssignmentRules("yes");
						lead1.setLeadSourceCategory(leadSourceCategory1.getLeadSourceCategory());
						lead1.setLeadSourceDetails("None");
									
						common.addLeadThroughWebServices(driver, lead1);
						String xml1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, lead1);
						//System.out.println(xml1);
						String ownerAssignedToLead1 = common.getValFromXML(xml1, "leadOwnerID");
						System.out.println("Owner Assigned to Lead "+lead1.getLeadFullName()+" >>>>>>>>" + ownerAssignedToLead1);
						
						// Validate
						if (! corpUsersForSource.contains(ownerAssignedToLead1)) {
							System.out.println("Owner should be among ::: " + corpUsersForSource);
										fc.utobj().throwsException("Assign Lead Owner By Division - Priority(Source) - API - FAILED");
									}
								}
					
					fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
					
				} catch (Exception e) {
					fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
				}
				}
	
	@Test(priority = 7 , dependsOnMethods="ConfigurationForOwnerAssignmentByDivision" , groups = { "Division_OwnerAssignment" , "sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_TC-07_Import", testCaseDescription = "Owner Assignment By Division - Priority(Source) - Import")
	private void OwnerAssignmentByDivision_TC_07_Import() throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		try {
			
			driver = fc.loginpage().login(driver);
					
					
					// Through Sales > IMPORT --------------------------------------------------------------------------------------------------------------------------------------------------------
					// Create CSV File with Lead Details
					String filePath = FranconnectUtil.config.get("testLogFolderPath")+"/ImportLead_TC_07.csv";
					System.out.println(filePath);
					List<Lead> leadList = new ArrayList<Lead>(); 

					for (int j = 0; j <= 5; j++) {
					Lead importLead1 = new Lead();
					importLead1.setFirstName("DivisionLead" +j);
					importLead1.setLastName("Import_PrioritySource" + fc.utobj().generateRandomNumber());
					importLead1.setEmail("frantest2017@gmail.com");
					importLead1.setDivision(addDivision1.getDivisionName());
					importLead1.setCountry("USA");
					importLead1.setStateProvince("Oregon");
					importLead1.setCounty("Morrow");
					importLead1.setBasedonAssignmentRules("yes");
					importLead1.setLeadSourceCategory(leadSourceCategory1.getLeadSourceCategory());
					importLead1.setLeadSourceDetails("None");
					
					leadList.add(importLead1);
					}
					
					common.writeCsvFile(filePath, leadList);
					
					// Import the CSV file 
					Import importlead = new Import();
					ImportTest importTest = new ImportTest(driver);
					
					fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver); // Navigate to Import Tab
					
					importlead.setImportType("Leads");
					importlead.setSpecifyFileFormat("CSV");
					importlead.setSalesDataFile(filePath);
					//importlead.setSalesDataFile(fc.utobj().getFilePathFromTestData("AsgmntByDivPrioritySalesTerritories_GroupByStatesIntl.csv"));
					
					importlead.setLeadStatus("New Lead");
					importlead.setLeadSourceCategory(leadSourceCategory1.getLeadSourceCategory());
					importlead.setLeadSourceDetails("None");
					
					importTest.importLeadsCSV(importlead);
					
					// Validation
					//String csvFile = fc.utobj().getFilePathFromTestData("AsgmntByDivPrioritySalesTerritories_GroupByStatesIntl.csv");
					Map<String,List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);
					
					Lead x = new Lead();
					List<String> leadsFirstNames = map.get("FN");
					List<String> leadsLastNames = map.get("LN");
					

					for(int a=1,b=1; a<leadsFirstNames.size() & b<leadsLastNames.size(); a++,b++)
					{
						System.out.println(leadsFirstNames.get(a)+ " " +leadsLastNames.get(b));
						x.setFirstName(leadsFirstNames.get(a));
						x.setLastName(leadsLastNames.get(b));
						
						String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
						String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
						System.out.println("Owner Assigned to Lead "+x.getLeadFullName()+" >>>>>>>>" + ownerAssignedToLeadx);
						
						// Validate Owner Assignment
						if (! corpUsersForSource.contains(ownerAssignedToLeadx)) {
							System.out.println("Owner should be among ::: " + corpUsersForSource);
							fc.utobj().throwsException("Assign Lead Owner By Division - Priority(Source) - Import - FAILED");
						}
					}
					
		
					fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
					
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
		}
	
	@Test(priority = 7 , dependsOnMethods="ConfigurationForOwnerAssignmentByDivision" , groups = { "Division_OwnerAssignment" , "sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_TC-07_WebForm", testCaseDescription = "Owner Assignment By Division - Priority(Source) - WebForm")
	private void OwnerAssignmentByDivision_TC_07_WebForm() throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		try {
			
			driver = fc.loginpage().login(driver);
					
					// Through WebForm --------------------------------------------------------------------------------------------------------------------------------------------------------------
					// Create WebForm 
						ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
						fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
						manageWebFormGenerator.setDivision(addDivision1.getDivisionName());
						common.set_ManageWebform(manageWebFormGenerator);
						//manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory1.getLeadSourceCategory());
						//manageWebFormGenerator.setLeadSourceDetails("None");
						manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);
						
						
					//Add Leads through WebForm and Validate Owner Assignment - Check Owner assignment by Division - Priority(Source) 
						Lead webformLead1 = new Lead();
							for (int j = 0; j <= 2; j++) {
								webformLead1.setFirstName("LeadDivWebform" + j);
								webformLead1.setLastName("PrioritySource" + fc.utobj().generateRandomNumber());
								webformLead1.setEmail("frantest2017@gmail.com");
								webformLead1.setDivision(addDivision1.getDivisionName());
								webformLead1.setCountry("USA");
								webformLead1.setStateProvince("Oregon");
								webformLead1.setCounty("Morrow");
								webformLead1.setBasedonAssignmentRules("yes");
								webformLead1.setLeadSourceCategory(leadSourceCategory1.getLeadSourceCategory());
								webformLead1.setLeadSourceDetails("None");

							manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, webformLead1); // ADD LEAD THROUGH WEBFORM
																											
							// fc.utobj().sleep();
							String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, webformLead1);
							//System.out.println(xml2);
							String ownerAssignedTowebformLead1 = common.getValFromXML(xmlWebForm1, "leadOwnerID");
							System.out.println("Owner Assigned to Lead "+webformLead1.getLeadFullName()+" >>>>>>>>" + ownerAssignedTowebformLead1);							
							// Validate
							if (! corpUsersForSource.contains(ownerAssignedTowebformLead1)) {
								System.out.println("Owner should be among ::: " + corpUsersForSource);
								fc.utobj().throwsException("Assign Lead Owner By Division - Priority(Source) - WebForm - FAILED");
							}
						}
						
							fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

			} catch (Exception e) {
				 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
	}
	}
	
	@Test(priority = 8 , dependsOnMethods="ConfigurationForOwnerAssignmentByDivision" , groups = { "Division_OwnerAssignment" , "DefaultOwnerAssignment_Test" })
	@TestCase(createdOn = "2018-05-28", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_TC-08_Api", testCaseDescription = "Owner Assignment by Division - Check default owner assignment - API")
	private void OwnerAssignmentByDivison_TC_08_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
	
		try {
			
			driver = fc.loginpage().login(driver);
			
			// Owner Assignment By Division - Default Owner 
			
			// Through API -------------------------------------------------------------------------------------------------------------------------------------------------------------------
			Lead leadApi = new Lead();
			leadApi.setFirstName("DefaultOwner");
			leadApi.setLastName("Api_Lead" + fc.utobj().generateRandomNumber());
			leadApi.setEmail("frantest2017@gmail.com");
			leadApi.setBasedonAssignmentRules("yes");
			leadApi.setLeadSourceCategory("Friends");
			leadApi.setLeadSourceDetails("Friends");
			common.addLeadThroughWebServices(driver, leadApi);
			// Fetch Owner assigned to Lead through Rest API 
			String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadApi);
			//System.out.println(xml14);
			String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
			System.out.println("Owner Assigned to Lead >>>>>>>>" +leadApi.getLeadFullName()+ ">>>" + ownerAssignedToLeadApi);
			// Validation
			if(! ownerAssignedToLeadApi.equalsIgnoreCase("FranConnect Administrator")) {
				System.out.println("Owner should be ::: FranConnect Administrator");
				fc.utobj().throwsException("Owner Assignment - to Default Owner(FranConnect Administrator) - API - FAILED");
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
}
	}
		
		@Test(priority = 8 , dependsOnMethods="ConfigurationForOwnerAssignmentByDivision" , groups = { "Division_OwnerAssignment" , "DefaultOwnerAssignment_Test" })
		@TestCase(createdOn = "2018-05-28", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_TC-08_Import", testCaseDescription = "Owner Assignment by Division - Check default owner assignment - Import")
		private void OwnerAssignmentByDivison_TC_08_Import() throws Exception {
			String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
			}.getClass().getEnclosingMethod().getName());

			WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
			try {
				
				driver = fc.loginpage().login(driver);
				
			// Through Sales > IMPORT --------------------------------------------------------------------------------------------------------------------------------------------------------
			// Create CSV File with Lead Details
			String filePath = FranconnectUtil.config.get("testLogFolderPath")+"/ImportLead_TC_08.csv";
			System.out.println(filePath);
			List<Lead> leadList = new ArrayList<Lead>(); 

			for (int j=0; j<1; j++) {
			Lead importLead1 = new Lead();
			importLead1.setFirstName("DefaultOwner" +j);
			importLead1.setLastName("Import_Lead" + fc.utobj().generateRandomNumber());
			importLead1.setEmail("frantest2017@gmail.com");
			importLead1.setBasedonAssignmentRules("yes");
			importLead1.setLeadSourceCategory("Friends");
			importLead1.setLeadSourceDetails("Friends");
			
			leadList.add(importLead1);
			}
			
			common.writeCsvFile(filePath, leadList);
			
			// Import the CSV file 
			Import importlead = new Import();
			ImportTest importTest = new ImportTest(driver);
			
			fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver); // Navigate to Import Tab
			
			importlead.setImportType("Leads");
			importlead.setSpecifyFileFormat("CSV");
			importlead.setSalesDataFile(filePath);
			//importlead.setSalesDataFile(fc.utobj().getFilePathFromTestData("AsgmntByDivPrioritySalesTerritories_GroupByStatesIntl.csv"));
			
			importlead.setLeadStatus("New Lead");
			importlead.setLeadSourceCategory("Friends");
			importlead.setLeadSourceDetails("Friends");
			
			importTest.importLeadsCSV(importlead);
			
			// Validation
			//String csvFile = fc.utobj().getFilePathFromTestData("AsgmntByDivPrioritySalesTerritories_GroupByStatesIntl.csv");
			Map<String,List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);
			
			Lead x = new Lead();
			List<String> leadsFirstNames = map.get("FN");
			List<String> leadsLastNames = map.get("LN");
			

			for(int a=1,b=1; a<leadsFirstNames.size() & b<leadsLastNames.size(); a++,b++)
			{
				System.out.println(leadsFirstNames.get(a)+ " " +leadsLastNames.get(b));
				x.setFirstName(leadsFirstNames.get(a));
				x.setLastName(leadsLastNames.get(b));
				
				String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
				String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
				System.out.println("Owner Assigned to Lead "+x.getLeadFullName()+" >>>>>>>> " + ownerAssignedToLeadx);
				
				// Validate Owner Assignment	
				if(! ownerAssignedToLeadx.equalsIgnoreCase("FranConnect Administrator")) {
					System.out.println("Owner should be ::: FranConnect Administrator");
					fc.utobj().throwsException("Owner Assignment - to Default Owner(FranConnect Administrator) - Import - FAILED");
				}
			}
			

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
}
	}
		
		@Test(priority = 8 , dependsOnMethods="ConfigurationForOwnerAssignmentByDivision" , groups = { "Division_OwnerAssignment" , "DefaultOwnerAssignment_Test" })
		@TestCase(createdOn = "2018-05-28", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_TC-08_WebForm", testCaseDescription = "Owner Assignment by Division - Check default owner assignment - WebForm")
		private void OwnerAssignmentByDivison_TC_08_WebForm() throws Exception {
			String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
			}.getClass().getEnclosingMethod().getName());

			WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
			try {
				
				driver = fc.loginpage().login(driver);
			
			// Through WebForm --------------------------------------------------------------------------------------------------------------------------------------------------------------
			// Create WebForm 
				ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
				fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
				// manageWebFormGenerator.setDivision(addDivision1.getDivisionName());
				common.set_ManageWebform(manageWebFormGenerator);
				//manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory1.getLeadSourceCategory());
				//manageWebFormGenerator.setLeadSourceDetails("None");
				manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);
				
				
			// Add Leads through WebForm
				Lead webformLead1 = new Lead();
					for (int j = 0; j <= 1; j++) {
						webformLead1.setFirstName("DefaultOwner" + j);
						webformLead1.setLastName("WebForm_Lead" + fc.utobj().generateRandomNumber());
						webformLead1.setEmail("frantest2017@gmail.com");
						webformLead1.setBasedonAssignmentRules("yes");
						webformLead1.setLeadSourceCategory("Friends");
						webformLead1.setLeadSourceDetails("Friends");

					manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, webformLead1); // ADD LEAD THROUGH WEBFORM
																									
					// fc.utobj().sleep();
					String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, webformLead1);
					//System.out.println(xml2);
					String ownerAssignedTowebformLead1 = common.getValFromXML(xmlWebForm1, "leadOwnerID");
					System.out.println("Owner Assigned to Lead "+webformLead1.getLeadFullName()+" >>>>>>>>" + ownerAssignedTowebformLead1);							
					// Validate
					if(! ownerAssignedTowebformLead1.equalsIgnoreCase("FranConnect Administrator")) {
						System.out.println("Owner should be ::: FranConnect Administrator");
						fc.utobj().throwsException("Owner Assignment - to Default Owner(FranConnect Administrator) - WebForm - FAILED");
					}
				}
					
					fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			
		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
		}
	
	@Test(priority = 9 ,  dependsOnMethods="ConfigurationForOwnerAssignmentByDivision" ,  groups = { "Division_OwnerAssignment" , "sales_OwnerAssignment" , "test41654"})
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_TC-09", testCaseDescription = "If a user is deleted/deactivated then user should not appear in assignment")
	private void OwnerAssignmentByDivison_TC_09() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		CorporateUser userToBeDeleted = new CorporateUser();
		AssignLeadOwnersTest assignLeadOwnersTest = new AssignLeadOwnersTest(driver);
		AdminCorporateUserTest adminCorporateUserTest = new AdminCorporateUserTest(driver);
	
		try {
			
			driver = fc.loginpage().login(driver);
			
			// Add Division
			// common.set_AddDivision(addDivision1); Random Dynamic Division Name
			AddDivision addDivisionTC09 = new AddDivision();
			ManageDivisionTest manageDivisionTest = new ManageDivisionTest(driver);
			addDivisionTC09.setDivisionName("DivOwnerAssignmentTC09" + fc.utobj().generateRandomNumber());
			fc.commonMethods().getModules().openAdminPage(driver).openAddDivision(driver);
			manageDivisionTest.addDivision(addDivisionTC09);
			
					// ADD Corporate Users - through API 
					fc.commonMethods().fillDefaultDataFor_CorporateUser(userToBeDeleted);
					userToBeDeleted.setFirstName("CorpUserToBeDeleted");
					userToBeDeleted.setLastName("AssignmentByDivision" + fc.utobj().generateRandomNumber());
					userToBeDeleted.setUserName("user" + fc.utobj().generateRandomChar());
					fc.commonMethods().addCorporateUser(driver, userToBeDeleted);
					String userToBeDeleted_fullname = userToBeDeleted.getFirstName() + " " + userToBeDeleted.getLastName();
					System.out.println("User to be Deleted ->> " +userToBeDeleted_fullname);
				
				// Verify that user is available in 'Assign Lead Owner by Division' - Default Owner DropDown list  
				fc.commonMethods().getModules().openAdminPage(driver).openAssignLeadOwnersPage(driver);
				assignLeadOwnersTest.set_assignLeadOwnerbyDivision();
				
				fc.utobj().printTestStep("Verify that user is present in Assign Lead Owner by Division - Default Owner list");
				if(! fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//label[contains(text(),'"+userToBeDeleted_fullname+"')]/input"))
						{
					fc.utobj().throwsException("Created User not visible in Default Owner dropdown list");
						}
				
				// Delete User 
				fc.commonMethods().getModules().openAdminPage(driver).openCorporateUserPage(driver);
				adminCorporateUserTest.delete_CorpUser(userToBeDeleted_fullname);
				
				// Verify that deleted user is not available in 'Assign Lead Owner by Division' - Default Owner DropDown list  
				fc.commonMethods().getModules().openAdminPage(driver).openAssignLeadOwnersPage(driver);
				assignLeadOwnersTest.set_assignLeadOwnerbyDivision();
				
				fc.utobj().printTestStep("Verify that deleted user is not present in Assign Lead Owner by Division - Default Owner list");
				if(fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//label[contains(text(),'"+userToBeDeleted_fullname+"')]/input"))
						{
					fc.utobj().throwsException("Deleted User still visible in Default Owner dropdown list");
						}
				
				fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
				
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
		}
	
	// *******************************************************************************************************************************************************************************************************
	// #######################################################################################################################################################################################################
	
	// @BeforeClass(groups = {"sales_OwnerAssignment"})
	@Test(priority = 15 , dependsOnMethods="ConfigurationForOwnerAssignmentByDivision" , groups = { "LeadSource_OwnerAssignment" , "sales_OwnerAssignment" , "LeadSourceAssignment_Configuration" , "sales_OwnerAssignment_failed" , "Sales_OwnerAssignment_WebForm" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_10" , testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source")
	private void ConfigurationForOwnerAssignmentByLeadSource() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		AssignLeadOwnersTest assignLeadOwnersTest = new AssignLeadOwnersTest(driver);
		ManageDivisionTest manageDivisionTest = new ManageDivisionTest(driver);
		ConfigureNewHierarchyLevel configureNewHierarchyLevel = new ConfigureNewHierarchyLevel();
		ConfigureNewHierarchyLevelTest configureNewHierarchyLevelTest = new ConfigureNewHierarchyLevelTest(driver);
		LeadSourceCategoryTest leadSourceCategoryTest = new LeadSourceCategoryTest(driver);
		SalesTerritoriesTest salesTerritoriesTest = new SalesTerritoriesTest(driver);
		
		
	
		try {
				
			 driver = fc.loginpage().login(driver);
				
				// Assignment Territory Preference --> Top:Zip Medium:County Low:State
				
				
				// ADD Corporate Users - through API 
				for (int i = 0; i <= 2; i++) {											// (Adding 3 Corporate Users)
					fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser2);
					corpUser2.setFirstName("CorpUser_LeadSource" + i);
					corpUser2.setLastName("ForLeadSource" + fc.utobj().generateRandomNumber());
					corpUser2.setUserName("user" + fc.utobj().generateRandomChar());
					fc.commonMethods().addCorporateUser(driver, corpUser2);
					System.out.println(corpUser2.getFirstName() + " " + corpUser2.getLastName());
					corpUsersForSource2.add(corpUser2.getFirstName() + " " + corpUser2.getLastName());
				}
				
				
				// ADD Corporate Users - through API (Associate different Owner for Sales Territories) - GroupBy(County)
				for (int i = 0; i <= 1; i++) {											// (Adding 2 Corporate Users)
					fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser2);
					corpUser2.setFirstName("CorpUser_LeadSource" + i);
					corpUser2.setLastName("ForST_County" + fc.utobj().generateRandomNumber());
					corpUser2.setUserName("user" + fc.utobj().generateRandomChar());
					fc.commonMethods().addCorporateUser(driver, corpUser2);
					System.out.println(corpUser2.getFirstName() + " " + corpUser2.getLastName());
					corpUsersForSalesTerritoriesCounty.add(corpUser2.getFirstName() + " " + corpUser2.getLastName());
				}
				// ADD Corporate Users - through API (Associate different Owner for Sales Territories) - GroupBy(ZIP/Postal Code)
				for (int i = 0; i <= 1; i++) {											// (Adding 2 Corporate Users)
					fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser2);
					corpUser2.setFirstName("CorpUser_LeadSource" + i);
					corpUser2.setLastName("ForST_Zip" + fc.utobj().generateRandomNumber());
					corpUser2.setUserName("user" + fc.utobj().generateRandomChar());
					fc.commonMethods().addCorporateUser(driver, corpUser2);
					System.out.println(corpUser2.getFirstName() + " " + corpUser2.getLastName());
					corpUsersForSalesTerritoriesZip.add(corpUser2.getFirstName() + " " + corpUser2.getLastName());
				}
				// ADD Corporate Users - through API (Associate different Owner for Sales Territories) - GroupBy(States) Domestic
				for (int i = 0; i <= 1; i++) {											// (Adding 2 Corporate Users)
					fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser2);
					corpUser2.setFirstName("CorpUser_LeadSource" + i);
					corpUser2.setLastName("ForST_StatesDom" + fc.utobj().generateRandomNumber());
					corpUser2.setUserName("user" + fc.utobj().generateRandomChar());
					fc.commonMethods().addCorporateUser(driver, corpUser2);
					System.out.println(corpUser2.getFirstName() + " " + corpUser2.getLastName());
					corpUsersForSalesTerritoriesStatesDomestic.add(corpUser2.getFirstName() + " " + corpUser2.getLastName());
				}
				// ADD Corporate Users - through API (Associate different Owner for Sales Territories) - GroupBy(States) International
				for (int i = 0; i <= 1; i++) {											// (Adding 2 Corporate Users)
					fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser2);
					corpUser2.setFirstName("CorpUser_LeadSource" + i);
					corpUser2.setLastName("ForST_StatesIntl" + fc.utobj().generateRandomNumber());
					corpUser2.setUserName("user" + fc.utobj().generateRandomChar());
					fc.commonMethods().addCorporateUser(driver, corpUser2);
					System.out.println(corpUser2.getFirstName() + " " + corpUser2.getLastName());
					corpUsersForSalesTerritoriesStatesInternational.add(corpUser2.getFirstName() + " " + corpUser2.getLastName());
				}
				
				
				// ADD Corporate Users - through API (Associate different Owner for Division)
				for (int i = 0; i <= 1; i++) {											// (Adding 2 Corporate Users)
					fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser2);
					corpUser2.setFirstName("CorpUser_LeadSource" + i);
					corpUser2.setLastName("ForDivision" + fc.utobj().generateRandomNumber());
					corpUser2.setUserName("user" + fc.utobj().generateRandomChar());
					fc.commonMethods().addCorporateUser(driver, corpUser2);
					System.out.println(corpUser2.getFirstName() + " " + corpUser2.getLastName());
					corpUsersForDivision.add(corpUser2.getFirstName() + " " + corpUser2.getLastName());
				}
			
				
				// Turn ON Division
				configureNewHierarchyLevel.setEnableNewHierarchyLevel("Yes");
				fc.commonMethods().getModules().openAdminPage(driver).openConfigureNewHierarchyLevel(driver);
				configureNewHierarchyLevelTest.EnableNewHierarchyLevel(configureNewHierarchyLevel);
				
				// Add Division #1
				common.set_AddDivision(addDivision2);
				fc.commonMethods().getModules().openAdminPage(driver).openAddDivision(driver);
				manageDivisionTest.addDivision(addDivision2);
				
				// Add Division #2
				common.set_AddDivision(addDivision3);
				fc.commonMethods().getModules().openAdminPage(driver).openAddDivision(driver);
				manageDivisionTest.addDivision(addDivision3);
				
				// Adding Lead Source Category #1
				fc.commonMethods().getModules().openAdminPage(driver).openFranchiseSourcePage(driver);
				leadSourceCategory2.setLeadSourceCategory("Lead Source " + fc.utobj().generateRandomNumber());
				leadSourceCategory2.setIncludeInWebPage("yes");
				leadSourceCategoryTest.addLeadSourceCategory(leadSourceCategory2).verify_addLeadSourceCategory(leadSourceCategory2);
				
				
				/*	
				// Adding Lead Source Category #2
				LeadSourceCategory leadSourceCategory2 = new LeadSourceCategory();
				leadSourceCategory2.setLeadSourceCategory("Lead Source " + fc.utobj().generateRandomNumber());
				leadSourceCategory2.setIncludeInWebPage("no");
				leadSourceCategoryTest.addLeadSourceCategory(leadSourceCategory2).verify_addLeadSourceCategory(leadSourceCategory2);
				
				// Adding Lead Source Category #3
				LeadSourceCategory leadSourceCategory3 = new LeadSourceCategory();
				leadSourceCategory3.setLeadSourceCategory("Lead Source " + fc.utobj().generateRandomNumber());
				leadSourceCategory3.setIncludeInWebPage("no");
				leadSourceCategoryTest.addLeadSourceCategory(leadSourceCategory3).verify_addLeadSourceCategory(leadSourceCategory3);
				*/
				
				// ADD Sales Territory #1 - GroupBy(County)
				ArrayList<String> SalesTerritories = new ArrayList<String>();
				common.setGroupByCounty_Domestic_filladdNewSalesTerritory(salesTerritoriesByCounty); // County Domestic
				fc.commonMethods().getModules().openAdminPage(driver).openSalesTerritoryPage(driver);
				salesTerritoriesTest.addNewSalesTerritory(salesTerritoriesByCounty);
				SalesTerritories.add(salesTerritoriesByCounty.getSalesTerritoryName()); 
				
				// ADD Sales Territory #2 - GroupBy(ZIP/Postal Code)
				common.setGroupByZipPostalCode_Domestic_filladdNewSalesTerritory(salesTerritoriesByZip);
				fc.commonMethods().getModules().openAdminPage(driver).openSalesTerritoryPage(driver);
				salesTerritoriesTest.addNewSalesTerritory(salesTerritoriesByZip);
				SalesTerritories.add(salesTerritoriesByZip.getSalesTerritoryName()); 
				
				// ADD Sales Territory #3 - GroupBy(States) Domestic
				common.setGroupByStates_Domestic_filladdNewSalesTerritory(salesTerritoriesByStatesDomestic);
				fc.commonMethods().getModules().openAdminPage(driver).openSalesTerritoryPage(driver);
				salesTerritoriesTest.addNewSalesTerritory(salesTerritoriesByStatesDomestic);
				SalesTerritories.add(salesTerritoriesByStatesDomestic.getSalesTerritoryName()); 

				// ADD Sales Territory #4 - GroupBy(States) International
				common.setGroupByStates_International_filladdNewSalesTerritory(salesTerritoriesByStatesInternational);
				fc.commonMethods().getModules().openAdminPage(driver).openSalesTerritoryPage(driver);
				salesTerritoriesTest.addNewSalesTerritory(salesTerritoriesByStatesInternational);
				SalesTerritories.add(salesTerritoriesByStatesInternational.getSalesTerritoryName()); 
				
				
				// Assign Lead Owner by Lead Source
				fc.commonMethods().getModules().openAdminPage(driver).openAssignLeadOwnersPage(driver);
				assignLeadOwnersTest.set_defaultOwner("FranConnect Administrator");
				// Assign Owners for Source + Assign Different Owner for Sales Territories + Assign Different Owner for Division + Set Priority(Sales Territories)
				assignLeadOwnersTest.set_assignLeadOwnerbyLeadSource().Configure_AssignLeadOwnerbyLeadSource(leadSourceCategory2.getLeadSourceCategory(), corpUsersForSource2).AssociateDifferentOwnerFor_SalesTerritories(corpUsersForSalesTerritoriesCounty, leadSourceCategory2.getLeadSourceCategory(), salesTerritoriesByCounty.getSalesTerritoryName()).AssociateDifferentOwnerFor_SalesTerritories(corpUsersForSalesTerritoriesZip, leadSourceCategory2.getLeadSourceCategory(), salesTerritoriesByZip.getSalesTerritoryName()).AssociateDifferentOwnerFor_SalesTerritories(corpUsersForSalesTerritoriesStatesDomestic, leadSourceCategory2.getLeadSourceCategory(), salesTerritoriesByStatesDomestic.getSalesTerritoryName()).AssociateDifferentOwnerFor_SalesTerritories(corpUsersForSalesTerritoriesStatesInternational, leadSourceCategory2.getLeadSourceCategory(), salesTerritoriesByStatesInternational.getSalesTerritoryName()).AssociateDifferentOwnerFor_Division(corpUsersForDivision, leadSourceCategory2.getLeadSourceCategory(), addDivision2.getDivisionName()).setPriority_SalesTerritory();
				
				
				fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

				} catch (Exception e) {
					fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
				}

			}
	
	
	
	@Test(priority = 16 , dependsOnMethods="ConfigurationForOwnerAssignmentByLeadSource" ,groups = { "LeadSource_OwnerAssignment", "sales_OwnerAssignment" , "sales_OwnerAssignment_Failed" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_11", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source - Round Robin")
	private void OwnerAssignmentByLeadSource_RoundRobin() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		
		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			
			 driver = fc.loginpage().login(driver);
			
			// Through API
			for (int j = 0; j <= 3; j++) {
				// Add Lead
				Lead lead = new Lead();
				lead.setFirstName("LeadSource" +j);
				lead.setLastName("RR" +fc.utobj().generateRandomNumber());
				lead.setEmail("frantest2017@gmail.com");
				lead.setBasedonAssignmentRules("yes");
				lead.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
				lead.setLeadSourceDetails("None");
				common.addLeadThroughWebServices(driver, lead);

				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(driver, lead);
				// System.out.println(xmlApi);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println("Owner Assigned to Lead >>>>>>>>" + lead.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);

				// Validation
				if (!corpUsersForSource2.contains(ownerAssignedToLeadApi)) {
					System.out.println("Owner should be among ::: " + corpUsersForSource2);
					fc.utobj().throwsException("Assign Lead Owner By LeadSource - Priority(Sales Territories) - RR - Api - FAILED");
				}
			}
			
			// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
			// Create CSV File with Lead Details
			String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_Source_RR.csv";
			System.out.println(filePath);
			List<Lead> leadList = new ArrayList<Lead>();

			for (int j = 0; j <= 3; j++) {
				Lead importLead = new Lead();
				importLead.setFirstName("LeadSource_Import" +j);
				importLead.setLastName("RR" +fc.utobj().generateRandomNumber());
				importLead.setEmail("frantest2017@gmail.com");
				// importLead.setDivision(addDivision1.getDivisionName());
				//importLead.setCountry("USA");
				//importLead.setStateProvince("Florida");
				//importLead.setCounty("Gulf");
				// importLead.setZipPostalCode(salesTerritories2.getZipPostalCodeText());
				importLead.setBasedonAssignmentRules("yes");
				importLead.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
				importLead.setLeadSourceDetails("None");

				leadList.add(importLead);
			}

			common.writeCsvFile(filePath, leadList);

			// Import the CSV file
			Import importlead = new Import();
			ImportTest importTest = new ImportTest(driver);

			// Navigate to Import Tab
			fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

			importlead.setImportType("Leads");
			importlead.setSpecifyFileFormat("CSV");
			importlead.setSalesDataFile(filePath);
			// importlead.setSalesDataFile(fc.utobj().getFilePathFromTestData("AsgmntByDivPrioritySalesTerritories_GroupByStatesIntl.csv"));
			importlead.setLeadStatus("New Lead");
			importlead.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
			importlead.setLeadSourceDetails("None");

			importTest.importLeadsCSV(importlead);

			// Validation
			Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

			Lead x = new Lead();
			List<String> leadsFirstNames = map.get("FN");
			List<String> leadsLastNames = map.get("LN");

			for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
				System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
				x.setFirstName(leadsFirstNames.get(a));
				x.setLastName(leadsLastNames.get(b));

				String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
				String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
				System.out.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

				// Validate Owner Assignment
				if (!corpUsersForSource2.contains(ownerAssignedToLeadx)) {
					System.out.println("Owner should be among ::: " + corpUsersForSource2);
					fc.utobj().throwsException("Assign Lead Owner by Lead Source - Priority(Sales Territories) - RR - Import - FAILED");
				}
			}
			
			// Through WebForm ----------------------------------------------------------------------------------------------------------------------------------------------------------
			// Create WebForm
			ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
			ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();
			
			fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
			common.set_ManageWebform(manageWebFormGenerator);
			manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
			manageWebFormGenerator.setLeadSourceDetails("None");
			// manageWebFormGenerator.setDivision(addDivision1.getDivisionName());
			manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

			// Add Lead - WebForm
			Lead webformLead = new Lead();
			for (int j = 0; j <= 2; j++) {
				webformLead.setFirstName("LeadSource_WebForm" + j);
				webformLead.setLastName("RR" + fc.utobj().generateRandomNumber());
				webformLead.setEmail("frantest2017@gmail.com");
				// webformLead.setDivision(addDivision1.getDivisionName());
				//webformLead.setCountry("USA");
				//webformLead.setStateProvince("Florida");
				//webformLead.setCounty("Gulf");
				//webformLead.setZipPostalCode(salesTerritories2.getZipPostalCodeText());
				webformLead.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
				webformLead.setLeadSourceDetails("None");

				// Add Lead through WebForm
				manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, webformLead);

				// fc.utobj().sleep();
				String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, webformLead);
				// System.out.println(xml2);
				String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
				System.out.println("Owner Assigned to Lead " + webformLead.getLeadFullName() + " >>>>>>>>"
						+ ownerAssignedTowebformLead);
				// Validate
				if (!corpUsersForSource2.contains(ownerAssignedTowebformLead)) {
					System.out.println("Owner should be among ::: " + corpUsersForSource2);

					fc.utobj().throwsException("Assign Lead Owner by Lead Source - Priority(Sales Territories) - RR - WebForm - FAILED");
				}
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			
		}
	}
				
	@Test(priority = 17 , dependsOnMethods="ConfigurationForOwnerAssignmentByLeadSource" ,groups = { "LeadSource_OwnerAssignment", "sales_OwnerAssignment" /*, "sales_OwnerAssignment_Failed"*/ })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_12", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source - RR_PriorityST_County")
	private void OwnerAssignmentByLeadSource_RR_PriorityST_County() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			try {
				
				 driver = fc.loginpage().login(driver);
				 
			// Check TerritoryCounty - RoundRobin - Priority(Sales Territory)
			// Through API ------------------------------------------------------------------------------------------------------------------------------------------------------------
			for (int j = 0; j <= 2; j++) {
				Lead leadApi = new Lead();
				leadApi.setFirstName("LeadSourceTerritory_Api" +j);
				leadApi.setLastName("RR_PriorityST_County" + fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				leadApi.setDivision(addDivision2.getDivisionName());
				leadApi.setCountry("USA");
				leadApi.setStateProvince("Florida");
				leadApi.setCounty("Gulf");
				// lead5.setZipPostalCode(salesTerritories2.getZipPostalCodeText());
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
				leadApi.setLeadSourceDetails("None");
				common.addLeadThroughWebServices(driver, leadApi);
				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadApi);
				// System.out.println(xmlApi);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println(
						"Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
				// Validation
				if (!corpUsersForSalesTerritoriesCounty.contains(ownerAssignedToLeadApi)) {
					fc.utobj().throwsException("Assign Lead Owner by Lead Source - For Territory(County) - API - FAILED");
				}
			}
			
			// CANNOT SET COUNTY THROUGH IMPORT - SO RESULTS WILL BE DIFFERENT
			// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
			// Create CSV File with Lead Details
			String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_Source_PriorityST_County.csv";
			System.out.println(filePath);
			List<Lead> leadList = new ArrayList<Lead>();

			for (int j = 0; j <= 2; j++) {
				Lead importLead = new Lead();
				importLead.setFirstName("LeadSourceTerritory_Import" + j);
				importLead.setLastName("RR_PriorityST_County" + fc.utobj().generateRandomNumber());
				importLead.setEmail("frantest2017@gmail.com");
				importLead.setDivision(addDivision2.getDivisionName());
				importLead.setCountry("USA");
				importLead.setStateProvince("Florida");
				importLead.setCounty("Gulf");
				//importLead.setZipPostalCode(salesTerritories2.getZipPostalCodeText());
				importLead.setBasedonAssignmentRules("yes");
				importLead.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
				importLead.setLeadSourceDetails("None");

				leadList.add(importLead);
			}

			common.writeCsvFile(filePath, leadList);

			// Import the CSV file
			Import importlead = new Import();
			ImportTest importTest = new ImportTest(driver);

			// Navigate to Import Tab
			fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

			importlead.setImportType("Leads");
			importlead.setSpecifyFileFormat("CSV");
			importlead.setSalesDataFile(filePath);
			// importlead.setSalesDataFile(fc.utobj().getFilePathFromTestData("AsgmntByDivPrioritySalesTerritories_GroupByStatesIntl.csv"));
			importlead.setLeadStatus("New Lead");
			importlead.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
			importlead.setLeadSourceDetails("None");

			importTest.importLeadsCSV(importlead);

			// Validation
			Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

			Lead x = new Lead();
			List<String> leadsFirstNames = map.get("FN");
			List<String> leadsLastNames = map.get("LN");

			for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
				System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
				x.setFirstName(leadsFirstNames.get(a));
				x.setLastName(leadsLastNames.get(b));

				String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
				String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
				System.out
						.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

				// Validate Owner Assignment
				if (!corpUsersForDivision.contains(ownerAssignedToLeadx)) {
					System.out.println("Owner should be among ::: " + corpUsersForDivision);

					fc.utobj().throwsException("Assign Lead Owner by Lead Source - For Territory(County) - Import - FAILED");
				}
			}
			
			
			// Through WebForm ----------------------------------------------------------------------------------------------------------------------------------------------------------
			// Create WebForm 
			ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
			ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

			fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
			common.set_ManageWebform(manageWebFormGenerator);
			manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
			manageWebFormGenerator.setLeadSourceDetails("None");
			manageWebFormGenerator.setDivision(addDivision2.getDivisionName());
			manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

			// Add Lead - WebForm
			Lead webformLead = new Lead();
			for (int j = 0; j <= 2; j++) {
				webformLead.setFirstName("LeadSourceTerritory_WebForm" + j);
				webformLead.setLastName("RR_PriorityST_County" + fc.utobj().generateRandomNumber());
				webformLead.setEmail("frantest2017@gmail.com");
				// webformLead.setDivision(addDivision1.getDivisionName());
				webformLead.setCountry("USA");
				webformLead.setStateProvince("Florida");
				webformLead.setCounty("Gulf");
				// webformLead.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				webformLead.setDivision(addDivision2.getDivisionName());
				webformLead.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
				webformLead.setLeadSourceDetails("None");

				// Add Lead through WebForm
				manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, webformLead); 

				// fc.utobj().sleep();
				String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, webformLead);
				// System.out.println(xml2);
				String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
				System.out.println("Owner Assigned to Lead " + webformLead.getLeadFullName() + " >>>>>>>>" + ownerAssignedTowebformLead);
				// Validate
				if (!corpUsersForSalesTerritoriesCounty.contains(ownerAssignedTowebformLead)) {
					System.out.println("Owner should be among ::: " + corpUsersForSalesTerritoriesCounty);
					fc.utobj().throwsException("Assign Lead Owner by Lead Source - For Territory(County) - WebForm - FAILED");
				}
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			
		}
	}
	
// ========================================================================================================================================================================================================================================================
	
	@Test(priority = 18 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" , "Sales_OwnerAssignment_13-Api" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_13-Api", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source-TC-33961")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33961_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

			try {
						
			// Through API ------------------------------------------------------------------------------------------------------------------------------------------------------------
						for (int j = 0; j <= 0; j++) {
							Lead leadApi = new Lead();
							
							leadApi.setFirstName("LeadSource_Api_33961");
							leadApi.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
							leadApi.setEmail("frantest2017@gmail.com");
							leadApi.setDivision(addDivision2.getDivisionName());
							leadApi.setCountry("USA");
							leadApi.setStateProvince("Florida");
							leadApi.setCounty("Gulf");
							leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
							leadApi.setBasedonAssignmentRules("yes");
							leadApi.setLeadSourceCategory("Friends");
							leadApi.setLeadSourceDetails("Friends");
							
							common.addLeadThroughWebServices(null, leadApi);
							// Fetch Owner assigned to Lead through Rest API
							String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(null, leadApi);
							// System.out.println(xmlApi);
							String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
							System.out.println(
									"Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
							// Validation
							if (!ownerAssignedToLeadApi.equalsIgnoreCase("FranConnect Administrator")) {
								System.out.println("Owner should be ::: FranConnect Administrator");
								fc.utobj().throwsException("Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - API - FAILED");
							}
						}
						
						fc.utobj().logoutAndQuitBrowser(null, testCaseId);
						
		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(null, e, testCaseId);

			/*System.out.println(e.getMessage());
			fc.utobj().throwsException1(e.getMessage());*/
		}
	}
	
	@Test(priority = 19 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_13-WebForm", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source-TC-33961")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33961_WebForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			// Through WebForm ----------------------------------------------------------------------------------------------------------------------------------------------------------
						// Create WebForm 
						ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
						ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

						fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
						common.set_ManageWebform(manageWebFormGenerator);
						manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
						manageWebFormGenerator.setLeadSourceDetails("None");
						manageWebFormGenerator.setDivision(addDivision2.getDivisionName());
						manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

						// Add Lead - WebForm
						for (int j = 0; j <= 0; j++) {
							Lead leadWebForm = new Lead();
							
							leadWebForm.setFirstName("LeadSource_Api_33961");
							leadWebForm.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
							leadWebForm.setEmail("frantest2017@gmail.com");
						// 	leadWebForm.setDivision(addDivision1.getDivisionName());
							leadWebForm.setCountry("USA");
							leadWebForm.setStateProvince("Florida");
							leadWebForm.setCounty("Gulf");
							leadWebForm.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
							// leadWebForm.setBasedonAssignmentRules("yes");
							leadWebForm.setLeadSourceCategory("Friends");
							leadWebForm.setLeadSourceDetails("Friends");

							// Add Lead through WebForm
							manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm); 

							// fc.utobj().sleep();
							String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
							String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
							System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>" + ownerAssignedTowebformLead);
							// Validation
							if (!ownerAssignedTowebformLead.equalsIgnoreCase("FranConnect Administrator")) {
								System.out.println("Owner should be ::: FranConnect Administrator");
								fc.utobj().throwsException("Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - WebForm - FAILED");
							}
						}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}
	
	@Test(priority = 20 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_13-Import", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source-TC-33961")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33961_Import() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
						// Create CSV File with Lead Details
						String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_33961.csv";
						System.out.println(filePath);
						List<Lead> leadList = new ArrayList<Lead>();

						for (int j = 0; j <= 2; j++) {
							Lead leadImport = new Lead();
							
							leadImport.setFirstName("LeadSource_Api_33961");
							leadImport.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
							leadImport.setEmail("frantest2017@gmail.com");
							leadImport.setDivision(addDivision2.getDivisionName());
							leadImport.setCountry("USA");
							leadImport.setStateProvince("Florida");
							leadImport.setCounty("Gulf");
							leadImport.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
							leadImport.setBasedonAssignmentRules("yes");
							leadImport.setLeadSourceCategory("Friends");
							leadImport.setLeadSourceDetails("Friends");

							leadList.add(leadImport);
						}

						common.writeCsvFile(filePath, leadList);

						// Import the CSV file
						Import importlead = new Import();
						ImportTest importTest = new ImportTest(driver);

						// Navigate to Import Tab
						fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

						importlead.setImportType("Leads");
						importlead.setSpecifyFileFormat("CSV");
						importlead.setSalesDataFile(filePath);
						importlead.setLeadStatus("New Lead");
						importlead.setLeadSourceCategory("Friends");
						importlead.setLeadSourceDetails("Friends");

						importTest.importLeadsCSV(importlead);

						// Validation
						Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

						Lead x = new Lead();
						List<String> leadsFirstNames = map.get("FN");
						List<String> leadsLastNames = map.get("LN");

						for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
							System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
							x.setFirstName(leadsFirstNames.get(a));
							x.setLastName(leadsLastNames.get(b));

							String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
							String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
							System.out
									.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

							// Validation
							if (!ownerAssignedToLeadx.equalsIgnoreCase("FranConnect Administrator")) {
								System.out.println("Owner should be :: FranConnect Administrator");
								fc.utobj().throwsException("Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - Import - FAILED");
							}
						}
						
						

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}


// ==========================================================================================================================================================================================================================================================	

	
	@Test(priority = 21 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_14-Api", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source-TC-33960")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33960_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

			try {
				

			// Through API
			// ------------------------------------------------------------------------------------------------------------------------------------------------------------
			for (int j = 0; j <= 0; j++) {
				Lead leadApi = new Lead();
				
				leadApi.setFirstName("LeadSource_Api_33960");
				leadApi.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				leadApi.setDivision(addDivision2.getDivisionName());
				leadApi.setCountry("USA");
				leadApi.setStateProvince("Florida");
				leadApi.setCounty("Gulf");
				leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory("Friends");
				leadApi.setLeadSourceDetails("Friends");
				
				common.addLeadThroughWebServices(null, leadApi);
				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(null, leadApi);
				// System.out.println(xmlApi);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println(
						"Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
				// Validation
				if (!ownerAssignedToLeadApi.equalsIgnoreCase("FranConnect Administrator")) {
					System.out.println("Owner should be ::: FranConnect Administrator");
					fc.utobj().throwsException(
							"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - Api - FAILED");
				}
			}
			
			fc.utobj().logoutAndQuitBrowser(null, testCaseId);

		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(null, e, testCaseId);

			/*System.out.println(e.getMessage());
			fc.utobj().throwsException1(e.getMessage());*/
		}
	}
	
	@Test(priority = 22 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_14-WebForm", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source-TC-33960")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33960_WebForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			// Through WebForm ----------------------------------------------------------------------------------------------------------------------------------------------------------
			// Create WebForm 
			ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
			ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

			fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
			common.set_ManageWebform(manageWebFormGenerator);
			manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
			manageWebFormGenerator.setLeadSourceDetails("None");
			manageWebFormGenerator.setDivision(addDivision2.getDivisionName());
			manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

			// Add Lead - WebForm
			for (int j = 0; j <= 0; j++) {
				Lead leadWebForm = new Lead();
				leadWebForm.setFirstName("LeadSource_Api_33960");
				leadWebForm.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadWebForm.setEmail("frantest2017@gmail.com");
				// leadWebForm.setDivision(addDivision1.getDivisionName());
				leadWebForm.setCountry("USA");
				leadWebForm.setStateProvince("Florida");
				leadWebForm.setCounty("Gulf");
				leadWebForm.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				// leadWebForm.setBasedonAssignmentRules("yes");
				leadWebForm.setLeadSourceCategory("Friends");
				leadWebForm.setLeadSourceDetails("Friends");

				// Add Lead through WebForm
				manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm); 

				// fc.utobj().sleep();
				String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
				String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
				System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>" + ownerAssignedTowebformLead);
				// Validation
				if (!ownerAssignedTowebformLead.equalsIgnoreCase("FranConnect Administrator")) {
					System.out.println("Owner should be ::: FranConnect Administrator");
					fc.utobj().throwsException("Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - WebForm - FAILED");
				}
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}
		
	
	@Test(priority = 23 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_14-Import", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source-TC-33960")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33960_Import() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
			// Create CSV File with Lead Details
			String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_33960.csv";
			System.out.println(filePath);
			List<Lead> leadList = new ArrayList<Lead>();

			for (int j = 0; j <= 2; j++) {
				Lead leadImport = new Lead();
				leadImport.setFirstName("LeadSource_Import_33960");
				leadImport.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadImport.setEmail("frantest2017@gmail.com");
				leadImport.setDivision(addDivision2.getDivisionName());
				leadImport.setCountry("USA");
				leadImport.setStateProvince("Florida");
				leadImport.setCounty("Gulf");
				leadImport.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadImport.setBasedonAssignmentRules("yes");
				leadImport.setLeadSourceCategory("Friends");
				leadImport.setLeadSourceDetails("Friends");

				leadList.add(leadImport);
			}

			common.writeCsvFile(filePath, leadList);

			// Import the CSV file
			Import importlead = new Import();
			ImportTest importTest = new ImportTest(driver);

			// Navigate to Import Tab
			fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

			importlead.setImportType("Leads");
			importlead.setSpecifyFileFormat("CSV");
			importlead.setSalesDataFile(filePath);
			importlead.setLeadStatus("New Lead");
			importlead.setLeadSourceCategory("Friends");
			importlead.setLeadSourceDetails("Friends");

			importTest.importLeadsCSV(importlead);

			// Validation
			Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

			Lead x = new Lead();
			List<String> leadsFirstNames = map.get("FN");
			List<String> leadsLastNames = map.get("LN");

			for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
				System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
				x.setFirstName(leadsFirstNames.get(a));
				x.setLastName(leadsLastNames.get(b));

				String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
				String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
				System.out
						.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

				// Validation
				if (!ownerAssignedToLeadx.equalsIgnoreCase("FranConnect Administrator")) {
					System.out.println("Owner should be ::: FranConnect Administrator");
					fc.utobj().throwsException("Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - Import - FAILED");
				}
			}
			
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}
	
// ==========================================================================================================================================================================================================================================================	
	
	@Test(priority = 24 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment", "sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_15-Api", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source-TC-33959")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33959_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

			try {
				
			// Through API
			// ------------------------------------------------------------------------------------------------------------------------------------------------------------
			for (int j = 0; j <= 0; j++) {
				Lead leadApi = new Lead();
				leadApi.setFirstName("LeadSource_Api_33959");
				leadApi.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				leadApi.setDivision(addDivision2.getDivisionName());
				leadApi.setCountry("USA");
				leadApi.setStateProvince("Florida");
				leadApi.setCounty("Gulf");
				leadApi.setZipPostalCode("5464685"); // unmatched
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory("Friends"); // unmatched
				leadApi.setLeadSourceDetails("Friends");
				common.addLeadThroughWebServices(null, leadApi);
				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(null, leadApi);
				// System.out.println(xmlApi);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println(
						"Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
				// Validation
				if (!ownerAssignedToLeadApi.equalsIgnoreCase("FranConnect Administrator")) {
					System.out.println("Owner should be ::: FranConnect Administrator");
					fc.utobj().throwsException(
							"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - API - FAILED");
				}
			}
			
			fc.utobj().logoutAndQuitBrowser(null, testCaseId);

		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(null, e, testCaseId);

			/*System.out.println(e.getMessage());
			fc.utobj().throwsException1(e.getMessage());*/			
		}
	}
	
	@Test(priority = 25 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_15-WebForm", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source-TC-33959")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33959_WebForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			// Through WebForm ----------------------------------------------------------------------------------------------------------------------------------------------------------
						// Create WebForm 
						ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
						ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

						fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
						common.set_ManageWebform(manageWebFormGenerator);
						manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
						manageWebFormGenerator.setLeadSourceDetails("None");
						manageWebFormGenerator.setDivision(addDivision2.getDivisionName());
						manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

						// Add Lead - WebForm
						for (int j = 0; j <= 0; j++) {
							Lead leadWebForm = new Lead();
							leadWebForm.setFirstName("LeadSource_Api_33959");
							leadWebForm.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
							leadWebForm.setEmail("frantest2017@gmail.com");
							// leadWebForm.setDivision(addDivision1.getDivisionName());
							leadWebForm.setCountry("USA");
							leadWebForm.setStateProvince("Florida");
							leadWebForm.setCounty("Gulf");
							leadWebForm.setZipPostalCode("5464685"); // unmatched
							// leadWebForm.setBasedonAssignmentRules("yes");
							leadWebForm.setLeadSourceCategory("Friends"); // unmatched
							leadWebForm.setLeadSourceDetails("Friends");

							// Add Lead through WebForm
							manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm); 

							// fc.utobj().sleep();
							String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
							String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
							System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>" + ownerAssignedTowebformLead);
							// Validation
							if (!ownerAssignedTowebformLead.equalsIgnoreCase("FranConnect Administrator")) {
								System.out.println("Owner should be ::: FranConnect Administrator");
								fc.utobj().throwsException("Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - WebForm - FAILED");
							}
						}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}
	
	@Test(priority = 26 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_15-Import", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source-TC-33959")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33959_Import() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
						// Create CSV File with Lead Details
						String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_33959.csv";
						System.out.println(filePath);
						List<Lead> leadList = new ArrayList<Lead>();

						for (int j = 0; j <= 2; j++) {
							Lead leadImport = new Lead();
							leadImport.setFirstName("LeadSource_Api_33959");
							leadImport.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
							leadImport.setEmail("frantest2017@gmail.com");
							leadImport.setDivision(addDivision2.getDivisionName());
							leadImport.setCountry("USA");
							leadImport.setStateProvince("Florida");
							leadImport.setCounty("Gulf");
							leadImport.setZipPostalCode("5464685"); // unmatched
							leadImport.setBasedonAssignmentRules("yes");
							leadImport.setLeadSourceCategory("Friends"); // unmatched
							leadImport.setLeadSourceDetails("Friends");

							leadList.add(leadImport);
						}

						common.writeCsvFile(filePath, leadList);

						// Import the CSV file
						Import importlead = new Import();
						ImportTest importTest = new ImportTest(driver);

						// Navigate to Import Tab
						fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

						importlead.setImportType("Leads");
						importlead.setSpecifyFileFormat("CSV");
						importlead.setSalesDataFile(filePath);
						importlead.setLeadStatus("New Lead");
						importlead.setLeadSourceCategory("Friends");
						importlead.setLeadSourceDetails("Friends");

						importTest.importLeadsCSV(importlead);

						// Validation
						Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

						Lead x = new Lead();
						List<String> leadsFirstNames = map.get("FN");
						List<String> leadsLastNames = map.get("LN");

						for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
							System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
							x.setFirstName(leadsFirstNames.get(a));
							x.setLastName(leadsLastNames.get(b));

							String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
							String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
							System.out
									.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

							// Validation
							if (!ownerAssignedToLeadx.equalsIgnoreCase("FranConnect Administrator")) {
								System.out.println("Owner should be ::: FranConnect Administrator");
								fc.utobj().throwsException(
										"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - WebForm - FAILED");
							}
						}
						
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}
	
// ============================================================================================================================================================================================================================================================
	
	@Test(priority = 27 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_16-Api", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source-TC-33958")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33958_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

			try {
				
			// Through API
			// ------------------------------------------------------------------------------------------------------------------------------------------------------------
			for (int j = 0; j <= 0; j++) {
				Lead leadApi = new Lead();
				leadApi.setFirstName("LeadSource_Api_33958");
				leadApi.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				leadApi.setDivision(addDivision2.getDivisionName());
				leadApi.setCountry("USA");
				// leadApi.setStateProvince("Florida"); unmatched
				leadApi.setCounty("Gulf");
				// leadApi.setZipPostalCode("5464685"); // blank
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory("Friends"); // unmatched
				leadApi.setLeadSourceDetails("Friends");
				
				common.addLeadThroughWebServices(null, leadApi);
				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(null, leadApi);
				// System.out.println(xmlApi);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println(
						"Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
				// Validation
				if (!ownerAssignedToLeadApi.equalsIgnoreCase("FranConnect Administrator")) {
					System.out.println("Owner should be ::: FranConnect Administrator");
					fc.utobj().throwsException(
							"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - API - FAILED");
				}
			}
			
			fc.utobj().logoutAndQuitBrowser(null, testCaseId);

		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(null, e, testCaseId);

			/*System.out.println(e.getMessage());
			fc.utobj().throwsException1(e.getMessage());*/			
		}
	}
	
	@Test(priority = 28 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_16-WebForm", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source-TC-33958")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33958_WebForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			// Through WebForm ----------------------------------------------------------------------------------------------------------------------------------------------------------
			// Create WebForm 
			ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
			ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

			fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
			common.set_ManageWebform(manageWebFormGenerator);
			manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
			manageWebFormGenerator.setLeadSourceDetails("None");
			manageWebFormGenerator.setDivision(addDivision2.getDivisionName());
			manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

			// Add Lead - WebForm
			for (int j = 0; j <= 0; j++) {
				Lead leadWebForm = new Lead();
				leadWebForm.setFirstName("LeadSource_WebForm_33958");
				leadWebForm.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadWebForm.setEmail("frantest2017@gmail.com");
				// leadWebForm.setDivision(addDivision1.getDivisionName());
				leadWebForm.setCountry("USA");
				// leadApi.setStateProvince("Florida"); unmatched
				leadWebForm.setCounty("Gulf");
				// leadApi.setZipPostalCode("5464685"); // blank
				// leadWebForm.setBasedonAssignmentRules("yes");
				leadWebForm.setLeadSourceCategory("Friends"); // unmatched
				leadWebForm.setLeadSourceDetails("Friends");

				// Add Lead through WebForm
				manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm); 

				// fc.utobj().sleep();
				String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
				String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
				System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>" + ownerAssignedTowebformLead);
				// Validation
				if (!ownerAssignedTowebformLead.equalsIgnoreCase("FranConnect Administrator")) {
					System.out.println("Owner should be ::: FranConnect Administrator");
					fc.utobj().throwsException("Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - WebForm - TC-33958 - FAILED");
				}
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}
	
	
	@Test(priority = 29 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_16-Import", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source-TC-33958")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33958_Import() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
			// Create CSV File with Lead Details
			String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_33958.csv";
			System.out.println(filePath);
			List<Lead> leadList = new ArrayList<Lead>();

			for (int j = 0; j <= 0; j++) {
				Lead leadImport = new Lead();
				leadImport.setFirstName("LeadSource_Import_33958");
				leadImport.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadImport.setEmail("frantest2017@gmail.com");
				leadImport.setDivision(addDivision2.getDivisionName());
				leadImport.setCountry("USA");
				// leadApi.setStateProvince("Florida"); unmatched
				leadImport.setCounty("Gulf");
				// leadApi.setZipPostalCode("5464685"); // blank
				leadImport.setBasedonAssignmentRules("yes");
				leadImport.setLeadSourceCategory("Friends"); // unmatched
				leadImport.setLeadSourceDetails("Friends");

				leadList.add(leadImport);
			}

			common.writeCsvFile(filePath, leadList);

			// Import the CSV file
			Import importlead = new Import();
			ImportTest importTest = new ImportTest(driver);

			// Navigate to Import Tab
			fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

			importlead.setImportType("Leads");
			importlead.setSpecifyFileFormat("CSV");
			importlead.setSalesDataFile(filePath);
			importlead.setLeadStatus("New Lead");
			importlead.setLeadSourceCategory("Friends");
			importlead.setLeadSourceDetails("Friends");

			importTest.importLeadsCSV(importlead);

			// Validation
			Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

			Lead x = new Lead();
			List<String> leadsFirstNames = map.get("FN");
			List<String> leadsLastNames = map.get("LN");

			for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
				System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
				x.setFirstName(leadsFirstNames.get(a));
				x.setLastName(leadsLastNames.get(b));

				String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
				String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
				System.out
						.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

				// Validation
				if (!ownerAssignedToLeadx.equalsIgnoreCase("FranConnect Administrator")) {
					System.out.println("Owner should be ::: FranConnect Administrator");
					fc.utobj().throwsException(
							"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - Import - TC-33958 - FAILED");
				}
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}

// ==========================================================================================================================================================================================================================================================

	@Test(priority = 30 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_17-Api", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source-TC-33957")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33957_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

			try {
				
			// Through API
			// ------------------------------------------------------------------------------------------------------------------------------------------------------------
			for (int j = 0; j <= 0; j++) {
				Lead leadApi = new Lead();
				leadApi.setFirstName("LeadSource_Api_33957");
				leadApi.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				// leadApi.setDivision(addDivision1.getDivisionName()); // Division
				leadApi.setCountry("USA");
				// leadApi.setStateProvince("Florida"); // blank
				leadApi.setCounty("Gulf");
				// leadApi.setZipPostalCode("5464685"); // blank
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory("Friends"); // unmatched
				leadApi.setLeadSourceDetails("Friends");
				
				common.addLeadThroughWebServices(null, leadApi);
				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(null, leadApi);
				// System.out.println(xmlApi);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println(
						"Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
				// Validation
				// Validate Owner Assignment
				if (!ownerAssignedToLeadApi.equalsIgnoreCase("FranConnect Administrator")) {
					System.out.println("Owner should be ::: FranConnect Administrator");
				fc.utobj().throwsException("Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33957 - API - FAILED");
				}
			}
			
			fc.utobj().logoutAndQuitBrowser(null, testCaseId);

		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(null, e, testCaseId);

			/*System.out.println(e.getMessage());
			fc.utobj().throwsException1(e.getMessage());	*/		
		}
	}
	
	@Test(priority = 31 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_17-WebForm", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source-TC-33957")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33957_WebForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			// Through WebForm ----------------------------------------------------------------------------------------------------------------------------------------------------------
						// Create WebForm 
						ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
						ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

						fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
						common.set_ManageWebform(manageWebFormGenerator);
						manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
						manageWebFormGenerator.setLeadSourceDetails("None");
						// manageWebFormGenerator.setDivision(addDivision1.getDivisionName());
						manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

						// Add Lead - WebForm
						for (int j = 0; j <= 0; j++) {
							Lead leadWebForm = new Lead();
							leadWebForm.setFirstName("LeadSource_WebForm_33957");
							leadWebForm.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
							leadWebForm.setEmail("frantest2017@gmail.com");
							// leadApi.setDivision(addDivision1.getDivisionName()); // Division
							leadWebForm.setCountry("USA");
							// leadApi.setStateProvince("Florida"); // blank
							leadWebForm.setCounty("Gulf");
							// leadApi.setZipPostalCode("5464685"); // blank
							// leadWebForm.setBasedonAssignmentRules("yes");
							leadWebForm.setLeadSourceCategory("Friends"); // unmatched
							leadWebForm.setLeadSourceDetails("Friends");

							// Add Lead through WebForm
							manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm); 

							// fc.utobj().sleep();
							String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
							String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
							System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>" + ownerAssignedTowebformLead);
							// Validation
							if (!ownerAssignedTowebformLead.equalsIgnoreCase("FranConnect Administrator")) {
								System.out.println("Owner should be ::: FranConnect Administrator");
								fc.utobj().throwsException("Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33957 - WebForm - FAILED");
								}
							}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}
	
	
	@Test(priority = 32 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_17-Import", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source - TC-33957")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33957_Import() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
						// Create CSV File with Lead Details
						String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_33957.csv";
						System.out.println(filePath);
						List<Lead> leadList = new ArrayList<Lead>();

						for (int j = 0; j <= 0; j++) {
							Lead leadImport = new Lead();
							leadImport.setFirstName("LeadSource_Import_33957");
							leadImport.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
							leadImport.setEmail("frantest2017@gmail.com");
							// leadApi.setDivision(addDivision1.getDivisionName()); // Division
							leadImport.setCountry("USA");
							// leadApi.setStateProvince("Florida"); // blank
							leadImport.setCounty("Gulf");
							// leadApi.setZipPostalCode("5464685"); // blank
							leadImport.setBasedonAssignmentRules("yes");
							leadImport.setLeadSourceCategory("Friends"); // unmatched
							leadImport.setLeadSourceDetails("Friends");

							leadList.add(leadImport);
						}

						common.writeCsvFile(filePath, leadList);

						// Import the CSV file
						Import importlead = new Import();
						ImportTest importTest = new ImportTest(driver);

						// Navigate to Import Tab
						fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

						importlead.setImportType("Leads");
						importlead.setSpecifyFileFormat("CSV");
						importlead.setSalesDataFile(filePath);
						importlead.setLeadStatus("New Lead");
						importlead.setLeadSourceCategory("Friends");
						importlead.setLeadSourceDetails("Friends");
						
						importTest.importLeadsCSV(importlead);

						// Validation
						Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

						Lead x = new Lead();
						List<String> leadsFirstNames = map.get("FN");
						List<String> leadsLastNames = map.get("LN");

						for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
							System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
							x.setFirstName(leadsFirstNames.get(a));
							x.setLastName(leadsLastNames.get(b));

							String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
							String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
							System.out
									.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

							// Validation
							if (!ownerAssignedToLeadx.equalsIgnoreCase("FranConnect Administrator")) {
								System.out.println("Owner should be ::: FranConnect Administrator");
								fc.utobj().throwsException(
										"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - Import - TC-33957 - FAILED");
							}
						}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}

// ===========================================================================================================================================================================================================================================================	
		
	@Test(priority = 33 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_18-Api", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source- TC-33956")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33956_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

			try {
				
			// Through API
			// ------------------------------------------------------------------------------------------------------------------------------------------------------------
			for (int j = 0; j <= 0; j++) {
				Lead leadApi = new Lead();
				leadApi.setFirstName("LeadSource_Api_33956");
				leadApi.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				leadApi.setDivision(addDivision2.getDivisionName()); // Division
																		// MATCHED
				leadApi.setCountry("USA");
				// leadApi.setStateProvince("Florida"); // blank
				leadApi.setCounty("Gulf");
				// leadApi.setZipPostalCode("5464685"); // blank
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory()); // MATCHED
				leadApi.setLeadSourceDetails("None");
				
				common.addLeadThroughWebServices(null, leadApi);
				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(null, leadApi);
				// System.out.println(xmlApi);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println(
						"Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
				// Validation
				if (!corpUsersForDivision.contains(ownerAssignedToLeadApi)) {
					System.out.println("Owner should be among ::: " + corpUsersForDivision);
					fc.utobj().throwsException("Assign Lead Owner by Lead Source - Api - TC-33956 - FAILED");
				}
			}
			
			fc.utobj().logoutAndQuitBrowser(null, testCaseId);
			

		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(null, e, testCaseId);

			/*System.out.println(e.getMessage());
			fc.utobj().throwsException1(e.getMessage());	*/		
		}
	}
	
	@Test(priority = 34 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_18-WebForm", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source-TC-33956")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33956_WebForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			// Through WebForm ----------------------------------------------------------------------------------------------------------------------------------------------------------
			// Create WebForm 
			ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
			ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

			fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
			common.set_ManageWebform(manageWebFormGenerator);
			manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
			manageWebFormGenerator.setLeadSourceDetails("None");
			manageWebFormGenerator.setDivision(addDivision2.getDivisionName());
			manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

			// Add Lead - WebForm
			for (int j = 0; j <= 0; j++) {
				Lead leadWebForm = new Lead();
				leadWebForm.setFirstName("LeadSource_Api_33956");
				leadWebForm.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadWebForm.setEmail("frantest2017@gmail.com");
			//	leadWebForm.setDivision(addDivision1.getDivisionName()); // Division
																		// MATCHED
				leadWebForm.setCountry("USA");
				// leadApi.setStateProvince("Florida"); // blank
				leadWebForm.setCounty("Gulf");
				// leadApi.setZipPostalCode("5464685"); // blank
				// leadWebForm.setBasedonAssignmentRules("yes");
				leadWebForm.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory()); // MATCHED
				leadWebForm.setLeadSourceDetails("None");

				// Add Lead through WebForm
				manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm); 

				// fc.utobj().sleep();
				String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
				String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
				System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>" + ownerAssignedTowebformLead);
				// Validation
				if (!corpUsersForDivision.contains(ownerAssignedTowebformLead)) {
					System.out.println("Owner should be among ::: " + corpUsersForDivision);
					fc.utobj().throwsException("Assign Lead Owner by Lead Source - WebForm - TC-33956 - FAILED");
				}
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}
	
	@Test(priority = 35 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_18 -Import", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33956")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33956_Import() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
			// Create CSV File with Lead Details
			String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_33956.csv";
			System.out.println(filePath);
			List<Lead> leadList = new ArrayList<Lead>();

			for (int j = 0; j <= 0; j++) {
				Lead leadImport = new Lead();
				leadImport.setFirstName("LeadSource_Api_33956");
				leadImport.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadImport.setEmail("frantest2017@gmail.com");
				leadImport.setDivision(addDivision2.getDivisionName()); // Division
																		// MATCHED
				leadImport.setCountry("USA");
				// leadApi.setStateProvince("Florida"); // blank
				leadImport.setCounty("Gulf");
				// leadApi.setZipPostalCode("5464685"); // blank
				leadImport.setBasedonAssignmentRules("yes");
				leadImport.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory()); // MATCHED
				leadImport.setLeadSourceDetails("None");
				leadList.add(leadImport);
			}

			common.writeCsvFile(filePath, leadList);

			// Import the CSV file
			Import importlead = new Import();
			ImportTest importTest = new ImportTest(driver);

			// Navigate to Import Tab
			fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

			importlead.setImportType("Leads");
			importlead.setSpecifyFileFormat("CSV");
			importlead.setSalesDataFile(filePath);
			importlead.setLeadStatus("New Lead");
			importlead.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
			importlead.setLeadSourceDetails("None");

			importTest.importLeadsCSV(importlead);

			// Validation
			Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

			Lead x = new Lead();
			List<String> leadsFirstNames = map.get("FN");
			List<String> leadsLastNames = map.get("LN");

			for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
				System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
				x.setFirstName(leadsFirstNames.get(a));
				x.setLastName(leadsLastNames.get(b));

				String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
				String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
				System.out
						.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

				// Validation
				if (!corpUsersForDivision.contains(ownerAssignedToLeadx)) {
					System.out.println("Owner should be among ::: " + corpUsersForDivision);
					fc.utobj().throwsException("Assign Lead Owner by Lead Source - WebForm - TC-33956 - FAILED");
				}
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}
	
// ==========================================================================================================================================================================================================================================================
	
	
	@Test(priority = 36 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_19-Api", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33955")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33955_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

			try {
				
			// Through API
			// ------------------------------------------------------------------------------------------------------------------------------------------------------------
			for (int j = 0; j <= 0; j++) {
				Lead leadApi = new Lead();
				
				leadApi.setFirstName("LeadSource_Api_33955");
				leadApi.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				// leadApi.setDivision(addDivision1.getDivisionName()); // Blank
				leadApi.setCountry("USA");
				// leadApi.setStateProvince("Florida"); // blank
				// leadApi.setCounty("Gulf"); // Blank
				// leadApi.setZipPostalCode("5464685"); // blank
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory()); // MATCHED
				leadApi.setLeadSourceDetails("None");
				
				common.addLeadThroughWebServices(null, leadApi);
				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(null, leadApi);
				// System.out.println(xmlApi);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println(
						"Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
				// Validation
				if (! corpUsersForSource2.contains(ownerAssignedToLeadApi)) {
					System.out.println("Owner should be among ::: " + corpUsersForSource2);
					fc.utobj().throwsException("Assign Lead Owner by Lead Source - Api - TC-33955 - FAILED");
				}
			}
			
			fc.utobj().logoutAndQuitBrowser(null, testCaseId);
			

		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(null, e, testCaseId);

			/*System.out.println(e.getMessage());
			fc.utobj().throwsException1(e.getMessage());*/			
		}
	}
	
	@Test(priority = 37 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_19-WebForm", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33955")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33955_WebForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			// Through WebForm ----------------------------------------------------------------------------------------------------------------------------------------------------------
						// Create WebForm 
						ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
						ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

						fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
						common.set_ManageWebform(manageWebFormGenerator);
						manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
						manageWebFormGenerator.setLeadSourceDetails("None");
						// manageWebFormGenerator.setDivision(addDivision1.getDivisionName());
						manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

						// Add Lead - WebForm
						for (int j = 0; j <= 0; j++) {
							Lead leadWebForm = new Lead();
							leadWebForm.setFirstName("LeadSource_WebForm_33955");
							leadWebForm.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
							leadWebForm.setEmail("frantest2017@gmail.com");
							// leadApi.setDivision(addDivision1.getDivisionName()); // Blank
							leadWebForm.setCountry("USA");
							// leadApi.setStateProvince("Florida"); // blank
							// leadApi.setCounty("Gulf"); // Blank
							// leadApi.setZipPostalCode("5464685"); // blank
							// leadWebForm.setBasedonAssignmentRules("yes");
							leadWebForm.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory()); // MATCHED
							leadWebForm.setLeadSourceDetails("None");

							// Add Lead through WebForm
							manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm); 

							// fc.utobj().sleep();
							String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
							String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
							System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>" + ownerAssignedTowebformLead);
							// Validation
							if (! corpUsersForSource2.contains(ownerAssignedTowebformLead)) {
								System.out.println("Owner should be among ::: " + corpUsersForSource2);
								fc.utobj().throwsException("Assign Lead Owner by Lead Source - WebForm - TC-33955 - FAILED");
							}
						}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}
	
	@Test(priority = 38 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_19-Import", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33955")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33955_Import() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
						// Create CSV File with Lead Details
						String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_33955.csv";
						System.out.println(filePath);
						List<Lead> leadList = new ArrayList<Lead>();

						for (int j = 0; j <= 0; j++) {
							Lead leadImport = new Lead();
							leadImport.setFirstName("LeadSource_Import_33955");
							leadImport.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
							leadImport.setEmail("frantest2017@gmail.com");
							// leadApi.setDivision(addDivision1.getDivisionName()); // Blank
							leadImport.setCountry("USA");
							// leadApi.setStateProvince("Florida"); // blank
							// leadApi.setCounty("Gulf"); // Blank
							// leadApi.setZipPostalCode("5464685"); // blank
							leadImport.setBasedonAssignmentRules("yes");
							leadImport.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory()); // MATCHED
							leadImport.setLeadSourceDetails("None");
							leadList.add(leadImport);
						}

						common.writeCsvFile(filePath, leadList);

						// Import the CSV file
						Import importlead = new Import();
						ImportTest importTest = new ImportTest(driver);

						// Navigate to Import Tab
						fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

						importlead.setImportType("Leads");
						importlead.setSpecifyFileFormat("CSV");
						importlead.setSalesDataFile(filePath);
						importlead.setLeadStatus("New Lead");
						importlead.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
						importlead.setLeadSourceDetails("None");

						importTest.importLeadsCSV(importlead);

						// Validation
						Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

						Lead x = new Lead();
						List<String> leadsFirstNames = map.get("FN");
						List<String> leadsLastNames = map.get("LN");

						for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
							System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
							x.setFirstName(leadsFirstNames.get(a));
							x.setLastName(leadsLastNames.get(b));

							String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
							String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
							System.out
									.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

							// Validation
							if (! corpUsersForSource2.contains(ownerAssignedToLeadx)) {
								System.out.println("Owner should be among ::: " + corpUsersForSource2);
								fc.utobj().throwsException("Assign Lead Owner by Lead Source - Import - TC-33955 - FAILED");
							}
						}

		

						fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

					} catch (Exception e) {
						fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

					}

	}
	
// ==========================================================================================================================================================================================================================================================
	
	@Test(priority = 39 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" , "TC_33954" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_20-Api", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source - TC-33954")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33954_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

			try {
				
			// Through API
			// ------------------------------------------------------------------------------------------------------------------------------------------------------------
			for (int j = 0; j <= 0; j++) {
				Lead leadApi = new Lead();
				
				leadApi.setFirstName("LeadSource_Api_33954");
				leadApi.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				// leadApi.setDivision(addDivision1.getDivisionName()); // Blank
				leadApi.setCountry("USA");
				// leadApi.setStateProvince("Florida"); // blank
				leadApi.setCounty("Gulf"); // MATCHED
				// leadApi.setZipPostalCode("5464685"); // blank
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory()); // MATCHED
				leadApi.setLeadSourceDetails("None");
				
				common.addLeadThroughWebServices(null, leadApi);
				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(null, leadApi);
				// System.out.println(xmlApi);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println(
						"Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
				// Validation
				// Validate Owner Assignment
				if (!corpUsersForSource2.contains(ownerAssignedToLeadApi)) {
					System.out.println("Owner should be among ::: " + corpUsersForSource2);
					fc.utobj().throwsException("Assign Lead Owner by Lead Source - Api - TC-33954 - FAILED");
				}
			}
			
			fc.utobj().logoutAndQuitBrowser(null, testCaseId);


		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(null, e, testCaseId);

			/*System.out.println(e.getMessage());
			fc.utobj().throwsException1(e.getMessage());	*/		
		}
	}
	
	@Test(priority = 40 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_20-WebForm", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source - TC-33954")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33954_WebForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			// Through WebForm ----------------------------------------------------------------------------------------------------------------------------------------------------------
			// Create WebForm 
			ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
			ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

			fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
			common.set_ManageWebform(manageWebFormGenerator);
			manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
			manageWebFormGenerator.setLeadSourceDetails("None");
			// manageWebFormGenerator.setDivision(addDivision1.getDivisionName());
			manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

			// Add Lead - WebForm
			for (int j = 0; j <= 0; j++) {
				Lead leadWebForm = new Lead();
				leadWebForm.setFirstName("LeadSource_Api_33954");
				leadWebForm.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadWebForm.setEmail("frantest2017@gmail.com");
				// leadApi.setDivision(addDivision1.getDivisionName()); // Blank
				leadWebForm.setCountry("USA");
				// leadApi.setStateProvince("Florida"); // blank
				leadWebForm.setCounty("Gulf"); // MATCHED
				// leadApi.setZipPostalCode("5464685"); // blank
				// leadWebForm.setBasedonAssignmentRules("yes");
				leadWebForm.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory()); // MATCHED
				leadWebForm.setLeadSourceDetails("None");

				// Add Lead through WebForm
				manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm); 

				// fc.utobj().sleep();
				String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
				String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
				System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>" + ownerAssignedTowebformLead);
				// Validation
				if (!corpUsersForSource2.contains(ownerAssignedTowebformLead)) {
					System.out.println("Owner should be among ::: " + corpUsersForSource2);
					fc.utobj().throwsException("Assign Lead Owner by Lead Source - WebForm - TC-33954 - FAILED");
				}
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}
	
	@Test(priority = 41 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_20-Import", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33954")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33954_Import() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
			// Create CSV File with Lead Details
			String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_33954.csv";
			System.out.println(filePath);
			List<Lead> leadList = new ArrayList<Lead>();

			for (int j = 0; j <= 0; j++) {
				Lead leadImport = new Lead();
				leadImport.setFirstName("LeadSource_Api_33954");
				leadImport.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadImport.setEmail("frantest2017@gmail.com");
				// leadApi.setDivision(addDivision1.getDivisionName()); // Blank
				leadImport.setCountry("USA");
				// leadApi.setStateProvince("Florida"); // blank
				leadImport.setCounty("Gulf"); // MATCHED
				// leadApi.setZipPostalCode("5464685"); // blank
				leadImport.setBasedonAssignmentRules("yes");
				leadImport.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory()); // MATCHED
				leadImport.setLeadSourceDetails("None");
				leadList.add(leadImport);
			}

			common.writeCsvFile(filePath, leadList);

			// Import the CSV file
			Import importlead = new Import();
			ImportTest importTest = new ImportTest(driver);

			// Navigate to Import Tab
			fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

			importlead.setImportType("Leads");
			importlead.setSpecifyFileFormat("CSV");
			importlead.setSalesDataFile(filePath);
			importlead.setLeadStatus("New Lead");
			importlead.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
			importlead.setLeadSourceDetails("None");

			importTest.importLeadsCSV(importlead);

			// Validation
			Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

			Lead x = new Lead();
			List<String> leadsFirstNames = map.get("FN");
			List<String> leadsLastNames = map.get("LN");

			for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
				System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
				x.setFirstName(leadsFirstNames.get(a));
				x.setLastName(leadsLastNames.get(b));

				String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
				String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
				System.out
						.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

				// Validation
				if (!corpUsersForSource2.contains(ownerAssignedToLeadx)) {
					System.out.println("Owner should be among ::: " + corpUsersForSource2);
					fc.utobj().throwsException("Assign Lead Owner by Lead Source - Import - TC-33954 - FAILED");
				}
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}
	
// ============================================================================================================================================================================================================================================================	
	
	// --------------- NEED CLARIFICATION
	
	@Test(priority = 42 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_21-Api", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33953")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33953() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

			try {
				
			// Through API
			// ------------------------------------------------------------------------------------------------------------------------------------------------------------
			for (int j = 0; j <= 0; j++) {
				Lead leadApi = new Lead();
				leadApi.setFirstName("LeadSource_Api_33953");
				leadApi.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				leadApi.setDivision(addDivision2.getDivisionName()); // MATCHED
				leadApi.setCountry("USA");
				leadApi.setStateProvince("Florida"); // MATCHED
				leadApi.setCounty("Gulf"); // MATCHED
				// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText()); // 
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory()); // MATCHED
				leadApi.setLeadSourceDetails("None");
				common.addLeadThroughWebServices(null, leadApi);
				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(null, leadApi);
				// System.out.println(xmlApi);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println(
						"Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
				// Validation
				// Validate Owner Assignment
				if (!corpUsersForSalesTerritoriesCounty.contains(ownerAssignedToLeadApi)) {
					System.out.println("Owner should be among ::: " + corpUsersForSalesTerritoriesCounty);
					fc.utobj().throwsException("Assign Lead Owner by Lead Source - Api - TC-33953 - FAILED");
				}
			}
			
			fc.utobj().logoutAndQuitBrowser(null, testCaseId);

		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(null, e, testCaseId);

			/*System.out.println(e.getMessage());
			fc.utobj().throwsException1(e.getMessage());	*/		
		}
	}

	// =======================================================================================================================================================================================================================================================
		
	@Test(priority = 43 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_22-Api", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33952")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33952_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

			try {
				
			// Through API
			// ------------------------------------------------------------------------------------------------------------------------------------------------------------
			for (int j = 0; j <= 0; j++) {
				Lead leadApi = new Lead();
				leadApi.setFirstName("LeadSource_Api_33952");
				leadApi.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				leadApi.setDivision(addDivision2.getDivisionName()); // MATCHED
				leadApi.setCountry("USA");
				// leadApi.setStateProvince("Florida"); // BLANK
				leadApi.setCounty("Gulf"); // MATCHED
				leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory()); // MATCHED
				leadApi.setLeadSourceDetails("None");
				
				common.addLeadThroughWebServices(null, leadApi);
				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(null, leadApi);
				// System.out.println(xmlApi);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println(
						"Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
				// Validation
				// Validate Owner Assignment
				if (!corpUsersForSalesTerritoriesZip.contains(ownerAssignedToLeadApi)) {
					System.out.println("Owner should be among ::: " + corpUsersForSalesTerritoriesZip);
					fc.utobj().throwsException("Assign Lead Owner by Lead Source - Api - TC-33952 - FAILED");
				}
			}	
			
			fc.utobj().logoutAndQuitBrowser(null, testCaseId);
		
		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(null, e, testCaseId);

			/*System.out.println(e.getMessage());
			fc.utobj().throwsException1(e.getMessage());*/
		}
	}
	
	
	@Test(priority = 44 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_22-WebForm", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33952")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33952_WebForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			// Through WebForm ----------------------------------------------------------------------------------------------------------------------------------------------------------
						// Create WebForm 
						ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
						ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

						fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
						common.set_ManageWebform(manageWebFormGenerator);
						manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
						manageWebFormGenerator.setLeadSourceDetails("None");
						manageWebFormGenerator.setDivision(addDivision2.getDivisionName());
						manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

						// Add Lead - WebForm
						for (int j = 0; j <= 0; j++) {
							Lead leadWebForm = new Lead();
							leadWebForm.setFirstName("LeadSource_WebForm_33952");
							leadWebForm.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
							leadWebForm.setEmail("frantest2017@gmail.com");
						//	leadWebForm.setDivision(addDivision1.getDivisionName()); // MATCHED
							leadWebForm.setCountry("USA");
							// leadApi.setStateProvince("Florida"); // BLANK
							leadWebForm.setCounty("Gulf"); // MATCHED
							leadWebForm.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
							// leadWebForm.setBasedonAssignmentRules("yes");
							leadWebForm.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory()); // MATCHED
							leadWebForm.setLeadSourceDetails("None");

							// Add Lead through WebForm
							manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm); 

							// fc.utobj().sleep();
							String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
							String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
							System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>" + ownerAssignedTowebformLead);
							// Validation
							if (!corpUsersForSalesTerritoriesZip.contains(ownerAssignedTowebformLead)) {
								System.out.println("Owner should be among ::: " + corpUsersForSalesTerritoriesZip);
								fc.utobj().throwsException("Assign Lead Owner by Lead Source - WebForm - TC-33952 - FAILED");
							}
						}	

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}
	
	@Test(priority = 45 ,dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_22-Import", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33952")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33952_Import() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
			// Create CSV File with Lead Details
			String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_33952.csv";
			System.out.println(filePath);
			List<Lead> leadList = new ArrayList<Lead>();

			for (int j = 0; j <= 0; j++) {
				Lead leadImport = new Lead();
				leadImport.setFirstName("LeadSource_Import_33952");
				leadImport.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadImport.setEmail("frantest2017@gmail.com");
				leadImport.setDivision(addDivision2.getDivisionName()); // MATCHED
				leadImport.setCountry("USA");
				// leadApi.setStateProvince("Florida"); // BLANK
				leadImport.setCounty("Gulf"); // MATCHED
				leadImport.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadImport.setBasedonAssignmentRules("yes");
				leadImport.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory()); // MATCHED
				leadImport.setLeadSourceDetails("None");
				leadList.add(leadImport);
			}

			common.writeCsvFile(filePath, leadList);

			// Import the CSV file
			Import importlead = new Import();
			ImportTest importTest = new ImportTest(driver);

			// Navigate to Import Tab
			fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

			importlead.setImportType("Leads");
			importlead.setSpecifyFileFormat("CSV");
			importlead.setSalesDataFile(filePath);
			importlead.setLeadStatus("New Lead");
			importlead.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
			importlead.setLeadSourceDetails("None");

			importTest.importLeadsCSV(importlead);

			// Validation
			Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

			Lead x = new Lead();
			List<String> leadsFirstNames = map.get("FN");
			List<String> leadsLastNames = map.get("LN");

			for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
				System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
				x.setFirstName(leadsFirstNames.get(a));
				x.setLastName(leadsLastNames.get(b));

				String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
				String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
				System.out
						.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

				// Validation
				if (!corpUsersForSalesTerritoriesZip.contains(ownerAssignedToLeadx)) {
					System.out.println("Owner should be among ::: " + corpUsersForSalesTerritoriesZip);
					fc.utobj().throwsException("Assign Lead Owner by Lead Source - Import - TC-33952 - FAILED");
				}
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}
	

// ============================================================================================================================================================================================================================================================	
	
	@Test(priority = 46 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_23-Api", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source - TC-33951")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33951_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

			try {
				
			// Through API
			// ------------------------------------------------------------------------------------------------------------------------------------------------------------
			for (int j = 0; j <= 0; j++) {
				Lead leadApi = new Lead();
				leadApi.setFirstName("LeadSource_Api_33951");
				leadApi.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				// leadApi.setDivision(addDivision1.getDivisionName()); // BLANK
				leadApi.setCountry("USA");
				leadApi.setStateProvince("Florida"); // BLANK
				leadApi.setCounty("Gulf"); // MATCHED
				leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory()); // MATCHED
				leadApi.setLeadSourceDetails("None");
				
				common.addLeadThroughWebServices(null, leadApi);
				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(null, leadApi);
				// System.out.println(xmlApi);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println(
						"Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
				// Validation
				// Validate Owner Assignment
				if (!corpUsersForSalesTerritoriesZip.contains(ownerAssignedToLeadApi)) {
					System.out.println("Owner should be among ::: " + corpUsersForSalesTerritoriesZip);
					fc.utobj().throwsException("Assign Lead Owner by Lead Source - Api - TC-33951 - FAILED");
				}
			}
			
			fc.utobj().logoutAndQuitBrowser(null, testCaseId);

		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(null, e, testCaseId);

		/*	System.out.println(e.getMessage());
			fc.utobj().throwsException1(e.getMessage());	*/		
		}
	}
	
	@Test(priority = 47 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_23-WebForm", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33951")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33951_WebForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			// Through WebForm ----------------------------------------------------------------------------------------------------------------------------------------------------------
			// Create WebForm 
			ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
			ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

			fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
			common.set_ManageWebform(manageWebFormGenerator);
			manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
			manageWebFormGenerator.setLeadSourceDetails("None");
			// manageWebFormGenerator.setDivision(addDivision1.getDivisionName());
			manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

			// Add Lead - WebForm
			for (int j = 0; j <= 0; j++) {
				Lead leadWebForm = new Lead();
				leadWebForm.setFirstName("LeadSource_WebForm_33951");
				leadWebForm.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadWebForm.setEmail("frantest2017@gmail.com");
				// leadApi.setDivision(addDivision1.getDivisionName()); // BLANK
				leadWebForm.setCountry("USA");
				leadWebForm.setStateProvince("Florida"); // BLANK
				leadWebForm.setCounty("Gulf"); // MATCHED
				leadWebForm.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				// leadWebForm.setBasedonAssignmentRules("yes");
				leadWebForm.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory()); // MATCHED
				leadWebForm.setLeadSourceDetails("None");

				// Add Lead through WebForm
				manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm); 

				// fc.utobj().sleep();
				String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
				String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
				System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>" + ownerAssignedTowebformLead);
				// Validation
				if (!corpUsersForSalesTerritoriesZip.contains(ownerAssignedTowebformLead)) {
					System.out.println("Owner should be among ::: " + corpUsersForSalesTerritoriesZip);
					fc.utobj().throwsException("Assign Lead Owner by Lead Source - WebForm - TC-33951 - FAILED");
				}
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}

	
	@Test(priority = 48 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_23-Import", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33951")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33951_Import() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
						// Create CSV File with Lead Details
						String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_33951.csv";
						System.out.println(filePath);
						List<Lead> leadList = new ArrayList<Lead>();

						for (int j = 0; j <= 0; j++) {
							Lead leadImport = new Lead();
							leadImport.setFirstName("LeadSource_Import_33951");
							leadImport.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
							leadImport.setEmail("frantest2017@gmail.com");
							// leadApi.setDivision(addDivision1.getDivisionName()); // BLANK
							leadImport.setCountry("USA");
							leadImport.setStateProvince("Florida"); // BLANK
							leadImport.setCounty("Gulf"); // MATCHED
							leadImport.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
							leadImport.setBasedonAssignmentRules("yes");
							leadImport.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory()); // MATCHED
							leadImport.setLeadSourceDetails("None");
							leadList.add(leadImport);
						}

						common.writeCsvFile(filePath, leadList);

						// Import the CSV file
						Import importlead = new Import();
						ImportTest importTest = new ImportTest(driver);

						// Navigate to Import Tab
						fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

						importlead.setImportType("Leads");
						importlead.setSpecifyFileFormat("CSV");
						importlead.setSalesDataFile(filePath);
						importlead.setLeadStatus("New Lead");
						importlead.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
						importlead.setLeadSourceDetails("None");

						importTest.importLeadsCSV(importlead);

						// Validation
						Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

						Lead x = new Lead();
						List<String> leadsFirstNames = map.get("FN");
						List<String> leadsLastNames = map.get("LN");

						for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
							System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
							x.setFirstName(leadsFirstNames.get(a));
							x.setLastName(leadsLastNames.get(b));

							String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
							String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
							System.out
									.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

							// Validation
							if (!corpUsersForSalesTerritoriesZip.contains(ownerAssignedToLeadx)) {
								System.out.println("Owner should be among ::: " + corpUsersForSalesTerritoriesZip);
								fc.utobj().throwsException("Assign Lead Owner by Lead Source - Import - TC-33951 - FAILED");
							}
						}


			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}

	
// =========================================================================================================================================================================================================================================================	
	
	@Test(priority = 49 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_24-Api", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33950")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33950_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

			try {
				
			// Through API
			// ------------------------------------------------------------------------------------------------------------------------------------------------------------
			for (int j = 0; j <= 0; j++) {
				Lead leadApi = new Lead();
				leadApi.setFirstName("LeadSource_Api_33950");
				leadApi.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				leadApi.setDivision(addDivision2.getDivisionName()); // BLANK
				leadApi.setCountry("USA");
				leadApi.setStateProvince("Florida"); // BLANK
				leadApi.setCounty("Gulf"); // MATCHED
				leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory()); // MATCHED
				leadApi.setLeadSourceDetails("None");
				
				common.addLeadThroughWebServices(null, leadApi);
				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(null, leadApi);
				// System.out.println(xmlApi);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println(
						"Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
				// Validation
				// Validate Owner Assignment
				if (!corpUsersForSalesTerritoriesZip.contains(ownerAssignedToLeadApi)) {
					System.out.println("Owner should be among ::: " + corpUsersForSalesTerritoriesZip);
					fc.utobj().throwsException("Assign Lead Owner by Lead Source - Api - TC-33950 - FAILED");
				}
			}
			
			
		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(null, e, testCaseId);

			/*System.out.println(e.getMessage());
			fc.utobj().throwsException1(e.getMessage());	*/		
		}
	}
	
	@Test(priority = 50 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_24-WebForm", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33950")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33950_WebForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			// Through WebForm ----------------------------------------------------------------------------------------------------------------------------------------------------------
						// Create WebForm 
						ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
						ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

						fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
						common.set_ManageWebform(manageWebFormGenerator);
						manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
						manageWebFormGenerator.setLeadSourceDetails("None");
						manageWebFormGenerator.setDivision(addDivision2.getDivisionName());
						manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

						// Add Lead - WebForm
						for (int j = 0; j <= 0; j++) {
							Lead leadWebForm = new Lead();
							leadWebForm.setFirstName("LeadSource_WebForm_33950");
							leadWebForm.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
							leadWebForm.setEmail("frantest2017@gmail.com");
						//	leadWebForm.setDivision(addDivision1.getDivisionName()); // BLANK
							leadWebForm.setCountry("USA");
							leadWebForm.setStateProvince("Florida"); // BLANK
							leadWebForm.setCounty("Gulf"); // MATCHED
							leadWebForm.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
							// leadWebForm.setBasedonAssignmentRules("yes");
							leadWebForm.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory()); // MATCHED
							leadWebForm.setLeadSourceDetails("None");

							// Add Lead through WebForm
							manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm); 

							// fc.utobj().sleep();
							String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
							String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
							System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>" + ownerAssignedTowebformLead);
							// Validation
							if (!corpUsersForSalesTerritoriesZip.contains(ownerAssignedTowebformLead)) {
								System.out.println("Owner should be among ::: " + corpUsersForSalesTerritoriesZip);
								fc.utobj().throwsException("Assign Lead Owner by Lead Source - WebForm - TC-33950 - FAILED");
							}
						}
		

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}
	
	@Test(priority = 51 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_24-Import", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33950")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33950_Import() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
			// Create CSV File with Lead Details
			String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_33950.csv";
			System.out.println(filePath);
			List<Lead> leadList = new ArrayList<Lead>();

			for (int j = 0; j <= 0; j++) {
				Lead leadImport = new Lead();
				leadImport.setFirstName("LeadSource_Api_33950");
				leadImport.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadImport.setEmail("frantest2017@gmail.com");
				leadImport.setDivision(addDivision2.getDivisionName()); // BLANK
				leadImport.setCountry("USA");
				leadImport.setStateProvince("Florida"); // BLANK
				leadImport.setCounty("Gulf"); // MATCHED
				leadImport.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadImport.setBasedonAssignmentRules("yes");
				leadImport.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory()); // MATCHED
				leadImport.setLeadSourceDetails("None");
				leadList.add(leadImport);
			}

			common.writeCsvFile(filePath, leadList);

			// Import the CSV file
			Import importlead = new Import();
			ImportTest importTest = new ImportTest(driver);

			// Navigate to Import Tab
			fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

			importlead.setImportType("Leads");
			importlead.setSpecifyFileFormat("CSV");
			importlead.setSalesDataFile(filePath);
			importlead.setLeadStatus("New Lead");
			importlead.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
			importlead.setLeadSourceDetails("None");

			importTest.importLeadsCSV(importlead);

			// Validation
			Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

			Lead x = new Lead();
			List<String> leadsFirstNames = map.get("FN");
			List<String> leadsLastNames = map.get("LN");

			for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
				System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
				x.setFirstName(leadsFirstNames.get(a));
				x.setLastName(leadsLastNames.get(b));

				String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
				String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
				System.out
						.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

				// Validation
				if (!corpUsersForSalesTerritoriesZip.contains(ownerAssignedToLeadx)) {
					System.out.println("Owner should be among ::: " + corpUsersForSalesTerritoriesZip);
					fc.utobj().throwsException("Assign Lead Owner by Lead Source - Import- TC-33950 - FAILED");
				}
			
			}
			

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}
	
// ===========================================================================================================================================================================================================================================================	
	
	@Test(priority = 52 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_25-Api", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33949")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33949_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

			try {
				
			// Through API
			// ------------------------------------------------------------------------------------------------------------------------------------------------------------
			for (int j = 0; j <= 0; j++) {
				Lead leadApi = new Lead();
				leadApi.setFirstName("LeadSource_Api_33949");
				leadApi.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				// leadApi.setDivision(addDivision1.getDivisionName()); // BLANK
				// leadApi.setCountry("USA");
				// leadApi.setStateProvince("Florida"); // BLANK
				// leadApi.setCounty("Gulf"); // MATCHED
				// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory("Friends"); // NOT MATCHED
				leadApi.setLeadSourceDetails("Friends");
				
				common.addLeadThroughWebServices(null, leadApi);
				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(null, leadApi);
				// System.out.println(xmlApi);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println(
						"Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
				// Validation
				// Validate Owner Assignment
				if (!ownerAssignedToLeadApi.equalsIgnoreCase("FranConnect Administrator")) {
					System.out.println("Owner should be ::: FranConnect Administrator");
					fc.utobj().throwsException("Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33949 - API - FAILED");
				}
			}
			
		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(null, e, testCaseId);

			/*System.out.println(e.getMessage());
			fc.utobj().throwsException1(e.getMessage());	*/		
		}
	}
	
	@Test(priority = 53 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_25-WebForm", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33949 ") 
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33949_WebForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			// Through WebForm ----------------------------------------------------------------------------------------------------------------------------------------------------------
			// Create WebForm 
			ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
			ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

			fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
			common.set_ManageWebform(manageWebFormGenerator);
			manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
			manageWebFormGenerator.setLeadSourceDetails("None");
			// manageWebFormGenerator.setDivision(addDivision1.getDivisionName());
			manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

			// Add Lead - WebForm
			for (int j = 0; j <= 0; j++) {
				Lead leadWebForm = new Lead();
				leadWebForm.setFirstName("LeadSource_WebForm_33949");
				leadWebForm.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadWebForm.setEmail("frantest2017@gmail.com");
				// leadApi.setDivision(addDivision1.getDivisionName()); // BLANK
				// leadApi.setCountry("USA");
				// leadApi.setStateProvince("Florida"); // BLANK
				// leadApi.setCounty("Gulf"); // MATCHED
				// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				// leadWebForm.setBasedonAssignmentRules("yes");
				leadWebForm.setLeadSourceCategory("Friends"); // NOT MATCHED
				leadWebForm.setLeadSourceDetails("Friends");

				// Add Lead through WebForm
				manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm); 

				// fc.utobj().sleep();
				String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
				String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
				System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>" + ownerAssignedTowebformLead);
				// Validation
				if (!ownerAssignedTowebformLead.equalsIgnoreCase("FranConnect Administrator")) {
					System.out.println("Owner should be ::: FranConnect Administrator");
					fc.utobj().throwsException("Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33949 - WebForm - FAILED");
				}
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}
	
	@Test(priority = 54 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_25-Import", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33949")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33949_Import() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
						// Create CSV File with Lead Details
						String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_33949.csv";
						System.out.println(filePath);
						List<Lead> leadList = new ArrayList<Lead>();

						for (int j = 0; j <= 0; j++) {
							Lead leadImport = new Lead();
							leadImport.setFirstName("LeadSource_Import_33949");
							leadImport.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
							leadImport.setEmail("frantest2017@gmail.com");
							// leadApi.setDivision(addDivision1.getDivisionName()); // BLANK
							// leadApi.setCountry("USA");
							// leadApi.setStateProvince("Florida"); // BLANK
							// leadApi.setCounty("Gulf"); // MATCHED
							// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
							// leadImport.setBasedonAssignmentRules("yes");
							leadImport.setLeadSourceCategory("Friends"); // NOT MATCHED
							leadImport.setLeadSourceDetails("Friends");
							
							leadList.add(leadImport);
						}

						common.writeCsvFile(filePath, leadList);

						// Import the CSV file
						Import importlead = new Import();
						ImportTest importTest = new ImportTest(driver);

						// Navigate to Import Tab
						fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

						importlead.setImportType("Leads");
						importlead.setSpecifyFileFormat("CSV");
						importlead.setSalesDataFile(filePath);
						importlead.setLeadStatus("New Lead");
						importlead.setLeadSourceCategory("Friends");
						importlead.setLeadSourceDetails("Friends");

						importTest.importLeadsCSV(importlead);

						// Validation
						Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

						Lead x = new Lead();
						List<String> leadsFirstNames = map.get("FN");
						List<String> leadsLastNames = map.get("LN");

						for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
							System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
							x.setFirstName(leadsFirstNames.get(a));
							x.setLastName(leadsLastNames.get(b));

							String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
							String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
							System.out
									.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

							// Validation
							if (!ownerAssignedToLeadx.equalsIgnoreCase("FranConnect Administrator")) {
								System.out.println("Owner should be ::: FranConnect Administrator");
								fc.utobj().throwsException("Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33949 - Import - FAILED");
							}
						
						}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}
	
// =========================================================================================================================================================================================================================================================	

	@Test(priority = 55 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_26-Api", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33948")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33948_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

			try {
				
			// Through API
			// ------------------------------------------------------------------------------------------------------------------------------------------------------------
			for (int j = 0; j <= 0; j++) {
				Lead leadApi = new Lead();
				leadApi.setFirstName("LeadSource_Api_33948");
				leadApi.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				leadApi.setDivision(addDivision2.getDivisionName()); // BLANK
				leadApi.setCountry("USA");
				leadApi.setStateProvince("Florida"); // BLANK
				leadApi.setCounty("Gulf"); // MATCHED
				leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory("Friends"); // NOT MATCHED 
				leadApi.setLeadSourceDetails("Friends");
				
				common.addLeadThroughWebServices(null, leadApi);
				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(null, leadApi);
				// System.out.println(xmlApi);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println(
						"Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
				// Validation
				// Validate Owner Assignment
				if (!ownerAssignedToLeadApi.equalsIgnoreCase("FranConnect Administrator")) {
					System.out.println("Owner should be ::: FranConnect Administrator");
					fc.utobj().throwsException(
							"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33948 - API - FAILED");
				}
			}
			

		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(null, e, testCaseId);

			/*System.out.println(e.getMessage());
			fc.utobj().throwsException1(e.getMessage());*/			
		}
	}
	
	@Test(priority = 56 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_26-WebForm", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33948")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33948_WebForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			// Through WebForm ----------------------------------------------------------------------------------------------------------------------------------------------------------
						// Create WebForm 
						ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
						ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

						fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
						common.set_ManageWebform(manageWebFormGenerator);
						manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
						manageWebFormGenerator.setLeadSourceDetails("None");
						manageWebFormGenerator.setDivision(addDivision2.getDivisionName());
						manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

						// Add Lead - WebForm
						for (int j = 0; j <= 0; j++) {
							Lead leadWebForm = new Lead();
							leadWebForm.setFirstName("LeadSource_WebForm_33948");
							leadWebForm.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
							leadWebForm.setEmail("frantest2017@gmail.com");
							// leadWebForm.setDivision(addDivision1.getDivisionName()); // BLANK
							leadWebForm.setCountry("USA");
							leadWebForm.setStateProvince("Florida"); // BLANK
							leadWebForm.setCounty("Gulf"); // MATCHED
							leadWebForm.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
							// leadWebForm.setBasedonAssignmentRules("yes");
							leadWebForm.setLeadSourceCategory("Friends"); // NOT MATCHED 
							leadWebForm.setLeadSourceDetails("Friends");

							// Add Lead through WebForm
							manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm); 

							// fc.utobj().sleep();
							String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
							String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
							System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>" + ownerAssignedTowebformLead);
							// Validation
							if (!ownerAssignedTowebformLead.equalsIgnoreCase("FranConnect Administrator")) {
								System.out.println("Owner should be ::: FranConnect Administrator");
								fc.utobj().throwsException(
										"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33948 - WebForm - FAILED");
							}
						}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}
	
	@Test(priority = 57 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_26-Import", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33948")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33948_Import() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
			// Create CSV File with Lead Details
			String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_33948.csv";
			System.out.println(filePath);
			List<Lead> leadList = new ArrayList<Lead>();

			for (int j = 0; j <= 0; j++) {
				Lead leadImport = new Lead();
				leadImport.setFirstName("LeadSource_Api_33948");
				leadImport.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadImport.setEmail("frantest2017@gmail.com");
				leadImport.setDivision(addDivision2.getDivisionName()); // BLANK
				leadImport.setCountry("USA");
				leadImport.setStateProvince("Florida"); // BLANK
				leadImport.setCounty("Gulf"); // MATCHED
				leadImport.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadImport.setBasedonAssignmentRules("yes");
				leadImport.setLeadSourceCategory("Friends"); // NOT MATCHED 
				leadImport.setLeadSourceDetails("Friends");
				leadList.add(leadImport);
			}

			common.writeCsvFile(filePath, leadList);

			// Import the CSV file
			Import importlead = new Import();
			ImportTest importTest = new ImportTest(driver);

			// Navigate to Import Tab
			fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

			importlead.setImportType("Leads");
			importlead.setSpecifyFileFormat("CSV");
			importlead.setSalesDataFile(filePath);
			importlead.setLeadStatus("New Lead");
			importlead.setLeadSourceCategory("Friends");
			importlead.setLeadSourceDetails("Friends");

			importTest.importLeadsCSV(importlead);

			// Validation
			Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

			Lead x = new Lead();
			List<String> leadsFirstNames = map.get("FN");
			List<String> leadsLastNames = map.get("LN");

			for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
				System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
				x.setFirstName(leadsFirstNames.get(a));
				x.setLastName(leadsLastNames.get(b));

				String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
				String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
				System.out
						.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

				// Validation
				if (!ownerAssignedToLeadx.equalsIgnoreCase("FranConnect Administrator")) {
					System.out.println("Owner should be ::: FranConnect Administrator");
					fc.utobj().throwsException(
							"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33948 - Import - FAILED");
				}
			}


			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}
	
// ===========================================================================================================================================================================================================================================================	
	
	@Test(priority = 58 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_27-Api", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33947")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33947_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

			try {
				
			// Through API
			// ------------------------------------------------------------------------------------------------------------------------------------------------------------
			for (int j = 0; j <= 0; j++) {
				Lead leadApi = new Lead();
				leadApi.setFirstName("LeadSource_Api_33947");
				leadApi.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				leadApi.setDivision(addDivision2.getDivisionName()); // BLANK
				leadApi.setCountry("USA");
				// leadApi.setStateProvince("Florida"); // BLANK
				leadApi.setCounty("Gulf"); // MATCHED
				leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory("Friends"); // NOT MATCHED
				leadApi.setLeadSourceDetails("Friends");
				common.addLeadThroughWebServices(null, leadApi);
				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(null, leadApi);
				// System.out.println(xmlApi);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println(
						"Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
				// Validation
				// Validate Owner Assignment
				if (!ownerAssignedToLeadApi.equalsIgnoreCase("FranConnect Administrator")) {
					System.out.println("Owner should be ::: FranConnect Administrator");
					fc.utobj().throwsException(
							"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33947 - API - FAILED");
				}
			}
			
		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(null, e, testCaseId);

			/*System.out.println(e.getMessage());
			fc.utobj().throwsException1(e.getMessage());*/			
		}
	}
	
	@Test(priority = 59 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_27-WebForm", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33947")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33947_WebForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			// Through WebForm ----------------------------------------------------------------------------------------------------------------------------------------------------------
			// Create WebForm 
			ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
			ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

			fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
			common.set_ManageWebform(manageWebFormGenerator);
			manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
			manageWebFormGenerator.setLeadSourceDetails("None");
			manageWebFormGenerator.setDivision(addDivision2.getDivisionName());
			manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

			// Add Lead - WebForm
			for (int j = 0; j <= 0; j++) {
				Lead leadWebForm = new Lead();
				leadWebForm.setFirstName("LeadSource_WebForm_33947");
				leadWebForm.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadWebForm.setEmail("frantest2017@gmail.com");
			//	leadWebForm.setDivision(addDivision1.getDivisionName()); // BLANK
				leadWebForm.setCountry("USA");
				// leadApi.setStateProvince("Florida"); // BLANK
				leadWebForm.setCounty("Gulf"); // MATCHED
				leadWebForm.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				// leadWebForm.setBasedonAssignmentRules("yes");
				leadWebForm.setLeadSourceCategory("Friends"); // NOT MATCHED
				leadWebForm.setLeadSourceDetails("Friends");

				// Add Lead through WebForm
				manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm); 

				// fc.utobj().sleep();
				String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
				String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
				System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>" + ownerAssignedTowebformLead);
				// Validation
				if (!ownerAssignedTowebformLead.equalsIgnoreCase("FranConnect Administrator")) {
					System.out.println("Owner should be ::: FranConnect Administrator");
					fc.utobj().throwsException(
							"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33947 - WebForm - FAILED");
				}
			}
			

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}
	
	@Test(priority = 60 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_27-Import", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33947")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33947_Import() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
						// Create CSV File with Lead Details
						String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_33947.csv";
						System.out.println(filePath);
						List<Lead> leadList = new ArrayList<Lead>();

						for (int j = 0; j <= 0; j++) {
							Lead leadImport = new Lead();
							leadImport.setFirstName("LeadSource_Import_33947");
							leadImport.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
							leadImport.setEmail("frantest2017@gmail.com");
							leadImport.setDivision(addDivision2.getDivisionName()); // BLANK
							leadImport.setCountry("USA");
							// leadApi.setStateProvince("Florida"); // BLANK
							leadImport.setCounty("Gulf"); // MATCHED
							leadImport.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
							leadImport.setBasedonAssignmentRules("yes");
							leadImport.setLeadSourceCategory("Friends"); // NOT MATCHED
							leadImport.setLeadSourceDetails("Friends");
							leadList.add(leadImport);
						}

						common.writeCsvFile(filePath, leadList);

						// Import the CSV file
						Import importlead = new Import();
						ImportTest importTest = new ImportTest(driver);

						// Navigate to Import Tab
						fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

						importlead.setImportType("Leads");
						importlead.setSpecifyFileFormat("CSV");
						importlead.setSalesDataFile(filePath);
						importlead.setLeadStatus("New Lead");
						importlead.setLeadSourceCategory("Friends");
						importlead.setLeadSourceDetails("Friends");

						importTest.importLeadsCSV(importlead);

						// Validation
						Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

						Lead x = new Lead();
						List<String> leadsFirstNames = map.get("FN");
						List<String> leadsLastNames = map.get("LN");

						for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
							System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
							x.setFirstName(leadsFirstNames.get(a));
							x.setLastName(leadsLastNames.get(b));

							String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
							String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
							System.out
									.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

							// Validation
							if (!ownerAssignedToLeadx.equalsIgnoreCase("FranConnect Administrator")) {
								System.out.println("Owner should be ::: FranConnect Administrator");
								fc.utobj().throwsException(
										"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33947 - Import - FAILED");
							}
						}


			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}

	
	
	
// ==============================================================================================================================================================================================================	

	@Test(priority = 61 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_28-Api", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33946")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33946_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

			try {
				
			// Through API
			// ------------------------------------------------------------------------------------------------------------------------------------------------------------
			for (int j = 0; j <= 0; j++) {
				Lead leadApi = new Lead();
				leadApi.setFirstName("LeadSource_Api_33946");
				leadApi.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				leadApi.setDivision(addDivision2.getDivisionName()); // BLANK
				leadApi.setCountry("USA");
				leadApi.setStateProvince("Florida"); // BLANK
				leadApi.setCounty("Gulf"); // MATCHED
				// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory("Friends"); // NOT MATCHED
				leadApi.setLeadSourceDetails("Friends");
				
				common.addLeadThroughWebServices(null, leadApi);
				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(null, leadApi);
				// System.out.println(xmlApi);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println(
						"Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
				// Validation
				// Validate Owner Assignment
				if (!ownerAssignedToLeadApi.equalsIgnoreCase("FranConnect Administrator")) {
					System.out.println("Owner should be ::: FranConnect Administrator");
					fc.utobj().throwsException(
							"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33946 - API - FAILED");
				}
			}
			
		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(null, e, testCaseId);

			/*System.out.println(e.getMessage());
			fc.utobj().throwsException1(e.getMessage());*/			
		}
	}
	
	
	@Test(priority = 62 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_28-WebForm", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33946")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33946_WebForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			// Through WebForm ----------------------------------------------------------------------------------------------------------------------------------------------------------
						// Create WebForm 
						ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
						ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

						fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
						common.set_ManageWebform(manageWebFormGenerator);
						manageWebFormGenerator.setLeadSourceCategory("Friends");
						manageWebFormGenerator.setLeadSourceDetails("Friends");
						manageWebFormGenerator.setDivision(addDivision2.getDivisionName());
						manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

						// Add Lead - WebForm
						for (int j = 0; j <= 0; j++) {
							Lead leadWebForm = new Lead();
							leadWebForm.setFirstName("LeadSource_WebForm_33946");
							leadWebForm.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
							leadWebForm.setEmail("frantest2017@gmail.com");
							// leadWebForm.setDivision(addDivision1.getDivisionName()); // BLANK
							leadWebForm.setCountry("USA");
							leadWebForm.setStateProvince("Florida"); // BLANK
							leadWebForm.setCounty("Gulf"); // MATCHED
							// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
							// leadWebForm.setBasedonAssignmentRules("yes");
							leadWebForm.setLeadSourceCategory("Friends"); // NOT MATCHED
							leadWebForm.setLeadSourceDetails("Friends");

							// Add Lead through WebForm
							manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm); 

							// fc.utobj().sleep();
							String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
							String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
							System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>" + ownerAssignedTowebformLead);
							// Validation
							if (!ownerAssignedTowebformLead.equalsIgnoreCase("FranConnect Administrator")) {
								System.out.println("Owner should be ::: FranConnect Administrator");
								fc.utobj().throwsException(
										"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33946 - WebForm - FAILED");
							}
						}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}
	
	@Test(priority = 63 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_28-Import", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33946")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33946_Import() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
			// Create CSV File with Lead Details
			String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_33946.csv";
			System.out.println(filePath);
			List<Lead> leadList = new ArrayList<Lead>();

			for (int j = 0; j <= 0; j++) {
				Lead leadImport = new Lead();
				leadImport.setFirstName("LeadSource_Import_33946");
				leadImport.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadImport.setEmail("frantest2017@gmail.com");
				leadImport.setDivision(addDivision2.getDivisionName()); // BLANK
				leadImport.setCountry("USA");
				leadImport.setStateProvince("Florida"); // BLANK
				leadImport.setCounty("Gulf"); // MATCHED
				// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				// leadImport.setBasedonAssignmentRules("yes");
				leadImport.setLeadSourceCategory("Friends"); // NOT MATCHED
				leadImport.setLeadSourceDetails("Friends");
				
				leadList.add(leadImport);
			}

			common.writeCsvFile(filePath, leadList);

			// Import the CSV file
			Import importlead = new Import();
			ImportTest importTest = new ImportTest(driver);

			// Navigate to Import Tab
			fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

			importlead.setImportType("Leads");
			importlead.setSpecifyFileFormat("CSV");
			importlead.setSalesDataFile(filePath);
			importlead.setLeadStatus("New Lead");
			importlead.setLeadSourceCategory("Friends");
			importlead.setLeadSourceDetails("Friends");

			importTest.importLeadsCSV(importlead);

			// Validation
			Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

			Lead x = new Lead();
			List<String> leadsFirstNames = map.get("FN");
			List<String> leadsLastNames = map.get("LN");

			for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
				System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
				x.setFirstName(leadsFirstNames.get(a));
				x.setLastName(leadsLastNames.get(b));

				String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
				String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
				System.out
						.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

				// Validation
				if (!ownerAssignedToLeadx.equalsIgnoreCase("FranConnect Administrator")) {
					System.out.println("Owner should be ::: FranConnect Administrator");
					fc.utobj().throwsException(
							"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33946 - Import - FAILED");
				}
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}

// =============================================================================================================================================================================================================	

	@Test(priority = 64 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_29-Api", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33945")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33945_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

			try {
				
			// Through API
			// ------------------------------------------------------------------------------------------------------------------------------------------------------------
			for (int j = 0; j <= 0; j++) {
				Lead leadApi = new Lead();
				leadApi.setFirstName("LeadSource_Api_33945");
				leadApi.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				leadApi.setDivision(addDivision2.getDivisionName()); // BLANK
				leadApi.setCountry("USA");
				// leadApi.setStateProvince("Florida"); // BLANK
				leadApi.setCounty("Gulf"); // MATCHED
				// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory("Friends"); // NOT MATCHED
				leadApi.setLeadSourceDetails("Friends");
				common.addLeadThroughWebServices(null, leadApi);
				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(null, leadApi);
				// System.out.println(xmlApi);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println(
						"Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
				// Validation
				// Validate Owner Assignment
				if (!ownerAssignedToLeadApi.equalsIgnoreCase("FranConnect Administrator")) {
					System.out.println("Owner should be ::: FranConnect Administrator");
					fc.utobj().throwsException(
							"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33945 - API - FAILED");
				}
			}
			

		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(null, e, testCaseId);

			/*System.out.println(e.getMessage());
			fc.utobj().throwsException1(e.getMessage());	*/			
		}
	}
	
	
	@Test(priority = 65 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_29-WebForm", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33945")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33945_WebForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			// Through WebForm ----------------------------------------------------------------------------------------------------------------------------------------------------------
			// Create WebForm 
			ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
			ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

			fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
			common.set_ManageWebform(manageWebFormGenerator);
			manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
			manageWebFormGenerator.setLeadSourceDetails("None");
			manageWebFormGenerator.setDivision(addDivision2.getDivisionName());
			manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

			// Add Lead - WebForm
			for (int j = 0; j <= 0; j++) {
				Lead leadWebForm = new Lead();
				leadWebForm.setFirstName("LeadSource_WebForm_33945");
				leadWebForm.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadWebForm.setEmail("frantest2017@gmail.com");
			//	leadWebForm.setDivision(addDivision1.getDivisionName()); // BLANK
				leadWebForm.setCountry("USA");
				// leadApi.setStateProvince("Florida"); // BLANK
				leadWebForm.setCounty("Gulf"); // MATCHED
				// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				// leadWebForm.setBasedonAssignmentRules("yes");
				leadWebForm.setLeadSourceCategory("Friends"); // NOT MATCHED
				leadWebForm.setLeadSourceDetails("Friends");

				// Add Lead through WebForm
				manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm); 

				// fc.utobj().sleep();
				String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
				String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
				System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>" + ownerAssignedTowebformLead);
				// Validation
				if (!ownerAssignedTowebformLead.equalsIgnoreCase("FranConnect Administrator")) {
					System.out.println("Owner should be ::: FranConnect Administrator");
					fc.utobj().throwsException(
							"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33945 - WebForm - FAILED");
				}
			}
		
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}

	@Test(priority = 66 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_29-Import", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33945")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33945_Import() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
						// Create CSV File with Lead Details
						String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_33945.csv";
						System.out.println(filePath);
						List<Lead> leadList = new ArrayList<Lead>();

						for (int j = 0; j <= 0; j++) {
							Lead leadImport = new Lead();
							leadImport.setFirstName("LeadSource_Import_33945");
							leadImport.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
							leadImport.setEmail("frantest2017@gmail.com");
							leadImport.setDivision(addDivision2.getDivisionName()); // BLANK
							leadImport.setCountry("USA");
							// leadApi.setStateProvince("Florida"); // BLANK
							leadImport.setCounty("Gulf"); // MATCHED
							// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
							leadImport.setBasedonAssignmentRules("yes");
							leadImport.setLeadSourceCategory("Friends"); // NOT MATCHED
							leadImport.setLeadSourceDetails("Friends");

							
							leadList.add(leadImport);
						}

						common.writeCsvFile(filePath, leadList);

						// Import the CSV file
						Import importlead = new Import();
						ImportTest importTest = new ImportTest(driver);

						// Navigate to Import Tab
						fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

						importlead.setImportType("Leads");
						importlead.setSpecifyFileFormat("CSV");
						importlead.setSalesDataFile(filePath);
						importlead.setLeadStatus("New Lead");
						importlead.setLeadSourceCategory("Friends");
						importlead.setLeadSourceDetails("Friends");

						importTest.importLeadsCSV(importlead);

						// Validation
						Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

						Lead x = new Lead();
						List<String> leadsFirstNames = map.get("FN");
						List<String> leadsLastNames = map.get("LN");

						for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
							System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
							x.setFirstName(leadsFirstNames.get(a));
							x.setLastName(leadsLastNames.get(b));

							String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
							String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
							System.out
									.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

							// Validation
							if (!ownerAssignedToLeadx.equalsIgnoreCase("FranConnect Administrator")) {
								System.out.println("Owner should be ::: FranConnect Administrator");
								fc.utobj().throwsException(
										"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33945 - Import - FAILED");
							}
						}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}

	
// ==============================================================================================================================================================================================================	

	@Test(priority = 67 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_30-Api", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33944")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33944_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

			try {
				
			// Through API
			// ------------------------------------------------------------------------------------------------------------------------------------------------------------
			for (int j = 0; j <= 0; j++) {
				Lead leadApi = new Lead();
				leadApi.setFirstName("LeadSource_Api_33944");
				leadApi.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				// leadApi.setDivision(addDivision1.getDivisionName()); // BLANK
				leadApi.setCountry("USA");
				// leadApi.setStateProvince("Florida"); // BLANK
				leadApi.setCounty("Gulf"); // MATCHED
				// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory("Friends"); // NOT MATCHED
				leadApi.setLeadSourceDetails("Friends");
				common.addLeadThroughWebServices(null, leadApi);
				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(null, leadApi);
				// System.out.println(xmlApi);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println(
						"Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
				// Validation
				// Validate Owner Assignment
				if (!ownerAssignedToLeadApi.equalsIgnoreCase("FranConnect Administrator")) {
					System.out.println("Owner should be ::: FranConnect Administrator");
					fc.utobj().throwsException(
							"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33944 - API - FAILED");
				}
			}
			
		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(null, e, testCaseId);

			/*System.out.println(e.getMessage());
			fc.utobj().throwsException1(e.getMessage());*/				
		}
	}
	

	@Test(priority = 68 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_30-WebForm", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33944_WebForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			// Through WebForm ----------------------------------------------------------------------------------------------------------------------------------------------------------
						// Create WebForm 
						ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
						ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

						fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
						common.set_ManageWebform(manageWebFormGenerator);
						manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
						manageWebFormGenerator.setLeadSourceDetails("None");
						// manageWebFormGenerator.setDivision(addDivision1.getDivisionName());
						manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

						// Add Lead - WebForm
						for (int j = 0; j <= 0; j++) {
							Lead leadWebForm = new Lead();
							leadWebForm.setFirstName("LeadSource_WebForm_33944");
							leadWebForm.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
							leadWebForm.setEmail("frantest2017@gmail.com");
							// leadApi.setDivision(addDivision1.getDivisionName()); // BLANK
							leadWebForm.setCountry("USA");
							// leadApi.setStateProvince("Florida"); // BLANK
							leadWebForm.setCounty("Gulf"); // MATCHED
							// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
							// leadWebForm.setBasedonAssignmentRules("yes");
							leadWebForm.setLeadSourceCategory("Friends"); // NOT MATCHED
							leadWebForm.setLeadSourceDetails("Friends");

							// Add Lead through WebForm
							manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm); 

							// fc.utobj().sleep();
							String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
							String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
							System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>" + ownerAssignedTowebformLead);
							// Validation
							if (!ownerAssignedTowebformLead.equalsIgnoreCase("FranConnect Administrator")) {
								System.out.println("Owner should be ::: FranConnect Administrator");
								fc.utobj().throwsException(
										"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33944 - WebForm - FAILED");
							}
						}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}
	
	@Test(priority = 69 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_30-Import", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33944")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33944_Import() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
			// Create CSV File with Lead Details
			String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_33944.csv";
			System.out.println(filePath);
			List<Lead> leadList = new ArrayList<Lead>();

			for (int j = 0; j <= 0; j++) {
				Lead leadImport = new Lead();
				leadImport.setFirstName("LeadSource_Import_33944");
				leadImport.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadImport.setEmail("frantest2017@gmail.com");
				// leadApi.setDivision(addDivision1.getDivisionName()); // BLANK
				leadImport.setCountry("USA");
				// leadApi.setStateProvince("Florida"); // BLANK
				leadImport.setCounty("Gulf"); // MATCHED
				// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadImport.setBasedonAssignmentRules("yes");
				leadImport.setLeadSourceCategory("Friends"); // NOT MATCHED
				leadImport.setLeadSourceDetails("Friends");

				
				leadList.add(leadImport);
			}

			common.writeCsvFile(filePath, leadList);

			// Import the CSV file
			Import importlead = new Import();
			ImportTest importTest = new ImportTest(driver);

			// Navigate to Import Tab
			fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

			importlead.setImportType("Leads");
			importlead.setSpecifyFileFormat("CSV");
			importlead.setSalesDataFile(filePath);
			importlead.setLeadStatus("New Lead");
			importlead.setLeadSourceCategory("Friends");
			importlead.setLeadSourceDetails("Friends");

			importTest.importLeadsCSV(importlead);

			// Validation
			Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

			Lead x = new Lead();
			List<String> leadsFirstNames = map.get("FN");
			List<String> leadsLastNames = map.get("LN");

			for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
				System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
				x.setFirstName(leadsFirstNames.get(a));
				x.setLastName(leadsLastNames.get(b));

				String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
				String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
				System.out
						.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

				// Validation
				if (!ownerAssignedToLeadx.equalsIgnoreCase("FranConnect Administrator")) {
					System.out.println("Owner should be ::: FranConnect Administrator");
					fc.utobj().throwsException(
							"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33944 - Import - FAILED");
				}
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}

// ==============================================================================================================================================================================================================
	
	@Test(priority = 70 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_31-Api", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33943")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33943_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

			try {
				
			// Through API
			// ------------------------------------------------------------------------------------------------------------------------------------------------------------
			for (int j = 0; j <= 0; j++) {
				Lead leadApi = new Lead();
				leadApi.setFirstName("LeadSource_Api_33943");
				leadApi.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				leadApi.setDivision(addDivision2.getDivisionName()); // BLANK
				leadApi.setCountry("USA");
				// leadApi.setStateProvince("Florida"); // BLANK
				// leadApi.setCounty("Gulf"); // BLANK
				// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory()); // NOT MATCHED
				leadApi.setLeadSourceDetails("None");
				
				common.addLeadThroughWebServices(null, leadApi);
				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(null, leadApi);
				// System.out.println(xmlApi);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println(
						"Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
				// Validation
				// Validate Owner Assignment
				if (!corpUsersForDivision.contains(ownerAssignedToLeadApi)) {
					System.out.println("Owner should be among ::: " + corpUsersForDivision);
					fc.utobj().throwsException("Owner Assignment - Assign Lead Owner by Lead Source - TC-33943 - API - FAILED");
				}
			}
			
			fc.utobj().logoutAndQuitBrowser(null, testCaseId);

		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(null, e, testCaseId);

			/*System.out.println(e.getMessage());
			fc.utobj().throwsException1(e.getMessage());	*/			
		}
	}
	
	
	@Test(priority = 71 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_31-WebForm", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33943")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33943_WebForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			// Through WebForm ----------------------------------------------------------------------------------------------------------------------------------------------------------
			// Create WebForm 
			ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
			ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

			fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
			common.set_ManageWebform(manageWebFormGenerator);
			manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
			manageWebFormGenerator.setLeadSourceDetails("None");
			manageWebFormGenerator.setDivision(addDivision2.getDivisionName());
			manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

			// Add Lead - WebForm
			for (int j = 0; j <= 0; j++) {
				Lead leadWebForm = new Lead();
				leadWebForm.setFirstName("LeadSource_WebForm_33943");
				leadWebForm.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadWebForm.setEmail("frantest2017@gmail.com");
			//	leadWebForm.setDivision(addDivision1.getDivisionName()); // BLANK
				leadWebForm.setCountry("USA");
				// leadApi.setStateProvince("Florida"); // BLANK
				// leadApi.setCounty("Gulf"); // BLANK
				// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				// leadWebForm.setBasedonAssignmentRules("yes");
				leadWebForm.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory()); // NOT MATCHED
				leadWebForm.setLeadSourceDetails("None");

				// Add Lead through WebForm
				manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm); 

				// fc.utobj().sleep();
				String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
				String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
				System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>" + ownerAssignedTowebformLead);
				// Validation
				if (!corpUsersForDivision.contains(ownerAssignedTowebformLead)) {
					System.out.println("Owner should be among ::: " + corpUsersForDivision);
					fc.utobj().throwsException("Owner Assignment - Assign Lead Owner by Lead Source - TC-33943 - WebForm - FAILED");
				}
			}


			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}

	@Test(priority = 72 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_31-Import", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33943")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33943_Import() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
						// Create CSV File with Lead Details
						String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_33943.csv";
						System.out.println(filePath);
						List<Lead> leadList = new ArrayList<Lead>();

						for (int j = 0; j <= 0; j++) {
							Lead leadImport = new Lead();
							leadImport.setFirstName("LeadSource_Import_33943");
							leadImport.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
							leadImport.setEmail("frantest2017@gmail.com");
							leadImport.setDivision(addDivision2.getDivisionName()); // BLANK
							leadImport.setCountry("USA");
							// leadApi.setStateProvince("Florida"); // BLANK
							// leadApi.setCounty("Gulf"); // BLANK
							// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
							leadImport.setBasedonAssignmentRules("yes");
							leadImport.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory()); // NOT MATCHED
							leadImport.setLeadSourceDetails("None");

							
							leadList.add(leadImport);
						}

						common.writeCsvFile(filePath, leadList);

						// Import the CSV file
						Import importlead = new Import();
						ImportTest importTest = new ImportTest(driver);

						// Navigate to Import Tab
						fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

						importlead.setImportType("Leads");
						importlead.setSpecifyFileFormat("CSV");
						importlead.setSalesDataFile(filePath);
						importlead.setLeadStatus("New Lead");
						importlead.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
						importlead.setLeadSourceDetails("None");

						importTest.importLeadsCSV(importlead);

						// Validation
						Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

						Lead x = new Lead();
						List<String> leadsFirstNames = map.get("FN");
						List<String> leadsLastNames = map.get("LN");

						for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
							System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
							x.setFirstName(leadsFirstNames.get(a));
							x.setLastName(leadsLastNames.get(b));

							String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
							String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
							System.out
									.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

							// Validation
							if (!corpUsersForDivision.contains(ownerAssignedToLeadx)) {
								System.out.println("Owner should be among ::: " + corpUsersForDivision);
								fc.utobj().throwsException("Owner Assignment - Assign Lead Owner by Lead Source - TC-33943 - Import - FAILED");
							}
						}


			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}

	
// ==============================================================================================================================================================================================================
	
	@Test(priority = 73 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_32-Api", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33942")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33942_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

			try {
				
			// Through API
			// ------------------------------------------------------------------------------------------------------------------------------------------------------------
			for (int j = 0; j <= 0; j++) {
				Lead leadApi = new Lead();
				leadApi.setFirstName("LeadSource_Api_33942");
				leadApi.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				//leadApi.setDivision(addDivision1.getDivisionName()); // BLANK
				leadApi.setCountry("USA");
				// leadApi.setStateProvince("Florida"); // BLANK
				// leadApi.setCounty("Gulf"); // BLANK
				// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
				leadApi.setLeadSourceDetails("None");
				
				common.addLeadThroughWebServices(null, leadApi);
				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(null, leadApi);
				// System.out.println(xmlApi);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println(
						"Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
				// Validation
				// Validate Owner Assignment
				if (!corpUsersForSource2.contains(ownerAssignedToLeadApi)) {
					System.out.println("Owner should be among ::: " + corpUsersForSource2);
					fc.utobj().throwsException(
							"Owner Assignment - Assign Lead Owner by Lead Source - TC-33942 - API - FAILED");
				}
			}
			
		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(null, e, testCaseId);

			/*System.out.println(e.getMessage());
			fc.utobj().throwsException1(e.getMessage());*/				
		}
	}
	
	@Test(priority = 74 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_32-WebForm", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33942")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33942_WebForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			// Through WebForm ----------------------------------------------------------------------------------------------------------------------------------------------------------
						// Create WebForm 
						ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
						ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

						fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
						common.set_ManageWebform(manageWebFormGenerator);
						manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
						manageWebFormGenerator.setLeadSourceDetails("None");
					//	manageWebFormGenerator.setDivision(addDivision1.getDivisionName());
						manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

						// Add Lead - WebForm
						for (int j = 0; j <= 0; j++) {
							Lead leadWebForm = new Lead();
							leadWebForm.setFirstName("LeadSource_WebForm_33942");
							leadWebForm.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
							leadWebForm.setEmail("frantest2017@gmail.com");
							//leadApi.setDivision(addDivision1.getDivisionName()); // BLANK
							leadWebForm.setCountry("USA");
							// leadApi.setStateProvince("Florida"); // BLANK
							// leadApi.setCounty("Gulf"); // BLANK
							// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
							// leadWebForm.setBasedonAssignmentRules("yes");
							leadWebForm.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
							leadWebForm.setLeadSourceDetails("None");
							
							// Add Lead through WebForm
							manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm); 

							// fc.utobj().sleep();
							String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
							String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
							System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>" + ownerAssignedTowebformLead);
							// Validation
							if (!corpUsersForSource2.contains(ownerAssignedTowebformLead)) {
								System.out.println("Owner should be among ::: " + corpUsersForSource2);
								fc.utobj().throwsException(
										"Owner Assignment - Assign Lead Owner by Lead Source - TC-33942 - WebForm - FAILED");
							}
						}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}
	
	@Test(priority = 75 ,dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_32-Import", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33942")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33942_Import() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
			// Create CSV File with Lead Details
			String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_33942.csv";
			System.out.println(filePath);
			List<Lead> leadList = new ArrayList<Lead>();

			for (int j = 0; j <= 0; j++) {
				Lead leadImport = new Lead();
				leadImport.setFirstName("LeadSource_Import_33942");
				leadImport.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadImport.setEmail("frantest2017@gmail.com");
				//leadApi.setDivision(addDivision1.getDivisionName()); // BLANK
				leadImport.setCountry("USA");
				// leadApi.setStateProvince("Florida"); // BLANK
				// leadApi.setCounty("Gulf"); // BLANK
				// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadImport.setBasedonAssignmentRules("yes");
				leadImport.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
				leadImport.setLeadSourceDetails("None");
				
				
				leadList.add(leadImport);
			}

			common.writeCsvFile(filePath, leadList);

			// Import the CSV file
			Import importlead = new Import();
			ImportTest importTest = new ImportTest(driver);

			// Navigate to Import Tab
			fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

			importlead.setImportType("Leads");
			importlead.setSpecifyFileFormat("CSV");
			importlead.setSalesDataFile(filePath);
			importlead.setLeadStatus("New Lead");
			importlead.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
			importlead.setLeadSourceDetails("None");

			importTest.importLeadsCSV(importlead);

			// Validation
			Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

			Lead x = new Lead();
			List<String> leadsFirstNames = map.get("FN");
			List<String> leadsLastNames = map.get("LN");

			for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
				System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
				x.setFirstName(leadsFirstNames.get(a));
				x.setLastName(leadsLastNames.get(b));

				String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
				String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
				System.out
						.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

				// Validation
				if (!corpUsersForSource2.contains(ownerAssignedToLeadx)) {
					System.out.println("Owner should be among ::: " + corpUsersForSource2);
					fc.utobj().throwsException(
							"Owner Assignment - Assign Lead Owner by Lead Source - TC-33942 - Import - FAILED");
				}
			}

			

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}
	
// =============================================================================================================================================================================================================	

	@Test(priority = 76 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_33-Api", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33941")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33941_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

			try {
								 
			// Through API
			// ------------------------------------------------------------------------------------------------------------------------------------------------------------
			for (int j = 0; j <= 0; j++) {
				Lead leadApi = new Lead();
				leadApi.setFirstName("LeadSource_Api_33941");
				leadApi.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				// leadApi.setDivision(addDivision1.getDivisionName()); // BLANK
				leadApi.setCountry("USA");
				// leadApi.setStateProvince("Florida"); // BLANK
				leadApi.setCounty("Gulf"); // BLANK
				// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
				leadApi.setLeadSourceDetails("None");
				
				common.addLeadThroughWebServices(null, leadApi);
				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(null, leadApi);
				// System.out.println(xmlApi);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println(
						"Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
				// Validation
				// Validate Owner Assignment
				if (!corpUsersForSource2.contains(ownerAssignedToLeadApi)) {
					System.out.println("Owner should be among ::: " + corpUsersForSource2);
					fc.utobj().throwsException(
							"Owner Assignment - Assign Lead Owner by Lead Source - TC-33941 - API - FAILED");
				}
			}
			

		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(null, e, testCaseId);

			/*System.out.println(e.getMessage());
			fc.utobj().throwsException1(e.getMessage());*/				
		}
	}
	
	@Test(priority = 77 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_33-WebForm", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33941")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33941_WebForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			// Through WebForm
			// ----------------------------------------------------------------------------------------------------------------------------------------------------------
			// Create WebForm
			ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
			ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

			fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
			common.set_ManageWebform(manageWebFormGenerator);
			manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
			manageWebFormGenerator.setLeadSourceDetails("None");
			// manageWebFormGenerator.setDivision(addDivision1.getDivisionName());
			manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

			// Add Lead - WebForm
			for (int j = 0; j <= 0; j++) {
				Lead leadWebForm = new Lead();
				leadWebForm.setFirstName("LeadSource_WebForm_33941");
				leadWebForm.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadWebForm.setEmail("frantest2017@gmail.com");
				// leadApi.setDivision(addDivision1.getDivisionName()); // BLANK
				leadWebForm.setCountry("USA");
				// leadApi.setStateProvince("Florida"); // BLANK
				leadWebForm.setCounty("Gulf"); // BLANK
				// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				// leadWebForm.setBasedonAssignmentRules("yes");
				leadWebForm.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
				leadWebForm.setLeadSourceDetails("None");

				// Add Lead through WebForm
				manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm);

				// fc.utobj().sleep();
				String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
				String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
				System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>"
						+ ownerAssignedTowebformLead);
				// Validation
				if (!corpUsersForSource2.contains(ownerAssignedTowebformLead)) {
					System.out.println("Owner should be among ::: " + corpUsersForSource2);
					fc.utobj().throwsException(
							"Owner Assignment - Assign Lead Owner by Lead Source - TC-33941 - WebForm - FAILED");
				}
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}
	
	@Test(priority = 78 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_33-Import", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33941")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33941_Import() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
						// Create CSV File with Lead Details
						String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_33941.csv";
						System.out.println(filePath);
						List<Lead> leadList = new ArrayList<Lead>();

						for (int j = 0; j <= 0; j++) {
							Lead leadImport = new Lead();
							leadImport.setFirstName("LeadSource_Import_33941");
							leadImport.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
							leadImport.setEmail("frantest2017@gmail.com");
							// leadApi.setDivision(addDivision1.getDivisionName()); // BLANK
							leadImport.setCountry("USA");
							// leadApi.setStateProvince("Florida"); // BLANK
							leadImport.setCounty("Gulf"); // BLANK
							// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
							leadImport.setBasedonAssignmentRules("yes");
							leadImport.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
							leadImport.setLeadSourceDetails("None");
							
							
							leadList.add(leadImport);
						}

						common.writeCsvFile(filePath, leadList);

						// Import the CSV file
						Import importlead = new Import();
						ImportTest importTest = new ImportTest(driver);

						// Navigate to Import Tab
						fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

						importlead.setImportType("Leads");
						importlead.setSpecifyFileFormat("CSV");
						importlead.setSalesDataFile(filePath);
						importlead.setLeadStatus("New Lead");
						importlead.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
						importlead.setLeadSourceDetails("None");

						importTest.importLeadsCSV(importlead);

						// Validation
						Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

						Lead x = new Lead();
						List<String> leadsFirstNames = map.get("FN");
						List<String> leadsLastNames = map.get("LN");

						for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
							System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
							x.setFirstName(leadsFirstNames.get(a));
							x.setLastName(leadsLastNames.get(b));

							String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
							String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
							System.out
									.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

							// Validation
							if (!corpUsersForSource2.contains(ownerAssignedToLeadx)) {
								System.out.println("Owner should be among ::: " + corpUsersForSource2);
								fc.utobj().throwsException(
										"Owner Assignment - Assign Lead Owner by Lead Source - TC-33941 - Import - FAILED");
							}
						}

							

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}


// ==============================================================================================================================================================================================================
	
	@Test(priority = 79 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_34-Api", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33940")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33940_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		try {
				
			// Through API
			// ------------------------------------------------------------------------------------------------------------------------------------------------------------
			for (int j = 0; j <= 0; j++) {
				Lead leadApi = new Lead();
				leadApi.setFirstName("LeadSource_Api_33940");
				leadApi.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				leadApi.setDivision(addDivision2.getDivisionName()); // BLANK
				leadApi.setCountry("USA");
				// leadApi.setStateProvince("Florida"); // BLANK
				leadApi.setCounty("Gulf"); // BLANK
				// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
				leadApi.setLeadSourceDetails("None");
				
				common.addLeadThroughWebServices(null, leadApi);
				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(null, leadApi);
				// System.out.println(xmlApi);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println(
						"Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
				// Validation
				// Validate Owner Assignment
				if (!corpUsersForDivision.contains(ownerAssignedToLeadApi)) {
					System.out.println("Owner should be among ::: " + corpUsersForDivision);
					fc.utobj().throwsException(
							"Owner Assignment - Assign Lead Owner by Lead Source - TC-33940 - API - FAILED");
				}
			}
			
		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(null, e, testCaseId);

			/*System.out.println(e.getMessage());
			fc.utobj().throwsException1(e.getMessage());*/				
		}
	}
	
	@Test(priority = 80 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_34-WebForm", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33940")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33940_WebForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			// Through WebForm
						// ----------------------------------------------------------------------------------------------------------------------------------------------------------
						// Create WebForm
						ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
						ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

						fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
						common.set_ManageWebform(manageWebFormGenerator);
						manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
						manageWebFormGenerator.setLeadSourceDetails("None");
						manageWebFormGenerator.setDivision(addDivision2.getDivisionName());
						manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

						// Add Lead - WebForm
						for (int j = 0; j <= 0; j++) {
							Lead leadWebForm = new Lead();
							leadWebForm.setFirstName("LeadSource_WebForm_33940");
							leadWebForm.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
							leadWebForm.setEmail("frantest2017@gmail.com");
					//		leadWebForm.setDivision(addDivision1.getDivisionName()); // BLANK
							leadWebForm.setCountry("USA");
							// leadApi.setStateProvince("Florida"); // BLANK
							leadWebForm.setCounty("Gulf"); // BLANK
							// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
							// leadWebForm.setBasedonAssignmentRules("yes");
							leadWebForm.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
							leadWebForm.setLeadSourceDetails("None");

							// Add Lead through WebForm
							manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm);

							// fc.utobj().sleep();
							String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
							String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
							System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>"
									+ ownerAssignedTowebformLead);
							// Validation
							if (!corpUsersForDivision.contains(ownerAssignedTowebformLead)) {
								System.out.println("Owner should be among ::: " + corpUsersForDivision);
								fc.utobj().throwsException(
										"Owner Assignment - Assign Lead Owner by Lead Source - TC-33940 - WebForm - FAILED");
							}
						}


			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}
	
	@Test(priority = 81 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_34-Import", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33940")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33940_Import() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
			// Create CSV File with Lead Details
			String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_33940.csv";
			System.out.println(filePath);
			List<Lead> leadList = new ArrayList<Lead>();

			for (int j = 0; j <= 0; j++) {
				Lead leadImport = new Lead();
				leadImport.setFirstName("LeadSource_Import_33940");
				leadImport.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadImport.setEmail("frantest2017@gmail.com");
				leadImport.setDivision(addDivision2.getDivisionName()); // BLANK
				leadImport.setCountry("USA");
				// leadApi.setStateProvince("Florida"); // BLANK
				leadImport.setCounty("Gulf"); // BLANK
				// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadImport.setBasedonAssignmentRules("yes");
				leadImport.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
				leadImport.setLeadSourceDetails("None");
				
				
				leadList.add(leadImport);
			}

			common.writeCsvFile(filePath, leadList);

			// Import the CSV file
			Import importlead = new Import();
			ImportTest importTest = new ImportTest(driver);

			// Navigate to Import Tab
			fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

			importlead.setImportType("Leads");
			importlead.setSpecifyFileFormat("CSV");
			importlead.setSalesDataFile(filePath);
			importlead.setLeadStatus("New Lead");
			importlead.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
			importlead.setLeadSourceDetails("None");

			importTest.importLeadsCSV(importlead);

			// Validation
			Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

			Lead x = new Lead();
			List<String> leadsFirstNames = map.get("FN");
			List<String> leadsLastNames = map.get("LN");

			for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
				System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
				x.setFirstName(leadsFirstNames.get(a));
				x.setLastName(leadsLastNames.get(b));

				String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
				String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
				System.out
						.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

				// Validation
				if (!corpUsersForDivision.contains(ownerAssignedToLeadx)) {
					System.out.println("Owner should be among ::: " + corpUsersForDivision);
					fc.utobj().throwsException(
							"Owner Assignment - Assign Lead Owner by Lead Source - TC-33940 - Import - FAILED");
				}
			}
				
			

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}

// ==============================================================================================================================================================================================================
	
	
	@Test(priority = 82 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_35-Api", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33939")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33939_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

			try {
	
			// Through API
			// ------------------------------------------------------------------------------------------------------------------------------------------------------------
			for (int j = 0; j <= 0; j++) {
				Lead leadApi = new Lead();
				leadApi.setFirstName("LeadSource_Api_33939");
				leadApi.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				leadApi.setDivision(addDivision2.getDivisionName()); // BLANK
				leadApi.setCountry("USA");
				leadApi.setStateProvince("Florida"); // BLANK
				leadApi.setCounty("Gulf"); // BLANK
				// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
				leadApi.setLeadSourceDetails("None");
				
				common.addLeadThroughWebServices(null, leadApi);
				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(null, leadApi);
				// System.out.println(xmlApi);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println(
						"Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
				// Validation
				// Validate Owner Assignment
				if (!corpUsersForSalesTerritoriesCounty.contains(ownerAssignedToLeadApi)) {
					System.out.println("Owner should be among ::: " + corpUsersForSalesTerritoriesCounty);
					fc.utobj().throwsException(
							"Owner Assignment - Assign Lead Owner by Lead Source - TC-33939 - API - FAILED");
				}
			}
			

		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(null, e, testCaseId);

			/*System.out.println(e.getMessage());
			fc.utobj().throwsException1(e.getMessage());	*/			
		}
	}

	
	@Test(priority = 83 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_35-WebForm", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33939")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33939_WebForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			// Through WebForm
			// ----------------------------------------------------------------------------------------------------------------------------------------------------------
			// Create WebForm
			ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
			ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

			fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
			common.set_ManageWebform(manageWebFormGenerator);
			manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
			manageWebFormGenerator.setLeadSourceDetails("None");
			manageWebFormGenerator.setDivision(addDivision2.getDivisionName());
			manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

			// Add Lead - WebForm
			for (int j = 0; j <= 0; j++) {
				Lead leadWebForm = new Lead();
				leadWebForm.setFirstName("LeadSource_WebForm_33939");
				leadWebForm.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadWebForm.setEmail("frantest2017@gmail.com");
			//	leadWebForm.setDivision(addDivision1.getDivisionName()); // BLANK
				leadWebForm.setCountry("USA");
				leadWebForm.setStateProvince("Florida"); // BLANK
				leadWebForm.setCounty("Gulf"); // BLANK
				// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				// leadWebForm.setBasedonAssignmentRules("yes");
				leadWebForm.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
				leadWebForm.setLeadSourceDetails("None");

				// Add Lead through WebForm
				manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm);

				// fc.utobj().sleep();
				String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
				String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
				System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>"
						+ ownerAssignedTowebformLead);
				// Validation
				if (!corpUsersForSalesTerritoriesCounty.contains(ownerAssignedTowebformLead)) {
					System.out.println("Owner should be among ::: " + corpUsersForSalesTerritoriesCounty);
					fc.utobj().throwsException(
							"Owner Assignment - Assign Lead Owner by Lead Source - TC-33939 - WebForm - FAILED");
				}
			}

			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}
	
	@Test(priority = 84 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" /*, "sales_OwnerAssignment_Failed"*/ })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_35-Import", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33939")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33939_Import() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
						// Create CSV File with Lead Details
						String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_33939.csv";
						System.out.println(filePath);
						List<Lead> leadList = new ArrayList<Lead>();

						for (int j = 0; j <= 0; j++) {
							Lead leadImport = new Lead();
							leadImport.setFirstName("LeadSource_Import_33939");
							leadImport.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
							leadImport.setEmail("frantest2017@gmail.com");
							leadImport.setDivision(addDivision2.getDivisionName()); // BLANK
							leadImport.setCountry("USA");
							leadImport.setStateProvince("Florida"); // BLANK
							leadImport.setCounty("Gulf"); // BLANK
							// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
							leadImport.setBasedonAssignmentRules("yes");
							leadImport.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
							leadImport.setLeadSourceDetails("None");
							
							
							leadList.add(leadImport);
						}

						common.writeCsvFile(filePath, leadList);

						// Import the CSV file
						Import importlead = new Import();
						ImportTest importTest = new ImportTest(driver);

						// Navigate to Import Tab
						fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

						importlead.setImportType("Leads");
						importlead.setSpecifyFileFormat("CSV");
						importlead.setSalesDataFile(filePath);
						importlead.setLeadStatus("New Lead");
						importlead.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
						importlead.setLeadSourceDetails("None");

						importTest.importLeadsCSV(importlead);

						// Validation
						Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

						Lead x = new Lead();
						List<String> leadsFirstNames = map.get("FN");
						List<String> leadsLastNames = map.get("LN");

						for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
							System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
							x.setFirstName(leadsFirstNames.get(a));
							x.setLastName(leadsLastNames.get(b));

							String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
							String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
							System.out
									.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

							// Validation
							if (!corpUsersForDivision.contains(ownerAssignedToLeadx)) {
								System.out.println("Owner should be among ::: " + corpUsersForDivision);
								fc.utobj().throwsException(
										"Owner Assignment - Assign Lead Owner by Lead Source - TC-33939 - Import - FAILED");
							}
						}

						
							
			

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}

	
// =============================================================================================================================================================================================================
	
	

	@Test(priority = 85 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_36-Api", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33938")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33938_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

			try {
				
			// Through API
			// ------------------------------------------------------------------------------------------------------------------------------------------------------------
			for (int j = 0; j <= 0; j++) {
				Lead leadApi = new Lead();
				leadApi.setFirstName("LeadSource_Api_33938");
				leadApi.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				leadApi.setDivision(addDivision2.getDivisionName()); // BLANK
				leadApi.setCountry("USA");
				// leadApi.setStateProvince("Florida"); // BLANK
				leadApi.setCounty("Gulf"); // BLANK
				leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
				leadApi.setLeadSourceDetails("None");
				
				common.addLeadThroughWebServices(null, leadApi);
				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(null, leadApi);
				// System.out.println(xmlApi);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println(
						"Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
				// Validation
				// Validate Owner Assignment
				if (!corpUsersForSalesTerritoriesZip.contains(ownerAssignedToLeadApi)) {
					System.out.println("Owner should be among ::: " + corpUsersForSalesTerritoriesZip);
					fc.utobj().throwsException(
							"Owner Assignment - Assign Lead Owner by Lead Source - TC-33938 - API - FAILED");
				}
			}
			
			fc.utobj().logoutAndQuitBrowser(null, testCaseId);

		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(null, e, testCaseId);

			/*System.out.println(e.getMessage());
			fc.utobj().throwsException1(e.getMessage());*/				
		}
	}
	
	@Test(priority = 86 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_36-WebForm", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33938")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33938_WebForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			// Through WebForm
						// ----------------------------------------------------------------------------------------------------------------------------------------------------------
						// Create WebForm
						ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
						ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

						fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
						common.set_ManageWebform(manageWebFormGenerator);
						manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
						manageWebFormGenerator.setLeadSourceDetails("None");
						manageWebFormGenerator.setDivision(addDivision2.getDivisionName());
						manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

						// Add Lead - WebForm
						for (int j = 0; j <= 0; j++) {
							Lead leadWebForm = new Lead();
							leadWebForm.setFirstName("LeadSource_WebForm_33938");
							leadWebForm.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
							leadWebForm.setEmail("frantest2017@gmail.com");
						//	leadWebForm.setDivision(addDivision1.getDivisionName()); // BLANK
							leadWebForm.setCountry("USA");
							// leadApi.setStateProvince("Florida"); // BLANK
							leadWebForm.setCounty("Gulf"); // BLANK
							leadWebForm.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
							// leadWebForm.setBasedonAssignmentRules("yes");
							leadWebForm.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
							leadWebForm.setLeadSourceDetails("None");

							// Add Lead through WebForm
							manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm);

							// fc.utobj().sleep();
							String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
							String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
							System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>"
									+ ownerAssignedTowebformLead);
							// Validation
							if (!corpUsersForSalesTerritoriesZip.contains(ownerAssignedTowebformLead)) {
								fc.utobj().throwsException(
										"Owner Assignment - Assign Lead Owner by Lead Source - TC-33938 - WebForm - FAILED");
							}
						}
						

						
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}

	
	@Test(priority = 87 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_36-Import", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33938")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33938_Import() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
			// Create CSV File with Lead Details
			String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_33938.csv";
			System.out.println(filePath);
			List<Lead> leadList = new ArrayList<Lead>();

			for (int j = 0; j <= 0; j++) {
				Lead leadImport = new Lead();
				leadImport.setFirstName("LeadSource_Import_33938");
				leadImport.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadImport.setEmail("frantest2017@gmail.com");
				leadImport.setDivision(addDivision2.getDivisionName()); // BLANK
				leadImport.setCountry("USA");
				// leadApi.setStateProvince("Florida"); // BLANK
				leadImport.setCounty("Gulf"); // BLANK
				leadImport.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadImport.setBasedonAssignmentRules("yes");
				leadImport.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
				leadImport.setLeadSourceDetails("None");
				
				
				leadList.add(leadImport);
			}

			common.writeCsvFile(filePath, leadList);

			// Import the CSV file
			Import importlead = new Import();
			ImportTest importTest = new ImportTest(driver);

			// Navigate to Import Tab
			fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

			importlead.setImportType("Leads");
			importlead.setSpecifyFileFormat("CSV");
			importlead.setSalesDataFile(filePath);
			importlead.setLeadStatus("New Lead");
			importlead.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
			importlead.setLeadSourceDetails("None");

			importTest.importLeadsCSV(importlead);

			// Validation
			Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

			Lead x = new Lead();
			List<String> leadsFirstNames = map.get("FN");
			List<String> leadsLastNames = map.get("LN");

			for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
				System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
				x.setFirstName(leadsFirstNames.get(a));
				x.setLastName(leadsLastNames.get(b));

				String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
				String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
				System.out
						.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

				// Validation
				if (!corpUsersForSalesTerritoriesZip.contains(ownerAssignedToLeadx)) {
					fc.utobj().throwsException(
							"Owner Assignment - Assign Lead Owner by Lead Source - TC-33938 - Import - FAILED");
				}
			}

			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}

	
// ==============================================================================================================================================================================================================
	
	@Test(priority = 88 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_37-Api", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33937")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33937_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

			try {
								 
			// Through API
			// ------------------------------------------------------------------------------------------------------------------------------------------------------------
			for (int j = 0; j <= 0; j++) {
				Lead leadApi = new Lead();
				leadApi.setFirstName("LeadSource_Api_33937");
				leadApi.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				// leadApi.setDivision(addDivision1.getDivisionName()); // BLANK
				leadApi.setCountry("USA");
				leadApi.setStateProvince("Florida"); // BLANK
				leadApi.setCounty("Gulf"); // BLANK
				leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
				leadApi.setLeadSourceDetails("None");
				
				common.addLeadThroughWebServices(null, leadApi);
				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(null, leadApi);
				// System.out.println(xmlApi);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println(
						"Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
				// Validation
				// Validate Owner Assignment
				if (!corpUsersForSalesTerritoriesZip.contains(ownerAssignedToLeadApi)) {
					System.out.println("Owner should be among ::: " + corpUsersForSalesTerritoriesZip);
					fc.utobj().throwsException(
							"Owner Assignment - Assign Lead Owner by Lead Source - TC-33937 - API - FAILED");
				}
			}
			
			fc.utobj().logoutAndQuitBrowser(null, testCaseId);
			
		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(null, e, testCaseId);

			/*System.out.println(e.getMessage());
			fc.utobj().throwsException1(e.getMessage());	*/			
		}
	}
	
	@Test(priority = 89 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_37-WebForm", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33937")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33937_WebForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			// Through WebForm
			// ----------------------------------------------------------------------------------------------------------------------------------------------------------
			// Create WebForm
			ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
			ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

			fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
			common.set_ManageWebform(manageWebFormGenerator);
			manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
			manageWebFormGenerator.setLeadSourceDetails("None");
			// manageWebFormGenerator.setDivision(addDivision1.getDivisionName());
			manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

			// Add Lead - WebForm
			for (int j = 0; j <= 0; j++) {
				Lead leadWebForm = new Lead();
				leadWebForm.setFirstName("LeadSource_WebForm_33937");
				leadWebForm.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadWebForm.setEmail("frantest2017@gmail.com");
				// leadApi.setDivision(addDivision1.getDivisionName()); // BLANK
				leadWebForm.setCountry("USA");
				leadWebForm.setStateProvince("Florida"); // BLANK
				leadWebForm.setCounty("Gulf"); // BLANK
				leadWebForm.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				// leadWebForm.setBasedonAssignmentRules("yes");
				leadWebForm.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
				leadWebForm.setLeadSourceDetails("None");

				// Add Lead through WebForm
				manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm);

				// fc.utobj().sleep();
				String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
				String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
				System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>"
						+ ownerAssignedTowebformLead);
				// Validation
				if (!corpUsersForSalesTerritoriesZip.contains(ownerAssignedTowebformLead)) {
					System.out.println("Owner should be among ::: " + corpUsersForSalesTerritoriesZip);
					fc.utobj().throwsException(
							"Owner Assignment - Assign Lead Owner by Lead Source - TC-33937 - WebForm - FAILED");
				}
			}
			


			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}
	
	@Test(priority = 90 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_37-Import", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33937")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33937_Import() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
						// Create CSV File with Lead Details
						String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_33937.csv";
						System.out.println(filePath);
						List<Lead> leadList = new ArrayList<Lead>();

						for (int j = 0; j <= 0; j++) {
							Lead leadImport = new Lead();
							leadImport.setFirstName("LeadSource_Import_33937");
							leadImport.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
							leadImport.setEmail("frantest2017@gmail.com");
							// leadApi.setDivision(addDivision1.getDivisionName()); // BLANK
							leadImport.setCountry("USA");
							leadImport.setStateProvince("Florida"); // BLANK
							leadImport.setCounty("Gulf"); // BLANK
							leadImport.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
							leadImport.setBasedonAssignmentRules("yes");
							leadImport.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
							leadImport.setLeadSourceDetails("None");
							
							
							leadList.add(leadImport);
						}

						common.writeCsvFile(filePath, leadList);

						// Import the CSV file
						Import importlead = new Import();
						ImportTest importTest = new ImportTest(driver);

						// Navigate to Import Tab
						fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

						importlead.setImportType("Leads");
						importlead.setSpecifyFileFormat("CSV");
						importlead.setSalesDataFile(filePath);
						importlead.setLeadStatus("New Lead");
						importlead.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
						importlead.setLeadSourceDetails("None");

						importTest.importLeadsCSV(importlead);

						// Validation
						Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

						Lead x = new Lead();
						List<String> leadsFirstNames = map.get("FN");
						List<String> leadsLastNames = map.get("LN");

						for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
							System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
							x.setFirstName(leadsFirstNames.get(a));
							x.setLastName(leadsLastNames.get(b));

							String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
							String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
							System.out
									.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

							// Validation
							if (!corpUsersForSalesTerritoriesZip.contains(ownerAssignedToLeadx)) {
								System.out.println("Owner should be among ::: " + corpUsersForSalesTerritoriesZip);
								fc.utobj().throwsException(
										"Owner Assignment - Assign Lead Owner by Lead Source - TC-33937 - Import - FAILED");
							}
						}
						

			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}

// ==============================================================================================================================================================================================================
	
	@Test(priority = 91 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_38-Api", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33936")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33936_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

			try {
				
			// Through API
			// ------------------------------------------------------------------------------------------------------------------------------------------------------------
			for (int j = 0; j <= 0; j++) {
				Lead leadApi = new Lead();
				leadApi.setFirstName("LeadSource_Api_33936");
				leadApi.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				leadApi.setDivision(addDivision2.getDivisionName()); // BLANK
				leadApi.setCountry("USA");
				leadApi.setStateProvince("Florida"); // BLANK
				leadApi.setCounty("Gulf"); // BLANK
				leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
				leadApi.setLeadSourceDetails("None");
				
				common.addLeadThroughWebServices(null, leadApi);
				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(null, leadApi);
				// System.out.println(xmlApi);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println(
						"Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
				// Validation
				// Validate Owner Assignment
				if (!corpUsersForSalesTerritoriesZip.contains(ownerAssignedToLeadApi)) {
					System.out.println("Owner should be among ::: " + corpUsersForSalesTerritoriesZip);
					fc.utobj().throwsException(
							"Owner Assignment - Assign Lead Owner by Lead Source - TC-33936 - API - FAILED");
				}
			}
			
			fc.utobj().logoutAndQuitBrowser(null, testCaseId);
			
		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(null, e, testCaseId);

			/*System.out.println(e.getMessage());
			fc.utobj().throwsException1(e.getMessage());	*/
		}
	}
	
	
	@Test(priority = 92 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_38-WebForm", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33936")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33936_WebForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			// Through WebForm
						// ----------------------------------------------------------------------------------------------------------------------------------------------------------
						// Create WebForm
						ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
						ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

						fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
						common.set_ManageWebform(manageWebFormGenerator);
						manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
						manageWebFormGenerator.setLeadSourceDetails("None");
						manageWebFormGenerator.setDivision(addDivision2.getDivisionName());
						manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

						// Add Lead - WebForm
						for (int j = 0; j <= 0; j++) {
							Lead leadWebForm = new Lead();
							leadWebForm.setFirstName("LeadSource_WebForm_33936");
							leadWebForm.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
							leadWebForm.setEmail("frantest2017@gmail.com");
					//		leadWebForm.setDivision(addDivision1.getDivisionName()); // BLANK
							leadWebForm.setCountry("USA");
							leadWebForm.setStateProvince("Florida"); // BLANK
							leadWebForm.setCounty("Gulf"); // BLANK
							leadWebForm.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
							// leadWebForm.setBasedonAssignmentRules("yes");
							leadWebForm.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
							leadWebForm.setLeadSourceDetails("None");
							
							// Add Lead through WebForm
							manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm);

							// fc.utobj().sleep();
							String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
							String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
							System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>"
									+ ownerAssignedTowebformLead);
							// Validation
							if (!corpUsersForSalesTerritoriesZip.contains(ownerAssignedTowebformLead)) {
								System.out.println("Owner should be among ::: " + corpUsersForSalesTerritoriesZip);
								fc.utobj().throwsException(
										"Owner Assignment - Assign Lead Owner by Lead Source - TC-33936 - WebForm - FAILED");
							}
						}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}
		
	@Test(priority = 93 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_38-Import", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33936")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33936_Import() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);
			
			// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
			// Create CSV File with Lead Details
			String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_33936.csv";
			System.out.println(filePath);
			List<Lead> leadList = new ArrayList<Lead>();

			for (int j = 0; j <= 0; j++) {
				Lead leadImport = new Lead();
				leadImport.setFirstName("LeadSource_Import_33936");
				leadImport.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadImport.setEmail("frantest2017@gmail.com");
				leadImport.setDivision(addDivision2.getDivisionName()); // BLANK
				leadImport.setCountry("USA");
				leadImport.setStateProvince("Florida"); // BLANK
				leadImport.setCounty("Gulf"); // BLANK
				leadImport.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadImport.setBasedonAssignmentRules("yes");
				leadImport.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
				leadImport.setLeadSourceDetails("None");
				
				
				leadList.add(leadImport);
			}

			common.writeCsvFile(filePath, leadList);

			// Import the CSV file
			Import importlead = new Import();
			ImportTest importTest = new ImportTest(driver);

			// Navigate to Import Tab
			fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

			importlead.setImportType("Leads");
			importlead.setSpecifyFileFormat("CSV");
			importlead.setSalesDataFile(filePath);
			importlead.setLeadStatus("New Lead");
			importlead.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
			importlead.setLeadSourceDetails("None");

			importTest.importLeadsCSV(importlead);

			// Validation
			Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

			Lead x = new Lead();
			List<String> leadsFirstNames = map.get("FN");
			List<String> leadsLastNames = map.get("LN");

			for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
				System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
				x.setFirstName(leadsFirstNames.get(a));
				x.setLastName(leadsLastNames.get(b));

				String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
				String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
				System.out
						.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

				// Validation
				if (!corpUsersForSalesTerritoriesZip.contains(ownerAssignedToLeadx)) {
					System.out.println("Owner should be among ::: " + corpUsersForSalesTerritoriesZip);
					fc.utobj().throwsException(
							"Owner Assignment - Assign Lead Owner by Lead Source - TC-33936 - Import - FAILED");
				}
			}
			
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}

// ==============================================================================================================================================================================================================	

	@Test(priority = 94 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_39-Api", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33935")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33935_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

			try {
				
			// Through API
			// ------------------------------------------------------------------------------------------------------------------------------------------------------------
			for (int j = 0; j <= 0; j++) {
				Lead leadApi = new Lead();
				leadApi.setFirstName("LeadSource_Api_33935");
				leadApi.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				// leadApi.setDivision(addDivision1.getDivisionName()); // BLANK
				leadApi.setCountry("USA");
				// leadApi.setStateProvince("Florida"); // BLANK
				// leadApi.setCounty("Gulf"); // BLANK
				// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory("Friends");
				leadApi.setLeadSourceDetails("Friends");
				
				common.addLeadThroughWebServices(null, leadApi);
				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(null, leadApi);
				// System.out.println(xmlApi);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println(
						"Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
				// Validation
				// Validate Owner Assignment
				if (!ownerAssignedToLeadApi.equalsIgnoreCase("FranConnect Administrator")) {
					System.out.println("Owner should be ::: FranConnect Administrator");
					fc.utobj().throwsException("Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33935 - API - FAILED");
				}
			}
			

			fc.utobj().logoutAndQuitBrowser(null, testCaseId);

		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(null, e, testCaseId);

			/*System.out.println(e.getMessage());
			fc.utobj().throwsException1(e.getMessage());*/				
		}
	}
	
	
	@Test(priority = 95 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_39-WebForm", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33935")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33935_WebForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			// Through WebForm
			// ----------------------------------------------------------------------------------------------------------------------------------------------------------
			// Create WebForm
			ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
			ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

			fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
			common.set_ManageWebform(manageWebFormGenerator);
			manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
			manageWebFormGenerator.setLeadSourceDetails("None");
		//	manageWebFormGenerator.setDivision(addDivision1.getDivisionName());
			manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

			// Add Lead - WebForm
			for (int j = 0; j <= 0; j++) {
				Lead leadWebForm = new Lead();
				leadWebForm.setFirstName("LeadSource_WebForm_33935");
				leadWebForm.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadWebForm.setEmail("frantest2017@gmail.com");
				// leadApi.setDivision(addDivision1.getDivisionName()); // BLANK
				leadWebForm.setCountry("USA");
				// leadApi.setStateProvince("Florida"); // BLANK
				// leadApi.setCounty("Gulf"); // BLANK
				// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				// leadWebForm.setBasedonAssignmentRules("yes");
				leadWebForm.setLeadSourceCategory("Friends");
				leadWebForm.setLeadSourceDetails("Friends");
				
				// Add Lead through WebForm
				manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm);

				// fc.utobj().sleep();
				String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
				String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
				System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>"
						+ ownerAssignedTowebformLead);
				// Validation
				if (!ownerAssignedTowebformLead.equalsIgnoreCase("FranConnect Administrator")) {
					System.out.println("Owner should be ::: FranConnect Administrator");
					fc.utobj().throwsException("Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33935 - WebForm - FAILED");
				}
			}
			

			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}
	
	@Test(priority = 96 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_39-Import", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33935")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33935_Import() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			
			// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
			// Create CSV File with Lead Details
			String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_33935.csv";
			System.out.println(filePath);
			List<Lead> leadList = new ArrayList<Lead>();

			for (int j = 0; j <= 0; j++) {
				Lead leadImport = new Lead();
				leadImport.setFirstName("LeadSource_Import_33935");
				leadImport.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadImport.setEmail("frantest2017@gmail.com");
				// leadApi.setDivision(addDivision1.getDivisionName()); // BLANK
				leadImport.setCountry("USA");
				// leadApi.setStateProvince("Florida"); // BLANK
				// leadApi.setCounty("Gulf"); // BLANK
				// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadImport.setBasedonAssignmentRules("yes");
				leadImport.setLeadSourceCategory("Friends");
				leadImport.setLeadSourceDetails("Friends");
				
				
				leadList.add(leadImport);
			}

			common.writeCsvFile(filePath, leadList);

			// Import the CSV file
			Import importlead = new Import();
			ImportTest importTest = new ImportTest(driver);

			// Navigate to Import Tab
			fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

			importlead.setImportType("Leads");
			importlead.setSpecifyFileFormat("CSV");
			importlead.setSalesDataFile(filePath);
			importlead.setLeadStatus("New Lead");
			importlead.setLeadSourceCategory("Friends");
			importlead.setLeadSourceDetails("Friends");

			importTest.importLeadsCSV(importlead);

			// Validation
			Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

			Lead x = new Lead();
			List<String> leadsFirstNames = map.get("FN");
			List<String> leadsLastNames = map.get("LN");

			for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
				System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
				x.setFirstName(leadsFirstNames.get(a));
				x.setLastName(leadsLastNames.get(b));

				String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
				String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
				System.out
						.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

				// Validation
				if (!ownerAssignedToLeadx.equalsIgnoreCase("FranConnect Administrator")) {
					System.out.println("Owner should be ::: FranConnect Administrator");
					fc.utobj().throwsException("Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33935 - Import - FAILED");
				}
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}

// ==============================================================================================================================================================================================================
	
	@Test(priority = 97 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_40-Api", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33934")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33934_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

			try {
				
			// Through API
			// ------------------------------------------------------------------------------------------------------------------------------------------------------------
			for (int j = 0; j <= 0; j++) {
				Lead leadApi = new Lead();
				leadApi.setFirstName("LeadSource_Api_33934");
				leadApi.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				// leadApi.setDivision(addDivision1.getDivisionName()); // BLANK
				leadApi.setCountry("USA");
				leadApi.setStateProvince("Florida"); // BLANK
				// leadApi.setCounty("Gulf"); // BLANK
				leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory("Friends");
				leadApi.setLeadSourceDetails("Friends");
				
				common.addLeadThroughWebServices(null, leadApi);
				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(null, leadApi);
				// System.out.println(xmlApi);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println(
						"Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
				// Validation
				// Validate Owner Assignment
				if (!ownerAssignedToLeadApi.equalsIgnoreCase("FranConnect Administrator")) {
					System.out.println("Owner should be ::: FranConnect Administrator");
					fc.utobj().throwsException("Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33934 - API - FAILED");
				}
			}
			
			fc.utobj().logoutAndQuitBrowser(null, testCaseId);
			
		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(null, e, testCaseId);

			/*System.out.println(e.getMessage());
			fc.utobj().throwsException1(e.getMessage());*/				
		}
	}		
	
	@Test(priority = 98 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_40-WebForm", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33934")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33934_WebForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			// Through WebForm
						// ----------------------------------------------------------------------------------------------------------------------------------------------------------
						// Create WebForm
						ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
						ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

						fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
						common.set_ManageWebform(manageWebFormGenerator);
						manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
						manageWebFormGenerator.setLeadSourceDetails("None");
					//	manageWebFormGenerator.setDivision(addDivision1.getDivisionName());
						manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

						// Add Lead - WebForm
						for (int j = 0; j <= 0; j++) {
							Lead leadWebForm = new Lead();
							leadWebForm.setFirstName("LeadSource_WebForm_33934");
							leadWebForm.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
							leadWebForm.setEmail("frantest2017@gmail.com");
							// leadApi.setDivision(addDivision1.getDivisionName()); // BLANK
							leadWebForm.setCountry("USA");
							leadWebForm.setStateProvince("Florida"); // BLANK
							// leadApi.setCounty("Gulf"); // BLANK
							leadWebForm.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
							// leadWebForm.setBasedonAssignmentRules("yes");
							leadWebForm.setLeadSourceCategory("Friends");
							leadWebForm.setLeadSourceDetails("Friends");
							
							// Add Lead through WebForm
							manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm);

							// fc.utobj().sleep();
							String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
							String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
							System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>"
									+ ownerAssignedTowebformLead);
							// Validation
							if (!ownerAssignedTowebformLead.equalsIgnoreCase("FranConnect Administrator")) {

								System.out.println("Owner should be ::: FranConnect Administrator");
								fc.utobj().throwsException("Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33934 - WebForm - FAILED");
							}
						}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}
	
	@Test(priority = 99 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_40-Import", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33934")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33934_Import() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
			// Create CSV File with Lead Details
			String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_33934.csv";
			System.out.println(filePath);
			List<Lead> leadList = new ArrayList<Lead>();

			for (int j = 0; j <= 0; j++) {
				Lead leadImport = new Lead();
				leadImport.setFirstName("LeadSource_Import_33934");
				leadImport.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadImport.setEmail("frantest2017@gmail.com");
				// leadApi.setDivision(addDivision1.getDivisionName()); // BLANK
				leadImport.setCountry("USA");
				leadImport.setStateProvince("Florida"); // BLANK
				// leadApi.setCounty("Gulf"); // BLANK
				leadImport.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadImport.setBasedonAssignmentRules("yes");
				leadImport.setLeadSourceCategory("Friends");
				leadImport.setLeadSourceDetails("Friends");
				
				leadList.add(leadImport);
			}

			common.writeCsvFile(filePath, leadList);

			// Import the CSV file
			Import importlead = new Import();
			ImportTest importTest = new ImportTest(driver);

			// Navigate to Import Tab
			fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

			importlead.setImportType("Leads");
			importlead.setSpecifyFileFormat("CSV");
			importlead.setSalesDataFile(filePath);
			importlead.setLeadStatus("New Lead");
			importlead.setLeadSourceCategory("Friends");
			importlead.setLeadSourceDetails("Friends");

			importTest.importLeadsCSV(importlead);

			// Validation
			Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

			Lead x = new Lead();
			List<String> leadsFirstNames = map.get("FN");
			List<String> leadsLastNames = map.get("LN");

			for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
				System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
				x.setFirstName(leadsFirstNames.get(a));
				x.setLastName(leadsLastNames.get(b));

				String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
				String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
				System.out
						.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

				// Validation
				if (!ownerAssignedToLeadx.equalsIgnoreCase("FranConnect Administrator")) {

					System.out.println("Owner should be ::: FranConnect Administrator");
					fc.utobj().throwsException("Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33934 - Import - FAILED");
				}
			}
				
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}

// ===========================================================================================================================================================================================================

	@Test(priority = 100 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_41-Api", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33933 ")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33933_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

			try {
				
			// Through API
			// ------------------------------------------------------------------------------------------------------------------------------------------------------------
			for (int j = 0; j <= 0; j++) {
				Lead leadApi = new Lead();
				leadApi.setFirstName("LeadSource_Api_33933");
				leadApi.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				// leadApi.setDivision(addDivision1.getDivisionName()); // BLANK
				leadApi.setCountry("USA");
				leadApi.setStateProvince("Florida"); // BLANK
				leadApi.setCounty("Gulf"); // BLANK
				// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory("Friends");
				leadApi.setLeadSourceDetails("Friends");
				
				common.addLeadThroughWebServices(null, leadApi);
				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(null, leadApi);
				// System.out.println(xmlApi);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println(
						"Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
				// Validation
				// Validate Owner Assignment
				if (!ownerAssignedToLeadApi.equalsIgnoreCase("FranConnect Administrator")) {

					System.out.println("Owner should be ::: FranConnect Administrator");
					fc.utobj().throwsException(
							"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33933 - API - FAILED");
				}
			}
			
			fc.utobj().logoutAndQuitBrowser(null, testCaseId);
			
		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(null, e, testCaseId);

			/*System.out.println(e.getMessage());
			fc.utobj().throwsException1(e.getMessage());*/				
		}
	}	
	
	@Test(priority = 101 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_41-WebForm", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33933")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33933_WebForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			// Through WebForm
			// ----------------------------------------------------------------------------------------------------------------------------------------------------------
			// Create WebForm
			ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
			ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

			fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
			common.set_ManageWebform(manageWebFormGenerator);
			manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
			manageWebFormGenerator.setLeadSourceDetails("None");
		//	manageWebFormGenerator.setDivision(addDivision1.getDivisionName());
			manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

			// Add Lead - WebForm
			for (int j = 0; j <= 0; j++) {
				Lead leadWebForm = new Lead();
				leadWebForm.setFirstName("LeadSource_WebForm_33933");
				leadWebForm.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadWebForm.setEmail("frantest2017@gmail.com");
				// leadApi.setDivision(addDivision1.getDivisionName()); // BLANK
				leadWebForm.setCountry("USA");
				leadWebForm.setStateProvince("Florida"); // BLANK
				leadWebForm.setCounty("Gulf"); // BLANK
				// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				// leadWebForm.setBasedonAssignmentRules("yes");
				leadWebForm.setLeadSourceCategory("Friends");
				leadWebForm.setLeadSourceDetails("Friends");
				
				// Add Lead through WebForm
				manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm);

				// fc.utobj().sleep();
				String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
				String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
				System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>"
						+ ownerAssignedTowebformLead);
				// Validation
				if (!ownerAssignedTowebformLead.equalsIgnoreCase("FranConnect Administrator")) {
					System.out.println("Owner should be ::: FranConnect Administrator");
					fc.utobj().throwsException(
							"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33933 - WebForm - FAILED");
				}
			}
			

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}
	
	@Test(priority = 102 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_41-Import", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33933")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33933_Import() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
			// Create CSV File with Lead Details
			String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_33933.csv";
			System.out.println(filePath);
			List<Lead> leadList = new ArrayList<Lead>();

			for (int j = 0; j <= 0; j++) {
				Lead leadImport = new Lead();
				leadImport.setFirstName("LeadSource_Import_33933");
				leadImport.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadImport.setEmail("frantest2017@gmail.com");
				// leadApi.setDivision(addDivision1.getDivisionName()); // BLANK
				leadImport.setCountry("USA");
				leadImport.setStateProvince("Florida"); // BLANK
				leadImport.setCounty("Gulf"); // BLANK
				// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadImport.setBasedonAssignmentRules("yes");
				leadImport.setLeadSourceCategory("Friends");
				leadImport.setLeadSourceDetails("Friends");
				
				leadList.add(leadImport);
			}

			common.writeCsvFile(filePath, leadList);

			// Import the CSV file
			Import importlead = new Import();
			ImportTest importTest = new ImportTest(driver);

			// Navigate to Import Tab
			fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

			importlead.setImportType("Leads");
			importlead.setSpecifyFileFormat("CSV");
			importlead.setSalesDataFile(filePath);
			importlead.setLeadStatus("New Lead");
			importlead.setLeadSourceCategory("Friends");
			importlead.setLeadSourceDetails("Friends");

			importTest.importLeadsCSV(importlead);

			// Validation
			Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

			Lead x = new Lead();
			List<String> leadsFirstNames = map.get("FN");
			List<String> leadsLastNames = map.get("LN");

			for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
				System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
				x.setFirstName(leadsFirstNames.get(a));
				x.setLastName(leadsLastNames.get(b));

				String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
				String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
				System.out
						.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

				// Validation
				if (!ownerAssignedToLeadx.equalsIgnoreCase("FranConnect Administrator")) {
					System.out.println("Owner should be ::: FranConnect Administrator");
					fc.utobj().throwsException(
							"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33933 - Import - FAILED");
				}
			}
			

				
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}
	
// ==============================================================================================================================================================================================================	
		
	@Test(priority = 103 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_42-Api", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33932")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33932_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

			try {
				
			// Through API
			// ------------------------------------------------------------------------------------------------------------------------------------------------------------
			for (int j = 0; j <= 0; j++) {
				Lead leadApi = new Lead();
				leadApi.setFirstName("LeadSource_Api_33932");
				leadApi.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				// leadApi.setDivision(addDivision1.getDivisionName()); // BLANK
				leadApi.setCountry("USA");
				// leadApi.setStateProvince("Florida"); // BLANK
				leadApi.setCounty("Gulf"); // BLANK
				leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory("Friends");
				leadApi.setLeadSourceDetails("Friends");
				
				common.addLeadThroughWebServices(null, leadApi);
				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(null, leadApi);
				// System.out.println(xmlApi);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println(
						"Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
				// Validation
				// Validate Owner Assignment
				if (!ownerAssignedToLeadApi.equalsIgnoreCase("FranConnect Administrator")) {
					System.out.println("Owner should be ::: FranConnect Administrator");
					fc.utobj().throwsException(
							"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33932 - API - FAILED");
				}
			}
			
			fc.utobj().logoutAndQuitBrowser(null, testCaseId);
						
		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(null, e, testCaseId);

			/*System.out.println(e.getMessage());
			fc.utobj().throwsException1(e.getMessage());	*/			
		}
	}
	
	@Test(priority = 104 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_42-WebForm", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33932")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33932_WebForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			// Through WebForm
			// ----------------------------------------------------------------------------------------------------------------------------------------------------------
			// Create WebForm
			ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
			ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

			fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
			common.set_ManageWebform(manageWebFormGenerator);
			manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
			manageWebFormGenerator.setLeadSourceDetails("None");
		//	manageWebFormGenerator.setDivision(addDivision1.getDivisionName());
			manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

			// Add Lead - WebForm
			for (int j = 0; j <= 0; j++) {
				Lead leadWebForm = new Lead();
				leadWebForm.setFirstName("LeadSource_WebForm_33932");
				leadWebForm.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadWebForm.setEmail("frantest2017@gmail.com");
				// leadApi.setDivision(addDivision1.getDivisionName()); // BLANK
				leadWebForm.setCountry("USA");
				// leadApi.setStateProvince("Florida"); // BLANK
				leadWebForm.setCounty("Gulf"); // BLANK
				leadWebForm.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				// leadWebForm.setBasedonAssignmentRules("yes");
				leadWebForm.setLeadSourceCategory("Friends");
				leadWebForm.setLeadSourceDetails("Friends");

				// Add Lead through WebForm
				manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm);

				// fc.utobj().sleep();
				String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
				String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
				System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>"
						+ ownerAssignedTowebformLead);
				// Validation
				if (!ownerAssignedTowebformLead.equalsIgnoreCase("FranConnect Administrator")) {
					System.out.println("Owner should be ::: FranConnect Administrator");
					fc.utobj().throwsException(
							"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33932 - WebForm - FAILED");
				}
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}
	
	@Test(priority = 105 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_42-Import", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33932")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33932_Import() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);
			
			// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
						// Create CSV File with Lead Details
						String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_33932.csv";
						System.out.println(filePath);
						List<Lead> leadList = new ArrayList<Lead>();

						for (int j = 0; j <= 0; j++) {
							Lead leadImport = new Lead();
							leadImport.setFirstName("LeadSource_Import_33932");
							leadImport.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
							leadImport.setEmail("frantest2017@gmail.com");
							// leadApi.setDivision(addDivision1.getDivisionName()); // BLANK
							leadImport.setCountry("USA");
							// leadApi.setStateProvince("Florida"); // BLANK
							leadImport.setCounty("Gulf"); // BLANK
							leadImport.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
							leadImport.setBasedonAssignmentRules("yes");
							leadImport.setLeadSourceCategory("Friends");
							leadImport.setLeadSourceDetails("Friends");
							
							leadList.add(leadImport);
						}

						common.writeCsvFile(filePath, leadList);

						// Import the CSV file
						Import importlead = new Import();
						ImportTest importTest = new ImportTest(driver);

						// Navigate to Import Tab
						fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

						importlead.setImportType("Leads");
						importlead.setSpecifyFileFormat("CSV");
						importlead.setSalesDataFile(filePath);
						importlead.setLeadStatus("New Lead");
						importlead.setLeadSourceCategory("Friends");
						importlead.setLeadSourceDetails("Friends");

						importTest.importLeadsCSV(importlead);

						// Validation
						Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

						Lead x = new Lead();
						List<String> leadsFirstNames = map.get("FN");
						List<String> leadsLastNames = map.get("LN");

						for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
							System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
							x.setFirstName(leadsFirstNames.get(a));
							x.setLastName(leadsLastNames.get(b));

							String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
							String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
							System.out
									.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

							// Validation
							if (!ownerAssignedToLeadx.equalsIgnoreCase("FranConnect Administrator")) {
								System.out.println("Owner should be ::: FranConnect Administrator");
								fc.utobj().throwsException(
										"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33932 - Import - FAILED");
							}
						}
		

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}

// ==============================================================================================================================================================================================================	
	

	@Test(priority = 106 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_43-Api", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33931")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33931_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

			try {
				
			// Through API
			// ------------------------------------------------------------------------------------------------------------------------------------------------------------
			for (int j = 0; j <= 0; j++) {
				Lead leadApi = new Lead();
				leadApi.setFirstName("LeadSource_Api_33931");
				leadApi.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				// leadApi.setDivision(addDivision1.getDivisionName()); // BLANK
				leadApi.setCountry("USA");
				leadApi.setStateProvince("Florida"); // BLANK
				leadApi.setCounty("Gulf"); // BLANK
				leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory("Friends");
				leadApi.setLeadSourceDetails("Friends");
				
				common.addLeadThroughWebServices(null, leadApi);
				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(null, leadApi);
				// System.out.println(xmlApi);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println(
						"Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
				// Validation
				// Validate Owner Assignment
				if (!ownerAssignedToLeadApi.equalsIgnoreCase("FranConnect Administrator")) {
					System.out.println("Owner should be ::: FranConnect Administrator");
					fc.utobj().throwsException(
							"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33931 - API - FAILED");
				}
			}
			
			fc.utobj().logoutAndQuitBrowser(null, testCaseId);
			
		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(null, e, testCaseId);

			/*System.out.println(e.getMessage());
			fc.utobj().throwsException1(e.getMessage());	*/			
		}
	}

	@Test(priority = 107 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_43-WebForm", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33931")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33931_WebForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			// Through WebForm
						// ----------------------------------------------------------------------------------------------------------------------------------------------------------
						// Create WebForm
						ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
						ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

						fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
						common.set_ManageWebform(manageWebFormGenerator);
						manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
						manageWebFormGenerator.setLeadSourceDetails("None");
					//	manageWebFormGenerator.setDivision(addDivision1.getDivisionName());
						manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

						// Add Lead - WebForm
						for (int j = 0; j <= 0; j++) {
							Lead leadWebForm = new Lead();
							leadWebForm.setFirstName("LeadSource_WebForm_33931");
							leadWebForm.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
							leadWebForm.setEmail("frantest2017@gmail.com");
							// leadApi.setDivision(addDivision1.getDivisionName()); // BLANK
							leadWebForm.setCountry("USA");
							leadWebForm.setStateProvince("Florida"); // BLANK
							leadWebForm.setCounty("Gulf"); // BLANK
							leadWebForm.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
							// leadWebForm.setBasedonAssignmentRules("yes");
							leadWebForm.setLeadSourceCategory("Friends");
							leadWebForm.setLeadSourceDetails("Friends");

							// Add Lead through WebForm
							manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm);

							// fc.utobj().sleep();
							String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
							String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
							System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>"
									+ ownerAssignedTowebformLead);
							// Validation
							if (!ownerAssignedTowebformLead.equalsIgnoreCase("FranConnect Administrator")) {
								System.out.println("Owner should be ::: FranConnect Administrator");
								fc.utobj().throwsException(
										"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33931 - WebForm - FAILED");
							}
						}


			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}

	
	@Test(priority = 108 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_43-Import", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33931")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33931_Import() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
			// Create CSV File with Lead Details
			String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_33931.csv";
			System.out.println(filePath);
			List<Lead> leadList = new ArrayList<Lead>();

			for (int j = 0; j <= 0; j++) {
				Lead leadImport = new Lead();
				leadImport.setFirstName("LeadSource_Import_33931");
				leadImport.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadImport.setEmail("frantest2017@gmail.com");
				// leadApi.setDivision(addDivision1.getDivisionName()); // BLANK
				leadImport.setCountry("USA");
				leadImport.setStateProvince("Florida"); // BLANK
				leadImport.setCounty("Gulf"); // BLANK
				leadImport.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				// leadImport.setBasedonAssignmentRules("yes");
				leadImport.setLeadSourceCategory("Friends");
				leadImport.setLeadSourceDetails("Friends");
				
				leadList.add(leadImport);
			}

			common.writeCsvFile(filePath, leadList);

			// Import the CSV file
			Import importlead = new Import();
			ImportTest importTest = new ImportTest(driver);

			// Navigate to Import Tab
			fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

			importlead.setImportType("Leads");
			importlead.setSpecifyFileFormat("CSV");
			importlead.setSalesDataFile(filePath);
			importlead.setLeadStatus("New Lead");
			importlead.setLeadSourceCategory("Friends");
			importlead.setLeadSourceDetails("Friends");

			importTest.importLeadsCSV(importlead);

			// Validation
			Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

			Lead x = new Lead();
			List<String> leadsFirstNames = map.get("FN");
			List<String> leadsLastNames = map.get("LN");

			for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
				System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
				x.setFirstName(leadsFirstNames.get(a));
				x.setLastName(leadsLastNames.get(b));

				String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
				String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
				System.out
						.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

				// Validation
				if (!ownerAssignedToLeadx.equalsIgnoreCase("FranConnect Administrator")) {
					System.out.println("Owner should be ::: FranConnect Administrator");
					fc.utobj().throwsException(
							"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33931 - WebForm - FAILED");
				}
			}

			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}
	
// =============================================================================================================================================================================================================
	
	@Test(priority = 109 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_44-Api", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33930")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33930_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		try {
				
			// Through API
			// ------------------------------------------------------------------------------------------------------------------------------------------------------------
			for (int j = 0; j <= 0; j++) {
				Lead leadApi = new Lead();
				leadApi.setFirstName("LeadSource_Api_33930");
				leadApi.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				// leadApi.setDivision(addDivision1.getDivisionName()); // BLANK
				leadApi.setCountry("USA");
				leadApi.setStateProvince("Florida"); // BLANK
				leadApi.setCounty("Gulf"); // BLANK
				leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory("Friends");
				leadApi.setLeadSourceDetails("Friends");
				
				common.addLeadThroughWebServices(null, leadApi);
				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(null, leadApi);
				// System.out.println(xmlApi);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println(
						"Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
				// Validation
				// Validate Owner Assignment
				// Validation
				if (!ownerAssignedToLeadApi.equalsIgnoreCase("FranConnect Administrator")) {
					System.out.println("Owner should be ::: FranConnect Administrator");
					fc.utobj().throwsException(
							"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33930 - API - FAILED");
				}
			}
			
			fc.utobj().logoutAndQuitBrowser(null, testCaseId);
			
		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(null, e, testCaseId);

			/*System.out.println(e.getMessage());
			fc.utobj().throwsException1(e.getMessage());	*/		
			}
	}
	
	
	@Test(priority = 110 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_44-WebForm", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33930")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33930_WebForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			
			driver = fc.loginpage().login(driver);

			// Through WebForm
			// ----------------------------------------------------------------------------------------------------------------------------------------------------------
			// Create WebForm
			ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
			ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

			fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
			common.set_ManageWebform(manageWebFormGenerator);
			manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
			manageWebFormGenerator.setLeadSourceDetails("None");
			// manageWebFormGenerator.setDivision(addDivision1.getDivisionName());
			manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

			// Add Lead - WebForm
				Lead leadWebForm = new Lead();
				leadWebForm.setFirstName("LeadSource_WebForm_33930");
				leadWebForm.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadWebForm.setEmail("frantest2017@gmail.com");
				// leadApi.setDivision(addDivision1.getDivisionName()); // BLANK
				leadWebForm.setCountry("USA");
				leadWebForm.setStateProvince("Florida"); // BLANK
				leadWebForm.setCounty("Gulf"); // BLANK
				leadWebForm.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				// leadWebForm.setBasedonAssignmentRules("yes");
				leadWebForm.setLeadSourceCategory("Friends");
				leadWebForm.setLeadSourceDetails("Friends");

				// Add Lead through WebForm
				manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm);

				// fc.utobj().sleep();
				String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
				String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
				System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>"
						+ ownerAssignedTowebformLead);
				// Validation
				if (!ownerAssignedTowebformLead.equalsIgnoreCase("FranConnect Administrator")) {
					System.out.println("Owner should be ::: FranConnect Administrator");
					fc.utobj().throwsException(
							"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33930 - WebForm - FAILED");
				}
				
			

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(priority = 111, dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_44-Import", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33930")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33930_Import() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			
			driver = fc.loginpage().login(driver);


			// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
						// Create CSV File with Lead Details
						String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_33930.csv";
						System.out.println(filePath);
						List<Lead> leadList = new ArrayList<Lead>();

						for (int j = 0; j <= 0; j++) {
							Lead leadImport = new Lead();
							leadImport.setFirstName("LeadSource_Import_33930");
							leadImport.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
							leadImport.setEmail("frantest2017@gmail.com");
							// leadApi.setDivision(addDivision1.getDivisionName()); // BLANK
							leadImport.setCountry("USA");
							leadImport.setStateProvince("Florida"); // BLANK
							leadImport.setCounty("Gulf"); // BLANK
							leadImport.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
							// leadImport.setBasedonAssignmentRules("yes");
							leadImport.setLeadSourceCategory("Friends");
							leadImport.setLeadSourceDetails("Friends");
							
							leadList.add(leadImport);
						}

						common.writeCsvFile(filePath, leadList);

						// Import the CSV file
						Import importlead = new Import();
						ImportTest importTest = new ImportTest(driver);

						// Navigate to Import Tab
						fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

						importlead.setImportType("Leads");
						importlead.setSpecifyFileFormat("CSV");
						importlead.setSalesDataFile(filePath);
						importlead.setLeadStatus("New Lead");
						importlead.setLeadSourceCategory("Friends");
						importlead.setLeadSourceDetails("Friends");

						importTest.importLeadsCSV(importlead);

						// Validation
						Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

						Lead x = new Lead();
						List<String> leadsFirstNames = map.get("FN");
						List<String> leadsLastNames = map.get("LN");

						for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
							System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
							x.setFirstName(leadsFirstNames.get(a));
							x.setLastName(leadsLastNames.get(b));

							String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
							String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
							System.out
									.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

							// Validation
							if (!ownerAssignedToLeadx.equalsIgnoreCase("FranConnect Administrator")) {
								System.out.println("Owner should be ::: FranConnect Administrator");
								fc.utobj().throwsException(
										"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33930 - Import - FAILED");
							}
							
						}


			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

// ==============================================================================================================================================================================================================
	

	@Test(priority =112 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_45-Api", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33929")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33929_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

			try {
				
			// Through API
			// ------------------------------------------------------------------------------------------------------------------------------------------------------------
			for (int j = 0; j <= 0; j++) {
				Lead leadApi = new Lead();
				leadApi.setFirstName("LeadSource_Api_33929");
				leadApi.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				leadApi.setDivision(addDivision2.getDivisionName()); // BLANK
				leadApi.setCountry("USA");
				leadApi.setStateProvince("Florida"); // BLANK
				leadApi.setCounty("Gulf"); // BLANK
				leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory("Friends");
				leadApi.setLeadSourceDetails("Friends");
				
				common.addLeadThroughWebServices(null, leadApi);
				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(null, leadApi);
				// System.out.println(xmlApi);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println(
						"Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
				// Validation
				// Validate Owner Assignment
				// Validation
				if (!ownerAssignedToLeadApi.equalsIgnoreCase("FranConnect Administrator")) {
					System.out.println("Owner should be ::: FranConnect Administrator");
					fc.utobj().throwsException("Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33929 - API - FAILED");
				}

			}
			
			fc.utobj().logoutAndQuitBrowser(null, testCaseId);
			
		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(null, e, testCaseId);

			/*System.out.println(e.getMessage());
			fc.utobj().throwsException1(e.getMessage());*/	
		}
	}
	
	@Test(priority =113 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_45-WebForm", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33929")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33929_WebForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);
			
			// Through WebForm
						// ----------------------------------------------------------------------------------------------------------------------------------------------------------
						// Create WebForm
						ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
						ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

						fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
						common.set_ManageWebform(manageWebFormGenerator);
						manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
						manageWebFormGenerator.setLeadSourceDetails("None");
						manageWebFormGenerator.setDivision(addDivision2.getDivisionName());
						manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

						// Add Lead - WebForm
						for (int j = 0; j <= 0; j++) {
							Lead leadWebForm = new Lead();
							
							leadWebForm.setFirstName("LeadSource_WebForm_33929");
							leadWebForm.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
							leadWebForm.setEmail("frantest2017@gmail.com");
							// leadWebForm.setDivision(addDivision1.getDivisionName()); // BLANK
							leadWebForm.setCountry("USA");
							leadWebForm.setStateProvince("Florida"); // BLANK
							leadWebForm.setCounty("Gulf"); // BLANK
							leadWebForm.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
							// leadWebForm.setBasedonAssignmentRules("yes");
							leadWebForm.setLeadSourceCategory("Friends");
							leadWebForm.setLeadSourceDetails("Friends");

							// Add Lead through WebForm
							manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm);

							// fc.utobj().sleep();
							String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
							String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
							System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>"
									+ ownerAssignedTowebformLead);
							// Validation
							if (!ownerAssignedTowebformLead.equalsIgnoreCase("FranConnect Administrator")) {
								System.out.println("Owner should be ::: FranConnect Administrator");
								fc.utobj().throwsException("Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33929 - WebForm - FAILED");
							}

						}


			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}
	
	
	@Test(priority =114, dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_45-Import", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33929")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33929_Import() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
			// Create CSV File with Lead Details
			String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_33929.csv";
			System.out.println(filePath);
			List<Lead> leadList = new ArrayList<Lead>();

			for (int j = 0; j <= 0; j++) {
				Lead leadImport = new Lead();
				
				leadImport.setFirstName("LeadSource_Import_33929");
				leadImport.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadImport.setEmail("frantest2017@gmail.com");
				leadImport.setDivision(addDivision2.getDivisionName()); // BLANK
				leadImport.setCountry("USA");
				leadImport.setStateProvince("Florida"); // BLANK
				leadImport.setCounty("Gulf"); // BLANK
				leadImport.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadImport.setBasedonAssignmentRules("yes");
				leadImport.setLeadSourceCategory("Friends");
				leadImport.setLeadSourceDetails("Friends");
				
				leadList.add(leadImport);
			}

			common.writeCsvFile(filePath, leadList);

			// Import the CSV file
			Import importlead = new Import();
			ImportTest importTest = new ImportTest(driver);

			// Navigate to Import Tab
			fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

			importlead.setImportType("Leads");
			importlead.setSpecifyFileFormat("CSV");
			importlead.setSalesDataFile(filePath);
			importlead.setLeadStatus("New Lead");
			importlead.setLeadSourceCategory("Friends");
			importlead.setLeadSourceDetails("Friends");

			importTest.importLeadsCSV(importlead);

			// Validation
			Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

			Lead x = new Lead();
			List<String> leadsFirstNames = map.get("FN");
			List<String> leadsLastNames = map.get("LN");

			for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
				System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
				x.setFirstName(leadsFirstNames.get(a));
				x.setLastName(leadsLastNames.get(b));

				String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
				String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
				System.out
						.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

				// Validation
				if (!ownerAssignedToLeadx.equalsIgnoreCase("FranConnect Administrator")) {
					System.out.println("Owner should be ::: FranConnect Administrator");
					fc.utobj().throwsException(
							"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33930 - Import - FAILED");
				}
				
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}
	
// =============================================================================================================================================================================================================	


	@Test(priority =115 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_46-Api", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33928")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33928_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

			try {
								 
			// Through API
			// ------------------------------------------------------------------------------------------------------------------------------------------------------------
			for (int j = 0; j <= 0; j++) {
				Lead leadApi = new Lead();
				leadApi.setFirstName("LeadSource_Api_33928");
				leadApi.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				// leadApi.setDivision(addDivision1.getDivisionName()); // BLANK
				leadApi.setCountry("USA");
				// leadApi.setStateProvince("Florida"); // BLANK
				// leadApi.setCounty("Gulf"); // BLANK
				// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
				leadApi.setLeadSourceDetails("None");
				
				common.addLeadThroughWebServices(null, leadApi);
				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(null, leadApi);
				// System.out.println(xmlApi);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println(
						"Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
				// Validation
				// Validate Owner Assignment
				// Validation
				if (!corpUsersForSource2.contains(ownerAssignedToLeadApi)) {
					System.out.println("Owner should be among ::: " + corpUsersForSource2);
					fc.utobj().throwsException(
							"Owner Assignment - Assign Lead Owner by Lead Source - TC-33928 - API - FAILED");
				}

			}
			
			fc.utobj().logoutAndQuitBrowser(null, testCaseId);
			
		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(null, e, testCaseId);

			/*System.out.println(e.getMessage());
			fc.utobj().throwsException1(e.getMessage());	*/
		}
	}
	
	@Test(priority = 116 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_46-WebForm", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33928")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33928_WebForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			// Through WebForm
			// ----------------------------------------------------------------------------------------------------------------------------------------------------------
			// Create WebForm
			ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
			ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

			fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
			common.set_ManageWebform(manageWebFormGenerator);
			manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
			manageWebFormGenerator.setLeadSourceDetails("None");
		//	manageWebFormGenerator.setDivision(addDivision1.getDivisionName());
			manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

			// Add Lead - WebForm
			for (int j = 0; j <= 0; j++) {
				Lead leadWebForm = new Lead();
				
				leadWebForm.setFirstName("LeadSource_WebForm_33928");
				leadWebForm.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadWebForm.setEmail("frantest2017@gmail.com");
				// leadApi.setDivision(addDivision1.getDivisionName()); // BLANK
				leadWebForm.setCountry("USA");
				// leadApi.setStateProvince("Florida"); // BLANK
				// leadApi.setCounty("Gulf"); // BLANK
				// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				// leadWebForm.setBasedonAssignmentRules("yes");
				leadWebForm.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
				leadWebForm.setLeadSourceDetails("None");

				// Add Lead through WebForm
				manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm);

				// fc.utobj().sleep();
				String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
				String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
				System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>"
						+ ownerAssignedTowebformLead);
				// Validation
				if (!corpUsersForSource2.contains(ownerAssignedTowebformLead)) {
					System.out.println("Owner should be among ::: " + corpUsersForSource2);
					fc.utobj().throwsException(
							"Owner Assignment - Assign Lead Owner by Lead Source - TC-33928 - WebForm - FAILED");
				}

			}


			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}
	
	@Test(priority =117 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_46-Import", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33928")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33928_Import() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
						// Create CSV File with Lead Details
						String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_33928.csv";
						System.out.println(filePath);
						List<Lead> leadList = new ArrayList<Lead>();

						for (int j = 0; j <= 0; j++) {
							Lead leadImport = new Lead();
							
							leadImport.setFirstName("LeadSource_Import_33928");
							leadImport.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
							leadImport.setEmail("frantest2017@gmail.com");
							// leadApi.setDivision(addDivision1.getDivisionName()); // BLANK
							leadImport.setCountry("USA");
							// leadApi.setStateProvince("Florida"); // BLANK
							// leadApi.setCounty("Gulf"); // BLANK
							// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
							leadImport.setBasedonAssignmentRules("yes");
							leadImport.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
							leadImport.setLeadSourceDetails("None");
							
							leadList.add(leadImport);
						}

						common.writeCsvFile(filePath, leadList);

						// Import the CSV file
						Import importlead = new Import();
						ImportTest importTest = new ImportTest(driver);

						// Navigate to Import Tab
						fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

						importlead.setImportType("Leads");
						importlead.setSpecifyFileFormat("CSV");
						importlead.setSalesDataFile(filePath);
						importlead.setLeadStatus("New Lead");
						importlead.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
						importlead.setLeadSourceDetails("None");

						importTest.importLeadsCSV(importlead);

						// Validation
						Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

						Lead x = new Lead();
						List<String> leadsFirstNames = map.get("FN");
						List<String> leadsLastNames = map.get("LN");

						for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
							System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
							x.setFirstName(leadsFirstNames.get(a));
							x.setLastName(leadsLastNames.get(b));

							String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
							String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
							System.out
									.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

							// Validation
							if (!corpUsersForSource2.contains(ownerAssignedToLeadx)) {
								System.out.println("Owner should be among ::: " + corpUsersForSource2);
								fc.utobj().throwsException(
										"Owner Assignment - Assign Lead Owner by Lead Source - TC-33928 - Import - FAILED");
							}

						}


			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}

	
// =========================================================================================================================================================================================================	
	

	@Test(priority = 118 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_47-Api", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33927")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33927_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

			try {
								 
			// Through API
			// ------------------------------------------------------------------------------------------------------------------------------------------------------------
			for (int j = 0; j <= 0; j++) {
				Lead leadApi = new Lead();
				leadApi.setFirstName("LeadSource_Api_33927");
				leadApi.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				leadApi.setDivision(addDivision2.getDivisionName()); // BLANK
				leadApi.setCountry("USA");
				// leadApi.setStateProvince("Florida"); // BLANK
				// leadApi.setCounty("Gulf"); // BLANK
				// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory("Friends");
				leadApi.setLeadSourceDetails("Friends");
				
				common.addLeadThroughWebServices(null, leadApi);
				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(null, leadApi);
				// System.out.println(xmlApi);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println(
						"Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
				// Validation
				// Validate Owner Assignment
				// Validation
				if (!ownerAssignedToLeadApi.equalsIgnoreCase("FranConnect Administrator")) {
					System.out.println("Owner should be ::: FranConnect Administrator");
					fc.utobj().throwsException("Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33927 - API - FAILED");
				}

			}
			

			fc.utobj().logoutAndQuitBrowser(null, testCaseId);
			
		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(null, e, testCaseId);

			/*System.out.println(e.getMessage());
			fc.utobj().throwsException1(e.getMessage());*/	
		}
	}
	
	
	@Test(priority = 119, dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" , })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_47-WebForm", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33927")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33927_WebForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			// Through WebForm
						// ----------------------------------------------------------------------------------------------------------------------------------------------------------
						// Create WebForm
						ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
						ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

						fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
						common.set_ManageWebform(manageWebFormGenerator);
						manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
						manageWebFormGenerator.setLeadSourceDetails("None");
						manageWebFormGenerator.setDivision(addDivision2.getDivisionName());
						manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

						// Add Lead - WebForm
						for (int j = 0; j <= 0; j++) {
							Lead leadWebForm = new Lead();
							
							leadWebForm.setFirstName("LeadSource_WebForm_33927");
							leadWebForm.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
							leadWebForm.setEmail("frantest2017@gmail.com");
							// leadWebForm.setDivision(addDivision1.getDivisionName()); // BLANK
							leadWebForm.setCountry("USA");
							// leadApi.setStateProvince("Florida"); // BLANK
							// leadApi.setCounty("Gulf"); // BLANK
							// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
							// leadWebForm.setBasedonAssignmentRules("yes");
							leadWebForm.setLeadSourceCategory("Friends");
							leadWebForm.setLeadSourceDetails("Friends");

							// Add Lead through WebForm
							manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm);

							// fc.utobj().sleep();
							String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
							String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
							System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>"
									+ ownerAssignedTowebformLead);
							// Validation
							if (!ownerAssignedTowebformLead.equalsIgnoreCase("FranConnect Administrator")) {
								System.out.println("Owner should be ::: FranConnect Administrator");
								fc.utobj().throwsException("Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33927 - WebForm - FAILED");
							}

						}


			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}
	
	@Test(priority =120,dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_47-Import", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33927") 
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33927_Import() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
			// Create CSV File with Lead Details
			String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_33927.csv";
			System.out.println(filePath);
			List<Lead> leadList = new ArrayList<Lead>();

			for (int j = 0; j <= 0; j++) {
				Lead leadImport = new Lead();
				
				leadImport.setFirstName("LeadSource_Import_33927");
				leadImport.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadImport.setEmail("frantest2017@gmail.com");
				leadImport.setDivision(addDivision2.getDivisionName()); // BLANK
				leadImport.setCountry("USA");
				// leadApi.setStateProvince("Florida"); // BLANK
				// leadApi.setCounty("Gulf"); // BLANK
				// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadImport.setBasedonAssignmentRules("yes");
				leadImport.setLeadSourceCategory("Friends");
				leadImport.setLeadSourceDetails("Friends");
				
				leadList.add(leadImport);
			}

			common.writeCsvFile(filePath, leadList);

			// Import the CSV file
			Import importlead = new Import();
			ImportTest importTest = new ImportTest(driver);

			// Navigate to Import Tab
			fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

			importlead.setImportType("Leads");
			importlead.setSpecifyFileFormat("CSV");
			importlead.setSalesDataFile(filePath);
			importlead.setLeadStatus("New Lead");
			importlead.setLeadSourceCategory("Friends");
			importlead.setLeadSourceDetails("Friends");

			importTest.importLeadsCSV(importlead);

			// Validation
			Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

			Lead x = new Lead();
			List<String> leadsFirstNames = map.get("FN");
			List<String> leadsLastNames = map.get("LN");

			for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
				System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
				x.setFirstName(leadsFirstNames.get(a));
				x.setLastName(leadsLastNames.get(b));

				String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
				String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
				System.out
						.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

				// Validation
				if (!ownerAssignedToLeadx.equalsIgnoreCase("FranConnect Administrator")) {
					System.out.println("Owner should be ::: FranConnect Administrator");
					fc.utobj().throwsException("Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33927 - Import - FAILED");
				}

			}
		
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}

// =============================================================================================================================================================================================================
	
	
	@Test(priority = 121 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_48-Api", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33926")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33926_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

			try {
								 
			// Through API
			// ------------------------------------------------------------------------------------------------------------------------------------------------------------
			for (int j = 0; j <= 0; j++) {
				Lead leadApi = new Lead();
				leadApi.setFirstName("LeadSource_Api_33926");
				leadApi.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				leadApi.setDivision(addDivision2.getDivisionName()); // BLANK
				leadApi.setCountry("USA");
				// leadApi.setStateProvince("Florida"); // BLANK
				// leadApi.setCounty("Gulf"); // BLANK
				// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
				leadApi.setLeadSourceDetails("None");
				
				common.addLeadThroughWebServices(null, leadApi);
				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(null, leadApi);
				// System.out.println(xmlApi);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println(
						"Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
				// Validation
				// Validate Owner Assignment
				// Validation
				if (!corpUsersForDivision.contains(ownerAssignedToLeadApi)) {
					System.out.println("Owner should be among ::: " + corpUsersForDivision);
					fc.utobj().throwsException(
							"Owner Assignment - Assign Lead Owner by Lead Source - TC-33926 - API - FAILED");
				}

			}
			

			fc.utobj().logoutAndQuitBrowser(null, testCaseId);
			
		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(null, e, testCaseId);

			/*System.out.println(e.getMessage());
			fc.utobj().throwsException1(e.getMessage());*/				
		}
	}
	
	
	@Test(priority =122, dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_48-WebForm", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33926")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33926_WebForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			// Through WebForm
			// ----------------------------------------------------------------------------------------------------------------------------------------------------------
			// Create WebForm
			ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
			ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

			fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
			common.set_ManageWebform(manageWebFormGenerator);
			manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
			manageWebFormGenerator.setLeadSourceDetails("None");
			manageWebFormGenerator.setDivision(addDivision2.getDivisionName());
			manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

			// Add Lead - WebForm
			for (int j = 0; j <= 0; j++) {
				Lead leadWebForm = new Lead();
				
				leadWebForm.setFirstName("LeadSource_WebForm_33926");
				leadWebForm.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadWebForm.setEmail("frantest2017@gmail.com");
				// leadWebForm.setDivision(addDivision1.getDivisionName()); // BLANK
				leadWebForm.setCountry("USA");
				// leadApi.setStateProvince("Florida"); // BLANK
				// leadApi.setCounty("Gulf"); // BLANK
				// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				// leadWebForm.setBasedonAssignmentRules("yes");
				leadWebForm.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
				leadWebForm.setLeadSourceDetails("None");
				// Add Lead through WebForm
				manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm);

				// fc.utobj().sleep();
				String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
				String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
				System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>"
						+ ownerAssignedTowebformLead);
				
				// Validation
				if (! corpUsersForDivision.contains(ownerAssignedTowebformLead)) {
					System.out.println("Owner should be among ::: " + corpUsersForDivision);
					fc.utobj().throwsException("Owner Assignment by LeadSource - Division - TC-33927 - WebForm - FAILED");
				}
				/*// Validation
				if (!ownerAssignedTowebformLead.equalsIgnoreCase("FranConnect Administrator")) {
					fc.utobj().throwsException("Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33927 - WebForm - FAILED");
				}*/

			}


			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}
	
	@Test(priority = 123 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" /*, "sales_OwnerAssignment_Failed"*/ })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_48-Import", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33926")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33926_Import() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
						// Create CSV File with Lead Details
						String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_33926.csv";
						System.out.println(filePath);
						List<Lead> leadList = new ArrayList<Lead>();

						for (int j = 0; j <= 0; j++) {
							Lead leadImport = new Lead();
							
							leadImport.setFirstName("LeadSource_Import_33926");
							leadImport.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
							leadImport.setEmail("frantest2017@gmail.com");
							leadImport.setDivision(addDivision2.getDivisionName()); // BLANK
							leadImport.setCountry("USA");
							// leadApi.setStateProvince("Florida"); // BLANK
							// leadApi.setCounty("Gulf"); // BLANK
							// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
							leadImport.setBasedonAssignmentRules("yes");
							leadImport.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
							leadImport.setLeadSourceDetails("None");
							
							leadList.add(leadImport);
						}

						common.writeCsvFile(filePath, leadList);

						// Import the CSV file
						Import importlead = new Import();
						ImportTest importTest = new ImportTest(driver);

						// Navigate to Import Tab
						fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

						importlead.setImportType("Leads");
						importlead.setSpecifyFileFormat("CSV");
						importlead.setSalesDataFile(filePath);
						importlead.setLeadStatus("New Lead");
						importlead.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
						importlead.setLeadSourceDetails("None");

						importTest.importLeadsCSV(importlead);

						// Validation
						Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

						Lead x = new Lead();
						List<String> leadsFirstNames = map.get("FN");
						List<String> leadsLastNames = map.get("LN");

						for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
							System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
							x.setFirstName(leadsFirstNames.get(a));
							x.setLastName(leadsLastNames.get(b));

							String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
							String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
							System.out
									.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

							
							// Validation
							if (! corpUsersForDivision.contains(ownerAssignedToLeadx)) {
								System.out.println("Owner should be among ::: " + corpUsersForDivision);
								fc.utobj().throwsException("Owner Assignment by LeadSource - Division - TC-33927 - Import - FAILED");
							}
							/*// Validation
							if (!ownerAssignedToLeadx.equalsIgnoreCase("FranConnect Administrator")) {
								fc.utobj().throwsException("Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33927 - Import - FAILED");
							} */

						}
					
			

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}


// ============================================================================================================================================================================================================	

	@Test(priority = 124 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_49-Api", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33925")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33925_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

			try {
							 
			// Through API
			// ------------------------------------------------------------------------------------------------------------------------------------------------------------
			for (int j = 0; j <= 0; j++) {
				Lead leadApi = new Lead();
				leadApi.setFirstName("LeadSource_Api_33925");
				leadApi.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				leadApi.setDivision(addDivision2.getDivisionName()); // BLANK
				leadApi.setCountry("USA");
				// leadApi.setStateProvince("Florida"); // BLANK
				leadApi.setCounty("Gulf"); // BLANK
				// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
				leadApi.setLeadSourceDetails("None");
				
				common.addLeadThroughWebServices(null, leadApi);
				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(null, leadApi);
				// System.out.println(xmlApi);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println(
						"Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
				// Validation
				// Validate Owner Assignment
				// Validation
				if (!corpUsersForDivision.contains(ownerAssignedToLeadApi)) {
					System.out.println("Owner should be among ::: " + corpUsersForDivision);
					fc.utobj().throwsException(
							"Owner Assignment - Assign Lead Owner by Lead Source - TC-33925 - API - FAILED");
				}

			}
			
			fc.utobj().logoutAndQuitBrowser(null, testCaseId);
			
		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(null, e, testCaseId);

			/*System.out.println(e.getMessage());
			fc.utobj().throwsException1(e.getMessage());	*/			
		}
	}
	
	@Test(priority = 125 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_49-WebForm", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33925")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33925_WebForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			// Through WebForm
						// ----------------------------------------------------------------------------------------------------------------------------------------------------------
						// Create WebForm
						ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
						ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

						fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
						common.set_ManageWebform(manageWebFormGenerator);
						manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
						manageWebFormGenerator.setLeadSourceDetails("None");
						manageWebFormGenerator.setDivision(addDivision2.getDivisionName());
						manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

						// Add Lead - WebForm
						for (int j = 0; j <= 0; j++) {
							Lead leadWebForm = new Lead();
							
							leadWebForm.setFirstName("LeadSource_WebForm_33925");
							leadWebForm.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
							leadWebForm.setEmail("frantest2017@gmail.com");
						//	leadWebForm.setDivision(addDivision1.getDivisionName()); // BLANK
							leadWebForm.setCountry("USA");
							// leadApi.setStateProvince("Florida"); // BLANK
							leadWebForm.setCounty("Gulf"); // BLANK
							// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
							// leadWebForm.setBasedonAssignmentRules("yes");
							leadWebForm.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
							leadWebForm.setLeadSourceDetails("None");
							
							// Add Lead through WebForm
							manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm);

							// fc.utobj().sleep();
							String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
							String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
							System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>"
									+ ownerAssignedTowebformLead);
							// Validation
							if (!corpUsersForDivision.contains(ownerAssignedTowebformLead)) {
								System.out.println("Owner should be among ::: " + corpUsersForDivision);
								fc.utobj().throwsException(
										"Owner Assignment - Assign Lead Owner by Lead Source - TC-33925 - WebForm - FAILED");
							}

						}

			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}

	
	@Test(priority = 126 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_49-Import", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33925")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33925_Import() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
			// Create CSV File with Lead Details
			String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_33925.csv";
			System.out.println(filePath);
			List<Lead> leadList = new ArrayList<Lead>();

			for (int j = 0; j <= 0; j++) {
				Lead leadImport = new Lead();
				
				leadImport.setFirstName("LeadSource_Import_33925");
				leadImport.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadImport.setEmail("frantest2017@gmail.com");
				leadImport.setDivision(addDivision2.getDivisionName()); // BLANK
				leadImport.setCountry("USA");
				// leadApi.setStateProvince("Florida"); // BLANK
				leadImport.setCounty("Gulf"); // BLANK
				// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadImport.setBasedonAssignmentRules("yes");
				leadImport.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
				leadImport.setLeadSourceDetails("None");
				
				leadList.add(leadImport);
			}

			common.writeCsvFile(filePath, leadList);

			// Import the CSV file
			Import importlead = new Import();
			ImportTest importTest = new ImportTest(driver);

			// Navigate to Import Tab
			fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

			importlead.setImportType("Leads");
			importlead.setSpecifyFileFormat("CSV");
			importlead.setSalesDataFile(filePath);
			importlead.setLeadStatus("New Lead");
			importlead.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
			importlead.setLeadSourceDetails("None");

			importTest.importLeadsCSV(importlead);

			// Validation
			Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

			Lead x = new Lead();
			List<String> leadsFirstNames = map.get("FN");
			List<String> leadsLastNames = map.get("LN");

			for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
				System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
				x.setFirstName(leadsFirstNames.get(a));
				x.setLastName(leadsLastNames.get(b));

				String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
				String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
				System.out
						.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

				// Validation
				
				if (!corpUsersForDivision.contains(ownerAssignedToLeadx)) {
					System.out.println("Owner should be among ::: " + corpUsersForDivision);
					fc.utobj().throwsException(
							"Owner Assignment - Assign Lead Owner by Lead Source - TC-33925 - Import - FAILED");
				}

			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}

// ============================================================================================================================================================================================================
	

	@Test(priority = 127 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_50 -Api", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33924")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33924_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

			try {
								 
			// Through API
			// ------------------------------------------------------------------------------------------------------------------------------------------------------------
			for (int j = 0; j <= 0; j++) {
				Lead leadApi = new Lead();
				leadApi.setFirstName("LeadSource_Api_33924");
				leadApi.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				leadApi.setDivision(addDivision2.getDivisionName()); // BLANK
				leadApi.setCountry("USA");
				leadApi.setStateProvince("Florida"); // BLANK
				leadApi.setCounty("Gulf"); // BLANK
				//leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
				leadApi.setLeadSourceDetails("None");
				
				common.addLeadThroughWebServices(null, leadApi);
				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(null, leadApi);
				// System.out.println(xmlApi);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println(
						"Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
				// Validation
				// Validate Owner Assignment
				// Validation
				if (!corpUsersForSalesTerritoriesCounty.contains(ownerAssignedToLeadApi)) {
					System.out.println("Owner should be among ::: " + corpUsersForSalesTerritoriesCounty);
					fc.utobj().throwsException(
							"Owner Assignment - Assign Lead Owner by Lead Source - TC-33924 - API - FAILED");
				}

			}
			
			fc.utobj().logoutAndQuitBrowser(null, testCaseId);
			
		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(null, e, testCaseId);

			/*System.out.println(e.getMessage());
			fc.utobj().throwsException1(e.getMessage());	*/		}
	}
	
	@Test(priority = 128 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_50-WebForm", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33924")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33924_WebForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			// Through WebForm
			// ----------------------------------------------------------------------------------------------------------------------------------------------------------
			// Create WebForm
			ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
			ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

			fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
			common.set_ManageWebform(manageWebFormGenerator);
			manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
			manageWebFormGenerator.setLeadSourceDetails("None");
			manageWebFormGenerator.setDivision(addDivision2.getDivisionName());
			manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

			// Add Lead - WebForm
			for (int j = 0; j <= 0; j++) {
				Lead leadWebForm = new Lead();
				
				leadWebForm.setFirstName("LeadSource_WebForm_33924");
				leadWebForm.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadWebForm.setEmail("frantest2017@gmail.com");
			//	leadWebForm.setDivision(addDivision1.getDivisionName()); // BLANK
				leadWebForm.setCountry("USA");
				leadWebForm.setStateProvince("Florida"); // BLANK
				leadWebForm.setCounty("Gulf"); // BLANK
				//leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				// leadWebForm.setBasedonAssignmentRules("yes");
				leadWebForm.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
				leadWebForm.setLeadSourceDetails("None");
				
				// Add Lead through WebForm
				manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm);

				// fc.utobj().sleep();
				String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
				String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
				System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>"
						+ ownerAssignedTowebformLead);
				// Validation
				if (!corpUsersForSalesTerritoriesCounty.contains(ownerAssignedTowebformLead)) {
					System.out.println("Owner should be among ::: " + corpUsersForSalesTerritoriesCounty);
					fc.utobj().throwsException(
							"Owner Assignment - Assign Lead Owner by Lead Source - TC-33924 - WebForm - FAILED");
				}

			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(priority = 129 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" /*, "sales_OwnerAssignment_Failed"*/ })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_50-Import", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33924")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33924_Import() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
						// Create CSV File with Lead Details
						String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_33924.csv";
						System.out.println(filePath);
						List<Lead> leadList = new ArrayList<Lead>();

						for (int j = 0; j <= 0; j++) {
							Lead leadImport = new Lead();
							
							leadImport.setFirstName("LeadSource_Import_33924");
							leadImport.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
							leadImport.setEmail("frantest2017@gmail.com");
							leadImport.setDivision(addDivision2.getDivisionName()); // BLANK
							leadImport.setCountry("USA");
							leadImport.setStateProvince("Florida"); // BLANK
							leadImport.setCounty("Gulf"); // BLANK
							//leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
							leadImport.setBasedonAssignmentRules("yes");
							leadImport.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
							leadImport.setLeadSourceDetails("None");
							
							leadList.add(leadImport);
						}

						common.writeCsvFile(filePath, leadList);

						// Import the CSV file
						Import importlead = new Import();
						ImportTest importTest = new ImportTest(driver);

						// Navigate to Import Tab
						fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

						importlead.setImportType("Leads");
						importlead.setSpecifyFileFormat("CSV");
						importlead.setSalesDataFile(filePath);
						importlead.setLeadStatus("New Lead");
						importlead.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
						importlead.setLeadSourceDetails("None");

						importTest.importLeadsCSV(importlead);

						// Validation
						Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

						Lead x = new Lead();
						List<String> leadsFirstNames = map.get("FN");
						List<String> leadsLastNames = map.get("LN");

						for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
							System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
							x.setFirstName(leadsFirstNames.get(a));
							x.setLastName(leadsLastNames.get(b));

							String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
							String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
							System.out
									.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

							if (! corpUsersForDivision.contains(ownerAssignedToLeadx)) {
								System.out.println("Owner should be among ::: " + corpUsersForDivision);
								fc.utobj().throwsException(
										"Owner Assignment - Assign Lead Owner by Lead Source - TC-33924 - Import - FAILED");
							}
							
							/*// Validation
							if (!corpUsersForSalesTerritoriesCounty.contains(ownerAssignedToLeadx)) {
								fc.utobj().throwsException(
										"Owner Assignment - Assign Lead Owner by Lead Source - TC-33924 - Import - FAILED");
							}*/

						}
							
				

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

// ============================================================================================================================================================================================================	

	@Test(priority = 130 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_51-Api", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33923")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33923_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

			try {
								 
			// Through API
			// ------------------------------------------------------------------------------------------------------------------------------------------------------------
			for (int j = 0; j <= 0; j++) {
				Lead leadApi = new Lead();
				leadApi.setFirstName("LeadSource_Api_33923");
				leadApi.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				leadApi.setDivision(addDivision2.getDivisionName()); // BLANK
				leadApi.setCountry("USA");
				// leadApi.setStateProvince("Florida"); // BLANK
				leadApi.setCounty("Gulf"); // BLANK
				leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
				leadApi.setLeadSourceDetails("None");
				
				common.addLeadThroughWebServices(null, leadApi);
				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(null, leadApi);
				// System.out.println(xmlApi);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println(
						"Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
				// Validation
				// Validate Owner Assignment
				// Validation
				if (!corpUsersForSalesTerritoriesZip.contains(ownerAssignedToLeadApi)) {
					System.out.println("Owner should be among ::: " + corpUsersForSalesTerritoriesZip);
					fc.utobj().throwsException(
							"Owner Assignment - Assign Lead Owner by Lead Source - TC-33923 - API - FAILED");
				}

			}
			

			fc.utobj().logoutAndQuitBrowser(null, testCaseId);
			
		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(null, e, testCaseId);

		/*	System.out.println(e.getMessage());
			fc.utobj().throwsException1(e.getMessage());	*/			
		}
	}
	
	
	@Test(priority = 131 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_51-WebForm", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33923")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33923_WebForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			// Through WebForm
						// ----------------------------------------------------------------------------------------------------------------------------------------------------------
						// Create WebForm
						ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
						ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

						fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
						common.set_ManageWebform(manageWebFormGenerator);
						manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
						manageWebFormGenerator.setLeadSourceDetails("None");
						manageWebFormGenerator.setDivision(addDivision2.getDivisionName());
						manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

						// Add Lead - WebForm
						for (int j = 0; j <= 0; j++) {
							Lead leadWebForm = new Lead();
							
							leadWebForm.setFirstName("LeadSource_WebForm_33923");
							leadWebForm.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
							leadWebForm.setEmail("frantest2017@gmail.com");
						//	leadWebForm.setDivision(addDivision1.getDivisionName()); // BLANK
							leadWebForm.setCountry("USA");
							// leadApi.setStateProvince("Florida"); // BLANK
							leadWebForm.setCounty("Gulf"); // BLANK
							leadWebForm.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
							// leadWebForm.setBasedonAssignmentRules("yes");
							leadWebForm.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
							leadWebForm.setLeadSourceDetails("None");
							
							// Add Lead through WebForm
							manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm);

							// fc.utobj().sleep();
							String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
							String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
							System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>"
									+ ownerAssignedTowebformLead);
							// Validation
							if (!corpUsersForSalesTerritoriesZip.contains(ownerAssignedTowebformLead)) {
								System.out.println("Owner should be among ::: " + corpUsersForSalesTerritoriesZip);
								fc.utobj().throwsException(
										"Owner Assignment - Assign Lead Owner by Lead Source - TC-33923 - WebForm - FAILED");
							}

						}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}
	
	@Test(priority =132,dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_51-Import", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33923")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33923_Import() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
			// Create CSV File with Lead Details
			String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_33923.csv";
			System.out.println(filePath);
			List<Lead> leadList = new ArrayList<Lead>();

			for (int j = 0; j <= 0; j++) {
				Lead leadImport = new Lead();
				
				leadImport.setFirstName("LeadSource_Import_33923");
				leadImport.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadImport.setEmail("frantest2017@gmail.com");
				leadImport.setDivision(addDivision2.getDivisionName()); // BLANK
				leadImport.setCountry("USA");
				// leadApi.setStateProvince("Florida"); // BLANK
				leadImport.setCounty("Gulf"); // BLANK
				leadImport.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadImport.setBasedonAssignmentRules("yes");
				leadImport.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
				leadImport.setLeadSourceDetails("None");
				
				leadList.add(leadImport);
			}

			common.writeCsvFile(filePath, leadList);

			// Import the CSV file
			Import importlead = new Import();
			ImportTest importTest = new ImportTest(driver);

			// Navigate to Import Tab
			fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

			importlead.setImportType("Leads");
			importlead.setSpecifyFileFormat("CSV");
			importlead.setSalesDataFile(filePath);
			importlead.setLeadStatus("New Lead");
			importlead.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
			importlead.setLeadSourceDetails("None");

			importTest.importLeadsCSV(importlead);

			// Validation
			Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

			Lead x = new Lead();
			List<String> leadsFirstNames = map.get("FN");
			List<String> leadsLastNames = map.get("LN");

			for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
				System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
				x.setFirstName(leadsFirstNames.get(a));
				x.setLastName(leadsLastNames.get(b));

				String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
				String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
				System.out
						.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

				// Validation
				if (!corpUsersForSalesTerritoriesZip.contains(ownerAssignedToLeadx)) {
					System.out.println("Owner should be among ::: " + corpUsersForSalesTerritoriesZip);
					fc.utobj().throwsException(
							"Owner Assignment - Assign Lead Owner by Lead Source - TC-33923 - Import - FAILED");
				}

			}
				
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}

// ============================================================================================================================================================================================================
	
	@Test(priority = 133 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_52-Api", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33922")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33922_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

			try {
								 
			// Through API
			// ------------------------------------------------------------------------------------------------------------------------------------------------------------
			for (int j = 0; j <= 0; j++) {
				Lead leadApi = new Lead();
				leadApi.setFirstName("LeadSource_Api_33922");
				leadApi.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				leadApi.setDivision(addDivision2.getDivisionName()); 
				leadApi.setCountry("USA");
				leadApi.setStateProvince("Florida"); 
				leadApi.setCounty("Gulf"); 
				leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
				leadApi.setLeadSourceDetails("None");
				
				common.addLeadThroughWebServices(null, leadApi);
				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(null, leadApi);
				// System.out.println(xmlApi);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println(
						"Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
				// Validation
				// Validate Owner Assignment
				// Validation
				if (!corpUsersForSalesTerritoriesZip.contains(ownerAssignedToLeadApi)) {
					System.out.println("Owner should be among ::: " + corpUsersForSalesTerritoriesZip);

					fc.utobj().throwsException(
							"Owner Assignment - Assign Lead Owner by Lead Source - TC-33922 - API - FAILED");
				}

			}
			
			fc.utobj().logoutAndQuitBrowser(null, testCaseId);
			
		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(null, e, testCaseId);

			/*System.out.println(e.getMessage());
			fc.utobj().throwsException1(e.getMessage());*/				
		}
	}
	
	
	@Test(priority =134, dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_52-WebForm", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33922")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33922_WebForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			// Through WebForm
			// ----------------------------------------------------------------------------------------------------------------------------------------------------------
			// Create WebForm
			ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
			ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

			fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
			common.set_ManageWebform(manageWebFormGenerator);
			manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
			manageWebFormGenerator.setLeadSourceDetails("None");
			manageWebFormGenerator.setDivision(addDivision2.getDivisionName());
			manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

			// Add Lead - WebForm
			for (int j = 0; j <= 0; j++) {
				Lead leadWebForm = new Lead();
				
				leadWebForm.setFirstName("LeadSource_WebForm_33922");
				leadWebForm.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadWebForm.setEmail("frantest2017@gmail.com");
			//	leadWebForm.setDivision(addDivision1.getDivisionName()); 
				leadWebForm.setCountry("USA");
				leadWebForm.setStateProvince("Florida"); 
				leadWebForm.setCounty("Gulf"); 
				leadWebForm.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				// leadWebForm.setBasedonAssignmentRules("yes");
				leadWebForm.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
				leadWebForm.setLeadSourceDetails("None");
				
				// Add Lead through WebForm
				manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm);

				// fc.utobj().sleep();
				String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
				String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
				System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>"
						+ ownerAssignedTowebformLead);
				// Validation
				if (!corpUsersForSalesTerritoriesZip.contains(ownerAssignedTowebformLead)) {
					System.out.println("Owner should be among ::: " + corpUsersForSalesTerritoriesZip);
					fc.utobj().throwsException(
							"Owner Assignment - Assign Lead Owner by Lead Source - TC-33922 - WebForm - FAILED");
				}

			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}
	
	
	@Test(priority = 135 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_52 -Import", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33922")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33922_Import() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
						// Create CSV File with Lead Details
						String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_33922.csv";
						System.out.println(filePath);
						List<Lead> leadList = new ArrayList<Lead>();

						for (int j = 0; j <= 0; j++) {
							Lead leadImport = new Lead();
							
							leadImport.setFirstName("LeadSource_Import_33922");
							leadImport.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
							leadImport.setEmail("frantest2017@gmail.com");
							leadImport.setDivision(addDivision2.getDivisionName()); 
							leadImport.setCountry("USA");
							leadImport.setStateProvince("Florida"); 
							leadImport.setCounty("Gulf"); 
							leadImport.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
							// leadImport.setBasedonAssignmentRules("yes");
							leadImport.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
							leadImport.setLeadSourceDetails("None");
							
							leadList.add(leadImport);
						}

						common.writeCsvFile(filePath, leadList);

						// Import the CSV file
						Import importlead = new Import();
						ImportTest importTest = new ImportTest(driver);

						// Navigate to Import Tab
						fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

						importlead.setImportType("Leads");
						importlead.setSpecifyFileFormat("CSV");
						importlead.setSalesDataFile(filePath);
						importlead.setLeadStatus("New Lead");
						importlead.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
						importlead.setLeadSourceDetails("None");

						importTest.importLeadsCSV(importlead);

						// Validation
						Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

						Lead x = new Lead();
						List<String> leadsFirstNames = map.get("FN");
						List<String> leadsLastNames = map.get("LN");

						for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
							System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
							x.setFirstName(leadsFirstNames.get(a));
							x.setLastName(leadsLastNames.get(b));

							String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
							String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
							System.out
									.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

							// Validation
							if (!corpUsersForSalesTerritoriesZip.contains(ownerAssignedToLeadx)) {
								System.out.println("Owner should be among ::: " + corpUsersForSalesTerritoriesZip);
								fc.utobj().throwsException(
										"Owner Assignment - Assign Lead Owner by Lead Source - TC-33922 - Import - FAILED");
							}

						}
							

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}

// ===========================================================================================================================================================================================================
	
	@Test(priority = 136 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_53-Api", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33921")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33921_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

			try {
								 
			// Through API
			// ------------------------------------------------------------------------------------------------------------------------------------------------------------
			for (int j = 0; j <= 0; j++) {
				Lead leadApi = new Lead();
				leadApi.setFirstName("LeadSource_Api_33921");
				leadApi.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				leadApi.setDivision(addDivision3.getDivisionName());
				leadApi.setCountry("USA");
				// leadApi.setStateProvince("Florida");
				// leadApi.setCounty("Gulf");
				leadApi.setZipPostalCode("564565");
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory("Friends");
				leadApi.setLeadSourceDetails("Friends");
				
				common.addLeadThroughWebServices(null, leadApi);
				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(null, leadApi);
				// System.out.println(xmlApi);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println(
						"Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
				// Validation
				// Validate Owner Assignment
				// Validation
				if (!ownerAssignedToLeadApi.equalsIgnoreCase("FranConnect Administrator")) {
					System.out.println("Owner should be ::: FranConnect Administrator");
					fc.utobj().throwsException("Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33921 - API - FAILED");
				}
			}
			

			fc.utobj().logoutAndQuitBrowser(null, testCaseId);
			
		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(null, e, testCaseId);

			/*System.out.println(e.getMessage());
			fc.utobj().throwsException1(e.getMessage());*/				
		}
	}
	
	@Test(priority =137 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_53-WebForm", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33921")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33921_WebForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			// Through WebForm
						// ----------------------------------------------------------------------------------------------------------------------------------------------------------
						// Create WebForm
						ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
						ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

						fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
						common.set_ManageWebform(manageWebFormGenerator);
						manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
						manageWebFormGenerator.setLeadSourceDetails("None");
						manageWebFormGenerator.setDivision(addDivision3.getDivisionName());
						manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

						// Add Lead - WebForm
						for (int j = 0; j <= 0; j++) {
							Lead leadWebForm = new Lead();
							
							leadWebForm.setFirstName("LeadSource_WebForm_33921");
							leadWebForm.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
							leadWebForm.setEmail("frantest2017@gmail.com");
					//		leadWebForm.setDivision(addDivision2.getDivisionName());
							leadWebForm.setCountry("USA");
							// leadApi.setStateProvince("Florida");
							// leadApi.setCounty("Gulf");
							leadWebForm.setZipPostalCode("564565");
							// leadWebForm.setBasedonAssignmentRules("yes");
							leadWebForm.setLeadSourceCategory("Friends");
							leadWebForm.setLeadSourceDetails("Friends");
							
							// Add Lead through WebForm
							manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm);

							// fc.utobj().sleep();
							String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
							String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
							System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>"
									+ ownerAssignedTowebformLead);
							// Validation
							if (!ownerAssignedTowebformLead.equalsIgnoreCase("FranConnect Administrator")) {
								System.out.println("Owner should be ::: FranConnect Administrator");
								fc.utobj().throwsException("Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33921 - WebForm - FAILED");
							}
						}
						

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}
	
	@Test(priority = 138 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_53-Import", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33921")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33921_Import() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
			// Create CSV File with Lead Details
			String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_33921.csv";
			System.out.println(filePath);
			List<Lead> leadList = new ArrayList<Lead>();

			for (int j = 0; j <= 0; j++) {
				Lead leadImport = new Lead();
				
				leadImport.setFirstName("LeadSource_Import_33921");
				leadImport.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadImport.setEmail("frantest2017@gmail.com");
				leadImport.setDivision(addDivision3.getDivisionName());
				leadImport.setCountry("USA");
				// leadApi.setStateProvince("Florida");
				// leadApi.setCounty("Gulf");
				leadImport.setZipPostalCode("564565");
				leadImport.setBasedonAssignmentRules("yes");
				leadImport.setLeadSourceCategory("Friends");
				leadImport.setLeadSourceDetails("Friends");
				
				
				leadList.add(leadImport);
			}

			common.writeCsvFile(filePath, leadList);

			// Import the CSV file
			Import importlead = new Import();
			ImportTest importTest = new ImportTest(driver);

			// Navigate to Import Tab
			fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

			importlead.setImportType("Leads");
			importlead.setSpecifyFileFormat("CSV");
			importlead.setSalesDataFile(filePath);
			importlead.setLeadStatus("New Lead");
			importlead.setLeadSourceCategory("Friends");
			importlead.setLeadSourceDetails("Friends");

			importTest.importLeadsCSV(importlead);

			// Validation
			Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

			Lead x = new Lead();
			List<String> leadsFirstNames = map.get("FN");
			List<String> leadsLastNames = map.get("LN");

			for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
				System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
				x.setFirstName(leadsFirstNames.get(a));
				x.setLastName(leadsLastNames.get(b));

				String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
				String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
				System.out
						.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

				// Validation
				if (!ownerAssignedToLeadx.equalsIgnoreCase("FranConnect Administrator")) {
					System.out.println("Owner should be ::: FranConnect Administrator");
					fc.utobj().throwsException("Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33921 - Import - FAILED");
				}
			}
		
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}

// ====================================================================================================================================================================================================	


	@Test(priority = 139 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_54-Api", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33920")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33920_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

			try {
								 
			// Through API
			// ------------------------------------------------------------------------------------------------------------------------------------------------------------
			for (int j = 0; j <= 0; j++) {
				Lead leadApi = new Lead();
				leadApi.setFirstName("LeadSource_Api_33920");
				leadApi.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				// leadApi.setDivision(addDivision2.getDivisionName());
				leadApi.setCountry("USA");
				leadApi.setStateProvince("Florida");
				// leadApi.setCounty("Gulf");
				leadApi.setZipPostalCode("564565");
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory("Friends");
				leadApi.setLeadSourceDetails("Friends");
				
				common.addLeadThroughWebServices(null, leadApi);
				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(null, leadApi);
				// System.out.println(xmlApi);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println(
						"Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
				// Validation
				// Validate Owner Assignment
				// Validation
				if (!ownerAssignedToLeadApi.equalsIgnoreCase("FranConnect Administrator")) {
					System.out.println("Owner should be ::: FranConnect Administrator");
					fc.utobj().throwsException(
							"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33920 - API - FAILED");
				}
			}
			
			fc.utobj().logoutAndQuitBrowser(null, testCaseId);
			
		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(null, e, testCaseId);

			/*System.out.println(e.getMessage());
			fc.utobj().throwsException1(e.getMessage());*/				
		}
	}
		
	
	@Test(priority = 140 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_54-WebForm", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33920")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33920_WebForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			// Through WebForm
			// ----------------------------------------------------------------------------------------------------------------------------------------------------------
			// Create WebForm
			ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
			ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

			fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
			common.set_ManageWebform(manageWebFormGenerator);
			manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
			manageWebFormGenerator.setLeadSourceDetails("None");
		// 	manageWebFormGenerator.setDivision(addDivision1.getDivisionName());
			manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

			// Add Lead - WebForm
			for (int j = 0; j <= 0; j++) {
				Lead leadWebForm = new Lead();
				
				leadWebForm.setFirstName("LeadSource_WebForm_33920");
				leadWebForm.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadWebForm.setEmail("frantest2017@gmail.com");
				// leadApi.setDivision(addDivision2.getDivisionName());
				leadWebForm.setCountry("USA");
				leadWebForm.setStateProvince("Florida");
				// leadApi.setCounty("Gulf");
				leadWebForm.setZipPostalCode("564565");
				// leadWebForm.setBasedonAssignmentRules("yes");
				leadWebForm.setLeadSourceCategory("Friends");
				leadWebForm.setLeadSourceDetails("Friends");
				
				// Add Lead through WebForm
				manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm);

				// fc.utobj().sleep();
				String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
				String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
				System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>"
						+ ownerAssignedTowebformLead);
				// Validation
				if (!ownerAssignedTowebformLead.equalsIgnoreCase("FranConnect Administrator")) {
					System.out.println("Owner should be ::: FranConnect Administrator");
					fc.utobj().throwsException(
							"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33920 - WebForm - FAILED");
				}
			}
			
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}
	
	@Test(priority = 141 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_54-Import", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33920")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33920_Import() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
						// Create CSV File with Lead Details
						String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_33920.csv";
						System.out.println(filePath);
						List<Lead> leadList = new ArrayList<Lead>();

						for (int j = 0; j <= 0; j++) {
							Lead leadImport = new Lead();
							
							leadImport.setFirstName("LeadSource_Import_33920");
							leadImport.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
							leadImport.setEmail("frantest2017@gmail.com");
							// leadApi.setDivision(addDivision2.getDivisionName());
							leadImport.setCountry("USA");
							leadImport.setStateProvince("Florida");
							// leadApi.setCounty("Gulf");
							leadImport.setZipPostalCode("564565");
							leadImport.setBasedonAssignmentRules("yes");
							leadImport.setLeadSourceCategory("Friends");
							leadImport.setLeadSourceDetails("Friends");
							
							leadList.add(leadImport);
						}

						common.writeCsvFile(filePath, leadList);

						// Import the CSV file
						Import importlead = new Import();
						ImportTest importTest = new ImportTest(driver);

						// Navigate to Import Tab
						fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

						importlead.setImportType("Leads");
						importlead.setSpecifyFileFormat("CSV");
						importlead.setSalesDataFile(filePath);
						importlead.setLeadStatus("New Lead");
						importlead.setLeadSourceCategory("Friends");
						importlead.setLeadSourceDetails("Friends");

						importTest.importLeadsCSV(importlead);

						// Validation
						Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

						Lead x = new Lead();
						List<String> leadsFirstNames = map.get("FN");
						List<String> leadsLastNames = map.get("LN");

						for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
							System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
							x.setFirstName(leadsFirstNames.get(a));
							x.setLastName(leadsLastNames.get(b));

							String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
							String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
							System.out
									.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

							// Validation
							if (!ownerAssignedToLeadx.equalsIgnoreCase("FranConnect Administrator")) {
								System.out.println("Owner should be ::: FranConnect Administrator");
								fc.utobj().throwsException(
										"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33920 - Import - FAILED");
							}
						}
						

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}
	
// ============================================================================================================================================================================================================
	
	@Test(priority = 142 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_55-Api", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33919")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33919_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

			try {
								 
			// Through API
			// ------------------------------------------------------------------------------------------------------------------------------------------------------------
			for (int j = 0; j <= 0; j++) {
				Lead leadApi = new Lead();
				leadApi.setFirstName("LeadSource_Api_33919");
				leadApi.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				// leadApi.setDivision(addDivision2.getDivisionName());
				leadApi.setCountry("USA");
				leadApi.setStateProvince("Florida");
				leadApi.setCounty("Gulf");
				// leadApi.setZipPostalCode("564565");
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory("Friends");
				leadApi.setLeadSourceDetails("Friends");
				
				common.addLeadThroughWebServices(null, leadApi);
				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(null, leadApi);
				// System.out.println(xmlApi);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println(
						"Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
				// Validation
				// Validate Owner Assignment
				// Validation
				if (!ownerAssignedToLeadApi.equalsIgnoreCase("FranConnect Administrator")) {
					System.out.println("Owner should be ::: FranConnect Administrator");
					fc.utobj().throwsException(
							"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33919 - API - FAILED");
				}
			}
			
			fc.utobj().logoutAndQuitBrowser(null, testCaseId);
			

		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(null, e, testCaseId);

			/*System.out.println(e.getMessage());
			fc.utobj().throwsException1(e.getMessage());	*/			
		}
	}
	
	@Test(priority = 143 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_55-WebForm", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33919")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33919_WebForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			// Through WebForm
						// ----------------------------------------------------------------------------------------------------------------------------------------------------------
						// Create WebForm
						ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
						ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

						fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
						common.set_ManageWebform(manageWebFormGenerator);
						manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
						manageWebFormGenerator.setLeadSourceDetails("None");
					//	manageWebFormGenerator.setDivision(addDivision1.getDivisionName());
						manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

						// Add Lead - WebForm
						for (int j = 0; j <= 0; j++) {
							Lead leadWebForm = new Lead();
							
							leadWebForm.setFirstName("LeadSource_WebForm_33919");
							leadWebForm.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
							leadWebForm.setEmail("frantest2017@gmail.com");
							// leadApi.setDivision(addDivision2.getDivisionName());
							leadWebForm.setCountry("USA");
							leadWebForm.setStateProvince("Florida");
							leadWebForm.setCounty("Gulf");
							// leadApi.setZipPostalCode("564565");
							// leadWebForm.setBasedonAssignmentRules("yes");
							leadWebForm.setLeadSourceCategory("Friends");
							leadWebForm.setLeadSourceDetails("Friends");
							
							// Add Lead through WebForm
							manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm);

							// fc.utobj().sleep();
							String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
							String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
							System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>"
									+ ownerAssignedTowebformLead);
							// Validation
							if (!ownerAssignedTowebformLead.equalsIgnoreCase("FranConnect Administrator")) {
								System.out.println("Owner should be ::: FranConnect Administrator");
								fc.utobj().throwsException(
										"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33919 - WebForm - FAILED");
							}
						}
						

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}
	
	@Test(priority =144,dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_55-Import", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33919")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33919_Import() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
			// Create CSV File with Lead Details
			String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_33919.csv";
			System.out.println(filePath);
			List<Lead> leadList = new ArrayList<Lead>();

			for (int j = 0; j <= 0; j++) {
				Lead leadImport = new Lead();
				
				leadImport.setFirstName("LeadSource_Import_33919");
				leadImport.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadImport.setEmail("frantest2017@gmail.com");
				// leadApi.setDivision(addDivision2.getDivisionName());
				leadImport.setCountry("USA");
				leadImport.setStateProvince("Florida");
				leadImport.setCounty("Gulf");
				// leadApi.setZipPostalCode("564565");
				leadImport.setBasedonAssignmentRules("yes");
				leadImport.setLeadSourceCategory("Friends");
				leadImport.setLeadSourceDetails("Friends");
				
				leadList.add(leadImport);
			}

			common.writeCsvFile(filePath, leadList);

			// Import the CSV file
			Import importlead = new Import();
			ImportTest importTest = new ImportTest(driver);

			// Navigate to Import Tab
			fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

			importlead.setImportType("Leads");
			importlead.setSpecifyFileFormat("CSV");
			importlead.setSalesDataFile(filePath);
			importlead.setLeadStatus("New Lead");
			importlead.setLeadSourceCategory("Friends");
			importlead.setLeadSourceDetails("Friends");

			importTest.importLeadsCSV(importlead);

			// Validation
			Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

			Lead x = new Lead();
			List<String> leadsFirstNames = map.get("FN");
			List<String> leadsLastNames = map.get("LN");

			for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
				System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
				x.setFirstName(leadsFirstNames.get(a));
				x.setLastName(leadsLastNames.get(b));

				String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
				String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
				System.out
						.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

				// Validation
				if (!ownerAssignedToLeadx.equalsIgnoreCase("FranConnect Administrator")) {
					System.out.println("Owner should be ::: FranConnect Administrator");
					fc.utobj().throwsException(
							"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33919 - Import - FAILED");
				}
			}
			

			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}
	
// ============================================================================================================================================================================================================
	

	@Test(priority =145,dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_56 -Api", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33918") 
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33918_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

			try {
								 
			// Through API
			// ------------------------------------------------------------------------------------------------------------------------------------------------------------
			for (int j = 0; j <= 0; j++) {
				Lead leadApi = new Lead();
				leadApi.setFirstName("LeadSource_Api_33918");
				leadApi.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				// leadApi.setDivision(addDivision2.getDivisionName());
				leadApi.setCountry("USA");
				// leadApi.setStateProvince("Florida");
				leadApi.setCounty("Gulf");
				leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory("Friends");
				leadApi.setLeadSourceDetails("Friends");
				
				common.addLeadThroughWebServices(null, leadApi);
				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(null, leadApi);
				// System.out.println(xmlApi);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println(
						"Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
				// Validation
				// Validate Owner Assignment
				// Validation
				if (!ownerAssignedToLeadApi.equalsIgnoreCase("FranConnect Administrator")) {
					System.out.println("Owner should be ::: FranConnect Administrator");
					fc.utobj().throwsException(
							"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33918 - API - FAILED");
				}
			}
			
			fc.utobj().logoutAndQuitBrowser(null, testCaseId);
			
		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(null, e, testCaseId);

		/*	System.out.println(e.getMessage());
			fc.utobj().throwsException1(e.getMessage());	*/			
		}
	}
	
	@Test(priority =146,dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_56 -WebForm", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33918")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33918_WebForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			// Through WebForm
			// ----------------------------------------------------------------------------------------------------------------------------------------------------------
			// Create WebForm
			ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
			ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

			fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
			common.set_ManageWebform(manageWebFormGenerator);
			manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
			manageWebFormGenerator.setLeadSourceDetails("None");
			// manageWebFormGenerator.setDivision(addDivision1.getDivisionName());
			manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

			// Add Lead - WebForm
			for (int j = 0; j <= 0; j++) {
				Lead leadWebForm = new Lead();
				
				leadWebForm.setFirstName("LeadSource_WebForm_33918");
				leadWebForm.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadWebForm.setEmail("frantest2017@gmail.com");
				// leadApi.setDivision(addDivision2.getDivisionName());
				leadWebForm.setCountry("USA");
				// leadApi.setStateProvince("Florida");
				leadWebForm.setCounty("Gulf");
				leadWebForm.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				// leadWebForm.setBasedonAssignmentRules("yes");
				leadWebForm.setLeadSourceCategory("Friends");
				leadWebForm.setLeadSourceDetails("Friends");
				
				
				// Add Lead through WebForm
				manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm);

				// fc.utobj().sleep();
				String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
				String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
				System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>"
						+ ownerAssignedTowebformLead);
				// Validation
				if (!ownerAssignedTowebformLead.equalsIgnoreCase("FranConnect Administrator")) {
					System.out.println("Owner should be ::: FranConnect Administrator");
					fc.utobj().throwsException(
							"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33918 - WebForm - FAILED");
				}
			}
			

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}
	
	
	@Test(priority =147,dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_56-Import", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33918")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33918_Import() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
			// Create CSV File with Lead Details
			String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_33918.csv";
			System.out.println(filePath);
			List<Lead> leadList = new ArrayList<Lead>();

			for (int j = 0; j <= 0; j++) {
				Lead leadImport = new Lead();
				
				leadImport.setFirstName("LeadSource_Import_33918");
				leadImport.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadImport.setEmail("frantest2017@gmail.com");
				// leadApi.setDivision(addDivision2.getDivisionName());
				leadImport.setCountry("USA");
				// leadApi.setStateProvince("Florida");
				leadImport.setCounty("Gulf");
				leadImport.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadImport.setBasedonAssignmentRules("yes");
				leadImport.setLeadSourceCategory("Friends");
				leadImport.setLeadSourceDetails("Friends");
				
				
				leadList.add(leadImport);
			}

			common.writeCsvFile(filePath, leadList);

			// Import the CSV file
			Import importlead = new Import();
			ImportTest importTest = new ImportTest(driver);

			// Navigate to Import Tab
			fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

			importlead.setImportType("Leads");
			importlead.setSpecifyFileFormat("CSV");
			importlead.setSalesDataFile(filePath);
			importlead.setLeadStatus("New Lead");
			importlead.setLeadSourceCategory("Friends");
			importlead.setLeadSourceDetails("Friends");

			importTest.importLeadsCSV(importlead);

			// Validation
			Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

			Lead x = new Lead();
			List<String> leadsFirstNames = map.get("FN");
			List<String> leadsLastNames = map.get("LN");

			for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
				System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
				x.setFirstName(leadsFirstNames.get(a));
				x.setLastName(leadsLastNames.get(b));

				String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
				String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
				System.out
						.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

				// Validation
				if (!ownerAssignedToLeadx.equalsIgnoreCase("FranConnect Administrator")) {
					System.out.println("Owner should be ::: FranConnect Administrator");
					fc.utobj().throwsException(
							"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33918 - Import - FAILED");
				}
			}
			


			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}
	
// ============================================================================================================================================================================================================	
		
	@Test(priority =148,dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_57-Api", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33917")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33917_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

			try {
								 
			// Through API
			// ------------------------------------------------------------------------------------------------------------------------------------------------------------
			for (int j = 0; j <= 0; j++) {
				Lead leadApi = new Lead();
				
				leadApi.setFirstName("LeadSource_Api_33917");
				leadApi.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				// leadApi.setDivision(addDivision2.getDivisionName());
				leadApi.setCountry("USA");
				leadApi.setStateProvince("Florida");
				leadApi.setCounty("Gulf");
				leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory("Friends");
				leadApi.setLeadSourceDetails("Friends");
				
				common.addLeadThroughWebServices(null, leadApi);
				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(null, leadApi);
				// System.out.println(xmlApi);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println(
						"Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
				// Validation
				// Validate Owner Assignment
				// Validation
				if (!ownerAssignedToLeadApi.equalsIgnoreCase("FranConnect Administrator")) {
					System.out.println("Owner should be ::: FranConnect Administrator");
					fc.utobj().throwsException(
							"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33917 - API - FAILED");
				}
			}
			

			fc.utobj().logoutAndQuitBrowser(null, testCaseId);
			
		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(null, e, testCaseId);

			/*System.out.println(e.getMessage());
			fc.utobj().throwsException1(e.getMessage());	*/			
		}
	}
	
	@Test(priority =149,dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_57-WebForm", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33917")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33917_WebForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			// Through WebForm
			// ----------------------------------------------------------------------------------------------------------------------------------------------------------
			// Create WebForm
			ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
			ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

			fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
			common.set_ManageWebform(manageWebFormGenerator);
			manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
			manageWebFormGenerator.setLeadSourceDetails("None");
			// manageWebFormGenerator.setDivision(addDivision1.getDivisionName());
			manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

			// Add Lead - WebForm
			for (int j = 0; j <= 0; j++) {
				Lead leadWebForm = new Lead();

				leadWebForm.setFirstName("LeadSource_WebForm_33917");
				leadWebForm.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadWebForm.setEmail("frantest2017@gmail.com");
				// leadApi.setDivision(addDivision2.getDivisionName());
				leadWebForm.setCountry("USA");
				leadWebForm.setStateProvince("Florida");
				leadWebForm.setCounty("Gulf");
				leadWebForm.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				// leadWebForm.setBasedonAssignmentRules("yes");
				leadWebForm.setLeadSourceCategory("Friends");
				leadWebForm.setLeadSourceDetails("Friends");
				

				// Add Lead through WebForm
				manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm);

				// fc.utobj().sleep();
				String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
				String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
				System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>"
						+ ownerAssignedTowebformLead);
				// Validation
				if (!ownerAssignedTowebformLead.equalsIgnoreCase("FranConnect Administrator")) {
					System.out.println("Owner should be ::: FranConnect Administrator");
					fc.utobj().throwsException(
							"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33917 - WebForm - FAILED");
				}
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}

	@Test(priority =150,dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
			"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_57-Import", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33917")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33917_Import() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
			// Create CSV File with Lead Details
			String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_33917.csv";
			System.out.println(filePath);
			List<Lead> leadList = new ArrayList<Lead>();

			for (int j = 0; j <= 0; j++) {
				Lead leadImport = new Lead();
				
				leadImport.setFirstName("LeadSource_Import_33917");
				leadImport.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadImport.setEmail("frantest2017@gmail.com");
				// leadApi.setDivision(addDivision2.getDivisionName());
				leadImport.setCountry("USA");
				leadImport.setStateProvince("Florida");
				leadImport.setCounty("Gulf");
				leadImport.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadImport.setBasedonAssignmentRules("yes");
				leadImport.setLeadSourceCategory("Friends");
				leadImport.setLeadSourceDetails("Friends");
				
				
				leadList.add(leadImport);
			}

			common.writeCsvFile(filePath, leadList);

			// Import the CSV file
			Import importlead = new Import();
			ImportTest importTest = new ImportTest(driver);

			// Navigate to Import Tab
			fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

			importlead.setImportType("Leads");
			importlead.setSpecifyFileFormat("CSV");
			importlead.setSalesDataFile(filePath);
			importlead.setLeadStatus("New Lead");
			importlead.setLeadSourceCategory("Friends");
			importlead.setLeadSourceDetails("Friends");

			importTest.importLeadsCSV(importlead);

			// Validation
			Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

			Lead x = new Lead();
			List<String> leadsFirstNames = map.get("FN");
			List<String> leadsLastNames = map.get("LN");

			for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
				System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
				x.setFirstName(leadsFirstNames.get(a));
				x.setLastName(leadsLastNames.get(b));

				String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
				String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
				System.out
						.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

				// Validation
				if (!ownerAssignedToLeadx.equalsIgnoreCase("FranConnect Administrator")) {
					System.out.println("Owner should be ::: FranConnect Administrator");
					fc.utobj().throwsException(
							"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33917 - Import - FAILED");
				}
			}
				
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}
	
// ============================================================================================================================================================================================================	
	
	@Test(priority = 151 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_58-Api", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33916")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33916_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

			try {
								 
			// Through API
			// ------------------------------------------------------------------------------------------------------------------------------------------------------------
			for (int j = 0; j <= 0; j++) {
				Lead leadApi = new Lead();
				
				leadApi.setFirstName("LeadSource_Api_33916");
				leadApi.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				// leadApi.setDivision(addDivision2.getDivisionName());
				leadApi.setCountry("USA");
				leadApi.setStateProvince("Florida");
				leadApi.setCounty("Gulf");
				leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
				leadApi.setLeadSourceDetails("None");
				
				common.addLeadThroughWebServices(null, leadApi);
				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(null, leadApi);
				// System.out.println(xmlApi);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println(
						"Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
				// Validation
				// Validate Owner Assignment
				// Validation
				if (!corpUsersForSalesTerritoriesZip.contains(ownerAssignedToLeadApi)) {
					System.out.println("Owner should be among ::: " + corpUsersForSalesTerritoriesZip);
					fc.utobj().throwsException(
							"Owner Assignment - Assign Lead Owner by Lead Source - TC-33916 - API - FAILED");
				}
			}
			
			fc.utobj().logoutAndQuitBrowser(null, testCaseId);
			
		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(null, e, testCaseId);

			/*System.out.println(e.getMessage());
			fc.utobj().throwsException1(e.getMessage());	*/			
		}
	}
	
	
	@Test(priority =152 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_58-WebForm", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33916")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33916_WebForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			try {
				
				 driver = fc.loginpage().login(driver);
				 
					// Through WebForm
					// ----------------------------------------------------------------------------------------------------------------------------------------------------------
					// Create WebForm
					ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
					ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

					fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
					common.set_ManageWebform(manageWebFormGenerator);
					manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
					manageWebFormGenerator.setLeadSourceDetails("None");
				//	manageWebFormGenerator.setDivision(addDivision1.getDivisionName());
					manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

					// Add Lead - WebForm
					for (int j = 0; j <= 0; j++) {
						Lead leadWebForm = new Lead();

						leadWebForm.setFirstName("LeadSource_WebForm_33916");
						leadWebForm.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
						leadWebForm.setEmail("frantest2017@gmail.com");
						// leadApi.setDivision(addDivision2.getDivisionName());
						leadWebForm.setCountry("USA");
						leadWebForm.setStateProvince("Florida");
						leadWebForm.setCounty("Gulf");
						leadWebForm.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
						// leadWebForm.setBasedonAssignmentRules("yes");
						leadWebForm.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
						leadWebForm.setLeadSourceDetails("None");
						

						// Add Lead through WebForm
						manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm);

						// fc.utobj().sleep();
						String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
						String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
						System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>"
								+ ownerAssignedTowebformLead);
						// Validation
						if (!corpUsersForSalesTerritoriesZip.contains(ownerAssignedTowebformLead)) {
							System.out.println("Owner should be among ::: " + corpUsersForSalesTerritoriesZip);
							fc.utobj().throwsException(
									"Owner Assignment - Assign Lead Owner by Lead Source - TC-33916 - WebForm - FAILED");
						}
					}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			
		}
	}
	
	
	@Test(priority = 153 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_58-Import", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33916")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33916_Import() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			try {
				
				 driver = fc.loginpage().login(driver);
				 
					// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
					// Create CSV File with Lead Details
					String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_33916.csv";
					System.out.println(filePath);
					List<Lead> leadList = new ArrayList<Lead>();

					for (int j = 0; j <= 0; j++) {
						Lead leadImport = new Lead();
						
						leadImport.setFirstName("LeadSource_Import_33916");
						leadImport.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
						leadImport.setEmail("frantest2017@gmail.com");
						// leadApi.setDivision(addDivision2.getDivisionName());
						leadImport.setCountry("USA");
						leadImport.setStateProvince("Florida");
						leadImport.setCounty("Gulf");
						leadImport.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
						leadImport.setBasedonAssignmentRules("yes");
						leadImport.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
						leadImport.setLeadSourceDetails("None");
						
						leadList.add(leadImport);
					}

					common.writeCsvFile(filePath, leadList);

					// Import the CSV file
					Import importlead = new Import();
					ImportTest importTest = new ImportTest(driver);

					// Navigate to Import Tab
					fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

					importlead.setImportType("Leads");
					importlead.setSpecifyFileFormat("CSV");
					importlead.setSalesDataFile(filePath);
					importlead.setLeadStatus("New Lead");
					importlead.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
					importlead.setLeadSourceDetails("None");

					importTest.importLeadsCSV(importlead);

					// Validation
					Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

					Lead x = new Lead();
					List<String> leadsFirstNames = map.get("FN");
					List<String> leadsLastNames = map.get("LN");

					for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
						System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
						x.setFirstName(leadsFirstNames.get(a));
						x.setLastName(leadsLastNames.get(b));

						String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
						String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
						System.out
								.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

						// Validation
						if (!corpUsersForSalesTerritoriesZip.contains(ownerAssignedToLeadx)) {
							System.out.println("Owner should be among ::: " + corpUsersForSalesTerritoriesZip);
							fc.utobj().throwsException(
									"Owner Assignment - Assign Lead Owner by Lead Source - TC-33916 - Import - FAILED");
						}
					}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			
		}
	}
	
// =============================================================================================================================================================================================================	
	
	@Test(priority =154 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_59-Api", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33915")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33915_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

			try {
								 
			// Through API
			// ------------------------------------------------------------------------------------------------------------------------------------------------------------
			for (int j = 0; j <= 0; j++) {
				Lead leadApi = new Lead();
				
				leadApi.setFirstName("LeadSource_Api_33915");
				leadApi.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				leadApi.setDivision(addDivision3.getDivisionName());
				leadApi.setCountry("USA");
				leadApi.setStateProvince("Florida");
				leadApi.setCounty("Gulf");
				leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory("Friends");
				leadApi.setLeadSourceDetails("Friends");
				
				common.addLeadThroughWebServices(null, leadApi);
				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(null, leadApi);
				// System.out.println(xmlApi);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println(
						"Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
				// Validation
				// Validate Owner Assignment
				// Validation
				if (!ownerAssignedToLeadApi.equalsIgnoreCase("FranConnect Administrator")) {
					System.out.println("Owner should be ::: FranConnect Administrator");
					fc.utobj().throwsException(
							"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33915 - API - FAILED");
				}
			}
			
			fc.utobj().logoutAndQuitBrowser(null, testCaseId);
			
		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(null, e, testCaseId);

			/*System.out.println(e.getMessage());
			fc.utobj().throwsException1(e.getMessage());	*/			
		}
	}
	
	@Test(priority = 155 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_59-WebForm", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33915")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33915_WebForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			try {
				
				 driver = fc.loginpage().login(driver);
				 
				// Through WebForm
					// ----------------------------------------------------------------------------------------------------------------------------------------------------------
					// Create WebForm
					ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
					ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

					fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
					common.set_ManageWebform(manageWebFormGenerator);
					manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
					manageWebFormGenerator.setLeadSourceDetails("None");
					manageWebFormGenerator.setDivision(addDivision3.getDivisionName());
					manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

					// Add Lead - WebForm
					for (int j = 0; j <= 0; j++) {
						Lead leadWebForm = new Lead();

						leadWebForm.setFirstName("LeadSource_WebForm_33915");
						leadWebForm.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
						leadWebForm.setEmail("frantest2017@gmail.com");
					//	leadWebForm.setDivision(addDivision2.getDivisionName());
						leadWebForm.setCountry("USA");
						leadWebForm.setStateProvince("Florida");
						leadWebForm.setCounty("Gulf");
						leadWebForm.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
						// leadWebForm.setBasedonAssignmentRules("yes");
						leadWebForm.setLeadSourceCategory("Friends");
						leadWebForm.setLeadSourceDetails("Friends");
						

						// Add Lead through WebForm
						manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm);

						// fc.utobj().sleep();
						String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
						String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
						System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>"
								+ ownerAssignedTowebformLead);
						// Validation
						if (!ownerAssignedTowebformLead.equalsIgnoreCase("FranConnect Administrator")) {
							System.out.println("Owner should be ::: FranConnect Administrator");
							fc.utobj().throwsException(
									"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33915 - WebForm - FAILED");
						}
					}
		

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			
		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			
		}
	}
	
	@Test(priority = 156 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_59-Import", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33915")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33915_Import() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			try {
				
				 driver = fc.loginpage().login(driver);
				 
				// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
					// Create CSV File with Lead Details
					String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_33915.csv";
					System.out.println(filePath);
					List<Lead> leadList = new ArrayList<Lead>();

					for (int j = 0; j <= 0; j++) {
						Lead leadImport = new Lead();
						
						leadImport.setFirstName("LeadSource_Import_33915");
						leadImport.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
						leadImport.setEmail("frantest2017@gmail.com");
						leadImport.setDivision(addDivision3.getDivisionName());
						leadImport.setCountry("USA");
						leadImport.setStateProvince("Florida");
						leadImport.setCounty("Gulf");
						leadImport.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
						leadImport.setBasedonAssignmentRules("yes");
						leadImport.setLeadSourceCategory("Friends");
						leadImport.setLeadSourceDetails("Friends");
						
						
						leadList.add(leadImport);
					}

					common.writeCsvFile(filePath, leadList);

					// Import the CSV file
					Import importlead = new Import();
					ImportTest importTest = new ImportTest(driver);

					// Navigate to Import Tab
					fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

					importlead.setImportType("Leads");
					importlead.setSpecifyFileFormat("CSV");
					importlead.setSalesDataFile(filePath);
					importlead.setLeadStatus("New Lead");
					importlead.setLeadSourceCategory("Friends");
					importlead.setLeadSourceDetails("Friends");

					importTest.importLeadsCSV(importlead);

					// Validation
					Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

					Lead x = new Lead();
					List<String> leadsFirstNames = map.get("FN");
					List<String> leadsLastNames = map.get("LN");

					for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
						System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
						x.setFirstName(leadsFirstNames.get(a));
						x.setLastName(leadsLastNames.get(b));

						String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
						String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
						System.out
								.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

						// Validation
						if (!ownerAssignedToLeadx.equalsIgnoreCase("FranConnect Administrator")) {
							System.out.println("Owner should be ::: FranConnect Administrator");
							fc.utobj().throwsException(
									"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33915 - Import - FAILED");
						}
					}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			
		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			
		}
	}
	
// =============================================================================================================================================================================================================	
	
	
	@Test(priority = 157 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_60-Api", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source_TC-33914")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33914_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

			try {
								 
			// Through API
			// ------------------------------------------------------------------------------------------------------------------------------------------------------------
			for (int j = 0; j <= 0; j++) {
				Lead leadApi = new Lead();
				
				leadApi.setFirstName("LeadSource_Api_33914");
				leadApi.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				//leadApi.setDivision(addDivision2.getDivisionName());
				leadApi.setCountry("USA");
				//leadApi.setStateProvince("Florida");
				//leadApi.setCounty("Gulf");
				//leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
				leadApi.setLeadSourceDetails("None");
				
				common.addLeadThroughWebServices(null, leadApi);
				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(null, leadApi);
				// System.out.println(xmlApi);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println(
						"Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
				// Validation
				// Validate Owner Assignment
				// Validation
				if (!corpUsersForSource2.contains(ownerAssignedToLeadApi)) {
					System.out.println("Owner should be among ::: " + corpUsersForSource2);
					fc.utobj().throwsException(
							"Owner Assignment - Assign Lead Owner by Lead Source - TC-33914 - API - FAILED");
				}
			}
			
			fc.utobj().logoutAndQuitBrowser(null, testCaseId);
			
		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(null, e, testCaseId);

			/*System.out.println(e.getMessage());
			fc.utobj().throwsException1(e.getMessage());*/				
		}
	}
	
	
	@Test(priority = 158 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" , "Sales_OwnerAssignment_60_WebForm" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_60-WebForm", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33914")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33914_WebForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			try {
				
				 driver = fc.loginpage().login(driver);
				
				// Through WebForm
					// ----------------------------------------------------------------------------------------------------------------------------------------------------------
					// Create WebForm
					ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
					ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

					fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
					common.set_ManageWebform(manageWebFormGenerator);
					manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
					manageWebFormGenerator.setLeadSourceDetails("None");
				//	manageWebFormGenerator.setDivision(addDivision1.getDivisionName());
					manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

					// Add Lead - WebForm
					for (int j = 0; j <= 0; j++) {
						Lead leadWebForm = new Lead();

						leadWebForm.setFirstName("LeadSource_WebForm_33914");
						leadWebForm.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
						leadWebForm.setEmail("frantest2017@gmail.com");
						//leadApi.setDivision(addDivision2.getDivisionName());
						leadWebForm.setCountry("USA");
						//leadApi.setStateProvince("Florida");
						//leadApi.setCounty("Gulf");
						//leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
						// leadWebForm.setBasedonAssignmentRules("yes");
						leadWebForm.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
						leadWebForm.setLeadSourceDetails("None");
						

						// Add Lead through WebForm
						manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm);

						// fc.utobj().sleep();
						String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
						String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
						System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>"
								+ ownerAssignedTowebformLead);
						// Validation
						
						if (!corpUsersForSource2.contains(ownerAssignedTowebformLead)) {
							System.out.println("Owner should be among ::: " + corpUsersForSource2);

							fc.utobj().throwsException(
									"Owner Assignment - Assign Lead Owner by Lead Source - TC-33914 - WebForm - FAILED");
						}
					}
			
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			
		}
	}
	
	
	@Test(priority = 159 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_60-Import", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33914")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33914_Import() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			try {
				
				 driver = fc.loginpage().login(driver);
				 
				// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
					// Create CSV File with Lead Details
					String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_33914.csv";
					System.out.println(filePath);
					List<Lead> leadList = new ArrayList<Lead>();

					for (int j = 0; j <= 0; j++) {
						Lead leadImport = new Lead();
						
						leadImport.setFirstName("LeadSource_Import_33914");
						leadImport.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
						leadImport.setEmail("frantest2017@gmail.com");
						//leadApi.setDivision(addDivision2.getDivisionName());
						leadImport.setCountry("USA");
						//leadApi.setStateProvince("Florida");
						//leadApi.setCounty("Gulf");
						//leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
						leadImport.setBasedonAssignmentRules("yes");
						leadImport.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
						leadImport.setLeadSourceDetails("None");
						
						leadList.add(leadImport);
					}

					common.writeCsvFile(filePath, leadList);

					// Import the CSV file
					Import importlead = new Import();
					ImportTest importTest = new ImportTest(driver);

					// Navigate to Import Tab
					fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

					importlead.setImportType("Leads");
					importlead.setSpecifyFileFormat("CSV");
					importlead.setSalesDataFile(filePath);
					importlead.setLeadStatus("New Lead");
					importlead.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
					importlead.setLeadSourceDetails("None");

					importTest.importLeadsCSV(importlead);

					// Validation
					Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

					Lead x = new Lead();
					List<String> leadsFirstNames = map.get("FN");
					List<String> leadsLastNames = map.get("LN");

					for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
						System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
						x.setFirstName(leadsFirstNames.get(a));
						x.setLastName(leadsLastNames.get(b));

						String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
						String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
						System.out
								.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

						// Validation
						if (!corpUsersForSource2.contains(ownerAssignedToLeadx)) {
							System.out.println("Owner should be among ::: " + corpUsersForSource2);

							fc.utobj().throwsException(
									"Owner Assignment - Assign Lead Owner by Lead Source - TC-33914 - Import - FAILED");
						}
					}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			
		}
	}
	
// =============================================================================================================================================================================================================	
	
	
	@Test(priority = 160 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_61-Api", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33913")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33913_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

			try {
								 
			// Through API
			// ------------------------------------------------------------------------------------------------------------------------------------------------------------
			for (int j = 0; j <= 0; j++) {
				Lead leadApi = new Lead();
				
				leadApi.setFirstName("LeadSource_Api_33913");
				leadApi.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				leadApi.setDivision(addDivision3.getDivisionName());
				leadApi.setCountry("USA");
				//leadApi.setStateProvince("Florida");
				//leadApi.setCounty("Gulf");
				//leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory("Friends");
				leadApi.setLeadSourceDetails("Friends");
				
				common.addLeadThroughWebServices(null, leadApi);
				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(null, leadApi);
				// System.out.println(xmlApi);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println(
						"Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
				// Validation
				// Validate Owner Assignment
				// Validation
				if (!ownerAssignedToLeadApi.equalsIgnoreCase("FranConnect Administrator")) {
					System.out.println("Owner should be ::: FranConnect Administrator");
					fc.utobj().throwsException(
							"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33913 - API - FAILED");
				}
			}
			
			fc.utobj().logoutAndQuitBrowser(null, testCaseId);
			

		} catch (Exception e) {
			
			 fc.utobj().quitBrowserOnCatch(null, e, testCaseId);

			/*System.out.println(e.getMessage());
			fc.utobj().throwsException1(e.getMessage());	*/			
		}
	}
	
	@Test(priority = 161 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_61-WebForm", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33913")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33913_WebForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			try {
				
				 driver = fc.loginpage().login(driver);
				 
				// Through WebForm
					// ----------------------------------------------------------------------------------------------------------------------------------------------------------
					// Create WebForm
					ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
					ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

					fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
					common.set_ManageWebform(manageWebFormGenerator);
					manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
					manageWebFormGenerator.setLeadSourceDetails("None");
					manageWebFormGenerator.setDivision(addDivision3.getDivisionName());
					manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

					// Add Lead - WebForm
					for (int j = 0; j <= 0; j++) {
						Lead leadWebForm = new Lead();

						leadWebForm.setFirstName("LeadSource_WebForm_33913");
						leadWebForm.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
						leadWebForm.setEmail("frantest2017@gmail.com");
						// leadWebForm.setDivision(addDivision2.getDivisionName());
						leadWebForm.setCountry("USA");
						//leadApi.setStateProvince("Florida");
						//leadApi.setCounty("Gulf");
						//leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
						// leadWebForm.setBasedonAssignmentRules("yes");
						leadWebForm.setLeadSourceCategory("Friends");
						leadWebForm.setLeadSourceDetails("Friends");
						

						// Add Lead through WebForm
						manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm);

						// fc.utobj().sleep();
						String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
						String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
						System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>"
								+ ownerAssignedTowebformLead);
						// Validation
						if (!ownerAssignedTowebformLead.equalsIgnoreCase("FranConnect Administrator")) {
							System.out.println("Owner should be ::: FranConnect Administrator");
							fc.utobj().throwsException(
									"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33913 - WebForm - FAILED");
						}
					}
			
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			
		}
	}
	
	@Test(priority = 162 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_61-Import", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33913")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33913_Import() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			try {
				
				 driver = fc.loginpage().login(driver);
				 
				// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
					// Create CSV File with Lead Details
					String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_33913.csv";
					System.out.println(filePath);
					List<Lead> leadList = new ArrayList<Lead>();

					for (int j = 0; j <= 0; j++) {
						Lead leadImport = new Lead();
						
						leadImport.setFirstName("LeadSource_Import_33913");
						leadImport.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
						leadImport.setEmail("frantest2017@gmail.com");
						leadImport.setDivision(addDivision3.getDivisionName());
						leadImport.setCountry("USA");
						//leadApi.setStateProvince("Florida");
						//leadApi.setCounty("Gulf");
						//leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
						leadImport.setBasedonAssignmentRules("yes");
						leadImport.setLeadSourceCategory("Friends");
						leadImport.setLeadSourceDetails("Friends");
						
						leadList.add(leadImport);
					}

					common.writeCsvFile(filePath, leadList);

					// Import the CSV file
					Import importlead = new Import();
					ImportTest importTest = new ImportTest(driver);

					// Navigate to Import Tab
					fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

					importlead.setImportType("Leads");
					importlead.setSpecifyFileFormat("CSV");
					importlead.setSalesDataFile(filePath);
					importlead.setLeadStatus("New Lead");
					importlead.setLeadSourceCategory("Friends");
					importlead.setLeadSourceDetails("Friends");

					importTest.importLeadsCSV(importlead);

					// Validation
					Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

					Lead x = new Lead();
					List<String> leadsFirstNames = map.get("FN");
					List<String> leadsLastNames = map.get("LN");

					for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
						System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
						x.setFirstName(leadsFirstNames.get(a));
						x.setLastName(leadsLastNames.get(b));

						String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
						String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
						System.out
								.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

						// Validation
						if (!ownerAssignedToLeadx.equalsIgnoreCase("FranConnect Administrator")) {
							System.out.println("Owner should be ::: FranConnect Administrator");
							fc.utobj().throwsException(
									"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33913 - Import - FAILED");
						}
					}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			
		}
	}
	
// =============================================================================================================================================================================================================	
	
	
	@Test(priority = 163 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_62-Api", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33912")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33912_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		try {
								 
			// Through API
			// ------------------------------------------------------------------------------------------------------------------------------------------------------------
			for (int j = 0; j <= 0; j++) {
				Lead leadApi = new Lead();
				
				leadApi.setFirstName("LeadSource_Api_33912");
				leadApi.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				leadApi.setDivision(addDivision3.getDivisionName());
				leadApi.setCountry("USA");
				//leadApi.setStateProvince("Florida");
				//leadApi.setCounty("Gulf");
				//leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
				leadApi.setLeadSourceDetails("None");
				
				common.addLeadThroughWebServices(null, leadApi);
				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(null, leadApi);
				// System.out.println(xmlApi);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println(
						"Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
				// Validation
				// Validate Owner Assignment
				// Validation
				if (!corpUsersForSource2.contains(ownerAssignedToLeadApi)) {
					System.out.println("Owner should be among ::: " + corpUsersForSource2);
					fc.utobj().throwsException(
							"Owner Assignment - Assign Lead Owner by Lead Source - TC-33912 - API - FAILED");
				}
			}
			
			fc.utobj().logoutAndQuitBrowser(null, testCaseId);
			
		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(null, e, testCaseId);

			/*System.out.println(e.getMessage());
			fc.utobj().throwsException1(e.getMessage());		*/		
		}
	}
	
	@Test(priority =164,dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_62-WebForm", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33912")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33912_WebForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			try {
				
				 driver = fc.loginpage().login(driver);
				 
				// Through WebForm
					// ----------------------------------------------------------------------------------------------------------------------------------------------------------
					// Create WebForm
					ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
					ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

					fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
					common.set_ManageWebform(manageWebFormGenerator);
					manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
					manageWebFormGenerator.setLeadSourceDetails("None");
					manageWebFormGenerator.setDivision(addDivision3.getDivisionName());
					manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

					// Add Lead - WebForm
					for (int j = 0; j <= 0; j++) {
						Lead leadWebForm = new Lead();

						leadWebForm.setFirstName("LeadSource_WebForm_33912");
						leadWebForm.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
						leadWebForm.setEmail("frantest2017@gmail.com");
					// leadWebForm.setDivision(addDivision2.getDivisionName());
						leadWebForm.setCountry("USA");
						//leadApi.setStateProvince("Florida");
						//leadApi.setCounty("Gulf");
						//leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
						// leadWebForm.setBasedonAssignmentRules("yes");
						leadWebForm.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
						leadWebForm.setLeadSourceDetails("None");
						

						// Add Lead through WebForm
						manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm);

						// fc.utobj().sleep();
						String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
						String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
						System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>"
								+ ownerAssignedTowebformLead);
						// Validation
						if (!corpUsersForSource2.contains(ownerAssignedTowebformLead)) {
							System.out.println("Owner should be among ::: " + corpUsersForSource2);
							fc.utobj().throwsException(
									"Owner Assignment - Assign Lead Owner by Lead Source - TC-33912 - WebForm - FAILED");
						}
					}
			
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			
		}
	}
	
	@Test(priority =165,dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_62-Import", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33912")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33912_Import() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			try {
				
				 driver = fc.loginpage().login(driver);
				 
				// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
					// Create CSV File with Lead Details
					String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_33912.csv";
					System.out.println(filePath);
					List<Lead> leadList = new ArrayList<Lead>();

					for (int j = 0; j <= 0; j++) {
						Lead leadImport = new Lead();
						
						leadImport.setFirstName("LeadSource_Import_33912");
						leadImport.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
						leadImport.setEmail("frantest2017@gmail.com");
						leadImport.setDivision(addDivision3.getDivisionName());
						leadImport.setCountry("USA");
						//leadApi.setStateProvince("Florida");
						//leadApi.setCounty("Gulf");
						//leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
						leadImport.setBasedonAssignmentRules("yes");
						leadImport.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
						leadImport.setLeadSourceDetails("None");
						
						leadList.add(leadImport);
					}

					common.writeCsvFile(filePath, leadList);

					// Import the CSV file
					Import importlead = new Import();
					ImportTest importTest = new ImportTest(driver);

					// Navigate to Import Tab
					fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

					importlead.setImportType("Leads");
					importlead.setSpecifyFileFormat("CSV");
					importlead.setSalesDataFile(filePath);
					importlead.setLeadStatus("New Lead");
					importlead.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
					importlead.setLeadSourceDetails("None");

					importTest.importLeadsCSV(importlead);

					// Validation
					Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

					Lead x = new Lead();
					List<String> leadsFirstNames = map.get("FN");
					List<String> leadsLastNames = map.get("LN");

					for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
						System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
						x.setFirstName(leadsFirstNames.get(a));
						x.setLastName(leadsLastNames.get(b));

						String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
						String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
						System.out
								.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

						// Validation
						if (!corpUsersForSource2.contains(ownerAssignedToLeadx)) {
							System.out.println("Owner should be among ::: " + corpUsersForSource2);
							fc.utobj().throwsException(
									"Owner Assignment - Assign Lead Owner by Lead Source - TC-33912 - Import - FAILED");
						}
					}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			
		}
	}
	
// =============================================================================================================================================================================================================	
	
	
	@Test(priority =166,dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_63-Api", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33911")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33911_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		try {
								 
			// Through API
			// ------------------------------------------------------------------------------------------------------------------------------------------------------------
			for (int j = 0; j <= 0; j++) {
				Lead leadApi = new Lead();
				
				leadApi.setFirstName("LeadSource_Api_33911");
				leadApi.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				leadApi.setDivision(addDivision3.getDivisionName());
				leadApi.setCountry("USA");
				//leadApi.setStateProvince("Florida");
				leadApi.setCounty("Gulf");
				//leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
				leadApi.setLeadSourceDetails("None");
				
				common.addLeadThroughWebServices(null, leadApi);
				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(null, leadApi);
				// System.out.println(xmlApi);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println(
						"Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
				// Validation
				// Validate Owner Assignment
				// Validation
				if (!corpUsersForSource2.contains(ownerAssignedToLeadApi)) {
					System.out.println("Owner should be among ::: " + corpUsersForSource2);
					fc.utobj().throwsException(
							"Owner Assignment - Assign Lead Owner by Lead Source - TC-33911 - API - FAILED");
				}
			}
			
			fc.utobj().logoutAndQuitBrowser(null, testCaseId);
			
		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(null, e, testCaseId);

			/*System.out.println(e.getMessage());
			fc.utobj().throwsException1(e.getMessage());*/				
		}
	}
	
	@Test(priority =167,dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_63-WebForm", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33911")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33911_WebForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			try {
				
				 driver = fc.loginpage().login(driver);
				 
				// Through WebForm
					// ----------------------------------------------------------------------------------------------------------------------------------------------------------
					// Create WebForm
					ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
					ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

					fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
					common.set_ManageWebform(manageWebFormGenerator);
					manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
					manageWebFormGenerator.setLeadSourceDetails("None");
					manageWebFormGenerator.setDivision(addDivision3.getDivisionName());
					manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

					// Add Lead - WebForm
					for (int j = 0; j <= 0; j++) {
						Lead leadWebForm = new Lead();

						leadWebForm.setFirstName("LeadSource_WebForm_33911");
						leadWebForm.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
						leadWebForm.setEmail("frantest2017@gmail.com");
					//	leadWebForm.setDivision(addDivision2.getDivisionName());
						leadWebForm.setCountry("USA");
						//leadApi.setStateProvince("Florida");
						leadWebForm.setCounty("Gulf");
						//leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
						// leadWebForm.setBasedonAssignmentRules("yes");
						leadWebForm.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
						leadWebForm.setLeadSourceDetails("None");
						

						// Add Lead through WebForm
						manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm);

						// fc.utobj().sleep();
						String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
						String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
						System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>"
								+ ownerAssignedTowebformLead);
						// Validation
						
						if (!corpUsersForSource2.contains(ownerAssignedTowebformLead)) {
							System.out.println("Owner should be among ::: " + corpUsersForSource2);
							fc.utobj().throwsException(
									"Owner Assignment - Assign Lead Owner by Lead Source - TC-33911 - WebForm - FAILED");
						}
					}
					
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			
		}
	}
	
	@Test(priority =168 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_63-Import", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33911")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33911_Import() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			try {
				
				 driver = fc.loginpage().login(driver);
				 
				// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
					// Create CSV File with Lead Details
					String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_33911.csv";
					System.out.println(filePath);
					List<Lead> leadList = new ArrayList<Lead>();

					for (int j = 0; j <= 0; j++) {
						Lead leadImport = new Lead();
						
						leadImport.setFirstName("LeadSource_Import_33911");
						leadImport.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
						leadImport.setEmail("frantest2017@gmail.com");
						leadImport.setDivision(addDivision3.getDivisionName());
						leadImport.setCountry("USA");
						//leadApi.setStateProvince("Florida");
						leadImport.setCounty("Gulf");
						//leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
						leadImport.setBasedonAssignmentRules("yes");
						leadImport.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
						leadImport.setLeadSourceDetails("None");
						
						leadList.add(leadImport);
					}

					common.writeCsvFile(filePath, leadList);

					// Import the CSV file
					Import importlead = new Import();
					ImportTest importTest = new ImportTest(driver);

					// Navigate to Import Tab
					fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

					importlead.setImportType("Leads");
					importlead.setSpecifyFileFormat("CSV");
					importlead.setSalesDataFile(filePath);
					importlead.setLeadStatus("New Lead");
					importlead.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
					importlead.setLeadSourceDetails("None");

					importTest.importLeadsCSV(importlead);

					// Validation
					Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

					Lead x = new Lead();
					List<String> leadsFirstNames = map.get("FN");
					List<String> leadsLastNames = map.get("LN");

					for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
						System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
						x.setFirstName(leadsFirstNames.get(a));
						x.setLastName(leadsLastNames.get(b));

						String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
						String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
						System.out
								.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

						// Validation
						
						if (!corpUsersForSource2.contains(ownerAssignedToLeadx)) {
							System.out.println("Owner should be among ::: " + corpUsersForSource2);
							fc.utobj().throwsException(
									"Owner Assignment - Assign Lead Owner by Lead Source - TC-33911 - Import - FAILED");
						}
					}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			
		}
	}
	
// =============================================================================================================================================================================================================	
	
	
	@Test(priority =169 ,dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_64-Api", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33910")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33910_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

			try {
								 
			// Through API
			// ------------------------------------------------------------------------------------------------------------------------------------------------------------
			for (int j = 0; j <= 0; j++) {
				Lead leadApi = new Lead();
				
				leadApi.setFirstName("LeadSource_Api_33910");
				leadApi.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				leadApi.setDivision(addDivision3.getDivisionName());
				leadApi.setCountry("USA");
				leadApi.setStateProvince("Florida");
				leadApi.setCounty("Gulf");
				//leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
				leadApi.setLeadSourceDetails("None");
				
				common.addLeadThroughWebServices(null, leadApi);
				
				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(null, leadApi);
				// System.out.println(xmlApi);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println(
						"Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
				// Validation
				// Validate Owner Assignment
				// Validation
				if (!corpUsersForSalesTerritoriesCounty.contains(ownerAssignedToLeadApi)) {
					System.out.println("Owner should be among ::: " + corpUsersForSalesTerritoriesCounty);
					fc.utobj().throwsException(
							"Owner Assignment - Assign Lead Owner by Lead Source - TC-33910 - API - FAILED");
				}
			}
			
			fc.utobj().logoutAndQuitBrowser(null, testCaseId);
			
		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(null, e, testCaseId);

			/*System.out.println(e.getMessage());
			fc.utobj().throwsException1(e.getMessage());*/				
		}
	}
	
	
	@Test(priority =170 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_64-WebForm", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33910")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33910_WebForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			try {
				
				 driver = fc.loginpage().login(driver);
				 
				// Through WebForm
					// ----------------------------------------------------------------------------------------------------------------------------------------------------------
					// Create WebForm
					ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
					ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

					fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
					common.set_ManageWebform(manageWebFormGenerator);
					manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
					manageWebFormGenerator.setLeadSourceDetails("None");
					manageWebFormGenerator.setDivision(addDivision3.getDivisionName());
					manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

					// Add Lead - WebForm
					for (int j = 0; j <= 0; j++) {
						Lead leadWebForm = new Lead();

						leadWebForm.setFirstName("LeadSource_WebForm_33910");
						leadWebForm.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
						leadWebForm.setEmail("frantest2017@gmail.com");
					//	leadWebForm.setDivision(addDivision2.getDivisionName());
						leadWebForm.setCountry("USA");
						leadWebForm.setStateProvince("Florida");
						leadWebForm.setCounty("Gulf");
						//leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
						// leadWebForm.setBasedonAssignmentRules("yes");
						leadWebForm.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
						leadWebForm.setLeadSourceDetails("None");
						

						// Add Lead through WebForm
						manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm);

						// fc.utobj().sleep();
						String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
						String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
						System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>"
								+ ownerAssignedTowebformLead);
						// Validation
						if (!corpUsersForSalesTerritoriesCounty.contains(ownerAssignedTowebformLead)) {
							System.out.println("Owner should be among ::: " + corpUsersForSalesTerritoriesCounty);
							fc.utobj().throwsException(
									"Owner Assignment - Assign Lead Owner by Lead Source - TC-33910 - WebForm - FAILED");
						}
					}
			
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			
		}
	}
	
	
	@Test(priority =171 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" , "sales_OwnerAssignment_failed"})
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_64-Import", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33910")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33910_Import() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			try {
				
				 driver = fc.loginpage().login(driver);
				 
				// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
					// Create CSV File with Lead Details
					String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_33910.csv";
					System.out.println(filePath);
					List<Lead> leadList = new ArrayList<Lead>();

					for (int j = 0; j <= 0; j++) {
						Lead leadImport = new Lead();
						
						leadImport.setFirstName("LeadSource_Import_33910");
						leadImport.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
						leadImport.setEmail("frantest2017@gmail.com");
						leadImport.setDivision(addDivision3.getDivisionName());
						leadImport.setCountry("USA");
						leadImport.setStateProvince("Florida");
						leadImport.setCounty("Gulf");
						//leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
						leadImport.setBasedonAssignmentRules("yes");
						leadImport.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
						leadImport.setLeadSourceDetails("None");
												
						leadList.add(leadImport);
					}

					common.writeCsvFile(filePath, leadList);

					// Import the CSV file
					Import importlead = new Import();
					ImportTest importTest = new ImportTest(driver);

					// Navigate to Import Tab
					fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

					importlead.setImportType("Leads");
					importlead.setSpecifyFileFormat("CSV");
					importlead.setSalesDataFile(filePath);
					importlead.setLeadStatus("New Lead");
					importlead.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
					importlead.setLeadSourceDetails("None");

					importTest.importLeadsCSV(importlead);

					// Validation
					Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

					Lead x = new Lead();
					List<String> leadsFirstNames = map.get("FN");
					List<String> leadsLastNames = map.get("LN");

					for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
						System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
						x.setFirstName(leadsFirstNames.get(a));
						x.setLastName(leadsLastNames.get(b));

						String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
						String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
						System.out
								.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

						// Validation
						
						if (!corpUsersForSource2.contains(ownerAssignedToLeadx)) {
							System.out.println("Owner should be among ::: " + corpUsersForSource2);
							fc.utobj().throwsException(
									"Owner Assignment - Assign Lead Owner by Lead Source - TC-33910 - Import - FAILED");
						}
					}
					
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			
		}
	}
	
// =============================================================================================================================================================================================================	
	
	
	@Test(priority = 172 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_65-Api", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33909" )
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33909_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

			try {
								 
			// Through API
			// ------------------------------------------------------------------------------------------------------------------------------------------------------------
			for (int j = 0; j <= 0; j++) {
				Lead leadApi = new Lead();
				
				leadApi.setFirstName("LeadSource_Api_33909");
				leadApi.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				leadApi.setDivision(addDivision3.getDivisionName());
				leadApi.setCountry("USA");
				// leadApi.setStateProvince("Florida");
				leadApi.setCounty("Gulf");
				leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
				leadApi.setLeadSourceDetails("None");
				
				common.addLeadThroughWebServices(null, leadApi);
				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(null, leadApi);
				// System.out.println(xmlApi);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println(
						"Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
				// Validation
				// Validate Owner Assignment
				// Validation
				if (!corpUsersForSalesTerritoriesZip.contains(ownerAssignedToLeadApi)) {
					System.out.println("Owner should be among ::: " + corpUsersForSalesTerritoriesZip);
					fc.utobj().throwsException(
							"Owner Assignment - Assign Lead Owner by Lead Source - TC-33909 - API - FAILED");
				}
			}
			
			fc.utobj().logoutAndQuitBrowser(null, testCaseId);
			
		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(null, e, testCaseId);

			/*System.out.println(e.getMessage());
			fc.utobj().throwsException1(e.getMessage());	*/			
		}
	}
	@Test(priority = 173 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_65-WebForm", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33909")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33909_WebForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			try {
				
				 driver = fc.loginpage().login(driver);
		
				// Through WebForm
					// ----------------------------------------------------------------------------------------------------------------------------------------------------------
					// Create WebForm
					ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
					ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

					fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
					common.set_ManageWebform(manageWebFormGenerator);
					manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
					manageWebFormGenerator.setLeadSourceDetails("None");
					manageWebFormGenerator.setDivision(addDivision3.getDivisionName());
					manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

					// Add Lead - WebForm
					for (int j = 0; j <= 0; j++) {
						Lead leadWebForm = new Lead();

						leadWebForm.setFirstName("LeadSource_WebForm_33909");
						leadWebForm.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
						leadWebForm.setEmail("frantest2017@gmail.com");
					//	leadWebForm.setDivision(addDivision2.getDivisionName());
						leadWebForm.setCountry("USA");
						// leadApi.setStateProvince("Florida");
						leadWebForm.setCounty("Gulf");
						leadWebForm.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
						// leadWebForm.setBasedonAssignmentRules("yes");
						leadWebForm.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
						leadWebForm.setLeadSourceDetails("None");

						// Add Lead through WebForm
						manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm);

						// fc.utobj().sleep();
						String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
						String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
						System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>"
								+ ownerAssignedTowebformLead);
						// Validation
						if (!corpUsersForSalesTerritoriesZip.contains(ownerAssignedTowebformLead)) {
							System.out.println("Owner should be among ::: " + corpUsersForSalesTerritoriesZip);
							fc.utobj().throwsException(
									"Owner Assignment - Assign Lead Owner by Lead Source - TC-33909 - WebForm - FAILED");
						}
					}
			
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			
		}
	}
	
	@Test(priority = 174 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_65-Import", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33909")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33909_Import() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			try {
				
				 driver = fc.loginpage().login(driver);
		
				// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
					// Create CSV File with Lead Details
					String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_33909.csv";
					System.out.println(filePath);
					List<Lead> leadList = new ArrayList<Lead>();

					for (int j = 0; j <= 0; j++) {
						Lead leadImport = new Lead();
						
						leadImport.setFirstName("LeadSource_Import_33909");
						leadImport.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
						leadImport.setEmail("frantest2017@gmail.com");
						leadImport.setDivision(addDivision3.getDivisionName());
						leadImport.setCountry("USA");
						// leadApi.setStateProvince("Florida");
						leadImport.setCounty("Gulf");
						leadImport.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
						leadImport.setBasedonAssignmentRules("yes");
						leadImport.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
						leadImport.setLeadSourceDetails("None");
						
						leadList.add(leadImport);
					}

					common.writeCsvFile(filePath, leadList);

					// Import the CSV file
					Import importlead = new Import();
					ImportTest importTest = new ImportTest(driver);

					// Navigate to Import Tab
					fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

					importlead.setImportType("Leads");
					importlead.setSpecifyFileFormat("CSV");
					importlead.setSalesDataFile(filePath);
					importlead.setLeadStatus("New Lead");
					importlead.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
					importlead.setLeadSourceDetails("None");

					importTest.importLeadsCSV(importlead);

					// Validation
					Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

					Lead x = new Lead();
					List<String> leadsFirstNames = map.get("FN");
					List<String> leadsLastNames = map.get("LN");

					for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
						System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
						x.setFirstName(leadsFirstNames.get(a));
						x.setLastName(leadsLastNames.get(b));

						String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
						String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
						System.out
								.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

						// Validation
						if (!corpUsersForSalesTerritoriesZip.contains(ownerAssignedToLeadx)) {
							System.out.println("Owner should be among ::: " + corpUsersForSalesTerritoriesZip);
							fc.utobj().throwsException(
									"Owner Assignment - Assign Lead Owner by Lead Source - TC-33909 - Import - FAILED");
						}
					}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			
		}
	}
	
// =============================================================================================================================================================================================================	
	
	
	@Test(priority = 175 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_66-Api", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33908")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33908_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

			try {
								 
			// Through API
			// ------------------------------------------------------------------------------------------------------------------------------------------------------------
			for (int j = 0; j <= 0; j++) {
				Lead leadApi = new Lead();
				
				leadApi.setFirstName("LeadSource_Api_33908");
				leadApi.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				leadApi.setDivision(addDivision3.getDivisionName());
				leadApi.setCountry("USA");
				leadApi.setStateProvince("Florida");
				leadApi.setCounty("Gulf");
				leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
				leadApi.setLeadSourceDetails("None");
				
				common.addLeadThroughWebServices(null, leadApi);
				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(null, leadApi);
				// System.out.println(xmlApi);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println(
						"Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
				// Validation
				// Validate Owner Assignment
				// Validation
				if (!corpUsersForSalesTerritoriesZip.contains(ownerAssignedToLeadApi)) {
					System.out.println("Owner should be among ::: " + corpUsersForSalesTerritoriesZip);
					fc.utobj().throwsException(
							"Owner Assignment - Assign Lead Owner by Lead Source - TC-33908 - API - FAILED");
				}
			}
			
			fc.utobj().logoutAndQuitBrowser(null, testCaseId);
			
		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(null, e, testCaseId);

		/*	System.out.println(e.getMessage());
			fc.utobj().throwsException1(e.getMessage());*/				
		}
	}
	
	
	@Test(priority = 176 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_66-WebForm", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead SourceTC-33908")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33908_WebForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			try {
				
				 driver = fc.loginpage().login(driver);
				 
				// Through WebForm
					// ----------------------------------------------------------------------------------------------------------------------------------------------------------
					// Create WebForm
					ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
					ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();
					
					fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
					common.set_ManageWebform(manageWebFormGenerator);
					manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
					manageWebFormGenerator.setLeadSourceDetails("None");
					manageWebFormGenerator.setDivision(addDivision3.getDivisionName());
					manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

					// Add Lead - WebForm
					for (int j = 0; j <= 0; j++) {
						Lead leadWebForm = new Lead();

						leadWebForm.setFirstName("LeadSource_WebForm_33908");
						leadWebForm.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
						leadWebForm.setEmail("frantest2017@gmail.com");
						//leadWebForm.setDivision(addDivision2.getDivisionName());
						leadWebForm.setCountry("USA");
						leadWebForm.setStateProvince("Florida");
						leadWebForm.setCounty("Gulf");
						leadWebForm.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
						// leadWebForm.setBasedonAssignmentRules("yes");
						leadWebForm.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
						leadWebForm.setLeadSourceDetails("None");

						// Add Lead through WebForm
						manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm);

						// fc.utobj().sleep();
						String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
						String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
						System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>"
								+ ownerAssignedTowebformLead);
						// Validation
						if (!corpUsersForSalesTerritoriesZip.contains(ownerAssignedTowebformLead)) {
							System.out.println("Owner should be among ::: " + corpUsersForSalesTerritoriesZip);
							fc.utobj().throwsException(
									"Owner Assignment - Assign Lead Owner by Lead Source - TC-33908 - WebForm - FAILED");
						}
					}
			
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			
		}
	}
	
	
	@Test(priority =177, dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_66-Import", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33908")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33908_Import() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			try {
				
				 driver = fc.loginpage().login(driver);
				 
				// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
					// Create CSV File with Lead Details
					String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_33908.csv";
					System.out.println(filePath);
					List<Lead> leadList = new ArrayList<Lead>();

					for (int j = 0; j <= 0; j++) {
						Lead leadImport = new Lead();
						
						leadImport.setFirstName("LeadSource_Import_33908");
						leadImport.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
						leadImport.setEmail("frantest2017@gmail.com");
						leadImport.setDivision(addDivision3.getDivisionName());
						leadImport.setCountry("USA");
						leadImport.setStateProvince("Florida");
						leadImport.setCounty("Gulf");
						leadImport.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
						leadImport.setBasedonAssignmentRules("yes");
						leadImport.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
						leadImport.setLeadSourceDetails("None");
						
						leadList.add(leadImport);
					}

					common.writeCsvFile(filePath, leadList);

					// Import the CSV file
					Import importlead = new Import();
					ImportTest importTest = new ImportTest(driver);

					// Navigate to Import Tab
					fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

					importlead.setImportType("Leads");
					importlead.setSpecifyFileFormat("CSV");
					importlead.setSalesDataFile(filePath);
					importlead.setLeadStatus("New Lead");
					importlead.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
					importlead.setLeadSourceDetails("None");

					importTest.importLeadsCSV(importlead);

					// Validation
					Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

					Lead x = new Lead();
					List<String> leadsFirstNames = map.get("FN");
					List<String> leadsLastNames = map.get("LN");

					for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
						System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
						x.setFirstName(leadsFirstNames.get(a));
						x.setLastName(leadsLastNames.get(b));

						String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
						String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
						System.out
								.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

						// Validation
						if (!corpUsersForSalesTerritoriesZip.contains(ownerAssignedToLeadx)) {
							System.out.println("Owner should be among ::: " + corpUsersForSalesTerritoriesZip);
							fc.utobj().throwsException(
									"Owner Assignment - Assign Lead Owner by Lead Source - TC-33908 - Import - FAILED");
						}
					}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			
		}
	}
	
// =============================================================================================================================================================================================================	
	
	
	@Test(priority =178 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_67-Api", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33907")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33907_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

			try {
				
			// Through API
			// ------------------------------------------------------------------------------------------------------------------------------------------------------------
			for (int j = 0; j <= 0; j++) {
				Lead leadApi = new Lead();
				
				leadApi.setFirstName("LeadSource_Api_33907");
				leadApi.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				// leadApi.setDivision(addDivision2.getDivisionName());
				leadApi.setCountry("USA");
				// leadApi.setStateProvince("Florida");
				// leadApi.setCounty("Gulf");
				// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory("Friends");
				leadApi.setLeadSourceDetails("Friends");
				
				common.addLeadThroughWebServices(null, leadApi);
				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(null, leadApi);
				// System.out.println(xmlApi);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println(
						"Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
				// Validation
				// Validate Owner Assignment
				// Validation
				if (!ownerAssignedToLeadApi.equalsIgnoreCase("FranConnect Administrator")) {
					System.out.println("Owner should be ::: FranConnect Administrator");
					fc.utobj().throwsException(
							"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33907 - API - FAILED");
				}
			}
			
			fc.utobj().logoutAndQuitBrowser(null, testCaseId);
			
		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(null, e, testCaseId);

			/*System.out.println(e.getMessage());
			fc.utobj().throwsException1(e.getMessage());	*/			
		}
	}
	
	@Test(priority =179 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_67-WebForm", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33907")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33907_WebForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			try {
				
				 driver = fc.loginpage().login(driver);
				 
				// Through WebForm
					// ----------------------------------------------------------------------------------------------------------------------------------------------------------
					// Create WebForm
					ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
					ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

					fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
					common.set_ManageWebform(manageWebFormGenerator);
					manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
					manageWebFormGenerator.setLeadSourceDetails("None");
					// manageWebFormGenerator.setDivision(addDivision1.getDivisionName());
					manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

					// Add Lead - WebForm
					for (int j = 0; j <= 0; j++) {
						Lead leadWebForm = new Lead();

						leadWebForm.setFirstName("LeadSource_WebForm_33907");
						leadWebForm.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
						leadWebForm.setEmail("frantest2017@gmail.com");
						// leadApi.setDivision(addDivision2.getDivisionName());
						leadWebForm.setCountry("USA");
						// leadApi.setStateProvince("Florida");
						// leadApi.setCounty("Gulf");
						// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
						// leadWebForm.setBasedonAssignmentRules("yes");
						leadWebForm.setLeadSourceCategory("Friends");
						leadWebForm.setLeadSourceDetails("Friends");

						// Add Lead through WebForm
						manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm);

						// fc.utobj().sleep();
						String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
						String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
						System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>"
								+ ownerAssignedTowebformLead);
						// Validation
						if (!ownerAssignedTowebformLead.equalsIgnoreCase("FranConnect Administrator")) {
							System.out.println("Owner should be ::: FranConnect Administrator");
							fc.utobj().throwsException(
									"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33907 - WebForm - FAILED");
						}
					}
					
			
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			
		}
	}
	
	@Test(priority = 180 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_67-Import", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33907")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33907_Import() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			try {
				
				 driver = fc.loginpage().login(driver);
			
				// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
					// Create CSV File with Lead Details
					String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_33907.csv";
					System.out.println(filePath);
					List<Lead> leadList = new ArrayList<Lead>();

					for (int j = 0; j <= 0; j++) {
						Lead leadImport = new Lead();
						
						leadImport.setFirstName("LeadSource_Import_33907");
						leadImport.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
						leadImport.setEmail("frantest2017@gmail.com");
						// leadApi.setDivision(addDivision2.getDivisionName());
						leadImport.setCountry("USA");
						// leadApi.setStateProvince("Florida");
						// leadApi.setCounty("Gulf");
						// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
						leadImport.setBasedonAssignmentRules("yes");
						leadImport.setLeadSourceCategory("Friends");
						leadImport.setLeadSourceDetails("Friends");
						
						leadList.add(leadImport);
					}

					common.writeCsvFile(filePath, leadList);

					// Import the CSV file
					Import importlead = new Import();
					ImportTest importTest = new ImportTest(driver);

					// Navigate to Import Tab
					fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

					importlead.setImportType("Leads");
					importlead.setSpecifyFileFormat("CSV");
					importlead.setSalesDataFile(filePath);
					importlead.setLeadStatus("New Lead");
					importlead.setLeadSourceCategory("Friends");
					importlead.setLeadSourceDetails("Friends");

					importTest.importLeadsCSV(importlead);

					// Validation
					Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

					Lead x = new Lead();
					List<String> leadsFirstNames = map.get("FN");
					List<String> leadsLastNames = map.get("LN");

					for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
						System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
						x.setFirstName(leadsFirstNames.get(a));
						x.setLastName(leadsLastNames.get(b));

						String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
						String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
						System.out
								.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

						// Validation
						if (!ownerAssignedToLeadx.equalsIgnoreCase("FranConnect Administrator")) {
							System.out.println("Owner should be ::: FranConnect Administrator");
							fc.utobj().throwsException(
									"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33907 - Import - FAILED");
						}
					}
					
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			
		}
	}
	
// =============================================================================================================================================================================================================	
	
	
	@Test(priority = 181 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_68-Api", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33906")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33906_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

			try {
				
				// Through API
			// ------------------------------------------------------------------------------------------------------------------------------------------------------------
			for (int j = 0; j <= 0; j++) {
				Lead leadApi = new Lead();
				
				leadApi.setFirstName("LeadSource_Api_33906");
				leadApi.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				// leadApi.setDivision(addDivision2.getDivisionName());
				leadApi.setCountry("USA");
				// leadApi.setStateProvince("Florida");
				// leadApi.setCounty("Gulf");
				leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory("Friends");
				leadApi.setLeadSourceDetails("Friends");
				
				common.addLeadThroughWebServices(null, leadApi);
				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(null, leadApi);
				// System.out.println(xmlApi);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println(
						"Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
				// Validation
				// Validate Owner Assignment
				// Validation
				if (!ownerAssignedToLeadApi.equalsIgnoreCase("FranConnect Administrator")) {
					System.out.println("Owner should be ::: FranConnect Administrator");
					fc.utobj().throwsException(
							"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33906 - API - FAILED");
				}
			}
			
			fc.utobj().logoutAndQuitBrowser(null, testCaseId);
			

		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(null, e, testCaseId);

			/*System.out.println(e.getMessage());
			fc.utobj().throwsException1(e.getMessage());*/				
		}
	}
	
	@Test(priority = 182 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_68-WebForm", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33906")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33906_WebForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			try {
				
				 driver = fc.loginpage().login(driver);
				 
				// Through WebForm
					// ----------------------------------------------------------------------------------------------------------------------------------------------------------
					// Create WebForm
					ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
					ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

					fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
					common.set_ManageWebform(manageWebFormGenerator);
					manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
					manageWebFormGenerator.setLeadSourceDetails("None");
					// manageWebFormGenerator.setDivision(addDivision1.getDivisionName());
					manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

					// Add Lead - WebForm
					for (int j = 0; j <= 0; j++) {
						Lead leadWebForm = new Lead();

						leadWebForm.setFirstName("LeadSource_WebForm_33906");
						leadWebForm.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
						leadWebForm.setEmail("frantest2017@gmail.com");
						// leadApi.setDivision(addDivision2.getDivisionName());
						leadWebForm.setCountry("USA");
						// leadApi.setStateProvince("Florida");
						// leadApi.setCounty("Gulf");
						leadWebForm.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
						// leadWebForm.setBasedonAssignmentRules("yes");
						leadWebForm.setLeadSourceCategory("Friends");
						leadWebForm.setLeadSourceDetails("Friends");
						

						// Add Lead through WebForm
						manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm);

						// fc.utobj().sleep();
						String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
						String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
						System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>"
								+ ownerAssignedTowebformLead);
						// Validation
						if (!ownerAssignedTowebformLead.equalsIgnoreCase("FranConnect Administrator")) {
							System.out.println("Owner should be ::: FranConnect Administrator");
							fc.utobj().throwsException(
									"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33906 - WebForm - FAILED");
						}
					}
			
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			
		}
	}
	
	@Test(priority = 183 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_68-Import", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33906")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33906_Import() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			try {
				
				 driver = fc.loginpage().login(driver);
				 
				// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
					// Create CSV File with Lead Details
					String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_33906.csv";
					System.out.println(filePath);
					List<Lead> leadList = new ArrayList<Lead>();

					for (int j = 0; j <= 0; j++) {
						Lead leadImport = new Lead();
						
						leadImport.setFirstName("LeadSource_Import_33906");
						leadImport.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
						leadImport.setEmail("frantest2017@gmail.com");
						// leadApi.setDivision(addDivision2.getDivisionName());
						leadImport.setCountry("USA");
						// leadApi.setStateProvince("Florida");
						// leadApi.setCounty("Gulf");
						leadImport.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
						leadImport.setBasedonAssignmentRules("yes");
						leadImport.setLeadSourceCategory("Friends");
						leadImport.setLeadSourceDetails("Friends");
						
						leadList.add(leadImport);
					}

					common.writeCsvFile(filePath, leadList);

					// Import the CSV file
					Import importlead = new Import();
					ImportTest importTest = new ImportTest(driver);

					// Navigate to Import Tab
					fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

					importlead.setImportType("Leads");
					importlead.setSpecifyFileFormat("CSV");
					importlead.setSalesDataFile(filePath);
					importlead.setLeadStatus("New Lead");
					importlead.setLeadSourceCategory("Friends");
					importlead.setLeadSourceDetails("Friends");

					importTest.importLeadsCSV(importlead);

					// Validation
					Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

					Lead x = new Lead();
					List<String> leadsFirstNames = map.get("FN");
					List<String> leadsLastNames = map.get("LN");

					for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
						System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
						x.setFirstName(leadsFirstNames.get(a));
						x.setLastName(leadsLastNames.get(b));

						String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
						String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
						System.out
								.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

						// Validation
						
						if (!ownerAssignedToLeadx.equalsIgnoreCase("FranConnect Administrator")) {
							System.out.println("Owner should be ::: FranConnect Administrator");
							fc.utobj().throwsException(
									"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33906 - Import - FAILED");
						}
					}
					
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			
		}
	}
	
// =============================================================================================================================================================================================================	
	
	
	@Test(priority = 184 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_69-Api", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33905")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33905_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

			try {
								 
			// Through API
			// ------------------------------------------------------------------------------------------------------------------------------------------------------------
			for (int j = 0; j <= 0; j++) {
				Lead leadApi = new Lead();
				
				leadApi.setFirstName("LeadSource_Api_33905");
				leadApi.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				// leadApi.setDivision(addDivision2.getDivisionName());
				leadApi.setCountry("USA");
				// leadApi.setStateProvince("Florida");
				leadApi.setCounty("Gulf");
				// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory("Friends");
				leadApi.setLeadSourceDetails("Friends");
				
				common.addLeadThroughWebServices(null, leadApi);
				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(null, leadApi);
				// System.out.println(xmlApi);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println(
						"Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
				// Validation
				// Validate Owner Assignment
				// Validation
				if (!ownerAssignedToLeadApi.equalsIgnoreCase("FranConnect Administrator")) {
					System.out.println("Owner should be ::: FranConnect Administrator");
					fc.utobj().throwsException(
							"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33905 - API - FAILED");
				}
			}
			
			fc.utobj().logoutAndQuitBrowser(null, testCaseId);
			
		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(null, e, testCaseId);

		/*	System.out.println(e.getMessage());
			fc.utobj().throwsException1(e.getMessage());	*/
		}
	}
	
	@Test(priority = 185 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_69-WebForm", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33905")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33905_WebForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			try {
				
				 driver = fc.loginpage().login(driver);
				 
				// Through WebForm
					// ----------------------------------------------------------------------------------------------------------------------------------------------------------
					// Create WebForm
					ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
					ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

					fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
					common.set_ManageWebform(manageWebFormGenerator);
					manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
					manageWebFormGenerator.setLeadSourceDetails("None");
					// manageWebFormGenerator.setDivision(addDivision1.getDivisionName());
					manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

					// Add Lead - WebForm
					for (int j = 0; j <= 0; j++) {
						Lead leadWebForm = new Lead();


						leadWebForm.setFirstName("LeadSource_WebForm_33905");
						leadWebForm.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
						leadWebForm.setEmail("frantest2017@gmail.com");
						// leadApi.setDivision(addDivision2.getDivisionName());
						leadWebForm.setCountry("USA");
						// leadApi.setStateProvince("Florida");
						leadWebForm.setCounty("Gulf");
						// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
						// leadWebForm.setBasedonAssignmentRules("yes");
						leadWebForm.setLeadSourceCategory("Friends");
						leadWebForm.setLeadSourceDetails("Friends");
						

						// Add Lead through WebForm
						manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm);

						// fc.utobj().sleep();
						String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
						String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
						System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>"
								+ ownerAssignedTowebformLead);
						// Validation
						// Validation
						if (!ownerAssignedTowebformLead.equalsIgnoreCase("FranConnect Administrator")) {

							System.out.println("Owner should be ::: FranConnect Administrator");
							fc.utobj().throwsException(
									"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33905 - WebForm - FAILED");
						}
					}
			
				 
			
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			
		}
	}
	
	@Test(priority = 186 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_69-Import", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33905")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33905_Import() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			try {
				
				 driver = fc.loginpage().login(driver);
				 
				// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
					// Create CSV File with Lead Details
					String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_33905.csv";
					System.out.println(filePath);
					List<Lead> leadList = new ArrayList<Lead>();

					for (int j = 0; j <= 0; j++) {
						Lead leadImport = new Lead();
						

						leadImport.setFirstName("LeadSource_Import_33905");
						leadImport.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
						leadImport.setEmail("frantest2017@gmail.com");
						// leadApi.setDivision(addDivision2.getDivisionName());
						leadImport.setCountry("USA");
						// leadApi.setStateProvince("Florida");
						leadImport.setCounty("Gulf");
						// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
						leadImport.setBasedonAssignmentRules("yes");
						leadImport.setLeadSourceCategory("Friends");
						leadImport.setLeadSourceDetails("Friends");
						
						leadList.add(leadImport);
					}

					common.writeCsvFile(filePath, leadList);

					// Import the CSV file
					Import importlead = new Import();
					ImportTest importTest = new ImportTest(driver);

					// Navigate to Import Tab
					fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

					importlead.setImportType("Leads");
					importlead.setSpecifyFileFormat("CSV");
					importlead.setSalesDataFile(filePath);
					importlead.setLeadStatus("New Lead");
					importlead.setLeadSourceCategory("Friends");
					importlead.setLeadSourceDetails("Friends");

					importTest.importLeadsCSV(importlead);

					// Validation
					Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

					Lead x = new Lead();
					List<String> leadsFirstNames = map.get("FN");
					List<String> leadsLastNames = map.get("LN");

					for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
						System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
						x.setFirstName(leadsFirstNames.get(a));
						x.setLastName(leadsLastNames.get(b));

						String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
						String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
						System.out
								.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

						// Validation
						// Validation
						if (!ownerAssignedToLeadx.equalsIgnoreCase("FranConnect Administrator")) {
							System.out.println("Owner should be ::: FranConnect Administrator");
							fc.utobj().throwsException(
									"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33905 - Import - FAILED");
						}
					}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			
		}
	}
	
// =============================================================================================================================================================================================================	
	
	
	@Test(priority = 187 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_70-Api", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33904")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33904_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

			try {
								 
			// Through API
			// ------------------------------------------------------------------------------------------------------------------------------------------------------------
			for (int j = 0; j <= 0; j++) {
				Lead leadApi = new Lead();
				
				leadApi.setFirstName("LeadSource_Api_33904");
				leadApi.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				// leadApi.setDivision(addDivision2.getDivisionName());
				leadApi.setCountry("USA");
				leadApi.setStateProvince("Florida");
				// leadApi.setCounty("Gulf");
				// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory("Friends");
				leadApi.setLeadSourceDetails("Friends");
				
				common.addLeadThroughWebServices(null, leadApi);
				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(null, leadApi);
				// System.out.println(xmlApi);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println(
						"Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
				// Validation
				// Validate Owner Assignment
				// Validation
				if (!ownerAssignedToLeadApi.equalsIgnoreCase("FranConnect Administrator")) {
					System.out.println("Owner should be ::: FranConnect Administrator");
					fc.utobj().throwsException(
							"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33904 - API - FAILED");
				}
			}
			
			fc.utobj().logoutAndQuitBrowser(null, testCaseId);
			
		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(null, e, testCaseId);

			/*System.out.println(e.getMessage());
			fc.utobj().throwsException1(e.getMessage());*/				
		}
	}
	
	@Test(priority = 188 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_70-WebForm", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33904")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33904_WebForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			try {
				
				 driver = fc.loginpage().login(driver);
				 
				// Through WebForm
					// ----------------------------------------------------------------------------------------------------------------------------------------------------------
					// Create WebForm
					ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
					ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

					fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
					common.set_ManageWebform(manageWebFormGenerator);
					manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
					manageWebFormGenerator.setLeadSourceDetails("None");
					// manageWebFormGenerator.setDivision(addDivision1.getDivisionName());
					manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

					// Add Lead - WebForm
					for (int j = 0; j <= 0; j++) {
						Lead leadWebForm = new Lead();

						leadWebForm.setFirstName("LeadSource_WebForm_33904");
						leadWebForm.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
						leadWebForm.setEmail("frantest2017@gmail.com");
						// leadApi.setDivision(addDivision2.getDivisionName());
						leadWebForm.setCountry("USA");
						leadWebForm.setStateProvince("Florida");
						// leadApi.setCounty("Gulf");
						// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
						// leadWebForm.setBasedonAssignmentRules("yes");
						leadWebForm.setLeadSourceCategory("Friends");
						leadWebForm.setLeadSourceDetails("Friends");
						

						// Add Lead through WebForm
						manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm);

						// fc.utobj().sleep();
						String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
						String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
						System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>"
								+ ownerAssignedTowebformLead);
						// Validation
						if (!ownerAssignedTowebformLead.equalsIgnoreCase("FranConnect Administrator")) {
							System.out.println("Owner should be ::: FranConnect Administrator");
							fc.utobj().throwsException(
									"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33904 - WebForm - FAILED");
						}
					}
			
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			
		}
	}
	
	@Test(priority = 189 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_70-Import", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33904")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33904_Import() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			try {
				
				 driver = fc.loginpage().login(driver);
				 
				// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
					// Create CSV File with Lead Details
					String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_33904.csv";
					System.out.println(filePath);
					List<Lead> leadList = new ArrayList<Lead>();

					for (int j = 0; j <= 0; j++) {
						Lead leadImport = new Lead();
						
						leadImport.setFirstName("LeadSource_Import_33904");
						leadImport.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
						leadImport.setEmail("frantest2017@gmail.com");
						// leadApi.setDivision(addDivision2.getDivisionName());
						leadImport.setCountry("USA");
						leadImport.setStateProvince("Florida");
						// leadApi.setCounty("Gulf");
						// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
						leadImport.setBasedonAssignmentRules("yes");
						leadImport.setLeadSourceCategory("Friends");
						leadImport.setLeadSourceDetails("Friends");
						
						leadList.add(leadImport);
					}

					common.writeCsvFile(filePath, leadList);

					// Import the CSV file
					Import importlead = new Import();
					ImportTest importTest = new ImportTest(driver);

					// Navigate to Import Tab
					fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

					importlead.setImportType("Leads");
					importlead.setSpecifyFileFormat("CSV");
					importlead.setSalesDataFile(filePath);
					importlead.setLeadStatus("New Lead");
					importlead.setLeadSourceCategory("Friends");
					importlead.setLeadSourceDetails("Friends");

					importTest.importLeadsCSV(importlead);

					// Validation
					Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

					Lead x = new Lead();
					List<String> leadsFirstNames = map.get("FN");
					List<String> leadsLastNames = map.get("LN");

					for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
						System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
						x.setFirstName(leadsFirstNames.get(a));
						x.setLastName(leadsLastNames.get(b));

						String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
						String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
						System.out
								.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

						// Validation
						if (!ownerAssignedToLeadx.equalsIgnoreCase("FranConnect Administrator")) {
							System.out.println("Owner should be ::: FranConnect Administrator");
							fc.utobj().throwsException(
									"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33904 - Import - FAILED");
						}
					}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			
		}
	}
	
// =============================================================================================================================================================================================================	
	
	
	@Test(priority =190 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_71-Api", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33903")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33903_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

			try {
								 
			// Through API
			// ------------------------------------------------------------------------------------------------------------------------------------------------------------
			for (int j = 0; j <= 0; j++) {
				Lead leadApi = new Lead();
				
				leadApi.setFirstName("LeadSource_Api_33903");
				leadApi.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				leadApi.setDivision(addDivision3.getDivisionName());
				leadApi.setCountry("USA");
				// leadApi.setStateProvince("Florida");
				leadApi.setCounty("Gulf");
				// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory("Friends");
				leadApi.setLeadSourceDetails("Friends");
				
				common.addLeadThroughWebServices(null, leadApi);
				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(null, leadApi);
				// System.out.println(xmlApi);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println(
						"Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
				// Validation
				// Validate Owner Assignment
				// Validation
				if (!ownerAssignedToLeadApi.equalsIgnoreCase("FranConnect Administrator")) {
					System.out.println("Owner should be ::: FranConnect Administrator");
					fc.utobj().throwsException(
							"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33903 - API - FAILED");
				}
			}
			
			fc.utobj().logoutAndQuitBrowser(null, testCaseId);
			
		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(null, e, testCaseId);

			/*System.out.println(e.getMessage());
			fc.utobj().throwsException1(e.getMessage());	*/
		}
	}
	
	@Test(priority = 191 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_71-WebForm", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33903")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33903_WebForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			try {
				
				 driver = fc.loginpage().login(driver);
				 
				// Through WebForm
					// ----------------------------------------------------------------------------------------------------------------------------------------------------------
					// Create WebForm
					ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
					ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

					fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
					common.set_ManageWebform(manageWebFormGenerator);
					manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
					manageWebFormGenerator.setLeadSourceDetails("None");
					manageWebFormGenerator.setDivision(addDivision2.getDivisionName());
					manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

					// Add Lead - WebForm
					for (int j = 0; j <= 0; j++) {
						Lead leadWebForm = new Lead();

						leadWebForm.setFirstName("LeadSource_WebForm_33903");
						leadWebForm.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
						leadWebForm.setEmail("frantest2017@gmail.com");
						// leadWebForm.setDivision(addDivision2.getDivisionName());
						leadWebForm.setCountry("USA");
						// leadApi.setStateProvince("Florida");
						leadWebForm.setCounty("Gulf");
						// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
						// leadWebForm.setBasedonAssignmentRules("yes");
						leadWebForm.setLeadSourceCategory("Friends");
						leadWebForm.setLeadSourceDetails("Friends");
						
						// Add Lead through WebForm
						manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm);

						// fc.utobj().sleep();
						String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
						String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
						System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>"
								+ ownerAssignedTowebformLead);
						// Validation
						if (!ownerAssignedTowebformLead.equalsIgnoreCase("FranConnect Administrator")) {
							System.out.println("Owner should be ::: FranConnect Administrator");
							fc.utobj().throwsException(
									"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33903 - WebForm - FAILED");
						}
					}
					
			
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			
		}
	}
	
	@Test(priority = 192 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_71-Import", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33903")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33903_Import() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			try {
				
				 driver = fc.loginpage().login(driver);
				 
				// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
					// Create CSV File with Lead Details
					String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_33903.csv";
					System.out.println(filePath);
					List<Lead> leadList = new ArrayList<Lead>();

					for (int j = 0; j <= 0; j++) {
						Lead leadImport = new Lead();
						
						leadImport.setFirstName("LeadSource_Import_33903");
						leadImport.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
						leadImport.setEmail("frantest2017@gmail.com");
						leadImport.setDivision(addDivision3.getDivisionName());
						leadImport.setCountry("USA");
						// leadApi.setStateProvince("Florida");
						leadImport.setCounty("Gulf");
						// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
						leadImport.setBasedonAssignmentRules("yes");
						leadImport.setLeadSourceCategory("Friends");
						leadImport.setLeadSourceDetails("Friends");
						
						leadList.add(leadImport);
					}

					common.writeCsvFile(filePath, leadList);

					// Import the CSV file
					Import importlead = new Import();
					ImportTest importTest = new ImportTest(driver);

					// Navigate to Import Tab
					fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

					importlead.setImportType("Leads");
					importlead.setSpecifyFileFormat("CSV");
					importlead.setSalesDataFile(filePath);
					importlead.setLeadStatus("New Lead");
					importlead.setLeadSourceCategory("Friends");
					importlead.setLeadSourceDetails("Friends");

					importTest.importLeadsCSV(importlead);

					// Validation
					Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

					Lead x = new Lead();
					List<String> leadsFirstNames = map.get("FN");
					List<String> leadsLastNames = map.get("LN");

					for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
						System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
						x.setFirstName(leadsFirstNames.get(a));
						x.setLastName(leadsLastNames.get(b));

						String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
						String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
						System.out
								.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

						// Validation
						if (!ownerAssignedToLeadx.equalsIgnoreCase("FranConnect Administrator")) {
							System.out.println("Owner should be ::: FranConnect Administrator");
							fc.utobj().throwsException(
									"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33903 - Import - FAILED");
						}
					}
					
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			
		}
	}
	
// =============================================================================================================================================================================================================	
	
	
	@Test(priority = 193 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_72-Api", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33902")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33902_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		try {
								 
			// Through API
			// ------------------------------------------------------------------------------------------------------------------------------------------------------------
			for (int j = 0; j <= 0; j++) {
				Lead leadApi = new Lead();
				
				leadApi.setFirstName("LeadSource_Api_33902");
				leadApi.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				// leadApi.setDivision(addDivision2.getDivisionName());
				leadApi.setCountry("USA");
				// leadApi.setStateProvince("Florida");
				leadApi.setCounty("Gulf");
				// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
				leadApi.setLeadSourceDetails("None");
				
				common.addLeadThroughWebServices(null, leadApi);
				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(null, leadApi);
				// System.out.println(xmlApi);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println(
						"Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
				// Validation
				// Validate Owner Assignment
				// Validation
				if (!corpUsersForSource2.contains(ownerAssignedToLeadApi)) {
					System.out.println("Owner should be among ::: " + corpUsersForSource2);
					fc.utobj().throwsException(
							"Owner Assignment - Assign Lead Owner by Lead Source - TC-33902 - API - FAILED");
				}
			}
			
			fc.utobj().logoutAndQuitBrowser(null, testCaseId);
			
		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(null, e, testCaseId);

			/*System.out.println(e.getMessage());
			fc.utobj().throwsException1(e.getMessage());*/	
		}
	}
	
	@Test(priority = 194 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_72-WebForm", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33902")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33902_WebForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			try {
				
				 driver = fc.loginpage().login(driver);
				 
				// Through WebForm
					// ----------------------------------------------------------------------------------------------------------------------------------------------------------
					// Create WebForm
					ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
					ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

					fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
					common.set_ManageWebform(manageWebFormGenerator);
					manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
					manageWebFormGenerator.setLeadSourceDetails("None");
					// manageWebFormGenerator.setDivision(addDivision1.getDivisionName());
					manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

					// Add Lead - WebForm
					for (int j = 0; j <= 0; j++) {
						Lead leadWebForm = new Lead();

						leadWebForm.setFirstName("LeadSource_WebForm_33902");
						leadWebForm.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
						leadWebForm.setEmail("frantest2017@gmail.com");
						// leadApi.setDivision(addDivision2.getDivisionName());
						leadWebForm.setCountry("USA");
						// leadApi.setStateProvince("Florida");
						leadWebForm.setCounty("Gulf");
						// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
						// leadWebForm.setBasedonAssignmentRules("yes");
						leadWebForm.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
						leadWebForm.setLeadSourceDetails("None");
						

						// Add Lead through WebForm
						manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm);

						// fc.utobj().sleep();
						String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
						String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
						System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>"
								+ ownerAssignedTowebformLead);
						// Validation
						if (!corpUsersForSource2.contains(ownerAssignedTowebformLead)) {
							System.out.println("Owner should be among ::: " + corpUsersForSource2);
							fc.utobj().throwsException(
									"Owner Assignment - Assign Lead Owner by Lead Source - TC-33902 - WebForm - FAILED");
						}
					}
			
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			
		}
	}
	
	
	@Test(priority = 195 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_72-Import", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33902")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33902_Import() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			try {
				
				 driver = fc.loginpage().login(driver);
				 
				// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
					// Create CSV File with Lead Details
					String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_33902.csv";
					System.out.println(filePath);
					List<Lead> leadList = new ArrayList<Lead>();

					for (int j = 0; j <= 0; j++) {
						Lead leadImport = new Lead();
						
						leadImport.setFirstName("LeadSource_Import_33902");
						leadImport.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
						leadImport.setEmail("frantest2017@gmail.com");
						// leadApi.setDivision(addDivision2.getDivisionName());
						leadImport.setCountry("USA");
						// leadApi.setStateProvince("Florida");
						leadImport.setCounty("Gulf");
						// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
						leadImport.setBasedonAssignmentRules("yes");
						leadImport.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
						leadImport.setLeadSourceDetails("None");
						
						leadList.add(leadImport);
					}

					common.writeCsvFile(filePath, leadList);

					// Import the CSV file
					Import importlead = new Import();
					ImportTest importTest = new ImportTest(driver);

					// Navigate to Import Tab
					fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

					importlead.setImportType("Leads");
					importlead.setSpecifyFileFormat("CSV");
					importlead.setSalesDataFile(filePath);
					importlead.setLeadStatus("New Lead");
					importlead.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
					importlead.setLeadSourceDetails("None");

					importTest.importLeadsCSV(importlead);

					// Validation
					Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

					Lead x = new Lead();
					List<String> leadsFirstNames = map.get("FN");
					List<String> leadsLastNames = map.get("LN");

					for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
						System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
						x.setFirstName(leadsFirstNames.get(a));
						x.setLastName(leadsLastNames.get(b));

						String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
						String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
						System.out
								.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

						// Validation
						if (!corpUsersForSource2.contains(ownerAssignedToLeadx)) {
							System.out.println("Owner should be among ::: " + corpUsersForSource2);
							fc.utobj().throwsException(
									"Owner Assignment - Assign Lead Owner by Lead Source - TC-33902 - Import - FAILED");
						}
					}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			
		}
	}
	
	
// =============================================================================================================================================================================================================	
	
	
	@Test(priority = 196 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_73-Api", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33901")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33901_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

			try {
								 
			// Through API
			// ------------------------------------------------------------------------------------------------------------------------------------------------------------
			for (int j = 0; j <= 0; j++) {
				Lead leadApi = new Lead();
				
				leadApi.setFirstName("LeadSource_Api_33901");
				leadApi.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				leadApi.setDivision(addDivision3.getDivisionName());
				leadApi.setCountry("USA");
				// leadApi.setStateProvince("Florida");
				leadApi.setCounty("Gulf");
				// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
				leadApi.setLeadSourceDetails("None");
				
				common.addLeadThroughWebServices(null, leadApi);
				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(null, leadApi);
				// System.out.println(xmlApi);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println(
						"Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
				// Validation
				// Validate Owner Assignment
				// Validation
				if (!corpUsersForSource2.contains(ownerAssignedToLeadApi)) {
					System.out.println("Owner should be among ::: " + corpUsersForSource2);
					fc.utobj().throwsException(
							"Owner Assignment - Assign Lead Owner by Lead Source - TC-33901 - API - FAILED");
				}
			}
			
			fc.utobj().logoutAndQuitBrowser(null, testCaseId);
			
		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(null, e, testCaseId);

			/*System.out.println(e.getMessage());
			fc.utobj().throwsException1(e.getMessage());	*/			
		}
	}
	
	@Test(priority = 197 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" /*, "sales_OwnerAssignment_Failed"*/ })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_73-WebForm", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33901")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33901_WebForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			try {
				
				 driver = fc.loginpage().login(driver);
				 
				// Through WebForm
					// ----------------------------------------------------------------------------------------------------------------------------------------------------------
					// Create WebForm
					ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
					ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

					fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
					common.set_ManageWebform(manageWebFormGenerator);
					manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
					manageWebFormGenerator.setLeadSourceDetails("None");
					manageWebFormGenerator.setDivision(addDivision2.getDivisionName());
					manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

					// Add Lead - WebForm
					for (int j = 0; j <= 0; j++) {
						Lead leadWebForm = new Lead();

						leadWebForm.setFirstName("LeadSource_WebForm_33901");
						leadWebForm.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
						leadWebForm.setEmail("frantest2017@gmail.com");
						// leadWebForm.setDivision(addDivision2.getDivisionName());
						leadWebForm.setCountry("USA");
						// leadApi.setStateProvince("Florida");
						leadWebForm.setCounty("Gulf");
						// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
						// leadWebForm.setBasedonAssignmentRules("yes");
						leadWebForm.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
						leadWebForm.setLeadSourceDetails("None");

						// Add Lead through WebForm
						manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm);

						// fc.utobj().sleep();
						String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
						String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
						System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>"
								+ ownerAssignedTowebformLead);
						
						// Validation
						if (! corpUsersForDivision.contains(ownerAssignedTowebformLead)) {
							System.out.println("Owner should be among ::: " + corpUsersForDivision);
							fc.utobj().throwsException(
									"Owner Assignment - Assign Lead Owner by Lead Source - TC-33901 - WebForm - FAILED");
						}
						
						/*// Validation
						if (!corpUsersForSource.contains(ownerAssignedTowebformLead)) {
							fc.utobj().throwsException(
									"Owner Assignment - Assign Lead Owner by Lead Source - TC-33901 - WebForm - FAILED");
						}*/
					}
					
			
				 
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			
		}
	}
	
	@Test(priority = 198 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_73-Import", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33901")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33901_Import() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			try {
				
				 driver = fc.loginpage().login(driver);
				 
				// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
					// Create CSV File with Lead Details
					String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_33901.csv";
					System.out.println(filePath);
					List<Lead> leadList = new ArrayList<Lead>();

					for (int j = 0; j <= 0; j++) {
						Lead leadImport = new Lead();
						
						leadImport.setFirstName("LeadSource_Import_33901");
						leadImport.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
						leadImport.setEmail("frantest2017@gmail.com");
						leadImport.setDivision(addDivision3.getDivisionName());
						leadImport.setCountry("USA");
						// leadApi.setStateProvince("Florida");
						leadImport.setCounty("Gulf");
						// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
						leadImport.setBasedonAssignmentRules("yes");
						leadImport.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
						leadImport.setLeadSourceDetails("None");
						
						leadList.add(leadImport);
					}

					common.writeCsvFile(filePath, leadList);

					// Import the CSV file
					Import importlead = new Import();
					ImportTest importTest = new ImportTest(driver);

					// Navigate to Import Tab
					fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

					importlead.setImportType("Leads");
					importlead.setSpecifyFileFormat("CSV");
					importlead.setSalesDataFile(filePath);
					importlead.setLeadStatus("New Lead");
					importlead.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
					importlead.setLeadSourceDetails("None");

					importTest.importLeadsCSV(importlead);

					// Validation
					Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

					Lead x = new Lead();
					List<String> leadsFirstNames = map.get("FN");
					List<String> leadsLastNames = map.get("LN");

					for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
						System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
						x.setFirstName(leadsFirstNames.get(a));
						x.setLastName(leadsLastNames.get(b));

						String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
						String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
						System.out
								.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

						// Validation
						if (!corpUsersForSource2.contains(ownerAssignedToLeadx)) {
							System.out.println("Owner should be among ::: " + corpUsersForSource2);
							fc.utobj().throwsException(
									"Owner Assignment - Assign Lead Owner by Lead Source - TC-33901 - Import - FAILED");
						}
					}
					
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			
		}
	}
	
// =============================================================================================================================================================================================================	
	
	
	@Test(priority = 199 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_74-Api", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33900")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33900_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

			try {
								 
			// Through API
			// ------------------------------------------------------------------------------------------------------------------------------------------------------------
			for (int j = 0; j <= 0; j++) {
				Lead leadApi = new Lead();
				
				leadApi.setFirstName("LeadSource_Api_33900");
				leadApi.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				// leadApi.setDivision(addDivision2.getDivisionName());
				leadApi.setCountry("USA");
				leadApi.setStateProvince("Florida");
				// leadApi.setCounty("Gulf");
				// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
				leadApi.setLeadSourceDetails("None");
				
				common.addLeadThroughWebServices(null, leadApi);
				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(null, leadApi);
				// System.out.println(xmlApi);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println(
						"Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
				// Validation
				// Validate Owner Assignment
				// Validation
				if (!corpUsersForSource2.contains(ownerAssignedToLeadApi)) {
					System.out.println("Owner should be among ::: " + corpUsersForSource2);
					fc.utobj().throwsException(
							"Owner Assignment - Assign Lead Owner by Lead Source - TC-33900 - API - FAILED");
				}
			}
			
			fc.utobj().logoutAndQuitBrowser(null, testCaseId);

		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(null, e, testCaseId);

			/*System.out.println(e.getMessage());
			fc.utobj().throwsException1(e.getMessage());	*/			
		}
	}
	
	
	@Test(priority =200,dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_74-WebForm", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33900")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33900_WebForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			try {
				
				 driver = fc.loginpage().login(driver);
				 
				// Through WebForm
					// ----------------------------------------------------------------------------------------------------------------------------------------------------------
					// Create WebForm
					ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
					ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

					fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
					common.set_ManageWebform(manageWebFormGenerator);
					manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
					manageWebFormGenerator.setLeadSourceDetails("None");
					// manageWebFormGenerator.setDivision(addDivision1.getDivisionName());
					manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

					// Add Lead - WebForm
					for (int j = 0; j <= 0; j++) {
						Lead leadWebForm = new Lead();

						leadWebForm.setFirstName("LeadSource_WebForm_33900");
						leadWebForm.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
						leadWebForm.setEmail("frantest2017@gmail.com");
						// leadApi.setDivision(addDivision2.getDivisionName());
						leadWebForm.setCountry("USA");
						leadWebForm.setStateProvince("Florida");
						// leadApi.setCounty("Gulf");
						// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
						// leadWebForm.setBasedonAssignmentRules("yes");
						leadWebForm.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
						leadWebForm.setLeadSourceDetails("None");
						

						// Add Lead through WebForm
						manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm);

						// fc.utobj().sleep();
						String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
						String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
						System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>"
								+ ownerAssignedTowebformLead);
						// Validation
						if (!corpUsersForSource2.contains(ownerAssignedTowebformLead)) {
							System.out.println("Owner should be among ::: " + corpUsersForSource2);
							fc.utobj().throwsException(
									"Owner Assignment - Assign Lead Owner by Lead Source - TC-33900 - WebForm - FAILED");
						}
					}
			
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			
		}
	}
	
	
	@Test(priority =201,dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_74-Import", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33900")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33900_Import() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			try {
				
				 driver = fc.loginpage().login(driver);
				 
				// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
					// Create CSV File with Lead Details
					String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_33900.csv";
					System.out.println(filePath);
					List<Lead> leadList = new ArrayList<Lead>();

					for (int j = 0; j <= 0; j++) {
						Lead leadImport = new Lead();
						
						leadImport.setFirstName("LeadSource_Import_33900");
						leadImport.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
						leadImport.setEmail("frantest2017@gmail.com");
						// leadApi.setDivision(addDivision2.getDivisionName());
						leadImport.setCountry("USA");
						leadImport.setStateProvince("Florida");
						// leadApi.setCounty("Gulf");
						// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
						leadImport.setBasedonAssignmentRules("yes");
						leadImport.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
						leadImport.setLeadSourceDetails("None");
						
						leadList.add(leadImport);
					}

					common.writeCsvFile(filePath, leadList);

					// Import the CSV file
					Import importlead = new Import();
					ImportTest importTest = new ImportTest(driver);

					// Navigate to Import Tab
					fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

					importlead.setImportType("Leads");
					importlead.setSpecifyFileFormat("CSV");
					importlead.setSalesDataFile(filePath);
					importlead.setLeadStatus("New Lead");
					importlead.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
					importlead.setLeadSourceDetails("None");

					importTest.importLeadsCSV(importlead);

					// Validation
					Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

					Lead x = new Lead();
					List<String> leadsFirstNames = map.get("FN");
					List<String> leadsLastNames = map.get("LN");

					for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
						System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
						x.setFirstName(leadsFirstNames.get(a));
						x.setLastName(leadsLastNames.get(b));

						String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
						String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
						System.out
								.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

						// Validation
						if (!corpUsersForSource2.contains(ownerAssignedToLeadx)) {
							System.out.println("Owner should be among ::: " + corpUsersForSource2);
							fc.utobj().throwsException(
									"Owner Assignment - Assign Lead Owner by Lead Source - TC-33900 - Import - FAILED");
						}
					}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			
		}
	}
	
// =============================================================================================================================================================================================================	
	
	
	@Test(priority =202,dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_75-Api", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33899")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33899_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

			try {
								 
			// Through API
			// ------------------------------------------------------------------------------------------------------------------------------------------------------------
			for (int j = 0; j <= 0; j++) {
				Lead leadApi = new Lead();
				
				leadApi.setFirstName("LeadSource_Api_33899");
				leadApi.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				leadApi.setDivision(addDivision3.getDivisionName());
				leadApi.setCountry("USA");
				leadApi.setStateProvince("Florida");
				// leadApi.setCounty("Gulf");
				// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory("Friends");
				leadApi.setLeadSourceDetails("Friends");
				
				common.addLeadThroughWebServices(null, leadApi);
				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(null, leadApi);
				// System.out.println(xmlApi);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println(
						"Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
				// Validation
				// Validate Owner Assignment
				// Validation
				if (!ownerAssignedToLeadApi.equalsIgnoreCase("FranConnect Administrator")) {
					System.out.println("Owner should be ::: FranConnect Administrator");
					fc.utobj().throwsException(
							"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33899 - API - FAILED");
				}
			}	
			
			fc.utobj().logoutAndQuitBrowser(null, testCaseId);

		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(null, e, testCaseId);

			/*System.out.println(e.getMessage());
			fc.utobj().throwsException1(e.getMessage());*/				
		}
	}
	
	@Test(priority =203,dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_75-WebForm", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33899 WebForm")
	private void OwnerAssignment_75_WebForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			try {
				
				 driver = fc.loginpage().login(driver);
				 
				// Through WebForm
					// ----------------------------------------------------------------------------------------------------------------------------------------------------------
					// Create WebForm
					ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
					ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

					fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
					common.set_ManageWebform(manageWebFormGenerator);
					manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
					manageWebFormGenerator.setLeadSourceDetails("None");
					manageWebFormGenerator.setDivision(addDivision3.getDivisionName());
					manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

					// Add Lead - WebForm
					for (int j = 0; j <= 0; j++) {
						Lead leadWebForm = new Lead();

						leadWebForm.setFirstName("LeadSource_WebForm_33899");
						leadWebForm.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
						leadWebForm.setEmail("frantest2017@gmail.com");
						// leadWebForm.setDivision(addDivision2.getDivisionName());
						leadWebForm.setCountry("USA");
						leadWebForm.setStateProvince("Florida");
						// leadApi.setCounty("Gulf");
						// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
						// leadWebForm.setBasedonAssignmentRules("yes");
						leadWebForm.setLeadSourceCategory("Friends");
						leadWebForm.setLeadSourceDetails("Friends");

						// Add Lead through WebForm
						manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm);

						// fc.utobj().sleep();
						String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
						String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
						System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>"
								+ ownerAssignedTowebformLead);
						// Validation
						if (!ownerAssignedTowebformLead.equalsIgnoreCase("FranConnect Administrator")) {
							System.out.println("Owner should be ::: FranConnect Administrator");
							fc.utobj().throwsException(
									"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33899 - WebForm - FAILED");
						}
					}
			
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			
		}
	}
	
	@Test(priority =204,dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_75-Import", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33899 , Import")
	private void OwnerAssignment_75_Import() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			try {
				
				 driver = fc.loginpage().login(driver);
				 
				// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
					// Create CSV File with Lead Details
					String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_33899.csv";
					System.out.println(filePath);
					List<Lead> leadList = new ArrayList<Lead>();

					for (int j = 0; j <= 0; j++) {
						Lead leadImport = new Lead();
						
						leadImport.setFirstName("LeadSource_Import_33899");
						leadImport.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
						leadImport.setEmail("frantest2017@gmail.com");
						leadImport.setDivision(addDivision3.getDivisionName());
						leadImport.setCountry("USA");
						leadImport.setStateProvince("Florida");
						// leadApi.setCounty("Gulf");
						// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
						leadImport.setBasedonAssignmentRules("yes");
						leadImport.setLeadSourceCategory("Friends");
						leadImport.setLeadSourceDetails("Friends");
						
						leadList.add(leadImport);
					}

					common.writeCsvFile(filePath, leadList);

					// Import the CSV file
					Import importlead = new Import();
					ImportTest importTest = new ImportTest(driver);

					// Navigate to Import Tab
					fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

					importlead.setImportType("Leads");
					importlead.setSpecifyFileFormat("CSV");
					importlead.setSalesDataFile(filePath);
					importlead.setLeadStatus("New Lead");
					importlead.setLeadSourceCategory("Friends");
					importlead.setLeadSourceDetails("Friends");

					importTest.importLeadsCSV(importlead);

					// Validation
					Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

					Lead x = new Lead();
					List<String> leadsFirstNames = map.get("FN");
					List<String> leadsLastNames = map.get("LN");

					for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
						System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
						x.setFirstName(leadsFirstNames.get(a));
						x.setLastName(leadsLastNames.get(b));

						String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
						String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
						System.out
								.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

						// Validation
						if (!ownerAssignedToLeadx.equalsIgnoreCase("FranConnect Administrator")) {
							System.out.println("Owner should be ::: FranConnect Administrator");
							fc.utobj().throwsException(
									"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33899 - Import - FAILED");
						}
					}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			
		}
	}
	
// =============================================================================================================================================================================================================	
	
	
	@Test(priority =205,dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_76-Api", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33898 , Api")
	private void OwnerAssignment_76_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());


		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			try {
				
				 driver = fc.loginpage().login(driver);
								 
			// Through API
			// ------------------------------------------------------------------------------------------------------------------------------------------------------------
			for (int j = 0; j <= 0; j++) {
				Lead leadApi = new Lead();
				
				leadApi.setFirstName("LeadSource_Api_33898");
				leadApi.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				leadApi.setDivision(addDivision3.getDivisionName());
				leadApi.setCountry("USA");
				leadApi.setStateProvince("Florida");
				// leadApi.setCounty("Gulf");
				// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory("Friends");
				leadApi.setLeadSourceDetails("Friends");
				
				common.addLeadThroughWebServices(null, leadApi);
				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(null, leadApi);
				// System.out.println(xmlApi);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println(
						"Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
				// Validation
				// Validate Owner Assignment
				// Validation
				if (!ownerAssignedToLeadApi.equalsIgnoreCase("FranConnect Administrator")) {
					System.out.println("Owner should be ::: FranConnect Administrator");
					fc.utobj().throwsException(
							"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33898 - API - FAILED");
				}
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

			} catch (Exception e) {
				 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
					
		}
	}
	
	
	@Test(priority =206 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_76-WebForm", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33898 , PriorityST WebForm")
	private void OwnerAssignment_76_WebForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			try {
				
				 driver = fc.loginpage().login(driver);
				 
				// Through WebForm
					// ----------------------------------------------------------------------------------------------------------------------------------------------------------
					// Create WebForm
					ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
					ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

					fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
					common.set_ManageWebform(manageWebFormGenerator);
					manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
					manageWebFormGenerator.setLeadSourceDetails("None");
					manageWebFormGenerator.setDivision(addDivision3.getDivisionName());
					manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

					// Add Lead - WebForm
					for (int j = 0; j <= 0; j++) {
						Lead leadWebForm = new Lead();

						leadWebForm.setFirstName("LeadSource_WebForm_33898");
						leadWebForm.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
						leadWebForm.setEmail("frantest2017@gmail.com");
						// leadWebForm.setDivision(addDivision2.getDivisionName());
						leadWebForm.setCountry("USA");
						leadWebForm.setStateProvince("Florida");
						// leadApi.setCounty("Gulf");
						// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
						// leadWebForm.setBasedonAssignmentRules("yes");
						leadWebForm.setLeadSourceCategory("Friends");
						leadWebForm.setLeadSourceDetails("Friends");

						// Add Lead through WebForm
						manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm);

						// fc.utobj().sleep();
						String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
						String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
						System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>"
								+ ownerAssignedTowebformLead);
						// Validation
						if (!ownerAssignedTowebformLead.equalsIgnoreCase("FranConnect Administrator")) {
							System.out.println("Owner should be ::: FranConnect Administrator");
							fc.utobj().throwsException(
									"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33898 - WebForm - FAILED");
						}
					}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			
		}
	}
	
	
	@Test(priority =207 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_76-Import", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33898 , PriorityST Import")
	private void OwnerAssignment_76_Import() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			try {
				
				 driver = fc.loginpage().login(driver);
				 
				// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
					// Create CSV File with Lead Details
					String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_33898.csv";
					System.out.println(filePath);
					List<Lead> leadList = new ArrayList<Lead>();

					for (int j = 0; j <= 0; j++) {
						Lead leadImport = new Lead();
						
						leadImport.setFirstName("LeadSource_Import_33898");
						leadImport.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
						leadImport.setEmail("frantest2017@gmail.com");
						leadImport.setDivision(addDivision3.getDivisionName());
						leadImport.setCountry("USA");
						leadImport.setStateProvince("Florida");
						// leadApi.setCounty("Gulf");
						// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
						leadImport.setBasedonAssignmentRules("yes");
						leadImport.setLeadSourceCategory("Friends");
						leadImport.setLeadSourceDetails("Friends");
						
						leadList.add(leadImport);
					}

					common.writeCsvFile(filePath, leadList);

					// Import the CSV file
					Import importlead = new Import();
					ImportTest importTest = new ImportTest(driver);

					// Navigate to Import Tab
					fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

					importlead.setImportType("Leads");
					importlead.setSpecifyFileFormat("CSV");
					importlead.setSalesDataFile(filePath);
					importlead.setLeadStatus("New Lead");
					importlead.setLeadSourceCategory("Friends");
					importlead.setLeadSourceDetails("Friends");

					importTest.importLeadsCSV(importlead);

					// Validation
					Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

					Lead x = new Lead();
					List<String> leadsFirstNames = map.get("FN");
					List<String> leadsLastNames = map.get("LN");

					for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
						System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
						x.setFirstName(leadsFirstNames.get(a));
						x.setLastName(leadsLastNames.get(b));

						String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
						String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
						System.out
								.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

						// Validation
						if (!ownerAssignedToLeadx.equalsIgnoreCase("FranConnect Administrator")) {
							System.out.println("Owner should be ::: FranConnect Administrator");
							fc.utobj().throwsException(
									"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33898 - Import - FAILED");
						}
					}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			
		}
	}
	
// =============================================================================================================================================================================================================	
	
	
	@Test(priority =208,dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_77-Api", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33897 ,  PriorityST Api")
	private void OwnerAssignment_77_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			try {
				
				 driver = fc.loginpage().login(driver);
								 
			// Through API
			// ------------------------------------------------------------------------------------------------------------------------------------------------------------
			for (int j = 0; j <= 0; j++) {
				Lead leadApi = new Lead();
				
				leadApi.setFirstName("LeadSource_Api_33897");
				leadApi.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				leadApi.setDivision(addDivision3.getDivisionName());
				leadApi.setCountry("USA");
				leadApi.setStateProvince("Florida");
				// leadApi.setCounty("Gulf");
				// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
				leadApi.setLeadSourceDetails("None");
				
				common.addLeadThroughWebServices(null, leadApi);
				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(null, leadApi);
				// System.out.println(xmlApi);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println(
						"Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
				// Validation
				// Validate Owner Assignment
				// Validation
				if (!corpUsersForSource2.contains(ownerAssignedToLeadApi)) {
					System.out.println("Owner should be among ::: " + corpUsersForSource2);
					fc.utobj().throwsException(
							"Owner Assignment - Assign Lead Owner by Lead Source - TC-33897 - API - FAILED");
				}
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

			} catch (Exception e) {
				 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);		
		}
	}
	
	@Test(priority =209 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_77-WebForm", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33897 , PriorityST WebForm")
	private void OwnerAssignment_77_WebForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			try {
				
				 driver = fc.loginpage().login(driver);
				 
				// Through WebForm
					// ----------------------------------------------------------------------------------------------------------------------------------------------------------
					// Create WebForm
					ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
					ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

					fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
					common.set_ManageWebform(manageWebFormGenerator);
					manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
					manageWebFormGenerator.setLeadSourceDetails("None");
					// manageWebFormGenerator.setDivision(addDivision2.getDivisionName());
					manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

					// Add Lead - WebForm
					for (int j = 0; j <= 0; j++) {
						Lead leadWebForm = new Lead();

						leadWebForm.setFirstName("LeadSource_WebForm_33897");
						leadWebForm.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
						leadWebForm.setEmail("frantest2017@gmail.com");
						// leadWebForm.setDivision(addDivision2.getDivisionName());
						leadWebForm.setCountry("USA");
						leadWebForm.setStateProvince("Florida");
						// leadApi.setCounty("Gulf");
						// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
						// leadWebForm.setBasedonAssignmentRules("yes");
						leadWebForm.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
						leadWebForm.setLeadSourceDetails("None");

						// Add Lead through WebForm
						manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm);

						// fc.utobj().sleep();
						String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
						String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
						System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>"
								+ ownerAssignedTowebformLead);
						// Validation
						if (!corpUsersForSource2.contains(ownerAssignedTowebformLead)) {
							System.out.println("Owner should be among ::: " + corpUsersForSource2);
							fc.utobj().throwsException(
									"Owner Assignment - Assign Lead Owner by Lead Source - TC-33897 - WebForm - FAILED");
						}
					}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			
		}
	}
	
	@Test(priority =210, dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_77-Import", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33897 , PriorityST Import")
	private void OwnerAssignment_77_Import() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			try {
				
				 driver = fc.loginpage().login(driver);
				 
				// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
					// Create CSV File with Lead Details
					String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_33897.csv";
					System.out.println(filePath);
					List<Lead> leadList = new ArrayList<Lead>();

					for (int j = 0; j <= 0; j++) {
						Lead leadImport = new Lead();
						
						leadImport.setFirstName("LeadSource_Import_33897");
						leadImport.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
						leadImport.setEmail("frantest2017@gmail.com");
						leadImport.setDivision(addDivision3.getDivisionName());
						leadImport.setCountry("USA");
						leadImport.setStateProvince("Florida");
						// leadApi.setCounty("Gulf");
						// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
						leadImport.setBasedonAssignmentRules("yes");
						leadImport.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
						leadImport.setLeadSourceDetails("None");
						
						leadList.add(leadImport);
					}

					common.writeCsvFile(filePath, leadList);

					// Import the CSV file
					Import importlead = new Import();
					ImportTest importTest = new ImportTest(driver);

					// Navigate to Import Tab
					fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

					importlead.setImportType("Leads");
					importlead.setSpecifyFileFormat("CSV");
					importlead.setSalesDataFile(filePath);
					importlead.setLeadStatus("New Lead");
					importlead.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
					importlead.setLeadSourceDetails("None");

					importTest.importLeadsCSV(importlead);

					// Validation
					Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

					Lead x = new Lead();
					List<String> leadsFirstNames = map.get("FN");
					List<String> leadsLastNames = map.get("LN");

					for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
						System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
						x.setFirstName(leadsFirstNames.get(a));
						x.setLastName(leadsLastNames.get(b));

						String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
						String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
						System.out
								.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

						// Validation
						if (!corpUsersForSource2.contains(ownerAssignedToLeadx)) {
							System.out.println("Owner should be among ::: " + corpUsersForSource2);
							fc.utobj().throwsException(
									"Owner Assignment - Assign Lead Owner by Lead Source - TC-33897 - Import - FAILED");
						}
					}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			
		}
	}
	
// =============================================================================================================================================================================================================	
	
	
	@Test(priority =211, dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_78-Api", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33896 PriorityST Api")
	private void OwnerAssignment_78_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			try {
				
				 driver = fc.loginpage().login(driver);
								 
			// Through API
			// ------------------------------------------------------------------------------------------------------------------------------------------------------------
			for (int j = 0; j <= 0; j++) {
				Lead leadApi = new Lead();
				
				leadApi.setFirstName("LeadSource_Api_33896");
				leadApi.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				// leadApi.setDivision(addDivision2.getDivisionName());
				leadApi.setCountry("USA");
				leadApi.setStateProvince("Florida");
				leadApi.setCounty("Gulf");
				// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
				leadApi.setLeadSourceDetails("None");
				
				common.addLeadThroughWebServices(null, leadApi);
				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(null, leadApi);
				// System.out.println(xmlApi);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println(
						"Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
				// Validate Owner Assignment
				// Validation
				if (!corpUsersForSalesTerritoriesCounty.contains(ownerAssignedToLeadApi)) {
					System.out.println("Owner should be among ::: " + corpUsersForSalesTerritoriesCounty);

					fc.utobj().throwsException(
							"Owner Assignment - Assign Lead Owner by Lead Source - TC-33896 - API - FAILED");
				}
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			
		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			/*System.out.println(e.getMessage());
			fc.utobj().throwsException1(e.getMessage());				*/
		}
	}
	
	
	@Test(priority =212,dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_78-WebForm", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33896 , PriorityST WebForm")
	private void OwnerAssignment_78_WebForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			try {
				
				 driver = fc.loginpage().login(driver);
				 
				// Through WebForm
					// ----------------------------------------------------------------------------------------------------------------------------------------------------------
					// Create WebForm
					ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
					ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

					fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
					common.set_ManageWebform(manageWebFormGenerator);
					manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
					manageWebFormGenerator.setLeadSourceDetails("None");
					// manageWebFormGenerator.setDivision(addDivision1.getDivisionName());
					manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

					// Add Lead - WebForm
					for (int j = 0; j <= 0; j++) {
						Lead leadWebForm = new Lead();

						leadWebForm.setFirstName("LeadSource_WebForm_33896");
						leadWebForm.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
						leadWebForm.setEmail("frantest2017@gmail.com");
						// leadApi.setDivision(addDivision2.getDivisionName());
						leadWebForm.setCountry("USA");
						leadWebForm.setStateProvince("Florida");
						leadWebForm.setCounty("Gulf");
						// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
						// leadWebForm.setBasedonAssignmentRules("yes");
						leadWebForm.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
						leadWebForm.setLeadSourceDetails("None");

						// Add Lead through WebForm
						manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm);

						// fc.utobj().sleep();
						String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
						String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
						System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>"
								+ ownerAssignedTowebformLead);
						// Validation
						if (!corpUsersForSalesTerritoriesCounty.contains(ownerAssignedTowebformLead)) {
							System.out.println("Owner should be among ::: " + corpUsersForSalesTerritoriesCounty);
							fc.utobj().throwsException(
									"Owner Assignment - Assign Lead Owner by Lead Source - TC-33896 - Webform - FAILED");
						}
					}
			
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			
		}
	}
	
	
	@Test(priority =213,dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" /*, "sales_OwnerAssignment_Failed"*/ })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_78-Import", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33896 , PriorityST , Import")
	private void OwnerAssignment_78_Import() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			try {
				
				 driver = fc.loginpage().login(driver);
				 
				// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
					// Create CSV File with Lead Details
					String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_33896.csv";
					System.out.println(filePath);
					List<Lead> leadList = new ArrayList<Lead>();

					for (int j = 0; j <= 0; j++) {
						Lead leadImport = new Lead();

						leadImport.setFirstName("LeadSource_Import_33896");
						leadImport.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
						leadImport.setEmail("frantest2017@gmail.com");
						// leadApi.setDivision(addDivision2.getDivisionName());
						leadImport.setCountry("USA");
						leadImport.setStateProvince("Florida");
						leadImport.setCounty("Gulf");
						// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
						leadImport.setBasedonAssignmentRules("yes");
						leadImport.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
						leadImport.setLeadSourceDetails("None");
						
						leadList.add(leadImport);
					}

					common.writeCsvFile(filePath, leadList);

					// Import the CSV file
					Import importlead = new Import();
					ImportTest importTest = new ImportTest(driver);

					// Navigate to Import Tab
					fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

					importlead.setImportType("Leads");
					importlead.setSpecifyFileFormat("CSV");
					importlead.setSalesDataFile(filePath);
					importlead.setLeadStatus("New Lead");
					importlead.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
					importlead.setLeadSourceDetails("None");

					importTest.importLeadsCSV(importlead);

					// Validation
					Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

					Lead x = new Lead();
					List<String> leadsFirstNames = map.get("FN");
					List<String> leadsLastNames = map.get("LN");

					for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
						System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
						x.setFirstName(leadsFirstNames.get(a));
						x.setLastName(leadsLastNames.get(b));

						String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
						String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
						System.out
								.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

						// Validation
						if (!corpUsersForSource2.contains(ownerAssignedToLeadx)) {
							System.out.println("Owner should be among ::: " + corpUsersForSource2);

							fc.utobj().throwsException(
									"Owner Assignment - Assign Lead Owner by Lead Source - TC-33896 - Import - FAILED");
						}
					}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			
		}
	}
	
// =============================================================================================================================================================================================================	
	
	
	@Test(priority =214 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_79-Api", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33895 , PriorityST , Api")
	private void OwnerAssignment_79_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			try {
				
				 driver = fc.loginpage().login(driver);
								 
			// Through API
			// ------------------------------------------------------------------------------------------------------------------------------------------------------------
			for (int j = 0; j <= 0; j++) {
				Lead leadApi = new Lead();
				
				leadApi.setFirstName("LeadSource_Api_33895");
				leadApi.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				leadApi.setDivision(addDivision3.getDivisionName());
				leadApi.setCountry("USA");
				leadApi.setStateProvince("Florida");
				leadApi.setCounty("Gulf");
				// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory("Friends");
				leadApi.setLeadSourceDetails("Friends");
				
				common.addLeadThroughWebServices(null, leadApi);
				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(null, leadApi);
				// System.out.println(xmlApi);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println(
						"Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
				// Validation
				// Validate Owner Assignment
				// Validation
				if (!ownerAssignedToLeadApi.equalsIgnoreCase("FranConnect Administrator")) {
					System.out.println("Owner should be ::: FranConnect Administrator");
					fc.utobj().throwsException(
							"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33895 - API - FAILED");
				}
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			
		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			/*System.out.println(e.getMessage());
			fc.utobj().throwsException1(e.getMessage());	*/
		}
	}
	
	@Test(priority =215, dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_79-WebForm", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33895 , PriorityST , WebForm")
	private void OwnerAssignment_79_WebForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			try {
				
				 driver = fc.loginpage().login(driver);
				 
				// Through WebForm
					// ----------------------------------------------------------------------------------------------------------------------------------------------------------
					// Create WebForm
					ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
					ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

					fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
					common.set_ManageWebform(manageWebFormGenerator);
					manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
					manageWebFormGenerator.setLeadSourceDetails("None");
					manageWebFormGenerator.setDivision(addDivision3.getDivisionName());
					manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

					// Add Lead - WebForm
					for (int j = 0; j <= 0; j++) {
						Lead leadWebForm = new Lead();

						leadWebForm.setFirstName("LeadSource_WebForm_33895");
						leadWebForm.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
						leadWebForm.setEmail("frantest2017@gmail.com");
						// leadWebForm.setDivision(addDivision2.getDivisionName());
						leadWebForm.setCountry("USA");
						leadWebForm.setStateProvince("Florida");
						leadWebForm.setCounty("Gulf");
						// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
						// leadWebForm.setBasedonAssignmentRules("yes");
						leadWebForm.setLeadSourceCategory("Friends");
						leadWebForm.setLeadSourceDetails("Friends");

						// Add Lead through WebForm
						manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm);

						// fc.utobj().sleep();
						String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
						String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
						System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>"
								+ ownerAssignedTowebformLead);
						// Validation
			
						if (!ownerAssignedTowebformLead.equalsIgnoreCase("FranConnect Administrator")) {
							System.out.println("Owner should be ::: FranConnect Administrator");
							fc.utobj().throwsException(
									"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33895 - WebForm - FAILED");
						}
					}
				 
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			
		}
	}
	
	@Test(priority =216 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_79-Import", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33895 , PriorityST Import")
	private void OwnerAssignment_79_Import() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			try {
				
				 driver = fc.loginpage().login(driver);
			
				// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
					// Create CSV File with Lead Details
					String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_Source_PriorityST_County.csv";
					System.out.println(filePath);
					List<Lead> leadList = new ArrayList<Lead>();

					for (int j = 0; j <= 0; j++) {
						Lead leadImport = new Lead();
						
						leadImport.setFirstName("LeadSource_Import_33895");
						leadImport.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
						leadImport.setEmail("frantest2017@gmail.com");
						leadImport.setDivision(addDivision3.getDivisionName());
						leadImport.setCountry("USA");
						leadImport.setStateProvince("Florida");
						leadImport.setCounty("Gulf");
						// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
						leadImport.setBasedonAssignmentRules("yes");
						leadImport.setLeadSourceCategory("Friends");
						leadImport.setLeadSourceDetails("Friends");
						
						leadList.add(leadImport);
					}

					common.writeCsvFile(filePath, leadList);

					// Import the CSV file
					Import importlead = new Import();
					ImportTest importTest = new ImportTest(driver);

					// Navigate to Import Tab
					fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

					importlead.setImportType("Leads");
					importlead.setSpecifyFileFormat("CSV");
					importlead.setSalesDataFile(filePath);
					importlead.setLeadStatus("New Lead");
					importlead.setLeadSourceCategory("Friends");
					importlead.setLeadSourceDetails("Friends");

					importTest.importLeadsCSV(importlead);

					// Validation
					Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

					Lead x = new Lead();
					List<String> leadsFirstNames = map.get("FN");
					List<String> leadsLastNames = map.get("LN");

					for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
						System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
						x.setFirstName(leadsFirstNames.get(a));
						x.setLastName(leadsLastNames.get(b));

						String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
						String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
						System.out
								.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

						// Validation
						if (!ownerAssignedToLeadx.equalsIgnoreCase("FranConnect Administrator")) {
							System.out.println("Owner should be ::: FranConnect Administrator");
							fc.utobj().throwsException(
									"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33895 - Import - FAILED");
						}
					}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			
		}
	}
	
// =============================================================================================================================================================================================================	
	
	
	@Test(priority =217 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_80-Api", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33894 , PriorityST , Api")
	private void OwnerAssignment_80_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

			try {
								 
			// Through API
			// ------------------------------------------------------------------------------------------------------------------------------------------------------------
			for (int j = 0; j <= 0; j++) {
				Lead leadApi = new Lead();
				
				leadApi.setFirstName("LeadSource_Api_33894");
				leadApi.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				leadApi.setDivision(addDivision3.getDivisionName());
				leadApi.setCountry("USA");
				// leadApi.setStateProvince("Florida");
				leadApi.setCounty("Gulf");
				// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory("Friends");
				leadApi.setLeadSourceDetails("Friends");
				
				common.addLeadThroughWebServices(null, leadApi);
				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(null, leadApi);
				// System.out.println(xmlApi);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println(
						"Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);

				// Validate Owner Assignment
				// Validation
				if (!ownerAssignedToLeadApi.equalsIgnoreCase("FranConnect Administrator")) {
					System.out.println("Owner should be ::: FranConnect Administrator");
					fc.utobj().throwsException(
							"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33894 - API - FAILED");
				}
			}
			
			fc.utobj().logoutAndQuitBrowser(null, testCaseId);
		
		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(null, e, testCaseId);

			/*System.out.println(e.getMessage());
			fc.utobj().throwsException1(e.getMessage());*/				
		}
	}
	
	@Test(priority =218 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" , "TC_33894" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_80-WebForm", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33894 Priority Sales Territories , Lead add through WebForm")
	private void OwnerAssignment_80_WebForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			try {
				
				 driver = fc.loginpage().login(driver);
				 
				// Through WebForm
					// ----------------------------------------------------------------------------------------------------------------------------------------------------------
					// Create WebForm
					ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
					ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

					fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
					common.set_ManageWebform(manageWebFormGenerator);
					manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
					manageWebFormGenerator.setLeadSourceDetails("None");
					manageWebFormGenerator.setDivision(addDivision3.getDivisionName());
					manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

					// Add Lead - WebForm
					for (int j = 0; j <= 0; j++) {
						Lead leadWebForm = new Lead();

						leadWebForm.setFirstName("LeadSource_WebForm_33894");
						leadWebForm.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
						leadWebForm.setEmail("frantest2017@gmail.com");
						// leadWebForm.setDivision(addDivision2.getDivisionName());
						leadWebForm.setCountry("USA");
						// leadApi.setStateProvince("Florida");
						leadWebForm.setCounty("Gulf");
						// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
						leadWebForm.setLeadSourceCategory("Friends");
						leadWebForm.setLeadSourceDetails("Friends");

						// Add Lead through WebForm
						manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm);

						// fc.utobj().sleep();
						String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
						String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
						System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>"
								+ ownerAssignedTowebformLead);
						// Validation
						if (!ownerAssignedTowebformLead.equalsIgnoreCase("FranConnect Administrator")) {
							System.out.println("Owner should be ::: FranConnect Administrator");
							fc.utobj().throwsException(
									"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33894 - WebForm - FAILED");
						}
					}
			
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			
		}
	}
	
	@Test(priority =219 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_80-Import", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33894 , Priority Sales Territories , Lead add through Import")
	private void OwnerAssignment_80_Import() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			try {
				
				 driver = fc.loginpage().login(driver);
				 
				// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
					// Create CSV File with Lead Details
					String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_33894.csv";
					System.out.println(filePath);
					List<Lead> leadList = new ArrayList<Lead>();

					for (int j = 0; j <= 0; j++) {
						Lead leadImport = new Lead();
						
						leadImport.setFirstName("LeadSource_Import_33894");
						leadImport.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
						leadImport.setEmail("frantest2017@gmail.com");
						leadImport.setDivision(addDivision3.getDivisionName());
						leadImport.setCountry("USA");
						// leadApi.setStateProvince("Florida");
						leadImport.setCounty("Gulf");
						// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
						leadImport.setBasedonAssignmentRules("yes");
						leadImport.setLeadSourceCategory("Friends");
						leadImport.setLeadSourceDetails("Friends");
						
						leadList.add(leadImport);
					}

					common.writeCsvFile(filePath, leadList);

					// Import the CSV file
					Import importlead = new Import();
					ImportTest importTest = new ImportTest(driver);

					// Navigate to Import Tab
					fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

					importlead.setImportType("Leads");
					importlead.setSpecifyFileFormat("CSV");
					importlead.setSalesDataFile(filePath);
					importlead.setLeadStatus("New Lead");
					importlead.setLeadSourceCategory("Friends");
					importlead.setLeadSourceDetails("Friends");

					importTest.importLeadsCSV(importlead);

					// Validation
					Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

					Lead x = new Lead();
					List<String> leadsFirstNames = map.get("FN");
					List<String> leadsLastNames = map.get("LN");

					for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
						System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
						x.setFirstName(leadsFirstNames.get(a));
						x.setLastName(leadsLastNames.get(b));

						String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
						String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
						System.out
								.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

						// Validation
						if (!ownerAssignedToLeadx.equalsIgnoreCase("FranConnect Administrator")) {
							System.out.println("Owner should be ::: FranConnect Administrator");
							fc.utobj().throwsException(
									"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33894 - Import - FAILED");
						}
					}
		
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			
		}
	}
	
// =============================================================================================================================================================================================================	
	
	
	@Test(priority =220 , dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_81-Api", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33893 Priority Sales Territories , when lead added through Api")
	private void OwnerAssignment_81_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

			try {
								 
			// Through API
			// ------------------------------------------------------------------------------------------------------------------------------------------------------------
			for (int j = 0; j <= 0; j++) {
				Lead leadApi = new Lead();
				
				leadApi.setFirstName("LeadSource_Api_33893");
				leadApi.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				// leadApi.setDivision(addDivision2.getDivisionName());
				leadApi.setCountry("USA");
				// leadApi.setStateProvince("Florida");
				// leadApi.setCounty("Gulf");
				leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
				leadApi.setLeadSourceDetails("None");
				
				common.addLeadThroughWebServices(null, leadApi);
				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(null, leadApi);
				// System.out.println(xmlApi);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println(
						"Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
				// Validation
				// Validate Owner Assignment
				// Validation
				if (!corpUsersForSalesTerritoriesZip.contains(ownerAssignedToLeadApi)) {
					System.out.println("Owner should be among ::: " + corpUsersForSalesTerritoriesZip);
					fc.utobj().throwsException("Owner Assignment - Assign Lead Owner by Lead Source - For Territory(ZIP) - TC-33893 - API - FAILED");
				}
			}
			
			fc.utobj().logoutAndQuitBrowser(null, testCaseId);
			
		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(null, e, testCaseId);

			/*System.out.println(e.getMessage());
			fc.utobj().throwsException1(e.getMessage());*/				
		}
	}
	
	@Test(priority =221,dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_81-WebForm", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source , adding lead through WebForm TC-33893 , Priority Sales Territories")
	private void OwnerAssignment_81_WebForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			try {
				
				 driver = fc.loginpage().login(driver);
				 
				// Through WebForm
					// ----------------------------------------------------------------------------------------------------------------------------------------------------------
					// Create WebForm
					ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
					ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

					fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
					common.set_ManageWebform(manageWebFormGenerator);
					manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
					manageWebFormGenerator.setLeadSourceDetails("None");
					// manageWebFormGenerator.setDivision(addDivision1.getDivisionName());
					manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

					// Add Lead - WebForm
					for (int j = 0; j <= 0; j++) {
						Lead leadWebForm = new Lead();

						leadWebForm.setFirstName("LeadSource_WebForm_33893");
						leadWebForm.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
						leadWebForm.setEmail("frantest2017@gmail.com");
						// leadApi.setDivision(addDivision2.getDivisionName());
						leadWebForm.setCountry("USA");
						// leadApi.setStateProvince("Florida");
						// leadApi.setCounty("Gulf");
						leadWebForm.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
						// leadWebForm.setBasedonAssignmentRules("yes");
						leadWebForm.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
						leadWebForm.setLeadSourceDetails("None");

						// Add Lead through WebForm
						manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm);

						// fc.utobj().sleep();
						String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
						String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
						System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>"
								+ ownerAssignedTowebformLead);
						// Validation
						if (!corpUsersForSalesTerritoriesZip.contains(ownerAssignedTowebformLead)) {
							System.out.println("Owner should be among ::: " + corpUsersForSalesTerritoriesZip);
							fc.utobj().throwsException("Owner Assignment - Assign Lead Owner by Lead Source - For Territory(ZIP) - TC-33893 - WebForm - FAILED");
						}
					}
					
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			
		}
	}
	
	@Test(priority =222, dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_81-Import", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33893")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33893_Import() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			try {
				
				 driver = fc.loginpage().login(driver);
				 
				// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
					// Create CSV File with Lead Details
					String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_33893.csv";
					System.out.println(filePath);
					List<Lead> leadList = new ArrayList<Lead>();

					for (int j = 0; j <= 0; j++) {
						Lead leadImport = new Lead();
						
						leadImport.setFirstName("LeadSource_Import_33893");
						leadImport.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
						leadImport.setEmail("frantest2017@gmail.com");
						// leadApi.setDivision(addDivision2.getDivisionName());
						leadImport.setCountry("USA");
						// leadApi.setStateProvince("Florida");
						// leadApi.setCounty("Gulf");
						leadImport.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
						leadImport.setBasedonAssignmentRules("yes");
						leadImport.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
						leadImport.setLeadSourceDetails("None");
						
						leadList.add(leadImport);
					}

					common.writeCsvFile(filePath, leadList);

					// Import the CSV file
					Import importlead = new Import();
					ImportTest importTest = new ImportTest(driver);

					// Navigate to Import Tab
					fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

					importlead.setImportType("Leads");
					importlead.setSpecifyFileFormat("CSV");
					importlead.setSalesDataFile(filePath);
					importlead.setLeadStatus("New Lead");
					importlead.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
					importlead.setLeadSourceDetails("None");

					importTest.importLeadsCSV(importlead);

					// Validation
					Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

					Lead x = new Lead();
					List<String> leadsFirstNames = map.get("FN");
					List<String> leadsLastNames = map.get("LN");

					for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
						System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
						x.setFirstName(leadsFirstNames.get(a));
						x.setLastName(leadsLastNames.get(b));

						String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
						String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
						System.out
								.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

						// Validation
						if (!corpUsersForSalesTerritoriesZip.contains(ownerAssignedToLeadx)) {
							System.out.println("Owner should be among ::: " + corpUsersForSalesTerritoriesZip);
							fc.utobj().throwsException("Owner Assignment - Assign Lead Owner by Lead Source - For Territory(ZIP) - TC-33893 - Import - FAILED");
						}
					}
					
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			
		}
	}
	
// =============================================================================================================================================================================================================	
	
	
	@Test(priority =223,dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_82-Api", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33892")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33892_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

			try {
								 
			// Through API
			// ------------------------------------------------------------------------------------------------------------------------------------------------------------
			for (int j = 0; j <= 0; j++) {
				Lead leadApi = new Lead();
				
				leadApi.setFirstName("LeadSource_Api_33892");
				leadApi.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				leadApi.setDivision(addDivision3.getDivisionName());
				leadApi.setCountry("USA");
				// leadApi.setStateProvince("Florida");
				// leadApi.setCounty("Gulf");
				leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory("Friends");
				leadApi.setLeadSourceDetails("Friends");
				
				common.addLeadThroughWebServices(null, leadApi);
				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(null, leadApi);
				// System.out.println(xmlApi);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println(
						"Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
				// Validation
				// Validate Owner Assignment
				// Validation
				if (!ownerAssignedToLeadApi.equalsIgnoreCase("FranConnect Administrator")) {
					System.out.println("Owner should be ::: FranConnect Administrator");
					fc.utobj().throwsException(
							"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33892 - API - FAILED");
				}
			}
			
			fc.utobj().logoutAndQuitBrowser(null, testCaseId);
			
		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(null, e, testCaseId);

		/*	System.out.println(e.getMessage());
			fc.utobj().throwsException1(e.getMessage());	*/			
		}
	}
	
	@Test(priority =224,dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_82-WebForm", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33892")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33892_WebForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			try {
				
				 driver = fc.loginpage().login(driver);
				 
				// Through WebForm
					// ----------------------------------------------------------------------------------------------------------------------------------------------------------
					// Create WebForm
					ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
					ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

					fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
					common.set_ManageWebform(manageWebFormGenerator);
					manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
					manageWebFormGenerator.setLeadSourceDetails("None");
					manageWebFormGenerator.setDivision(addDivision3.getDivisionName());
					manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

					// Add Lead - WebForm
					for (int j = 0; j <= 0; j++) {
						Lead leadWebForm = new Lead();

						leadWebForm.setFirstName("LeadSource_WebForm_33892");
						leadWebForm.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
						leadWebForm.setEmail("frantest2017@gmail.com");
						// leadWebForm.setDivision(addDivision2.getDivisionName());
						leadWebForm.setCountry("USA");
						// leadApi.setStateProvince("Florida");
						// leadApi.setCounty("Gulf");
						leadWebForm.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
						// leadWebForm.setBasedonAssignmentRules("yes");
						leadWebForm.setLeadSourceCategory("Friends");
						leadWebForm.setLeadSourceDetails("Friends");

						// Add Lead through WebForm
						manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm);

						// fc.utobj().sleep();
						String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
						String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
						System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>"
								+ ownerAssignedTowebformLead);
						// Validation
						if (!ownerAssignedTowebformLead.equalsIgnoreCase("FranConnect Administrator")) {
							System.out.println("Owner should be ::: FranConnect Administrator");

							fc.utobj().throwsException(
									"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33892 - WebForm - FAILED");
						}
					}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			
		}
	}
	
	@Test(priority =225,dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_82-Import", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33892")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33892_Import() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			try {
				
				 driver = fc.loginpage().login(driver);
				 
				// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
					// Create CSV File with Lead Details
					String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_33892.csv";
					System.out.println(filePath);
					List<Lead> leadList = new ArrayList<Lead>();

					for (int j = 0; j <= 0; j++) {
						Lead leadImport = new Lead();
						
						leadImport.setFirstName("LeadSource_Import_33892");
						leadImport.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
						leadImport.setEmail("frantest2017@gmail.com");
						leadImport.setDivision(addDivision3.getDivisionName());
						leadImport.setCountry("USA");
						// leadApi.setStateProvince("Florida");
						// leadApi.setCounty("Gulf");
						leadImport.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
						leadImport.setBasedonAssignmentRules("yes");
						leadImport.setLeadSourceCategory("Friends");
						leadImport.setLeadSourceDetails("Friends");
						
						leadList.add(leadImport);
					}

					common.writeCsvFile(filePath, leadList);

					// Import the CSV file
					Import importlead = new Import();
					ImportTest importTest = new ImportTest(driver);

					// Navigate to Import Tab
					fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

					importlead.setImportType("Leads");
					importlead.setSpecifyFileFormat("CSV");
					importlead.setSalesDataFile(filePath);
					importlead.setLeadStatus("New Lead");
					importlead.setLeadSourceCategory("Friends");
					importlead.setLeadSourceDetails("Friends");

					importTest.importLeadsCSV(importlead);

					// Validation
					Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

					Lead x = new Lead();
					List<String> leadsFirstNames = map.get("FN");
					List<String> leadsLastNames = map.get("LN");

					for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
						System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
						x.setFirstName(leadsFirstNames.get(a));
						x.setLastName(leadsLastNames.get(b));

						String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
						String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
						System.out
								.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

						// Validation
						if (!ownerAssignedToLeadx.equalsIgnoreCase("FranConnect Administrator")) {
							System.out.println("Owner should be ::: FranConnect Administrator");

							fc.utobj().throwsException(
									"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33892 - Import - FAILED");
						}
					}
				 
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			
		}
	}
	
// =============================================================================================================================================================================================================	
	
	
	@Test(priority =226,dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_83-Api", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33891")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33891_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

			try {
								 
			// Through API
			// ------------------------------------------------------------------------------------------------------------------------------------------------------------
			for (int j = 0; j <= 0; j++) {
				Lead leadApi = new Lead();
				
				leadApi.setFirstName("LeadSource_Api_33891");
				leadApi.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				leadApi.setDivision(addDivision3.getDivisionName());
				leadApi.setCountry("USA");
				// leadApi.setStateProvince("Florida");
				// leadApi.setCounty("Gulf");
				leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
				leadApi.setLeadSourceDetails("None");
				
				common.addLeadThroughWebServices(null, leadApi);
				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(null, leadApi);
				// System.out.println(xmlApi);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println(
						"Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
				// Validation
				// Validate Owner Assignment
				// Validation
				if (!corpUsersForSalesTerritoriesZip.contains(ownerAssignedToLeadApi)) {
					System.out.println("Owner should be among ::: " + corpUsersForSalesTerritoriesZip);
					fc.utobj().throwsException("Owner Assignment - Assign Lead Owner by Lead Source - For Territory(ZIP) - TC-33891 - API - FAILED");
				}
			}
			

			fc.utobj().logoutAndQuitBrowser(null, testCaseId);

		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(null, e, testCaseId);

			/*System.out.println(e.getMessage());
			fc.utobj().throwsException1(e.getMessage());	*/			
		}
	}
	
	
	@Test(priority =227,dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_83-WebForm", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33891")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33891_Webform() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			try {
				
				 driver = fc.loginpage().login(driver);
				 
				// Through WebForm
					// ----------------------------------------------------------------------------------------------------------------------------------------------------------
					// Create WebForm
					ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
					ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

					fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
					common.set_ManageWebform(manageWebFormGenerator);
					manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
					manageWebFormGenerator.setLeadSourceDetails("None");
					manageWebFormGenerator.setDivision(addDivision3.getDivisionName());
					manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

					// Add Lead - WebForm
					for (int j = 0; j <= 0; j++) {
						Lead leadWebForm = new Lead();

						leadWebForm.setFirstName("LeadSource_WebForm_33891");
						leadWebForm.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
						leadWebForm.setEmail("frantest2017@gmail.com");
						// leadWebForm.setDivision(addDivision2.getDivisionName());
						leadWebForm.setCountry("USA");
						// leadApi.setStateProvince("Florida");
						// leadApi.setCounty("Gulf");
						leadWebForm.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
						// leadWebForm.setBasedonAssignmentRules("yes");
						leadWebForm.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
						leadWebForm.setLeadSourceDetails("None");

						// Add Lead through WebForm
						manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm);

						// fc.utobj().sleep();
						String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
						String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
						System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>"
								+ ownerAssignedTowebformLead);
						// Validation
						if (!corpUsersForSalesTerritoriesZip.contains(ownerAssignedTowebformLead)) {
							System.out.println("Owner should be among ::: " + corpUsersForSalesTerritoriesZip);
							fc.utobj().throwsException("Owner Assignment - Assign Lead Owner by Lead Source - For Territory(ZIP) - TC-33891 - WebForm - FAILED");
						}
					}
			
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			
		}
	}
	
	@Test(priority =228,dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_83-Import", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33891")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33891_Import() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			try {
				
				 driver = fc.loginpage().login(driver);
				 
				// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
					// Create CSV File with Lead Details
					String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_33891.csv";
					System.out.println(filePath);
					List<Lead> leadList = new ArrayList<Lead>();

					for (int j = 0; j <= 0; j++) {
						Lead leadImport = new Lead();
						
						leadImport.setFirstName("LeadSource_Import_33891");
						leadImport.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
						leadImport.setEmail("frantest2017@gmail.com");
						leadImport.setDivision(addDivision3.getDivisionName());
						leadImport.setCountry("USA");
						// leadApi.setStateProvince("Florida");
						// leadApi.setCounty("Gulf");
						leadImport.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
						leadImport.setBasedonAssignmentRules("yes");
						leadImport.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
						leadImport.setLeadSourceDetails("None");
						
						leadList.add(leadImport);
					}

					common.writeCsvFile(filePath, leadList);

					// Import the CSV file
					Import importlead = new Import();
					ImportTest importTest = new ImportTest(driver);

					// Navigate to Import Tab
					fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

					importlead.setImportType("Leads");
					importlead.setSpecifyFileFormat("CSV");
					importlead.setSalesDataFile(filePath);
					importlead.setLeadStatus("New Lead");
					importlead.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
					importlead.setLeadSourceDetails("None");

					importTest.importLeadsCSV(importlead);

					// Validation
					Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

					Lead x = new Lead();
					List<String> leadsFirstNames = map.get("FN");
					List<String> leadsLastNames = map.get("LN");

					for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
						System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
						x.setFirstName(leadsFirstNames.get(a));
						x.setLastName(leadsLastNames.get(b));

						String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
						String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
						System.out
								.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

						// Validation
						if (!corpUsersForSalesTerritoriesZip.contains(ownerAssignedToLeadx)) {
							System.out.println("Owner should be among ::: " + corpUsersForSalesTerritoriesZip);
							fc.utobj().throwsException("Owner Assignment - Assign Lead Owner by Lead Source - For Territory(ZIP) - TC-33891 - Import - FAILED");
						}
					}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			
		}
	}
// =============================================================================================================================================================================================================	
	
	
	@Test(priority =229,dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_84-Api", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33890")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33890_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

			try {
							 
			// Through API
			// ------------------------------------------------------------------------------------------------------------------------------------------------------------
			for (int j = 0; j <= 0; j++) {
				Lead leadApi = new Lead();
				
				leadApi.setFirstName("LeadSource_Api_33890");
				leadApi.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				// leadApi.setDivision(addDivision2.getDivisionName());
				leadApi.setCountry("USA");
				leadApi.setStateProvince("Florida");
				// leadApi.setCounty("Gulf");
				leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
				leadApi.setLeadSourceDetails("None");
				
				common.addLeadThroughWebServices(null, leadApi);
				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(null, leadApi);
				// System.out.println(xmlApi);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println(
						"Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
				// Validation
				// Validate Owner Assignment
				// Validation
				if (!corpUsersForSalesTerritoriesZip.contains(ownerAssignedToLeadApi)) {
					System.out.println("Owner should be among ::: " + corpUsersForSalesTerritoriesZip);
					fc.utobj().throwsException("Owner Assignment - Assign Lead Owner by Lead Source - For Territory(ZIP) - TC-33890 - API - FAILED");
				}
			}
			
			fc.utobj().logoutAndQuitBrowser(null, testCaseId);
			
		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(null, e, testCaseId);

			/*System.out.println(e.getMessage());
			fc.utobj().throwsException1(e.getMessage());		*/		
		}
	}
	
	@Test(priority =230,dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_84-WebForm", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33890")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33890_WebForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			try {
				
				 driver = fc.loginpage().login(driver);
				 
				// Through WebForm
					// ----------------------------------------------------------------------------------------------------------------------------------------------------------
					// Create WebForm
					ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
					ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

					fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
					common.set_ManageWebform(manageWebFormGenerator);
					manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
					manageWebFormGenerator.setLeadSourceDetails("None");
					// manageWebFormGenerator.setDivision(addDivision1.getDivisionName());
					manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

					// Add Lead - WebForm
					for (int j = 0; j <= 0; j++) {
						Lead leadWebForm = new Lead();

						leadWebForm.setFirstName("LeadSource_WebForm_33890");
						leadWebForm.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
						leadWebForm.setEmail("frantest2017@gmail.com");
						// leadApi.setDivision(addDivision2.getDivisionName());
						leadWebForm.setCountry("USA");
						leadWebForm.setStateProvince("Florida");
						// leadApi.setCounty("Gulf");
						leadWebForm.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
						// leadWebForm.setBasedonAssignmentRules("yes");
						leadWebForm.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
						leadWebForm.setLeadSourceDetails("None");

						// Add Lead through WebForm
						manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm);

						// fc.utobj().sleep();
						String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
						String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
						System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>"
								+ ownerAssignedTowebformLead);
						// Validation
						if (!corpUsersForSalesTerritoriesZip.contains(ownerAssignedTowebformLead)) {
							System.out.println("Owner should be among ::: " + corpUsersForSalesTerritoriesZip);
							fc.utobj().throwsException("Owner Assignment - Assign Lead Owner by Lead Source - For Territory(ZIP) - TC-33890 - WebForm - FAILED");
						}
					}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			
		}
	}
	
	@Test(priority =231,dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_84-Import", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33890")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33890_Import() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			try {
				
				 driver = fc.loginpage().login(driver);
				 
				// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
					// Create CSV File with Lead Details
					String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_33890.csv";
					System.out.println(filePath);
					List<Lead> leadList = new ArrayList<Lead>();

					for (int j = 0; j <= 0; j++) {
						Lead leadImport = new Lead();
						
						leadImport.setFirstName("LeadSource_Import_33890");
						leadImport.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
						leadImport.setEmail("frantest2017@gmail.com");
						// leadApi.setDivision(addDivision2.getDivisionName());
						leadImport.setCountry("USA");
						leadImport.setStateProvince("Florida");
						// leadApi.setCounty("Gulf");
						leadImport.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
						leadImport.setBasedonAssignmentRules("yes");
						leadImport.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
						leadImport.setLeadSourceDetails("None");
						
						leadList.add(leadImport);
					}

					common.writeCsvFile(filePath, leadList);

					// Import the CSV file
					Import importlead = new Import();
					ImportTest importTest = new ImportTest(driver);

					// Navigate to Import Tab
					fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

					importlead.setImportType("Leads");
					importlead.setSpecifyFileFormat("CSV");
					importlead.setSalesDataFile(filePath);
					importlead.setLeadStatus("New Lead");
					importlead.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
					importlead.setLeadSourceDetails("None");

					importTest.importLeadsCSV(importlead);

					// Validation
					Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

					Lead x = new Lead();
					List<String> leadsFirstNames = map.get("FN");
					List<String> leadsLastNames = map.get("LN");

					for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
						System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
						x.setFirstName(leadsFirstNames.get(a));
						x.setLastName(leadsLastNames.get(b));

						String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
						String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
						System.out
								.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

						// Validation
						if (!corpUsersForSalesTerritoriesZip.contains(ownerAssignedToLeadx)) {
							System.out.println("Owner should be among ::: " + corpUsersForSalesTerritoriesZip);
							fc.utobj().throwsException("Owner Assignment - Assign Lead Owner by Lead Source - For Territory(ZIP) - TC-33890 - Import - FAILED");
						}
					}
		
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			
		}
	}
	
// =============================================================================================================================================================================================================	
		
	@Test(priority =232,dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_85-Api", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33889")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33889_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

			try {
								 
			// Through API
			// ------------------------------------------------------------------------------------------------------------------------------------------------------------
			for (int j = 0; j <= 0; j++) {
				Lead leadApi = new Lead();
				
				leadApi.setFirstName("LeadSource_Api_33889");
				leadApi.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				leadApi.setDivision(addDivision3.getDivisionName());
				leadApi.setCountry("USA");
				leadApi.setStateProvince("Florida");
				// leadApi.setCounty("Gulf");
				leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory("Friends");
				leadApi.setLeadSourceDetails("Friends");
				
				common.addLeadThroughWebServices(null, leadApi);
				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(null, leadApi);
				// System.out.println(xmlApi);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println(
						"Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
				// Validation
				// Validate Owner Assignment
				// Validation
				if (!ownerAssignedToLeadApi.equalsIgnoreCase("FranConnect Administrator")) {
					System.out.println("Owner should be ::: FranConnect Administrator");
					fc.utobj().throwsException(
							"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33889 - API - FAILED");
				}
			}
			
			fc.utobj().logoutAndQuitBrowser(null, testCaseId);
		
		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(null, e, testCaseId);

			/*System.out.println(e.getMessage());
			fc.utobj().throwsException1(e.getMessage());*/				
		}
	}
	
	@Test(priority =233,dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_85-WebForm", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33889")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33889_WebForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			try {
				
				 driver = fc.loginpage().login(driver);
				 
				// Through WebForm
					// ----------------------------------------------------------------------------------------------------------------------------------------------------------
					// Create WebForm
					ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
					ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

					fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
					common.set_ManageWebform(manageWebFormGenerator);
					manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
					manageWebFormGenerator.setLeadSourceDetails("None");
					manageWebFormGenerator.setDivision(addDivision3.getDivisionName());
					manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

					// Add Lead - WebForm
					for (int j = 0; j <= 0; j++) {
						Lead leadWebForm = new Lead();

						leadWebForm.setFirstName("LeadSource_WebForm_33889");
						leadWebForm.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
						leadWebForm.setEmail("frantest2017@gmail.com");
						// leadWebForm.setDivision(addDivision2.getDivisionName());
						leadWebForm.setCountry("USA");
						leadWebForm.setStateProvince("Florida");
						// leadApi.setCounty("Gulf");
						leadWebForm.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
						// leadWebForm.setBasedonAssignmentRules("yes");
						leadWebForm.setLeadSourceCategory("Friends");
						leadWebForm.setLeadSourceDetails("Friends");
						
						
						// Add Lead through WebForm
						manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm);

						// fc.utobj().sleep();
						String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
						String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
						System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>"
								+ ownerAssignedTowebformLead);
						// Validation
						if (!ownerAssignedTowebformLead.equalsIgnoreCase("FranConnect Administrator")) {
							System.out.println("Owner should be ::: FranConnect Administrator");
							fc.utobj().throwsException(
									"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33889 - WebForm - FAILED");
						}
					}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			
		}
	}
	
	@Test(priority =234,dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment"})
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_85-Import", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33889")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33889_Import() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			try {
				
				 driver = fc.loginpage().login(driver);
				
				// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
					// Create CSV File with Lead Details
					String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_33889.csv";
					System.out.println(filePath);
					List<Lead> leadList = new ArrayList<Lead>();

					for (int j = 0; j <= 0; j++) {
						Lead leadImport = new Lead();
						
						leadImport.setFirstName("LeadSource_Import_33889");
						leadImport.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
						leadImport.setEmail("frantest2017@gmail.com");
						leadImport.setDivision(addDivision3.getDivisionName());
						leadImport.setCountry("USA");
						leadImport.setStateProvince("Florida");
						// leadApi.setCounty("Gulf");
						leadImport.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
						leadImport.setBasedonAssignmentRules("yes");
						leadImport.setLeadSourceCategory("Friends");
						leadImport.setLeadSourceDetails("Friends");
						
						
						leadList.add(leadImport);
					}

					common.writeCsvFile(filePath, leadList);

					// Import the CSV file
					Import importlead = new Import();
					ImportTest importTest = new ImportTest(driver);

					// Navigate to Import Tab
					fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

					importlead.setImportType("Leads");
					importlead.setSpecifyFileFormat("CSV");
					importlead.setSalesDataFile(filePath);
					importlead.setLeadStatus("New Lead");
					importlead.setLeadSourceCategory("Friends");
					importlead.setLeadSourceDetails("Friends");

					importTest.importLeadsCSV(importlead);

					// Validation
					Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

					Lead x = new Lead();
					List<String> leadsFirstNames = map.get("FN");
					List<String> leadsLastNames = map.get("LN");

					for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
						System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
						x.setFirstName(leadsFirstNames.get(a));
						x.setLastName(leadsLastNames.get(b));

						String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
						String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
						System.out
								.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

						// Validation
						if (!ownerAssignedToLeadx.equalsIgnoreCase("FranConnect Administrator")) {
							System.out.println("Owner should be ::: FranConnect Administrator");
							fc.utobj().throwsException(
									"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33889 - Import - FAILED");
						}
					}
						
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			
		}
	}
	
// =============================================================================================================================================================================================================	
	
	
	@Test(priority =235,dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment"  })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_86-Api", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33888")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33888_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		try {
								 
			// Through API
			// ------------------------------------------------------------------------------------------------------------------------------------------------------------
			for (int j = 0; j <= 0; j++) {
				Lead leadApi = new Lead();
				
				leadApi.setFirstName("LeadSource_Api_33888");
				leadApi.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				leadApi.setDivision(addDivision3.getDivisionName());
				leadApi.setCountry("USA");
				leadApi.setStateProvince("Florida");
				// leadApi.setCounty("Gulf");
				leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
				leadApi.setLeadSourceDetails("None");
				
				common.addLeadThroughWebServices(null, leadApi);
				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(null, leadApi);
				// System.out.println(xmlApi);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println(
						"Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
				// Validation
				// Validate Owner Assignment
				// Validation
				if (!corpUsersForSalesTerritoriesZip.contains(ownerAssignedToLeadApi)) {
					System.out.println("Owner should be among ::: " + corpUsersForSalesTerritoriesZip);
					fc.utobj().throwsException("Owner Assignment - Assign Lead Owner by Lead Source - For Territory(ZIP) - TC-33888 - API - FAILED");
				}
			}
			
			fc.utobj().logoutAndQuitBrowser(null, testCaseId);
						
		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(null, e, testCaseId);

			/*System.out.println(e.getMessage());
			fc.utobj().throwsException1(e.getMessage());*/				
		}
	}
	
	@Test(priority =236,dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_86-WebForm", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33888")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33888_WebForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			try {
				
				 driver = fc.loginpage().login(driver);
				 
				// Through WebForm
					// ----------------------------------------------------------------------------------------------------------------------------------------------------------
					// Create WebForm
					ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
					ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

					fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
					common.set_ManageWebform(manageWebFormGenerator);
					manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
					manageWebFormGenerator.setLeadSourceDetails("None");
					manageWebFormGenerator.setDivision(addDivision3.getDivisionName());
					manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

					// Add Lead - WebForm
					for (int j = 0; j <= 0; j++) {
						Lead leadWebForm = new Lead();

						leadWebForm.setFirstName("LeadSource_WebForm_33888");
						leadWebForm.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
						leadWebForm.setEmail("frantest2017@gmail.com");
						// leadWebForm.setDivision(addDivision2.getDivisionName());
						leadWebForm.setCountry("USA");
						leadWebForm.setStateProvince("Florida");
						// leadApi.setCounty("Gulf");
						leadWebForm.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
						// leadWebForm.setBasedonAssignmentRules("yes");
						leadWebForm.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
						leadWebForm.setLeadSourceDetails("None");
						

						// Add Lead through WebForm
						manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm);

						// fc.utobj().sleep();
						String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
						String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
						System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>"
								+ ownerAssignedTowebformLead);
						// Validation
						if (!corpUsersForSalesTerritoriesZip.contains(ownerAssignedTowebformLead)) {
							System.out.println("Owner should be among ::: " + corpUsersForSalesTerritoriesZip);
							fc.utobj().throwsException("Owner Assignment - Assign Lead Owner by Lead Source - For Territory(ZIP) - TC-33888 - WebForm - FAILED");
						}
					}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			
		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			
		}
	}
	
	@Test(priority =237,dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_86-Import", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33888")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33888_Import() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			try {
				
				 driver = fc.loginpage().login(driver);
			
				// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
					// Create CSV File with Lead Details
					String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_33888.csv";
					System.out.println(filePath);
					List<Lead> leadList = new ArrayList<Lead>();

					for (int j = 0; j <= 0; j++) {
						Lead leadImport = new Lead();
						
						leadImport.setFirstName("LeadSource_Import_33888");
						leadImport.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
						leadImport.setEmail("frantest2017@gmail.com");
						leadImport.setDivision(addDivision3.getDivisionName());
						leadImport.setCountry("USA");
						leadImport.setStateProvince("Florida");
						// leadApi.setCounty("Gulf");
						leadImport.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
						leadImport.setBasedonAssignmentRules("yes");
						leadImport.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
						leadImport.setLeadSourceDetails("None");
						
						leadList.add(leadImport);
					}

					common.writeCsvFile(filePath, leadList);

					// Import the CSV file
					Import importlead = new Import();
					ImportTest importTest = new ImportTest(driver);

					// Navigate to Import Tab
					fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

					importlead.setImportType("Leads");
					importlead.setSpecifyFileFormat("CSV");
					importlead.setSalesDataFile(filePath);
					importlead.setLeadStatus("New Lead");
					importlead.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
					importlead.setLeadSourceDetails("None");

					importTest.importLeadsCSV(importlead);

					// Validation
					Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

					Lead x = new Lead();
					List<String> leadsFirstNames = map.get("FN");
					List<String> leadsLastNames = map.get("LN");

					for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
						System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
						x.setFirstName(leadsFirstNames.get(a));
						x.setLastName(leadsLastNames.get(b));

						String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
						String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
						System.out
								.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

						// Validation
						if (!corpUsersForSalesTerritoriesZip.contains(ownerAssignedToLeadx)) {
							System.out.println("Owner should be among ::: " + corpUsersForSalesTerritoriesZip);
							fc.utobj().throwsException("Owner Assignment - Assign Lead Owner by Lead Source - For Territory(ZIP) - TC-33888 - Import - FAILED");
						}
					}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			
		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			
		}
	}
	
// =============================================================================================================================================================================================================	
	
	
	@Test(priority =238,dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_87-Api", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33887")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33887_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

			try {
								 
			// Through API
			// ------------------------------------------------------------------------------------------------------------------------------------------------------------
			for (int j = 0; j <= 0; j++) {
				Lead leadApi = new Lead();
				
				leadApi.setFirstName("LeadSource_Api_33887");
				leadApi.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				// leadApi.setDivision(addDivision2.getDivisionName());
				leadApi.setCountry("USA");
				// leadApi.setStateProvince("Florida");
				leadApi.setCounty("Gulf");
				leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
				leadApi.setLeadSourceDetails("None");
				
				common.addLeadThroughWebServices(null, leadApi);
				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(null, leadApi);
				// System.out.println(xmlApi);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println(
						"Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
				// Validation
				// Validate Owner Assignment
				// Validation
				if (!corpUsersForSalesTerritoriesZip.contains(ownerAssignedToLeadApi)) {
					System.out.println("Owner should be among ::: " + corpUsersForSalesTerritoriesZip);

					fc.utobj().throwsException("Owner Assignment - Assign Lead Owner by Lead Source - For Territory(ZIP) - TC-33888 - API - FAILED");
				}
			}
			
			fc.utobj().logoutAndQuitBrowser(null, testCaseId);
			
		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(null, e, testCaseId);

			/*System.out.println(e.getMessage());
			fc.utobj().throwsException1(e.getMessage());	*/			
		}
	}
	
	@Test(priority =239,dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_87-WebForm", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33887")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33887_WebForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			try {
				
				 driver = fc.loginpage().login(driver);
				 
				// Through WebForm
					// ----------------------------------------------------------------------------------------------------------------------------------------------------------
					// Create WebForm
					ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
					ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

					fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
					common.set_ManageWebform(manageWebFormGenerator);
					manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
					manageWebFormGenerator.setLeadSourceDetails("None");
					// manageWebFormGenerator.setDivision(addDivision1.getDivisionName());
					manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

					// Add Lead - WebForm
					for (int j = 0; j <= 0; j++) {
						Lead leadWebForm = new Lead();

						leadWebForm.setFirstName("LeadSource_WebForm_33887");
						leadWebForm.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
						leadWebForm.setEmail("frantest2017@gmail.com");
						// leadApi.setDivision(addDivision2.getDivisionName());
						leadWebForm.setCountry("USA");
						// leadApi.setStateProvince("Florida");
						leadWebForm.setCounty("Gulf");
						leadWebForm.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
						// leadWebForm.setBasedonAssignmentRules("yes");
						leadWebForm.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
						leadWebForm.setLeadSourceDetails("None");

						// Add Lead through WebForm
						manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm);

						// fc.utobj().sleep();
						String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
						String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
						System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>"
								+ ownerAssignedTowebformLead);
						// Validation
						if (!corpUsersForSalesTerritoriesZip.contains(ownerAssignedTowebformLead)) {
							System.out.println("Owner should be among ::: " + corpUsersForSalesTerritoriesZip);
							fc.utobj().throwsException("Owner Assignment - Assign Lead Owner by Lead Source - For Territory(ZIP) - TC-33888 - WebForm - FAILED");
						}
					}
			
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			
		}
	}
	
	@Test(priority =240,dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_87-Import", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33887")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33887_Import() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			try {
				
				 driver = fc.loginpage().login(driver);
				 
				// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
					// Create CSV File with Lead Details
					String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_33887.csv";
					System.out.println(filePath);
					List<Lead> leadList = new ArrayList<Lead>();

					for (int j = 0; j <= 0; j++) {
						Lead leadImport = new Lead();
						
						leadImport.setFirstName("LeadSource_Import_33887");
						leadImport.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
						leadImport.setEmail("frantest2017@gmail.com");
						// leadApi.setDivision(addDivision2.getDivisionName());
						leadImport.setCountry("USA");
						// leadApi.setStateProvince("Florida");
						leadImport.setCounty("Gulf");
						leadImport.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
						leadImport.setBasedonAssignmentRules("yes");
						leadImport.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
						leadImport.setLeadSourceDetails("None");
						
						leadList.add(leadImport);
					}

					common.writeCsvFile(filePath, leadList);

					// Import the CSV file
					Import importlead = new Import();
					ImportTest importTest = new ImportTest(driver);

					// Navigate to Import Tab
					fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

					importlead.setImportType("Leads");
					importlead.setSpecifyFileFormat("CSV");
					importlead.setSalesDataFile(filePath);
					importlead.setLeadStatus("New Lead");
					importlead.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
					importlead.setLeadSourceDetails("None");

					importTest.importLeadsCSV(importlead);

					// Validation
					Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

					Lead x = new Lead();
					List<String> leadsFirstNames = map.get("FN");
					List<String> leadsLastNames = map.get("LN");

					for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
						System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
						x.setFirstName(leadsFirstNames.get(a));
						x.setLastName(leadsLastNames.get(b));

						String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
						String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
						System.out
								.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

						// Validation
						if (!corpUsersForSalesTerritoriesZip.contains(ownerAssignedToLeadx)) {
							System.out.println("Owner should be among ::: " + corpUsersForSalesTerritoriesZip);

							fc.utobj().throwsException("Owner Assignment - Assign Lead Owner by Lead Source - For Territory(ZIP) - TC-33888 - Import - FAILED");
						}
					}
			
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			
		}
	}
	
// =============================================================================================================================================================================================================	
	
	
	@Test(priority =241,dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_88-Api", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33886")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33886_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

			try {
								 
			// Through API
			// ------------------------------------------------------------------------------------------------------------------------------------------------------------
			for (int j = 0; j <= 0; j++) {
				Lead leadApi = new Lead();
				
				leadApi.setFirstName("LeadSource_Api_33886");
				leadApi.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				leadApi.setDivision(addDivision3.getDivisionName());
				leadApi.setCountry("USA");
				// leadApi.setStateProvince("Florida");
				leadApi.setCounty("Gulf");
				leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory("Friends");
				leadApi.setLeadSourceDetails("Friends");
				
				common.addLeadThroughWebServices(null, leadApi);
				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(null, leadApi);
				// System.out.println(xmlApi);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println(
						"Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
				// Validation
				// Validate Owner Assignment
				// Validation
				if (!ownerAssignedToLeadApi.equalsIgnoreCase("FranConnect Administrator")) {
					System.out.println("Owner should be ::: FranConnect Administrator");
					fc.utobj().throwsException(
							"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33886 - API - FAILED");
				}
			}
			
			fc.utobj().logoutAndQuitBrowser(null, testCaseId);
			
		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(null, e, testCaseId);

			/*System.out.println(e.getMessage());
			fc.utobj().throwsException1(e.getMessage());*/				
		}
	}
	
	@Test(priority =242,dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_88-WebForm", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33886")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33886_WebForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			try {
				
				 driver = fc.loginpage().login(driver);
				 
				// Through WebForm
					// ----------------------------------------------------------------------------------------------------------------------------------------------------------
					// Create WebForm
					ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
					ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

					fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
					common.set_ManageWebform(manageWebFormGenerator);
					manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
					manageWebFormGenerator.setLeadSourceDetails("None");
					manageWebFormGenerator.setDivision(addDivision3.getDivisionName());
					manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

					// Add Lead - WebForm
					for (int j = 0; j <= 0; j++) {
						Lead leadWebForm = new Lead();

						leadWebForm.setFirstName("LeadSource_WebForm_33886");
						leadWebForm.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
						leadWebForm.setEmail("frantest2017@gmail.com");
						// leadWebForm.setDivision(addDivision2.getDivisionName());
						leadWebForm.setCountry("USA");
						// leadApi.setStateProvince("Florida");
						leadWebForm.setCounty("Gulf");
						leadWebForm.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
						// leadWebForm.setBasedonAssignmentRules("yes");
						leadWebForm.setLeadSourceCategory("Friends");
						leadWebForm.setLeadSourceDetails("Friends");

						// Add Lead through WebForm
						manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm);

						// fc.utobj().sleep();
						String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
						String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
						System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>"
								+ ownerAssignedTowebformLead);
						// Validation
						if (!ownerAssignedTowebformLead.equalsIgnoreCase("FranConnect Administrator")) {
							System.out.println("Owner should be ::: FranConnect Administrator");

							fc.utobj().throwsException(
									"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33886 - WebForm - FAILED");
						}
					}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			
		}
	}
	
	@Test(priority =243,dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_88-Import", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33886")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33886_Import() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			try {
				
				 driver = fc.loginpage().login(driver);
				 
				// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
					// Create CSV File with Lead Details
					String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_33886.csv";
					System.out.println(filePath);
					List<Lead> leadList = new ArrayList<Lead>();

					for (int j = 0; j <= 0; j++) {
						Lead leadImport = new Lead();
						
						leadImport.setFirstName("LeadSource_Import_33886");
						leadImport.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
						leadImport.setEmail("frantest2017@gmail.com");
						leadImport.setDivision(addDivision3.getDivisionName());
						leadImport.setCountry("USA");
						// leadApi.setStateProvince("Florida");
						leadImport.setCounty("Gulf");
						leadImport.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
						leadImport.setBasedonAssignmentRules("yes");
						leadImport.setLeadSourceCategory("Friends");
						leadImport.setLeadSourceDetails("Friends");
						
						leadList.add(leadImport);
					}

					common.writeCsvFile(filePath, leadList);

					// Import the CSV file
					Import importlead = new Import();
					ImportTest importTest = new ImportTest(driver);

					// Navigate to Import Tab
					fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

					importlead.setImportType("Leads");
					importlead.setSpecifyFileFormat("CSV");
					importlead.setSalesDataFile(filePath);
					importlead.setLeadStatus("New Lead");
					importlead.setLeadSourceCategory("Friends");
					importlead.setLeadSourceDetails("Friends");
					
					importTest.importLeadsCSV(importlead);

					// Validation
					Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

					Lead x = new Lead();
					List<String> leadsFirstNames = map.get("FN");
					List<String> leadsLastNames = map.get("LN");

					for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
						System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
						x.setFirstName(leadsFirstNames.get(a));
						x.setLastName(leadsLastNames.get(b));

						String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
						String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
						System.out
								.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

						// Validation
						if (!ownerAssignedToLeadx.equalsIgnoreCase("FranConnect Administrator")) {
							System.out.println("Owner should be ::: FranConnect Administrator");

							fc.utobj().throwsException(
									"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33886 - Import - FAILED");
						}
					}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			
		}
	}
	
// =============================================================================================================================================================================================================	
	
	
	@Test(priority =244,dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_89-Api", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33885")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33885_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

			try {
								 
			// Through API
			// ------------------------------------------------------------------------------------------------------------------------------------------------------------
			for (int j = 0; j <= 0; j++) {
				Lead leadApi = new Lead();
				
				leadApi.setFirstName("LeadSource_Api_33885");
				leadApi.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				leadApi.setDivision(addDivision3.getDivisionName());
				leadApi.setCountry("USA");
				// leadApi.setStateProvince("Florida");
				leadApi.setCounty("Gulf");
				leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
				leadApi.setLeadSourceDetails("None");
				
				common.addLeadThroughWebServices(null, leadApi);
				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(null, leadApi);
				// System.out.println(xmlApi);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println(
						"Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
				// Validation
				// Validate Owner Assignment
				// Validation
				if (!corpUsersForSalesTerritoriesZip.contains(ownerAssignedToLeadApi)) {
					
					System.out.println("Owner should be among ::: " + corpUsersForSalesTerritoriesZip);
					fc.utobj().throwsException("Owner Assignment - Assign Lead Owner by Lead Source - TC-33885 - API - FAILED");
				}
			}
			
			fc.utobj().logoutAndQuitBrowser(null, testCaseId);
			
		} catch (Exception e) {
			
			
			 fc.utobj().quitBrowserOnCatch(null, e, testCaseId);

			/*System.out.println(e.getMessage());
			fc.utobj().throwsException1(e.getMessage());	*/			
		}
	}
	
	
	@Test(priority =245,dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_89-WebForm", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33885")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33885_WebForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			try {
				
				 driver = fc.loginpage().login(driver);
				 
				// Through WebForm
					// ----------------------------------------------------------------------------------------------------------------------------------------------------------
					// Create WebForm
					ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
					ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

					fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
					common.set_ManageWebform(manageWebFormGenerator);
					manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
					manageWebFormGenerator.setLeadSourceDetails("None");
					manageWebFormGenerator.setDivision(addDivision3.getDivisionName());
					manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

					// Add Lead - WebForm
					for (int j = 0; j <= 0; j++) {
						Lead leadWebForm = new Lead();

						leadWebForm.setFirstName("LeadSource_WebForm_33885");
						leadWebForm.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
						leadWebForm.setEmail("frantest2017@gmail.com");
						// leadWebForm.setDivision(addDivision2.getDivisionName());
						leadWebForm.setCountry("USA");
						// leadApi.setStateProvince("Florida");
						leadWebForm.setCounty("Gulf");
						leadWebForm.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
						// leadWebForm.setBasedonAssignmentRules("yes");
						leadWebForm.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
						leadWebForm.setLeadSourceDetails("None");

						// Add Lead through WebForm
						manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm);

						// fc.utobj().sleep();
						String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
						String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
						System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>"
								+ ownerAssignedTowebformLead);
						// Validation
						if (!corpUsersForSalesTerritoriesZip.contains(ownerAssignedTowebformLead)) {
							System.out.println("Owner should be among ::: " + corpUsersForSalesTerritoriesZip);

							fc.utobj().throwsException("Owner Assignment - Assign Lead Owner by Lead Source - TC-33885 - WebForm - FAILED");
						}
					}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			
		}
	}
	
	
	@Test(priority =246,dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_89-Import", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33885")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33885_Import() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			try {
				
				 driver = fc.loginpage().login(driver);
				 
				// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
					// Create CSV File with Lead Details
					String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_33885.csv";
					System.out.println(filePath);
					List<Lead> leadList = new ArrayList<Lead>();

					for (int j = 0; j <= 0; j++) {
						Lead leadImport = new Lead();
						
						leadImport.setFirstName("LeadSource_Import_33885");
						leadImport.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
						leadImport.setEmail("frantest2017@gmail.com");
						leadImport.setDivision(addDivision3.getDivisionName());
						leadImport.setCountry("USA");
						// leadApi.setStateProvince("Florida");
						leadImport.setCounty("Gulf");
						leadImport.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
						leadImport.setBasedonAssignmentRules("yes");
						leadImport.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
						leadImport.setLeadSourceDetails("None");
						
						leadList.add(leadImport);
					}

					common.writeCsvFile(filePath, leadList);

					// Import the CSV file
					Import importlead = new Import();
					ImportTest importTest = new ImportTest(driver);

					// Navigate to Import Tab
					fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

					importlead.setImportType("Leads");
					importlead.setSpecifyFileFormat("CSV");
					importlead.setSalesDataFile(filePath);
					importlead.setLeadStatus("New Lead");
					importlead.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
					importlead.setLeadSourceDetails("None");

					importTest.importLeadsCSV(importlead);

					// Validation
					Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

					Lead x = new Lead();
					List<String> leadsFirstNames = map.get("FN");
					List<String> leadsLastNames = map.get("LN");

					for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
						System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
						x.setFirstName(leadsFirstNames.get(a));
						x.setLastName(leadsLastNames.get(b));

						String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
						String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
						System.out
								.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

						// Validation
						if (!corpUsersForSalesTerritoriesZip.contains(ownerAssignedToLeadx)) {
							System.out.println("Owner should be among ::: " + corpUsersForSalesTerritoriesZip);

							fc.utobj().throwsException("Owner Assignment - Assign Lead Owner by Lead Source - TC-33885 - Import - FAILED");
						}
					}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			
		}
	}
	
// =============================================================================================================================================================================================================	
	
	
	@Test(priority =247,dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_90-Api", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33884")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33884_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

			try {
								 
			// Through API
			// ------------------------------------------------------------------------------------------------------------------------------------------------------------
			for (int j = 0; j <= 0; j++) {
				Lead leadApi = new Lead();
				
				leadApi.setFirstName("LeadSource_Api_33884");
				leadApi.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				// leadApi.setDivision(addDivision2.getDivisionName());
				leadApi.setCountry("USA");
				// leadApi.setStateProvince("Florida");
				leadApi.setCounty("Gulf");
				leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory("Friends");
				leadApi.setLeadSourceDetails("Friends");
				
				common.addLeadThroughWebServices(null, leadApi);
				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(null, leadApi);
				// System.out.println(xmlApi);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println(
						"Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
				// Validation
				// Validate Owner Assignment
				// Validation
				if (!ownerAssignedToLeadApi.equalsIgnoreCase("FranConnect Administrator")) {
					System.out.println("Owner should be ::: FranConnect Administrator");
					fc.utobj().throwsException(
							"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33884 - API - FAILED");
				}
			}
			
			fc.utobj().logoutAndQuitBrowser(null, testCaseId);
			
		} catch (Exception e) {
			
			 fc.utobj().quitBrowserOnCatch(null, e, testCaseId);

			/*System.out.println(e.getMessage());
			fc.utobj().throwsException1(e.getMessage());			*/	
		}
	}
	
	@Test(priority =248,dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_90-WebForm", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33884")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33884_WebForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			try {
				
				 driver = fc.loginpage().login(driver);
				 
				// Through WebForm
					// ----------------------------------------------------------------------------------------------------------------------------------------------------------
					// Create WebForm
					ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
					ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

					fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
					common.set_ManageWebform(manageWebFormGenerator);
					manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
					manageWebFormGenerator.setLeadSourceDetails("None");
					// manageWebFormGenerator.setDivision(addDivision1.getDivisionName());
					manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

					// Add Lead - WebForm
					for (int j = 0; j <= 0; j++) {
						Lead leadWebForm = new Lead();

						leadWebForm.setFirstName("LeadSource_Webform_33884");
						leadWebForm.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
						leadWebForm.setEmail("frantest2017@gmail.com");
						// leadApi.setDivision(addDivision2.getDivisionName());
						leadWebForm.setCountry("USA");
						// leadApi.setStateProvince("Florida");
						leadWebForm.setCounty("Gulf");
						leadWebForm.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
						// leadWebForm.setBasedonAssignmentRules("yes");
						leadWebForm.setLeadSourceCategory("Friends");
						leadWebForm.setLeadSourceDetails("Friends");

						// Add Lead through WebForm
						manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm);

						// fc.utobj().sleep();
						String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
						String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
						System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>"
								+ ownerAssignedTowebformLead);
						// Validation
						if (!ownerAssignedTowebformLead.equalsIgnoreCase("FranConnect Administrator")) {
							System.out.println("Owner should be ::: FranConnect Administrator");
							fc.utobj().throwsException(
									"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33884 - WebForm - FAILED");
						}
					}
				 
		
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			
		}
	}
	
	@Test(priority =249,dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_90-Import", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33884")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33884_Import() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			try {
				
				 driver = fc.loginpage().login(driver);
				 
				// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
					// Create CSV File with Lead Details
					String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_33884.csv";
					System.out.println(filePath);
					List<Lead> leadList = new ArrayList<Lead>();

					for (int j = 0; j <= 0; j++) {
						Lead leadImport = new Lead();
						
						leadImport.setFirstName("LeadSource_Import_33884");
						leadImport.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
						leadImport.setEmail("frantest2017@gmail.com");
						// leadApi.setDivision(addDivision2.getDivisionName());
						leadImport.setCountry("USA");
						// leadApi.setStateProvince("Florida");
						leadImport.setCounty("Gulf");
						leadImport.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
						leadImport.setBasedonAssignmentRules("yes");
						leadImport.setLeadSourceCategory("Friends");
						leadImport.setLeadSourceDetails("Friends");
						
						leadList.add(leadImport);
					}

					common.writeCsvFile(filePath, leadList);

					// Import the CSV file
					Import importlead = new Import();
					ImportTest importTest = new ImportTest(driver);

					// Navigate to Import Tab
					fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

					importlead.setImportType("Leads");
					importlead.setSpecifyFileFormat("CSV");
					importlead.setSalesDataFile(filePath);
					importlead.setLeadStatus("New Lead");
					importlead.setLeadSourceCategory("Friends");
					importlead.setLeadSourceDetails("Friends");

					importTest.importLeadsCSV(importlead);

					// Validation
					Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

					Lead x = new Lead();
					List<String> leadsFirstNames = map.get("FN");
					List<String> leadsLastNames = map.get("LN");

					for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
						System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
						x.setFirstName(leadsFirstNames.get(a));
						x.setLastName(leadsLastNames.get(b));

						String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
						String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
						System.out
								.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

						// Validation
						if (!ownerAssignedToLeadx.equalsIgnoreCase("FranConnect Administrator")) {
							System.out.println("Owner should be ::: FranConnect Administrator");
							fc.utobj().throwsException(
									"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33884 - Import - FAILED");
						}
					}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			
		}
	}
	
// =============================================================================================================================================================================================================	
	
	
	@Test(priority =250,dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_91-Api", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33883")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33883_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		try {
								 
			// Through API
			// ------------------------------------------------------------------------------------------------------------------------------------------------------------
			for (int j = 0; j <= 0; j++) {
				Lead leadApi = new Lead();
				
				leadApi.setFirstName("LeadSource_Api_33883");
				leadApi.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				// leadApi.setDivision(addDivision2.getDivisionName());
				leadApi.setCountry("USA");
				leadApi.setStateProvince("Florida");
				leadApi.setCounty("Gulf");
				// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory("Friends");
				leadApi.setLeadSourceDetails("Friends");
				
				common.addLeadThroughWebServices(null, leadApi);
				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(null, leadApi);
				// System.out.println(xmlApi);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println(
						"Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
				// Validation
				// Validate Owner Assignment
				// Validation
				if (!ownerAssignedToLeadApi.equalsIgnoreCase("FranConnect Administrator")) {
					System.out.println("Owner should be ::: FranConnect Administrator");
					fc.utobj().throwsException(
							"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33883 - API - FAILED");
				}
			}
			
			fc.utobj().logoutAndQuitBrowser(null, testCaseId);
			
		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(null, e, testCaseId);

			/*System.out.println(e.getMessage());
			fc.utobj().throwsException1(e.getMessage());	*/			
		}
	}
	

	@Test(priority =251,dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_91-WebForm", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33883")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33883_WebForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			try {
				
				 driver = fc.loginpage().login(driver);
		
				// Through WebForm
					// ----------------------------------------------------------------------------------------------------------------------------------------------------------
					// Create WebForm
					ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
					ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

					fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
					common.set_ManageWebform(manageWebFormGenerator);
					manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
					manageWebFormGenerator.setLeadSourceDetails("None");
					// manageWebFormGenerator.setDivision(addDivision1.getDivisionName());
					manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

					// Add Lead - WebForm
					for (int j = 0; j <= 0; j++) {
						Lead leadWebForm = new Lead();

						leadWebForm.setFirstName("LeadSource_Api_33883");
						leadWebForm.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
						leadWebForm.setEmail("frantest2017@gmail.com");
						// leadApi.setDivision(addDivision2.getDivisionName());
						leadWebForm.setCountry("USA");
						leadWebForm.setStateProvince("Florida");
						leadWebForm.setCounty("Gulf");
						// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
						// leadWebForm.setBasedonAssignmentRules("yes");
						leadWebForm.setLeadSourceCategory("Friends");
						leadWebForm.setLeadSourceDetails("Friends");

						// Add Lead through WebForm
						manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm);

						// fc.utobj().sleep();
						String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
						String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
						System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>"
								+ ownerAssignedTowebformLead);
						// Validation
						if (!ownerAssignedTowebformLead.equalsIgnoreCase("FranConnect Administrator")) {
							System.out.println("Owner should be ::: FranConnect Administrator");
							fc.utobj().throwsException(
									"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33883 - WebForm - FAILED");
						}
					}
				 
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			
		}
	}
	

	@Test(priority =252,dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_91-Import", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33883")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33883_Import() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			try {
				
				 driver = fc.loginpage().login(driver);
				 
				// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
					// Create CSV File with Lead Details
					String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_33883.csv";
					System.out.println(filePath);
					List<Lead> leadList = new ArrayList<Lead>();

					for (int j = 0; j <= 0; j++) {
						Lead leadImport = new Lead();
						
						leadImport.setFirstName("LeadSource_Api_33883");
						leadImport.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
						leadImport.setEmail("frantest2017@gmail.com");
						// leadApi.setDivision(addDivision2.getDivisionName());
						leadImport.setCountry("USA");
						leadImport.setStateProvince("Florida");
						leadImport.setCounty("Gulf");
						// leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
						leadImport.setBasedonAssignmentRules("yes");
						leadImport.setLeadSourceCategory("Friends");
						leadImport.setLeadSourceDetails("Friends");
						
						leadList.add(leadImport);
					}

					common.writeCsvFile(filePath, leadList);

					// Import the CSV file
					Import importlead = new Import();
					ImportTest importTest = new ImportTest(driver);

					// Navigate to Import Tab
					fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

					importlead.setImportType("Leads");
					importlead.setSpecifyFileFormat("CSV");
					importlead.setSalesDataFile(filePath);
					importlead.setLeadStatus("New Lead");
					importlead.setLeadSourceCategory("Friends");
					importlead.setLeadSourceDetails("Friends");

					importTest.importLeadsCSV(importlead);

					// Validation
					Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

					Lead x = new Lead();
					List<String> leadsFirstNames = map.get("FN");
					List<String> leadsLastNames = map.get("LN");

					for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
						System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
						x.setFirstName(leadsFirstNames.get(a));
						x.setLastName(leadsLastNames.get(b));

						String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
						String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
						System.out
								.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

						// Validation
						if (!ownerAssignedToLeadx.equalsIgnoreCase("FranConnect Administrator")) {
							System.out.println("Owner should be ::: FranConnect Administrator");
							fc.utobj().throwsException(
									"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33883 - Import - FAILED");
						}
					}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			
		}
	}
	
// =============================================================================================================================================================================================================	
	
	
	@Test(priority =253,dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_92-Api", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33882")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33882_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		try {
								 
			// Through API
			// ------------------------------------------------------------------------------------------------------------------------------------------------------------
			for (int j = 0; j <= 0; j++) {
				Lead leadApi = new Lead();
				
				leadApi.setFirstName("LeadSource_Api_33882");
				leadApi.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				// leadApi.setDivision(addDivision2.getDivisionName());
				leadApi.setCountry("USA");
				leadApi.setStateProvince("Florida");
				// leadApi.setCounty("Gulf");
				leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory("Friends");
				leadApi.setLeadSourceDetails("Friends");
				
				common.addLeadThroughWebServices(null, leadApi);
				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(null, leadApi);
				// System.out.println(xmlApi);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println(
						"Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
				// Validation
				// Validate Owner Assignment
				// Validation
				if (!ownerAssignedToLeadApi.equalsIgnoreCase("FranConnect Administrator")) {
					System.out.println("Owner should be ::: FranConnect Administrator");
					fc.utobj().throwsException(
							"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33882 - API - FAILED");
				}
			}
			
			fc.utobj().logoutAndQuitBrowser(null, testCaseId);
			
		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(null, e, testCaseId);

			 
			 
			/*System.out.println(e.getMessage());
			fc.utobj().throwsException1(e.getMessage());	*/			
		}
	}
	
	@Test(priority =254,dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_92-WebForm", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33882")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33882_WebForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			try {
				
				 driver = fc.loginpage().login(driver);
				 
				// Through WebForm
					// ----------------------------------------------------------------------------------------------------------------------------------------------------------
					// Create WebForm
					ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
					ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

					fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
					common.set_ManageWebform(manageWebFormGenerator);
					manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
					manageWebFormGenerator.setLeadSourceDetails("None");
					// manageWebFormGenerator.setDivision(addDivision1.getDivisionName());
					manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

					// Add Lead - WebForm
					for (int j = 0; j <= 0; j++) {
						Lead leadWebForm = new Lead();

						leadWebForm.setFirstName("LeadSource_Api_33882");
						leadWebForm.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
						leadWebForm.setEmail("frantest2017@gmail.com");
						// leadApi.setDivision(addDivision2.getDivisionName());
						leadWebForm.setCountry("USA");
						leadWebForm.setStateProvince("Florida");
						// leadApi.setCounty("Gulf");
						leadWebForm.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
						// leadWebForm.setBasedonAssignmentRules("yes");
						leadWebForm.setLeadSourceCategory("Friends");
						leadWebForm.setLeadSourceDetails("Friends");
						

						// Add Lead through WebForm
						manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm);

						// fc.utobj().sleep();
						String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
						String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
						System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>"
								+ ownerAssignedTowebformLead);
						// Validation
						if (!ownerAssignedTowebformLead.equalsIgnoreCase("FranConnect Administrator")) {
							System.out.println("Owner should be ::: FranConnect Administrator");
							fc.utobj().throwsException(
									"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33882 - WebForm - FAILED");
						}
					}
			
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			
		}
	}
	
	@Test(priority =255,dependsOnMethods = "ConfigurationForOwnerAssignmentByLeadSource", groups = { "LeadSource_OwnerAssignment",
	"sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_92-Import", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source TC-33882")
	private void OwnerAssignmentByLeadSource_PriorityST_TC_33882_Import() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			try {
				
				 driver = fc.loginpage().login(driver);
				 
				// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
					// Create CSV File with Lead Details
					String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_33882.csv";
					System.out.println(filePath);
					List<Lead> leadList = new ArrayList<Lead>();

					for (int j = 0; j <= 0; j++) {
						Lead leadImport = new Lead();
						
						leadImport.setFirstName("LeadSource_Api_33882");
						leadImport.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
						leadImport.setEmail("frantest2017@gmail.com");
						// leadApi.setDivision(addDivision2.getDivisionName());
						leadImport.setCountry("USA");
						leadImport.setStateProvince("Florida");
						// leadApi.setCounty("Gulf");
						leadImport.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
						leadImport.setBasedonAssignmentRules("yes");
						leadImport.setLeadSourceCategory("Friends");
						leadImport.setLeadSourceDetails("Friends");
						
						leadList.add(leadImport);
					}

					common.writeCsvFile(filePath, leadList);

					// Import the CSV file
					Import importlead = new Import();
					ImportTest importTest = new ImportTest(driver);

					// Navigate to Import Tab
					fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

					importlead.setImportType("Leads");
					importlead.setSpecifyFileFormat("CSV");
					importlead.setSalesDataFile(filePath);
					importlead.setLeadStatus("New Lead");
					importlead.setLeadSourceCategory("Friends");
					importlead.setLeadSourceDetails("Friends");

					importTest.importLeadsCSV(importlead);

					// Validation
					Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

					Lead x = new Lead();
					List<String> leadsFirstNames = map.get("FN");
					List<String> leadsLastNames = map.get("LN");

					for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
						System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
						x.setFirstName(leadsFirstNames.get(a));
						x.setLastName(leadsLastNames.get(b));

						String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
						String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
						System.out
								.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

						// Validation
						if (!ownerAssignedToLeadx.equalsIgnoreCase("FranConnect Administrator")) {
							System.out.println("Owner should be ::: FranConnect Administrator");
							fc.utobj().throwsException(
									"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - TC-33882 - Import - FAILED");
						}
					}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			
		}
	}
	
// =============================================================================================================================================================================================================	
	
	
	@Test(priority =256,dependsOnMethods="ConfigurationForOwnerAssignmentByLeadSource" , groups = { "LeadSource_OwnerAssignment", "sales_OwnerAssignment" /*, "sales_OwnerAssignment_Failed"*/ })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_93", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source , Round Robin Priority Sales Territories Zip")
	private void OwnerAssignmentByLeadSource_RR_PriorityST_Zip() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			try {
				
				 driver = fc.loginpage().login(driver);
				 
			
			// Check TerritoryZip - RoundRobin - Priority(Sales Territory)
			// Through API ----------------------------------------------------------------------------------------------------------------------------------------------------------------
			for (int j = 0; j <= 2; j++) {
				Lead leadApi = new Lead();
				leadApi.setFirstName("LeadSourceTerritory_Api"+j);
				leadApi.setLastName("RR_PriorityTerritory_ZIP" + fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				leadApi.setDivision(addDivision2.getDivisionName());
				leadApi.setCountry("USA");
				leadApi.setStateProvince("Florida");
				leadApi.setCounty("Gulf");
				leadApi.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
				leadApi.setLeadSourceDetails("None");
				common.addLeadThroughWebServices(driver, leadApi);
				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadApi);
				// System.out.println(xml8);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println(
						"Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
				// Validation
				if (!corpUsersForSalesTerritoriesZip.contains(ownerAssignedToLeadApi)) {
					System.out.println("Owner should be among ::: " + corpUsersForSalesTerritoriesZip);
					fc.utobj().throwsException("Owner Assignment - Assign Lead Owner by Lead Source - For Territory(ZIP) - API - FAILED");
				}
			}
			
			// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
			// Create CSV File with Lead Details
			String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_Source_RR_PriorityST_Zip.csv";
			System.out.println(filePath);
			List<Lead> leadList = new ArrayList<Lead>();

			for (int j = 0; j <= 2; j++) {
				Lead importLead = new Lead();
				importLead.setFirstName("LeadSourceTerritory_Import" + j);
				importLead.setLastName("RR_PriorityTerritory_ZIP" + fc.utobj().generateRandomNumber());
				importLead.setEmail("frantest2017@gmail.com");
				importLead.setDivision(addDivision2.getDivisionName());
				importLead.setCountry("USA");
				importLead.setStateProvince("Florida");
				importLead.setCounty("Gulf");
				importLead.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				importLead.setBasedonAssignmentRules("yes");
				importLead.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
				importLead.setLeadSourceDetails("None");

				leadList.add(importLead);
			}

			common.writeCsvFile(filePath, leadList);

			// Import the CSV file
			Import importlead = new Import();
			ImportTest importTest = new ImportTest(driver);

			// Navigate to Import Tab
			fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver); 

			importlead.setImportType("Leads");
			importlead.setSpecifyFileFormat("CSV");
			importlead.setSalesDataFile(filePath);
			// importlead.setSalesDataFile(fc.utobj().getFilePathFromTestData("AsgmntByDivPrioritySalesTerritories_GroupByStatesIntl.csv"));
			importlead.setLeadStatus("New Lead");
			importlead.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
			importlead.setLeadSourceDetails("None");

			importTest.importLeadsCSV(importlead);

			// Validation
			Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

			Lead x = new Lead();
			List<String> leadsFirstNames = map.get("FN");
			List<String> leadsLastNames = map.get("LN");

			for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
				System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
				x.setFirstName(leadsFirstNames.get(a));
				x.setLastName(leadsLastNames.get(b));

				String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
				String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
				System.out.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

				// Validate Owner Assignment
				if (!corpUsersForSalesTerritoriesZip.contains(ownerAssignedToLeadx)) {
					System.out.println("Owner should be among ::: " + corpUsersForSalesTerritoriesZip);
					fc.utobj().throwsException("Assign Lead Owner by Lead Source - For Territory(ZIP) - Import - FAILED");
				}
			}
			
			// Through WebForm ----------------------------------------------------------------------------------------------------------------------------------------------------------------
			// Create WebForm 
			ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
			ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();
			
			fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
			common.set_ManageWebform(manageWebFormGenerator);
			manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
			manageWebFormGenerator.setLeadSourceDetails("None");
			manageWebFormGenerator.setDivision(addDivision2.getDivisionName());
			manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);
			
			// Add Lead - WebForm
			Lead webformLead = new Lead();
			for (int j = 0; j <= 2; j++) {
				webformLead.setFirstName("LeadSourceTerritory_WebForm" + j);
				webformLead.setLastName("RR_PriorityTerritory_ZIP" + fc.utobj().generateRandomNumber());
				webformLead.setEmail("frantest2017@gmail.com");
				webformLead.setDivision(addDivision2.getDivisionName());
				webformLead.setCountry("USA");
				webformLead.setStateProvince("Oregon");
				webformLead.setCounty("Morrow");
				webformLead.setZipPostalCode(salesTerritoriesByZip.getZipPostalCodeText());
				webformLead.setDivision(addDivision2.getDivisionName());
				webformLead.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
				webformLead.setLeadSourceDetails("None");

				manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, webformLead); // ADD LEAD THROUGH WEBFORM
																								
				// fc.utobj().sleep();
				String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, webformLead);
				//System.out.println(xml2);
				String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
				System.out.println("Owner Assigned to Lead "+webformLead.getLeadFullName()+" >>>>>>>>" + ownerAssignedTowebformLead);							
				// Validate
				if (! corpUsersForSalesTerritoriesZip.contains(ownerAssignedTowebformLead)) {
					System.out.println("Owner should be among ::: " + corpUsersForSalesTerritoriesZip);
					fc.utobj().throwsException("Assign Lead Owner by Lead Source - Priority(Sales Territories - GroupBy(ZIP/Postal Code)) - WebForm - FAILED");
				}
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
							
		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			
		}
	}

	@Test(priority =257, dependsOnMethods="ConfigurationForOwnerAssignmentByLeadSource" , groups = { "LeadSource_OwnerAssignment", "sales_OwnerAssignment" /*, "sales_OwnerAssignment_Failed"*/ })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_94", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source , Round Robin  Priority Sales Territories State Domestic")
	private void OwnerAssignmentByLeadSource_RR_PriorityST_StateDomestic() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			try {
				
				 driver = fc.loginpage().login(driver);
				 

			// Territory(States)Domestic - RoundRobin - Priority(Sales Territory)
			// Through API -----------------------------------------------------------------------------------------------------------------------------------------------------
			for (int j = 0; j <= 2; j++) {
				Lead leadApi = new Lead();
				leadApi.setFirstName("LeadSourceTerritory_Api");
				leadApi.setLastName("RR_PriorityTerritory_StateDom" + fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				leadApi.setDivision(addDivision2.getDivisionName());
				leadApi.setCountry("USA");
				leadApi.setStateProvince("Arizona");
				// lead11.setCounty("Gulf");
				// lead11.setZipPostalCode(salesTerritories2.getZipPostalCodeText());
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
				leadApi.setLeadSourceDetails("None");
				common.addLeadThroughWebServices(driver, leadApi);
				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadApi);
				// System.out.println(xml10);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println("Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
				// Validation
				if (!corpUsersForSalesTerritoriesStatesDomestic.contains(ownerAssignedToLeadApi)) {
					System.out.println("Owner should be among ::: " + corpUsersForSalesTerritoriesStatesDomestic);
					fc.utobj().throwsException("Assign Lead Owner by Lead Source - For Territory(States)Domestic - API - FAILED");
				}
			}
			
			// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
			// Create CSV File with Lead Details
			String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_Source_RR_PriorityST_StateDomestic.csv";
			System.out.println(filePath);
			List<Lead> leadList = new ArrayList<Lead>();

			for (int j = 0; j <= 2; j++) {
				Lead importLead = new Lead();
				importLead.setFirstName("LeadSourceTerritory_Import" + j);
				importLead.setLastName("RR_PriorityTerritory_StateDom" + fc.utobj().generateRandomNumber());
				importLead.setEmail("frantest2017@gmail.com");
				importLead.setDivision(addDivision2.getDivisionName());
				importLead.setCountry("USA");
				importLead.setStateProvince("Arizona");
				// importLead.setCounty("Gulf");
				// importLead.setZipPostalCode(salesTerritories2.getZipPostalCodeText());
				importLead.setBasedonAssignmentRules("yes");
				importLead.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
				importLead.setLeadSourceDetails("None");

				leadList.add(importLead);
			}

			common.writeCsvFile(filePath, leadList);

			// Import the CSV file
			Import importlead = new Import();
			ImportTest importTest = new ImportTest(driver);

			// Navigate to Import Tab
			fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

			importlead.setImportType("Leads");
			importlead.setSpecifyFileFormat("CSV");
			importlead.setSalesDataFile(filePath);
			// importlead.setSalesDataFile(fc.utobj().getFilePathFromTestData("AsgmntByDivPrioritySalesTerritories_GroupByStatesIntl.csv"));
			importlead.setLeadStatus("New Lead");
			importlead.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
			importlead.setLeadSourceDetails("None");

			importTest.importLeadsCSV(importlead);

			// Validation
			Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

			Lead x = new Lead();
			List<String> leadsFirstNames = map.get("FN");
			List<String> leadsLastNames = map.get("LN");

			for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
				System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
				x.setFirstName(leadsFirstNames.get(a));
				x.setLastName(leadsLastNames.get(b));

				String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
				String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
				System.out
						.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

				// Validate Owner Assignment
				if (!corpUsersForSalesTerritoriesStatesDomestic.contains(ownerAssignedToLeadx)) {
					System.out.println("Owner should be among ::: " + corpUsersForSalesTerritoriesStatesDomestic);

					fc.utobj().throwsException("Assign Lead Owner by Lead Source - For Territory(States)Domestic - Import - FAILED");
				}
			}
						
			// Through WebForm ----------------------------------------------------------------------------------------------------------------------------------------------------------------
			// Create WebForm
			ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
			ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

			fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
			common.set_ManageWebform(manageWebFormGenerator);
			manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
			manageWebFormGenerator.setLeadSourceDetails("None");
			manageWebFormGenerator.setDivision(addDivision2.getDivisionName());
			manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

			// Add Lead - WebForm
			Lead webformLead = new Lead();
			for (int j = 0; j <= 2; j++) {
				webformLead.setFirstName("LeadSourceTerritory_WebForm" + j);
				webformLead.setLastName("RR_PriorityTerritory_StateDom" + fc.utobj().generateRandomNumber());
				webformLead.setEmail("frantest2017@gmail.com");
				webformLead.setDivision(addDivision2.getDivisionName());
				webformLead.setCountry("USA");
				webformLead.setStateProvince("Arizona");
				//webformLead.setCounty("Morrow");
				//webformLead.setZipPostalCode(salesTerritories2.getZipPostalCodeText());
				webformLead.setDivision(addDivision2.getDivisionName());
				webformLead.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
				webformLead.setLeadSourceDetails("None");

				// Add Lead through WebForm
				manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, webformLead); 

				// fc.utobj().sleep();
				String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, webformLead);
				// System.out.println(xml2);
				String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
				System.out.println("Owner Assigned to Lead " + webformLead.getLeadFullName() + " >>>>>>>>"
						+ ownerAssignedTowebformLead);
				// Validate
				if (!corpUsersForSalesTerritoriesStatesDomestic.contains(ownerAssignedTowebformLead)) {
					System.out.println("Owner should be among ::: " + corpUsersForSalesTerritoriesStatesDomestic);
					fc.utobj().throwsException("Assign Lead Owner By Division - For Territory(States)Domestic - WebForm - FAILED");
				}
			}	

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			
		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			
		}
	}

				
	@Test(priority =258,dependsOnMethods="ConfigurationForOwnerAssignmentByLeadSource" , groups = { "LeadSource_OwnerAssignment", "sales_OwnerAssignment" , "sales_OwnerAssignment_Failed" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_95", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source , LeadSource_OwnerAssignment Round Robin Priority SalesTerritory State International")
	private void OwnerAssignmentByLeadSource_RR_PriorityST_StateIntl() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			try {
				
				 driver = fc.loginpage().login(driver);
				 
			
			// Check Territory(States)International - RoundRobin - Priority(Sales Territory) 

			// Through API ----------------------------------------------------------------------------------------------------------------------------------------------------------
			for (int j = 0; j <= 2; j++) {
				Lead leadApi = new Lead();
				leadApi.setFirstName("LeadSourceTerritory_Api");
				leadApi.setLastName("RR_PriorityTerritory_StateIntl" + fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				leadApi.setDivision(addDivision2.getDivisionName());
				leadApi.setCountry("United Kingdom");
				leadApi.setStateProvince("Berkshire");
				// lead13.setCounty("Gulf");
				// lead13.setZipPostalCode(salesTerritories2.getZipPostalCodeText());
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
				leadApi.setLeadSourceDetails("None");
				common.addLeadThroughWebServices(driver, leadApi);
				// Fetch Owner assigned to Lead through Rest API
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadApi);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println("Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
				// Validation
				if (!corpUsersForSalesTerritoriesStatesInternational.contains(ownerAssignedToLeadApi)) {
					System.out.println("Owner should be among ::: " + corpUsersForSalesTerritoriesStatesInternational);

					fc.utobj().throwsException("Assign Lead Owner by Lead Source - For Territory(States)International - API - FAILED");
				}
			}
			
			// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
			// Create CSV File with Lead Details
			String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_Source_RR_PriorityST_StateIntl.csv";
			System.out.println(filePath);
			List<Lead> leadList = new ArrayList<Lead>();

			for (int j = 0; j <= 2; j++) {
				Lead importLead = new Lead();
				importLead.setFirstName("LeadSourceTerritory_Import" + j);
				importLead.setLastName("RR_PriorityTerritory_StateIntl" + fc.utobj().generateRandomNumber());
				importLead.setEmail("frantest2017@gmail.com");
				importLead.setDivision(addDivision2.getDivisionName());
				importLead.setCountry("United Kingdom");
				importLead.setStateProvince("Berkshire");
				// importLead.setCounty("Gulf");
				// importLead.setZipPostalCode(salesTerritories2.getZipPostalCodeText());
				importLead.setBasedonAssignmentRules("yes");
				importLead.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
				importLead.setLeadSourceDetails("None");

				leadList.add(importLead);
			}

			common.writeCsvFile(filePath, leadList);

			// Import the CSV file
			Import importlead = new Import();
			ImportTest importTest = new ImportTest(driver);

			// Navigate to Import Tab
			fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

			importlead.setImportType("Leads");
			importlead.setSpecifyFileFormat("CSV");
			importlead.setSalesDataFile(filePath);
			// importlead.setSalesDataFile(fc.utobj().getFilePathFromTestData("AsgmntByDivPrioritySalesTerritories_GroupByStatesIntl.csv"));
			importlead.setLeadStatus("New Lead");
			importlead.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
			importlead.setLeadSourceDetails("None");

			importTest.importLeadsCSV(importlead);

			// Validation
			Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

			Lead x = new Lead();
			List<String> leadsFirstNames = map.get("FN");
			List<String> leadsLastNames = map.get("LN");

			for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
				System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
				x.setFirstName(leadsFirstNames.get(a));
				x.setLastName(leadsLastNames.get(b));

				String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
				String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
				System.out
						.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

				// Validate Owner Assignment
				if (!corpUsersForSalesTerritoriesStatesInternational.contains(ownerAssignedToLeadx)) {
					System.out.println("Owner should be among ::: " + corpUsersForSalesTerritoriesStatesInternational);

					fc.utobj().throwsException("Assign Lead Owner by Lead Source -  For Territory(States)International - Import - FAILED");
				}
			}
			
			// Through WebForm ----------------------------------------------------------------------------------------------------------------------------------------------------------------
			// Create WebForm
			ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
			ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

			fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
			common.set_ManageWebform(manageWebFormGenerator);
			manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
			manageWebFormGenerator.setLeadSourceDetails("None");
			manageWebFormGenerator.setDivision(addDivision2.getDivisionName());
			manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

			// Add Lead - WebForm
			Lead webformLead = new Lead();
			for (int j = 0; j <= 2; j++) {
				webformLead.setFirstName("LeadSourceTerritory_WebForm" + j);
				webformLead.setLastName("RR_PriorityTerritory_StateIntl" + fc.utobj().generateRandomNumber());
				webformLead.setEmail("frantest2017@gmail.com");
				webformLead.setDivision(addDivision2.getDivisionName());
				webformLead.setCountry("United Kingdom");
				webformLead.setStateProvince("Berkshire");
				// webformLead.setCounty("Morrow");
				// webformLead.setZipPostalCode(salesTerritories2.getZipPostalCodeText());
				webformLead.setDivision(addDivision2.getDivisionName());
				webformLead.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
				webformLead.setLeadSourceDetails("None");

				// Add Lead through WebForm
				manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, webformLead);

				// fc.utobj().sleep();
				String xmlWebForm = common.getLeadDetailsThroughWebServices_MajorFields(driver, webformLead);
				// System.out.println(xml2);
				String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm, "leadOwnerID");
				System.out.println("Owner Assigned to Lead " + webformLead.getLeadFullName() + " >>>>>>>>" + ownerAssignedTowebformLead);
				// Validate
				if (!corpUsersForSalesTerritoriesStatesInternational.contains(ownerAssignedTowebformLead)) {
					System.out.println("Owner should be among ::: " + corpUsersForSalesTerritoriesStatesInternational);

					fc.utobj().throwsException("Assign Lead Owner by Lead Source - For Territory(States)International - WebForm - FAILED");
				}
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			
		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			
		}

	}

	@Test(priority =259,dependsOnMethods="ConfigurationForOwnerAssignmentByLeadSource" , groups = { "LeadSource_OwnerAssignment", "sales_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_96", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source , RoundRobin , Having priority Division")
	private void OwnerAssignmentByLeadSource_RR_PriorityDiv() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			try {
				
				 driver = fc.loginpage().login(driver);
				 
			// Set Priority(Division)
			AssignLeadOwnersTest assignLeadOwnersTest = new AssignLeadOwnersTest(driver);
			AssignLeadOwnerbyLeadSourceTest assignLeadOwnerbyLeadSourceTest = new AssignLeadOwnerbyLeadSourceTest(driver);
			fc.commonMethods().getModules().openAdminPage(driver).openAssignLeadOwnersPage(driver);
			assignLeadOwnersTest.set_assignLeadOwnerbyLeadSource();
			assignLeadOwnerbyLeadSourceTest.setPriority_Division();

			// TerritoryCounty - RoundRobin - Priority(Division)
			// Through API
			for(int j=0; j<=2; j++)
			{
			Lead leadApi = new Lead();
			leadApi.setFirstName("LeadSource_Api" +j);
			leadApi.setLastName("RR_PriorityDiv" + fc.utobj().generateRandomNumber());
			leadApi.setEmail("frantest2017@gmail.com");
			leadApi.setDivision(addDivision2.getDivisionName());
			leadApi.setCountry("USA");
			leadApi.setStateProvince("Florida");
			leadApi.setCounty("Gulf");
			leadApi.setBasedonAssignmentRules("yes");
			leadApi.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
			leadApi.setLeadSourceDetails("None");
			common.addLeadThroughWebServices(driver, leadApi);
			// Fetch Owner assigned to Lead through Rest API
			String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadApi);
			// System.out.println(xmlApi);
			String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
			System.out.println("Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
			// Validation
			if (!corpUsersForDivision.contains(ownerAssignedToLeadApi)) {
				System.out.println("Owner should be among ::: " + corpUsersForDivision);

				fc.utobj().throwsException("Owner Assignment - Assign Lead Owner by Lead Source - For Division - API - FAILED");
			}
			}
			
			// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
			// Create CSV File with Lead Details
			String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_Source_RR_PriorityDiv.csv";
			System.out.println(filePath);
			List<Lead> leadList = new ArrayList<Lead>();

			for (int j = 0; j <= 2; j++) {
				Lead importLead = new Lead();
				importLead.setFirstName("LeadSource_Import" + j);
				importLead.setLastName("RR_PriorityDiv" + fc.utobj().generateRandomNumber());
				importLead.setEmail("frantest2017@gmail.com");
				importLead.setDivision(addDivision2.getDivisionName());
				importLead.setCountry("USA");
				importLead.setStateProvince("Florida");
				importLead.setCounty("Gulf");
				// importLead.setZipPostalCode(salesTerritories2.getZipPostalCodeText());
				importLead.setBasedonAssignmentRules("yes");
				importLead.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
				importLead.setLeadSourceDetails("None");

				leadList.add(importLead);
			}

			common.writeCsvFile(filePath, leadList);

			// Import the CSV file
			Import importlead = new Import();
			ImportTest importTest = new ImportTest(driver);

			// Navigate to Import Tab
			fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

			importlead.setImportType("Leads");
			importlead.setSpecifyFileFormat("CSV");
			importlead.setSalesDataFile(filePath);
			// importlead.setSalesDataFile(fc.utobj().getFilePathFromTestData("AsgmntByDivPrioritySalesTerritories_GroupByStatesIntl.csv"));
			importlead.setLeadStatus("New Lead");
			importlead.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
			importlead.setLeadSourceDetails("None");

			importTest.importLeadsCSV(importlead);

			// Validation
			Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

			Lead x = new Lead();
			List<String> leadsFirstNames = map.get("FN");
			List<String> leadsLastNames = map.get("LN");

			for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
				System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
				x.setFirstName(leadsFirstNames.get(a));
				x.setLastName(leadsLastNames.get(b));

				String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
				String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
				System.out.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

				// Validate Owner Assignment
				if (!corpUsersForDivision.contains(ownerAssignedToLeadx)) {
					System.out.println("Owner should be among ::: " + corpUsersForDivision);

					fc.utobj().throwsException("Assign Lead Owner by Lead Source -  For Division - Import - FAILED");
				}
			}

			// Through WebForm ---------------------------------------------------------------------------------------------------------------------------------------------------------
			// Create WebForm
			ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
			ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

			fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
			common.set_ManageWebform(manageWebFormGenerator);
			manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
			manageWebFormGenerator.setLeadSourceDetails("None");
			manageWebFormGenerator.setDivision(addDivision2.getDivisionName());
			manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

			// Add leads from WebForm and verify Division RoundRobin
			Lead leadWebForm = new Lead();
			for (int j = 0; j <= 2; j++) {
				leadWebForm.setFirstName("LeadSource_WebForm");
				leadWebForm.setLastName("RR_PriorityDiv" + fc.utobj().generateRandomNumber());
				leadWebForm.setEmail("frantest2017@gmail.com");
				// leadWebForm.setDivision(addDivision1.getDivisionName());
				leadWebForm.setCountry("USA");
				leadWebForm.setStateProvince("Florida");
				leadWebForm.setCounty("Gulf");
				// leadWebForm.setBasedonAssignmentRules("yes");
				leadWebForm.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
				leadWebForm.setLeadSourceDetails("None");
				
				// ADD LEAD THROUGH WEBFORM
				manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm); 

				// fc.utobj().sleep();
				String xmlWebForm = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
				// System.out.println(xmlWebForm);
				String ownerAssignedToLeadWebForm = common.getValFromXML(xmlWebForm, "leadOwnerID");
				System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadWebForm);
				// Validate
				if (!corpUsersForDivision.contains(ownerAssignedToLeadWebForm)) {
					System.out.println("Owner should be among ::: " + corpUsersForDivision);

					fc.utobj().throwsException("Owner Assignment - Assign Lead Owner by Lead Source - For Division - WebForm - FAILED");
				}
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			
		}
	}
		
	@Test(priority =260,dependsOnMethods="ConfigurationForOwnerAssignmentByLeadSource" , groups = { "LeadSource_OwnerAssignment", "sales_OwnerAssignment"  , "sales_OwnerAssignment_failed"})
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_97", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source , DefaultOwner")
	private void OwnerAssignmentByLeadSource_DefaultOwner() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			try {
				
				 driver = fc.loginpage().login(driver);
				 
			// Default Owner Assignment
			// Through API
			Lead leadApi = new Lead();
			leadApi.setFirstName("DefaultOwner_Api");
			leadApi.setLastName("LeadSource" + fc.utobj().generateRandomNumber());
			leadApi.setEmail("frantest2017@gmail.com");
			leadApi.setBasedonAssignmentRules("yes");
			leadApi.setLeadSourceCategory("Friends");
			leadApi.setLeadSourceDetails("Friends");
			common.addLeadThroughWebServices(driver, leadApi);
			// Fetch Owner assigned to Lead through Rest API
			String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadApi);
			// System.out.println(xml14);
			String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
			System.out.println("Owner Assigned to Lead >>>>>>>>" + leadApi.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);
			// Validation
			if (!ownerAssignedToLeadApi.equalsIgnoreCase("FranConnect Administrator")) {
				System.out.println("Owner should be ::: FranConnect Administrator");
				fc.utobj().throwsException("Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - API - FAILED");
			}
			
			// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
			// Create CSV File with Lead Details
			String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_Division_DefaultOwner.csv";
			System.out.println(filePath);
			List<Lead> leadList = new ArrayList<Lead>();

			for (int j = 0; j <= 2; j++) {
				Lead importLead = new Lead();
				importLead.setFirstName("DefaultOwner_Import" + j);
				importLead.setLastName("LeadSource" + fc.utobj().generateRandomNumber());
				importLead.setEmail("frantest2017@gmail.com");
				//importLead.setDivision(addDivision1.getDivisionName());
				//importLead.setCountry("United Kingdom");
				//importLead.setStateProvince("Berkshire");
				// importLead.setCounty("Gulf");
				// importLead.setZipPostalCode(salesTerritories2.getZipPostalCodeText());
				importLead.setBasedonAssignmentRules("yes");
				importLead.setLeadSourceCategory("Friends");
				importLead.setLeadSourceDetails("Friends");

				leadList.add(importLead);
			}

			common.writeCsvFile(filePath, leadList);

			// Import the CSV file
			Import importlead = new Import();
			ImportTest importTest = new ImportTest(driver);

			// Navigate to Import Tab
			fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

			importlead.setImportType("Leads");
			importlead.setSpecifyFileFormat("CSV");
			importlead.setSalesDataFile(filePath);
			// importlead.setSalesDataFile(fc.utobj().getFilePathFromTestData("AsgmntByDivPrioritySalesTerritories_GroupByStatesIntl.csv"));
			importlead.setLeadStatus("New Lead");
			importlead.setLeadSourceCategory("Friends");
			importlead.setLeadSourceDetails("Friends");

			importTest.importLeadsCSV(importlead);

			// Validation
			Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

			Lead x = new Lead();
			List<String> leadsFirstNames = map.get("FN");
			List<String> leadsLastNames = map.get("LN");

			for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
				System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
				x.setFirstName(leadsFirstNames.get(a));
				x.setLastName(leadsLastNames.get(b));

				String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
				String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
				System.out.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

				// Validate Owner Assignment
				if (!ownerAssignedToLeadx.equalsIgnoreCase("FranConnect Administrator")) {
					System.out.println("Owner should be ::: FranConnect Administrator");
					fc.utobj().throwsException("Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - Import - FAILED");
				}
				
				// 
			}

			// Through WebForm
			// Create WebForm
			ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
			ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

			fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
			common.set_ManageWebform(manageWebFormGenerator);
			manageWebFormGenerator.setDivision(addDivision2.getDivisionName());
			manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

			// Add lead from WebForm
			Lead leadWebForm = new Lead();
			leadWebForm.setFirstName("DefaultOwner");
			leadWebForm.setLastName("LeadWebForm" + fc.utobj().generateRandomNumber());
			leadWebForm.setEmail("frantest2017@gmail.com");

			// ADD LEAD THROUGH WEBFORM
			manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm); 																					
			fc.utobj().sleep();
			String xmlWebForm = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
			// System.out.println(xmlWebForm);
			String ownerAssignedToLeadWebForm = common.getValFromXML(xmlWebForm, "leadOwnerID");
			System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadWebForm);
			// Validate
			if (!ownerAssignedToLeadWebForm.equalsIgnoreCase("FranConnect Administrator")) {
				System.out.println("Owner should be ::: FranConnect Administrator");
				fc.utobj().throwsException("Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - WebForm - FAILED");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			
			} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			
		}
	}
	
	// ******************************************************************************************************************************************************************************************************
	// ######################################################################################################################################################################################################
	
	// @BeforeClass(groups = {"sales_OwnerAssignment"})
		@Test(priority = 261 , dependsOnMethods="ConfigurationForOwnerAssignmentByLeadSource" , groups = { "RR_OwnerAssignment", "sales_OwnerAssignment"  , "sales_OwnerAssignment_failed" })
		@TestCase(createdOn = "2018-05-02", updatedOn = "2018-05-28", testCaseId = "Sales_OwnerAssignment_98", testCaseDescription = "Verify Round Robin Owner Assignment Configuration")
		void Configure_LeadOwnerByRoundRobin() throws Exception {
			String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
			}.getClass().getEnclosingMethod().getName());

			WebDriver driver = fc.commonMethods().browsers().openBrowser();
			
			AssignLeadOwnersTest assignLeadOwnersTest = new AssignLeadOwnersTest(driver);
			SetupSalesLeadOwnersTest setupSalesLeadOwnersTest = new SetupSalesLeadOwnersTest(driver);
			AssignLeadOwnerbyRoundRobinTest assignLeadOwnerbyRoundRobinTest = new AssignLeadOwnerbyRoundRobinTest(driver);

			try {
				driver = fc.loginpage().login(driver);

				for (int i = 0; i <= 2; i++) {
					fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser3);
					corpUser3.setFirstName("CorpUser" + i);
					corpUser3.setLastName("RoundRobin" + fc.utobj().generateRandomNumber());
					corpUser3.setUserName("user" + fc.utobj().generateRandomChar());
					fc.commonMethods().addCorporateUser(driver, corpUser3);
					//System.out.println(corpUser.getFirstName() + " " + corpUser.getLastName());
					corpUsersList.add(corpUser3.getFirstName() + " " + corpUser3.getLastName());
				}

				fc.commonMethods().getModules().openAdminPage(driver).openAssignLeadOwnersPage(driver);
				assignLeadOwnersTest.set_defaultOwner("FranConnect Administrator");
				assignLeadOwnersTest.setAssignLeadOwnerbyRoundRobin();
				assignLeadOwnerbyRoundRobinTest.moveAllUserTo_AvailableLeadOwners();

				System.out.println(corpUsersList);
				for (String temp : corpUsersList) {
					assignLeadOwnerbyRoundRobinTest.moveUserTo_ConfiguredLeadOwners(temp);
				}
				assignLeadOwnerbyRoundRobinTest.updateButton();

				fc.commonMethods().getModules().openAdminPage(driver).openSetupFranchiseSalesLeadOwnersPage(driver);
				setupSalesLeadOwnersTest.setAllusersAsSalesLeadOwners_AndSubmit();
				
				fc.commonMethods().getModules().openAdminPage(driver).openConfigureSalesEmailParsingServer(driver);
				ConfigureSalesEmailParsingServerTest configureSalesEmailParsingServerTest = new ConfigureSalesEmailParsingServerTest(driver);
				configureSalesEmailParsingServerTest.configureSalesEmailParsingServer();


				fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
				/*fc.utobj().throwsException(e.toString());
				System.out.println(testCaseId);*/
			}
		}
		
		 @Test(priority = 262 , dependsOnMethods="Configure_LeadOwnerByRoundRobin" , groups = { "RR_OwnerAssignment" , "sales_OwnerAssignment"})
		@TestCase(createdOn = "2018-04-27", updatedOn = "2018-05-27", testCaseId = "Sales_OwnerAssignment_99", testCaseDescription = "Verify Round Robin Owner Assignment through Lead addition from FrontEnd")
		void RoundRobin_FrontEnd() throws Exception {
			String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
			}.getClass().getEnclosingMethod().getName());

			WebDriver driver = fc.commonMethods().browsers().openBrowser();

			SetupSalesLeadOwnersTest setupSalesLeadOwnersTest = new SetupSalesLeadOwnersTest(driver);
			Lead leadFrontEnd = new Lead();
			PrimaryInfoTest primaryInfoTest = new PrimaryInfoTest(driver);
			LeadManagementTest leadManagementTest = new LeadManagementTest(driver);
			
			try { 
				
				driver = fc.loginpage().login(driver);
				
				fc.commonMethods().getModules().openAdminPage(driver).openSetupFranchiseSalesLeadOwnersPage(driver);
				setupSalesLeadOwnersTest.setAllusersAsSalesLeadOwners_AndSubmit();

				for (int j = 0, k = 0; j <= 5; j++, k++) {
					leadFrontEnd.setFirstName("Lead_FrontEnd" +j);
					leadFrontEnd.setLastName("RoundRobin" + fc.utobj().generateRandomNumber());
					leadFrontEnd.setEmail("frantest2017@gmail.com");
					leadFrontEnd.setBasedonAssignmentRules("yes");
					leadFrontEnd.setLeadSourceCategory("Friends");
					leadFrontEnd.setLeadSourceDetails("Friends");
					
					fc.commonMethods().getModules().sales().sales_common().fsModule(driver).leadManagement(driver);
					leadManagementTest.clickAddLead();
					primaryInfoTest.fillLeadInfo_ClickSave(leadFrontEnd);
					
					String xmlFrontEnd = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadFrontEnd);
					String ownerAssignedToLeadFrontEnd = common.getValFromXML(xmlFrontEnd, "leadOwnerID");
					System.out.println("Owner Assigned to Lead : "+leadFrontEnd.getLeadFullName()+" >>>>>>>>" + ownerAssignedToLeadFrontEnd);
					if (k > 2)
						k = 0;
					if (!corpUsersList.get(k).equalsIgnoreCase(ownerAssignedToLeadFrontEnd)) {
						System.out.println("Owner should be ::: " + corpUsersList.get(k));
						fc.utobj().throwsException("Lead Owner Mismatch - FrontEnd - Issue in RoundRobin Lead Owner Assignment");
					}
				}
				
				fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
				/*fc.utobj().throwsException(e.toString());
				System.out.println(testCaseId);*/
			}
		}

		
		@Test(priority = 263 , dependsOnMethods="Configure_LeadOwnerByRoundRobin" , groups = { "RR_OwnerAssignment" , "sales_OwnerAssignment" /*, "sales_OwnerAssignment_Failed"*/ })
		@TestCase(createdOn = "2018-04-27", updatedOn = "2018-05-27", testCaseId = "Sales_OwnerAssignment_100", testCaseDescription = "Verify Round Robin Owner Assignment through Lead addition from WebForm")
		void RoundRobin_WebForm() throws Exception {
			String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
			}.getClass().getEnclosingMethod().getName());

			WebDriver driver = fc.commonMethods().browsers().openBrowser();

			ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();
			ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);		

			try {
				driver = fc.loginpage().login(driver);

				fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
				common.set_ManageWebform(manageWebFormGenerator);
				manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

				fc.commonMethods().getModules().openAdminPage(driver).openFranchiseSalesManageWebFormGenerator(driver);

				Lead leadWebForm = new Lead();
				for (int j = 0, k = 0; j <= 5; j++, k++) {
					leadWebForm.setFirstName("Lead_WebForm" +j);
					leadWebForm.setLastName("RoundRobin" + fc.utobj().generateRandomNumber());
					leadWebForm.setEmail("frantest2017@gmail.com");
					leadWebForm.setLeadSourceCategory("Friends");
					leadWebForm.setLeadSourceDetails("Friends");
					// Add Lead through WebForm
					manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm); 

					String xml = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
					String ownerAssignedToLeadWebForm = common.getValFromXML(xml, "leadOwnerID");
					System.out.println("Owner Assigned to Lead : "+leadWebForm.getLeadFullName()+" >>>>>>>>" + ownerAssignedToLeadWebForm);
					if (k > 2)
						k = 0;
					if (!corpUsersList.get(k).equalsIgnoreCase(ownerAssignedToLeadWebForm)) {
						System.out.println("Owner should be ::: " + corpUsersList.get(k));
						fc.utobj().throwsException("Lead Owner Mismatch - Issue in RoundRobin Lead Owner Assignment");
					}
			}
				
				fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
				/*fc.utobj().throwsException(e.toString());
				System.out.println(testCaseId);*/
			}
				
		}
		
		

			 @Test(priority = 264 , dependsOnMethods="Configure_LeadOwnerByRoundRobin" , groups = { "RR_OwnerAssignment" ,  "sales_OwnerAssignment"})
			@TestCase(createdOn = "2018-04-27", updatedOn = "2018-05-27", testCaseId = "Sales_OwnerAssignment_101", testCaseDescription = "Verify Round Robin Owner Assignment through Lead addition from Api")
			void RoundRobin_Api() throws Exception {
				String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
				}.getClass().getEnclosingMethod().getName());
				
				WebDriver driver = fc.commonMethods().browsers().openBrowser();


				try {
					
					driver = fc.loginpage().login(driver);
					
					// Through API -------------------------------------------------------------------------------------------------------------------------------------------------------------------
					for(int j=0,k=0; j<=5; j++,k++)
					{
					Lead leadApi = new Lead();
					leadApi.setFirstName("Lead_Api"+j);
					leadApi.setLastName("RoundRobin" +fc.utobj().generateRandomNumber());
					leadApi.setEmail("frantest2017@gmail.com");
					leadApi.setBasedonAssignmentRules("yes");
					leadApi.setLeadSourceCategory("Friends");
					leadApi.setLeadSourceDetails("Friends");
					common.addLeadThroughWebServices(driver, leadApi);
					
					// Fetch Owner assigned to Lead through Rest API 
					String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadApi);
					String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
					System.out.println("Owner Assigned to Lead "+leadApi.getLeadFullName()+" >>>>>>>>"+ ownerAssignedToLeadApi);
					
					// Validation
					if (k > 2)
						k = 0;
					if(! corpUsersList.get(k).equalsIgnoreCase(ownerAssignedToLeadApi)) {
						System.out.println("Owner should be ::: " + corpUsersList.get(k));
						fc.utobj().throwsException("Owner Assignment - RoundRobin - API - FAILED");
					}
					}
					
					fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
					
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			//	fc.utobj().throwsException(e.toString());
			//	System.out.println(testCaseId);
			}
			
		}
			
			@Test(priority = 265 , dependsOnMethods="Configure_LeadOwnerByRoundRobin" , groups = { "RR_OwnerAssignment" ,  "sales_OwnerAssignment"})
			@TestCase(createdOn = "2018-04-27", updatedOn = "2018-05-27", testCaseId = "Sales_OwnerAssignment_102", testCaseDescription = "Verify Round Robin Owner Assignment through Lead addition from Import")
			void RoundRobin_Import() throws Exception {
				String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
				}.getClass().getEnclosingMethod().getName());
				
				WebDriver driver = fc.commonMethods().browsers().openBrowser();

				try {
					
					driver = fc.loginpage().login(driver);
					
					// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
					// Create CSV File with Lead Details
					String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_RoundRobin.csv";
					System.out.println(filePath);
					List<Lead> leadList = new ArrayList<Lead>();

					for(int j=0 ;j<=5; j++) {
						Lead leadImport = new Lead();
						
						leadImport.setFirstName("Lead_Import"+j);
						leadImport.setLastName("RoundRobin" +fc.utobj().generateRandomNumber());
						leadImport.setEmail("frantest2017@gmail.com");
						leadImport.setBasedonAssignmentRules("yes");
						leadImport.setLeadSourceCategory("Friends");
						leadImport.setLeadSourceDetails("Friends");

						leadList.add(leadImport);
					}

					common.writeCsvFile(filePath, leadList);

					// Import the CSV file
					Import importlead = new Import();
					ImportTest importTest = new ImportTest(driver);

					// Navigate to Import Tab
					fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

					importlead.setImportType("Leads");
					importlead.setSpecifyFileFormat("CSV");
					importlead.setSalesDataFile(filePath);
					importlead.setLeadStatus("New Lead");
					importlead.setLeadSourceCategory("Friends");
					importlead.setLeadSourceDetails("Friends");

					importTest.importLeadsCSV(importlead);

					// Validation
					Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

					Lead x = new Lead();
					List<String> leadsFirstNames = map.get("FN");
					List<String> leadsLastNames = map.get("LN");

					for (int a = 1, b = 1, k = 0; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++, k++) {
						System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
						x.setFirstName(leadsFirstNames.get(a));
						x.setLastName(leadsLastNames.get(b));

						String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
						String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
						System.out.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

						// Validation
						if (k > 2) k = 0;
						if(! corpUsersList.get(k).equalsIgnoreCase(ownerAssignedToLeadx)) {
							System.out.println("Owner should be ::: " + corpUsersList.get(k));
							fc.utobj().throwsException("Owner Assignment - RoundRobin - Import - FAILED");
						}
						
					}
					
					fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
				// fc.utobj().throwsException(e.toString());
			//	System.out.println(testCaseId);
			}
			
		}
			
			/*@Test(dependsOnMethods="Configure_LeadOwnerByRoundRobin" , groups = { "RR_OwnerAssignment" ,  "mytestgroupakshat" , "sales_OwnerAssignment"})
			@TestCase(createdOn = "2018-04-27", updatedOn = "2018-05-27", testCaseId = "RoundRobin_Assignment_Parsing", testCaseDescription = "Verify Round Robin Owner Assignment through Lead addition from Parsing")
			void RoundRobin_Parsing() throws Exception {
				String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
				}.getClass().getEnclosingMethod().getName());
							
				try {
					
					for(int j=0,k=0 ;j<=5; j++,k++) {
					
						Lead leadParsing = new Lead();
						
						leadParsing.setFirstName("Lead_Parsing"+j);
						leadParsing.setLastName("RoundRobin" +fc.utobj().generateRandomNumber());
						leadParsing.setEmail("frantest2017@gmail.com");
						leadParsing.setBasedonAssignmentRules("yes");
						leadParsing.setLeadSourceCategory("Friends");
						leadParsing.setLeadSourceDetails("Friends");
						
						CodeUtility codeUtility = new CodeUtility();
						codeUtility.sendEmailwithsendGrid("Parsing Lead Mail Subject", "First name: "+ leadParsing.getFirstName() +" , Last name: "+leadParsing.getLastName()+" , Email: "+leadParsing.getEmail()+" , Country: "+leadParsing.getCountry()+" , County: "+leadParsing.getCounty()+" , Zip: "+leadParsing.getZipPostalCode()+" , State: "+leadParsing.getStateProvince()+"" , "uatparsing@franconnect.com", "uatparsing@franconnect.com", "uatparsing@franconnect.com" , "salesautomation@staffex.com", "salesautomation@staffex.com", null);
						
						// Fetch Owner assigned to Lead through Rest API 
						String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadParsing);
						String ownerAssignedToLeadParsing = common.getValFromXML(xmlApi, "leadOwnerID");
						System.out.println("Owner Assigned to Lead "+leadParsing.getLeadFullName()+" >>>>>>>>"+ ownerAssignedToLeadParsing);
						
						// Validation
						if (k > 2) k = 0;
						if(! corpUsersList.get(k).equalsIgnoreCase(ownerAssignedToLeadParsing)) {
							fc.utobj().throwsException("Owner Assignment - RoundRobin - Parsing - FAILED");
						}
					}
				} catch (Exception e) {
					//	fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
						fc.utobj().throwsException(e.toString());
						System.out.println(testCaseId);
					}	
				}*/
			
		
			/*@Test(groups = { "RoundRobin_OwnerAssignment" , "salesRoundRobinThroughAPI" , "sales"})
			@TestCase(createdOn = "2018-05-01", updatedOn = "2018-05-28", testCaseId = "RoundRobin_Assignment_API", testCaseDescription = "Verify RoundRobin Lead Owner Assignment through API")
			void RoundRobin_Api() throws Exception {
				String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
				}.getClass().getEnclosingMethod().getName());

				driver = fc.commonMethods().browsers().openBrowser();

				try {
					driver = fc.loginpage().login(driver);

					fc.utobj().printTestStep("Add a user with name Round Robin1");
					CorporateUser corpUser = new CorporateUser();
					corpUser.setFirstName("Round");
					corpUser.setLastName("Robin1");
					fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
					fc.commonMethods().addCorporateUser(driver, corpUser);

					fc.utobj().printTestStep("Add a user with name Round Robin2");
					CorporateUser corpUser2 = new CorporateUser();
					corpUser2.setFirstName("Round");
					corpUser2.setLastName("Robin2");
					fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser2);
					fc.commonMethods().addCorporateUser(driver, corpUser2);

					fc.utobj().printTestStep("Add a user with name Round Robin3");
					CorporateUser corpUser3 = new CorporateUser();
					corpUser3.setFirstName("Round");
					corpUser3.setLastName("Robin3");
					fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser3);
					fc.commonMethods().addCorporateUser(driver, corpUser3);

					fc.adminpage().adminPage(driver);
					fc.adminpage().openAdminSalesAssignLeadOwners(driver);

					AssignLeadOwnersTest assignLeadOwner = new AssignLeadOwnersTest(driver);
					AssignLeadOwnerbyRoundRobinTest assignment = assignLeadOwner.setAssignLeadOwnerbyRoundRobin();
					assignment.moveAllUserTo_AvailableLeadOwners();
					assignment.moveUserTo_ConfiguredLeadOwners(corpUser.getuserFullName());
					assignment.moveUserTo_ConfiguredLeadOwners(corpUser2.getuserFullName());
					assignment.moveUserTo_ConfiguredLeadOwners(corpUser3.getuserFullName()).updateButton();

					Sales sales = new Sales(driver);

					Lead lead1 = sales.commonMethods().fillDefaultValue_LeadDetails();
					lead1.setBasedonAssignmentRules("yes");
					sales.commonMethods().addLeadThroughWebServices(driver, lead1);
					String response = sales.commonMethods().getLeadDetailsThroughWebServices_MajorFields(driver, lead1);
					if (sales.commonMethods().verifyDatainWebServiceResponse(driver, response,
							corpUser.getuserFullName()) == false) {
						fc.utobj().throwsException("Lead not assigned to the user.");
					}

					Lead lead2 = sales.commonMethods().fillDefaultValue_LeadDetails();
					lead2.setBasedonAssignmentRules("yes");
					sales.commonMethods().addLeadThroughWebServices(driver, lead2);
					response = sales.commonMethods().getLeadDetailsThroughWebServices_MajorFields(driver, lead2);
					if (sales.commonMethods().verifyDatainWebServiceResponse(driver, response,
							corpUser2.getuserFullName()) == false) {
						fc.utobj().throwsException("Lead not assigned to the user.");
					}

					Lead lead3 = sales.commonMethods().fillDefaultValue_LeadDetails();
					lead3.setBasedonAssignmentRules("yes");
					sales.commonMethods().addLeadThroughWebServices(driver, lead3);
					response = sales.commonMethods().getLeadDetailsThroughWebServices_MajorFields(driver, lead3);
					if (sales.commonMethods().verifyDatainWebServiceResponse(driver, response,
							corpUser3.getuserFullName()) == false) {
						fc.utobj().throwsException("Lead not assigned to the user.");
					}

					Lead lead4 = sales.commonMethods().fillDefaultValue_LeadDetails();
					lead4.setBasedonAssignmentRules("yes");
					sales.commonMethods().addLeadThroughWebServices(driver, lead4);
					response = sales.commonMethods().getLeadDetailsThroughWebServices_MajorFields(driver, lead4);
					if (sales.commonMethods().verifyDatainWebServiceResponse(driver, response,
							corpUser.getuserFullName()) == false) {
						fc.utobj().throwsException("Lead not assigned to the user.");
					}

				} catch (Exception e) {
					fc.utobj().throwsException(e.getMessage().toString());
					}
			}*/

	
}
	
	
	