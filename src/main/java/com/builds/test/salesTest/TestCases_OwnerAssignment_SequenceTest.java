package com.builds.test.salesTest;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.test.common.AddDivision;
import com.builds.test.common.ConfigureNewHierarchyLevel;
import com.builds.test.common.ConfigureNewHierarchyLevelTest;
import com.builds.test.common.CorporateUser;
import com.builds.test.common.ManageDivisionTest;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class TestCases_OwnerAssignment_SequenceTest {
	
	FranconnectUtil fc = new FranconnectUtil();
	Sales_Common_New common = new Sales_Common_New();
	CorporateUser corpUser = new CorporateUser();
	
	ArrayList<String> corpUsers = new ArrayList<String>();
	ArrayList<String> corpUsersForSalesTerritories1 = new ArrayList<String>();
	ArrayList<String> corpUsersForSalesTerritories2 = new ArrayList<String>();
	ArrayList<String> corpUsersForSalesTerritories3 = new ArrayList<String>();
	ArrayList<String> corpUsersForSalesTerritories4 = new ArrayList<String>();
	ArrayList<String> corpUsersForSource = new ArrayList<String>();
	
	SalesTerritories salesTerritories2 = new SalesTerritories(); 
	LeadSourceCategory leadSourceCategory1 = new LeadSourceCategory();
	AddDivision addDivision1 = new AddDivision();
	ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();
	
	// ====================================================================================================================
	
	CorporateUser corpUser2 = new CorporateUser();

	ArrayList<String> corpUsersForSource2 = new ArrayList<String>();
	ArrayList<String> corpUsersForSalesTerritoriesCounty = new ArrayList<String>();
	ArrayList<String> corpUsersForSalesTerritoriesZip = new ArrayList<String>();
	ArrayList<String> corpUsersForSalesTerritoriesStatesDomestic = new ArrayList<String>();
	ArrayList<String> corpUsersForSalesTerritoriesStatesInternational = new ArrayList<String>();
	ArrayList<String> corpUsersForDivision = new ArrayList<String>();
	
	SalesTerritories salesTerritoriesByCounty = new SalesTerritories(); 
	SalesTerritories salesTerritoriesByZip = new SalesTerritories(); 
	SalesTerritories salesTerritoriesByStatesDomestic = new SalesTerritories(); 
	SalesTerritories salesTerritoriesByStatesInternational = new SalesTerritories(); 

	LeadSourceCategory leadSourceCategory2 = new LeadSourceCategory();
	AddDivision addDivision2 = new AddDivision();
	AddDivision addDivision3 = new AddDivision();
	
	// ================================================================================================================
	
	CorporateUser corpUser3 = new CorporateUser();
	ArrayList<String> corpUsersList = new ArrayList<String>();
	
	// ======================================================================================================================
	
	// @BeforeClass(groups = {"sales_OwnerAssignment_SeqTest"})
	@Test( priority = 1 , groups = { "Division_OwnerAssignment" , "sales_OwnerAssignment_SeqTest", "123456" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_TC-01", testCaseDescription = "Configure Admin content required for Owner Assignment By Division")
	private void ConfigurationForOwnerAssignmentByDivision() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			
			driver = fc.loginpage().login(driver);
				
		// ADD Corporate Users - through API 
				for (int i = 0; i <= 2; i++) {											// (Adding 3 Corporate Users)
					fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
					corpUser.setFirstName("CorpUser" + i);
					corpUser.setLastName("DivisionRR" + fc.utobj().generateRandomNumber());
					corpUser.setUserName("user" + fc.utobj().generateRandomChar());
					fc.commonMethods().addCorporateUser(null, corpUser);
					System.out.println(corpUser.getFirstName() + " " + corpUser.getLastName());
					corpUsers.add(corpUser.getFirstName() + " " + corpUser.getLastName());
							}
				
				// ADD Corporate Users - through API (Associate different Owner for Sales Territories) - GroupBy(County)
							for (int i = 0; i <= 1; i++) {											// (Adding 2 Corporate Users)
								fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
								corpUser.setFirstName("CorpAssignByDiv" + i);
								corpUser.setLastName("STbyCounty" + fc.utobj().generateRandomNumber());
								corpUser.setUserName("user" + fc.utobj().generateRandomChar());
								fc.commonMethods().addCorporateUser(null, corpUser);
								System.out.println(corpUser.getFirstName() + " " + corpUser.getLastName());
								corpUsersForSalesTerritories1.add(corpUser.getFirstName() + " " + corpUser.getLastName());
							}

							// ADD Corporate Users - through API (Associate different Owner for Sales Territories) - GroupBy(ZIP/Postal Code)
							for (int i = 0; i <= 1; i++) {											// (Adding 2 Corporate Users)
								fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
								corpUser.setFirstName("CorpAssignByDiv" + i);
								corpUser.setLastName("STbyZIP" + fc.utobj().generateRandomNumber());
								corpUser.setUserName("user" + fc.utobj().generateRandomChar());
								fc.commonMethods().addCorporateUser(null, corpUser);
								System.out.println(corpUser.getFirstName() + " " + corpUser.getLastName());
								corpUsersForSalesTerritories2.add(corpUser.getFirstName() + " " + corpUser.getLastName());
							}
							// ADD Corporate Users - through API (Associate different Owner for Sales Territories) - GroupBy(States) Domestic
							for (int i = 0; i <= 1; i++) {											// (Adding 2 Corporate Users)
								fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
								corpUser.setFirstName("CorpAssignByDiv" + i);
								corpUser.setLastName("STbyStatesDomestic" + fc.utobj().generateRandomNumber());
								corpUser.setUserName("user" + fc.utobj().generateRandomChar());
								fc.commonMethods().addCorporateUser(null, corpUser);
								System.out.println(corpUser.getFirstName() + " " + corpUser.getLastName());
								corpUsersForSalesTerritories3.add(corpUser.getFirstName() + " " + corpUser.getLastName());
							}
							// ADD Corporate Users - through API (Associate different Owner for Sales Territories) - GroupBy(States) International
							for (int i = 0; i <= 1; i++) {											// (Adding 2 Corporate Users)
								fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
								corpUser.setFirstName("CorpAssignByDiv" + i);
								corpUser.setLastName("STbyStatesIntl" + fc.utobj().generateRandomNumber());
								corpUser.setUserName("user" + fc.utobj().generateRandomChar());
								fc.commonMethods().addCorporateUser(null, corpUser);
								System.out.println(corpUser.getFirstName() + " " + corpUser.getLastName());
								corpUsersForSalesTerritories4.add(corpUser.getFirstName() + " " + corpUser.getLastName());
							}
							
							// ADD Corporate Users - through API (Associate different Owner for : Source)
										for (int i = 0; i <= 2; i++) {											// (Adding 3 Corporate Users)
											fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
											corpUser.setFirstName("CorpAssignByDiv" + i);
											corpUser.setLastName("Source" + fc.utobj().generateRandomNumber());
											corpUser.setUserName("user" + fc.utobj().generateRandomChar());
											fc.commonMethods().addCorporateUser(null, corpUser);
											System.out.println(corpUser.getFirstName() + " " + corpUser.getLastName());
											corpUsersForSource.add(corpUser.getFirstName() + " " + corpUser.getLastName());
										}

		SetupSalesLeadOwnersTest setupSalesLeadOwnersTest = new SetupSalesLeadOwnersTest(driver);
		AssignLeadOwnersTest assignLeadOwnersTest = new AssignLeadOwnersTest(driver);

		ManageDivisionTest manageDivisionTest = new ManageDivisionTest(driver);
		
		SalesTerritoriesTest salesTerritoriesTest = new SalesTerritoriesTest(driver);
		
		ConfigureNewHierarchyLevelTest configureNewHierarchyLevelTest = new ConfigureNewHierarchyLevelTest(driver);
		ConfigureNewHierarchyLevel configureNewHierarchyLevel = new ConfigureNewHierarchyLevel();
		
		// Lead Source
		LeadSourceCategoryTest leadSourceCategoryTest = new LeadSourceCategoryTest(driver);

			// Turn ON Division
			configureNewHierarchyLevel.setEnableNewHierarchyLevel("Yes");
			configureNewHierarchyLevel.setLeadsExclusiveToNewHierarchyLevel("Yes");
			fc.commonMethods().getModules().openAdminPage(driver).openConfigureNewHierarchyLevel(driver);
			configureNewHierarchyLevelTest.EnableNewHierarchyLevel(configureNewHierarchyLevel);

			// Add Division #1
			//common.set_AddDivision(addDivision1); Random Dynamic Division Name
			addDivision1.setDivisionName("Division" +fc.utobj().generateRandomNumber());
			fc.commonMethods().getModules().openAdminPage(driver).openAddDivision(driver);
			manageDivisionTest.addDivision(addDivision1);
			
			// Adding Lead Source Category #1
			fc.commonMethods().getModules().openAdminPage(driver).openFranchiseSourcePage(driver);
			leadSourceCategory1.setLeadSourceCategory("Lead Source " + fc.utobj().generateRandomNumber());
			leadSourceCategory1.setIncludeInWebPage("yes");
			leadSourceCategoryTest.addLeadSourceCategory(leadSourceCategory1).verify_addLeadSourceCategory(leadSourceCategory1);

			// Setup Sales Lead Owners as ALL
			fc.commonMethods().getModules().openAdminPage(driver).openSetupFranchiseSalesLeadOwnersPage(driver);
			setupSalesLeadOwnersTest.setAllusersAsSalesLeadOwners_AndSubmit();
			
			// ADD Sales Territory #1 - GroupBy(County)
			ArrayList<String> SalesTerritories = new ArrayList<String>();
			SalesTerritories salesTerritories1 = new SalesTerritories(); 
			common.setGroupByCounty_Domestic_filladdNewSalesTerritory(salesTerritories1); // County Domestic
			salesTerritories1.getStates().clear();
			salesTerritories1.getCounties().clear();
			salesTerritories1.getStates().add("Oregon");
			salesTerritories1.getCounties().add("Morrow");
			fc.commonMethods().getModules().openAdminPage(driver).openSalesTerritoryPage(driver);
			salesTerritoriesTest.addNewSalesTerritory(salesTerritories1);
			SalesTerritories.add(salesTerritories1.getSalesTerritoryName()); 
			
			// ADD Sales Territory #2 - GroupBy(ZIP/Postal Code)
			common.setGroupByZipPostalCode_Domestic_filladdNewSalesTerritory(salesTerritories2);
			fc.commonMethods().getModules().openAdminPage(driver).openSalesTerritoryPage(driver);
			salesTerritoriesTest.addNewSalesTerritory(salesTerritories2);
			SalesTerritories.add(salesTerritories2.getSalesTerritoryName()); 
			
			// ADD Sales Territory #3 - GroupBy(States) Domestic
			SalesTerritories salesTerritories3 = new SalesTerritories(); 
			common.setGroupByStates_Domestic_filladdNewSalesTerritory(salesTerritories3);
			salesTerritories3.getStates().clear();
			salesTerritories3.getStates().add("Ohio");
			fc.commonMethods().getModules().openAdminPage(driver).openSalesTerritoryPage(driver);
			salesTerritoriesTest.addNewSalesTerritory(salesTerritories3);
			SalesTerritories.add(salesTerritories3.getSalesTerritoryName()); 

			// ADD Sales Territory #4 - GroupBy(States) International
			SalesTerritories salesTerritories4 = new SalesTerritories(); 
			common.setGroupByStates_International_filladdNewSalesTerritory(salesTerritories4);
			salesTerritories4.getCountries().clear();
			salesTerritories4.getStates().clear();
			salesTerritories4.getCountries().add("Germany");
			salesTerritories4.getStates().add("Hessen");
			fc.commonMethods().getModules().openAdminPage(driver).openSalesTerritoryPage(driver);
			salesTerritoriesTest.addNewSalesTerritory(salesTerritories4);
			SalesTerritories.add(salesTerritories4.getSalesTerritoryName()); 
			
			// Set -> Assign Lead Owner by Division
			fc.commonMethods().getModules().openAdminPage(driver).openAssignLeadOwnersPage(driver);
			assignLeadOwnersTest.set_defaultOwner("FranConnect Administrator");
			assignLeadOwnersTest.set_assignLeadOwnerbyDivision().Configure_AssignLeadOwnerbyDivision(addDivision1.getDivisionName(), corpUsers).AssociateDifferentOwnerFor_Source(corpUsersForSource, addDivision1.getDivisionName(), leadSourceCategory1.getLeadSourceCategory()).AssociateDifferentOwnerFor_SalesTerritories(corpUsersForSalesTerritories1, addDivision1.getDivisionName(),salesTerritories1.getSalesTerritoryName()).AssociateDifferentOwnerFor_SalesTerritories(corpUsersForSalesTerritories2, addDivision1.getDivisionName(), salesTerritories2.getSalesTerritoryName()).AssociateDifferentOwnerFor_SalesTerritories(corpUsersForSalesTerritories3, addDivision1.getDivisionName(), salesTerritories3.getSalesTerritoryName()).AssociateDifferentOwnerFor_SalesTerritories(corpUsersForSalesTerritories4, addDivision1.getDivisionName(), salesTerritories4.getSalesTerritoryName()).setPriority_SalesTerritory();
			
		fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		
		} catch (Exception e) {
			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			// fc.utobj().throwsException(e.toString());
			// System.out.println("Exception =" + e +" "+ "TestCaseID =" +testCaseId);
		}
	}
	
	@Test(priority = 2 , dependsOnMethods="ConfigurationForOwnerAssignmentByDivision" , groups = { "DivPrioritySource123" , "Division_OwnerAssignment" , "sales_OwnerAssignment_SeqTest" , "123456" })
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_TC-02_Api", testCaseDescription = "Owner Assignment By Division - RoundRobin - Priority(Source) - Api")
	private void OwnerAssignmentByDivision_TC_02_Api() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
				
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		try {
			
			driver = fc.loginpage().login(driver);
			
			// Verify Division RoundRobin
			fc.utobj().printTestStep("Check Division RoundRobin Lead Owner Assignment through API - Adding 6 Leads");
			
			// Through API  ---------------------------------------------------------------------------------------------------------------------------------------------------------------
			Lead lead3 = new Lead();
			for (int j = 0, k = 0; j <= 5; j++, k++) {
				lead3.setFirstName("DivisionLead" + j);
				lead3.setLastName("Api_RoundRobin" + fc.utobj().generateRandomNumber());
				lead3.setEmail("frantest2017@gmail.com");
				lead3.setDivision(addDivision1.getDivisionName());
				lead3.setBasedonAssignmentRules("yes");
				lead3.setLeadSourceCategory("Friends");
				lead3.setLeadSourceDetails("Friends");
				common.addLeadThroughWebServices(driver, lead3);

				
				String xml3 = common.getLeadDetailsThroughWebServices_MajorFields(driver, lead3);
				String ownerAssignedToLead3 = common.getValFromXML(xml3, "leadOwnerID");
				System.out.println("Owner Assigned to Lead "+lead3.getLeadFullName()+" >>>>>>>>" + ownerAssignedToLead3);
				if (k > 2)
					k = 0;
				if (!corpUsers.get(k).equalsIgnoreCase(ownerAssignedToLead3)) {
					fc.utobj().throwsException("Division RoundRobin Lead Owner Assignment - API  - FAILED");
				}
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			}
	}
	
	// @BeforeClass(groups = {"sales_OwnerAssignment_SeqTest"})
		@Test(priority = 15 /*, dependsOnMethods="ConfigurationForOwnerAssignmentByDivision"*/ , groups = { "LeadSource_OwnerAssignment" , "sales_OwnerAssignment_SeqTest" , "LeadSourceAssignment_Configuration" , "sales_OwnerAssignment_Failed" , "Sales_OwnerAssignment_WebForm" })
		@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_10" , testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source")
		private void ConfigurationForOwnerAssignmentByLeadSource() throws Exception {
			String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
			}.getClass().getEnclosingMethod().getName());

			 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			
			AssignLeadOwnersTest assignLeadOwnersTest = new AssignLeadOwnersTest(driver);
			ManageDivisionTest manageDivisionTest = new ManageDivisionTest(driver);
			ConfigureNewHierarchyLevel configureNewHierarchyLevel = new ConfigureNewHierarchyLevel();
			ConfigureNewHierarchyLevelTest configureNewHierarchyLevelTest = new ConfigureNewHierarchyLevelTest(driver);
			LeadSourceCategoryTest leadSourceCategoryTest = new LeadSourceCategoryTest(driver);
			SalesTerritoriesTest salesTerritoriesTest = new SalesTerritoriesTest(driver);
			
			
		
			try {
					
				 driver = fc.loginpage().login(driver);
					
					// Assignment Territory Preference --> Top:Zip Medium:County Low:State
					
					
					// ADD Corporate Users - through API 
					for (int i = 0; i <= 2; i++) {											// (Adding 3 Corporate Users)
						fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser2);
						corpUser2.setFirstName("CorpUser_LeadSource" + i);
						corpUser2.setLastName("ForLeadSource" + fc.utobj().generateRandomNumber());
						corpUser2.setUserName("user" + fc.utobj().generateRandomChar());
						fc.commonMethods().addCorporateUser(driver, corpUser2);
						System.out.println(corpUser2.getFirstName() + " " + corpUser2.getLastName());
						corpUsersForSource2.add(corpUser2.getFirstName() + " " + corpUser2.getLastName());
					}
					
					
					// ADD Corporate Users - through API (Associate different Owner for Sales Territories) - GroupBy(County)
					for (int i = 0; i <= 1; i++) {											// (Adding 2 Corporate Users)
						fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser2);
						corpUser2.setFirstName("CorpUser_LeadSource" + i);
						corpUser2.setLastName("ForST_County" + fc.utobj().generateRandomNumber());
						corpUser2.setUserName("user" + fc.utobj().generateRandomChar());
						fc.commonMethods().addCorporateUser(driver, corpUser2);
						System.out.println(corpUser2.getFirstName() + " " + corpUser2.getLastName());
						corpUsersForSalesTerritoriesCounty.add(corpUser2.getFirstName() + " " + corpUser2.getLastName());
					}
					// ADD Corporate Users - through API (Associate different Owner for Sales Territories) - GroupBy(ZIP/Postal Code)
					for (int i = 0; i <= 1; i++) {											// (Adding 2 Corporate Users)
						fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser2);
						corpUser2.setFirstName("CorpUser_LeadSource" + i);
						corpUser2.setLastName("ForST_Zip" + fc.utobj().generateRandomNumber());
						corpUser2.setUserName("user" + fc.utobj().generateRandomChar());
						fc.commonMethods().addCorporateUser(driver, corpUser2);
						System.out.println(corpUser2.getFirstName() + " " + corpUser2.getLastName());
						corpUsersForSalesTerritoriesZip.add(corpUser2.getFirstName() + " " + corpUser2.getLastName());
					}
					// ADD Corporate Users - through API (Associate different Owner for Sales Territories) - GroupBy(States) Domestic
					for (int i = 0; i <= 1; i++) {											// (Adding 2 Corporate Users)
						fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser2);
						corpUser2.setFirstName("CorpUser_LeadSource" + i);
						corpUser2.setLastName("ForST_StatesDom" + fc.utobj().generateRandomNumber());
						corpUser2.setUserName("user" + fc.utobj().generateRandomChar());
						fc.commonMethods().addCorporateUser(driver, corpUser2);
						System.out.println(corpUser2.getFirstName() + " " + corpUser2.getLastName());
						corpUsersForSalesTerritoriesStatesDomestic.add(corpUser2.getFirstName() + " " + corpUser2.getLastName());
					}
					// ADD Corporate Users - through API (Associate different Owner for Sales Territories) - GroupBy(States) International
					for (int i = 0; i <= 1; i++) {											// (Adding 2 Corporate Users)
						fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser2);
						corpUser2.setFirstName("CorpUser_LeadSource" + i);
						corpUser2.setLastName("ForST_StatesIntl" + fc.utobj().generateRandomNumber());
						corpUser2.setUserName("user" + fc.utobj().generateRandomChar());
						fc.commonMethods().addCorporateUser(driver, corpUser2);
						System.out.println(corpUser2.getFirstName() + " " + corpUser2.getLastName());
						corpUsersForSalesTerritoriesStatesInternational.add(corpUser2.getFirstName() + " " + corpUser2.getLastName());
					}
					
					
					// ADD Corporate Users - through API (Associate different Owner for Division)
					for (int i = 0; i <= 1; i++) {											// (Adding 2 Corporate Users)
						fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser2);
						corpUser2.setFirstName("CorpUser_LeadSource" + i);
						corpUser2.setLastName("ForDivision" + fc.utobj().generateRandomNumber());
						corpUser2.setUserName("user" + fc.utobj().generateRandomChar());
						fc.commonMethods().addCorporateUser(driver, corpUser2);
						System.out.println(corpUser2.getFirstName() + " " + corpUser2.getLastName());
						corpUsersForDivision.add(corpUser2.getFirstName() + " " + corpUser2.getLastName());
					}
				
					
					// Turn ON Division
					configureNewHierarchyLevel.setEnableNewHierarchyLevel("Yes");
					fc.commonMethods().getModules().openAdminPage(driver).openConfigureNewHierarchyLevel(driver);
					configureNewHierarchyLevelTest.EnableNewHierarchyLevel(configureNewHierarchyLevel);
					
					// Add Division #1
					common.set_AddDivision(addDivision2);
					fc.commonMethods().getModules().openAdminPage(driver).openAddDivision(driver);
					manageDivisionTest.addDivision(addDivision2);
					
					// Add Division #2
					common.set_AddDivision(addDivision3);
					fc.commonMethods().getModules().openAdminPage(driver).openAddDivision(driver);
					manageDivisionTest.addDivision(addDivision3);
					
					// Adding Lead Source Category #1
					fc.commonMethods().getModules().openAdminPage(driver).openFranchiseSourcePage(driver);
					leadSourceCategory2.setLeadSourceCategory("Lead Source " + fc.utobj().generateRandomNumber());
					leadSourceCategory2.setIncludeInWebPage("yes");
					leadSourceCategoryTest.addLeadSourceCategory(leadSourceCategory2).verify_addLeadSourceCategory(leadSourceCategory2);
					
					
					/*	
					// Adding Lead Source Category #2
					LeadSourceCategory leadSourceCategory2 = new LeadSourceCategory();
					leadSourceCategory2.setLeadSourceCategory("Lead Source " + fc.utobj().generateRandomNumber());
					leadSourceCategory2.setIncludeInWebPage("no");
					leadSourceCategoryTest.addLeadSourceCategory(leadSourceCategory2).verify_addLeadSourceCategory(leadSourceCategory2);
					
					// Adding Lead Source Category #3
					LeadSourceCategory leadSourceCategory3 = new LeadSourceCategory();
					leadSourceCategory3.setLeadSourceCategory("Lead Source " + fc.utobj().generateRandomNumber());
					leadSourceCategory3.setIncludeInWebPage("no");
					leadSourceCategoryTest.addLeadSourceCategory(leadSourceCategory3).verify_addLeadSourceCategory(leadSourceCategory3);
					*/
					
					// ADD Sales Territory #1 - GroupBy(County)
					ArrayList<String> SalesTerritories = new ArrayList<String>();
					common.setGroupByCounty_Domestic_filladdNewSalesTerritory(salesTerritoriesByCounty); // County Domestic
					fc.commonMethods().getModules().openAdminPage(driver).openSalesTerritoryPage(driver);
					salesTerritoriesTest.addNewSalesTerritory(salesTerritoriesByCounty);
					SalesTerritories.add(salesTerritoriesByCounty.getSalesTerritoryName()); 
					
					// ADD Sales Territory #2 - GroupBy(ZIP/Postal Code)
					common.setGroupByZipPostalCode_Domestic_filladdNewSalesTerritory(salesTerritoriesByZip);
					fc.commonMethods().getModules().openAdminPage(driver).openSalesTerritoryPage(driver);
					salesTerritoriesTest.addNewSalesTerritory(salesTerritoriesByZip);
					SalesTerritories.add(salesTerritoriesByZip.getSalesTerritoryName()); 
					
					// ADD Sales Territory #3 - GroupBy(States) Domestic
					common.setGroupByStates_Domestic_filladdNewSalesTerritory(salesTerritoriesByStatesDomestic);
					fc.commonMethods().getModules().openAdminPage(driver).openSalesTerritoryPage(driver);
					salesTerritoriesTest.addNewSalesTerritory(salesTerritoriesByStatesDomestic);
					SalesTerritories.add(salesTerritoriesByStatesDomestic.getSalesTerritoryName()); 

					// ADD Sales Territory #4 - GroupBy(States) International
					common.setGroupByStates_International_filladdNewSalesTerritory(salesTerritoriesByStatesInternational);
					fc.commonMethods().getModules().openAdminPage(driver).openSalesTerritoryPage(driver);
					salesTerritoriesTest.addNewSalesTerritory(salesTerritoriesByStatesInternational);
					SalesTerritories.add(salesTerritoriesByStatesInternational.getSalesTerritoryName()); 
					
					
					// Assign Lead Owner by Lead Source
					fc.commonMethods().getModules().openAdminPage(driver).openAssignLeadOwnersPage(driver);
					assignLeadOwnersTest.set_defaultOwner("FranConnect Administrator");
					// Assign Owners for Source + Assign Different Owner for Sales Territories + Assign Different Owner for Division + Set Priority(Sales Territories)
					assignLeadOwnersTest.set_assignLeadOwnerbyLeadSource().Configure_AssignLeadOwnerbyLeadSource(leadSourceCategory2.getLeadSourceCategory(), corpUsersForSource2).AssociateDifferentOwnerFor_SalesTerritories(corpUsersForSalesTerritoriesCounty, leadSourceCategory2.getLeadSourceCategory(), salesTerritoriesByCounty.getSalesTerritoryName()).AssociateDifferentOwnerFor_SalesTerritories(corpUsersForSalesTerritoriesZip, leadSourceCategory2.getLeadSourceCategory(), salesTerritoriesByZip.getSalesTerritoryName()).AssociateDifferentOwnerFor_SalesTerritories(corpUsersForSalesTerritoriesStatesDomestic, leadSourceCategory2.getLeadSourceCategory(), salesTerritoriesByStatesDomestic.getSalesTerritoryName()).AssociateDifferentOwnerFor_SalesTerritories(corpUsersForSalesTerritoriesStatesInternational, leadSourceCategory2.getLeadSourceCategory(), salesTerritoriesByStatesInternational.getSalesTerritoryName()).AssociateDifferentOwnerFor_Division(corpUsersForDivision, leadSourceCategory2.getLeadSourceCategory(), addDivision2.getDivisionName()).setPriority_SalesTerritory();
					
					
					fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

					} catch (Exception e) {
						fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
					}

				}
		
		
		
		@Test(priority = 16 , dependsOnMethods="ConfigurationForOwnerAssignmentByLeadSource" ,groups = { "LeadSource_OwnerAssignment", "sales_OwnerAssignment_SeqTest" , "sales_OwnerAssignment_Failed" })
		@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "Sales_OwnerAssignment_11", testCaseDescription = "To verify Owner Assignment : Assign Lead Owner by Lead Source - Round Robin")
		private void OwnerAssignmentByLeadSource_RoundRobin() throws Exception {
			String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
			}.getClass().getEnclosingMethod().getName());
			
			 WebDriver driver = fc.commonMethods().browsers().openBrowser();
			try {
				
				 driver = fc.loginpage().login(driver);
				
				// Through API
				for (int j = 0; j <= 3; j++) {
					// Add Lead
					Lead lead = new Lead();
					lead.setFirstName("LeadSource" +j);
					lead.setLastName("RR" +fc.utobj().generateRandomNumber());
					lead.setEmail("frantest2017@gmail.com");
					lead.setBasedonAssignmentRules("yes");
					lead.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
					lead.setLeadSourceDetails("None");
					common.addLeadThroughWebServices(driver, lead);

					// Fetch Owner assigned to Lead through Rest API
					String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(driver, lead);
					// System.out.println(xmlApi);
					String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
					System.out.println("Owner Assigned to Lead >>>>>>>>" + lead.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);

					// Validation
					if (!corpUsersForSource2.contains(ownerAssignedToLeadApi)) {
						fc.utobj().throwsException("Assign Lead Owner By LeadSource - Priority(Sales Territories) - RR - Api - FAILED");
					}
				}
				
				// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
				// Create CSV File with Lead Details
				String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_Source_RR.csv";
				System.out.println(filePath);
				List<Lead> leadList = new ArrayList<Lead>();

				for (int j = 0; j <= 3; j++) {
					Lead importLead = new Lead();
					importLead.setFirstName("LeadSource_Import" +j);
					importLead.setLastName("RR" +fc.utobj().generateRandomNumber());
					importLead.setEmail("frantest2017@gmail.com");
					// importLead.setDivision(addDivision1.getDivisionName());
					//importLead.setCountry("USA");
					//importLead.setStateProvince("Florida");
					//importLead.setCounty("Gulf");
					// importLead.setZipPostalCode(salesTerritories2.getZipPostalCodeText());
					importLead.setBasedonAssignmentRules("yes");
					importLead.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
					importLead.setLeadSourceDetails("None");

					leadList.add(importLead);
				}

				common.writeCsvFile(filePath, leadList);

				// Import the CSV file
				Import importlead = new Import();
				ImportTest importTest = new ImportTest(driver);

				// Navigate to Import Tab
				fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

				importlead.setImportType("Leads");
				importlead.setSpecifyFileFormat("CSV");
				importlead.setSalesDataFile(filePath);
				// importlead.setSalesDataFile(fc.utobj().getFilePathFromTestData("AsgmntByDivPrioritySalesTerritories_GroupByStatesIntl.csv"));
				importlead.setLeadStatus("New Lead");
				importlead.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
				importlead.setLeadSourceDetails("None");

				importTest.importLeadsCSV(importlead);

				// Validation
				Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

				Lead x = new Lead();
				List<String> leadsFirstNames = map.get("FN");
				List<String> leadsLastNames = map.get("LN");

				for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
					System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
					x.setFirstName(leadsFirstNames.get(a));
					x.setLastName(leadsLastNames.get(b));

					String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
					String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
					System.out.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

					// Validate Owner Assignment
					if (!corpUsersForSource2.contains(ownerAssignedToLeadx)) {
						fc.utobj().throwsException("Assign Lead Owner by Lead Source - Priority(Sales Territories) - RR - Import - FAILED");
					}
				}
				
				// Through WebForm ----------------------------------------------------------------------------------------------------------------------------------------------------------
				// Create WebForm
				ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
				ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();
				
				fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
				common.set_ManageWebform(manageWebFormGenerator);
				manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
				manageWebFormGenerator.setLeadSourceDetails("None");
				// manageWebFormGenerator.setDivision(addDivision1.getDivisionName());
				manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

				// Add Lead - WebForm
				Lead webformLead = new Lead();
				for (int j = 0; j <= 2; j++) {
					webformLead.setFirstName("LeadSource_WebForm" + j);
					webformLead.setLastName("RR" + fc.utobj().generateRandomNumber());
					webformLead.setEmail("frantest2017@gmail.com");
					// webformLead.setDivision(addDivision1.getDivisionName());
					//webformLead.setCountry("USA");
					//webformLead.setStateProvince("Florida");
					//webformLead.setCounty("Gulf");
					//webformLead.setZipPostalCode(salesTerritories2.getZipPostalCodeText());
					webformLead.setLeadSourceCategory(leadSourceCategory2.getLeadSourceCategory());
					webformLead.setLeadSourceDetails("None");

					// Add Lead through WebForm
					manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, webformLead);

					// fc.utobj().sleep();
					String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, webformLead);
					// System.out.println(xml2);
					String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
					System.out.println("Owner Assigned to Lead " + webformLead.getLeadFullName() + " >>>>>>>>"
							+ ownerAssignedTowebformLead);
					// Validate
					if (!corpUsersForSource2.contains(ownerAssignedTowebformLead)) {
						fc.utobj().throwsException("Assign Lead Owner by Lead Source - Priority(Sales Territories) - RR - WebForm - FAILED");
					}
				}
				
				fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
				
			}
		}
		
		// @BeforeClass(groups = {"sales_OwnerAssignment_SeqTest"})
				@Test(priority = 261 /*, dependsOnMethods="ConfigurationForOwnerAssignmentByDivision"*/ , groups = { "RR_OwnerAssignment", "sales_OwnerAssignment_SeqTest"  , "sales_OwnerAssignment_Failed" })
				@TestCase(createdOn = "2018-05-02", updatedOn = "2018-05-28", testCaseId = "Sales_OwnerAssignment_98", testCaseDescription = "Verify Round Robin Owner Assignment Configuration")
				void Configure_LeadOwnerByRoundRobin() throws Exception {
					String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
					}.getClass().getEnclosingMethod().getName());

					WebDriver driver = fc.commonMethods().browsers().openBrowser();
					
					AssignLeadOwnersTest assignLeadOwnersTest = new AssignLeadOwnersTest(driver);
					SetupSalesLeadOwnersTest setupSalesLeadOwnersTest = new SetupSalesLeadOwnersTest(driver);
					AssignLeadOwnerbyRoundRobinTest assignLeadOwnerbyRoundRobinTest = new AssignLeadOwnerbyRoundRobinTest(driver);

					try {
						driver = fc.loginpage().login(driver);

						for (int i = 0; i <= 2; i++) {
							fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser3);
							corpUser3.setFirstName("CorpUser" + i);
							corpUser3.setLastName("RoundRobin" + fc.utobj().generateRandomNumber());
							corpUser3.setUserName("user" + fc.utobj().generateRandomChar());
							fc.commonMethods().addCorporateUser(driver, corpUser3);
							//System.out.println(corpUser.getFirstName() + " " + corpUser.getLastName());
							corpUsersList.add(corpUser3.getFirstName() + " " + corpUser3.getLastName());
						}

						fc.commonMethods().getModules().openAdminPage(driver).openAssignLeadOwnersPage(driver);
						assignLeadOwnersTest.set_defaultOwner("FranConnect Administrator");
						assignLeadOwnersTest.setAssignLeadOwnerbyRoundRobin();
						assignLeadOwnerbyRoundRobinTest.moveAllUserTo_AvailableLeadOwners();

						System.out.println(corpUsersList);
						for (String temp : corpUsersList) {
							assignLeadOwnerbyRoundRobinTest.moveUserTo_ConfiguredLeadOwners(temp);
						}
						assignLeadOwnerbyRoundRobinTest.updateButton();

						fc.commonMethods().getModules().openAdminPage(driver).openSetupFranchiseSalesLeadOwnersPage(driver);
						setupSalesLeadOwnersTest.setAllusersAsSalesLeadOwners_AndSubmit();
						
						fc.commonMethods().getModules().openAdminPage(driver).openConfigureSalesEmailParsingServer(driver);
						ConfigureSalesEmailParsingServerTest configureSalesEmailParsingServerTest = new ConfigureSalesEmailParsingServerTest(driver);
						configureSalesEmailParsingServerTest.configureSalesEmailParsingServer();


						fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

					} catch (Exception e) {
						fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
						/*fc.utobj().throwsException(e.toString());
						System.out.println(testCaseId);*/
					}
				}
				
				 @Test(priority = 262 , dependsOnMethods="Configure_LeadOwnerByRoundRobin" , groups = { "RR_OwnerAssignment" , "sales_OwnerAssignment_SeqTest"})
				@TestCase(createdOn = "2018-04-27", updatedOn = "2018-05-27", testCaseId = "Sales_OwnerAssignment_99", testCaseDescription = "Verify Round Robin Owner Assignment through Lead addition from FrontEnd")
				void RoundRobin_FrontEnd() throws Exception {
					String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
					}.getClass().getEnclosingMethod().getName());

					WebDriver driver = fc.commonMethods().browsers().openBrowser();

					SetupSalesLeadOwnersTest setupSalesLeadOwnersTest = new SetupSalesLeadOwnersTest(driver);
					Lead leadFrontEnd = new Lead();
					PrimaryInfoTest primaryInfoTest = new PrimaryInfoTest(driver);
					LeadManagementTest leadManagementTest = new LeadManagementTest(driver);
					
					try { 
						
						driver = fc.loginpage().login(driver);
						
						fc.commonMethods().getModules().openAdminPage(driver).openSetupFranchiseSalesLeadOwnersPage(driver);
						setupSalesLeadOwnersTest.setAllusersAsSalesLeadOwners_AndSubmit();

						for (int j = 0, k = 0; j <= 5; j++, k++) {
							leadFrontEnd.setFirstName("Lead_FrontEnd" +j);
							leadFrontEnd.setLastName("RoundRobin" + fc.utobj().generateRandomNumber());
							leadFrontEnd.setEmail("frantest2017@gmail.com");
							leadFrontEnd.setBasedonAssignmentRules("yes");
							leadFrontEnd.setLeadSourceCategory("Friends");
							leadFrontEnd.setLeadSourceDetails("Friends");
							
							fc.commonMethods().getModules().sales().sales_common().fsModule(driver).leadManagement(driver);
							leadManagementTest.clickAddLead();
							primaryInfoTest.fillLeadInfo_ClickSave(leadFrontEnd);
							
							String xmlFrontEnd = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadFrontEnd);
							String ownerAssignedToLeadFrontEnd = common.getValFromXML(xmlFrontEnd, "leadOwnerID");
							System.out.println("Owner Assigned to Lead : "+leadFrontEnd.getLeadFullName()+" >>>>>>>>" + ownerAssignedToLeadFrontEnd);
							if (k > 2)
								k = 0;
							if (!corpUsersList.get(k).equalsIgnoreCase(ownerAssignedToLeadFrontEnd)) {
								fc.utobj().throwsException("Lead Owner Mismatch - FrontEnd - Issue in RoundRobin Lead Owner Assignment");
							}
						}
						
						fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

					} catch (Exception e) {
						fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
						/*fc.utobj().throwsException(e.toString());
						System.out.println(testCaseId);*/
					}
				}

}
