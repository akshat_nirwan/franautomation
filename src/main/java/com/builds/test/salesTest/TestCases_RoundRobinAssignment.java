package com.builds.test.salesTest;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterGroups;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.builds.test.common.CorporateUser;
import com.builds.test.common.CorporateUserTest;
import com.builds.test.crm.LeadSummaryPage;
import com.builds.uimaps.fs.AssignLeadOwnerbyRoundRobinUI;
import com.builds.uimaps.fs.ManageWebFormGeneratorUI;
import com.builds.utilities.CodeUtility;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

class TestCases_RoundRobinAssignment {

	FranconnectUtil fc = new FranconnectUtil();
	Sales_Common_New common = new Sales_Common_New();
	CorporateUser corpUser3 = new CorporateUser();
	ArrayList<String> corpUsersList = new ArrayList<String>();

	// @BeforeClass(groups = {"sales_OwnerAssignment111"})
	@Test(priority = 261 , groups = { "RR_OwnerAssignment", "sales_OwnerAssignment111"  , "sales_OwnerAssignment_Failed" })
	@TestCase(createdOn = "2018-05-02", updatedOn = "2018-05-28", testCaseId = "Sales_OwnerAssignment_98", testCaseDescription = "Verify Round Robin Owner Assignment Configuration")
	void Configure_LeadOwnerByRoundRobin() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		AssignLeadOwnersTest assignLeadOwnersTest = new AssignLeadOwnersTest(driver);
		SetupSalesLeadOwnersTest setupSalesLeadOwnersTest = new SetupSalesLeadOwnersTest(driver);
		AssignLeadOwnerbyRoundRobinTest assignLeadOwnerbyRoundRobinTest = new AssignLeadOwnerbyRoundRobinTest(driver);

		try {
			driver = fc.loginpage().login(driver);

			for (int i = 0; i <= 2; i++) {
				fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser3);
				corpUser3.setFirstName("CorpUser" + i);
				corpUser3.setLastName("RoundRobin" + fc.utobj().generateRandomNumber());
				corpUser3.setUserName("user" + fc.utobj().generateRandomChar());
				fc.commonMethods().addCorporateUser(driver, corpUser3);
				//System.out.println(corpUser.getFirstName() + " " + corpUser.getLastName());
				corpUsersList.add(corpUser3.getFirstName() + " " + corpUser3.getLastName());
			}

			fc.commonMethods().getModules().openAdminPage(driver).openAssignLeadOwnersPage(driver);
			assignLeadOwnersTest.set_defaultOwner("FranConnect Administrator");
			assignLeadOwnersTest.setAssignLeadOwnerbyRoundRobin();
			assignLeadOwnerbyRoundRobinTest.moveAllUserTo_AvailableLeadOwners();

			System.out.println(corpUsersList);
			for (String temp : corpUsersList) {
				assignLeadOwnerbyRoundRobinTest.moveUserTo_ConfiguredLeadOwners(temp);
			}
			assignLeadOwnerbyRoundRobinTest.updateButton();

			fc.commonMethods().getModules().openAdminPage(driver).openSetupFranchiseSalesLeadOwnersPage(driver);
			setupSalesLeadOwnersTest.setAllusersAsSalesLeadOwners_AndSubmit();
			
			fc.commonMethods().getModules().openAdminPage(driver).openConfigureSalesEmailParsingServer(driver);
			ConfigureSalesEmailParsingServerTest configureSalesEmailParsingServerTest = new ConfigureSalesEmailParsingServerTest(driver);
			configureSalesEmailParsingServerTest.configureSalesEmailParsingServer();


			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			/*fc.utobj().throwsException(e.toString());
			System.out.println(testCaseId);*/
		}
	}
	
	 @Test(priority = 262 , dependsOnMethods="Configure_LeadOwnerByRoundRobin" , groups = { "RR_OwnerAssignment" , "sales_OwnerAssignment111"})
	@TestCase(createdOn = "2018-04-27", updatedOn = "2018-05-27", testCaseId = "Sales_OwnerAssignment_99", testCaseDescription = "Verify Round Robin Owner Assignment through Lead addition from FrontEnd")
	void RoundRobin_FrontEnd() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		SetupSalesLeadOwnersTest setupSalesLeadOwnersTest = new SetupSalesLeadOwnersTest(driver);
		Lead leadFrontEnd = new Lead();
		PrimaryInfoTest primaryInfoTest = new PrimaryInfoTest(driver);
		LeadManagementTest leadManagementTest = new LeadManagementTest(driver);
		
		try { 
			
			driver = fc.loginpage().login(driver);
			
			fc.commonMethods().getModules().openAdminPage(driver).openSetupFranchiseSalesLeadOwnersPage(driver);
			setupSalesLeadOwnersTest.setAllusersAsSalesLeadOwners_AndSubmit();

			for (int j = 0, k = 0; j <= 5; j++, k++) {
				leadFrontEnd.setFirstName("Lead_FrontEnd" +j);
				leadFrontEnd.setLastName("RoundRobin" + fc.utobj().generateRandomNumber());
				leadFrontEnd.setEmail("frantest2017@gmail.com");
				leadFrontEnd.setBasedonAssignmentRules("yes");
				leadFrontEnd.setLeadSourceCategory("Friends");
				leadFrontEnd.setLeadSourceDetails("Friends");
				
				fc.commonMethods().getModules().sales().sales_common().fsModule(driver).leadManagement(driver);
				leadManagementTest.clickAddLead();
				primaryInfoTest.fillLeadInfo_ClickSave(leadFrontEnd);
				
				String xmlFrontEnd = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadFrontEnd);
				String ownerAssignedToLeadFrontEnd = common.getValFromXML(xmlFrontEnd, "leadOwnerID");
				System.out.println("Owner Assigned to Lead : "+leadFrontEnd.getLeadFullName()+" >>>>>>>>" + ownerAssignedToLeadFrontEnd);
				if (k > 2)
					k = 0;
				if (!corpUsersList.get(k).equalsIgnoreCase(ownerAssignedToLeadFrontEnd)) {
					fc.utobj().throwsException("Lead Owner Mismatch - FrontEnd - Issue in RoundRobin Lead Owner Assignment");
				}
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			/*fc.utobj().throwsException(e.toString());
			System.out.println(testCaseId);*/
		}
	}

	
	@Test(priority = 263 , dependsOnMethods="Configure_LeadOwnerByRoundRobin" , groups = { "RR_OwnerAssignment" , "sales_OwnerAssignment111" /*, "sales_OwnerAssignment_Failed"*/ })
	@TestCase(createdOn = "2018-04-27", updatedOn = "2018-05-27", testCaseId = "Sales_OwnerAssignment_100", testCaseDescription = "Verify Round Robin Owner Assignment through Lead addition from WebForm")
	void RoundRobin_WebForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();
		ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);		

		try {
			driver = fc.loginpage().login(driver);

			fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
			common.set_ManageWebform(manageWebFormGenerator);
			manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

			fc.commonMethods().getModules().openAdminPage(driver).openFranchiseSalesManageWebFormGenerator(driver);

			Lead leadWebForm = new Lead();
			for (int j = 0, k = 0; j <= 5; j++, k++) {
				leadWebForm.setFirstName("Lead_WebForm" +j);
				leadWebForm.setLastName("RoundRobin" + fc.utobj().generateRandomNumber());
				leadWebForm.setEmail("frantest2017@gmail.com");
				leadWebForm.setLeadSourceCategory("Friends");
				leadWebForm.setLeadSourceDetails("Friends");
				// Add Lead through WebForm
				manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm); 

				String xml = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
				String ownerAssignedToLeadWebForm = common.getValFromXML(xml, "leadOwnerID");
				System.out.println("Owner Assigned to Lead : "+leadWebForm.getLeadFullName()+" >>>>>>>>" + ownerAssignedToLeadWebForm);
				if (k > 2)
					k = 0;
				if (!corpUsersList.get(k).equalsIgnoreCase(ownerAssignedToLeadWebForm)) {
					fc.utobj().throwsException("Lead Owner Mismatch - Issue in RoundRobin Lead Owner Assignment");
				}
		}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			/*fc.utobj().throwsException(e.toString());
			System.out.println(testCaseId);*/
		}
			
	}
	
	

		 @Test(priority = 264 , dependsOnMethods="Configure_LeadOwnerByRoundRobin" , groups = { "RR_OwnerAssignment" ,  "sales_OwnerAssignment111"})
		@TestCase(createdOn = "2018-04-27", updatedOn = "2018-05-27", testCaseId = "Sales_OwnerAssignment_101", testCaseDescription = "Verify Round Robin Owner Assignment through Lead addition from Api")
		void RoundRobin_Api() throws Exception {
			String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
			}.getClass().getEnclosingMethod().getName());
			
			WebDriver driver = fc.commonMethods().browsers().openBrowser();


			try {
				
				driver = fc.loginpage().login(driver);
				
				// Through API -------------------------------------------------------------------------------------------------------------------------------------------------------------------
				for(int j=0,k=0; j<=5; j++,k++)
				{
				Lead leadApi = new Lead();
				leadApi.setFirstName("Lead_Api"+j);
				leadApi.setLastName("RoundRobin" +fc.utobj().generateRandomNumber());
				leadApi.setEmail("frantest2017@gmail.com");
				leadApi.setBasedonAssignmentRules("yes");
				leadApi.setLeadSourceCategory("Friends");
				leadApi.setLeadSourceDetails("Friends");
				common.addLeadThroughWebServices(driver, leadApi);
				
				// Fetch Owner assigned to Lead through Rest API 
				String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadApi);
				String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
				System.out.println("Owner Assigned to Lead "+leadApi.getLeadFullName()+" >>>>>>>>"+ ownerAssignedToLeadApi);
				
				// Validation
				if (k > 2)
					k = 0;
				if(! corpUsersList.get(k).equalsIgnoreCase(ownerAssignedToLeadApi)) {
					fc.utobj().throwsException("Owner Assignment - RoundRobin - API - FAILED");
				}
				}
				
				fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
				
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		//	fc.utobj().throwsException(e.toString());
		//	System.out.println(testCaseId);
		}
		
	}
		
		@Test(priority = 265 , dependsOnMethods="Configure_LeadOwnerByRoundRobin" , groups = { "RR_OwnerAssignment" ,  "sales_OwnerAssignment111"})
		@TestCase(createdOn = "2018-04-27", updatedOn = "2018-05-27", testCaseId = "Sales_OwnerAssignment_102", testCaseDescription = "Verify Round Robin Owner Assignment through Lead addition from Import")
		void RoundRobin_Import() throws Exception {
			String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
			}.getClass().getEnclosingMethod().getName());
			
			WebDriver driver = fc.commonMethods().browsers().openBrowser();

			try {
				
				driver = fc.loginpage().login(driver);
				
				// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
				// Create CSV File with Lead Details
				String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_RoundRobin.csv";
				System.out.println(filePath);
				List<Lead> leadList = new ArrayList<Lead>();

				for(int j=0 ;j<=5; j++) {
					Lead leadImport = new Lead();
					
					leadImport.setFirstName("Lead_Import"+j);
					leadImport.setLastName("RoundRobin" +fc.utobj().generateRandomNumber());
					leadImport.setEmail("frantest2017@gmail.com");
					leadImport.setBasedonAssignmentRules("yes");
					leadImport.setLeadSourceCategory("Friends");
					leadImport.setLeadSourceDetails("Friends");

					leadList.add(leadImport);
				}

				common.writeCsvFile(filePath, leadList);

				// Import the CSV file
				Import importlead = new Import();
				ImportTest importTest = new ImportTest(driver);

				// Navigate to Import Tab
				fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

				importlead.setImportType("Leads");
				importlead.setSpecifyFileFormat("CSV");
				importlead.setSalesDataFile(filePath);
				importlead.setLeadStatus("New Lead");
				importlead.setLeadSourceCategory("Friends");
				importlead.setLeadSourceDetails("Friends");

				importTest.importLeadsCSV(importlead);

				// Validation
				Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

				Lead x = new Lead();
				List<String> leadsFirstNames = map.get("FN");
				List<String> leadsLastNames = map.get("LN");

				for (int a = 1, b = 1, k = 0; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++, k++) {
					System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
					x.setFirstName(leadsFirstNames.get(a));
					x.setLastName(leadsLastNames.get(b));

					String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
					String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
					System.out.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

					// Validation
					if (k > 2) k = 0;
					if(! corpUsersList.get(k).equalsIgnoreCase(ownerAssignedToLeadx)) {
						fc.utobj().throwsException("Owner Assignment - RoundRobin - Import - FAILED");
					}
					
				}
				
				fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			// fc.utobj().throwsException(e.toString());
		//	System.out.println(testCaseId);
		}
		
	}
		
		/*@Test(dependsOnMethods="Configure_LeadOwnerByRoundRobin" , groups = { "RR_OwnerAssignment" ,  "mytestgroupakshat" , "sales_OwnerAssignment111"})
		@TestCase(createdOn = "2018-04-27", updatedOn = "2018-05-27", testCaseId = "RoundRobin_Assignment_Parsing", testCaseDescription = "Verify Round Robin Owner Assignment through Lead addition from Parsing")
		void RoundRobin_Parsing() throws Exception {
			String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
			}.getClass().getEnclosingMethod().getName());
						
			try {
				
				for(int j=0,k=0 ;j<=5; j++,k++) {
				
					Lead leadParsing = new Lead();
					
					leadParsing.setFirstName("Lead_Parsing"+j);
					leadParsing.setLastName("RoundRobin" +fc.utobj().generateRandomNumber());
					leadParsing.setEmail("frantest2017@gmail.com");
					leadParsing.setBasedonAssignmentRules("yes");
					leadParsing.setLeadSourceCategory("Friends");
					leadParsing.setLeadSourceDetails("Friends");
					
					CodeUtility codeUtility = new CodeUtility();
					codeUtility.sendEmailwithsendGrid("Parsing Lead Mail Subject", "First name: "+ leadParsing.getFirstName() +" , Last name: "+leadParsing.getLastName()+" , Email: "+leadParsing.getEmail()+" , Country: "+leadParsing.getCountry()+" , County: "+leadParsing.getCounty()+" , Zip: "+leadParsing.getZipPostalCode()+" , State: "+leadParsing.getStateProvince()+"" , "uatparsing@franconnect.com", "uatparsing@franconnect.com", "uatparsing@franconnect.com" , "salesautomation@staffex.com", "salesautomation@staffex.com", null);
					
					// Fetch Owner assigned to Lead through Rest API 
					String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadParsing);
					String ownerAssignedToLeadParsing = common.getValFromXML(xmlApi, "leadOwnerID");
					System.out.println("Owner Assigned to Lead "+leadParsing.getLeadFullName()+" >>>>>>>>"+ ownerAssignedToLeadParsing);
					
					// Validation
					if (k > 2) k = 0;
					if(! corpUsersList.get(k).equalsIgnoreCase(ownerAssignedToLeadParsing)) {
						fc.utobj().throwsException("Owner Assignment - RoundRobin - Parsing - FAILED");
					}
				}
			} catch (Exception e) {
				//	fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
					fc.utobj().throwsException(e.toString());
					System.out.println(testCaseId);
				}	
			}*/
		
	
		/*@Test(groups = { "RoundRobin_OwnerAssignment" , "salesRoundRobinThroughAPI" , "sales"})
		@TestCase(createdOn = "2018-05-01", updatedOn = "2018-05-28", testCaseId = "RoundRobin_Assignment_API", testCaseDescription = "Verify RoundRobin Lead Owner Assignment through API")
		void RoundRobin_Api() throws Exception {
			String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
			}.getClass().getEnclosingMethod().getName());

			driver = fc.commonMethods().browsers().openBrowser();

			try {
				driver = fc.loginpage().login(driver);

				fc.utobj().printTestStep("Add a user with name Round Robin1");
				CorporateUser corpUser = new CorporateUser();
				corpUser.setFirstName("Round");
				corpUser.setLastName("Robin1");
				fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
				fc.commonMethods().addCorporateUser(driver, corpUser);

				fc.utobj().printTestStep("Add a user with name Round Robin2");
				CorporateUser corpUser2 = new CorporateUser();
				corpUser2.setFirstName("Round");
				corpUser2.setLastName("Robin2");
				fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser2);
				fc.commonMethods().addCorporateUser(driver, corpUser2);

				fc.utobj().printTestStep("Add a user with name Round Robin3");
				CorporateUser corpUser3 = new CorporateUser();
				corpUser3.setFirstName("Round");
				corpUser3.setLastName("Robin3");
				fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser3);
				fc.commonMethods().addCorporateUser(driver, corpUser3);

				fc.adminpage().adminPage(driver);
				fc.adminpage().openAdminSalesAssignLeadOwners(driver);

				AssignLeadOwnersTest assignLeadOwner = new AssignLeadOwnersTest(driver);
				AssignLeadOwnerbyRoundRobinTest assignment = assignLeadOwner.setAssignLeadOwnerbyRoundRobin();
				assignment.moveAllUserTo_AvailableLeadOwners();
				assignment.moveUserTo_ConfiguredLeadOwners(corpUser.getuserFullName());
				assignment.moveUserTo_ConfiguredLeadOwners(corpUser2.getuserFullName());
				assignment.moveUserTo_ConfiguredLeadOwners(corpUser3.getuserFullName()).updateButton();

				Sales sales = new Sales(driver);

				Lead lead1 = sales.commonMethods().fillDefaultValue_LeadDetails();
				lead1.setBasedonAssignmentRules("yes");
				sales.commonMethods().addLeadThroughWebServices(driver, lead1);
				String response = sales.commonMethods().getLeadDetailsThroughWebServices_MajorFields(driver, lead1);
				if (sales.commonMethods().verifyDatainWebServiceResponse(driver, response,
						corpUser.getuserFullName()) == false) {
					fc.utobj().throwsException("Lead not assigned to the user.");
				}

				Lead lead2 = sales.commonMethods().fillDefaultValue_LeadDetails();
				lead2.setBasedonAssignmentRules("yes");
				sales.commonMethods().addLeadThroughWebServices(driver, lead2);
				response = sales.commonMethods().getLeadDetailsThroughWebServices_MajorFields(driver, lead2);
				if (sales.commonMethods().verifyDatainWebServiceResponse(driver, response,
						corpUser2.getuserFullName()) == false) {
					fc.utobj().throwsException("Lead not assigned to the user.");
				}

				Lead lead3 = sales.commonMethods().fillDefaultValue_LeadDetails();
				lead3.setBasedonAssignmentRules("yes");
				sales.commonMethods().addLeadThroughWebServices(driver, lead3);
				response = sales.commonMethods().getLeadDetailsThroughWebServices_MajorFields(driver, lead3);
				if (sales.commonMethods().verifyDatainWebServiceResponse(driver, response,
						corpUser3.getuserFullName()) == false) {
					fc.utobj().throwsException("Lead not assigned to the user.");
				}

				Lead lead4 = sales.commonMethods().fillDefaultValue_LeadDetails();
				lead4.setBasedonAssignmentRules("yes");
				sales.commonMethods().addLeadThroughWebServices(driver, lead4);
				response = sales.commonMethods().getLeadDetailsThroughWebServices_MajorFields(driver, lead4);
				if (sales.commonMethods().verifyDatainWebServiceResponse(driver, response,
						corpUser.getuserFullName()) == false) {
					fc.utobj().throwsException("Lead not assigned to the user.");
				}

			} catch (Exception e) {
				fc.utobj().throwsException(e.getMessage().toString());
				}
		}*/

	}
