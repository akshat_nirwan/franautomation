package com.builds.test.salesTest;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.builds.test.common.AddDivision;
import com.builds.test.common.ConfigureNewHierarchyLevel;
import com.builds.test.common.ConfigureNewHierarchyLevelTest;
import com.builds.test.common.CorporateUser;
import com.builds.test.common.ManageAreaRegion;
import com.builds.test.common.ManageAreaRegionTest;
import com.builds.test.common.ManageDivisionTest;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class TestCases_SalesTerritoriesAssignment {
	
	WebDriver driver;
	FranconnectUtil fc = new FranconnectUtil();
	Sales_Common_New common = new Sales_Common_New();
	
	ArrayList<String> corpUsersSalesTerritories1 = new ArrayList<String>();
	ArrayList<String> corpUsersSalesTerritories2 = new ArrayList<String>();
	ArrayList<String> corpUsersSalesTerritories3 = new ArrayList<String>();
	ArrayList<String> corpUsersSalesTerritories4 = new ArrayList<String>();
	
	ArrayList<String> corpUsersSalesTerritories1_Source= new ArrayList<String>();
	ArrayList<String> corpUsersSalesTerritories2_Source = new ArrayList<String>();
	ArrayList<String> corpUsersSalesTerritories3_Source = new ArrayList<String>();
	ArrayList<String> corpUsersSalesTerritories4_Source = new ArrayList<String>();
	
	ArrayList<String> corpUsersSalesTerritories1_Division= new ArrayList<String>();
	ArrayList<String> corpUsersSalesTerritories2_Division = new ArrayList<String>();
	ArrayList<String> corpUsersSalesTerritories3_Division = new ArrayList<String>();
	ArrayList<String> corpUsersSalesTerritories4_Division = new ArrayList<String>();
	
	LeadSourceCategory leadSourceCategory1 = new LeadSourceCategory();
	LeadSourceCategory leadSourceCategory2 = new LeadSourceCategory();
	LeadSourceCategory leadSourceCategory3 = new LeadSourceCategory();
	LeadSourceCategory leadSourceCategory4 = new LeadSourceCategory();
	
	AddDivision addDivision1 = new AddDivision();
	AddDivision addDivision2= new AddDivision();
	AddDivision addDivision3 = new AddDivision();
	AddDivision addDivision4 = new AddDivision();

	
	// @BeforeClass(groups={"" , "sales"})
	@Test(groups = { "SalesTerritory_OwnerAssignment" })
	@TestCase(createdOn = "2018-05-04", updatedOn = "2018-05-04", testCaseId = "Configure_SalesTerritoriesAssignment", testCaseDescription = "test case description Add Sales Territories + Enable New Hierarchy Level + Add Division + Assign User By Sales Territory")
	private void Configure_SalesTerritoriesAssignment () throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		driver = fc.commonMethods().browsers().openBrowser();
		
		SetupSalesLeadOwnersTest setupSalesLeadOwnersTest = new SetupSalesLeadOwnersTest(driver);
		
		CorporateUser corpUser = new CorporateUser();
		
		ManageAreaRegionTest manageAreaRegionTest = new ManageAreaRegionTest(driver);
		ManageAreaRegion manageAreaRegion = new ManageAreaRegion();
		
		SalesTerritories salesTerritories1 = new SalesTerritories(); 
		SalesTerritories salesTerritories2 = new SalesTerritories(); 
		SalesTerritories salesTerritories3 = new SalesTerritories(); 
		SalesTerritories salesTerritories4 = new SalesTerritories(); 

		
		SalesTerritoriesTest salesTerritoriesTest = new SalesTerritoriesTest(driver);
		
		ManageDivisionTest manageDivisionTest = new ManageDivisionTest(driver);
		ConfigureNewHierarchyLevelTest configureNewHierarchyLevelTest = new ConfigureNewHierarchyLevelTest(driver);
		ConfigureNewHierarchyLevel configureNewHierarchyLevel = new ConfigureNewHierarchyLevel();
		
		AssignLeadOwners assignLeadOwners = new AssignLeadOwners();
		AssignLeadOwnerbySalesTerritoriesTest assignLeadOwnerbySalesTerritoriesTest = new AssignLeadOwnerbySalesTerritoriesTest(driver);
		AssignLeadOwnersTest assignLeadOwnersTest = new AssignLeadOwnersTest(driver);
		
		LeadSourceCategoryTest leadSourceCategoryTest = new LeadSourceCategoryTest(driver);
		

		try {
			
			driver = fc.loginpage().login(driver);
			
			// ADD Corporate Users for Sales Territory #1
			for (int i = 0; i <= 3; i++) {
				fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
				corpUser.setFirstName("CorpUser" + i);
				corpUser.setLastName("SalesTerritory" + fc.utobj().generateRandomNumber());
				corpUser.setUserName("user" + fc.utobj().generateRandomChar());
				fc.commonMethods().addCorporateUser(driver, corpUser);
				System.out.println(corpUser.getFirstName() + " " + corpUser.getLastName());
				corpUsersSalesTerritories1.add(corpUser.getFirstName() + " " + corpUser.getLastName());
			}
			
			// ADD Corporate Users for Sales Territory #2
			for (int i = 0; i <= 3; i++) {
				fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
				corpUser.setFirstName("CorpUser" + i);
				corpUser.setLastName("SalesTerritory" + fc.utobj().generateRandomNumber());
				corpUser.setUserName("user" + fc.utobj().generateRandomChar());
				fc.commonMethods().addCorporateUser(driver, corpUser);
				System.out.println(corpUser.getFirstName() + " " + corpUser.getLastName());
				corpUsersSalesTerritories2.add(corpUser.getFirstName() + " " + corpUser.getLastName());
			}
			
			// ADD Corporate Users for Sales Territory #3
			for (int i = 0; i <= 3; i++) {
				fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
				corpUser.setFirstName("CorpUser" + i);
				corpUser.setLastName("SalesTerritory" + fc.utobj().generateRandomNumber());
				corpUser.setUserName("user" + fc.utobj().generateRandomChar());
				fc.commonMethods().addCorporateUser(driver, corpUser);
				System.out.println(corpUser.getFirstName() + " " + corpUser.getLastName());
				corpUsersSalesTerritories3.add(corpUser.getFirstName() + " " + corpUser.getLastName());
			}
			
			// ADD Corporate Users for Sales Territory #4
			for (int i = 0; i <= 3; i++) {
				fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
				corpUser.setFirstName("CorpUser" + i);
				corpUser.setLastName("SalesTerritory" + fc.utobj().generateRandomNumber());
				corpUser.setUserName("user" + fc.utobj().generateRandomChar());
				fc.commonMethods().addCorporateUser(driver, corpUser);
				System.out.println(corpUser.getFirstName() + " " + corpUser.getLastName());
				corpUsersSalesTerritories4.add(corpUser.getFirstName() + " " + corpUser.getLastName());
			}
			
			// ADD Corporate Users for Sales Territory #1 - Source
			for (int i = 0; i < 3; i++) {
				fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
				corpUser.setFirstName("CorpUser" + i);
				corpUser.setLastName("SalesTerritory" + fc.utobj().generateRandomNumber());
				corpUser.setUserName("user" + fc.utobj().generateRandomChar());
				fc.commonMethods().addCorporateUser(driver, corpUser);
				System.out.println(corpUser.getFirstName() + " " + corpUser.getLastName());
				corpUsersSalesTerritories1_Source.add(corpUser.getFirstName() + " " + corpUser.getLastName());
			}
			
			// ADD Corporate Users for Sales Territory #2 - Source
						for (int i = 0; i < 3; i++) {
							fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
							corpUser.setFirstName("CorpUser" + i);
							corpUser.setLastName("SalesTerritory" + fc.utobj().generateRandomNumber());
							corpUser.setUserName("user" + fc.utobj().generateRandomChar());
							fc.commonMethods().addCorporateUser(driver, corpUser);
							System.out.println(corpUser.getFirstName() + " " + corpUser.getLastName());
							corpUsersSalesTerritories2_Source.add(corpUser.getFirstName() + " " + corpUser.getLastName());
						}
						
						// ADD Corporate Users for Sales Territory #3 - Source
						for (int i = 0; i < 3; i++) {
							fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
							corpUser.setFirstName("CorpUser" + i);
							corpUser.setLastName("SalesTerritory" + fc.utobj().generateRandomNumber());
							corpUser.setUserName("user" + fc.utobj().generateRandomChar());
							fc.commonMethods().addCorporateUser(driver, corpUser);
							System.out.println(corpUser.getFirstName() + " " + corpUser.getLastName());
							corpUsersSalesTerritories3_Source.add(corpUser.getFirstName() + " " + corpUser.getLastName());
						}
						
						
						// ADD Corporate Users for Sales Territory #4 - Source
						for (int i = 0; i < 3; i++) {
							fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
							corpUser.setFirstName("CorpUser" + i);
							corpUser.setLastName("SalesTerritory" + fc.utobj().generateRandomNumber());
							corpUser.setUserName("user" + fc.utobj().generateRandomChar());
							fc.commonMethods().addCorporateUser(driver, corpUser);
							System.out.println(corpUser.getFirstName() + " " + corpUser.getLastName());
							corpUsersSalesTerritories4_Source.add(corpUser.getFirstName() + " " + corpUser.getLastName());
						}
						
						// ADD Corporate Users for Sales Territory #1 - Division
						for (int i = 0; i < 3; i++) {
							fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
							corpUser.setFirstName("CorpUser" + i);
							corpUser.setLastName("SalesTerritory" + fc.utobj().generateRandomNumber());
							corpUser.setUserName("user" + fc.utobj().generateRandomChar());
							fc.commonMethods().addCorporateUser(driver, corpUser);
							System.out.println(corpUser.getFirstName() + " " + corpUser.getLastName());
							corpUsersSalesTerritories1_Division.add(corpUser.getFirstName() + " " + corpUser.getLastName());
						}
						
						// ADD Corporate Users for Sales Territory #2 - Division
						for (int i = 0; i < 3; i++) {
							fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
							corpUser.setFirstName("CorpUser" + i);
							corpUser.setLastName("SalesTerritory" + fc.utobj().generateRandomNumber());
							corpUser.setUserName("user" + fc.utobj().generateRandomChar());
							fc.commonMethods().addCorporateUser(driver, corpUser);
							System.out.println(corpUser.getFirstName() + " " + corpUser.getLastName());
							corpUsersSalesTerritories2_Division.add(corpUser.getFirstName() + " " + corpUser.getLastName());
						}
						
						// ADD Corporate Users for Sales Territory #3 - Division
						for (int i = 0; i < 3; i++) {
							fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
							corpUser.setFirstName("CorpUser" + i);
							corpUser.setLastName("SalesTerritory" + fc.utobj().generateRandomNumber());
							corpUser.setUserName("user" + fc.utobj().generateRandomChar());
							fc.commonMethods().addCorporateUser(driver, corpUser);
							System.out.println(corpUser.getFirstName() + " " + corpUser.getLastName());
							corpUsersSalesTerritories3_Division.add(corpUser.getFirstName() + " " + corpUser.getLastName());
						}
						
						// ADD Corporate Users for Sales Territory #4 - Division
						for (int i = 0; i < 3; i++) {
							fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
							corpUser.setFirstName("CorpUser" + i);
							corpUser.setLastName("SalesTerritory" + fc.utobj().generateRandomNumber());
							corpUser.setUserName("user" + fc.utobj().generateRandomChar());
							fc.commonMethods().addCorporateUser(driver, corpUser);
							System.out.println(corpUser.getFirstName() + " " + corpUser.getLastName());
							corpUsersSalesTerritories4_Division.add(corpUser.getFirstName() + " " + corpUser.getLastName());
						}
			
			ArrayList<String> SalesTerritories = new ArrayList<String>();
			
			// ADD Sales Territory #1
			common.setGroupByCounty_Domestic_filladdNewSalesTerritory(salesTerritories1);
			fc.commonMethods().getModules().openAdminPage(driver).openSalesTerritoryPage(driver);
			salesTerritoriesTest.addNewSalesTerritory(salesTerritories1);
			SalesTerritories.add(salesTerritories1.getSalesTerritoryName()); 
			
			// ADD Sales Territory #2
			common.setGroupByStates_Domestic_filladdNewSalesTerritory(salesTerritories2);
			fc.commonMethods().getModules().openAdminPage(driver).openSalesTerritoryPage(driver);
			salesTerritoriesTest.addNewSalesTerritory(salesTerritories2);
			SalesTerritories.add(salesTerritories2.getSalesTerritoryName()); 

			// ADD Sales Territory #3
			common.setGroupByStates_International_filladdNewSalesTerritory(salesTerritories3);
			fc.commonMethods().getModules().openAdminPage(driver).openSalesTerritoryPage(driver);
			salesTerritoriesTest.addNewSalesTerritory(salesTerritories3);
			SalesTerritories.add(salesTerritories3.getSalesTerritoryName()); 
			
			// ADD Sales Territory #4
			common.setGroupByZipPostalCode_Domestic_filladdNewSalesTerritory(salesTerritories4);
			fc.commonMethods().getModules().openAdminPage(driver).openSalesTerritoryPage(driver);
			salesTerritoriesTest.addNewSalesTerritory(salesTerritories4);
			SalesTerritories.add(salesTerritories4.getSalesTerritoryName()); 
			
			// Turn ON Division
			configureNewHierarchyLevel.setEnableNewHierarchyLevel("Yes");
			fc.commonMethods().getModules().openAdminPage(driver).openConfigureNewHierarchyLevel(driver);
			configureNewHierarchyLevelTest.EnableNewHierarchyLevel(configureNewHierarchyLevel);
			
			// Add Division #1
			common.set_AddDivision(addDivision1);
			fc.commonMethods().getModules().openAdminPage(driver).openAddDivision(driver);
			manageDivisionTest.addDivision(addDivision1);
			
			// Add Division #2
			common.set_AddDivision(addDivision2);
			fc.commonMethods().getModules().openAdminPage(driver).openAddDivision(driver);
			manageDivisionTest.addDivision(addDivision2);
						
			// Add Division #3
			common.set_AddDivision(addDivision3);
			fc.commonMethods().getModules().openAdminPage(driver).openAddDivision(driver);
			manageDivisionTest.addDivision(addDivision3);
			
			// Add Division #4
			common.set_AddDivision(addDivision4);
			fc.commonMethods().getModules().openAdminPage(driver).openAddDivision(driver);
			manageDivisionTest.addDivision(addDivision4);
			
			// Adding Lead Source Category #1
			fc.commonMethods().getModules().openAdminPage(driver).openFranchiseSourcePage(driver);
			leadSourceCategory1.setLeadSourceCategory("Lead Source " + fc.utobj().generateRandomNumber());
			leadSourceCategory1.setIncludeInWebPage("no");
			leadSourceCategoryTest.addLeadSourceCategory(leadSourceCategory1).verify_addLeadSourceCategory(leadSourceCategory1);
			
			// Adding Lead Source Category #2
			fc.commonMethods().getModules().openAdminPage(driver).openFranchiseSourcePage(driver);
			leadSourceCategory2.setLeadSourceCategory("Lead Source " + fc.utobj().generateRandomNumber());
			leadSourceCategory2.setIncludeInWebPage("no");
			leadSourceCategoryTest.addLeadSourceCategory(leadSourceCategory2).verify_addLeadSourceCategory(leadSourceCategory2);
			
			// Adding Lead Source Category #3
			fc.commonMethods().getModules().openAdminPage(driver).openFranchiseSourcePage(driver);
			leadSourceCategory3.setLeadSourceCategory("Lead Source " + fc.utobj().generateRandomNumber());
			leadSourceCategory3.setIncludeInWebPage("no");
			leadSourceCategoryTest.addLeadSourceCategory(leadSourceCategory3).verify_addLeadSourceCategory(leadSourceCategory3);
			
			// Adding Lead Source Category #4
			fc.commonMethods().getModules().openAdminPage(driver).openFranchiseSourcePage(driver);
			leadSourceCategory4.setLeadSourceCategory("Lead Source " + fc.utobj().generateRandomNumber());
			leadSourceCategory4.setIncludeInWebPage("no");
			leadSourceCategoryTest.addLeadSourceCategory(leadSourceCategory4).verify_addLeadSourceCategory(leadSourceCategory4);
			
			// Setup Sales Lead Owners as ALL
			fc.commonMethods().getModules().openAdminPage(driver).openSetupFranchiseSalesLeadOwnersPage(driver);
			setupSalesLeadOwnersTest.setAllusersAsSalesLeadOwners_AndSubmit();
			
			// Assign Lead Owner by Sales Territories
			
			fc.commonMethods().getModules().openAdminPage(driver).openAssignLeadOwnersPage(driver);
			assignLeadOwnersTest.set_defaultOwner("FranConnect Administrator");
			assignLeadOwnersTest.set_AssignLeadOwnerbySalesTerritories().configure_AssignLeadOwnerbySalesTerritories(corpUsersSalesTerritories1 , salesTerritories1).configure_AssignLeadOwnerbySalesTerritories(corpUsersSalesTerritories2, salesTerritories2).configure_AssignLeadOwnerbySalesTerritories(corpUsersSalesTerritories3, salesTerritories3).configure_AssignLeadOwnerbySalesTerritories(corpUsersSalesTerritories4, salesTerritories4).AssociateDifferentOwnerFor_Division(corpUsersSalesTerritories1_Division, addDivision1.getDivisionName(), salesTerritories1.getSalesTerritoryName()).AssociateDifferentOwnerFor_Division(corpUsersSalesTerritories2_Division, addDivision2.getDivisionName(), salesTerritories2.getSalesTerritoryName()).AssociateDifferentOwnerFor_Division(corpUsersSalesTerritories3_Division, addDivision3.getDivisionName(), salesTerritories3.getSalesTerritoryName()).AssociateDifferentOwnerFor_Division(corpUsersSalesTerritories4_Division, addDivision4.getDivisionName(), salesTerritories4.getSalesTerritoryName()).AssociateDifferentOwnerFor_Source(corpUsersSalesTerritories1_Source, leadSourceCategory1.getLeadSourceCategory()	, salesTerritories1.getSalesTerritoryName()).AssociateDifferentOwnerFor_Source(corpUsersSalesTerritories2_Source, leadSourceCategory2.getLeadSourceCategory(), salesTerritories2.getSalesTerritoryName()).AssociateDifferentOwnerFor_Source(corpUsersSalesTerritories3_Source, leadSourceCategory3.getLeadSourceCategory(), salesTerritories3.getSalesTerritoryName()).AssociateDifferentOwnerFor_Source(corpUsersSalesTerritories4_Source, leadSourceCategory4.getLeadSourceCategory(), salesTerritories4.getSalesTerritoryName());
			
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}
	
	@Test(groups = { "SalesTerritory_OwnerAssignment" })
	@TestCase(createdOn = "2018-07-04", updatedOn = "2018-07-04", testCaseId = "SalesTerritoriesAssignment_01", testCaseDescription = "To verify Assign Lead Owners Based on Assignment Rules Assign Lead Owner by Sales Territories : API")
	private void SalesTerritoriesAssignment_01() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			
			// Through API
						for (int j = 0; j <= 3; j++) {
							// Add Lead
							Lead lead = new Lead();
							lead.setFirstName("SalesTerritory" +j);
							lead.setLastName("Lead" +fc.utobj().generateRandomNumber());
							lead.setEmail("frantest2017@gmail.com");
							lead.setCountry("USA");
							lead.setStateProvince("Florida");
							lead.setCounty("Gulf");
							lead.setBasedonAssignmentRules("yes");
							lead.setLeadSourceCategory("Friends");
							lead.setLeadSourceDetails("Friends");
							common.addLeadThroughWebServices(driver, lead);

							// Fetch Owner assigned to Lead through Rest API
							String xmlApi = common.getLeadDetailsThroughWebServices_MajorFields(driver, lead);
							// System.out.println(xmlApi);
							String ownerAssignedToLeadApi = common.getValFromXML(xmlApi, "leadOwnerID");
							System.out.println("Owner Assigned to Lead >>>>>>>>" + lead.getLeadFullName() + ">>>" + ownerAssignedToLeadApi);

							// Validation
							if (!corpUsersSalesTerritories1.contains(ownerAssignedToLeadApi)) {
								fc.utobj().throwsException("Assign Lead Owner By SalesTerritories - Api - FAILED");
							}
						}
						

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	
	@Test(groups = { "SalesTerritory_OwnerAssignment" })
	@TestCase(createdOn = "2018-07-04", updatedOn = "2018-07-04", testCaseId = "SalesTerritoriesAssignment_02", testCaseDescription = "To verify Assign Lead Owners Based on Assignment Rules Assign Lead Owner by Sales Territories : WebForm")
	private void SalesTerritoriesAssignment_02() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);

			// Through WebForm ----------------------------------------------------------------------------------------------------------------------------------------------------------
						// Create WebForm 
						ManageWebformGeneratorTest manageWebformGeneratorTest = new ManageWebformGeneratorTest(driver);
						ManageWebFormGenerator manageWebFormGenerator = new ManageWebFormGenerator();

						fc.commonMethods().getModules().openAdminPage(driver).openManageWebFormGeneratorPage(driver);
						common.set_ManageWebform(manageWebFormGenerator);
						manageWebFormGenerator.setLeadSourceCategory(leadSourceCategory1.getLeadSourceCategory());
						manageWebFormGenerator.setLeadSourceDetails("None");
						manageWebFormGenerator.setDivision(addDivision1.getDivisionName());
						manageWebformGeneratorTest.createWebForm(manageWebFormGenerator);

						// Add Lead - WebForm
						for (int j = 0; j <= 0; j++) {
							Lead leadWebForm = new Lead();
							leadWebForm.setFirstName("SalesTerritories" +j);
							leadWebForm.setLastName("WebForm" + fc.utobj().generateRandomNumber());
							leadWebForm.setEmail("frantest2017@gmail.com");
							// leadWebForm.setDivision(addDivision1.getDivisionName());
							leadWebForm.setCountry("USA");
							leadWebForm.setStateProvince("Florida");
							leadWebForm.setCounty("Gulf");
							leadWebForm.setZipPostalCode("5464685"); // unmatched
							// leadWebForm.setBasedonAssignmentRules("yes");
							leadWebForm.setLeadSourceCategory("Friends"); // unmatched
							leadWebForm.setLeadSourceDetails("Friends");

							// Add Lead through WebForm
							manageWebformGeneratorTest.launchAndTest_Webform(manageWebFormGenerator, leadWebForm); 

							// fc.utobj().sleep();
							String xmlWebForm1 = common.getLeadDetailsThroughWebServices_MajorFields(driver, leadWebForm);
							String ownerAssignedTowebformLead = common.getValFromXML(xmlWebForm1, "leadOwnerID");
							System.out.println("Owner Assigned to Lead " + leadWebForm.getLeadFullName() + " >>>>>>>>" + ownerAssignedTowebformLead);
							// Validation
							if (!ownerAssignedTowebformLead.equalsIgnoreCase("FranConnect Administrator")) {
								fc.utobj().throwsException("Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - WebForm - FAILED");
							}
						}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(groups = { "SalesTerritory_OwnerAssignment" })
	@TestCase(createdOn = "2018-07-04", updatedOn = "2018-07-04", testCaseId = "SalesTerritoriesAssignment_03", testCaseDescription = "To verify Assign Lead Owners Based on Assignment Rules Assign Lead Owner by Sales Territories : Import")
	private void SalesTerritoriesAssignment_03() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);

			// Through Sales > IMPORT -------------------------------------------------------------------------------------------------------------------------------------------------------
						// Create CSV File with Lead Details
						String filePath = FranconnectUtil.config.get("testLogFolderPath")+ "/ImportLead_33959.csv";
						System.out.println(filePath);
						List<Lead> leadList = new ArrayList<Lead>();

						for (int j = 0; j <= 2; j++) {
							Lead leadImport = new Lead();
							leadImport.setFirstName("LeadSource_Api_33959");
							leadImport.setLastName("PriorityST" + fc.utobj().generateRandomNumber());
							leadImport.setEmail("frantest2017@gmail.com");
							leadImport.setDivision(addDivision1.getDivisionName());
							leadImport.setCountry("USA");
							leadImport.setStateProvince("Florida");
							leadImport.setCounty("Gulf");
							leadImport.setZipPostalCode("5464685"); // unmatched
							leadImport.setBasedonAssignmentRules("yes");
							leadImport.setLeadSourceCategory("Friends"); // unmatched
							leadImport.setLeadSourceDetails("Friends");

							leadList.add(leadImport);
						}

						common.writeCsvFile(filePath, leadList);

						// Import the CSV file
						Import importlead = new Import();
						ImportTest importTest = new ImportTest(driver);

						// Navigate to Import Tab
						fc.commonMethods().getModules().sales().sales_common().fsModule(driver).importPage(driver);

						importlead.setImportType("Leads");
						importlead.setSpecifyFileFormat("CSV");
						importlead.setSalesDataFile(filePath);
						importlead.setLeadStatus("New Lead");
						importlead.setLeadSourceCategory("Friends");
						importlead.setLeadSourceDetails("Friends");

						importTest.importLeadsCSV(importlead);

						// Validation
						Map<String, List<String>> map = common.getFullNameOfLeadsFromCSV(filePath);

						Lead x = new Lead();
						List<String> leadsFirstNames = map.get("FN");
						List<String> leadsLastNames = map.get("LN");

						for (int a = 1, b = 1; a < leadsFirstNames.size() & b < leadsLastNames.size(); a++, b++) {
							System.out.println(leadsFirstNames.get(a) + " " + leadsLastNames.get(b));
							x.setFirstName(leadsFirstNames.get(a));
							x.setLastName(leadsLastNames.get(b));

							String xmlx = common.getLeadDetailsThroughWebServices_MajorFields(driver, x);
							String ownerAssignedToLeadx = common.getValFromXML(xmlx, "leadOwnerID");
							System.out
									.println("Owner Assigned to Lead " + x.getLeadFullName() + " >>>>>>>>" + ownerAssignedToLeadx);

							// Validation
							if (!ownerAssignedToLeadx.equalsIgnoreCase("FranConnect Administrator")) {
								fc.utobj().throwsException(
										"Owner Assignment by LeadSource - to Default Owner(FranConnect Administrator) - WebForm - FAILED");
							}
						}
						
						
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	


}

