package com.builds.test.salesTest;

import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.builds.test.admin.Roles;
import com.builds.test.admin.RolesTest;
import com.builds.test.common.AddDivision;
import com.builds.test.common.CommonMethods;
import com.builds.test.common.ConfigureNewHierarchyLevel;
import com.builds.test.common.ConfigureNewHierarchyLevelTest;
import com.builds.test.common.CorporateUser;
import com.builds.test.common.DivisionalUser;
import com.builds.test.common.LoginPageTest;
import com.builds.test.common.ManageAreaRegion;
import com.builds.test.common.ManageAreaRegionTest;
import com.builds.test.common.ManageDivisionTest;
import com.builds.test.common.RegionalUser;
import com.builds.test.common.SalesModule;
import com.builds.uimaps.fs.CreateTemplateUI;
import com.builds.uimaps.fs.LeadSummaryUI;
import com.builds.uimaps.fs.SalesUI;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class TestCases_UserAccessibility {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(retryAnalyzer = Retry.class , groups = { "User_Accessibility" , "User_Accessibility_01" })
	@TestCase(createdOn = "2018-07-17", updatedOn = "2018-07-17", testCaseId = "User_Accessibility_01", testCaseDescription = "To test the accessibility of private group by user")
	void userAccessibility_01() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			
			driver = fc.loginpage().login(driver);
			
			CorporateUser corporateUser = new CorporateUser();
			fc.commonMethods().fillDefaultDataFor_CorporateUser(corporateUser);
			corporateUser.setFirstName("Corp" + fc.utobj().generateRandomNumber());
			corporateUser.setLastName("User" + fc.utobj().generateRandomNumber());
			fc.commonMethods().addCorporateUser(driver, corporateUser);
									
			fc.home_page().logout(driver);
	
			LoginPageTest loginPageTest = new LoginPageTest();
			loginPageTest.loginWithUserNameAndPassword(driver, corporateUser.getUserName(), corporateUser.getPassword());
			
			
			fc.commonMethods().getModules().clickSalesModule(driver); // click on Sales
			
			Sales salesModule = new Sales(driver);
			salesModule.clickGroups();	// click on Groups
			
			GroupTest groupTest = new GroupTest();
			
			//  Create a private group
			Group group = new Group();
			groupTest.clickCreateGroupBtnAndSwitchToFrame(driver);
			group.setName("Test Private Group to "+corporateUser.getFirstName());
			group.setDescription("Group Description" +corporateUser.getLastName());
			group.setVisibility("Private");
			groupTest.fillGroupDetails(driver, group);
			groupTest.clickCreateBtn(driver);
			
			fc.utobj().switchFrameToDefault(driver);
			
			groupTest.verify_addedGroup(driver, group.getName());
			
			// Create a public group
			Group group2 = new Group();
			groupTest.clickCreateGroupBtnAndSwitchToFrame(driver);
			group2.setName("Test Public Group to "+corporateUser.getFirstName());
			group2.setDescription("Group Description" +corporateUser.getLastName());
			group2.setVisibility("Public");
			groupTest.fillGroupDetails(driver, group2);
			groupTest.clickCreateBtn(driver);
			
			fc.utobj().switchFrameToDefault(driver);
			
			groupTest.verify_addedGroup(driver, group2.getName());
			
			fc.home_page().logout(driver);
			
			CorporateUser corporateUser2 = new CorporateUser();
			fc.commonMethods().fillDefaultDataFor_CorporateUser(corporateUser2);
			corporateUser2.setFirstName("Corp" + fc.utobj().generateRandomNumber());
			corporateUser2.setLastName("User" + fc.utobj().generateRandomNumber());
			fc.commonMethods().addCorporateUser(driver, corporateUser2);
			
			loginPageTest.loginWithUserNameAndPassword(driver, corporateUser2.getUserName(), corporateUser2.getPassword());
			fc.commonMethods().getModules().clickSalesModule(driver);
			salesModule.clickGroups();
			
			// Validation
			if(fc.utobj().assertLinkText(driver, group.getName()))
			{
				fc.utobj().throwsException("Private group of Corp User is visible to some other Corp User - FAILED");
			}
			
			// Validation
			if(! fc.utobj().assertLinkText(driver, group2.getName()))
			{
				fc.utobj().throwsException("Public group of Corp User not visible to another Corp User - FAILED");
			}
			
			groupTest.clickCreateGroupBtnAndSwitchToFrame(driver);

			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			
			
						
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(retryAnalyzer = Retry.class , groups = { "User_Accessibility" , "User_Accessibility_02" })
	@TestCase(createdOn = "2018-07-17", updatedOn = "2018-07-17", testCaseId = "User_Accessibility_02", testCaseDescription = "To test the accessibility of reports by regional user")
	void userAccessibility_02() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			
			driver = fc.loginpage().login(driver);
			
			fc.commonMethods().getModules().openAdminPage(driver).openManageAreaRegion(driver);

			ManageAreaRegionTest manageAreaRegionTest = new ManageAreaRegionTest(driver);
			ManageAreaRegion manageAreaRegion = new ManageAreaRegion();
			
			manageAreaRegion.setAreaRegionName("AreaRegion1954");
			manageAreaRegion.setCategory("International");
			manageAreaRegion.setGroupBy("States");
			manageAreaRegion.getCountries().add("France");
			manageAreaRegion.getCountries().add("Australia");
			manageAreaRegion.getStates().add("Victoria");
			manageAreaRegion.getStates().add("South Australia");
			manageAreaRegion.getStates().add("Western Australia");
			manageAreaRegion.getStates().add("France");
			manageAreaRegion.getStates().add("Centre");			
			
			if(! (fc.utobj().assertLinkText(driver, manageAreaRegion.getAreaRegionName())))
			{
				manageAreaRegionTest.addAreaRegion(manageAreaRegion);
			}
			
			RegionalUser regionalUser = new RegionalUser();
			fc.commonMethods().fillDefaultDataFor_RegionalUser(regionalUser);
			regionalUser.setFirstName("Reg" + fc.utobj().generateRandomNumber());
			regionalUser.setLastName("User" + fc.utobj().generateRandomNumber());
			regionalUser.setUserName("regUser" + fc.utobj().generateRandomChar());
			regionalUser.setArea_Region(manageAreaRegion.getAreaRegionName());
			fc.commonMethods().addRegionalUser(driver, regionalUser);
			
			fc.home_page().logout(driver);

			LoginPageTest loginPageTest = new LoginPageTest();
			loginPageTest.loginWithUserNameAndPassword(driver, regionalUser.getUserName(), regionalUser.getPassword());
			
			Sales_Common_New common = new Sales_Common_New();
			int leadCount = 0;
			// Add leads through API
			for(int i=0; i<10 ; i++)
			{
			Lead lead1 = new Lead();
						lead1.setFirstName("RegionalOwner");
						lead1.setLastName("Lead" +fc.utobj().generateRandomNumber());
						lead1.setEmail("frantest2017@gmail.com");
						lead1.setLeadOwner(regionalUser.getuserFullName());
						lead1.setLeadSourceCategory("Friends");
						lead1.setLeadSourceDetails("Friends");
						common.addLeadThroughWebServices(null, lead1);

						// Fetch Owner assigned to Lead through Rest API
						String xml1 = common.getLeadDetailsThroughWebServices_MajorFields(null, lead1);
						String ownerAssignedToLead1 = common.getValFromXML(xml1, "leadOwnerID");
						System.out.println("Owner Assigned to Lead >>>>>>>>" + lead1.getLeadFullName() + ">>>" + ownerAssignedToLead1);
						leadCount ++;
			}
			
			fc.commonMethods().getModules().clickSalesModule(driver); // click on Sales
			
			Sales salesModule = new Sales(driver);
			salesModule.clickReports();
			
			ReportsTest reportsTest = new ReportsTest(driver);
			reportsTest.clickOn_LeadStatusReport();
			LeadStatusReportTest leadStatusReportTest = new LeadStatusReportTest(driver);
			ArrayList<String> regionalUserNames = new ArrayList<String>();
			regionalUserNames.add(regionalUser.getuserFullName());
			leadStatusReportTest.select_ViewLeadsBelongingTo_Filter(regionalUserNames);
			
			fc.utobj().switchFrameById(driver, "reportiframe");
			WebElement newLeadCountElement = fc.utobj().getElementByXpath(driver, ".//table[contains(@class,'summaryTbl')]//tr[@align='center']/td[contains(text(),'New Lead')]/ancestor::tr[contains(@class,'bText12')]/td[@align]/a");
			fc.utobj().moveToElement(driver, newLeadCountElement);
			String newLeadCount = newLeadCountElement.getText();
			System.out.println(newLeadCount);
			
			// Validation
			if(! (leadCount == (Integer.parseInt(newLeadCount))))
			{
				fc.utobj().throwsException("Lead count mismatch in report - FAILED");
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(retryAnalyzer = Retry.class , groups = { "User_Accessibility" , "User_Accessibility_03" })
	@TestCase(createdOn = "2018-07-17", updatedOn = "2018-07-17", testCaseId = "User_Accessibility_03", testCaseDescription = "To test the accessibility of private campaign by user")
	void userAccessibility_03() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);

			fc.commonMethods().getModules().openAdminPage(driver).openManageAreaRegion(driver);

			// Create Area Region
			ManageAreaRegionTest manageAreaRegionTest = new ManageAreaRegionTest(driver);
			ManageAreaRegion manageAreaRegion = new ManageAreaRegion();
			manageAreaRegion.setAreaRegionName("AreaRegion1954");
			manageAreaRegion.setCategory("International");
			manageAreaRegion.setGroupBy("States");
			manageAreaRegion.getCountries().add("France");
			manageAreaRegion.getCountries().add("Australia");
			manageAreaRegion.getStates().add("Victoria");
			manageAreaRegion.getStates().add("South Australia");
			manageAreaRegion.getStates().add("Western Australia");
			manageAreaRegion.getStates().add("France");
			manageAreaRegion.getStates().add("Centre");

			// Only create Area Region if it does not exist
			if (!(fc.utobj().assertLinkText(driver, manageAreaRegion.getAreaRegionName()))) {
				manageAreaRegionTest.addAreaRegion(manageAreaRegion);
			}

			// Create Corporate User through API
			CorporateUser corpUser = new CorporateUser();
			fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setFirstName("CorpUser");
			corpUser.setLastName("PrivateCampaign" + fc.utobj().generateRandomNumber());
			corpUser.setUserName("user" + fc.utobj().generateRandomChar());
			fc.commonMethods().addCorporateUser(driver, corpUser);

			// Create Regional User
			RegionalUser regionalUser = new RegionalUser();
			fc.commonMethods().fillDefaultDataFor_RegionalUser(regionalUser);
			regionalUser.setFirstName("Reg" + fc.utobj().generateRandomNumber());
			regionalUser.setLastName("User" + fc.utobj().generateRandomNumber());
			regionalUser.setUserName("regUser" + fc.utobj().generateRandomChar());
			regionalUser.setArea_Region(manageAreaRegion.getAreaRegionName());
			fc.commonMethods().addRegionalUser(driver, regionalUser);

			// LogOut
			fc.home_page().logout(driver);

			// Login with created Regional User
			LoginPageTest loginPageTest = new LoginPageTest();
			loginPageTest.loginWithUserNameAndPassword(driver, regionalUser.getUserName(), regionalUser.getPassword());

			fc.commonMethods().getModules().clickSalesModule(driver);

			Sales salesModule = new Sales(driver);
			salesModule.clickCampaignCenter();

			Campaign campaign = new Campaign();
			campaign.setCampaignName("Campaign" + fc.utobj().generateRandomNumber());
			campaign.setDescription("Description" + fc.utobj().generateRandomChar());
			campaign.setAccessibility("Private");
			CampaignCenterTest campaignCenterTest = new CampaignCenterTest(driver);
			campaignCenterTest.createCampaign(campaign);

			CreateTemplateTest createTemplateTest = new CreateTemplateTest(driver);
			createTemplateTest.clickCodeYourOwn();

			Template template = new Template();
			template.setTemplateName("testCampaignTemplate " + fc.utobj().generateRandomNumber());
			template.setEmailSubject("templateSubject " + fc.utobj().generateRandomNumber());
			template.setAccessibility("Private");
			template.setEmailType("Graphical");
			template.setBody("Campaign Body");
			createTemplateTest.fillTemplateDetails(driver, template);
			createTemplateTest.click_SaveAndContinue();
			CampaignCenter_SelectRecipientsTest campaignCenter_SelectRecipientsTest = new CampaignCenter_SelectRecipientsTest(
					driver);
			campaignCenter_SelectRecipientsTest.click_ContinueBtn();
			fc.utobj().acceptAlertBox(driver);

			salesModule.clickCampaignCenter();
			campaignCenterTest.click_manageCampaigns();

			// Validation - Created private Campaign should be visible
			if (!(fc.utobj().assertLinkText(driver, campaign.getCampaignName()))) {
				fc.utobj().throwsException("Created private template not visible to the regional user - FAILED");
			}

			// LogOut
			fc.home_page().logout(driver);

			// Login with created Corporate User
			loginPageTest.loginWithUserNameAndPassword(driver, corpUser.getUserName(), corpUser.getPassword());

			fc.commonMethods().getModules().clickSalesModule(driver);
			salesModule.clickCampaignCenter();

			Campaign campaign2 = new Campaign();
			campaign2.setCampaignName("Campaign" + fc.utobj().generateRandomNumber());
			campaign2.setDescription("Description" + fc.utobj().generateRandomChar());
			campaign2.setAccessibility("Private");
			campaignCenterTest.createCampaign(campaign2);

			createTemplateTest.clickCodeYourOwn();

			Template template2 = new Template();
			template2.setTemplateName("testCampaignTemplate " + fc.utobj().generateRandomNumber());
			template2.setEmailSubject("templateSubject " + fc.utobj().generateRandomNumber());
			template2.setAccessibility("Private");
			template2.setEmailType("Graphical");
			template2.setBody("Campaign Body");
			createTemplateTest.fillTemplateDetails(driver, template2);
			createTemplateTest.click_SaveAndContinue();
			campaignCenter_SelectRecipientsTest.click_ContinueBtn();
			fc.utobj().acceptAlertBox(driver);

			salesModule.clickCampaignCenter();
			campaignCenterTest.click_manageCampaigns();

			// campaignCenterTest.open_ManageCampaignsFromTopLeftCornerMenu();

			// Validation - Created private campaign should not be visible to
			// any other user
			if (fc.utobj().assertLinkText(driver, campaign.getCampaignName())) {
				fc.utobj().throwsException(
						"Created private template of some regional user is visible to other corp user - FAILED");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test(retryAnalyzer = Retry.class , groups = { "User_Accessibility", "User_Accessibility_04" })
	@TestCase(createdOn = "2018-07-17", updatedOn = "2018-07-17", testCaseId = "User_Accessibility_04", testCaseDescription = "To test the accessibility of leads by divisional user")
	void userAccessibility_04() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			
			driver = fc.loginpage().login(driver);
			
			// Turn ON Division
			ConfigureNewHierarchyLevelTest configureNewHierarchyLevelTest = new ConfigureNewHierarchyLevelTest(driver);
			ConfigureNewHierarchyLevel configureNewHierarchyLevel = new ConfigureNewHierarchyLevel();
			configureNewHierarchyLevel.setEnableNewHierarchyLevel("Yes");
			configureNewHierarchyLevel.setLeadsExclusiveToNewHierarchyLevel("Yes");
			fc.commonMethods().getModules().openAdminPage(driver).openConfigureNewHierarchyLevel(driver);
			configureNewHierarchyLevelTest.EnableNewHierarchyLevel(configureNewHierarchyLevel);
		
			// Add Division #1
			AddDivision addDivision1 = new AddDivision();
			ManageDivisionTest manageDivisionTest = new ManageDivisionTest(driver);
			addDivision1.setDivisionName("Division" +fc.utobj().generateRandomNumber());
			fc.commonMethods().getModules().openAdminPage(driver).openAddDivision(driver);
			manageDivisionTest.addDivision(addDivision1);
			
			// Add Division #2
			AddDivision addDivision2 = new AddDivision();
			addDivision2.setDivisionName("Division_2_" +fc.utobj().generateRandomNumber());
			fc.commonMethods().getModules().openAdminPage(driver).openAddDivision(driver);
			manageDivisionTest.addDivision(addDivision2);
			
			// Setup Sales Lead Owners - ALL
			SetupSalesLeadOwnersTest setupSalesLeadOwnersTest = new SetupSalesLeadOwnersTest(driver);
			fc.commonMethods().getModules().openAdminPage(driver).openSetupFranchiseSalesLeadOwnersPage(driver);
			setupSalesLeadOwnersTest.setAllusersAsSalesLeadOwners_AndSubmit();
			
			// Add Divisional User - Rest Api
			DivisionalUser divisionalUser = new DivisionalUser();
			fc.commonMethods().fillDefaultDataFor_DivisionalUser(divisionalUser);
			divisionalUser.setFirstName("Divisional_1");
			divisionalUser.setLastName("User" +fc.utobj().generateRandomNumber());
			divisionalUser.setDivision(addDivision1.getDivisionName());
			fc.commonMethods().addDivisionalUser(driver, divisionalUser);
			
			// Add Divisional User - Rest Api
			DivisionalUser divisionalUser1 = new DivisionalUser();
			fc.commonMethods().fillDefaultDataFor_DivisionalUser(divisionalUser1);
			divisionalUser1.setFirstName("Divisional_2");
			divisionalUser1.setLastName("User" +fc.utobj().generateRandomNumber());
			divisionalUser1.setDivision(addDivision2.getDivisionName());
			fc.commonMethods().addDivisionalUser(driver, divisionalUser1);
			
			// Set -> Assign Lead Owner by Division
			AssignLeadOwnersTest assignLeadOwnersTest = new AssignLeadOwnersTest(driver);
			fc.commonMethods().getModules().openAdminPage(driver).openAssignLeadOwnersPage(driver);
			assignLeadOwnersTest.set_defaultOwner("FranConnect Administrator");
			ArrayList <String> divUsers = new ArrayList<String>();
			divUsers.add(divisionalUser.getuserFullName());
			ArrayList<String> divUsers2 = new ArrayList<String>();
			divUsers2.add(divisionalUser1.getuserFullName());
			assignLeadOwnersTest.set_assignLeadOwnerbyDivision().Configure_AssignLeadOwnerbyDivision(addDivision1.getDivisionName(), divUsers).Configure_AssignLeadOwnerbyDivision(addDivision2.getDivisionName(), divUsers2);

			// Adding 2 leads - Assigning to above created divisional user
			Sales_Common_New sales_Common = new Sales_Common_New();
			Lead lead1 = new Lead();
			lead1.setFirstName("Lead1_UserAccesibility");
			lead1.setLastName(fc.utobj().generateRandomNumber());
			lead1.setEmail("frantest2017@gmail.com");
			lead1.setLeadOwner(divisionalUser.getuserFullName());
			lead1.setLeadSourceCategory("Friends");
			lead1.setLeadSourceDetails("Friends");
			sales_Common.addLeadThroughWebServices(null, lead1);
			
			Lead lead2 = new Lead();
			lead2.setFirstName("Lead2_UserAccesibility");
			lead2.setLastName(fc.utobj().generateRandomNumber());
			lead2.setEmail("frantest2017@gmail.com");
			lead2.setLeadOwner(divisionalUser.getuserFullName());
			lead2.setLeadSourceCategory("Friends");
			lead2.setLeadSourceDetails("Friends");
			sales_Common.addLeadThroughWebServices(null, lead2);
			
			// Add lead - with matching division
			Lead lead3 = new Lead();
			lead3.setFirstName("Lead3_UserAccesibility");
			lead3.setLastName("Division1_"+fc.utobj().generateRandomNumber());
			lead3.setEmail("frantest2017@gmail.com");
			lead3.setBasedonAssignmentRules("yes");	
			lead3.setDivision(addDivision1.getDivisionName());
			lead3.setLeadSourceCategory("Friends");
			lead3.setLeadSourceDetails("Friends");
			sales_Common.addLeadThroughWebServices(null, lead3);
			
			// Add lead - with non matching division
			Lead lead4 = new Lead();
			lead4.setFirstName("Lead4_UserAccesibility");
			lead4.setLastName("Division2_"+fc.utobj().generateRandomNumber());
			lead4.setEmail("frantest2017@gmail.com");
			lead4.setBasedonAssignmentRules("yes");	
			lead4.setDivision(addDivision2.getDivisionName());
			lead4.setLeadSourceCategory("Friends");
			lead4.setLeadSourceDetails("Friends");
			sales_Common.addLeadThroughWebServices(null, lead4);

			// LogOut
			fc.home_page().logout(driver);
			
			// Login with created Divisional User
			LoginPageTest loginPageTest = new LoginPageTest();
			loginPageTest.loginWithUserNameAndPassword(driver, divisionalUser.getUserName(), divisionalUser.getPassword());
			
			// Click on Sales
			fc.commonMethods().getModules().clickSalesModule(driver); 
			
			// Click on Lead Management
			Sales salesModule = new Sales(driver);
			salesModule.clickLeadManagement();
			
			// Validation - Divisional user should be able to see leads assigned to him
			if(! (fc.utobj().assertLinkText(driver, lead1.getLeadFullName())))
			{
				fc.utobj().throwsException("Assigned Lead not visible to the User - FAILED");
			}
			if(! (fc.utobj().assertLinkText(driver, lead2.getLeadFullName())))
			{
				fc.utobj().throwsException("Assigned Lead not visible to the User - FAILED");
			}
			if(! (fc.utobj().assertLinkText(driver, lead3.getLeadFullName())))
			{
				fc.utobj().throwsException(" Lead with same division not visible to the User - FAILED");
			}
			
			// Validation - Divisional user should not be able to see leads who are not assigned to him and are  of other division 
			if(fc.utobj().assertLinkText(driver, lead4.getLeadFullName()))
			{
				fc.utobj().throwsException("Lead is visible to Divisional user even when lead belongs to some other division and is not assigned to this user - FAILED");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(retryAnalyzer = Retry.class , groups = { "User_Accessibility", "User_Accessibility_05" })
	@TestCase(createdOn = "2018-07-17", updatedOn = "2018-07-17", testCaseId = "User_Accessibility_05", testCaseDescription = "To test the accessibility of leads by corporate user")
	void userAccessibility_05() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			
			driver = fc.loginpage().login(driver);
			
			// Turn ON Division
			ConfigureNewHierarchyLevelTest configureNewHierarchyLevelTest = new ConfigureNewHierarchyLevelTest(driver);
			ConfigureNewHierarchyLevel configureNewHierarchyLevel = new ConfigureNewHierarchyLevel();
			configureNewHierarchyLevel.setEnableNewHierarchyLevel("Yes");
			configureNewHierarchyLevel.setLeadsExclusiveToNewHierarchyLevel("Yes");
			fc.commonMethods().getModules().openAdminPage(driver).openConfigureNewHierarchyLevel(driver);
			configureNewHierarchyLevelTest.EnableNewHierarchyLevel(configureNewHierarchyLevel);
			
			// Add Division #1
			AddDivision addDivision1 = new AddDivision();
			ManageDivisionTest manageDivisionTest = new ManageDivisionTest(driver);
			addDivision1.setDivisionName("Division_1_" +fc.utobj().generateRandomNumber());
			fc.commonMethods().getModules().openAdminPage(driver).openAddDivision(driver);
			manageDivisionTest.addDivision(addDivision1);
			
			// Add Division #2
			AddDivision addDivision2 = new AddDivision();
			addDivision2.setDivisionName("Division_2_" +fc.utobj().generateRandomNumber());
			fc.commonMethods().getModules().openAdminPage(driver).openAddDivision(driver);
			manageDivisionTest.addDivision(addDivision2);
			
			// Add Divisional User - Rest Api
			DivisionalUser divisionalUser = new DivisionalUser();
			fc.commonMethods().fillDefaultDataFor_DivisionalUser(divisionalUser);
			divisionalUser.setFirstName("Divisional_1");
			divisionalUser.setLastName("User" + fc.utobj().generateRandomNumber());
			divisionalUser.setDivision(addDivision1.getDivisionName());
			fc.commonMethods().addDivisionalUser(driver, divisionalUser);

			// Add Divisional User - Rest Api
			DivisionalUser divisionalUser1 = new DivisionalUser();
			fc.commonMethods().fillDefaultDataFor_DivisionalUser(divisionalUser1);
			divisionalUser1.setFirstName("Divisional_2");
			divisionalUser1.setLastName("User" + fc.utobj().generateRandomNumber());
			divisionalUser1.setDivision(addDivision2.getDivisionName());
			fc.commonMethods().addDivisionalUser(driver, divisionalUser1);
			
			// Setup Sales Lead Owners - ALL
			SetupSalesLeadOwnersTest setupSalesLeadOwnersTest = new SetupSalesLeadOwnersTest(driver);
			fc.commonMethods().getModules().openAdminPage(driver).openSetupFranchiseSalesLeadOwnersPage(driver);
			setupSalesLeadOwnersTest.setAllusersAsSalesLeadOwners_AndSubmit();
		
			// Create Corporate User through API
			CorporateUser corpUser = new CorporateUser();
			fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setFirstName("CorpUser");
			corpUser.setLastName("PrivateCampaign" + fc.utobj().generateRandomNumber());
			corpUser.setUserName("user" + fc.utobj().generateRandomChar());
			fc.commonMethods().addCorporateUser(driver, corpUser);
			ArrayList<String> corpUserList = new ArrayList<String>();
			
			// Set -> Assign Lead Owner by Division
			AssignLeadOwnersTest assignLeadOwnersTest = new AssignLeadOwnersTest(driver);
			fc.commonMethods().getModules().openAdminPage(driver).openAssignLeadOwnersPage(driver);
			assignLeadOwnersTest.set_defaultOwner("FranConnect Administrator");
			ArrayList <String> divUsers = new ArrayList<String>();
			divUsers.add(divisionalUser.getuserFullName());
			ArrayList<String> divUsers2 = new ArrayList<String>();
			divUsers2.add(divisionalUser1.getuserFullName());
			assignLeadOwnersTest.set_assignLeadOwnerbyDivision().Configure_AssignLeadOwnerbyDivision(addDivision1.getDivisionName(), divUsers).Configure_AssignLeadOwnerbyDivision(addDivision2.getDivisionName(), divUsers2);
			
			// Adding 2 leads - Assigning to above created divisional user
			Sales_Common_New sales_Common = new Sales_Common_New();
			Lead lead1 = new Lead();
			lead1.setFirstName("Lead1_UserAccesibility_" +fc.utobj().generateRandomChar());
			lead1.setLastName(fc.utobj().generateRandomNumber());
			lead1.setEmail("frantest2017@gmail.com");
			lead1.setLeadOwner(divisionalUser.getuserFullName());
			lead1.setLeadSourceCategory("Friends");
			lead1.setLeadSourceDetails("Friends");
			sales_Common.addLeadThroughWebServices(null, lead1);
			
			Lead lead2 = new Lead();
			lead2.setFirstName("Lead2_UserAccesibility_" +fc.utobj().generateRandomChar());
			lead2.setLastName(fc.utobj().generateRandomNumber());
			lead2.setEmail("frantest2017@gmail.com");
			lead2.setLeadOwner(divisionalUser.getuserFullName());
			lead2.setLeadSourceCategory("Friends");
			lead2.setLeadSourceDetails("Friends");
			sales_Common.addLeadThroughWebServices(null, lead2);
			
			// Add lead - with matching division
			Lead lead3 = new Lead();
			lead3.setFirstName("Lead3_UserAccesibility_" +fc.utobj().generateRandomChar());
			lead3.setLastName("Division1_"+fc.utobj().generateRandomNumber());
			lead3.setEmail("frantest2017@gmail.com");
			lead3.setBasedonAssignmentRules("yes");	
			lead3.setDivision(addDivision1.getDivisionName());
			lead3.setLeadSourceCategory("Friends");
			lead3.setLeadSourceDetails("Friends");
			sales_Common.addLeadThroughWebServices(null, lead3);
			
			// Add lead - with non matching division
			Lead lead4 = new Lead();
			lead4.setFirstName("Lead4_UserAccesibility_" +fc.utobj().generateRandomChar());
			lead4.setLastName("Division2_"+fc.utobj().generateRandomNumber());
			lead4.setEmail("frantest2017@gmail.com");
			lead4.setBasedonAssignmentRules("yes");	
			lead4.setDivision(addDivision2.getDivisionName());
			lead4.setLeadSourceCategory("Friends");
			lead4.setLeadSourceDetails("Friends");
			sales_Common.addLeadThroughWebServices(null, lead4);

			// LogOut
			fc.home_page().logout(driver);
			
			// Login with created Corporate User
			LoginPageTest loginPageTest = new LoginPageTest();
			loginPageTest.loginWithUserNameAndPassword(driver, corpUser.getUserName(), corpUser.getPassword());
			
			// Click on Sales
			fc.commonMethods().getModules().clickSalesModule(driver); 
			
			// Click on Lead Management
			Sales salesModule = new Sales(driver);
			salesModule.clickLeadManagement();
			
			// Set Lead Owners Filter as - All Selected -> click on Search
			LeadManagementTest leadManagementTest = new LeadManagementTest(driver);
			leadManagementTest.showFilters();
			leadManagementTest.selectALL_leadOwnersFilter();
			leadManagementTest.click_SearchBtn_Filters();
			
			// Validation - Corporate User should be able to see all leads 
			//leadManagementTest.leadSearch(lead1);
			if(! fc.utobj().assertLinkText(driver, lead1.getLeadFullName()))
			{
				System.out.println(lead1.getLeadFullName());
				fc.utobj().throwsException("Corporate User not able to see leads which are assigned to divisional user - FAILED");
			}
			
			//leadManagementTest.leadSearch(lead2);
			if(! fc.utobj().assertLinkText(driver, lead2.getLeadFullName()))
			{
				fc.utobj().throwsException("Corporate User not able to see leads which are assigned to divisional user - FAILED");
			}
			
			//leadManagementTest.leadSearch(lead3);
			if(! fc.utobj().assertLinkText(driver, lead3.getLeadFullName()))
			{
				fc.utobj().throwsException("Corporate User not able to see leads which belong to some division_1  - FAILED");
			}
			
			//leadManagementTest.leadSearch(lead4);
			if(! fc.utobj().assertLinkText(driver, lead4.getLeadFullName()))
			{
				fc.utobj().throwsException("Corporate User not able to see leads which belong to some division_2  - FAILED");
			}
			
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(retryAnalyzer = Retry.class , groups = { "User_Accessibility", "User_Accessibility_06" })
	@TestCase(createdOn = "2018-07-17", updatedOn = "2018-07-17", testCaseId = "User_Accessibility_06", testCaseDescription = "To test the visibility of leads on Lead summary page for regional user when role to access all leads is not given to him")
	void userAccessibility_06() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			
			driver = fc.loginpage().login(driver);
			
			fc.commonMethods().getModules().openAdminPage(driver).openRolesPage(driver);
			
			Roles roles = new Roles();
			roles.setRoleName("Regional_No_AccessAllLeads" +fc.utobj().generateRandomNumber());
			roles.setRoleType("Regional Level");
			RolesTest rolesTest = new RolesTest(driver);
			rolesTest.addNewRole_DivisionLevel_CannotAccessAllLeads(roles);

			fc.commonMethods().getModules().openAdminPage(driver).openManageAreaRegion(driver);
			
			// Create Area Region
			ManageAreaRegionTest manageAreaRegionTest = new ManageAreaRegionTest(driver);
			ManageAreaRegion manageAreaRegion = new ManageAreaRegion();
			manageAreaRegion.setAreaRegionName("AreaRegion1954");
			manageAreaRegion.setCategory("International");
			manageAreaRegion.setGroupBy("States");
			manageAreaRegion.getCountries().add("France");
			manageAreaRegion.getCountries().add("Australia");
			manageAreaRegion.getStates().add("Victoria");
			manageAreaRegion.getStates().add("South Australia");
			manageAreaRegion.getStates().add("Western Australia");
			manageAreaRegion.getStates().add("France");
			manageAreaRegion.getStates().add("Centre");

			// Only create Area Region if it does not exist
			if (! (fc.utobj().assertLinkText(driver, manageAreaRegion.getAreaRegionName()))) {
				manageAreaRegionTest.addAreaRegion(manageAreaRegion);
			}

			// Create Regional User - 1
			RegionalUser regionalUser = new RegionalUser();
			fc.commonMethods().fillDefaultDataFor_RegionalUser(regionalUser);
			regionalUser.setFirstName("Reg" + fc.utobj().generateRandomNumber());
			regionalUser.setLastName("User" + fc.utobj().generateRandomNumber());
			regionalUser.setUserName("regUser" + fc.utobj().generateRandomChar());
			regionalUser.setRole(roles.getRoleName());
			regionalUser.setArea_Region(manageAreaRegion.getAreaRegionName());
			fc.commonMethods().addRegionalUser(driver, regionalUser);
			
			// Create Regional User - 2
			RegionalUser regionalUser2 = new RegionalUser();
			fc.commonMethods().fillDefaultDataFor_RegionalUser(regionalUser2);
			regionalUser2.setFirstName("Reg" + fc.utobj().generateRandomNumber());
			regionalUser2.setLastName("User" + fc.utobj().generateRandomNumber());
			regionalUser2.setUserName("regUser" + fc.utobj().generateRandomChar());
			regionalUser2.setRole(roles.getRoleName());
			regionalUser2.setArea_Region(manageAreaRegion.getAreaRegionName());
			fc.commonMethods().addRegionalUser(driver, regionalUser2);
			
			// Create Regional User - 3
			RegionalUser regionalUser3 = new RegionalUser();
			fc.commonMethods().fillDefaultDataFor_RegionalUser(regionalUser3);
			regionalUser3.setFirstName("Reg" + fc.utobj().generateRandomNumber());
			regionalUser3.setLastName("User" + fc.utobj().generateRandomNumber());
			regionalUser3.setUserName("regUser" + fc.utobj().generateRandomChar());
			regionalUser3.setArea_Region(manageAreaRegion.getAreaRegionName());
			fc.commonMethods().addRegionalUser(driver, regionalUser3);
			
			// Adding 3 leads - Assigning to above created regional user
			// Lead 1 - Regional User 1
			Sales_Common_New sales_Common = new Sales_Common_New();
			Lead lead1 = new Lead();
			lead1.setFirstName("Lead1_UserAccesibility");
			lead1.setLastName(fc.utobj().generateRandomNumber());
			lead1.setEmail("frantest2017@gmail.com");
			lead1.setLeadOwner(regionalUser.getuserFullName());
			lead1.setLeadSourceCategory("Friends");
			lead1.setLeadSourceDetails("Friends");
			sales_Common.addLeadThroughWebServices(null, lead1);
			
			// Lead 2 - Regional User 2
			Lead lead2 = new Lead();
			lead2.setFirstName("Lead2_UserAccesibility");
			lead2.setLastName(fc.utobj().generateRandomNumber());
			lead2.setEmail("frantest2017@gmail.com");
			lead2.setLeadOwner(regionalUser2.getuserFullName());
			lead2.setLeadSourceCategory("Friends");
			lead2.setLeadSourceDetails("Friends");
			sales_Common.addLeadThroughWebServices(null, lead2);
			
			// Lead 3 - Regional User 3
			Lead lead3 = new Lead();
			lead3.setFirstName("Lead3_UserAccesibility");
			lead3.setLastName(fc.utobj().generateRandomNumber());
			lead3.setEmail("frantest2017@gmail.com");
			lead3.setLeadOwner(regionalUser3.getuserFullName());
			lead3.setLeadSourceCategory("Friends");
			lead3.setLeadSourceDetails("Friends");
			sales_Common.addLeadThroughWebServices(null, lead3);

			// LogOut
			fc.home_page().logout(driver);
			
			// Login with created Regional User
			LoginPageTest loginPageTest = new LoginPageTest();
			loginPageTest.loginWithUserNameAndPassword(driver, regionalUser.getUserName(), regionalUser.getPassword());

			// Click on Sales
			fc.commonMethods().getModules().clickSalesModule(driver); 
			
			// Click on Lead Management
			Sales salesModule = new Sales(driver);
			salesModule.clickLeadManagement();
			
			// Validation - other regional lead owners name should not be present in the 'Lead Owners' dropdown
			LeadManagementTest leadManagementTest = new LeadManagementTest(driver);
			leadManagementTest.showFilters();
			
			ArrayList<String> leadOwnersList = leadManagementTest.fetchLeadOwnersFilterDropDown_label();
			
			if(leadOwnersList.contains(regionalUser2.getuserFullName()) && leadOwnersList.contains(regionalUser3.getuserFullName()))
			{
				fc.utobj().throwsException("Other Regional Owners are visible in Lead Owners dropdown filter - FAILED ");
			}
			
			if(fc.utobj().assertLinkText(driver, lead2.getLeadFullName()))
			{
				fc.utobj().throwsException("Lead of some other regional user is visible - FAILED");
			}
			
			if(fc.utobj().assertLinkText(driver, lead3.getLeadFullName()))
			{
				fc.utobj().throwsException("Lead of some other regional user is visible - FAILED");
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	

	@Test(retryAnalyzer = Retry.class , groups = { "User_Accessibility", "User_Accessibility_07" })
	@TestCase(createdOn = "2018-07-17", updatedOn = "2018-07-17", testCaseId = "User_Accessibility_07", testCaseDescription = "To test the visibility of leads on Lead summary page for corporate user when role to access all leads is not given to him")
	void userAccessibility_07() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			
			driver = fc.loginpage().login(driver);
			
			// Create Corporate User through API
			CorporateUser corpUser = new CorporateUser();
			fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setFirstName("CorpUser");
			corpUser.setLastName("PrivateCampaign" + fc.utobj().generateRandomNumber());
			corpUser.setUserName("user" + fc.utobj().generateRandomChar());
			fc.commonMethods().addCorporateUser(driver, corpUser);
			
			// Lead 1 - assigned to Corporate User
			Sales_Common_New sales_Common = new Sales_Common_New();
			Lead lead1 = new Lead();
			lead1.setFirstName("Lead1_UserAccesibility");
			lead1.setLastName(fc.utobj().generateRandomNumber());
			lead1.setEmail("frantest2017@gmail.com");
			lead1.setLeadOwner(corpUser.getuserFullName());
			lead1.setLeadSourceCategory("Friends");
			lead1.setLeadSourceDetails("Friends");
			sales_Common.addLeadThroughWebServices(null, lead1);
			
			// Turn ON Division
			ConfigureNewHierarchyLevelTest configureNewHierarchyLevelTest = new ConfigureNewHierarchyLevelTest(driver);
			ConfigureNewHierarchyLevel configureNewHierarchyLevel = new ConfigureNewHierarchyLevel();
			configureNewHierarchyLevel.setEnableNewHierarchyLevel("Yes");
			configureNewHierarchyLevel.setLeadsExclusiveToNewHierarchyLevel("Yes");
			fc.commonMethods().getModules().openAdminPage(driver).openConfigureNewHierarchyLevel(driver);
			configureNewHierarchyLevelTest.EnableNewHierarchyLevel(configureNewHierarchyLevel);
			
			// Add Division #1
			AddDivision addDivision1 = new AddDivision();
			ManageDivisionTest manageDivisionTest = new ManageDivisionTest(driver);
			addDivision1.setDivisionName("Division_1_" +fc.utobj().generateRandomNumber());
			fc.commonMethods().getModules().openAdminPage(driver).openAddDivision(driver);
			manageDivisionTest.addDivision(addDivision1);
			
			// Add Divisional User - Rest Api
			DivisionalUser divisionalUser = new DivisionalUser();
			fc.commonMethods().fillDefaultDataFor_DivisionalUser(divisionalUser);
			divisionalUser.setFirstName("Divisional_1");
			divisionalUser.setLastName("User" + fc.utobj().generateRandomNumber());
			divisionalUser.setDivision(addDivision1.getDivisionName());
			fc.commonMethods().addDivisionalUser(driver, divisionalUser);
			
			// Lead 2 - assigned to Divisional User
			Lead lead2 = new Lead();
			lead2.setFirstName("Lead1_UserAccesibility");
			lead2.setLastName(fc.utobj().generateRandomNumber());
			lead2.setEmail("frantest2017@gmail.com");
			lead2.setLeadOwner(divisionalUser.getuserFullName());
			lead2.setLeadSourceCategory("Friends");
			lead2.setLeadSourceDetails("Friends");
			sales_Common.addLeadThroughWebServices(null, lead2);
			
			// Create Area Region
		   	fc.commonMethods().getModules().openAdminPage(driver).openManageAreaRegion(driver);
			ManageAreaRegionTest manageAreaRegionTest = new ManageAreaRegionTest(driver);
			ManageAreaRegion manageAreaRegion = new ManageAreaRegion();
			manageAreaRegion.setAreaRegionName("AreaRegion1954");
			manageAreaRegion.setCategory("International");
			manageAreaRegion.setGroupBy("States");
			manageAreaRegion.getCountries().add("France");
			manageAreaRegion.getCountries().add("Australia");
			manageAreaRegion.getStates().add("Victoria");
			manageAreaRegion.getStates().add("South Australia");
			manageAreaRegion.getStates().add("Western Australia");
			manageAreaRegion.getStates().add("France");
			manageAreaRegion.getStates().add("Centre");
			
			// Only create Area Region if it does not exist
			if (!(fc.utobj().assertLinkText(driver, manageAreaRegion.getAreaRegionName()))) {
				manageAreaRegionTest.addAreaRegion(manageAreaRegion);
			}
			
			// Create Regional User - 1
			RegionalUser regionalUser = new RegionalUser();
			fc.commonMethods().fillDefaultDataFor_RegionalUser(regionalUser);
			regionalUser.setFirstName("Reg" + fc.utobj().generateRandomNumber());
			regionalUser.setLastName("User" + fc.utobj().generateRandomNumber());
			regionalUser.setUserName("regUser" + fc.utobj().generateRandomChar());
			regionalUser.setArea_Region(manageAreaRegion.getAreaRegionName());
			fc.commonMethods().addRegionalUser(driver, regionalUser);
			
			// Lead 3 - assigned to Regional User
			Lead lead3 = new Lead();
			lead3.setFirstName("Lead1_UserAccesibility");
			lead3.setLastName(fc.utobj().generateRandomNumber());
			lead3.setEmail("frantest2017@gmail.com");
			lead3.setLeadOwner(regionalUser.getuserFullName());
			lead3.setLeadSourceCategory("Friends");
			lead3.setLeadSourceDetails("Friends");
			sales_Common.addLeadThroughWebServices(null, lead3);
			
			// LogOut
			fc.home_page().logout(driver);
			
			// Login with created Corporate User
			LoginPageTest loginPageTest = new LoginPageTest();
			loginPageTest.loginWithUserNameAndPassword(driver, corpUser.getUserName(), corpUser.getPassword());
			
			// Click on Sales
			fc.commonMethods().getModules().clickSalesModule(driver);

			// Click on Lead Management
			Sales salesModule = new Sales(driver);
			salesModule.clickLeadManagement();
			
			// Validation - All the users should be available in Lead Owner Filter
			LeadManagementTest leadManagementTest = new LeadManagementTest(driver);
			leadManagementTest.showFilters();
			ArrayList<String> leadOwnersList = leadManagementTest.fetchLeadOwnersFilterDropDown_label();

			if(! (leadOwnersList.contains(divisionalUser.getuserFullName()) && leadOwnersList.contains(regionalUser.getuserFullName())))
			{
				fc.utobj().throwsException("Other users are not available in lead owner filter - FAILED");
			}
			
			leadManagementTest.selectALL_leadOwnersFilter();
			leadManagementTest.click_SearchBtn_Filters();
			
			// Validation - Leads assigned to other users should be visible to corporate user
			boolean lead2visible = fc.utobj().assertLinkText(driver, lead2.getLeadFullName());
			boolean lead3visible = fc.utobj().assertLinkText(driver, lead3.getLeadFullName());
			if(! (lead2visible && lead3visible))
			{
				fc.utobj().throwsException("Leads assigned to other users are not visible to Corporate User having access to all leads - FAILED");
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(retryAnalyzer = Retry.class , groups = { "User_Accessibility", "User_Accessibility_08" })
	@TestCase(createdOn = "2018-07-17", updatedOn = "2018-07-17", testCaseId = "User_Accessibility_08", testCaseDescription = "To test the visibility of leads on Lead summary page for corporate user when role to access all leads is not given to him")
	void userAccessibility_08() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			
			driver = fc.loginpage().login(driver);
			
			fc.commonMethods().getModules().openAdminPage(driver).openRolesPage(driver);
			
			Roles roles = new Roles();
			roles.setRoleName("Corporate_No_AccessAllLeads" +fc.utobj().generateRandomNumber());
			roles.setRoleType("Corporate Level");
			RolesTest rolesTest = new RolesTest(driver);
			rolesTest.addNewRole_AnyLevel_CannotAccessAllLeads(roles);
			
			// Create Corporate User through API
			CorporateUser corpUser = new CorporateUser();
			fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setFirstName("CorpUser");
			corpUser.setLastName("Default_Role" + fc.utobj().generateRandomNumber());
			corpUser.setUserName("user" + fc.utobj().generateRandomChar());
			fc.commonMethods().addCorporateUser(driver, corpUser);

			// Lead 1 - assigned to Corporate User
			Sales_Common_New sales_Common = new Sales_Common_New();
			Lead lead1 = new Lead();
			lead1.setFirstName("Lead1_UserAccesibility");
			lead1.setLastName(fc.utobj().generateRandomNumber());
			lead1.setEmail("frantest2017@gmail.com");
			lead1.setLeadOwner(corpUser.getuserFullName());
			lead1.setLeadSourceCategory("Friends");
			lead1.setLeadSourceDetails("Friends");
			sales_Common.addLeadThroughWebServices(null, lead1);

			// Turn ON Division
			ConfigureNewHierarchyLevelTest configureNewHierarchyLevelTest = new ConfigureNewHierarchyLevelTest(driver);
			ConfigureNewHierarchyLevel configureNewHierarchyLevel = new ConfigureNewHierarchyLevel();
			configureNewHierarchyLevel.setEnableNewHierarchyLevel("Yes");
			configureNewHierarchyLevel.setLeadsExclusiveToNewHierarchyLevel("Yes");
			fc.commonMethods().getModules().openAdminPage(driver).openConfigureNewHierarchyLevel(driver);
			configureNewHierarchyLevelTest.EnableNewHierarchyLevel(configureNewHierarchyLevel);

			// Add Division #1
			AddDivision addDivision1 = new AddDivision();
			ManageDivisionTest manageDivisionTest = new ManageDivisionTest(driver);
			addDivision1.setDivisionName("Division_1_" + fc.utobj().generateRandomNumber());
			fc.commonMethods().getModules().openAdminPage(driver).openAddDivision(driver);
			manageDivisionTest.addDivision(addDivision1);

			// Add Divisional User - Rest Api
			DivisionalUser divisionalUser = new DivisionalUser();
			fc.commonMethods().fillDefaultDataFor_DivisionalUser(divisionalUser);
			divisionalUser.setFirstName("Divisional_1");
			divisionalUser.setLastName("User" + fc.utobj().generateRandomNumber());
			divisionalUser.setDivision(addDivision1.getDivisionName());
			fc.commonMethods().addDivisionalUser(driver, divisionalUser);

			// Lead 2 - assigned to Divisional User
			Lead lead2 = new Lead();
			lead2.setFirstName("Lead1_UserAccesibility");
			lead2.setLastName(fc.utobj().generateRandomNumber());
			lead2.setEmail("frantest2017@gmail.com");
			lead2.setLeadOwner(divisionalUser.getuserFullName());
			lead2.setLeadSourceCategory("Friends");
			lead2.setLeadSourceDetails("Friends");
			sales_Common.addLeadThroughWebServices(null, lead2);

			// Create Area Region
			fc.commonMethods().getModules().openAdminPage(driver).openManageAreaRegion(driver);
			ManageAreaRegionTest manageAreaRegionTest = new ManageAreaRegionTest(driver);
			ManageAreaRegion manageAreaRegion = new ManageAreaRegion();
			manageAreaRegion.setAreaRegionName("AreaRegion1954");
			manageAreaRegion.setCategory("International");
			manageAreaRegion.setGroupBy("States");
			manageAreaRegion.getCountries().add("France");
			manageAreaRegion.getCountries().add("Australia");
			manageAreaRegion.getStates().add("Victoria");
			manageAreaRegion.getStates().add("South Australia");
			manageAreaRegion.getStates().add("Western Australia");
			manageAreaRegion.getStates().add("France");
			manageAreaRegion.getStates().add("Centre");

			// Only create Area Region if it does not exist
			if (!(fc.utobj().assertLinkText(driver, manageAreaRegion.getAreaRegionName()))) {
				manageAreaRegionTest.addAreaRegion(manageAreaRegion);
			}

			// Create Regional User - 1
			RegionalUser regionalUser = new RegionalUser();
			fc.commonMethods().fillDefaultDataFor_RegionalUser(regionalUser);
			regionalUser.setFirstName("Reg" + fc.utobj().generateRandomNumber());
			regionalUser.setLastName("User" + fc.utobj().generateRandomNumber());
			regionalUser.setUserName("regUser" + fc.utobj().generateRandomChar());
			regionalUser.setArea_Region(manageAreaRegion.getAreaRegionName());
			fc.commonMethods().addRegionalUser(driver, regionalUser);

			// Lead 3 - assigned to Regional User
			Lead lead3 = new Lead();
			lead3.setFirstName("Lead1_UserAccesibility");
			lead3.setLastName(fc.utobj().generateRandomNumber());
			lead3.setEmail("frantest2017@gmail.com");
			lead3.setLeadOwner(regionalUser.getuserFullName());
			lead3.setLeadSourceCategory("Friends");
			lead3.setLeadSourceDetails("Friends");
			sales_Common.addLeadThroughWebServices(null, lead3);

			// Create Corporate User through API - with the above created Role
			CorporateUser corpUser1 = new CorporateUser();
			fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser1);
			corpUser1.setFirstName("CorpUser");
			corpUser1.setLastName("NotAllLeadsAccess" + fc.utobj().generateRandomNumber());
			corpUser1.setUserName("user" + fc.utobj().generateRandomChar());
			corpUser1.setRole(roles.getRoleName());
			fc.commonMethods().addCorporateUser(driver, corpUser1);
			
			// Lead 3 - assigned to Regional User
			Lead lead4 = new Lead();
			lead4.setFirstName("Lead1_UserAccesibility");
			lead4.setLastName(fc.utobj().generateRandomNumber());
			lead4.setEmail("frantest2017@gmail.com");
			lead4.setLeadOwner(corpUser1.getuserFullName());
			lead4.setLeadSourceCategory("Friends");
			lead4.setLeadSourceDetails("Friends");
			sales_Common.addLeadThroughWebServices(null, lead4);
			
			// LogOut
			fc.home_page().logout(driver);
			
			// Login with created Corporate User
			LoginPageTest loginPageTest = new LoginPageTest();
			loginPageTest.loginWithUserNameAndPassword(driver, corpUser1.getUserName(), corpUser1.getPassword());
			
			// Click on Sales
			fc.commonMethods().getModules().clickSalesModule(driver);

			// Click on Lead Management
			Sales salesModule = new Sales(driver);
			salesModule.clickLeadManagement();
			
			// Validation - All other users should not be available in Lead Owner Filter
			LeadManagementTest leadManagementTest = new LeadManagementTest(driver);
			leadManagementTest.showFilters();
			ArrayList<String> leadOwnersList = leadManagementTest.fetchLeadOwnersFilterDropDown_label();
			
			if(leadOwnersList.contains(divisionalUser.getuserFullName()) && leadOwnersList.contains(regionalUser.getuserFullName()) && leadOwnersList.contains(corpUser.getuserFullName()))
			{
				fc.utobj().throwsException("Other users are available in lead owner filter - FAILED");
			}
			
			leadManagementTest.selectALL_leadOwnersFilter();
			leadManagementTest.click_SearchBtn_Filters();
			
			// Validation - Leads assigned to other users should not be visible to corporate user
			boolean isLead1_visible = fc.utobj().assertLinkText(driver, lead1.getLeadFullName());
			boolean isLead2_visible = fc.utobj().assertLinkText(driver, lead2.getLeadFullName());
			boolean isLead3_visible = fc.utobj().assertLinkText(driver, lead3.getLeadFullName());		
			
			if(isLead1_visible || isLead2_visible || isLead3_visible)
			{
				fc.utobj().throwsException("Leads assigned to other users are visible to Corporate User having access to all leads - FAILED");
			}
			
			// Validation - Lead Assigned to Corporate User with customised role should be visible 
			if(! fc.utobj().assertLinkText(driver, lead4.getLeadFullName()))
			{
				fc.utobj().throwsException("Lead assigned to corp user with customised role is not visible - FAILED");
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(retryAnalyzer = Retry.class , groups = { "User_Accessibility", "User_Accessibility_09" })
	@TestCase(createdOn = "2018-07-17", updatedOn = "2018-07-17", testCaseId = "User_Accessibility_09", testCaseDescription = "To test the visibility of leads on Lead summary page for divisional user when role to access all leads is given to him")
	void userAccessibility_09() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			
			driver = fc.loginpage().login(driver);
			
			// Turn ON Division
			ConfigureNewHierarchyLevelTest configureNewHierarchyLevelTest = new ConfigureNewHierarchyLevelTest(driver);
			ConfigureNewHierarchyLevel configureNewHierarchyLevel = new ConfigureNewHierarchyLevel();
			configureNewHierarchyLevel.setEnableNewHierarchyLevel("Yes");
			configureNewHierarchyLevel.setLeadsExclusiveToNewHierarchyLevel("Yes");
			fc.commonMethods().getModules().openAdminPage(driver).openConfigureNewHierarchyLevel(driver);
			configureNewHierarchyLevelTest.EnableNewHierarchyLevel(configureNewHierarchyLevel);

			// Add Division #1
			AddDivision addDivision1 = new AddDivision();
			ManageDivisionTest manageDivisionTest = new ManageDivisionTest(driver);
			addDivision1.setDivisionName("Division_1_" + fc.utobj().generateRandomNumber());
			fc.commonMethods().getModules().openAdminPage(driver).openAddDivision(driver);
			manageDivisionTest.addDivision(addDivision1);
			
			// Add Divisional User 1 - Rest Api - role to access all leads is by default in Divisional Default Role
			DivisionalUser divisionalUser = new DivisionalUser();
			fc.commonMethods().fillDefaultDataFor_DivisionalUser(divisionalUser);
			divisionalUser.setFirstName("Divisional_1");
			divisionalUser.setLastName("User" + fc.utobj().generateRandomNumber());
			divisionalUser.setDivision(addDivision1.getDivisionName());
			fc.commonMethods().addDivisionalUser(driver, divisionalUser);
			
			// Add Divisional User 2 - Rest Api
			DivisionalUser divisionalUser2 = new DivisionalUser();
			fc.commonMethods().fillDefaultDataFor_DivisionalUser(divisionalUser2);
			divisionalUser2.setFirstName("Divisional_1");
			divisionalUser2.setLastName("User" + fc.utobj().generateRandomNumber());
			divisionalUser2.setDivision(addDivision1.getDivisionName());
			fc.commonMethods().addDivisionalUser(driver, divisionalUser2);
			
			// Set -> Assign Lead Owner by Division
			AssignLeadOwnersTest assignLeadOwnersTest = new AssignLeadOwnersTest(driver);
			fc.commonMethods().getModules().openAdminPage(driver).openAssignLeadOwnersPage(driver);
			assignLeadOwnersTest.set_defaultOwner("FranConnect Administrator");
			ArrayList<String> divUsers = new ArrayList<String>();
			divUsers.add(divisionalUser.getuserFullName());
			assignLeadOwnersTest.set_assignLeadOwnerbyDivision().Configure_AssignLeadOwnerbyDivision(addDivision1.getDivisionName(), divUsers);
			
			// Lead 1 - assign to divisional user
			Sales_Common_New sales_Common = new Sales_Common_New();
			Lead lead1 = new Lead();
			lead1.setFirstName("Lead1_UserAccesibility");
			lead1.setLastName(fc.utobj().generateRandomNumber());
			lead1.setEmail("frantest2017@gmail.com");
			lead1.setLeadOwner(divisionalUser.getuserFullName());
			lead1.setLeadSourceCategory("Friends");
			lead1.setLeadSourceDetails("Friends");
			sales_Common.addLeadThroughWebServices(null, lead1);
			
			// Lead 1 - assign to Division
			Lead lead2 = new Lead();
			lead2.setFirstName("Lead1_UserAccesibility");
			lead2.setLastName(fc.utobj().generateRandomNumber());
			lead2.setEmail("frantest2017@gmail.com");
			lead2.setDivision(addDivision1.getDivisionName());
			lead2.setLeadSourceCategory("Friends");
			lead2.setLeadSourceDetails("Friends");
			sales_Common.addLeadThroughWebServices(null, lead2);

			// Create Area Region
			fc.commonMethods().getModules().openAdminPage(driver).openManageAreaRegion(driver);
			ManageAreaRegionTest manageAreaRegionTest = new ManageAreaRegionTest(driver);
			ManageAreaRegion manageAreaRegion = new ManageAreaRegion();
			manageAreaRegion.setAreaRegionName("AreaRegion1954");
			manageAreaRegion.setCategory("International");
			manageAreaRegion.setGroupBy("States");
			manageAreaRegion.getCountries().add("France");
			manageAreaRegion.getCountries().add("Australia");
			manageAreaRegion.getStates().add("Victoria");
			manageAreaRegion.getStates().add("South Australia");
			manageAreaRegion.getStates().add("Western Australia");
			manageAreaRegion.getStates().add("France");
			manageAreaRegion.getStates().add("Centre");

			// Only create Area Region if it does not exist
			if (!(fc.utobj().assertLinkText(driver, manageAreaRegion.getAreaRegionName()))) {
				manageAreaRegionTest.addAreaRegion(manageAreaRegion);
			}

			// Create Regional User - 1
			RegionalUser regionalUser = new RegionalUser();
			fc.commonMethods().fillDefaultDataFor_RegionalUser(regionalUser);
			regionalUser.setFirstName("Reg" + fc.utobj().generateRandomNumber());
			regionalUser.setLastName("User" + fc.utobj().generateRandomNumber());
			regionalUser.setUserName("regUser" + fc.utobj().generateRandomChar());
			regionalUser.setArea_Region(manageAreaRegion.getAreaRegionName());
			fc.commonMethods().addRegionalUser(driver, regionalUser);

			// Create Corporate User through API
			CorporateUser corpUser = new CorporateUser();
			fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setFirstName("CorpUser");
			corpUser.setLastName("Default_Role" + fc.utobj().generateRandomNumber());
			corpUser.setUserName("user" + fc.utobj().generateRandomChar());
			fc.commonMethods().addCorporateUser(driver, corpUser);

			// LogOut
			fc.home_page().logout(driver);

			// Login with created Corporate User
			LoginPageTest loginPageTest = new LoginPageTest();
			loginPageTest.loginWithUserNameAndPassword(driver, divisionalUser.getUserName(),
					divisionalUser.getPassword());

			// Click on Sales
			fc.commonMethods().getModules().clickSalesModule(driver);

			// Click on Lead Management
			Sales salesModule = new Sales(driver);
			salesModule.clickLeadManagement();

			// Validation - All other users should not be available in Lead
			// Owner Filter
			LeadManagementTest leadManagementTest = new LeadManagementTest(driver);
			leadManagementTest.showFilters();
			ArrayList<String> leadOwnersList = leadManagementTest.fetchLeadOwnersFilterDropDown_label();

			leadManagementTest.selectALL_leadOwnersFilter();
			leadManagementTest.click_SearchBtn_Filters();
			
			/*for(String lo : leadOwnersList)
			{
				System.out.println(lo);
			}*/
			
			if (! leadOwnersList.contains(regionalUser.getuserFullName()))
			{
				fc.utobj().throwsException("Regional users is not available in lead owners filter - FAILED");
			}
			
			if(leadOwnersList.contains(corpUser.getuserFullName()))
			{
				fc.utobj().throwsException("Corporate user is available in lead owners filter - FAILED");
			}
			
			// Validation - Leads assigned to divisional user and leads belonging to same division should be visible
			if(! (fc.utobj().assertLinkText(driver, lead1.getLeadFullName()) && fc.utobj().assertLinkText(driver, lead2.getLeadFullName())))
			{
				fc.utobj().throwsException("Leads assigned to divisional user and to same division are not visible on lead management - FAILED");
			}
					
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(retryAnalyzer = Retry.class , groups = { "User_Accessibility", "User_Accessibility_10" })
	@TestCase(createdOn = "2018-07-17", updatedOn = "2018-07-17", testCaseId = "User_Accessibility_10", testCaseDescription = "To test the visibility of leads on Lead summary page for divisional user when role to access all leads is given to him")
	void userAccessibility_10() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			
			driver = fc.loginpage().login(driver);
			
			// Turn ON Division
			ConfigureNewHierarchyLevelTest configureNewHierarchyLevelTest = new ConfigureNewHierarchyLevelTest(driver);
			ConfigureNewHierarchyLevel configureNewHierarchyLevel = new ConfigureNewHierarchyLevel();
			configureNewHierarchyLevel.setEnableNewHierarchyLevel("Yes");
			configureNewHierarchyLevel.setLeadsExclusiveToNewHierarchyLevel("Yes");
			fc.commonMethods().getModules().openAdminPage(driver).openConfigureNewHierarchyLevel(driver);
			configureNewHierarchyLevelTest.EnableNewHierarchyLevel(configureNewHierarchyLevel);

			// Add Division #1
			AddDivision addDivision1 = new AddDivision();
			ManageDivisionTest manageDivisionTest = new ManageDivisionTest(driver);
			addDivision1.setDivisionName("Division_1_" + fc.utobj().generateRandomNumber());
			fc.commonMethods().getModules().openAdminPage(driver).openAddDivision(driver);
			manageDivisionTest.addDivision(addDivision1);
			
			// Create role - Divisional with (No access to all Leads)
			fc.commonMethods().getModules().openAdminPage(driver).openRolesPage(driver);
			Roles roles = new Roles();
			roles.setRoleName("Divisional_No_AccessAllLeads" +fc.utobj().generateRandomNumber());
			roles.setRoleType("Division Level");
			RolesTest rolesTest = new RolesTest(driver);
			rolesTest.addNewRole_AnyLevel_CannotAccessAllLeads(roles);
			
			// Create Corporate User through API - with the above created Role
			CorporateUser corpUser1 = new CorporateUser();
			fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser1);
			corpUser1.setFirstName("CorpUser");
			corpUser1.setLastName("NotAllLeadsAccess" + fc.utobj().generateRandomNumber());
			corpUser1.setUserName("user" + fc.utobj().generateRandomChar());
			fc.commonMethods().addCorporateUser(driver, corpUser1);
			
			// Add Divisional User 1 - Rest Api - role to access all leads is by default in Divisional Default Role
			DivisionalUser divisionalUser = new DivisionalUser();
			fc.commonMethods().fillDefaultDataFor_DivisionalUser(divisionalUser);
			divisionalUser.setFirstName("Divisional_1");
			divisionalUser.setLastName("User_NotAllLeads" + fc.utobj().generateRandomNumber());
			divisionalUser.setRole(roles.getRoleName());
			divisionalUser.setDivision(addDivision1.getDivisionName());
			fc.commonMethods().addDivisionalUser(driver, divisionalUser);
			
			// Add Divisional User 2 - Rest Api
			DivisionalUser divisionalUser2 = new DivisionalUser();
			fc.commonMethods().fillDefaultDataFor_DivisionalUser(divisionalUser2);
			divisionalUser2.setFirstName("Divisional_1");
			divisionalUser2.setLastName("User" + fc.utobj().generateRandomNumber());
			divisionalUser2.setDivision(addDivision1.getDivisionName());
			fc.commonMethods().addDivisionalUser(driver, divisionalUser2);
			
			// Set -> Assign Lead Owner by Division
			AssignLeadOwnersTest assignLeadOwnersTest = new AssignLeadOwnersTest(driver);
			fc.commonMethods().getModules().openAdminPage(driver).openAssignLeadOwnersPage(driver);
			assignLeadOwnersTest.set_defaultOwner("FranConnect Administrator");
			ArrayList<String> divUsers = new ArrayList<String>();
			divUsers.add(divisionalUser.getuserFullName());
			assignLeadOwnersTest.set_assignLeadOwnerbyDivision().Configure_AssignLeadOwnerbyDivision(addDivision1.getDivisionName(), divUsers);
			
			// Lead 1 - assign to divisional user
			Sales_Common_New sales_Common = new Sales_Common_New();
			Lead lead1 = new Lead();
			lead1.setFirstName("Lead1_UserAccesibility");
			lead1.setLastName(fc.utobj().generateRandomNumber());
			lead1.setEmail("frantest2017@gmail.com");
			lead1.setLeadOwner(divisionalUser.getuserFullName());
			lead1.setLeadSourceCategory("Friends");
			lead1.setLeadSourceDetails("Friends");
			sales_Common.addLeadThroughWebServices(null, lead1);
			
			// Lead 1 - assign to Division and assign to different Divisional user 2
			Lead lead2 = new Lead();
			lead2.setFirstName("Lead1_UserAccesibility");
			lead2.setLastName(fc.utobj().generateRandomNumber());
			lead2.setEmail("frantest2017@gmail.com");
			lead2.setDivision(addDivision1.getDivisionName());
			lead2.setLeadOwner(divisionalUser2.getuserFullName());
			lead2.setLeadSourceCategory("Friends");
			lead2.setLeadSourceDetails("Friends");
			sales_Common.addLeadThroughWebServices(null, lead2);

			// Create Area Region
			fc.commonMethods().getModules().openAdminPage(driver).openManageAreaRegion(driver);
			ManageAreaRegionTest manageAreaRegionTest = new ManageAreaRegionTest(driver);
			ManageAreaRegion manageAreaRegion = new ManageAreaRegion();
			manageAreaRegion.setAreaRegionName("AreaRegion1954");
			manageAreaRegion.setCategory("International");
			manageAreaRegion.setGroupBy("States");
			manageAreaRegion.getCountries().add("France");
			manageAreaRegion.getCountries().add("Australia");
			manageAreaRegion.getStates().add("Victoria");
			manageAreaRegion.getStates().add("South Australia");
			manageAreaRegion.getStates().add("Western Australia");
			manageAreaRegion.getStates().add("France");
			manageAreaRegion.getStates().add("Centre");

			// Only create Area Region if it does not exist
			if (!(fc.utobj().assertLinkText(driver, manageAreaRegion.getAreaRegionName()))) {
				manageAreaRegionTest.addAreaRegion(manageAreaRegion);
			}

			// Create Regional User - 1
			RegionalUser regionalUser = new RegionalUser();
			fc.commonMethods().fillDefaultDataFor_RegionalUser(regionalUser);
			regionalUser.setFirstName("Reg" + fc.utobj().generateRandomNumber());
			regionalUser.setLastName("User" + fc.utobj().generateRandomNumber());
			regionalUser.setUserName("regUser" + fc.utobj().generateRandomChar());
			regionalUser.setArea_Region(manageAreaRegion.getAreaRegionName());
			fc.commonMethods().addRegionalUser(driver, regionalUser);

			// Create Corporate User through API
			CorporateUser corpUser = new CorporateUser();
			fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setFirstName("CorpUser");
			corpUser.setLastName("Default_Role" + fc.utobj().generateRandomNumber());
			corpUser.setUserName("user" + fc.utobj().generateRandomChar());
			fc.commonMethods().addCorporateUser(driver, corpUser);

			// LogOut
			fc.home_page().logout(driver);

			// Login with created Divisional User
			LoginPageTest loginPageTest = new LoginPageTest();
			loginPageTest.loginWithUserNameAndPassword(driver, divisionalUser.getUserName(),
					divisionalUser.getPassword());

			// Click on Sales
			fc.commonMethods().getModules().clickSalesModule(driver);

			// Click on Lead Management
			Sales salesModule = new Sales(driver);
			salesModule.clickLeadManagement();

			// Validation - All other users should not be available in Lead
			// Owner Filter
			LeadManagementTest leadManagementTest = new LeadManagementTest(driver);
			leadManagementTest.showFilters();
			ArrayList<String> leadOwnersList = leadManagementTest.fetchLeadOwnersFilterDropDown_label();

			/*leadManagementTest.selectALL_leadOwnersFilter();
			leadManagementTest.click_SearchBtn_Filters();*/
			
			for(String lo : leadOwnersList)
			{
				System.out.println(lo);
			}
			
			// Validation - other Regional users should not be visible in the Lead Owner dropdown filter
			if (leadOwnersList.contains(regionalUser.getuserFullName()))
			{
				fc.utobj().throwsException("Regional users are available in lead owners filter - FAILED");
			}
			
			// Validation - Other Corporate and Divisional users should not be visible in the Lead Owner dropdown filter
			if(leadOwnersList.contains(corpUser.getuserFullName()) && leadOwnersList.contains(divisionalUser2.getuserFullName()))
			{
				fc.utobj().throwsException("Corporate and Other Divisional users are available in lead owners filter - FAILED");
			}
			
			// Validation - Leads assigned to divisional user and leads belonging to same division should be visible
			if(! fc.utobj().assertLinkText(driver, lead1.getLeadFullName()))
			{
				fc.utobj().throwsException("Leads assigned to divisional user is not visible on lead management - FAILED");
			}
			
			if(fc.utobj().assertLinkText(driver, lead2.getLeadFullName()))
			{
				fc.utobj().throwsException("Lead assigned to other divisional user is visible on lead management - FAILED");
			}
					
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(retryAnalyzer = Retry.class , groups = { "User_Accessibility", "User_Accessibility_11" })
	@TestCase(createdOn = "2018-07-17", updatedOn = "2018-07-17", testCaseId = "User_Accessibility_11", testCaseDescription = "To test the visibility of leads on Lead summary page for regional user when role to access all leads is given to him")
	void userAccessibility_11() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			
			driver = fc.loginpage().login(driver);
			
			// Create Area Region
			fc.commonMethods().getModules().openAdminPage(driver).openManageAreaRegion(driver);
			ManageAreaRegionTest manageAreaRegionTest = new ManageAreaRegionTest(driver);
			ManageAreaRegion manageAreaRegion = new ManageAreaRegion();
			manageAreaRegion.setAreaRegionName("AreaRegion1954");
			manageAreaRegion.setCategory("International");
			manageAreaRegion.setGroupBy("States");
			manageAreaRegion.getCountries().add("France");
			manageAreaRegion.getCountries().add("Australia");
			manageAreaRegion.getStates().add("Victoria");
			manageAreaRegion.getStates().add("South Australia");
			manageAreaRegion.getStates().add("Western Australia");
			manageAreaRegion.getStates().add("France");
			manageAreaRegion.getStates().add("Centre");

			// Only create Area Region if it does not exist
			if (! (fc.utobj().assertLinkText(driver, manageAreaRegion.getAreaRegionName()))) {
				manageAreaRegionTest.addAreaRegion(manageAreaRegion);
			}
			
			// Create Regional User - 1
			RegionalUser regionalUser = new RegionalUser();
			fc.commonMethods().fillDefaultDataFor_RegionalUser(regionalUser);
			regionalUser.setFirstName("Reg" + fc.utobj().generateRandomNumber());
			regionalUser.setLastName("User" + fc.utobj().generateRandomNumber());
			regionalUser.setUserName("regUser" + fc.utobj().generateRandomChar());
			regionalUser.setArea_Region(manageAreaRegion.getAreaRegionName());
			fc.commonMethods().addRegionalUser(driver, regionalUser);
			
			// Lead1 assigned to Regional User
			Sales_Common_New sales_Common = new Sales_Common_New();
			Lead lead1 = new Lead();
			lead1.setFirstName("Lead1_UserAccesibility");
			lead1.setLastName(fc.utobj().generateRandomNumber());
			lead1.setEmail("frantest2017@gmail.com");
			lead1.setLeadOwner(regionalUser.getuserFullName());
			lead1.setLeadSourceCategory("Friends");
			lead1.setLeadSourceDetails("Friends");
			sales_Common.addLeadThroughWebServices(null, lead1);
			
			// Lead2 assigned to Region
			Lead lead2 = new Lead();
			lead2.setFirstName("Lead1_UserAccesibility");
			lead2.setLastName(fc.utobj().generateRandomNumber());
			lead2.setEmail("frantest2017@gmail.com");
			lead2.setCountry("France");
			lead2.setStateProvince("France");
			lead2.setLeadSourceCategory("Friends");
			lead2.setLeadSourceDetails("Friends");
			sales_Common.addLeadThroughWebServices(null, lead2);
			
			// LogOut
			fc.home_page().logout(driver);

			// Login with created Divisional User
			LoginPageTest loginPageTest = new LoginPageTest();
			loginPageTest.loginWithUserNameAndPassword(driver, regionalUser.getUserName(),
					regionalUser.getPassword());

			// Click on Sales
			fc.commonMethods().getModules().clickSalesModule(driver);

			// Click on Lead Management
			Sales salesModule = new Sales(driver);
			salesModule.clickLeadManagement();
			
			LeadManagementTest leadManagementTest = new LeadManagementTest(driver);
			leadManagementTest.showFilters();
			
			// Select All - Lead Owners Filter
			leadManagementTest.selectALL_leadOwnersFilter();
			leadManagementTest.click_SearchBtn_Filters();
			
			// Validation - Lead assigned to Regional User should be visible
			if(! fc.utobj().assertLinkText(driver, lead1.getLeadFullName()))
			{
				fc.utobj().throwsException("Lead assigned to Regional user is not visible - FAILED");
			}
			
			// Validation - Lead assigned to Region should be visible
			if(! fc.utobj().assertLinkText(driver, lead2.getLeadFullName()))
			{
				fc.utobj().throwsException("Lead assigned to Region is not visible - FAILED");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			}
		}
	
	@Test(retryAnalyzer = Retry.class , groups = { "User_Accessibility", "User_Accessibility_12" })
	@TestCase(createdOn = "2018-07-17", updatedOn = "2018-07-17", testCaseId = "User_Accessibility_12", testCaseDescription = "To test the visibility of leads on Lead summary page for regional user when role to access all leads is given to him")
	void userAccessibility_12() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			
			driver = fc.loginpage().login(driver);
			
			// Create role with No access to Reports
			fc.commonMethods().getModules().openAdminPage(driver).openRolesPage(driver);
			Roles roles = new Roles();
			roles.setRoleName("Corporate_No_AccessAllLeads" +fc.utobj().generateRandomNumber());
			roles.setRoleType("Corporate Level");
			RolesTest rolesTest = new RolesTest(driver);
			rolesTest.addNewRole_AnyLevel_CannotAccessReports(roles);
			
			// Create Corporate User 1 - through API - Default Corporate Role 
			CorporateUser corpUser = new CorporateUser();
			fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setFirstName("CorpUser");
			corpUser.setLastName("Default_Role" + fc.utobj().generateRandomNumber());
			corpUser.setUserName("user" + fc.utobj().generateRandomChar());
			fc.commonMethods().addCorporateUser(driver, corpUser);
			
			// Create Corporate User 2 - through API - Role without reports access
			CorporateUser corpUser2 = new CorporateUser();
			fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser2);
			corpUser2.setFirstName("CorpUser");
			corpUser2.setLastName("NoReports_Role" + fc.utobj().generateRandomNumber());
			corpUser2.setRole(roles.getRoleName());
			corpUser2.setUserName("user" + fc.utobj().generateRandomChar());
			fc.commonMethods().addCorporateUser(driver, corpUser2);

			// LogOut
			fc.home_page().logout(driver);
			
			// Login with created Corporatel User
			LoginPageTest loginPageTest = new LoginPageTest();
			loginPageTest.loginWithUserNameAndPassword(driver, corpUser.getUserName(), corpUser.getPassword());
			
			// Click on Sales
			fc.commonMethods().getModules().clickSalesModule(driver);

			// Click on Reports
			Sales salesModule = new Sales(driver);
			salesModule.clickReports();
			
			SalesUI ui = new SalesUI(driver);
			fc.utobj().clickElement(driver, ui.Reports);
			
			// Validation - Reports link should be available 
			if(! fc.utobj().isElementPresent(driver, ui.Reports))
			{
				fc.utobj().throwsException("Reports link is not present - FAILED");
			}
			
			// LogOut
			fc.home_page().logout(driver);
			
			// Login with user with NO access to reports and validate 

			// Login with created Divisional User
			loginPageTest.loginWithUserNameAndPassword(driver, corpUser2.getUserName(), corpUser2.getPassword());

			// Click on Sales
			fc.commonMethods().getModules().clickSalesModule(driver);

			// Validation - Reports link should not be available
			if (fc.utobj().isElementPresent(driver, ui.Reports)) {
				fc.utobj().throwsException("Reports link is present - FAILED");
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(retryAnalyzer = Retry.class , groups = { "User_Accessibility", "User_Accessibility_13" })
	@TestCase(createdOn = "2018-07-17", updatedOn = "2018-07-17", testCaseId = "User_Accessibility_13", testCaseDescription = "To test the accessibility of reports by divisional user")
	void userAccessibility_13() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			
			driver = fc.loginpage().login(driver);
			
			// Create role with No access to Reports
			fc.commonMethods().getModules().openAdminPage(driver).openRolesPage(driver);
			Roles roles = new Roles();
			roles.setRoleName("Division_No_AccessReports" + fc.utobj().generateRandomNumber());
			roles.setRoleType("Division Level");
			RolesTest rolesTest = new RolesTest(driver);
			rolesTest.addNewRole_AnyLevel_CannotAccessReports(roles);

			// Turn ON Division
			ConfigureNewHierarchyLevelTest configureNewHierarchyLevelTest = new ConfigureNewHierarchyLevelTest(driver);
			ConfigureNewHierarchyLevel configureNewHierarchyLevel = new ConfigureNewHierarchyLevel();
			configureNewHierarchyLevel.setEnableNewHierarchyLevel("Yes");
			configureNewHierarchyLevel.setLeadsExclusiveToNewHierarchyLevel("Yes");
			fc.commonMethods().getModules().openAdminPage(driver).openConfigureNewHierarchyLevel(driver);
			configureNewHierarchyLevelTest.EnableNewHierarchyLevel(configureNewHierarchyLevel);

			// Add Division #1
			AddDivision addDivision1 = new AddDivision();
			ManageDivisionTest manageDivisionTest = new ManageDivisionTest(driver);
			addDivision1.setDivisionName("Division_1_" + fc.utobj().generateRandomNumber());
			fc.commonMethods().getModules().openAdminPage(driver).openAddDivision(driver);
			manageDivisionTest.addDivision(addDivision1);
			
			// Add Divisional User 1 - Rest Api - role to access all reports  is by default in Divisional Default Role
			DivisionalUser divisionalUser = new DivisionalUser();
			fc.commonMethods().fillDefaultDataFor_DivisionalUser(divisionalUser);
			divisionalUser.setFirstName("Divisional_1");
			divisionalUser.setLastName("User_NotAllLeads" + fc.utobj().generateRandomNumber());
			divisionalUser.setDivision(addDivision1.getDivisionName());
			fc.commonMethods().addDivisionalUser(driver, divisionalUser);
			
			// Add Divisional User 2 - Rest Api - assigned to role with no-access to reports
			DivisionalUser divisionalUser2 = new DivisionalUser();
			fc.commonMethods().fillDefaultDataFor_DivisionalUser(divisionalUser2);
			divisionalUser2.setFirstName("Divisional_1");
			divisionalUser2.setLastName("User" + fc.utobj().generateRandomNumber());
			divisionalUser2.setDivision(addDivision1.getDivisionName());
			divisionalUser2.setRole(roles.getRoleName());
			fc.commonMethods().addDivisionalUser(driver, divisionalUser2);
			
			// LogOut
			fc.home_page().logout(driver);
			
			// Login with created Corporatel User
			LoginPageTest loginPageTest = new LoginPageTest();
			loginPageTest.loginWithUserNameAndPassword(driver, divisionalUser.getUserName(), divisionalUser.getPassword());
			
			// Click on Sales
			fc.commonMethods().getModules().clickSalesModule(driver);

			// Click on Reports
			Sales salesModule = new Sales(driver);
			salesModule.clickReports();
			
			SalesUI ui = new SalesUI(driver);
			fc.utobj().clickElement(driver, ui.Reports);
			
			// Validation - Reports link should be available 
			if(! fc.utobj().isElementPresent(driver, ui.Reports))
			{
				fc.utobj().throwsException("Reports link is not present - FAILED");
			}
			
			// LogOut
			fc.home_page().logout(driver);
			
			// Login with user with NO access to reports and validate 
			// Login with created Divisional User with no access to Reports
			loginPageTest.loginWithUserNameAndPassword(driver, divisionalUser2.getUserName(), divisionalUser2.getPassword());

			// Click on Sales
			fc.commonMethods().getModules().clickSalesModule(driver);

			// Validation - Reports link should not be available
			if (fc.utobj().isElementPresent(driver, ui.Reports)) {
				fc.utobj().throwsException("Reports link is present - FAILED");
			}
			
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(retryAnalyzer = Retry.class , groups = { "User_Accessibility", "User_Accessibility_14" })
	@TestCase(createdOn = "2018-07-17", updatedOn = "2018-07-17", testCaseId = "User_Accessibility_14", testCaseDescription = "To test the accessibility of reports by divisional user")
	void userAccessibility_14() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			
			driver = fc.loginpage().login(driver);
			
			// Create role with No access to - Grants Privilege to Add New Lead 
			fc.commonMethods().getModules().openAdminPage(driver).openRolesPage(driver);
			Roles roles_cannotAccessReports = new Roles();
			roles_cannotAccessReports.setRoleName("Division_No_AccessReports" + fc.utobj().generateRandomNumber());
			roles_cannotAccessReports.setRoleType("Division Level");
			RolesTest rolesTest = new RolesTest(driver);
			rolesTest.addNewRole_AnyLevel_CannotAddNewLead(roles_cannotAccessReports);
			
			// Create role with No access to - Grants Privilege to Add New Lead
			fc.commonMethods().getModules().openAdminPage(driver).openRolesPage(driver);
			Roles roles_CannotDeleteLead = new Roles();
			roles_CannotDeleteLead.setRoleName("Division_No_AccessReports" + fc.utobj().generateRandomNumber());
			roles_CannotDeleteLead.setRoleType("Division Level");
			rolesTest.addNewRole_AnyLevel_CannotAddNewLead(roles_CannotDeleteLead);
			
			// Create role with No access to - Grants Privilege to Add New Lead
			fc.commonMethods().getModules().openAdminPage(driver).openRolesPage(driver);
			Roles roles_CannotAccessAllLeads = new Roles();
			roles_CannotAccessAllLeads.setRoleName("Division_No_AccessReports" + fc.utobj().generateRandomNumber());
			roles_CannotAccessAllLeads.setRoleType("Division Level");
			rolesTest.addNewRole_AnyLevel_CannotAccessAllLeads(roles_CannotAccessAllLeads);
			
			// Create Corporate User through API 
			CorporateUser corpUser1 = new CorporateUser();
			fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser1);
			corpUser1.setFirstName("CorpUser");
			corpUser1.setLastName("Report" + fc.utobj().generateRandomNumber());
			corpUser1.setUserName("user" + fc.utobj().generateRandomChar());
			fc.commonMethods().addCorporateUser(driver, corpUser1);
			
			// Create Corporate User through API - Cannot Access Reports
			CorporateUser corpUser2 = new CorporateUser();
			fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser2);
			corpUser2.setFirstName("CorpUser");
			corpUser2.setLastName("Report" + fc.utobj().generateRandomNumber());
			corpUser2.setUserName("user" + fc.utobj().generateRandomChar());
			corpUser2.setRole(roles_cannotAccessReports.getRoleName());
			fc.commonMethods().addCorporateUser(driver, corpUser2);

			// Create Corporate User through API - Cannot Delete Lead
			CorporateUser corpUser3 = new CorporateUser();
			fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser3);
			corpUser3.setFirstName("CorpUser");
			corpUser3.setLastName("Report" + fc.utobj().generateRandomNumber());
			corpUser3.setUserName("user" + fc.utobj().generateRandomChar());
			corpUser3.setRole(roles_CannotDeleteLead.getRoleName());
			fc.commonMethods().addCorporateUser(driver, corpUser3);

			// Create Corporate User through API - Cannot Access All Leads
			CorporateUser corpUser4 = new CorporateUser();
			fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser4);
			corpUser4.setFirstName("CorpUser");
			corpUser4.setLastName("Report" + fc.utobj().generateRandomNumber());
			corpUser4.setUserName("user" + fc.utobj().generateRandomChar());
			corpUser4.setRole(roles_CannotAccessAllLeads.getRoleName());
			fc.commonMethods().addCorporateUser(driver, corpUser4);
			
			// Lead 1 - assign to user 1
			Sales_Common_New sales_Common = new Sales_Common_New();
			Lead lead1 = new Lead();
			lead1.setFirstName("Lead1_UserAccesibility");
			lead1.setLastName(fc.utobj().generateRandomNumber());
			lead1.setEmail("frantest2017@gmail.com");
			lead1.setLeadOwner(corpUser1.getuserFullName());
			lead1.setLeadSourceCategory("Friends");
			lead1.setLeadSourceDetails("Friends");
			sales_Common.addLeadThroughWebServices(null, lead1);
			
			// Lead 2 - assign to user 2
			Lead lead2 = new Lead();
			lead2.setFirstName("Lead1_UserAccesibility");
			lead2.setLastName(fc.utobj().generateRandomNumber());
			lead2.setEmail("frantest2017@gmail.com");
			lead2.setLeadOwner(corpUser2.getuserFullName());
			lead2.setLeadSourceCategory("Friends");
			lead2.setLeadSourceDetails("Friends");
			sales_Common.addLeadThroughWebServices(null, lead2);
			
			// Lead 3 - assign to user 3
			Lead lead3 = new Lead();
			lead3.setFirstName("Lead1_UserAccesibility");
			lead3.setLastName(fc.utobj().generateRandomNumber());
			lead3.setEmail("frantest2017@gmail.com");
			lead3.setLeadOwner(corpUser3.getuserFullName());
			lead3.setLeadSourceCategory("Friends");
			lead3.setLeadSourceDetails("Friends");
			sales_Common.addLeadThroughWebServices(null, lead3);
			
			// Lead 4 - assign to user 4
			Lead lead4 = new Lead();
			lead4.setFirstName("Lead1_UserAccesibility");
			lead4.setLastName(fc.utobj().generateRandomNumber());
			lead4.setEmail("frantest2017@gmail.com");
			lead4.setLeadOwner(corpUser4.getuserFullName());
			lead4.setLeadSourceCategory("Friends");
			lead4.setLeadSourceDetails("Friends");
			sales_Common.addLeadThroughWebServices(null, lead4);
			
			// LogOut
			fc.home_page().logout(driver);

			// Login with created Corporatel User
			LoginPageTest loginPageTest = new LoginPageTest();
			loginPageTest.loginWithUserNameAndPassword(driver, corpUser1.getUserName(),
					corpUser1.getPassword());

			// Click on Sales
			fc.commonMethods().getModules().clickSalesModule(driver);
			
			// Click on Reports
			Sales salesModule = new Sales(driver);
			salesModule.clickReports();

			SalesUI ui = new SalesUI(driver);
			fc.utobj().clickElement(driver, ui.Reports);

			// Validation - Reports link should be available
			if (!fc.utobj().isElementPresent(driver, ui.Reports)) {
				fc.utobj().throwsException("Reports link is not present - FAILED");
			}

			// LogOut
			fc.home_page().logout(driver);

			// Login with created Corporatel User
			loginPageTest.loginWithUserNameAndPassword(driver, corpUser2.getUserName(),
					corpUser2.getPassword());

			// Click on Sales
			fc.commonMethods().getModules().clickSalesModule(driver);

			// Validation - Reports link should not be available
			if (fc.utobj().isElementPresent(driver, ui.Reports)) {
				fc.utobj().throwsException("Reports link is present - FAILED");
			}
			
			// LogOut
			fc.home_page().logout(driver);

			// Login with created Corporatel User
			loginPageTest.loginWithUserNameAndPassword(driver, corpUser3.getUserName(), corpUser3.getPassword());
			
			salesModule.clickLeadManagement();
			
			LeadSummaryUI leadSummaryUI = new LeadSummaryUI(driver);
			
			// Validation - Delete button should not be there
			if(fc.utobj().isElementPresent(driver, leadSummaryUI.deleteBottomBtn))
			{
				fc.utobj().throwsException("Lead Delete button is available despite no such privilege to the user - FAILED");
			}
			
			// LogOut
			fc.home_page().logout(driver);
			
			// Login with created Corporatel User
			loginPageTest.loginWithUserNameAndPassword(driver, corpUser4.getUserName(), corpUser4.getPassword());
			
			// Click on Sales
			fc.commonMethods().getModules().clickSalesModule(driver);
			
			salesModule.clickLeadManagement();
			
			// Validation - All other users should not be available in Lead Owner Filter
			LeadManagementTest leadManagementTest = new LeadManagementTest(driver);
			leadManagementTest.showFilters();
			ArrayList<String> leadOwnersList = leadManagementTest.fetchLeadOwnersFilterDropDown_label();

			/*leadManagementTest.selectALL_leadOwnersFilter();
			leadManagementTest.click_SearchBtn_Filters();*/
			
			for(String lo : leadOwnersList)
			{
				System.out.println(lo);
			}
			
			// Validation - Other Corporate Users should not be visible in the Lead Owners filter dropdown
			
			
			// Validation - Leads assigned to other users should not be visible to this user
			
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(retryAnalyzer = Retry.class , groups = { "User_Accessibility", "User_Accessibility_15" })
	@TestCase(createdOn = "2018-07-17", updatedOn = "2018-07-17", testCaseId = "User_Accessibility_15", testCaseDescription = "To test the accessibility of reports by divisional user")
	void userAccessibility_15() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			
			driver = fc.loginpage().login(driver);
			
			// Create Corporate User through API 
			CorporateUser corpUser1 = new CorporateUser();
			fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser1);
			corpUser1.setFirstName("CorpUser");
			corpUser1.setLastName("Report" + fc.utobj().generateRandomNumber());
			corpUser1.setUserName("user" + fc.utobj().generateRandomChar());
			fc.commonMethods().addCorporateUser(driver, corpUser1);
			
			// LogOut
			fc.home_page().logout(driver);
			
			// Login with created Corporatel User
			LoginPageTest loginPageTest = new LoginPageTest();
			loginPageTest.loginWithUserNameAndPassword(driver, corpUser1.getUserName(), corpUser1.getPassword());
			
			// Click on Sales
			fc.commonMethods().getModules().clickSalesModule(driver);
			
			// Click on Lead Management 
			Sales sales = new Sales(driver);
			sales.clickLeadManagement();
			
			// Validation - All other users should not be available in Lead Owner Filter
			LeadManagementTest leadManagementTest = new LeadManagementTest(driver);
			leadManagementTest.showFilters();
			ArrayList<String> leadOwnersList = leadManagementTest.fetchLeadOwnersFilterDropDown_label();
		
			/*for(String lo : leadOwnersList)
			{
				System.out.println("######### >>>>>>>>>>>>>" +lo+ "<<<<<<<<<<< ##################");
			}*/
			
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
}
