package com.builds.test.salesTest;

class Workflow {

	private String WhenaLeadis;
	private String WhichLeadsdoyouwanttoexecutethisworkflowon;
	private String SendEmailCampaign;
	private String ChangeStatus;
	private String ChangeEmail;
	private String Task;
	private String ChangeCandidatePortalStatus;
	
	
	
	public String getWhenaLeadis() {
		return WhenaLeadis;
	}
	public void setWhenaLeadis(String whenaLeadis) {
		WhenaLeadis = whenaLeadis;
	}
	public String getWhichLeadsdoyouwanttoexecutethisworkflowon() {
		return WhichLeadsdoyouwanttoexecutethisworkflowon;
	}
	public void setWhichLeadsdoyouwanttoexecutethisworkflowon(String whichLeadsdoyouwanttoexecutethisworkflowon) {
		WhichLeadsdoyouwanttoexecutethisworkflowon = whichLeadsdoyouwanttoexecutethisworkflowon;
	}
	public String getSendEmailCampaign() {
		return SendEmailCampaign;
	}
	public void setSendEmailCampaign(String sendEmailCampaign) {
		SendEmailCampaign = sendEmailCampaign;
	}
	public String getChangeStatus() {
		return ChangeStatus;
	}
	public void setChangeStatus(String changeStatus) {
		ChangeStatus = changeStatus;
	}
	public String getChangeEmail() {
		return ChangeEmail;
	}
	public void setChangeEmail(String changeEmail) {
		ChangeEmail = changeEmail;
	}
	public String getTask() {
		return Task;
	}
	public void setTask(String task) {
		Task = task;
	}
	public String getChangeCandidatePortalStatus() {
		return ChangeCandidatePortalStatus;
	}
	public void setChangeCandidatePortalStatus(String changeCandidatePortalStatus) {
		ChangeCandidatePortalStatus = changeCandidatePortalStatus;
	}
	
	
}
