package com.builds.test.smoke;

import java.util.HashMap;
import java.util.Map;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminFranchiseLocationAddFranchiseLocationPageTest;
import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.common.CRMModule;
import com.builds.test.common.CorporateUser;
import com.builds.test.common.FCHomePageTest;
import com.builds.test.crm.Addleadpage;
import com.builds.test.crm.Leadgetset;
import com.builds.test.fieldops.AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest;
import com.builds.test.fieldops.AdminFieldOpsManageVisitFormPageTest;
import com.builds.test.fieldops.AdminFieldOpsQuestionLibraryPageTest;
import com.builds.test.fieldops.FieldOpsVisitsPageTest;
import com.builds.test.fin.AdminFinanceAgreementVersionsPageTest;
import com.builds.test.fin.AdminFinanceFinanceSetupPreferencesPageTest;
import com.builds.test.fin.FinanceSalesPageTest;
import com.builds.test.thehub.AdminTheHubLibraryPageTest;
import com.builds.uimaps.common.FCHomePage;
import com.builds.uimaps.common.LoginPage;
import com.builds.uimaps.fieldops.FieldOpsVisitsPage;
import com.builds.uimaps.fin.AdminFinanceFinanceSetupPreferencesPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AcceptanceTest {

	public static String BATRemarks = "";

	private String password = "t0n1ght1";
	private String divisionName;

	private String corporateUserId;
	private String corporateUserFirstName;
	private String corporateUserLastName;

	private String divisionalUserId;
	private String divisionalUserFirstName;
	private String divisionalUserLastName;

	private String regionalUserId;
	private String regionalFirstName;
	private String regionalLastName;

	private String franchiseUserId;
	private String franchiseUserFirstName;
	private String franchiseUserLastName;

	private String ownerName;

	FranconnectUtil fc = new FranconnectUtil();
	SmokeCommon com = new SmokeCommon();


	@Test(groups = "BuildAcceptanceTest")
	@TestCase(createdOn = "2018-05-07", updatedOn = "2018-05-07", testCaseDescription = "Verify Listing & Reputation", testCaseId = "Verify_listing_And_Reputation")
	public void verifyListingReputation() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String moduleName = FranconnectUtil.RyverNextLineChar + "Listing And Reputation";

		String text = "";

		try {
			driver = fc.loginpage().login(driver);

			String loc = "";
			String reg = "";

			try {

				com.goToAdmin(driver);

				loc = fc.utobj().generateTestData("Location");
				reg = fc.utobj().generateTestData("Reg");

				com.addAreaRegion(driver, reg);
				com.goToAdmin(driver);
				addFranchise(driver, reg, loc);
				com.goToAdmin(driver);
				com.openUserLink(driver, "Configure Vendasta Listings",
						driver.findElement(By.xpath(".//a[@qat_adminlink='Configure Vendasta Local Listings']")));

				fc.utobj().printTestStep("Select location and Search");
				fc.utobj().selectDropDown(driver, driver.findElement(By.id("franchiseMenu")), loc);

				fc.utobj().clickElement(driver, driver.findElement(By.id("search")));

				fc.utobj().printTestStep("Select Provision from Action Icon");
				// fc.utobj().singleActionIcon(driver, "Provision");
				fc.utobj().actionImgOption(driver, loc, "Provision");

				if (fc.utobj().assertPageSource(driver, "Activated") == false) {
					text = FranconnectUtil.RyverNextLineCharWithBullet + "Unable to Move location to Provision";
				} else {
					fc.utobj().printTestStep("Go to Listing > Home");
					fc.utobj().printTestStep("Click on View Listing");
				}

			} catch (Exception e) {
				text = FranconnectUtil.RyverNextLineCharWithBullet + "Unable to Move location to Provision";
			}

			com.goToAdmin(driver);

			com.openUserLink(driver, "Configure Reputation Management",
					driver.findElement(By.xpath(".//a[@qat_adminlink='Configure Reputation Management']")));

			try {
				fc.utobj().printTestStep("Select location and Search");
				fc.utobj().selectDropDown(driver, driver.findElement(By.id("franchiseMenu")), loc);

				fc.utobj().clickElement(driver, driver.findElement(By.id("search")));

				fc.utobj().printTestStep("Select Start from Action Icon");
				// fc.utobj().singleActionIcon(driver, "Start");
				fc.utobj().actionImgOption(driver, loc, "Start");
				if (fc.utobj().assertPageSource(driver, "Activation Initialized") == false) {
					text = FranconnectUtil.RyverNextLineCharWithBullet + "Unable to Move location to Reputation";
				} else {
					fc.utobj().printTestStep("Go to Reputation > Home");
					fc.utobj().printTestStep("Click on View Reputation");
				}
			} catch (Exception e) {
				
				if(text.equalsIgnoreCase("")){
					text = FranconnectUtil.RyverNextLineCharWithBullet+"Unable to add Listing and Reputation";
				}
				
				text = FranconnectUtil.RyverNextLineCharWithBullet + "Unable to Move location to Reputation";
				if (!text.isEmpty()) {
					fc.utobj().throwsException(e.getMessage());
				}
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			BATRemarks = BATRemarks + FranconnectUtil.RyverNextLineChar + moduleName
					+ FranconnectUtil.RyverNextLineChar + text;
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "BuildAcceptanceTest")
	@TestCase(createdOn = "2018-06-12", updatedOn = "2018-06-12", testCaseDescription = "Verify upload of ArtWork", testCaseId = "Upload_Artwork")
	public void uploadArtwork() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String moduleName = FranconnectUtil.RyverNextLineChar + "Print Ads";

		try {
			driver = fc.loginpage().login(driver);

			com.goToAdmin(driver);

			com.openUserLink(driver, "Manage Print Vendors",
					driver.findElement(By.xpath(".//a[@qat_adminlink='Manage Print Vendors']")));

			String vendorName = fc.utobj().generateTestData("Vendor");

			try {
				fc.utobj().printTestStep("Click Add Vendor and Fill Vendor Details");
				fc.utobj().clickElement(driver, driver.findElement(By.xpath(".//a[contains(@href,'addAbVendor')]")));

				fc.utobj().sendKeys(driver, driver.findElement(By.id("vendorName")), vendorName);
				fc.utobj().sendKeys(driver, driver.findElement(By.id("firstName")),
						fc.utobj().generateTestData("Fname"));
				fc.utobj().sendKeys(driver, driver.findElement(By.id("lastName")),
						fc.utobj().generateTestData("Lname"));
				fc.utobj().sendKeys(driver, driver.findElement(By.id("address")),
						fc.utobj().generateTestData("Address"));
				fc.utobj().sendKeys(driver, driver.findElement(By.id("city")), "NYC");
				fc.utobj().selectDropDown(driver, driver.findElement(By.id("country")), "USA");
				fc.utobj().sendKeys(driver, driver.findElement(By.id("zipcode")),
						fc.utobj().generateRandomNumber6Digit());
				fc.utobj().selectDropDown(driver, driver.findElement(By.id("stateID")), "Alaska");
				fc.utobj().sendKeys(driver, driver.findElement(By.id("phone1")),
						fc.utobj().generateRandomNumber6Digit());

				fc.utobj().sendKeys(driver, driver.findElement(By.id("emailID")), "test@franconnect.net");
			} catch (Exception e) {
				fc.utobj().throwsException("Unable to fill Vendor Details");
			}

			try {
				fc.utobj().printTestStep("Click on Save Button");
				fc.utobj().clickElement(driver, driver.findElement(By.id("Submit")));
			} catch (Exception e) {
				fc.utobj().throwsException("Unable to click on save button");
			}

			com.goToAdmin(driver);

			com.openUserLink(driver, "Artwork Management",
					driver.findElement(By.xpath(".//a[@qat_adminlink='Artwork Management']")));

			String docTitle = fc.utobj().generateTestData("Doc");
			try {
				fc.utobj().printTestStep("Click Add Artwork and Fill Details");
				fc.utobj().clickElement(driver, driver.findElement(By.xpath(".//a[contains(@onclick,'addDocument')]")));
				fc.utobj().sendKeys(driver, driver.findElement(By.id("documentTitle")), docTitle);
				// fc.utobj().selectDropDown(driver,
				// driver.findElement(By.id("ParentFolder")), folderName);

				fc.utobj().sendKeys(driver, driver.findElement(By.id("docFile")),
						fc.utobj().getFilePathFromTestData("taskFile.pdf"));
				
				fc.utobj().selectDropDown(driver, driver.findElement(By.id("supplierName")), vendorName);
				
				fc.utobj().sendKeys(driver, driver.findElement(By.id("artworkWeight")), "10");
				fc.utobj().printTestStep("Click on Save Button");
				fc.utobj().clickElement(driver, driver.findElement(By.id("saveButton")));
			} catch (Exception e) {
				fc.utobj().throwsException("Unable to Fill Artwork Details " + e.getMessage());
			}

			try {
				fc.utobj().printTestStep("Accept message : Do you still want to upload the Artwork");
				fc.utobj().clickElement(driver,
						driver.findElement(By.xpath(".//input[@value='Yes' and @type='button']")));
			} catch (Exception e) {

			}

			try {
				fc.utobj().printTestStep("Search if artwork is added");
				fc.utobj().sendKeys(driver, driver.findElement(By.id("searchAdminAMDocuments")), docTitle);
				fc.utobj().clickElement(driver, driver.findElement(By.xpath(".//img[contains(@src,'sIcon.png')]")));
			} catch (Exception e) {
				fc.utobj().throwsException("Not able to search artwork " + e.getMessage());
			}

			if (fc.utobj().assertPageSource(driver, docTitle) == false) {
				fc.utobj().throwsException("Artwork not found");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			BATRemarks = BATRemarks + FranconnectUtil.RyverNextLineChar + moduleName
					+ FranconnectUtil.RyverNextLineCharWithBullet + "Upload Artwork";
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "BuildAcceptanceTest")
	@TestCase(createdOn = "2018-05-07", updatedOn = "2018-05-07", testCaseDescription = "CRM > Lead Summary > Add Lead", testCaseId = "CRM_Add_Lead_And_Contact")
	public void leadInfopage() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String moduleName = FranconnectUtil.RyverNextLineChar + "CRM";
		String text = "";

		try {
			driver = fc.loginpage().login(driver);

			// This will open the CRM Module
			FCHomePageTest fcHomePageTest = fc.commonMethods().getModules();
			Leadgetset lead = new Leadgetset();
			CRMModule crm = new CRMModule();
			// Go to Lead in CRM

			fc.utobj().printTestStep("Open CRM Lead Page and Click on Add lead");
			crm.crm_common().openCRMLeadsPage(fcHomePageTest, driver).clickAddLead(driver);

			lead = crm.crm_common().getDefaultDataForLead(lead);

			try {
				Addleadpage addlead = new Addleadpage();
				fc.utobj().printTestStep("Fill Lead Details");
				addlead.fillLeadDetails(driver, lead);
				fc.utobj().printTestStep("Add Lead");
				new SmokeTest().enterDataCustomField(driver);
				addlead.save(driver);
				if (fc.utobj().assertNotInPageSource(driver, lead.getFirstname() + " " + lead.getLastname()) == false) {
					text = FranconnectUtil.RyverNextLineCharWithBullet + "Lead is not added";
				}
			} catch (Exception e) {
				text = FranconnectUtil.RyverNextLineCharWithBullet + "Lead is not added";
			}

			fc.utobj().printTestStep("Open CRM Lead Page and Click on Add Contact");
			crm.crm_common().openCRMContactsPage(fcHomePageTest, driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			
			if(text.equalsIgnoreCase("")){
				text = FranconnectUtil.RyverNextLineCharWithBullet + "Unable to add lead";
			}
			
			BATRemarks = BATRemarks + FranconnectUtil.RyverNextLineChar + moduleName
					+ FranconnectUtil.RyverNextLineChar + text;
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "BuildAcceptanceTest" })
	@TestCase(createdOn = "2018-06-04", updatedOn = "2018-06-04", testCaseDescription = "Verify The Corporate User is getting created", testCaseId = "User_Management_Create_All_Users")
	private void addUsers() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		boolean scenario = true;

		String moduleName = FranconnectUtil.RyverNextLineChar + "User Management";
		String text = "";

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			String franchiseName = fc.utobj().generateTestData("Franchise");
			String divisionName = fc.utobj().generateTestData("Division");

			// Add Corporate User
			com.goToAdmin(driver);
			try {
				addCorporateUser(driver);
			} catch (Exception e) {
				scenario = false;
				text = text + FranconnectUtil.RyverNextLineCharWithBullet + "Corporate user is not getting added.";
			}

			// Add Divisional User
			try {
				fc.loginpage().login(driver);
				com.goToAdmin(driver);
				com.createDivision(driver, divisionName);
				com.goToAdmin(driver);
				addDivisionalUser(driver);
			} catch (Exception e) {
				scenario = false;
				text = text + FranconnectUtil.RyverNextLineCharWithBullet + "Divisional user is not getting added.";
			}

			// Add Regional User
			String regionName = null;
			try {
				fc.loginpage().login(driver);
				com.goToAdmin(driver);
				regionName = com.addAreaRegion(driver, fc.utobj().generateTestData("Region"));
			} catch (Exception e) {
				scenario = false;
				text = text + FranconnectUtil.RyverNextLineCharWithBullet + "Region is not getting added.";
			}

			try {
				com.goToAdmin(driver);
				addRegionalUser(driver, regionName);
			} catch (Exception e) {
				scenario = false;
				text = text + FranconnectUtil.RyverNextLineCharWithBullet + "Regional user is not getting added.";
			}

			// Add Franchise User
			try {
				fc.loginpage().login(driver);
				com.goToAdmin(driver);
				ownerName = addFranchise(driver, regionName, franchiseName);
			} catch (Exception e) {
				scenario = false;
				text = text + FranconnectUtil.RyverNextLineCharWithBullet + "Franchise is not getting added.";
			}

			try {
				com.goToAdmin(driver);
				addFranchiseUser(driver, franchiseName);
			} catch (Exception e) {
				scenario = false;
				text = text + FranconnectUtil.RyverNextLineCharWithBullet + "Franchise User is not getting added.";
			}

			if (scenario == true) {
				text = "User Management: " + FranconnectUtil.RyverNextLineChar + BATRemarks;
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			BATRemarks = BATRemarks + FranconnectUtil.RyverNextLineChar + moduleName
					+ FranconnectUtil.RyverNextLineChar + text;
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	private void addFranchiseUser(WebDriver driver, String franchiseName) throws Exception {
		com.openUserLink(driver, "Franchise User",
				driver.findElement(By.xpath(".//a[@qat_adminlink='Franchise User']")));

		try {
			fc.utobj().clickElement(driver,
					".//input[@value='Add Franchise User' or contains(@onclick,'usersAddFranchiseeUser')]");
		} catch (Exception e) {
			fc.utobj().throwsException("Not able to click on Add Franchise User");
		}

		String userName = fillFranchiseUserDetails(driver, franchiseName);

		searchUserOnManageUserPage(driver, userName);
		com.logout(driver);
		fc.loginpage().loginWithParameter(driver, franchiseUserId, password);
		fc.utobj().assertPageSource(driver, userName);
	}

	private String fillFranchiseUserDetails(WebDriver driver, String franchiseName) throws Exception {

		franchiseUserId = fc.utobj().generateTestData("test");
		franchiseUserFirstName = fc.utobj().generateTestData("test");
		franchiseUserLastName = fc.utobj().generateTestData("test");

		String userName = franchiseUserFirstName + " " + franchiseUserLastName;
		Thread.sleep(2000);
		fc.utobj().selectDropDown(driver, driver.findElement(By.id("franchiseeNo")), franchiseName);
		fc.utobj().clickElement(driver, driver.findElement(By.name("addUser")));

		fc.utobj().sendKeys(driver, driver.findElement(By.id("userName")), franchiseUserId);
		fc.utobj().sendKeys(driver, driver.findElement(By.xpath(".//input[@id='password' and @type='password']")),
				password);
		fc.utobj().sendKeys(driver,
				driver.findElement(By.xpath(".//input[@id='confirmPassword' and @type='password']")), password);

		try {
			fc.utobj().selectValFromMultiSelect(driver, driver.findElement(By.id("ms-parentroleID")),
					"Default Franchise Role");
		} catch (Exception e) {
			fc.utobj().selectValFromMultiSelect(driver, driver.findElement(By.id("ms-parentroleID")),
					"Default Brand Role");
		}

		try {
			fc.utobj().selectDropDown(driver, driver.findElement(By.id("salutation")), "Mr.");
		} catch (Exception e) {

		}

		try {
			fc.utobj().sendKeys(driver, driver.findElement(By.id("firstName")), franchiseUserFirstName);
			fc.utobj().sendKeys(driver, driver.findElement(By.id("lastName")), franchiseUserLastName);

		} catch (Exception e) {
			userName = ownerName;
		}

		fc.utobj().sendKeys(driver, driver.findElement(By.id("city")), "NYC");
		fc.utobj().selectDropDown(driver, driver.findElement(By.id("country")), "USA");
		fc.utobj().selectDropDown(driver, driver.findElement(By.id("state")), "Alabama");
		fc.utobj().sendKeys(driver, driver.findElement(By.id("phone1")), "9966559966");
		fc.utobj().sendKeys(driver, driver.findElement(By.id("email")), "test@franconnect.com");
		fc.utobj().clickElement(driver, driver.findElement(By.id("Submit")));
		fc.utobj().printTestStep("Verify The Added Franchise User");
		return userName;
	}

	private String addFranchise(WebDriver driver, String regionName, String franchiseName) throws Exception {

		String ownerFirstName = fc.utobj().generateTestData("Test");
		String ownerLastName = fc.utobj().generateTestData("Test");
		String ownerName = ownerFirstName + " " + ownerLastName;

		try {
			fc.utobj().printTestStep("Click on Add New Franchise Location");
			fc.utobj().clickElement(driver,
					driver.findElement(By.xpath(".//a[@qat_adminlink='Add New Franchise Location']")));
		} catch (Exception e) {
			fc.utobj().throwsException("Unable to click on Add New Franchise Location Link");
		}
		try {
			fc.utobj().printTestStep("Fill Location Details");
			fc.utobj().sendKeys(driver, driver.findElement(By.id("franchiseeName")), franchiseName);
			fc.utobj().sendKeys(driver, driver.findElement(By.id("centerName")), "TestCenter");
			fc.utobj().selectDropDown(driver, driver.findElement(By.id("areaID")), regionName);
			fc.utobj().selectDropDown(driver, driver.findElement(By.id("storeTypeId")), "Default");
			fc.utobj().sendKeys(driver, driver.findElement(By.id("openingDate")), "06/28/2018");
			fc.utobj().sendKeys(driver, driver.findElement(By.id("address")), "Address");
			fc.utobj().sendKeys(driver, driver.findElement(By.id("city")), "NYC");
			fc.utobj().selectDropDown(driver, driver.findElement(By.id("countryID")), "USA");
			fc.utobj().selectDropDown(driver, driver.findElement(By.id("regionNo")), "New York");
			fc.utobj().sendKeys(driver, driver.findElement(By.id("zipcode")), fc.utobj().generateRandomNumber6Digit());
			fc.utobj().sendKeys(driver, driver.findElement(By.id("storePhone")), "9988776655");
			fc.utobj().sendKeys(driver, driver.findElement(By.id("ownerFirstName")), ownerFirstName);
			fc.utobj().sendKeys(driver, driver.findElement(By.id("ownerLastName")), ownerLastName);
		} catch (Exception e) {
			fc.utobj().throwsException("Unable to Fill the Franchise Location Details");
		}

		try {
			fc.utobj().printTestStep("Click on Save Button");
			fc.utobj().clickElement(driver, driver.findElement(By.id("Submit")));
		} catch (Exception e) {
			fc.utobj().throwsException("Unable to click on Save");
		}

		return ownerName;

	}

	private void fillDivisionalUserDetails(WebDriver driver) throws Exception {
		divisionalUserId = fc.utobj().generateTestData("test");
		divisionalUserFirstName = fc.utobj().generateTestData("test");
		divisionalUserLastName = fc.utobj().generateTestData("test");

		fc.utobj().sendKeys(driver, driver.findElement(By.id("userName")), divisionalUserId);
		fc.utobj().sendKeys(driver, driver.findElement(By.xpath(".//input[@id='password' and @type='password']")),
				password);
		fc.utobj().sendKeys(driver,
				driver.findElement(By.xpath(".//input[@id='confirmPassword' and @type='password']")), password);

		try {
			fc.utobj().selectValFromMultiSelect(driver, driver.findElement(By.id("ms-parentroleID")),
					"Default Division Role");
		} catch (Exception e) {
			fc.utobj().selectValFromMultiSelect(driver, driver.findElement(By.id("ms-parentroleID")),
					"Default Brand Role");
		}

		fc.utobj().selectValFromMultiSelect(driver, driver.findElement(By.id("ms-parentdivision")), divisionName);

		fc.utobj().sendKeys(driver, driver.findElement(By.id("firstName")), divisionalUserFirstName);
		fc.utobj().sendKeys(driver, driver.findElement(By.id("lastName")), divisionalUserLastName);

		fc.utobj().sendKeys(driver, driver.findElement(By.id("city")), "NYC");
		fc.utobj().selectDropDown(driver, driver.findElement(By.id("country")), "USA");
		fc.utobj().selectDropDown(driver, driver.findElement(By.id("state")), "Alabama");
		fc.utobj().sendKeys(driver, driver.findElement(By.id("phone1")), "9966559966");
		fc.utobj().sendKeys(driver, driver.findElement(By.id("email")), "test@franconnect.com");

		fc.utobj().clickElement(driver, driver.findElement(By.id("Submit")));

		fc.utobj().printTestStep("Verify The Added Divisional User");

	}

	private void addCorporateUser(WebDriver driver) throws Exception {

		com.openUserLink(driver, "Corporate User",
				driver.findElement(By.xpath(".//a[@qat_adminlink='Corporate User']")));

		try {
			fc.utobj().clickElement(driver,
					".//input[@value='Add Corporate User' or contains(@onclick,'usersCorporateForm')]");
		} catch (Exception e) {
			fc.utobj().throwsException("Not able to click on Add Corporate User");
		}

		fillCorporateDetails(driver);

		searchUserOnManageUserPage(driver, corporateUserFirstName + " " + corporateUserLastName);
		com.logout(driver);
		fc.loginpage().loginWithParameter(driver, corporateUserId, password);
		fc.utobj().assertPageSource(driver, corporateUserFirstName + " " + corporateUserLastName);

	}

	private void addDivisionalUser(WebDriver driver) throws Exception {
		com.openUserLink(driver, "Divisional User",
				driver.findElement(By.xpath(".//a[@qat_adminlink='Divisional Users']")));

		try {
			fc.utobj().clickElement(driver, driver.findElement(By.xpath(
					".//input[contains((@value),'Divisional User') or contains(@onclick,'usersCorporateForm')]")));
		} catch (Exception e) {
			fc.utobj().throwsException("Not able to click on Add Divisional User");
		}

		fillDivisionalUserDetails(driver);

		searchUserOnManageUserPage(driver, divisionalUserFirstName + " " + divisionalUserLastName);
		com.logout(driver);
		fc.loginpage().loginWithParameter(driver, divisionalUserId, password);
		fc.utobj().assertPageSource(driver, divisionalUserFirstName + " " + divisionalUserLastName);

	}

	private void addRegionalUser(WebDriver driver, String regionName) throws Exception {

		com.openUserLink(driver, "Regional User", driver.findElement(By.xpath(".//a[@qat_adminlink='Regional User']")));

		try {
			fc.utobj().clickElement(driver,
					".//input[@value='Add Regional User' or contains(@onclick,'usersRegionalForm')]");
		} catch (Exception e) {
			fc.utobj().throwsException("Not able to click on Add Regional User");
		}

		fillRegionalDetails(driver, regionName);

		searchUserOnManageUserPage(driver, regionalFirstName + " " + regionalLastName);
		com.logout(driver);
		fc.loginpage().loginWithParameter(driver, regionalUserId, password);
		fc.utobj().assertPageSource(driver, regionalFirstName + " " + regionalLastName);

	}

	private void fillRegionalDetails(WebDriver driver, String regionName) throws Exception {

		fc.utobj().printTestStep("Filling the User Details page.");

		regionalFirstName = fc.utobj().generateTestData("user");
		regionalLastName = fc.utobj().generateTestData("user");
		regionalUserId = fc.utobj().generateTestData("user");

		fc.utobj().sendKeys(driver, driver.findElement(By.id("userName")), regionalUserId);
		fc.utobj().sendKeys(driver, driver.findElement(By.xpath(".//input[@id='password' and @type='password']")),
				password);
		fc.utobj().sendKeys(driver, driver.findElement(By.xpath(".//input[@id='confirmPassword']")), password);
		fc.utobj().selectValFromMultiSelect(driver, driver.findElement(By.id("ms-parentroleID")),
				"Default Regional Role");

		fc.utobj().selectDropDown(driver, driver.findElement(By.id("region")), regionName);

		fc.utobj().sendKeys(driver, driver.findElement(By.id("firstName")), regionalFirstName);
		fc.utobj().sendKeys(driver, driver.findElement(By.id("lastName")), regionalLastName);
		fc.utobj().sendKeys(driver, driver.findElement(By.id("city")), "NYC");
		fc.utobj().selectDropDown(driver, driver.findElement(By.id("country")), "USA");
		fc.utobj().selectDropDown(driver, driver.findElement(By.id("state")), "Kansas");
		fc.utobj().sendKeys(driver, driver.findElement(By.id("phone1")), "9988776655");
		fc.utobj().sendKeys(driver, driver.findElement(By.id("email")),
				fc.utobj().generateTestData("automation@franconnect.net"));

		fc.utobj().printTestStep("Click on Add Button");
		fc.utobj().clickElement(driver, driver.findElement(By.id("Submit")));
	}

	private void fillCorporateDetails(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Filling the User Details page.");

		corporateUserFirstName = fc.utobj().generateTestData("user");
		corporateUserLastName = fc.utobj().generateTestData("user");
		corporateUserId = fc.utobj().generateTestData("user");

		fc.utobj().sendKeys(driver, driver.findElement(By.id("userName")), corporateUserId);
		fc.utobj().sendKeys(driver, driver.findElement(By.xpath(".//input[@id='password' and @type='password']")),
				password);
		fc.utobj().sendKeys(driver, driver.findElement(By.xpath(".//input[@id='confirmPassword']")), password);
		fc.utobj().selectValFromMultiSelect(driver, driver.findElement(By.id("ms-parentroleID")),
				"Corporate Administrator");
		fc.utobj().sendKeys(driver, driver.findElement(By.id("firstName")), corporateUserFirstName);
		fc.utobj().sendKeys(driver, driver.findElement(By.id("lastName")), corporateUserLastName);
		fc.utobj().sendKeys(driver, driver.findElement(By.id("city")), "NYC");
		fc.utobj().selectDropDown(driver, driver.findElement(By.id("country")), "USA");
		fc.utobj().selectDropDown(driver, driver.findElement(By.id("state")), "Kansas");
		fc.utobj().sendKeys(driver, driver.findElement(By.id("phone1")), "9988776655");
		fc.utobj().sendKeys(driver, driver.findElement(By.id("email")), "automation@franconnect.net");

		fc.utobj().printTestStep("Click on Add Button");
		fc.utobj().clickElement(driver, driver.findElement(By.id("Submit")));
	}

	private void searchUserOnManageUserPage(WebDriver driver, String userName) throws Exception {

		fc.utobj().printTestStep("Search User on Manage User Page");
		fc.utobj().printTestStep("Enter User Name in Search text box");
		fc.utobj().sendKeys(driver, driver.findElement(By.id("search")), userName);
		fc.utobj().printTestStep("Click on Search Button");
		fc.utobj().clickElement(driver, driver.findElement(By.id("searchButton")));

		fc.utobj().printTestStep("Verify if the user is present in searched record");
		if (fc.utobj().assertLinkText(driver, userName) == false) {
			fc.utobj().throwsException("User not found in Searched Record");
		} else {
			fc.utobj().printTestStep("User found in Searched Record");
		}
	}

	@Test(groups = { "BuildAcceptanceTest"})
	@TestCase(createdOn = "2018-06-08", updatedOn = "2018-06-04", testCaseDescription = "Verify The Task Checklist is getting created", testCaseId = "Opener_Task_CheckList_Creation")
	private void addOpenerTask() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		String moduleName = FranconnectUtil.RyverNextLineChar + "Opener";

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			String responsibility = com.addResponsibleDepartment(driver);

			com.goToAdmin(driver);

			String region = com.addAreaRegion(driver, fc.utobj().generateTestData("Reg"));

			try {
				fc.utobj().printTestStep("Click on Store Opener");
				fc.utobj().clickElement(driver, driver.findElement(By.xpath(".//li[@id='module_storeopener']/a")));
			} catch (Exception e) {
				fc.utobj().throwsException("Not able to move to Opener Module.");
			}

			try {
				fc.utobj().printTestStep("Click on Store Summary sub tab");
				fc.utobj().clickElement(driver, driver.findElement(By.xpath(".//a[@qat_submodule='Store Summary']")));
			} catch (Exception e) {
				fc.utobj().throwsException("Not able to move to Store Summary Sub Tab");
			}

			try {
				fc.utobj().printTestStep("Click on Add New Franchise Location link");
				fc.utobj().clickElement(driver, driver.findElement(By
						.xpath(".//table[@id='exportHtmlTableViewbradcum']//a[contains(@href,'locFranchiseeForm')]")));
			} catch (Exception e) {
				fc.utobj().throwsException("Unable to click on 	Add New Franchise Location.");
			}

			String fid = fc.utobj().generateTestData("Test");

			try {
				fc.utobj().printTestStep("Fill Franchise details");
				fc.utobj().sendKeys(driver, driver.findElement(By.id("franchiseeName")), fid);
				fc.utobj().sendKeys(driver, driver.findElement(By.id("centerName")), "TestCenter");
				fc.utobj().selectDropDown(driver, driver.findElement(By.id("areaID")), region);
				fc.utobj().selectDropDown(driver, driver.findElement(By.id("storeTypeId")), "Default");
				fc.utobj().sendKeys(driver, driver.findElement(By.id("grandStoreOpeningDate")), "06/28/2018");
				fc.utobj().sendKeys(driver, driver.findElement(By.id("city")), "NYC");
				fc.utobj().selectDropDown(driver, driver.findElement(By.id("countryID")), "USA");
				fc.utobj().selectDropDown(driver, driver.findElement(By.id("regionNo")), "New York");
				fc.utobj().sendKeys(driver, driver.findElement(By.id("storePhone")), "9988776655");
				fc.utobj().sendKeys(driver, driver.findElement(By.id("ownerFirstName")), "Testf1");
				fc.utobj().sendKeys(driver, driver.findElement(By.id("ownerLastName")), "Testl1");
				new SmokeTest().enterDataCustomField(driver);
				fc.utobj().clickElement(driver, driver.findElement(By.id("Submit")));
			} catch (Exception e) {
				fc.utobj().throwsException("Unable to Fill Francshise Details page");
			}

			try {
				fc.utobj().printTestStep("Click on Store Summary sub tab");
				fc.utobj().clickElement(driver, driver.findElement(By.xpath(".//a[@qat_submodule='Store Summary']")));
			} catch (Exception e) {
				fc.utobj().throwsException("Unable to Click on Store Summary sub tab.");
			}

			try {
				fc.utobj().printTestStep("Click on Show filter and search the store.");
				fc.utobj().clickElement(driver, driver.findElement(By.id("showFilter")));
				fc.utobj().selectValFromMultiSelect(driver, driver.findElement(By.id("ms-parentsearchStoreNo")), fid);
				fc.utobj().printTestStep("Open Store by clicking on the store name");
				fc.utobj().clickElement(driver, driver.findElement(By.id("search")));
				fc.utobj().sleep();
				fc.utobj().clickElement(driver, driver.findElement(By.linkText(fid)));
			} catch (Exception e) {
				fc.utobj().throwsException("Unable to open the Store : " + e.getMessage());
			}

			try {
				fc.utobj().printTestStep("Click on Task Checklist tab");
				fc.utobj().clickElement(driver, driver.findElement(By.xpath(".//a[@qat_tabname='Task Checklist']")));
			} catch (Exception e) {
				fc.utobj().throwsException("Unable to Click Task Checklist Tab");
			}

			try {
				fc.utobj().printTestStep("Click on Add Checklist");
				fc.utobj().clickElement(driver, driver.findElement(
						By.xpath(".//table[@id='exportHtmlTablesmTab']//a[contains(@href,'addSMTaskChecklist')]")));
			} catch (Exception e) {
				fc.utobj().throwsException("Unable to Click on Add Checklist");
			}

			String checklistName = fc.utobj().generateTestData("Checklist");

			try {
				fc.utobj().printTestStep("Fill checklist details");
				fc.utobj().sendKeys(driver, driver.findElement(By.id("smUserTaskChecklist_0task")), checklistName);

				fc.utobj().selectValFromMultiSelect(driver,
						driver.findElement(By.id("ms-parentsmUserTaskChecklist_0responsibilityArea")), responsibility);

				fc.utobj().selectDropDown(driver, driver.findElement(By.id("smUserTaskChecklist_0franchiseeAccess")),
						"Update Status");
				fc.utobj().selectDropDown(driver, driver.findElement(By.name("smUserTaskChecklist_0referenceParent")),
						"None (Timeless)");
				fc.utobj().selectValFromMultiSelect(driver,
						driver.findElement(By.id("ms-parentsmUserTaskChecklist_0contact")),
						"FranConnect Administrator");
			} catch (Exception e) {
				fc.utobj().throwsException("Unable to fill checklist details " + e.getMessage());
			}

			try {
				fc.utobj().printTestStep("Click on submit button and verify the checklist is created");
				fc.utobj().clickElement(driver, driver.findElement(By.name("Submit")));
			} catch (Exception e) {
				fc.utobj().throwsException("Unable to click submit button.");
			}

			if (fc.utobj().assertPageSource(driver, checklistName) == false) {
				fc.utobj().throwsException("Checklist was not added.");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			BATRemarks = BATRemarks + FranconnectUtil.RyverNextLineChar + moduleName
					+ FranconnectUtil.RyverNextLineCharWithBullet + "Task CheckList Creation";
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "BuildAcceptanceTest" })
	@TestCase(createdOn = "2018-06-08", updatedOn = "2018-06-08", testCaseDescription = "Verify The Sales Lead is getting created", testCaseId = "Sales_Lead_Creation")
	private void addSalesLead() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String moduleName = FranconnectUtil.RyverNextLineChar + "Sales Module";

		try {

			driver = fc.loginpage().login(driver);

			try {
				fc.utobj().printTestStep("Click on Sales Module");
				fc.utobj().clickElement(driver, driver.findElement(By.xpath(".//li[@id='module_fs']/a")));
			} catch (Exception e) {
				fc.utobj().throwsException("Unable to click on sales module");
			}

			try {
				fc.utobj().printTestStep("Click on Lead Management Tab");
				fc.utobj().clickElement(driver, driver.findElement(By.xpath(".//a[@qat_submodule='Lead Management']")));
			} catch (Exception e) {
				fc.utobj().throwsException("Unable to click on Lead Management sub tab");
			}

			try {
				fc.utobj().printTestStep("Click on Add lead link");
				fc.utobj().clickElement(driver, driver.findElement(By.xpath(".//a[contains(@href,'addAnotherLead')]")));
			} catch (Exception e) {
				fc.utobj().throwsException("Unable to click on Add lead");
			}

			String fName = fc.utobj().generateTestData("Fname");
			String lName = fc.utobj().generateTestData("Lname");

			try {
				fc.utobj().printTestStep("Fill lead Info");
				fc.utobj().sendKeys(driver, driver.findElement(By.id("firstName")), fName);
				fc.utobj().sendKeys(driver, driver.findElement(By.id("lastName")), lName);
				fc.utobj().sendKeys(driver, driver.findElement(By.id("emailID")), "test@franconnect.net");
				fc.utobj().selectDropDown(driver, driver.findElement(By.id("leadOwnerID")),
						"FranConnect Administrator");
				fc.utobj().selectDropDown(driver, driver.findElement(By.id("leadSource2ID")), "Import");
				try {
					fc.utobj().selectDropDown(driver, driver.findElement(By.id("leadSource3ID")), "None");
				} catch (Exception e) {
					fc.utobj().selectDropDownByIndex(driver, driver.findElement(By.id("leadSource3ID")), 1);
				}
				fc.utobj().clickElement(driver, driver.findElement(By.id("assignAutomaticCampaign1")));

			} catch (Exception e) {
				fc.utobj().throwsException("Unable to fill lead details " + e.getMessage());
			}

			try {
				fc.utobj().printTestStep("Click on Submit Button");
				new SmokeTest().enterDataCustomField(driver);
				fc.utobj().clickElement(driver, driver.findElement(By.id("Submit")));
			} catch (Exception e) {
				fc.utobj().throwsException("Unable to click on submit button");
			}

			fc.utobj().printTestStep("Verify that the lead is added : " + fName + " " + lName);

			if (!fc.utobj().assertPageSource(driver, fName + " " + lName)) {
				fc.utobj().throwsException("Lead is Not getting added");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			BATRemarks = BATRemarks + FranconnectUtil.RyverNextLineChar + moduleName
					+ FranconnectUtil.RyverNextLineCharWithBullet + "Sales Lead Creation";
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "BuildAcceptanceTest"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Move In Development Location to Open", testCaseId = "InfoMgr_InDevelopment_To_Open")
	private void moveInDevelopmentLocationToFranchise() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String moduleName = FranconnectUtil.RyverNextLineChar + "Infomgr";
		try {
			driver = fc.loginpage().login(driver);

			com.goToAdmin(driver);

			String franchiseName = fc.utobj().generateTestData("Fran");
			String region = fc.utobj().generateTestData("Reg");
			com.addAreaRegion(driver, region);

			try {
				fc.utobj().printTestStep("Click on InfoMgr module");
				fc.utobj().clickElement(driver, driver.findElement(By.id("module_fim")));
			} catch (Exception e) {
				fc.utobj().throwsException("Unable to click on Infomgr module");
			}

			try {
				fc.utobj().printTestStep("Click on In Development tab");
				fc.utobj().clickElement(driver, driver.findElement(By.xpath(".//a[@qat_submodule='In Development']")));
			} catch (Exception e) {
				fc.utobj().throwsException("Unable to click on In Development tab");
			}

			try {
				fc.utobj().printTestStep("Click on Link : Add In Development Location");
				fc.utobj().clickElement(driver,
						driver.findElement(By.xpath(".//a[contains(@href,'subMenu=fimInDevelopment&newStore')]")));
			} catch (Exception e) {
				fc.utobj().throwsException("Unable to click on Add In Development Location");
			}

			String fid = fc.utobj().generateTestData("Fid");

			try {
				fc.utobj().printTestStep("Fill In Development details");
				fc.utobj().sendKeys(driver, driver.findElement(By.id("franchiseeName")), franchiseName);
				fc.utobj().sendKeys(driver, driver.findElement(By.id("centerName")), fid);
				fc.utobj().selectDropDown(driver, driver.findElement(By.id("areaID")), region);
				fc.utobj().selectDropDown(driver, driver.findElement(By.id("storeTypeId")), "Default");
				fc.utobj().sendKeys(driver, driver.findElement(By.id("grandStoreOpeningDate")), "06/28/2018");
				fc.utobj().sendKeys(driver, driver.findElement(By.id("city")), "NYC");
				fc.utobj().selectDropDown(driver, driver.findElement(By.id("countryID")), "USA");
				fc.utobj().selectDropDown(driver, driver.findElement(By.id("regionNo")), "New York");

				fc.utobj().sendKeys(driver, driver.findElement(By.id("storePhone")), "9988776655");
				fc.utobj().sendKeys(driver, driver.findElement(By.id("ownerFirstName")), "Testf1");
				fc.utobj().sendKeys(driver, driver.findElement(By.id("ownerLastName")), "Testl1");
			} catch (Exception e) {
				fc.utobj().throwsException("Unable to Fill In Development details");
			}

			try {
				fc.utobj().printTestStep("Click on Submit Button");
				new SmokeTest().enterDataCustomField(driver);
				fc.utobj().clickElement(driver, driver.findElement(By.id("Submit")));
			} catch (Exception e) {
				fc.utobj().throwsException("Unable to submit Location details");
			}
			try {
				fc.utobj().printTestStep("Click on In Development tab");
				fc.utobj().clickElement(driver, driver.findElement(By.xpath(".//a[@qat_submodule='In Development']")));
			} catch (Exception e) {
				fc.utobj().throwsException("Unable to click on In Development tab");
			}

			try {
				fc.utobj().printTestStep("Search Franchise Location and Click to Open the Details");
				fc.utobj().clickElement(driver, driver.findElement(By.id("showFilter")));
				fc.utobj().selectValFromMultiSelect(driver, driver.findElement(By.id("ms-parentfilterfranchiseeNo")),
						franchiseName);
				fc.utobj().clickElement(driver, driver.findElement(By.id("search")));

				fc.utobj().clickLink(driver, franchiseName);
			} catch (Exception e) {
				fc.utobj().throwsException("Unable to search Location");
			}

			try {
				fc.utobj().printTestStep("Select Mark As Open");
				fc.utobj().selectDropDown(driver, driver.findElement(By.id("selectHistory")), "Mark As Open");
			} catch (Exception e) {
				fc.utobj().throwsException("Unable to Select Mark As Open");
			}

			try {
				fc.utobj().printTestStep("Accept to move location from In Development and Move to Franchise Tab");
				fc.utobj().acceptAlertBox(driver);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().clickElement(driver,
						driver.findElement(By.xpath(".//input[@class='cm_new_button' and contains(@value,'OK')]")));
				fc.utobj().switchFrameToDefault(driver);
			} catch (Exception e) {
				fc.utobj().throwsException("Unable to move location to Franchise tab");
			}

			try {
				fc.utobj().printTestStep("Search Franchise Location and Click to Open the Details");
				fc.utobj().clickElement(driver, driver.findElement(By.id("showFilter")));
				fc.utobj().selectValFromMultiSelect(driver, driver.findElement(By.id("ms-parentfilterfranchiseeNo")),
						franchiseName);
				fc.utobj().clickElement(driver, driver.findElement(By.id("search")));

				fc.utobj().clickLink(driver, franchiseName);
			} catch (Exception e) {
				fc.utobj().throwsException("Unable to search Location");
			}

			try {
				fc.utobj().printTestStep("Click on Center Info Tab");
				fc.utobj().clickElement(driver, driver.findElement(By.xpath(".//a[@qat_tabname='Center Info']")));
			} catch (Exception e) {
				fc.utobj().throwsException("Unable to click on Center Info Tab");
			}

			try {
				fc.utobj().printTestStep("Click on Modify Link");
				fc.utobj().clickElement(driver, driver.findElement(By.xpath(".//a[contains(@href,'modify(')]")));
			} catch (Exception e) {
				fc.utobj().throwsException("Unable to modify the lead");
			}

			try {
				fc.utobj().printTestStep("Click on Save button");
				fc.utobj().clickElementByJS(driver, driver.findElement(By.name("Submit")));
			} catch (Exception e) {
				fc.utobj().throwsException("Unable to Click on save button");
			}

			if (fc.utobj().assertPageSource(driver, fid) == false) {
				fc.utobj().throwsException("Unable to Save the modified Location Details");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			BATRemarks = BATRemarks + FranconnectUtil.RyverNextLineChar + moduleName
					+ FranconnectUtil.RyverNextLineCharWithBullet + "Move Location From InDevelopment To Open";
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "BuildAcceptanceTest" })
	@TestCase(createdOn = "2018-06-04", updatedOn = "2018-06-04", testCaseDescription = "Verify Library Document is getting added.", testCaseId = "Add_Library_Document")
	private void addDocumentLibrary() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		String moduleName = FranconnectUtil.RyverNextLineChar + "TheHub";

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			com.goToAdmin(driver);

			String folderName = fc.utobj().generateTestData("AaaFolder");

			try {
				fc.utobj().printTestStep("Click on Library Link");
				fc.utobj().clickElement(driver, driver.findElement(By.xpath(".//a[@qat_adminlink='Library']")));
			} catch (Exception e) {
				fc.utobj().throwsException("Unable to Click on Library Link");
			}

			try {
				fc.utobj().printTestStep("Click on Add Folder");
				fc.utobj().clickElement(driver, driver.findElement(By.xpath(".//img[contains(@src,'icnAd.png')]")));
			} catch (Exception e) {
				fc.utobj().printTestStep("Unable to click on Add Folder");
			}

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			try {
				fc.utobj().printTestStep("Fill Folder Details");
				fc.utobj().sendKeys(driver, driver.findElement(By.id("folderName")), folderName);
				fc.utobj().sendKeys(driver, driver.findElement(By.id("folderSummary")), "Folder Summary");
				fc.utobj().clickElement(driver, driver.findElement(By.name("Submit")));
				fc.utobj().clickElement(driver, driver.findElement(By.name("SubmitButton")));
			} catch (Exception e) {
				fc.utobj().throwsException("Unable to fill the folder details");
			}
			fc.utobj().switchFrameToDefault(driver);
			//AdminTheHubLibraryPageTest library_page=new AdminTheHubLibraryPageTest();
			try {
				fc.utobj().printTestStep("Click on Created Folder");
				//library_page.clickOverMoreLink(driver);
				fc.utobj().clickElement(driver,
						driver.findElement(By.xpath(".//span[contains(text(),'" + folderName + "')]")));
			} catch (Exception e) {
				fc.utobj().throwsException("Unable to click on the created folder");
			}

			try {
				fc.utobj().printTestStep("Click on Add Library Document");
				fc.utobj().clickElement(driver, driver
						.findElement(By.xpath(".//a[contains(@onclick,'modifyDocument?fromWhere=Admin&folder')]")));
			} catch (Exception e) {
				fc.utobj().throwsException("Unable to click on Add Library Document");
			}
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			String documentTitle = fc.utobj().generateTestData("Document");

			try {
				fc.utobj().printTestStep("Fill the details for Library Document");
				fc.utobj().sendKeys(driver, driver.findElement(By.id("documentTitle")), documentTitle);

				fc.utobj().sendKeys(driver, driver.findElement(By.id("summary")), "Summary");

				fc.utobj().sendKeys(driver, driver.findElement(By.id("displayDate")), "06/07/2019");

				fc.utobj().sendKeys(driver, driver.findElement(By.id("fileClick")),
						fc.utobj().getFilePathFromTestData("001_Fish-Wallpaper-HD.jpg"));

				fc.utobj().clickElement(driver, driver.findElement(By.id("add1")));
			} catch (Exception e) {
				fc.utobj().throwsException("Unable to fill the Library Document details");
			}
			try {
				fc.utobj().printTestStep("Click on Submit Button");
				fc.utobj().sleep();
				fc.utobj().clickElement(driver, driver.findElement(By.name("SubmitButton")));
			} catch (Exception e) {
				fc.utobj().throwsException("Unable to click on Submit Button");
			}

			try {
				fc.utobj().printTestStep("Search for the Document through Search Field");
				fc.utobj().sendKeys(driver, driver.findElement(By.id("searchDocuments")), documentTitle);
				fc.utobj().clickElement(driver, driver.findElement(By.xpath(".//img[contains(@src,'sIcon.png')]")));
			} catch (Exception e) {
				fc.utobj().throwsException("Unable to search the document through Search Field");
			}

			if (fc.utobj().assertPageSource(driver, documentTitle) == false) {
				fc.utobj().throwsException("Document not added.");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			BATRemarks = BATRemarks + FranconnectUtil.RyverNextLineChar + moduleName
					+ FranconnectUtil.RyverNextLineCharWithBullet + "Add Library Document";
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "BuildAcceptanceTest" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Ticket is getting Created", testCaseId = "Create_Support_Ticket")
	private void createTicket() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String moduleName = FranconnectUtil.RyverNextLineChar + "Support";

		try {
			driver = fc.loginpage().login(driver);

			String ticketSubject = fc.utobj().generateTestData("Ticket Subject");

			com.goToAdmin(driver);
			String department = fc.utobj().generateTestData("Dep");

			com.openUserLink(driver, "Manage Department",
					driver.findElement(By.xpath(".//a[@qat_adminlink='Manage Department']")));

			try {
				fc.utobj().printTestStep("Add Department and fill department details");
				fc.utobj().clickElement(driver,
						driver.findElement(By.xpath(".//a[contains(@onclick,'supportAddDepartment')]")));
				fc.commonMethods().switch_cboxIframe_frameId(driver);

				fc.utobj().sendKeys(driver, driver.findElement(By.id("departmentName")), department);
				fc.utobj().sendKeys(driver, driver.findElement(By.id("departmentAbbr")),
						fc.utobj().getRandomChar() + fc.utobj().getRandomChar());
				fc.utobj().selectValFromMultiSelect(driver, driver.findElement(By.id("ms-parentassignedTo")),
						"FranConnect Administrator");
				fc.utobj().sendKeys(driver, driver.findElement(By.id("departmentEmail")), "test@franconnect.net");
				fc.utobj().clickElement(driver, driver.findElement(By.id("Submit")));
				fc.utobj().clickElement(driver, driver.findElement(By.name("Submit")));

			} catch (Exception e) {
				fc.utobj().throwsException("Unable to add department. " + e.getMessage());
			}

			try {
				fc.utobj().printTestStep("Open Support Module");
				/*fc.utobj().clickElement(driver, driver.findElement(By.id("test1")));
				fc.utobj().clickElement(driver, driver.findElement(By.id("module_support")));*/
				
				fc.home_page().openSupportModule(driver);;
			} catch (Exception e) {
				fc.utobj().throwsException("Unable to open Support Module");
			}

			try {
				fc.utobj().printTestStep("Open Ticket tab");
				fc.utobj().clickElement(driver, driver.findElement(By.xpath(".//a[@qat_submodule='Tickets']")));
			} catch (Exception e) {
				fc.utobj().throwsException("Unable to open Tickets tab");
			}

			try {
				fc.utobj().printTestStep("Click on Create Ticket");
				fc.utobj().clickElement(driver, driver.findElement(By.xpath(".//img[contains(@src,'icntkt.png')]")));
			} catch (Exception e) {
				fc.utobj().throwsException("Unable to click on Create Ticket");
			}

			try {
				fc.utobj().printTestStep("Fill Ticket details and submit");
				fc.utobj().selectDropDown(driver, driver.findElement(By.id("departmentId")), department);
				fc.utobj().sendKeys(driver, driver.findElement(By.id("subject")), ticketSubject);
				fc.utobj().sendKeys(driver, driver.findElement(By.id("description")), "Ticket Description");

				fc.utobj().clickElement(driver, driver.findElement(By.name("Submit")));

				fc.utobj().clickElement(driver,
						driver.findElement(By.xpath(".//input[@value='Ok' and @class='cm_new_button']")));
			} catch (Exception e) {
				fc.utobj().throwsException("Unable to create ticket " + e.getMessage());
			}

			try {
				fc.utobj().printTestStep("Search for created ticket");
				fc.utobj().sendKeys(driver, driver.findElement(By.name("supportTopSearch")), ticketSubject);
				fc.utobj().clickElement(driver, driver.findElement(By.xpath(".//img[contains(@src,'sIcon.png')]")));
			} catch (Exception e) {
				fc.utobj().throwsException("Unable to search Created Ticket");
			}

			try {
				fc.utobj().printTestStep("Change the status of the ticket to Closed");
				fc.utobj().singleActionIcon(driver, "Update Status");
				fc.commonMethods().switch_cboxIframe_frameId(driver);

				fc.utobj().selectDropDown(driver, driver.findElement(By.id("statusID")), "Closed");
				fc.utobj().clickElement(driver, driver.findElement(By.name("Submit")));

				fc.utobj().clickElement(driver, driver.findElement(By.xpath(".//input[@value='Close']")));
			} catch (Exception e) {
				fc.utobj().throwsException("Unable to change the status of the ticket");
			}

			if (fc.utobj().assertPageSource(driver, "Closed") == false) {
				fc.utobj().throwsException("Ticket Not Closed");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			BATRemarks = BATRemarks + FranconnectUtil.RyverNextLineChar + moduleName
					+ FranconnectUtil.RyverNextLineCharWithBullet + "Create Support Ticket";
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "BuildAcceptanceTest"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Create Visit at Field Ops Visits Page", testCaseId = "Create_Visit_Form")
	private void createVisit01() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String moduleName = FranconnectUtil.RyverNextLineChar + "FieldOps";

		try {
			driver = fc.loginpage().login(driver);
			FieldOpsVisitsPage pobj = new FieldOpsVisitsPage(driver);

			Map<String, String> data2 = new HashMap<String, String>();

			data2.put("visitFormName", "TestVisit");
			data2.put("description", "TestVisitForm");
			data2.put("frequency", "Monthly");
			data2.put("isPrivate", "Yes");
			data2.put("type", "Single Page");
			data2.put("layout", "List");
			data2.put("showResponseScore", "Yes");
			data2.put("questionTypeNewOrLibrary", "New Question");
			data2.put("questionText", "It is a Sample Question");
			data2.put("helpForUserText", "It is a single line text question");
			data2.put("responseIsMandatory", "Yes");
			data2.put("criticalLevel", "Critical");
			data2.put("responseTypeQuestion", "Paragraph Text");
			data2.put("comments", "It is a Sample Visit");
			data2.put("responseTypeQuestionR", "Rating");
			data2.put("minRange", "45");
			data2.put("maxRange", "90");
			data2.put("responseTypeQuestion", "Paragraph Text");
			data2.put("responseTypeQuestionSingleSelectR", "Single Select (Radio Button)");
			data2.put("firstChoice", "Yes");
			data2.put("secondChoice", "No");
			data2.put("firstScore", "45");
			data2.put("secondScore", "90");
			data2.put("responseTypeQuestionSingleSelectDrop", "Single Select (Drop Down)");

			fc.utobj().printTestStep("Navigate To Admin > Fieldops > Manage Visit Form");
			fc.utobj().printTestStep("Add Private Visit Form");
			AdminFieldOpsManageVisitFormPageTest visitFormPage = new AdminFieldOpsManageVisitFormPageTest();
			String visitFormName = visitFormPage.addVisitFormPrivate(driver, data2);
			fc.utobj().printTestStep("Add Question");
			fc.utobj().clickElement(driver, pobj.addQuestion);
			AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest customizeVisit = new AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest();
			customizeVisit.addQuestion(driver, data2);

			fc.utobj().printTestStep("Add Question In Visit Form Rating Type");
			AdminFieldOpsQuestionLibraryPageTest questionLibrary = new AdminFieldOpsQuestionLibraryPageTest();
			fc.utobj().clickElement(driver, pobj.addQuestion);
			questionLibrary.addQuestionRatingType(driver, data2);

			fc.utobj().printTestStep("Add Question In Visit Form Single Select Drop Down Type");
			fc.utobj().clickElement(driver, pobj.addQuestion);
			questionLibrary.addQuestionSingleSelectDropDownType(driver, data2);

			fc.utobj().printTestStep("Add Question In Visit Form Single Select Radio Button Type");
			fc.utobj().clickElement(driver, pobj.addQuestion);
			questionLibrary.addQuestionSingleSelectRadioButtonType(driver, data2);
			fc.utobj().clickElement(driver, pobj.finishBtn);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");
			String regionName = fc.utobj().generateTestData("TestR");
			String storeType = fc.utobj().generateTestData("TestS");
			String franchiseId = fc.utobj().generateTestData("TestFID");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All(driver, franchiseId, regionName, storeType);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			String emailId = "fieldopsautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setConsultant("Y");
			corpUser.setEmail(emailId);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To FieldOps > Visits Page");
			fc.fieldops().fieldops_common().fieldopsVisitCreateVisitPage(driver);
			fc.utobj().printTestStep("Create Visit");
			fc.utobj().clickElement(driver, pobj.createVisit);

			fc.utobj().selectValFromMultiSelectRadioBtn(driver, pobj.selectVisitForm, visitFormName);
			fc.utobj().selectValFromMultiSelectWithoutReset(driver, pobj.selectFranchiseId, franchiseId);
			fc.utobj().selectValFromMultiSelectRadioBtn(driver, pobj.selectConsultantMultiSelect,
					corpUser.getuserFullName());
			String futureDate = fc.utobj().getFutureDateUSFormat(20);
			fc.utobj().sendKeys(driver, pobj.scheduleDateTime, futureDate);
			String comments = data2.get("comments");
			fc.utobj().sendKeys(driver, pobj.comments, comments);
			fc.utobj().clickElement(driver, pobj.schedule);

			fc.utobj().printTestStep("Verify Visit At FieldOps Visit Page");
			new FieldOpsVisitsPageTest().defaultViewByFranchiseId(driver, franchiseId);
			boolean isStatusPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[.='" + franchiseId + "']/ancestor::tr/td[contains(text () ,'Scheduled')]");
			if (isStatusPresent == false) {
				fc.utobj().throwsException("Was not able to create Visit");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			BATRemarks = BATRemarks + FranconnectUtil.RyverNextLineChar + moduleName
					+ FranconnectUtil.RyverNextLineCharWithBullet + "Create Visit Form";
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "BuildAcceptanceTest" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-05", testCaseId = "Enter_Sales_Report", testCaseDescription = "Verify_Sales_Report")
	public void VerifySalesReportFinance() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String moduleName = FranconnectUtil.RyverNextLineChar + "Finance";

		String agrmntVrsnName = fc.utobj().generateTestData("AgreementVersion");
		String royaltyPrcntg = "10";
		String advPercentage = "5";
		String royaltyAreaFranchiseValue = "4";
		String advAreaFranchiseValue = "3";
		

		try {
			driver = fc.loginpage().login(driver);
			AdminFinanceFinanceSetupPreferencesPage pobj = new AdminFinanceFinanceSetupPreferencesPage(driver);
			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName, royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("Testbsr");
			String storeType = fc.utobj().generateTestData("Testbsr");
			String franchiseId = fc.utobj().generateTestData("Testbsr");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config,
					salesReportType);

			boolean isNormalSalesReport = false;

			try {
				fc.utobj().clickElement(driver, pobj.financeAndCrmTransaction);
				isNormalSalesReport = fc.utobj().isElementPresent(driver, pobj.isFinanceCRMTransaction);
			} catch (Exception e) {
				isNormalSalesReport = false;
			}

			try {
				if (!isNormalSalesReport) {
					if (!fc.utobj().isSelected(driver, pobj.noFinanceCRMTransaction)) {
						fc.utobj().clickElement(driver, pobj.noFinanceCRMTransaction);
					}
					fc.utobj().clickElement(driver, pobj.saveCRMFinanceTransaction);
				}
			} catch (Exception e) {
				Reporter.log("Not able to save Finance CRM Transaction Integration : " + e.getMessage().toString());
			}

			if (isNormalSalesReport) {
				String reportId = new FinanceSalesPageTest().enterSalesReportFinanceCRMTransactionEnable(driver,
						franchiseId);

				fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
				new FinanceSalesPageTest().filterSalesReport(driver, franchiseId);

				fc.utobj().printTestStep("View Sales Report By Action Image Icon");

				boolean isReportIdPresent = fc.utobj().assertPageSource(driver, reportId);
				if (!isReportIdPresent) {
					fc.utobj().throwsException("Not able to verify Finance Sales Report");
				}
			} else {
				fc.utobj().printTestStep("Navigate To Finance > Sales");
				fc.utobj().printTestStep("Enter Sales Report");
				String categoryQuantity = "110";
				String categoryAmount = "6000";
				String reportId = new FinanceSalesPageTest().enterSalesReport(driver, franchiseId, categoryQuantity,
						categoryAmount);

				fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
				new FinanceSalesPageTest().filterSalesReport(driver, franchiseId);

				fc.utobj().printTestStep("View Sales Report By Action Image Icon");
				fc.utobj().actionImgOption(driver, reportId, "View Sales Details");

				fc.utobj().printTestStep("Verify Total Sales Quantity");
				String totalQuantity = fc.utobj().getText(driver, fc.utobj().getElementByXpath(driver,
						".//td[contains(text () ,' Total Sales')]/following-sibling::td[1]"));
				try {

					if (totalQuantity.contains(",")) {

						totalQuantity = totalQuantity.replaceAll(",", "");
					}

					float quantitiyValue = Float.parseFloat(totalQuantity);
					float categoryQuantityFloat = Float.parseFloat(categoryQuantity);

					if (quantitiyValue != categoryQuantityFloat) {

						fc.utobj().throwsException("was not able to verify Total Sales Quantitiy Value");
					}
				} catch (Exception e) {
					fc.utobj().throwsException("was not able to verify Total Sales Quantitiy Value");
				}

				fc.utobj().printTestStep("Verify Total Sales Amount");
				String totalAmount = fc.utobj().getText(driver, fc.utobj().getElementByXpath(driver,
						".//td[contains(text () ,' Total Sales')]/following-sibling::td[2]"));

				try {

					if (totalAmount.contains(",")) {
						totalAmount = totalAmount.replaceAll(",", "");
					}

					float amountValue = Float.parseFloat(totalAmount);
					float categoryAmountFloat = Float.parseFloat(categoryAmount);

					if (amountValue != categoryAmountFloat) {

						fc.utobj().throwsException("was not able to verify Total Sales Amount Value");
					}
				} catch (Exception e) {
					fc.utobj().throwsException("was not able to verify Total Sales Amount Value");
				}
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			BATRemarks = BATRemarks + FranconnectUtil.RyverNextLineChar + moduleName
					+ FranconnectUtil.RyverNextLineCharWithBullet + "Verify Sales Report";
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "BuildAcceptanceTest")
	@TestCase(createdOn = "2018-05-07", updatedOn = "2018-05-07", testCaseDescription = "Verify SOLR Search", testCaseId = "Verify_SOLR_Search")
	public void verifySolrSearch() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String moduleName = FranconnectUtil.RyverNextLineChar + "SOLR Search";
		String text = "";

		try {
			driver = fc.loginpage().login(driver);

			try {
				fc.utobj().printTestStep("Click on Sales Module");
				fc.utobj().clickElement(driver, driver.findElement(By.xpath(".//li[@id='module_fs']/a")));
			} catch (Exception e) {
				fc.utobj().throwsException("Unable to click on sales module");
			}

			try {
				fc.utobj().printTestStep("Click on Lead Management Tab");
				fc.utobj().clickElement(driver, driver.findElement(By.xpath(".//a[@qat_submodule='Lead Management']")));
			} catch (Exception e) {
				fc.utobj().throwsException("Unable to click on Lead Management sub tab");
			}

			try {
				fc.utobj().printTestStep("Click on Add lead link");
				fc.utobj().clickElement(driver, driver.findElement(By.xpath(".//a[contains(@href,'addAnotherLead')]")));
			} catch (Exception e) {
				fc.utobj().throwsException("Unable to click on Add lead");
			}

			String fName = fc.utobj().generateTestData("Fname");
			String lName = fc.utobj().generateTestData("Lname");

			try {
				fc.utobj().printTestStep("Fill lead Info");
				fc.utobj().sendKeys(driver, driver.findElement(By.id("firstName")), fName);
				fc.utobj().sendKeys(driver, driver.findElement(By.id("lastName")), lName);
				fc.utobj().sendKeys(driver, driver.findElement(By.id("emailID")), "test@franconnect.net");
				fc.utobj().selectDropDown(driver, driver.findElement(By.id("leadOwnerID")),
						"FranConnect Administrator");
				fc.utobj().selectDropDown(driver, driver.findElement(By.id("leadSource2ID")), "Import");
				fc.utobj().selectDropDown(driver, driver.findElement(By.id("leadSource3ID")), "None");
				fc.utobj().clickElement(driver, driver.findElement(By.id("assignAutomaticCampaign1")));

			} catch (Exception e) {
				fc.utobj().throwsException("Unable to fill lead details " + e.getMessage());
			}

			try {
				fc.utobj().printTestStep("Click on Submit Button");
				fc.utobj().clickElement(driver, driver.findElement(By.id("Submit")));
			} catch (Exception e) {
				fc.utobj().throwsException("Unable to click on submit button");
			}

			fc.utobj().printTestStep("Verify that the lead is added : " + fName + " " + lName);

			if (!fc.utobj().assertPageSource(driver, fName + " " + lName)) {
				fc.utobj().throwsException("Lead is Not getting added");
			}

			try {
				fc.utobj().sendKeys(driver, driver.findElement(By.id("searchTermKey")), fName + " " + lName);

				fc.utobj().clickEnterOnElement(driver, driver.findElement(By.id("searchTermKey")));

				fc.utobj().sleep();

				if (driver.findElements(By.xpath(".//*[contains(@ng-click,'showDetails(result)')]")).size() <= 0) {
					text = FranconnectUtil.RyverNextLineCharWithBullet + "Solr Search Is not working for new lead";
				}

			} catch (Exception e) {
				text = FranconnectUtil.RyverNextLineCharWithBullet + "Solr Search Is not working for new leads";
				fc.utobj().throwsException(e.getMessage());
			}

		} catch (Exception e) {
			
			if(text.equalsIgnoreCase("")){
				text = FranconnectUtil.RyverNextLineCharWithBullet + "Unable to search through SOLR";
			}
			
			BATRemarks = BATRemarks + FranconnectUtil.RyverNextLineChar + moduleName
					+ FranconnectUtil.RyverNextLineChar + text;
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	/*
	 *  Verify BaseThreads is working
	 */
	
	@Test(groups = {"BuildAcceptanceTest"})
	@TestCase(createdOn = "2018-09-06", updatedOn = "2018-09-06", testCaseDescription = "Verify BaseThreads", testCaseId = "Verify_BaseThreads")
	public void verifyBaseThreads() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		String moduleName = FranconnectUtil.RyverNextLineChar + "BaseThreads";
		String text = "";

		try {
			String buildUrl = FranconnectUtil.config.get("buildUrl");
			try {
				
				int a=buildUrl.lastIndexOf("/fc");
				System.out.println(a);
				buildUrl=buildUrl.substring(0,a);
				buildUrl=buildUrl.concat("/basethreads/");
				System.out.println(buildUrl);
				
			} catch (Exception e) {
				Reporter.log(e.getMessage() , true);
			}
			fc.utobj().printTestStep("Enter Build Url");

			try {
				driver.get(buildUrl);
			} catch (Exception driverNotOpened) {

			}

			try {
				LoginPage pobj = new LoginPage(driver);
				if (fc.utobj().isElementPresent(driver, pobj.userid) == false) {
					throw new Exception();
				}
			} catch (Exception elementNotFound) {
				try {
					driver.get(buildUrl);
				} catch (Exception driverNotOpened) {
					fc.utobj().throwsException("Unable to open the Build (Wait time 3 minutes)");
				}
			}

			LoginPage pobj = new LoginPage(driver);
			if (fc.utobj().isElementPresent(driver, pobj.userid) == false) {
				fc.utobj().throwsException("Login page not found!");
			}
			
			try {
				pobj = new LoginPage(driver);
				fc.utobj().printTestStep("Enter User Credentials");
				fc.utobj().sendKeys(driver, pobj.userid, FranconnectUtil.config.get("userName"));
				fc.utobj().sendKeys(driver, pobj.password, FranconnectUtil.config.get("password"));
				fc.utobj().printTestStep("Click on Login Button");
				fc.utobj().clickElement(driver, pobj.loginBtBtn);
			} catch (Exception e) {
				Reporter.log(e.toString());
				fc.utobj().throwsException("Issues with Login / Build is not accessible.");
			}

			FCHomePage verifyPage = new FCHomePage(driver);
			try {
				verifyPage.btLoggedOut.isDisplayed();
			} catch (Exception e) {
				text = FranconnectUtil.RyverNextLineCharWithBullet + "Please check We are not able to login in basethreads";
				fc.utobj().throwsException("Issue after hitting login button.");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			BATRemarks = BATRemarks + FranconnectUtil.RyverNextLineChar + moduleName
					+ FranconnectUtil.RyverNextLineChar + text;
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

}