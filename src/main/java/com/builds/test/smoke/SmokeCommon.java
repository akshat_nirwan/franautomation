package com.builds.test.smoke;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.builds.utilities.FranconnectUtil;

public class SmokeCommon {

	FranconnectUtil fc = new FranconnectUtil();

	/**
	 * Add Area Region
	 * 
	 * @param driver
	 * @param regionName
	 * @return
	 * @throws Exception
	 */
	String addAreaRegion(WebDriver driver, String regionName) throws Exception {
		try {
			fc.utobj().printTestStep("Click on Add New Area / Region");
			fc.utobj().clickElement(driver,
					driver.findElement(By.xpath(".//*[@qat_adminlink='Add New Area / Region']")));
		} catch (Exception e) {
			fc.utobj().throwsException("Unable to Click Add New Area / Region link " + e.getMessage());
		}

		try {
			fc.utobj().printTestStep("Fill Region Details");
			fc.utobj().selectDropDown(driver, driver.findElement(By.id("category")), "Domestic");

			fc.utobj().sendKeys(driver, driver.findElement(By.name("regioname")), regionName);
			fc.utobj().selectDropDown(driver, driver.findElement(By.id("groupBy")), "Zip Codes");
			fc.utobj().clickElement(driver, driver.findElement(By.id("all")));
			fc.utobj().sendKeys(driver, driver.findElement(By.name("ziplist")),
					fc.utobj().generateRandomNumber6Digit() + fc.utobj().getRandomChar());

			fc.utobj().printTestStep("Click on Submit Button");
			fc.utobj().clickElement(driver,
					driver.findElement(By.xpath(".//input[@type='button' and @value='Submit']")));

			fc.utobj().printTestStep("Click on Next Button");
			fc.utobj().clickElement(driver,
					driver.findElement(By.xpath(".//input[@value='Next' and @class='cm_new_button']")));
		} catch (Exception e) {
			fc.utobj().throwsException("Unable to Add Region " + e.getMessage());
		}
		return regionName;
	}

	/**
	 * Adds Responsible Department under Opener Section in Admin
	 * 
	 * @param driver
	 * @return
	 * @throws Exception
	 */
	String addResponsibleDepartment(WebDriver driver) throws Exception {
		goToAdmin(driver);
		String responsibility = fc.utobj().generateTestData("Responsible");
		try {
			openUserLink(driver, "Responsible Department",
					driver.findElement(By.xpath(".//a[@qat_adminlink='Responsible Department']")));

			fc.utobj().printTestStep("Click on Add More link");

			fc.utobj().clickElement(driver,
					driver.findElement(By.xpath(".//input[@class='cm_new_button_link' and @value='Add More']")));
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().printTestStep("Enter Responsible Department : " + responsibility);

			fc.utobj().sendKeys(driver, driver.findElement(By.id("responsibilityArea")), responsibility);

			fc.utobj().printTestStep("Click on Submit Button");

			fc.utobj().clickElement(driver, driver.findElement(By.name("Submit")));

			fc.utobj().switchFrameToDefault(driver);
		} catch (Exception e) {
			fc.utobj().throwsException("Unable to Add Responsible Department. " + e.getMessage());
		}
		return responsibility;
	}

	/**
	 * Click on User Action and Click on Admin Link
	 * 
	 * @param driver
	 * @throws Exception
	 */
	void goToAdmin(WebDriver driver) throws Exception {
		clickUserAction(driver);
		try {
			fc.utobj().printTestStep("Click on Admin Link");
			fc.utobj().clickElement(driver, ".//*[contains(@href,'nextUrl=administration') or *[.=Admin]]");
		} catch (Exception e) {
			fc.utobj().throwsException("Not able to click on Admin link");
		}
	}

	/**
	 * Go to Admin > Clicks Add Division Link
	 * 
	 * @param driver
	 * @throws Exception
	 */
	void openDivision(WebDriver driver) throws Exception {
		goToAdmin(driver);
		try {
			fc.utobj().printTestStep("Open Add Division Link");
			fc.utobj().clickElement(driver, driver.findElement(By.xpath(".//a[@qat_adminlink='Add Division']")));
		} catch (Exception e) {
			fc.utobj().throwsException("Not able to click on Add Division link");
		}
	}

	/**
	 * Configure New Hierarchy Level > Open Division Link > Fill division
	 * Details and Save.
	 * 
	 * @param driver
	 * @param divisionName
	 * @throws Exception
	 */
	void createDivision(WebDriver driver, String divisionName) throws Exception {
		goToAdmin(driver);
		configureNewHierarchyLevel(driver);
		openDivision(driver);
		fillDivisionDetailsAndSave(driver, divisionName);
	}

	void configureNewHierarchyLevel(WebDriver driver) throws Exception {
		try {
			fc.utobj().printTestStep("Click on link : Configure New Hierarchy Level");
			fc.utobj().clickElement(driver,
					driver.findElement(By.xpath(".//a[@qat_adminlink='Configure New Hierarchy Level']")));
		} catch (Exception e) {
			fc.utobj().throwsException("Not able to click on Configure New Hierarchy Level link");
		}

		try {
			fc.utobj().printTestStep("Enable New Hierarchy Level : Yes");

			fc.utobj().moveToElement(driver,
					driver.findElement(By.xpath(".//input[@name='isDivisionConfigureDisplay' and @value='Y']")));

			if (driver.findElement(By.xpath(".//input[@name='isDivisionConfigureDisplay' and @value='Y']"))
					.isSelected() == false) {
				fc.utobj().clickElement(driver,
						driver.findElement(By.xpath(".//input[@name='isDivisionConfigureDisplay' and @value='Y']")));
			}
		} catch (Exception e) {
			fc.utobj().throwsException("Not able to click on Enable New Hierarchy Level : Yes");
		}

		try {
			fc.utobj().printTestStep("Click on Save Button");
			fc.utobj().clickElement(driver, driver.findElement(By.xpath(".//input[@name='Submit' and @value='Save']")));
		} catch (Exception e) {
			fc.utobj().throwsException("Unable to click on Save Button");
		}
	}

	void fillDivisionDetailsAndSave(WebDriver driver, String divisionName) throws Exception {
		try {
			fc.utobj().sendKeys(driver, driver.findElement(By.name("divisionName")), divisionName);
		} catch (Exception e) {
			fc.utobj().throwsException("Unable to Enter data in division name");
		}

		try {
			fc.utobj().printTestStep("Click on Save Button");
			fc.utobj().clickElement(driver,
					driver.findElement(By.xpath(".//input[@name='button' and @value='Submit']")));
		} catch (Exception e) {
			fc.utobj().throwsException("Unable to click on Save Button");
		}

		try {
			fc.utobj().printTestStep("Click on Search Button");
			fc.utobj().clickElement(driver, driver.findElement(By.name("searchButton")));
		} catch (Exception e) {
			fc.utobj().throwsException("Unable to click on Search Button");
		}

		try {
			fc.utobj().printTestStep("Verify the division is getting searched : " + divisionName);
			fc.utobj().selectValFromMultiSelect(driver, driver.findElement(By.id("ms-parentdivisionKey")),
					divisionName);

			if (fc.utobj().assertPageSource(driver, divisionName) == true) {
				fc.utobj().printTestStep("Division Found");
			} else {
				fc.utobj().throwsException("Unable to Search the added division : " + divisionName);
			}
		} catch (Exception e) {
			fc.utobj().throwsException("Unable to Search the added division : " + divisionName);
		}
	}

	void openUserLink(WebDriver driver, String userType, WebElement element) throws Exception {
		try {
			fc.utobj().printTestStep("Click on " + userType + " Link");
			fc.utobj().clickElement(driver, element);
		} catch (Exception e) {
			fc.utobj().throwsException("Not able to click on "+userType+" link");
		}
	}

	void clickUserAction(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Click on User Options");
		try {
			fc.utobj().clickElement(driver, ".//*[@id='dropdown']");
		} catch (Exception e) {
			fc.utobj().throwsException("Not able to click on User Option");
		}
	}

	void logout(WebDriver driver) throws Exception {
		clickUserAction(driver);
		try {
			fc.utobj().printTestStep("Logout from the Build");
			fc.utobj().clickElement(driver, driver.findElement(By.xpath(".//*[@href='logout']")));
		} catch (Exception e) {
			fc.utobj().throwsException("Not able to click on Admin link");
		}
	}

	void clickOnModule(WebDriver driver, String moduleName) throws Exception {
		fc.utobj().printTestStep("Click on Module : " + moduleName);

		try {
			if (moduleName.trim().toLowerCase().equalsIgnoreCase("sales")) {

			} else if (moduleName.trim().toLowerCase().equalsIgnoreCase("opener")) {

			} else if (moduleName.trim().toLowerCase().equalsIgnoreCase("infomgr")) {

			} else if (moduleName.trim().toLowerCase().equalsIgnoreCase("thehub")) {

			} else if (moduleName.trim().toLowerCase().equalsIgnoreCase("training")) {

			} else if (moduleName.trim().toLowerCase().equalsIgnoreCase("crm")) {

			} else if (moduleName.trim().toLowerCase().equalsIgnoreCase("fieldops")) {

			} else if (moduleName.trim().toLowerCase().equalsIgnoreCase("finance")) {

			} else if (moduleName.trim().toLowerCase().equalsIgnoreCase("smartconnect")) {

			} else if (moduleName.trim().toLowerCase().equalsIgnoreCase("insights")) {

			} else if (moduleName.trim().toLowerCase().equalsIgnoreCase("ads")) {

			} else if (moduleName.trim().toLowerCase().equalsIgnoreCase("shop")) {

			} else if (moduleName.trim().toLowerCase().equalsIgnoreCase("listings")) {

			} else if (moduleName.trim().toLowerCase().equalsIgnoreCase("reputation")) {

			}
		} catch (Exception e) {

		}

	}

}
