package com.builds.test.smoke;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.parsers.ParserConfigurationException;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Reporter;
import org.testng.annotations.Test;
import org.xml.sax.SAXException;

import com.builds.test.admin.AdminAreaRegionAddAreaRegionPageTest;
import com.builds.test.admin.AdminConfigurationConfigureStoreTypePageTest;
import com.builds.test.admin.AdminDivisionAddDivisionPageTest;
import com.builds.test.admin.AdminFranchiseLocationAddFranchiseLocationPageTest;
import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.admin.AdminUsersManageCorporateUsersPageTest;
import com.builds.test.common.CorporateUser;
import com.builds.test.crm.CRMLeadsPageTest;
import com.builds.test.fieldops.AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest;
import com.builds.test.fieldops.AdminFieldOpsManageVisitFormPageTest;
import com.builds.test.fieldops.AdminFieldOpsQuestionLibraryPageTest;
import com.builds.test.fieldops.FieldOpsVisitsPageTest;
import com.builds.test.fin.AdminFinanceAgreementVersionsPageTest;
import com.builds.test.fin.AdminFinanceFinanceSetupPreferencesPageTest;
import com.builds.test.fin.FinanceSalesPageTest;
import com.builds.test.fs.FSLeadSummaryPageTest;
import com.builds.test.fs.Sales;
import com.builds.test.opener.AdminOpenerConfigureProjectStatusPageTest;
import com.builds.test.opener.AdminOpenerManageReferenceDatesPageTest;
import com.builds.test.opener.OpenerStoreSummaryStoreListPageTest;
import com.builds.test.support.AdminSupportManageDepartmentPageTest;
import com.builds.test.support.SupportTicketsPageTest;
import com.builds.test.thehub.AdminTheHubLibraryPageTest;
import com.builds.test.thehub.Library;
import com.builds.test.training.AdminTrainingCourseManagementAddSectionPageTest;
import com.builds.test.training.Category;
import com.builds.test.training.Course;
import com.builds.test.training.Lesson;
import com.builds.test.training.Training;
import com.builds.uimaps.admin.AdminFranchiseLocationAddFranchiseLocationPage;
import com.builds.uimaps.admin.AdminFranchiseLocationManageFranchiseLocationsAddUserPage;
import com.builds.uimaps.admin.AdminUsersManageCorporateUsersAddCorporateUserPage;
import com.builds.uimaps.admin.AdminUsersManageDivisionalUsersAddDivisionalUsersPage;
import com.builds.uimaps.admin.AdminUsersManageManageFranchiseUsersPage;
import com.builds.uimaps.crm.CRMLeadsPage;
import com.builds.uimaps.fieldops.FieldOpsVisitsPage;
import com.builds.uimaps.fin.AdminFinanceFinanceSetupPreferencesPage;
import com.builds.uimaps.fs.FSLeadSummaryAddLeadPage;
import com.builds.uimaps.fs.SearchUI;
import com.builds.uimaps.opener.OpenerStoreSummaryStoreListPage;
import com.builds.uimaps.support.SupportTicketsPage;
import com.builds.uimaps.training.AdminTrainingCoureseManagementPage;
import com.builds.uimaps.training.AdminTrainingPlanAndCertificatePage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class SmokeTest {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "FcSanity"})
	@TestCase(createdOn = "2017-08-14", updatedOn = "2017-08-14", testCaseDescription = "Verify The Add Franchise User", testCaseId = "TC_Add_Franchisee_User")
	private void addFranchiseUser() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			driver = fc.loginpage().login(driver);
			String emailId = "salesautomation@staffex.com";
			fc.utobj().printTestStep("Navigate To > Admin > Area / Region >  Add Area / Region");
			AdminAreaRegionAddAreaRegionPageTest p1 = new AdminAreaRegionAddAreaRegionPageTest();
			String regionName = fc.utobj().generateTestData("TestRegName");
			fc.utobj().printTestStep("Add Region");
			p1.addAreaRegion(driver, regionName);

			
			fc.utobj().printTestStep("Navigate To > Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store");
			AdminConfigurationConfigureStoreTypePageTest p2 = new AdminConfigurationConfigureStoreTypePageTest();
			String storeType = fc.utobj().generateTestData("TestStore");
			p2.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To > Admin > Franchise Location > Add Franchise Location");
			fc.adminpage().adminAddNewFranchiseLocationLnk(driver);
			AdminFranchiseLocationAddFranchiseLocationPage pobj = new AdminFranchiseLocationAddFranchiseLocationPage(
					driver);

			fc.utobj().printTestStep("Add Franchise Location With User");
			String franchiseName = fc.utobj().generateTestData("franchiseName");
			fc.utobj().sendKeys(driver, pobj.franchiseeID, franchiseName);
			fc.utobj().selectDropDownByVisibleTextTrimed(driver, pobj.areaRegion, regionName);
			fc.utobj().sendKeys(driver, pobj.centerName, "124578");
			fc.utobj().selectDropDownByVisibleTextTrimed(driver, pobj.storeTypeId, storeType);
			fc.utobj().sendKeys(driver, pobj.openingDate, fc.utobj().getCurrentDateUSFormat());
			fc.utobj().sendKeys(driver, pobj.city, "Test City");
			fc.utobj().selectDropDown(driver, pobj.stateID, 1);
			fc.utobj().sendKeys(driver, pobj.storePhone, "8989898989");

			fc.utobj().sendKeys(driver, pobj.ownerFirstName, "Fname");
			fc.utobj().sendKeys(driver, pobj.ownerLastName, "Lname");
			fc.utobj().clickElement(driver, pobj.submit);
			AdminFranchiseLocationManageFranchiseLocationsAddUserPage pobj2 = new AdminFranchiseLocationManageFranchiseLocationsAddUserPage(
					driver);
			fc.utobj().sendKeys(driver, pobj2.loginId, fc.utobj().generateRandomNumber());
			fc.utobj().sendKeys(driver, pobj2.password, "t0n1ght@123");
			fc.utobj().sendKeys(driver, pobj2.confirmPassword, "t0n1ght@123");
			fc.utobj().selectValFromMultiSelect(driver, pobj2.rolesId, "Default Franchise Role");
			
			
			fc.utobj().selectDropDown(driver, pobj2.state, 1);
			fc.utobj().sendKeys(driver, pobj2.phone1, "1234567890");
			fc.utobj().sendKeys(driver, pobj2.phoneExt1, "2");

			fc.utobj().sendKeys(driver, pobj2.email, emailId);
			fc.utobj().clickElement(driver, pobj2.submit);

			fc.utobj().printTestStep("Verify The Added Franchise User");
			fc.utobj().sleep();
			fc.utobj().selectValFromMultiSelect(driver, pobj2.selectarea, regionName);
			fc.utobj().clickElement(driver, pobj2.searchBtn12);

			boolean isTextPresent = fc.utobj().assertLinkPartialText(driver, franchiseName);

			if (isTextPresent == false) {
				fc.utobj().throwsException("Not able to add Franchise user");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "FcSanity"})
	@TestCase(createdOn = "2017-08-14", updatedOn = "2017-08-14", testCaseDescription = "Verify The Add Divisional User", testCaseId = "TC_Add_Divisional_User")
	private void AddDivisionalUser() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin >  Division >  Add Division");
			String divisionName = fc.utobj().generateTestData("TestDiv");
			AdminDivisionAddDivisionPageTest p1 = new AdminDivisionAddDivisionPageTest();
			p1.addDivision(driver, divisionName);

			fc.utobj().printTestStep("Navigate To Admin >  Users >  Manage Divisional Users");
			AdminUsersManageDivisionalUsersAddDivisionalUsersPage pobj = new AdminUsersManageDivisionalUsersAddDivisionalUsersPage(
					driver);

			fc.utobj().printTestStep("Add Divisional User");
			fc.adminpage().adminUserDivisionalUserAddDivisionalUser(driver);

			String userName = fc.utobj().generateTestData("TestDivUser");
			String emailId = "salesautomation@staffex.com";
			String password = "t0n1ght@123";

			fc.utobj().sendKeys(driver, pobj.userName, userName);
			fc.utobj().sendKeys(driver, pobj.password, password);
			fc.utobj().sendKeys(driver, pobj.confirmPassword, password);

			try {
				fc.utobj().selectValFromMultiSelect(driver, pobj.roleMultiSelectBtn, "Default Division Role");
			} catch (Exception e) {
				fc.utobj().selectValFromMultiSelect(driver, pobj.roleMultiSelectBtn, "Default Brand Role");
			}

			fc.utobj().selectDropDown(driver, pobj.timeZone, "GMT -06:00 US/Canada/Central");
			fc.utobj().selectValFromMultiSelect(driver, pobj.multiselectDivision, divisionName);

			fc.utobj().sendKeys(driver, pobj.firstName, userName);
			fc.utobj().sendKeys(driver, pobj.lastName, userName);

			fc.utobj().sendKeys(driver, pobj.city, "11");
			fc.utobj().selectDropDown(driver, pobj.country, "USA");
			fc.utobj().selectDropDown(driver, pobj.state, "Alabama");
			fc.utobj().sendKeys(driver, pobj.phone1, "1234567890");
			fc.utobj().sendKeys(driver, pobj.phoneExt1, "2");
			fc.utobj().sendKeys(driver, pobj.email, emailId);

			try {
				if (fc.utobj().isSelected(driver,pobj.auditor)) {
				} else {
					fc.utobj().clickElement(driver, pobj.auditor);
				}
			} catch (Exception e) {

			}
			fc.utobj().clickElement(driver, pobj.submit);

			fc.utobj().printTestStep("Verify The Added Divisional User");

			fc.utobj().sendKeys(driver, new AdminUsersManageCorporateUsersAddCorporateUserPage(driver).searchCorpUser,
					userName);
			fc.utobj().clickElement(driver,
					new AdminUsersManageCorporateUsersAddCorporateUserPage(driver).searchCorpUserBtn);

			boolean isTextPresent = fc.utobj().assertLinkPartialText(driver, userName);

			if (isTextPresent == false) {
				fc.utobj().throwsException("Not able to add Divisional user");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}

	@Test(groups = { "FcSanity"})
	@TestCase(createdOn = "2017-08-14", updatedOn = "2017-08-14", testCaseDescription = "Verify The Add Regional User", testCaseId = "TC_Add_Regional_User")
	private void addRegionalUser() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Admin >Area / Region >  Add Area / Region");

			String regionName = fc.utobj().generateTestData("TestReg");
			AdminAreaRegionAddAreaRegionPageTest p1 = new AdminAreaRegionAddAreaRegionPageTest();
			p1.addAreaRegion(driver, regionName);

			fc.utobj().printTestStep("Navigate To Admin >  Users >  Manage Regional Users");
			fc.adminpage().adminUsersManageRegionalUsersAddRegionalUserPage(driver);
			AdminUsersManageManageFranchiseUsersPage pobj = new AdminUsersManageManageFranchiseUsersPage(driver);

			String userName = fc.utobj().generateTestData("TestRegUser");
			String emailId = "salesautomation@staffex.com";
			String password = "t0n1ght@123";

			fc.utobj().printTestStep("Add Regional User");

			fc.utobj().sendKeys(driver, pobj.userName, userName);
			fc.utobj().sendKeys(driver, pobj.password, password);
			fc.utobj().sendKeys(driver, pobj.confirmPassword, password);
			fc.utobj().selectValFromMultiSelect(driver, pobj.roleMultiSelectBtn, "Default Regional Role");
			fc.utobj().selectDropDown(driver, pobj.timeZone, "GMT -06:00 US/Canada/Central");
			fc.utobj().selectDropDown(driver, pobj.region, regionName);

			fc.utobj().sendKeys(driver, pobj.firstName, userName);
			fc.utobj().sendKeys(driver, pobj.lastName, userName);
			fc.utobj().sendKeys(driver, pobj.city, "11");

			fc.utobj().selectDropDown(driver, pobj.country, "USA");
			fc.utobj().selectDropDown(driver, pobj.state, "Alabama");

			fc.utobj().sendKeys(driver, pobj.phone1, "1234567890");
			fc.utobj().sendKeys(driver, pobj.phoneExt1, "22");
			fc.utobj().sendKeys(driver, pobj.email, emailId);
			pobj.submit.click();

			fc.utobj().printTestStep("Verify The Added Regional User");
			fc.utobj().sendKeys(driver, new AdminUsersManageCorporateUsersAddCorporateUserPage(driver).searchCorpUser,
					userName);
			fc.utobj().clickElement(driver,
					new AdminUsersManageCorporateUsersAddCorporateUserPage(driver).searchCorpUserBtn);

			boolean isTextPresent = fc.utobj().assertLinkPartialText(driver, userName);
			if (isTextPresent == false) {
				fc.utobj().throwsException("Not able to add Regional user");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "FcSanity" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Create Visit at Field Ops Visits Page", testCaseId = "TC_Verify_Created_Visit")
	private void createVisit01() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FieldOpsVisitsPage pobj = new FieldOpsVisitsPage(driver);

			Map<String, String> data2 = new HashMap<String, String>();

			data2.put("visitFormName", "TestVisit");
			data2.put("description", "TestVisitForm");
			data2.put("frequency", "Monthly");
			data2.put("isPrivate", "Yes");
			data2.put("type", "Single Page");
			data2.put("layout", "List");
			data2.put("showResponseScore", "Yes");
			data2.put("questionTypeNewOrLibrary", "New Question");
			data2.put("questionText", "It is a Sample Question");
			data2.put("helpForUserText", "It is a single line text question");
			data2.put("responseIsMandatory", "Yes");
			data2.put("criticalLevel", "Critical");
			data2.put("responseTypeQuestion", "Paragraph Text");
			data2.put("comments", "It is a Sample Visit");
			data2.put("responseTypeQuestionR", "Rating");
			data2.put("minRange", "45");
			data2.put("maxRange", "90");
			data2.put("responseTypeQuestion", "Paragraph Text");
			data2.put("responseTypeQuestionSingleSelectR", "Single Select (Radio Button)");
			data2.put("firstChoice", "Yes");
			data2.put("secondChoice", "No");
			data2.put("firstScore", "45");
			data2.put("secondScore", "90");
			data2.put("responseTypeQuestionSingleSelectDrop", "Single Select (Drop Down)");

			fc.utobj().printTestStep("Navigate To Admin > Fieldops > Manage Visit Form");
			fc.utobj().printTestStep("Add Private Visit Form");
			AdminFieldOpsManageVisitFormPageTest visitFormPage = new AdminFieldOpsManageVisitFormPageTest();
			String visitFormName = visitFormPage.addVisitFormPrivate(driver, data2);
			fc.utobj().printTestStep("Add Question");
			fc.utobj().clickElement(driver, pobj.addQuestion);
			AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest customizeVisit = new AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest();
			customizeVisit.addQuestion(driver, data2);

			fc.utobj().printTestStep("Add Question In Visit Form Rating Type");
			AdminFieldOpsQuestionLibraryPageTest questionLibrary = new AdminFieldOpsQuestionLibraryPageTest();
			fc.utobj().clickElement(driver, pobj.addQuestion);
			questionLibrary.addQuestionRatingType(driver, data2);

			fc.utobj().printTestStep("Add Question In Visit Form Single Select Drop Down Type");
			fc.utobj().clickElement(driver, pobj.addQuestion);
			questionLibrary.addQuestionSingleSelectDropDownType(driver, data2);

			fc.utobj().printTestStep("Add Question In Visit Form Single Select Radio Button Type");
			fc.utobj().clickElement(driver, pobj.addQuestion);
			questionLibrary.addQuestionSingleSelectRadioButtonType(driver, data2);
			fc.utobj().clickElement(driver, pobj.finishBtn);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");
			String regionName = fc.utobj().generateTestData("TestR");
			String storeType = fc.utobj().generateTestData("TestS");
			String franchiseId = fc.utobj().generateTestData("TestFID");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All(driver, franchiseId, regionName, storeType);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			String emailId = "fieldopsautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setConsultant("Y");
			corpUser.setEmail(emailId);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To FieldOps > Visits Page");
			fc.fieldops().fieldops_common().fieldopsVisitCreateVisitPage(driver);
			fc.utobj().printTestStep("Create Visit");
			fc.utobj().clickElement(driver, pobj.createVisit);

			fc.utobj().selectValFromMultiSelectRadioBtn(driver, pobj.selectVisitForm, visitFormName);
			fc.utobj().selectValFromMultiSelectWithoutReset(driver, pobj.selectFranchiseId, franchiseId);
			fc.utobj().selectValFromMultiSelectRadioBtn(driver, pobj.selectConsultantMultiSelect,
					corpUser.getuserFullName());
			String futureDate = fc.utobj().getFutureDateUSFormat(20);
			fc.utobj().sendKeys(driver, pobj.scheduleDateTime, futureDate);
			String comments = data2.get("comments");
			fc.utobj().sendKeys(driver, pobj.comments, comments);
			fc.utobj().clickElement(driver, pobj.schedule);

			fc.utobj().printTestStep("Verify Visit At FieldOps Visit Page");
			new FieldOpsVisitsPageTest().defaultViewByFranchiseId(driver, franchiseId);
			boolean isStatusPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[.='" + franchiseId + "']/ancestor::tr/td[contains(text () ,'Scheduled')]");
			if (isStatusPresent == false) {
				fc.utobj().throwsException("Was not able to create Visit");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "FcSanity"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-05", testCaseId = "TC_Enter_Sales_Report", testCaseDescription = "Verify Entered Sales Report")
	public void VerifySalesReportFinance() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String agrmntVrsnName = fc.utobj().generateTestData("AgreementVersion");
		String royaltyPrcntg = "10";
		String advPercentage = "5";
		String royaltyAreaFranchiseValue = "4";
		String advAreaFranchiseValue = "3";


		try {
			driver = fc.loginpage().login(driver);
			AdminFinanceFinanceSetupPreferencesPage pobj = new AdminFinanceFinanceSetupPreferencesPage(driver);
			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName, royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("Test");
			String storeType = fc.utobj().generateTestData("Test");
			String franchiseId = fc.utobj().generateTestData("Test");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config,
					salesReportType);
			
			boolean isNormalSalesReport=false;
			
			try {
				fc.utobj().clickElement(driver, pobj.financeAndCrmTransaction);
				isNormalSalesReport=fc.utobj().isElementPresent(driver, pobj.isFinanceCRMTransaction);
			} catch (Exception e) {
				isNormalSalesReport=false;
			}
			
			try {
				if (!isNormalSalesReport) {
					if (!fc.utobj().isSelected(driver, pobj.noFinanceCRMTransaction)) {
						fc.utobj().clickElement(driver, pobj.noFinanceCRMTransaction);
					}
					fc.utobj().clickElement(driver, pobj.saveCRMFinanceTransaction);
				}
			} catch (Exception e) {
				Reporter.log("Not able to save Finance CRM Transaction Integration : "+e.getMessage().toString());
			}
			
			if (isNormalSalesReport) {
				String reportId=new FinanceSalesPageTest().enterSalesReportFinanceCRMTransactionEnable(driver, franchiseId);
				
				fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
				new FinanceSalesPageTest().filterSalesReport(driver, franchiseId);

				fc.utobj().printTestStep("View Sales Report By Action Image Icon");
				
				boolean isReportIdPresent=fc.utobj().assertPageSource(driver, reportId);
				if (!isReportIdPresent) {
					fc.utobj().throwsException("Not able to verify Finance Sales Report");
				}
			}else {
				fc.utobj().printTestStep("Navigate To Finance > Sales");
				fc.utobj().printTestStep("Enter Sales Report");
				String categoryQuantity = "110";
				String categoryAmount = "6000";
				String reportId = new FinanceSalesPageTest().enterSalesReport(driver, franchiseId, categoryQuantity,
						categoryAmount);

				fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
				new FinanceSalesPageTest().filterSalesReport(driver, franchiseId);

				fc.utobj().printTestStep("View Sales Report By Action Image Icon");
				fc.utobj().actionImgOption(driver, reportId, "View Sales Details");

				fc.utobj().printTestStep("Verify Total Sales Quantity");
				String totalQuantity = fc.utobj().getText(driver, fc.utobj().getElementByXpath(driver,
						".//td[contains(text () ,' Total Sales')]/following-sibling::td[1]"));
				try {

					if (totalQuantity.contains(",")) {

						totalQuantity = totalQuantity.replaceAll(",", "");
					}

					float quantitiyValue = Float.parseFloat(totalQuantity);
					float categoryQuantityFloat = Float.parseFloat(categoryQuantity);

					if (quantitiyValue != categoryQuantityFloat) {

						fc.utobj().throwsException("was not able to verify Total Sales Quantitiy Value");
					}
				} catch (Exception e) {
					fc.utobj().throwsException("was not able to verify Total Sales Quantitiy Value");
				}

				fc.utobj().printTestStep("Verify Total Sales Amount");
				String totalAmount = fc.utobj().getText(driver, fc.utobj().getElementByXpath(driver,
						".//td[contains(text () ,' Total Sales')]/following-sibling::td[2]"));

				try {

					if (totalAmount.contains(",")) {
						totalAmount = totalAmount.replaceAll(",", "");
					}

					float amountValue = Float.parseFloat(totalAmount);
					float categoryAmountFloat = Float.parseFloat(categoryAmount);

					if (amountValue != categoryAmountFloat) {

						fc.utobj().throwsException("was not able to verify Total Sales Amount Value");
					}
				} catch (Exception e) {
					fc.utobj().throwsException("was not able to verify Total Sales Amount Value");
				}
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "FcSanity"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add Category At Admin Training Course Management Page", testCaseId = "TC_Add_Training_Category")
	private void addCategoryTest() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			AdminTrainingCoureseManagementPage pobj = new AdminTrainingCoureseManagementPage(driver);
			fc.training().training_common().adminTraingCourseManagementPage(driver);

			fc.utobj().printTestStep("Manage Category > Add New Category");

			fc.utobj().clickElement(driver, pobj.manageCategory);
			fc.utobj().clickElement(driver, pobj.addCategory);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);

			String categoryName = fc.utobj().generateTestData("TestCategory");
			fc.utobj().sendKeys(driver, pobj.categoryName, categoryName);

			String categoryDescription = fc.utobj().generateTestData("Categorydesc");
			fc.utobj().sendKeys(driver, pobj.categoryDescription, categoryDescription);
			fc.utobj().clickElement(driver, pobj.addCategoryButton);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//div[@data-role='ico_Filter']"));
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='categoryName']"), categoryName);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='search']"));
			boolean isTextPresent = fc.utobj().assertPageSource(driver, categoryName);
			if (isTextPresent == false) {
				fc.utobj().throwsException("Was not able to add Category !!!");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "FcSanity"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add Document At The Hub > Library", testCaseId = "TC_Added_Document")
	private void verifyUploadedDocument() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			SearchUI search_page = new SearchUI(driver);
			AdminTheHubLibraryPageTest libraryPage = new AdminTheHubLibraryPageTest();

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData("TestFolder");
			String folderSummary = fc.utobj().generateTestData("FolderSumm");
			String summaryFormat = "HTML";
			String accessTo = "Regional";
			String htmlText="TestHtmlText";
			
			Library library=new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			library.setHtmlText(htmlText);
			libraryPage.addFolder(driver,library);
			
			fc.utobj().printTestStep("Add Document In Folder");
			String documentTitle = fc.utobj().generateTestData("DocTitle");
			String briefSummary = fc.utobj().generateTestData("BriefSum");
			String currentDate = fc.utobj().getCurrentDateUSFormat();
			String afterDate = "2";
			int val = Integer.parseInt(afterDate);
			String futureDate = fc.utobj().getFutureDateUSFormat(val);

			library.setDocumentType("Web Link");
			library.setLinkUrl("www.franconnect.test.net/pdfDownload");
			library.setRecommendedDoc(true);
			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setCurrentDate(currentDate);
			library.setFutureDate(futureDate);
			libraryPage.addDocument(driver,library);

			fc.utobj().printTestStep("Navigate To The Hub > Library > Home");
			fc.hub().hub_common().theHubLibrarySubModule(driver);

			fc.utobj().printTestStep("Verify Recently Uploaded");
			fc.utobj().isTextDisplayed(driver, documentTitle, "was not able to verify Recent Uploaded document");
			
			
			fc.utobj().printTestStep("Verify Added Hub Documents search by SOLR search");

			boolean isSearchTrue = false;
			for (int i = 0; i < 3; i++) {
				if (isSearchTrue == false) {
					fc.utobj().sendKeys(driver, search_page.topSearchField, documentTitle);
					Thread.sleep(5000);
					if (i == 2) {
						Thread.sleep(2000);
					}
					fc.utobj().clickEnterOnElement(driver, search_page.topSearchField);

					try {
						isSearchTrue = fc.utobj().isElementPresent(driver, fc.utobj().getElementByXpath(driver,
								".//custom[contains(text () , '" + documentTitle + "')]"));
					} catch (Exception e) {
						Reporter.log(e.getMessage());
					}
				}
			}

			if (isSearchTrue == false) {
				fc.utobj().throwsException("was not able to search Document By SOLR Search");
			}
			fc.utobj().refresh(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "FcSanity" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-29", testCaseDescription = "Verify the Create Ticket At Support Ticket Page", testCaseId = "TC_Create_Support_Ticket")
	private void createTicket01() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			SupportTicketsPage pobj = new SupportTicketsPage(driver);
			SearchUI search_page = new SearchUI(driver);
			fc.utobj().printTestStep("Navigate to Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest cu = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			String emailId = "supportautomation@staffex.com";

			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			cu.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			String userName = corpUser.getuserFullName();

			fc.utobj().printTestStep("Navigate To Admin > Support > Manage Department");
			fc.utobj().printTestStep("Add Department");
			String departmentName = fc.utobj().generateTestData("TestD");
			AdminSupportManageDepartmentPageTest departmentPage = new AdminSupportManageDepartmentPageTest();
			departmentPage.addDepartments(driver, departmentName, userName);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");
			AdminFranchiseLocationAddFranchiseLocationPageTest addFranPage = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String franchiseId = fc.utobj().generateTestData("TestF");
			String regionName = fc.utobj().generateTestData("TestR");
			String storeType = fc.utobj().generateTestData("TestS");
			franchiseId = addFranPage.addFranchiseLocation_All(driver, franchiseId, regionName, storeType);

			fc.utobj().printTestStep("Navigate To Support > Ticket");
			fc.utobj().printTestStep("Create Ticket");
			fc.support().support_common().supportTickets(driver);
			fc.utobj().clickElement(driver, pobj.createTicketLink);

			fc.utobj().selectDropDown(driver, pobj.departmentDropDown, departmentName);
			fc.utobj().selectDropDown(driver, pobj.priorityDropDown, "Low");
			fc.utobj().selectDropDown(driver, pobj.franchiseIdDropDown, franchiseId);
			fc.utobj().sendKeys(driver, pobj.phoneTextBox, "1236547893");
			String subject = fc.utobj().generateTestData("TestSubject");
			fc.utobj().sendKeys(driver, pobj.subjectTextBox, subject);
			String description = fc.utobj().generateTestData("TestDes");
			fc.utobj().sendKeys(driver, pobj.descriptionTextBox, description);
			String file = fc.utobj().getFilePathFromTestData("taskFile.pdf");

			fc.utobj().sendKeys(driver, pobj.attachFileBrowseBox, file);
			fc.utobj().clickElement(driver, pobj.submitButton);
			fc.utobj().clickElement(driver, pobj.okBtn);

			fc.utobj().printTestStep("Verify Added Support Ticket search by SOLR search");

			boolean isSearchTrue = false;
			for (int i = 0; i < 3; i++) {
				if (isSearchTrue == false) {
					fc.utobj().sendKeys(driver, search_page.topSearchField, subject);
					Thread.sleep(5000);
					if (i == 2) {
						Thread.sleep(2000);
					}
					fc.utobj().clickEnterOnElement(driver, search_page.topSearchField);

					try {
						isSearchTrue = fc.utobj().verifyElementOnVisible_ByXpath(driver,
								".//custom[contains(text () , '" + subject + "')]");
					} catch (Exception e) {
						Reporter.log(e.getMessage());
						System.out.println(e.getMessage());
					}
				}
			}

			if (isSearchTrue == false) {
				fc.utobj().throwsException("Not able to search Ticket By SOLR Search");
			}

			fc.utobj().refresh(driver);

			new SupportTicketsPageTest().searchTicketBySubject(driver, subject);

			fc.utobj().printTestStep("Verify Create Ticket");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, subject);

			if (isTextPresent == false) {
				fc.utobj().throwsSkipException("was not able to verify subject of Ticket");
			}

			boolean isTextPresent1 = fc.utobj().assertPageSource(driver, departmentName);

			if (isTextPresent1 == false) {
				fc.utobj().throwsSkipException("was not able to verify department Name");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "FcSanity"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Added Store At Opener > Store Summary > Store List.", testCaseId = "TC_Add_Store")
	public void addNewFranchiseLocationTest() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			Map<String, String> data2 = new HashMap<String, String>();
			data2.put("storeNoFranchiseID", "TestStore1");
			data2.put("centerName", "TestCent1");
			data2.put("regionName", "TestAdReg1");
			data2.put("storeType", "TestStType1");
			data2.put("userName", "TestCor1");
			data2.put("projectStatus", "TestPro1");
			data2.put("referenceDate", "TestRef1");
			data2.put("streetAddress", "street street");
			data2.put("address", "address address");
			data2.put("city", "city");
			data2.put("divisionName", "TestDiv1");
			OpenerStoreSummaryStoreListPage pobj = new OpenerStoreSummaryStoreListPage(driver);
			SearchUI search_page = new SearchUI(driver);
			String storeNoFranchiseID = fc.utobj().generateTestData(data2.get("storeNoFranchiseID"));
			String centerName = fc.utobj().generateTestData(data2.get("centerName"));
			String licenseNo = fc.utobj().generateRandomNumber();

			fc.utobj().printTestStep("Navigate To Admin > Add Area/Region");
			fc.utobj().printTestStep("Add Region");

			String regionName = fc.utobj().generateTestData(data2.get("regionName"));
			AdminAreaRegionAddAreaRegionPageTest addRegionPage = new AdminAreaRegionAddAreaRegionPageTest();
			addRegionPage.addAreaRegion(driver, regionName);

			String divisionName = fc.utobj().generateTestData(data2.get("divisionName"));
			AdminDivisionAddDivisionPageTest divisionPage = new AdminDivisionAddDivisionPageTest();
			divisionPage.addDivision(driver, divisionName);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type");
			fc.utobj().printTestStep("Add Store Type");

			String storeType = fc.utobj().generateTestData(data2.get("storeType"));
			AdminConfigurationConfigureStoreTypePageTest storeTypePage = new AdminConfigurationConfigureStoreTypePageTest();
			storeTypePage.addStoreType(driver, storeType);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "openerautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser.setConsultant("Y");
			corPage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			String projectStatus = fc.utobj().generateTestData(data2.get("projectStatus"));
			AdminOpenerConfigureProjectStatusPageTest projectstausPage = new AdminOpenerConfigureProjectStatusPageTest();
			projectstausPage.addProjectStatus(driver, projectStatus);

			String referenceDate = fc.utobj().generateTestData(data2.get("referenceDate"));
			AdminOpenerManageReferenceDatesPageTest refDatePage = new AdminOpenerManageReferenceDatesPageTest();
			refDatePage.addReferenceDate(driver, referenceDate);

			fc.utobj().printTestStep("Navigate To Opener > Store Summary > Add New Franchise Location");
			fc.opener().opener_common().openerStoreSummary(driver);
			fc.utobj().clickElement(driver, pobj.addNewFranchiseLocation);
			fc.utobj().sendKeys(driver, pobj.StoreNoFranchiseID, storeNoFranchiseID);
			fc.utobj().sendKeys(driver, pobj.centerName, centerName);
			fc.utobj().selectDropDown(driver, pobj.areaRegionDrp, regionName);

			try {
				fc.utobj().selectValFromMultiSelect(driver, pobj.selectDivision, divisionName);
			} catch (Exception e) {
				fc.utobj().selectDropDown(driver, pobj.divisionName, divisionName);
			}

			fc.utobj().sendKeys(driver, pobj.licenseNo, licenseNo);
			fc.utobj().selectDropDown(driver, pobj.storeType, storeType);
			fc.utobj().selectDropDown(driver, pobj.corporateLocation, "No");

			String currentDate = fc.utobj().currentDate();
			fc.utobj().sendKeys(driver, pobj.royaltyReportingStartDate, currentDate);

			fc.utobj().selectDropDown(driver, pobj.fbc, corpUser.getuserFullName());

			fc.utobj().sendKeys(driver, pobj.expectedOpeningDate, currentDate);

			fc.utobj().selectDropDown(driver, pobj.projectStatus, projectStatus);
			fc.utobj().sendKeys(driver,
					fc.utobj().getElementByXpath(driver,
							".//td[contains(text () ,'" + referenceDate + "')]/following-sibling::td[1]//input"),
					currentDate);

			try {
				List<WebElement> list = driver.findElements(
						By.xpath(".//select[contains(@id,'franchiseeS') and contains(@name,'franchiseeS') ]"));
				System.out.println(list.size());

				for (int i = 0; i < list.size(); i++) {
					String id = list.get(i).getAttribute("id").trim();
					fc.utobj().selectDropDown(driver, fc.utobj().getElementByID(driver, id),
							"FranConnect Administrator");
				}
			} catch (Exception e) {
			}

			fc.utobj().sendKeys(driver, pobj.streetAddressTextBox, data2.get("streetAddress"));
			fc.utobj().sendKeys(driver, pobj.address2TextBox, data2.get("address"));
			fc.utobj().sendKeys(driver, pobj.cityTextBox, data2.get("city"));
			fc.utobj().selectDropDown(driver, pobj.countryDropdown, "USA");
			fc.utobj().sendKeys(driver, pobj.zipcodeTextBox, "123456");
			fc.utobj().selectDropDown(driver, pobj.stateProvinceDropdown, "Alaska");
			fc.utobj().sendKeys(driver, pobj.PhoneTextBox, "1236547896");
			fc.utobj().sendKeys(driver, pobj.ownerFirstName, "firstName");
			fc.utobj().sendKeys(driver, pobj.ownerLastName, "lastName");
			fc.utobj().clickElement(driver, pobj.addBtn);

			fc.opener().opener_common().openerStoreSummary(driver);
			new OpenerStoreSummaryStoreListPageTest().setDefaultFilterOpener(driver, storeNoFranchiseID);

			fc.utobj().printTestStep("Verify The Add New Franchise Location");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, storeNoFranchiseID);

			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to add new franchise Location");
			}
			
			fc.utobj().printTestStep("Verify Added Store Franchise Location Search by SOLR Search");

			boolean isSearchTrue = false;
			for (int i = 0; i < 3; i++) {
				if (isSearchTrue == false) {
					fc.utobj().sendKeys(driver, search_page.topSearchField, storeNoFranchiseID);
					Thread.sleep(5000);
					if (i == 2) {
						Thread.sleep(2000);
					}
					fc.utobj().clickEnterOnElement(driver, search_page.topSearchField);

					try {
						Thread.sleep(2000);
						isSearchTrue = fc.utobj().isElementPresent(driver
								.findElements(By.xpath(".//custom[contains(text () , '" + storeNoFranchiseID + "')]")));
					} catch (Exception e) {
						Reporter.log(e.getMessage());
						System.out.println(e.getMessage());
					}
				}
			}

			if (isSearchTrue == false) {
				fc.utobj().throwsException("Not able to search Store through SOLR Search");
			}
			fc.utobj().refresh(driver);
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "FcSanity"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Add_Lead", testCaseDescription = "Verify Add lead from Lead Management Page and check all details")
	void addLeadSales() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		String firstName = fc.utobj().generateTestData("Fname");
		String lastName = fc.utobj().generateTestData("Lname");

		try {
			driver = fc.loginpage().login(driver);
			SearchUI search_page = new SearchUI(driver);
			String userName = "FranConnect Administrator";
			String email="salesautomation@staffex.com";

			fc.utobj().printTestStep("Navigate To Sales > Lead Summary");
			fc.sales().sales_common().fsModule(driver);
			fc.utobj().printTestStep("Add a lead");

			Sales sales = new Sales();
			sales.leadManagement(driver);
			new FSLeadSummaryPageTest().clickAddLeadLink(driver);

			FSLeadSummaryAddLeadPage pobj = new FSLeadSummaryAddLeadPage(driver);
			fc.utobj().sendKeys(driver, pobj.firstName, firstName);
			fc.utobj().sendKeys(driver, pobj.lastName, lastName);
			fc.utobj().selectDropDownByVisibleText(driver, pobj.country, "USA");

			fc.utobj().selectDropDownByVisibleText(driver, pobj.state, "Alabama");
			fc.utobj().sendKeys(driver, pobj.city, "TestCity");
			fc.utobj().sendKeys(driver, pobj.phone, "9214578963");
			fc.utobj().sendKeys(driver, pobj.emailID, email);
			fc.utobj().sendKeys(driver, pobj.zip, "123456");
			fc.utobj().sendKeys(driver, pobj.phone, "12345678");
			fc.utobj().sendKeys(driver, pobj.address, "New Highway Road");
			fc.utobj().selectDropDownByVisibleText(driver, pobj.leadOwnerID, userName);
			fc.utobj().selectDropDownByIndex(driver, pobj.leadSource2ID, 1);
			fc.utobj().selectDropDownByIndex(driver, pobj.leadSource3ID, 1);
			
			fc.utobj().printTestStep("Enter Data In Custom Field");
			enterDataCustomField(driver);
			
			fc.utobj().clickElement(driver, pobj.save);
			String leadName = firstName + " " + lastName;

			fc.utobj().printTestStep("Verifry The Added Lead At Primary Info Page");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, firstName);

			if (isTextPresent == false) {
				fc.utobj().throwsException("Not able to add Lead");
			}

			fc.utobj().printTestStep("Verify The Added Lead Search By SOLR Search");
			boolean isSearchTrue = false;
			for (int i = 0; i < 3; i++) {
				if (isSearchTrue == false) {
					fc.utobj().sendKeys(driver, search_page.topSearchField, leadName);

					if (i == 2) {

					}
					fc.utobj().clickEnterOnElement(driver, search_page.topSearchField);

					try {
						fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
								".//custom[contains(text () , '" + leadName + "')]"));
						isSearchTrue = true;
					} catch (Exception e) {
						Reporter.log(e.getMessage());
						System.out.println(e.getMessage());
					}
				}
			}

			if (isSearchTrue == false) {
				fc.utobj().throwsException("was not able to search Lead through SOLR Search");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "FcSanity" })
	@TestCase(createdOn = "2017-08-14", updatedOn = "2017-08-14", testCaseDescription = "Verify The Added Corporate User", testCaseId = "TC_Add_Corporate_User")
	private void addCorporateUser() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminUsersManageCorporateUsersAddCorporateUserPage pobj = new AdminUsersManageCorporateUsersAddCorporateUserPage(
					driver);

			fc.utobj().printTestStep("Navigate To Admin Users > Manage Corporate Users > Add Corporate User");
			fc.adminpage().adminUsersManageCorporateUsersAddCorporateUserPage(driver);
			String userName = fc.utobj().generateTestData("Testcu");

			fc.utobj().sendKeys(driver, pobj.userName, userName);
			fc.utobj().sendKeys(driver, pobj.password, "T0n1ght1");
			fc.utobj().sendKeys(driver, pobj.confirmPassword, "T0n1ght1");
			fc.utobj().selectDropDown(driver, pobj.type, "Normal User");

			fc.utobj().clickElement(driver, pobj.rolesBtn);
			fc.utobj().sendKeys(driver, pobj.searchRoles, "Corporate Administrator");
			fc.utobj().clickElement(driver, pobj.selectAll);

			fc.utobj().selectDropDown(driver, pobj.timeZone, "GMT -06:00 US/Canada/Central");

			fc.utobj().sendKeys(driver, pobj.firstName, userName);
			fc.utobj().sendKeys(driver, pobj.lastName, userName);

			fc.utobj().sendKeys(driver, pobj.city, "11");
			fc.utobj().selectDropDown(driver, pobj.country, "USA");
			fc.utobj().selectDropDown(driver, pobj.state, "Colorado");

			fc.utobj().sendKeys(driver, pobj.phone1, "1234567890");
			fc.utobj().sendKeys(driver, pobj.phoneExt1, "2");
			fc.utobj().sendKeys(driver, pobj.email, "testautomation@franqa.net");

			try {
				if (fc.utobj().isSelected(driver,pobj.consultant)) {
				} else {
					fc.utobj().clickElement(driver, pobj.consultant);
				}
			} catch (Exception e) {
			}

			fc.utobj().clickElement(driver, pobj.submit);

			fc.utobj().printTestStep("Verify The Added Corporate Users");
			String userFullName = userName + " " + userName;
			new AdminUsersManageCorporateUsersPageTest().searchCorporateUsers(driver, userFullName);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "FcSanity"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add Lead At CRM > Lead > Lead Summary", testCaseId = "TC_58_Add_Lead")
	public void addLead() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);
			SearchUI search_page = new SearchUI(driver);
			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");
			fc.crm().crm_common().CRMLeadsLnk(driver);

			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.leadLnk);
			fc.utobj().selectDropDown(driver, pobj.title, dataSet.get("title"));
			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			fc.utobj().sendKeys(driver, pobj.leadFirstName, firstName);
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			fc.utobj().sendKeys(driver, pobj.leadLastName, lastName);
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			fc.utobj().sendKeys(driver, pobj.companyName, company);
			fc.utobj().selectDropDown(driver, pobj.primaryContactMethodSelect, dataSet.get("primaryContactMethod"));
			if (!fc.utobj().isSelected(driver, pobj.assignToCorporate)) {
				fc.utobj().clickElement(driver, pobj.assignToCorporate);
			}
			fc.utobj().selectDropDown(driver, pobj.selectCorporateUser, corpUser.getuserFullName());
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			fc.utobj().sendKeys(driver, pobj.address, address);
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			fc.utobj().sendKeys(driver, pobj.city, city);
			fc.utobj().selectDropDown(driver, pobj.country, "USA");
			fc.utobj().selectDropDown(driver, pobj.state, "Alabama");
			fc.utobj().sendKeys(driver, pobj.zipcode, "12345");
			fc.utobj().sendKeys(driver, pobj.phoneNumbers, "1236547896");
			fc.utobj().sendKeys(driver, pobj.extn, "12");
			fc.utobj().sendKeys(driver, pobj.faxNumbers, "1263985487");
			fc.utobj().sendKeys(driver, pobj.mobileNumbers, "8896724441");
			String email = dataSet.get("emailId");
			fc.utobj().sendKeys(driver, pobj.emailIds, email);
			fc.utobj().sendKeys(driver, pobj.alternateEmail, "test@gmail.com");
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			fc.utobj().sendKeys(driver, pobj.suffix, suffix);
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			fc.utobj().sendKeys(driver, pobj.position, jobTitle);
			/*fc.utobj().sendKeys(driver, pobj.birthdate, "11/24/1980");
			fc.utobj().sendKeys(driver, pobj.anniversarydate, "11/24/1999");*/
			fc.utobj().selectDropDown(driver, pobj.rating, dataSet.get("rating"));
			fc.utobj().sendKeys(driver, pobj.bestTimeToContact, "12:00 PM EST");
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			fc.utobj().sendKeys(driver, pobj.comments, comment);
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("Verify The Added Lead By Top Search");
			String leadName = firstName + " " + lastName;

			boolean isSearchTrue = false;
			for (int i = 0; i < 3; i++) {
				if (isSearchTrue == false) {
					fc.utobj().sendKeys(driver, search_page.topSearchField, leadName);
					Thread.sleep(2000);
					if (i == 2) {
						Thread.sleep(2000);
					}
					fc.utobj().clickEnterOnElement(driver, search_page.topSearchField);

					try {
						isSearchTrue = fc.utobj().verifyElementOnVisible_ByXpath(driver,
								".//custom[contains(text () , '" + leadName + "')]");
					} catch (Exception e) {
						Reporter.log(e.getMessage());
						System.out.println(e.getMessage());
					}
				}
			}

			if (isSearchTrue == false) {
				fc.utobj().throwsException("Not able to search Lead By Top Search");
			}

			fc.utobj().refresh(driver);
			fc.utobj().printTestStep("Verify Lead At Lead Home Page");
			fc.utobj().clickElement(driver, pobj.leadsLink);
			new CRMLeadsPageTest().searchLeadByOwner(driver, corpUser.getuserFullName(), "Open Leads");

			boolean verifyName = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + firstName + " " + lastName + "']");
			if (verifyName == false) {
				fc.utobj().throwsException("was not able to verify Name");
			}
			boolean ownerOfLead = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + firstName + " " + lastName
					+ "']/ancestor::tr/td[contains(text () , '" + corpUser.getuserFullName() + "')]");
			if (ownerOfLead == false) {
				fc.utobj().throwsException("was not able to verify owner of the lead");
			}
			/*boolean statusOfLead = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + firstName + " " + lastName + "']/ancestor::tr/td[contains(text () , 'New')]");
			if (statusOfLead == false) {
				fc.utobj().throwsException("was not able to verify status of the lead");
			}*/
			boolean emailOfLead = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + firstName + " " + lastName
					+ "']/ancestor::tr/td/a[contains(text () , '" + email + "')]");
			if (emailOfLead == false) {
				fc.utobj().throwsException("was not able to verify email of the lead");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	public void enterDataCustomField(WebDriver driver) throws Exception{
		//fill default value in custom field if any field is mandatory
		
		try {
			List<WebElement> listElement=fc.utobj().getElementListByXpath(driver, ".//*[@id='leadPrimaryInfoForm']//span[@class='urgent_fields' and not(@style='visibility:hidden')]/parent::*/following-sibling::td[position()=1]/*[contains(@id ,'_') or contains(@name , '_')]");
			for (int i = 0; i < listElement.size(); i++) {
				
				String classNameAtrribute=fc.utobj().getAttributeValue(listElement.get(i), "class");
				
				//multi select drodown
				if (classNameAtrribute.equalsIgnoreCase("ms-parent")) {
					
					try {
						WebElement element=fc.utobj().getElementByID(driver, fc.utobj().getAttributeValue(listElement.get(i), "id"));
						
						fc.utobj().clickElement(driver, element);
						fc.utobj().clickElement(driver, element.findElement(By.xpath(".//input[@id='selectAll']")));
						fc.utobj().clickElement(driver, element);
					} catch (Exception e) {
					}
				
				//select drop down	
				}else if (classNameAtrribute.equalsIgnoreCase("multiList")) {
					try {
						WebElement element=fc.utobj().getElementByID(driver, fc.utobj().getAttributeValue(listElement.get(i), "id"));
						fc.utobj().selectDropDownByIndex(driver, element, 1);
					} catch (Exception e) {
						// TODO: handle exception
					}
					
					
				//text box
				}else if (classNameAtrribute.equalsIgnoreCase("fTextBox")) {
					try {
						
						String id=fc.utobj().getAttributeValue(listElement.get(i), "id");
						
						WebElement element=fc.utobj().getElementByID(driver, id);
						
						if (id.contains("phone")||id.contains("Phone")) {
							fc.utobj().sendKeys(driver, element, "1245879632");
						}else {
							fc.utobj().sendKeys(driver, element, fc.utobj().generateRandomChar());
						}
					} catch (Exception e) {
					}
				//textBox Date field
				}else if (classNameAtrribute.equalsIgnoreCase("fTextBoxDate")) {
					WebElement element=fc.utobj().getElementByID(driver, fc.utobj().getAttributeValue(listElement.get(i), "id"));
					fc.utobj().sendKeys(driver, element, fc.utobj().getCurrentDateUserDefineTimeZoneUSFormat("GMT-5:00"));
				}
			}
			
		} catch (Exception e) {
			Reporter.log("No custome field is available in sale lead page");
		}
		
		try {
			List<WebElement> listProfile=fc.utobj().getElementListByXpath(driver, ".//*[contains(@id,'franchiseeS')]");
			for (int i = 0; i < listProfile.size(); i++) {
				fc.utobj().selectDropDownByIndex(driver, listProfile.get(i), 1);
			}
		} catch (Exception e) {
			Reporter.log("No Profile is available");
		}
	}
	
	@Test(groups = {"takeQuizdev"})
	@TestCase(createdOn = "2018-01-03", updatedOn = "2017-01*03", testCaseDescription = "Verify the quiz functionality in its preview", testCaseId = "Tc_Verify_quiz_functionality_preview")
	private void validateQuizFunctionalityPreview() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			driver = fc.loginpage().login(driver);
			
			fc.utobj().printTestStep("Navigate To Admin Training Course Management Page");
			AdminTrainingCoureseManagementPage pobj = new AdminTrainingCoureseManagementPage(driver);
			fc.training().training_common().adminTraingCourseManagementPage(driver);
			fc.utobj().clickElement(driver, pobj.manageCategory);
			
			fc.utobj().printTestStep("Add Category");
			
			String categoryName=fc.utobj().generateTestData("Testcatname");
			String categoryDes=fc.utobj().generateTestData("TestcategoryDes");
			
			fc.utobj().clickElement(driver, pobj.addCategory);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			fc.utobj().sendKeys(driver, pobj.categoryName, categoryName);
			fc.utobj().sendKeys(driver, pobj.categoryDescription, categoryDes);
			fc.utobj().clickElement(driver, pobj.addCategoryButton);
			fc.utobj().switchFrameToDefault(driver);
			
			fc.utobj().printTestStep("Add Course");
			Thread.sleep(1000);
			fc.utobj().clickElement(driver, ".//div[@data-role='ico_Filter']");
			Thread.sleep(1000);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='categoryName']"),
					categoryName);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='search']"));
			String xpath = ".//*[contains(text () , '" + categoryName
					+ "')]/ancestor::div/following-sibling::div//div[@data-role='ico_ThreeDots']";
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, xpath));
			Thread.sleep(1000);
			fc.utobj().clickElement(driver, pobj.addCourse);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			Thread.sleep(1000);
			
			String courseName=fc.utobj().generateTestData("Testcoursename");
			String courseobj=fc.utobj().generateTestData("Testcourseobj");
			String courseInstructions=fc.utobj().generateTestData("Testcourseinstruction");
			
			fc.utobj().sendKeys(driver, pobj.courseName, courseName);
			fc.utobj().sendKeys(driver, pobj.courseObjective, courseobj);
			fc.utobj().clickElement(driver, ".//*[@id='fc-drop-parentuserCombo']/button");
			
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			WebElement element = driver.findElement(By.xpath(".//*[@id='fc-drop-parentuserCombo']/div/div/input"));
			try {
				jse.executeScript("arguments[0].value='FranConnect Administrator'", element);
				Actions actions = new Actions(driver);
				Thread.sleep(1000);
				actions.moveToElement(element);
				actions.sendKeys(Keys.ENTER).click(element).build().perform();
				actions.release().perform();
				actions.sendKeys(Keys.ENTER).click(element).keyUp(Keys.ENTER).build().perform();
				Thread.sleep(1000);
			} catch (Exception e) {
				fc.utobj().clickElement(driver, ".//*[@id='fc-drop-parentuserCombo']/button");
				fc.utobj().sendKeys(driver, element, "FranConnect Administrator");
				Thread.sleep(5000);
				element.sendKeys(Keys.ENTER);
				Thread.sleep(1000);
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//label/span[contains(text(),'FranConnect Administrator')]"));
			}

			fc.utobj().sendKeys(driver, pobj.instructions, courseInstructions);
			fc.utobj().clickElement(driver, pobj.addCourseButton);
			fc.utobj().switchFrameToDefault(driver);
			
			fc.utobj().printTestStep("Create Lesson");
			fc.utobj().clickElement(driver, pobj.addLesson);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			String sectionTitle = fc.utobj().generateTestData("sectionTitle");
			fc.utobj().sendKeys(driver, pobj.sectionTitle, sectionTitle);
			String summary = fc.utobj().generateTestData("summary");
			fc.utobj().sendKeys(driver, pobj.summary, summary);
			fc.utobj().clickElement(driver, pobj.addLesson);
			driver.switchTo().defaultContent();
			fc.utobj().clickElement(driver, pobj.uploadFile);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			String fileName = fc.utobj().getFilePathFromTestData("taskFile.pdf");
			fc.utobj().sendKeys(driver, pobj.fileuploader, fileName);
			
			fc.utobj().printTestStep("Create Quiz");
			
			String quizquestion="Which date is today ??";
			String quizname = "NewQuiz" + fc.utobj().generateRandomNumber();
			fc.utobj().clickElement(driver, pobj.createQuizButton);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			fc.utobj().sendKeys(driver, pobj.quizName, quizname);
			fc.utobj().sendKeys(driver, pobj.quizSummary, "This is a Quiz");
			fc.utobj().clickElement(driver, pobj.addquiz);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, pobj.AddNewQuestion);
			fc.utobj().clickElement(driver, pobj.ResponseTypeDate);
			fc.utobj().sendKeys(driver, pobj.text1, quizquestion);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='questionMarks1']/option[@value=\"2\"]"));
			fc.utobj().clickElement(driver, pobj.saveQuiz);
			fc.utobj().clickElement(driver, pobj.back);
			fc.utobj().clickElement(driver, pobj.publishcourse);
			fc.utobj().clickElement(driver, pobj.Confirm);
			
			fc.utobj().printTestStep("Navigate To Training > Course");
			AdminTrainingPlanAndCertificatePage pobjpnc = new AdminTrainingPlanAndCertificatePage(driver);
			fc.training().training_common().trainingCourse(driver);
			fc.utobj().clickElement(driver, pobjpnc.availablePlan);
			fc.utobj().clickElement(driver, pobjpnc.FilterBtn);
			fc.utobj().sendKeys(driver, pobjpnc.coursename, courseName);
			fc.utobj().clickElement(driver, pobjpnc.applyFilters);
			fc.utobj().clickElement(driver, pobjpnc.start);
			
			String parentWindowHandle=driver.getWindowHandle();
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//button[contains(@onclick, 'startT')]"));
			Set<String> allWindowHandles1 = driver.getWindowHandles();
			
			for (String currentWindowHandle : allWindowHandles1) {
				if (!currentWindowHandle.equals(parentWindowHandle)) {
					driver.switchTo().window(currentWindowHandle);
					
					try {
						fc.utobj().getElementByXpath(driver, ".//p[contains(text () , '" + sectionTitle + "')]")
						.isDisplayed();
						fc.utobj().moveToElement(driver, fc.utobj().getElement(driver, pobjpnc.startquiz));
						fc.utobj().clickElement(driver, pobjpnc.startquiz);
					} catch (Exception e) {
					}
					driver.switchTo().window(parentWindowHandle);
				}
			}
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			fc.utobj().clickElement(driver, pobjpnc.startquiz);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, pobjpnc.insertDate);
			
			DateFormat dateFormat2 = new SimpleDateFormat("dd");
			Date date2 = new Date();
			String today = dateFormat2.format(date2);
			
			WebElement dateWidget = fc.utobj().getElementByID(driver, "fc-datepicker-div");
			List<WebElement> columns = dateWidget.findElements(By.tagName("a"));
			for (WebElement cell : columns) {
				if (cell.getText().equals(today)) {
					cell.click();
					break;
				}
			}
			fc.utobj().clickElement(driver, pobjpnc.finishQuiz);
			fc.utobj().clickElement(driver, pobjpnc.SubmitQuiz);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			fc.utobj().clickElement(driver, pobjpnc.Close);
			
			fc.utobj().printTestStep("Verify Retake quiz button in visible");
			boolean isRetakeQuiz=fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'Retake Quiz')]");
			if (!isRetakeQuiz) {
				fc.utobj().throwsException("Not able take quiz");
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
}
