package com.builds.test.support;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.common.CorporateUser;
import com.builds.uimaps.support.AdminSupportManageContactInformationPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminSupportManageContactInformationPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "support" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-09-07", testCaseDescription = "Verify the Add Contact Address At Admin > Support > Add New Contact", testCaseId = "TC_20_Add_Contact_Address")
	private void addContactAddress() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("support", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			AdminSupportManageContactInformationPage pobj = new AdminSupportManageContactInformationPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate Users");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest cu = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "supportautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			cu.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			String userName = corpUser.getuserFullName();

			fc.utobj().printTestStep("Navigate to Admin > Support > Manage Department");
			fc.utobj().printTestStep("Add Department");

			String departmentName = fc.utobj().generateTestData(dataSet.get("departmentName"));
			AdminSupportManageDepartmentPageTest addDepartmentPage = new AdminSupportManageDepartmentPageTest();
			addDepartmentPage.addDepartments(driver, departmentName, userName);

			fc.utobj().printTestStep("Navigate To Admin > Support > Manage Contact Information");
			fc.support().support_common().adminSupportManageContactInformationAddNewContact(driver);

			fc.utobj().printTestStep("Add Contact Address");

			fc.utobj().clickElement(driver, pobj.addContactAddressLnk);
			fc.utobj().selectDropDown(driver, pobj.departmentName, departmentName);
			String contactName = fc.utobj().generateTestData(dataSet.get("contactName"));
			fc.utobj().sendKeys(driver, pobj.firstName, contactName);
			fc.utobj().sendKeys(driver, pobj.lastName, contactName);
			fc.utobj().sendKeys(driver, pobj.address, "Address1");
			fc.utobj().sendKeys(driver, pobj.address2, "23423");
			fc.utobj().sendKeys(driver, pobj.city, "2234234");
			fc.utobj().selectDropDown(driver, pobj.countryId, "USA");
			fc.utobj().sendKeys(driver, pobj.zipCode, "234234");
			fc.utobj().selectDropDown(driver, pobj.state, "Alaska");
			fc.utobj().sendKeys(driver, pobj.phone1, "2234234");
			fc.utobj().sendKeys(driver, pobj.email, "test@gmail.com");
			fc.utobj().sendKeys(driver, pobj.fax, "234234");
			fc.utobj().sendKeys(driver, pobj.phone2, "435345");
			fc.utobj().clickElement(driver, pobj.addBtn);

			fc.utobj().printTestStep("Verify Contact Address");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, contactName);

			if (isTextPresent == false) {
				fc.utobj().throwsSkipException("was not able to add Contact Address");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "support" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-09-07", testCaseDescription = "Verify the Cancel Button at Add Contact Address At Admin > Support > Add New Contact", testCaseId = "TC_20_Add_Contact_Address_2")
	private void addContactAddressCancelButton() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("support", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			AdminSupportManageContactInformationPage pobj = new AdminSupportManageContactInformationPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate Users");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest cu = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "supportautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			cu.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			String userName = corpUser.getuserFullName();

			fc.utobj().printTestStep("Navigate to Admin > Support > Manage Department");
			fc.utobj().printTestStep("Add Department");

			String departmentName = fc.utobj().generateTestData(dataSet.get("departmentName"));
			AdminSupportManageDepartmentPageTest addDepartmentPage = new AdminSupportManageDepartmentPageTest();
			addDepartmentPage.addDepartments(driver, departmentName, userName);

			fc.utobj().printTestStep("Navigate To Admin > Support > Manage Contact Information");
			fc.support().support_common().adminSupportManageContactInformationAddNewContact(driver);

			fc.utobj().printTestStep("Add Contact Address");

			fc.utobj().clickElement(driver, pobj.addContactAddressLnk);

			fc.utobj().printTestStep("Click on Cancel Button");

			fc.utobj().clickElement(driver, pobj.CancelBtn);

			fc.utobj().printTestStep("Verify the page is redirected to Manage Contact Information");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, "Add Contact Address");

			if (isTextPresent == false) {
				fc.utobj().throwsSkipException(
						"On clicking the cancel button the Manage Contact Information page did not come.");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void addContactAddress(WebDriver driver, String contactName, String departmentName,
			Map<String, String> config) throws Exception {

		String testCaseId = "TC_Add_Contact_Address";

		if (fc.utobj().validate(testCaseId)) {
			try {
				fc.support().support_common().adminSupportManageContactInformationAddNewContact(driver);
				AdminSupportManageContactInformationPage pobj = new AdminSupportManageContactInformationPage(driver);

				fc.utobj().clickElement(driver, pobj.addContactAddressLnk);
				fc.utobj().selectDropDown(driver, pobj.departmentName, departmentName);
				fc.utobj().sendKeys(driver, pobj.firstName, contactName);
				fc.utobj().sendKeys(driver, pobj.lastName, contactName);
				fc.utobj().sendKeys(driver, pobj.address, "Address1");
				fc.utobj().sendKeys(driver, pobj.address2, "23423");
				fc.utobj().sendKeys(driver, pobj.city, "2234234");
				fc.utobj().selectDropDown(driver, pobj.countryId, "USA");

				fc.utobj().sendKeys(driver, pobj.zipCode, "234234");
				fc.utobj().selectDropDown(driver, pobj.state, "Alaska");
				fc.utobj().sendKeys(driver, pobj.phone1, "2234234");
				fc.utobj().sendKeys(driver, pobj.email, "test@gmail.com");
				fc.utobj().sendKeys(driver, pobj.fax, "234234");
				fc.utobj().sendKeys(driver, pobj.phone2, "435345");
				fc.utobj().clickElement(driver, pobj.addBtn);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException(
					"was not able to add Contacts Address :: Admin > Support > Manage > Contact > Information");

		}
	}

	@Test(groups = { "support"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Modification Contact Address At Admin > Support > Add New Contact", testCaseId = "TC_21_Modify_Contact_Address")
	private void modfiyContactAddress() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("support", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.support().support_common().adminSupportManageContactInformationAddNewContact(driver);
			AdminSupportManageContactInformationPage pobj = new AdminSupportManageContactInformationPage(driver);
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			AdminSupportManageDepartmentPageTest addDepartmentPage = new AdminSupportManageDepartmentPageTest();

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest cu = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "supportautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			cu.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			String userName1 = corpUser.getuserFullName();

			fc.utobj().printTestStep("Navigate to Admin > Support > Manage Department");
			fc.utobj().printTestStep("Add Department");

			String departmentName1 = fc.utobj().generateTestData(dataSet.get("departmentName"));
			addDepartmentPage.addDepartments(driver, departmentName1, userName1);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");

			CorporateUser corpUser2 = new CorporateUser();
			corpUser2 = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser2);
			corpUser2.setEmail(emailId);
			corpUser2 = corPage.createDefaultUser(driver, corpUser2);

			fc.utobj().printTestStep("Navigate to Admin > Support > Manage Department");
			fc.utobj().printTestStep("Add Department");

			String departmentName = fc.utobj().generateTestData(dataSet.get("departmentName"));
			addDepartmentPage.addDepartments(driver, departmentName, corpUser2.getuserFullName());

			fc.utobj().printTestStep("Navigate To Admin > Support > Manage Contact Information");
			fc.utobj().printTestStep("Add Contact Address");
			String contactName = fc.utobj().generateTestData(dataSet.get("contactName"));
			addContactAddress(driver, contactName, departmentName, config);

			fc.utobj().printTestStep("Modify Contact Address");
			WebElement el3 = null;
			List<WebElement> list = fc.utobj().getElementListByXpath(driver, ".//*[@id='siteMainTable']/tbody/tr/td/table/tbody/tr[5]/td[2]/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr/td[2]");
			for (WebElement abc : list) {
				if (abc.getText().contains(departmentName) && abc.getText().contains("Department")) {
					el3 = abc.findElement(By.xpath("./ancestor::tr/td[3]/div[@id='menuBar']/layer"));
					break;
				}
			}

			String alterText = el3.getAttribute("id").trim();
			alterText = alterText.replace("Actions_dynamicmenu", "");
			alterText = alterText.replace("Bar", "");
			fc.utobj().clickElement(driver, el3.findElement(By.xpath("./a/img")));
			String actionString = "Modify";
			
			List<String> linkArray = fc.utobj().translate(actionString);
			if (!(linkArray.size() < 1)) {
				for (int i = 0; i < linkArray.size(); i++) {
					try {
						actionString = linkArray.get(i);
						fc.utobj().clickElement(driver,
								fc.utobj().getElementByXpath(driver, ".//div[@id='Actions_dynamicmenu" + alterText
										+ "Menu']/span[contains(text () , '" + actionString + "')]"));
						break;
					} catch (Exception e) {
					}
				}
			} else {
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//div[@id='Actions_dynamicmenu" + alterText + "Menu']/span[contains(text () , 'Modify')]"));
			}
			contactName = fc.utobj().generateTestData(dataSet.get("contactName"));
			fc.utobj().selectDropDown(driver, pobj.departmentName, departmentName1);
			fc.utobj().sendKeys(driver, pobj.firstName, contactName);
			fc.utobj().sendKeys(driver, pobj.lastName, contactName);
			fc.utobj().sendKeys(driver, pobj.address, "Address1");
			fc.utobj().sendKeys(driver, pobj.address2, "23423");
			fc.utobj().sendKeys(driver, pobj.city, "2234234");
			fc.utobj().selectDropDown(driver, pobj.countryId, "USA");

			fc.utobj().sendKeys(driver, pobj.zipCode1, "234234");
			fc.utobj().selectDropDown(driver, pobj.state, "Alaska");
			fc.utobj().sendKeys(driver, pobj.phone1, "2234234");
			fc.utobj().sendKeys(driver, pobj.email, "test@gmail.com");
			fc.utobj().sendKeys(driver, pobj.fax, "234234");
			fc.utobj().sendKeys(driver, pobj.phone2, "435345");
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("Verify The Modify Contact Address");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, contactName);

			if (isTextPresent == false) {
				fc.utobj().throwsSkipException("was not able to modify Contact Address");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "support" ,"supportscroll"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Deletion Contact Address At Admin > Support > Add New Contact", testCaseId = "TC_22_Delete_Contact_Address")
	private void deleteContactAddress() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("support", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminSupportManageDepartmentPageTest addDepartmentPage = new AdminSupportManageDepartmentPageTest();

			fc.utobj().printTestStep("Navigate to Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest cu = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "supportautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			cu.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			String userName = corpUser.getuserFullName();

			fc.utobj().printTestStep("Navigate to Admin > Support > Manage Department");
			fc.utobj().printTestStep("Add Department");
			String departmentName = fc.utobj().generateTestData(dataSet.get("departmentName"));
			addDepartmentPage.addDepartments(driver, departmentName, userName);

			fc.utobj().printTestStep("Add Contact Address");
			String contactName = fc.utobj().generateTestData(dataSet.get("contactName"));
			addContactAddress(driver, contactName, departmentName, config);

			fc.utobj().printTestStep("Delete Contact Address");
			WebElement el3 = null;
			List<WebElement> list = fc.utobj().getElementListByXpath(driver,".//*[@id='siteMainTable']/tbody/tr/td/table/tbody/tr[5]/td[2]/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr/td[2]");
			
			for (WebElement abc : list) {
				if (abc.getText().contains(departmentName) && abc.getText().contains("Department")) {
					el3 = abc.findElement(By.xpath("./ancestor::tr/td[3]/div[@id='menuBar']/layer"));
					break;
				}
			}
			String alterText = el3.getAttribute("id").trim();
			alterText = alterText.replace("Actions_dynamicmenu", "");
			alterText = alterText.replace("Bar", "");
			fc.utobj().clickElement(driver, el3.findElement(By.xpath("./a/img")));
			String deleteString = "Delete";
			List<String> linkArray = fc.utobj().translate(deleteString);
			if (!(linkArray.size() < 1)) {
				for (int i = 0; i < linkArray.size(); i++) {
					try {
						deleteString = linkArray.get(i);
						fc.utobj().clickElement(driver,
								fc.utobj().getElementByXpath(driver, ".//div[@id='Actions_dynamicmenu" + alterText
										+ "Menu']/span[contains(text () , '" + deleteString + "')]"));
						break;
					} catch (Exception e) {
					}
				}
			} else {
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//div[@id='Actions_dynamicmenu"
						+ alterText + "Menu']/span[contains(text () , '" + deleteString + "')]"));
			}

			fc.utobj().acceptAlertBox(driver);
			fc.utobj().printTestStep("Verify Contact Address");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, contactName);

			if (isTextPresent == true) {
				fc.utobj().throwsSkipException("was not able to delete Contact Address");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
}
