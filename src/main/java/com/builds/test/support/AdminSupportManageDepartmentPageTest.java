package com.builds.test.support;

import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.common.CorporateUser;
import com.builds.uimaps.support.AdminSupportManageDepartmentPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminSupportManageDepartmentPageTest {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "support"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify User Department View At Admin > Support > Manage Department", testCaseId = "TC_13_Verify_User_Department_View")
	private void verifyUserDepartmentView() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("support", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminSupportManageDepartmentPage pobj = new AdminSupportManageDepartmentPage(driver);

			fc.utobj().printTestStep("Navigate to Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest cu = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "supportautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			cu.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			String userName = corpUser.getuserFullName();

			fc.utobj().printTestStep("Navigate to Admin > Support > Manage Department");
			fc.utobj().printTestStep("Add Department");
			String departmentName = fc.utobj().generateTestData(dataSet.get("departmentName"));
			addDepartments(driver, departmentName, userName);

			fc.utobj().printTestStep("Verify User Deparment View");
			userDepartmentView(driver, pobj);
			String text = fc.utobj().getText(driver,
					fc.utobj().getElementByXpath(driver, ".//table[@class='summaryTblex']//tr[1]/td[1]/a"));

			boolean istextpresent = false;
			List<String> matchString = fc.utobj().translate("User");
			for (int i = 0; i < matchString.size(); i++) {
				if (text.equalsIgnoreCase(matchString.get(i))) {
					istextpresent = true;
					break;
				}
			}
			if (istextpresent == false) {
				fc.utobj().throwsException("was not able to verify User Department View");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//table[@class='summaryTblex']//tr/td[1][contains(text () , '" + userName + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify user");

			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}

	@Test(groups = { "support", "supportsup023" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-09-08", testCaseDescription = "Verify Department User View At Admin > Support > Manage Department", testCaseId = "TC_14_Verify_Department_User_View")
	private void verifyDepartmentUserView() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("support", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminSupportManageDepartmentPage pobj = new AdminSupportManageDepartmentPage(driver);

			fc.utobj().printTestStep("Navigate to Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest cu = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "supportautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			cu.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			String userName = corpUser.getuserFullName();
			fc.utobj().printTestStep("Navigate to Admin > Support > Manage Department");
			fc.utobj().printTestStep("Add Department");
			String departmentName = fc.utobj().generateTestData(dataSet.get("departmentName"));
			addDepartments(driver, departmentName, userName);

			fc.utobj().printTestStep("Verify Department User View");
			departmentUserView(driver, pobj);

			String text = fc.utobj().getText(driver,
					fc.utobj().getElementByXpath(driver, ".//table[@class='summaryTblex']//tr[1]/td[1]/a"));
			boolean istextpresent = false;
			List<String> matchString = fc.utobj().translate("Department");
			for (int i = 0; i < matchString.size(); i++) {
				if (text.equalsIgnoreCase(matchString.get(i))) {
					istextpresent = true;
					break;
				}
			}
			if (istextpresent == false) {
				fc.utobj().throwsException("was not able to verify User Department View");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//table[@class='summaryTblex']//tr/td[2][contains(text () , '" + userName + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify user");

			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}

	@Test(groups = { "support" , "support0525"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Add Department  At Admin > Support > Manage Department", testCaseId = "TC_15_Add_Department")
	private void addDepartments() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("support", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			AdminSupportManageDepartmentPage pobj = new AdminSupportManageDepartmentPage(driver);
			fc.utobj().printTestStep("Navigate to Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest cu = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "supportautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			cu.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			String userName = corpUser.getuserFullName();

			fc.utobj().printTestStep(
					"Verify Come to support section from admin side and Second link should be of manage department.");

			List<WebElement> listElement = driver.findElements(By.xpath(".//*[@id='Support']/ul/li/a"));

			for (int i = 0; i < listElement.size(); i++) {

				String text = listElement.get(i).getText().trim();
				if (i == 1) {
					if (!text.equalsIgnoreCase(fc.utobj().translateString("Manage Department"))) {
						fc.utobj().throwsException("Second link is not manage department at Admin Support");
						break;
					}
				}
			}
			fc.utobj().printTestStep("Navigate To Admin > Support > Manage Department");
			fc.support().support_common().adminSupportManageDepartment(driver);

			fc.utobj().printTestStep("Verify The Function of close button at add department cbox");
			fc.utobj().clickElement(driver, pobj.addDepartementLnk);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.commonMethods().Click_Close_Input_ByValue(driver);

			boolean isTextPresentAtCbox = fc.utobj().isElementPresent(driver,
					fc.utobj().getElement(driver, pobj.addDepartementLnk));
			if (isTextPresentAtCbox == false) {
				fc.utobj().throwsException("Not able to verify function of close button at cbox");
			}
			fc.utobj().printTestStep("Add Department");
			fc.utobj().clickElement(driver, pobj.addDepartementLnk);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			String departmentName = fc.utobj().generateTestData(dataSet.get("departmentName"));
			fc.utobj().sendKeys(driver, pobj.departmentName, departmentName);
			fc.utobj().sendKeys(driver, pobj.departmentAbbr, "TD");
			fc.utobj().selectValFromMultiSelect(driver, pobj.assignUserSelectBtn, userName);
			fc.utobj().selectDropDown(driver, pobj.defaultOwner, userName);
			fc.utobj().sendKeys(driver, pobj.departmentEmail, "test@gmail.com");
			fc.utobj().clickElement(driver, pobj.submit);
			fc.utobj().clickElement(driver, pobj.close);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().showAll(driver);
			fc.utobj().printTestStep("Verify Added Department");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, departmentName);

			if (isTextPresent == false) {
				fc.utobj().throwsSkipException("Not able to add Department");
			}

			fc.utobj().printTestStep(
					"Verify After Adding department four number of columns should appear that is department, users, date created and action");

			List<WebElement> listElementColumn = driver
					.findElements(By.xpath(".//tr[@qat_maintable='withheader']//tr[1]/td/a"));

			if (listElementColumn.size() == 0) {
				fc.utobj().throwsException("No column is present");
			}

			for (int i = 0; i < listElementColumn.size(); i++) {

				String text = listElementColumn.get(i).getText().trim();

				if (i == 0) {

					if (!text.equalsIgnoreCase(fc.utobj().translateString("Department"))) {
						fc.utobj().throwsException("Not able to verify the Department Column");
					}
				}

				if (i == 1) {

					if (!text.equalsIgnoreCase(fc.utobj().translateString("Date Created"))) {
						fc.utobj().throwsException("Not able to verify the Date Created Column");
					}
				}
			}

			List<WebElement> listElementColumnlist = driver
					.findElements(By.xpath(".//tr[@qat_maintable='withheader']//tr[1]/td"));

			if (listElementColumnlist.size() == 0) {
				fc.utobj().throwsException("No column is present");
			}

			for (int i = 0; i < listElementColumnlist.size(); i++) {

				String text = listElementColumnlist.get(i).getText().trim();

				if (i == 1) {

					if (!text.equalsIgnoreCase(fc.utobj().translateString("User(s)"))) {
						fc.utobj().throwsException("Not able to verify the User(s) Column");
					}
				}

				if (i == 3) {

					if (!text.equalsIgnoreCase(fc.utobj().translateString("Action"))) {
						fc.utobj().throwsException("Not able to verify the Action Column");
					}
				}
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}

	public void addDepartments(WebDriver driver, String departmentName, String userName) throws Exception {

		String testCaseId = "TC_Add_Departments";

		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminSupportManageDepartmentPage pobj = new AdminSupportManageDepartmentPage(driver);

				fc.support().support_common().adminSupportManageDepartment(driver);

				fc.utobj().clickElement(driver, pobj.addDepartementLnk);
				fc.commonMethods().switch_cboxIframe_frameId(driver);

				fc.utobj().sendKeys(driver, pobj.departmentName, departmentName);
				fc.utobj().sendKeys(driver, pobj.departmentAbbr, "TD");
				fc.utobj().selectValFromMultiSelect(driver, pobj.assignUserSelectBtn, userName);

				fc.utobj().selectDropDown(driver, pobj.defaultOwner, userName.trim());

				fc.utobj().sendKeys(driver, pobj.departmentEmail, "test@gmail.com");
				fc.utobj().clickElement(driver, pobj.submit);
				fc.utobj().sleep();
				fc.commonMethods().Click_Close_Input_ByValue(driver);
				fc.utobj().switchFrameToDefault(driver);
				fc.utobj().printTestStep("Department added Successfully");

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to add Departments :: Admin > Support > Manage Departments");

		}
	}

	@Test(groups = { "support"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-22", testCaseDescription = "Verify the Modification Department At Admin > Support > Manage Department", testCaseId = "TC_16_Modify_Department")
	private void modifyDepartment() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("support", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminSupportManageDepartmentPage pobj = new AdminSupportManageDepartmentPage(driver);

			fc.utobj().printTestStep("Navigate to Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest cu = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "supportautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			cu.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			String userName = corpUser.getuserFullName();

			fc.utobj().printTestStep("Navigate to Admin > Support > Manage Department");
			fc.utobj().printTestStep("Add Department");
			String departmentName = fc.utobj().generateTestData(dataSet.get("departmentName"));
			addDepartments(driver, departmentName, userName);
			
			departmentUserView(driver, pobj);
			
			fc.utobj().printTestStep("Modify Department");
			modifyIconMenu(driver, departmentName);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			departmentName = fc.utobj().generateTestData(dataSet.get("departmentName"));
			fc.utobj().sendKeys(driver, pobj.departmentName, departmentName);
			fc.utobj().sendKeys(driver, pobj.departmentAbbr, "TD");

			fc.utobj().sendKeys(driver, pobj.departmentEmail, "test@gmail.com");
			fc.utobj().clickElement(driver, pobj.submit);
			fc.commonMethods().Click_Close_Input_ByValue(driver);

			fc.utobj().showAll(driver);
			fc.utobj().printTestStep("Verify Modify Department");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, departmentName);
			if (isTextPresent == false) {
				fc.utobj().throwsSkipException("was not able to modify Department");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "support", "support145214" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Deletion Department  At Admin > Support > Manage Department", testCaseId = "TC_17_Delete_Department")
	private void deleteDepartment() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("support", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminSupportManageDepartmentPage pobj = new AdminSupportManageDepartmentPage(driver);

			fc.utobj().printTestStep("Navigate to Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest cu = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "supportautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			cu.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			String userName = corpUser.getuserFullName();

			fc.utobj().printTestStep("Navigate to Admin > Support > Manage Department");
			fc.utobj().printTestStep("Add Department");
			String departmentName = fc.utobj().generateTestData(dataSet.get("departmentName"));
			addDepartments(driver, departmentName, userName);
			departmentUserView(driver, pobj);

			fc.utobj().printTestStep("Delete Department");
			fc.utobj().actionImgOption(driver, departmentName, "Delete");
			fc.utobj().acceptAlertBox(driver);
			fc.utobj().showAll(driver);

			fc.utobj().printTestStep("Verify Delete Department");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, departmentName);

			if (isTextPresent == true) {
				fc.utobj().throwsSkipException("was not able to delete department");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "support")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Assign Department  :: Admin > Support > Manage Department", testCaseId = "TC_18_Assign_Department")
	private void assignDepartment() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("support", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminSupportManageDepartmentPage pobj = new AdminSupportManageDepartmentPage(driver);

			fc.utobj().printTestStep("Navigate to Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest addCorporateUserPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "supportautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = addCorporateUserPage.createDefaultUser(driver, corpUser);

			fc.utobj().printTestStep("Navigate to Admin > Support > Manage Department");
			fc.utobj().printTestStep("Add Department");
			String departmentName1 = fc.utobj().generateTestData(dataSet.get("departmentName"));
			addDepartments(driver, departmentName1, corpUser.getuserFullName());

			fc.utobj().printTestStep("Navigate to Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest cu = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			CorporateUser corpUser2 = new CorporateUser();
			corpUser2.setEmail(emailId);
			corpUser2 = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser2);
			cu.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser2);
			String userName = corpUser2.getuserFullName();

			fc.utobj().printTestStep("Navigate to Admin > Support > Manage Department");
			fc.utobj().printTestStep("Add Department");

			String departmentName = fc.utobj().generateTestData(dataSet.get("departmentName"));
			addDepartments(driver, departmentName, userName);
			userDepartmentView(driver, pobj);

			fc.utobj().printTestStep("Assign Department To Corporate User");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[contains(text () , '" + departmentName + "')]/following-sibling::td/a"));
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//td[.='" + departmentName1 + "']/ancestor::tr/td/input"));
			fc.utobj().clickElement(driver, pobj.assignBtn);

			fc.utobj().showAll(driver);

			fc.utobj().printTestStep("Verify Assign Department To Corporate User");
			String text = fc.utobj().getText(driver, fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , '" + userName + "')]/following-sibling::td[1]"));

			if (!text.contains(departmentName1)) {
				fc.utobj().throwsSkipException("was not able to assign Department");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"support" , "support0525"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Add Department at assign Department page At Admin > Support > Manage Department", testCaseId = "TC_19_Add_Department_Assign_Department")
	private void addDepartmentAssignDepartment() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("support", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminSupportManageDepartmentPage pobj = new AdminSupportManageDepartmentPage(driver);

			fc.utobj().printTestStep("Navigate to Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest cu = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "supportautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			cu.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			String userName = corpUser.getuserFullName();

			fc.utobj().printTestStep("Navigate to Admin > Support > Manage Department");
			fc.utobj().printTestStep("Add Department");
			String departmentName = fc.utobj().generateTestData(dataSet.get("departmentName"));
			addDepartments(driver, departmentName, userName);
			userDepartmentView(driver, pobj);

			fc.utobj().printTestStep("Assign Department to Corporate User");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[contains(text () , '" + departmentName + "')]/following-sibling::td/a"));
			fc.utobj().clickElement(driver, pobj.addDepartementLnk);

			fc.commonMethods().switch_cboxIframe_frameId(driver);

			departmentName = fc.utobj().generateTestData(dataSet.get("departmentName"));
			fc.utobj().sendKeys(driver, pobj.departmentName, departmentName);
			fc.utobj().sendKeys(driver, pobj.departmentAbbr, "TD");

			fc.utobj().selectValFromMultiSelect(driver, pobj.assignUserSelectBtn, userName);
			fc.utobj().selectDropDown(driver, pobj.defaultOwner, userName.trim());

			fc.utobj().sendKeys(driver, pobj.departmentEmail, "test@gmail.com");
			fc.utobj().clickElement(driver, pobj.submit);

			fc.commonMethods().Click_Close_Input_ByValue(driver);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().showAll(driver);

			fc.utobj().printTestStep("Verify Assign Department");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, departmentName);

			if (isTextPresent == false) {
				fc.utobj().throwsSkipException("was not able to add Department at Assign Department page");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	void departmentUserView(WebDriver driver, AdminSupportManageDepartmentPage pobj) {
		try {
			// make department userView
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			if (fc.utobj().isElementPresent(driver, pobj.departmentUserViewLink)) {
				fc.utobj().clickElement(driver, pobj.departmentUserViewLink);
			}
			fc.utobj().clickElement(driver, pobj.showAllLnk);
		} catch (Exception e) {
		} finally {
			driver.manage().timeouts().implicitlyWait(45, TimeUnit.SECONDS);
		}
	}

	void userDepartmentView(WebDriver driver, AdminSupportManageDepartmentPage pobj) {
		try {
			// make department userView
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			if (fc.utobj().isElementPresent(driver, pobj.userDepartmentViewLink)) {
				fc.utobj().clickElement(driver, pobj.userDepartmentViewLink);
			}
			fc.utobj().clickElement(driver, pobj.showAllLnk);
		} catch (Exception e) {
		} finally {
			driver.manage().timeouts().implicitlyWait(45, TimeUnit.SECONDS);
		}
	}

	void modifyIconMenu(WebDriver driver, String departmentName) throws Exception {
		String alterText = fc.utobj()
				.getElementByXpath(driver,
						".//td[contains(text () , '" + departmentName + "')]/ancestor::tr/td/div[@id='menuBar']/layer")
				.getAttribute("id").trim();
		alterText = alterText.replace("Actions_dynamicmenu", "");
		alterText = alterText.replace("Bar", "");

		fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
				".//td[contains(text () , '" + departmentName + "')]/ancestor::tr/td/div[@id='menuBar']/layer/a/img"));
		fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
				".//div[@id='Actions_dynamicmenu" + alterText + "Menu']/span[contains(text () , 'Modify')]"));
	}
}
