package com.builds.test.support;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.builds.uimaps.fs.SearchUI;
import com.builds.uimaps.support.AdminSupportManageFAQsPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminSupportManageFAQsPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "support", "supportsmoke123xcv", "support999" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Add New Category At Admin > Support > Manage FAQs", testCaseId = "TC_01_Add_New_Category")
	private void addNewCategory() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("support", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminSupportManageFAQsPage pobj = new AdminSupportManageFAQsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Support > Manage FAQs Page");
			fc.utobj().printTestStep("Add New Category");

			fc.support().support_common().adminSupportManageFAQs(driver);
			fc.utobj().clickElement(driver, pobj.addNewCategoryLink);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String categoryName = fc.utobj().generateTestData(dataSet.get("categoryName"));
			fc.utobj().sendKeys(driver, pobj.categoryName, categoryName);
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().sendKeys(driver, pobj.description, description);

			if (!fc.utobj().isSelected(driver ,pobj.roleBase.get(0))) {
				fc.utobj().clickElement(driver, pobj.roleBase.get(0));
			}

			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.commonMethods().Click_Close_Input_ByValue(driver);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().showAll(driver);

			fc.utobj().printTestStep("Verify Add New Category");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, categoryName);

			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to verify category Name");
			}

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, categoryName));
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			if (!fc.utobj().getElementByXpath(driver, ".//td[.='" + categoryName + "']").isDisplayed()
					&& !fc.utobj().getElementByXpath(driver, ".//td[.='" + description + "']").isDisplayed()
					&& !fc.utobj().getElementByXpath(driver, ".//td[.='All']").isDisplayed()) {

				fc.utobj().throwsException("was not able to verify category Name, description and Accessible To");
			}
			fc.commonMethods().Click_Close_Input_ByValue(driver);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep(
					"Verify After adding new category four number of columns should appear that is Category, Updated by, Number of FAQ's, Action.");

			List<WebElement> listElement = driver
					.findElements(By.xpath(".//tr[@qat_maintable='withheader']//tr[1]/td/a"));

			for (int i = 0; i < listElement.size(); i++) {

				String text = listElement.get(i).getText().trim();

				if (i == 0) {
					if (!text.equalsIgnoreCase(fc.utobj().translateString("Category"))) {
						fc.utobj().throwsException("Category Column is not at first place");
					}
				}

				if (i == 1) {
					if (!text.equalsIgnoreCase(fc.utobj().translateString("Updated By"))) {
						fc.utobj().throwsException("Updated By Column is not at second place");
					}
				}

				if (i == 2) {
					if (!text.equalsIgnoreCase(fc.utobj().translateString("No. Of FAQ's"))) {
						fc.utobj().throwsException("No. Of FAQ's Column is not at third place");
					}
				}
			}

			List<WebElement> listElement1 = driver
					.findElements(By.xpath(".//tr[@qat_maintable='withheader']//tr[1]/td[4]"));

			for (int i = 0; i < listElement1.size(); i++) {

				String text = listElement1.get(i).getText().trim();

				if (i == 0) {
					if (!text.equalsIgnoreCase(fc.utobj().translateString("Action"))) {
						fc.utobj().throwsException("Action Column is not at fourth place");
					}
				}
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void addNewCategory(WebDriver driver, String categoryName, String description) throws Exception {

		String testCaseId = "TC_Add_New_Category_FAQ";

		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminSupportManageFAQsPage pobj = new AdminSupportManageFAQsPage(driver);

				fc.support().support_common().adminSupportManageFAQs(driver);
				fc.utobj().clickElement(driver, pobj.addNewCategoryLink);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().sendKeys(driver, pobj.categoryName, categoryName);
				fc.utobj().sendKeys(driver, pobj.description, description);

				if (!fc.utobj().isSelected(driver,pobj.roleBase.get(0))) {
					fc.utobj().clickElement(driver, pobj.roleBase.get(0));
				}
				fc.utobj().clickElement(driver, pobj.addBtn);
				fc.commonMethods().Click_Close_Input_ByValue(driver);
				fc.utobj().switchFrameToDefault(driver);
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException(
					"was not able to add new Category At FAQ Page : Admin > Support > Manage FAQ Page");

		}

	}

	@Test(groups = { "support", "support0426"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Add New Category  with Corporate Access At Admin > Support > Manage FAQs", testCaseId = "TC_02_Add_New_Category_01")
	private void addNewCategoryWithCoporateAccess() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("support", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminSupportManageFAQsPage pobj = new AdminSupportManageFAQsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Support > Manage FAQs Page");
			fc.utobj().printTestStep("Add New Category With Corporate Role Access");

			fc.support().support_common().adminSupportManageFAQs(driver);
			fc.utobj().clickElement(driver, pobj.addNewCategoryLink);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String categoryName = fc.utobj().generateTestData(dataSet.get("categoryName"));
			fc.utobj().sendKeys(driver, pobj.categoryName, categoryName);
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().sendKeys(driver, pobj.description, description);

			if (!fc.utobj().isSelected(driver,pobj.roleBase.get(1))) {
				fc.utobj().clickElement(driver, pobj.roleBase.get(1));
			}

			fc.utobj().clickElement(driver, pobj.defaultCorporateRole);
			
			fc.utobj().clickElementByJS(driver, pobj.addBtn);
			fc.commonMethods().Click_Close_Input_ByValue(driver);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().showAll(driver);
			
			fc.utobj().printTestStep("Verify Category");
			
			boolean isTextPresent = fc.utobj().assertPageSource(driver, categoryName);

			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to verify category Name");
			}

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, categoryName));
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			if (!fc.utobj().getElementByXpath(driver, ".//td[.='" + categoryName + "']").isDisplayed()
					&& !fc.utobj().getElementByXpath(driver, ".//td[.='" + description + "']").isDisplayed()
					&& !fc.utobj()
							.getElementByXpath(driver, ".//td[.='Default Corporate Role, Corporate Administrator']")
							.isDisplayed()) {

				fc.utobj().throwsException("was not able to verify category Name , description and Accessible To");
			}
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"support" , "support0426"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Add New Category  with Division Access At Admin > Support > Manage FAQs", testCaseId = "TC_03_Add_New_Category_02")
	private void addNewCategoryWithDivisionalAccess() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("support", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminSupportManageFAQsPage pobj = new AdminSupportManageFAQsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Support > Manage FAQs Page");
			fc.utobj().printTestStep("Add New Category");

			fc.support().support_common().adminSupportManageFAQs(driver);
			fc.utobj().clickElement(driver, pobj.addNewCategoryLink);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String categoryName = fc.utobj().generateTestData(dataSet.get("categoryName"));
			System.out.println(categoryName);
			fc.utobj().sendKeys(driver, pobj.categoryName, categoryName);
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().sendKeys(driver, pobj.description, description);

			if (!fc.utobj().isSelected(driver,pobj.roleBase.get(1))) {
				fc.utobj().clickElement(driver, pobj.roleBase.get(1));
			}
			fc.utobj().clickElement(driver, pobj.defaultDivisionalRole);
			
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.commonMethods().Click_Close_Input_ByValue(driver);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().showAll(driver);
			fc.utobj().printTestStep("Verify Add Category");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, categoryName);

			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to verify category Name");
			}

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, categoryName));
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			if (!fc.utobj().getElementByXpath(driver, ".//td[.='" + categoryName + "']").isDisplayed()
					&& !fc.utobj().getElementByXpath(driver, ".//td[.='" + description + "']").isDisplayed()
					&& !fc.utobj().getElementByXpath(driver, ".//td[.='Corporate Administrator']").isDisplayed()) {

				fc.utobj().throwsException("was not able to verify category Name , description and Accessible To");
			}
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"support" , "support0426"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-09-11", testCaseDescription = "Verify the Add New Category with Regional Access At Admin > Support > Manage FAQs", testCaseId = "TC_04_Add_New_Category_04")
	private void addNewCategoryWithRegionalAccess() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("support", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminSupportManageFAQsPage pobj = new AdminSupportManageFAQsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Support > Manage FAQs Page");
			fc.utobj().printTestStep("Add New Category");

			fc.support().support_common().adminSupportManageFAQs(driver);
			fc.utobj().clickElement(driver, pobj.addNewCategoryLink);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String categoryName = fc.utobj().generateTestData(dataSet.get("categoryName"));
			fc.utobj().sendKeys(driver, pobj.categoryName, categoryName);
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().sendKeys(driver, pobj.description, description);

			if (!fc.utobj().isSelected(driver,pobj.roleBase.get(1))) {
				fc.utobj().clickElement(driver, pobj.roleBase.get(1));
			}

			fc.utobj().clickElement(driver, pobj.defaultRegionalRole);
			
			fc.utobj().clickElementByJS(driver, pobj.addBtn);
			fc.commonMethods().Click_Close_Input_ByValue(driver);
			fc.utobj().switchFrameToDefault(driver);

			// verify

			fc.utobj().showAll(driver);

			fc.utobj().printTestStep("Verify Add Category");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, categoryName);

			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to verify category Name");
			}

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, categoryName));
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			if (!fc.utobj().getElementByXpath(driver, ".//td[.='" + categoryName + "']").isDisplayed()
					&& !fc.utobj().getElementByXpath(driver, ".//td[.='" + description + "']").isDisplayed()
					&& !fc.utobj()
							.getElementByXpath(driver, ".//td[.='Corporate Administrator, Default Regional Role']")
							.isDisplayed()) {

				fc.utobj().throwsException("was not able to verify category Name , description and Accessible To");
			}
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"support" , "support0426"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Add New Category  with Franchise Access At Admin > Support > Manage FAQs", testCaseId = "TC_05_Add_New_Category_05")
	private void addNewCategoryWithFranchiseAccess() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("support", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminSupportManageFAQsPage pobj = new AdminSupportManageFAQsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Support > Manage FAQs Page");
			fc.utobj().printTestStep("Add Category");

			fc.support().support_common().adminSupportManageFAQs(driver);
			fc.utobj().clickElement(driver, pobj.addNewCategoryLink);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String categoryName = fc.utobj().generateTestData(dataSet.get("categoryName"));
			fc.utobj().sendKeys(driver, pobj.categoryName, categoryName);
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().sendKeys(driver, pobj.description, description);

			if (!fc.utobj().isSelected(driver,pobj.roleBase.get(1))) {
				fc.utobj().clickElement(driver, pobj.roleBase.get(1));
			}

			fc.utobj().clickElement(driver, pobj.defaultFranchiseRole);
			
			fc.utobj().clickElementByJS(driver, pobj.addBtn);
			fc.commonMethods().Click_Close_Input_ByValue(driver);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().showAll(driver);

			fc.utobj().printTestStep("Verify Added Category");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, categoryName);

			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to verify category Name");
			}

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, categoryName));
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			if (!fc.utobj().getElementByXpath(driver, ".//td[.='" + categoryName + "']").isDisplayed()
					&& !fc.utobj().getElementByXpath(driver, ".//td[.='" + description + "']").isDisplayed()
					&& !fc.utobj()
							.getElementByXpath(driver, ".//td[.='Default Franchise Role, Corporate Administrator']")
							.isDisplayed()) {

				fc.utobj().throwsException("was not able to verify category Name , description and Accessible To");
			}
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "support")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Modifiation of Category  At Admin > Support > Manage FAQs", testCaseId = "TC_06_Modify_Category")
	private void modifyCategory() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("support", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminSupportManageFAQsPage pobj = new AdminSupportManageFAQsPage(driver);


			fc.utobj().printTestStep("Navigate To Admin > Support > Manage FAQs");
			fc.utobj().printTestStep("Add New Category");

			fc.support().support_common().adminSupportManageFAQs(driver);
			String categoryName = fc.utobj().generateTestData(dataSet.get("categoryName"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			addNewCategory(driver, categoryName, description);

			fc.utobj().showAll(driver);

			fc.utobj().printTestStep("Modify Category");

			actionImgOption(driver, categoryName, "Modify");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			categoryName = fc.utobj().generateTestData(dataSet.get("categoryName"));
			fc.utobj().sendKeys(driver, pobj.categoryName, categoryName);
			description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().sendKeys(driver, pobj.description, description);

			if (!fc.utobj().isSelected(driver,pobj.roleBase.get(0))) {
				fc.utobj().clickElement(driver, pobj.roleBase.get(0));
			}

			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.commonMethods().Click_Close_Input_ByValue(driver);
			fc.utobj().switchFrameToDefault(driver);

			try {
				fc.utobj().clickElement(driver, pobj.showAll);
			} catch (Exception e) {

			}

			fc.utobj().printTestStep("Verify Modify Category");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, categoryName);

			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to verify category Name");
			}

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, categoryName));
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			if (!fc.utobj().getElementByXpath(driver, ".//td[.='" + categoryName + "']").isDisplayed()
					&& !fc.utobj().getElementByXpath(driver, ".//td[.='" + description + "']").isDisplayed()
					&& !fc.utobj().getElementByXpath(driver, ".//td[.='All']").isDisplayed()) {

				fc.utobj().throwsException("was not able to verify category Name , description and Accessible To");
			}
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "support")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_07_Delete_Category", testCaseDescription = "Verify the Deletion of Category  At Admin > Support > Manage FAQs ")
	private void deleteCategory() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("support", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > Support > Manage FAQs");
			fc.utobj().printTestStep("Add New Category");

			fc.support().support_common().adminSupportManageFAQs(driver);
			String categoryName = fc.utobj().generateTestData(dataSet.get("categoryName"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			addNewCategory(driver, categoryName, description);

			fc.utobj().showAll(driver);
			fc.utobj().printTestStep("Delete Category");
			actionImgOption(driver, categoryName, "Delete");
			fc.utobj().acceptAlertBox(driver);
			fc.utobj().showAll(driver);
			fc.utobj().printTestStep("Verify Delete Category");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, categoryName);
			if (isTextPresent == true) {
				fc.utobj().throwsException("was not able to delete Category Name");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "support"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Add FAQ At Admin > Support > Manage FAQs", testCaseId = "TC_08_Add_FAQ")
	private void addFAQ() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("support", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminSupportManageFAQsPage pobj = new AdminSupportManageFAQsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Support > Manage FAQs");
			fc.utobj().printTestStep("Add New Category");

			fc.support().support_common().adminSupportManageFAQs(driver);
			String categoryName = fc.utobj().generateTestData(dataSet.get("categoryName"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			addNewCategory(driver, categoryName, description);
			fc.utobj().showAll(driver);

			fc.utobj().printTestStep("Add FAQ");
			actionImgOption(driver, categoryName, "Add FAQ");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String faqQuestion = fc.utobj().generateTestData(dataSet.get("faqQuestion"));
			fc.utobj().sendKeys(driver, pobj.faqQuestion, faqQuestion);
			String faqAnswer = fc.utobj().generateTestData(dataSet.get("faqAnswer"));
			fc.utobj().sendKeys(driver, pobj.faqAnswer, faqAnswer);
			fc.utobj().clickElement(driver, pobj.addUpdateBtn);
			
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String file = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			fc.utobj().sendKeys(driver, pobj.attachmentName, file);
			fc.utobj().clickElement(driver, pobj.attachBtn);
			
			pobj = new AdminSupportManageFAQsPage(driver);
			fc.utobj().clickElement(driver, pobj.doneBtn);
			fc.utobj().switchFrameToDefault(driver);
			
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.commonMethods().Click_Close_Input_ByValue(driver);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().showAll(driver);
			fc.utobj().printTestStep("Verify Add FAQ");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[.='" + categoryName + "']/ancestor::tr/td/a[.='1']"));

			if (!fc.utobj().getElementByXpath(driver, ".//*[contains(text () , '" + faqQuestion + "')]").isDisplayed()
					&& !fc.utobj().getElementByXpath(driver, ".//*[contains(text () , '" + faqAnswer + "')]")
							.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify faqQuestion and answer");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void addFAQ(WebDriver driver, String categoryName, String faqQuestion, String faqAnswer,
			/* File file , */String parentWindow) throws Exception {

		String testCaseId = "TC_Add_FAQ_At_FAQ_Page";

		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminSupportManageFAQsPage pobj = new AdminSupportManageFAQsPage(driver);
				try {
					fc.utobj().clickElement(driver, pobj.showAll);
				} catch (Exception e) {

				}
				actionImgOption(driver, categoryName, "Add FAQ");
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().sendKeys(driver, pobj.faqQuestion, faqQuestion);
				fc.utobj().sendKeys(driver, pobj.faqAnswer, faqAnswer);

				/*
				 * fc.utobj().clickElement(driver, pobj.addUpdateBtn);
				 * 
				 * fc.commonMethods().switch_cboxIframe_frameId(driver);
				 * fc.utobj().sendKeys(driver, pobj.attachmentName,
				 * file.getAbsolutePath()); fc.utobj().clickElement(driver,
				 * pobj.attachBtn); fc.utobj().clickElement(driver,
				 * pobj.doneBtn); fc.utobj().switchFrameToDefault(driver);
				 * 
				 * fc.commonMethods().switch_cboxIframe_frameId(driver);
				 */
				fc.utobj().clickElement(driver, pobj.addBtn);
				fc.commonMethods().Click_Close_Input_ByValue(driver);
				fc.utobj().switchFrameToDefault(driver);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Was not able to add FAQ :: Admin Support > Manage FAQs");

		}
	}

	@Test(groups = "support")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Modification FAQ At Admin > Support > Manage FAQs", testCaseId = "TC_09_Modify_FAQ")
	private void modifyFAQ() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("support", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminSupportManageFAQsPage pobj = new AdminSupportManageFAQsPage(driver);
			String parentWindow = driver.getWindowHandle();

			fc.utobj().printTestStep("Navigate To Admin > Support > Manage FAQs");
			fc.utobj().printTestStep("Add New Category");

			fc.support().support_common().adminSupportManageFAQs(driver);
			String categoryName = fc.utobj().generateTestData(dataSet.get("categoryName"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			addNewCategory(driver, categoryName, description);

			fc.utobj().printTestStep("Add FAQ");
			String faqQuestion = fc.utobj().generateTestData(dataSet.get("faqQuestion"));
			String faqAnswer = fc.utobj().generateTestData(dataSet.get("faqAnswer"));

			addFAQ(driver, categoryName, faqQuestion, faqAnswer, /* file, */parentWindow);
			fc.utobj().showAll(driver);
			fc.utobj().actionImgOption(driver, categoryName, "Manage FAQ");

			fc.utobj().printTestStep("Modify FAQ");
			actionImgOption(driver, faqQuestion, "Modify");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			faqQuestion = fc.utobj().generateTestData(dataSet.get("faqQuestion"));
			fc.utobj().sendKeys(driver, pobj.faqQuestion, faqQuestion);
			faqAnswer = fc.utobj().generateTestData(dataSet.get("faqAnswer"));
			fc.utobj().sendKeys(driver, pobj.faqAnswer, faqAnswer);
			fc.utobj().clickElement(driver, pobj.addUpdateBtn);
			
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String file = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			fc.utobj().sendKeys(driver, pobj.attachmentName, file);
			fc.utobj().clickElement(driver, pobj.attachBtn);
			fc.utobj().clickElement(driver, pobj.doneBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.commonMethods().Click_Close_Input_ByValue(driver);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify Modify FAQ");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, faqQuestion);
			if (isTextPresent == false) {
				fc.utobj().throwsSkipException("was not able to modify FAQ at FAQ list page");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "support")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Deletion FAQ  At Admin > Support > Manage FAQs", testCaseId = "TC_10_Delete_FAQ")
	private void deleteFAQ() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("support", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			String parentWindow = driver.getWindowHandle();

			fc.utobj().printTestStep("Navigate To Admin > Support > Manage FAQs");
			fc.utobj().printTestStep("Add New Category");

			fc.support().support_common().adminSupportManageFAQs(driver);
			String categoryName = fc.utobj().generateTestData(dataSet.get("categoryName"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			addNewCategory(driver, categoryName, description);

			fc.utobj().printTestStep("Add FAQ");
			String faqQuestion = fc.utobj().generateTestData(dataSet.get("faqQuestion"));
			String faqAnswer = fc.utobj().generateTestData(dataSet.get("faqAnswer"));

			addFAQ(driver, categoryName, faqQuestion, faqAnswer, parentWindow);

			// got to FAQ List Page

			fc.utobj().showAll(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[.='" + categoryName + "']/ancestor::tr/td/a[.='1']"));

			fc.utobj().printTestStep("Delete FAQ");
			actionImgOption(driver, faqQuestion, "Delete");

			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Delete FAQ");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, faqQuestion);

			if (isTextPresent == true) {
				fc.utobj().throwsException("was not able to delete FAQ");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "support" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Search FAQ At Admin > Support > Manage FAQs", testCaseId = "TC_11_Verify_Search_Manage_FAQ")
	private void verifySearchAtManageFAQ() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("support", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminSupportManageFAQsPage pobj = new AdminSupportManageFAQsPage(driver);
			String parentWindow = driver.getWindowHandle();
			SearchUI search_page = new SearchUI(driver);

			fc.utobj().printTestStep("Navigate To Admin > Support > Manage FAQs");
			fc.utobj().printTestStep("Add New Category");

			fc.support().support_common().adminSupportManageFAQs(driver);
			String categoryName = fc.utobj().generateTestData(dataSet.get("categoryName"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			addNewCategory(driver, categoryName, description);

			fc.utobj().printTestStep("Add FAQ");
			String faqQuestion = fc.utobj().generateTestData(dataSet.get("faqQuestion"));
			String faqAnswer = fc.utobj().generateTestData(dataSet.get("faqAnswer"));
			addFAQ(driver, categoryName, faqQuestion, faqAnswer, parentWindow);

			fc.utobj().printTestStep("Search FAQ");
			fc.utobj().sendKeys(driver, pobj.searchTxBx, faqQuestion);
			fc.utobj().clickElement(driver, pobj.searchImgBtn);

			fc.utobj().printTestStep("Verify Search FAQ");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, faqQuestion);

			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to search FAQ Question");
			}

			fc.utobj().printTestStep("Search FAQ's By SOLR Search");

			boolean isSearchTrue = false;
			for (int i = 0; i < 3; i++) {
				if (isSearchTrue == false) {
					fc.utobj().sendKeys(driver, search_page.topSearchField, faqQuestion);
					Thread.sleep(5000);
					if (i == 2) {
						Thread.sleep(5000);
					}
					fc.utobj().clickEnterOnElement(driver, search_page.topSearchField);

					try {
						isSearchTrue = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//custom[contains(text () , '" + faqQuestion + "')]");
					} catch (Exception e) {
						Reporter.log(e.getMessage());
					}
				}
			}
			if (isSearchTrue == false) {
				fc.utobj().throwsException("Not able to search FAQ's By Top Search");
			}
			fc.utobj().refresh(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "support")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Add FAQ At FAQ List At Admin > Support > Manage FAQs ", testCaseId = "TC_12_Add_FAQ_FAQ_List")
	private void addFAQAtFAQList() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("support", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminSupportManageFAQsPage pobj = new AdminSupportManageFAQsPage(driver);
			String parentWindow = driver.getWindowHandle();

			fc.utobj().printTestStep("Navigate To Admin > Support > Manage FAQs");
			fc.utobj().printTestStep("Add New Category");

			fc.support().support_common().adminSupportManageFAQs(driver);
			String categoryName = fc.utobj().generateTestData(dataSet.get("categoryName"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			addNewCategory(driver, categoryName, description);

			fc.utobj().printTestStep("Add FAQ");
			String faqQuestion = fc.utobj().generateTestData(dataSet.get("faqQuestion"));
			String faqAnswer = fc.utobj().generateTestData(dataSet.get("faqAnswer"));
			addFAQ(driver, categoryName, faqQuestion, faqAnswer, parentWindow);

			fc.utobj().showAll(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[.='" + categoryName + "']/ancestor::tr/td/a[.='1']"));

			fc.utobj().printTestStep("Add FAQ At FAQ List");
			fc.utobj().clickElement(driver, pobj.addFAQLink);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			faqQuestion = fc.utobj().generateTestData(dataSet.get("faqQuestion"));
			fc.utobj().sendKeys(driver, pobj.faqQuestion, faqQuestion);
			faqAnswer = fc.utobj().generateTestData(dataSet.get("faqAnswer"));
			fc.utobj().sendKeys(driver, pobj.faqAnswer, faqAnswer);
			fc.utobj().clickElement(driver, pobj.addUpdateBtn);

			fc.commonMethods().switch_cboxIframe_frameId(driver);

			String file = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			fc.utobj().sendKeys(driver, pobj.attachmentName, file);
			fc.utobj().clickElement(driver, pobj.attachBtn);
			fc.utobj().clickElement(driver, pobj.doneBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.commonMethods().Click_Close_Input_ByValue(driver);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify FAQ At FAQ List Page");
			if (!fc.utobj().getElementByXpath(driver, ".//*[contains(text () , '" + faqQuestion + "')]").isDisplayed()
					&& !fc.utobj().getElementByXpath(driver, ".//*[contains(text () , '" + faqAnswer + "')]")
							.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify faqQuestion and answer");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	private void actionImgOption(WebDriver driver, String text, String option) throws Exception {

		String alterText = fc.utobj()
				.getElementByXpath(driver, ".//*[.='" + text + "']/ancestor::tr/td/div[@id='menuBar']/layer")
				.getAttribute("id").trim();
		alterText = alterText.replace("Actions_dynamicmenu", "");
		alterText = alterText.replace("Bar", "");

		fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
				".//*[.='" + text + "']/ancestor::tr/td/div[@id='menuBar']/layer/a/img"));

		List<String> linkArray = fc.utobj().translate(option);
		if (!(linkArray.size() < 1)) {
			for (int i = 0; i < linkArray.size(); i++) {
				try {
					option = linkArray.get(i);
					fc.utobj().clickElement(driver,
							fc.utobj().getElementByXpath(driver, ".//div[@id='Actions_dynamicmenu" + alterText
									+ "Menu']/span[contains(text () , '" + option + "')]"));
					break;
				} catch (Exception e) {
				}
			}
		} else {
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//div[@id='Actions_dynamicmenu"
					+ alterText + "Menu']/span[contains(text () , '" + option + "')]"));
		}

	}
}
