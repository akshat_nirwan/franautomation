package com.builds.test.support;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.uimaps.support.AdminSupportManageMessageTemplatesPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminSupportManageMessageTemplatesPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = "support")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Add Message Template At Admin > Support > Message Template Page", testCaseId = "TC_23_Add_Message_Template")
	private void addMessageTemplates() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("support", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > Support > Manage Message Templates");
			fc.support().support_common().adminSupportManageMessageTemplates(driver);

			fc.utobj().printTestStep("Add Messages Template");
			AdminSupportManageMessageTemplatesPage pobj = new AdminSupportManageMessageTemplatesPage(driver);

			fc.utobj().clickElement(driver, pobj.addMessageTemplate);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String testMessage = fc.utobj().generateTestData(dataSet.get("testMessage"));
			fc.utobj().sendKeys(driver, pobj.title, testMessage);
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().sendKeys(driver, pobj.description, description);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.commonMethods().Click_Close_Input_ByValue(driver);

			fc.utobj().printTestStep("Verify Message Template");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, testMessage);

			if (isTextPresent == false) {
				fc.utobj().throwsSkipException("was not able to add Message Templates");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void addMessageTemplates(WebDriver driver, String testMessage, String description) throws Exception {

		String testCaseId = "TC_Add_Message_Templates";

		if (fc.utobj().validate(testCaseId)) {
			try {
				String parentWindow = driver.getWindowHandle();
				fc.support().support_common().adminSupportManageMessageTemplates(driver);
				AdminSupportManageMessageTemplatesPage pobj = new AdminSupportManageMessageTemplatesPage(driver);

				fc.utobj().clickElement(driver, pobj.addMessageTemplate);

				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().sendKeys(driver, pobj.title, testMessage);
				fc.utobj().sendKeys(driver, pobj.description, description);

				fc.utobj().clickElement(driver, pobj.saveBtn);
				fc.commonMethods().Click_Close_Input_ByValue(driver);
				driver.switchTo().window(parentWindow);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException(
					"was not able to add Message Templates:: Admin > Support > Manage Message Templates");

		}
	}

	@Test(groups = "support")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the modification of Messages Template At Admin > Support > Message Template", testCaseId = "TC_24_Modify_Message_Template")
	private void modifyMessageTemplate() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("support", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > Support > Manage Message Templates");
			fc.utobj().printTestStep("Add Message Template");
			AdminSupportManageMessageTemplatesPage pobj = new AdminSupportManageMessageTemplatesPage(driver);
			String testMessage = fc.utobj().generateTestData(dataSet.get("testMessage"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			addMessageTemplates(driver, testMessage, description);

			fc.utobj().printTestStep("Modify Message Template");
			actionImgOption(driver, testMessage, "Modify");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.title, testMessage);
			fc.utobj().sendKeys(driver, pobj.description, description);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.commonMethods().Click_Close_Input_ByValue(driver);

			fc.utobj().printTestStep("Verify Modify Messages Template");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, testMessage);

			if (isTextPresent == false) {
				fc.utobj().throwsSkipException("was not able to add Message Templates");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "support" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Deletion of Message Template At Admin > Support > Message Template", testCaseId = "TC_25_Delete_Message_Template")
	private void deleteMessageTemplate() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("support", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > Support > Manage Message Templates");
			fc.utobj().printTestStep("Add Messages Template");
			String testMessage = fc.utobj().generateTestData(dataSet.get("testMessage"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			addMessageTemplates(driver, testMessage, description);

			fc.utobj().printTestStep("Delete Message Template");
			actionImgOption(driver, testMessage, "Delete");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify Delete Message Template");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, testMessage);

			if (isTextPresent == true) {
				fc.utobj().throwsSkipException("was not able to add Message Templates");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void actionImgOption(WebDriver driver, String task, String option) throws Exception {

		String alterText = fc.utobj()
				.getElementByXpath(driver,
						".//*[contains(text () , '" + task + "')]/ancestor::tr/td/div[@id='menuBar']/layer")
				.getAttribute("id").trim();
		alterText = alterText.replace("Actions_dynamicmenu", "");
		alterText = alterText.replace("Bar", "");

		fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
				".//*[contains(text () , '" + task + "')]/ancestor::tr/td/div[@id='menuBar']/layer/a/img"));
		// fc.utobj().clickElement(driver,
		// fc.utobj().getElementByXpath(driver,".//div[@id='Actions_dynamicmenu"+alterText+"Menu']/span[contains(text
		// () , '"+option+"')]")));

		List<String> linkArray = fc.utobj().translate(option);
		if (!(linkArray.size() < 1)) {
			for (int i = 0; i < linkArray.size(); i++) {
				try {
					option = linkArray.get(i);
					fc.utobj().clickElement(driver,
							fc.utobj().getElementByXpath(driver, ".//div[@id='Actions_dynamicmenu" + alterText
									+ "Menu']/span[contains(text () , '" + option + "')]"));
					break;
				} catch (Exception e) {
				}
			}
		} else {
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//div[@id='Actions_dynamicmenu"
					+ alterText + "Menu']/span[contains(text () , '" + option + "')]"));
		}

	}

}