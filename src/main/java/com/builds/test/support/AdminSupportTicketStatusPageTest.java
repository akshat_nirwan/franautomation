package com.builds.test.support;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.uimaps.support.AdminSupportTicketStatusPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminSupportTicketStatusPageTest {
	FranconnectUtil fc = new FranconnectUtil();
	
	@Test(groups = {"support"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add Contact Address At Admin > Support > Ticket Status", testCaseId = "TC_26_Add_Ticket_Status")
	private void addTicketStatus() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("support", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		try {
			driver = fc.loginpage().login(driver);
			AdminSupportTicketStatusPage pobj = new AdminSupportTicketStatusPage(driver);
			String parentWindow = driver.getWindowHandle();

			fc.utobj().printTestStep("Navigate To Admin > Support > Ticket Status Page");
			fc.support().support_common().adminSupportTicketStatus(driver);

			fc.utobj().printTestStep("Add Ticket Status");
			fc.utobj().clickElement(driver, pobj.addTicketStatus);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String statusName = fc.utobj().generateTestData(dataSet.get("statusName"));
			fc.utobj().sendKeys(driver, pobj.statusName, statusName);
			fc.utobj().clickElement(driver, pobj.add);
			fc.commonMethods().Click_Close_Input_ByValue(driver);
			driver.switchTo().window(parentWindow);

			fc.utobj().printTestStep("Verify Add Ticket Status");
			boolean isValPresent = fc.utobj().selectDropDownByVisibleTextTrimed(driver, pobj.listing, statusName);

			if (isValPresent == false) {
				fc.utobj().throwsException("was not able to add status");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void addTicketStatus(WebDriver driver, String statusName) throws Exception {

		String testCaseId = "TC_Add_Ticket_Status";

		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminSupportTicketStatusPage pobj = new AdminSupportTicketStatusPage(driver);
				fc.support().support_common().adminSupportTicketStatus(driver);
				String parentWindow = driver.getWindowHandle();

				fc.utobj().clickElement(driver, pobj.addTicketStatus);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().sendKeys(driver, pobj.statusName, statusName);
				fc.utobj().clickElement(driver, pobj.add);
				fc.commonMethods().Click_Close_Input_ByValue(driver);
				driver.switchTo().window(parentWindow);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to add Ticket Status :: Admin > Support > Ticket Status Page");

		}
	}

	@Test(groups = { "support", "supportTest22" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-29", testCaseDescription = "Verify Admin > Support > Ticket Status", testCaseId = "TC_27_Modify_Ticket_Status")
	private void modifyTicketStatus() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("support", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminSupportTicketStatusPage pobj = new AdminSupportTicketStatusPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Support > Ticket Status Page");
			fc.utobj().printTestStep("Add Ticket Status");
			String statusName = fc.utobj().generateTestData(dataSet.get("statusName"));
			addTicketStatus(driver, statusName);

			fc.utobj().printTestStep("Modify Ticket Status");
			fc.utobj().selectDropDownByPartialText(driver, pobj.listing, statusName);
			fc.utobj().clickElement(driver, pobj.modifyBtn);

			String parentWindow = driver.getWindowHandle();
			statusName = fc.utobj().generateTestData(dataSet.get("statusName"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.statusName, statusName);
			fc.utobj().clickElement(driver, pobj.modifyBtn);
			fc.commonMethods().Click_Close_Input_ByValue(driver);
			driver.switchTo().window(parentWindow);

			fc.utobj().printTestStep("Verify Modify Ticket Status");
			boolean isValPresent = fc.utobj().selectDropDownByVisibleTextTrimed(driver, pobj.listing, statusName);

			if (isValPresent == false) {
				fc.utobj().throwsException("was not able to modify status");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "support")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Deletion of Ticket at Admin > Support > Ticket Status", testCaseId = "TC_28_Delete_Ticket_Status")
	private void deleteTicketStatus() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("support", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminSupportTicketStatusPage pobj = new AdminSupportTicketStatusPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Support > Ticket Status Page");
			fc.utobj().printTestStep("Add Ticket Status");
			String statusName = fc.utobj().generateTestData(dataSet.get("statusName"));
			addTicketStatus(driver, statusName);

			fc.utobj().printTestStep("Delete Ticket Status");
			fc.utobj().selectDropDownByPartialText(driver, pobj.listing, statusName);
			fc.utobj().clickElement(driver, pobj.deleteBtn);
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify Delete Ticket Status");
			boolean isValPresent = fc.utobj().selectDropDownByVisibleTextTrimed(driver, pobj.listing, statusName);

			if (isValPresent == true) {
				fc.utobj().throwsException("Was not able to delete status");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "support")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Cancel Button at Admin > Support > Ticket Status", testCaseId = "TC_28_Cancel_Ticket_Status")
	private void cancelTicketStatus() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("support", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminSupportTicketStatusPage pobj = new AdminSupportTicketStatusPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Support > Ticket Status Page");

			fc.support().support_common().adminSupportTicketStatus(driver);

			fc.utobj().printTestStep("Click on Cancel Button.");
			fc.utobj().clickElement(driver, pobj.cancelBtn);

			boolean isValPresent = fc.utobj().assertPageSource(driver, "Configure the statuses of tickets.");

			if (isValPresent == false) {
				fc.utobj().throwsException("Cancel Button not working at Admin > Support > Configure Ticket Status");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

}
