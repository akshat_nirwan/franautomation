package com.builds.test.support;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.common.CorporateUser;
import com.builds.uimaps.support.SupportArchivedTicketsPage;
import com.builds.uimaps.support.SupportTicketsPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class SupportArchivedTicketsPageTest {
	FranconnectUtil fc = new FranconnectUtil();
	SupportTicketsPageTest ticket_PageTest = new SupportTicketsPageTest();

	@Test(groups = { "support"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Restore Ticket At Support > Archived Tickets", testCaseId = "TC_49_Restore_Tickets")
	private void restoreTicket() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("support", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			SupportArchivedTicketsPage pobj = new SupportArchivedTicketsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest cu = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "supportautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			cu.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			String userName = corpUser.getuserFullName();

			fc.utobj().printTestStep("Navigate to Admin > Support > Manage Department");
			fc.utobj().printTestStep("Add Department");
			String departmentName = fc.utobj().generateTestData(dataSet.get("departmentName"));
			AdminSupportManageDepartmentPageTest departmentPage = new AdminSupportManageDepartmentPageTest();
			departmentPage.addDepartments(driver, departmentName, userName);

			fc.utobj().printTestStep("Navigate To Support > Ticket");
			fc.utobj().printTestStep("Create Ticket");
			SupportTicketsPageTest ticketPage = new SupportTicketsPageTest();
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			ticketPage.createTicket(driver, departmentName, description, subject);

			ticket_PageTest.searchTicketBySubject(driver, subject);

			fc.utobj().printTestStep("Archive Ticket");
			ticketPage.actionImgOption(driver, subject, "Archive");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.okBtn);
			fc.commonMethods().Click_Close_Input_ByValue(driver);
			fc.utobj().switchFrameToDefault(driver);

			String url = config.get("buildUrl");
			url = url.concat("control/troubleTicketsReportedArchived?isArchivedTickets=Y&fromWhere=fromSearch");

			try {

				fc.utobj().moveToElementThroughAction(driver, pobj.moreActionBtn);
				fc.utobj().clickElement(driver, pobj.archiveTicketsLink);
			} catch (Exception e) {
				driver.navigate().to(url);

			}

			fc.utobj().printTestStep("Navigate To Archive Ticket Page");

			/*
			 * fc.utobj().sendKeys(driver, pobj.supportTopSearch, subject);
			 * fc.utobj().clickElement(driver, pobj.searchImgBtn);
			 */

			ticket_PageTest.filterTicketByDepartment(driver, new SupportTicketsPage(driver), departmentName);

			fc.utobj().printTestStep("Restore Ticket");
			ticketPage.actionImgOption(driver, subject, "Restore");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.okBtn);
			fc.commonMethods().Click_Close_Input_ByValue(driver);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Navigate To Support > Ticket");

			fc.utobj().moveToElementThroughAction(driver, pobj.moreActionBtn);
			fc.utobj().clickElement(driver, pobj.ticketsLink);

			/*
			 * fc.utobj().sendKeys(driver, pobj.supportTopSearch, subject);
			 * fc.utobj().clickElement(driver, pobj.searchImgBtn);
			 */

			ticket_PageTest.filterTicketByDepartment(driver, new SupportTicketsPage(driver), departmentName);

			fc.utobj().printTestStep("Verify Restore Ticket");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, subject);
			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to restore a tickets");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
}
