package com.builds.test.support;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.common.CorporateUser;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class SupportContactInfoPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = "support")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Contact Info At Support > Contact Info", testCaseId = "TC_52_Verify_Contact_Info")
	private void verifyContactInfo() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("support", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest cu = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "supportautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			cu.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			String userName = corpUser.getuserFullName();

			fc.utobj().printTestStep("Navigate to Admin > Support > Manage Department");
			fc.utobj().printTestStep("Add Department");

			String departmentName = fc.utobj().generateTestData(dataSet.get("departmentName"));
			AdminSupportManageDepartmentPageTest addDepartmentPage = new AdminSupportManageDepartmentPageTest();
			addDepartmentPage.addDepartments(driver, departmentName, userName);

			fc.utobj().printTestStep("Navigate To Admin > Support > Manage Contact Information Page");
			fc.utobj().printTestStep("Add Contact Address");
			AdminSupportManageContactInformationPageTest contactInfoPage = new AdminSupportManageContactInformationPageTest();
			String contactName = fc.utobj().generateTestData(dataSet.get("contactName"));
			contactInfoPage.addContactAddress(driver, contactName, departmentName, config);

			fc.utobj().printTestStep("Navigate To Support > Contact Info Page");
			fc.support().support_common().supportContactInfo(driver);

			fc.utobj().printTestStep("Verify Contact Address Info");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, contactName);
			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to verify contact Info");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
}
