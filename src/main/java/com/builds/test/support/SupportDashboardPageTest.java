package com.builds.test.support;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.common.CorporateUser;
import com.builds.uimaps.support.SupportDashboardPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class SupportDashboardPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = "support")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Ticket Summary At Support > DashBoard", testCaseId = "TC_29_Verify_Ticket_Summary")
	private void verifyTicketSummary() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("support", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminSupportManageDepartmentPageTest manageDepartmentPage = new AdminSupportManageDepartmentPageTest();

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest cu = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "supportautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			cu.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			String userName = corpUser.getuserFullName();

			fc.utobj().printTestStep("Navigate to Admin > Support > Manage Department");
			fc.utobj().printTestStep("Add Department");

			String departmentName = fc.utobj().generateTestData(dataSet.get("departmentName"));
			manageDepartmentPage.addDepartments(driver, departmentName, userName);

			fc.utobj().printTestStep("Navigate To Admin > Support > Ticket Status Page");
			fc.utobj().printTestStep("Add Ticket Status");
			AdminSupportTicketStatusPageTest ticketstatusPage = new AdminSupportTicketStatusPageTest();
			String statusName = fc.utobj().generateTestData(dataSet.get("statusName"));
			ticketstatusPage.addTicketStatus(driver, statusName);

			fc.utobj().printTestStep("Navigate To Support > DashBoard Page");
			SupportDashboardPage pobj = new SupportDashboardPage(driver);
			fc.support().support_common().supportDashBoard(driver);

			fc.utobj().printTestStep("View Report");
			fc.utobj().selectValFromMultiSelect(driver, pobj.ownersSelectBtn, userName);

			fc.utobj().selectDropDown(driver, pobj.reportingDateDropDown, "All");
			fc.utobj().clickElement(driver, pobj.viewReport);

			fc.utobj().printTestStep("Verify Ticket Summary");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, departmentName);

			if (isTextPresent == false) {
				fc.utobj().throwsSkipException("was not able to verify department Name at DashBoard");
			}

			boolean isTextPresent1 = fc.utobj().assertPageSource(driver, statusName);

			if (isTextPresent1 == false) {
				fc.utobj().throwsSkipException("was not able to verify department Name at DashBoard");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
}
