package com.builds.test.support;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.uimaps.support.TheHubPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class SupportFAQsPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "support"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify FAQs At Support > FAQs", testCaseId = "TC_51_Verify_FAQ")
	private void verifyFAQ() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("support", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			String parentWindow = driver.getWindowHandle();

			fc.utobj().printTestStep("Navigate To Admin > Support > Manage FAQs");
			fc.utobj().printTestStep("Add New Category");

			AdminSupportManageFAQsPageTest addFAQPage = new AdminSupportManageFAQsPageTest();
			String categoryName = fc.utobj().generateTestData(dataSet.get("categoryName"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			addFAQPage.addNewCategory(driver, categoryName, description);

			fc.utobj().printTestStep("Add FAQ");
			String faqQuestion = fc.utobj().generateTestData(dataSet.get("faqQuestion"));
			String faqAnswer = fc.utobj().generateTestData(dataSet.get("faqAnswer"));
			addFAQPage.addFAQ(driver, categoryName, faqQuestion, faqAnswer, parentWindow);

			fc.utobj().printTestStep("Navigate To Support > FAQs Page");
			fc.support().support_common().supportFAQs(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text() , '" + categoryName + "')]"));

			fc.utobj().printTestStep("Verify FAQ");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, faqQuestion);
			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to verify FAQs");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"support"})

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify FAQ At The Hub What's New", testCaseId = "TC_53_Verify_FAQ")
	private void verifyFAQATTheHubHomePage() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("support", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			TheHubPage pobj = new TheHubPage(driver);
			String parentWindow = driver.getWindowHandle();

			fc.utobj().printTestStep("Navigate To Admin > Support > Manage FAQs");
			fc.utobj().printTestStep("Add New Category");

			AdminSupportManageFAQsPageTest addFAQPage = new AdminSupportManageFAQsPageTest();
			String categoryName = fc.utobj().generateTestData(dataSet.get("categoryName"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			addFAQPage.addNewCategory(driver, categoryName, description);

			String faqQuestion = fc.utobj().generateTestData(dataSet.get("faqQuestion"));
			String faqAnswer = fc.utobj().generateTestData(dataSet.get("faqAnswer"));

			fc.utobj().printTestStep("Add FAQ");
			addFAQPage.addFAQ(driver, categoryName, faqQuestion, faqAnswer, parentWindow);

			fc.utobj().printTestStep("Navigate To The Hub > What's New");
			fc.hub().hub_common().theHubWhatsNew(driver);

			fc.utobj().printTestStep("Verify FAQ");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, categoryName);
			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to verify FAQs at The Hub What's New Page");
			}

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, faqQuestion));
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			if (!fc.utobj().getElementByXpath(driver, ".//td[.='" + categoryName + "']").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify category Name");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//td[.='" + faqQuestion + "']").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify FAQs Question");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//td[.='" + faqAnswer + "']").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify FAQs Answer");
			}

			fc.commonMethods().Click_Close_Input_ByValue(driver);
			driver.switchTo().window(parentWindow);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/*
	 * @Test(groups="support")
	 * 
	 * @TestCase(createdOn="2017-05-01",updatedOn="2018-06-14",
	 * testCaseDescription = "Verify Support Ticket At The Hub Home Page",
	 * testCaseId = "TC_54_Verify_Support_Ticket") private void
	 * verifySupportTicket() throws Exception {
	 * 
	 * String testCaseId =
	 * fc.utobj().readTestCaseInfo(this.getClass().getName()+"."+new
	 * Object(){}.getClass().getEnclosingMethod().getName());
	 * 
	 * Map<String,String> dataSet = fc.utobj().readTestData(config, testCaseId);
	 * WebDriver driver = fc.utobj().openDriver(config);
	 * 
	 * try{ driver=fc.loginpage().login(driver, config);
	 * 
	 * TheHubPage pobj=new TheHubPage(driver); String
	 * parentWindow=driver.getWindowHandle();
	 * 
	 * SupportTicketsPageTest createTicketPage=new SupportTicketsPageTest();
	 * 
	 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
	 * addCorporateUserPage=new
	 * AdminUsersManageCorporateUsersAddCorporateUserPageTest(); String
	 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
	 * userName=addCorporateUserPage.addCorporateUser(driver, userName, config);
	 * String
	 * departmentName=fc.utobj().generateTestData(dataSet.get("departmentName"))
	 * ; AdminSupportManageDepartmentPageTest departmentPage=new
	 * AdminSupportManageDepartmentPageTest();
	 * departmentPage.addDepartments(driver, departmentName, userName,
	 * parentWindow, config);
	 * 
	 * AdminFranchiseLocationAddFranchiseLocationPageTest addFranPage=new
	 * AdminFranchiseLocationAddFranchiseLocationPageTest(); String
	 * franchiseId=fc.utobj().generateTestData(dataSet.get("franchiseId"));
	 * String regionName=fc.utobj().generateTestData(dataSet.get("regionName"));
	 * String storeType=fc.utobj().generateTestData(dataSet.get("storeType"));
	 * franchiseId=addFranPage.addFranchiseLocation_All(driver, franchiseId,
	 * regionName, storeType, config);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Create Ticket"); File file = new
	 * File(config.get("testDataPath").concat("/").concat("document").concat("/"
	 * )+dataSet.get("uploadFile")); String
	 * subject=fc.utobj().generateTestData(dataSet.get("subject")); String
	 * description=fc.utobj().generateTestData(dataSet.get("description"));
	 * createTicketPage.createTicket(driver, dataSet, departmentName,
	 * franchiseId, file, description, subject, config);
	 * 
	 * fc.utobj().sendKeys(driver, pobj.supportTopSearch, subject);
	 * fc.utobj().clickElement(driver, pobj.searchImgBtn);
	 * 
	 * String text=fc.utobj().getText(driver,
	 * fc.utobj().getElementByXpath(driver, ".//*[contains(text () , '"
	 * +subject+"')]/ancestor::tr/td/a"))); SupportTicketsPageTest
	 * ticketPage=new SupportTicketsPageTest();
	 * ticketPage.actionImgOption(driver, subject, "Assign Owner");
	 * fc.commonMethods().switch_cboxIframe_frameId(driver);
	 * fc.utobj().clickElement(driver, pobj.assignToMeChckBx);
	 * fc.utobj().clickElement(driver, pobj.saveBtn);
	 * fc.commonMethods().Click_Close_Input_ByValue(driver);
	 * driver.switchTo().window(parentWindow);
	 * 
	 * fc.hub().hub_common().theHubHome( driver);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Verify Support Ticket"); boolean
	 * isTextPresent=fc.utobj().assertPageSource(driver, text); if
	 * (isTextPresent==false) { fc.utobj().throwsException(
	 * "was not able to verify Ticket Number at The Hub Home Page"); }
	 * 
	 * fc.utobj().logoutAndQuitBrowser(driver, config, testCaseId);
	 * }catch(Exception e){
	 * 
	 * fc.utobj().quitBrowserOnCatch(driver, config, e, testCaseId);} }
	 */

}
