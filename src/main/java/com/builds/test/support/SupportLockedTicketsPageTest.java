package com.builds.test.support;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminFranchiseLocationAddFranchiseLocationPageTest;
import com.builds.uimaps.support.SupportLockedTicketsPage;
import com.builds.uimaps.support.SupportTicketsPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class SupportLockedTicketsPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	SupportTicketsPageTest ticket_PageTest = new SupportTicketsPageTest();

	@Test(groups = { "support" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Archive Ticket At Support > Locked Tickets", testCaseId = "TC_39_Archive_Tickets")
	private void archiveTicket() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("support", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			SupportLockedTicketsPage pobj = new SupportLockedTicketsPage(driver);
			String userName = " FranConnect Administrator";

			fc.utobj().printTestStep("Navigate to Admin > Support > Manage Department");
			fc.utobj().printTestStep("Add Department");

			String departmentName = fc.utobj().generateTestData(dataSet.get("departmentName"));
			AdminSupportManageDepartmentPageTest departmentPage = new AdminSupportManageDepartmentPageTest();
			departmentPage.addDepartments(driver, departmentName, userName);

			fc.utobj().printTestStep("Navigate To Support > Ticket");
			fc.utobj().printTestStep("Create Ticket");

			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));

			SupportTicketsPageTest supportTicketPage = new SupportTicketsPageTest();
			supportTicketPage.createTicket(driver, departmentName, description, subject);
			ticket_PageTest.searchTicketBySubject(driver, subject);

			fc.utobj().printTestStep("Lock Ticket");
			supportTicketPage.getLock(driver, subject);

			fc.utobj().printTestStep("Navigate To Locked Ticket Page");

			String url = FranconnectUtil.config.get("buildUrl");
			url = url.concat("control/troubleTicketsLocked?isLockedTickets=Y&fromWhere=fromSearch");

			try {
				fc.utobj().moveToElementThroughAction(driver, pobj.moreActionBtn);
				fc.utobj().clickElement(driver, pobj.lockedTicketLink);
			} catch (Exception e) {
				driver.navigate().to(url);
			}

			/*
			 * fc.utobj().sendKeys(driver, pobj.supportTopSearch, subject);
			 * fc.utobj().clickElement(driver, pobj.searchImgBtn);
			 */

			ticket_PageTest.filterTicketByDepartment(driver, new SupportTicketsPage(driver), departmentName);

			fc.utobj().printTestStep("Archive Ticket");
			supportTicketPage.actionImgOption(driver, subject, "Archive");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.okBtn);
			fc.commonMethods().Click_Close_Input_ByValue(driver);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify Archive Ticket At Lock Ticket Page");
			boolean isTextPresent = fc.utobj().assertNotInPageSource(driver, subject);
			if (isTextPresent == true) {
				fc.utobj().throwsSkipException("was not able to archive a ticket");
			}

			fc.utobj().printTestStep("Navigate To Archived Ticket Page");
			fc.utobj().printTestStep("Verify Archive Ticket");

			fc.utobj().moveToElementThroughAction(driver, pobj.moreActionBtn);
			fc.utobj().clickElement(driver, pobj.archiveTicketsLink);

			/*
			 * fc.utobj().sendKeys(driver, pobj.supportTopSearch, subject);
			 * fc.utobj().clickElement(driver, pobj.searchImgBtn);
			 */

			ticket_PageTest.filterTicketByDepartment(driver, new SupportTicketsPage(driver), departmentName);

			boolean isTextPresent1 = fc.utobj().assertPageSource(driver, subject);
			if (isTextPresent1 == false) {
				fc.utobj().throwsSkipException("was not able to archive a ticket");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "support" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Assign Owner At Support > Locked Tickets", testCaseId = "TC_40_Assign_Owner")
	private void assignOwner() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("support", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			SupportLockedTicketsPage pobj = new SupportLockedTicketsPage(driver);

			String userName = " FranConnect Administrator";

			fc.utobj().printTestStep("Navigate to Admin > Support > Manage Department");
			fc.utobj().printTestStep("Add Department");

			String departmentName = fc.utobj().generateTestData(dataSet.get("departmentName"));
			AdminSupportManageDepartmentPageTest departmentPage = new AdminSupportManageDepartmentPageTest();
			departmentPage.addDepartments(driver, departmentName, userName);

			fc.utobj().printTestStep("Navigate To Support > Ticket");
			fc.utobj().printTestStep("Create Ticket");

			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			SupportTicketsPageTest supportTicketPage = new SupportTicketsPageTest();
			supportTicketPage.createTicket(driver, departmentName, description, subject);

			ticket_PageTest.searchTicketBySubject(driver, subject);

			fc.utobj().printTestStep("Get Lock");
			supportTicketPage.getLock(driver, subject);

			fc.utobj().printTestStep("Navigate To Locked Ticket Page");

			String url = FranconnectUtil.config.get("buildUrl");
			url = url.concat("control/troubleTicketsLocked?isLockedTickets=Y&fromWhere=fromSearch");

			try {
				fc.utobj().moveToElementThroughAction(driver, pobj.moreActionBtn);
				fc.utobj().clickElement(driver, pobj.lockedTicketLink);
			} catch (Exception e) {
				driver.navigate().to(url);

			}

			ticket_PageTest.filterTicketByDepartment(driver, new SupportTicketsPage(driver), departmentName);

			fc.utobj().printTestStep("Assign Owner");
			supportTicketPage.actionImgOption(driver, subject, "Assign Owner");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.assignToMeChckBx);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.commonMethods().Click_Close_Input_ByValue(driver);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify Assign Owner");

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () ,'" + subject
									+ "')]/following-sibling::td[2][contains(text () , 'FranConnect Administrator')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to assign a ticket to a current User");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"support"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Publish As A FAQ At Support > Locked Tickets", testCaseId = "TC_41_Publish_FAQ")
	private void publishAsFAQ() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("support", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			SupportLockedTicketsPage pobj = new SupportLockedTicketsPage(driver);
			String userName = " FranConnect Administrator";

			fc.utobj().printTestStep("Navigate To Admin > Support > Manage FAQs");
			fc.utobj().printTestStep("Add New Category");

			AdminSupportManageFAQsPageTest manageFAQPage = new AdminSupportManageFAQsPageTest();
			String categoryName = fc.utobj().generateTestData(dataSet.get("categoryName"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			manageFAQPage.addNewCategory(driver, categoryName, description);

			fc.utobj().printTestStep("Navigate to Admin > Support > Manage Department");
			fc.utobj().printTestStep("Add Department");
			String departmentName = fc.utobj().generateTestData(dataSet.get("departmentName"));
			AdminSupportManageDepartmentPageTest departmentPage = new AdminSupportManageDepartmentPageTest();
			departmentPage.addDepartments(driver, departmentName, userName);

			fc.utobj().printTestStep("Navigate To Support > Ticket");
			fc.utobj().printTestStep("Create Ticket");

			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			description = fc.utobj().generateTestData(dataSet.get("description"));

			SupportTicketsPageTest supportTicketPage = new SupportTicketsPageTest();
			supportTicketPage.createTicket(driver, departmentName, description, subject);

			/*
			 * fc.utobj().sendKeys(driver, pobj.supportTopSearch, subject);
			 * fc.utobj().clickElement(driver, pobj.searchImgBtn);
			 */

			ticket_PageTest.searchTicketBySubject(driver, subject);

			fc.utobj().printTestStep("Get Lock");
			supportTicketPage.getLock(driver, subject);

			fc.utobj().printTestStep("Navigate To Locked Ticket Page");

			String url = FranconnectUtil.config.get("buildUrl");
			url = url.concat("control/troubleTicketsLocked?isLockedTickets=Y&fromWhere=fromSearch");

			try {
				fc.utobj().moveToElementThroughAction(driver, pobj.moreActionBtn);
				fc.utobj().clickElement(driver, pobj.lockedTicketLink);
			} catch (Exception e) {
				driver.navigate().to(url);

			}

			/*
			 * fc.utobj().sendKeys(driver, pobj.supportTopSearch, subject);
			 * fc.utobj().clickElement(driver, pobj.searchImgBtn);
			 */

			ticket_PageTest.filterTicketByDepartment(driver, new SupportTicketsPage(driver), departmentName);
			fc.utobj().printTestStep("Publish As A FAQ");
			supportTicketPage.actionImgOption(driver, subject, "Publish as FAQ");
			fc.utobj().selectDropDown(driver, pobj.categoryName, categoryName);
			String faqQuestion = fc.utobj().generateTestData(dataSet.get("faqQuestion"));
			fc.utobj().sendKeys(driver, pobj.faqQuestion, faqQuestion);
			String faqAnswer = fc.utobj().generateTestData(dataSet.get("faqAnswer"));

			fc.utobj().sendKeys(driver, pobj.faqAnswer, faqAnswer);
			String file = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));

			fc.utobj().sendKeys(driver, pobj.attacheFile, file);
			fc.utobj().clickElement(driver, pobj.submitBtn);
			fc.utobj().clickElement(driver, pobj.okFaqBtn);
			
			fc.utobj().printTestStep("Go To Support FAQ Page");
			fc.support().support_common().supportFAQs(driver);

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text() , '" + categoryName + "')]"));

			fc.utobj().printTestStep("Verify Publish As A FAQ");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, faqQuestion);

			if (isTextPresent == false) {
				fc.utobj().throwsSkipException("was not able to verify publish as FAQ");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "support" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Update Status At Support > Locked Tickets", testCaseId = "TC_42_Update_Status")
	private void updateStatus() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("support", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			SupportLockedTicketsPage pobj = new SupportLockedTicketsPage(driver);
			String userName = " FranConnect Administrator";

			fc.utobj().printTestStep("Navigate To Admin > Support > Ticket Status Page");
			fc.utobj().printTestStep("Add Ticket Status");

			AdminSupportTicketStatusPageTest statusPage = new AdminSupportTicketStatusPageTest();
			String statusName = fc.utobj().generateTestData(dataSet.get("statusName"));
			statusPage.addTicketStatus(driver, statusName);

			fc.utobj().printTestStep("Navigate to Admin > Support > Manage Department");
			fc.utobj().printTestStep("Add Department");
			String departmentName = fc.utobj().generateTestData(dataSet.get("departmentName"));
			AdminSupportManageDepartmentPageTest departmentPage = new AdminSupportManageDepartmentPageTest();
			departmentPage.addDepartments(driver, departmentName, userName);

			fc.utobj().printTestStep("Navigate To Support > Ticket");
			fc.utobj().printTestStep("Create Ticket");

			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));

			SupportTicketsPageTest supportTicketPage = new SupportTicketsPageTest();
			supportTicketPage.createTicket(driver, departmentName, description, subject);

			ticket_PageTest.searchTicketBySubject(driver, subject);

			fc.utobj().printTestStep("Get Lock");
			supportTicketPage.getLock(driver, subject);

			fc.utobj().printTestStep("Navigate To Locked Ticket Page");

			String url = FranconnectUtil.config.get("buildUrl");
			url = url.concat("control/troubleTicketsLocked?isLockedTickets=Y&fromWhere=fromSearch");

			try {
				fc.utobj().moveToElementThroughAction(driver, pobj.moreActionBtn);
				fc.utobj().clickElement(driver, pobj.lockedTicketLink);
			} catch (Exception e) {
				driver.navigate().to(url);

			}

			/*
			 * fc.utobj().sendKeys(driver, pobj.supportTopSearch, subject);
			 * fc.utobj().clickElement(driver, pobj.searchImgBtn);
			 */

			ticket_PageTest.filterTicketByDepartment(driver, new SupportTicketsPage(driver), departmentName);

			fc.utobj().printTestStep("Update Status");
			supportTicketPage.actionImgOption(driver, subject, "Update Status");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().selectDropDown(driver, pobj.stausDropDown, statusName);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.commonMethods().Click_Close_Input_ByValue(driver);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify Updated Status");
			String text = fc.utobj().getText(driver, fc.utobj().getElementByXpath(driver,
					".//td[contains(text () ,'" + subject + "')]/following-sibling::td[3]"));

			if (!text.equalsIgnoreCase(statusName)) {
				fc.utobj().throwsSkipException("was not able to update status of Ticket");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "support", "ravi123" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Unlock Ticket At Support > Locked Tickets", testCaseId = "TC_43_Unlock_Tickets")
	private void unlockTicket() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("support", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			SupportLockedTicketsPage pobj = new SupportLockedTicketsPage(driver);
			String userName = " FranConnect Administrator";

			fc.utobj().printTestStep("Navigate to Admin > Support > Manage Department");
			fc.utobj().printTestStep("Add Department");

			String departmentName = fc.utobj().generateTestData(dataSet.get("departmentName"));
			AdminSupportManageDepartmentPageTest departmentPage = new AdminSupportManageDepartmentPageTest();
			departmentPage.addDepartments(driver, departmentName, userName);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			AdminFranchiseLocationAddFranchiseLocationPageTest addFranPage = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String franchiseId = fc.utobj().generateTestData(dataSet.get("franchiseId"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			franchiseId = addFranPage.addFranchiseLocation_All(driver, franchiseId, regionName, storeType);

			fc.utobj().printTestStep("Navigate To Support > Ticket Page");
			fc.utobj().printTestStep("Create Ticket");

			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));

			// File file = new
			// File(config.get("testDataPath").concat("/").concat("document").concat("/")+dataSet.get("uploadFile"));

			SupportTicketsPageTest supportTicketPage = new SupportTicketsPageTest();
			supportTicketPage.createTicketWithFranchiseId(driver, departmentName, franchiseId, description, subject);

			/*
			 * fc.utobj().sendKeys(driver, pobj.supportTopSearch, subject);
			 * fc.utobj().clickElement(driver, pobj.searchImgBtn);
			 */

			ticket_PageTest.searchTicketBySubject(driver, subject);

			fc.utobj().printTestStep("Get Lock");
			supportTicketPage.getLock(driver, subject);

			fc.utobj().printTestStep("Navigate To Locked Ticket Page");

			String url = FranconnectUtil.config.get("buildUrl");
			url = url.concat("control/troubleTicketsLocked?isLockedTickets=Y&fromWhere=fromSearch");

			try {
				fc.utobj().moveToElementThroughAction(driver, pobj.moreActionBtn);
				fc.utobj().clickElement(driver, pobj.lockedTicketLink);
			} catch (Exception e) {
				driver.navigate().to(url);
			}

			ticket_PageTest.filterTicketByDepartment(driver, new SupportTicketsPage(driver), departmentName);

			fc.utobj().printTestStep("Unlock Ticket");
			supportTicketPage.actionImgOption(driver, subject, "Unlock");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.okBtn);
			fc.commonMethods().Click_Close_Input_ByValue(driver);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Navigate To Support > Ticket Page");

			fc.utobj().moveToElementThroughAction(driver, pobj.moreActionBtn);

			fc.utobj().clickElement(driver, pobj.ticketsLink);

			/*
			 * fc.utobj().sendKeys(driver, pobj.supportTopSearch, subject);
			 * fc.utobj().clickElement(driver, pobj.searchImgBtn);
			 */

			ticket_PageTest.filterTicketByDepartment(driver, new SupportTicketsPage(driver), departmentName);

			fc.utobj().printTestStep("Verify Unlock Ticket");
			boolean isTextPrrsent = fc.utobj().assertPageSource(driver, subject);
			if (isTextPrrsent == false) {
				fc.utobj().throwsSkipException("was not able to unlock status");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
}
