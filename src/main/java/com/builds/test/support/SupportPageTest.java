package com.builds.test.support;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminAreaRegionAddAreaRegionPageTest;
import com.builds.test.admin.AdminDivisionAddDivisionPageTest;
import com.builds.test.admin.AdminFranchiseLocationAddFranchiseLocationPageTest;
import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.admin.AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest;
import com.builds.test.admin.AdminUsersManageManageFranchiseUsersPageTest;
import com.builds.test.admin.AdminUsersManageRegionalUsersAddRegionalUserPageTest;
import com.builds.test.common.CorporateUser;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class SupportPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "support" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Sub Tab Through Franchise User At Support Module", testCaseId = "TC_60_Verify_Sub_Tab_FU")
	private void verifySubTabFAQ() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("support", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			AdminFranchiseLocationAddFranchiseLocationPageTest addFranLocPage = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String franchiseId = fc.utobj().generateTestData(dataSet.get("franchiseId"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			addFranLocPage.addFranchiseLocation_All(driver, franchiseId, regionName, storeType);

			fc.utobj().printTestStep("Add Franchise User");

			AdminUsersManageManageFranchiseUsersPageTest addFranUserPage = new AdminUsersManageManageFranchiseUsersPageTest();
			String userName = fc.utobj().generateTestData(dataSet.get("userName"));
			String password = "t0n1ght123";
			String roleName = "Default Franchise Role";
			String emailId = "supportautomation@staffex.com";
			addFranUserPage.addFranchiseUser(driver, userName, password, franchiseId, roleName, emailId);

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With Franchise User");
			fc.loginpage().loginWithParameter(driver, userName, password);

			fc.utobj().printTestStep("Navigate To Support > FAQs Page");
			fc.support().support_common().supportFAQs(driver);

			List<WebElement> list = driver.findElements(By.xpath(".//*[@id='module_support']/ul/li/a"));

			String[] text = new String[3];
			text[0] = fc.utobj().translateString("Ask Corporate");
			text[1] = fc.utobj().translateString("FAQs");
			String option = "Contact Info";
			List<String> linkArray = fc.utobj().translate(option);
			if (!(linkArray.size() < 1)) {

				for (int i = 0; i < linkArray.size(); i++) {
					try {
						text[i + 2] = linkArray.get(i);
					} catch (Exception e) {
					}
				}
			}
			fc.utobj().printTestStep("Verify Sub Tab Of Support Module");
			boolean isTabPresent = true;
			int counter1 = 0;

			for (int y = 0; y < text.length; y++) {
				for (int x = 0; x < list.size(); x++) {
					System.out.println(list.get(x).getText().trim());
					if (list.get(x).getText().trim().equalsIgnoreCase(text[y].trim())) {
						counter1++;
					}
				}
			}
			if (counter1 < 3) {
				isTabPresent = false;
			}

			if (isTabPresent == false) {
				fc.utobj().throwsException("Was not able to find all Tab");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "support" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Sub Tab through Corporate User At Support Module", testCaseId = "TC_61_Verify_Sub_Tab_CU")
	private void verifySubTabCorporateUser() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To  Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest addCorPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "supportautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			addCorPage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With Corporate User");
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());

			fc.utobj().printTestStep("Navigate To Support > FAQs Page");
			fc.support().support_common().supportFAQs(driver);

			List<WebElement> list = driver.findElements(By.xpath(".//*[@id='module_support']/ul/li/a"));

			String[] text = new String[6];
			text[0] = "DashBoard";
			text[1] = "Tickets";
			text[2] = "Search";
			text[3] = "FAQs";
			text[4] = "Contact Info";
			text[5] = "Reports";

			fc.utobj().printTestStep("Verify Sub Tabs Of Support Module");
			boolean isTabPresent = false;
			int counter1 = 0;

			for (int y = 0; y < text.length; y++) {
				for (int x = 0; x < list.size(); x++) {
					System.out.println(list.get(x).getText().trim());
					if (list.get(x).getText().trim()
							.equalsIgnoreCase(/* text[y] */fc.utobj().translate(text[y]).get(0))) {
						counter1++;
					}
					if (counter1 > 0) {
						isTabPresent = true;
					} else {
						isTabPresent = false;
						break;
					}
				}
			}

			if (isTabPresent == false) {
				fc.utobj().throwsException("Was not able to find all Tab");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "support"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Sub Tab through Divisional User", testCaseId = "TC_62_Verify_Sub_Tab_DU")
	private void verifySubTabDivisionalUser() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("support", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > Division > Add Division");
			fc.utobj().printTestStep("Add Division");

			AdminDivisionAddDivisionPageTest addDivPage = new AdminDivisionAddDivisionPageTest();
			String divisionName = fc.utobj().generateTestData(dataSet.get("divisionName"));
			addDivPage.addDivision(driver, divisionName);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Divisional User");
			fc.utobj().printTestStep("Add Divisional User");

			AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest addDivUserPage = new AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest();
			String userName = fc.utobj().generateTestData(dataSet.get("userName"));
			String password = "t0n1ght123";
			String emailId = "supportautomation@staffex.com";
			addDivUserPage.addDivisionalUser(driver, userName, password, divisionName, emailId);

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With Divisional User");
			fc.loginpage().loginWithParameter(driver, userName, password);

			fc.utobj().printTestStep("Navigate To Support > FAQs Page");
			fc.support().support_common().supportFAQs(driver);

			List<WebElement> list = driver.findElements(By.xpath(".//*[@id='module_support']/ul/li/a"));

			String[] text = new String[6];
			text[0] = "DashBoard";
			text[1] = "Tickets";
			text[2] = "Search";
			text[3] = "FAQs";
			text[4] = "Contact Info";
			text[5] = "Reports";

			fc.utobj().printTestStep("Verify Sub Tab Of Support Module");
			boolean isTabPresent = false;
			int counter1 = 0;

			for (int y = 0; y < text.length; y++) {
				for (int x = 0; x < list.size(); x++) {
					System.out.println(list.get(x).getText().trim());
					if (list.get(x).getText().trim().equalsIgnoreCase(text[y].trim())) {
						counter1++;
					}
					if (counter1 > 0) {
						isTabPresent = true;
					} else {
						isTabPresent = false;
						break;
					}
				}
			}

			if (isTabPresent == false) {
				fc.utobj().throwsException("Was not able to find all Tab");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "support"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Sub Tab through Regional User At Support Module", testCaseId = "TC_63_Verify_Sub_Tab_RU")
	private void verifySubTabRegionalUser() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("support", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > Area/Region > Add Area/Region");
			fc.utobj().printTestStep("Add Area/Region");

			AdminAreaRegionAddAreaRegionPageTest addRegionPage = new AdminAreaRegionAddAreaRegionPageTest();
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			addRegionPage.addAreaRegion(driver, regionName);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Regional User");
			fc.utobj().printTestStep("Add Regional User");

			AdminUsersManageRegionalUsersAddRegionalUserPageTest addRegUserPage = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
			String userName = fc.utobj().generateTestData(dataSet.get("userName"));
			String password = "t0n1ght123";
			String emailId = "supportautomation@staffex.com";
			addRegUserPage.addRegionalUser(driver, userName, password, regionName, emailId);

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With Regional User");
			fc.loginpage().loginWithParameter(driver, userName, password);

			fc.utobj().printTestStep("Navigate To Support > FAQs Page");
			fc.support().support_common().supportFAQs(driver);

			List<WebElement> list = driver.findElements(By.xpath(".//*[@id='module_support']/ul/li/a"));

			String[] text = new String[3];
			text[0] = "Ask Corporate";
			text[1] = "FAQs";
			text[2] = "Contact Info";
			//
			fc.utobj().printTestStep("Verify Sub Tabs Of Support Module");
			boolean isTabPresent = false;
			int counter1 = 0;

			for (int y = 0; y < text.length; y++) {
				for (int x = 0; x < list.size(); x++) {
					System.out.println(list.get(x).getText().trim());
					if (list.get(x).getText().trim()
							.equalsIgnoreCase(/* text[y].trim() */fc.utobj().translate(text[y]).get(0))) {
						counter1++;
					}
					if (counter1 > 0) {
						isTabPresent = true;
					} else {
						isTabPresent = false;
						break;
					}
				}
			}

			if (isTabPresent == false) {
				fc.utobj().throwsException("Was not able to find all Tab");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
}
