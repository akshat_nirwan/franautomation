package com.builds.test.support;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.uimaps.support.SupportReportedTicketsPage;
import com.builds.uimaps.support.SupportTicketsPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class SupportReportedTicketsPageTest {
	FranconnectUtil fc = new FranconnectUtil();
	SupportTicketsPageTest ticket_PageTest = new SupportTicketsPageTest();

	@Test(groups = { "support", "newcheckapply1" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Archive Ticket At Support > Reported Tickets", testCaseId = "TC_44_Archive_Tickets")
	private void archiveTicket() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("support", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			SupportReportedTicketsPage pobj = new SupportReportedTicketsPage(driver);
			String userName = " FranConnect Administrator";

			fc.utobj().printTestStep("Navigate to Admin > Support > Manage Department");
			fc.utobj().printTestStep("Add Department");
			String departmentName = fc.utobj().generateTestData(dataSet.get("departmentName"));
			AdminSupportManageDepartmentPageTest departmentPage = new AdminSupportManageDepartmentPageTest();
			departmentPage.addDepartments(driver, departmentName, userName);

			fc.utobj().printTestStep("Navigate To Support > Ticket");
			fc.utobj().printTestStep("Create Ticket");
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));

			SupportTicketsPageTest supportTicketPage = new SupportTicketsPageTest();
			supportTicketPage.createTicket(driver, departmentName, description, subject);

			/*
			 * fc.utobj().sendKeys(driver, pobj.supportTopSearch, subject);
			 * fc.utobj().clickElement(driver, pobj.searchImgBtn);
			 */

			ticket_PageTest.searchTicketBySubject(driver, subject);

			String url = FranconnectUtil.config.get("buildUrl");
			url = url.concat("control/troubleTicketsReported?isReportedTickets=Y&fromWhere=fromSearch");

			try {
				fc.utobj().moveToElementThroughAction(driver, pobj.moreActionBtn);
				fc.utobj().clickElement(driver, pobj.reportedTickets);
			} catch (Exception e) {
				driver.navigate().to(url);

			}

			/*
			 * fc.utobj().sendKeys(driver, pobj.supportTopSearch, subject);
			 * fc.utobj().clickElement(driver, pobj.searchImgBtn);
			 */

			ticket_PageTest.filterTicketByDepartment(driver, new SupportTicketsPage(driver), departmentName);

			fc.utobj().printTestStep("Archive Ticket");
			supportTicketPage.actionImgOption(driver, subject, "Archive");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.okBtn);
			fc.commonMethods().Click_Close_Input_ByValue(driver);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify Archive Ticket");
			boolean isTextPresent = fc.utobj().assertNotInPageSource(driver, subject);
			if (isTextPresent == true) {
				fc.utobj().throwsSkipException("was not able to archive a ticket");
			}

			fc.utobj().moveToElementThroughAction(driver, pobj.moreActionBtn);

			fc.utobj().clickElement(driver, pobj.archiveTicketsLink);

			/*
			 * fc.utobj().sendKeys(driver, pobj.supportTopSearch, subject);
			 * fc.utobj().clickElement(driver, pobj.searchImgBtn);
			 */

			ticket_PageTest.filterTicketByDepartment(driver, new SupportTicketsPage(driver), departmentName);

			boolean isTextPresent1 = fc.utobj().assertPageSource(driver, subject);
			if (isTextPresent1 == false) {
				fc.utobj().throwsSkipException("was not able to archive a ticket");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "support" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Assign Owner At Support > Reported Tickets", testCaseId = "TC_45_Assign_Owner")
	private void assignOwner() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("support", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			SupportReportedTicketsPage pobj = new SupportReportedTicketsPage(driver);

			String userName = " FranConnect Administrator";
			fc.utobj().printTestStep("Navigate to Admin > Support > Manage Department");
			fc.utobj().printTestStep("Add Department");
			String departmentName = fc.utobj().generateTestData(dataSet.get("departmentName"));
			AdminSupportManageDepartmentPageTest departmentPage = new AdminSupportManageDepartmentPageTest();
			departmentPage.addDepartments(driver, departmentName, userName);

			fc.utobj().printTestStep("Navigate To Support > Ticket");
			fc.utobj().printTestStep("Create Ticket");

			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			SupportTicketsPageTest supportTicketPage = new SupportTicketsPageTest();
			supportTicketPage.createTicket(driver, departmentName, description, subject);

			ticket_PageTest.searchTicketBySubject(driver, subject);

			String url = FranconnectUtil.config.get("buildUrl");
			url = url.concat("control/troubleTicketsReported?isReportedTickets=Y&fromWhere=fromSearch");

			try {
				fc.utobj().moveToElementThroughAction(driver, pobj.moreActionBtn);
				fc.utobj().clickElement(driver, pobj.reportedTickets);
			} catch (Exception e) {
				driver.navigate().to(url);

			}

			ticket_PageTest.filterTicketByDepartment(driver, new SupportTicketsPage(driver), departmentName);

			fc.utobj().printTestStep("Assign Owner");
			supportTicketPage.actionImgOption(driver, subject, "Assign Owner");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.assignToMeChckBx);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.commonMethods().Click_Close_Input_ByValue(driver);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().moveToElementThroughAction(driver, pobj.moreActionBtn);
			fc.utobj().clickElement(driver, pobj.ticketsLink);

			/*
			 * fc.utobj().sendKeys(driver, pobj.supportTopSearch, subject);
			 * fc.utobj().clickElement(driver, pobj.searchImgBtn);
			 */

			ticket_PageTest.filterTicketByDepartment(driver, new SupportTicketsPage(driver), departmentName);

			fc.utobj().printTestStep("Verify Assign Owner");
			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () ,'" + subject
									+ "')]/following-sibling::td[2][contains(text () , 'FranConnect Administrator')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to assign a ticket to a current User");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sp123" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Publish As A FAQ At Support > Reported Tickets", testCaseId = "TC_46_Publish_FAQ")
	private void publishAsFAQ() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("support", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			SupportReportedTicketsPage pobj = new SupportReportedTicketsPage(driver);
			String userName = " FranConnect Administrator";

			fc.utobj().printTestStep("Navigate To Admin > Support > Manage FAQs");
			fc.utobj().printTestStep("Add New Category");

			AdminSupportManageFAQsPageTest manageFAQPage = new AdminSupportManageFAQsPageTest();
			String categoryName = fc.utobj().generateTestData(dataSet.get("categoryName"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			manageFAQPage.addNewCategory(driver, categoryName, description);

			fc.utobj().printTestStep("Navigate to Admin > Support > Manage Department");
			fc.utobj().printTestStep("Add Department");

			String departmentName = fc.utobj().generateTestData(dataSet.get("departmentName"));
			AdminSupportManageDepartmentPageTest departmentPage = new AdminSupportManageDepartmentPageTest();
			departmentPage.addDepartments(driver, departmentName, userName);

			fc.utobj().printTestStep("Navigate To Support > Ticket");
			fc.utobj().printTestStep("Create Ticket");

			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			description = fc.utobj().generateTestData(dataSet.get("description"));

			SupportTicketsPageTest supportTicketPage = new SupportTicketsPageTest();
			supportTicketPage.createTicket(driver, departmentName, description, subject);
			ticket_PageTest.searchTicketBySubject(driver, subject);

			String url = FranconnectUtil.config.get("buildUrl");
			url = url.concat("control/troubleTicketsReported?isReportedTickets=Y&fromWhere=fromSearch");

			try {
				fc.utobj().moveToElementThroughAction(driver, pobj.moreActionBtn);
				fc.utobj().clickElement(driver, pobj.reportedTickets);
			} catch (Exception e) {
				driver.navigate().to(url);

			}
			ticket_PageTest.filterTicketByDepartment(driver, new SupportTicketsPage(driver), departmentName);

			fc.utobj().printTestStep("Publish As FAQ");
			supportTicketPage.actionImgOption(driver, subject, "Publish as FAQ");
			fc.utobj().selectDropDown(driver, pobj.categoryName, categoryName);
			String faqQuestion = fc.utobj().generateTestData(dataSet.get("faqQuestion"));
			fc.utobj().sendKeys(driver, pobj.faqQuestion, faqQuestion);
			String faqAnswer = fc.utobj().generateTestData(dataSet.get("faqAnswer"));
			fc.utobj().sendKeys(driver, pobj.faqAnswer, faqAnswer);

			String file = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));

			fc.utobj().sendKeys(driver, pobj.attacheFile, file);
			fc.utobj().clickElement(driver, pobj.submitBtn);
			fc.utobj().clickElement(driver, pobj.okFaqBtn);
			fc.support().support_common().supportFAQs(driver);

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text() , '" + categoryName + "')]"));

			fc.utobj().printTestStep("Verify Pubish As A FAQ");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, faqQuestion);

			if (isTextPresent == false) {
				fc.utobj().throwsSkipException("was not able to verify publish as FAQ");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "support", "supporttest5487"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Update Status At Support > Reported Tickets", testCaseId = "TC_47_Updated_Status")
	private void updateStatus() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("support", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			SupportReportedTicketsPage pobj = new SupportReportedTicketsPage(driver);

			String userName = " FranConnect Administrator";

			AdminSupportTicketStatusPageTest statusPage = new AdminSupportTicketStatusPageTest();
			String statusName = fc.utobj().generateTestData(dataSet.get("statusName"));
			statusPage.addTicketStatus(driver, statusName);

			fc.utobj().printTestStep("Navigate to Admin > Support > Manage Department");
			fc.utobj().printTestStep("Add Department");
			String departmentName = fc.utobj().generateTestData(dataSet.get("departmentName"));
			AdminSupportManageDepartmentPageTest departmentPage = new AdminSupportManageDepartmentPageTest();
			departmentPage.addDepartments(driver, departmentName, userName);

			fc.utobj().printTestStep("Navigate To Support > Ticket");
			fc.utobj().printTestStep("Create Ticket");

			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));

			SupportTicketsPageTest supportTicketPage = new SupportTicketsPageTest();
			supportTicketPage.createTicket(driver, departmentName, description, subject);

			String url = FranconnectUtil.config.get("buildUrl");
			url = url.concat("control/troubleTicketsReported?isReportedTickets=Y&fromWhere=fromSearch");

			try {
				fc.utobj().moveToElementThroughAction(driver, pobj.moreActionBtn);
				fc.utobj().clickElement(driver, pobj.reportedTickets);
			} catch (Exception e) {
				driver.navigate().to(url);

			}

			ticket_PageTest.filterTicketByDepartment(driver, new SupportTicketsPage(driver), departmentName);
			
			fc.utobj().printTestStep("Update Status");
			supportTicketPage.actionImgOption(driver, subject, "Update Status");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().selectDropDown(driver, pobj.stausDropDown, statusName);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.commonMethods().Click_Close_Input_ByValue(driver);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify Update Status");
			String text = fc.utobj().getText(driver, fc.utobj().getElementByXpath(driver,
					".//td[contains(text () ,'" + subject + "')]/following-sibling::td[3]"));

			if (!text.equalsIgnoreCase(statusName)) {
				fc.utobj().throwsSkipException("was not able to update status of Ticket");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "support" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Get Lock Ticket At Support > Reported Tickets", testCaseId = "TC_48_Get_Lock")
	private void getLock() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("support", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			SupportReportedTicketsPage pobj = new SupportReportedTicketsPage(driver);
			String userName = " FranConnect Administrator";

			fc.utobj().printTestStep("Navigate to Admin > Support > Manage Department");
			fc.utobj().printTestStep("Add Department");
			String departmentName = fc.utobj().generateTestData(dataSet.get("departmentName"));
			AdminSupportManageDepartmentPageTest departmentPage = new AdminSupportManageDepartmentPageTest();
			departmentPage.addDepartments(driver, departmentName, userName);

			fc.utobj().printTestStep("Navigate To Support > Ticket");
			fc.utobj().printTestStep("Create Ticket");

			SupportTicketsPageTest ticketsPage = new SupportTicketsPageTest();
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			ticketsPage.createTicket(driver, departmentName, description, subject);

			fc.utobj().printTestStep("Navigate To Reported Ticket Page");

			String url = FranconnectUtil.config.get("buildUrl");
			url = url.concat("control/troubleTicketsReported?isReportedTickets=Y&fromWhere=fromSearch");

			try {
				fc.utobj().moveToElementThroughAction(driver, pobj.moreActionBtn);
				fc.utobj().clickElement(driver, pobj.reportedTickets);
			} catch (Exception e) {
				driver.navigate().to(url);

			}
			ticket_PageTest.filterTicketByDepartment(driver, new SupportTicketsPage(driver), departmentName);

			fc.utobj().printTestStep("Get Lock");
			ticketsPage.actionImgOption(driver, subject, "Get Lock");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.okBtn);
			fc.commonMethods().Click_Close_Input_ByValue(driver);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Navigate To Locked Ticket Page");

			fc.utobj().moveToElementThroughAction(driver, pobj.moreActionBtn);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(@href , '?isLockedTickets')]"));
			ticket_PageTest.filterTicketByDepartment(driver, new SupportTicketsPage(driver), departmentName);

			fc.utobj().printTestStep("Verify Get Lock");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, subject);

			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to lock the ticket");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
}
