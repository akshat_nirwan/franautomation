package com.builds.test.support;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class SupportReportsPageTest extends FranconnectUtil {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "supportReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click Over Tickets by Owner(s) and Status", testCaseId = "TC_Support_Report_01")
	private void supportReports01() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("support",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Support > Reports Page > Tickets by Owner(s) and Status");
			fc.support().support_common().supportReports(driver);
			navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'Tickets by Owner(s) and Status')]"));

			// Automatic fill multi Drop Down If Present in the page
			setSelectAllMultiDropDown(driver);

			navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "supportReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click Over Tickets by Department(s) and Status", testCaseId = "TC_Support_Report_02")
	private void supportReports02() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("support",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Support > Reports Page > Tickets by Department(s) and Status");
			fc.support().support_common().supportReports(driver);
			navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'Tickets by Department(s) and Status')]"));
			// Automatic fill multi Drop Down If Present in the page
			setSelectAllMultiDropDown(driver);

			navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "supportReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click Over Ticket Timeline Report", testCaseId = "TC_Support_Report_03")
	private void supportReports03() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("support",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Support > Reports Page > Ticket Timeline Report");
			fc.support().support_common().supportReports(driver);
			navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'Ticket Timeline Report')]"));

			// Automatic fill multi Drop Down If Present in the page
			setSelectAllMultiDropDown(driver);

			navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void navigateToSessionSizePage(WebDriver driver) {

		/*
		 * driver.navigate().to(
		 * "http://cluster.franconnect.net/fcautomation/sessionSize.jsp");
		 * driver.navigate().back();
		 */
	}

	public void setSelectAllMultiDropDown(WebDriver driver) {
		try {
			List<WebElement> multiSelect = driver.findElements(By.xpath(".//div[@class='ms-parent']"));

			for (WebElement webElement : multiSelect) {

				WebElement element = fc.utobj().getElementByID(driver, webElement.getAttribute("id"));

				fc.utobj().clickElement(driver, element);

				WebElement selectAll = element.findElement(By.xpath(".//input[@id='selectAll']"));
				if (fc.utobj().isSelected(driver, selectAll) == false) {
					fc.utobj().clickElement(driver, selectAll);
				}
				fc.utobj().clickElement(driver, element);
			}

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@class='smlFbtn' and contains(@value , 'View Report')]"));

		} catch (Exception e) {
			System.out.println("Problem In Setting Multi Drop");
		}
	}

}
