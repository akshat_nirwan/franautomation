package com.builds.test.support;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminAreaRegionAddAreaRegionPageTest;
import com.builds.test.admin.AdminDivisionAddDivisionPageTest;
import com.builds.test.admin.AdminFranchiseLocationAddFranchiseLocationPageTest;
import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.admin.AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest;
import com.builds.test.admin.AdminUsersManageManageFranchiseUsersPageTest;
import com.builds.test.admin.AdminUsersManageRegionalUsersAddRegionalUserPageTest;
import com.builds.test.common.CorporateUser;
import com.builds.uimaps.fs.SearchUI;
import com.builds.uimaps.support.AdminSupportManageDepartmentPage;
import com.builds.uimaps.support.SupportSearchPage;
import com.builds.uimaps.support.SupportTicketsPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class SupportTicketsPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "support", "supportsmoke", "supportemail"})

	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-29", testCaseDescription = "Verify the Create Ticket At Support Ticket", testCaseId = "TC_30_Create_Ticket")
	private void createTicket() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("support", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			SupportTicketsPage pobj = new SupportTicketsPage(driver);
			SearchUI search_page = new SearchUI(driver);

			fc.utobj().printTestStep("Navigate to Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest cu = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			String emailId = "supportautomation@staffex.com";

			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			cu.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			String userName = corpUser.getuserFullName();

			fc.utobj().printTestStep("Navigate To Admin > Support > Manage Department");
			fc.utobj().printTestStep("Add Department");
			String departmentName = fc.utobj().generateTestData(dataSet.get("departmentName"));
			AdminSupportManageDepartmentPageTest departmentPage = new AdminSupportManageDepartmentPageTest();
			departmentPage.addDepartments(driver, departmentName, userName);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");
			AdminFranchiseLocationAddFranchiseLocationPageTest addFranPage = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String franchiseId = fc.utobj().generateTestData(dataSet.get("franchiseId"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			franchiseId = addFranPage.addFranchiseLocation_All(driver, franchiseId, regionName, storeType);

			fc.utobj().printTestStep("Navigate To Support > Ticket");
			fc.utobj().printTestStep("Create Ticket");
			fc.support().support_common().supportTickets(driver);
			fc.utobj().clickElement(driver, pobj.createTicketLink);

			fc.utobj().selectDropDown(driver, pobj.departmentDropDown, departmentName);
			fc.utobj().selectDropDown(driver, pobj.priorityDropDown, dataSet.get("priority"));
			fc.utobj().selectDropDown(driver, pobj.franchiseIdDropDown, franchiseId);
			fc.utobj().sendKeys(driver, pobj.phoneTextBox, "1236547893");
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subjectTextBox, subject);
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().sendKeys(driver, pobj.descriptionTextBox, description);
			String file = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));

			fc.utobj().sendKeys(driver, pobj.attachFileBrowseBox, file);
			fc.utobj().clickElement(driver, pobj.submitButton);
			fc.utobj().clickElement(driver, pobj.okBtn);

			boolean isSearchTrue = false;
			for (int i = 0; i < 3; i++) {
				if (isSearchTrue == false) {
					fc.utobj().sendKeys(driver, search_page.topSearchField, subject);
					Thread.sleep(5000);
					if (i == 2) {
						Thread.sleep(5000);
					}
					fc.utobj().clickEnterOnElement(driver, search_page.topSearchField);
					try {
						isSearchTrue = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//custom[contains(text () , '" + subject + "')]");
					} catch (Exception e) {
						Reporter.log(e.getMessage());
						System.out.println(e.getMessage());
					}
				}
			}
			if (isSearchTrue == false) {
				fc.utobj().throwsException("Not able to search Ticket By Top Search");
			}
			fc.utobj().refresh(driver);
			searchTicketBySubject(driver, subject);
			String ticketNumber = fc.utobj().getText(driver,
					fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + subject
							+ "')]/preceding-sibling::td/a[contains(@href , 'supportTroubleTicketDetails')]"));

			fc.utobj().printTestStep("Verify Create Ticket");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, subject);

			if (isTextPresent == false) {
				fc.utobj().throwsSkipException("was not able to verify subject of Ticket");
			}

			boolean isTextPresent1 = fc.utobj().assertPageSource(driver, departmentName);

			if (isTextPresent1 == false) {
				fc.utobj().throwsSkipException("was not able to verify department Name");
			}

			fc.utobj().printTestStep("Verify Ticket Creation Mail");

			Map<String, String> mailData = new HashMap<String, String>();

			mailData = fc.utobj().readMailBox("Ticket #" + ticketNumber + " assigned", ticketNumber, emailId,
					"sdg@1a@Hfs");

			if (mailData.size() == 0 || !mailData.get("mailBody").contains(ticketNumber)) {

				fc.utobj().throwsException("was not able to verify ");
			}

			if (mailData.size() == 0 || !mailData.get("mailBody").contains(subject)) {

				fc.utobj().throwsException("was not able to verify subject in mail");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String createTicket(WebDriver driver,
			/* Map<String, String> dataSet , */String departmentName,
			/* String franchiseId , *//* File file , */String description, String subject) throws Exception {
		String testCaseId = "TC_Create_Ticket";

		if (fc.utobj().validate(testCaseId)) {
			try {
				SupportTicketsPage pobj = new SupportTicketsPage(driver);
				fc.support().support_common().supportTickets(driver);
				fc.utobj().clickElement(driver, pobj.createTicketLink);
				fc.utobj().selectDropDown(driver, pobj.departmentDropDown, departmentName);
				// fc.utobj().selectDropDown(driver, pobj.priorityDropDown,
				// dataSet.get("priority"));
				// fc.utobj().selectDropDown(driver, pobj.franchiseIdDropDown,
				// franchiseId);
				fc.utobj().sendKeys(driver, pobj.phoneTextBox, "1236547893");
				fc.utobj().sendKeys(driver, pobj.subjectTextBox, subject);
				fc.utobj().sendKeys(driver, pobj.descriptionTextBox, description);
				// fc.utobj().sendKeys(driver, pobj.attachFileBrowseBox,
				// file.getAbsolutePath());
				fc.utobj().clickElement(driver, pobj.submitButton);
				fc.utobj().clickElement(driver, pobj.okBtn);
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Was not Able to Create Ticket :: Support > Ticket");

		}
		return subject;
	}

	public String createTicketWithFranchiseId(WebDriver driver,
			/* Map<String, String> dataSet , */String departmentName, String franchiseId,
			/* File file , */String description, String subject) throws Exception {
		String testCaseId = "TC_Create_Ticket";

		if (fc.utobj().validate(testCaseId)) {
			try {
				SupportTicketsPage pobj = new SupportTicketsPage(driver);
				fc.support().support_common().supportTickets(driver);
				fc.utobj().clickElement(driver, pobj.createTicketLink);
				fc.utobj().selectDropDown(driver, pobj.departmentDropDown, departmentName);
				// fc.utobj().selectDropDown(driver, pobj.priorityDropDown,
				// dataSet.get("priority"));
				fc.utobj().selectDropDown(driver, pobj.franchiseIdDropDown, franchiseId);
				fc.utobj().sendKeys(driver, pobj.phoneTextBox, "1236547893");
				fc.utobj().sendKeys(driver, pobj.subjectTextBox, subject);
				fc.utobj().sendKeys(driver, pobj.descriptionTextBox, description);
				// fc.utobj().sendKeys(driver, pobj.attachFileBrowseBox,
				// file.getAbsolutePath());
				fc.utobj().clickElement(driver, pobj.submitButton);
				fc.utobj().clickElement(driver, pobj.okBtn);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Was not Able to Create Ticket :: Support > Ticket");

		}
		return subject;
	}

	@Test(groups = { "support", "support_demo" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Archive Ticket At Support Ticket", testCaseId = "TC_31_Archive_Ticket")
	private void archiveTicket() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		
		Map<String, String> dataSet = fc.utobj().readTestData("support", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			SupportTicketsPage pobj = new SupportTicketsPage(driver);
			String parentWindow = driver.getWindowHandle();

			fc.utobj().printTestStep("Navigate to Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest cu = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "supportautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			cu.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			String userName = corpUser.getuserFullName();

			fc.utobj().printTestStep("Navigate to Admin > Support > Manage Department");
			fc.utobj().printTestStep("Add Department");
			String departmentName = fc.utobj().generateTestData(dataSet.get("departmentName"));
			AdminSupportManageDepartmentPageTest departmentPage = new AdminSupportManageDepartmentPageTest();
			departmentPage.addDepartments(driver, departmentName, userName);

			fc.utobj().printTestStep("Navigate To Support > Ticket");
			fc.utobj().printTestStep("Create Ticket");

			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			createTicket(driver, departmentName, description, subject);

			/*
			 * fc.utobj().sendKeys(driver, pobj.supportTopSearch, subject);
			 * fc.utobj().clickElement(driver, pobj.searchImgBtn);
			 */

			searchTicketBySubject(driver, subject);

			fc.utobj().printTestStep("Archive Ticket");
			actionImgOption(driver, subject, "Archive");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.okBtn);
			fc.commonMethods().Click_Close_Input_ByValue(driver);
			driver.switchTo().window(parentWindow);

			fc.utobj().printTestStep("Verify Archive Ticket");
			boolean isTextPresent = fc.utobj().assertNotInPageSource(driver, subject);
			if (isTextPresent == true) {
				fc.utobj().throwsSkipException("was not able to archive a ticket");
			}

			fc.utobj().printTestStep("Verify Archive Ticket At Archived Ticket Page");

			fc.utobj().moveToElementThroughAction(driver, pobj.moreActionBtn);
			fc.utobj().clickElement(driver, pobj.archiveTicketsLink);

			/*
			 * fc.utobj().sendKeys(driver, pobj.supportTopSearch, subject);
			 * fc.utobj().clickElement(driver, pobj.searchImgBtn);
			 */

			filterTicketByDepartment(driver, pobj, departmentName);

			boolean isTextPresent1 = fc.utobj().assertPageSource(driver, subject);
			if (isTextPresent1 == false) {
				fc.utobj().throwsSkipException("was not able to archive a ticket");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"support" , "supporttest0803"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Assign Owner with Option to Current Logged User At Support Ticket Page", testCaseId = "TC_32_Assign_Owner_Assign_To_Me")
	private void assignOwnerAssignToMe() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("support", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			SupportTicketsPage pobj = new SupportTicketsPage(driver);

			fc.utobj().printTestStep("Navigate to Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest cu = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "supportautomation@staffex.com";

			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			cu.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			String userName = corpUser.getuserFullName();

			fc.utobj().printTestStep("Navigate to Admin > Support > Manage Department");
			fc.utobj().printTestStep("Add Department");
			String departmentName = fc.utobj().generateTestData(dataSet.get("departmentName"));
			AdminSupportManageDepartmentPageTest departmentPage = new AdminSupportManageDepartmentPageTest();
			departmentPage.addDepartments(driver, departmentName, userName);

			fc.utobj().printTestStep("Navigate To Support > Ticket");
			fc.utobj().printTestStep("Create Ticket");

			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			createTicket(driver, departmentName, description, subject);
			
			fc.utobj().printTestStep("Add corporate to user");
			
			CorporateUser corpUser1 = new CorporateUser();
			corpUser1.setEmail(emailId);
			corpUser1 = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser1);
			cu.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser1);
			
			fc.utobj().printTestStep("Navigate to Admin > Support > Manage Department");
			fc.utobj().printTestStep("Modify Department Add Department");
			
			AdminSupportManageDepartmentPage department_page = new AdminSupportManageDepartmentPage(driver);
			fc.support().support_common().adminSupportManageDepartment(driver);
			departmentPage.departmentUserView(driver, new AdminSupportManageDepartmentPage(driver));
			
			List<WebElement> listOfElement=fc.utobj().getElementListByXpath(driver, ".//*[@qat_maintable='withoutheader']/td[1]");
			boolean isTextFound=false;
			int k=0;
			for (int i = 0; i < listOfElement.size(); i++) {
				String dep=listOfElement.get(i).getText();
				
				if (dep.toLowerCase().contains(departmentName.toLowerCase())) {
					isTextFound=true;
					k=i;
					break;
				}
			}
			if (isTextFound) {
				String alterText=listOfElement.get(k).findElement(By.xpath("./ancestor::tr/td/div[@id='menuBar']/layer")).getAttribute("id").trim();
				alterText = alterText.replace("Actions_dynamicmenu", "");
				alterText = alterText.replace("Bar", "");
				fc.utobj().clickElement(driver, listOfElement.get(k).findElement(By.xpath("./ancestor::tr/td/div[@id='menuBar']/layer/a/img")));
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//div[@id='Actions_dynamicmenu" + alterText + "Menu']/span[contains(text () , 'Modify')]"));
			}
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			List<String> listInfo=new ArrayList<>();
			listInfo.add(userName);
			listInfo.add(corpUser1.getuserFullName());
			fc.utobj().selectMultipleValFromMultiSelect(driver, department_page.assignUserSelectBtn, listInfo);
			fc.utobj().clickElement(driver, department_page.submit);
			fc.commonMethods().Click_Close_Input_ByValue(driver);
			fc.utobj().switchFrameToDefault(driver);
			
			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With newly created Corporate User");
			fc.loginpage().loginWithParameter(driver, corpUser1.getUserName(), corpUser1.getPassword());

			fc.utobj().printTestStep("Navigate To Support > Search");
			fc.support().support_common().supportTickets(driver);
			
			fc.utobj().printTestStep("Search Ticket");
			searchTicketBySubject(driver, subject);
			
			fc.utobj().printTestStep("Assign Owner");
			actionImgOption(driver, subject, "Assign Owner");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.assignToMeChckBx);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.commonMethods().Click_Close_Input_ByValue(driver);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify Assign Owner");
			if (!fc.utobj().getElementByXpath(driver,".//td[contains(text () ,'" + subject
									+ "')]/following-sibling::td[2][contains(text () , '"+corpUser1.getuserFullName()+"')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to assign a ticket to a current User");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "support", "supportemail"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = " Verify the Assign Owner with option Within Department At Support Tickets", testCaseId = "TC_33_Assign_Owner_Within_Department")
	private void assignOwnerAssignToWithinDepartment() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("support", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			SupportTicketsPage pobj = new SupportTicketsPage(driver);

			fc.utobj().printTestStep("Navigate to Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest cu = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			String emailId = "supportautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			cu.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			String userName = corpUser.getuserFullName();

			fc.utobj().printTestStep("Navigate to Admin > Support > Manage Department");
			fc.utobj().printTestStep("Add Department");
			String departmentName = fc.utobj().generateTestData(dataSet.get("departmentName"));
			AdminSupportManageDepartmentPageTest departmentPage = new AdminSupportManageDepartmentPageTest();
			departmentPage.addDepartments(driver, departmentName, userName);

			fc.utobj().printTestStep("Navigate To Support > Ticket");
			fc.utobj().printTestStep("Create Ticket");

			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			createTicket(driver, departmentName, description, subject);

			fc.utobj().printTestStep("Search Ticket By Subject");

			searchTicketBySubject(driver, subject);
			String ticketNumber = fc.utobj().getText(driver,
					fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + subject
							+ "')]/preceding-sibling::td/a[contains(@href , 'supportTroubleTicketDetails')]"));

			fc.utobj().printTestStep("Assign Owner To Assign Within Department");
			actionImgOption(driver, subject, "Assign Owner");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.radioButton.get(0));
			fc.utobj().selectDropDown(driver, pobj.selectOwnerDropDown, userName);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().switchFrameToDefault(driver);
			
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.commonMethods().Click_Close_Input_ByValue(driver);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify Assign Owner");
			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () ,'" + subject
					+ "')]/following-sibling::td[2][contains(text () , '" + userName + "')]").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify assigned user");
			}

			fc.utobj().printTestStep("Verify Ticket Assignment Mail");
			Map<String, String> mailData = new HashMap<String, String>();
			mailData = fc.utobj().readMailBox("Ticket #" + ticketNumber + " assigned", ticketNumber, emailId,
					"sdg@1a@Hfs");

			if (mailData.size() == 0 || !mailData.get("mailBody").contains(ticketNumber)) {

				fc.utobj().throwsException("was not able to verify ");
			}
			if (mailData.size() == 0 || !mailData.get("mailBody").contains(subject)) {
				fc.utobj().throwsException("was not able to verify subject in mail");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"support"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Assign Owner with option Across Department At Support Tickets Page", testCaseId = "TC_34_Assign_Owner_Across_Department")
	private void assignOwnerAssignToAcrossDepartment() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		
		Map<String, String> dataSet = fc.utobj().readTestData("support", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			SupportTicketsPage pobj = new SupportTicketsPage(driver);

			fc.utobj().printTestStep("Navigate to Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest cu = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			String emailId = "supportautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			cu.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			String userName = corpUser.getuserFullName();

			fc.utobj().printTestStep("Navigate to Admin > Support > Manage Department");
			fc.utobj().printTestStep("Add Department");
			String departmentName = fc.utobj().generateTestData(dataSet.get("departmentName"));
			AdminSupportManageDepartmentPageTest departmentPage = new AdminSupportManageDepartmentPageTest();
			departmentPage.addDepartments(driver, departmentName, userName);

			fc.utobj().printTestStep("Add Corporate User to assign");
			CorporateUser corpUserAssign = new CorporateUser();
			corpUserAssign.setEmail(emailId);
			corpUserAssign = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUserAssign);
			cu.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUserAssign);

			fc.utobj().printTestStep("Navigate to Admin > Support > Manage Department");
			fc.utobj().printTestStep("Add Department");
			String departmentNameAssign = fc.utobj().generateTestData(dataSet.get("departmentName"));
			departmentPage.addDepartments(driver, departmentNameAssign, corpUserAssign.getuserFullName());
			
			fc.utobj().printTestStep("Navigate To Support > Ticket");
			fc.utobj().printTestStep("Create Ticket");

			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			createTicket(driver, departmentName, description, subject);

			searchTicketBySubject(driver, subject);

			fc.utobj().printTestStep("Assign Owner To Across Department");
			actionImgOption(driver, subject, "Assign Owner");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().clickElement(driver, pobj.radioButton.get(1));
			
			fc.utobj().selectDropDown(driver, pobj.selectDepartmentDropDown, departmentNameAssign);
			fc.utobj().selectDropDown(driver, pobj.selectOwnerAcross, corpUserAssign.getuserFullName());
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.commonMethods().Click_Close_Input_ByValue(driver);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify Assign Owner to Across Department");

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () ,'"+subject+"')]/ancestor::tr/td[contains(text(),'"+corpUserAssign.getuserFullName()+"')]").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify assigned user");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "support","supportnewgui"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Ticket Publish as FAQ At Support Tickets", testCaseId = "TC_35_Publish_FAQ")
	private void publishAsFAQ() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		
		Map<String, String> dataSet = fc.utobj().readTestData("support", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			SupportTicketsPage pobj = new SupportTicketsPage(driver);

			fc.utobj().printTestStep("Navigate to Admin > Support > Manage FAQs");
			fc.utobj().printTestStep("Add New Category");

			AdminSupportManageFAQsPageTest manageFAQPage = new AdminSupportManageFAQsPageTest();
			String categoryName = fc.utobj().generateTestData(dataSet.get("categoryName"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			manageFAQPage.addNewCategory(driver, categoryName, description);

			fc.utobj().printTestStep("Navigate to Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest cu = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			String emailId = "supportautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			cu.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			String userName = corpUser.getuserFullName();

			fc.utobj().printTestStep("Navigate to Admin > Support > Manage Department");
			fc.utobj().printTestStep("Add Department");
			String departmentName = fc.utobj().generateTestData(dataSet.get("departmentName"));
			AdminSupportManageDepartmentPageTest departmentPage = new AdminSupportManageDepartmentPageTest();
			departmentPage.addDepartments(driver, departmentName, userName);

			fc.utobj().printTestStep("Navigate To Support > Ticket");
			fc.utobj().printTestStep("Create Ticket");

			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			description = fc.utobj().generateTestData(dataSet.get("description"));

			createTicket(driver, departmentName, description, subject);
			searchTicketBySubject(driver, subject);

			fc.utobj().printTestStep("Publish As FAQ");
			actionImgOption(driver, subject, "Publish as FAQ");
			fc.utobj().selectDropDown(driver, pobj.categoryName, categoryName);
			String faqQuestion = fc.utobj().generateTestData(dataSet.get("faqQuestion"));
			fc.utobj().sendKeys(driver, pobj.faqQuestion, faqQuestion);
			String faqAnswer = fc.utobj().generateTestData(dataSet.get("faqAnswer"));
			fc.utobj().sendKeys(driver, pobj.faqAnswer, faqAnswer);
			String file = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			fc.utobj().sendKeys(driver, pobj.attacheFile, file);
			fc.utobj().clickElement(driver, pobj.submitBtn);
			fc.utobj().clickElement(driver, pobj.okFaqBtn);
			fc.utobj().printTestStep("Navigate to Support Faqs page");
			fc.support().support_common().supportFAQs(driver);

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text() , '" + categoryName + "')]"));

			fc.utobj().printTestStep("Verify Publish As A FAQ");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, faqQuestion);

			if (isTextPresent == false) {
				fc.utobj().throwsSkipException("was not able to verify publish as FAQ");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "support"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the update Status of Tickets", testCaseId = "TC_36_Update_Status")
	private void updateStatus() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		
		Map<String, String> dataSet = fc.utobj().readTestData("support", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			SupportTicketsPage pobj = new SupportTicketsPage(driver);
			String parentWindow = driver.getWindowHandle();

			fc.utobj().printTestStep("Navigate to Admin > Support > Ticket Status");
			fc.utobj().printTestStep("Add Ticket Status");

			AdminSupportTicketStatusPageTest statusPage = new AdminSupportTicketStatusPageTest();
			String statusName = fc.utobj().generateTestData(dataSet.get("statusName"));
			statusPage.addTicketStatus(driver, statusName);

			fc.utobj().printTestStep("Navigate to Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest cu = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			String emailId = "supportautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			cu.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			String userName = corpUser.getuserFullName();

			fc.utobj().printTestStep("Navigate to Admin > Support > Manage Department");
			fc.utobj().printTestStep("Add Department");
			String departmentName = fc.utobj().generateTestData(dataSet.get("departmentName"));
			AdminSupportManageDepartmentPageTest departmentPage = new AdminSupportManageDepartmentPageTest();
			departmentPage.addDepartments(driver, departmentName, userName);

			fc.utobj().printTestStep("Navigate To Support > Ticket");
			fc.utobj().printTestStep("Create Ticket");

			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			createTicket(driver, departmentName, description, subject);

			searchTicketBySubject(driver, subject);

			fc.utobj().printTestStep("Update Status");
			actionImgOption(driver, subject, "Update Status");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().selectDropDown(driver, pobj.stausDropDown, statusName);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.commonMethods().Click_Close_Input_ByValue(driver);
			driver.switchTo().window(parentWindow);

			fc.utobj().printTestStep("Verify Updated Status");
			String text = fc.utobj().getText(driver, fc.utobj().getElementByXpath(driver,
					".//td[contains(text () ,'" + subject + "')]/following-sibling::td[3]"));
			
			if (!text.equalsIgnoreCase(statusName)) {
				fc.utobj().throwsSkipException("was not able to update status of Ticket");
			}
			fc.utobj().printTestStep("Verify Mail After Changed In Status");
			
			Map<String, String> mailData = new HashMap<String, String>();
			mailData = fc.utobj().readMailBox("Ticket Status Changed", statusName, emailId, "sdg@1a@Hfs");

			if (mailData.size() == 0 || !mailData.get("mailBody").contains("Ticket Status Changed")) {

				fc.utobj().throwsException("was not able to verify Status Changed");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "support" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Get Lock Ticket At Support Ticket Page", testCaseId = "TC_37_Get_Lock")
	private void getLock() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		
		Map<String, String> dataSet = fc.utobj().readTestData("support", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			SupportTicketsPage pobj = new SupportTicketsPage(driver);
			String parentWindow = driver.getWindowHandle();

			String userName = " FranConnect Administrator";

			fc.utobj().printTestStep("Navigate to Admin > Support > Manage Department");
			fc.utobj().printTestStep("Add Department");
			String departmentName = fc.utobj().generateTestData(dataSet.get("departmentName"));
			AdminSupportManageDepartmentPageTest departmentPage = new AdminSupportManageDepartmentPageTest();
			departmentPage.addDepartments(driver, departmentName, userName);

			fc.utobj().printTestStep("Navigate To Support > Ticket");
			fc.utobj().printTestStep("Create Ticket");

			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			createTicket(driver, departmentName, description, subject);

			searchTicketBySubject(driver, subject);

			fc.utobj().printTestStep("Get Lock");
			actionImgOption(driver, subject, "Get Lock");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.okBtn);
			fc.commonMethods().Click_Close_Input_ByValue(driver);
			driver.switchTo().window(parentWindow);

			fc.utobj().printTestStep("Navigate To Locket Ticket Link");

			fc.utobj().moveToElementThroughAction(driver, pobj.moreActionBtn);
			fc.utobj().clickElement(driver, pobj.lockedTicketLink);

			/*
			 * fc.utobj().sendKeys(driver, pobj.supportTopSearch, subject);
			 * fc.utobj().clickElement(driver, pobj.searchImgBtn);
			 */

			filterTicketByDepartment(driver, pobj, departmentName);

			fc.utobj().printTestStep("Verify Get Lock");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, subject);

			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to lock the ticket");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "support")

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify FAQs for Franchise User At Support Tickets Page", testCaseId = "TC_56_Verify_FAQ_FU")
	private void verifyFAQFranchiseUser() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		
		Map<String, String> dataSet = fc.utobj().readTestData("support", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			String parentWindow = driver.getWindowHandle();

			fc.utobj().printTestStep("Navigate to Admin > Support > Manage FAQs");
			fc.utobj().printTestStep("Add New Category");

			AdminSupportManageFAQsPageTest addFAQPage = new AdminSupportManageFAQsPageTest();
			String categoryName = fc.utobj().generateTestData(dataSet.get("categoryName"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			addFAQPage.addNewCategory(driver, categoryName, description);

			fc.utobj().printTestStep("Add FAQ");
			String faqQuestion = fc.utobj().generateTestData(dataSet.get("faqQuestion"));
			String faqAnswer = fc.utobj().generateTestData(dataSet.get("faqAnswer"));
			// File file = new
			// File(config.get("testDataPath").concat("/").concat("document").concat("/")+dataSet.get("uploadFile"));
			addFAQPage.addFAQ(driver, categoryName, faqQuestion, faqAnswer, parentWindow);

			fc.utobj().printTestStep("Navigate to Admin > Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			AdminFranchiseLocationAddFranchiseLocationPageTest addFranLocPage = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String franchiseId = fc.utobj().generateTestData(dataSet.get("franchiseId"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			addFranLocPage.addFranchiseLocation_All(driver, franchiseId, regionName, storeType);

			fc.utobj().printTestStep("Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest addFranUserPage = new AdminUsersManageManageFranchiseUsersPageTest();
			String userName = fc.utobj().generateTestData(dataSet.get("userName"));
			String password = "t0n1ght123";
			String roleName = "Default Franchise Role";
			String emailId = "supportautomation@staffex.com";
			addFranUserPage.addFranchiseUser(driver, userName, password, franchiseId, roleName, emailId);

			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login With Franchise User");
			fc.loginpage().loginWithParameter(driver, userName, password);

			fc.utobj().printTestStep("Navigate To Support FAQs");
			fc.support().support_common().supportFAQs(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text() , '" + categoryName + "')]"));

			fc.utobj().printTestStep("Verify FAQ");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, faqQuestion);
			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to verify FAQs");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "support")

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the FAQs For Corporate User At Support Tickets Page", testCaseId = "TC_57_Verify_FAQ_CU")
	private void verifyFAQCorporateUser() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		
		Map<String, String> dataSet = fc.utobj().readTestData("support", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			String parentWindow = driver.getWindowHandle();

			fc.utobj().printTestStep("Navigate to Admin > Support > Manage FAQs");
			fc.utobj().printTestStep("Add New Category");

			AdminSupportManageFAQsPageTest addFAQPage = new AdminSupportManageFAQsPageTest();
			String categoryName = fc.utobj().generateTestData(dataSet.get("categoryName"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			addFAQPage.addNewCategory(driver, categoryName, description);

			fc.utobj().printTestStep("Add FAQ");
			String faqQuestion = fc.utobj().generateTestData(dataSet.get("faqQuestion"));
			String faqAnswer = fc.utobj().generateTestData(dataSet.get("faqAnswer"));
			addFAQPage.addFAQ(driver, categoryName, faqQuestion, faqAnswer, parentWindow);

			fc.utobj().printTestStep("Navigate to Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest addCorUserPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "supportautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			addCorUserPage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login With Corporate User");
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());

			fc.utobj().printTestStep("Navigate To Support > FAQs Page");
			fc.support().support_common().supportFAQs(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text() , '" + categoryName + "')]"));

			fc.utobj().printTestStep("Verify FAQ");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, faqQuestion);
			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to verify FAQs");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "support", "supportCheck12" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify FAQs For Divisional User At Support Tickets", testCaseId = "TC_58_Verify_FAQ_DU")
	private void verifyFAQDivisionalUser() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		
		Map<String, String> dataSet = fc.utobj().readTestData("support", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			String parentWindow = driver.getWindowHandle();

			fc.utobj().printTestStep("Navigate to Admin > Support > Manage FAQs");
			fc.utobj().printTestStep("Add New Category");

			AdminSupportManageFAQsPageTest addFAQPage = new AdminSupportManageFAQsPageTest();
			String categoryName = fc.utobj().generateTestData(dataSet.get("categoryName"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			addFAQPage.addNewCategory(driver, categoryName, description);

			fc.utobj().printTestStep("Add FAQ");
			String faqQuestion = fc.utobj().generateTestData(dataSet.get("faqQuestion"));
			String faqAnswer = fc.utobj().generateTestData(dataSet.get("faqAnswer"));
			// File file = new
			// File(config.get("testDataPath").concat("/").concat("document").concat("/")+dataSet.get("uploadFile"));
			addFAQPage.addFAQ(driver, categoryName, faqQuestion, faqAnswer, parentWindow);

			fc.utobj().printTestStep("Navigate To Admin > Division > Add Division");
			fc.utobj().printTestStep("Add Division");

			AdminDivisionAddDivisionPageTest addDivPage = new AdminDivisionAddDivisionPageTest();
			String divisionName = fc.utobj().generateTestData(dataSet.get("divisionName"));
			addDivPage.addDivision(driver, divisionName);

			fc.utobj().printTestStep("Navigate to Admin > Users > Manage Divisional User");
			fc.utobj().printTestStep("Add Divisional User");

			AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest addDivUserPage = new AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest();
			String userName = fc.utobj().generateTestData(dataSet.get("userName"));
			String password = "t0n1ght123";
			String emailId = "supportautomation@staffex.com";
			addDivUserPage.addDivisionalUser(driver, userName, password, divisionName, emailId);

			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login With Divisional User");
			fc.loginpage().loginWithParameter(driver, userName, password);

			fc.utobj().printTestStep("Navigate To Support > FAQs Page");

			fc.support().support_common().supportFAQs(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text() , '" + categoryName + "')]"));

			fc.utobj().printTestStep("Verify FAQ");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, faqQuestion);
			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to verify FAQs");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "support")

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify FAQs For Regional User At Support Tickets", testCaseId = "TC_59_Verify_FAQ_RU")
	private void verifyFAQRigionalUser() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		
		Map<String, String> dataSet = fc.utobj().readTestData("support", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			String parentWindow = driver.getWindowHandle();

			fc.utobj().printTestStep("Navigate to Admin > Support > Manage FAQs");
			fc.utobj().printTestStep("Add New Category");

			AdminSupportManageFAQsPageTest addFAQPage = new AdminSupportManageFAQsPageTest();
			String categoryName = fc.utobj().generateTestData(dataSet.get("categoryName"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			addFAQPage.addNewCategory(driver, categoryName, description);

			fc.utobj().printTestStep("Add FAQ");
			String faqQuestion = fc.utobj().generateTestData(dataSet.get("faqQuestion"));
			String faqAnswer = fc.utobj().generateTestData(dataSet.get("faqAnswer"));
			// File file = new
			// File(config.get("testDataPath").concat("/").concat("document").concat("/")+dataSet.get("uploadFile"));
			addFAQPage.addFAQ(driver, categoryName, faqQuestion, faqAnswer, parentWindow);

			fc.utobj().printTestStep("Navigate To Admin > Area/Region > Add Area/Region");
			fc.utobj().printTestStep("Add Area/Region");

			AdminAreaRegionAddAreaRegionPageTest addRegionPage = new AdminAreaRegionAddAreaRegionPageTest();
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			addRegionPage.addAreaRegion(driver, regionName);

			fc.utobj().printTestStep("Navigate to Admin > Users > Manage Regional Users");
			fc.utobj().printTestStep("Add Regional User");

			AdminUsersManageRegionalUsersAddRegionalUserPageTest addRegUserPage = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
			String userName = fc.utobj().generateTestData(dataSet.get("userName"));
			String password = "t0n1ght123";
			String emailId = "supportautomation@staffex.com";
			addRegUserPage.addRegionalUser(driver, userName, password, regionName, emailId);

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With Regional User");
			fc.loginpage().loginWithParameter(driver, userName, password);

			fc.utobj().printTestStep("Navigate To Support FAQs");
			fc.support().support_common().supportFAQs(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text() , '" + categoryName + "')]"));

			fc.utobj().printTestStep("Verify FAQ");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, faqQuestion);
			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to verify FAQs");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "support")

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Ask Corporate DU At Support Tickets [Divisional User]", testCaseId = "TC_64_Verify_Ask_Corporate_DU")
	private void verifyAskCorporateDU() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		
		Map<String, String> dataSet = fc.utobj().readTestData("support", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			SupportTicketsPage pobj = new SupportTicketsPage(driver);

			fc.utobj().printTestStep("Navigate to Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest addCorPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "supportautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			addCorPage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			String password = "t0n1ght123";

			fc.utobj().printTestStep("Navigate to Admin > Support > Manage Department");
			fc.utobj().printTestStep("Add Department");
			AdminSupportManageDepartmentPageTest addDepartmenrPage = new AdminSupportManageDepartmentPageTest();
			String departmentName = fc.utobj().generateTestData(dataSet.get("departmentName"));
			addDepartmenrPage.addDepartments(driver, departmentName, corpUser.getuserFullName());

			fc.utobj().printTestStep("Navigate To Admin > Division > Add Division");
			fc.utobj().printTestStep("Add Division");
			AdminDivisionAddDivisionPageTest addDivPage = new AdminDivisionAddDivisionPageTest();
			String divisionName = fc.utobj().generateTestData(dataSet.get("divisionName"));
			addDivPage.addDivision(driver, divisionName);

			fc.utobj().printTestStep("Navigate to Admin > Users > Manage Divisional Users");
			fc.utobj().printTestStep("Add Divisional User");
			AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest addDivUserPage = new AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest();
			String userName = fc.utobj().generateTestData(dataSet.get("userName"));

			addDivUserPage.addDivisionalUser(driver, userName, password, divisionName, emailId);

			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login With Divisional User");
			fc.loginpage().loginWithParameter(driver, userName, password);

			fc.utobj().printTestStep("Navigate To Support > Ask Corporate");
			fc.support().support_common().supportAskCorporate(driver);

			fc.utobj().printTestStep("Ask Corporate");

			fc.utobj().selectDropDown(driver, pobj.selectDepartmentAskCorporate, departmentName);
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subjectTextBox1, subject);
			fc.utobj().sendKeys(driver, pobj.descriptionTextBoxAskCo, dataSet.get("description"));
			fc.utobj().clickElement(driver, pobj.submitBtnAsk);
			fc.utobj().clickElement(driver, pobj.okBtn);

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With Corporate User");
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());

			fc.utobj().printTestStep("Navigate To Support Ticket Page");
			fc.support().support_common().supportTickets(driver);

			fc.utobj().printTestStep("Verify Ticket");
			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + subject + "')]").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify subject at Corporate Side");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + departmentName + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify departmentName at Corporate Side");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + userName + "')]").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Divisional User[Reported User] at Corporate Side");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "support", "supportLan" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Tickets through Divisional User At Support Tickets [Divisional User]", testCaseId = "TC_65_Verify_Ticket_DU")
	private void verifyTicketDU() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("support", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			SupportTicketsPage pobj = new SupportTicketsPage(driver);

			fc.utobj().printTestStep("Navigate to Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest addCorPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String password = "t0n1ght123";
			String emailId = "supportautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			addCorPage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate to Admin > Support > Manage Department");
			fc.utobj().printTestStep("Add Department");
			AdminSupportManageDepartmentPageTest addDepartmenrPage = new AdminSupportManageDepartmentPageTest();
			String departmentName = fc.utobj().generateTestData(dataSet.get("departmentName"));
			addDepartmenrPage.addDepartments(driver, departmentName, corpUser.getuserFullName());

			fc.utobj().printTestStep("Navigate To Admin > Division > Add Division");
			fc.utobj().printTestStep("Add Division");
			AdminDivisionAddDivisionPageTest addDivPage = new AdminDivisionAddDivisionPageTest();
			String divisionName = fc.utobj().generateTestData(dataSet.get("divisionName"));
			addDivPage.addDivision(driver, divisionName);

			fc.utobj().printTestStep("Navigate to Admin > Users > Manage Divisional Users");
			fc.utobj().printTestStep("Add Divisional User");
			AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest addDivUserPage = new AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest();
			String userName = fc.utobj().generateTestData(dataSet.get("userName"));

			addDivUserPage.addDivisionalUser(driver, userName, password, divisionName, emailId);

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With Divisional User");
			fc.loginpage().loginWithParameter(driver, userName, password);

			fc.utobj().printTestStep("Navigate To Ask Corporate Page");

			fc.support().support_common().supportAskCorporate(driver);
			fc.utobj().printTestStep("Ask Corporate");
			fc.utobj().selectDropDown(driver, pobj.selectDepartmentAskCorporate, departmentName);
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subjectTextBox1, subject);
			fc.utobj().sendKeys(driver, pobj.descriptionTextBoxAskCo, dataSet.get("description"));
			fc.utobj().clickElement(driver, pobj.submitBtnAsk);
			fc.utobj().clickElement(driver, pobj.okBtn);

			fc.utobj().printTestStep("Verify Ticket");
			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + subject + "')]").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify subject at Corporate Side");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + departmentName + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify departmentName at Corporate Side");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + userName + "')]").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Divisional User[Reported User] at Corporate Side");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "support")

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Reported Ticket through Divisional User At Support Tickets", testCaseId = "TC_66_Verify_Reported_Ticket_DU")
	private void verifyReportedTicketsDU() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		
		Map<String, String> dataSet = fc.utobj().readTestData("support", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			SupportTicketsPage pobj = new SupportTicketsPage(driver);

			fc.utobj().printTestStep("Navigate to Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest addCorPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String password = "t0n1ght123";
			String emailId = "supportautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			addCorPage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate to Admin > Support > Manage Department");
			fc.utobj().printTestStep("Add Department");
			AdminSupportManageDepartmentPageTest addDepartmenrPage = new AdminSupportManageDepartmentPageTest();
			String departmentName = fc.utobj().generateTestData(dataSet.get("departmentName"));
			addDepartmenrPage.addDepartments(driver, departmentName, corpUser.getuserFullName());

			fc.utobj().printTestStep("Navigate To Admin > Division > Add Division");
			fc.utobj().printTestStep("Add Division");
			AdminDivisionAddDivisionPageTest addDivPage = new AdminDivisionAddDivisionPageTest();
			String divisionName = fc.utobj().generateTestData(dataSet.get("divisionName"));
			addDivPage.addDivision(driver, divisionName);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Divisional User");
			fc.utobj().printTestStep("Add Divisional User");
			AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest addDivUserPage = new AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest();
			String userName = fc.utobj().generateTestData(dataSet.get("userName"));
			/* String emailId="supportautomation@staffex.com"; */
			addDivUserPage.addDivisionalUser(driver, userName, password, divisionName, emailId);

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With Divisional User");
			fc.loginpage().loginWithParameter(driver, userName, password);

			fc.utobj().printTestStep("Navigate To Support > Ask Corporate Page");
			fc.support().support_common().supportAskCorporate(driver);

			fc.utobj().printTestStep("Ask Corporate");
			fc.utobj().selectDropDown(driver, pobj.selectDepartmentAskCorporate, departmentName);
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subjectTextBox1, subject);
			fc.utobj().sendKeys(driver, pobj.descriptionTextBoxAskCo, dataSet.get("description"));
			fc.utobj().clickElement(driver, pobj.submitBtnAsk);
			fc.utobj().clickElement(driver, pobj.okBtn);

			fc.utobj().printTestStep("Verify Reported Ticket");
			fc.utobj().clickElement(driver, pobj.reportedTickets);

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + subject + "')]").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify subject at Corporate Side");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + departmentName + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify departmentName at Corporate Side");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "support", "supportGhY11232" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Archived Ticket through Divisional User At Support Tickets", testCaseId = "TC_67_Verify_Archived_Ticket_DU")
	private void ArchivedTicketsDU() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		
		Map<String, String> dataSet = fc.utobj().readTestData("support", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			String parentWindow = driver.getWindowHandle();
			SupportTicketsPage pobj = new SupportTicketsPage(driver);

			fc.utobj().printTestStep("Navigate to Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest addCorPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String password = "t0n1ght123";
			String emailId = "supportautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			addCorPage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate to Admin > Support > Manage Department");
			fc.utobj().printTestStep("Add Department");
			AdminSupportManageDepartmentPageTest addDepartmenrPage = new AdminSupportManageDepartmentPageTest();
			String departmentName = fc.utobj().generateTestData(dataSet.get("departmentName"));
			addDepartmenrPage.addDepartments(driver, departmentName, corpUser.getuserFullName());

			fc.utobj().printTestStep("Navigate To Admin > Division > Add Division");
			fc.utobj().printTestStep("Add Division");
			AdminDivisionAddDivisionPageTest addDivPage = new AdminDivisionAddDivisionPageTest();
			String divisionName = fc.utobj().generateTestData(dataSet.get("divisionName"));
			addDivPage.addDivision(driver, divisionName);

			fc.utobj().printTestStep("Navigate to Admin > Users > Manage Divisional Users");
			fc.utobj().printTestStep("Add Divisional User");

			AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest addDivUserPage = new AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest();
			String userName = fc.utobj().generateTestData(dataSet.get("userName"));

			addDivUserPage.addDivisionalUser(driver, userName, password, divisionName, emailId);

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With Divisional User");
			fc.loginpage().loginWithParameter(driver, userName, password);

			fc.utobj().printTestStep("Navigate To Support > Ask Corporate");
			fc.support().support_common().supportAskCorporate(driver);

			fc.utobj().printTestStep("Ask Corporate");
			fc.utobj().selectDropDown(driver, pobj.selectDepartmentAskCorporate, departmentName);
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subjectTextBox1, subject);
			fc.utobj().sendKeys(driver, pobj.descriptionTextBoxAskCo, dataSet.get("description"));
			fc.utobj().clickElement(driver, pobj.submitBtnAsk);
			fc.utobj().clickElement(driver, pobj.okBtn);

			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login With Corporate User");
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());

			fc.utobj().printTestStep("Navigate To Support > Tickets");

			fc.support().support_common().supportTickets(driver);

			/*
			 * fc.utobj().sendKeys(driver, pobj.supportTopSearch, subject);
			 * fc.utobj().clickElement(driver, pobj.searchImgBtn);
			 */

			searchTicketBySubject(driver, subject);

			fc.utobj().printTestStep("Archive Ticket");
			actionImgOption(driver, subject, "Archive");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.okBtn);
			fc.commonMethods().Click_Close_Input_ByValue(driver);
			driver.switchTo().window(parentWindow);

			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login With Divisional User");
			fc.loginpage().loginWithParameter(driver, userName, password);

			fc.utobj().printTestStep("Navigate To Support > Ask Corporate");
			fc.support().support_common().supportAskCorporate(driver);
			fc.utobj().clickElement(driver, pobj.archivedTickets);

			fc.utobj().printTestStep("Verify Archive Ticket");
			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + subject + "')]").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify subject at Corporate Side");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + departmentName + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify departmentName at Corporate Side");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "support")

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Ask Corporate through Regional User At Support Tickets", testCaseId = "TC_68_Verify_Ask_Corporate_RU")
	private void verifyAskCorporateRU() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		
		Map<String, String> dataSet = fc.utobj().readTestData("support", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			SupportTicketsPage pobj = new SupportTicketsPage(driver);

			fc.utobj().printTestStep("Navigate to Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest addCorPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String password = "t0n1ght123";

			String emailId = "supportautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			addCorPage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate to Admin > Support > Manage Department");
			fc.utobj().printTestStep("Add Department");
			AdminSupportManageDepartmentPageTest addDepartmenrPage = new AdminSupportManageDepartmentPageTest();
			String departmentName = fc.utobj().generateTestData(dataSet.get("departmentName"));
			addDepartmenrPage.addDepartments(driver, departmentName, corpUser.getuserFullName());

			fc.utobj().printTestStep("Navigate To Admin > Area/Region > Add Area/Region");
			fc.utobj().printTestStep("Add Area/Region");

			AdminAreaRegionAddAreaRegionPageTest addRegionPage = new AdminAreaRegionAddAreaRegionPageTest();
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			addRegionPage.addAreaRegion(driver, regionName);

			fc.utobj().printTestStep("Navigate to Admin > Users > Manage Regional Users");
			fc.utobj().printTestStep("Add Regional User");

			AdminUsersManageRegionalUsersAddRegionalUserPageTest addRegUserPage = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
			String userName = fc.utobj().generateTestData(dataSet.get("userName"));
			addRegUserPage.addRegionalUser(driver, userName, password, regionName, emailId);

			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login With Regional User");
			fc.loginpage().loginWithParameter(driver, userName, password);

			fc.utobj().printTestStep("Ask Corporate");
			fc.support().support_common().supportAskCorporate(driver);
			fc.utobj().selectDropDown(driver, pobj.selectDepartmentAskCorporate, departmentName);
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subjectTextBox1, subject);
			fc.utobj().sendKeys(driver, pobj.descriptionTextBoxAskCo, dataSet.get("description"));
			fc.utobj().clickElement(driver, pobj.submitBtnAsk);
			fc.utobj().clickElement(driver, pobj.okBtn);

			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login With Corporate User");
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());

			fc.utobj().printTestStep("Navigate To Support > Ticket");
			fc.support().support_common().supportTickets(driver);

			fc.utobj().printTestStep("Verify Ask Corporate");
			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + subject + "')]").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify subject at Corporate Side");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + departmentName + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify departmentName at Corporate Side");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + userName + "')]").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Divisional User[Reported User] at Corporate Side");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + corpUser.getFirstName() + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Divisional User[Reported User] at Corporate Side");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "support")

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Tickets Through Regional User At Support Tickets", testCaseId = "TC_69_Verify_Ticket_RU")
	private void verifyTicketsRU() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		
		Map<String, String> dataSet = fc.utobj().readTestData("support", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			SupportTicketsPage pobj = new SupportTicketsPage(driver);

			fc.utobj().printTestStep("Navigate to Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest addCorPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String password = "t0n1ght123";
			String emailId = "supportautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			addCorPage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate to Admin > Support > Manage Department");
			fc.utobj().printTestStep("Add Department");
			AdminSupportManageDepartmentPageTest addDepartmenrPage = new AdminSupportManageDepartmentPageTest();
			String departmentName = fc.utobj().generateTestData(dataSet.get("departmentName"));
			addDepartmenrPage.addDepartments(driver, departmentName, corpUser.getuserFullName());

			fc.utobj().printTestStep("Navigate To Admin > Area/Region > Add Area/Region");
			fc.utobj().printTestStep("Add Area/Region");

			AdminAreaRegionAddAreaRegionPageTest addRegionPage = new AdminAreaRegionAddAreaRegionPageTest();
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			addRegionPage.addAreaRegion(driver, regionName);

			fc.utobj().printTestStep("Navigate to Admin > Users > Manage Regional Users");
			fc.utobj().printTestStep("Add Regional User");

			AdminUsersManageRegionalUsersAddRegionalUserPageTest addRegUserPage = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
			String userName = fc.utobj().generateTestData(dataSet.get("userName"));

			addRegUserPage.addRegionalUser(driver, userName, password, regionName, emailId);

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With Regional User");
			fc.loginpage().loginWithParameter(driver, userName, password);

			fc.utobj().printTestStep("Navigate To Support > Ask Corporate");
			fc.support().support_common().supportAskCorporate(driver);
			fc.utobj().printTestStep("Ask Corporate");
			fc.utobj().selectDropDown(driver, pobj.selectDepartmentAskCorporate, departmentName);
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subjectTextBox1, subject);
			fc.utobj().sendKeys(driver, pobj.descriptionTextBoxAskCo, dataSet.get("description"));
			fc.utobj().clickElement(driver, pobj.submitBtnAsk);
			fc.utobj().clickElement(driver, pobj.okBtn);

			fc.utobj().printTestStep("Verify Ask Corporate");
			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + subject + "')]").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify subject at Corporate Side");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + departmentName + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify departmentName at Corporate Side");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + userName + "')]").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Divisional User[Reported User] at Corporate Side");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "support")

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Reported Ticket through Regional User At Support Tickets", testCaseId = "TC_70_Verify_Reported_Ticket_RU")
	private void verifyReportedTicketsRU() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		
		Map<String, String> dataSet = fc.utobj().readTestData("support", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			SupportTicketsPage pobj = new SupportTicketsPage(driver);

			fc.utobj().printTestStep("Navigate to Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest addCorPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String password = "t0n1ght123";
			String emailId = "supportautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			addCorPage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate to Admin > Support > Manage Department");
			fc.utobj().printTestStep("Add Department");
			AdminSupportManageDepartmentPageTest addDepartmenrPage = new AdminSupportManageDepartmentPageTest();
			String departmentName = fc.utobj().generateTestData(dataSet.get("departmentName"));
			addDepartmenrPage.addDepartments(driver, departmentName, corpUser.getuserFullName());

			fc.utobj().printTestStep("Navigate To Admin > Area/Region > Add Area/Region");
			fc.utobj().printTestStep("Add Area/Region");

			AdminAreaRegionAddAreaRegionPageTest addRegionPage = new AdminAreaRegionAddAreaRegionPageTest();
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			addRegionPage.addAreaRegion(driver, regionName);

			fc.utobj().printTestStep("Navigate to Admin > Users > Manage Regional Users");
			fc.utobj().printTestStep("Add Regional User");

			AdminUsersManageRegionalUsersAddRegionalUserPageTest addRegUserPage = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
			String userName = fc.utobj().generateTestData(dataSet.get("userName"));

			addRegUserPage.addRegionalUser(driver, userName, password, regionName, emailId);

			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login With Regional User");
			fc.loginpage().loginWithParameter(driver, userName, password);

			fc.utobj().printTestStep("Navigate To Support > Ask Corporate");
			fc.support().support_common().supportAskCorporate(driver);
			fc.utobj().printTestStep("Ask Corporate");
			fc.utobj().selectDropDown(driver, pobj.selectDepartmentAskCorporate, departmentName);
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subjectTextBox1, subject);
			fc.utobj().sendKeys(driver, pobj.descriptionTextBoxAskCo, dataSet.get("description"));
			fc.utobj().clickElement(driver, pobj.submitBtnAsk);
			fc.utobj().clickElement(driver, pobj.okBtn);

			fc.utobj().printTestStep("Navigate To Reported Ticket");
			fc.utobj().clickElement(driver, pobj.reportedTickets);

			fc.utobj().printTestStep("Verify Ask Corporate");
			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + subject + "')]").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify subject at Corporate Side");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + departmentName + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify departmentName at Corporate Side");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "support","supportfailed1101"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Archived Ticket through Regional User At Support Tickets", testCaseId = "TC_71_Archived_Ticket_RU")
	private void ArchivedTicketsRU() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		
		Map<String, String> dataSet = fc.utobj().readTestData("support", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			String parentWindow = driver.getWindowHandle();
			SupportTicketsPage pobj = new SupportTicketsPage(driver);

			fc.utobj().printTestStep("Navigate to Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest addCorPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String password = "t0n1ght123";
			String emailId = "supportautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			addCorPage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate to Admin > Support > Manage Department");
			fc.utobj().printTestStep("Add Department");
			AdminSupportManageDepartmentPageTest addDepartmenrPage = new AdminSupportManageDepartmentPageTest();
			String departmentName = fc.utobj().generateTestData(dataSet.get("departmentName"));
			addDepartmenrPage.addDepartments(driver, departmentName, corpUser.getuserFullName());

			fc.utobj().printTestStep("Navigate To Admin > Area/Region > Add Area/Region");
			fc.utobj().printTestStep("Add Area/Region");

			AdminAreaRegionAddAreaRegionPageTest addRegionPage = new AdminAreaRegionAddAreaRegionPageTest();
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			addRegionPage.addAreaRegion(driver, regionName);

			fc.utobj().printTestStep("Navigate to Admin > Users > Manage Regional Users");
			fc.utobj().printTestStep("Add Regional User");

			AdminUsersManageRegionalUsersAddRegionalUserPageTest addRegUserPage = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
			String userName = fc.utobj().generateTestData(dataSet.get("userName"));
			addRegUserPage.addRegionalUser(driver, userName, password, regionName, emailId);

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With Regional User");
			fc.loginpage().loginWithParameter(driver, userName, password);

			fc.utobj().printTestStep("Navigate To Support > Ask Corporate");

			fc.utobj().printTestStep("Ask COrporate");
			fc.support().support_common().supportAskCorporate(driver);
			fc.utobj().selectDropDown(driver, pobj.selectDepartmentAskCorporate, departmentName);
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subjectTextBox1, subject);
			fc.utobj().sendKeys(driver, pobj.descriptionTextBoxAskCo, dataSet.get("description"));
			fc.utobj().clickElement(driver, pobj.submitBtnAsk);
			fc.utobj().clickElement(driver, pobj.okBtn);

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With Corporate User");
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());

			fc.utobj().printTestStep("Navigate To Support Ticket");
			fc.support().support_common().supportTickets(driver);

			/*
			 * fc.utobj().sendKeys(driver, pobj.supportTopSearch, subject);
			 * fc.utobj().clickElement(driver, pobj.searchImgBtn);
			 */

			searchTicketBySubject(driver, subject);

			fc.utobj().printTestStep("Archive Ticket");
			actionImgOption(driver, subject, "Archive");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.okBtn);
			fc.commonMethods().Click_Close_Input_ByValue(driver);
			driver.switchTo().window(parentWindow);

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With Regional User");
			fc.loginpage().loginWithParameter(driver, userName, password);

			fc.utobj().printTestStep("Navigate to Support > Ask Corporate");
			fc.support().support_common().supportAskCorporate(driver);

			fc.utobj().printTestStep("Verify Archive Ticket");
			fc.utobj().clickElement(driver, pobj.archivedTickets);

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + subject + "')]").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify subject at Corporate Side");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + departmentName + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify departmentName at Corporate Side");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "support" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify FAQs Search For Franchise User At Support FAQs Search", testCaseId = "TC_72_Verify_Search_FAQ_FU")
	private void verifySearchFAQFranchiseUser() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		
		Map<String, String> dataSet = fc.utobj().readTestData("support", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			String parentWindow = driver.getWindowHandle();

			fc.utobj().printTestStep("Navigate To Admin > Support > Manage FAQs");
			fc.utobj().printTestStep("Add New Category");

			AdminSupportManageFAQsPageTest addFAQPage = new AdminSupportManageFAQsPageTest();
			String categoryName = fc.utobj().generateTestData(dataSet.get("categoryName"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			addFAQPage.addNewCategory(driver, categoryName, description);

			fc.utobj().printTestStep("Add FAQ");
			String faqQuestion = fc.utobj().generateTestData(dataSet.get("faqQuestion"));
			String faqAnswer = fc.utobj().generateTestData(dataSet.get("faqAnswer"));
			addFAQPage.addFAQ(driver, categoryName, faqQuestion, faqAnswer, parentWindow);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			AdminFranchiseLocationAddFranchiseLocationPageTest addFranLocPage = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String franchiseId = fc.utobj().generateTestData(dataSet.get("franchiseId"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			addFranLocPage.addFranchiseLocation_All(driver, franchiseId, regionName, storeType);

			fc.utobj().printTestStep("Add Franchise User");

			AdminUsersManageManageFranchiseUsersPageTest addFranUserPage = new AdminUsersManageManageFranchiseUsersPageTest();
			String userName = fc.utobj().generateTestData(dataSet.get("userName"));
			String password = "t0n1ght123";
			String roleName = "Default Franchise Role";
			String emailId = "supportautomation@staffex.com";
			addFranUserPage.addFranchiseUser(driver, userName, password, franchiseId, roleName, emailId);

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With Franchise User");
			fc.loginpage().loginWithParameter(driver, userName, password);

			fc.utobj().printTestStep("Navigate To Support > FAQs");

			fc.support().support_common().supportFAQs(driver);

			fc.utobj().printTestStep("Search FAQ");

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchFAQTxBx, faqQuestion);
			 * fc.utobj().clickElement(driver, pobj.searchBtn);
			 */

			fc.utobj().printTestStep("Verify FAQ");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, faqQuestion);
			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to verify FAQs Search");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "support" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify FAQs Search through Divisional User At Support FAQs Search", testCaseId = "TC_74_Verify_Search_FAQ_DU")
	private void verifySearchFAQDivisionalUser() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		
		Map<String, String> dataSet = fc.utobj().readTestData("support", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			String parentWindow = driver.getWindowHandle();

			fc.utobj().printTestStep("Navigate To Admin > Support > Manage FAQs");
			fc.utobj().printTestStep("Add New Category");

			AdminSupportManageFAQsPageTest addFAQPage = new AdminSupportManageFAQsPageTest();
			String categoryName = fc.utobj().generateTestData(dataSet.get("categoryName"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			addFAQPage.addNewCategory(driver, categoryName, description);

			fc.utobj().printTestStep("Add FAQ");
			String faqQuestion = fc.utobj().generateTestData(dataSet.get("faqQuestion"));
			String faqAnswer = fc.utobj().generateTestData(dataSet.get("faqAnswer"));
			// File file = new
			// File(config.get("testDataPath").concat("/").concat("document").concat("/")+dataSet.get("uploadFile"));
			addFAQPage.addFAQ(driver, categoryName, faqQuestion, faqAnswer, parentWindow);

			fc.utobj().printTestStep("Navigate To Admin > Division > Add Division");
			fc.utobj().printTestStep("Add Division");
			AdminDivisionAddDivisionPageTest addDivPage = new AdminDivisionAddDivisionPageTest();
			String divisionName = fc.utobj().generateTestData(dataSet.get("divisionName"));
			addDivPage.addDivision(driver, divisionName);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Divisional User");
			fc.utobj().printTestStep("Add Divisional User");

			AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest addDivUserPage = new AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest();
			String userName = fc.utobj().generateTestData(dataSet.get("userName"));
			String password = "t0n1ght123";
			String emailId = "supportautomation@staffex.com";
			addDivUserPage.addDivisionalUser(driver, userName, password, divisionName, emailId);
			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login With Divisional User");
			fc.loginpage().loginWithParameter(driver, userName, password);

			fc.utobj().printTestStep("Navigate To Support > FAQs");

			fc.support().support_common().supportFAQs(driver);

			fc.utobj().printTestStep("Search FAQ");

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchFAQTxBx, faqQuestion);
			 * fc.utobj().clickElement(driver, pobj.searchBtn);
			 */

			fc.utobj().printTestStep("Verify FAQ");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, faqQuestion);
			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to verify FAQs Search");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "support" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify FAQs Search through Regional User At Support FAQs Search", testCaseId = "TC_76_Verify_Search_FAQ_RU")
	private void verifySearchFAQRegionalUser() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		
		Map<String, String> dataSet = fc.utobj().readTestData("support", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			String parentWindow = driver.getWindowHandle();

			fc.utobj().printTestStep("Navigate To Admin > Support > Manage FAQs");
			fc.utobj().printTestStep("Add New Category");

			AdminSupportManageFAQsPageTest addFAQPage = new AdminSupportManageFAQsPageTest();
			String categoryName = fc.utobj().generateTestData(dataSet.get("categoryName"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			addFAQPage.addNewCategory(driver, categoryName, description);

			fc.utobj().printTestStep("Add FAQ");
			String faqQuestion = fc.utobj().generateTestData(dataSet.get("faqQuestion"));
			String faqAnswer = fc.utobj().generateTestData(dataSet.get("faqAnswer"));
			// File file = new
			// File(config.get("testDataPath").concat("/").concat("document").concat("/")+dataSet.get("uploadFile"));
			addFAQPage.addFAQ(driver, categoryName, faqQuestion, faqAnswer, parentWindow);

			fc.utobj().printTestStep("Navigate To Admin > Area/Region > Add Area/Region");
			fc.utobj().printTestStep("Add Area/Region");

			AdminAreaRegionAddAreaRegionPageTest addRegionPage = new AdminAreaRegionAddAreaRegionPageTest();
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			addRegionPage.addAreaRegion(driver, regionName);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Regional User");
			fc.utobj().printTestStep("Add Regional User");

			AdminUsersManageRegionalUsersAddRegionalUserPageTest addRegUserPage = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
			String userName = fc.utobj().generateTestData(dataSet.get("userName"));
			String password = "t0n1ght123";
			String emailId = "supportautomation@staffex.com";
			addRegUserPage.addRegionalUser(driver, userName, password, regionName, emailId);

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With Regional USer");
			fc.loginpage().loginWithParameter(driver, userName, password);

			fc.utobj().printTestStep("Navigate To Support > FAQs");
			fc.support().support_common().supportFAQs(driver);

			fc.utobj().printTestStep("Search FAQ");

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchFAQTxBx, faqQuestion);
			 * fc.utobj().clickElement(driver, pobj.searchBtn);
			 */

			fc.utobj().printTestStep("Verify Search FAQ");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, faqQuestion);
			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to verify FAQs Search");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "support" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Ticket Search through Franchise User At Support Tickets", testCaseId = "TC_73_Verify_Search_Ticket_FU")
	private void verifySearchTicketsFU() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		
		Map<String, String> dataSet = fc.utobj().readTestData("support", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);


			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");
			AdminFranchiseLocationAddFranchiseLocationPageTest addFranLocPage = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String franchiseId = fc.utobj().generateTestData(dataSet.get("franchiseId"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType")).concat("_73");
			addFranLocPage.addFranchiseLocation_All(driver, franchiseId, regionName, storeType);

			fc.utobj().printTestStep("Add Franchise User");

			AdminUsersManageManageFranchiseUsersPageTest addFranUserPage = new AdminUsersManageManageFranchiseUsersPageTest();
			String userName = fc.utobj().generateTestData(dataSet.get("userName"));
			String password = "t0n1ght123";
			String roleName = "Default Franchise Role";
			String emailId = "supportautomation@staffex.com";
			addFranUserPage.addFranchiseUser(driver, userName, password, franchiseId, roleName, emailId);

			fc.utobj().printTestStep("Navigate to Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest addCorPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			addCorPage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate to Admin > Support > Manage Department");
			fc.utobj().printTestStep("Add Department");
			AdminSupportManageDepartmentPageTest addDeparmentaPage = new AdminSupportManageDepartmentPageTest();
			String departmentName = fc.utobj().generateTestData(dataSet.get("departmentName"));
			addDeparmentaPage.addDepartments(driver, departmentName, corpUser.getuserFullName());

			fc.utobj().printTestStep("Create Ticket");
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			createTicketWithFranchiseId(driver, departmentName, franchiseId, description, subject);
			searchTicketBySubject(driver, subject);
			String ticketId = fc.utobj().getText(driver, fc.utobj().getElementByXpath(driver,
					".//*[contains(text () , '" + subject + "')]/ancestor::tr/td/a"));

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With Franchise User");
			fc.loginpage().loginWithParameter(driver, userName, password);

			fc.support().support_common().supportAskCorporate(driver);
			fc.utobj().printTestStep("Verify Search Ticket");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, ticketId);
			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to verify Ticket Search");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "support" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Ticket Search through Divisional User At Support Tickets", testCaseId = "TC_75_Verify_Search_Ticket_DU")
	private void verifySearchTicketsDU() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		
		Map<String, String> dataSet = fc.utobj().readTestData("support", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			SupportTicketsPage pobj = new SupportTicketsPage(driver);
			

			fc.utobj().printTestStep("Navigate to Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest addCorPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String password = "t0n1ght123";
			String emailId = "supportautomation@staffex.com";

			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			addCorPage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate to Admin > Support > Manage Department");
			fc.utobj().printTestStep("Add Department");
			AdminSupportManageDepartmentPageTest addDepartmenrPage = new AdminSupportManageDepartmentPageTest();
			String departmentName = fc.utobj().generateTestData(dataSet.get("departmentName"));
			addDepartmenrPage.addDepartments(driver, departmentName, corpUser.getuserFullName());

			fc.utobj().printTestStep("Navigate To Admin > Division > Add Division");
			fc.utobj().printTestStep("Add Division");
			AdminDivisionAddDivisionPageTest addDivPage = new AdminDivisionAddDivisionPageTest();
			String divisionName = fc.utobj().generateTestData(dataSet.get("divisionName"));
			addDivPage.addDivision(driver, divisionName);

			fc.utobj().printTestStep("Navigate To Admin > User > Manage Divisional User");
			fc.utobj().printTestStep("Add Divisional User");
			AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest addDivUserPage = new AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest();
			String userName = fc.utobj().generateTestData(dataSet.get("userName"));
			addDivUserPage.addDivisionalUser(driver, userName, password, divisionName, emailId);

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With Divisional User");
			fc.loginpage().loginWithParameter(driver, userName, password);

			fc.utobj().printTestStep("Ask Corporate");
			fc.support().support_common().supportAskCorporate(driver);
			fc.utobj().selectDropDown(driver, pobj.selectDepartmentAskCorporate, departmentName);
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subjectTextBox1, subject);
			fc.utobj().sendKeys(driver, pobj.descriptionTextBoxAskCo, dataSet.get("description"));
			fc.utobj().clickElement(driver, pobj.submitBtnAsk);
			fc.utobj().clickElement(driver, pobj.okBtn);

			String ticketId = fc.utobj().getText(driver, fc.utobj().getElementByXpath(driver,
					".//*[contains(text () ,'" + subject + "')]/ancestor::tr/td/a"));

			fc.utobj().printTestStep("Search Ticket");
			searchTicketBySubject(driver, subject);

			fc.utobj().printTestStep("Verify Search Ticket");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, ticketId);

			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to search Ticket");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "support" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Ticket Search through Regional User At Support Tickets", testCaseId = "TC_77_Verify_Search_Ticket_RU")
	private void verifySearchTicketsRU() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		
		Map<String, String> dataSet = fc.utobj().readTestData("support", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			AdminFranchiseLocationAddFranchiseLocationPageTest addFranLocPage = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String franchiseId = fc.utobj().generateTestData(dataSet.get("franchiseId"));
			String regionName = fc.utobj().generateTestData(dataSet.get("Test77regionName"));
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			addFranLocPage.addFranchiseLocation_All(driver, franchiseId, regionName, storeType);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Regional User");
			fc.utobj().printTestStep("Add Regional User");

			AdminUsersManageRegionalUsersAddRegionalUserPageTest addRegUserPage = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
			String userName = fc.utobj().generateTestData(dataSet.get("userName"));
			String password = "t0n1ght123";
			String emailId = "supportautomation@staffex.com";
			addRegUserPage.addRegionalUser(driver, userName, password, regionName, emailId);

			fc.utobj().printTestStep("Navigate to Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest addCorPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			addCorPage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate to Admin > Support > Manage Department");
			fc.utobj().printTestStep("Add Department");
			AdminSupportManageDepartmentPageTest addDeparmentaPage = new AdminSupportManageDepartmentPageTest();
			String departmentName = fc.utobj().generateTestData(dataSet.get("departmentName"));
			addDeparmentaPage.addDepartments(driver, departmentName, corpUser.getuserFullName());

			fc.utobj().printTestStep("Create Ticket");
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			// File file = new
			// File(config.get("testDataPath").concat("/").concat("document").concat("/")+dataSet.get("uploadFile"));
			createTicketWithFranchiseId(driver, departmentName, franchiseId, description, subject);

			/*
			 * fc.utobj().sendKeys(driver, pobj.supportTopSearch, subject);
			 * fc.utobj().clickElement(driver, pobj.searchImgBtn);
			 */

			searchTicketBySubject(driver, subject);

			String ticketId = fc.utobj().getText(driver, fc.utobj().getElementByXpath(driver,
					".//*[contains(text () , '" + subject + "')]/ancestor::tr/td/a"));
			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login With Regional User");
			fc.loginpage().loginWithParameter(driver, userName, password);

			fc.utobj().printTestStep("Search Ticket");
			fc.support().support_common().supportAskCorporate(driver);

			/*
			 * fc.utobj().sendKeys(driver, pobj.supportTopSearch, ticketId);
			 * fc.utobj().clickElement(driver, pobj.searchImgBtn);
			 */

			// fc.utobj().clickElement(driver, pobj.ticketTab);

			fc.utobj().printTestStep("Verify Search Ticket");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, ticketId);
			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to verify Ticket Search");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "support")

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Ask Corporate through Franchise User At Support Tickets ", testCaseId = "TC_78_Verify_Ask_Corporate_FU")
	private void verifyAskCorporateFU() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		
		Map<String, String> dataSet = fc.utobj().readTestData("support", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			
			SupportTicketsPage pobj = new SupportTicketsPage(driver);

			fc.utobj().printTestStep("Navigate to Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest addCorPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String password = "t0n1ght123";

			String emailId = "supportautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			addCorPage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate to Admin > Support > Manage Department");
			fc.utobj().printTestStep("Add Department");
			AdminSupportManageDepartmentPageTest addDepartmenrPage = new AdminSupportManageDepartmentPageTest();
			String departmentName = fc.utobj().generateTestData(dataSet.get("departmentName"));
			addDepartmenrPage.addDepartments(driver, departmentName, corpUser.getuserFullName());

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			AdminFranchiseLocationAddFranchiseLocationPageTest addFranLocPage = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String franchiseId = fc.utobj().generateTestData(dataSet.get("franchiseId"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			addFranLocPage.addFranchiseLocation_All(driver, franchiseId, regionName, storeType);

			fc.utobj().printTestStep("Add Franchise User");

			AdminUsersManageManageFranchiseUsersPageTest addFranUserPage = new AdminUsersManageManageFranchiseUsersPageTest();
			String userName = fc.utobj().generateTestData(dataSet.get("userName"));
			String roleName = "Default Franchise Role";

			addFranUserPage.addFranchiseUser(driver, userName, password, franchiseId, roleName, emailId);

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With Franchise User");
			fc.loginpage().loginWithParameter(driver, userName, password);

			fc.utobj().printTestStep("Navigate To Support > Ask Corporate");

			fc.utobj().printTestStep("Ask Corporate");
			fc.support().support_common().supportAskCorporate(driver);
			fc.utobj().selectDropDown(driver, pobj.selectDepartmentAskCorporate, departmentName);
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subjectTextBox1, subject);
			fc.utobj().sendKeys(driver, pobj.descriptionTextBoxAskCo, dataSet.get("description"));
			fc.utobj().clickElement(driver, pobj.submitBtnAsk);
			fc.utobj().clickElement(driver, pobj.okBtn);
			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login With Corporate User");
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());

			fc.utobj().printTestStep("Navigate To Support > Ticket");

			fc.utobj().printTestStep("Verify Ask Corporate");
			fc.support().support_common().supportTickets(driver);

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + subject + "')]").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify subject at Corporate Side");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + departmentName + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify departmentName at Corporate Side");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "support")

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Tickets through Franchise User At Support Franchise User", testCaseId = "TC_79_Verify_Ticket_FU")
	private void verifyTicketsFU() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		
		Map<String, String> dataSet = fc.utobj().readTestData("support", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			SupportTicketsPage pobj = new SupportTicketsPage(driver);

			fc.utobj().printTestStep("Navigate to Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest addCorPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String password = "t0n1ght123";
			/*
			 * String
			 * userName1=fc.utobj().generateTestData(dataSet.get("userNameCor"))
			 * ; String password="t0n1ght123";
			 * userName1=addCorPage.addCorporateUser(driver, userName1,
			 * password, config);
			 */

			String emailId = "supportautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			addCorPage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate to Admin > Support > Manage Department");
			fc.utobj().printTestStep("Add Department");
			AdminSupportManageDepartmentPageTest addDepartmenrPage = new AdminSupportManageDepartmentPageTest();
			String departmentName = fc.utobj().generateTestData(dataSet.get("departmentName"));
			addDepartmenrPage.addDepartments(driver, departmentName, corpUser.getuserFullName());

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			AdminFranchiseLocationAddFranchiseLocationPageTest addFranLocPage = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String franchiseId = fc.utobj().generateTestData(dataSet.get("franchiseId"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			addFranLocPage.addFranchiseLocation_All(driver, franchiseId, regionName, storeType);

			fc.utobj().printTestStep("Add Franchise User");

			AdminUsersManageManageFranchiseUsersPageTest addFranUserPage = new AdminUsersManageManageFranchiseUsersPageTest();
			String userName = fc.utobj().generateTestData(dataSet.get("userName"));
			String roleName = "Default Franchise Role";

			addFranUserPage.addFranchiseUser(driver, userName, password, franchiseId, roleName, emailId);

			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login With Franchise User");
			fc.loginpage().loginWithParameter(driver, userName, password);

			fc.utobj().printTestStep("Navigate To Support > Ask Corporate");

			fc.utobj().printTestStep("Ask Corporate");
			fc.support().support_common().supportAskCorporate(driver);
			fc.utobj().selectDropDown(driver, pobj.selectDepartmentAskCorporate, departmentName);
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subjectTextBox1, subject);
			fc.utobj().sendKeys(driver, pobj.descriptionTextBoxAskCo, dataSet.get("description"));
			fc.utobj().clickElement(driver, pobj.submitBtnAsk);
			fc.utobj().clickElement(driver, pobj.okBtn);

			fc.utobj().printTestStep("Verify Ask Corporate");
			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + subject + "')]").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify subject at Corporate Side");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + departmentName + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify departmentName at Corporate Side");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "support")

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Reported Ticket Franchise User At Support Tickets", testCaseId = "TC_80_Verify_Reported_Ticket_FU")
	private void verifyReportedTicketsFU() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		
		Map<String, String> dataSet = fc.utobj().readTestData("support", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			SupportTicketsPage pobj = new SupportTicketsPage(driver);

			fc.utobj().printTestStep("Navigate to Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest addCorPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String password = "t0n1ght123";
			/*
			 * String
			 * userName1=fc.utobj().generateTestData(dataSet.get("userNameCor"))
			 * ; String password="t0n1ght123";
			 * userName1=addCorPage.addCorporateUser(driver, userName1,
			 * password, config);
			 */
			String emailId = "supportautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			addCorPage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate to Admin > Support > Manage Department");
			fc.utobj().printTestStep("Add Department");
			AdminSupportManageDepartmentPageTest addDepartmenrPage = new AdminSupportManageDepartmentPageTest();
			String departmentName = fc.utobj().generateTestData(dataSet.get("departmentName"));
			addDepartmenrPage.addDepartments(driver, departmentName, corpUser.getuserFullName());

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			AdminFranchiseLocationAddFranchiseLocationPageTest addFranLocPage = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String franchiseId = fc.utobj().generateTestData(dataSet.get("franchiseId"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			addFranLocPage.addFranchiseLocation_All(driver, franchiseId, regionName, storeType);

			fc.utobj().printTestStep("Add Franchise User");

			AdminUsersManageManageFranchiseUsersPageTest addFranUserPage = new AdminUsersManageManageFranchiseUsersPageTest();
			String userName = fc.utobj().generateTestData(dataSet.get("userName"));
			String roleName = "Default Franchise Role";
			addFranUserPage.addFranchiseUser(driver, userName, password, franchiseId, roleName, emailId);
			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login WIth Franchise User");
			fc.loginpage().loginWithParameter(driver, userName, password);

			fc.utobj().printTestStep("Navigate To Support > Ask Corporate Page");

			fc.utobj().printTestStep("Ask Corporate");
			fc.support().support_common().supportAskCorporate(driver);
			fc.utobj().selectDropDown(driver, pobj.selectDepartmentAskCorporate, departmentName);
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subjectTextBox1, subject);
			fc.utobj().sendKeys(driver, pobj.descriptionTextBoxAskCo, dataSet.get("description"));
			fc.utobj().clickElement(driver, pobj.submitBtnAsk);
			fc.utobj().clickElement(driver, pobj.okBtn);

			fc.utobj().printTestStep("Verify Reported Ticket");
			fc.utobj().clickElement(driver, pobj.reportedTickets);

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + subject + "')]").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify subject at Corporate Side");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + departmentName + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify departmentName at Corporate Side");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "support" ,"supportfailed1101"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Archived Ticket through Franchise User At Support Ticket Page", testCaseId = "TC_81_Archived_Ticket_FU")
	private void ArchivedTicketsFU() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		
		Map<String, String> dataSet = fc.utobj().readTestData("support", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			String parentWindow = driver.getWindowHandle();
			SupportTicketsPage pobj = new SupportTicketsPage(driver);

			fc.utobj().printTestStep("Navigate to Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest addCorPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String password = "t0n1ght123";

			String emailId = "supportautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			addCorPage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate to Admin > Support > Manage Department");
			fc.utobj().printTestStep("Add Department");
			AdminSupportManageDepartmentPageTest addDepartmenrPage = new AdminSupportManageDepartmentPageTest();
			String departmentName = fc.utobj().generateTestData(dataSet.get("departmentName"));
			addDepartmenrPage.addDepartments(driver, departmentName, corpUser.getuserFullName());

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			AdminFranchiseLocationAddFranchiseLocationPageTest addFranLocPage = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String franchiseId = fc.utobj().generateTestData(dataSet.get("franchiseId"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			addFranLocPage.addFranchiseLocation_All(driver, franchiseId, regionName, storeType);

			fc.utobj().printTestStep("Add Franchise User");

			AdminUsersManageManageFranchiseUsersPageTest addFranUserPage = new AdminUsersManageManageFranchiseUsersPageTest();
			String userName = fc.utobj().generateTestData(dataSet.get("userName"));
			String roleName = "Default Franchise Role";
			addFranUserPage.addFranchiseUser(driver, userName, password, franchiseId, roleName, emailId);

			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login With Franchise User");
			fc.loginpage().loginWithParameter(driver, userName, password);

			fc.utobj().printTestStep("Navigate To Support > Ask Corporate");

			fc.utobj().printTestStep("Ask Corporate");
			fc.support().support_common().supportAskCorporate(driver);
			fc.utobj().selectDropDown(driver, pobj.selectDepartmentAskCorporate, departmentName);
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subjectTextBox1, subject);
			fc.utobj().sendKeys(driver, pobj.descriptionTextBoxAskCo, dataSet.get("description"));
			fc.utobj().clickElement(driver, pobj.submitBtnAsk);
			fc.utobj().clickElement(driver, pobj.okBtn);

			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login With Corporate User");
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());

			fc.utobj().printTestStep("Navigate To Support > Ticket");
			fc.support().support_common().supportTickets(driver);

			/*
			 * fc.utobj().sendKeys(driver, pobj.supportTopSearch, subject);
			 * fc.utobj().clickElement(driver, pobj.searchImgBtn);
			 */

			searchTicketBySubject(driver, subject);

			fc.utobj().printTestStep("Archive Ticket");
			actionImgOption(driver, subject, "Archive");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.okBtn);
			fc.commonMethods().Click_Close_Input_ByValue(driver);
			driver.switchTo().window(parentWindow);
			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login With Franchise User");
			fc.loginpage().loginWithParameter(driver, userName, password);

			fc.utobj().printTestStep("Navigate To Support > Ask Corporate");

			fc.utobj().printTestStep("Verify Archive Ticket");
			fc.support().support_common().supportAskCorporate(driver);
			fc.utobj().clickElement(driver, pobj.archivedTickets);

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + subject + "')]").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify subject at Corporate Side");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + departmentName + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify departmentName at Corporate Side");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "support", "supportsmoke", "supportemail","sprt02013"})

	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-30", testCaseDescription = "Verify The Post Message", testCaseId = "TC_Support_Post_Message")
	private void postMessage() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("support", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			SupportTicketsPage pobj = new SupportTicketsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Support > Manage Message Templates");
			fc.utobj().printTestStep("Add Message Template");

			String testMessage = fc.utobj().generateTestData("TestMessage");
			String description1 = fc.utobj().generateTestData("Test Support Ticket description");

			new AdminSupportManageMessageTemplatesPageTest().addMessageTemplates(driver, testMessage, description1);

			fc.utobj().printTestStep("Navigate to Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest cu = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "supportautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			cu.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			String userName = corpUser.getuserFullName();

			fc.utobj().printTestStep("Navigate to Admin > Support > Manage Department");
			fc.utobj().printTestStep("Add Department");
			String departmentName = fc.utobj().generateTestData(dataSet.get("departmentName"));
			AdminSupportManageDepartmentPageTest departmentPage = new AdminSupportManageDepartmentPageTest();
			departmentPage.addDepartments(driver, departmentName, userName);

			fc.utobj().printTestStep("Navigate To Support > Tickets > Create Ticket");
			fc.utobj().printTestStep("Create Ticket");

			String description = fc.utobj().generateTestData(dataSet.get("description"));
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			createTicket(driver, departmentName, description, subject);
			searchTicketBySubject(driver, subject);

			String ticketNumber = fc.utobj().getText(driver,
					fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + subject
							+ "')]/preceding-sibling::td/a[contains(@href , 'supportTroubleTicketDetails')]"));

			fc.utobj().clickLink(driver, ticketNumber);
			fc.utobj().clickElement(driver, pobj.postMessage);

			fc.utobj().selectDropDown(driver, pobj.templateID, testMessage);
			fc.utobj().clickElement(driver, pobj.addBtn);

			boolean isMessagePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () , '" + description1 + "')]");
			if (isMessagePresent == false) {
				fc.utobj().throwsException("was not able to verify Posted Message");
			}

			fc.utobj().printTestStep("Verify Post Messages Mail");

			Map<String, String> mailData = new HashMap<String, String>();

			mailData = fc.utobj().readMailBox("Message posted in Ticket # " + ticketNumber + "", ticketNumber, emailId,
					"sdg@1a@Hfs");

			if (mailData.size() == 0 || !mailData.get("mailBody").contains(ticketNumber)) {

				fc.utobj().throwsException("was not able to verify Ticket Number");
			}

			if (mailData.size() == 0 || !mailData.get("mailBody").contains("New Message Added")) {

				fc.utobj().throwsException("was not able to verify New Message Added Email");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void getLock(WebDriver driver, String subject) throws Exception {

		String testCaseId = "TC_Get_Lock_Ticket";

		if (fc.utobj().validate(testCaseId)) {
			try {
				SupportTicketsPage pobj = new SupportTicketsPage(driver);
				actionImgOption(driver, subject, "Get Lock");
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().clickElement(driver, pobj.okBtn);
				fc.commonMethods().Click_Close_Input_ByValue(driver);
				fc.utobj().switchFrameToDefault(driver);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to Lock The Ticket : : Support > Ticket");

		}
	}

	public void actionImgOption(WebDriver driver, String task, String option) throws Exception {

		String alterText = fc.utobj()
				.getElementByXpath(driver,
						".//*[contains(text () , '" + task + "')]/ancestor::tr/td/div[@id='menuBar']/layer")
				.getAttribute("id").trim();
		alterText = alterText.replace("Actions_dynamicmenu", "");
		alterText = alterText.replace("Bar", "");

		fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
				".//*[contains(text () , '" + task + "')]/ancestor::tr/td/div[@id='menuBar']/layer/a/img"));

		List<String> linkArray = fc.utobj().translate(option);
		if (!(linkArray.size() < 1)) {
			for (int i = 0; i < linkArray.size(); i++) {
				try {
					option = linkArray.get(i);
					fc.utobj().clickElement(driver,
							fc.utobj().getElementByXpath(driver, ".//div[@id='Actions_dynamicmenu" + alterText
									+ "Menu']/span[contains(text () , '" + option + "')]"));
					break;
				} catch (Exception e) {
				}
			}
		} else {
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//div[@id='Actions_dynamicmenu"
					+ alterText + "Menu']/span[contains(text () , '" + option + "')]"));
		}
	}

	public void searchTicketBySubject(WebDriver driver, String subject) throws Exception {

		SupportSearchPage pobj = new SupportSearchPage(driver);
		fc.utobj().clickElement(driver, pobj.searchTabLnk);
		fc.utobj().sendKeys(driver, pobj.subjectTextBox, subject);
		try {
			fc.utobj().clickElement(driver, pobj.searchBtn);
		} catch (Exception e) {
			fc.utobj().clickEnterOnElement(driver, pobj.searchBtn);
		}
		
	}

	public void filterTicketByDepartment(WebDriver driver, SupportTicketsPage pobj, String departmentName)
			throws Exception {

		try {
			fc.utobj().clickElement(driver, pobj.showFilter);
		} catch (Exception e) {
			Reporter.log("Show filter is not enable");
		}
		fc.utobj().setToDefault(driver, pobj.selectStatus);
		fc.utobj().setToDefault(driver, pobj.selectDepartment);
		fc.utobj().selectValFromMultiSelect(driver, pobj.selectDepartment, departmentName);
		fc.utobj().clickElement(driver, pobj.searchBtn);
		try {
			fc.utobj().clickElement(driver, pobj.showFilter);
		} catch (Exception e) {
			Reporter.log("Show filter is not enable");
		}
	}
}
