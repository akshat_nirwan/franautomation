package com.builds.test.support;

import org.openqa.selenium.WebDriver;
import org.testng.Reporter;

import com.builds.test.admin.AdminPageTest;
import com.builds.test.common.FCHomePageTest;
import com.builds.uimaps.common.FCHomePage;
import com.builds.utilities.FranconnectUtil;

public class Support_Common extends FranconnectUtil {

	public void adminSupportManageFAQs(WebDriver driver) throws Exception {
		Reporter.log("Navigating to adminSupportManageFAQs...");
		adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openManageFAQsLnk(driver);
	}

	public void adminSupportManageDepartment(WebDriver driver) throws Exception {
		Reporter.log("Navigating to adminSupportManageDepartment...");
		adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openManageDepartmentLnk(driver);
	}

	public void adminSupportManageContactInformationAddNewContact(WebDriver driver) throws Exception {
		Reporter.log("Navigating to adminSupportManageContactInformationAddNewContact...");
		adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openManageContactInformationLnk(driver);
	}

	public void adminSupportManageMessageTemplates(WebDriver driver) throws Exception {
		Reporter.log("Navigating to adminSupportManageMessageTemplates...");
		adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openManageMessageTemplatesLnk(driver);
	}

	public void adminSupportTicketStatus(WebDriver driver) throws Exception {
		Reporter.log("Navigating to adminSupportTicketStatus...");
		adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openConfigureTicketStatusLnk(driver);
	}

	public void supportDashBoard(WebDriver driver) throws Exception {
		Reporter.log("Navigating to supportDashBoard...");

		home_page().openSupportModule(driver);
		support().support_common().openSupportDashBoard(home_page(), driver);
	}

	public void supportTickets(WebDriver driver) throws Exception {
		Reporter.log("Navigating to supportTickets...");
		home_page().openSupportModule(driver);
		support().support_common().openSupportTickets(home_page(), driver);
	}

	public void supportFAQs(WebDriver driver) throws Exception {
		Reporter.log("Navigating to supportFAQs...");
		home_page().openSupportModule(driver);
		support().support_common().openSupportFAQs(home_page(), driver);
	}

	public void supportContactInfo(WebDriver driver) throws Exception {
		Reporter.log("Navigating to supportContactInfo...");
		home_page().openSupportModule(driver);
		support().support_common().openSupportContactInfo(home_page(), driver);
	}

	public void supportAskCorporate(WebDriver driver) throws Exception {
		Reporter.log("Navigating to supportAskCorporate...");
		home_page().openSupportModule(driver);
		support().support_common().openSupportAskCorporate(home_page(), driver);
	}

	public void supportReports(WebDriver driver) throws Exception {
		Reporter.log("Navigating to supportReports...");
		home_page().openSupportModule(driver);
		support().support_common().openSupportReports(home_page(), driver);
	}

	public void openSupportAskCorporate(FCHomePageTest fcHomePageTest, WebDriver driver) throws Exception {

		FCHomePage pobj = new FCHomePage(driver);

		utobj().clickElement(driver, pobj.askCorporate);
		Reporter.log("Navigating to Support Ask Corporate...");
	}

	public void openSupportReports(FCHomePageTest fcHomePageTest, WebDriver driver) throws Exception {

		FCHomePage pobj = new FCHomePage(driver);
		utobj().clickElement(driver, pobj.reports);
		Reporter.log("Navigating to Support Reports...");
	}

	public void openSupportContactInfo(FCHomePageTest fcHomePageTest, WebDriver driver) throws Exception {

		FCHomePage pobj = new FCHomePage(driver);
		utobj().clickElement(driver, pobj.contactInfo);
		Reporter.log("Navigating to Support Contact Info...");
	}

	public void openSupportFAQs(FCHomePageTest fcHomePageTest, WebDriver driver) throws Exception {

		FCHomePage pobj = new FCHomePage(driver);

		utobj().clickElement(driver, pobj.faqs);
		Reporter.log("Navigating to Support FAQs...");
	}

	public void openSupportTickets(FCHomePageTest fcHomePageTest, WebDriver driver) throws Exception {
		FCHomePage pobj = new FCHomePage(driver);
		utobj().clickElement(driver, pobj.supportTickets);
		Reporter.log("Navigating to Support Tickets...");
	}

	public void openSupportDashBoard(FCHomePageTest fcHomePageTest, WebDriver driver) throws Exception {
		FCHomePage pobj = new FCHomePage(driver);
		utobj().clickElement(driver, pobj.supportDashBoard);
		Reporter.log("Navigating to Support DashBoard...");
	}
}
