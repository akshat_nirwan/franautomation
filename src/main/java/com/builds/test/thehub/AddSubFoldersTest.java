package com.builds.test.thehub;


import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.common.CorporateUser;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AddSubFoldersTest {
	
	FranconnectUtil fc=new FranconnectUtil();
	
	@Test(groups = {"hubfolder"})
	@TestCase(createdOn = "2018-04-19", updatedOn = "2018-04-19", testCaseDescription = "Verify inaccessible News items and folders through Global Solr search", testCaseId = "TC_Hub_Folder_NewsItem_Privileges_02")
	private void folderPrivileges() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubNewsPageTest hub_news= new TheHubNewsPageTest();
			String emailId = "hubautomation@staffex.com";
			
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporate_user = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			fc.utobj().printTestStep(
					"Navigate To Admin > Users > Manage Corporate Users > Add  Corporate User with added role");
			CorporateUser corpUser1 = new CorporateUser();
			corpUser1 = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser1);
			corpUser1.setEmail(emailId);
			corpUser1 = corporate_user.createDefaultUser(driver, corpUser1);

			AdminTheHubNewsPageTest admin_news=new AdminTheHubNewsPageTest();
			String folderName = fc.utobj().generateTestData("folderName");
			String folderSummary = fc.utobj().generateTestData("folderSummary");
			String accessTo = "All";
			
			News news=new News();
			news.setFolderName(folderName);
			news.setFolderSummary(folderSummary);
			news.setAccessTo(accessTo);
			
			fc.utobj().printTestStep("Add Folder");
			admin_news.addFolder(driver, news);
			
			fc.utobj().printTestStep("Navigate To The Hub > News Page");
			fc.hub().hub_common().theHubNewsSubModule(driver);
			
			fc.utobj().printTestStep("Add first sub folder");
			String subFolderName=fc.utobj().generateTestData("Testsubfoldername");
			String subFolderSummary=fc.utobj().generateTestData("Test sub folder summary");
			hub_news.addSubFolderInFolder(driver, folderName, subFolderName, subFolderSummary);
			
			fc.utobj().printTestStep("Add second sub folder");
			fc.utobj().clickElementWithActions(driver, fc.utobj().getElementByLinkText(driver, folderName));
			
			String subFolderName2=fc.utobj().generateTestData("Testsubfoldername");
			String subFolderSummary2=fc.utobj().generateTestData("Test sub folder summary");
			hub_news.addSubFolderInFolder(driver, subFolderName, subFolderName2, subFolderSummary2);
			
			fc.utobj().clickElementWithActions(driver, fc.utobj().getElementByLinkText(driver, folderName));
			fc.utobj().clickElementWithActions(driver, fc.utobj().getElementByLinkText(driver, subFolderName));
			
			fc.utobj().printTestStep("Add third sub folder");
			String subFolderName3=fc.utobj().generateTestData("Testsubfoldername");
			String subFolderSummary3=fc.utobj().generateTestData("Test sub folder summary");
			hub_news.addSubFolderInFolder(driver, subFolderName2, subFolderName3, subFolderSummary3);
			
			fc.utobj().clickElementWithActions(driver, fc.utobj().getElementByLinkText(driver, folderName));
			fc.utobj().clickElementWithActions(driver, fc.utobj().getElementByLinkText(driver, subFolderName));
			fc.utobj().clickElementWithActions(driver, fc.utobj().getElementByLinkText(driver, subFolderName2));
			
			fc.utobj().printTestStep("Add fourth sub folder");
			String subFolderName4=fc.utobj().generateTestData("Testsubfoldername");
			String subFolderSummary4=fc.utobj().generateTestData("Test sub folder summary");
			hub_news.addSubFolderInFolder(driver, subFolderName3, subFolderName4, subFolderSummary4);
			
			fc.utobj().clickElementWithActions(driver, fc.utobj().getElementByLinkText(driver, folderName));
			fc.utobj().clickElementWithActions(driver, fc.utobj().getElementByLinkText(driver, subFolderName));
			fc.utobj().clickElementWithActions(driver, fc.utobj().getElementByLinkText(driver, subFolderName2));
			fc.utobj().clickElementWithActions(driver, fc.utobj().getElementByLinkText(driver, subFolderName3));
			
			fc.utobj().printTestStep("Add fifth sub folder");
			String subFolderName5=fc.utobj().generateTestData("Testsubfoldername");
			String subFolderSummary5=fc.utobj().generateTestData("Test sub folder summary");
			hub_news.addSubFolderInFolder(driver, subFolderName4, subFolderName5, subFolderSummary5);
			
			fc.utobj().clickElementWithActions(driver, fc.utobj().getElementByLinkText(driver, folderName));
			fc.utobj().clickElementWithActions(driver, fc.utobj().getElementByLinkText(driver, subFolderName));
			fc.utobj().clickElementWithActions(driver, fc.utobj().getElementByLinkText(driver, subFolderName2));
			fc.utobj().clickElementWithActions(driver, fc.utobj().getElementByLinkText(driver, subFolderName3));
			fc.utobj().clickElementWithActions(driver, fc.utobj().getElementByLinkText(driver, subFolderName4));
			
			fc.utobj().printTestStep("Add sixth sub folder");
			String subFolderName6=fc.utobj().generateTestData("Testsubfoldername");
			String subFolderSummary6=fc.utobj().generateTestData("Test sub folder summary");
			hub_news.addSubFolderInFolder(driver, subFolderName5, subFolderName6, subFolderSummary6);
			
			fc.utobj().clickElementWithActions(driver, fc.utobj().getElementByLinkText(driver, folderName));
			fc.utobj().clickElementWithActions(driver, fc.utobj().getElementByLinkText(driver, subFolderName));
			fc.utobj().clickElementWithActions(driver, fc.utobj().getElementByLinkText(driver, subFolderName2));
			fc.utobj().clickElementWithActions(driver, fc.utobj().getElementByLinkText(driver, subFolderName3));
			fc.utobj().clickElementWithActions(driver, fc.utobj().getElementByLinkText(driver, subFolderName4));
			fc.utobj().clickElementWithActions(driver, fc.utobj().getElementByLinkText(driver, subFolderName5));
			
			fc.utobj().printTestStep("Add sixth sub folder");
			String subFolderName7=fc.utobj().generateTestData("Testsubfoldername");
			String subFolderSummary7=fc.utobj().generateTestData("Test sub folder summary");
			hub_news.addSubFolderInFolder(driver, subFolderName6, subFolderName7, subFolderSummary7);
			
			fc.utobj().clickElementWithActions(driver, fc.utobj().getElementByLinkText(driver, folderName));
			fc.utobj().clickElementWithActions(driver, fc.utobj().getElementByLinkText(driver, subFolderName));
			fc.utobj().clickElementWithActions(driver, fc.utobj().getElementByLinkText(driver, subFolderName2));
			fc.utobj().clickElementWithActions(driver, fc.utobj().getElementByLinkText(driver, subFolderName3));
			fc.utobj().clickElementWithActions(driver, fc.utobj().getElementByLinkText(driver, subFolderName4));
			fc.utobj().clickElementWithActions(driver, fc.utobj().getElementByLinkText(driver, subFolderName5));
			fc.utobj().clickElementWithActions(driver, fc.utobj().getElementByLinkText(driver, subFolderName6));
			
			fc.utobj().printTestStep("Add sixth sub folder");
			String subFolderName8=fc.utobj().generateTestData("Testsubfoldername");
			String subFolderSummary8=fc.utobj().generateTestData("Test sub folder summary");
			hub_news.addSubFolderInFolder(driver, subFolderName7, subFolderName8, subFolderSummary8);
			
			fc.utobj().clickElementWithActions(driver, fc.utobj().getElementByLinkText(driver, folderName));
			fc.utobj().clickElementWithActions(driver, fc.utobj().getElementByLinkText(driver, subFolderName));
			fc.utobj().clickElementWithActions(driver, fc.utobj().getElementByLinkText(driver, subFolderName2));
			fc.utobj().clickElementWithActions(driver, fc.utobj().getElementByLinkText(driver, subFolderName3));
			fc.utobj().clickElementWithActions(driver, fc.utobj().getElementByLinkText(driver, subFolderName4));
			fc.utobj().clickElementWithActions(driver, fc.utobj().getElementByLinkText(driver, subFolderName5));
			fc.utobj().clickElementWithActions(driver, fc.utobj().getElementByLinkText(driver, subFolderName6));
			fc.utobj().clickElementWithActions(driver, fc.utobj().getElementByLinkText(driver, subFolderName7));
			
			fc.utobj().printTestStep("Add sixth sub folder");
			String subFolderName9=fc.utobj().generateTestData("Testsubfoldername");
			String subFolderSummary9=fc.utobj().generateTestData("Test sub folder summary");
			hub_news.addSubFolderInFolder(driver, subFolderName8, subFolderName9, subFolderSummary9);
			
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
}
