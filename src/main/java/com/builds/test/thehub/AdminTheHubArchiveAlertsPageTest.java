package com.builds.test.thehub;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.builds.uimaps.thehub.AdminTheHubArchiveAlertsPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminTheHubArchiveAlertsPageTest {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "thehub" ,"hub_archiveAlerts"})
	@TestCase(createdOn = "2018-01-23", updatedOn = "2018-01-23", testCaseDescription = "Verify the Alert functionality from Admin> Archived Alerts Section", testCaseId = "TC_Admin_Hub_Archived_Alerts_01")
	private void archivedAlerts() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			
			AdminTheHubArchiveAlertsPage pobj = new AdminTheHubArchiveAlertsPage(driver);
			fc.utobj().printTestStep("Navigate To Admin > The Hub > Archive Alerts");
			fc.hub().hub_common().adminTheHubArchiveAlertsPage(driver);

			String allText = fc.utobj().getText(driver,
					fc.utobj().getElementByXpath(driver, ".//form[@name='archiveForm']//td[@class='TextLbl']"));

			List<WebElement> listElement = driver.findElements(By.xpath(".//input[@name='days' and @type='radio']"));
			boolean is30Days = false;
			boolean is60Days = false;
			boolean is90Days = false;
			boolean is120Days = false;
			boolean isUserDefinedDays = false;

			if (allText.contains(fc.utobj().translateString("30 Days"))) {
				is30Days = true;
			}
			if (allText.contains(fc.utobj().translateString("60 Days"))) {
				is60Days = true;
			}
			if (allText.contains(fc.utobj().translateString("90 Days"))) {
				is90Days = true;
			}
			if (allText.contains(fc.utobj().translateString("120 Days"))) {
				is120Days = true;
			}
			if (allText.contains(fc.utobj().translateString("User Defined"))) {
				isUserDefinedDays = true;
			}

			if (listElement.size() != 5) {
				fc.utobj().throwsException("Days Radio Button is not present");
			}

			boolean isuserDefineBox = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//input[@name='sdays' and @type='text']");
			boolean isArchiveBtnPresent = fc.utobj()
					.isElementPresent(driver, fc.utobj().getElement(driver, pobj.archiveBtn));

			if (is30Days == false) {

				fc.utobj().throwsException("30 Days radio button is not present");
			}

			if (is60Days == false) {
				fc.utobj().throwsException("60 Days radio button is not present");
			}
			if (is90Days == false) {
				fc.utobj().throwsException("90 Days radio button is not present");
			}
			if (is120Days == false) {
				fc.utobj().throwsException("120 Days radio button is not present");
			}
			if (isuserDefineBox == false) {
				fc.utobj().throwsException("User define text box is not present");
			}
			if (isArchiveBtnPresent == false) {
				fc.utobj().throwsException("Archive Button is not present");
			}
			if (isUserDefinedDays == false) {
				fc.utobj().throwsException("User define days radio box");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
}
