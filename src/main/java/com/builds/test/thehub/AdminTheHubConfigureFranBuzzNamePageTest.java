package com.builds.test.thehub;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.builds.uimaps.thehub.AdminTheHubConfigureFranBuzzNamePage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminTheHubConfigureFranBuzzNamePageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = {"thehub" ,"hub_franbuzz"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Configure FranBuzz Name :Admin > The Hub > Configure FranBuzz Name", testCaseId = "TC_116_Configure_FranBuzz_Name")
	public void modifyFranBuzzName() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			AdminTheHubConfigureFranBuzzNamePage pobj = new AdminTheHubConfigureFranBuzzNamePage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Configure FranBuzz Name Page");
			fc.hub().hub_common().adminTheHubConfigureFranBuzzNamePage(driver);

			fc.utobj().printTestStep("Modify FranBuzz Name");
			fc.utobj().clickElement(driver, pobj.modifyBtn);
			fc.utobj().sendKeys(driver, pobj.franBuzzFieldBx, dataSet.get("franBuzzName"));
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("Verify The Modify FranBuzz Name");
			fc.utobj().isTextDisplayed(driver, dataSet.get("franBuzzName"), "was not able to Configure FranBuzz Name");

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void configureFranBuzzName(WebDriver driver) throws Exception {
		try {
			AdminTheHubConfigureFranBuzzNamePage pobj = new AdminTheHubConfigureFranBuzzNamePage(driver);
			fc.hub().hub_common().adminTheHubConfigureFranBuzzNamePage(driver);
			String franbuzzName = "";
			try {
				franbuzzName = fc.utobj().getText(driver, fc.utobj().getElementByXpath(driver,
						".//*[contains(text(),'FranBuzz Name :')]/following-sibling::td[2]"));
			} catch (Exception e) {
				// TODO: handle exception
			}
			if (!franbuzzName.isEmpty() && !franbuzzName.equals("Franbuzz")) {
				fc.utobj().clickElement(driver, pobj.modifyBtn);
				fc.utobj().sendKeys(driver, pobj.franBuzzFieldBx, "Franbuzz");
				fc.utobj().clickElement(driver, pobj.saveBtn);
			}
		} catch (Exception e) {
			Reporter.log("Not able to configure FranBuzz Name : " + e.getMessage());
			fc.utobj().throwsException("Not able to configure FranBuzz Name : " + e.getMessage());
		}
	}
}
