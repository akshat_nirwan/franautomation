package com.builds.test.thehub;


import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.annotations.BeforeGroups;

import com.builds.uimaps.thehub.AdminTheHubConfigureServerPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminTheHubConfigureServerPageTest {
	
	FranconnectUtil fc = new FranconnectUtil();
	HubCommonMethods hub_common_method = new HubCommonMethods();

	@BeforeGroups(groups="serverhub")
	@TestCase(createdOn = "2018-08-28", updatedOn = "2018-08-28", testCaseDescription = "Configure Server", testCaseId = "TC_TheHub_Configure_Server_01")
	private void configureHubServer() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		ConfigureServer cs = new ConfigureServer();
		try {
			driver = fc.loginpage().login(driver);
			AdminTheHubConfigureServerPage pobj=new AdminTheHubConfigureServerPage(driver);
			
			fc.utobj().printTestStep("Navigate To Admin > The Hub > Configure Server");
			fc.hub().hub_common().adminTheHubConfigureServerPage(driver);
			
			if ("FranConnect S3".equalsIgnoreCase(FranconnectUtil.config.get("hubServerName"))) {
				
				if (!fc.utobj().isSelected(driver, pobj.FranConnectS3)) {
					fc.utobj().clickElement(driver, pobj.FranConnectS3);
				}
				
			}else if ("FTP server".equalsIgnoreCase(FranconnectUtil.config.get("hubServerName"))) {
				
				if (!fc.utobj().isSelected(driver, pobj.FTPServer)) {
					fc.utobj().clickElement(driver, pobj.FTPServer);
					cs.setFTPServerIP("fulton.franconnect.net");
					cs.setFTPServerUN("ftpir");
					cs.setFTPServerP("ftpir@123");
					fc.utobj().sendKeys(driver, pobj.FTPServerIP, cs.getFTPServerIP());
					fc.utobj().sendKeys(driver, pobj.FTPServerUN, cs.getFTPServerUN());
					fc.utobj().sendKeys(driver, pobj.FTPServerP, cs.getFTPServerP());
					fc.utobj().clickElement(driver, pobj.submitBtn);
					fc.utobj().clickElement(driver, pobj.okayBtn);
				}
			}else if ("SFTP server".equalsIgnoreCase(FranconnectUtil.config.get("hubServerName"))) {
				
				if (!fc.utobj().isSelected(driver, pobj.SFTPServer)) {
					fc.utobj().clickElement(driver, pobj.SFTPServer);
					cs.setSFTPServerIP("secure.myfranconnect.com");
					cs.setSFTPServerUN("fcskytest");
					cs.setSFTPServerP("fcskytest@123");
					fc.utobj().sendKeys(driver, pobj.SFTPServerIP, cs.getSFTPServerIP());
					fc.utobj().sendKeys(driver, pobj.SFTPServerUN, cs.getSFTPServerUN());
					fc.utobj().sendKeys(driver, pobj.SFTPServerP, cs.getSFTPServerP());
					fc.utobj().clickElement(driver, pobj.submitBtn);
					fc.utobj().clickElement(driver, pobj.okayBtn);
				}
				
			}else if ("OneDrive".equalsIgnoreCase(FranconnectUtil.config.get("hubServerName"))) {
				
				if (!fc.utobj().isSelected(driver, pobj.OneDrive)) {
					fc.utobj().clickElement(driver, pobj.OneDrive);
					cs.setOneDriveUN("OPRFeb18");
					cs.setOneDriveP("12345");
					cs.setOneDriveClientID("000000004C10270B");
					cs.setOneDriveClientSecretID("dIvUlhOhlnkJWNVH-5AxXWWfkcbcClwA");
					
					fc.utobj().sendKeys(driver, pobj.OneDriveUN, cs.getOneDriveUN());
					fc.utobj().sendKeys(driver, pobj.OneDriveP, cs.getOneDriveP());
					fc.utobj().sendKeys(driver, pobj.OneDriveClientID, cs.getOneDriveClientID());
					fc.utobj().sendKeys(driver, pobj.OneDriveClientSecretID, cs.getOneDriveClientSecretID());
					fc.utobj().clickElement(driver, pobj.submitBtn);
					fc.utobj().clickElement(driver, pobj.okayBtn);
				}
				
			}else if ("Customer S3".equalsIgnoreCase(FranconnectUtil.config.get("hubServerName"))) {
				if (!fc.utobj().isSelected(driver, pobj.CustomerS3)) {
					fc.utobj().clickElement(driver, pobj.CustomerS3);
					
					cs.setCustomerS3UN("rajkumar.yadav");
					cs.setCustomerS3P("Raj^uataw3");
					cs.setCustomerS3AccessKey("AKIAILWVUR6V6ZPGB52A");
					cs.setCustomerS3SecretKey("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");
					
					fc.utobj().sendKeys(driver, pobj.CustomerS3UN, cs.getCustomerS3UN());
					fc.utobj().sendKeys(driver, pobj.CustomerS3P, cs.getCustomerS3P());
					fc.utobj().sendKeys(driver, pobj.CustomerS3AccessKey, cs.getCustomerS3AccessKey());
					fc.utobj().sendKeys(driver, pobj.CustomerS3SecretKey, cs.getCustomerS3SecretKey());
					fc.utobj().clickElement(driver, pobj.submitBtn);
					fc.utobj().clickElement(driver, pobj.okayBtn);
				}
				
				
			}else if ("Google Drive".equalsIgnoreCase(FranconnectUtil.config.get("hubServerName"))) {
				
				if (!fc.utobj().isSelected(driver, pobj.GoogleDrive)) {
					fc.utobj().clickElement(driver, pobj.GoogleDrive);
					
					/*cs.setGoogleDriveUN(googleDriveUN);
					cs.setGoogleDriveP(googleDriveP);
					cs.setGoogleDriveClientID(googleDriveClientID);
					cs.setGoogleDriveClientSecretID(googleDriveClientSecretID);
					
					fc.utobj().sendKeys(driver, pobj.GoogleDriveUN, value);
					fc.utobj().sendKeys(driver, pobj.GoogleDriveP, value);
					fc.utobj().sendKeys(driver, pobj.GoogleDriveClientID, value);
					fc.utobj().sendKeys(driver, pobj.GoogleDriveClientSecretID, value);
					fc.utobj().clickElement(driver, pobj.submitBtn);
					fc.utobj().clickElement(driver, pobj.okayBtn);*/
				}
			}else{
				if (!fc.utobj().isSelected(driver, pobj.FranConnectS3)) {
					fc.utobj().clickElement(driver, pobj.FranConnectS3);
				}
				Reporter.log("Hub server value : "+FranconnectUtil.config.get("hubServerName"));
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
}
