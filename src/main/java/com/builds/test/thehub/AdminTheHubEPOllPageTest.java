package com.builds.test.thehub;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.admin.AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest;
import com.builds.test.admin.AdminUsersRolesAddNewRolePageTest;
import com.builds.test.common.CorporateUser;
import com.builds.uimaps.thehub.AdminTheHubEPOllPage;
import com.builds.uimaps.thehub.TheHubHomePage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminTheHubEPOllPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = {"thehub" ,"hub_Epoll"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Post New EPoll At Admin > The Hub > EPoll", testCaseId = "TC_45_Post_New_Poll")
	private void postNewPoll() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminTheHubEPOllPage pobj = new AdminTheHubEPOllPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > EPoll > New EPoll Page");
			fc.utobj().printTestStep("Post New Poll");
			fc.hub().hub_common().adminTheHubEPollNewEPollPage(driver);
			
			fc.utobj().printTestStep("To Verify EPoll link feature through admin");
			if (!fc.utobj().isElementPresent(driver, pobj.postedBySelectBtn)) {
				fc.utobj().printTestStep("Posted By	Multi drop down is not present");
			}
			if (!fc.utobj().isElementPresent(driver, pobj.pollStartDate)) {
				fc.utobj().printTestStep("Poll Start date field is not present");			
			}
			if (!fc.utobj().isElementPresent(driver, pobj.pollEndDate)) {
				fc.utobj().printTestStep("Poll End Date field is not present");
			}
			if (!fc.utobj().isElementPresent(driver, pobj.viewArchiveBtn)) {
				fc.utobj().printTestStep("View Archive Button is not present");
			}
			fc.utobj().clickElement(driver, pobj.postNewPollBtn);

			String epollTitle = fc.utobj().generateTestData(dataSet.get("epollTitle"));
			fc.utobj().sendKeys(driver, pobj.epollTitle, epollTitle);
			String startDate = fc.utobj().getCurrentDateUserDefineTimeZoneUSFormat("GMT-5:00");
			fc.utobj().sendKeys(driver, pobj.dateOfPublishingStartDate, startDate);
			String endDate = fc.utobj().getFutureDateUserDefineTimeZoneUSFormat("GMT-5:00", 2);
			fc.utobj().sendKeys(driver, pobj.dateOfPublishingEndDate, endDate);

			if (!fc.utobj().isSelected(driver,pobj.allUsers)) {
				fc.utobj().clickElement(driver, pobj.allUsers);
			}
			if (!fc.utobj().isSelected(driver,pobj.flafView.get(0))) {
				fc.utobj().clickElement(driver, pobj.flafView.get(0));
			}
			String epollQuestion = fc.utobj().generateTestData(dataSet.get("epollQuestion"));
			fc.utobj().sendKeys(driver, pobj.epollQuestion, epollQuestion);
			fc.utobj().selectDropDown(driver, pobj.epollQuestionType, dataSet.get("epollQuestionType"));
			String option1 = fc.utobj().generateTestData(dataSet.get("option1"));
			fc.utobj().sendKeys(driver, pobj.option1, option1);
			fc.utobj().clickElement(driver, pobj.addOptions);
			String option2 = fc.utobj().generateTestData(dataSet.get("option2"));
			fc.utobj().sendKeys(driver, pobj.option2, option2);
			fc.utobj().clickElement(driver, pobj.addBtn);

			fc.utobj().showAll(driver);
			fc.utobj().isTextDisplayed(driver, epollTitle, "was not able to verify epollTitle");

			fc.utobj().printTestStep("Verify The Post Epoll");
			
			fc.utobj().actionImgOption(driver, epollTitle, "View");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String text = fc.utobj().getElementByXpath(driver, ".//td[@class='bText12']").getText();
			fc.utobj().isTextDisplayed(driver, epollTitle, "was not able to verify epollTitle");
			fc.utobj().isTextDisplayed(driver, "Yes, after casting the Poll",
					"was not able to verify 'can user view The Result Option text'");
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			if (!text.contains(epollQuestion)) {
				fc.utobj().throwsException("was not able to verify epollQuestion");
			}

			if (!text.contains(option1)) {
				fc.utobj().throwsException("was not able to verify epollOption 1");
			}

			if (!text.contains(option2)) {
				fc.utobj().throwsException("was not able to verify Option 2");
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void postNewPoll(WebDriver driver, String epollTitle, String startDate, String endDate, String epollQuestion,
			String epollQuestionType, String option1, String option2, String canTakePart, String canViewResult,
			Map<String, String> config) throws Exception {

		String testCaseId = "TC_Post_New_Poll_TheHub";

		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminTheHubEPOllPage pobj = new AdminTheHubEPOllPage(driver);

				fc.hub().hub_common().adminTheHubEPollNewEPollPage(driver);
				fc.utobj().clickElement(driver, pobj.postNewPollBtn);

				fc.utobj().sendKeys(driver, pobj.epollTitle, epollTitle);
				fc.utobj().sendKeys(driver, pobj.dateOfPublishingStartDate, startDate);
				fc.utobj().sendKeys(driver, pobj.dateOfPublishingEndDate, endDate);

				if (canTakePart.equalsIgnoreCase("All")) {

					if (!fc.utobj().isSelected(driver,pobj.allUsers)) {
						fc.utobj().clickElement(driver, pobj.allUsers);
					}

				} else if (canTakePart.equalsIgnoreCase("CorporateUsers")) {

					if (fc.utobj().isSelected(driver,pobj.allUsers)) {
						fc.utobj().clickElement(driver, pobj.allUsers);
					} else {
						fc.utobj().clickElement(driver, pobj.allUsers);
						fc.utobj().clickElement(driver, pobj.allUsers);
					}

					fc.utobj().clickElement(driver, pobj.corporateUsers);

				} else if (canTakePart.equalsIgnoreCase("DivisiopnalUser")) {

					if (fc.utobj().isSelected(driver,pobj.allUsers)) {
						fc.utobj().clickElement(driver, pobj.allUsers);
					} else {
						fc.utobj().clickElement(driver, pobj.allUsers);
						fc.utobj().clickElement(driver, pobj.allUsers);
					}

					fc.utobj().clickElement(driver, pobj.divisionUsers);

				} else if (canTakePart.equalsIgnoreCase("RegionalUsers")) {

					if (fc.utobj().isSelected(driver,pobj.allUsers)) {
						fc.utobj().clickElement(driver, pobj.allUsers);
					} else {
						fc.utobj().clickElement(driver, pobj.allUsers);
						fc.utobj().clickElement(driver, pobj.allUsers);
					}

					fc.utobj().clickElement(driver, pobj.regionalUsers);

				} else if (canTakePart.equalsIgnoreCase("FranchiseUsers")) {

					if (fc.utobj().isSelected(driver,pobj.allUsers)) {
						fc.utobj().clickElement(driver, pobj.allUsers);
					} else {
						fc.utobj().clickElement(driver, pobj.allUsers);
						fc.utobj().clickElement(driver, pobj.allUsers);
					}

					fc.utobj().clickElement(driver, pobj.franchiseeUsers);

				}

				if (canViewResult.equalsIgnoreCase("Yes After Casting the Poll")) {

					if (!fc.utobj().isSelected(driver,pobj.flafView.get(0))) {
						fc.utobj().clickElement(driver, pobj.flafView.get(0));
					}

				} else if (canViewResult.equalsIgnoreCase("Yes After Polling is Over")) {

					if (!fc.utobj().isSelected(driver,pobj.flafView.get(1))) {
						fc.utobj().clickElement(driver, pobj.flafView.get(1));
					}

				} else if (canViewResult.equalsIgnoreCase("No")) {

					if (!fc.utobj().isSelected(driver,pobj.flafView.get(2))) {
						fc.utobj().clickElement(driver, pobj.flafView.get(2));
					}
				}

				fc.utobj().sendKeys(driver, pobj.epollQuestion, epollQuestion);
				fc.utobj().selectDropDown(driver, pobj.epollQuestionType, epollQuestionType);
				fc.utobj().sendKeys(driver, pobj.option1, option1);
				fc.utobj().clickElement(driver, pobj.addOptions);
				fc.utobj().sendKeys(driver, pobj.option2, option2);
				fc.utobj().clickElement(driver, pobj.addBtn);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to post New Poll");

		}
	}

	@Test(groups = {"thehub","hub_Epoll"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Report Of New Poll At Admin > The Hub > EPoll", testCaseId = "TC_46_Report_Poll")
	private void reportPollActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > EPoll > New EPoll Page");
			fc.utobj().printTestStep("Post New Poll");
			String epollTitle = fc.utobj().generateTestData(dataSet.get("epollTitle"));
			String startDate = fc.utobj().getCurrentDateUserDefineTimeZoneUSFormat("GMT-5:00");
			String endDate = fc.utobj().getFutureDateUserDefineTimeZoneUSFormat("GMT-5:00" , 2);
			String epollQuestion = fc.utobj().generateTestData(dataSet.get("epollQuestion"));
			String option1 = fc.utobj().generateTestData(dataSet.get("option1"));
			String option2 = fc.utobj().generateTestData(dataSet.get("option2"));
			String epollQuestionType = dataSet.get("epollQuestionType");
			String canTakePart = "All";
			String canViewResult = "Yes After Polling is Over";
			postNewPoll(driver, epollTitle, startDate, endDate, epollQuestion, epollQuestionType, option1, option2,
					canTakePart, canViewResult, config);

			fc.utobj().printTestStep("Verify Report Of New Poll");
			fc.utobj().showAll(driver);
			fc.utobj().actionImgOption(driver, epollTitle, "Report");

			fc.utobj().isTextDisplayed(driver, epollTitle, "was not able to veriy Epoll Title");
			fc.utobj().isTextDisplayed(driver, epollQuestion, "was not able to verify epollQuestion");

			List<WebElement> list = driver.findElements(By.xpath(".//select[@class='dropdown_list']/option"));

			if (!list.get(0).getText().trim().equalsIgnoreCase(option1)) {
				fc.utobj().throwsException("was not able to verify option 1");
			}
			if (!list.get(1).getText().trim().equalsIgnoreCase(option2)) {
				fc.utobj().throwsException("was not able to verify option 2");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(groups = {"thehub","hub_Epoll"})
	@TestCase(createdOn = "2018-06-06", updatedOn = "2018-06-06", testCaseDescription = "Verify the View functionality for Ongoing Epolls", testCaseId = "TC_Admin_The_Hub_EPoll_02")
	private void viewEpollActionIcon() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminTheHubEPOllPage pobj=new AdminTheHubEPOllPage(driver);
			fc.utobj().printTestStep("Navigate To Admin > The Hub > EPoll > New EPoll Page");
			fc.utobj().printTestStep("Post New Poll");
			String epollTitle = fc.utobj().generateTestData(dataSet.get("epollTitle"));
			String startDate = fc.utobj().getCurrentDateUserDefineTimeZoneUSFormat("GMT-5:00");
			String endDate = fc.utobj().getFutureDateUserDefineTimeZoneUSFormat("GMT-5:00" , 2);
			String epollQuestion = fc.utobj().generateTestData(dataSet.get("epollQuestion"));
			String option1 = fc.utobj().generateTestData(dataSet.get("option1"));
			String option2 = fc.utobj().generateTestData(dataSet.get("option2"));
			String epollQuestionType = dataSet.get("epollQuestionType");
			String canTakePart = "All";
			String canViewResult = "Yes After Polling is Over";
			postNewPoll(driver, epollTitle, startDate, endDate, epollQuestion, epollQuestionType, option1, option2,
					canTakePart, canViewResult, config);

			fc.utobj().printTestStep("Verify View Of New EPoll");
			fc.utobj().showAll(driver);
			fc.utobj().actionImgOption(driver, epollTitle, "View");
			
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().isTextDisplayed(driver, epollTitle, "was not able to veriy Epoll Title");
			fc.utobj().isTextDisplayed(driver, epollQuestion, "was not able to verify epollQuestion");

			List<WebElement> list = driver.findElements(By.xpath(".//select[@class='dropdown_list']/option"));

			if (!list.get(0).getText().trim().equalsIgnoreCase(option1)) {
				fc.utobj().throwsException("was not able to verify option 1");
			}
			if (!list.get(1).getText().trim().equalsIgnoreCase(option2)) {
				fc.utobj().throwsException("was not able to verify option 2");
			}
			
			fc.utobj().printTestStep("Verify print and close Should be present");
			
			if (!fc.utobj().isElementPresent(driver, pobj.printBtn)) {
				fc.utobj().printTestStep("Print Button is not present at View cbox iframe");
			}
			
			if (!fc.utobj().isElementPresent(driver, pobj.closeBtn)) {
				fc.utobj().printTestStep("Close Button is not present at View cbox iframe");
			}
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(groups = {"thehub","hub_Epoll"})
	@TestCase(createdOn = "2018-06-04", updatedOn = "2018-06-04", testCaseDescription = "Verify Epoll options orders", testCaseId = "TC_Admin_Hub_EPoll_01")
	private void epollQuestionOrder() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			String emailId="hubautomation@staffex.com";
			fc.utobj().printTestStep(
					"Navigate To Admin > Users > Manage Corporate Users > Add  Corporate User with added role");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporate_user = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = corporate_user.createDefaultUser(driver, corpUser);
			
			AdminTheHubEPOllPage pobj = new AdminTheHubEPOllPage(driver);
			fc.utobj().printTestStep("Navigate To Admin > The Hub > EPoll > New EPoll Page");
			fc.hub().hub_common().adminTheHubEPollNewEPollPage(driver);
			fc.utobj().printTestStep("Post New Poll");
			fc.utobj().clickElement(driver, pobj.postNewPollBtn);
			
			String epollTitle=fc.utobj().generateTestData("Epoll01Title");
			String startDate=fc.utobj().getCurrentDateUserDefineTimeZoneUSFormat("GMT-5:00");
			String endDate=fc.utobj().getFutureDateUserDefineTimeZoneUSFormat("GMT-5:00", 2);
			fc.utobj().sendKeys(driver, pobj.epollTitle, epollTitle);
			fc.utobj().sendKeys(driver, pobj.dateOfPublishingStartDate, startDate);
			fc.utobj().sendKeys(driver, pobj.dateOfPublishingEndDate, endDate);
			
			fc.utobj().clickElement(driver, pobj.allUsers);
			
			fc.utobj().clickElement(driver, pobj.corporateUsers);
			
			if (!fc.utobj().isSelected(driver,pobj.flafView.get(0))) {
				fc.utobj().clickElement(driver, pobj.flafView.get(0));
			}
			String epollQuestion = fc.utobj().generateTestData("Testyears");
			fc.utobj().sendKeys(driver, pobj.epollQuestion, epollQuestion);
			
			fc.utobj().selectDropDown(driver, pobj.epollQuestionType, "Radio");
			
			
			String [] monthName={"Jan","Feb","March","April","May","Jun","July","August","Sep","Oct","Nov","Dec"};
			
			for (int i = 0; i <11; i++) {
				fc.utobj().clickElement(driver, pobj.addOptions);
			}
			
			List<WebElement> listElement=fc.utobj().getElementListByXpath(driver, ".//input[@name='epollOptions_0answers']");
			for (int i = 0; i < listElement.size(); i++) {
				fc.utobj().sendKeys(driver, listElement.get(i), monthName[i]);
			}
			fc.utobj().clickElement(driver, pobj.addBtn);
			
			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login with newly created corporate user");
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());
			
			fc.utobj().printTestStep("Navigate To The Hub > Home");
			fc.hub().hub_common().theHubHome(driver);
			
			fc.utobj().clickElement(driver, new TheHubHomePage(driver).allOngoingPoll);
			
			WebElement optionsListElement=fc.utobj().getElementByXpath(driver, ".//*[contains(text(),'"+epollQuestion+"')]");
			String lines[] = fc.utobj().getText(driver, optionsListElement).split("\\r?\\n");
			
			for (int i = 0; i <12; i++) {
				if (!lines[2+i].equalsIgnoreCase(monthName[i])) {
					fc.utobj().throwsException("Not able to verify order of questions");
				}
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"thehub","hub_Epoll"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Delete EPoll At Admin > The Hub > EPoll", testCaseId = "TC_47_Delete_Poll")
	private void deletePollActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > EPoll > New EPoll Page");
			fc.utobj().printTestStep("Post New Poll");
			String epollTitle = fc.utobj().generateTestData(dataSet.get("epollTitle"));
			String startDate = fc.utobj().getCurrentDateUSFormat();
			String endDate = fc.utobj().getFutureDateUSFormat(2);
			String epollQuestion = fc.utobj().generateTestData(dataSet.get("epollQuestion"));
			String option1 = fc.utobj().generateTestData(dataSet.get("option1"));
			String option2 = fc.utobj().generateTestData(dataSet.get("option2"));
			String epollQuestionType = dataSet.get("epollQuestionType");
			String canTakePart = "All";
			String canViewResult = "Yes After Polling is Over";
			postNewPoll(driver, epollTitle, startDate, endDate, epollQuestion, epollQuestionType, option1, option2,
					canTakePart, canViewResult, config);
			fc.utobj().showAll(driver);

			fc.utobj().printTestStep("Delete The New Poll");
			fc.utobj().actionImgOption(driver, epollTitle, "Delete");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Delete EPoll");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, epollTitle);
			if (isTextPresent == true) {
				fc.utobj().throwsException("was not able to verify delete EPOLL");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"thehub","hub_Epoll"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Modify EPOll At Admin > The Hub > EPoll", testCaseId = "TC_48_Modify_Poll")
	private void modifyPollActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminTheHubEPOllPage pobj = new AdminTheHubEPOllPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > EPoll > New EPoll Page");
			fc.utobj().printTestStep("Post New Poll");
			String epollTitle = fc.utobj().generateTestData(dataSet.get("epollTitle"));
			String startDate = fc.utobj().getCurrentDateUSFormat();
			String endDate = fc.utobj().getFutureDateUSFormat(2);
			String epollQuestion = fc.utobj().generateTestData(dataSet.get("epollQuestion"));
			String option1 = fc.utobj().generateTestData(dataSet.get("option1"));
			String option2 = fc.utobj().generateTestData(dataSet.get("option2"));
			String epollQuestionType = dataSet.get("epollQuestionType");
			String canTakePart = "All";
			String canViewResult = "Yes After Polling is Over";
			postNewPoll(driver, epollTitle, startDate, endDate, epollQuestion, epollQuestionType, option1, option2,
					canTakePart, canViewResult, config);
			fc.utobj().showAll(driver);

			fc.utobj().printTestStep("Modify The Poll");
			fc.utobj().actionImgOption(driver, epollTitle, "Modify");

			epollTitle = fc.utobj().generateTestData(dataSet.get("epollTitle"));
			endDate = fc.utobj().getFutureDateUSFormat(2);
			epollQuestion = fc.utobj().generateTestData(dataSet.get("epollQuestion"));
			option1 = fc.utobj().generateTestData(dataSet.get("option1"));
			option2 = fc.utobj().generateTestData(dataSet.get("option2"));
			String epollQuestionType1 = dataSet.get("epollQuestionType");
			canTakePart = "All";
			canViewResult = "No";

			fc.utobj().sendKeys(driver, pobj.epollTitle, epollTitle);
			fc.utobj().sendKeys(driver, pobj.dateOfPublishingEndDate, endDate);

			if (fc.utobj().isSelected(driver,pobj.allUsers)) {
				fc.utobj().clickElement(driver, pobj.allUsers);
			} else {
				fc.utobj().clickElement(driver, pobj.allUsers);
				fc.utobj().clickElement(driver, pobj.allUsers);
			}

			fc.utobj().clickElement(driver, pobj.corporateUsers);

			if (!fc.utobj().isSelected(driver,pobj.flafView.get(2))) {
				fc.utobj().clickElement(driver, pobj.flafView.get(2));

			}
			fc.utobj().sendKeys(driver, pobj.epollQuestion, epollQuestion);
			fc.utobj().selectDropDown(driver, pobj.epollQuestionType, epollQuestionType1);
			fc.utobj().sendKeys(driver, pobj.option1, option1);
			fc.utobj().sendKeys(driver, pobj.option2, option2);
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("Verify The Modify Epoll");
			fc.utobj().showAll(driver);
			fc.utobj().isTextDisplayed(driver, epollTitle, "was not able to verify epollTitle");

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"thehub","hub_Epoll"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Deactivate EPoll At Admin > The Hub > EPoll", testCaseId = "TC_49_Deactive_Poll")
	private void deactivePollActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminTheHubEPOllPage pobj = new AdminTheHubEPOllPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > EPoll > New EPoll Page");
			fc.utobj().printTestStep("Post New Poll");
			String epollTitle = fc.utobj().generateTestData(dataSet.get("epollTitle"));
			String startDate = fc.utobj().getCurrentDateUserDefineTimeZoneUSFormat("GMT-5:00");
			String endDate = fc.utobj().getFutureDateUserDefineTimeZoneUSFormat("GMT-5:00", 2);
			String epollQuestion = fc.utobj().generateTestData(dataSet.get("epollQuestion"));
			String option1 = fc.utobj().generateTestData(dataSet.get("option1"));
			String option2 = fc.utobj().generateTestData(dataSet.get("option2"));
			String epollQuestionType = dataSet.get("epollQuestionType");
			String canTakePart = "All";
			String canViewResult = "Yes After Polling is Over";
			postNewPoll(driver, epollTitle, startDate, endDate, epollQuestion, epollQuestionType, option1, option2,
					canTakePart, canViewResult, config);
			fc.utobj().showAll(driver);

			fc.utobj().printTestStep("Deactivate Poll");
			fc.utobj().actionImgOption(driver, epollTitle, "Deactivate");
			fc.utobj().acceptAlertBox(driver);
			fc.utobj().selectValFromMultiSelect(driver, pobj.postedBySelectBtn, "FranConnect Administrator");
			fc.utobj().sendKeys(driver, pobj.pollStartDate, startDate);
			fc.utobj().sendKeys(driver, pobj.pollEndDate, endDate);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			fc.utobj().showAll(driver);
			fc.utobj().printTestStep("Verify The Modify Epoll");
			fc.utobj().isTextDisplayed(driver, epollTitle, "ws not able to verify Epoll title");
			fc.utobj().isTextDisplayed(driver, "Deactivated", "was not able to verify Status of Poll");
			fc.utobj().isTextDisplayed(driver, "FranConnect Administrator", "was not able to verify Status of Poll");
			fc.utobj().isTextDisplayed(driver, startDate, "was not able to verify Status of Poll");
			fc.utobj().isTextDisplayed(driver, endDate, "was not able to verify Status of Poll");

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_role" ,"hub_Epoll"})
	@TestCase(createdOn = "2018-01-08", updatedOn = "2018-01-08", testCaseDescription = "Verify On deselecting Can View EPoll Privilege , Epoll section should not appear under the Admin > HUB Module By Corporate User", testCaseId = "TC_Admin_Hub_Epoll_Privilege_01")
	private void adminEpollPrivileges01() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Go to admin > Roles > Create Corporate Role with Can View EPoll as deselected");

			AdminUsersRolesAddNewRolePageTest roles_page = new AdminUsersRolesAddNewRolePageTest();
			String roleType = dataSet.get("roleType");
			String roleName = fc.utobj().generateTestData("TcCorporateRole");
			String moduleName = "The Hub";
			String emailId = "hubautomation@staffex.com";

			Map<String, String> privileges = new HashMap<String, String>();
			privileges.put("Can View EPoll", "No");
			roles_page.addRolesWithModulesPrivileges(driver, roleType, roleName, moduleName, privileges, testCaseId);
			fc.utobj().printTestStep(
					"Navigate To Admin > Users > Manage Corporate Users > Add  Corporate User with added role");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporate_user = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser.setRole(roleName);
			corpUser = corporate_user.createDefaultUser(driver, corpUser);

			fc.utobj().printTestStep("Logged out from ADM User");
			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Log In with newly created Corporate user");
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());

			fc.utobj().printTestStep("Navigate To Admin");
			fc.adminpage().adminPage(driver);

			fc.utobj().printTestStep("Verify The Epoll Section should not be visible at Admin");

			boolean isEpollSectionPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[@id='Intranet']//a[@qat_adminlink='EPoll']");

			if (isEpollSectionPresent) {
				fc.utobj().throwsException("Epoll section is visible at Admin");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_role" ,"hub_Epoll"})
	@TestCase(createdOn = "2018-01-08", updatedOn = "2018-01-08", testCaseDescription = "Verify On deselecting Can View EPoll Privilege , Epoll section should not appear under the Admin > HUB Module By Divisional User", testCaseId = "TC_Admin_Hub_Epoll_Privilege_02")
	private void adminEpollPrivileges02() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Go to admin > Roles > Create Divisional Role with Can View EPoll as deselected");

			AdminUsersRolesAddNewRolePageTest roles_page = new AdminUsersRolesAddNewRolePageTest();
			String roleType = dataSet.get("roleType");
			String roleName = fc.utobj().generateTestData("TcDivisionalRole");
			String moduleName = "The Hub";
			String emailId = "hubautomation@staffex.com";
			String userName = fc.utobj().generateTestData("du");
			String password = "t0n1ght@123";
			String divisionName = fc.utobj().generateTestData("Testdiv");
			Map<String, String> privileges = new HashMap<String, String>();
			privileges.put("Can View EPoll", "No");
			roles_page.addRolesWithModulesPrivileges(driver, roleType, roleName, moduleName, privileges, testCaseId);

			fc.utobj().printTestStep(
					"Navigate To Admin > Users > Manage Divisional Users > Add  Divisional User with added role");
			AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest divisional_user = new AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest();
			divisional_user.addDivisionalUserWithRole(driver, userName, divisionName, roleName, password, emailId);

			fc.utobj().printTestStep("Logged out from ADM User");
			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Log In with newly created Divisional user");
			fc.loginpage().loginWithParameter(driver, userName, password);

			fc.utobj().printTestStep("Navigate To Admin");
			fc.adminpage().adminPage(driver);
			fc.utobj().printTestStep("Verify The Epoll Section should not be visible at Admin");
			boolean isEpollSectionPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[@id='Intranet']//a[@qat_adminlink='EPoll']");
			if (isEpollSectionPresent) {
				fc.utobj().throwsException("Epoll section is visible at Admin");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
}
