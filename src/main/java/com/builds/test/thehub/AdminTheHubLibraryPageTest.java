package com.builds.test.thehub;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminAreaRegionAddAreaRegionPageTest;
import com.builds.test.admin.AdminDivisionAddDivisionPageTest;
import com.builds.test.admin.AdminFranchiseLocationAddFranchiseLocationPageTest;
import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.admin.AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest;
import com.builds.test.admin.AdminUsersManageManageFranchiseUsersPageTest;
import com.builds.test.admin.AdminUsersManageRegionalUsersAddRegionalUserPageTest;
import com.builds.test.admin.AdminUsersRolesAddNewRolePageTest;
import com.builds.test.admin.AdminUsersRolesAddRoleAddUsersforRolePageTest;
import com.builds.test.common.CorporateUser;
import com.builds.test.common.SearchTest;
import com.builds.uimaps.admin.AdminUsersRolesAddNewRolePage;
import com.builds.uimaps.thehub.AdminTheHubLibraryPage;
import com.builds.uimaps.thehub.TheHubLibraryPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminTheHubLibraryPageTest {

	FranconnectUtil fc = new FranconnectUtil();
	HubCommonMethods hub_common_method = new HubCommonMethods();

	@Test(groups = { "thehub", "hub_library","hubf1121"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Verify The Add Folder At Admin > The Hub > Library >  Resource Library", testCaseId = "TC_50_Add_Folder")
	private void addFolder() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = "Text";
			String accessTo = "All";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			addFolder(driver, library);

			clickOverMoreLink(driver, folderName);
			fc.utobj().printTestStep("Verify The Add Folder");
			fc.utobj().isTextDisplayed(driver, folderName, "was not able to add Folder");

			fc.utobj().printTestStep("Verify Archived Documents Tab should be present at Library Page");
			boolean isArchivedDocuments = fc.utobj().assertLinkPartialText(driver, "Archived Documents");
			if (!isArchivedDocuments) {
				fc.utobj().throwsException("Archived Documents tab is not present");
			}
			
			//Add Corporate User
			fc.utobj().printTestStep("Verify By The Corporate User that folder shoud be searched by folder name and description");
			
			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users Page");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUserPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "hubautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corporateUserPage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			
			//Add Divisional User
			fc.utobj().printTestStep("Add Divisional User");
			AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest divisional_user=new AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest();
			String userName=fc.utobj().generateTestData("Test50DivUser");
			String divisionName=fc.utobj().generateTestData("Test50DivName");
			String password="fran1234";
			AdminDivisionAddDivisionPageTest p1 = new AdminDivisionAddDivisionPageTest();
			p1.addDivision(driver, divisionName);
			divisional_user.addDivisionalUser(driver, userName, password, divisionName, emailId);
			
			//Add Regional User
			AdminAreaRegionAddAreaRegionPageTest addRegionPage = new AdminAreaRegionAddAreaRegionPageTest();
			String regionName = fc.utobj().generateTestData("Test50regname");
			addRegionPage.addAreaRegion(driver, regionName);

			fc.utobj().printTestStep("Navigate To Admin > Users > Mange Regional Users Page");
			fc.utobj().printTestStep("Add Regional User");
			AdminUsersManageRegionalUsersAddRegionalUserPageTest regionalUserPage = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
			String userNameReg = fc.utobj().generateTestData("Test50reguser");
			regionalUserPage.addRegionalUser(driver, userNameReg, password, regionName, emailId);
			
			//Add Franchise User
			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			String regionNameFran = "Test50Region".concat(fc.utobj().generateRandomNumber());
			String storeType = "Test50StoreType".concat(fc.utobj().generateRandomNumber());
			String franchiseId = "Test50FranId".concat(fc.utobj().generateRandomNumber());

			AdminFranchiseLocationAddFranchiseLocationPageTest addFranchiseLocation = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String firstName = fc.utobj().generateTestData("Test50");
			franchiseId = addFranchiseLocation.addFranchiseLocation_AllWithFName(driver, franchiseId, regionNameFran,
					storeType, firstName);

			fc.utobj().printTestStep("Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest addFranchiseUser = new AdminUsersManageManageFranchiseUsersPageTest();
			String roleName = "Default Franchise Role";
			String userNameFran = fc.utobj().generateTestData(dataSet.get("userName"));
			addFranchiseUser.addFranchiseUser(driver, userNameFran, password, franchiseId, roleName, emailId);
			
			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login with corporate user");
			
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());
			
			fc.utobj().printTestStep("Search Folder By Folder Name");
			library.setSearchBy("Folder");
			if(!hub_common_method.searchItemByGlobalSearch(driver, library)){
				fc.utobj().throwsException("Not able to search foler by folder name");
			}
			
			fc.utobj().printTestStep("Search Folder By Folder Summary");
			library.setSearchBy("FolderSummary");
			if(!hub_common_method.searchItemByGlobalSearch(driver, library)){
				fc.utobj().throwsException("Not able to search foler by folder Summary");
			}
			
			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login with Divisional user");
			
			fc.loginpage().loginWithParameter(driver, userName, password);
			
			fc.utobj().printTestStep("Search Folder By Folder Name");
			library.setSearchBy("Folder");
			if(!hub_common_method.searchItemByGlobalSearch(driver, library)){
				fc.utobj().throwsException("Not able to search foler by folder name");
			}
			
			fc.utobj().printTestStep("Search Folder By Folder Summary");
			library.setSearchBy("FolderSummary");
			if(!hub_common_method.searchItemByGlobalSearch(driver, library)){
				fc.utobj().throwsException("Not able to search foler by folder Summary");
			}
			
			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login with Regional user");
			
			fc.loginpage().loginWithParameter(driver, userNameReg, password);
			
			fc.utobj().printTestStep("Search Folder By Folder Name");
			library.setSearchBy("Folder");
			if(!hub_common_method.searchItemByGlobalSearch(driver, library)){
				fc.utobj().throwsException("Not able to search foler by folder name");
			}
			
			fc.utobj().printTestStep("Search Folder By Folder Summary");
			library.setSearchBy("FolderSummary");
			if(!hub_common_method.searchItemByGlobalSearch(driver, library)){
				fc.utobj().throwsException("Not able to search foler by folder Summary");
			}
			
			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login with Franchise user");
			
			fc.loginpage().loginWithParameter(driver, userNameFran, password);
			
			fc.utobj().printTestStep("Search Folder By Folder Name");
			library.setSearchBy("Folder");
			if(!hub_common_method.searchItemByGlobalSearch(driver, library)){
				fc.utobj().throwsException("Not able to search foler by folder name");
			}
			
			fc.utobj().printTestStep("Search Folder By Folder Summary");
			library.setSearchBy("FolderSummary");
			if(!hub_common_method.searchItemByGlobalSearch(driver, library)){
				fc.utobj().throwsException("Not able to search foler by folder Summary");
			}
						
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(groups = { "thehub", "hub_library" ,"thehubdev0808"})
	@TestCase(createdOn = "2018-08-07", updatedOn = "2018-08-07", testCaseDescription = "Verify that if User has the privilege to View Library but don't have the Role which is present for accessible to folder", testCaseId = "TC_Admin_Hub_Add_Folder_01")
	private void addFolderPrivilege() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = "Text";
			String accessTo = "Corporate";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			addFolder(driver, library);
			
			String documentTitle=fc.utobj().generateTestData("Testdoctitle");
			String briefSummary=fc.utobj().generateTestData("Tesbriefsummary");
			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setCurrentDate(fc.utobj().getCurrentDateUSFormat());
			library.setFutureDate(fc.utobj().getFutureDateUSFormat(2));
			library.setDocumentType("Upload File");
			library.setDocumentSubType("PDF");
			library.setRecommendedDoc(false);
			library.setFile("taskFile_87.pdf");
			addDocument(driver, library);
			
			//Add Corporate User
			fc.utobj().printTestStep("Add Corporate Role");
			String roleName = fc.utobj().generateTestData("TcCorFolder");
			new AdminUsersRolesAddNewRolePageTest().addCorporateRoles(driver, roleName);
			
			fc.utobj().printTestStep("Verify By The Corporate User that folder shoud be searched by folder name and description");
			
			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users Page");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUserPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "hubautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser.setRole(roleName);
			corporateUserPage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			
			//Add Divisional User
			fc.utobj().printTestStep("Add Divisional User");
			AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest divisional_user=new AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest();
			String userName=fc.utobj().generateTestData("Test50DivUser");
			String divisionName=fc.utobj().generateTestData("Test50DivName");
			String password="fran1234";
			AdminDivisionAddDivisionPageTest p1 = new AdminDivisionAddDivisionPageTest();
			p1.addDivision(driver, divisionName);
			divisional_user.addDivisionalUser(driver, userName, password, divisionName, emailId);
			
			//Add Regional User
			AdminAreaRegionAddAreaRegionPageTest addRegionPage = new AdminAreaRegionAddAreaRegionPageTest();
			String regionName = fc.utobj().generateTestData("Test50regname");
			addRegionPage.addAreaRegion(driver, regionName);

			fc.utobj().printTestStep("Navigate To Admin > Users > Mange Regional Users Page");
			fc.utobj().printTestStep("Add Regional User");
			AdminUsersManageRegionalUsersAddRegionalUserPageTest regionalUserPage = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
			String userNameReg = fc.utobj().generateTestData("Test50reguser");
			regionalUserPage.addRegionalUser(driver, userNameReg, password, regionName, emailId);
			
			//Add Franchise User
			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			String regionNameFran = "Test50Region".concat(fc.utobj().generateRandomNumber());
			String storeType = "Test50StoreType".concat(fc.utobj().generateRandomNumber());
			String franchiseId = "Test50FranId".concat(fc.utobj().generateRandomNumber());

			AdminFranchiseLocationAddFranchiseLocationPageTest addFranchiseLocation = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String firstName = fc.utobj().generateTestData("Test50");
			franchiseId = addFranchiseLocation.addFranchiseLocation_AllWithFName(driver, franchiseId, regionNameFran,
					storeType, firstName);

			fc.utobj().printTestStep("Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest addFranchiseUser = new AdminUsersManageManageFranchiseUsersPageTest();
			String roleNameDiv = "Default Franchise Role";
			String userNameFran = fc.utobj().generateTestData("Testfranuser");
			addFranchiseUser.addFranchiseUser(driver, userNameFran, password, franchiseId, roleNameDiv, emailId);
			
			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login with corporate user");
			
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());
			
			fc.utobj().printTestStep("Search Folder By Folder Name");
			library.setSearchBy("Folder");
			library.setWaitTime(20);
			if(hub_common_method.searchItemByGlobalSearch(driver, library)){
				fc.utobj().throwsException("Folder is searchable by folder name");
			}
			
			fc.utobj().printTestStep("Search Folder By Folder Summary");
			library.setSearchBy("FolderSummary");
			if(hub_common_method.searchItemByGlobalSearch(driver, library)){
				fc.utobj().throwsException("Folder is searchable by folder Summary");
			}
			
			fc.utobj().printTestStep("Search Document By Document Title");
			library.setSearchBy("Document Title");
			if(hub_common_method.searchItemByGlobalSearch(driver, library)){
				fc.utobj().throwsException("Document is searchable by Document title");
			}
			
			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login with Divisional user");
			
			fc.loginpage().loginWithParameter(driver, userName, password);
			
			fc.utobj().printTestStep("Search Folder By Folder Name");
			library.setSearchBy("Folder");
			if(hub_common_method.searchItemByGlobalSearch(driver, library)){
				fc.utobj().throwsException("Folder is searchable by folder name");
			}
			
			fc.utobj().printTestStep("Search Folder By Folder Summary");
			library.setSearchBy("FolderSummary");
			if(hub_common_method.searchItemByGlobalSearch(driver, library)){
				fc.utobj().throwsException("Folder is searchable by folder Summary");
			}
			
			fc.utobj().printTestStep("Search Document By Document Title");
			library.setSearchBy("Document Title");
			if(hub_common_method.searchItemByGlobalSearch(driver, library)){
				fc.utobj().throwsException("Document is searchable by Document title");
			}
			
			
			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login with Regional user");
			
			fc.loginpage().loginWithParameter(driver, userNameReg, password);
			
			fc.utobj().printTestStep("Search Folder By Folder Name");
			library.setSearchBy("Folder");
			if(hub_common_method.searchItemByGlobalSearch(driver, library)){
				fc.utobj().throwsException("Folder is searchable by folder name");
			}
			
			fc.utobj().printTestStep("Search Folder By Folder Summary");
			library.setSearchBy("FolderSummary");
			if(hub_common_method.searchItemByGlobalSearch(driver, library)){
				fc.utobj().throwsException("Folder is searchable by folder Summary");
			}
			
			fc.utobj().printTestStep("Search Document By Document Title");
			library.setSearchBy("Document Title");
			if(hub_common_method.searchItemByGlobalSearch(driver, library)){
				fc.utobj().throwsException("Document is searchable by Document title");
			}
			
			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login with Franchise user");
			
			fc.loginpage().loginWithParameter(driver, userNameFran, password);
			
			fc.utobj().printTestStep("Search Folder By Folder Name");
			library.setSearchBy("Folder");
			if(hub_common_method.searchItemByGlobalSearch(driver, library)){
				fc.utobj().throwsException("Folder is searchable by folder name");
			}
			
			fc.utobj().printTestStep("Search Folder By Folder Summary");
			library.setSearchBy("FolderSummary");
			if(hub_common_method.searchItemByGlobalSearch(driver, library)){
				fc.utobj().throwsException("Folder is searchable by folder Summary");
			}
			
			fc.utobj().printTestStep("Search Document By Document Title");
			library.setSearchBy("Document Title");
			if(hub_common_method.searchItemByGlobalSearch(driver, library)){
				fc.utobj().throwsException("Document is searchable by Document title");
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(groups = { "thehub", "hub_library","hubfailed0829"})
	@TestCase(createdOn = "2018-08-07", updatedOn = "2018-08-07", testCaseDescription = "Verify that if User don't have the privilege to View Library but has the Role which is present for accessible to folder", testCaseId = "TC_Admin_Hub_Add_Folder_02")
	private void addFolderPrivilege01() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = "Text";
			String accessTo = "All";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			addFolder(driver, library);
			
			fc.utobj().printTestStep("Add Document");
			String documentTitle=fc.utobj().generateTestData("Testdoctitle");
			String briefSummary=fc.utobj().generateTestData("Tesbriefsummary");
			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setCurrentDate(fc.utobj().getCurrentDateUSFormat());
			library.setFutureDate(fc.utobj().getFutureDateUSFormat(2));
			library.setDocumentType("Upload File");
			library.setDocumentSubType("PDF");
			library.setRecommendedDoc(false);
			library.setFile("taskFile_88.pdf");
			addDocument(driver, library);
			
			fc.utobj().printTestStep("Add Corporate Role with Can View Library Privilege No");
			String corpRoleName = fc.utobj().generateTestData("TcCorFolder");
			String roleType="Corporate Role";
			String moduleName="The Hub";
			Map<String, String> privileges=new HashMap<String, String>();
			privileges.put("Can View Library", "No");
			new AdminUsersRolesAddNewRolePageTest().addRolesWithModulesPrivileges(driver, roleType, corpRoleName, moduleName, privileges, testCaseId);
			
			fc.utobj().printTestStep("Verify By The Corporate User that folder shoud be searched by folder name and description");
			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users Page");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUserPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "hubautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser.setRole(corpRoleName);
			corporateUserPage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			
			fc.utobj().printTestStep("Add Divisional Role with Can View Library Privilege No");
			String divRoleName = fc.utobj().generateTestData("TcDivFolder");
			String divRoleType="Division Level";
			new AdminUsersRolesAddNewRolePageTest().addRolesWithModulesPrivileges(driver, divRoleType, divRoleName, moduleName, privileges, testCaseId);
			
			fc.utobj().printTestStep("Add Divisional User");
			AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest divisional_user=new AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest();
			String userName=fc.utobj().generateTestData("Test50DivUser");
			String divisionName=fc.utobj().generateTestData("Test50DivName");
			String password="fran1234";
			divisional_user.addDivisionalUserWithRole(driver, userName, divisionName, divRoleName, password, emailId);
			
			fc.utobj().printTestStep("Add Regional Role with Can View Library Privilege No");
			String regRoleName = fc.utobj().generateTestData("TcRegFolder");
			String regRoleType="Regional Level";
			new AdminUsersRolesAddNewRolePageTest().addRolesWithModulesPrivileges(driver, regRoleType, regRoleName, moduleName, privileges, testCaseId);
			
			String regionName = fc.utobj().generateTestData("Test50regname");
			fc.utobj().printTestStep("Navigate To Admin > Users > Mange Regional Users Page");
			fc.utobj().printTestStep("Add Regional User");
			AdminUsersManageRegionalUsersAddRegionalUserPageTest regionalUserPage = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
			String userNameReg = fc.utobj().generateTestData("Test50reguser");
			regionalUserPage.addRegionalUserWithRole(driver, userNameReg, password, regionName, regRoleName, emailId);
			
			fc.utobj().printTestStep("Add Franchise Role with Can View Library Privilege No");
			String franRoleName = fc.utobj().generateTestData("TcFranFolder");
			String franRoleType="Franchise Level";
			new AdminUsersRolesAddNewRolePageTest().addRolesWithModulesPrivileges(driver, franRoleType, franRoleName, moduleName, privileges, testCaseId);
			
			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			String regionNameFran = "Test50Region".concat(fc.utobj().generateRandomNumber());
			String storeType = "Test50StoreType".concat(fc.utobj().generateRandomNumber());
			String franchiseId = "Test50FranId".concat(fc.utobj().generateRandomNumber());

			AdminFranchiseLocationAddFranchiseLocationPageTest addFranchiseLocation = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String firstName = fc.utobj().generateTestData("Test50");
			franchiseId = addFranchiseLocation.addFranchiseLocation_AllWithFName(driver, franchiseId, regionNameFran,
					storeType, firstName);

			fc.utobj().printTestStep("Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest addFranchiseUser = new AdminUsersManageManageFranchiseUsersPageTest();
			String userNameFran = fc.utobj().generateTestData("Testfranuser");
			addFranchiseUser.addFranchiseUser(driver, userNameFran, password, franchiseId, franRoleName, emailId);
			
			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login with corporate user");
			
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());
			
			fc.utobj().printTestStep("Search Folder By Folder Name");
			library.setSearchBy("Folder");
			library.setWaitTime(20);
			if(hub_common_method.searchItemByGlobalSearch(driver, library)){
				fc.utobj().throwsException("Folder is searchable by folder name");
			}
			
			fc.utobj().printTestStep("Search Folder By Folder Summary");
			library.setSearchBy("FolderSummary");
			if(hub_common_method.searchItemByGlobalSearch(driver, library)){
				fc.utobj().throwsException("Folder is searchable by folder Summary");
			}
			
			fc.utobj().printTestStep("Search Document by Document Title");
			library.setSearchBy("Document Title");
			if(hub_common_method.searchItemByGlobalSearch(driver, library)){
				fc.utobj().throwsException("Document is search able by Document Title");
			}
			
			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login with Divisional user");
			
			fc.loginpage().loginWithParameter(driver, userName, password);
			
			fc.utobj().printTestStep("Search Folder By Folder Name");
			library.setSearchBy("Folder");
			if(hub_common_method.searchItemByGlobalSearch(driver, library)){
				fc.utobj().throwsException("Folder is searchable by folder name");
			}
			
			fc.utobj().printTestStep("Search Folder By Folder Summary");
			library.setSearchBy("FolderSummary");
			if(hub_common_method.searchItemByGlobalSearch(driver, library)){
				fc.utobj().throwsException("Folder is searchable by folder Summary");
			}
			
			fc.utobj().printTestStep("Search Document by Document Title");
			library.setSearchBy("Document Title");
			if(hub_common_method.searchItemByGlobalSearch(driver, library)){
				fc.utobj().throwsException("Document is search able by Document Title");
			}
			
			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login with Regional user");
			
			fc.loginpage().loginWithParameter(driver, userNameReg, password);
			
			fc.utobj().printTestStep("Search Folder By Folder Name");
			library.setSearchBy("Folder");
			if(hub_common_method.searchItemByGlobalSearch(driver, library)){
				fc.utobj().throwsException("Folder is searchable by folder name");
			}
			
			fc.utobj().printTestStep("Search Folder By Folder Summary");
			library.setSearchBy("FolderSummary");
			if(hub_common_method.searchItemByGlobalSearch(driver, library)){
				fc.utobj().throwsException("Folder is searchable by folder Summary");
			}
			
			fc.utobj().printTestStep("Search Document by Document Title");
			library.setSearchBy("Document Title");
			if(hub_common_method.searchItemByGlobalSearch(driver, library)){
				fc.utobj().throwsException("Document is search able by Document Title");
			}
			
			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login with Franchise user");
			
			fc.loginpage().loginWithParameter(driver, userNameFran, password);
			
			fc.utobj().printTestStep("Search Folder By Folder Name");
			library.setSearchBy("Folder");
			if(hub_common_method.searchItemByGlobalSearch(driver, library)){
				fc.utobj().throwsException("Folder is searchable by folder name");
			}
			
			fc.utobj().printTestStep("Search Folder By Folder Summary");
			library.setSearchBy("FolderSummary");
			if(hub_common_method.searchItemByGlobalSearch(driver, library)){
				fc.utobj().throwsException("Folder is searchable by folder Summary");
			}
			
			fc.utobj().printTestStep("Search Document by Document Title");
			library.setSearchBy("Document Title");
			if(hub_common_method.searchItemByGlobalSearch(driver, library)){
				fc.utobj().throwsException("Document is search able by Document Title");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(groups = { "thehub", "hub_library" })
	@TestCase(createdOn = "2018-06-20", updatedOn = "2018-06-20", testCaseDescription = "Verify Addition of Library Folder without filled mandatory fields", testCaseId = "TC_Admin_The_Hub_Folder_01")
	private void verifyFolderMandatoryFields() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData("TestFolder");
			String folderSummary = fc.utobj().generateTestData("TestFolderSum");
			String summaryFormat = "Text";
			String accessTo = "All";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);

			AdminTheHubLibraryPage pobj = new AdminTheHubLibraryPage(driver);
			fc.hub().hub_common().adminTheHubLibraryPage(driver);
			fc.utobj().clickElement(driver, pobj.addFolder);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().printTestStep("Verify Folder name text field should be mandatory");

			library.setFolderName("");
			fc.utobj().sendKeys(driver, pobj.folderName, library.getFolderName());

			if (!fc.utobj().isSelected(driver, pobj.summaryForamte.get(0))) {
				fc.utobj().clickElement(driver, pobj.summaryForamte.get(0));
			}
			fc.utobj().sendKeys(driver, pobj.folderSummary, library.getFolderSummary());
			fc.utobj().clickElement(driver, pobj.addBtn1);

			boolean isAlertPresent = false;
			String msg = "";

			try {
				msg = fc.utobj().acceptAlertBox(driver);
				isAlertPresent = true;
			} catch (Exception e) {
				isAlertPresent = false;
			}

			if (!isAlertPresent) {
				fc.utobj().throwsException("No alert is present after leaving folder name field empty");
			}

			if (!msg.equalsIgnoreCase("Please specify the Folder Name.")) {
				fc.utobj()
						.throwsException("Not able to verify the alert messages after leaving folder name field empty");
			}

			fc.utobj().printTestStep("Verify Folder Summary text field should be mandatory");

			library.setFolderName(folderName);
			fc.utobj().sendKeys(driver, pobj.folderName, library.getFolderName());

			if (!fc.utobj().isSelected(driver, pobj.summaryForamte.get(0))) {
				fc.utobj().clickElement(driver, pobj.summaryForamte.get(0));
			}

			library.setFolderSummary("");
			fc.utobj().sendKeys(driver, pobj.folderSummary, library.getFolderSummary());
			fc.utobj().clickElement(driver, pobj.addBtn1);

			boolean isAlertPresentSum = false;
			String msgSum = "";

			try {
				msgSum = fc.utobj().acceptAlertBox(driver);
				isAlertPresentSum = true;
			} catch (Exception e) {
				isAlertPresentSum = false;
			}

			if (!isAlertPresentSum) {
				fc.utobj().throwsException("No alert is present after leaving folder summary field empty");
			}

			if (!msgSum.equalsIgnoreCase("Please specify the Folder Summary.")) {
				fc.utobj().throwsException(
						"Not able to verify the alert messages after leaving folder summary field empty");
			}

			fc.commonMethods().Click_Close_Input_ByValue(driver);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void addFolder(WebDriver driver, Library library) throws Exception {
		try {
			AdminTheHubLibraryPage pobj = new AdminTheHubLibraryPage(driver);
			fc.hub().hub_common().adminTheHubLibraryPage(driver);
			try {
				fc.utobj().clickElement(driver, pobj.homeTab);
			} catch (Exception e) {
				Reporter.log("Not able to click home tab");
			}
			fc.utobj().clickElement(driver, pobj.addFolder);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.folderName, library.getFolderName());

			if (library.getSummaryFormat().equalsIgnoreCase("Text")) {

				if (!fc.utobj().isSelected(driver, pobj.summaryForamte.get(0))) {
					fc.utobj().clickElement(driver, pobj.summaryForamte.get(0));
				}

				fc.utobj().sendKeys(driver, pobj.folderSummary, library.getFolderSummary());

			} else if (library.getSummaryFormat().equalsIgnoreCase("HTML")) {

				String htmlText = fc.utobj().generateTestData(library.getHtmlText());

				if (!fc.utobj().isSelected(driver, pobj.summaryForamte.get(1))) {
					fc.utobj().clickElement(driver, pobj.summaryForamte.get(1));
				}

				fc.utobj().switchFrameById(driver, "ta_ifr");

				Actions actions = new Actions(driver);
				actions.moveToElement(fc.utobj().getElement(driver, pobj.editorTextArea));
				actions.click();
				actions.sendKeys(htmlText);
				fc.utobj().logReportMsg("Entered Text", htmlText);
				actions.build().perform();
				fc.utobj().switchFrameToDefault(driver);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
			}

			if (library.getAccessTo().equalsIgnoreCase("All")) {
				fc.utobj().moveToElement(driver, fc.utobj().getElement(driver, pobj.accesToAll));
				if (!fc.utobj().isSelected(driver, pobj.accesToAll)) {
					fc.utobj().clickElement(driver, pobj.accesToAll);
				}

			} else if (library.getAccessTo().equalsIgnoreCase("Corporate")) {
				fc.utobj().moveToElement(driver, fc.utobj().getElement(driver, pobj.accesToAllNo));
				if (!fc.utobj().isSelected(driver, pobj.accesToAllNo)) {
					fc.utobj().clickElement(driver, pobj.accesToAllNo);
				}

				fc.utobj().clickElement(driver, pobj.corporateUserAccess);

			} else if (library.getAccessTo().equalsIgnoreCase("Divisional")) {
				fc.utobj().moveToElement(driver, fc.utobj().getElement(driver, pobj.accesToAllNo));
				if (!fc.utobj().isSelected(driver, pobj.accesToAllNo)) {
					fc.utobj().clickElement(driver, pobj.accesToAllNo);
				}

				fc.utobj().clickElement(driver, pobj.divisionalUserAccess);

			} else if (library.getAccessTo().equalsIgnoreCase("Franchise")) {
				fc.utobj().moveToElement(driver, fc.utobj().getElement(driver, pobj.accesToAllNo));
				if (!fc.utobj().isSelected(driver, pobj.accesToAllNo)) {
					fc.utobj().clickElement(driver, pobj.accesToAllNo);
				}

				fc.utobj().clickElement(driver, pobj.franchiseUserAccess);

			} else if (library.getAccessTo().equalsIgnoreCase("Regional")) {

				fc.utobj().clickElementByJS(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='startRecords']"));

				if (!fc.utobj().isSelected(driver, pobj.accesToAllNo)) {
					fc.utobj().clickElement(driver, pobj.accesToAllNo);
				}

				fc.utobj().clickElement(driver, pobj.regionalUserAccess);
			}

			if (!fc.utobj().isSelected(driver, pobj.addDocumentFolder.get(1))) {
				fc.utobj().clickElement(driver, pobj.addDocumentFolder.get(1));
			}

			fc.utobj().clickElement(driver, pobj.addBtn1);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

		} catch (Exception e) {
			Reporter.log("Not able to add Folder : " + e.getMessage().toString());
			fc.utobj().throwsException("Not able to add Folder");
		}
	}

	@Test(groups = { "thehub", "hub_library" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Verify The Add Folder With Document Admin > The Hub > Library >  Resource Library", testCaseId = "TC_51_Add_Folder_With_Document")
	private void addFolderWithDocument() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			Library library = new Library();

			AdminTheHubLibraryPage pobj = new AdminTheHubLibraryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder With Document");
			fc.hub().hub_common().adminTheHubLibraryPage(driver);
			fc.utobj().clickElement(driver, pobj.homeTab);
			fc.utobj().clickElement(driver, pobj.addFolder);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			fc.utobj().sendKeys(driver, pobj.folderName, folderName);

			if (!fc.utobj().isSelected(driver, pobj.summaryForamte.get(0))) {
				fc.utobj().clickElement(driver, pobj.summaryForamte.get(0));
			}
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			fc.utobj().sendKeys(driver, pobj.folderSummary, folderSummary);
			if (!fc.utobj().isSelected(driver, pobj.roleBase.get(0))) {
				fc.utobj().clickElement(driver, pobj.roleBase.get(0));
			}
			if (!fc.utobj().isSelected(driver, pobj.addDocumentFolder.get(0))) {
				fc.utobj().clickElement(driver, pobj.addDocumentFolder.get(0));
			}

			fc.utobj().sleep();
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			fc.utobj().sendKeys(driver, pobj.documentTitle1, documentTitle);
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			fc.utobj().sendKeys(driver, pobj.briefSummary1, briefSummary);

			String currentDate = fc.utobj().getCurrentDateUSFormat();
			String futureDate = fc.utobj().getFutureDateUSFormat(2);
			fc.utobj().sendKeys(driver, pobj.startDisplayDate1, currentDate);
			fc.utobj().sendKeys(driver, pobj.expirationDate1, futureDate);

			if (!fc.utobj().isSelected(driver, pobj.documentType1.get(0))) {
				fc.utobj().clickElement(driver, pobj.documentType1.get(0));
			}
			fc.utobj().selectDropDown(driver, pobj.documentSubType1, dataSet.get("subType"));

			String file = fc.utobj().getFilePathFromTestData(dataSet.get("fileName"));

			fc.utobj().moveToElement(driver, pobj.selectFile1);
			pobj.selectFile1.sendKeys(file);
			if (fc.utobj().isSelected(driver, pobj.thumbnailImage1.get(1))) {
				fc.utobj().clickElement(driver, pobj.thumbnailImage1.get(1));
			}

			if (!fc.utobj().isSelected(driver, pobj.accessibilityPublic)) {
				fc.utobj().clickElement(driver, pobj.accessibilityPublic);
			}

			fc.utobj().clickElement(driver, pobj.addBtn1);
			fc.utobj().clickElement(driver, pobj.closeConfirmation);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Add Folder With Document");
			clickOverMoreLink(driver, folderName);

			fc.utobj().clickLink(driver, folderName);
			fc.utobj().isTextDisplayed(driver, folderName, "was not able to add Folder");
			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, folderName));
			fc.utobj().isTextDisplayed(driver, folderName, "was not able to verify Folder Title");
			fc.utobj().isTextDisplayed(driver, folderSummary, "was not able to verify Summary of folder");

			if (!fc.utobj().getElementByXpath(driver, ".//*[@id='thumDtls']//a[.='" + documentTitle + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Documnet Title");
			}
			fc.utobj().isTextDisplayed(driver, briefSummary, "was not able to verify Document Brief Summary");

			fc.utobj().printTestStep("Verify The Document Search By Title");
			fc.utobj().sendKeys(driver, pobj.searchDoc, documentTitle);

			boolean isDocPresent = fc.utobj().assertPageSource(driver, documentTitle);
			if (!isDocPresent) {
				fc.utobj().throwsException("Document is not search by Title");
			}

			fc.utobj().printTestStep("Verify The Document Search By Document Description");
			fc.utobj().sendKeys(driver, pobj.searchDoc, documentTitle);

			boolean isDocPresent12 = fc.utobj().assertPageSource(driver, briefSummary);
			if (!isDocPresent12) {
				fc.utobj().throwsException("Document is not search by Description");
			}

			fc.utobj()
					.printTestStep("Search Document Title by Admin > Search > Search All The Hub Documents search box");
			library.setDocumentTitle(documentTitle);
			new SearchTest().searchLibraryDocumentBySearchAllTheHubDocumentSearchBox(driver, library);

			boolean isDocumentTitlePresent = fc.utobj().assertLinkText(driver, library.getDocumentTitle());
			if (!isDocumentTitlePresent) {
				fc.utobj().throwsException("Document is not search able by Search All The Hub Documents search box");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library","hubf1121"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Verify The Add Document At Admin > The Hub > Library >  Resource Library", testCaseId = "TC_52_Add_Document")
	private void addDocument() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			AdminTheHubLibraryPage pobj = new AdminTheHubLibraryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			addFolder(driver, library);

			fc.utobj().printTestStep("Add Document");
			fc.utobj().clickElement(driver, pobj.homeTab);
			fc.utobj().clickElement(driver, pobj.addDocument);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			
			fc.utobj().printTestStep("Verify After clicking over No radio button Window should be replaced by single add document to add only single document");
			if (!fc.utobj().isSelected(driver, pobj.multipleDoc.get(0))) {
				fc.utobj().clickElement(driver, pobj.multipleDoc.get(0));
			}
			if (!fc.utobj().isSelected(driver, pobj.multipleDoc.get(1))) {
				fc.utobj().clickElement(driver, pobj.multipleDoc.get(1));
			}
			
			try {
				fc.utobj().selectDropDown(driver, pobj.folderSelect, folderName);
			} catch (Exception e) {
				// TODO: handle exception
			}
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			fc.utobj().sendKeys(driver, pobj.documentTitle, documentTitle);
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			fc.utobj().sendKeys(driver, pobj.briefSummary, briefSummary);

			String currentDate = fc.utobj().getCurrentDateUSFormat();
			String futureDate = fc.utobj().getFutureDateUSFormat(2);
			fc.utobj().sendKeys(driver, pobj.displayDate, currentDate);
			fc.utobj().sendKeys(driver, pobj.expirationDate, futureDate);

			if (!fc.utobj().isSelected(driver, pobj.documentType.get(0))) {
				fc.utobj().clickElement(driver, pobj.documentType.get(0));
			}

			fc.utobj().selectDropDown(driver, pobj.documentSubType, dataSet.get("subType"));

			String file = fc.utobj().getFilePathFromTestData(dataSet.get("fileName"));
			fc.utobj().moveToElement(driver, pobj.fileClick);
			pobj.fileClick.sendKeys(file);

			if (!fc.utobj().isSelected(driver, pobj.thumbFileUploaded.get(1))) {
				fc.utobj().clickElement(driver, pobj.thumbFileUploaded.get(1));
			}

			if (!fc.utobj().isSelected(driver, pobj.accessibilityPublic)) {
				fc.utobj().clickElement(driver, pobj.accessibilityPublic);
			}

			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().clickElement(driver, pobj.closeConfirmation);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Add Document");
			fc.utobj().clickElement(driver, pobj.homeTab);
			if (!fc.utobj().getElementByXpath(driver, ".//a[.='" + documentTitle + "']").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document title at Resource Libraray Page");
			}

			clickOverMoreLink(driver, folderName);
			fc.utobj().clickLink(driver, folderName);
			fc.utobj().isTextDisplayed(driver, folderName, "was not able to add Folder");
			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, folderName));
			fc.utobj().isTextDisplayed(driver, folderName, "was not able to verify Folder Title");
			fc.utobj().isTextDisplayed(driver, folderSummary, "was not able to verify Summary of folder");

			if (!fc.utobj().getElementByXpath(driver, ".//*[@id='thumDtls']//a[.='" + documentTitle + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Documnet Title");
			}
			fc.utobj().isTextDisplayed(driver, briefSummary, "was not able to verify Document Brief Summary");

			fc.utobj().sendKeys(driver, pobj.searchSystem, documentTitle);
			fc.utobj().clickElement(driver, pobj.systemSearchBtn);
			fc.utobj().isTextDisplayed(driver, documentTitle, "was not able to verify document title after search");
			fc.utobj().isTextDisplayed(driver, briefSummary,
					"was not able to verify document brief summary after search");
			
			//Verify "Uploaded By" field not coming for library documents through franchise users only
			fc.utobj().printTestStep("Add A franchise user");
			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			String regionName = "Test127Region".concat(fc.utobj().generateRandomNumber());
			String storeType = "Test127StoreType".concat(fc.utobj().generateRandomNumber());
			String franchiseId = "Test127FranId".concat(fc.utobj().generateRandomNumber());
			
			AdminFranchiseLocationAddFranchiseLocationPageTest addFranchiseLocation = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			franchiseId = addFranchiseLocation.addFranchiseLocation_AllWithFName(driver, franchiseId, regionName,
					storeType, firstName);

			fc.utobj().printTestStep("Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest addFranchiseUser = new AdminUsersManageManageFranchiseUsersPageTest();
			String roleName = "Default Franchise Role";
			String userName = fc.utobj().generateTestData(dataSet.get("userName"));
			String password = "t0n1ght123";
			String emailId = "hubautomation@staffex.com";
			addFranchiseUser.addFranchiseUser(driver, userName, password, franchiseId, roleName, emailId);
			
			fc.home_page().logout(driver);
			
			fc.utobj().printTestStep("Login With Franchise User");
			fc.loginpage().loginWithParameter(driver, userName, password);
			
			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library");
			
			fc.hub().hub_common().theHubLibrarySubModule(driver);
			library.setDocumentTitle(documentTitle);
			TheHubLibraryPage hub_library=new TheHubLibraryPage(driver);
			fc.utobj().sendKeys(driver, hub_library.searchAllDocument, library.getDocumentTitle());
			fc.utobj().clickElement(driver, hub_library.searchItem);
			fc.utobj().clickLink(driver, library.getFolderName());
			
			if(!fc.utobj().isElementPresent(driver, fc.utobj().getElementByXpath(driver, ".//*[contains(text(),'Uploaded By:')]")))
				fc.utobj().throwsException("Uploaded By is not present for franchise user at library page");
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library" , "sep15_22_1"})
	@TestCase(createdOn = "2018-06-12", updatedOn = "2018-06-12", testCaseDescription = "Verify The Added Document At Admin > The Hub > Library", testCaseId = "TC_Admin_The_Hub_Add_Document_01")
	private void addDocumentWithHtmlPage() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			AdminTheHubLibraryPage pobj = new AdminTheHubLibraryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			addFolder(driver, library);

			fc.utobj().printTestStep("Add Draft Document");
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String currentDate = /* fc.utobj().getCurrentDateUSFormat() */fc.utobj().getFutureDateUSFormat(1);
			String futureDate = fc.utobj().getFutureDateUSFormat(2);

			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setCurrentDate(currentDate);
			library.setFutureDate(futureDate);
			library.setDocumentType(dataSet.get("documentType"));
			library.setEditorText(fc.utobj().generateTestData("Testeditortext"));
			library.setRecommendedDoc(false);
			addDocument(driver, library);

			fc.utobj().printTestStep("Verify The Added Draft Document");
			fc.utobj().clickElement(driver, pobj.draftsTab);

			/*fc.utobj().isTextDisplayed(driver, documentTitle, "was not able to verify document title after search");
			fc.utobj().isTextDisplayed(driver, briefSummary,
					"was not able to verify document's brief summary after search");

			fc.utobj().printTestStep("Verify The Entered Html Text");
			fc.utobj().clickLink(driver, documentTitle);

			boolean isEditorTextPresent = fc.utobj().assertPageSource(driver, library.getEditorText());
			if (!isEditorTextPresent) {
				fc.utobj().throwsException("Entered Html Text is not present");
			}

			fc.utobj().printTestStep("Verify Document Properties option from document Option from Draft > List View");
			fc.utobj().clickElement(driver, pobj.draftsTab);
			fc.utobj().clickElement(driver, pobj.listView);

			hub_common_method.sortByRecentlyUploadedDocuments(driver);
			String parentWindow = driver.getWindowHandle();
			actionImgIconDetailsView(driver, documentTitle, "Document Properties");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver,
					".//td[contains(text () , 'Document URL')]/following-sibling::td/a[contains(@href,'"
							+ FranconnectUtil.config.get("buildUrl") + "')]");

			fc.utobj().printTestStep("Verify Html Document window should be opened");

			boolean isHtmlEditortTextPresent = false;
			Set<String> windowsHandles = driver.getWindowHandles();
			for (String window : windowsHandles) {
				if (!window.equalsIgnoreCase(parentWindow)) {
					driver.switchTo().window(window);
					if (driver.getCurrentUrl().contains("htmlPageView.jsp")) {
						isHtmlEditortTextPresent = fc.utobj().assertPageSource(driver, library.getEditorText());
						driver.close();
						break;
					}
				}
			}
			driver.switchTo().window(parentWindow);
			if (!isHtmlEditortTextPresent) {
				fc.utobj().throwsException("HTML documents is not visible");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			// Modify And Set Upload Document
			fc.utobj().clickElement(driver, pobj.draftsTab);
			fc.utobj().clickElement(driver, pobj.listView);
			fc.utobj().printTestStep("Modify The Document and changed document type to upload");
			actionImgIconDetailsView(driver, documentTitle, "Modify");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			if (!fc.utobj().isSelected(driver, pobj.documentType.get(0))) {
				fc.utobj().clickElement(driver, pobj.documentType.get(0));
			}

			fc.utobj().selectDropDown(driver, pobj.documentSubType, "PDF");
			fc.utobj().moveToElement(driver, pobj.fileClick);

			String fileName = "taskFile_06.pdf";
			pobj.fileClick.sendKeys("taskFile_06.pdf");
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify After clicking over Document url document should be downloaded");
			hub_common_method.sortByRecentlyUploadedDocuments(driver);
			actionImgIconListView(driver, library.getDocumentTitle(), "Document Properties");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver,
					".//td[contains(text () , 'Document URL')]/following-sibling::td/a[contains(@href,'"
							+ FranconnectUtil.config.get("buildUrl") + "')]");

			fc.utobj().printTestStep("Verify The Downloaded File");
			boolean isFileFound = fc.utobj().isFileFound(new File(FranconnectUtil.config.get("downloadFilepath")),
					fileName);
			if (!isFileFound) {
				fc.utobj().throwsException("Document not downled from document properties url");
			}
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			// Modify And Set Link Document
			fc.utobj().printTestStep("Verify After clicking over Document url Link Url window should be open");
			fc.utobj().clickElement(driver, pobj.draftsTab);
			fc.utobj().clickElement(driver, pobj.listView);

			fc.utobj().printTestStep("Modify The Document and changed document type to Web Url");
			actionImgIconDetailsView(driver, documentTitle, "Modify");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			if (!fc.utobj().isSelected(driver, pobj.documentType.get(1))) {
				fc.utobj().clickElement(driver, pobj.documentType.get(1));
			}

			String lnkUrl = "https://in.yahoo.com/";
			fc.utobj().sendKeys(driver, pobj.linkUrl, lnkUrl);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Test Web Url");
			fc.utobj().clickElement(driver, pobj.draftsTab);
			fc.utobj().clickElement(driver, pobj.listView);

			hub_common_method.sortByRecentlyUploadedDocuments(driver);
			actionImgIconListView(driver, library.getDocumentTitle(), "Document Properties");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver,
					".//td[contains(text () , 'Document URL')]/following-sibling::td/a[contains(@href,'"
							+ FranconnectUtil.config.get("buildUrl") + "')]");

			boolean isLinkUrlWIndowOpen = false;
			Set<String> windowsHandlesLink = driver.getWindowHandles();
			for (String window : windowsHandlesLink) {
				if (!window.equalsIgnoreCase(parentWindow)) {
					driver.switchTo().window(window);
					if (driver.getCurrentUrl().toLowerCase().contains("yahoo")) {
						isLinkUrlWIndowOpen = true;
						driver.close();
						break;
					}
				}
			}
			driver.switchTo().window(parentWindow);
			if (!isLinkUrlWIndowOpen) {
				fc.utobj().throwsException("Link is not opening");
			}
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);*/

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library","hub_download","hubf1121"})
	@TestCase(createdOn = "2018-06-12", updatedOn = "2018-06-12", testCaseDescription = "Verify The Added Document At Admin > The Hub > Library", testCaseId = "TC_Admin_The_Hub_Add_Document_02")
	private void documentPropertiesFolder01() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			AdminTheHubLibraryPage pobj = new AdminTheHubLibraryPage(driver);
			
			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			addFolder(driver, library);

			fc.utobj().printTestStep("Add Document");
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String currentDate = fc.utobj().getCurrentDateUSFormat();
			String futureDate = fc.utobj().getFutureDateUSFormat(2);

			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setCurrentDate(currentDate);
			library.setFutureDate(futureDate);
			library.setDocumentType(dataSet.get("documentType"));
			library.setEditorText(fc.utobj().generateTestData("Testeditortext"));
			library.setRecommendedDoc(false);
			addDocument(driver, library);
			String HtmlDocTitle=library.getDocumentTitle();
			
			fc.utobj().printTestStep("Verify Document Properties option from document Option from Folder > List View");
			clickOverMoreLink(driver, folderName);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//span[contains(text(),'" + folderName + "')]"));
			fc.utobj().clickElement(driver, pobj.listView);

			hub_common_method.sortByRecentlyUploadedDocuments(driver);
			String parentWindow = driver.getWindowHandle();
			actionImgIconListView(driver, HtmlDocTitle, "Document Properties");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver,
					".//td[contains(text () , 'Document URL')]/following-sibling::td/a[contains(@href,'"
							+ FranconnectUtil.config.get("buildUrl") + "')]");

			fc.utobj().printTestStep("Verify Html Document window should be opened");

			boolean isHtmlEditortTextPresent = false;
			Set<String> windowsHandles = driver.getWindowHandles();
			for (String window : windowsHandles) {
				if (!window.equalsIgnoreCase(parentWindow)) {
					driver.switchTo().window(window);
					if (driver.getCurrentUrl().contains("htmlPageView.jsp")) {
						isHtmlEditortTextPresent = fc.utobj().assertPageSource(driver, library.getEditorText());
						driver.close();
						break;
					}
				}
			}
			driver.switchTo().window(parentWindow);
			if (!isHtmlEditortTextPresent) {
				fc.utobj().throwsException("HTML documents is not visible");
			}
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			clickOverMoreLink(driver, folderName);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//span[contains(text(),'" + folderName + "')]"));
			fc.utobj().clickElement(driver, pobj.listView);
			
			fc.utobj().printTestStep("Modify The Document and changed document type to upload file");
			actionImgIconListView(driver, HtmlDocTitle, "Modify");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String docTitleForDoc=fc.utobj().generateTestData("TestDocTitle");
			fc.utobj().sendKeys(driver, pobj.documentTitle, docTitleForDoc);
			if (!fc.utobj().isSelected(driver, pobj.documentType.get(0))) {
				fc.utobj().clickElement(driver, pobj.documentType.get(0));
			}
			fc.utobj().selectDropDown(driver, pobj.documentSubType, "PDF");
			fc.utobj().moveToElement(driver, pobj.fileClick);
			String fileName = "taskFile_07.pdf";
			String filePath=fc.utobj().getFilePathFromTestData(fileName);
			
			pobj.fileClick.sendKeys(filePath);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify After clicking over Document url document should be downloaded");
			hub_common_method.sortByRecentlyUploadedDocuments(driver);
			actionImgIconListView(driver, docTitleForDoc, "Document Properties");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver,
					".//td[contains(text () , 'Document URL')]/following-sibling::td/a[contains(@href,'"
							+ FranconnectUtil.config.get("buildUrl") + "')]");

			fc.utobj().printTestStep("Verify The Downloaded File");
			boolean isFileFound = fc.utobj().isFileFound(new File(FranconnectUtil.config.get("downloadFilepath")),
					fileName);
			if (!isFileFound) {
				fc.utobj().throwsException("Document not downled from document properties url");
			}
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);
			
			//Delete Downloaded Document
			library.setFile(fileName);
			hub_common_method.deleteDownloadedFile(library);
			
			fc.utobj().printTestStep("Verify Html Document At Archived Document Tab : List View");
			fc.utobj().clickElement(driver, pobj.archivedDocumentsTab);
			fc.utobj().clickElement(driver, pobj.listView);
			hub_common_method.sortByRecentlyUploadedDocuments(driver);
			actionImgIconListView(driver, HtmlDocTitle, "Document Properties");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver,
					".//td[contains(text () , 'Document URL')]/following-sibling::td/a[contains(@href,'"
							+ FranconnectUtil.config.get("buildUrl") + "')]");

			fc.utobj().printTestStep("Verify Html Document window should be opened from Archived > List View");
			boolean isHtmlEditortTextPresentArch = false;
			Set<String> windowsHandlesArch = driver.getWindowHandles();
			for (String window : windowsHandlesArch) {
				if (!window.equalsIgnoreCase(parentWindow)) {
					driver.switchTo().window(window);
					if (driver.getCurrentUrl().contains("htmlPageView.jsp")) {
						isHtmlEditortTextPresentArch = fc.utobj().assertPageSource(driver, library.getEditorText());
						driver.close();
						break;
					}
				}
			}
			driver.switchTo().window(parentWindow);
			if (!isHtmlEditortTextPresentArch) {
				fc.utobj().throwsException("HTML documents is not visible");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);
			
			//TC-66384 : Verify Download history for archived html doc documents
			fc.utobj().printTestStep("From Archived section, download count of HTML Document should be increased like other type documents");
			fc.utobj().clickElement(driver, pobj.archivedDocumentsTab);
			fc.utobj().clickElement(driver, pobj.listView);
			hub_common_method.sortByRecentlyUploadedDocuments(driver);
			String downloadCount=fc.utobj().getText(driver, fc.utobj().getElementByXpath(driver, ".//*[.='"+library.getDocumentTitle()+"' and  @class='textTheme14 ']/following-sibling::div/strong[1]"));
			if (Integer.parseInt(downloadCount)!=1) {
				fc.utobj().throwsException("From Archived section, download count of Html Document is not increasing like other type documents");
			}
			
			fc.utobj().printTestStep("Verify at Archived Document Tab : Thumb View");
			fc.utobj().clickElement(driver, pobj.thumbnailView);
			hub_common_method.sortByRecentlyUploadedDocuments(driver);
			thumbnailOption(driver, HtmlDocTitle, "Document Properties");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver,
					".//td[contains(text () , 'Document URL')]/following-sibling::td/a[contains(@href,'"
							+ FranconnectUtil.config.get("buildUrl") + "')]");

			fc.utobj().printTestStep("Verify Html Document window should be opened from Archived > Thumb View");

			boolean isHtmlEditortTextPresentArcht = false;
			Set<String> windowsHandlesArcht = driver.getWindowHandles();
			for (String window : windowsHandlesArcht) {
				if (!window.equalsIgnoreCase(parentWindow)) {
					driver.switchTo().window(window);
					if (driver.getCurrentUrl().contains("htmlPageView.jsp")) {
						isHtmlEditortTextPresentArcht = fc.utobj().assertPageSource(driver, library.getEditorText());
						driver.close();
						break;
					}
				}
			}
			driver.switchTo().window(parentWindow);
			if (!isHtmlEditortTextPresentArcht) {
				fc.utobj().throwsException("HTML documents is not visible");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);
			
			fc.utobj().printTestStep("Verify at Archived Document Tab : Details View");
			fc.utobj().clickElement(driver, pobj.detailView);

			hub_common_method.sortByRecentlyUploadedDocuments(driver);
			actionImgIconDetailsView(driver, HtmlDocTitle, "Document Properties");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver,
					".//td[contains(text () , 'Document URL')]/following-sibling::td/a[contains(@href,'"
							+ FranconnectUtil.config.get("buildUrl") + "')]");

			fc.utobj().printTestStep("Verify Html Document window should be opened from Archived > Details View");

			boolean isHtmlEditortTextPresentArchD = false;
			Set<String> windowsHandlesArchD = driver.getWindowHandles();
			for (String window : windowsHandlesArchD) {
				if (!window.equalsIgnoreCase(parentWindow)) {
					driver.switchTo().window(window);
					if (driver.getCurrentUrl().contains("htmlPageView.jsp")) {
						isHtmlEditortTextPresentArchD = fc.utobj().assertPageSource(driver, library.getEditorText());
						driver.close();
						break;
					}
				}
			}
			driver.switchTo().window(parentWindow);
			if (!isHtmlEditortTextPresentArchD) {
				fc.utobj().throwsException("HTML documents is not visible");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);
						
			//Modify And Set Link Document
			fc.utobj().printTestStep("Verify After clicking over Document url Link Url window should be open");
			clickOverMoreLink(driver, folderName);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//span[contains(text(),'" + folderName + "')]"));
			fc.utobj().clickElement(driver, pobj.listView);

			fc.utobj().printTestStep("Modify The Document and changed document type to Web Url");
			actionImgIconDetailsView(driver, docTitleForDoc, "Modify");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String docTitleForLink=fc.utobj().generateTestData("TestDocTitle");
			fc.utobj().sendKeys(driver, pobj.documentTitle, docTitleForLink);
			if (!fc.utobj().isSelected(driver, pobj.documentType.get(1))) {
				fc.utobj().clickElement(driver, pobj.documentType.get(1));
			}

			String lnkUrl = "https://in.yahoo.com/";
			fc.utobj().sendKeys(driver, pobj.linkUrl, lnkUrl);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Test Web Url");
			clickOverMoreLink(driver, folderName);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//span[contains(text(),'" + folderName + "')]"));
			fc.utobj().clickElement(driver, pobj.listView);

			hub_common_method.sortByRecentlyUploadedDocuments(driver);
			actionImgIconListView(driver, docTitleForLink, "Document Properties");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver,
					".//td[contains(text () , 'Document URL')]/following-sibling::td/a[contains(@href,'"
							+ FranconnectUtil.config.get("buildUrl") + "')]");

			boolean isLinkUrlWIndowOpen = false;
			Set<String> windowsHandlesLink = driver.getWindowHandles();
			for (String window : windowsHandlesLink) {
				if (!window.equalsIgnoreCase(parentWindow)) {
					driver.switchTo().window(window);
					if (driver.getCurrentUrl().toLowerCase().contains("yahoo")) {
						isLinkUrlWIndowOpen = true;
						driver.close();
						break;
					}
				}
			}
			driver.switchTo().window(parentWindow);
			if (!isLinkUrlWIndowOpen) {
				fc.utobj().throwsException("Link is not opening");
			}
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);
			
			//Verify The Upload Doc At Archived Tab : List View
			fc.utobj().printTestStep("Verify After clicking over Document url document should be downloaded");
			fc.utobj().clickElement(driver, pobj.archivedDocumentsTab);
			fc.utobj().clickElement(driver, pobj.listView);
			hub_common_method.sortByRecentlyUploadedDocuments(driver);
			actionImgIconListView(driver, docTitleForDoc, "Document Properties");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver,
					".//td[contains(text () , 'Document URL')]/following-sibling::td/a[contains(@href,'"
							+ FranconnectUtil.config.get("buildUrl") + "')]");

			fc.utobj().printTestStep("Verify The Downloaded File");
			boolean isFileFound12 = fc.utobj().isFileFound(new File(FranconnectUtil.config.get("downloadFilepath")),
					fileName);
			if (!isFileFound12) {
				fc.utobj().throwsException("Document not downled from document properties url");
			}
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);
			
			//Delete File
			hub_common_method.deleteDownloadedFile(library);
			
			
			//Verify The Upload Doc At Archived Tab : Thumb View
			fc.utobj().printTestStep("Verify After clicking over Document url document should be downloaded");
			fc.utobj().clickElement(driver, pobj.thumbnailView);
			hub_common_method.sortByRecentlyUploadedDocuments(driver);
			thumbnailOption(driver, docTitleForDoc, "Document Properties");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver,
					".//td[contains(text () , 'Document URL')]/following-sibling::td/a[contains(@href,'"
							+ FranconnectUtil.config.get("buildUrl") + "')]");

			fc.utobj().printTestStep("Verify The Downloaded File");
			boolean isFileFound122 = fc.utobj().isFileFound(new File(FranconnectUtil.config.get("downloadFilepath")),
					fileName);
			if (!isFileFound122) {
				fc.utobj().throwsException("Document not downled from document properties url");
			}
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);
			
			//Delete File
			hub_common_method.deleteDownloadedFile(library);
			
			//Verify The Upload Doc At Archived Tab : Details View
			fc.utobj().printTestStep("Verify After clicking over Document url document should be downloaded");
			fc.utobj().clickElement(driver, pobj.detailView);
			hub_common_method.sortByRecentlyUploadedDocuments(driver);
			actionImgIconDetailsView(driver, docTitleForDoc, "Document Properties");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver,
					".//td[contains(text () , 'Document URL')]/following-sibling::td/a[contains(@href,'"
							+ FranconnectUtil.config.get("buildUrl") + "')]");

			fc.utobj().printTestStep("Verify The Downloaded File");
			boolean isFileFound1222 = fc.utobj().isFileFound(new File(FranconnectUtil.config.get("downloadFilepath")),
					fileName);
			if (!isFileFound1222) {
				fc.utobj().throwsException("Document not downled from document properties url");
			}
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);
			
			//Delete File
			hub_common_method.deleteDownloadedFile(library);
			
			//Modify The Link Url And Change Into upload doc
			clickOverMoreLink(driver, folderName);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//span[contains(text(),'" + folderName + "')]"));
			fc.utobj().clickElement(driver, pobj.listView);
			fc.utobj().printTestStep("Modify The Document and changed document type to upload");
			actionImgIconDetailsView(driver, HtmlDocTitle, "Modify");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			if (!fc.utobj().isSelected(driver, pobj.documentType.get(0))) {
				fc.utobj().clickElement(driver, pobj.documentType.get(0));
			}

			fc.utobj().selectDropDown(driver, pobj.documentSubType, "PDF");
			fc.utobj().moveToElement(driver, pobj.fileClick);
			pobj.fileClick.sendKeys(filePath);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);
			
			//Verify The Web Link In Archived : List View
			fc.utobj().clickElement(driver, pobj.archivedDocumentsTab);
			fc.utobj().clickElement(driver, pobj.listView);
			hub_common_method.sortByRecentlyUploadedDocuments(driver);
			actionImgIconListView(driver, docTitleForLink, "Document Properties");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver,
					".//td[contains(text () , 'Document URL')]/following-sibling::td/a[contains(@href,'"
							+ FranconnectUtil.config.get("buildUrl") + "')]");

			boolean isLinkUrlWIndowArchive = false;
			Set<String> windowsHandlesLinkArchive = driver.getWindowHandles();
			for (String window : windowsHandlesLinkArchive) {
				if (!window.equalsIgnoreCase(parentWindow)) {
					driver.switchTo().window(window);
					if (driver.getCurrentUrl().toLowerCase().contains("yahoo")) {
						isLinkUrlWIndowArchive = true;
						driver.close();
						break;
					}
				}
			}
			driver.switchTo().window(parentWindow);
			if (!isLinkUrlWIndowArchive) {
				fc.utobj().throwsException("Link is not opening");
			}
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);
			
			//Verify The Web Link In Archived : List View
			fc.utobj().clickElement(driver, pobj.archivedDocumentsTab);
			fc.utobj().clickElement(driver, pobj.listView);
			hub_common_method.sortByRecentlyUploadedDocuments(driver);
			actionImgIconListView(driver, docTitleForLink, "Document Properties");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver,
					".//td[contains(text () , 'Document URL')]/following-sibling::td/a[contains(@href,'"
							+ FranconnectUtil.config.get("buildUrl") + "')]");

			boolean isLinkUrlWIndowArchive1 = false;
			Set<String> windowsHandlesLinkArchive1 = driver.getWindowHandles();
			for (String window : windowsHandlesLinkArchive1) {
				if (!window.equalsIgnoreCase(parentWindow)) {
					driver.switchTo().window(window);
					if (driver.getCurrentUrl().toLowerCase().contains("yahoo")) {
						isLinkUrlWIndowArchive1 = true;
						driver.close();
						break;
					}
				}
			}
			driver.switchTo().window(parentWindow);
			if (!isLinkUrlWIndowArchive1) {
				fc.utobj().throwsException("Link is not opening");
			}
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);
			
			//Verify The Web Link In Archived : Thumb View
			fc.utobj().clickElement(driver, pobj.archivedDocumentsTab);
			fc.utobj().clickElement(driver, pobj.thumbnailView);
			hub_common_method.sortByRecentlyUploadedDocuments(driver);
			thumbnailOption(driver, docTitleForLink, "Document Properties");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver,
					".//td[contains(text () , 'Document URL')]/following-sibling::td/a[contains(@href,'"
							+ FranconnectUtil.config.get("buildUrl") + "')]");

			boolean isLinkUrlWIndowArchiveTh = false;
			Set<String> windowsHandlesLinkArchiveTh = driver.getWindowHandles();
			for (String window : windowsHandlesLinkArchiveTh) {
				if (!window.equalsIgnoreCase(parentWindow)) {
					driver.switchTo().window(window);
					if (driver.getCurrentUrl().toLowerCase().contains("yahoo")) {
						isLinkUrlWIndowArchiveTh = true;
						driver.close();
						break;
					}
				}
			}
			driver.switchTo().window(parentWindow);
			if (!isLinkUrlWIndowArchiveTh) {
				fc.utobj().throwsException("Link is not opening");
			}
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			
			//Verify The Web Link In Archived : Details
			fc.utobj().clickElement(driver, pobj.archivedDocumentsTab);
			fc.utobj().clickElement(driver, pobj.detailView);
			hub_common_method.sortByRecentlyUploadedDocuments(driver);
			actionImgIconDetailsView(driver, docTitleForLink, "Document Properties");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver,
					".//td[contains(text () , 'Document URL')]/following-sibling::td/a[contains(@href,'"
							+ FranconnectUtil.config.get("buildUrl") + "')]");

			boolean isLinkUrlWIndowArchiveDV = false;
			Set<String> windowsHandlesLinkArchiveThDV = driver.getWindowHandles();
			for (String window : windowsHandlesLinkArchiveThDV) {
				if (!window.equalsIgnoreCase(parentWindow)) {
					driver.switchTo().window(window);
					if (driver.getCurrentUrl().toLowerCase().contains("yahoo")) {
						isLinkUrlWIndowArchiveDV = true;
						driver.close();
						break;
					}
				}
			}
			driver.switchTo().window(parentWindow);
			if (!isLinkUrlWIndowArchiveDV) {
				fc.utobj().throwsException("Link is not opening");
			}
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	public void addDocument(WebDriver driver, Library library) throws Exception {
		try {
			AdminTheHubLibraryPage pobj = new AdminTheHubLibraryPage(driver);
			try {
				fc.utobj().clickElement(driver, pobj.homeTab);
			} catch (Exception e) {
			}finally {
			}
			fc.utobj().sleep(2000);
			fc.utobj().clickElement(driver, pobj.addDocument);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			if (!fc.utobj().isSelected(driver, pobj.multipleDoc.get(1))) {
				fc.utobj().clickElement(driver, pobj.multipleDoc.get(1));
			}
			try {
				fc.utobj().selectDropDown(driver, pobj.folderSelect, library.getFolderName());
			} catch (Exception e) {
			}
			fc.utobj().sendKeys(driver, pobj.documentTitle, library.getDocumentTitle());
			fc.utobj().sendKeys(driver, pobj.briefSummary, library.getBriefSummary());

			if (library.getTypeOfDocument()!=null && library.getTypeOfDocument().equalsIgnoreCase("Draft")) {
				fc.utobj().sendKeys(driver, pobj.displayDate, library.getFutureDate());
				fc.utobj().sendKeys(driver, pobj.expirationDate, library.getSecondFutureDate());
			}else {
				if (library.getCurrentDate()!=null) {
					fc.utobj().sendKeys(driver, pobj.displayDate, library.getCurrentDate());
				}
				fc.utobj().sendKeys(driver, pobj.expirationDate, library.getFutureDate());
			}
			
			if (library.getDocumentType()!=null && library.getDocumentType().equalsIgnoreCase("Upload File")) {

				if (library.getDocumentSubType() != null) {
					fc.utobj().selectDropDown(driver, pobj.documentSubType, library.getDocumentSubType());
				}
				String file = fc.utobj().getFilePathFromTestData(library.getFile());

				if (!fc.utobj().isSelected(driver, pobj.documentType.get(0))) {
					fc.utobj().clickElement(driver, pobj.documentType.get(0));
				}

				fc.utobj().moveToElement(driver, pobj.fileClick);
				pobj.fileClick.sendKeys(file);

			} else if (library.getDocumentType()!=null && library.getDocumentType().equalsIgnoreCase("Web Link")) {

				if (!fc.utobj().isSelected(driver, pobj.documentType.get(1))) {
					fc.utobj().clickElement(driver, pobj.documentType.get(1));
				}

				fc.utobj().sendKeys(driver, pobj.linkUrl, library.getLinkUrl());

			} else if (library.getDocumentType()!=null && library.getDocumentType().equalsIgnoreCase("HTML Page")) {

				String editorText = fc.utobj().generateTestData(library.getEditorText());

				if (!fc.utobj().isSelected(driver, pobj.documentType.get(2))) {
					fc.utobj().clickElement(driver, pobj.documentType.get(2));
				}

				fc.utobj().switchFrameById(driver, "htmlSummary_ifr");

				Actions actions = new Actions(driver);
				actions.moveToElement(fc.utobj().getElement(driver, pobj.editorTextArea));
				actions.click();
				actions.sendKeys(editorText);
				fc.utobj().logReportMsg("Entered Text", editorText);
				actions.build().perform();
				fc.utobj().switchFrameToDefault(driver);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
			}

			if (!fc.utobj().isSelected(driver, pobj.thumbFileUploaded.get(1))) {
				fc.utobj().clickElement(driver, pobj.thumbFileUploaded.get(1));
			}

			if (library.getRecommendedDoc()) {
				fc.utobj().clickElement(driver, pobj.recommmendedDocumnet);
				fc.utobj().sendKeys(driver, pobj.recommendedExpDate, fc.utobj().getFutureDateUSFormat(4));

			} else {
				if (fc.utobj().isSelected(driver, pobj.recommmendedDocumnet)) {
					fc.utobj().clickElement(driver, pobj.recommmendedDocumnet);
				}
			}

			if (!fc.utobj().isSelected(driver, pobj.accessibilityPublic)) {
				fc.utobj().clickElement(driver, pobj.accessibilityPublic);
			}

			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().clickElement(driver, pobj.closeConfirmation);
			fc.utobj().switchFrameToDefault(driver);
		} catch (Exception e) {
			Reporter.log("Not able to add Document in folder : " + e.getMessage().toString());
			fc.utobj().throwsException("Not able to add Document in folder");
		}
	}


	@Test(groups = { "thehub", "hub_library"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-07-27", testCaseDescription = "Verify The Move Folders At Admin > The Hub > Library >  Resource Library", testCaseId = "TC_53_Move_Folder")
	private void moveFolders() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminTheHubLibraryPage pobj = new AdminTheHubLibraryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			addFolder(driver, library);
			
			fc.utobj().printTestStep("Add Document in folder");
			String documentTitle = fc.utobj().generateTestData("TestDocTitle");
			String briefSummary = fc.utobj().generateTestData("Test Brief Summary");
			String currentDate = fc.utobj().getCurrentDateUSFormat();
			String futureDate = fc.utobj().getFutureDateUSFormat(2);
			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setCurrentDate(currentDate);
			library.setFutureDate(futureDate);
			library.setDocumentType("Web Link");
			library.setLinkUrl("https://in.yahoo.com/");
			library.setRecommendedDoc(false);
			addDocument(driver, library);
			
			Library libraryHtml = new Library();
			fc.utobj().printTestStep("Add Html Type Document Type");
			fc.utobj().printTestStep("Add Document in folder");
			String documentTitleHtml = fc.utobj().generateTestData("TestDocTitle");
			String briefSummaryHtml = fc.utobj().generateTestData("Test Brief Summary");
			String currentDateHtml = fc.utobj().getCurrentDateUSFormat();
			String futureDateHtml = fc.utobj().getFutureDateUSFormat(2);
			libraryHtml.setDocumentTitle(documentTitleHtml);
			libraryHtml.setBriefSummary(briefSummaryHtml);
			libraryHtml.setCurrentDate(currentDateHtml);
			libraryHtml.setFutureDate(futureDateHtml);
			libraryHtml.setDocumentType("HTML Page");
			libraryHtml.setEditorText(fc.utobj().generateTestData("Test Editor Text"));
			libraryHtml.setRecommendedDoc(false);
			libraryHtml.setFolderName(folderName);
			addDocument(driver, libraryHtml);
			
			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Another Folder");
			String folderName1 = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary1 = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat1 = dataSet.get("summaryFormat");
			library.setFolderName(folderName1);
			library.setFolderSummary(folderSummary1);
			library.setSummaryFormat(summaryFormat1);
			library.setAccessTo(accessTo);
			addFolder(driver, library);

			fc.utobj().printTestStep("Move Folder");
			Actions builder = new Actions(driver);
			builder.moveToElement(fc.utobj().getElement(driver, pobj.moreActions))
					.click(fc.utobj().getElement(driver, pobj.moreActions));
			builder.perform();
			fc.utobj().getElement(driver, pobj.moveFolders).click();

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().selectDropDown(driver, pobj.moveFromSelect, folderName);
			fc.utobj().selectDropDown(driver, pobj.moveToSelect, folderName1);
			fc.utobj().clickElement(driver, pobj.moveBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			clickOverMoreLink(driver, folderName1);
			fc.utobj().printTestStep("Verify The Move Folders");
			fc.utobj().clickElement(driver,fc.utobj().getElementByXpath(driver, ".//*[contains(text () , '" + folderName1 + "')]"));
			fc.utobj().isTextDisplayed(driver, folderSummary1, "was not able to verify Summary of Second Folder");
			
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[.='" + folderName + "' and @class='wrapmenu bText12']"));

			fc.utobj().isTextDisplayed(driver, folderName1, "was not able to verify Second Folder");
			fc.utobj().isTextDisplayed(driver, folderName, "was not able to verify First Folder Name");
			fc.utobj().isTextDisplayed(driver, folderSummary, "Was not able to verify First Folder Summary");
			
			if(!fc.utobj().isElementPresent(driver, fc.utobj().getElementByXpath(driver, ".//a[.='"+documentTitle+"' and @class='textTheme14 ']")))
				fc.utobj().throwsException("Not able to verify the moved folder document");
			
			fc.utobj().printTestStep("Verify Web Link move folder / documents functionality");
			String parentWindow=driver.getWindowHandle();
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@class='textTheme14 ']/*[contains(text(),'"+documentTitle+"')]"));
			
			boolean isLinkUrlWIndowOpen = false;
			Set<String> windowsHandlesLink = driver.getWindowHandles();
			for (String window : windowsHandlesLink) {
				if (!window.equalsIgnoreCase(parentWindow)) {
					driver.switchTo().window(window);
					System.out.println(driver.getCurrentUrl().toLowerCase());
					if (driver.getCurrentUrl().toLowerCase().contains("yahoo")) {
						isLinkUrlWIndowOpen=true;
						driver.close();
						break;
					}
				}
			}
			driver.switchTo().window(parentWindow);
			if (!isLinkUrlWIndowOpen) {
				fc.utobj().throwsException("Link is not opening");
			}
			
			fc.utobj().printTestStep("Html Page Should be open properly");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@class='textTheme14 ']/*[contains(text(),'"+documentTitleHtml+"')]"));
			if(!fc.utobj().assertPageSource(driver, libraryHtml.getEditorText())){
				fc.utobj().throwsException("Html page is not opening");
			}
			
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//div[@id='main']/a[.='"+folderName1+"']"));
			fc.utobj().clickElement(driver,fc.utobj().getElementByXpath(driver, ".//*[@class='wrapmenu bText12' and contains(text(),'"+folderName+"')]"));
			
			fc.utobj().printTestStep("Verify General action options throughout all type views");
			fc.utobj().printTestStep("Verify The Add Document link should be present");
			if (!fc.utobj().isElementPresent(driver, pobj.addDocument)) {
				fc.utobj().throwsException("Add Document link is not present");
			}
			
			fc.utobj().printTestStep("Verify The Modify link should be present");
			if (!fc.utobj().isElementPresent(driver, pobj.modifyFolderLink)) {
				fc.utobj().throwsException("Modify Folder link is not present");
			}
			
			fc.utobj().printTestStep("Verify The Delete Folder link should be present");
			if (!fc.utobj().isElementPresent(driver, pobj.addDocument)) {
				fc.utobj().throwsException("Delete Folder link is not present");
			}
			
			fc.utobj().printTestStep("Verify More Actions and its options");
			fc.utobj().clickElementWithActions(driver, pobj.moreActionsUnderFolder);
			
			if (!fc.utobj().isElementPresent(driver, fc.utobj().getElementByXpath(driver, ".//*[contains(text(),'Add Sub Folder')]"))) {
				fc.utobj().throwsException("Define Folder is not present under More Links");
			}

			if (!fc.utobj().isElementPresent(driver, fc.utobj().getElementByXpath(driver, ".//*[contains(text(),'Define Sub Folder Sequence')]"))) {
				fc.utobj().throwsException("Define Sub Folder Sequence is not present under More Links");
			}
			
			if (!fc.utobj().isElementPresent(driver, fc.utobj().getElementByXpath(driver, ".//*[contains(text(),'Define Document Sequence')]"))) {
				fc.utobj().throwsException("Define Document Sequence is not present under More Links");
			}

			fc.utobj().printTestStep("Verify The Actions options of list view");
			fc.utobj().clickElement(driver, pobj.listView);
			WebElement element = fc.utobj().getElementByXpath(driver, ".//*[contains(text () ,'" + documentTitle
					+ "')]/ancestor::table[@class='selectBdr']//tr/td//*[contains(@onclick, 'showActionOver')]");
			fc.utobj().moveToElement(driver, element);
			String text = element.getAttribute("onclick");
			text = text.replace("showActionOver(event,'", "");
			text = text.replace("')", "");
			try {
				fc.utobj().clickElement(driver, element);
			} catch (Exception e) {
				fc.utobj().clickElementWithoutMove(driver, element);
			}
			
			List<WebElement> listEl=fc.utobj().getElementListByXpath(driver,".//*[@id='actionListButtons"+text+"']//span");
			ArrayList<String> arrList=new ArrayList<>();
			
			for (WebElement listElement : listEl) {
				arrList.add(fc.utobj().getText(driver, listElement));
			}
			
			ArrayList<String> input=new ArrayList<>();
			input.add("Report");
			input.add("Modify");
			input.add("Delete");
			input.add("Copy URL");
			input.add("Document Properties");
			input.add("Version History");
			
			ArrayList<String> al3= new ArrayList<String>();
			
			for (String string : arrList) {
				al3.add(input.contains(string) ? "Yes" : "No");
			}
			
			if (al3.get(0).equalsIgnoreCase("No"))
				fc.utobj().throwsException("Report options is not visible");			
			
			if (al3.get(1).equalsIgnoreCase("No"))
				fc.utobj().throwsException("Modify options is not visible");
			
			if (al3.get(2).equalsIgnoreCase("No"))
				fc.utobj().throwsException("Delete options is not visible");		
			
			if (al3.get(3).equalsIgnoreCase("No"))
				fc.utobj().throwsException("Copy URL options is not visible");
			
			if (al3.get(4).equalsIgnoreCase("No"))
				fc.utobj().throwsException("Document Properties options is not visible");		
			
			if (al3.get(5).equalsIgnoreCase("No"))
				fc.utobj().throwsException("Version History options is not visible");
			
			fc.utobj().printTestStep("Verify The Actions options of Thumbnail view");
			fc.utobj().clickElement(driver, pobj.thumbnailView);
			
			if(!fc.utobj().isElementPresent(driver, pobj.modifyDocument))
				fc.utobj().throwsException("Modify Button icon is not present at thumbnail view");
			
			if(!fc.utobj().isElementPresent(driver, pobj.deleteDocument))
				fc.utobj().throwsException("Delete Button icon is not present at thumbnail view");
			
			WebElement elementThumb = fc.utobj().getElementByXpath(driver, ".//*[@id='thums']//*[.='" + documentTitle + "']"
					+ "//following::table[@class='topLine pt5'][1]//td/a[contains(@onclick, 'showActionOver')]");

			String textThumb = elementThumb.getAttribute("onclick").trim();
			textThumb = textThumb.replace("showActionOver(event,'", "");
			textThumb = textThumb.replace("')", "");

			try {
				fc.utobj().clickElementWithoutMove(driver, elementThumb);
			} catch (Exception e) {
				fc.utobj().clickElement(driver, elementThumb);
			}
			
			List<WebElement> listElThumb=fc.utobj().getElementListByXpath(driver,".//*[@id='actionListButtons"+textThumb+"']//span");
			ArrayList<String> arrListThumb=new ArrayList<>();
			
			for (WebElement listElement : listElThumb) {
				arrListThumb.add(fc.utobj().getText(driver, listElement));
			}
			
			ArrayList<String> inputThumb=new ArrayList<>();
			inputThumb.add("Report");
			inputThumb.add("Copy URL");
			inputThumb.add("Document Properties");
			inputThumb.add("Post Comment");
			inputThumb.add("Version History");
			
			ArrayList<String> al4= new ArrayList<String>();
			for (String string : arrListThumb) {
				al4.add(inputThumb.contains(string) ? "Yes" : "No");
			}
			
			if (al4.get(0).equalsIgnoreCase("No"))
				fc.utobj().throwsException("Report options is not visible");			
			
			if (al4.get(1).equalsIgnoreCase("No"))
				fc.utobj().throwsException("Copy URL options is not visible");
			

			if (al4.get(2).equalsIgnoreCase("No"))
				fc.utobj().throwsException("Document Properties options is not visible");
			
			if (al4.get(3).equalsIgnoreCase("No"))
				fc.utobj().throwsException("Post Comment options is not visible");		
			
			if (al4.get(4).equalsIgnoreCase("No"))
				fc.utobj().throwsException("Version History options is not visible");
			
			fc.utobj().printTestStep("Verify The Actions options of Details view");
			fc.utobj().clickElement(driver, pobj.detailView);
			
			WebElement elementDetails = fc.utobj().getElementByXpath(driver, ".//table[@id='DtlsInner']//*[contains(text(),'"
					+ documentTitle + "')]/ancestor::tr/td/following-sibling::td/a[contains(@onclick ,'showActionOver')]");
			fc.utobj().moveToElement(driver, elementDetails);
			String textDetails = elementDetails.getAttribute("onclick");
			textDetails = textDetails.replace("showActionOver(event,'", "");
			textDetails = textDetails.replace("')", "");
			try {
				fc.utobj().clickElement(driver, elementDetails);
			} catch (Exception e) {
				fc.utobj().clickElementWithoutMove(driver, elementDetails);
			}
			
			List<WebElement> listElDetails=fc.utobj().getElementListByXpath(driver,".//*[@id='actionListButtons"+textDetails+"']//span");
			ArrayList<String> arrListDetails=new ArrayList<>();
			
			for (WebElement listElement : listElDetails) {
				arrListDetails.add(fc.utobj().getText(driver, listElement));
			}
			
			ArrayList<String> inputDetails=new ArrayList<>();
			inputDetails.add("Report");
			inputDetails.add("Modify");
			inputDetails.add("Delete");
			inputDetails.add("Copy URL");
			inputDetails.add("Document Properties");
			inputDetails.add("Post Comment");
			inputDetails.add("Version History");
			
			ArrayList<String> al5= new ArrayList<String>();
			for (String string : arrListDetails) {
				al5.add(inputDetails.contains(string) ? "Yes" : "No");
			}
			
			if (al5.get(0).equalsIgnoreCase("No"))
				fc.utobj().throwsException("Report options is not visible");	
			
			if (al5.get(1).equalsIgnoreCase("No"))
				fc.utobj().throwsException("Modify options is not visible");	
			
			if (al5.get(2).equalsIgnoreCase("No"))
				fc.utobj().throwsException("Delete options is not visible");	
			
			if (al5.get(3).equalsIgnoreCase("No"))
				fc.utobj().throwsException("Copy URL options is not visible");
			
			if (al5.get(4).equalsIgnoreCase("No"))
				fc.utobj().throwsException("Document Properties options is not visible");
			
			if (al5.get(5).equalsIgnoreCase("No"))
				fc.utobj().throwsException("Post Comment options is not visible");		
			
			if (al5.get(6).equalsIgnoreCase("No"))
				fc.utobj().throwsException("Version History options is not visible");

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library" ,"hubfailed0928_1","hubf1121"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-07-27", testCaseDescription = "Verify The Move Documents At Admin > The Hub > Library >  Resource Library", testCaseId = "TC_54_Move_Documents")
	private void moveDocuments() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminTheHubLibraryPage pobj = new AdminTheHubLibraryPage(driver);

			//Verify that Search is working properly when document is moved from one folder to another
			
			//Add Corporate User
			fc.utobj().printTestStep("Verify By The Corporate User that folder shoud be searched by folder name and description");
			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users Page");
			
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUserPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "hubautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corporateUserPage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			
			//Add Divisional User
			fc.utobj().printTestStep("Add Divisional User");
			AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest divisional_user=new AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest();
			String userName=fc.utobj().generateTestData("Test50DivUser");
			String divisionName=fc.utobj().generateTestData("Test50DivName");
			String password="fran1234";
			AdminDivisionAddDivisionPageTest p1 = new AdminDivisionAddDivisionPageTest();
			p1.addDivision(driver, divisionName);
			divisional_user.addDivisionalUser(driver, userName, password, divisionName, emailId);
			
			//Add Regional User
			AdminAreaRegionAddAreaRegionPageTest addRegionPage = new AdminAreaRegionAddAreaRegionPageTest();
			String regionName = fc.utobj().generateTestData("Test50regname");
			addRegionPage.addAreaRegion(driver, regionName);

			fc.utobj().printTestStep("Navigate To Admin > Users > Mange Regional Users Page");
			fc.utobj().printTestStep("Add Regional User");
			AdminUsersManageRegionalUsersAddRegionalUserPageTest regionalUserPage = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
			String userNameReg = fc.utobj().generateTestData("Test50reguser");
			regionalUserPage.addRegionalUser(driver, userNameReg, password, regionName, emailId);
			
			//Add Franchise User
			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			String regionNameFran = "Test50Region".concat(fc.utobj().generateRandomNumber());
			String storeType = "Test50StoreType".concat(fc.utobj().generateRandomNumber());
			String franchiseId = "Test50FranId".concat(fc.utobj().generateRandomNumber());

			AdminFranchiseLocationAddFranchiseLocationPageTest addFranchiseLocation = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String firstName = fc.utobj().generateTestData("Test50");
			franchiseId = addFranchiseLocation.addFranchiseLocation_AllWithFName(driver, franchiseId, regionNameFran,
					storeType, firstName);

			fc.utobj().printTestStep("Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest addFranchiseUser = new AdminUsersManageManageFranchiseUsersPageTest();
			String roleName = "Default Franchise Role";
			String userNameFran = fc.utobj().generateTestData(dataSet.get("userName"));
			addFranchiseUser.addFranchiseUser(driver, userNameFran, password, franchiseId, roleName, emailId);
			
			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			addFolder(driver, library);

			fc.utobj().printTestStep("Add Document");
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String currentDate = fc.utobj().getCurrentDateUSFormat();
			String afterDate = dataSet.get("afterDate");
			int val = Integer.parseInt(afterDate);
			String futureDate = fc.utobj().getFutureDateUSFormat(val);
			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setCurrentDate(currentDate);
			library.setFutureDate(futureDate);
			library.setDocumentSubType(dataSet.get("documentSubType"));
			library.setFile(dataSet.get("fileName"));
			library.setDocumentType(dataSet.get("documentType"));
			library.setRecommendedDoc(false);
			addDocument(driver, library);
			
			//Verify that Library document is being searched on the basis of Name,Description and Content of the document
			
			fc.home_page().logout(driver);
			
			fc.utobj().printTestStep("Login With Corporate User");
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());
			
			library.setSearchBy("Brief Summary");
			if (!hub_common_method.searchItemByGlobalSearch(driver, library)) {
				fc.utobj().throwsException("Not able to search document by document brief summary at corporate user");
			}
			
			fc.home_page().logout(driver);
			
			fc.utobj().printTestStep("Login With Divisional User");
			fc.loginpage().loginWithParameter(driver, userName, password);
			
			if (!hub_common_method.searchItemByGlobalSearch(driver, library)) {
				fc.utobj().throwsException("Not able to search document by document brief summary at divisional");
			}
			
			fc.home_page().logout(driver);
			
			fc.utobj().printTestStep("Login With Regional User");
			fc.loginpage().loginWithParameter(driver, userNameReg, password);
			
			if (!hub_common_method.searchItemByGlobalSearch(driver, library)) {
				fc.utobj().throwsException("Not able to search document by document brief summary at regional user");
			}
			
			fc.home_page().logout(driver);
			
			fc.utobj().printTestStep("Login With Franchise User");
			fc.loginpage().loginWithParameter(driver, userNameFran, password);
			
			if (!hub_common_method.searchItemByGlobalSearch(driver, library)) {
				fc.utobj().throwsException("Not able to search document by document brief summary at franchise user");
			}
			
			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With ADM user");
			
			fc.loginpage().loginWithParameter(driver, FranconnectUtil.config.get("userName"), FranconnectUtil.config.get("password"));
			
			fc.utobj().printTestStep("Go To Admin > The Hub > Library");
			fc.hub().hub_common().adminTheHubLibraryPage(driver);
			
			fc.utobj().printTestStep("Modify Document");
			fc.utobj().clickElement(driver, pobj.indexTab);
			fc.utobj().clickElement(driver, pobj.detailView);
			new HubCommonMethods().sortByRecentlyUploadedDocuments(driver);
			actionImgIconDetailsView(driver, documentTitle, "Modify");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			fc.utobj().sendKeys(driver, pobj.documentTitle, documentTitle);
			briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			fc.utobj().sendKeys(driver, pobj.briefSummary, briefSummary);

			String currentDate1 = fc.utobj().getCurrentDateUSFormat();
			String futureDate1 = fc.utobj().getFutureDateUSFormat(val);
			fc.utobj().sendKeys(driver, pobj.displayDate, currentDate1);
			fc.utobj().sendKeys(driver, pobj.expirationDate, futureDate1);

			if (!fc.utobj().isSelected(driver, pobj.documentType.get(0))) {
				fc.utobj().clickElement(driver, pobj.documentType.get(0));
			}

			fc.utobj().selectDropDown(driver, pobj.documentSubType, dataSet.get("documentSubType"));

			String file = fc.utobj().getFilePathFromTestData(dataSet.get("fileName"));
			fc.utobj().moveToElement(driver, pobj.fileClick);
			pobj.fileClick.sendKeys(file);

			if (!fc.utobj().isSelected(driver, pobj.thumbFileUploaded.get(1))) {
				fc.utobj().clickElement(driver, pobj.thumbFileUploaded.get(1));
			}

			if (!fc.utobj().isSelected(driver, pobj.accessibilityPublic)) {
				fc.utobj().clickElement(driver, pobj.accessibilityPublic);
			}

			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().clickElement(driver, pobj.closeConfirmation);
			fc.utobj().switchFrameToDefault(driver);
			
			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder another folder where to move document");
			String folderName1 = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary1 = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat1 = dataSet.get("summaryFormat");

			library.setFolderName(folderName1);
			library.setFolderSummary(folderSummary1);
			library.setSummaryFormat(summaryFormat1);
			addFolder(driver, library);

			fc.utobj().printTestStep("Move Document document from first folder to another");
			Actions builder = new Actions(driver);
			builder.moveToElement(fc.utobj().getElement(driver, pobj.moreActions))
					.click(fc.utobj().getElement(driver, pobj.moreActions));
			builder.perform();
			fc.utobj().getElement(driver, pobj.moveDocuments).click();
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().selectDropDown(driver, pobj.moveFromSelect, folderName);
			fc.utobj().clickElement(driver, pobj.continueBtn);
			fc.utobj().selectDropDown(driver, pobj.documentItemSelect, documentTitle);
			fc.utobj().selectDropDown(driver, pobj.moveToSelect, folderName1);
			fc.utobj().clickElement(driver, pobj.moveBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Move Documents");
			clickOverMoreLink(driver, folderName);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='folderDiv']//*[contains(text () , '" + folderName1 + "')]"));

			fc.utobj().isTextDisplayed(driver, folderName1, "was not able to verify Folder");
			fc.utobj().isTextDisplayed(driver, folderSummary1, "was not able to verify Folder Summary");

			if (!fc.utobj().getElementByXpath(driver, ".//*[@id='thumDtls']//a[.='" + documentTitle + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document Title after moving the document");
			}
			fc.utobj().isTextDisplayed(driver, briefSummary, "was not able to verify Document Brief Summary");
			
			actionImgIconListView(driver, documentTitle, "Version History");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//td[contains(text(),'FranConnect Administrator')]/preceding-sibling::td/a[contains(text(),'"
									+ fc.utobj().getCurrentYear() + "')]"));
			
			/*if (!fc.utobj().isFileFound(new File(FranconnectUtil.config.get("downloadFilepath")), dataSet.get("fileName"))) {
				fc.utobj().throwsException("Not able to doenload Version History Document");
			}*/
			
			fc.commonMethods().Click_Close_Input_ByValue(driver);
			fc.utobj().switchFrameToDefault(driver);
			
			fc.home_page().logout(driver);
			
			fc.utobj().printTestStep("Verify that Search is working properly when document is moved from one folder to another");
			fc.utobj().printTestStep("Login with corporate user");
			
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());
			
			fc.utobj().printTestStep("Search Document By Document Title");
			library.setSearchBy("Document Title");
			if(!hub_common_method.searchItemByGlobalSearch(driver, library)){
				fc.utobj().throwsException("Not able to search Document by Document Title");
			}
			
			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login with Divisional user");
			
			fc.loginpage().loginWithParameter(driver, userName, password);

			fc.utobj().printTestStep("Search Document By Document Title");
			library.setSearchBy("Document Title");
			if(!hub_common_method.searchItemByGlobalSearch(driver, library)){
				fc.utobj().throwsException("Not able to search Document by Document Title");
			}
			
			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login with Regional user");
			
			fc.loginpage().loginWithParameter(driver, userNameReg, password);
			

			fc.utobj().printTestStep("Search Document By Document Title");
			library.setSearchBy("Document Title");
			if(!hub_common_method.searchItemByGlobalSearch(driver, library)){
				fc.utobj().throwsException("Not able to search Document by Document Title");
			}
			
			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login with Franchise user");
			
			fc.loginpage().loginWithParameter(driver, userNameFran, password);
			

			fc.utobj().printTestStep("Search Document By Document Title");
			library.setSearchBy("Document Title");
			if(!hub_common_method.searchItemByGlobalSearch(driver, library)){
				fc.utobj().throwsException("Not able to search Document by Document Title");
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library","hubf1121" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Verify The Add Document Sub Types At Admin > The Hub > Library >  Resource Library > Configure Document SubType", testCaseId = "TC_56_Add_Document_Sub_Type")
	private void addDocumentSubtype() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminTheHubLibraryPage pobj = new AdminTheHubLibraryPage(driver);

			fc.utobj().printTestStep("Navigate TO Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Document Sub Type");
			fc.hub().hub_common().adminTheHubLibraryPage(driver);

			fc.utobj().clickElementWithActions(driver, pobj.moreActions);
			boolean isDefineFolderSequenceLink = fc.utobj().assertLinkText(driver, "Define Folder Sequence");
			if (!isDefineFolderSequenceLink) {
				fc.utobj().throwsException("Define Folder Sequence is not present under More Links");
			}

			boolean isLibraryDocumentsLogsLink = fc.utobj().assertLinkText(driver, "Library Documents Logs");
			if (!isLibraryDocumentsLogsLink) {
				fc.utobj().throwsException("Library Documents Logs is not present under More Links");
			}

			fc.utobj().clickElementWithActions(driver, pobj.configureDocumentSubtype);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.addDocumentSubType);
			String documentSubTypeTx = fc.utobj().generateTestData(dataSet.get("documentSubTypeTx"));
			fc.utobj().sendKeys(driver, pobj.documentSubTypeTxBx, documentSubTypeTx);
			fc.utobj().clickElement(driver, pobj.addBtn1);
			fc.utobj().clickElement(driver, pobj.closeBtn);

			fc.utobj().printTestStep("Verify The Document Sub Type");
			boolean isValPresent = fc.utobj().inSelectBox(driver, pobj.templateOrder, documentSubTypeTx);
			if (isValPresent == false) {
				fc.utobj().throwsException("was not able to add Document Subtype ");
			}
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, pobj.addDocument);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			boolean isValPresent1 = fc.utobj().inSelectBox(driver, pobj.documentSubType, documentSubTypeTx);
			if (isValPresent1 == false) {
				fc.utobj().throwsException("was not able to verify Document Subtypeat add Document Page");
			}
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void addSubType(WebDriver driver, String documentSubTypeTx) throws Exception {

		String testCaseId = "TC_Add_Sub_Type_Admin_Library";

		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminTheHubLibraryPage pobj = new AdminTheHubLibraryPage(driver);
				fc.hub().hub_common().adminTheHubLibraryPage(driver);
				Actions builder = new Actions(driver);
				builder.moveToElement(fc.utobj().getElement(driver, pobj.moreActions))
						.click(fc.utobj().getElement(driver, pobj.moreActions));
				builder.perform();
				fc.utobj().getElement(driver, pobj.configureDocumentSubtype).click();
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().clickElement(driver, pobj.addDocumentSubType);
				fc.utobj().sendKeys(driver, pobj.documentSubTypeTxBx, documentSubTypeTx);
				fc.utobj().clickElement(driver, pobj.addBtn1);
				fc.utobj().clickElement(driver, pobj.closeBtn);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to Add SubType");

		}
	}

	@Test(groups = { "thehub", "hub_library" ,"hubf1121"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Verify The Modify Document Sub Type  At Admin > The Hub > Library >  Resource Library > Configure Document SubType", testCaseId = "TC_57_Modify_Document_Sub_Type")
	private void modifyDocumentSubType() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate TO Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Document Sub Type");
			AdminTheHubLibraryPage pobj = new AdminTheHubLibraryPage(driver);
			String documentSubTypeTx = fc.utobj().generateTestData(dataSet.get("documentSubTypeTx"));
			addSubType(driver, documentSubTypeTx);

			fc.utobj().printTestStep("Modify Document Sub Type");
			fc.utobj().selectDropDown(driver, pobj.templateOrder, documentSubTypeTx);
			fc.utobj().clickElement(driver, pobj.modifyBtn);
			String subTypeText = fc.utobj().generateTestData(dataSet.get("documentSubTypeTx"));
			fc.utobj().sendKeys(driver, pobj.documentSubTypeTxBx, subTypeText);
			fc.utobj().clickElement(driver, pobj.modifyBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);

			fc.utobj().printTestStep("Verify The Document Sub Type");
			boolean isValPresent = fc.utobj().inSelectBox(driver, pobj.templateOrder, subTypeText);
			if (isValPresent == false) {
				fc.utobj().throwsException("was not able to modify Document subType");
			}
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library","hubf1121" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Verify the Delete Document Sub Type At Admin > The Hub > Library >  Resource Library > Configure Document SubType", testCaseId = "TC_58_Delete_Document_Sub_Type")
	private void deleteDocumentSubType() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate TO Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Document Sub Type");
			AdminTheHubLibraryPage pobj = new AdminTheHubLibraryPage(driver);
			String documentSubTypeTx = fc.utobj().generateTestData(dataSet.get("documentSubTypeTx"));
			addSubType(driver, documentSubTypeTx);

			fc.utobj().printTestStep("Delete Document Sub Type");
			fc.utobj().selectDropDown(driver, pobj.templateOrder, documentSubTypeTx);
			fc.utobj().clickElement(driver, pobj.deleteBtn);
			fc.utobj().acceptAlertBox(driver);
			fc.utobj().sleep(2500);

			fc.utobj().printTestStep("Verify The Delete Document Sub Type");
			boolean isValPresent = fc.utobj().inSelectBox(driver, pobj.templateOrder, documentSubTypeTx);
			if (isValPresent) {
				fc.utobj().throwsException("was not able to delete Document subType");
			}
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "thehubtestFailed", "hub_library","hubf1121"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Verify Recommeneded Document At Admin > The Hub > Library > Index ", testCaseId = "TC_59_Recommended_Document")
	private void verifyRecommendedDocument() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminTheHubLibraryPage pobj = new AdminTheHubLibraryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String parenWindow = driver.getWindowHandle();
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "Regional";
			String htmlText = dataSet.get("htmlText");

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			library.setHtmlText(htmlText);
			addFolder(driver, library);
			
			fc.utobj().printTestStep("Add Recomnded Document");
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String currentDate = fc.utobj().getCurrentDateUSFormat();
			String afterDate = dataSet.get("afterDate");
			int val = Integer.parseInt(afterDate);
			String futureDate = fc.utobj().getFutureDateUSFormat(val);
			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setCurrentDate(currentDate);
			library.setFutureDate(futureDate);
			library.setDocumentSubType(dataSet.get("documentSubType"));
			library.setLinkUrl(dataSet.get("linkUrl"));
			library.setDocumentType(dataSet.get("documentType"));
			library.setRecommendedDoc(true);

			addDocument(driver, library);
			fc.utobj().clickElement(driver, pobj.indexTab);

			fc.utobj().clickElement(driver, pobj.recommendedDocumentsLink);

			fc.utobj().printTestStep("Verify Recomnded Document");

			if (fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[@id='thumDtls']//a[.='" + documentTitle + "']") == false) {
				fc.utobj().throwsException("was not able to verify Documnet Title");
			}
			fc.utobj().isTextDisplayed(driver, briefSummary, "was not able to verify Document Brief Summary");

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='thumDtls']//a[.='" + documentTitle + "']"));

			Set<String> allWindows = driver.getWindowHandles();
			for (String currentWindow : allWindows) {

				if (!currentWindow.equalsIgnoreCase(parenWindow)) {

					driver.switchTo().window(currentWindow);

					boolean isTextPresent = driver.getPageSource().contains("The requested URL could not be retrieved");
					if (isTextPresent == false) {
						driver.close();
					} else {
						String text = driver.getCurrentUrl().trim();
						System.out.println(text);
						text = text.replace("http://", "");
						if (!text.equalsIgnoreCase(dataSet.get("linkUrl"))) {
							fc.utobj().throwsException("was not able to verify WebLink Type Document");
						}

						driver.close();
					}
					driver.switchTo().window(parenWindow);
				}
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Verify The Report Document At Admin > The Hub > Library >  Index[Detail View]", testCaseId = "TC_60_Report_Document")
	private void reportDocumentActionImg01() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			AdminTheHubLibraryPage pobj = new AdminTheHubLibraryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			addFolder(driver, library);

			fc.utobj().printTestStep("Add Recomnded Document");

			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));

			String currentDate = fc.utobj().getCurrentDateUSFormat();
			String afterDate = dataSet.get("afterDate");
			int val = Integer.parseInt(afterDate);
			String futureDate = fc.utobj().getFutureDateUSFormat(val);

			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setCurrentDate(currentDate);
			library.setFutureDate(futureDate);
			library.setDocumentSubType(dataSet.get("documentSubType"));
			library.setFile(dataSet.get("fileName"));
			library.setDocumentType(dataSet.get("documentType"));
			library.setRecommendedDoc(false);
			addDocument(driver, library);

			fc.utobj().printTestStep("Filter Documents by Filter");

			fc.utobj().clickElement(driver, pobj.indexTab);
			fc.utobj().clickElement(driver, pobj.detailView);
			hub_common_method.sortDocumentsByFilter(driver, library);
			fc.utobj().clickLink(driver, documentTitle);

			if (!fc.utobj()
					.getElementByXpath(driver, ".//*[@id='DtlsInner']//a[contains(text () ,'" + documentTitle + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document Title At Detail View Page");
			}
			if (!fc.utobj()
					.getElementByXpath(driver, ".//*[@id='DtlsInner']//*[contains(text () ,'" + briefSummary + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document's Brief Summary");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//*[@id='DtlsInner']//*[contains(text () ,'" + documentTitle
					+ "')]/..//following-sibling::td[.='" + currentDate + "']").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Update Date");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//*[@id='DtlsInner']//*[contains(text () ,'" + documentTitle
					+ "')]/..//following-sibling::td[.='" + folderName + "']").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Parent Directory");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//*[@id='DtlsInner']//*[contains(text () ,'" + documentTitle
					+ "')]/..//following-sibling::td[.='FranConnect Administrator']").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Parent Directory");
			}

			fc.utobj().printTestStep("Verify Report of Recomnded Document");
			actionImgIconDetailsView(driver, documentTitle, "Report");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().isTextDisplayed(driver, documentTitle, "was not able to verify document Title");

			if (!fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , 'Brief Summary :')]/following-sibling::td[.='" + briefSummary + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Brief Summary :");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Uploaded By :')]/following-sibling::td[.='FranConnect Administrator']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Uploaded By :");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Uploaded On :')]/following-sibling::td[.='" + currentDate + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Uploaded Date :");
			}

			if (!fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , 'Start Display Date :')]/following-sibling::td[.='" + currentDate + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Start Display Date :");
			}

			if (!fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , 'Expiration Date :')]/following-sibling::td[.='" + futureDate + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Start Display Date :");
			}
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			// Verify Report option from document Comment link from Index > List
			fc.utobj().printTestStep("Verify Report option from document Action Menu at Index > List View");

			fc.utobj().clickElement(driver, pobj.listView);
			actionImgIconListView(driver, documentTitle, "Report");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().isTextDisplayed(driver, documentTitle, "Was not able to verify document Title");

			if (!fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , 'Brief Summary :')]/following-sibling::td[.='" + briefSummary + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Brief Summary :");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Uploaded By :')]/following-sibling::td[.='FranConnect Administrator']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Uploaded By :");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Uploaded On :')]/following-sibling::td[.='" + currentDate + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Uploaded Date :");
			}

			if (!fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , 'Start Display Date :')]/following-sibling::td[.='" + currentDate + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Start Display Date :");
			}

			if (!fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , 'Expiration Date :')]/following-sibling::td[.='" + futureDate + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Start Display Date :");
			}

			fc.utobj().printTestStep("Verify the document download details");
			boolean isDocDownloadDetailsPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text(),'User Name')]/parent::*/following-sibling::tr/td[contains(text(),'FranConnect Administrator')]/preceding-sibling::td[contains(text(),'1')]");
			if (!isDocDownloadDetailsPresent) {
				fc.utobj().throwsException("Document Details is not present");
			}

			boolean isPrintButtonPresent = fc.utobj().isElementPresent(driver, pobj.printButton);
			if (!isPrintButtonPresent) {
				fc.utobj().throwsException("Print button is not present at Report cbox");
			}
			boolean isExportAsExcelButtonPresent = fc.utobj().isElementPresent(driver, pobj.exportAsExcelButton);
			if (!isExportAsExcelButtonPresent) {
				fc.utobj().throwsException("Export As Excel Button is not present at Report cbox");
			}
			boolean isCloseButtonPresent = fc.utobj().isElementPresent(driver, pobj.closeBtn);
			if (!isCloseButtonPresent) {
				fc.utobj().throwsException("Close Button is not present at Report cbox");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library" ,"hub_download","hubf1121"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Verify The Modify Document At Admin > The Hub > Library >  Index[Detail View]", testCaseId = "TC_61_Modify_Document")
	private void modifyDocumentActionImg01() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			AdminTheHubLibraryPage pobj = new AdminTheHubLibraryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			addFolder(driver, library);

			fc.utobj().printTestStep("Add Document");
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String tempDocTitle = documentTitle;
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String tempBriefSummary=briefSummary;

			String currentDate = fc.utobj().getCurrentDateUSFormat();
			String afterDate = dataSet.get("afterDate");
			int val = Integer.parseInt(afterDate);
			String futureDate = fc.utobj().getFutureDateUSFormat(val);

			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setCurrentDate(currentDate);
			library.setFutureDate(futureDate);
			library.setDocumentSubType(dataSet.get("documentSubType"));
			library.setFile(dataSet.get("fileName"));
			library.setDocumentType(dataSet.get("documentType"));
			library.setRecommendedDoc(false);
			addDocument(driver, library);

			fc.utobj().printTestStep("Modify Document");
			fc.utobj().clickElement(driver, pobj.indexTab);
			fc.utobj().clickElement(driver, pobj.detailView);
			new HubCommonMethods().sortByRecentlyUploadedDocuments(driver);
			actionImgIconDetailsView(driver, documentTitle, "Modify");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			fc.utobj().sendKeys(driver, pobj.documentTitle, documentTitle);
			briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			fc.utobj().sendKeys(driver, pobj.briefSummary, briefSummary);

			String currentDate1 = fc.utobj().getCurrentDateUSFormat();
			String futureDate1 = fc.utobj().getFutureDateUSFormat(val);
			fc.utobj().sendKeys(driver, pobj.displayDate, currentDate1);
			fc.utobj().sendKeys(driver, pobj.expirationDate, futureDate1);

			if (!fc.utobj().isSelected(driver, pobj.documentType.get(0))) {
				fc.utobj().clickElement(driver, pobj.documentType.get(0));
			}

			fc.utobj().selectDropDown(driver, pobj.documentSubType, dataSet.get("documentSubType"));

			String file = fc.utobj().getFilePathFromTestData(dataSet.get("fileName"));
			fc.utobj().moveToElement(driver, pobj.fileClick);
			pobj.fileClick.sendKeys(file);

			if (!fc.utobj().isSelected(driver, pobj.thumbFileUploaded.get(1))) {
				fc.utobj().clickElement(driver, pobj.thumbFileUploaded.get(1));
			}

			if (!fc.utobj().isSelected(driver, pobj.accessibilityPublic)) {
				fc.utobj().clickElement(driver, pobj.accessibilityPublic);
			}

			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().clickElement(driver, pobj.closeConfirmation);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, pobj.detailView);
			new HubCommonMethods().sortByRecentlyUploadedDocuments(driver);

			if (!fc.utobj()
					.getElementByXpath(driver, ".//*[@id='DtlsInner']//a[contains(text () ,'" + documentTitle + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document Title At Detail View Page");
			}
			if (!fc.utobj()
					.getElementByXpath(driver, ".//*[@id='DtlsInner']//*[contains(text () ,'" + briefSummary + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document's Brief Summary");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//*[@id='DtlsInner']//*[contains(text () ,'" + documentTitle
					+ "')]/..//following-sibling::td[.='" + currentDate + "']").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Update Date");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//*[@id='DtlsInner']//*[contains(text () ,'" + documentTitle
					+ "')]/..//following-sibling::td[.='" + folderName + "']").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Parent Directory");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//*[@id='DtlsInner']//*[contains(text () ,'" + documentTitle
					+ "')]/..//following-sibling::td[.='FranConnect Administrator']").isDisplayed()) {
				fc.utobj().throwsException("was not able verify to Uploaded By");
			}

			fc.utobj().printTestStep("Verify The Modify Document Report");
			actionImgIconDetailsView(driver, documentTitle, "Report");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().isTextDisplayed(driver, documentTitle, "Was not able to verify document Title");

			if (!fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , 'Brief Summary :')]/following-sibling::td[.='" + briefSummary + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Brief Summary :");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Uploaded By :')]/following-sibling::td[.='FranConnect Administrator']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Uploaded By :");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Uploaded On :')]/following-sibling::td[.='" + currentDate + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Uploaded Date :");
			}

			if (!fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , 'Start Display Date :')]/following-sibling::td[.='" + currentDate1 + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Start Display Date :");
			}

			if (!fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , 'Expiration Date :')]/following-sibling::td[.='" + futureDate1 + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Start Display Date :");
			}
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Go To Admin > The Hub > Library");
			fc.hub().hub_common().adminTheHubLibraryPage(driver);
			
			fc.utobj()
					.printTestStep("Verify previous documents should be moved to Archived section after modification");
			fc.utobj().clickElement(driver, pobj.archivedDocumentsTab);
			fc.utobj().clickElement(driver, pobj.detailView);
			postCommentActionImg(driver, tempDocTitle, "Post Comment");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.comment);
			String commentText = fc.utobj().generateTestData("Test Comment Text");
			WebElement element12 = fc.utobj().getElementByXpath(driver, ".//*[contains(@id , 'ta')]");
			fc.utobj().sendKeys(driver, element12, commentText);
			fc.utobj().clickElement(driver, pobj.post);

			fc.utobj().printTestStep("Verify The Post Comment");
			fc.utobj().isTextDisplayed(driver, commentText, "was not able to verify Comment Text");
			
			//Delete Posted Commented : TC-63734
			fc.utobj().printTestStep("Delete Comment");
			fc.utobj().clickElement(driver, pobj.deleteComment);
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Delete Document");
			boolean isTextPresent = fc.utobj().assertNotInPageSource(driver, commentText);
			if (isTextPresent) {
				fc.utobj().throwsException("was not able to delete Comment");
			}
			fc.commonMethods().Click_Close_Input_ByValue(driver);
			fc.utobj().switchFrameToDefault(driver);
			
			//Deleted The Posted Comment at Thumb View :TC-63734
			fc.utobj().clickElement(driver, pobj.archivedDocumentsTab);
			fc.utobj().clickElement(driver, pobj.thumbnailView);
			thumbnailOption(driver, tempDocTitle, "Post Comment");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().isTextDisplayed(driver, tempDocTitle, "was not able to verify Document Title");
			fc.utobj().isTextDisplayed(driver, tempBriefSummary, "was not able to verify Brief Summary");

			fc.utobj().printTestStep("Delete Comment");
			fc.utobj().clickElement(driver, pobj.comment);
			String commentTextThumb = fc.utobj().generateTestData("Test Comment");
			WebElement element12Thumb = fc.utobj().getElementByXpath(driver, ".//*[contains(@id , 'ta')]");
			fc.utobj().sendKeys(driver, element12Thumb, commentTextThumb);
			fc.utobj().clickElement(driver, pobj.post);
			fc.utobj().clickElement(driver, pobj.deleteComment);
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Delete Comment");
			boolean isTextPresentThumb = fc.utobj().assertNotInPageSource(driver, commentTextThumb);
			if (isTextPresentThumb) {
				fc.utobj().throwsException("was not able to delete Comment from thumbnail view");
			}

			fc.commonMethods().Click_Close_Input_ByValue(driver);
			fc.utobj().switchFrameToDefault(driver);
			
			fc.utobj().printTestStep("Verify The Document at Archived Details View");
			fc.utobj().clickElement(driver, pobj.archivedDocumentsTab);
			fc.utobj().clickElement(driver, pobj.detailView);
			new HubCommonMethods().sortByRecentlyUploadedDocuments(driver);
			boolean isDocumentTitlePresent = fc.utobj().assertPageSource(driver, tempDocTitle);
			if (!isDocumentTitlePresent) {
				fc.utobj().throwsException("Document is not present at archived documents tab");
			}

			fc.utobj().printTestStep("Verify version history details");
			actionImgIconDetailsView(driver, tempDocTitle, "Version History");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			boolean isDocTitleVerisonHistory = fc.utobj().assertPageSource(driver, tempDocTitle);
			if (!isDocTitleVerisonHistory) {
				fc.utobj().throwsException("Document is not present at version history cbox iframe");
			}
			boolean isDateLinkPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[@id='versionHistorytable']//td[contains(text(),'FranConnect Administrator')]/preceding-sibling::td/a");
			if (!isDateLinkPresent) {
				fc.utobj().throwsException("version history with dates link is not present");
			}

			fc.utobj().clickElement(driver,
					".//*[@id='versionHistorytable']//td[contains(text(),'FranConnect Administrator')]/preceding-sibling::td/a");
			fc.utobj().printTestStep("Verify Document should be downloaded successfully from details view");
			boolean isFileFound = fc.utobj().isFileFound(new File(FranconnectUtil.config.get("downloadFilepath")),
					library.getFile());
			if (!isFileFound) {
				fc.utobj().throwsException("Not able to download the file");
			}
			hub_common_method.deleteDownloadedFile(library);

			fc.commonMethods().Click_Close_Input_ByValue(driver);
			fc.utobj().switchFrameToDefault(driver);
			
			//TC-77067
			fc.utobj().printTestStep("Verify Archived Documents functionality");
			fc.utobj().clickElement(driver, pobj.archivedDocumentsTab);
			hub_common_method.sortByRecentlyUploadedDocuments(driver);
			
			fc.utobj().printTestStep("Restore the archived document");
			actionImgIconListView(driver, tempDocTitle, "Restore");
			fc.utobj().acceptAlertBox(driver);
			
			clickOverMoreLink(driver, folderName);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='folderDiv']//*[contains(text(), '" + folderName + "')]"));
			if (!fc.utobj().assertPageSource(driver, tempDocTitle)) {
				fc.utobj().throwsException("Not able to restore archived document");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Verify The Delete Document At Admin > The Hub > Library >  Index[Detail View]", testCaseId = "TC_62_Delete_Document")
	private void deleteDocumentActionImg01() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			AdminTheHubLibraryPage pobj = new AdminTheHubLibraryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "Franchise";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			addFolder(driver, library);

			fc.utobj().printTestStep("Add Document");
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String currentDate = fc.utobj().getCurrentDateUSFormat();
			String afterDate = dataSet.get("afterDate");
			int val = Integer.parseInt(afterDate);
			String futureDate = fc.utobj().getFutureDateUSFormat(val);

			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setCurrentDate(currentDate);
			library.setFutureDate(futureDate);
			library.setDocumentSubType(dataSet.get("documentSubType"));
			library.setLinkUrl(dataSet.get("linkUrl"));
			library.setDocumentType(dataSet.get("documentType"));
			library.setRecommendedDoc(false);
			addDocument(driver, library);

			fc.utobj().printTestStep("Delete Document");
			fc.utobj().clickElement(driver, pobj.indexTab);
			fc.utobj().clickElement(driver, pobj.detailView);
			new HubCommonMethods().sortByRecentlyUploadedDocuments(driver);
			actionImgIconDetailsView(driver, documentTitle, "Delete");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Delete Document");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, documentTitle);
			if (isTextPresent) {
				fc.utobj().throwsException("was not able to delete document");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library","hubf1121"})
	@TestCase(createdOn = "2017-06-22", updatedOn = "2017-06-22", testCaseDescription = "Verify The Delete Document At Admin > The Hub > Library >  Index[List View]", testCaseId = "TC_Admin_The_Hub_Delete_Document_01")
	private void deleteDocumentActionImgAtListView() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			AdminTheHubLibraryPage pobj = new AdminTheHubLibraryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "Franchise";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			addFolder(driver, library);

			fc.utobj().printTestStep("Add Document");
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String currentDate = fc.utobj().getCurrentDateUSFormat();
			String afterDate = dataSet.get("afterDate");
			int val = Integer.parseInt(afterDate);
			String futureDate = fc.utobj().getFutureDateUSFormat(val);

			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setCurrentDate(currentDate);
			library.setFutureDate(futureDate);
			library.setDocumentSubType(dataSet.get("documentSubType"));
			library.setLinkUrl(dataSet.get("linkUrl"));
			library.setDocumentType(dataSet.get("documentType"));
			library.setRecommendedDoc(false);
			addDocument(driver, library);

			String oldDocumentTitle=library.getDocumentTitle();
			fc.utobj().printTestStep("Modify The Document");
			fc.utobj().clickElement(driver, pobj.indexTab);
			fc.utobj().clickElement(driver, pobj.listView);
			new HubCommonMethods().sortByRecentlyUploadedDocuments(driver);
			actionImgIconListView(driver, documentTitle, "Modify");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			fc.utobj().sendKeys(driver, pobj.documentTitle, documentTitle);
			briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			fc.utobj().sendKeys(driver, pobj.briefSummary, briefSummary);
			String currentDate1 = fc.utobj().getCurrentDateUSFormat();
			String futureDate1 = fc.utobj().getFutureDateUSFormat(val);
			fc.utobj().sendKeys(driver, pobj.displayDate, currentDate1);
			fc.utobj().sendKeys(driver, pobj.expirationDate, futureDate1);
			fc.utobj().sendKeys(driver, pobj.linkUrl, "https://www.google.com");
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Modified Document");
			library.setDocumentTitle(documentTitle);
			new HubCommonMethods().searchAdminHubLibraryDocuments(driver, library);
			fc.utobj().clickElement(driver, pobj.listView);

			boolean isModifiedDocumentPresent = fc.utobj().assertPageSource(driver, library.getDocumentTitle());
			if (!isModifiedDocumentPresent) {
				fc.utobj().throwsException("Not able to modify the document");
			}

			//TC-66384 : Verify Download history for archived web link documents
			fc.utobj().printTestStep("Go To Archived Documents Tab > List View");
			fc.utobj().clickElement(driver, pobj.archivedDocumentsTab);
			hub_common_method.sortByRecentlyUploadedDocuments(driver);
			
			String parentWindow=driver.getWindowHandle();
			fc.utobj().clickLink(driver, oldDocumentTitle);
			
			Set<String> windows=driver.getWindowHandles();
			for (String string : windows) {
				if (string!=parentWindow) {
					driver.switchTo().window(string);
					if (driver.getCurrentUrl().contains("yahoo")) {
						driver.close();
						break;
					}
				}
				driver.switchTo().window(parentWindow);
			}
			
			fc.utobj().clickElement(driver, pobj.archivedDocumentsTab);
			hub_common_method.sortByRecentlyUploadedDocuments(driver);
			String downloadCount=fc.utobj().getText(driver, fc.utobj().getElementByXpath(driver, ".//*[.='"+oldDocumentTitle+"' and  @class='textTheme14 ']/following-sibling::div/strong[1]"));
			if (Integer.parseInt(downloadCount)!=1) {
				fc.utobj().throwsException("From Archived section, download count is not increasing like other type documents");
			}
			 
			fc.utobj().printTestStep("Delete Document from intex tab > list view");
			fc.utobj().clickElement(driver, pobj.indexTab);
			fc.utobj().clickElement(driver, pobj.listView);
			new HubCommonMethods().sortByRecentlyUploadedDocuments(driver);
			actionImgIconListView(driver, documentTitle, "Delete");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Delete Document");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, documentTitle);
			if (isTextPresent) {
				fc.utobj().throwsException("was not able to delete document");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library", "hub0709" ,"hubDownload0725"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Verify The Document Properties At Admin > The Hub > Library >  Index[Detail View]", testCaseId = "TC_63_Document_Properties")
	private void documentPropertiesActionImg01() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			AdminTheHubLibraryPage pobj = new AdminTheHubLibraryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			addFolder(driver, library);

			fc.utobj().printTestStep("Add Document");
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String currentDate = fc.utobj().getCurrentDateUSFormat();
			String afterDate = dataSet.get("afterDate");
			int val = Integer.parseInt(afterDate);
			String futureDate = fc.utobj().getFutureDateUSFormat(val);

			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setCurrentDate(currentDate);
			library.setFutureDate(futureDate);
			library.setDocumentSubType(dataSet.get("documentSubType"));
			library.setFile(dataSet.get("fileName"));
			library.setDocumentType(dataSet.get("documentType"));
			library.setRecommendedDoc(false);
			addDocument(driver, library);

			fc.utobj().printTestStep("Verify The Document Properties");
			fc.utobj().clickElement(driver, pobj.indexTab);
			fc.utobj().clickElement(driver, pobj.detailView);

			new HubCommonMethods().sortByRecentlyUploadedDocuments(driver);
			actionImgIconDetailsView(driver, documentTitle, "Document Properties");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			if (!fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , 'Document Title')]/following-sibling::td[.='" + documentTitle + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document Title");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Summary')]/following-sibling::td[.='" + briefSummary + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document's Brief Summary");
			}

			if (!fc.utobj().isElementPresent(driver,
					fc.utobj().getElementByXpath(driver,
							".//td[contains(text () , 'Document URL')]/following-sibling::td/a[contains(@href,'"
									+ FranconnectUtil.config.get("buildUrl") + "')]"))) {
				fc.utobj().throwsException("Document URL is not present");

			}

			if (!fc.utobj().isElementPresent(driver,
					fc.utobj().getElementByXpath(driver,
							".//td[contains(text () , 'Active From')]/following-sibling::td[contains(text(),'"
									+ library.getCurrentDate() + "')]"))) {
				fc.utobj().throwsException("Acttive From Label is not present");

			}

			if (!fc.utobj().isElementPresent(driver,
					fc.utobj().getElementByXpath(driver,
							".//td[contains(text () , 'Creation Date')]/following-sibling::td[contains(text(),'"
									+ library.getCurrentDate() + "')]"))) {
				fc.utobj().throwsException("Acttive From Label is not present");

			}

			if (!fc.utobj().isElementPresent(driver,
					fc.utobj().getElementByXpath(driver,
							".//td[contains(text () , 'Last Modified')]/following-sibling::td[contains(text(),'"
									+ library.getCurrentDate() + "')]"))) {
				fc.utobj().throwsException("Acttive From Label is not present");

			}

			if (!fc.utobj().isElementPresent(driver, fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , 'Document Size ')]/following-sibling::td[contains(text(),'KB')]"))) {
				fc.utobj().throwsException("Acttive From Label is not present");

			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Document Type')]/following-sibling::td[contains(text () , '"
									+ dataSet.get("documentType") + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document Type");
			}
			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Document Subtype')]/following-sibling::td[contains(text () , '"
									+ dataSet.get("documentSubType") + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify DocumentSub Type");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Uploaded By')]/following-sibling::td[contains(text () , 'FranConnect Administrator')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Uploaded By");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Recommended')]/following-sibling::td[contains(text () , '"
									+ dataSet.get("recommendedDoc") + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Recommended");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			// To verify Document Properties option from document Action Icon
			// from Index > List View
			fc.utobj().printTestStep(
					"Verify Document Properties option from document Action Icon from Index > List View");
			fc.utobj().clickElement(driver, pobj.listView);
			new HubCommonMethods().sortByRecentlyUploadedDocuments(driver);
			actionImgIconListView(driver, documentTitle, "Document Properties");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			if (!fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , 'Document Title')]/following-sibling::td[.='" + documentTitle + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document Title");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Summary')]/following-sibling::td[.='" + briefSummary + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document's Brief Summary");
			}

			if (!fc.utobj().isElementPresent(driver,
					fc.utobj().getElementByXpath(driver,
							".//td[contains(text () , 'Document URL')]/following-sibling::td/a[contains(@href,'"
									+ FranconnectUtil.config.get("buildUrl") + "')]"))) {
				fc.utobj().throwsException("Document URL is not present");

			}

			if (!fc.utobj().isElementPresent(driver,
					fc.utobj().getElementByXpath(driver,
							".//td[contains(text () , 'Active From')]/following-sibling::td[contains(text(),'"
									+ library.getCurrentDate() + "')]"))) {
				fc.utobj().throwsException("Acttive From Label is not present");

			}

			if (!fc.utobj().isElementPresent(driver,
					fc.utobj().getElementByXpath(driver,
							".//td[contains(text () , 'Creation Date')]/following-sibling::td[contains(text(),'"
									+ library.getCurrentDate() + "')]"))) {
				fc.utobj().throwsException("Acttive From Label is not present");

			}

			if (!fc.utobj().isElementPresent(driver,
					fc.utobj().getElementByXpath(driver,
							".//td[contains(text () , 'Last Modified')]/following-sibling::td[contains(text(),'"
									+ library.getCurrentDate() + "')]"))) {
				fc.utobj().throwsException("Acttive From Label is not present");

			}

			if (!fc.utobj().isElementPresent(driver, fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , 'Document Size ')]/following-sibling::td[contains(text(),'KB')]"))) {
				fc.utobj().throwsException("Acttive From Label is not present");

			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Document Type')]/following-sibling::td[contains(text () , '"
									+ dataSet.get("documentType") + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document Type");
			}
			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Document Subtype')]/following-sibling::td[contains(text () , '"
									+ dataSet.get("documentSubType") + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify DocumentSub Type");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Uploaded By')]/following-sibling::td[contains(text () , 'FranConnect Administrator')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Uploaded By");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Recommended')]/following-sibling::td[contains(text () , '"
									+ dataSet.get("recommendedDoc") + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Recommended");
			}

			// TC_63_Document_Properties
			fc.utobj().printTestStep("Verify clicking on URL document should be downloaded successfully");
			fc.utobj().clickElement(driver,
					".//td[contains(text () , 'Document URL')]/following-sibling::td/a[contains(@href,'"
							+ FranconnectUtil.config.get("buildUrl") + "')]");

			/*if (!fc.utobj().isFileFound(new File(FranconnectUtil.config.get("downloadFilepath")), library.getFile())) {
				fc.utobj().throwsException("Not able to download file from on clicking Document url");
			}*/

			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library","hubf1121"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Verify The Post Comment At Admin > The Hub > Library >  Index[Detail View]", testCaseId = "TC_64_Post_Comment")
	private void postCommentActionImg01() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			
			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users Page");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUserPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			
			String emailId = "hubautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corporateUserPage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			
			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login with newly added corporate user");
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());
			
			AdminTheHubLibraryPage pobj = new AdminTheHubLibraryPage(driver);
			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			addFolder(driver, library);

			fc.utobj().printTestStep("Add Document");
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String currentDate = fc.utobj().getCurrentDateUSFormat();
			String afterDate = dataSet.get("afterDate");
			int val = Integer.parseInt(afterDate);
			String futureDate = fc.utobj().getFutureDateUSFormat(val);

			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setCurrentDate(currentDate);
			library.setFutureDate(futureDate);
			library.setDocumentSubType(dataSet.get("documentSubType"));
			library.setFile(dataSet.get("fileName"));
			library.setDocumentType(dataSet.get("documentType"));
			library.setRecommendedDoc(false);
			addDocument(driver, library);
			
			fc.utobj().printTestStep("Logged Out from corporate user");
			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Log in by Adm User and post comment on newly added document");
			fc.loginpage().loginWithParameter(driver, FranconnectUtil.config.get("userName"), FranconnectUtil.config.get("password"));
			
			fc.utobj().printTestStep("Go To Admin > The Hub > Library");
			fc.hub().hub_common().adminTheHubLibraryPage(driver);
			
			fc.utobj().printTestStep("Post Comment");
			fc.utobj().clickElement(driver, pobj.indexTab);
			fc.utobj().clickElement(driver, pobj.detailView);

			postCommentActionImg(driver, documentTitle, "Post Comment");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().isTextDisplayed(driver, documentTitle, "was not able to verify Document Title");
			fc.utobj().isTextDisplayed(driver, briefSummary, "was not able to verify Brief Summary");

			fc.utobj().printTestStep("Verify The Post Comment");
			fc.utobj().clickElement(driver, pobj.comment);
			String commentText = fc.utobj().generateTestData(dataSet.get("commentText"));
			WebElement element12 = fc.utobj().getElementByXpath(driver, ".//*[contains(@id , 'ta')]");
			fc.utobj().sendKeys(driver, element12, commentText);
			fc.utobj().clickElement(driver, pobj.post);
			fc.utobj().isTextDisplayed(driver, commentText, "was not able to verify Comment Text");
			fc.commonMethods().Click_Close_Input_ByValue(driver);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify Post Comment option from document Comment link from Index > List View");
			String commentTextList = fc.utobj().generateTestData("Test Comment Text");
			library.setCommentText(commentTextList);
			hub_common_method.postCommentAtListView(driver, pobj, library);
			boolean isCommentTextPresent = fc.utobj().assertPageSource(driver, library.getCommentText());
			if (!isCommentTextPresent) {
				fc.utobj().throwsException("Not able to verify added Comment at List View");
			}
			
			fc.utobj().printTestStep("Verify A Commented mail should be come to document uploader");
			Map<String, String> mailInfo=new HashMap<String,String>();
			mailInfo=fc.utobj().readMailBox("Library Document Comment", library.getCommentText(), corpUser.getEmail(), "sdg@1a@Hfs");
			if (!mailInfo.get("mailBody").contains(library.getCommentText())) {
				fc.utobj().throwsException("Not able to verify mail after posting comment");
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library","hubf1121"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Verify The Delete Comment At Admin > The Hub > Library >  Index[Detail View]", testCaseId = "TC_65_Delete_Comment")
	private void deleteComment01() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			AdminTheHubLibraryPage pobj = new AdminTheHubLibraryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			addFolder(driver, library);

			fc.utobj().printTestStep("Add Document");
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String currentDate = fc.utobj().getCurrentDateUSFormat();
			String afterDate = dataSet.get("afterDate");
			int val = Integer.parseInt(afterDate);
			String futureDate = fc.utobj().getFutureDateUSFormat(val);

			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setCurrentDate(currentDate);
			library.setFutureDate(futureDate);
			library.setDocumentSubType(dataSet.get("documentSubType"));
			library.setFile(dataSet.get("fileName"));
			library.setDocumentType(dataSet.get("documentType"));
			library.setRecommendedDoc(false);
			addDocument(driver, library);

			fc.utobj().printTestStep("Search By Document search");
			hub_common_method.searchAdminHubLibraryDocuments(driver, library);
			
			fc.utobj().printTestStep("Post Comment");
			fc.utobj().clickElement(driver, pobj.detailView);
			postCommentActionImg(driver, documentTitle, "Post Comment");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().isTextDisplayed(driver, documentTitle, "was not able to verify Document Title");
			fc.utobj().isTextDisplayed(driver, briefSummary, "was not able to verify Brief Summary");

			fc.utobj().printTestStep("Delete Comment");
			fc.utobj().clickElement(driver, pobj.comment);
			String commentText = fc.utobj().generateTestData(dataSet.get("commentText"));
			WebElement element12 = fc.utobj().getElementByXpath(driver, ".//*[contains(@id , 'ta')]");
			fc.utobj().sendKeys(driver, element12, commentText);
			fc.utobj().clickElement(driver, pobj.post);
			fc.utobj().clickElement(driver, pobj.deleteComment);
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Delete Document");
			boolean isTextPresent = fc.utobj().assertNotInPageSource(driver, commentText);
			if (isTextPresent) {
				fc.utobj().throwsException("was not able to delete Comment");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library","hub_download" ,"hubf1121"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Verify The Version History At Admin > The Hub > Library >  Index[Detail View]", testCaseId = "TC_66_Version_History")
	private void versionHistory01() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			AdminTheHubLibraryPage pobj = new AdminTheHubLibraryPage(driver);
			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			addFolder(driver, library);

			fc.utobj().printTestStep("Add Document");
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String currentDate = fc.utobj().getCurrentDateUSFormat();
			String afterDate = dataSet.get("afterDate");
			int val = Integer.parseInt(afterDate);
			String futureDate = fc.utobj().getFutureDateUSFormat(val);
			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setCurrentDate(currentDate);
			library.setFutureDate(futureDate);
			library.setDocumentSubType(dataSet.get("documentSubType"));
			library.setFile(dataSet.get("fileName"));
			library.setDocumentType(dataSet.get("documentType"));
			library.setRecommendedDoc(false);
			addDocument(driver, library);
			fc.utobj().printTestStep("Verify The Version History");
			fc.utobj().clickElement(driver, pobj.indexTab);
			fc.utobj().clickElement(driver, pobj.detailView);
			actionImgIconDetailsView(driver, documentTitle, "Version History");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Document Title')]/following-sibling::td[contains(text () , '"
									+ documentTitle + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document Title");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Brief Summary')]/following-sibling::td[contains(text () , '"
									+ briefSummary + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document's Brief Summary");
			}

			boolean isExpirationDateIsPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text(),'Expiration Date')]/following-sibling::td[contains(text(),'"
							+ library.getFutureDate() + "')]");
			if (!isExpirationDateIsPresent) {
				fc.utobj().throwsException("Not able to verify the Expiration Date at version history frame");
			}

			String text = fc.utobj().getText(driver,
					fc.utobj().getElementByXpath(driver,
							".//td[contains(text(),'FranConnect Administrator')]/preceding-sibling::td/a[contains(text(),'"
									+ fc.utobj().getCurrentYear() + "')]"));
			String pattern = "yyyyMMdd";
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
			String date = simpleDateFormat.format(new Date());
			date = date.concat("_01");

			if (!text.equalsIgnoreCase(date)) {
				fc.utobj().throwsException("not able to verify document link format at version history frame");
			}

			fc.utobj().clickElement(driver,
					".//td[contains(text(),' FranConnect Administrator')]/preceding-sibling::td/a[contains(text(),'"
							+ fc.utobj().getCurrentYear() + "')]");

			boolean isFileFound = fc.utobj().isFileFound(new File(FranconnectUtil.config.get("downloadFilepath")),
					library.getFile());
			if (!isFileFound) {
				fc.utobj().throwsException("Not able to download version history document");
			}
			hub_common_method.deleteDownloadedFile(library);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library","hubf1121"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Verify The Delete Document At Admin > The Hub > Library >  Index[Detail View].", testCaseId = "TC_67_Delete_Document")
	private void deleteDocument01() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			AdminTheHubLibraryPage pobj = new AdminTheHubLibraryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			addFolder(driver, library);

			fc.utobj().printTestStep("Add Document");
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String currentDate = fc.utobj().getCurrentDateUSFormat();
			String afterDate = dataSet.get("afterDate");
			int val = Integer.parseInt(afterDate);
			String futureDate = fc.utobj().getFutureDateUSFormat(val);

			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setCurrentDate(currentDate);
			library.setFutureDate(futureDate);
			library.setDocumentSubType(dataSet.get("documentSubType"));
			library.setFile(dataSet.get("fileName"));
			library.setDocumentType(dataSet.get("documentType"));
			library.setRecommendedDoc(false);
			addDocument(driver, library);

			fc.utobj().printTestStep("Delete Document");
			fc.utobj().clickElement(driver, pobj.indexTab);
			fc.utobj().clickElement(driver, pobj.detailView);
			fc.utobj().clickElement(driver, pobj.deleteLink);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[.='" + documentTitle + "']/ancestor::tr/td/input"));
			fc.utobj().clickElement(driver, pobj.deleteBtn);
			fc.utobj().sleep();
			fc.utobj().acceptAlertBox(driver);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, pobj.detailView);

			fc.utobj().printTestStep("Verify The Delete Document");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, documentTitle);
			if (isTextPresent == true) {
				fc.utobj().throwsException("was not able to delete document");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Verify The Report Document At Admin > The Hub > Library >  Index[Thumbnail View]", testCaseId = "TC_68_Report_Document")
	private void reportDocument02() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			AdminTheHubLibraryPage pobj = new AdminTheHubLibraryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			addFolder(driver, library);

			fc.utobj().printTestStep("Add Document");
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String currentDate = fc.utobj().getCurrentDateUSFormat();
			String afterDate = dataSet.get("afterDate");
			int val = Integer.parseInt(afterDate);
			String futureDate = fc.utobj().getFutureDateUSFormat(val);

			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setCurrentDate(currentDate);
			library.setFutureDate(futureDate);
			library.setDocumentSubType(dataSet.get("documentSubType"));
			library.setFile(dataSet.get("fileName"));
			library.setDocumentType(dataSet.get("documentType"));
			library.setRecommendedDoc(false);
			addDocument(driver, library);

			fc.utobj().printTestStep("Verift The Report Of Document");
			fc.utobj().clickElement(driver, pobj.indexTab);
			fc.utobj().clickElement(driver, pobj.thumbnailView);

			library.setSortBy("Most Downloaded");
			hub_common_method.sortDocumentsByFilter(driver, library);

			if (!fc.utobj().getElementByXpath(driver, ".//*[@id='thums']//a[.='" + documentTitle + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document Title At Thumbnail View Page");
			}

			thumbnailOption(driver, documentTitle, "Report");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().isTextDisplayed(driver, documentTitle, "Was not able to verify document Title");

			if (!fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , 'Brief Summary :')]/following-sibling::td[.='" + briefSummary + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Brief Summary :");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Uploaded By :')]/following-sibling::td[.='FranConnect Administrator']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Uploaded By :");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Uploaded On :')]/following-sibling::td[.='" + currentDate + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Uploaded Date :");
			}

			if (!fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , 'Start Display Date :')]/following-sibling::td[.='" + currentDate + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Start Display Date :");
			}

			if (!fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , 'Expiration Date :')]/following-sibling::td[.='" + futureDate + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Start Display Date :");
			}
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Verify The Document Properties At Admin > The Hub > Library >  Index[Thumbnail View]", testCaseId = "TC_69_Document_Properties")
	private void documentProperties02() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			AdminTheHubLibraryPage pobj = new AdminTheHubLibraryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			addFolder(driver, library);

			fc.utobj().printTestStep("Add Document");
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String currentDate = fc.utobj().getCurrentDateUSFormat();
			String afterDate = dataSet.get("afterDate");
			int val = Integer.parseInt(afterDate);
			String futureDate = fc.utobj().getFutureDateUSFormat(val);

			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setCurrentDate(currentDate);
			library.setFutureDate(futureDate);
			library.setDocumentSubType(dataSet.get("documentSubType"));
			library.setFile(dataSet.get("fileName"));
			library.setDocumentType(dataSet.get("documentType"));
			library.setRecommendedDoc(false);

			addDocument(driver, library);

			fc.utobj().printTestStep("Verify The Document Properties");
			fc.utobj().clickElement(driver, pobj.indexTab);
			fc.utobj().clickElement(driver, pobj.thumbnailView);

			thumbnailOption(driver, documentTitle, "Document Properties");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			if (!fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , 'Document Title')]/following-sibling::td[.='" + documentTitle + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document Title");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Summary')]/following-sibling::td[.='" + briefSummary + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document's Brief Summary");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Document Type')]/following-sibling::td[contains(text () , '"
									+ dataSet.get("documentType") + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document Type");
			}
			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Document Subtype')]/following-sibling::td[contains(text () , '"
									+ dataSet.get("documentSubType") + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify DocumentSub Type");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Uploaded By')]/following-sibling::td[contains(text () , 'FranConnect Administrator')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify DocumentSub Type");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Recommended')]/following-sibling::td[contains(text () , '"
									+ dataSet.get("recommendedDoc") + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Recommended");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Verify The Post Comment At Admin > The Hub > Library >  Index[Thumbnail View]", testCaseId = "TC_70_Post_Comment")
	private void postComment02() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			AdminTheHubLibraryPage pobj = new AdminTheHubLibraryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			addFolder(driver, library);

			fc.utobj().printTestStep("Add Document");
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String currentDate = fc.utobj().getCurrentDateUSFormat();
			String afterDate = dataSet.get("afterDate");
			int val = Integer.parseInt(afterDate);
			String futureDate = fc.utobj().getFutureDateUSFormat(val);

			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setCurrentDate(currentDate);
			library.setFutureDate(futureDate);
			library.setDocumentSubType(dataSet.get("documentSubType"));
			library.setFile(dataSet.get("fileName"));
			library.setDocumentType(dataSet.get("documentType"));
			library.setRecommendedDoc(false);

			addDocument(driver, library);

			fc.utobj().printTestStep("Post Comment");
			fc.utobj().clickElement(driver, pobj.indexTab);
			fc.utobj().clickElement(driver, pobj.thumbnailView);
			thumbnailOption(driver, documentTitle, "Post Comment");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().isTextDisplayed(driver, documentTitle, "was not able to verify Document Title");
			fc.utobj().isTextDisplayed(driver, briefSummary, "was not able to verify Brief Summary");

			fc.utobj().printTestStep("Verify The Post Comment");
			fc.utobj().clickElement(driver, pobj.comment);
			String commentText = fc.utobj().generateTestData(dataSet.get("commentText"));
			WebElement element12 = fc.utobj().getElementByXpath(driver, ".//*[contains(@id , 'ta')]");
			System.out.println(element12);
			fc.utobj().sendKeys(driver, element12, commentText);
			fc.utobj().clickElement(driver, pobj.post);

			boolean isCommentTextPresent = fc.utobj().isElementPresent(driver,
					fc.utobj().getElementByXpath(driver, ".//div[contains(text(),'" + commentText + "')]"));

			if (isCommentTextPresent == false) {
				fc.utobj().throwsException("was not able to verify Comment Text");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Verify The Delete Comment At Admin > The Hub > Library >  Index[Thumbnail View]", testCaseId = "TC_71_Delete_Comment")
	private void deleteComment02() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			AdminTheHubLibraryPage pobj = new AdminTheHubLibraryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			addFolder(driver, library);

			fc.utobj().printTestStep("Add Document");
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String currentDate = fc.utobj().getCurrentDateUSFormat();
			String afterDate = dataSet.get("afterDate");
			int val = Integer.parseInt(afterDate);
			String futureDate = fc.utobj().getFutureDateUSFormat(val);

			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setCurrentDate(currentDate);
			library.setFutureDate(futureDate);
			library.setDocumentSubType(dataSet.get("documentSubType"));
			library.setFile(dataSet.get("fileName"));
			library.setDocumentType(dataSet.get("documentType"));
			library.setRecommendedDoc(false);
			addDocument(driver, library);

			fc.utobj().printTestStep("Search by Library Documents");
			hub_common_method.searchAdminHubLibraryDocuments(driver, library);
			
			fc.utobj().printTestStep("Add Comment");
			/*fc.utobj().clickElement(driver, pobj.indexTab);*/
			
			fc.utobj().clickElement(driver, pobj.thumbnailView);
			thumbnailOption(driver, documentTitle, "Post Comment");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().isTextDisplayed(driver, documentTitle, "was not able to verify Document Title");
			fc.utobj().isTextDisplayed(driver, briefSummary, "was not able to verify Brief Summary");

			fc.utobj().printTestStep("Delete Comment");
			fc.utobj().clickElement(driver, pobj.comment);
			String commentText = fc.utobj().generateTestData(dataSet.get("commentText"));
			WebElement element12 = fc.utobj().getElementByXpath(driver, ".//*[contains(@id , 'ta')]");
			System.out.println(element12);
			fc.utobj().sendKeys(driver, element12, commentText);
			fc.utobj().clickElement(driver, pobj.post);
			fc.utobj().clickElement(driver, pobj.deleteComment);
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Delete Comment");
			boolean isTextPresent = fc.utobj().assertNotInPageSource(driver, commentText);
			if (isTextPresent) {
				fc.utobj().throwsException("was not able to delete Comment");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Verify The Version History At Admin > The Hub > Library >  Index[Thumbnail View]", testCaseId = "TC_72_Version_History")
	private void versionHistory02() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			AdminTheHubLibraryPage pobj = new AdminTheHubLibraryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			addFolder(driver, library);

			fc.utobj().printTestStep("Add Document");
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String currentDate = fc.utobj().getCurrentDateUSFormat();
			String afterDate = dataSet.get("afterDate");
			int val = Integer.parseInt(afterDate);
			String futureDate = fc.utobj().getFutureDateUSFormat(val);

			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setCurrentDate(currentDate);
			library.setFutureDate(futureDate);
			library.setDocumentSubType(dataSet.get("documentSubType"));
			library.setFile(dataSet.get("fileName"));
			library.setDocumentType(dataSet.get("documentType"));
			library.setRecommendedDoc(false);

			addDocument(driver, library);

			fc.utobj().printTestStep("Verify The Version History");
			fc.utobj().clickElement(driver, pobj.indexTab);
			fc.utobj().clickElement(driver, pobj.thumbnailView);
			thumbnailOption(driver, documentTitle, "Version History");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Document Title')]/following-sibling::td[contains(text () , '"
									+ documentTitle + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document Title");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Brief Summary')]/following-sibling::td[contains(text () , '"
									+ briefSummary + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document's Brief Summary");
			}
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library" ,"hub_0806"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Verify The Delete Document At Admin > The Hub > Library >  Index[Thumbnail View]", testCaseId = "TC_73_Delete_Document")
	private void deleteDocumentBtn() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			AdminTheHubLibraryPage pobj = new AdminTheHubLibraryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			addFolder(driver, library);

			fc.utobj().printTestStep("Add Document");
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String currentDate = fc.utobj().getCurrentDateUSFormat();
			String afterDate = dataSet.get("afterDate");
			int val = Integer.parseInt(afterDate);
			String futureDate = fc.utobj().getFutureDateUSFormat(val);

			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setCurrentDate(currentDate);
			library.setFutureDate(futureDate);
			library.setDocumentSubType(dataSet.get("documentSubType"));
			library.setFile(dataSet.get("fileName"));
			library.setDocumentType(dataSet.get("documentType"));
			library.setRecommendedDoc(false);

			addDocument(driver, library);

			fc.utobj().printTestStep("Delete Document");
			fc.utobj().clickElement(driver, pobj.indexTab);
			fc.utobj().clickElement(driver, pobj.thumbnailView);

			WebElement element = fc.utobj().getElementByXpath(driver, ".//*[@id='thums']//*[.='" + documentTitle + "']"
					+ "//following::table[@class='topLine pt5'][1]//td/a[contains(@onclick, 'deletedocument')]");
			fc.utobj().clickElement(driver, element);
			fc.utobj().acceptAlertBox(driver);
			fc.utobj().clickElement(driver, pobj.thumbnailView);

			fc.utobj().printTestStep("Verify The Delete Comment");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, documentTitle);

			if (isTextPresent == true) {
				fc.utobj().throwsException("was not able to delete Library Document");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library","hub_download","hubf1121"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Verify The Modify Document At Admin > The Hub > Library >  Index[Thumbnail View]", testCaseId = "TC_74_Modify_Document")
	private void modifyDocumentBtn() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			AdminTheHubLibraryPage pobj = new AdminTheHubLibraryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			addFolder(driver, library);

			fc.utobj().printTestStep("Add Document");
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String currentDate = fc.utobj().getCurrentDateUSFormat();
			String afterDate = dataSet.get("afterDate");
			int val = Integer.parseInt(afterDate);
			String futureDate = fc.utobj().getFutureDateUSFormat(val);

			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setCurrentDate(currentDate);
			library.setFutureDate(futureDate);
			library.setDocumentSubType(dataSet.get("documentSubType"));
			library.setFile(dataSet.get("fileName"));
			library.setDocumentType(dataSet.get("documentType"));
			library.setRecommendedDoc(false);
			String tempDocTitle = library.getDocumentTitle();
			addDocument(driver, library);

			String tempBriefSummary=briefSummary;
			
			fc.utobj().printTestStep("Modify Document");
			fc.utobj().clickElement(driver, pobj.indexTab);
			fc.utobj().clickElement(driver, pobj.thumbnailView);
			WebElement element = fc.utobj().getElementByXpath(driver, ".//*[@id='thums']//*[.='" + documentTitle + "']"
					+ "//following::table[@class='topLine pt5'][1]//td/a/img[@title='Modify']");
			fc.utobj().clickElement(driver, element);

			fc.commonMethods().switch_cboxIframe_frameId(driver);

			documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			fc.utobj().sendKeys(driver, pobj.documentTitle, documentTitle);
			briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			fc.utobj().sendKeys(driver, pobj.briefSummary, briefSummary);

			String currentDate1 = fc.utobj().getCurrentDateUSFormat();
			String futureDate1 = fc.utobj().getFutureDateUSFormat(val);
			fc.utobj().sendKeys(driver, pobj.displayDate, currentDate1);
			fc.utobj().sendKeys(driver, pobj.expirationDate, futureDate1);

			if (!fc.utobj().isSelected(driver, pobj.documentType.get(0))) {
				fc.utobj().clickElement(driver, pobj.documentType.get(0));
			}

			fc.utobj().selectDropDown(driver, pobj.documentSubType, dataSet.get("documentSubType"));

			String file = fc.utobj().getFilePathFromTestData(dataSet.get("fileName"));
			fc.utobj().moveToElement(driver, pobj.fileClick);
			pobj.fileClick.sendKeys(file);

			if (!fc.utobj().isSelected(driver, pobj.thumbFileUploaded.get(1))) {
				fc.utobj().clickElement(driver, pobj.thumbFileUploaded.get(1));
			}

			if (!fc.utobj().isSelected(driver, pobj.accessibilityPublic)) {
				fc.utobj().clickElement(driver, pobj.accessibilityPublic);
			}

			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Modify Document");
			fc.utobj().clickElement(driver, pobj.thumbnailView);

			if (!fc.utobj().getElementByXpath(driver, ".//*[@id='thums']//a[.='" + documentTitle + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document Title At Thumbnail View Page");
			}

			thumbnailOption(driver, documentTitle, "Report");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().isTextDisplayed(driver, documentTitle, "Was not able to verify document Title");
			if (!fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , 'Brief Summary :')]/following-sibling::td[.='" + briefSummary + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Brief Summary :");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Uploaded By :')]/following-sibling::td[.='FranConnect Administrator']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Uploaded By :");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Uploaded On :')]/following-sibling::td[.='" + currentDate + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Uploaded Date :");
			}

			if (!fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , 'Start Display Date :')]/following-sibling::td[.='" + currentDate1 + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Start Display Date :");
			}

			if (!fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , 'Expiration Date :')]/following-sibling::td[.='" + futureDate1 + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Start Display Date :");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify Post Comment option from document action icon from Archived >Thumb View");
			fc.utobj().clickElement(driver, pobj.archivedDocumentsTab);
			fc.utobj().clickElement(driver, pobj.thumbnailView);
			thumbnailOption(driver, tempDocTitle, "Post Comment");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().isTextDisplayed(driver, tempDocTitle, "was not able to verify Document Title");
			fc.utobj().isTextDisplayed(driver, tempBriefSummary, "was not able to verify Brief Summary");

			fc.utobj().clickElement(driver, pobj.comment);
			String commentText = fc.utobj().generateTestData(dataSet.get("commentText"));
			WebElement element12 = fc.utobj().getElementByXpath(driver, ".//*[contains(@id , 'ta')]");
			fc.utobj().sendKeys(driver, element12, commentText);
			fc.utobj().clickElement(driver, pobj.post);

			fc.utobj().printTestStep("Verify The Post Comment");
			fc.utobj().isTextDisplayed(driver, commentText, "was not able to verify Comment Text");

			fc.commonMethods().Click_Close_Input_ByValue(driver);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj()
					.printTestStep("Verify previous documents should be moved to Archived section after modification");
			fc.utobj().clickElement(driver, pobj.thumbnailView);
			new HubCommonMethods().sortByRecentlyUploadedDocuments(driver);

			boolean isDocumentTitlePresent = fc.utobj().assertPageSource(driver, tempDocTitle);
			if (!isDocumentTitlePresent) {
				fc.utobj().throwsException("Document is not present at archived documents tab");
			}

			fc.utobj().printTestStep("Verify version history details");
			thumbnailOption(driver, tempDocTitle, "Version History");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			boolean isDocTitleVerisonHistory = fc.utobj().assertPageSource(driver, tempDocTitle);
			if (!isDocTitleVerisonHistory) {
				fc.utobj().throwsException("Document is not present at version history cbox iframe");
			}

			boolean isDateLinkPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					"//*[@id='versionHistorytable']//td[contains(text(),'FranConnect Administrator')]/preceding-sibling::td/a");
			if (!isDateLinkPresent) {
				fc.utobj().throwsException("version history with dates link is not present");
			}

			fc.utobj().clickElement(driver,
					".//*[@id='versionHistorytable']//td[contains(text(),'FranConnect Administrator')]/preceding-sibling::td/a");
			fc.utobj().printTestStep("Verify Document should be downloaded successfully from thumbnail view");
			boolean isFileFound = fc.utobj().isFileFound(new File(FranconnectUtil.config.get("downloadFilepath")),
					library.getFile());
			if (!isFileFound) {
				fc.utobj().throwsException("Not able to download the file");
			}
			hub_common_method.deleteDownloadedFile(library);

			fc.commonMethods().Click_Close_Input_ByValue(driver);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Verify The Report Document At Admin > The Hub > Library >  Draft [Detail View]", testCaseId = "TC_75_Report_Document")
	private void reportDocumentActionImg03() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			AdminTheHubLibraryPage pobj = new AdminTheHubLibraryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";
			
			fc.utobj().printTestStep("Add Folder");
			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			addFolder(driver, library);

			fc.utobj().printTestStep("Add Draft Document");
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String futureDate1 = fc.utobj().getFutureDateUSFormat(3);
			String futureDate2 = fc.utobj().getFutureDateUSFormat(4);
			String currentDate = fc.utobj().getCurrentDateUSFormat();
			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setFutureDate(futureDate1);
			library.setSecondFutureDate(futureDate2);
			library.setCurrentDate(currentDate);
			library.setDocumentSubType(dataSet.get("documentSubType"));
			library.setDocumentType(dataSet.get("documentType"));
			library.setFile(dataSet.get("fileName"));
			library.setRecommendedDoc(false);
			library.setTypeOfDocument("Draft");
			addDocument(driver, library);
			
			fc.utobj().clickElement(driver, pobj.draftsTab);
			fc.utobj().clickElement(driver, pobj.detailView);
			fc.utobj().clickLink(driver, documentTitle);

			if (!fc.utobj()
					.getElementByXpath(driver, ".//*[@id='DtlsInner']//a[contains(text () ,'" + documentTitle + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document Title At Detail View Page");
			}
			if (!fc.utobj()
					.getElementByXpath(driver, ".//*[@id='DtlsInner']//*[contains(text () ,'" + briefSummary + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document's Brief Summary");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//*[@id='DtlsInner']//*[contains(text () ,'" + documentTitle
					+ "')]/..//following-sibling::td[.='" + currentDate + "']").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Update Date");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//*[@id='DtlsInner']//*[contains(text () ,'" + documentTitle
					+ "')]/..//following-sibling::td[.='" + folderName + "']").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Parent Directory");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//*[@id='DtlsInner']//*[contains(text () ,'" + documentTitle
					+ "')]/..//following-sibling::td[.='FranConnect Administrator']").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Parent Directory");
			}

			fc.utobj().printTestStep("Verify The Report Draft Document");
			actionImgIconDetailsView(driver, documentTitle, "Report");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().isTextDisplayed(driver, documentTitle, "Was not able to verify document Title");

			if (!fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , 'Brief Summary :')]/following-sibling::td[.='" + briefSummary + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Brief Summary :");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Uploaded By :')]/following-sibling::td[.='FranConnect Administrator']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Uploaded By :");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Uploaded On :')]/following-sibling::td[.='" + currentDate + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Uploaded Date :");
			}

			if (!fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , 'Start Display Date :')]/following-sibling::td[.='" + futureDate1 + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Start Display Date :");
			}
			if (!fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , 'Expiration Date :')]/following-sibling::td[.='" + futureDate2 + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Start Display Date :");
			}
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			// To verify Report option from document Comment link from Draft >
			// List View
			fc.utobj().printTestStep("Verify Report option from document Comment link from Draft > List View");

			fc.utobj().clickElement(driver, pobj.listView);
			actionImgIconListView(driver, documentTitle, "Report");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().isTextDisplayed(driver, documentTitle, "Was not able to verify document Title");

			if (!fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , 'Brief Summary :')]/following-sibling::td[.='" + briefSummary + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Brief Summary :");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Uploaded By :')]/following-sibling::td[.='FranConnect Administrator']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Uploaded By :");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Uploaded On :')]/following-sibling::td[.='" + currentDate + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Uploaded Date :");
			}

			if (!fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , 'Start Display Date :')]/following-sibling::td[.='" + futureDate1 + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Start Display Date :");
			}

			fc.utobj().printTestStep("Verify the document download details");
			boolean isDocDownloadDetailsPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text(),'User Name')]/parent::*/following-sibling::tr/td[contains(text(),'FranConnect Administrator')]/preceding-sibling::td[contains(text(),'1')]");
			if (!isDocDownloadDetailsPresent) {
				fc.utobj().throwsException("Document Details is not present");
			}

			boolean isPrintButtonPresent = fc.utobj().isElementPresent(driver, pobj.printButton);
			if (!isPrintButtonPresent) {
				fc.utobj().throwsException("Print button is not present at Report cbox");
			}
			boolean isExportAsExcelButtonPresent = fc.utobj().isElementPresent(driver, pobj.exportAsExcelButton);
			if (!isExportAsExcelButtonPresent) {
				fc.utobj().throwsException("Export As Excel Button is not present at Report cbox");
			}
			boolean isCloseButtonPresent = fc.utobj().isElementPresent(driver, pobj.closeBtn);
			if (!isCloseButtonPresent) {
				fc.utobj().throwsException("Close Button is not present at Report cbox");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library","hub_download","hubf1121"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Verify The Modify Document At Admin > The Hub > Library > Drafts", testCaseId = "TC_76_Modify_Document")
	private void modifyDocumentActionImg03() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			AdminTheHubLibraryPage pobj = new AdminTheHubLibraryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			addFolder(driver, library);

			fc.utobj().printTestStep("Add Draft Document");
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String futureDate1 = fc.utobj().getFutureDateUSFormat(1);
			String afterDate = dataSet.get("afterDate");
			int val = Integer.parseInt(afterDate);
			String futureDate2 = fc.utobj().getFutureDateUSFormat(val);
			
			library.setTypeOfDocument("Draft");
			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setFutureDate(futureDate1);
			library.setSecondFutureDate(futureDate2);
			library.setDocumentType(dataSet.get("documentType"));
			library.setDocumentSubType(dataSet.get("documentSubType"));
			library.setFile(dataSet.get("fileName"));
			library.setRecommendedDoc(false);
			addDocument(driver, library);
			
			fc.utobj().printTestStep("Modify The Draft Document");
			fc.utobj().clickElement(driver, pobj.draftsTab);
			fc.utobj().clickElement(driver, pobj.detailView);
			new HubCommonMethods().sortByRecentlyUploadedDocuments(driver);
			actionImgIconDetailsView(driver, documentTitle, "Modify");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			fc.utobj().sendKeys(driver, pobj.documentTitle, documentTitle);
			briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			fc.utobj().sendKeys(driver, pobj.briefSummary, briefSummary);

			String futureDate3 = fc.utobj().getFutureDateUSFormat(1);
			String futureDate4 = fc.utobj().getFutureDateUSFormat(val);
			String currentDate = fc.utobj().getCurrentDateUSFormat();
			fc.utobj().sendKeys(driver, pobj.displayDate, futureDate3);
			fc.utobj().sendKeys(driver, pobj.expirationDate, futureDate4);

			if (!fc.utobj().isSelected(driver, pobj.documentType.get(0))) {
				fc.utobj().clickElement(driver, pobj.documentType.get(0));
			}

			fc.utobj().selectDropDown(driver, pobj.documentSubType, dataSet.get("documentSubType"));

			String file = fc.utobj().getFilePathFromTestData("taskFile_04.pdf");
			fc.utobj().moveToElement(driver, pobj.fileClick);
			pobj.fileClick.sendKeys(file);

			if (!fc.utobj().isSelected(driver, pobj.thumbFileUploaded.get(1))) {
				fc.utobj().clickElement(driver, pobj.thumbFileUploaded.get(1));
			}

			if (!fc.utobj().isSelected(driver, pobj.accessibilityPublic)) {
				fc.utobj().clickElement(driver, pobj.accessibilityPublic);
			}

			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Modified Draft Document");

			fc.utobj().clickElement(driver, pobj.detailView);
			new HubCommonMethods().sortByRecentlyUploadedDocuments(driver);

			if (!fc.utobj()
					.getElementByXpath(driver, ".//*[@id='DtlsInner']//a[contains(text () ,'" + documentTitle + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document Title At Detail View Page");
			}
			if (!fc.utobj()
					.getElementByXpath(driver, ".//*[@id='DtlsInner']//*[contains(text () ,'" + briefSummary + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document's Brief Summary");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//*[@id='DtlsInner']//*[contains(text () ,'" + documentTitle
					+ "')]/..//following-sibling::td[.='" + currentDate + "']").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Update Date");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//*[@id='DtlsInner']//*[contains(text () ,'" + documentTitle
					+ "')]/..//following-sibling::td[.='" + folderName + "']").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Parent Directory");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//*[@id='DtlsInner']//*[contains(text () ,'" + documentTitle
					+ "')]/..//following-sibling::td[.='FranConnect Administrator']").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Parent Directory");
			}

			fc.utobj().printTestStep("Verify The Report of Draft Document");
			actionImgIconDetailsView(driver, documentTitle, "Report");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().isTextDisplayed(driver, documentTitle, "Was not able to verify document Title");

			if (!fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , 'Brief Summary :')]/following-sibling::td[.='" + briefSummary + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Brief Summary :");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Uploaded By :')]/following-sibling::td[.='FranConnect Administrator']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Uploaded By :");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Uploaded On :')]/following-sibling::td[.='" + currentDate + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Uploaded Date :");
			}

			if (!fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , 'Start Display Date :')]/following-sibling::td[.='" + futureDate3 + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Start Display Date :");
			}
			if (!fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , 'Expiration Date :')]/following-sibling::td[.='" + futureDate4 + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Start Display Date :");
			}
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj()
					.printTestStep("To verify Version history option from document action icon from Draft > List View");
			fc.utobj().clickElement(driver, pobj.listView);
			actionImgIconListView(driver, documentTitle, "Version History");

			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().printTestStep("Verify The First Version History Document");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='versionHistorytable']//td[contains(text(),'FranConnect Administrator')]/preceding-sibling::td/a[contains(text(),'_01')]"));

			boolean isFileFound1 = fc.utobj().isFileFound(new File(FranconnectUtil.config.get("downloadFilepath")),
					"taskFile_04.pdf");
			if (!isFileFound1) {
				fc.utobj().throwsException("Not able to download first file from version history iframe");
			}

			fc.commonMethods().Click_Close_Input_ByValue(driver);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Verify The Delete Document At Admin > The Hub > Library >  Drafts [Detail View]", testCaseId = "TC_77_Delete_Document")
	private void deleteDocumentActionImg03() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			AdminTheHubLibraryPage pobj = new AdminTheHubLibraryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "Franchise";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			addFolder(driver, library);

			fc.utobj().printTestStep("Add Draft Document");
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String futureDate1 = fc.utobj().getFutureDateUSFormat(1);
			String afterDate = dataSet.get("afterDate");
			int val = Integer.parseInt(afterDate);
			String futureDate2 = fc.utobj().getFutureDateUSFormat(val);
			
			library.setTypeOfDocument("Draft");
			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setFutureDate(futureDate1);
			library.setSecondFutureDate(futureDate2);
			library.setDocumentType(dataSet.get("documentType"));
			library.setLinkUrl(dataSet.get("linkUrl"));
			library.setRecommendedDoc(false);
			addDocument(driver, library);
			
			fc.utobj().printTestStep("Delete Document");
			fc.utobj().clickElement(driver, pobj.draftsTab);
			fc.utobj().clickElement(driver, pobj.detailView);
			new HubCommonMethods().sortByRecentlyUploadedDocuments(driver);
			actionImgIconDetailsView(driver, documentTitle, "Delete");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Delete Document");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, documentTitle);
			if (isTextPresent == true) {
				fc.utobj().throwsException("was not able to delete document");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library","hub_0806","hubf1121"})
	@TestCase(createdOn = "2017-06-22", updatedOn = "2017-06-22", testCaseDescription = "Verify The Delete Document At Admin > The Hub > Library >  Drafts [List View]", testCaseId = "TC_The_Hub_Draft_List_View_Delete_Document_01")
	private void deleteDocumentActionImgListView03() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			AdminTheHubLibraryPage pobj = new AdminTheHubLibraryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "Franchise";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			addFolder(driver, library);

			fc.utobj().printTestStep("Add Draft Document");
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String futureDate1 = fc.utobj().getFutureDateUSFormat(1);
			String afterDate = dataSet.get("afterDate");
			int val = Integer.parseInt(afterDate);
			String futureDate2 = fc.utobj().getFutureDateUSFormat(val);
			
			library.setTypeOfDocument("Draft");
			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setFutureDate(futureDate1);
			library.setSecondFutureDate(futureDate2);
			library.setDocumentType(dataSet.get("documentType"));
			library.setLinkUrl(dataSet.get("linkUrl"));
			library.setRecommendedDoc(false);
			library.setDocumentSubType(dataSet.get("documentSubType"));
			library.setFile(dataSet.get("fileName"));
			addDocument(driver, library);
			
			fc.utobj().printTestStep("Modify The Document Draft Tab > List View");
			fc.utobj().clickElement(driver, pobj.draftsTab);
			fc.utobj().clickElement(driver, pobj.listView);
			actionImgIconListView(driver, documentTitle, "Modify");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			documentTitle = fc.utobj().generateTestData("TestDoc");
			fc.utobj().sendKeys(driver, pobj.documentTitle, documentTitle);
			fc.utobj().sendKeys(driver, pobj.briefSummary, briefSummary);

			fc.utobj().selectDropDown(driver, pobj.documentSubType, dataSet.get("documentSubType"));
			if (!fc.utobj().isSelected(driver, pobj.documentType.get(0))) {
				fc.utobj().clickElement(driver, pobj.documentType.get(0));
			}

			String file = fc.utobj().getFilePathFromTestData(dataSet.get("fileName"));
			fc.utobj().moveToElement(driver, pobj.fileClick);
			pobj.fileClick.sendKeys(file);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().clickElement(driver, pobj.closeConfirmation);
			fc.utobj().switchFrameToDefault(driver);
			hub_common_method.sortByRecentlyUploadedDocuments(driver);

			boolean isDocumentTitlePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[@class='textTheme14 ' and .='" + documentTitle + "']");
			if (!isDocumentTitlePresent) {
				fc.utobj().throwsException("Not able to modify the Draft Document at List View");
			}
			fc.utobj().printTestStep("Delete Document");
			actionImgIconListView(driver, documentTitle, "Delete");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Delete Document");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, documentTitle);
			if (isTextPresent == true) {
				fc.utobj().throwsException("was not able to delete document");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library", "hub0605" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Verify The Document Properties At Admin > The Hub > Library >  Drafts [Detail View].", testCaseId = "TC_78_Document_Properties")
	private void documentPropertiesActionImg03() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			AdminTheHubLibraryPage pobj = new AdminTheHubLibraryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			addFolder(driver, library);

			fc.utobj().printTestStep("Add Draft Document");
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String futureDate1 = fc.utobj().getFutureDateUSFormat(1);
			String afterDate = dataSet.get("afterDate");
			int val = Integer.parseInt(afterDate);
			String futureDate2 = fc.utobj().getFutureDateUSFormat(val);
			String currentDate=fc.utobj().getCurrentDateUSFormat();
			
			library.setTypeOfDocument("Draft");
			library.setDocumentSubType(dataSet.get("documentSubType"));
			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setCurrentDate(currentDate);
			library.setFile(dataSet.get("fileName"));
			library.setFutureDate(futureDate1);
			library.setSecondFutureDate(futureDate2);
			library.setRecommendedDoc(false);
			library.setDocumentType(dataSet.get("documentType"));
			addDocument(driver, library);
			
			fc.utobj().printTestStep("Verify The Properties Of Draft DOcument");
			fc.utobj().clickElement(driver, pobj.draftsTab);
			fc.utobj().clickElement(driver, pobj.detailView);
			new HubCommonMethods().sortByRecentlyUploadedDocuments(driver);
			actionImgIconDetailsView(driver, documentTitle, "Document Properties");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			if (!fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , 'Document Title')]/following-sibling::td[.='" + documentTitle + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document Title");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Summary')]/following-sibling::td[.='" + briefSummary + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document's Brief Summary");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Document Type')]/following-sibling::td[contains(text () , '"
									+ dataSet.get("documentType") + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document Type");
			}
			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Document Subtype')]/following-sibling::td[contains(text () , '"
									+ dataSet.get("documentSubType") + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify DocumentSub Type");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Uploaded By')]/following-sibling::td[contains(text () , 'FranConnect Administrator')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify DocumentSub Type");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Recommended')]/following-sibling::td[contains(text () , '"
									+ dataSet.get("recommendedDoc") + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Recommended");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library" , "hubfailed0928_1","hubf1121"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Verify the Post Comment At Admin > The Hub > Library >  Drafts [Detail View]", testCaseId = "TC_79_Post_Comment")
	private void postCommentActionImg02() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users Page");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUserPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			
			String emailId = "hubautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corporateUserPage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			
			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login with newly added corporate user");
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());

			AdminTheHubLibraryPage pobj = new AdminTheHubLibraryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			addFolder(driver, library);

			fc.utobj().printTestStep("Add Draft Document");
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String futureDate1 = fc.utobj().getFutureDateUSFormat(1);
			String afterDate = dataSet.get("afterDate");
			int val = Integer.parseInt(afterDate);
			String futureDate2 = fc.utobj().getFutureDateUSFormat(val);
			library.setTypeOfDocument("Draft");
			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setFutureDate(futureDate1);
			library.setSecondFutureDate(futureDate2);
			library.setDocumentSubType(dataSet.get("documentSubType"));
			library.setDocumentType(dataSet.get("documentType"));
			library.setFile(dataSet.get("fileName"));
			library.setRecommendedDoc(false);
			addDocument(driver, library);

			fc.utobj().printTestStep("Logged Out from corporate user");
			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Log in by Adm User and post comment on newly added document");
			fc.loginpage().loginWithParameter(driver, FranconnectUtil.config.get("userName"), FranconnectUtil.config.get("password"));
			
			fc.utobj().printTestStep("Go To Admin > The Hub Library");
			fc.hub().hub_common().adminTheHubLibraryPage(driver);
			
			fc.utobj().printTestStep("Post Comment");
			fc.utobj().clickElement(driver, pobj.draftsTab);
			fc.utobj().clickElement(driver, pobj.detailView);

			postCommentActionImg(driver, documentTitle, "Post Comment");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().isTextDisplayed(driver, documentTitle, "was not able to verify Document Title");
			fc.utobj().isTextDisplayed(driver, briefSummary, "was not able to verify Brief Summary");

			fc.utobj().clickElement(driver, pobj.comment);
			String commentText = fc.utobj().generateTestData(dataSet.get("commentText"));
			WebElement element12 = fc.utobj().getElementByXpath(driver, ".//*[contains(@id , 'ta')]");
			fc.utobj().sendKeys(driver, element12, commentText);
			fc.utobj().clickElement(driver, pobj.post);

			fc.utobj().printTestStep("Verify The Post Comment");
			fc.utobj().isTextDisplayed(driver, commentText, "was not able to verify Comment Text");

			fc.commonMethods().Click_Close_Input_ByValue(driver);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify Post Comment option from document Comment link from Index > List View");
			String commentTextList = fc.utobj().generateTestData("Test Comment Text");
			library.setCommentText(commentTextList);
			hub_common_method.sortByRecentlyUploadedDocuments(driver);
			hub_common_method.postCommentAtListView(driver, pobj, library);
			boolean isCommentTextPresent = fc.utobj().assertPageSource(driver, library.getCommentText());
			if (!isCommentTextPresent) {
				fc.utobj().throwsException("Not able to verify added Comment at List View");
			}
			
			fc.utobj().printTestStep("Verify A Commented mail should be come to document uploader");
			Map<String, String> mailInfo=new HashMap<String,String>();
			mailInfo=fc.utobj().readMailBox("Library Document Comment", library.getCommentText(), /*corpUser.getEmail()*/"hubautomation@staffex.com", "sdg@1a@Hfs");
			if (!mailInfo.get("mailBody").contains(library.getCommentText())) {
				fc.utobj().throwsException("Not able to verify mail after posting comment at draft document");
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"thehub", "hub_library"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Verify The Delete Comment At Admin > The Hub > Library >  Drafts [Detail View].", testCaseId = "TC_80_Delete_Comment")
	private void deleteComment03() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			AdminTheHubLibraryPage pobj = new AdminTheHubLibraryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			addFolder(driver, library);

			fc.utobj().printTestStep("Add Draft Document");
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String futureDate1 = fc.utobj().getFutureDateUSFormat(1);
			String afterDate = dataSet.get("afterDate");
			int val = Integer.parseInt(afterDate);
			String futureDate2 = fc.utobj().getFutureDateUSFormat(val);
			
			library.setTypeOfDocument("Draft");
			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setFutureDate(futureDate1);
			library.setSecondFutureDate(futureDate2);
			library.setDocumentType(dataSet.get("documentType"));
			library.setDocumentSubType(dataSet.get("documentSubType"));
			library.setFile(dataSet.get("fileName"));
			library.setRecommendedDoc(false);
			addDocument(driver, library);
			
			fc.utobj().printTestStep("Delete The Post Comment");
			fc.utobj().clickElement(driver, pobj.draftsTab);
			fc.utobj().clickElement(driver, pobj.detailView);
			postCommentActionImg(driver, documentTitle, "Post Comment");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().isTextDisplayed(driver, documentTitle, "was not able to verify Document Title");
			fc.utobj().isTextDisplayed(driver, briefSummary, "was not able to verify Brief Summary");

			fc.utobj().clickElement(driver, pobj.comment);
			String commentText = fc.utobj().generateTestData(dataSet.get("commentText"));
			WebElement element12 = fc.utobj().getElementByXpath(driver, ".//*[contains(@id , 'ta')]");
			fc.utobj().sendKeys(driver, element12, commentText);
			fc.utobj().clickElement(driver, pobj.post);
			fc.utobj().clickElement(driver, pobj.deleteComment);
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Delete Comment");
			boolean isTextPresent = fc.utobj().assertNotInPageSource(driver, commentText);
			if (isTextPresent) {
				fc.utobj().throwsException("was not able to delete Comment");
			}
			fc.commonMethods().Click_Close_Input_ByValue(driver);
			fc.utobj().switchFrameToDefault(driver);
			
			//TC-63729
			fc.utobj().printTestStep("Add Comment at list view of draft document");
			fc.utobj().clickElement(driver, pobj.draftsTab);
			fc.utobj().clickElement(driver, pobj.listView);
			String commentTextListView = fc.utobj().generateTestData("Comment Text");
			library.setDocumentTitle(documentTitle);
			library.setCommentText(commentTextListView);
			hub_common_method.postCommentAtListView(driver, pobj, library);
			
			if (!fc.utobj().assertPageSource(driver, library.getCommentText())) {
				fc.utobj().throwsException("Not able to verify the post comment at draft at list view");
			}

			fc.utobj().clickElement(driver, pobj.deleteComment);
			fc.utobj().acceptAlertBox(driver);
			
			boolean isCommentPresent=fc.utobj().assertNotInPageSource(driver, library.getCommentText());
			if (isCommentPresent) {
				fc.utobj().throwsException("Not able to delete the draft document comment text at list view");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Verify The Version History At Admin > The Hub > Library >  Drafts [Detail View]", testCaseId = "TC_81_Version_History")
	private void versionHistory03() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			AdminTheHubLibraryPage pobj = new AdminTheHubLibraryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			addFolder(driver, library);

			fc.utobj().printTestStep("Add Draft Document");
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String futureDate1 = fc.utobj().getFutureDateUSFormat(1);
			String afterDate = dataSet.get("afterDate");
			int val = Integer.parseInt(afterDate);
			String futureDate2 = fc.utobj().getFutureDateUSFormat(val);
			
			library.setTypeOfDocument("Draft");
			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setFutureDate(futureDate1);
			library.setSecondFutureDate(futureDate2);
			library.setDocumentType(dataSet.get("documentType"));
			library.setDocumentSubType(dataSet.get("documentSubType"));
			library.setFile(dataSet.get("fileName"));
			library.setRecommendedDoc(false);
			addDocument(driver, library);
			
			
			fc.utobj().printTestStep("Verify The Version history");
			fc.utobj().clickElement(driver, pobj.draftsTab);
			fc.utobj().clickElement(driver, pobj.detailView);
			actionImgIconDetailsView(driver, documentTitle, "Version History");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Document Title')]/following-sibling::td[contains(text () , '"
									+ documentTitle + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document Title");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Brief Summary')]/following-sibling::td[contains(text () , '"
									+ briefSummary + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document's Brief Summary");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library","hub_0806","hubf1121"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Verify The Delete Document At Admin > The Hub > Library >  Drafts [Detail View]", testCaseId = "TC_82_Delete_Document")
	private void deleteDocument02() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			AdminTheHubLibraryPage pobj = new AdminTheHubLibraryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			addFolder(driver, library);

			fc.utobj().printTestStep("Add Draft Document");
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String futureDate1 = fc.utobj().getFutureDateUSFormat(1);
			String afterDate = dataSet.get("afterDate");
			int val = Integer.parseInt(afterDate);
			String futureDate2 = fc.utobj().getFutureDateUSFormat(val);
			
			library.setTypeOfDocument("Draft");
			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setFutureDate(futureDate1);
			library.setSecondFutureDate(futureDate2);
			library.setDocumentType(dataSet.get("documentType"));
			library.setDocumentSubType(dataSet.get("documentSubType"));
			library.setFile(dataSet.get("fileName"));
			library.setRecommendedDoc(false);
			addDocument(driver, library);
			
			fc.utobj().printTestStep("Delete Document");
			fc.utobj().clickElement(driver, pobj.draftsTab);
			fc.utobj().clickElement(driver, pobj.detailView);
			fc.utobj().clickElement(driver, pobj.deleteLink);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[.='" + documentTitle + "']/ancestor::tr/td/input"));
			fc.utobj().clickElement(driver, pobj.deleteBtn);
			fc.utobj().acceptAlertBox(driver);
			
			fc.utobj().printTestStep("Verify The Delete Draft Document at details View");
			fc.utobj().clickElement(driver, pobj.draftsTab);
			fc.utobj().clickElement(driver, pobj.detailView);
			
			boolean isTextPresent = fc.utobj().assertPageSource(driver, documentTitle);
			if (isTextPresent) {
				fc.utobj().throwsException("was not able to delete draft document");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Verify The Report Document At Admin > The Hub > Library >  Drafts [Thumbnail View]", testCaseId = "TC_83_Report_Document")
	private void reportDocument03() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			AdminTheHubLibraryPage pobj = new AdminTheHubLibraryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			addFolder(driver, library);

			fc.utobj().printTestStep("Add Draft Document");
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String futureDate1 = fc.utobj().getFutureDateUSFormat(1);
			String afterDate = dataSet.get("afterDate");
			int val = Integer.parseInt(afterDate);
			String futureDate2 = fc.utobj().getFutureDateUSFormat(val);
			String currentDate = fc.utobj().getCurrentDateUSFormat();
			
			library.setTypeOfDocument("Draft");
			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setFutureDate(futureDate1);
			library.setSecondFutureDate(futureDate2);
			library.setDocumentType(dataSet.get("documentType"));
			library.setDocumentSubType(dataSet.get("documentSubType"));
			library.setFile(dataSet.get("fileName"));
			library.setRecommendedDoc(false);
			addDocument(driver, library);

			fc.utobj().clickElement(driver, pobj.draftsTab);
			fc.utobj().clickElement(driver, pobj.thumbnailView);

			if (!fc.utobj().getElementByXpath(driver, ".//*[@id='thums']//a[.='" + documentTitle + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document Title At Thumbnail View Page");
			}

			fc.utobj().printTestStep("Verify The Report of Document");
			thumbnailOption(driver, documentTitle, "Report");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().isTextDisplayed(driver, documentTitle, "Was not able to verify document Title");

			if (!fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , 'Brief Summary :')]/following-sibling::td[.='" + briefSummary + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Brief Summary :");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Uploaded By :')]/following-sibling::td[.='FranConnect Administrator']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Uploaded By :");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Uploaded On :')]/following-sibling::td[.='" + currentDate + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Uploaded Date :");
			}

			if (!fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , 'Start Display Date :')]/following-sibling::td[.='" + futureDate1 + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Start Display Date :");
			}

			if (!fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , 'Expiration Date :')]/following-sibling::td[.='" + futureDate2 + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Start Display Date :");
			}
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Verify The Document Properties At Admin > The Hub > Library >  Drafts [Thumbnail View]", testCaseId = "TC_84_Document_Properties")
	private void documentProperties03() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			AdminTheHubLibraryPage pobj = new AdminTheHubLibraryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			addFolder(driver, library);

			fc.utobj().printTestStep("Add Draft Document");
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String futureDate1 = fc.utobj().getFutureDateUSFormat(1);
			String afterDate = dataSet.get("afterDate");
			int val = Integer.parseInt(afterDate);
			String futureDate2 = fc.utobj().getFutureDateUSFormat(val);

			library.setTypeOfDocument("Draft");
			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setFutureDate(futureDate1);
			library.setSecondFutureDate(futureDate2);
			library.setDocumentType(dataSet.get("documentType"));
			library.setDocumentSubType(dataSet.get("documentSubType"));
			library.setFile(dataSet.get("fileName"));
			library.setRecommendedDoc(false);
			addDocument(driver, library);

			fc.utobj().printTestStep("Verify The Document Properties");
			fc.utobj().clickElement(driver, pobj.draftsTab);
			fc.utobj().clickElement(driver, pobj.thumbnailView);

			thumbnailOption(driver, documentTitle, "Document Properties");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			if (!fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , 'Document Title')]/following-sibling::td[.='" + documentTitle + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document Title");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Summary')]/following-sibling::td[.='" + briefSummary + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document's Brief Summary");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Document Type')]/following-sibling::td[contains(text () , '"
									+ dataSet.get("documentType") + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document Type");
			}
			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Document Subtype')]/following-sibling::td[contains(text () , '"
									+ dataSet.get("documentSubType") + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify DocumentSub Type");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Uploaded By')]/following-sibling::td[contains(text () , 'FranConnect Administrator')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify DocumentSub Type");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Recommended')]/following-sibling::td[contains(text () , '"
									+ dataSet.get("recommendedDoc") + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Recommended");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Verify The Post Comment At Admin > The Hub > Library >  Drafts [Thumbnail View]", testCaseId = "TC_85_Post_Comment")
	private void postComment03() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			AdminTheHubLibraryPage pobj = new AdminTheHubLibraryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			addFolder(driver, library);

			fc.utobj().printTestStep("Add Draft Document");
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String futureDate1 = fc.utobj().getFutureDateUSFormat(1);
			String afterDate = dataSet.get("afterDate");
			int val = Integer.parseInt(afterDate);
			String futureDate2 = fc.utobj().getFutureDateUSFormat(val);
			library.setTypeOfDocument("Draft");
			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setFutureDate(futureDate1);
			library.setSecondFutureDate(futureDate2);
			library.setDocumentType(dataSet.get("documentType"));
			library.setDocumentSubType(dataSet.get("documentSubType"));
			library.setFile(dataSet.get("fileName"));
			library.setRecommendedDoc(false);
			addDocument(driver, library);

			fc.utobj().printTestStep("Post Comment");
			fc.utobj().clickElement(driver, pobj.draftsTab);
			fc.utobj().clickElement(driver, pobj.thumbnailView);
			thumbnailOption(driver, documentTitle, "Post Comment");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().isTextDisplayed(driver, documentTitle, "was not able to verify Document Title");
			fc.utobj().isTextDisplayed(driver, briefSummary, "was not able to verify Brief Summary");

			fc.utobj().clickElement(driver, pobj.comment);
			String commentText = fc.utobj().generateTestData(dataSet.get("commentText"));
			WebElement element12 = fc.utobj().getElementByXpath(driver, ".//*[contains(@id , 'ta')]");
			System.out.println(element12);
			fc.utobj().sendKeys(driver, element12, commentText);
			fc.utobj().clickElement(driver, pobj.post);

			fc.utobj().printTestStep("Verify The Post Comment");
			fc.utobj().isTextDisplayed(driver, commentText, "was not able to verify Comment Text");

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Delete Comment : All Users Access : Summary Format 'Text' : Admin > The Hub > Library >  Drafts [Thumbnail View]", testCaseId = "TC_86_Delete_Comment")
	private void deleteComment04() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			AdminTheHubLibraryPage pobj = new AdminTheHubLibraryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			addFolder(driver, library);

			fc.utobj().printTestStep("Add Draft Document");
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String futureDate1 = fc.utobj().getFutureDateUSFormat(1);
			String afterDate = dataSet.get("afterDate");
			int val = Integer.parseInt(afterDate);
			String futureDate2 = fc.utobj().getFutureDateUSFormat(val);
			library.setTypeOfDocument("Draft");
			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setFutureDate(futureDate1);
			library.setSecondFutureDate(futureDate2);
			library.setDocumentSubType(dataSet.get("documentSubType"));
			library.setDocumentType(dataSet.get("documentType"));
			library.setFile(dataSet.get("fileName"));
			library.setRecommendedDoc(false);
			addDocument(driver, library);

			fc.utobj().printTestStep("Post Comment");
			fc.utobj().clickElement(driver, pobj.draftsTab);
			fc.utobj().clickElement(driver, pobj.thumbnailView);
			thumbnailOption(driver, documentTitle, "Post Comment");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().isTextDisplayed(driver, documentTitle, "was not able to verify Document Title");
			fc.utobj().isTextDisplayed(driver, briefSummary, "was not able to verify Brief Summary");

			fc.utobj().printTestStep("Delete Document");
			fc.utobj().clickElement(driver, pobj.comment);
			String commentText = fc.utobj().generateTestData(dataSet.get("commentText"));
			WebElement element12 = fc.utobj().getElementByXpath(driver, ".//*[contains(@id , 'ta')]");
			System.out.println(element12);
			fc.utobj().sendKeys(driver, element12, commentText);
			fc.utobj().clickElement(driver, pobj.post);
			fc.utobj().clickElement(driver, pobj.deleteComment);
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Delete Document");
			boolean isTextPresent = fc.utobj().assertNotInPageSource(driver, commentText);
			if (isTextPresent) {
				fc.utobj().throwsException("was not able to delete Comment");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Verify The Version History At Admin > The Hub > Library >  Drafts [Thumbnail View]", testCaseId = "TC_87_Version_History")
	private void versionHistory04() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			AdminTheHubLibraryPage pobj = new AdminTheHubLibraryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			addFolder(driver, library);

			fc.utobj().printTestStep("Add Draft Document");
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String futureDate1 = fc.utobj().getFutureDateUSFormat(1);
			String afterDate = dataSet.get("afterDate");
			int val = Integer.parseInt(afterDate);
			String futureDate2 = fc.utobj().getFutureDateUSFormat(val);
			library.setTypeOfDocument("Draft");
			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setFutureDate(futureDate1);
			library.setSecondFutureDate(futureDate2);
			library.setDocumentType(dataSet.get("documentType"));
			library.setDocumentSubType(dataSet.get("documentSubType"));
			library.setFile(dataSet.get("fileName"));
			library.setRecommendedDoc(false);
			addDocument(driver, library);

			fc.utobj().printTestStep("Verify The Version History");
			fc.utobj().clickElement(driver, pobj.draftsTab);
			fc.utobj().clickElement(driver, pobj.thumbnailView);
			thumbnailOption(driver, documentTitle, "Version History");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Document Title')]/following-sibling::td[contains(text () , '"
									+ documentTitle + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document Title");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Brief Summary')]/following-sibling::td[contains(text () , '"
									+ briefSummary + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document's Brief Summary");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Verify The Delete Document At Admin > The Hub > Library >  Drafts [Thumbnail View]", testCaseId = "TC_88_Delete_Document")
	private void deleteDocumentBtn01() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			AdminTheHubLibraryPage pobj = new AdminTheHubLibraryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";
			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			addFolder(driver, library);

			fc.utobj().printTestStep("Add Draft Document");

			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String futureDate1 = fc.utobj().getFutureDateUSFormat(1);
			String afterDate = dataSet.get("afterDate");
			int val = Integer.parseInt(afterDate);
			String futureDate2 = fc.utobj().getFutureDateUSFormat(val);
			library.setTypeOfDocument("Draft");
			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setFutureDate(futureDate1);
			library.setSecondFutureDate(futureDate2);
			library.setDocumentType(dataSet.get("documentType"));
			library.setDocumentSubType(dataSet.get("documentSubType"));
			library.setFile(dataSet.get("fileName"));
			library.setRecommendedDoc(false);
			addDocument(driver, library);

			fc.utobj().printTestStep("Delete Document");
			fc.utobj().clickElement(driver, pobj.draftsTab);
			fc.utobj().clickElement(driver, pobj.thumbnailView);

			WebElement element = fc.utobj().getElementByXpath(driver, ".//*[@id='thums']//*[.='" + documentTitle + "']"
					+ "//following::table[@class='topLine pt5'][1]//td/a[contains(@onclick, 'deletedocument')]");
			fc.utobj().clickElement(driver, element);
			fc.utobj().acceptAlertBox(driver);
			fc.utobj().clickElement(driver, pobj.thumbnailView);

			fc.utobj().printTestStep("Verify The Delete Document");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, documentTitle);

			if (isTextPresent == true) {
				fc.utobj().throwsException("was not able to delete Library Document");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library","hubf1121"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Verify The Modify Document At Admin > The Hub > Library >  Drafts [Thumbnail View]", testCaseId = "TC_89_Modify_Document")
	private void modifyDocumentBtn02() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			AdminTheHubLibraryPage pobj = new AdminTheHubLibraryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";
			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			addFolder(driver, library);

			fc.utobj().printTestStep("Add Draft Document");
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String old_document=documentTitle;
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String futureDate1 = fc.utobj().getFutureDateUSFormat(1);
			String afterDate = dataSet.get("afterDate");
			int val = Integer.parseInt(afterDate);
			String futureDate2 = fc.utobj().getFutureDateUSFormat(val);
			library.setTypeOfDocument("Draft");
			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setFutureDate(futureDate1);
			library.setSecondFutureDate(futureDate2);
			library.setDocumentType(dataSet.get("documentType"));
			library.setDocumentSubType(dataSet.get("documentSubType"));
			library.setFile(dataSet.get("fileName"));
			library.setRecommendedDoc(false);
			addDocument(driver, library);

			fc.utobj().printTestStep("Modify The Document");
			fc.utobj().clickElement(driver, pobj.draftsTab);
			fc.utobj().clickElement(driver, pobj.thumbnailView);
			WebElement element = fc.utobj().getElementByXpath(driver, ".//*[@id='thums']//*[.='" + documentTitle + "']"
					+ "//following::table[@class='topLine pt5'][1]//td/a/img[@title='Modify']");
			fc.utobj().clickElement(driver, element);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			fc.utobj().sendKeys(driver, pobj.documentTitle, documentTitle);
			briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			fc.utobj().sendKeys(driver, pobj.briefSummary, briefSummary);

			String futureDate3 = fc.utobj().getFutureDateUSFormat(1);
			String futureDate4 = fc.utobj().getFutureDateUSFormat(val);
			String currentDate = fc.utobj().getCurrentDateUSFormat();
			fc.utobj().sendKeys(driver, pobj.displayDate, futureDate3);
			fc.utobj().sendKeys(driver, pobj.expirationDate, futureDate4);

			if (!fc.utobj().isSelected(driver, pobj.documentType.get(0))) {
				fc.utobj().clickElement(driver, pobj.documentType.get(0));
			}

			fc.utobj().selectDropDown(driver, pobj.documentSubType, dataSet.get("documentSubType"));
			String file =fc.utobj().getFilePathFromTestData(dataSet.get("fileName"));

			fc.utobj().moveToElement(driver, pobj.fileClick);
			pobj.fileClick.sendKeys(file);

			if (!fc.utobj().isSelected(driver, pobj.thumbFileUploaded.get(1))) {
				fc.utobj().clickElement(driver, pobj.thumbFileUploaded.get(1));
			}

			if (!fc.utobj().isSelected(driver, pobj.accessibilityPublic)) {
				fc.utobj().clickElement(driver, pobj.accessibilityPublic);
			}

			fc.utobj().clickElement(driver, pobj.saveBtn);
			pobj = new AdminTheHubLibraryPage(driver);
			fc.utobj().clickElement(driver, pobj.closeConfirmation);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, pobj.thumbnailView);
			if (!fc.utobj().getElementByXpath(driver, ".//*[@id='thums']//a[.='" + documentTitle + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document Title At Thumbnail View Page");
			}

			fc.utobj().printTestStep("Verify The Modify Document");
			thumbnailOption(driver, documentTitle, "Report");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().isTextDisplayed(driver, documentTitle, "Was not able to verify document Title");

			if (!fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , 'Brief Summary :')]/following-sibling::td[.='" + briefSummary + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Brief Summary :");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Uploaded By :')]/following-sibling::td[.='FranConnect Administrator']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Uploaded By :");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Uploaded On :')]/following-sibling::td[.='" + currentDate + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Uploaded Date :");
			}

			if (!fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , 'Start Display Date :')]/following-sibling::td[.='" + futureDate3 + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Start Display Date :");
			}

			if (!fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , 'Expiration Date :')]/following-sibling::td[.='" + futureDate4 + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Start Display Date :");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"thehub", "hub_library"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Verify The Add Document At Admin > The Hub > Library >  Folder", testCaseId = "TC_90_Add_Document")
	private void addDocument01() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			AdminTheHubLibraryPage pobj = new AdminTheHubLibraryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";
			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			addFolder(driver, library);

			fc.utobj().printTestStep("Add Document");
			clickOverMoreLink(driver, folderName);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='folderDiv']//*[contains(text(), '" + folderName + "')]"));
			fc.utobj().clickElement(driver, pobj.addDocument);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			if (!fc.utobj().isSelected(driver, pobj.multipleDoc.get(1))) {
				fc.utobj().clickElement(driver, pobj.multipleDoc.get(1));
			}

			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle").concat("random"));
			fc.utobj().sendKeys(driver, pobj.documentTitle, documentTitle);
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			fc.utobj().sendKeys(driver, pobj.briefSummary, briefSummary);

			String currentDate = fc.utobj().getCurrentDateUSFormat();
			String futureDate = fc.utobj().getFutureDateUSFormat(2);
			fc.utobj().sendKeys(driver, pobj.displayDate, currentDate);
			fc.utobj().sendKeys(driver, pobj.expirationDate, futureDate);

			if (!fc.utobj().isSelected(driver, pobj.documentType.get(0))) {
				fc.utobj().clickElement(driver, pobj.documentType.get(0));
			}

			fc.utobj().selectDropDown(driver, pobj.documentSubType, dataSet.get("subType"));
			String file = fc.utobj().getFilePathFromTestData(dataSet.get("fileName"));
			fc.utobj().moveToElement(driver, pobj.fileClick);
			pobj.fileClick.sendKeys(file);

			if (!fc.utobj().isSelected(driver, pobj.thumbFileUploaded.get(1))) {
				fc.utobj().clickElement(driver, pobj.thumbFileUploaded.get(1));
			}

			if (!fc.utobj().isSelected(driver, pobj.accessibilityPublic)) {
				fc.utobj().clickElement(driver, pobj.accessibilityPublic);
			}

			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().sleep();
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Add Document");
			clickOverMoreLink(driver, folderName);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='folderDiv']//*[contains(text(), '" + folderName + "')]"));

			fc.utobj().clickElement(driver, pobj.listView);

			library.setSortBy("Recently Uploaded");
			library.setDocumentSubType(dataSet.get("subType"));
			hub_common_method.sortDocumentsByFilter(driver, library);

			if (!fc.utobj()
					.getElementByXpath(driver, ".//*[@id='thumDtls']//a[contains(text () ,'" + documentTitle + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document title Folder List View");
			}

			if (!fc.utobj()
					.getElementByXpath(driver, ".//*[@id='thumDtls']//span[contains(text () , '" + briefSummary + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Brief Summary Of document at List View");
			}

			fc.utobj().clickElement(driver, pobj.thumbnailView);

			if (!fc.utobj()
					.getElementByXpath(driver, ".//*[@id='thums']//a[contains(text () ,'" + documentTitle + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document at List View");
			}
			fc.utobj().clickElement(driver, pobj.detailView);
			fc.utobj().isTextDisplayed(driver, documentTitle, "was not able to verify Document at List View");

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library" })
	@TestCase(createdOn = "2018-06-11", updatedOn = "2017-06-11", testCaseDescription = "Verify The Addition of multiple Document At Admin > The Hub > Library > Folder", testCaseId = "TC_Admin_The_Hub_Add_Multiple_Document_01")
	private void addMultipleDocuments() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			AdminTheHubLibraryPage pobj = new AdminTheHubLibraryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";
			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			addFolder(driver, library);

			fc.utobj().printTestStep("Add Multiple Documents");
			fc.utobj().clickElement(driver, pobj.addDocument);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			if (!fc.utobj().isSelected(driver, pobj.multipleDoc.get(0))) {
				fc.utobj().clickElement(driver, pobj.multipleDoc.get(0));
			}

			fc.utobj().printTestStep("Verify Add More Link");
			boolean isLinkPressent = fc.utobj().assertLinkText(driver, "Add More");
			if (!isLinkPressent) {
				fc.utobj().throwsException("Add More Link is not present");
			}

			try {
				fc.utobj().selectDropDown(driver, pobj.folderSelect, library.getFolderName());
			} catch (Exception e) {
				// TODO: handle exception
			}

			ArrayList<String> docTitleList = new ArrayList<>();
			docTitleList.add(fc.utobj().generateTestData("Test1doctitle"));
			docTitleList.add(fc.utobj().generateTestData("Test2doctitle"));
			docTitleList.add(fc.utobj().generateTestData("Test3doctitle"));
			docTitleList.add(fc.utobj().generateTestData("Test4doctitle"));
			docTitleList.add(fc.utobj().generateTestData("Test5doctitle"));

			ArrayList<String> docBriefList = new ArrayList<>();
			docBriefList.add(fc.utobj().generateTestData("Test1docsummary"));
			docBriefList.add(fc.utobj().generateTestData("Test2docsummary"));
			docBriefList.add(fc.utobj().generateTestData("Test3docsummary"));
			docBriefList.add(fc.utobj().generateTestData("Test4docsummary"));
			docBriefList.add(fc.utobj().generateTestData("Test5docsummary"));

			for (int i = 0; i < docTitleList.size(); i++) {
				fc.utobj().sendKeys(driver,
						fc.utobj().getElementByID(driver, "libraryDocuments_" + i + "documentTitle"),
						docTitleList.get(i));
				fc.utobj().sendKeys(driver, fc.utobj().getElementByID(driver, "libraryDocuments_" + i + "summary"),
						docBriefList.get(i));

				if (!fc.utobj().getElementByName(driver, "isWebDocument" + i + "").isSelected()) {
					fc.utobj().clickElement(driver, fc.utobj().getElementByName(driver, "isWebDocument" + i + ""));
				}
				fc.utobj().selectDropDown(driver,
						fc.utobj().getElementByName(driver, "libraryDocuments_" + i + "typeID"), "PDF");
				fc.utobj().sendKeys(driver, fc.utobj().getElementByName(driver, "libraryDocuments_" + i + "docFile"),
						fc.utobj().getFilePathFromTestData("taskFile.pdf"));
			}

			fc.utobj().clickElement(driver, pobj.multiDocAddBtn);
			fc.utobj().sleep();
			fc.utobj().clickElement(driver, pobj.closeConfirmation);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Add Document");
			clickOverMoreLink(driver, folderName);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='folderDiv']//*[contains(text(), '" + folderName + "')]"));
			fc.utobj().clickElement(driver, pobj.listView);
			boolean b[] = new boolean[5];

			for (int i = 0; i < docTitleList.size(); i++) {
				b[i] = fc.utobj().assertPageSource(driver, docTitleList.get(i));
			}

			for (int i = 0; i < docTitleList.size(); i++) {
				if (!b[i]) {
					fc.utobj().throwsException("Documnet is not present");
				}
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addFolderDocument(WebDriver driver, String folderName, String documentTitle, String briefSummary,
			Map<String, String> dataSet, String currentDate, String futureDate) throws Exception {

		String testCaseId = "TC_Add_Folder_Document_The_Hub";
		String editorText = null;

		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminTheHubLibraryPage pobj = new AdminTheHubLibraryPage(driver);
				clickOverMoreLink(driver, folderName);
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[@id='folderDiv']//*[contains(text() , '" + folderName + "')]"));
				fc.utobj().clickElement(driver, pobj.addDocument);
				fc.commonMethods().switch_cboxIframe_frameId(driver);

				if (!fc.utobj().isSelected(driver, pobj.multipleDoc.get(1))) {
					fc.utobj().clickElement(driver, pobj.multipleDoc.get(1));
				}

				fc.utobj().sendKeys(driver, pobj.documentTitle, documentTitle);
				fc.utobj().sendKeys(driver, pobj.briefSummary, briefSummary);

				fc.utobj().sendKeys(driver, pobj.displayDate, currentDate);
				fc.utobj().sendKeys(driver, pobj.expirationDate, futureDate);

				String documentType = dataSet.get("documentType");

				if (documentType.equalsIgnoreCase("Upload File")) {
					fc.utobj().selectDropDown(driver, pobj.documentSubType, dataSet.get("documentSubType"));

					String file = fc.utobj().getFilePathFromTestData(dataSet.get("fileName"));

					if (!fc.utobj().isSelected(driver, pobj.documentType.get(0))) {
						fc.utobj().clickElement(driver, pobj.documentType.get(0));
					}

					fc.utobj().moveToElement(driver, pobj.fileClick);
					pobj.fileClick.sendKeys(file);

				} else if (documentType.equalsIgnoreCase("Web Link")) {

					if (!fc.utobj().isSelected(driver, pobj.documentType.get(1))) {
						fc.utobj().clickElement(driver, pobj.documentType.get(1));
					}

					fc.utobj().sendKeys(driver, pobj.linkUrl, dataSet.get("linkUrl"));

				} else if (documentType.equalsIgnoreCase("HTML Page")) {

					editorText = fc.utobj().generateTestData(dataSet.get("editorText"));

					if (!fc.utobj().isSelected(driver, pobj.documentType.get(2))) {
						fc.utobj().clickElement(driver, pobj.documentType.get(2));
					}

					fc.utobj().switchFrameById(driver, "htmlSummary_ifr");

					Actions actions = new Actions(driver);
					actions.moveToElement(fc.utobj().getElement(driver, pobj.editorTextArea));
					actions.click();
					actions.sendKeys(editorText);
					fc.utobj().logReportMsg("Entered Text", editorText);
					actions.build().perform();
					fc.utobj().switchFrameToDefault(driver);
				}

				if (!fc.utobj().isSelected(driver, pobj.thumbFileUploaded.get(1))) {
					fc.utobj().clickElement(driver, pobj.thumbFileUploaded.get(1));
				}

				if (dataSet.get("recommendedDoc").equalsIgnoreCase("Yes")) {
					fc.utobj().clickElement(driver, pobj.recommmendedDocumnet);

					fc.utobj().sendKeys(driver, pobj.recommendedExpDate, fc.utobj().getFutureDateUSFormat(4));

				} else if (dataSet.get("recommendedDoc").equalsIgnoreCase("No")) {

					if (fc.utobj().isSelected(driver, pobj.recommmendedDocumnet)) {
						fc.utobj().clickElement(driver, pobj.recommmendedDocumnet);
					}
				}

				if (!fc.utobj().isSelected(driver, pobj.accessibilityPublic)) {
					fc.utobj().clickElement(driver, pobj.accessibilityPublic);
				}

				fc.utobj().clickElement(driver, pobj.addBtn);
				fc.utobj().switchFrameToDefault(driver);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().clickElement(driver, pobj.closeBtn);
				fc.utobj().switchFrameToDefault(driver);

				clickOverMoreLink(driver, folderName);
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[@id='folderDiv']//*[contains(text(), '" + folderName + "')]"));
				fc.utobj().sleep();
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to add Folder Document");

		}
		return editorText;
	}

	@Test(groups = { "thehub", "hub_library" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Verify The Modify Folder At Admin > The Hub > Library >  Folder", testCaseId = "TC_91_Modify_Folder")
	private void modifyFolder() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			AdminTheHubLibraryPage pobj = new AdminTheHubLibraryPage(driver);

			fc.utobj().printTestStep("Add Corporate Role");
			String roleName = fc.utobj().generateTestData("Tc91CorFolder");
			new AdminUsersRolesAddNewRolePageTest().addCorporateRoles(driver, roleName);

			String roleName1 = fc.utobj().generateTestData("Tc91CorFolder");
			new AdminUsersRolesAddNewRolePageTest().addCorporateRoles(driver, roleName1);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";
			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			addFolder(driver, library);

			fc.utobj().printTestStep("Modify The Folder");
			clickOverMoreLink(driver, folderName);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='folderDiv']//*[contains(text() , '" + folderName + "')]"));
			fc.utobj().clickElement(driver, pobj.modifyFolderLink);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			fc.utobj().sendKeys(driver, pobj.folderName, folderName);
			String htmlText = fc.utobj().generateTestData(dataSet.get("htmlText"));

			if (!fc.utobj().isSelected(driver, pobj.accesToAllNo)) {
				fc.utobj().clickElement(driver, pobj.accesToAllNo);
			}

			WebElement element = fc.utobj().getElementByXpath(driver, ".//*[@id='roleDiv']//tr[1]/td[2]");
			String lines[] = fc.utobj().getText(driver, element).split("\\r?\\n");

			fc.utobj().printTestStep("Choose first role");

			for (int i = 1; i <= lines.length; i++) {

				if (lines[i].equalsIgnoreCase(roleName)) {
					System.out.println(lines[i]);
					i = i + 1;
					WebElement inputElement = element.findElement(By.xpath("./input[" + i + "]"));
					fc.utobj().clickElement(driver, inputElement);
					break;
				}
			}

			fc.utobj().printTestStep("Choose second role");
			for (int i = 1; i <= lines.length; i++) {

				if (lines[i].equalsIgnoreCase(roleName1)) {
					System.out.println(lines[i]);
					i = i + 1;
					WebElement inputElement = element.findElement(By.xpath("./input[" + i + "]"));
					fc.utobj().clickElement(driver, inputElement);
					break;
				}
			}

			if (!fc.utobj().isSelected(driver, pobj.summaryForamte.get(1))) {
				fc.utobj().clickElement(driver, pobj.summaryForamte.get(1));
			}

			fc.utobj().printTestStep(
					"TinyMCE Editor should be displayed with text field to Create Folders with HTML pages");

			try {
				fc.utobj().switchFrameById(driver, "ta_ifr");
			} catch (Exception e) {
				fc.utobj().throwsException(
						"TinyMCE Editor is not displayed with text field to Create Folders with HTML pages");
			}

			Actions actions = new Actions(driver);
			actions.moveToElement(fc.utobj().getElement(driver, pobj.editorTextArea));
			actions.click();
			actions.sendKeys(htmlText);
			fc.utobj().logReportMsg("Entered Text", htmlText);
			actions.build().perform();
			fc.utobj().switchFrameToDefault(driver);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();",
					fc.utobj().getElementByXpath(driver, ".//*[@id='startRecords']"));

			if (!fc.utobj().isSelected(driver, pobj.roleBase.get(1))) {
				fc.utobj().clickElement(driver, pobj.roleBase.get(1));
			}

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//input[@value='4']"));

			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			clickOverMoreLink(driver, folderName);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='folderDiv']//*[contains(text() , '" + folderName + "')]"));

			fc.utobj().printTestStep("Verify The Modified Folder");
			fc.utobj().isTextDisplayed(driver, folderName, "was not able to verify Folder Name");
			fc.utobj().isTextDisplayed(driver, htmlText, "was not able to verify Folder Summary");

			fc.utobj().printTestStep("Again Modify Folder and verify that selected role should be checked");
			fc.utobj().clickElement(driver, pobj.modifyFolderLink);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			WebElement elementModify = fc.utobj().getElementByXpath(driver, ".//*[@id='roleDiv']//tr[1]/td[2]");
			String linesModify[] = fc.utobj().getText(driver, elementModify).split("\\r?\\n");

			fc.utobj().printTestStep("Get First selected role status");
			boolean isFirstRoleChecked = false;

			for (int i = 1; i <= linesModify.length; i++) {

				if (linesModify[i].equalsIgnoreCase(roleName)) {
					System.out.println(linesModify[i]);
					i = i + 1;
					WebElement inputElement = elementModify.findElement(By.xpath("./input[" + i + "]"));
					isFirstRoleChecked = inputElement.isSelected();
					break;
				}
			}

			fc.utobj().printTestStep("Get Second selected role status");
			boolean isFirstRole1Checked = false;
			for (int i = 1; i <= linesModify.length; i++) {

				if (linesModify[i].equalsIgnoreCase(roleName1)) {
					System.out.println(linesModify[i]);
					i = i + 1;
					WebElement inputElement = elementModify.findElement(By.xpath("./input[" + i + "]"));
					isFirstRole1Checked = inputElement.isSelected();
					break;
				}
			}

			if (!isFirstRoleChecked) {
				fc.utobj().throwsException("First Role is not selected ater modifying the folder accessability");
			}
			if (!isFirstRole1Checked) {
				fc.utobj().throwsException("First Role is not selected ater modifying the folder accessability");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Verify The Delete Folder At Admin > The Hub > Library >  Folder", testCaseId = "TC_92_Delete_Folder")
	private void deleteFolder() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminTheHubLibraryPage pobj = new AdminTheHubLibraryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";
			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			addFolder(driver, library);

			fc.utobj().printTestStep("Delete Folder");
			clickOverMoreLink(driver, folderName);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='folderDiv']//*[contains(text() , '" + folderName + "')]"));
			fc.utobj().clickElement(driver, pobj.deleteFolderLink);
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Delete Folder");
			clickOverMoreLink(driver);
			
			boolean isTextPresent = fc.utobj().assertNotInPageSource(driver, folderName);
			if (isTextPresent) {
				fc.utobj().throwsException("was not able to delete Folder");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library","hubf1121"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-08-16", testCaseDescription = "Verify The Add Sub Folder In Folder At Admin > The Hub > Library >  Folder ", testCaseId = "TC_93_Add_Sub_Folder")
	private void addSubFolder() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminTheHubLibraryPage pobj = new AdminTheHubLibraryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";
			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			addFolder(driver, library);

			fc.utobj().printTestStep("Add Sub Folder");
			clickOverMoreLink(driver, folderName);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='folderDiv']//*[contains(text() , '" + folderName + "')]"));

			WebElement element1 = fc.utobj().getElementByXpath(driver,
					".//*[@id='librarybody']//*[contains(text () , 'More Actions')]");
			fc.utobj().moveToElement(driver, element1);
			Actions builder = new Actions(driver);
			builder.moveToElement(element1).build().perform();
			fc.utobj().getElement(driver, pobj.addSubFolder).click();

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String folderName1 = fc.utobj().generateTestData(dataSet.get("folderName"));
			fc.utobj().sendKeys(driver, pobj.folderName, folderName1);
			String htmlText = fc.utobj().generateTestData(dataSet.get("htmlText"));

			if (!fc.utobj().isSelected(driver, pobj.summaryForamte.get(1))) {
				fc.utobj().clickElement(driver, pobj.summaryForamte.get(1));
			}
			fc.utobj().switchFrameById(driver, "ta_ifr");
			Actions actions = new Actions(driver);
			actions.moveToElement(fc.utobj().getElement(driver, pobj.editorTextArea));
			actions.click();
			actions.sendKeys(htmlText);
			fc.utobj().logReportMsg("Entered Text", htmlText);
			actions.build().perform();
			fc.utobj().switchFrameToDefault(driver);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();",
					fc.utobj().getElementByXpath(driver, ".//*[@id='startRecords']"));

			if (!fc.utobj().isSelected(driver, pobj.roleBase.get(1))) {
				fc.utobj().clickElement(driver, pobj.roleBase.get(1));
			}

			fc.utobj().clickElement(driver, pobj.regionalUserAccess);

			fc.utobj().clickElement(driver, pobj.addBtn1);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			clickOverMoreLink(driver, folderName);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='folderDiv']//*[contains(text() , '" + folderName + "')]"));
			
			/*fc.utobj().clickElement(driver, ".//a[@class='wrapmenu bText12' and .='"+folderName1+"']");*/
			fc.utobj().sleep(2000);
			fc.utobj().clickElement(driver, ".//a[@class='wrapmenu bText12' and .='"+folderName1+"']");
			
			fc.utobj().printTestStep("Verify The Add Sub Folder");
			fc.utobj().isTextDisplayed(driver, folderName, "was not able to verify Parent Folder");
			fc.utobj().isTextDisplayed(driver, folderName1, "was not able to verify child Folder");
			fc.utobj().isTextDisplayed(driver, htmlText, "was not able to verify Summary of Child Folder");

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, folderName));
			fc.utobj().isTextDisplayed(driver, folderName, "was not able to verify Parent Folder");
			fc.utobj().isTextDisplayed(driver, folderName1, "was not able to verify child Folder");
			fc.utobj().isTextDisplayed(driver, folderSummary, "was not able to verify Summary of Parent Folder");
			
			fc.utobj().printTestStep("Modify The Added Sub folder");
			fc.utobj().clickElement(driver, ".//a[@class='wrapmenu bText12' and .='"+folderName1+"']");
			
			folderName1=fc.utobj().generateTestData("Testmsubfolder");
			fc.utobj().clickElement(driver, pobj.modifyFolderLink);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.folderName, folderName1);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().clickElement(driver, pobj.closeConfirmation);
			fc.utobj().switchFrameToDefault(driver);
			
			clickOverMoreLink(driver, folderName);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='folderDiv']//*[contains(text() , '" + folderName + "')]"));
			
			fc.utobj().printTestStep("Verify The Modified Sub Folder");
			if (!fc.utobj().isElementPresent(driver, ".//a[@class='wrapmenu bText12' and .='"+folderName1+"']")) {
				fc.utobj().throwsException("Modified sub folder is not present");
			}
			
			fc.utobj().printTestStep("Add Another sub folder");
			clickOverMoreLink(driver, folderName);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='folderDiv']//*[contains(text() , '" + folderName + "')]"));

			WebElement element3 = fc.utobj().getElementByXpath(driver,
					".//*[@id='librarybody']//*[contains(text () , 'More Actions')]");
			fc.utobj().moveToElement(driver, element3);
			builder.moveToElement(element3).build().perform();
			fc.utobj().getElement(driver, pobj.addSubFolder).click();

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String folderName2 = fc.utobj().generateTestData(dataSet.get("folderName"));
			fc.utobj().sendKeys(driver, pobj.folderName, folderName2);
			
			if (!fc.utobj().isSelected(driver, pobj.summaryForamte.get(0))) {
				fc.utobj().clickElement(driver, pobj.summaryForamte.get(0));
			}
			fc.utobj().sendKeys(driver, pobj.folderSummary, folderSummary);
			fc.utobj().clickElement(driver, pobj.addBtn1);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);
			
			fc.utobj().printTestStep("Go to Roles and create a corporate role with 'Can view Library' privilege only for Library section");
			fc.utobj().printTestStep("Admin > Users > Roles > Add New Role");
			fc.adminpage().adminUsersRolesAddNewRolePage(driver);
			AdminUsersRolesAddNewRolePage roles_page = new AdminUsersRolesAddNewRolePage(driver);
			fc.utobj().selectDropDownByValue(driver, roles_page.roleType, "1");
			String roleName=fc.utobj().generateTestData("TestCorRole");
			fc.utobj().sendKeys(driver, roles_page.roleName, roleName);
			
			if (fc.utobj().isSelected(driver, roles_page.allPrivileges)) {
				fc.utobj().clickElement(driver, roles_page.allPrivileges);
			}
			if (!fc.utobj().isSelected(driver, roles_page.canViewLibrary)) {
				fc.utobj().clickElement(driver, roles_page.canViewLibrary);
			}
			fc.utobj().clickElement(driver, roles_page.submit);
			AdminUsersRolesAddRoleAddUsersforRolePageTest p2 = new AdminUsersRolesAddRoleAddUsersforRolePageTest();
			p2.assignLater(driver);

			fc.utobj().printTestStep("Verify The Added Role");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, roleName);
			if (isTextPresent == false) {
				fc.utobj().throwsException("Added Corporate Role is not present at Admin > Users > Roles Page");
			}
			
			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users Page");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUserPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			
			String emailId = "hubautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser.setRole(roleName);
			corporateUserPage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			
			fc.home_page().logout(driver);
			
			fc.utobj().printTestStep("Login with newly added corporate user");
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());
			
			fc.utobj().printTestStep("Go To Admin > The hub > Library");
			fc.hub().hub_common().theHubLibrarySubModule(driver);
			
			clickOverMoreLink(driver, folderName);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,".//*[@id='folderDiv']//*[contains(text() , '" + folderName + "')]"));
			fc.utobj().clickLink(driver, folderName2);
			
			if(fc.utobj().isElementPresent(driver, pobj.moreLink)){
				fc.utobj().throwsException("More Link is visible");
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Verify The Report Document At Admin > The Hub > Library >  Folder [Detail View]", testCaseId = "TC_94_Report_Document")
	private void reportDocumentActionImg04() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			AdminTheHubLibraryPage pobj = new AdminTheHubLibraryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			addFolder(driver, library);

			fc.utobj().printTestStep("Add Document In Folder");
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String currentDate = fc.utobj().getCurrentDateUSFormat();
			String afterDate = dataSet.get("afterDate");
			int val = Integer.parseInt(afterDate);
			String futureDate = fc.utobj().getFutureDateUSFormat(val);
			addFolderDocument(driver, folderName, documentTitle, briefSummary, dataSet, currentDate, futureDate);

			fc.utobj().clickElement(driver, pobj.detailView);
			fc.utobj().clickLink(driver, documentTitle);
			if (!fc.utobj()
					.getElementByXpath(driver, ".//*[@id='DtlsInner']//a[contains(text () ,'" + documentTitle + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document Title At Detail View Page");
			}
			if (!fc.utobj()
					.getElementByXpath(driver, ".//*[@id='DtlsInner']//*[contains(text () ,'" + briefSummary + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document's Brief Summary");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//*[@id='DtlsInner']//*[contains(text () ,'" + documentTitle
					+ "')]/..//following-sibling::td[.='" + currentDate + "']").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Update Date");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//*[@id='DtlsInner']//*[contains(text () ,'" + documentTitle
					+ "')]/..//following-sibling::td[.='FranConnect Administrator']").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Parent Directory");
			}

			fc.utobj().printTestStep("Verify The Report In Document");
			actionImgIconDetailsView(driver, documentTitle, "Report");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().isTextDisplayed(driver, documentTitle, "Was not able to verify document Title");

			if (!fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , 'Brief Summary :')]/following-sibling::td[.='" + briefSummary + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Brief Summary :");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Uploaded By :')]/following-sibling::td[.='FranConnect Administrator']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Uploaded By :");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			// To verify Report option from document Comment link from Folder >
			// List View
			fc.utobj().printTestStep("Verify Report option from document Action Menu at Index > List View");

			fc.utobj().clickElement(driver, pobj.listView);
			actionImgIconListView(driver, documentTitle, "Report");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().isTextDisplayed(driver, documentTitle, "Was not able to verify document Title");

			if (!fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , 'Brief Summary :')]/following-sibling::td[.='" + briefSummary + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Brief Summary :");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Uploaded By :')]/following-sibling::td[.='FranConnect Administrator']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Uploaded By :");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Uploaded On :')]/following-sibling::td[.='" + currentDate + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Uploaded Date :");
			}

			if (!fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , 'Start Display Date :')]/following-sibling::td[.='" + currentDate + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Start Display Date :");
			}

			if (!fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , 'Expiration Date :')]/following-sibling::td[.='" + futureDate + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Start Display Date :");
			}

			fc.utobj().printTestStep("Verify the document download details");
			boolean isDocDownloadDetailsPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text(),'User Name')]/parent::*/following-sibling::tr/td[contains(text(),'FranConnect Administrator')]/preceding-sibling::td[contains(text(),'1')]");
			if (!isDocDownloadDetailsPresent) {
				fc.utobj().throwsException("Document Details is not present");
			}

			boolean isPrintButtonPresent = fc.utobj().isElementPresent(driver, pobj.printButton);
			if (!isPrintButtonPresent) {
				fc.utobj().throwsException("Print button is not present at Report cbox");
			}
			boolean isExportAsExcelButtonPresent = fc.utobj().isElementPresent(driver, pobj.exportAsExcelButton);
			if (!isExportAsExcelButtonPresent) {
				fc.utobj().throwsException("Export As Excel Button is not present at Report cbox");
			}
			boolean isCloseButtonPresent = fc.utobj().isElementPresent(driver, pobj.closeBtn);
			if (!isCloseButtonPresent) {
				fc.utobj().throwsException("Close Button is not present at Report cbox");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library","hub_download","hubf1121"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Verify The Modify Document At Admin > The Hub > Library >  Folder [List View]", testCaseId = "TC_95_Modify_Document")
	private void modifyDocumentActionImg04() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			AdminTheHubLibraryPage pobj = new AdminTheHubLibraryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			addFolder(driver, library);

			fc.utobj().printTestStep("Add Document In Folder");
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String tempDocTitle = documentTitle;

			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String currentDate = fc.utobj().getCurrentDateUSFormat();
			String afterDate = dataSet.get("afterDate");
			int val = Integer.parseInt(afterDate);
			String futureDate = fc.utobj().getFutureDateUSFormat(val);
			addFolderDocument(driver, folderName, documentTitle, briefSummary, dataSet, currentDate, futureDate);

			fc.utobj().printTestStep("Modify The Document");
			fc.utobj().clickElement(driver, pobj.listView);
			new HubCommonMethods().sortByRecentlyUploadedDocuments(driver);
			actionImgIconListView(driver, documentTitle, "Modify");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			fc.utobj().sendKeys(driver, pobj.documentTitle, documentTitle);
			briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			fc.utobj().sendKeys(driver, pobj.briefSummary, briefSummary);

			String currentDate1 = fc.utobj().getCurrentDateUSFormat();
			String futureDate1 = fc.utobj().getFutureDateUSFormat(val);
			fc.utobj().sendKeys(driver, pobj.displayDate, currentDate1);
			fc.utobj().sendKeys(driver, pobj.expirationDate, futureDate1);

			if (!fc.utobj().isSelected(driver, pobj.documentType.get(0))) {
				fc.utobj().clickElement(driver, pobj.documentType.get(0));
			}
			fc.utobj().selectDropDown(driver, pobj.documentSubType, dataSet.get("documentSubType"));
			String secondFileUpload="taskFile_03.pdf";
			String file = fc.utobj().getFilePathFromTestData(secondFileUpload);
			fc.utobj().moveToElement(driver, pobj.fileClick);
			pobj.fileClick.sendKeys(file);

			if (!fc.utobj().isSelected(driver, pobj.thumbFileUploaded.get(1))) {
				fc.utobj().clickElement(driver, pobj.thumbFileUploaded.get(1));
			}
			if (!fc.utobj().isSelected(driver, pobj.accessibilityPublic)) {
				fc.utobj().clickElement(driver, pobj.accessibilityPublic);
			}
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Again Modify The Document and uplaod xls file");
			fc.utobj().clickElement(driver, pobj.listView);
			new HubCommonMethods().sortByRecentlyUploadedDocuments(driver);
			actionImgIconListView(driver, documentTitle, "Modify");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			if (!fc.utobj().isSelected(driver, pobj.documentType.get(0))) {
				fc.utobj().clickElement(driver, pobj.documentType.get(0));
			}
			fc.utobj().selectDropDown(driver, pobj.documentSubType, dataSet.get("documentSubType"));
			String xlsFileUpload = "sample_xls_02.xlsx";
			String xlsFile = fc.utobj().getFilePathFromTestData(xlsFileUpload);
			fc.utobj().moveToElement(driver, pobj.fileClick);
			pobj.fileClick.sendKeys(xlsFile);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			library.setDocumentTitle(documentTitle);
			new HubCommonMethods().searchAdminHubLibraryDocuments(driver, library);
			fc.utobj().clickElement(driver, pobj.listView);

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//table[@class='selectBdr']//a[contains(text () ,'" + documentTitle + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document Title At Detail View Page");
			}
			if (!fc.utobj()
					.getElementByXpath(driver,
							".//table[@class='selectBdr']//*[contains(text () ,'" + briefSummary + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document's Brief Summary");
			}

			fc.utobj().printTestStep("Verify The Modify Document");
			actionImgIconListView(driver, documentTitle, "Report");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().isTextDisplayed(driver, documentTitle, "Was not able to verify document Title");

			if (!fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , 'Brief Summary :')]/following-sibling::td[.='" + briefSummary + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Brief Summary :");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Uploaded By :')]/following-sibling::td[.='FranConnect Administrator']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Uploaded By :");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify version history details");
			actionImgIconListView(driver, documentTitle, "Version History");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			boolean isDocTitleVerisonHistory01 = fc.utobj().assertPageSource(driver, documentTitle);
			if (!isDocTitleVerisonHistory01) {
				fc.utobj().throwsException("Document is not present at version history cbox iframe");
			}

			boolean isDateLinkPresent01 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[@id='versionHistorytable']//td[contains(text(),'FranConnect Administrator')]/preceding-sibling::td/a[contains(text(),'_01')]");
			if (!isDateLinkPresent01) {
				fc.utobj().throwsException("version history with dates link is not present");
			}

			fc.utobj().clickElement(driver,
					"//*[@id='versionHistorytable']//td[contains(text(),'FranConnect Administrator')]/preceding-sibling::td/a[contains(text(),'_01')]");
			fc.utobj().printTestStep("Verify Document should be downloaded successfully from list view");
			boolean isFileFound01 = fc.utobj().isFileFound(new File(FranconnectUtil.config.get("downloadFilepath")),
					library.getFile());
			if (!isFileFound01) {
				fc.utobj().throwsException("Not able to download the file");
			}
			hub_common_method.deleteDownloadedFile(library);

			fc.utobj().clickElement(driver,
					"//*[@id='versionHistorytable']//td[contains(text(),'FranConnect Administrator')]/preceding-sibling::td/a[contains(text(),'_02')]");
			fc.utobj().printTestStep("Verify Document should be downloaded successfully from list view");
			boolean isFileFound03 = fc.utobj().isFileFound(new File(FranconnectUtil.config.get("downloadFilepath")),
					xlsFileUpload);
			if (!isFileFound03) {
				fc.utobj().throwsException("Not able to download xlsx file from Version History");
			}
			hub_common_method.deleteDownloadedFile(library);

			fc.commonMethods().Click_Close_Input_ByValue(driver);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj()
					.printTestStep("Verify previous documents should be moved to Archived section after modification");
			fc.utobj().clickElement(driver, pobj.archivedDocumentsTab);

			library.setDocumentTitle(tempDocTitle);
			String commentText = fc.utobj().generateTestData("Comment Text");
			library.setCommentText(commentText);
			hub_common_method.postCommentAtListView(driver, pobj, library);

			if (!fc.utobj().assertPageSource(driver, library.getCommentText())) {
				fc.utobj().throwsException("Not able to verify the post comment at archived Tab at list view");
			}

			//Delete Posted Comment at Archived List View : TC-63732
			fc.utobj().clickElement(driver, pobj.deleteComment);
			fc.utobj().acceptAlertBox(driver);
			
			boolean isCommentPresent=fc.utobj().assertNotInPageSource(driver, library.getCommentText());
			if (isCommentPresent) {
				fc.utobj().throwsException("Not able to delete the Archived document's comment text at list view");
			}
			
			fc.utobj().clickElement(driver, pobj.listView);
			new HubCommonMethods().sortByRecentlyUploadedDocuments(driver);
			boolean isDocumentTitlePresent = fc.utobj().assertPageSource(driver, tempDocTitle);
			if (!isDocumentTitlePresent) {
				fc.utobj().throwsException("Document is not present at archived documents tab");
			}

			fc.utobj().printTestStep("Verify version history details");
			actionImgIconListView(driver, tempDocTitle, "Version History");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			boolean isDocTitleVerisonHistory = fc.utobj().assertPageSource(driver, tempDocTitle);
			if (!isDocTitleVerisonHistory) {
				fc.utobj().throwsException("Document is not present at version history cbox iframe");
			}

			boolean isDateLinkPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[@id='versionHistorytable']//td[contains(text(),'FranConnect Administrator')]/preceding-sibling::td/a[contains(text(),'_01')]");
			if (!isDateLinkPresent) {
				fc.utobj().throwsException("version history with dates link is not present");
			}

			fc.utobj().clickElement(driver,
					".//*[@id='versionHistorytable']//td[contains(text(),'FranConnect Administrator')]/preceding-sibling::td/a[contains(text(),'_01')]");
			fc.utobj().printTestStep("Verify Document should be downloaded successfully from list view");
			boolean isFileFound = fc.utobj().isFileFound(new File(FranconnectUtil.config.get("downloadFilepath")),
					secondFileUpload);
			if (!isFileFound) {
				fc.utobj().throwsException("Not able to download the file");
			}
			hub_common_method.deleteDownloadedFile(library);

			fc.utobj().printTestStep(
					"Verify The uploaded file should be downloaded from Archived Tab version history");

			fc.utobj().clickElement(driver,
					".//*[@id='versionHistorytable']//td[contains(text(),'FranConnect Administrator')]/preceding-sibling::td/a[contains(text(),'_02')]");
			fc.utobj().printTestStep("Verify Document should be downloaded successfully from list view");
			boolean isFileFound02 = fc.utobj().isFileFound(new File(FranconnectUtil.config.get("downloadFilepath")),
					secondFileUpload);
			if (!isFileFound02) {
				fc.utobj().throwsException("Not able to download file from Archived Tab version history");
			}
			hub_common_method.deleteDownloadedFile(library);

			fc.commonMethods().Click_Close_Input_ByValue(driver);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library", "thehudev","hubf1121" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Verify The Delete Document By At Admin > The Hub > Library >  Folder [Detail View]", testCaseId = "TC_96_Delete_Document")
	private void deleteDocumentActionImg04() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			AdminTheHubLibraryPage pobj = new AdminTheHubLibraryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "Franchise";
			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			addFolder(driver, library);

			fc.utobj().printTestStep("Add Document In Folder");
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String currentDate = fc.utobj().getCurrentDateUSFormat();
			String afterDate = dataSet.get("afterDate");
			int val = Integer.parseInt(afterDate);
			String futureDate = fc.utobj().getFutureDateUSFormat(val);
			addFolderDocument(driver, folderName, documentTitle, briefSummary, dataSet, currentDate, futureDate);

			fc.utobj().printTestStep("Delete Document");
			fc.utobj().clickElement(driver, pobj.detailView);

			hub_common_method.sortDocumentsByFilter(driver, library);

			actionImgIconDetailsView(driver, documentTitle, "Delete");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Delete Document");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, documentTitle);
			if (isTextPresent == true) {
				fc.utobj().throwsException("was not able to delete document");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library", "thehudev" })
	@TestCase(createdOn = "2017-06-22", updatedOn = "2017-06-22", testCaseDescription = "Verify The Delete Document By At Admin > The Hub > Library >  Folder [List View]", testCaseId = "TC_Admin_The_Hub_Folder_List_View_Delete_Document_01")
	private void deleteDocumentActionImgListView04() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);
			AdminTheHubLibraryPage pobj = new AdminTheHubLibraryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "Franchise";
			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			addFolder(driver, library);

			fc.utobj().printTestStep("Add Document In Folder");
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String currentDate = fc.utobj().getCurrentDateUSFormat();
			String afterDate = dataSet.get("afterDate");
			int val = Integer.parseInt(afterDate);
			String futureDate = fc.utobj().getFutureDateUSFormat(val);
			addFolderDocument(driver, folderName, documentTitle, briefSummary, dataSet, currentDate, futureDate);

			fc.utobj().printTestStep("Delete Document");
			fc.utobj().clickElement(driver, pobj.listView);
			hub_common_method.sortDocumentsByFilter(driver, library);

			actionImgIconListView(driver, documentTitle, "Delete");
			fc.utobj().acceptAlertBox(driver);
			fc.utobj().printTestStep("Verify The Delete Document");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, documentTitle);
			if (isTextPresent == true) {
				fc.utobj().throwsException("was not able to delete document");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library", "hubdev0709" ,"thehubdevTest"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Verify The Document Properties At Admin > The Hub > Library >  Folder [Detail View]", testCaseId = "TC_97_Document_Properties")
	private void documentPropertiesActionImg04() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			AdminTheHubLibraryPage pobj = new AdminTheHubLibraryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			addFolder(driver, library);

			fc.utobj().printTestStep("Add Document In Folder");
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String currentDate = fc.utobj().getCurrentDateUSFormat();
			String afterDate = dataSet.get("afterDate");
			int val = Integer.parseInt(afterDate);
			String futureDate = fc.utobj().getFutureDateUSFormat(val);
			addFolderDocument(driver, folderName, documentTitle, briefSummary, dataSet, currentDate, futureDate);

			fc.utobj().printTestStep("Verify The Document Properties");
			fc.utobj().clickElement(driver, pobj.detailView);
			new HubCommonMethods().sortByRecentlyUploadedDocuments(driver);
			actionImgIconDetailsView(driver, documentTitle, "Document Properties");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			if (!fc.utobj().isElementPresent(driver, fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , 'Document Title')]/following-sibling::td[.='" + documentTitle + "']"))) {
				fc.utobj().throwsException("was not able to verify Document Title");
			}

			if (!fc.utobj().isElementPresent(driver, fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , 'Summary')]/following-sibling::td[.='" + briefSummary + "']"))) {
				fc.utobj().throwsException("was not able to verify Document's Brief Summary");
			}

			if (!fc.utobj().isElementPresent(driver,
					fc.utobj().getElementByXpath(driver,
							".//td[contains(text () , 'Document URL')]/following-sibling::td/a[contains(@href,'"
									+ FranconnectUtil.config.get("buildUrl") + "')]"))) {
				fc.utobj().throwsException("Document URL is not present");

			}

			if (!fc.utobj().isElementPresent(driver,
					fc.utobj().getElementByXpath(driver,
							".//td[contains(text () , 'Active From')]/following-sibling::td[contains(text(),'"
									+ currentDate + "')]"))) {
				fc.utobj().throwsException("Acttive From Label is not present");

			}

			if (!fc.utobj().isElementPresent(driver,
					fc.utobj().getElementByXpath(driver,
							".//td[contains(text () , 'Creation Date')]/following-sibling::td[contains(text(),'"
									+ currentDate + "')]"))) {
				fc.utobj().throwsException("Acttive From Label is not present");

			}

			if (!fc.utobj().isElementPresent(driver,
					fc.utobj().getElementByXpath(driver,
							".//td[contains(text () , 'Last Modified')]/following-sibling::td[contains(text(),'"
									+ currentDate + "')]"))) {
				fc.utobj().throwsException("Acttive From Label is not present");

			}

			if (!fc.utobj().isElementPresent(driver, fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , 'Document Size ')]/following-sibling::td[contains(text(),'KB')]"))) {
				fc.utobj().throwsException("Acttive From Label is not present");

			}
			if (!fc.utobj().isElementPresent(driver,
					fc.utobj().getElementByXpath(driver,
							".//td[contains(text () , 'Document Type')]/following-sibling::td[contains(text () , '"
									+ dataSet.get("documentType") + "')]"))) {
				fc.utobj().throwsException("was not able to verify Document Type");
			}
			if (!fc.utobj().isElementPresent(driver,
					fc.utobj().getElementByXpath(driver,
							".//td[contains(text () , 'Document Subtype')]/following-sibling::td[contains(text () , '"
									+ dataSet.get("documentSubType") + "')]"))) {
				fc.utobj().throwsException("was not able to verify DocumentSub Type");
			}

			if (!fc.utobj().isElementPresent(driver, fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , 'Uploaded By')]/following-sibling::td[contains(text () , 'FranConnect Administrator')]"))) {
				fc.utobj().throwsException("was not able to verify Uploaded By");
			}

			if (!fc.utobj().isElementPresent(driver,
					fc.utobj().getElementByXpath(driver,
							".//td[contains(text () , 'Recommended')]/following-sibling::td[contains(text () , '"
									+ dataSet.get("recommendedDoc") + "')]"))) {
				fc.utobj().throwsException("was not able to verify Recommended");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library","hubf1121"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Verify The Post Comment At Admin > The Hub > Library >  Folder", testCaseId = "TC_98_Post_Comment")
	private void postCommentActionImg04() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			AdminTheHubLibraryPage pobj = new AdminTheHubLibraryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";
			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			addFolder(driver, library);

			fc.utobj().printTestStep("Add Document In Folder");
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String currentDate = fc.utobj().getCurrentDateUSFormat();
			String afterDate = dataSet.get("afterDate");
			int val = Integer.parseInt(afterDate);
			String futureDate = fc.utobj().getFutureDateUSFormat(val);
			addFolderDocument(driver, folderName, documentTitle, briefSummary, dataSet, currentDate, futureDate);

			fc.utobj().clickElement(driver, pobj.detailView);
			new HubCommonMethods().sortByRecentlyUploadedDocuments(driver);

			fc.utobj().printTestStep("Post Comment");
			postCommentActionImg(driver, documentTitle, "Post Comment");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().isTextDisplayed(driver, documentTitle, "was not able to verify Document Title");
			fc.utobj().isTextDisplayed(driver, briefSummary, "was not able to verify Brief Summary");

			fc.utobj().clickElement(driver, pobj.comment);
			String commentText = fc.utobj().generateTestData(dataSet.get("commentText"));
			WebElement element12 = fc.utobj().getElementByXpath(driver, ".//*[contains(@id , 'ta')]");
			fc.utobj().sendKeys(driver, element12, commentText);
			fc.utobj().clickElement(driver, pobj.post);

			fc.utobj().printTestStep("Verify The Post Comment");
			fc.utobj().isTextDisplayed(driver, commentText, "was not able to verify Comment Text");
			fc.commonMethods().Click_Close_Input_ByValue(driver);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify Post Comment option from document Comment link from Folder > List View");
			String commentTextList = fc.utobj().generateTestData("Test Comment Text");
			library.setCommentText(commentTextList);
			library.setDocumentTitle(documentTitle);
			hub_common_method.postCommentAtListView(driver, pobj, library);

			if (!fc.utobj().assertPageSource(driver, library.getCommentText())) {
				fc.utobj().throwsException("Not able to verify comment text at folder list view");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library", "hub06051"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Verify The Delete Comment At Admin > The Hub > Library >  Folder [Detail View]", testCaseId = "TC_99_Delete_Comment")
	private void deleteComment05() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			AdminTheHubLibraryPage pobj = new AdminTheHubLibraryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			addFolder(driver, library);

			fc.utobj().printTestStep("Add Document In Folder");
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String currentDate = fc.utobj().getCurrentDateUSFormat();
			String afterDate = dataSet.get("afterDate");
			int val = Integer.parseInt(afterDate);
			String futureDate = fc.utobj().getFutureDateUSFormat(val);
			addFolderDocument(driver, folderName, documentTitle, briefSummary, dataSet, currentDate, futureDate);

			//To verify Delete Comment option from document Comment link from Folder > List View
			fc.utobj().printTestStep("verify Delete Comment option from document Comment link from Folder > List View");
			fc.utobj().clickElement(driver, pobj.listView);
			new HubCommonMethods().sortByRecentlyUploadedDocuments(driver);
			fc.utobj().printTestStep("Post Comment");
			String commentTextList = fc.utobj().generateTestData("Test Comment Text");
			library.setCommentText(commentTextList);
			library.setDocumentTitle(documentTitle);
			hub_common_method.postCommentAtListView(driver, pobj, library);
			boolean isCommentTextPresent = fc.utobj().assertPageSource(driver, library.getCommentText());
			if (!isCommentTextPresent) {
				fc.utobj().throwsException("Not able to verify added Comment at List View");
			}
			
			fc.utobj().printTestStep("Delete Comment from List View");
			fc.utobj().clickElement(driver, pobj.deleteComment);
			fc.utobj().acceptAlertBox(driver);
			if(fc.utobj().assertNotInPageSource(driver, library.getCommentText())){
				fc.utobj().throwsException("Not able to delete posted comment from list view");
			}
			
			fc.utobj().printTestStep("Post Comment at details View");
			fc.utobj().clickElement(driver, pobj.detailView);
			new HubCommonMethods().sortByRecentlyUploadedDocuments(driver);

			fc.utobj().printTestStep("Post Comment");
			postCommentActionImg(driver, documentTitle, "Post Comment");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().isTextDisplayed(driver, documentTitle, "was not able to verify Document Title");
			fc.utobj().isTextDisplayed(driver, briefSummary, "was not able to verify Brief Summary");

			fc.utobj().printTestStep("Delete Comment");
			fc.utobj().clickElement(driver, pobj.comment);
			String commentText = fc.utobj().generateTestData(dataSet.get("commentText"));
			WebElement element12 = fc.utobj().getElementByXpath(driver, ".//*[contains(@id , 'ta')]");
			fc.utobj().sendKeys(driver, element12, commentText);
			fc.utobj().clickElement(driver, pobj.post);
			fc.utobj().clickElement(driver, pobj.deleteComment);
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Delete Comment");
			boolean isTextPresent = fc.utobj().assertNotInPageSource(driver, commentText);
			if (isTextPresent) {
				fc.utobj().throwsException("was not able to delete Comment");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library", "hub0605","hubf1121"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Verify The Version History At Admin > The Hub > Library >  Folder [Detail View]", testCaseId = "TC_100_Version_History")
	private void versionHistory05() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			AdminTheHubLibraryPage pobj = new AdminTheHubLibraryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			addFolder(driver, library);

			fc.utobj().printTestStep("Add Document In Folder");
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String currentDate = fc.utobj().getCurrentDateUSFormat();
			String afterDate = dataSet.get("afterDate");
			int val = Integer.parseInt(afterDate);
			String futureDate = fc.utobj().getFutureDateUSFormat(val);
			addFolderDocument(driver, folderName, documentTitle, briefSummary, dataSet, currentDate, futureDate);

			fc.utobj().printTestStep("Verify The Version History");
			fc.utobj().clickElementByJS(driver, fc.utobj().getElement(driver, pobj.detailView));
			new HubCommonMethods().sortByRecentlyUploadedDocuments(driver);
			actionImgIconDetailsView(driver, documentTitle, "Version History");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Document Title')]/following-sibling::td[contains(text () , '"
									+ documentTitle + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document Title");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Brief Summary')]/following-sibling::td[contains(text () , '"
									+ briefSummary + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document's Brief Summary");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library" ,"hubfailed0927","hubf1121"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Verify The Delete Document At Admin > The Hub > Library >  Folder [Detail View]", testCaseId = "TC_101_Delete_Document")
	private void deleteDocumentBtn03() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			AdminTheHubLibraryPage pobj = new AdminTheHubLibraryPage(driver);
			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			addFolder(driver, library);

			fc.utobj().printTestStep("Add Document In Folder");
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String currentDate = fc.utobj().getCurrentDateUSFormat();
			String afterDate = dataSet.get("afterDate");
			int val = Integer.parseInt(afterDate);
			String futureDate = fc.utobj().getFutureDateUSFormat(val);
			addFolderDocument(driver, folderName, documentTitle, briefSummary, dataSet, currentDate, futureDate);

			fc.utobj().clickElement(driver, pobj.detailView);
			new HubCommonMethods().sortByRecentlyUploadedDocuments(driver);

			fc.utobj().printTestStep("Delete Document");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='librarybody']//span[.='Delete']"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[.='" + documentTitle + "']/ancestor::tr/td/input"));
			fc.utobj().clickElement(driver, pobj.deleteBtn);
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Delete Document");
			library.setDocumentTitle(documentTitle);
			hub_common_method.searchAdminHubLibraryDocuments(driver, library);
			fc.utobj().clickElement(driver, pobj.detailView);
			new HubCommonMethods().sortByRecentlyUploadedDocuments(driver);
			boolean isTextPresent = /*fc.utobj().assertPageSource(driver, documentTitle);*/fc.utobj().isElementPresent(driver, fc.utobj().getElementByXpath(driver, ".//td[contains(text(),'No documents found')]"));
			if (!isTextPresent) {
				fc.utobj().throwsException("was not able to delete document");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library" ,"hubf1121"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Verify The Report Document At Admin > The Hub > Library >  Folder [Thumbnail View]", testCaseId = "TC_102_Report_Document")
	private void reportDocument05() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			AdminTheHubLibraryPage pobj = new AdminTheHubLibraryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			addFolder(driver, library);

			fc.utobj().printTestStep("Add Document In Folder");
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String currentDate = fc.utobj().getCurrentDateUSFormat();
			String afterDate = dataSet.get("afterDate");
			int val = Integer.parseInt(afterDate);
			String futureDate = fc.utobj().getFutureDateUSFormat(val);
			addFolderDocument(driver, folderName, documentTitle, briefSummary, dataSet, currentDate, futureDate);
			fc.utobj().clickElement(driver, pobj.thumbnailView);

			hub_common_method.sortDocumentsByFilter(driver, library);

			if (!fc.utobj().getElementByXpath(driver, ".//*[@id='thums']//a[.='" + documentTitle + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document Title At Thumbnail View Page");
			}

			fc.utobj().printTestStep("Verify The Report Document");
			thumbnailOption(driver, documentTitle, "Report");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().isTextDisplayed(driver, documentTitle, "Was not able to verify document Title");

			if (!fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , 'Brief Summary :')]/following-sibling::td[.='" + briefSummary + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Brief Summary :");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Uploaded By :')]/following-sibling::td[.='FranConnect Administrator']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Uploaded By :");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Uploaded On :')]/following-sibling::td[.='" + currentDate + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Uploaded Date :");
			}

			if (!fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , 'Start Display Date :')]/following-sibling::td[.='" + currentDate + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Start Display Date :");
			}

			if (!fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , 'Expiration Date :')]/following-sibling::td[.='" + futureDate + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Start Display Date :");
			}
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Verify The Document Properties At Admin > The Hub > Library >  Folder [Thumbnail View]", testCaseId = "TC_103_Document_Properties")
	private void documentProperties05() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			AdminTheHubLibraryPage pobj = new AdminTheHubLibraryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			addFolder(driver, library);

			fc.utobj().printTestStep("Add Document In Folder");
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String currentDate = fc.utobj().getCurrentDateUSFormat();
			String afterDate = dataSet.get("afterDate");
			int val = Integer.parseInt(afterDate);
			String futureDate = fc.utobj().getFutureDateUSFormat(val);
			addFolderDocument(driver, folderName, documentTitle, briefSummary, dataSet, currentDate, futureDate);
			fc.utobj().clickElement(driver, pobj.thumbnailView);

			fc.utobj().printTestStep("Verify The Document Properties");
			thumbnailOption(driver, documentTitle, "Document Properties");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			if (!fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , 'Document Title')]/following-sibling::td[.='" + documentTitle + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document Title");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Summary')]/following-sibling::td[.='" + briefSummary + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document's Brief Summary");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Document Type')]/following-sibling::td[contains(text () , '"
									+ dataSet.get("documentType") + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document Type");
			}
			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Document Subtype')]/following-sibling::td[contains(text () , '"
									+ dataSet.get("documentSubType") + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify DocumentSub Type");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Uploaded By')]/following-sibling::td[contains(text () , 'FranConnect Administrator')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify DocumentSub Type");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Recommended')]/following-sibling::td[contains(text () , '"
									+ dataSet.get("recommendedDoc") + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Recommended");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library","hubf1121"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Verify The Post Comment At Admin > The Hub > Library >  Folder [Thumbnail View]", testCaseId = "TC_104_Post_Comment")
	private void postComment05() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			AdminTheHubLibraryPage pobj = new AdminTheHubLibraryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			addFolder(driver, library);

			fc.utobj().printTestStep("Add Document In Folder");
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String currentDate = fc.utobj().getCurrentDateUSFormat();
			String afterDate = dataSet.get("afterDate");
			int val = Integer.parseInt(afterDate);
			String futureDate = fc.utobj().getFutureDateUSFormat(val);
			addFolderDocument(driver, folderName, documentTitle, briefSummary, dataSet, currentDate, futureDate);
			fc.utobj().clickElement(driver, pobj.thumbnailView);
			thumbnailOption(driver, documentTitle, "Post Comment");
			fc.utobj().printTestStep("Post Comment");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().isTextDisplayed(driver, documentTitle, "was not able to verify Document Title");
			fc.utobj().isTextDisplayed(driver, briefSummary, "was not able to verify Brief Summary");

			fc.utobj().clickElement(driver, pobj.comment);
			String commentText = fc.utobj().generateTestData(dataSet.get("commentText"));
			WebElement element12 = fc.utobj().getElementByXpath(driver, ".//*[contains(@id , 'ta')]");
			fc.utobj().sendKeys(driver, element12, commentText);
			fc.utobj().clickElement(driver, pobj.post);

			fc.utobj().printTestStep("Verify The Post Comment");
			fc.utobj().isTextDisplayed(driver, commentText, "was not able to verify Comment Text");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Verify The Delete Comment At Admin > The Hub > Library >  Folder [Thumbnail View]", testCaseId = "TC_105_Delete_Comment")
	private void deleteComment06() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			AdminTheHubLibraryPage pobj = new AdminTheHubLibraryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			addFolder(driver, library);

			fc.utobj().printTestStep("Add Document In Folder");
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String currentDate = fc.utobj().getCurrentDateUSFormat();
			String afterDate = dataSet.get("afterDate");
			int val = Integer.parseInt(afterDate);
			String futureDate = fc.utobj().getFutureDateUSFormat(val);
			addFolderDocument(driver, folderName, documentTitle, briefSummary, dataSet, currentDate, futureDate);
			fc.utobj().clickElement(driver, pobj.thumbnailView);
			thumbnailOption(driver, documentTitle, "Post Comment");
			fc.utobj().printTestStep("Post Comment");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().isTextDisplayed(driver, documentTitle, "was not able to verify Document Title");
			fc.utobj().isTextDisplayed(driver, briefSummary, "was not able to verify Brief Summary");

			fc.utobj().printTestStep("Delete Comment");
			fc.utobj().clickElement(driver, pobj.comment);
			String commentText = fc.utobj().generateTestData(dataSet.get("commentText"));
			WebElement element12 = fc.utobj().getElementByXpath(driver, ".//*[contains(@id , 'ta')]");
			fc.utobj().sendKeys(driver, element12, commentText);
			fc.utobj().clickElement(driver, pobj.post);
			fc.utobj().clickElement(driver, pobj.deleteComment);
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Deleted Comment");

			boolean isTextPresent = fc.utobj().assertNotInPageSource(driver, commentText);
			if (isTextPresent) {
				fc.utobj().throwsException("was not able to delete Comment");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library" ,"hubf1121"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Verify The Version History At Admin > The Hub > Library >  Folder [Thumbnail View]", testCaseId = "TC_106_Version_History")
	private void versionHistory06() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			AdminTheHubLibraryPage pobj = new AdminTheHubLibraryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			addFolder(driver, library);

			fc.utobj().printTestStep("Add Document In Folder");
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String currentDate = fc.utobj().getCurrentDateUSFormat();
			String afterDate = dataSet.get("afterDate");
			int val = Integer.parseInt(afterDate);
			String futureDate = fc.utobj().getFutureDateUSFormat(val);
			addFolderDocument(driver, folderName, documentTitle, briefSummary, dataSet, currentDate, futureDate);
			fc.utobj().clickElement(driver, pobj.thumbnailView);
			thumbnailOption(driver, documentTitle, "Version History");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().printTestStep("Verify The Version History");

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Document Title')]/following-sibling::td[contains(text () , '"
									+ documentTitle + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document Title");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Brief Summary')]/following-sibling::td[contains(text () , '"
									+ briefSummary + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document's Brief Summary");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library","hub_0806" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Verify The Delete Document At Admin > The Hub > Library >  Folder [Thumbnail View]", testCaseId = "TC_107_Delete_Document")
	private void deleteDocumentBtn04() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			AdminTheHubLibraryPage pobj = new AdminTheHubLibraryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			addFolder(driver, library);

			fc.utobj().printTestStep("Add Document In Folder");
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String currentDate = fc.utobj().getCurrentDateUSFormat();
			String afterDate = dataSet.get("afterDate");
			int val = Integer.parseInt(afterDate);
			String futureDate = fc.utobj().getFutureDateUSFormat(val);
			addFolderDocument(driver, folderName, documentTitle, briefSummary, dataSet, currentDate, futureDate);
			fc.utobj().clickElement(driver, pobj.thumbnailView);

			fc.utobj().printTestStep("Delete Document");
			WebElement element = fc.utobj().getElementByXpath(driver, ".//*[@id='thums']//*[.='" + documentTitle + "']"
					+ "//following::table[@class='topLine pt5'][1]//td/a[contains(@onclick, 'deletedocument')]");
			fc.utobj().clickElement(driver, element);
			fc.utobj().acceptAlertBox(driver);

			library.setDocumentTitle(documentTitle);
			new HubCommonMethods().searchAdminHubLibraryDocuments(driver, library);
			fc.utobj().clickElement(driver, pobj.thumbnailView);

			fc.utobj().printTestStep("Verify The Delete Document");
			boolean isTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//table[@id='thumDtls']//td[contains(text(),'No documents found')]");

			if (!isTextPresent) {
				fc.utobj().throwsException("was not able to delete Library Document");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Verify The Modify Document At Admin > The Hub > Library >  Folder [Thumbnail View]", testCaseId = "TC_108_Modify_Document")
	private void modifyDocumentBtn04() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			AdminTheHubLibraryPage pobj = new AdminTheHubLibraryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";
			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			addFolder(driver, library);

			fc.utobj().printTestStep("Add Document In Folder");
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String currentDate = fc.utobj().getCurrentDateUSFormat();
			String afterDate = dataSet.get("afterDate");
			int val = Integer.parseInt(afterDate);
			String futureDate = fc.utobj().getFutureDateUSFormat(val);
			addFolderDocument(driver, folderName, documentTitle, briefSummary, dataSet, currentDate, futureDate);

			fc.utobj().printTestStep("Modify The Document");
			fc.utobj().clickElement(driver, pobj.thumbnailView);

			WebElement element = fc.utobj().getElementByXpath(driver, ".//*[@id='thums']//*[.='" + documentTitle + "']"
					+ "//following::table[@class='topLine pt5'][1]//td/a/img[@title='Modify']");
			fc.utobj().clickElement(driver, element);

			fc.commonMethods().switch_cboxIframe_frameId(driver);

			documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			fc.utobj().sendKeys(driver, pobj.documentTitle, documentTitle);
			briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			fc.utobj().sendKeys(driver, pobj.briefSummary, briefSummary);

			String currentDate1 = fc.utobj().getCurrentDateUSFormat();
			String futureDate1 = fc.utobj().getFutureDateUSFormat(val);
			fc.utobj().sendKeys(driver, pobj.displayDate, currentDate1);
			fc.utobj().sendKeys(driver, pobj.expirationDate, futureDate1);

			if (!fc.utobj().isSelected(driver, pobj.documentType.get(0))) {
				fc.utobj().clickElement(driver, pobj.documentType.get(0));
			}

			fc.utobj().selectDropDown(driver, pobj.documentSubType, dataSet.get("documentSubType"));
			
			String file = fc.utobj().getFilePathFromTestData(dataSet.get("fileName"));
			
			fc.utobj().moveToElement(driver, pobj.fileClick);
			pobj.fileClick.sendKeys(file);

			if (!fc.utobj().isSelected(driver, pobj.thumbFileUploaded.get(1))) {
				fc.utobj().clickElement(driver, pobj.thumbFileUploaded.get(1));
			}

			if (!fc.utobj().isSelected(driver, pobj.accessibilityPublic)) {
				fc.utobj().clickElement(driver, pobj.accessibilityPublic);
			}

			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			clickOverMoreLink(driver, folderName);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='folderDiv']//*[contains(text(), '" + folderName + "')]"));
			fc.utobj().sleep();
			fc.utobj().clickElement(driver, pobj.thumbnailView);
			if (!fc.utobj().getElementByXpath(driver, ".//*[@id='thums']//a[.='" + documentTitle + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document Title At Thumbnail View Page");
			}

			fc.utobj().printTestStep("Verify The Modified The Document");
			thumbnailOption(driver, documentTitle, "Report");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().isTextDisplayed(driver, documentTitle, "Was not able to verify document Title");

			if (!fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , 'Brief Summary :')]/following-sibling::td[.='" + briefSummary + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Brief Summary :");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Uploaded By :')]/following-sibling::td[.='FranConnect Administrator']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Uploaded By :");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Uploaded On :')]/following-sibling::td[.='" + currentDate + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Uploaded Date :");
			}

			if (!fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , 'Start Display Date :')]/following-sibling::td[.='" + currentDate1 + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Start Display Date :");
			}

			if (!fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , 'Expiration Date :')]/following-sibling::td[.='" + futureDate1 + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Start Display Date :");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library" })
	@TestCase(createdOn = "2018-05-02", updatedOn = "2018-05-02", testCaseDescription = "Verify the Library Main Folders on home page", testCaseId = "TC_Admin_Hub_Library_More_Link_01")
	private void folderMoreLink() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			driver = fc.loginpage().login(driver);

			AdminTheHubLibraryPage pobj = new AdminTheHubLibraryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = null;

			boolean moreLinkPresent = false;

			fc.hub().hub_common().adminTheHubLibraryPage(driver);

			List<WebElement> folderList = fc.utobj().getElementListByXpath(driver, ".//*[@id='folderDiv']/ul/li/a");

			if (folderList.size() == 0) {
				moreLinkPresent = false;
			} else if (folderList.size() >= 25) {
				moreLinkPresent = true;
			}

			if (moreLinkPresent == false) {
				for (int i = 0; i <= 25 - folderList.size(); i++) {
					folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
					String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
					String summaryFormat = "Text";
					String accessTo = "All";

					Library library = new Library();
					library.setFolderName(folderName);
					library.setFolderSummary(folderSummary);
					library.setSummaryFormat(summaryFormat);
					library.setAccessTo(accessTo);
					addFolder(driver, library);
				}
				if (!fc.utobj().isElementPresent(driver, pobj.moreLink)) {
					fc.utobj().throwsException("More Link is present at Admin > Library");
				}

				fc.utobj().clickElement(driver, pobj.moreLink);
				boolean isFolderPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
						".//*[@id='folderDiv']//*[contains(text(),'" + folderName + "')]");

				if (!isFolderPresent) {
					fc.utobj().throwsException("After Clicking over More Link Folder is not present");
				}
			}
			if (moreLinkPresent) {
				if (!fc.utobj().isElementPresent(driver, pobj.moreLink)) {
					fc.utobj().throwsException("More Link is present at Admin > Library");
				}
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub" ,"hub_download"})
	@TestCase(createdOn = "2018-05-04", updatedOn = "2018-05-04", testCaseDescription = "Verify The Add Document At Admin > The Hub > Library >  Resource Library", testCaseId = "TC_Admin_The_Hub_Document_Version_History_01")
	private void versionHistory07() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminTheHubLibraryPage pobj = new AdminTheHubLibraryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";
			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			addFolder(driver, library);

			fc.utobj().printTestStep("Add Document");
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String currentDate = fc.utobj().getCurrentDateUSFormat();
			String futureDate = fc.utobj().getFutureDateUSFormat(2);
			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setCurrentDate(currentDate);
			library.setFutureDate(futureDate);
			library.setDocumentSubType(dataSet.get("documentSubType"));
			library.setFile(dataSet.get("fileName"));
			library.setDocumentType(dataSet.get("documentType"));
			library.setRecommendedDoc(false);
			addDocument(driver, library);
			new HubCommonMethods().searchAdminHubLibraryDocuments(driver, library);

			fc.utobj().printTestStep("Modify the uploaded document and now select HTML Radio and save it");
			actionImgIconListView(driver, documentTitle, "Modify");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.documentType.get(2));
			String htmlText = fc.utobj().generateTestData("Html Text");
			fc.utobj().switchFrameById(driver, "htmlSummary_ifr");
			Actions actions = new Actions(driver);
			actions.moveToElement(fc.utobj().getElement(driver, pobj.editorTextArea));
			actions.click();
			actions.sendKeys(htmlText);
			actions.build().perform();
			fc.utobj().switchFrameToDefault(driver);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Now again modify and upload word type file");
			actionImgIconListView(driver, documentTitle, "Modify");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.documentType.get(0));
			fc.utobj().selectDropDown(driver, pobj.documentSubType, "Word");
			fc.utobj().sendKeys(driver, pobj.fileClick, fc.utobj().getFilePathFromTestData("sample_word_01.doc"));
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Now again modify and upload ppt type file");
			actionImgIconListView(driver, documentTitle, "Modify");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.documentType.get(0));
			fc.utobj().selectDropDown(driver, pobj.documentSubType, "Power Point");
			fc.utobj().sendKeys(driver, pobj.fileClick, fc.utobj().getFilePathFromTestData("sample_ppt_01.pptx"));
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Now again modify and upload Excel type file");
			actionImgIconListView(driver, documentTitle, "Modify");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.documentType.get(0));
			fc.utobj().selectDropDown(driver, pobj.documentSubType, "Excel");
			fc.utobj().sendKeys(driver, pobj.fileClick, fc.utobj().getFilePathFromTestData("sample_xls_01.xlsx"));
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Navigate To Admin > Options > Search");
			fc.utobj().printTestStep("Search Document by Document Title");
			new SearchTest().searchLibraryDocument(driver, library);

			fc.utobj().printTestStep("View Version History of document");
			fc.utobj().actionImgOption(driver, library.getDocumentTitle(), "Version History");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().printTestStep(
					"Verify All the documents should be downloaded successfully for all version history");

			ArrayList<String> arrayList = new ArrayList<>();
			arrayList.add("sample_xls_01.xlsx");
			arrayList.add("sample_ppt_01.pptx");
			arrayList.add("sample_word_01.doc");
			arrayList.add("dummay");
			arrayList.add(dataSet.get("fileName"));

			List<WebElement> listElement = fc.utobj().getElementListByXpath(driver,
					".//*[@id='versionHistorytable']//table[@class='summaryTblex']//tr/td[1][not(contains(@class,'thead'))]/a");

			boolean b[] = new boolean[5];
			for (int i = 0; i < listElement.size(); i++) {
				try {
					if (i == 3) {
						fc.utobj().clickElement(driver, listElement.get(i));
						fc.commonMethods().switch_cboxIframe_frameId(driver);
						b[i] = fc.utobj().isElementPresent(fc.utobj().getElementListByXpath(driver,
								".//*[@id='documentTable']//p[contains(text(),'" + htmlText + "')]"));
						fc.utobj().clickElement(driver, new AdminTheHubLibraryPage(driver).backBtn);
						fc.utobj().switchFrameToDefault(driver);
						fc.commonMethods().switch_cboxIframe_frameId(driver);
						listElement = fc.utobj().getElementListByXpath(driver,
								".//*[@id='versionHistorytable']//table[@class='summaryTblex']//tr/td[1][not(contains(@class,'thead'))]/a");
					} else {
						fc.utobj().clickElement(driver, listElement.get(i));
						b[i] = fc.utobj().isFileFound(new File(FranconnectUtil.config.get("downloadFilepath")),
								arrayList.get(i));
					}
				} catch (Exception e) {
					b[i] = false;
				}
			}

			fc.utobj().clickElement(driver, new AdminTheHubLibraryPage(driver).closeBtn);

			if (!b[0]) {
				fc.utobj().throwsException("Not able to download xlsx type version document");
			}
			if (!b[1]) {
				fc.utobj().throwsException("Not able to download pptx type version document");
			}
			if (!b[2]) {
				fc.utobj().throwsException("Not able to download doc type version document");
			}
			if (!b[3]) {
				fc.utobj().throwsException("Not able to verify html type version document");
			}
			if (!b[4]) {
				fc.utobj().throwsException("Not able to download pdf type version document");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	
	@Test(groups = { "thehub", "hub_library","hubdev0726","hubf1121"})
	@TestCase(createdOn = "2018-07-26", updatedOn = "2018-07-26", testCaseDescription = "Verify Privileged based recommended documents are not appearing on Library home page", testCaseId = "TC_Privileged_Document_01")
	private void privilegedBasedRecomndedDocument() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users Page");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUserPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			
			String emailId = "hubautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corporateUserPage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			
			
			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData("Testfolder");
			String folderSummary = fc.utobj().generateTestData("Testfoldersummary");
			String summaryFormat = "Text";
			String accessTo = "Corporate";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			addFolder(driver, library);
			
			fc.utobj().printTestStep("Add Recommended Type Document in Privileged Folder");
			library.setDocumentTitle(fc.utobj().generateTestData("DocumentTitle"));
			library.setBriefSummary("Test Brief Summary");
			library.setCurrentDate(fc.utobj().getCurrentDateUSFormat());
			library.setFutureDate(fc.utobj().getFutureDateUSFormat(2));
			library.setDocumentSubType("PDF");
			library.setDocumentType("Web Link");
			library.setLinkUrl("https://www.msn.com");
			library.setRecommendedDoc(true);
			addDocument(driver, library);
			fc.home_page().logout(driver);
			
			fc.utobj().printTestStep("Login With Corporate user");
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());
			
			fc.utobj().printTestStep("Go To Hub > Home Page");
			fc.hub().hub_common().theHubHome(driver);
			
			fc.utobj().printTestStep("Verify The Document should be present in Recommended span");
			fc.utobj().clickElement(driver, new TheHubLibraryPage(driver).recommendedDocumentsAnchor);
			
			boolean isLnkPresent=fc.utobj().assertLinkPartialText(driver, library.getDocumentTitle());
			if (!isLnkPresent) {
				fc.utobj().throwsException("Recommended Document is not present");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	
	public void thumbnailOption(WebDriver driver, String documentTitle, String option) throws Exception {

		WebElement element = fc.utobj().getElementByXpath(driver, ".//*[@id='thums']//*[.='" + documentTitle + "']"
				+ "//following::table[@class='topLine pt5'][1]//td/a[contains(@onclick, 'showActionOver')]");

		String text = element.getAttribute("onclick").trim();
		text = text.replace("showActionOver(event,'", "");
		text = text.replace("')", "");

		try {
			fc.utobj().clickElementWithoutMove(driver, element);
		} catch (Exception e) {
			fc.utobj().clickElement(driver, element);
		}
		fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
				".//*[@id='actionListButtons" + text + "']//*[contains(text () , '" + option + "')]"));
	}

	public void actionImgIconListView(WebDriver driver, String documentTitle, String option) throws Exception {

		WebElement element = fc.utobj().getElementByXpath(driver, ".//*[contains(text () ,'" + documentTitle
				+ "')]/ancestor::table[@class='selectBdr']//tr/td//*[contains(@onclick, 'showActionOver')]");
		fc.utobj().moveToElement(driver, element);
		String text = element.getAttribute("onclick");
		text = text.replace("showActionOver(event,'", "");
		text = text.replace("')", "");
		try {
			fc.utobj().clickElement(driver, element);
		} catch (Exception e) {
			fc.utobj().clickElementWithoutMove(driver, element);
		}
		fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
				".//*[@id='actionListButtons" + text + "']//*[contains(text () , '" + option + "')]"));

	}

	public void actionImgIconDetailsView(WebDriver driver, String documentTitle, String option) throws Exception {

		WebElement element = fc.utobj().getElementByXpath(driver, ".//table[@id='DtlsInner']//*[contains(text(),'"
				+ documentTitle + "')]/ancestor::tr/td/following-sibling::td/a[contains(@onclick ,'showActionOver')]");
		fc.utobj().moveToElement(driver, element);
		String text = element.getAttribute("onclick");
		text = text.replace("showActionOver(event,'", "");
		text = text.replace("')", "");
		try {
			fc.utobj().clickElement(driver, element);
		} catch (Exception e) {
			fc.utobj().clickElementWithoutMove(driver, element);
		}
		fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
				".//*[@id='actionListButtons" + text + "']//*[contains(text () , '" + option + "')]"));

	}

	public void postCommentActionImg(WebDriver driver, String documentTittle, String option) throws Exception {

		WebElement element = fc.utobj().getElementByXpath(driver, ".//*[contains(text () ,'" + documentTittle
				+ "')]/ancestor::tr/td/a[contains(@onclick, 'showActionOver')]");
		fc.utobj().moveToElement(driver, element);
		String text = element.getAttribute("onclick");
		text = text.replace("showActionOver(event,'", "");
		text = text.replace("')", "");

		try {
			fc.utobj().clickElementWithoutMove(driver, element);
		} catch (Exception e) {
			fc.utobj().clickElement(driver, element);
		}
		fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
				".//*[@id='actionListButtons" + text + "']//*[contains(text () , '" + option + "')]"));
	}

	public void clickOverMoreLink(WebDriver driver, String folderName) throws Exception {

		boolean isMoreLinkPresent = false;

		try {
			List<WebElement> listElement = fc.utobj().getElementListByXpath(driver,
					".//*[@id='folderDiv']/ul/li/a/span");
			if (listElement.size() > 24) {
				isMoreLinkPresent = true;
			} else {
				isMoreLinkPresent = false;
			}
		} catch (Exception e) {
			isMoreLinkPresent = false;
		}
		if (isMoreLinkPresent) {
			for (int i = 0; i < 100; i++) {
				List<WebElement> list = null;
				try {
					list = fc.utobj().getElementListByXpath(driver, ".//*[@id='moreLink']/a");
				} catch (Exception e) {
				}
				if (list != null && list.size() > 0) {
					try {
						fc.utobj().clickElement(driver, list.get(0));
						driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
						boolean isFolderNamePresent = fc.utobj().isElementPresent(fc.utobj()
								.getElementListByXpath(driver, ".//span[contains(text(),'" + folderName + "')]"));
						if (isFolderNamePresent == true) {
							break;
						}
					} catch (Exception e) {
					} finally {
						driver.manage().timeouts().implicitlyWait(45, TimeUnit.SECONDS);
					}
				} else {
					break;
				}
			}
		}
	}

	public void clickOverMoreLink(WebDriver driver) throws Exception {

		boolean isMoreLinkPresent = false;

		try {
			List<WebElement> listElement = fc.utobj().getElementListByXpath(driver,
					".//*[@id='folderDiv']/ul/li/a/span");
			if (listElement.size() > 24) {
				isMoreLinkPresent = true;
			} else {
				isMoreLinkPresent = false;
			}
		} catch (Exception e) {
			isMoreLinkPresent = false;
		}
		if (isMoreLinkPresent) {
			for (int i = 0; i < 100; i++) {

				List<WebElement> list = null;
				try {
					list = fc.utobj().getElementListByXpath(driver, ".//*[@id='moreLink']/a");
				} catch (Exception e) {
				}
				if (list != null && list.size() > 0) {
					try {
						fc.utobj().clickElement(driver, list.get(0));
					} catch (Exception e) {
						break;
					}
				} else if (list.size() == 0) {
					break;
				}
			}
		}
	}
	
}
