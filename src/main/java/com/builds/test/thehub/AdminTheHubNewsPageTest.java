package com.builds.test.thehub;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminAreaRegionAddAreaRegionPageTest;
import com.builds.test.admin.AdminDivisionAddDivisionPageTest;
import com.builds.test.admin.AdminFranchiseLocationAddFranchiseLocationPageTest;
import com.builds.test.admin.AdminHiddenLinksConfigureNewHierarchyLevelPageTest;
import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.admin.AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest;
import com.builds.test.admin.AdminUsersManageManageFranchiseUsersPageTest;
import com.builds.test.admin.AdminUsersManageRegionalUsersAddRegionalUserPageTest;
import com.builds.test.admin.AdminUsersRolesAddNewRolePageTest;
import com.builds.test.admin.Roles;
import com.builds.test.common.CorporateUser;
import com.builds.test.common.SearchTest;
import com.builds.uimaps.fs.SearchUI;
import com.builds.uimaps.thehub.AdminTheHubNewsPage;
import com.builds.uimaps.thehub.TheHubNewsPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminTheHubNewsPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = {"thehub" ,"hub_news"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add Folder with accessibilty to All At Admin > The Hub > News > Home", testCaseId = "TC_18_Add_Folder_Access_All")
	private void addFolderAccessAll() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > News Page");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String accessTo="All";
			
			News news=new News();
			news.setFolderName(folderName);
			news.setFolderSummary(folderSummary);
			news.setAccessTo(accessTo);
			
			fc.utobj().printTestStep("Add Folder");
			addFolder(driver, news);

			fc.utobj().printTestStep("Verify The Add Folder");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, folderName);
			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to add Folder");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	void addFolder(WebDriver driver, News news) throws Exception {
		try {
			AdminTheHubNewsPage pobj = new AdminTheHubNewsPage(driver);
			fc.hub().hub_common().adminTheHubNewsPage(driver);
			HubCommonMethods hcom_method=new HubCommonMethods();
			
			fc.utobj().clickElement(driver, pobj.homeTab);
			fc.utobj().clickElement(driver, pobj.addFolderLink);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.folderName, news.getFolderName());
			fc.utobj().sendKeys(driver, pobj.folderSummary, news.getFolderSummary());

			if (news.getAccessTo().equalsIgnoreCase("All")) {

				if (!fc.utobj().isSelected(driver, pobj.accessibleToAll.get(0))) {
					fc.utobj().clickElement(driver, pobj.accessibleToAll.get(0));
				}

			} else if (news.getAccessTo().equalsIgnoreCase("Corporate")) {

				if (!fc.utobj().isSelected(driver, pobj.accessibleToAll.get(1))) {
					fc.utobj().clickElement(driver, pobj.accessibleToAll.get(1));
				}
				
				if (news.geRoleName()!=null) {
					hcom_method.selectRolesInNews(driver, news);
				}else if (news.geRoleName()==null) {
					fc.utobj().clickElement(driver,
							fc.utobj().getElementByXpath(driver, ".//input[@name ='chkRole' and @value='3']"));
				}

			} else if (news.getAccessTo().equalsIgnoreCase("Divisional")) {

				if (!fc.utobj().isSelected(driver, pobj.accessibleToAll.get(1))) {
					fc.utobj().clickElement(driver, pobj.accessibleToAll.get(1));
				}
				
				if (news.geRoleName()!=null) {
					hcom_method.selectRolesInNews(driver, news);
				}else if (news.geRoleName()==null) {
					fc.utobj().clickElement(driver,
							fc.utobj().getElementByXpath(driver, ".//input[@name ='chkRole' and @value='5']"));
				}

			} else if (news.getAccessTo().equalsIgnoreCase("Franchise")) {

				if (!fc.utobj().isSelected(driver, pobj.accessibleToAll.get(1))) {
					fc.utobj().clickElement(driver, pobj.accessibleToAll.get(1));
				}

				if (news.geRoleName()!=null) {
					hcom_method.selectRolesInNews(driver, news);
				}else if (news.geRoleName()==null) {
					fc.utobj().clickElement(driver,
							fc.utobj().getElementByXpath(driver, ".//input[@name ='chkRole' and @value='2']"));
				}

			} else if (news.getAccessTo().equalsIgnoreCase("Regional")) {

				if (!fc.utobj().isSelected(driver, pobj.accessibleToAll.get(1))) {
					fc.utobj().clickElement(driver, pobj.accessibleToAll.get(1));
				}

				if (news.geRoleName()!=null) {
					hcom_method.selectRolesInNews(driver, news);
				}else if (news.geRoleName()==null) {
					fc.utobj().clickElement(driver,
							fc.utobj().getElementByXpath(driver, ".//input[@name ='chkRole' and @value='4']"));
				}
			}
			
			if (news.getAddNewsItemFolder()) {
				if (!fc.utobj().isSelected(driver, pobj.addNewsItemFolder.get(0))) {
					fc.utobj().clickElement(driver, pobj.addNewsItemFolder.get(0));
				}
				
				fc.utobj().sendKeys(driver, pobj.newsItemTitle, news.getNewsItemTitle());
				fc.utobj().sendKeys(driver, pobj.briefSummary, news.getNewsBriefSummary());

				if (news.getFile()!=null) {
					fc.utobj().sendKeys(driver, pobj.newsItemFile, news.getFile());
				}
				
				fc.utobj().switchFrameById(driver, "completeNews_ifr");

				Actions actions = new Actions(driver);
				actions.moveToElement(fc.utobj().getElement(driver, pobj.editorTextArea));
				actions.click();
				actions.sendKeys(news.getNewsEditorText());
				actions.build().perform();
				fc.utobj().switchFrameToDefault(driver);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
			}
			
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

		} catch (Exception e) {
			Reporter.log(e.toString());
			fc.utobj().throwsException("Not able to add Folder");
		}
	}

	@Test(groups = {"thehub","hub_news"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add News Item in this Folder (At the time of folder creation) At Admin > The Hub > News > Home", testCaseId = "TC_19_Add_News_Item_In_Folder")
	private void addNewsItemToFolder() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminTheHubNewsPage pobj=new AdminTheHubNewsPage(driver);
			
			String emailId="hubautomation@staffex.com";
			
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporate_user=new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			fc.utobj().printTestStep(
					"Navigate To Admin > Users > Manage Corporate Users > Add  Corporate User");
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = corporate_user.createDefaultUser(driver, corpUser);
			
			fc.utobj().printTestStep("Navigate To Admin > The Hub > News Page");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String accessTo="All";
			
			boolean addNewsItemFolder=true;
			String newsItemTitle = fc.utobj().generateTestData(dataSet.get("newsItemTitle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String editorText = fc.utobj().generateTestData(dataSet.get("editorText"));
			
			News news=new News();
			news.setFolderName(folderName);
			news.setFolderSummary(folderSummary);
			news.setAccessTo(accessTo);
			news.setAddNewsItemFolder(addNewsItemFolder);
			news.setNewsItemTitle(newsItemTitle);
			news.setNewsBriefSummary(briefSummary);
			news.setNewsEditorText(editorText);
			
			fc.utobj().printTestStep("Add News Item In Folder");
			addFolder(driver, news);

			fc.utobj().printTestStep("Verify News Item In Folder");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, folderName);
			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to verify folder Name");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + newsItemTitle + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify News Item");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//span[contains(text () , '" + briefSummary + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Brief Summary Of News Item");
			}
			
			fc.home_page().logout(driver);
			
			fc.utobj().printTestStep("Login By Newly Created Corporate User");
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());
			fc.hub().hub_common().adminTheHubNewsPage(driver);
			
			fc.utobj().sendKeys(driver, pobj.searchNewsTextBx, newsItemTitle);
			fc.utobj().clickElement(driver, pobj.searchNewsTextBx);
			
			boolean isNewsItem=fc.utobj().assertPageSource(driver, newsItemTitle);
			if (!isNewsItem) {
				fc.utobj().throwsException("Search News is not working with Corporate User");
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"thehub","hub_news"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Add Folder with accessibility Corporarte Users At Admin > The Hub > News > Home", testCaseId = "TC_20_Add_Folder_Access_Corporate_Users")
	private void addFolderAccessToCorporateUsers() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > News Page");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String accessTo = "Corporate";
			
			News news=new News();
			news.setFolderName(folderName);
			news.setFolderSummary(folderSummary);
			news.setAccessTo(accessTo);
			
			fc.utobj().printTestStep("Add Folder");
			addFolder(driver,news);

			fc.utobj().printTestStep("Verify The Add Folder");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, folderName);
			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to verify Folder Creation With Corporate Access");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"thehub","hub_news"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add Folder with accessibility Divisional Users At Admin > The Hub > News > Home", testCaseId = "Tc_21_Add_Folder_Access_Divisional_Users")
	private void addFolderAccessToDivisionalUsers() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > Hidden Links > Configure New Hierarchy Level Page");
			AdminHiddenLinksConfigureNewHierarchyLevelPageTest configureHierarchyLevel = new AdminHiddenLinksConfigureNewHierarchyLevelPageTest();
			configureHierarchyLevel.ConfigureNewHierarchyLevel(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > News Page");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String accessTo = "Divisional";

			News news=new News();
			news.setFolderName(folderName);
			news.setFolderSummary(folderSummary);
			news.setAccessTo(accessTo);
			
			fc.utobj().printTestStep("Add Folder");
			addFolder(driver , news);

			fc.utobj().printTestStep("Verify Add Folder");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, folderName);
			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to verify Folder Creation With Divisional Usesrs Access");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"thehub","hub_news"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add Folder with accessibility Regional Users At Admin > The Hub > News > Home", testCaseId = "TC_22_Add_Folder_Access_Regional_Users")
	private void addFolderAccessToRegionalUsers() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > News Page");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String accessTo = "Regional";

			News news=new News();
			news.setFolderName(folderName);
			news.setFolderSummary(folderSummary);
			news.setAccessTo(accessTo);
			
			fc.utobj().printTestStep("Add Folder");
			addFolder(driver, news);

			fc.utobj().printTestStep("Verify Add Folder");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, folderName);
			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to verify Folder Creation With Regional Access");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub","hub_news"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add Folder with accessibility Franchise Users At Admin > The Hub > News > Home ", testCaseId = "TC_23_Add_Folder_Access_Franchise_Users")
	private void addFolderAccessToFranchiseUsers() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > News Page");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String accessTo = "Franchise";

			News news=new News();
			news.setFolderName(folderName);
			news.setFolderSummary(folderSummary);
			news.setAccessTo(accessTo);
			
			fc.utobj().printTestStep("Add Folder");
			addFolder(driver, news);

			fc.utobj().printTestStep("Verify Add Folder");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, folderName);
			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to verify Folder Creation With Franchise Access");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"thehub","hub_news"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Move Folders At Admin > The Hub > News > Home", testCaseId = "TC_24_Move_Folders")
	private void moveFolders() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminTheHubNewsPage pobj = new AdminTheHubNewsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > News Page");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String accessTo = "Franchise";

			News news=new News();
			news.setFolderName(folderName);
			news.setFolderSummary(folderSummary);
			news.setAccessTo(accessTo);
			
			fc.utobj().printTestStep("Add First Folder");
			addFolder(driver, news);

			fc.utobj().printTestStep("Add Second Folder");
			accessTo = "All";
			String folderName1 = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary1 = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			
			news.setFolderName(folderName1);
			news.setFolderSummary(folderSummary1);
			news.setAccessTo(accessTo);
			addFolder(driver, news);

			fc.utobj().printTestStep("Move Folder");
			fc.utobj().clickElement(driver, pobj.homeTab);

			Actions builder = new Actions(driver);
			builder.moveToElement(fc.utobj().getElement(driver, pobj.moreActions))
					.click(fc.utobj().getElement(driver, pobj.moreActions));
			builder.perform();
			fc.utobj().getElement(driver, pobj.moveFoldersLink).click();

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().selectDropDown(driver, pobj.moveFromSelect, folderName);
			fc.utobj().selectDropDown(driver, pobj.moveToSelect, folderName1);
			fc.utobj().clickElement(driver, pobj.moveBtn);
			fc.utobj().clickElement(driver, pobj.okayBtn);

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + folderName1 + "')]"));
			fc.utobj().printTestStep("Verify The Moved Folder");

			boolean isTextPresent = fc.utobj().assertPageSource(driver, folderName1);
			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to move folder");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub","hub_news"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Move News Item into another Folder At Admin > The Hub > News > Home ", testCaseId = "TC_25_Move_News_Items")
	private void moveNewsItems() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminTheHubNewsPage pobj = new AdminTheHubNewsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > News Page");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String accessTo = "Franchise";
			
			News news=new News();
			news.setFolderName(folderName);
			news.setFolderSummary(folderSummary);
			news.setAccessTo(accessTo);
			
			fc.utobj().printTestStep("Add Folder");
			addFolder(driver, news);

			fc.utobj().printTestStep("Add Folder");
			accessTo = "All";
			String folderName1 = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary1 = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			
			news.setFolderName(folderName1);
			news.setFolderSummary(folderSummary1);
			news.setAccessTo(accessTo);
			addFolder(driver, news);
			
			fc.utobj().printTestStep("Add News Item");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + folderName + "')]"));
			
			String newsItemTitle = fc.utobj().generateTestData(dataSet.get("newsItemTitle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String editorText = fc.utobj().generateTestData(dataSet.get("editorText"));
			news.setNewsItemTitle(newsItemTitle);
			news.setNewsBriefSummary(briefSummary);
			news.setNewsEditorText(editorText);
			String file = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			news.setFile(file);
			addNewsItems(driver, news);

			fc.utobj().printTestStep("Move News Item");
			fc.utobj().clickElement(driver, pobj.homeTab);
			fc.utobj().moveToElementThroughAction(driver, pobj.moreActions);
			fc.utobj().getElement(driver, pobj.moveNewsItemsLink).click();

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().selectDropDown(driver, pobj.moveFromSelect, folderName);
			fc.utobj().clickElement(driver, pobj.continueBtn);
			fc.utobj().selectDropDown(driver, pobj.newsItemSelect, newsItemTitle);
			fc.utobj().selectDropDown(driver, pobj.moveToSelect, folderName1);
			fc.utobj().clickElement(driver, pobj.moveBtn);
			fc.utobj().clickElement(driver, pobj.okayBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, pobj.homeTab);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + folderName1 + "')]"));

			fc.utobj().printTestStep("Verify Move News Item");
			if (!fc.utobj().getElementByXpath(driver, ".//a[.='" + newsItemTitle + "']").isDisplayed()) {
				fc.utobj().throwsException("was not able to move news item into folder");
			}

			fc.utobj().printTestStep("Verify Define Folder Sequence Option should be under More Actions");
			fc.utobj().sleep();
			fc.utobj().clickElement(driver, pobj.homeTab);
			fc.utobj().moveToElementThroughAction(driver, pobj.moreActions);
			
			fc.utobj().getElement(driver, pobj.defineFolderSequence).click();
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			if (fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[@id='newsSequence']//*[contains(text(),'Define Folder Sequence')]") == false) {
				fc.utobj().throwsException("Define Folder Sequence Option is not working");
			}
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"thehub","hub_news"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add News Item In Folder At Admin > The Hub > News > Folder", testCaseId = "TC_26_Add_News_Items_In_Folder")
	private void addNewsItemInFolder() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminTheHubNewsPage pobj = new AdminTheHubNewsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > News Page");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String accessTo = "All";

			News news=new News();
			news.setFolderName(folderName);
			news.setFolderSummary(folderSummary);
			news.setAccessTo(accessTo);
			
			fc.utobj().printTestStep("Add Folder");
			addFolder(driver,news);

			fc.utobj().printTestStep("Add News Item In Folder");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + folderName + "')]"));

			fc.utobj().clickElement(driver, pobj.addNewsItemLink);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String newsItemTitle = fc.utobj().generateTestData(dataSet.get("newsItemTitle"));
			fc.utobj().sendKeys(driver, pobj.newsItemTitle, newsItemTitle);
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			fc.utobj().sendKeys(driver, pobj.briefSummary, briefSummary);

			String file = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));

			fc.utobj().sendKeys(driver, pobj.newsItemFile, file);

			String editorText = fc.utobj().generateTestData(dataSet.get("editorText"));
			fc.utobj().switchFrameById(driver, "completeNews_ifr");

			Actions actions = new Actions(driver);
			actions.moveToElement(fc.utobj().getElement(driver, pobj.editorTextArea));
			actions.click();
			actions.sendKeys(editorText);
			fc.utobj().logReportMsg("Entered Text", editorText);
			actions.build().perform();
			fc.utobj().switchFrameToDefault(driver);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().sleep();
			
			fc.utobj().printTestStep("Verify The Add News Item In Folder");
			if (!fc.utobj().getElementByXpath(driver, ".//a[.='" + newsItemTitle + "']").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify add News Item Story");
			}

			fc.utobj().printTestStep(
					"Verify Download, Report, Modify, Archive, Delete option should be there under news item action icon");
			List<String> options = new ArrayList<>();
			options.add("Download");
			options.add("Report");
			options.add("Modify");
			options.add("Archive");
			options.add("Delete");
			fc.utobj().optionPresentInActionMenu(driver, newsItemTitle, options);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	void addNewsItems(WebDriver driver, News news) throws Exception {
		try {
			AdminTheHubNewsPage pobj = new AdminTheHubNewsPage(driver);
			fc.utobj().clickElement(driver, pobj.addNewsItemLink);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.newsItemTitle, news.getNewsItemTitle());
			fc.utobj().sendKeys(driver, pobj.briefSummary, news.getNewsBriefSummary());
			fc.utobj().sendKeys(driver, pobj.newsItemFile, news.getFile());
			fc.utobj().switchFrameById(driver, "completeNews_ifr");

			Actions actions = new Actions(driver);
			actions.moveToElement(fc.utobj().getElement(driver, pobj.editorTextArea));
			actions.click();
			actions.sendKeys(news.getNewsEditorText());
			actions.build().perform();
			fc.utobj().switchFrameToDefault(driver);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);
		} catch (Exception e) {
			Reporter.log(e.getMessage().toString());
			fc.utobj().throwsException("Not able to add News Item In Folder");
		}
	}

	@Test(groups = {"thehub","hub_news"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Modify Folder At Admin > The Hub > News > Folder", testCaseId = "TC_27_Modify_Folder")
	private void modifyFolder() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminTheHubNewsPage pobj = new AdminTheHubNewsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > News Page");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String accessTo = "All";

			News news=new News();
			news.setFolderName(folderName);
			news.setFolderSummary(folderSummary);
			news.setAccessTo(accessTo);
			
			fc.utobj().printTestStep("Add Folder");
			addFolder(driver, news);

			fc.utobj().printTestStep("Modify Folder");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + folderName + "')]"));
			fc.utobj().clickElement(driver, pobj.modifyFolder);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			fc.utobj().sendKeys(driver, pobj.folderName, folderName);
			fc.utobj().sendKeys(driver, pobj.folderSummary, folderSummary);
			if (!fc.utobj().isSelected(driver, pobj.accessibleToAll.get(0))) {
				fc.utobj().clickElement(driver, pobj.accessibleToAll.get(0));
			}
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + folderName + "')]"));

			fc.utobj().printTestStep("Verify The Modify Folder");
			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + folderName + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to modify Folder Name");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"thehub","hub_news"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Delete Folder At Admin > The Hub > News > Folder", testCaseId = "TC_28_Delete_Folder")
	private void deleteFolder() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminTheHubNewsPage pobj = new AdminTheHubNewsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > News Page");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String accessTo = "All";
			
			News news=new News();
			news.setFolderName(folderName);
			news.setFolderSummary(folderSummary);
			news.setAccessTo(accessTo);
			
			fc.utobj().printTestStep("Add Folder");
			addFolder(driver, news);

			fc.utobj().printTestStep("Delete Folder");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + folderName + "')]"));
			fc.utobj().clickElement(driver, pobj.deleteFolder);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.okayBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Folder");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, folderName);
			if (isTextPresent == true) {
				fc.utobj().throwsException("was not able to delete folder");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"thehub","hub_news"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Archive Folder At Admin > The Hub > News > Folder", testCaseId = "TC_29_Archive_Folder")
	private void archiveFolder() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminTheHubNewsPage pobj = new AdminTheHubNewsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > News Page");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String accessTo = "All";

			News news=new News();
			news.setFolderName(folderName);
			news.setFolderSummary(folderSummary);
			news.setAccessTo(accessTo);
			
			fc.utobj().printTestStep("Add Folder");
			addFolder(driver, news);

			fc.utobj().printTestStep("Archive Folder");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + folderName + "')]"));

			fc.utobj().printTestStep("Verify The Define Sub Folder Sequence Link");
			fc.utobj().moveToElementThroughAction(driver, pobj.moreActions);
			if (fc.utobj().isElementPresent(driver, pobj.defineSubFolderSequenceLink) == false) {
				fc.utobj().throwsException("not able to verify Define Sub Folder Sequence Link");
			}

			fc.utobj().printTestStep("Verify The Define News Item Sequence Link");
			fc.utobj().moveToElementThroughAction(driver, pobj.moreActions);
			if (fc.utobj().isElementPresent(driver, pobj.defineNewsItemSequenceLink) == false) {
				fc.utobj().throwsException("not able to verify Define News Item Sequence Link");
			}

			fc.utobj().printTestStep("Navigate To Archived Folder Tab");
			fc.utobj().moveToElementThroughAction(driver, pobj.moreActions);
			fc.utobj().getElement(driver, pobj.archiveLink).click();

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.okayBtn);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().moveToElementThroughAction(driver, pobj.archivedNewsTab);
			
			WebElement element = fc.utobj().getElementByXpath(driver, ".//a[.='" + folderName + "']");
			fc.utobj().logReport("Clicking on Element", element);
			element.click();

			fc.utobj().printTestStep("Verify The Archive Folder");
			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + folderName + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to archive folder");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"thehub","hub_news"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add Sub Folder At Admin > The Hub > News > Folder ", testCaseId = "TC_30_Add_Sub_Folder")
	private void addSubFolder() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminTheHubNewsPage pobj = new AdminTheHubNewsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > News Page");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String accessTo = "All";
			
			News news=new News();
			news.setFolderName(folderName);
			news.setFolderSummary(folderSummary);
			news.setAccessTo(accessTo);
			
			fc.utobj().printTestStep("Add Folder");
			addFolder(driver, news);

			fc.utobj().printTestStep("Add Sub Folder");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + folderName + "')]"));

			Actions builder = new Actions(driver);
			builder.moveToElement(fc.utobj().getElement(driver, pobj.moreActions))
					.click(fc.utobj().getElement(driver, pobj.moreActions));
			builder.perform();
			fc.utobj().getElement(driver, pobj.addSubFolder).click();

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String subFolderName = fc.utobj().generateTestData(dataSet.get("subFolderName"));
			String subFolderSummary = fc.utobj().generateTestData(dataSet.get("subFolderSummary"));
			fc.utobj().sendKeys(driver, pobj.folderName, subFolderName);
			fc.utobj().sendKeys(driver, pobj.folderSummary, subFolderSummary);
			if (!fc.utobj().isSelected(driver, pobj.accessibleToAll.get(0))) {
				fc.utobj().clickElement(driver, pobj.accessibleToAll.get(0));
			}
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			WebElement element = fc.utobj().getElementByXpath(driver, ".//a[.='" + folderName + "']");
			fc.utobj().logReport("Move To Element", element);
			builder.moveToElement(element).click(element);
			builder.perform();

			WebElement element1 = fc.utobj().getElementByXpath(driver, ".//a[.='" + subFolderName + "']");
			fc.utobj().logReport("Clicking on Element", element1);
			element1.click();

			fc.utobj().printTestStep("Verify The Add Sub Folder");
			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + subFolderName + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to add Sub folder");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"thehub","hub_news"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Report News Item At Admin > The Hub > News > Folder", testCaseId = "TC_31_Report_News_Item")
	private void reportNewsItemActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminTheHubNewsPage pobj = new AdminTheHubNewsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > News Page");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String accessTo = "All";
			
			News news=new News();
			news.setFolderName(folderName);
			news.setFolderSummary(folderSummary);
			news.setAccessTo(accessTo);
			
			fc.utobj().printTestStep("Add Folder");
			addFolder(driver, news);
			fc.utobj().printTestStep("Add News Item");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + folderName + "')]"));
			String newsItemTitle = fc.utobj().generateTestData(dataSet.get("newsItemTitle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String editorText = fc.utobj().generateTestData(dataSet.get("editorText"));
			String file = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			news.setNewsItemTitle(newsItemTitle);
			news.setNewsBriefSummary(briefSummary);
			news.setNewsEditorText(editorText);
			news.setFile(file);
			addNewsItems(driver,news);
			
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + news.getFolderName() + "')]"));
			fc.utobj().sleep();
			fc.utobj().printTestStep("Verify The Report Of News Item");
			fc.utobj().actionImgOption(driver, newsItemTitle, "Report");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			WebElement element = fc.utobj().getElementByXpath(driver,
					".//td[contains(text () ,'" + newsItemTitle + "')]");
			fc.utobj().moveToElement(driver, element);
			if (!element.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify News Item Title");
			}

			WebElement element1 = fc.utobj().getElementByXpath(driver,
					".//td[contains(text () ,'" + briefSummary + "')]");
			fc.utobj().moveToElement(driver, element1);
			if (!element1.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify News Item's Summary");
			}

			WebElement element3 = fc.utobj().getElementByXpath(driver,
					".//td[contains(text () ,'FranConnect Administrator')]");
			fc.utobj().moveToElement(driver, element3);
			if (!element3.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify User[By Add]");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"thehub","hub_news"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Modify News Item By Action Img Icon At Admin > The Hub > News > Folder", testCaseId = "TC_32_Modify_News_Item")
	private void modifyNewsItemActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminTheHubNewsPage pobj = new AdminTheHubNewsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > News Page");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String accessTo = "All";
			
			News news=new News();
			news.setFolderName(folderName);
			news.setFolderSummary(folderSummary);
			news.setAccessTo(accessTo);
			fc.utobj().printTestStep("Add Folder");
			addFolder(driver, news);
			
			fc.utobj().printTestStep("Add News Item");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + folderName + "')]"));
			String newsItemTitle = fc.utobj().generateTestData(dataSet.get("newsItemTitle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String editorText = fc.utobj().generateTestData(dataSet.get("editorText"));
			String file = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			news.setNewsItemTitle(newsItemTitle);
			news.setNewsBriefSummary(briefSummary);
			news.setNewsEditorText(editorText);
			news.setFile(file);
			addNewsItems(driver,news);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + news.getFolderName() + "')]"));
			fc.utobj().sleep();
			
			fc.utobj().printTestStep("Modify News Item");
			fc.utobj().actionImgOption(driver, newsItemTitle, "Modify");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			newsItemTitle = fc.utobj().generateTestData(dataSet.get("newsItemTitle"));
			briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String editorText1 = fc.utobj().generateTestData(dataSet.get("editorText"));

			fc.utobj().sendKeys(driver, pobj.newsItemTitle, newsItemTitle);
			fc.utobj().sendKeys(driver, pobj.briefSummary, briefSummary);
			fc.utobj().sendKeys(driver, pobj.newsItemFile, file);
			fc.utobj().switchFrameById(driver, "completeNews_ifr");

			Actions actions = new Actions(driver);
			actions.moveToElement(fc.utobj().getElement(driver, pobj.editorTextArea));
			actions.click();
			actions.sendKeys(editorText1);
			actions.build().perform();
			fc.utobj().switchFrameToDefault(driver);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().sleep();
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			if (!fc.utobj().getElementByXpath(driver, ".//a[.='" + newsItemTitle + "']").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify add News Item Story");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//*[.='" + briefSummary + "']").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify add News Item Story's Summary");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//td/span[contains(text () , 'FranConnect Administrator')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify add News Item Story's Summary");
			}

			fc.utobj().printTestStep("Verify The Modified News Item");

			WebElement elementCLik = fc.utobj().getElementByLinkText(driver, newsItemTitle);
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", elementCLik);

			fc.commonMethods().switch_cboxIframe_frameId(driver);

			WebElement element = fc.utobj().getElementByXpath(driver,
					".//td[contains(text () ,'" + newsItemTitle + "')]");
			fc.utobj().moveToElement(driver, element);
			if (!element.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify News Item Title");
			}

			WebElement element1 = fc.utobj().getElementByXpath(driver,
					".//td[contains(text () ,'" + briefSummary + "')]");
			fc.utobj().moveToElement(driver, element1);
			if (!element1.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify News Item's Summary");
			}

			String text = editorText.concat(editorText1);
			WebElement element3 = fc.utobj().getElementByXpath(driver, ".//*[contains(text () ,'" + text + "')]");
			fc.utobj().moveToElement(driver, element3);
			if (!element3.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify EditorText");
			}

			fc.utobj().sleep();
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"thehub","hub_news"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Archive News Item By Action Img Icon At Admin > The Hub > News > Folder", testCaseId = "TC_33_Archive_News_Item")
	private void archiveNewsItemActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminTheHubNewsPage pobj = new AdminTheHubNewsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > News Page");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String accessTo = "All";
			
			News news=new News();
			news.setFolderName(folderName);
			news.setFolderSummary(folderSummary);
			news.setAccessTo(accessTo);
			
			fc.utobj().printTestStep("Add Folder");
			addFolder(driver, news);

			fc.utobj().printTestStep("Add News Item for archive");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + news.getFolderName() + "')]"));
			String newsItemTitle1 = fc.utobj().generateTestData(dataSet.get("newsItemTitle"));
			String briefSummary1 = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String editorText1 = fc.utobj().generateTestData(dataSet.get("editorText"));
			String file = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			
			news.setNewsItemTitle(newsItemTitle1);
			news.setNewsBriefSummary(briefSummary1);
			news.setNewsEditorText(editorText1);
			news.setFile(file);
			
			fc.utobj().printTestStep("Add News Item to search in archive news");
			addNewsItems(driver,news);
			
			String newsItemTitle = fc.utobj().generateTestData(dataSet.get("newsItemTitle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String editorText = fc.utobj().generateTestData(dataSet.get("editorText"));

			news.setNewsItemTitle(newsItemTitle);
			news.setNewsBriefSummary(briefSummary);
			news.setNewsEditorText(editorText);
			
			addNewsItems(driver,news);
			
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + news.getFolderName() + "')]"));
			fc.utobj().sleep();
			
			fc.utobj().printTestStep("Archive News Item");
			fc.utobj().actionImgOption(driver, newsItemTitle, "Archive");
			fc.utobj().acceptAlertBox(driver);

			boolean isTextPresent = fc.utobj().assertPageSource(driver, newsItemTitle);
			if (isTextPresent == true) {
				fc.utobj().throwsException("was not able to Archive News Item :: Verification At News Item Page");
			}

			fc.utobj().printTestStep("Verify The Archive News Item At Archived Tab");
			fc.utobj().clickElement(driver, pobj.archivedNewsTab);

			fc.utobj().printTestStep("Search Archive News By Search News");
			fc.utobj().sendKeys(driver, pobj.searchNewsTextBx, newsItemTitle);
			fc.utobj().clickElement(driver, pobj.searchNewsTextBx);
			
			if (!fc.utobj().getElementByXpath(driver, ".//a[.='" + newsItemTitle + "']").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify add News Item Story");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//*[.='" + briefSummary + "']").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify add News Item Story's Summary");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//td/span[contains(text () , 'FranConnect Administrator')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify add News Item Story's Summary");
			}

			WebElement elementCLik = fc.utobj().getElementByLinkText(driver, newsItemTitle);
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", elementCLik);

			fc.commonMethods().switch_cboxIframe_frameId(driver);

			WebElement element = fc.utobj().getElementByXpath(driver,
					".//td[contains(text () ,'" + newsItemTitle + "')]");
			fc.utobj().moveToElement(driver, element);
			if (!element.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify News Item Title");
			}

			WebElement element1 = fc.utobj().getElementByXpath(driver,
					".//td[contains(text () ,'" + briefSummary + "')]");
			fc.utobj().moveToElement(driver, element1);
			if (!element1.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify News Item's Summary");
			}

			WebElement element3 = fc.utobj().getElementByXpath(driver, ".//*[contains(text () ,'" + editorText + "')]");
			fc.utobj().moveToElement(driver, element3);
			if (!element3.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify EditorText");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);
			
			fc.utobj().printTestStep("verify Not Archive news should not search by search news");
			
			fc.utobj().printTestStep("Search Archive News By Search News");
			fc.utobj().sendKeys(driver, pobj.searchNewsTextBx, newsItemTitle1);
			fc.utobj().clickElement(driver, pobj.searchNewsTextBx);
			
			boolean isNotArchivedNews=fc.utobj().assertPageSource(driver, newsItemTitle1);
			if (isNotArchivedNews) {
				fc.utobj().throwsException("Not archived news is searchable by search news");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"thehub","hub_news"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Delete News Item By Action Img Icon At Admin > The Hub > News > Folder", testCaseId = "TC_34_Delete_News_Item")
	private void deleteNewsItemActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > News Page");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String accessTo = "All";
			
			News news=new News();
			news.setFolderName(folderName);
			news.setFolderSummary(folderSummary);
			news.setAccessTo(accessTo);
			fc.utobj().printTestStep("Add Folder");
			addFolder(driver, news);
			
			fc.utobj().printTestStep("Add News Item");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + news.getFolderName() + "')]"));
			String newsItemTitle = fc.utobj().generateTestData(dataSet.get("newsItemTitle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String editorText = fc.utobj().generateTestData(dataSet.get("editorText"));
			String file = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			news.setNewsItemTitle(newsItemTitle);
			news.setNewsBriefSummary(briefSummary);
			news.setNewsEditorText(editorText);
			news.setFile(file);
			addNewsItems(driver,news);
			
			fc.utobj().printTestStep("Delete News Item");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + news.getFolderName() + "')]"));
			fc.utobj().sleep();
			fc.utobj().actionImgOption(driver, newsItemTitle, "Delete");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Delete News Item");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, newsItemTitle);

			if (isTextPresent == true) {
				fc.utobj().throwsException("was not able to Delete News Item :: Verification At News Item Page");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"thehub","hub_news"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Report News Item At Admin > The Hub > News > Archived News", testCaseId = "TC_35_Report_News_Item")
	private void reportNewsItemActionImgArchivedNews() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminTheHubNewsPage pobj = new AdminTheHubNewsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > News Page");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String accessTo = "All";

			News news=new News();
			news.setFolderName(folderName);
			news.setFolderSummary(folderSummary);
			news.setAccessTo(accessTo);
			
			fc.utobj().printTestStep("Add Folder");
			addFolder(driver, news);

			fc.utobj().printTestStep("Add News Item In Folder");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + news.getFolderName() + "')]"));
			String newsItemTitle = fc.utobj().generateTestData(dataSet.get("newsItemTitle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String editorText = fc.utobj().generateTestData(dataSet.get("editorText"));
			String file = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			
			news.setNewsItemTitle(newsItemTitle);
			news.setNewsBriefSummary(briefSummary);
			news.setNewsEditorText(editorText);
			news.setFile(file);
			
			addNewsItems(driver,news);

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + news.getFolderName() + "')]"));
			fc.utobj().sleep();
			
			fc.utobj().printTestStep("Archive News Item");
			fc.utobj().actionImgOption(driver, newsItemTitle, "Archive");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Report Of Archive News Item");
			fc.utobj().clickElement(driver, pobj.archivedNewsTab);

			fc.utobj().actionImgOption(driver, newsItemTitle, "Report");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			WebElement element = fc.utobj().getElementByXpath(driver,
					".//*[contains(text () ,'" + newsItemTitle + "')]");
			fc.utobj().moveToElement(driver, element);
			if (!element.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify News Item Title");
			}

			WebElement element1 = fc.utobj().getElementByXpath(driver,
					".//*[contains(text () ,'" + briefSummary + "')]");
			fc.utobj().moveToElement(driver, element1);
			if (!element1.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify News Item's Summary");
			}

			WebElement element3 = fc.utobj().getElementByXpath(driver,
					".//*[contains(text () ,'FranConnect Administrator')]");
			fc.utobj().moveToElement(driver, element3);
			if (!element3.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify User[By Add]");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"thehub","hub_news"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Delete News Item By Action Img Icon At Admin > The Hub > News > Archived News ", testCaseId = "TC_36_Delete_News_Item")
	private void deleteNewsItemActionImgArchivedNews() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminTheHubNewsPage pobj = new AdminTheHubNewsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > News Page");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String accessTo = "All";
			
			News news=new News();
			news.setFolderName(folderName);
			news.setFolderSummary(folderSummary);
			news.setAccessTo(accessTo);
			
			fc.utobj().printTestStep("Add Folder");
			addFolder(driver, news);

			fc.utobj().printTestStep("Add News Item");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + folderName + "')]"));
			
			String newsItemTitle = fc.utobj().generateTestData(dataSet.get("newsItemTitle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String editorText = fc.utobj().generateTestData(dataSet.get("editorText"));
			String file = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));

			news.setNewsItemTitle(newsItemTitle);
			news.setNewsBriefSummary(briefSummary);
			news.setNewsEditorText(editorText);
			news.setFile(file);
			
			addNewsItems(driver,news);

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + news.getFolderName() + "')]"));
			fc.utobj().sleep();
			
			fc.utobj().printTestStep("Archive News Item");
			fc.utobj().actionImgOption(driver, newsItemTitle, "Archive");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().clickElement(driver, pobj.archivedNewsTab);

			fc.utobj().printTestStep("Delete Archive News Item");
			fc.utobj().actionImgOption(driver, newsItemTitle, "Delete");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().clickElement(driver, pobj.archivedNewsTab);

			fc.utobj().printTestStep("Verify The Delete Archive News Item");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, newsItemTitle);

			if (isTextPresent == true) {
				fc.utobj().throwsException("was not able to Delete News Item :: Verification At Archived News Page");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"thehub","hub_news"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add News Item In Top Stories Folder With Media Type Image At Admin > The Hub > News > Top Stories", testCaseId = "TC_37_Add_News_Item")
	private void addNewsItemTopStories() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminTheHubNewsPage pobj = new AdminTheHubNewsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > News Page");
			fc.hub().hub_common().adminTheHubNewsPage(driver);

			fc.utobj().printTestStep("Add News Item In Top Stories");
			fc.utobj().clickElement(driver, pobj.topStoriesTab);

			String newsItemTitle = fc.utobj().generateTestData(dataSet.get("newsItemTitle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String editorText = fc.utobj().generateTestData(dataSet.get("editorText"));
			String file = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			
			News news=new News();
			news.setNewsItemTitle(newsItemTitle);
			news.setNewsBriefSummary(briefSummary);
			news.setNewsEditorText(editorText);
			news.setFile(file);
			addNewsItems(driver, news);

			if (!fc.utobj().getElementByXpath(driver, ".//a[.='" + newsItemTitle + "']").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify add News Item Story");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//*[.='" + briefSummary + "']").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify add News Item Story's Summary");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//td/span[contains(text () , 'FranConnect Administrator')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify add News Item Story's Summary");
			}

			fc.utobj().printTestStep("Verify The News Item In Top Stories Folder");

			WebElement elementCLik = fc.utobj().getElementByLinkText(driver, newsItemTitle);
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", elementCLik);

			fc.commonMethods().switch_cboxIframe_frameId(driver);

			WebElement element = fc.utobj().getElementByXpath(driver,
					".//*[contains(text () ,'" + newsItemTitle + "')]");
			fc.utobj().moveToElement(driver, element);
			if (!element.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify News Item Title");
			}

			WebElement element1 = fc.utobj().getElementByXpath(driver,
					".//*[contains(text () ,'" + briefSummary + "')]");
			fc.utobj().moveToElement(driver, element1);
			if (!element1.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify News Item's Summary");
			}

			WebElement element3 = fc.utobj().getElementByXpath(driver, ".//*[contains(text () ,'" + editorText + "')]");
			fc.utobj().moveToElement(driver, element3);
			if (!element3.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify EditorText");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	void addNewsItemInTopStories(WebDriver driver, News news) throws Exception {
		try {
			AdminTheHubNewsPage pobj = new AdminTheHubNewsPage(driver);
			fc.hub().hub_common().adminTheHubNewsPage(driver);
			fc.utobj().clickElement(driver, pobj.topStoriesTab);

			fc.utobj().clickElement(driver, pobj.addNewsItemLink);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().sendKeys(driver, pobj.newsItemTitle, news.getNewsItemTitle());
			fc.utobj().sendKeys(driver, pobj.briefSummary, news.getNewsBriefSummary());

			if (news.getMediaType().equalsIgnoreCase("Image")) {

				if (!fc.utobj().isSelected(driver, pobj.mediaType.get(0))) {
					fc.utobj().clickElement(driver, pobj.mediaType.get(0));
				}

				fc.utobj().sendKeys(driver, pobj.newsItemFile, news.getFile());

			} else if (news.getMediaType().equalsIgnoreCase("Video")) {

				if (!fc.utobj().isSelected(driver, pobj.mediaType.get(1))) {
					fc.utobj().clickElement(driver, pobj.mediaType.get(1));
				}

				fc.utobj().sendKeys(driver, pobj.videoFile, news.getFile());

			} else if (news.getMediaType().equalsIgnoreCase("Embed Code")) {

				if (!fc.utobj().isSelected(driver, pobj.mediaType.get(2))) {
					fc.utobj().clickElement(driver, pobj.mediaType.get(2));
				}

				fc.utobj().sendKeys(driver, pobj.embedCode, news.getNewsEmbedCode());
			}

			fc.utobj().switchFrameById(driver, "completeNews_ifr");

			Actions actions = new Actions(driver);
			actions.moveToElement(fc.utobj().getElement(driver, pobj.editorTextArea));
			actions.click();
			actions.sendKeys(news.getNewsEditorText());
			actions.build().perform();
			fc.utobj().switchFrameToDefault(driver);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);
		} catch (Exception e) {
			Reporter.log(e.getMessage().toString());
			fc.utobj().throwsException("Not able to add Top Story");
		}
	}

	@Test(groups = {"thehub","hub_news"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Report News Item In Top Stories Folder With Media Type Image At Admin > The Hub > News > Top Stories", testCaseId = "TC_38_Report_News_Item")
	private void reportNewsItemActionImgTopStories() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminTheHubNewsPage pobj = new AdminTheHubNewsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > News > Top Stories");
			fc.utobj().printTestStep("Add News Item In Top Stories Folder");
			String newsItemTitle = fc.utobj().generateTestData(dataSet.get("newsItemTitle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String editorText = fc.utobj().generateTestData(dataSet.get("editorText"));
			String mediaType = "Video";
			String file = fc.utobj().getFilePathFromTestData(dataSet.get("videoFile"));
			
			News news=new News();
			news.setNewsItemTitle(newsItemTitle);
			news.setNewsBriefSummary(briefSummary);
			news.setNewsEditorText(editorText);
			news.setMediaType(mediaType);
			news.setFile(file);
			
			addNewsItemInTopStories(driver, news);

			fc.utobj().printTestStep("Verify The Report");
			fc.utobj().actionImgOption(driver, newsItemTitle, "Report");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			WebElement element = fc.utobj().getElementByXpath(driver,
					".//*[contains(text () ,'" + newsItemTitle + "')]");
			fc.utobj().moveToElement(driver, element);
			if (!element.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify News Item Title");
			}

			WebElement element1 = fc.utobj().getElementByXpath(driver,
					".//*[contains(text () ,'" + briefSummary + "')]");
			fc.utobj().moveToElement(driver, element1);
			if (!element1.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify News Item's Summary");
			}

			WebElement element2 = fc.utobj().getElementByXpath(driver,
					".//*[contains(text () , 'FranConnect Administrator')]");
			fc.utobj().moveToElement(driver, element2);
			if (!element2.isDisplayed()) {
				fc.utobj().throwsException("was not able to added By");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"thehub","hub_news"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Modify News Item In Top Stories Folder With Media Type Image At Admin > The Hub > News > Top Stories", testCaseId = "TC_39_Modify_News_Item")
	private void modifyNewsItemActionImgTopStories() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminTheHubNewsPage pobj = new AdminTheHubNewsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > News > Top Stories");
			String newsItemTitle = fc.utobj().generateTestData(dataSet.get("newsItemTitle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String editorText = fc.utobj().generateTestData(dataSet.get("editorText"));
			String mediaType = "Video";
			String file = fc.utobj().getFilePathFromTestData(dataSet.get("videoFile"));
			
			News news=new News();
			news.setNewsItemTitle(newsItemTitle);
			news.setNewsBriefSummary(briefSummary);
			news.setNewsEditorText(editorText);
			news.setMediaType(mediaType);
			news.setFile(file);

			fc.utobj().printTestStep("Add News Item In Top Stories Folder");
			addNewsItemInTopStories(driver,news);

			fc.utobj().printTestStep("Modify The News Item");
			fc.utobj().actionImgOption(driver, newsItemTitle, "Modify");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			newsItemTitle = fc.utobj().generateTestData(dataSet.get("newsItemTitle"));
			briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String editorText1 = fc.utobj().generateTestData(dataSet.get("editorText"));
			String file1 = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));

			fc.utobj().sendKeys(driver, pobj.newsItemTitle, newsItemTitle);
			fc.utobj().sendKeys(driver, pobj.briefSummary, briefSummary);

			if (!fc.utobj().isSelected(driver, pobj.mediaType.get(0))) {
				fc.utobj().clickElement(driver, pobj.mediaType.get(0));
			}

			fc.utobj().sendKeys(driver, pobj.newsItemFile, file1);
			fc.utobj().switchFrameById(driver, "completeNews_ifr");

			Actions actions = new Actions(driver);
			actions.moveToElement(fc.utobj().getElement(driver, pobj.editorTextArea));
			actions.click();
			actions.sendKeys(editorText1);
			actions.build().perform();
			fc.utobj().switchFrameToDefault(driver);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Modify News Item");
			WebElement element = fc.utobj().getElementByXpath(driver,
					".//*[contains(text () ,'" + newsItemTitle + "')]");
			fc.utobj().moveToElement(driver, element);
			if (!element.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify News Item Title");
			}

			WebElement element1 = fc.utobj().getElementByXpath(driver,
					".//*[contains(text () ,'" + briefSummary + "')]");
			fc.utobj().moveToElement(driver, element1);
			if (!element1.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify News Item's Summary");
			}

			WebElement element2 = fc.utobj().getElementByXpath(driver,
					".//*[contains(text () , 'FranConnect Administrator')]");
			fc.utobj().moveToElement(driver, element2);
			if (!element2.isDisplayed()) {
				fc.utobj().throwsException("was not able to added By");
			}

			WebElement elementCLik = fc.utobj().getElementByLinkText(driver, newsItemTitle);
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", elementCLik);

			fc.commonMethods().switch_cboxIframe_frameId(driver);

			WebElement element3 = fc.utobj().getElementByXpath(driver,
					".//*[contains(text () ,'" + newsItemTitle + "')]");
			fc.utobj().moveToElement(driver, element3);
			if (!element3.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify News Item Title");
			}

			WebElement element4 = fc.utobj().getElementByXpath(driver,
					".//*[contains(text () ,'" + briefSummary + "')]");
			fc.utobj().moveToElement(driver, element4);
			if (!element4.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify News Item's Summary");
			}

			String text = editorText.concat(editorText1);
			System.out.println(text);
			WebElement element5 = fc.utobj().getElementByXpath(driver, ".//*[contains(text () ,'" + text + "')]");
			fc.utobj().moveToElement(driver, element5);
			if (!element5.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify EditorText");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"thehub","hub_news"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Archive News Item In Top Stories Folder With Media Type Image At Admin > The Hub > News > Top Stories", testCaseId = "TC_40_Archive_News_Item")
	private void archiveNewsItemActionImgTopStories() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminTheHubNewsPage pobj = new AdminTheHubNewsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > News > Top Stories");
			String newsItemTitle = fc.utobj().generateTestData(dataSet.get("newsItemTitle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String editorText = fc.utobj().generateTestData(dataSet.get("editorText"));
			String mediaType = "Video";
			String file = fc.utobj().getFilePathFromTestData(dataSet.get("videoFile"));
			
			News news=new News();
			news.setNewsItemTitle(newsItemTitle);
			news.setNewsBriefSummary(briefSummary);
			news.setNewsEditorText(editorText);
			news.setMediaType(mediaType);
			news.setFile(file);

			fc.utobj().printTestStep("Add News Item In Top Stories Folder");
			addNewsItemInTopStories(driver,news);

			fc.utobj().printTestStep("Archive News Item");
			fc.utobj().actionImgOption(driver, newsItemTitle, "Archive");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().clickElement(driver, pobj.archivedNewsTab);

			fc.utobj().printTestStep("Verify The Archive News Item");
			WebElement element = fc.utobj().getElementByXpath(driver,
					".//*[contains(text () ,'" + newsItemTitle + "')]");
			fc.utobj().moveToElement(driver, element);
			if (!element.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify News Item Title");
			}

			WebElement element1 = fc.utobj().getElementByXpath(driver,
					".//*[contains(text () ,'" + briefSummary + "')]");
			fc.utobj().moveToElement(driver, element1);
			if (!element1.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify News Item's Summary");
			}

			WebElement element2 = fc.utobj().getElementByXpath(driver,
					".//*[contains(text () , 'FranConnect Administrator')]");
			fc.utobj().moveToElement(driver, element2);
			if (!element2.isDisplayed()) {
				fc.utobj().throwsException("was not able to added By");
			}

			WebElement elementCLik = fc.utobj().getElementByLinkText(driver, newsItemTitle);
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", elementCLik);

			fc.commonMethods().switch_cboxIframe_frameId(driver);

			WebElement element3 = fc.utobj().getElementByXpath(driver,
					".//td[contains(text () ,'" + newsItemTitle + "')]");
			fc.utobj().moveToElement(driver, element3);
			if (!element3.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify News Item Title");
			}

			WebElement element4 = fc.utobj().getElementByXpath(driver,
					".//td[contains(text () ,'" + briefSummary + "')]");
			fc.utobj().moveToElement(driver, element4);
			if (!element4.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify News Item's Summary");
			}

			WebElement element5 = fc.utobj().getElementByXpath(driver, ".//*[contains(text () ,'" + editorText + "')]");
			fc.utobj().moveToElement(driver, element5);
			if (!element5.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify EditorText");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "thehub")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Delete News Item In Top Stories Folder With Media Type Image At Admin > The Hub > News > Top Stories", testCaseId = "TC_41_Delete_News_Item")
	private void deleteNewsItemActionImgTopStories() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Admin > The Hub > News > Top Stories");
			String newsItemTitle = fc.utobj().generateTestData(dataSet.get("newsItemTitle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String editorText = fc.utobj().generateTestData(dataSet.get("editorText"));
			String mediaType = "Embed Code";
			String embedCode = dataSet.get("embedCode");
			
			News news=new News();
			news.setNewsItemTitle(newsItemTitle);
			news.setNewsBriefSummary(briefSummary);
			news.setNewsEditorText(editorText);
			news.setMediaType(mediaType);
			news.setNewsEmbedCode(embedCode);
			fc.utobj().printTestStep("Add News Item In Top Stories Folder");
			addNewsItemInTopStories(driver,news);

			fc.utobj().printTestStep("Delete News Item");
			fc.utobj().actionImgOption(driver, newsItemTitle, "Delete");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify Delete News Item");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, newsItemTitle);
			if (isTextPresent == true) {
				fc.utobj().throwsException("was not able to delete News Item at Top Story Page");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Modify Folder At Admin > The Hub > News > Top Stories", testCaseId = "TC_42_Modify_Folder")
	private void modifyFolderTopStories() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminTheHubNewsPage pobj = new AdminTheHubNewsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > News Page");
			fc.hub().hub_common().adminTheHubNewsPage(driver);

			fc.utobj().printTestStep("Modify Top Stories Folder");
			fc.utobj().clickElement(driver, pobj.topStoriesTab);

			fc.utobj().clickElement(driver, pobj.modifyFolder);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().sendKeys(driver, pobj.folderName, "Top Stories");
			String folderSummary = dataSet.get("folderSummary");
			fc.utobj().sendKeys(driver, pobj.folderSummary, folderSummary);

			if (!fc.utobj().isSelected(driver, pobj.accessibleToAll.get(0))) {
				fc.utobj().clickElement(driver, pobj.accessibleToAll.get(0));
			}

			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Modify Top Stories Folder");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, "Top Stories");
			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to Modify Folder");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void addSubFolder(WebDriver driver,News news) throws Exception{
		try {
			AdminTheHubNewsPage pobj = new AdminTheHubNewsPage(driver);
			fc.utobj().printTestStep("Add Sub Folder");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + news.getFolderName() + "')]"));
			fc.utobj().clickElementWithActions(driver, pobj.moreActions);
			fc.utobj().getElement(driver, pobj.addSubFolder).click();

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.folderName, news.getSubFolderName());
			fc.utobj().sendKeys(driver, pobj.folderSummary, news.getSubFolderSummary());
			
			if (news.getAccessTo().equalsIgnoreCase("Corporate")) {
				fc.utobj().clickElement(driver,
						fc.utobj().getElementByXpath(driver, ".//input[@name ='chkRole' and @value='3']"));
			}else {
				if (!fc.utobj().isSelected(driver, pobj.accessibleToAll.get(0))) {
					fc.utobj().clickElement(driver, pobj.accessibleToAll.get(0));
				}
			}
			
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);
		} catch (Exception e) {
			Reporter.log(e.getMessage().toString());
			fc.utobj().throwsException("Not able to add Sub Folder");
		}
	}
	
	
	@Test(groups = {"thehub"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add Sub Folder At Admin > The Hub > News > Top Stories", testCaseId = "TC_43_Add_Sub_Folder")
	private void addSubFolderTopStories() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminTheHubNewsPage pobj = new AdminTheHubNewsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > News Page");
			fc.hub().hub_common().adminTheHubNewsPage(driver);

			fc.utobj().clickElement(driver, pobj.topStoriesTab);

			fc.utobj().printTestStep("Add Sub Folder In Top Stories");
			Actions builder = new Actions(driver);
			builder.moveToElement(fc.utobj().getElement(driver, pobj.moreActions))
					.click(fc.utobj().getElement(driver, pobj.moreActions));
			builder.perform();
			
			boolean isstoriesPresent = fc.utobj().assertLinkText(driver, "View All Stories");

			if (isstoriesPresent == false) {
				fc.utobj().throwsException("View All Stories Link is not present under More Action");
			}
			
			fc.utobj().getElement(driver, pobj.addSubFolder).click();

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			fc.utobj().sendKeys(driver, pobj.folderName, folderName);
			fc.utobj().sendKeys(driver, pobj.folderSummary, folderSummary);

			if (!fc.utobj().isSelected(driver, pobj.accessibleToAll.get(0))) {
				fc.utobj().clickElement(driver, pobj.accessibleToAll.get(0));
			}

			if (!fc.utobj().isSelected(driver, pobj.addNewsItemFolder.get(1))) {
				fc.utobj().clickElement(driver, pobj.addNewsItemFolder.get(1));
			}
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			builder.moveToElement(fc.utobj().getElement(driver, pobj.topStoriesTab))
					.click(fc.utobj().getElement(driver, pobj.topStoriesTab));
			builder.perform();

			fc.utobj().printTestStep("Verify The Add Sub Folder");
			WebElement element1 = fc.utobj().getElementByXpath(driver, ".//a[.='" + folderName + "']");
			element1.click();

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + folderName + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to add Sub folder");
			}

			fc.utobj().printTestStep("Verify All The Options Link Listed under More Action Icon Of Top Stories");
			Actions builder21 = new Actions(driver);
			builder21.moveToElement(fc.utobj().getElement(driver, pobj.moreActions))
					.click(fc.utobj().getElement(driver, pobj.moreActions));
			builder21.perform();

			boolean isfolderPresent = fc.utobj().assertLinkText(driver, "Add Sub Folder");

			if (isfolderPresent == false) {
				fc.utobj().throwsException("Add Sub Folder Link is not present under More Action");
			}

			/*boolean isstoriesPresent = fc.utobj().assertLinkText(driver, "View All Stories");

			if (isstoriesPresent == false) {
				fc.utobj().throwsException("View All Stories Link is not present under More Action");
			}*/

			boolean isfolderSequencePresent = fc.utobj().assertLinkText(driver, "Define Sub Folder Sequence");

			if (isfolderSequencePresent == false) {
				fc.utobj().throwsException("Define Sub Folder Sequence Link is not present under More Action");
			}

			boolean isItemPresent = fc.utobj().assertLinkText(driver, "Define News Item Sequence");

			if (isItemPresent == false) {
				fc.utobj().throwsException("Define News Item Sequence Link is not present under More Action");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(groups = {"thehub"})
	@TestCase(createdOn = "2018-04-20", updatedOn = "2018-04-20", testCaseDescription = "To verify the news accessibility of folder and items for corporate user", testCaseId = "TC_Folder_Privilege_Corporate_01")
	private void corporatePrivilege01() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminTheHubNewsPage pobj = new AdminTheHubNewsPage(driver);
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporate_user = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "hubautomation@staffex.com";

			AdminUsersRolesAddNewRolePageTest roles_page=new AdminUsersRolesAddNewRolePageTest();
			
			fc.utobj().printTestStep("Add Corporate Role");
			String corRoleName = fc.utobj().generateTestData("TcCorNews");
			Roles roles=new Roles();
			roles.setRoleType("Corporate");
			roles.setRoleName(corRoleName);
			roles_page.addRoles(driver, roles);
			
			fc.utobj().printTestStep(
					"Navigate To Admin > Users > Manage Corporate Users > Add  Corporate User with added role");
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser.setRole(corRoleName);
			corpUser = corporate_user.createDefaultUser(driver, corpUser);
			
			fc.utobj().printTestStep("Add Divisional Role");
			String divRoleName = fc.utobj().generateTestData("TcDivNews");
			roles.setRoleType("Division");
			roles.setRoleName(divRoleName);
			roles_page.addRoles(driver, roles);
			
			fc.utobj().printTestStep("Add Divisional User");
			
			String divUserName=fc.utobj().generateTestData("Testdivuser");
			String divisionName=fc.utobj().generateTestData("Testdivname");
			String password="t0n1ght@123";
			AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest div_user=new AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest();
			div_user.addDivisionalUserWithRole(driver, divUserName, divisionName, divRoleName, password, emailId);
			
			fc.utobj().printTestStep("Add Regional Role");
			String regRoleName = fc.utobj().generateTestData("TcRegNews");
			roles.setRoleType("Regional");
			roles.setRoleName(regRoleName);
			roles_page.addRoles(driver, roles);
			
			fc.utobj().printTestStep("Add Regional User");
			String regUser=fc.utobj().generateTestData("Testreguser");
			String regionName=fc.utobj().generateTestData("Testregname");
			AdminUsersManageRegionalUsersAddRegionalUserPageTest regional_user=new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
			regional_user.addRegionalUserWithRole(driver, regUser, password, regionName, regRoleName, emailId);
			
			fc.utobj().printTestStep("Add Franchise Role");
			String franRoleName = fc.utobj().generateTestData("TcFranNews");
			roles.setRoleType("Franchise");
			roles.setRoleName(franRoleName);
			roles_page.addRoles(driver, roles);
			fc.utobj().printTestStep("Add Franchise User");
			
			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			AdminFranchiseLocationAddFranchiseLocationPageTest franchiseLocation = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String franchiseId = fc.utobj().generateTestData("Testfranid");
			String storeType = fc.utobj().generateTestData("Teststorytype");
			String franRegionName=fc.utobj().generateTestData("Testregname");
			franchiseLocation.addFranchiseLocation_All(driver, franchiseId, franRegionName, storeType);

			fc.utobj().printTestStep("Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest franchiseUser = new AdminUsersManageManageFranchiseUsersPageTest();
			String franUserName = fc.utobj().generateTestData("Testfuser");
			franchiseUser.addFranchiseUser(driver, franUserName, password, franchiseId, franRoleName, emailId);
			
			fc.utobj().printTestStep("Navigate To Admin > The Hub > News Page");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String accessTo = "Corporate";
			boolean addNewsItemFolder=true;	
			String newsItemTitle = fc.utobj().generateTestData(dataSet.get("newsItemTitle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String editorText = fc.utobj().generateTestData(dataSet.get("editorText"));
			
			News news=new News();
			news.setFolderName(folderName);
			news.setFolderSummary(folderSummary);
			news.setAccessTo(accessTo);
			news.setAddNewsItemFolder(addNewsItemFolder);
			news.setNewsItemTitle(newsItemTitle);
			news.setNewsBriefSummary(briefSummary);
			news.setNewsEditorText(editorText);
			
			fc.utobj().printTestStep("Add Folder With News Item");
			addFolder(driver,news);
			
			String subFolderName = fc.utobj().generateTestData(dataSet.get("subFolderName"));
			String subFolderSummary = fc.utobj().generateTestData(dataSet.get("subFolderSummary"));
			
			news.setSubFolderName(subFolderName);
			news.setSubFolderSummary(subFolderSummary);
			
			fc.utobj().printTestStep("Add SubFolder in Folder");
			addSubFolder(driver, news);
			
			fc.home_page().logout(driver);
			
			fc.utobj().printTestStep("Login By Newly Added Corporate User");
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());

			fc.utobj().printTestStep("Verify News Item should not be searchable by Admin > Search > News Search");
			new SearchTest().searchNewsItems(driver, news);
			
			boolean isTextPresent=fc.utobj().assertPageSource(driver, news.getNewsItemTitle());
			if (isTextPresent) {
				fc.utobj().throwsException("News Item Title is searchable by Admin > Search News");
			}
			
			fc.utobj().printTestStep("Verify News Item should not be searchable by The hub >  What's New > Search All Hub Items");
			fc.hub().hub_common().theHubWhatsNew(driver);
			
			fc.utobj().sendKeys(driver, new TheHubNewsPage(driver).searchAllDocument, newsItemTitle);
			fc.utobj().clickElement(driver, new TheHubNewsPage(driver).searchItem);

			boolean isWhatsNewsSearch = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text() , 'No Records Found.')]");
			if (isWhatsNewsSearch==false) {
				fc.utobj().throwsException("News Item is searchable by The hub >  What's New > Search All Hub Items even after not privilege");
			}
			
			fc.utobj().printTestStep("Navigate To The Hub > News Page");
			fc.hub().hub_common().theHubNewsSubModule(driver);
			
			fc.utobj().printTestStep("Verify the inaccessible news folder items on News Home page");
			fc.utobj().clickElement(driver, pobj.homeTab);
			
			boolean isNewsPresent=fc.utobj().assertPageSource(driver, newsItemTitle);
			if (isNewsPresent) {
				fc.utobj().throwsException("News Item is visible at News > Home Page");
			}
			
			fc.utobj().sendKeys(driver, new TheHubNewsPage(driver).searchAllDocument, newsItemTitle);
			fc.utobj().clickElement(driver, new TheHubNewsPage(driver).searchItem);

			boolean oiSearchByTopSearch = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text() , 'No Records Found.')]");
			if (oiSearchByTopSearch==false) {
				fc.utobj().throwsException("News Item is searchable by Search All Hub Items even after not privilege");
			}
			
			fc.utobj().sendKeys(driver, new SearchUI(driver).topSearchField, newsItemTitle);
			fc.utobj().clickEnterOnElement(driver, new SearchUI(driver).topSearchField);

			boolean isNewsItemAccessable = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[@id='searchResult']//*[contains(text () ,'"+newsItemTitle+"') and contains(text() , 'did not match any items')]");
			if (isNewsItemAccessable == false) {
				fc.utobj().throwsException("News Item Title is searchable by Solr even after not privilege");
			}
			
			fc.utobj().sendKeys(driver, new SearchUI(driver).topSearchField, subFolderName);
			fc.utobj().clickEnterOnElement(driver, new SearchUI(driver).topSearchField);

			boolean isFolderAccessable = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[@id='searchResult']//*[contains(text () ,'"+subFolderName+"') and contains(text() , 'did not match any items')]");
			if (isFolderAccessable == false) {
				fc.utobj().throwsException("Sub folder is searchable by Solr even after not privilege");
			}
			fc.utobj().refresh(driver);
			
			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With Divisional User");
			fc.loginpage().loginWithParameter(driver, divUserName, password);
			
			fc.utobj().printTestStep("Verify News Item should not be searchable by Admin > Search > News Search");
			new SearchTest().searchNewsItems(driver, news);
			
			boolean isDivTextPresent=fc.utobj().assertPageSource(driver, news.getNewsItemTitle());
			if (isDivTextPresent) {
				fc.utobj().throwsException("News Item Title is searchable by Admin > Search News");
			}
			
			fc.utobj().printTestStep("Verify News Item should not be searchable by The hub >  What's New > Search All Hub Items");
			fc.hub().hub_common().theHubWhatsNew(driver);
			
			fc.utobj().sendKeys(driver, new TheHubNewsPage(driver).searchAllDocument, newsItemTitle);
			fc.utobj().clickElement(driver, new TheHubNewsPage(driver).searchItem);

			boolean isDivWhatsNewsSearch = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text() , 'No Records Found.')]");
			if (isDivWhatsNewsSearch==false) {
				fc.utobj().throwsException("News Item is searchable by The hub >  What's New > Search All Hub Items even after not privilege");
			}
			
			fc.utobj().printTestStep("Navigate To The Hub > News Page");
			fc.hub().hub_common().theHubNewsSubModule(driver);
			
			fc.utobj().printTestStep("Verify the inaccessible news folder items on News Home page");
			fc.utobj().clickElement(driver, pobj.homeTab);
			
			boolean isDivNewsPresent=fc.utobj().assertPageSource(driver, newsItemTitle);
			if (isDivNewsPresent) {
				fc.utobj().throwsException("News Item is visible at News > Home Page");
			}
			
			fc.utobj().sendKeys(driver, new TheHubNewsPage(driver).searchAllDocument, newsItemTitle);
			fc.utobj().clickElement(driver, new TheHubNewsPage(driver).searchItem);

			boolean isDivSearchByTopSearch = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text() , 'No Records Found.')]");
			if (isDivSearchByTopSearch==false) {
				fc.utobj().throwsException("News Item is searchable by Search All Hub Items even after not privilege");
			}
			
			fc.utobj().sendKeys(driver, new SearchUI(driver).topSearchField, newsItemTitle);
			fc.utobj().clickEnterOnElement(driver, new SearchUI(driver).topSearchField);

			boolean isDivNewsItemAccessable = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[@id='searchResult']//*[contains(text () ,'"+newsItemTitle+"') and contains(text() , 'did not match any items')]");
			if (isDivNewsItemAccessable == false) {
				fc.utobj().throwsException("News Item Title is searchable by Solr even after not privilege");
			}
			
			fc.utobj().sendKeys(driver, new SearchUI(driver).topSearchField, subFolderName);
			fc.utobj().clickEnterOnElement(driver, new SearchUI(driver).topSearchField);

			boolean isDivFolderAccessable = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[@id='searchResult']//*[contains(text () ,'"+subFolderName+"') and contains(text() , 'did not match any items')]");
			if (isDivFolderAccessable == false) {
				fc.utobj().throwsException("Sub folder is searchable by Solr even after not privilege");
			}
			fc.utobj().refresh(driver);
			
			fc.home_page().logout(driver);
			
			fc.utobj().printTestStep("Login By Newly Added Regional User");
			fc.loginpage().loginWithParameter(driver, regUser,password);

			fc.utobj().printTestStep("Verify News Item should not be searchable by Admin > Search > News Search");
			new SearchTest().searchNewsItems(driver, news);
			
			boolean isRegTextPresent=fc.utobj().assertPageSource(driver, news.getNewsItemTitle());
			if (isRegTextPresent) {
				fc.utobj().throwsException("News Item Title is searchable by Admin > Search News");
			}
			
			fc.utobj().printTestStep("Verify News Item should not be searchable by The hub >  What's New > Search All Hub Items");
			fc.hub().hub_common().theHubWhatsNew(driver);
			
			fc.utobj().sendKeys(driver, new TheHubNewsPage(driver).searchAllDocument, newsItemTitle);
			fc.utobj().clickElement(driver, new TheHubNewsPage(driver).searchItem);

			boolean isRegWhatsNewsSearch = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text() , 'No Records Found.')]");
			if (isRegWhatsNewsSearch==false) {
				fc.utobj().throwsException("News Item is searchable by The hub >  What's New > Search All Hub Items even after not privilege");
			}
			
			fc.utobj().printTestStep("Navigate To The Hub > News Page");
			fc.hub().hub_common().theHubNewsSubModule(driver);
			
			fc.utobj().printTestStep("Verify the inaccessible news folder items on News Home page");
			fc.utobj().clickElement(driver, pobj.homeTab);
			
			boolean isRegNewsPresent=fc.utobj().assertPageSource(driver, newsItemTitle);
			if (isRegNewsPresent) {
				fc.utobj().throwsException("News Item is visible at News > Home Page");
			}
			
			fc.utobj().sendKeys(driver, new TheHubNewsPage(driver).searchAllDocument, newsItemTitle);
			fc.utobj().clickElement(driver, new TheHubNewsPage(driver).searchItem);

			boolean oiRegSearchByTopSearch = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text() , 'No Records Found.')]");
			if (oiRegSearchByTopSearch==false) {
				fc.utobj().throwsException("News Item is searchable by Search All Hub Items even after not privilege");
			}
			
			fc.utobj().sendKeys(driver, new SearchUI(driver).topSearchField, newsItemTitle);
			fc.utobj().clickEnterOnElement(driver, new SearchUI(driver).topSearchField);

			boolean isRegNewsItemAccessable = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[@id='searchResult']//*[contains(text () ,'"+newsItemTitle+"') and contains(text() , 'did not match any items')]");
			if (isRegNewsItemAccessable == false) {
				fc.utobj().throwsException("News Item Title is searchable by Solr even after not privilege");
			}
			
			fc.utobj().sendKeys(driver, new SearchUI(driver).topSearchField, subFolderName);
			fc.utobj().clickEnterOnElement(driver, new SearchUI(driver).topSearchField);

			boolean isRegFolderAccessable = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[@id='searchResult']//*[contains(text () ,'"+subFolderName+"') and contains(text() , 'did not match any items')]");
			if (isRegFolderAccessable == false) {
				fc.utobj().throwsException("Sub folder is searchable by Solr even after not privilege");
			}
			fc.utobj().refresh(driver);
			
			fc.home_page().logout(driver);
			
			fc.utobj().printTestStep("Login By Newly Added Franchise User");
			fc.loginpage().loginWithParameter(driver, franUserName,password);

			fc.utobj().printTestStep("Verify News Item should not be searchable by Admin > Search > News Search");
			new SearchTest().searchNewsItems(driver, news);
			
			boolean isFranTextPresent=fc.utobj().assertPageSource(driver, news.getNewsItemTitle());
			if (isFranTextPresent) {
				fc.utobj().throwsException("News Item Title is searchable by Admin > Search News");
			}
			
			fc.utobj().printTestStep("Verify News Item should not be searchable by The hub >  What's New > Search All Hub Items");
			fc.hub().hub_common().theHubWhatsNew(driver);
			
			fc.utobj().sendKeys(driver, new TheHubNewsPage(driver).searchAllDocument, newsItemTitle);
			fc.utobj().clickElement(driver, new TheHubNewsPage(driver).searchItem);

			boolean isFranWhatsNewsSearch = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text() , 'No Records Found.')]");
			if (isFranWhatsNewsSearch==false) {
				fc.utobj().throwsException("News Item is searchable by The hub >  What's New > Search All Hub Items even after not privilege");
			}
			
			fc.utobj().printTestStep("Navigate To The Hub > News Page");
			fc.hub().hub_common().theHubNewsSubModule(driver);
			
			fc.utobj().printTestStep("Verify the inaccessible news folder items on News Home page");
			fc.utobj().clickElement(driver, pobj.homeTab);
			
			boolean isFranNewsPresent=fc.utobj().assertPageSource(driver, newsItemTitle);
			if (isFranNewsPresent) {
				fc.utobj().throwsException("News Item is visible at News > Home Page");
			}
			
			fc.utobj().sendKeys(driver, new TheHubNewsPage(driver).searchAllDocument, newsItemTitle);
			fc.utobj().clickElement(driver, new TheHubNewsPage(driver).searchItem);

			boolean oiFranSearchByTopSearch = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text() , 'No Records Found.')]");
			if (oiFranSearchByTopSearch==false) {
				fc.utobj().throwsException("News Item is searchable by Search All Hub Items even after not privilege");
			}
			
			fc.utobj().sendKeys(driver, new SearchUI(driver).topSearchField, newsItemTitle);
			fc.utobj().clickEnterOnElement(driver, new SearchUI(driver).topSearchField);

			boolean isFranNewsItemAccessable = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[@id='searchResult']//*[contains(text () ,'"+newsItemTitle+"') and contains(text() , 'did not match any items')]");
			if (isFranNewsItemAccessable == false) {
				fc.utobj().throwsException("News Item Title is searchable by Solr even after not privilege");
			}
			
			fc.utobj().sendKeys(driver, new SearchUI(driver).topSearchField, subFolderName);
			fc.utobj().clickEnterOnElement(driver, new SearchUI(driver).topSearchField);

			boolean isFranFolderAccessable = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[@id='searchResult']//*[contains(text () ,'"+subFolderName+"') and contains(text() , 'did not match any items')]");
			if (isFranFolderAccessable == false) {
				fc.utobj().throwsException("Sub folder is searchable by Solr even after not privilege");
			}
			fc.utobj().refresh(driver);
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	void searchNewsByNewsItemTitle(WebDriver driver, News news) throws Exception{
		AdminTheHubNewsPage pobj = new AdminTheHubNewsPage(driver);
		fc.utobj().sendKeys(driver, pobj.searchNewsTextBx, news.getNewsItemTitle());
		fc.utobj().clickElement(driver, pobj.searchNewsImg);
	}
}
