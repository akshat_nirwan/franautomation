package com.builds.test.thehub;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminHiddenLinksConfigureNewHierarchyLevelPageTest;
import com.builds.uimaps.thehub.AdminTheHubRSSFeedsPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminTheHubRSSFeedsPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = {"thehub" ,"hub_rssfeed"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Add, modify and delete RSS Feeds At Admin > The Hub > RSS Feeds", testCaseId = "TC_16_Add_RSS_Feeds")
	private void addModifyDeleteRSSFeeds() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			AdminTheHubRSSFeedsPage pobj = new AdminTheHubRSSFeedsPage(driver);

			fc.utobj().printTestStep("Add RSS Feeds");

			fc.hub().hub_common().adminTheHubStoriesRSSFeedsandHomePageView(driver);
			fc.utobj().clickElement(driver, pobj.rssFeedsLink);
			String rssDesc = fc.utobj().generateTestData(dataSet.get("rssDesc"));
			// if same rss feeds available delete this
			boolean isRssFeedAvailable = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text (), '" + dataSet.get("rssFeedName") + "')]");

			if (isRssFeedAvailable == true) {

				fc.utobj().actionImgOption(driver, dataSet.get("rssFeedName"), "Delete");
				fc.utobj().acceptAlertBox(driver);
			}

			fc.utobj().clickElement(driver, pobj.addRSSFeedsLink);
			fc.utobj().sendKeys(driver, pobj.rssFeedName, dataSet.get("rssFeedName"));
			fc.utobj().sendKeys(driver, pobj.rssDesc, rssDesc);
			if (!fc.utobj().isSelected(driver,pobj.roleBase.get(0))) {
				fc.utobj().clickElement(driver, pobj.roleBase.get(0));
			}

			fc.utobj().clickElement(driver, pobj.submitBtn);

			fc.utobj().printTestStep("Verify The Add RSS");
			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + dataSet.get("rssFeedName") + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify RSSFeeds");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + rssDesc + "')]").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify RSSFeeds Description");
			}

			if (!fc.utobj().getElementByXpath(driver,
					".//*[contains(text () , '" + rssDesc + "')]/following-sibling::td[contains(text () ,'Inactive')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify RSSFeeds Status");
			}

			fc.utobj().printTestStep("Modify The RSS");
			fc.utobj().actionImgOption(driver, rssDesc, "Modify");
			fc.utobj().sendKeys(driver, pobj.rssFeedName, dataSet.get("rssFeedName"));
			rssDesc = fc.utobj().generateTestData(dataSet.get("rssDesc"));
			fc.utobj().sendKeys(driver, pobj.rssDesc, rssDesc);

			if (!fc.utobj().isSelected(driver,pobj.roleBase.get(0))) {
				fc.utobj().clickElement(driver, pobj.roleBase.get(0));
			}

			fc.utobj().clickElement(driver, pobj.submitBtn);

			fc.utobj().printTestStep("Verify The Modify RSS");
			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + dataSet.get("rssFeedName") + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify RSSFeeds Name After Modification");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + rssDesc + "')]").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify RSSFeeds Description After Modification");
			}

			if (!fc.utobj().getElementByXpath(driver,
					".//*[contains(text () , '" + rssDesc + "')]/following-sibling::td[contains(text () ,'Inactive')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify RSSFeeds Status After Modification");
			}

			fc.utobj().printTestStep("Delete RSS");
			fc.utobj().actionImgOption(driver, rssDesc, "Delete");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Delet RSS");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, rssDesc);
			if (isTextPresent == true) {
				fc.utobj().throwsException("was not able to verify delete RSS Feeds");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"thehub" ,"hub_rssfeed"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Add, modify and delete RSS Feeds with Corporate, Regional and Divisional Access At Admin > The Hub > RSS Feeds", testCaseId = "TC_17_Add_RSS_Feeds")
	private void addModifyDeleteRSSFeedsWithCorporateAccess() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			AdminTheHubRSSFeedsPage pobj = new AdminTheHubRSSFeedsPage(driver);

			AdminHiddenLinksConfigureNewHierarchyLevelPageTest configureHierarchyLevel = new AdminHiddenLinksConfigureNewHierarchyLevelPageTest();
			configureHierarchyLevel.ConfigureNewHierarchyLevel(driver);

			fc.utobj().printTestStep("Add RSS Feeds");
			fc.hub().hub_common().adminTheHubStoriesRSSFeedsandHomePageView(driver);
			fc.utobj().clickElement(driver, pobj.rssFeedsLink);

			// if same rss feeds available delete this
			boolean isRssFeedAvailable = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text (), '" + dataSet.get("rssFeedName") + "')]");

			if (isRssFeedAvailable == true) {

				fc.utobj().actionImgOption(driver, dataSet.get("rssFeedName"), "Delete");
				fc.utobj().acceptAlertBox(driver);
			}

			fc.utobj().clickElement(driver, pobj.addRSSFeedsLink);
			fc.utobj().sendKeys(driver, pobj.rssFeedName, dataSet.get("rssFeedName"));
			String rssDesc = fc.utobj().generateTestData(dataSet.get("rssDesc"));
			fc.utobj().sendKeys(driver, pobj.rssDesc, rssDesc);

			if (!fc.utobj().isSelected(driver,pobj.roleBase.get(1))) {
				fc.utobj().clickElement(driver, pobj.roleBase.get(1));
			}

			fc.utobj().setToDefault(driver, pobj.roles);
			fc.utobj().selectValFromMultiSelect(driver, pobj.roles, /*"Default Divisional Role"*/"Default Division Role");

			fc.utobj().clickElement(driver, pobj.submitBtn);

			fc.utobj().printTestStep("Verify The Add RSS");
			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + dataSet.get("rssFeedName") + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify RSSFeeds");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + rssDesc + "')]").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify RSSFeeds Description");
			}

			if (!fc.utobj().getElementByXpath(driver,
					".//*[contains(text () , '" + rssDesc + "')]/following-sibling::td[contains(text () ,'Inactive')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify RSSFeeds Status");
			}

			fc.utobj().printTestStep("Modify The RSS");
			fc.utobj().actionImgOption(driver, rssDesc, "Modify");
			fc.utobj().sendKeys(driver, pobj.rssFeedName, dataSet.get("rssFeedName"));
			rssDesc = fc.utobj().generateTestData(dataSet.get("rssDesc"));
			fc.utobj().sendKeys(driver, pobj.rssDesc, rssDesc);

			if (!fc.utobj().isSelected(driver,pobj.roleBase.get(1))) {
				fc.utobj().clickElement(driver, pobj.roleBase.get(1));
			}

			fc.utobj().selectValFromMultiSelect(driver, pobj.roles, " Default Regional Role");

			fc.utobj().clickElement(driver, pobj.submitBtn);

			fc.utobj().printTestStep("Verify The Modify RSS");
			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + dataSet.get("rssFeedName") + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify RSSFeeds Name After Modification");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + rssDesc + "')]").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify RSSFeeds Description After Modification");
			}

			if (!fc.utobj().getElementByXpath(driver,
					".//*[contains(text () , '" + rssDesc + "')]/following-sibling::td[contains(text () ,'Inactive')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify RSSFeeds Status After Modification");
			}

			fc.utobj().printTestStep("Delete RSS");
			fc.utobj().actionImgOption(driver, rssDesc, "Delete");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Delet RSS");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, rssDesc);
			if (isTextPresent == true) {
				fc.utobj().throwsException("was not able to verify delete RSS Feeds");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
}
