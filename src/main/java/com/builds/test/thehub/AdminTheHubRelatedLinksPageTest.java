package com.builds.test.thehub;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminHiddenLinksConfigureNewHierarchyLevelPageTest;
import com.builds.uimaps.thehub.AdminTheHubRelatedLinksPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminTheHubRelatedLinksPageTest {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = {"thehub" ,"hub_relatedLink"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Add New Link At Admin > The Hub > Related Links >  Add New Links", testCaseId = "TC_109_Add_New_Link_Access_All")
	public void addNewLinksAccessAll() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminTheHubRelatedLinksPage pobj = new AdminTheHubRelatedLinksPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Related Links Page");
			fc.utobj().printTestStep("Add New Link");
			fc.hub().hub_common().adminTheHubRelatedLinksPage(driver);
			fc.utobj().clickElement(driver, pobj.addNewLink);
			String text = fc.utobj().generateRandomNumber();
			String linkUrl = "www." + text + ".com";
			fc.utobj().sendKeys(driver, pobj.linkUrl, linkUrl);
			String linkTitle = fc.utobj().generateTestData(dataSet.get("linkTitle"));
			fc.utobj().sendKeys(driver, pobj.linkTitle, linkTitle);
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().sendKeys(driver, pobj.description, description);
			if (!fc.utobj().isSelected(driver,pobj.roleBaseAccessLink.get(1))) {
				fc.utobj().clickElement(driver, pobj.roleBaseAccessLink.get(1));
			}
			fc.utobj().clickElement(driver, pobj.addBtn);

			fc.utobj().printTestStep("Verify The Add New Link");
			fc.utobj().showAll(driver);
			fc.utobj().isTextDisplayed(driver, linkTitle, "Was not able to verify Title of the Link");
			
			/*WebElement element = fc.utobj().getElementByXpath(driver, ".//*[.='" + linkTitle
					+ "']/ancestor::tr/td[contains(text () , '" + fc.utobj().getCurrentDateUserDefineTimeZoneUSFormat("GMT-5:00")+ "')]");
			fc.utobj().moveToElement(driver, element);
			if (!element.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Added On Date");
			}*/
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addNewLink(WebDriver driver, String text, String linkTitle, String description, String accessTo)
			throws Exception {

		String testCaseId = "TC_Add_New_Link_TheHub_Related_Link";
		String linkUrl = null;

		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminTheHubRelatedLinksPage pobj = new AdminTheHubRelatedLinksPage(driver);
				fc.hub().hub_common().adminTheHubRelatedLinksPage(driver);
				fc.utobj().clickElement(driver, pobj.addNewLink);
				linkUrl = "www." + text + ".com";
				fc.utobj().sendKeys(driver, pobj.linkUrl, linkUrl);
				fc.utobj().sendKeys(driver, pobj.linkTitle, linkTitle);
				fc.utobj().sendKeys(driver, pobj.description, description);

				if (accessTo.equalsIgnoreCase("All")) {

					if (!fc.utobj().isSelected(driver,pobj.roleBaseAccessLink.get(1))) {
						fc.utobj().clickElement(driver, pobj.roleBaseAccessLink.get(1));
					}
				} else if (accessTo.equalsIgnoreCase("CorporateUsers")) {

					if (!fc.utobj().isSelected(driver,pobj.roleBaseAccessLink.get(0))) {
						fc.utobj().clickElement(driver, pobj.roleBaseAccessLink.get(0));
					}
					fc.utobj().clickElement(driver, pobj.defaultCorporateRoleCheckBox);
				} else if (accessTo.equalsIgnoreCase("DivisionalUsers")) {

					if (!fc.utobj().isSelected(driver,pobj.roleBaseAccessLink.get(0))) {
						fc.utobj().clickElement(driver, pobj.roleBaseAccessLink.get(0));
					}
					fc.utobj().clickElement(driver, pobj.defaultDivisionalRoleCheckBox);
				} else if (accessTo.equalsIgnoreCase("RegionalUsers")) {

					if (!fc.utobj().isSelected(driver,pobj.roleBaseAccessLink.get(0))) {
						fc.utobj().clickElement(driver, pobj.roleBaseAccessLink.get(0));
					}
					fc.utobj().clickElement(driver, pobj.defaultRegionalRoleCheckBox);
				} else if (accessTo.equalsIgnoreCase("FranchiseUsers")) {

					if (!fc.utobj().isSelected(driver,pobj.roleBaseAccessLink.get(0))) {
						fc.utobj().clickElement(driver, pobj.roleBaseAccessLink.get(0));
					}
					fc.utobj().clickElement(driver, pobj.defaultFranchiseRoleCheckBox);
				}

				fc.utobj().clickElement(driver, pobj.addBtn);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to add New Link");

		}
		return linkUrl;
	}

	@Test(groups = {"thehub" ,"hub_relatedLink"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add New Link Access To Corporate User", testCaseId = "TC_110_Add_New_Link_AccessToCU")
	private void addNewLinksAccessToCorporateUsers() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Related Links Page");
			String linkTitle = fc.utobj().generateTestData(dataSet.get("linkTitle"));
			String text = fc.utobj().generateRandomNumber();
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			String accessTo = "CorporateUsers";

			fc.utobj().printTestStep("Add New Link");
			addNewLink(driver, text, linkTitle, description, accessTo);

			fc.utobj().printTestStep("Verify The Add New Link");
			fc.utobj().showAll(driver);
			fc.utobj().isTextDisplayed(driver, linkTitle, "was not able to verify Title of the Link");
			
			
			/*WebElement element = fc.utobj().getElementByXpath(driver, ".//*[.='" + linkTitle
					+ "']/ancestor::tr/td[contains(text () , '" + fc.utobj().getCurrentDateUserDefineTimeZoneUSFormat("GMT-5:00") + "')]");
			fc.utobj().moveToElement(driver, element);
			if (!element.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Added On Date");
			}*/

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"thehub" ,"hub_relatedLink"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add New Link Access To Divisional Users At Admin > The Hub > Related Links >  Add New Links", testCaseId = "TC_111_Add_New_Link_AccessToDU")
	private void addNewLinksAccessToDivisionalUsers() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > Hidden Links > Configure New Hierarchy Level Page");
			AdminHiddenLinksConfigureNewHierarchyLevelPageTest level = new AdminHiddenLinksConfigureNewHierarchyLevelPageTest();
			level.ConfigureNewHierarchyLevel(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Related Links Page");
			fc.utobj().printTestStep("Add New Link");
			String linkTitle = fc.utobj().generateTestData(dataSet.get("linkTitle"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			String text = fc.utobj().generateRandomNumber();
			String accessTo = "DivisionalUsers";
			addNewLink(driver, text, linkTitle, description, accessTo);

			fc.utobj().printTestStep("Verify The Add New Link");
			fc.utobj().showAll(driver);
			fc.utobj().isTextDisplayed(driver, linkTitle, "was not able to verify Title of the Link");
			
			
			/*WebElement element = fc.utobj().getElementByXpath(driver, ".//*[.='" + linkTitle
					+ "']/ancestor::tr/td[contains(text () , '" + fc.utobj().getCurrentDateUserDefineTimeZoneUSFormat("GMT-5:00") + "')]");
			fc.utobj().moveToElement(driver, element);
			if (!element.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Added On Date");
			}*/

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"thehub" ,"hub_relatedLink"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add New Link Access To Regional Users At Admin > The Hub > Related Links >  Add New Links", testCaseId = "TC_112_Add_New_Link_AccessToRU")
	private void addNewLinksAccessToRegionalUsers() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Related Links Page");
			String linkTitle = fc.utobj().generateTestData(dataSet.get("linkTitle"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			String text = fc.utobj().generateRandomNumber();
			String accessTo = "RegionalUsers";

			fc.utobj().printTestStep("Add New Link");
			addNewLink(driver, text, linkTitle, description, accessTo);

			fc.utobj().printTestStep("Verify The Add New Link");
			fc.utobj().showAll(driver);
			fc.utobj().isTextDisplayed(driver, linkTitle, "was not able to verify Title of the Link");
			
			
			/*WebElement element = fc.utobj().getElementByXpath(driver, ".//*[.='" + linkTitle
					+ "']/ancestor::tr/td[contains(text () , '" + fc.utobj().getCurrentDateUserDefineTimeZoneUSFormat("GMT-5:00") + "')]");
			fc.utobj().moveToElement(driver, element);
			if (!element.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Added On Date");
			}*/

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"thehub" ,"hub_relatedLink"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Add New Link Access To Franchise Users At Admin > The Hub > Related Links >  Add New Links", testCaseId = "TC_113_Add_New_Link_AccessToFU")
	private void addNewLinksAccessToFranchiseUsers() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			String linkTitle = fc.utobj().generateTestData(dataSet.get("linkTitle"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			String text = fc.utobj().generateRandomNumber();
			String accessTo = "FranchiseUsers";

			fc.utobj().printTestStep("Add New Link");
			addNewLink(driver, text, linkTitle, description, accessTo);

			fc.utobj().printTestStep("Verify The Add New Link");
			fc.utobj().showAll(driver);
			fc.utobj().isTextDisplayed(driver, linkTitle, "was not able to verify Title of the Link");
			
			
			/*WebElement element = fc.utobj().getElementByXpath(driver, ".//*[.='" + linkTitle
					+ "']/ancestor::tr/td[contains(text () , '" + fc.utobj().getCurrentDateUserDefineTimeZoneUSFormat("GMT-5:00") + "')]");
			fc.utobj().moveToElement(driver, element);
			if (!element.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Added On Date");
			}*/

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"thehub" ,"hub_relatedLink"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Modify New Link Access To Franchise Users At Admin > The Hub > Related Links >  Add New Links", testCaseId = "TC_114_Modify_Links")
	private void modifyNewLinks() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminTheHubRelatedLinksPage pobj = new AdminTheHubRelatedLinksPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Related Links Page");
			fc.utobj().printTestStep("Add New Link");
			String linkTitle = fc.utobj().generateTestData(dataSet.get("linkTitle"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			String text = fc.utobj().generateRandomNumber();
			String accessTo = "FranchiseUsers";
			addNewLink(driver, text, linkTitle, description, accessTo);

			fc.utobj().printTestStep("Modify New Link");
			fc.utobj().showAll(driver);
			fc.utobj().actionImgOption(driver, linkTitle, "Modify");

			text = fc.utobj().generateRandomNumber();
			String linkUrl = "www." + text + ".com";
			fc.utobj().sendKeys(driver, pobj.linkUrl, linkUrl);
			linkTitle = fc.utobj().generateTestData(dataSet.get("linkTitle"));
			fc.utobj().sendKeys(driver, pobj.linkTitle, linkTitle);
			description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().sendKeys(driver, pobj.description, description);

			if (!fc.utobj().isSelected(driver,pobj.roleBaseAccessLink.get(1))) {
				fc.utobj().clickElement(driver, pobj.roleBaseAccessLink.get(1));
			}
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("Verify The Modify New Link");
			fc.utobj().showAll(driver);
			fc.utobj().isTextDisplayed(driver, linkTitle, "was not able to verify Title of the Link");
			
			/*WebElement element = fc.utobj().getElementByXpath(driver, ".//*[.='" + linkTitle
					+ "']/ancestor::tr/td[contains(text () , '" + fc.utobj().getCurrentDateUserDefineTimeZoneUSFormat("GMT-5:00") + "')]");
			fc.utobj().moveToElement(driver, element);
			if (!element.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Added On Date");
			}*/

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"thehub" ,"hub_relatedLink"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add New Link Access To Franchise Users At Admin > The Hub > Related Links >  Add New Links", testCaseId = "TC_115_Delete_Link")
	private void deleteNewLinks() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Related Links Page");
			fc.utobj().printTestStep("Add New Link");
			String linkTitle = fc.utobj().generateTestData(dataSet.get("linkTitle"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			String text = fc.utobj().generateRandomNumber();
			String accessTo = "FranchiseUsers";
			addNewLink(driver, text, linkTitle, description, accessTo);

			fc.utobj().printTestStep("Delete New Link");
			fc.utobj().showAll(driver);
			fc.utobj().actionImgOption(driver, linkTitle, "Delete");
			fc.utobj().acceptAlertBox(driver);
			fc.utobj().showAll(driver);

			fc.utobj().printTestStep("Verify The Delete New Link");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, linkTitle);
			if (isTextPresent == true) {
				fc.utobj().throwsException("was not able to delete Link");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
}
