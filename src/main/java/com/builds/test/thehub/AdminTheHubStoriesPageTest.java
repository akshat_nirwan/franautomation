package com.builds.test.thehub;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminHiddenLinksConfigureNewHierarchyLevelPageTest;
import com.builds.test.admin.AdminUsersRolesAddNewRolePageTest;
import com.builds.uimaps.thehub.AdminTheHubStoriesPage;
import com.builds.uimaps.thehub.TheHubHomePage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminTheHubStoriesPageTest {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "thehub" ,"hub_stories"})
	@TestCase(createdOn = "2017-01-16", updatedOn = "2018-01-16", testCaseDescription = "Verify The Add Story At Admin > The Hub > Stories", testCaseId = "TC_01_Add_Stories_Access_All")
	public void addStoriesAccessAll() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminTheHubStoriesPage pobj = new AdminTheHubStoriesPage(driver);

			String parentWindow = driver.getWindowHandle();
			fc.utobj().printTestStep("Navigate To Admin > The Hub > Stories Page");
			fc.hub().hub_common().adminTheHubStoriesRSSFeedsandHomePageView(driver);

			fc.utobj().printTestStep("Add Embed Video Type Stories");
			fc.utobj().clickElement(driver, pobj.storiesTab);
			fc.utobj().clickElement(driver, pobj.addStoriesLink);

			String title = fc.utobj().generateTestData(dataSet.get("title"));
			fc.utobj().sendKeys(driver, pobj.title, title);
			String summary = fc.utobj().generateTestData(dataSet.get("summary"));
			fc.utobj().sendKeys(driver, pobj.summary, summary);
			String curDate = fc.utobj().getCurrentDateUSFormat();
			fc.utobj().sendKeys(driver, pobj.startDate, curDate);

			String futureDate = fc.utobj().getFutureDateUSFormat(1);
			fc.utobj().sendKeys(driver, pobj.endDate, futureDate);

			if (!fc.utobj().isSelected(driver,pobj.storyAccessibleAll.get(0))) {
				fc.utobj().clickElement(driver, pobj.storyAccessibleAll.get(0));
			}
			fc.utobj().printTestStep(
					"Verify Alert message should be come to enter complete story under TinyMCe Editor after clicking over Preview and Save Btn");

			fc.utobj().printTestStep("Click over preview button and verify alert");
			fc.utobj().clickElement(driver, pobj.previewBtn);
			fc.utobj().sleep(2000);
			
			boolean isAlertPresent = false;
			String text = null;
			try {
				text = fc.utobj().acceptAlertBox(driver);
				isAlertPresent = true;
			} catch (Exception e) {
				isAlertPresent = false;
			}

			if (!isAlertPresent) {
				fc.utobj().throwsException("No alert present after clicking over preview button");
			}

			if (text == null || !text.equalsIgnoreCase("Please specify Complete Story.")) {
				fc.utobj().throwsException("Not able to verify Alert msg after clicking over preview button");
			}

			if (!fc.utobj().isSelected(driver,pobj.satisfiedWithPreview)) {
				fc.utobj().clickElement(driver, pobj.satisfiedWithPreview);
			}

			fc.utobj().printTestStep("Click over Save button and verify alert");
			fc.utobj().clickElementWithoutMove(driver, fc.utobj().getElement(driver, pobj.addBtn));
			fc.utobj().sleep(2000);
			
			boolean isAlertPresent1 = false;
			String text1 = null;
			try {
				text1 = fc.utobj().acceptAlertBox(driver);
				isAlertPresent1 = true;
			} catch (Exception e) {
				isAlertPresent1 = false;
			}

			if (!isAlertPresent1) {
				fc.utobj().throwsException("No alert present after clicking over preview button");
			}

			if (text1 == null || !text1.equalsIgnoreCase("Please specify Complete Story.")) {
				fc.utobj().throwsException("Not able to verify Alert msg after clicking over preview button");
			}

			if (!fc.utobj().isSelected(driver,pobj.satisfiedWithPreview)) {
				fc.utobj().clickElement(driver, pobj.satisfiedWithPreview);
			}

			fc.utobj().switchFrameById(driver, "ta_ifr");
			String editorText = fc.utobj().generateTestData(dataSet.get("editorText"));

			Actions actions = new Actions(driver);
			actions.moveToElement(fc.utobj().getElement(driver, pobj.editorTextArea));
			actions.click();
			actions.sendKeys(editorText);
			fc.utobj().logReportMsg("Entered Text", editorText);
			actions.build().perform();

			fc.utobj().switchFrameToDefault(driver);

			if (!fc.utobj().isSelected(driver,pobj.mediaType.get(2))) {
				fc.utobj().clickElement(driver, pobj.mediaType.get(2));
			}
			fc.utobj().sendKeys(driver, pobj.embedCode, dataSet.get("embedCode"));
			if (!fc.utobj().isSelected(driver,pobj.topStoryAppearsHomePage.get(0))) {
				fc.utobj().clickElement(driver, pobj.topStoryAppearsHomePage.get(0));
			}
			if (fc.utobj().isSelected(driver,pobj.satisfiedWithPreview)) {
				fc.utobj().clickElement(driver, pobj.satisfiedWithPreview);
			}
			fc.utobj().printTestStep("Verify Preview of story");
			fc.utobj().clickElement(driver, pobj.previewBtn);

			boolean isPreviewWindow = false;
			boolean isTitlePresent = false;
			Set<String> allWindow = driver.getWindowHandles();

			for (String cWindow : allWindow) {
				if (!cWindow.equalsIgnoreCase(parentWindow)) {
					driver.switchTo().window(cWindow);
					if (driver.getTitle().equalsIgnoreCase("Preview Story")) {
						isPreviewWindow = true;
						isTitlePresent = fc.utobj().assertPageSource(driver, title);
						fc.utobj().clickElementWithoutMove(driver, fc.utobj().getElement(driver, pobj.closeBtn));
						break;
					}
					driver.switchTo().window(parentWindow);
				}
			}
			driver.switchTo().window(parentWindow);
			if (!isPreviewWindow) {
				fc.utobj().throwsException("No Story Preview window available");
			}
			if (!isTitlePresent) {
				fc.utobj().throwsException("Not able to verify title in story preview window");
			}
			if (!fc.utobj().isSelected(driver,pobj.satisfiedWithPreview)) {
				fc.utobj().clickElement(driver, pobj.satisfiedWithPreview);
			}
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().showAll(driver);
			fc.utobj().printTestStep("Verify The Add Stories");
			WebElement element = fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + title + "')]");
			fc.utobj().moveToElement(driver, element);
			if (!element.isDisplayed()) {
				fc.utobj().throwsException("was not able to add Stories");
			}
			
			fc.utobj().printTestStep("Verify Added Story At Hub Home page");
			fc.hub().hub_common().theHubHome(driver);
			
			fc.utobj().printTestStep("Verify The Stories");
			boolean isStoryPresnt = false;
			boolean isSingleStoryPresent = false;
			try {
				List<WebElement> nextElement = driver
						.findElements(By.xpath(".//*[@id='currentStory' and contains(text(),'of')]"));

				String totalStory = null;
				if (nextElement.size() == 1) {
					totalStory = nextElement.get(0).getText();
					totalStory = totalStory.substring(totalStory.lastIndexOf("f") + 1);
					if (totalStory != null && totalStory.length() > 0) {
						totalStory = totalStory.trim();
					}
					for (int i = 0; i < Integer.parseInt(totalStory); i++) {
						fc.utobj().clickElement(driver,
								fc.utobj().getElement(driver, new TheHubHomePage(driver).nextStoryLnk));
						List<WebElement> elementFront = driver
								.findElements(By.xpath(".//*[contains(text(),'" + title + "')]"));
						if (elementFront.size() == 1) {
							isStoryPresnt = true;
							isSingleStoryPresent = false;
							break;
						}
					}
				} else {
					isSingleStoryPresent = true;
					fc.utobj().isTextDisplayed(driver, title, "was not able to verify title of Top Story");
					fc.utobj().isTextDisplayed(driver, editorText, "was not able to verify summary of Top Story");
				}
			} catch (Exception e) {
				
			}
			if (isStoryPresnt == false && isSingleStoryPresent == false) {
				fc.utobj().throwsException("Not able to verify title of Top Story");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void addStories(WebDriver driver, String title, String summary, String editorText, String accessTo,
			String roleName, String mediaFileType, Map<String, String> dataSet, String currentDate, String futureDate)
			throws Exception {

		String testCaseId = "TC_Add_Stories_TheHub_Stories_Page";

		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminTheHubStoriesPage pobj = new AdminTheHubStoriesPage(driver);

				fc.hub().hub_common().adminTheHubStoriesRSSFeedsandHomePageView(driver);

				fc.utobj().clickElement(driver, pobj.storiesTab);
				fc.utobj().clickElement(driver, pobj.addStoriesLink);

				fc.utobj().sendKeys(driver, pobj.title, title);
				fc.utobj().sendKeys(driver, pobj.summary, summary);
				fc.utobj().sendKeys(driver, pobj.startDate, currentDate);
				fc.utobj().sendKeys(driver, pobj.endDate, futureDate);

				if (accessTo.equalsIgnoreCase("All")) {

					if (!fc.utobj().isSelected(driver,pobj.storyAccessibleAll.get(0))) {
						fc.utobj().clickElement(driver, pobj.storyAccessibleAll.get(0));
					}
				} else if (accessTo.equalsIgnoreCase("CorporateUsers")) {

					if (!fc.utobj().isSelected(driver,pobj.storyAccessibleAll.get(1))) {
						fc.utobj().clickElement(driver, pobj.storyAccessibleAll.get(1));
					}
					try {
						fc.utobj().clickElement(driver, pobj.userSelect);
						String text = fc.utobj()
								.getElementByXpath(driver,
										".//label[contains(text(), 'Corporate Administrator')]/input")
								.getAttribute("disabled");

						if (!text.equalsIgnoreCase("true")) {
							fc.utobj().throwsException(
									"By default Corporate Administrator role is not as checked and disabled");
						}
						fc.utobj().clickElement(driver, pobj.userSelect);
					} catch (Exception e) {
						fc.utobj().throwsException(
								"By default Corporate Administrator role is not as checked and disabled");
					}

				} else if (accessTo.equalsIgnoreCase("DivisionalUsers")) {

					if (!fc.utobj().isSelected(driver,pobj.storyAccessibleAll.get(1))) {
						fc.utobj().clickElement(driver, pobj.storyAccessibleAll.get(1));
					}

					fc.utobj().clickElement(driver, pobj.userSelect);
					fc.utobj().sendKeys(driver, pobj.searchBoxSelectRole, roleName);
					fc.utobj().sleep(2500);
					fc.utobj().clickElement(driver, pobj.searchIcon);
					fc.utobj().sleep(2500);
					WebElement elemntDiv = fc.utobj().getElementByXpath(driver,
							".//*[@id='ms-parentrolesStory']//label[contains(text () ,'" + roleName + "')]/input");
					fc.utobj().clickElement(driver, elemntDiv);
					fc.utobj().clickElement(driver, pobj.userSelect);

				} else if (accessTo.equalsIgnoreCase("RegionalUsers")) {
					if (!fc.utobj().isSelected(driver,pobj.storyAccessibleAll.get(1))) {
						fc.utobj().clickElement(driver, pobj.storyAccessibleAll.get(1));
					}
					fc.utobj().clickElement(driver, pobj.userSelect);
					fc.utobj().sendKeys(driver, pobj.searchBoxSelectRole, roleName);
					fc.utobj().clickElement(driver, pobj.searchIcon);
					
					WebElement elemntDiv = fc.utobj().getElementByXpath(driver,
							".//*[@id='ms-parentrolesStory']//label[contains(text () ,'" + roleName + "')]/input");
					fc.utobj().clickElement(driver, elemntDiv);
					fc.utobj().clickElement(driver, pobj.userSelect);

				} else if (accessTo.equalsIgnoreCase("FranchiseUsers")) {

					if (!fc.utobj().isSelected(driver,pobj.storyAccessibleAll.get(1))) {
						fc.utobj().clickElement(driver, pobj.storyAccessibleAll.get(1));
					}

					fc.utobj().clickElement(driver, pobj.userSelect);
					fc.utobj().sendKeys(driver, pobj.searchBoxSelectRole, roleName);
					fc.utobj().clickElement(driver, pobj.searchIcon);
					
					WebElement elemntDiv = fc.utobj().getElementByXpath(driver,
							".//*[@id='ms-parentrolesStory']//label[contains(text () ,'" + roleName + "')]/input");
					fc.utobj().clickElement(driver, elemntDiv);
					fc.utobj().clickElement(driver, pobj.userSelect);

				} else if (accessTo.equalsIgnoreCase("Custom")) {
					if (!fc.utobj().isSelected(driver,pobj.storyAccessibleAll.get(1))) {
						fc.utobj().clickElement(driver, pobj.storyAccessibleAll.get(1));
					}

					fc.utobj().clickElement(driver, pobj.userSelect);
					fc.utobj().sendKeys(driver, pobj.searchBoxSelectRole, roleName);
					fc.utobj().clickElement(driver, pobj.searchIcon);
						
					WebElement elemntDiv = fc.utobj().getElementByXpath(driver,
							".//*[@id='ms-parentrolesStory']//label[contains(text () ,'" + roleName + "')]/input");
					fc.utobj().clickElement(driver, elemntDiv);
					fc.utobj().clickElement(driver, pobj.userSelect);
				}

				fc.utobj().switchFrameById(driver, "ta_ifr");

				Actions actions = new Actions(driver);
				actions.moveToElement(fc.utobj().getElement(driver, pobj.editorTextArea));
				actions.click();
				actions.sendKeys(editorText);
				fc.utobj().logReportMsg("Entered Text", editorText);
				actions.build().perform();
				fc.utobj().switchFrameToDefault(driver);
				System.out.println("---------------     " + mediaFileType);
				if (mediaFileType.equalsIgnoreCase("Image")) {

					if (!fc.utobj().isSelected(driver,pobj.mediaType.get(0))) {
						fc.utobj().clickElement(driver, pobj.mediaType.get(0));
					}
					if (!fc.utobj().isSelected(driver,pobj.selectedFile.get(0))) {
						fc.utobj().clickElement(driver, pobj.selectedFile.get(0));
					}
					String file = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));

					fc.utobj().sendKeys(driver, pobj.imageFile1, file);

				} else if (mediaFileType.equalsIgnoreCase("Video")) {

					if (!fc.utobj().isSelected(driver,pobj.mediaType.get(1))) {
						fc.utobj().clickElement(driver, pobj.mediaType.get(1));
					}
					String file = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
					fc.utobj().sendKeys(driver, pobj.videoFile, file);

				} else if (mediaFileType.equalsIgnoreCase("Embed Video")) {

					if (!fc.utobj().isSelected(driver,pobj.mediaType.get(2))) {
						fc.utobj().clickElement(driver, pobj.mediaType.get(2));
					}
					fc.utobj().sendKeys(driver, pobj.embedCode, dataSet.get("embedCode"));
				}

				if (!fc.utobj().isSelected(driver,pobj.topStoryAppearsHomePage.get(0))) {
					fc.utobj().clickElement(driver, pobj.topStoryAppearsHomePage.get(0));
				}
				if (!fc.utobj().isSelected(driver,pobj.satisfiedWithPreview)) {
					fc.utobj().clickElement(driver, pobj.satisfiedWithPreview);
				}
				fc.utobj().clickElement(driver, pobj.addBtn);
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to add Stories ");
		}
	}

	@Test(groups = { "thehub" ,"hub_stories"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add Story Accessibility to corporate user with media type Video At Admin > The Hub > Stories", testCaseId = "TC_02_Add_Stories_Access_Corporate_Users")
	private void addStoriesAccessToCorporatesUsers() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Stories Page");
			fc.utobj().printTestStep("Add Stories");
			String title = fc.utobj().generateTestData(dataSet.get("title"));
			String summary = /*
								 * fc.utobj().generateTestData(dataSet.get(
								 * "summary"));
								 */

					"A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart. "
							+ "I am alone, and feel the charm of existence in this spot, which was created for the bliss of souls like mine. I am so happy, my dear "
							+ "friend, so absorbed in the exquisite sense of mere tranquil existence, that I neglect my talents. I should be incapable of drawing a "
							+ "single stroke at the present moment; and yet I feel that I never was a greater artist than now. When, while the lovely valley teems "
							+ "with vapour around me, and the meridian sun strikes the upper surface of the impenetrable foliage of my trees, and but a few stray "
							+ "gleams steal into the inner sanctuary, I throw myself down among the tall grass by the trickling stream; and, as I lie close to the "
							+ "earth, a thousand unknown plants are noticed by me: when I hear the buzz of the little world among the stalks, and grow familiar with "
							+ "the countless indescribable forms of the insects and flies, then I feel the presence of the Almighty, who formed us in his own image, "
							+ "and the breath of that universal love which bears and sustains us, as it floats around us in an eternity of bliss; and then, my friend, "
							+ "when darkness overspreads my eyes, and heaven and earth seem to dwell in my soul and absorb its power, like the form of a beloved mistress, "
							+ "then I often think with longing, Oh, would I could describe these conceptions, could impress upon paper all that is living so full and warm "
							+ "within me, that it might be the mirror of my soul, as my soul is the mirror of the infinite God! O my friend -- but it is too much for my "
							+ "strength -- I sink under the weight of the splendour of these visions! A wonderful serenity has taken possession of my entire soul, like "
							+ "these sweet mornings of spring which I enjoy with my whole heart. I am alone, and feel the charm of existence in this spot, which was created "
							+ "for the bliss of souls like mine. I am so happy, my dear friend, so absorbed in the exquisite sense of mere tranquil existence, that I neglect "
							+ "my talents. I should be incapable of drawing a single stroke at the present moment; and yet I feel that I never was a greater artist than now. "
							+ "When, while the lovely valley teems with vapour around me, and the meridian sun strikes the upper surface of the impenetrable foliage of my trees, "
							+ "and but a few stray gleams steal into the inner sanctuary, I throw myself down among the tall grass by the trickling stream; and, as I lie close to "
							+ "the earth, a thousand unknown plants are noticed by me: when I hear the buzz of the little world among the stalks, and grow familiar with the";

			String editorText = fc.utobj().generateTestData(dataSet.get("editorText"));
			String accessTo = "CorporateUsers";
			String roleName = "";
			String mediaFile = "Video";

			String currentDate = fc.utobj().getCurrentDateUSFormat();
			String futureDate = fc.utobj().getFutureDateUSFormat(1);
			addStories(driver, title, summary, editorText, accessTo, roleName, mediaFile, dataSet, currentDate,
					futureDate);
			fc.utobj().showAll(driver);

			fc.utobj().printTestStep("Verify The Add Stories Titlle");
			WebElement element = fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + title + "')]");
			fc.utobj().moveToElement(driver, element);
			if (!element.isDisplayed()) {
				fc.utobj().throwsException("was not able to add Stories");
			}

			WebElement element1 = fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + title
					+ "')]/ancestor::tr[@class='bText12 grAltRw3' or @class='bText12']/td[contains(text () ,'FranConnect Administrator')]");
			fc.utobj().moveToElement(driver, element1);
			if (!element1.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Added By Column");
			}

			fc.utobj().printTestStep("Verify The Added Stories Summary");

			boolean isSummaryPresent = fc.utobj().assertPageSource(driver, summary);

			if (!isSummaryPresent) {
				fc.utobj().throwsException("Not able to verify the added summary");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"thehub","hub_stories"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add Story Accessibility to Divisional User with media file embed video At Admin > The Hub > Stories", testCaseId = "TC_03_Add_Stories_Access_Divisional_Users")
	private void addStoriesAccessToDivisionalUsers() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > Hidden Links Configure New Hierarchy Level Page");
			AdminHiddenLinksConfigureNewHierarchyLevelPageTest configureHierarchyLevel = new AdminHiddenLinksConfigureNewHierarchyLevelPageTest();
			configureHierarchyLevel.ConfigureNewHierarchyLevel(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Stories Page");
			fc.utobj().printTestStep("Add Stories");
			String title = fc.utobj().generateTestData(dataSet.get("title"));
			String summary = fc.utobj().generateTestData(dataSet.get("summary"));
			String editorText = fc.utobj().generateTestData(dataSet.get("editorText"));
			String accessTo = "DivisionalUsers";
			String roleName = "Default Division Role";
			String mediaFile = "Embed Video";
			String currentDate = fc.utobj().getCurrentDateUSFormat();
			String futureDate = fc.utobj().getFutureDateUSFormat(1);
			addStories(driver, title, summary, editorText, accessTo, roleName, mediaFile, dataSet, currentDate,
					futureDate);
			fc.utobj().showAll(driver);

			fc.utobj().printTestStep("Verify The Add Stories At Admin > Hub > Stories");
			WebElement element = fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + title + "')]");
			fc.utobj().moveToElement(driver, element);
			if (!element.isDisplayed()) {
				fc.utobj().throwsException("was not able to add Stories");
			}

			WebElement element1 = fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + title
					+ "')]/ancestor::tr[@class='bText12 grAltRw3' or @class='bText12']/td[contains(text () ,'FranConnect Administrator')]");
			fc.utobj().moveToElement(driver, element1);
			if (!element1.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Added By Column");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub","hub_stories"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add Story with accessibility to Regional Users and with option Image Upload At Admin > The Hub > Stories", testCaseId = "TC_04_Add_Stories_Access_Regional_Users")
	private void addStoriesAccessToRegionalUsers() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminTheHubStoriesPage pobj = new AdminTheHubStoriesPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Stories Page");
			fc.utobj().printTestStep("Add Stories");
			String title = fc.utobj().generateTestData(dataSet.get("title"));
			String summary = fc.utobj().generateTestData(dataSet.get("summary"));
			String editorText = fc.utobj().generateTestData(dataSet.get("editorText"));
			String accessTo = "RegionalUsers";
			String roleName = "Default Regional Role";
			String mediaFile = "Image";
			String futureDate = fc.utobj().getFutureDateUSFormat(1);

			addStories(driver, title, summary, editorText, accessTo, roleName, mediaFile, dataSet, "", futureDate);
			fc.utobj().sleep(2000);
			fc.utobj().printTestStep(
					"Verify Proper alert message should be displayed to enter complete story while Fill the details except start display date");

			boolean isAlertPresent = false;
			String text="";
			try {
				text = fc.utobj().acceptAlertBox(driver);
				isAlertPresent = true;
			} catch (Exception e) {
				isAlertPresent = false;
			}
			if (!isAlertPresent) {
				fc.utobj().throwsException("No alert is present");
			}
			
			if (text.isEmpty() && !text.equalsIgnoreCase("Please specify Start Date.")) {
				fc.utobj().throwsException("Alert text not verified");
			}

			String currentDate = fc.utobj().getCurrentDateUSFormat();
			fc.utobj().sendKeys(driver, pobj.startDate, currentDate);
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().showAll(driver);

			fc.utobj().printTestStep("Verify The Add Stories");
			WebElement element = fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + title + "')]");
			fc.utobj().moveToElement(driver, element);
			if (!element.isDisplayed()) {
				fc.utobj().throwsException("was not able to add Stories");
			}

			WebElement element1 = fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + title
					+ "')]/ancestor::tr[@class='bText12 grAltRw3' or @class='bText12']/td[contains(text () ,'FranConnect Administrator')]");
			fc.utobj().moveToElement(driver, element1);
			if (!element1.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Added By Column");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"thehub","hub_stories"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add Story with accessibility to Franchise Users and with option Video .flv upload At Admin > The Hub > Stories", testCaseId = "TC_05_Add_Stories_Access_Franchise_Users")
	private void addStoriesAccessToFranchiseeUsers() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Admin > The Hub > Stories Page");
			fc.utobj().printTestStep("Add Stories");
			String title = fc.utobj().generateTestData(dataSet.get("title"));
			String summary = fc.utobj().generateTestData(dataSet.get("summary"));
			String editorText = fc.utobj().generateTestData(dataSet.get("editorText"));
			String accessTo = "FranchiseUsers";
			String roleName = "Default Franchise Role";
			String mediaFile = "Video";
			String currentDate = fc.utobj().getCurrentDateUSFormat();
			String futureDate = fc.utobj().getFutureDateUSFormat(1);
			addStories(driver, title, summary, editorText, accessTo, roleName, mediaFile, dataSet, currentDate,
					futureDate);
			fc.utobj().showAll(driver);

			fc.utobj().printTestStep("Verify The Add Stories");
			WebElement element = fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + title + "')]");
			fc.utobj().moveToElement(driver, element);
			if (!element.isDisplayed()) {
				fc.utobj().throwsException("was not able to add Stories");
			}

			WebElement element1 = fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + title
					+ "')]/ancestor::tr[@class='bText12 grAltRw3' or @class='bText12']/td[contains(text () ,'FranConnect Administrator')]");
			fc.utobj().moveToElement(driver, element1);
			if (!element1.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Added By Column");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"thehub","hub_stories"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Preview Of Story At Admin > The Hub > Stories", testCaseId = "TC_06_Preview_Stories_Access_All")
	private void previewStoriesAccessToAll() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			AdminTheHubStoriesPage pobj = new AdminTheHubStoriesPage(driver);
			String parentWindowHandle = driver.getWindowHandle();

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Stories Page");
			fc.utobj().printTestStep("Add Stories");
			fc.hub().hub_common().adminTheHubStoriesRSSFeedsandHomePageView(driver);

			fc.utobj().clickElement(driver, pobj.storiesTab);
			fc.utobj().clickElement(driver, pobj.addStoriesLink);

			String title = fc.utobj().generateTestData(dataSet.get("title"));
			fc.utobj().sendKeys(driver, pobj.title, title);
			String summary = fc.utobj().generateTestData(dataSet.get("summary"));
			fc.utobj().sendKeys(driver, pobj.summary, summary);
			String curDate = fc.utobj().getCurrentDateUSFormat();
			fc.utobj().sendKeys(driver, pobj.startDate, curDate);
			String futureDate = fc.utobj().getFutureDateUSFormat(1);
			fc.utobj().sendKeys(driver, pobj.endDate, futureDate);
			if (!fc.utobj().isSelected(driver,pobj.storyAccessibleAll.get(0))) {
				fc.utobj().clickElement(driver, pobj.storyAccessibleAll.get(0));
			}
			fc.utobj().switchFrameById(driver, "ta_ifr");
			String editorText = fc.utobj().generateTestData(dataSet.get("editorText"));

			Actions actions = new Actions(driver);
			actions.moveToElement(fc.utobj().getElement(driver, pobj.editorTextArea));
			actions.click();
			actions.sendKeys(editorText);
			fc.utobj().logReportMsg("Entered Text", editorText);
			actions.build().perform();

			fc.utobj().switchFrameToDefault(driver);
			if (!fc.utobj().isSelected(driver,pobj.mediaType.get(0))) {
				fc.utobj().clickElement(driver, pobj.mediaType.get(0));
			}
			if (!fc.utobj().isSelected(driver,pobj.selectedFile.get(0))) {
				fc.utobj().clickElement(driver, pobj.selectedFile.get(0));
			}
			String file = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));

			fc.utobj().sendKeys(driver, pobj.imageFile1, file);
			if (!fc.utobj().isSelected(driver,pobj.topStoryAppearsHomePage.get(0))) {
				fc.utobj().clickElement(driver, pobj.topStoryAppearsHomePage.get(0));
			}

			fc.utobj().printTestStep("Verify Preview Of Stories");
			fc.utobj().clickElement(driver, pobj.previewBtn);

			Set<String> allWindowsHandles = driver.getWindowHandles();

			for (String currentWindowHandle : allWindowsHandles) {

				if (!currentWindowHandle.equals(parentWindowHandle)) {

					driver.switchTo().window(currentWindowHandle);

					try {

						fc.utobj().getElementByXpath(driver, ".//td[contains(text () ,'" + title + "')]").isDisplayed();

						if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () ,'" + title + "')]")
								.isDisplayed()) {
							fc.utobj().throwsException("was not able to verify title of the story");
						}

						if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () ,'" + editorText + "')]")
								.isDisplayed()) {
							fc.utobj().throwsException("was not able to verify Editor Text");
						}

					} catch (Exception e) {
						driver.close();
					}
					driver.switchTo().window(parentWindowHandle);
				}
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub","hub_stories"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Archive Story  At Admin > The Hub > Stories", testCaseId = "TC_07_Archive_Stories")
	private void archiveStoriesActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminTheHubStoriesPage pobj = new AdminTheHubStoriesPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Stories Page");
			fc.utobj().printTestStep("Add Stories");
			String title = fc.utobj().generateTestData(dataSet.get("title"));
			String summary = fc.utobj().generateTestData(dataSet.get("summary"));
			String editorText = fc.utobj().generateTestData(dataSet.get("editorText"));
			String accessTo = "All";
			String roleName = "";
			String mediaFile = "Image";
			String currentDate = fc.utobj().getCurrentDateUSFormat();
			String futureDate = fc.utobj().getFutureDateUSFormat(-5);
			addStories(driver, title, summary, editorText, accessTo, roleName, mediaFile, dataSet, currentDate,
					futureDate);

			fc.utobj().printTestStep(
					"Verify Proper alert message should be displayed that Expiration date can not be before Start date while Fiil the expiration date before start date");
			boolean isAlertPresent = false;
			String text="";
			try {
				text = fc.utobj().acceptAlertBox(driver);
				isAlertPresent = true;
			} catch (Exception e) {
				isAlertPresent = false;
			}
			
			if (isAlertPresent == false) {
				fc.utobj().throwsException("No alert is present");
			}
			
			if (text.isEmpty() && !text.equalsIgnoreCase(
					fc.utobj().translateString("Start Date cannot be greater than Expiration Date."))) {
				fc.utobj().throwsException("Alert text is not verified");
			}
			

			futureDate = fc.utobj().getFutureDateUSFormat(1);

			fc.utobj().sendKeys(driver, pobj.endDate, futureDate);
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().showAll(driver);

			fc.utobj().printTestStep("Archive Story");
			fc.utobj().actionImgOption(driver, title, "Archive");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().showAll(driver);

			fc.utobj().printTestStep("Verify Archive Story");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, title);
			if (isTextPresent == true) {
				fc.utobj().throwsException("was not able to archive story :: At story Page Verification");
			}

			fc.utobj().clickElement(driver, pobj.archivedStoriesLink);
			fc.utobj().showAll(driver);

			boolean isTextPresent1 = fc.utobj().assertPageSource(driver, title);
			if (isTextPresent1 == false) {
				fc.utobj().throwsException("was not able to archive story :: at Archived page verification");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub","hub_stories"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Delete Stories with option video upload  At Admin > The Hub > Stories", testCaseId = "TC_08_Delete_Stories")
	private void deleteStoriesActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminTheHubStoriesPage pobj = new AdminTheHubStoriesPage(driver);
			fc.utobj().printTestStep("Navigate To Admin > The Hub > Stories Page");
			fc.utobj().printTestStep("Add Story");
			String title = fc.utobj().generateTestData(dataSet.get("title"));
			String summary = fc.utobj().generateTestData(dataSet.get("summary"));
			String editorText = fc.utobj().generateTestData(dataSet.get("editorText"));
			String accessTo = "All";
			String roleName = "";
			String mediaFile = "Video";
			String currentDate = fc.utobj().getCurrentDateUSFormat();
			addStories(driver, title, summary, editorText, accessTo, roleName, mediaFile, dataSet, currentDate, "");

			fc.utobj().printTestStep(
					"Verify Proper alert message should be displayed to Expiration date date of story while Fill the details except Expiration date");
			fc.utobj().sleep(2500);
			boolean isAlertPresent = false;
			String text="";
			try {
				text = fc.utobj().acceptAlertBox(driver);
				isAlertPresent = true;
			} catch (Exception e) {
				isAlertPresent = false;
			}
			if (isAlertPresent == false) {
				fc.utobj().throwsException("No alert is present");
			}
			
			if (text.isEmpty() && !text.equalsIgnoreCase(fc.utobj().translateString("Please specify Expiration Date."))) {
				fc.utobj().throwsException("Alert text not verified");
			}

			String futureDate = fc.utobj().getFutureDateUSFormat(1);
			fc.utobj().sendKeys(driver, pobj.endDate, futureDate);
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().showAll(driver);

			fc.utobj().printTestStep("Delete Story");
			fc.utobj().actionImgOption(driver, title, "Delete");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().showAll(driver);

			fc.utobj().printTestStep("Verify The Delete Story");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, title);
			if (isTextPresent == true) {
				fc.utobj().throwsException("was not able to verify delete story");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub","hubfailed0904_1","hub_stories"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-16", testCaseDescription = "Modify Stories with option video upload  At Admin > The Hub > Stories", testCaseId = "TC_09_Modify_Stories")
	private void modifyStoriesActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Create New Corporate Role");
			AdminUsersRolesAddNewRolePageTest role_page = new AdminUsersRolesAddNewRolePageTest();
			String roleName = fc.utobj().generateTestData("TestCRole");
			role_page.addCorporateRoles(driver, roleName);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Stories Page");
			fc.utobj().printTestStep("Add Story");
			AdminTheHubStoriesPage pobj = new AdminTheHubStoriesPage(driver);
			String title = fc.utobj().generateTestData(dataSet.get("title"));
			String summary = fc.utobj().generateTestData(dataSet.get("summary"));
			String editorText = fc.utobj().generateTestData(dataSet.get("editorText"));
			String accessTo = "Custom";
			String mediaFile = "Video";
			String currentDate = fc.utobj().getCurrentDateUSFormat();
			String futureDate = fc.utobj().getFutureDateUSFormat(1);
			addStories(driver, title, summary, editorText, accessTo, roleName, mediaFile, dataSet, currentDate,
					futureDate);
			fc.utobj().showAll(driver);
			fc.utobj().printTestStep("Modify Story");
			fc.utobj().actionImgOption(driver, title, "Modify");
			fc.utobj().printTestStep(
					"Verify Only selected Role should be appeared under Do you want to make this Story accessible to all while modifying story");
			String text = fc.utobj().getText(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='ms-parentrolesStory']/button/span"));

			if (!text.contains(roleName)) {
				fc.utobj().throwsException(
						"Only selected Role is not appearing under Do you want to make this Story accessible to all while modifying story");
			}
			
			/*Actions actions = new Actions(driver);
			title = fc.utobj().generateTestData(dataSet.get("title"));
			fc.utobj().clipBoardData(title);
			actions.moveToElement(pobj.title).build().perform();
			pobj.title.click();
			pobj.title.clear();
			pobj.title.sendKeys(Keys.chord(Keys.CONTROL, "v"), "");
			actions.keyUp(Keys.CONTROL);*/

			fc.utobj().printTestStep("Enter Title");
			title = fc.utobj().generateTestData(dataSet.get("title"));
			fc.utobj().sendKeys(driver, pobj.title, title);
			
			
			/*summary = fc.utobj().generateTestData(dataSet.get("summary"));
			fc.utobj().clipBoardData(summary);
			actions.moveToElement(pobj.summary).build().perform();
			pobj.summary.click();
			pobj.summary.clear();
			pobj.summary.sendKeys(Keys.chord(Keys.CONTROL, "v"), "");
			actions.keyUp(Keys.CONTROL);*/

			fc.utobj().printTestStep("Enter summary of story");
			summary = fc.utobj().generateTestData(dataSet.get("summary"));
			fc.utobj().sendKeys(driver, pobj.summary, summary);
			
			String currentDate1 = fc.utobj().getCurrentDateUSFormat();
			fc.utobj().sendKeys(driver, pobj.startDate, currentDate1);
			String futureDate1 = fc.utobj().getFutureDateUSFormat(1);
			fc.utobj().sendKeys(driver, pobj.endDate, futureDate1);
			if (!fc.utobj().isSelected(driver,pobj.storyAccessibleAll.get(0))) {
				fc.utobj().clickElement(driver, pobj.storyAccessibleAll.get(0));
			}
			fc.utobj().switchFrameById(driver, "ta_ifr");
			String editorText1 = fc.utobj().generateTestData(dataSet.get("editorText"));
			
			Actions actions=new Actions(driver);
			actions.moveToElement(fc.utobj().getElement(driver, pobj.editorTextArea));
			actions.click();
			pobj.editorTextArea.clear();
			actions.sendKeys(editorText1);
			actions.build().perform();
			
			fc.utobj().switchFrameToDefault(driver);
			if (!fc.utobj().isSelected(driver,pobj.mediaType.get(1))) {
				fc.utobj().clickElement(driver, pobj.mediaType.get(1));
			}
			if (!fc.utobj().isSelected(driver,pobj.selectedFile.get(0))) {
				fc.utobj().clickElement(driver, pobj.selectedFile.get(0));
			}
			String file = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));

			fc.utobj().sendKeys(driver, pobj.imageFile1, file);
			if (!fc.utobj().isSelected(driver,pobj.topStoryAppearsHomePage.get(0))) {
				fc.utobj().clickElement(driver, pobj.topStoryAppearsHomePage.get(0));
			}

			fc.utobj().printTestStep("Verify The Title and Full Story At Preview Window");

			if (fc.utobj().isSelected(driver,pobj.satisfiedWithPreview)) {
				fc.utobj().clickElement(driver, pobj.satisfiedWithPreview);
			}

			String parentWindow = driver.getWindowHandle();
			fc.utobj().clickElement(driver, pobj.previewBtn);
			
			boolean isPreviewWindow = false;
			boolean isTitlePresent = false;
			boolean isStoryPresent = false;
			Set<String> allWindow = driver.getWindowHandles();

			for (String cWindow : allWindow) {
				if (!cWindow.equalsIgnoreCase(parentWindow)) {
					driver.switchTo().window(cWindow);
					if (driver.getTitle().equalsIgnoreCase("Preview Story")) {
						isPreviewWindow = true;
						isTitlePresent = fc.utobj().assertPageSource(driver, title);
						isStoryPresent = fc.utobj().assertPageSource(driver, editorText1);
						fc.utobj().clickElementWithoutMove(driver, fc.utobj().getElement(driver, pobj.closeBtn));
						break;
					}
					driver.switchTo().window(parentWindow);
				}
			}

			driver.switchTo().window(parentWindow);
			if (!isPreviewWindow) {
				fc.utobj().throwsException("No Story Preview window available");
			}

			if (!isTitlePresent) {
				fc.utobj().throwsException("Not able to verify title in story preview window");
			}

			if (!isStoryPresent) {
				fc.utobj().throwsException("Not able to verify Editor Text in story preview window");
			}

			if (!fc.utobj().isSelected(driver,pobj.satisfiedWithPreview)) {
				fc.utobj().clickElement(driver, pobj.satisfiedWithPreview);
			}

			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().showAll(driver);

			fc.utobj().printTestStep("Verify The Modify Story");
			WebElement element = fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + title + "')]");
			fc.utobj().moveToElement(driver, element);
			if (!element.isDisplayed()) {
				fc.utobj().throwsException("was not able to add Stories");
			}

			fc.utobj().printTestStep("Verify The Modified Summary And Html Editor Text");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, summary);

			if (!isTextPresent) {
				fc.utobj().throwsException("Not able to verify the summary of story at story page");
			}

			fc.utobj().clickElement(driver, element);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			boolean isStrySummaryPresentCbox = fc.utobj().assertPageSource(driver, summary);
			boolean isStryHtmlTextPresentCbox = fc.utobj().assertPageSource(driver, editorText1);
			boolean isStryTitlePresentCbox = fc.utobj().assertPageSource(driver, title);

			if (!isStrySummaryPresentCbox) {
				fc.utobj().throwsException("Not able to verify the summary of story at summary cbox");
			}

			if (!isStryHtmlTextPresentCbox) {
				fc.utobj().throwsException("Not able to verify the Editor Text of story at summary cbox");
			}

			if (!isStryTitlePresentCbox) {
				fc.utobj().throwsException("Not able to verify the Title of story at summary cbox");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			boolean isTitlePresent12 = fc.utobj().assertPageSource(driver, title);
			if (!isTitlePresent12) {
				fc.utobj().throwsException(
						"Not able to verify Window should get closed after click over close button at cbox");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"thehub","hub_stories"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Delete Stories with option Embed Code At Admin > The Hub > Stories", testCaseId = "TC_10_Delete_Story")
	private void deleteStoriesBottomBtn() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminTheHubStoriesPage pobj = new AdminTheHubStoriesPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Stories Page");
			fc.utobj().printTestStep("Add Story");
			String title = fc.utobj().generateTestData(dataSet.get("title"));
			String summary = fc.utobj().generateTestData(dataSet.get("summary"));
			String editorText = fc.utobj().generateTestData(dataSet.get("editorText"));
			String accessTo = "All";
			String roleName = "";
			String mediaFile = "Embed Code";
			String currentDate = fc.utobj().getCurrentDateUSFormat();
			String futureDate = fc.utobj().getFutureDateUSFormat(1);
			addStories(driver, title, summary, editorText, accessTo, roleName, mediaFile, dataSet, currentDate,
					futureDate);
			fc.utobj().showAll(driver);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[contains(text () ,'" + title + "')]/ancestor::tr/td/input"));

			fc.utobj().printTestStep("Delete Story");
			fc.utobj().clickElement(driver, pobj.deleteBottomBtn);
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().showAll(driver);

			fc.utobj().printTestStep("Verify The Delete Story");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, title);
			if (isTextPresent == true) {
				fc.utobj().throwsException("was not able to delete story from bottom btn");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub","hub_stories"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Archive Story By Botton Button with Image Upload  At Admin > The Hub > Stories", testCaseId = "TC_11_Archive_Story")
	private void archiveStoriesBottomBtn() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminTheHubStoriesPage pobj = new AdminTheHubStoriesPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Stories Page");
			fc.utobj().printTestStep("Add Story");
			String title = fc.utobj().generateTestData(dataSet.get("title"));
			String summary = fc.utobj().generateTestData(dataSet.get("summary"));

			String editorText = fc.utobj().generateTestData(dataSet.get("editorText"));
			String accessTo = "All";
			String roleName = "";
			String mediaFile = "Image";
			String currentDate = fc.utobj().getCurrentDateUSFormat();
			String futureDate = fc.utobj().getFutureDateUSFormat(1);
			addStories(driver, title, "", editorText, accessTo, roleName, mediaFile, dataSet, currentDate, futureDate);

			fc.utobj().printTestStep(
					"Verify Proper alert message should be displayed to enter summary of story while Fill the details except summary ");
			fc.utobj().sleep(2500);
			boolean isAlertPresent = false;
			String text="";
			try {
				text = fc.utobj().acceptAlertBox(driver);
				isAlertPresent = true;
			} catch (Exception e) {
				isAlertPresent = false;
			}
			if (!isAlertPresent) {
				fc.utobj().throwsException("No alert is present");
			}
			if (text.isEmpty() && !text.equalsIgnoreCase(fc.utobj().translateString("Please specify Summary."))) {
				fc.utobj().throwsException("Alert text not verified");
			}

			fc.utobj().sendKeys(driver, pobj.summary, summary);
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().showAll(driver);

			fc.utobj().printTestStep("Archive Story");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[contains(text () ,'" + title + "')]/ancestor::tr/td/input"));
			fc.utobj().clickElement(driver, pobj.archiveBottomBtn);
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().showAll(driver);

			fc.utobj().printTestStep("Verify Archive Story");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, title);
			if (isTextPresent == true) {
				fc.utobj().throwsException("was not able to archive story :: At Story Page Verification");
			}

			fc.utobj().clickElement(driver, pobj.archivedStoriesLink);
			fc.utobj().showAll(driver);

			boolean isTextPresent1 = fc.utobj().assertPageSource(driver, title);
			if (isTextPresent1 == false) {
				fc.utobj().throwsException("was not able to archive story :: at Archived page verification");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub","hub_stories"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Restore Story By Action Img Icon with Image Upload Option At Admin > The Hub > Stories > Archived Stories", testCaseId = "TC_12_Restore_Story")
	private void restoreStoriesActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminTheHubStoriesPage pobj = new AdminTheHubStoriesPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Stories Page");
			fc.utobj().printTestStep("Add Story");
			String title = fc.utobj().generateTestData(dataSet.get("title"));
			String summary = fc.utobj().generateTestData(dataSet.get("summary"));
			String editorText = fc.utobj().generateTestData(dataSet.get("editorText"));
			String accessTo = "All";
			String roleName = "";
			String mediaFile = "Image";
			String currentDate = fc.utobj().getCurrentDateUSFormat();
			String futureDate = fc.utobj().getFutureDateUSFormat(1);
			addStories(driver, "", summary, editorText, accessTo, roleName, mediaFile, dataSet, currentDate,
					futureDate);

			fc.utobj().printTestStep(
					"Verify Proper message should be displayed to enter Title of story while Fill the details except Title");

			boolean isAlertPresent = false;
			String text="";
			try {
				text = fc.utobj().acceptAlertBox(driver);
				isAlertPresent = true;
			} catch (Exception e) {
				isAlertPresent = false;
			}
			if (isAlertPresent == false) {
				fc.utobj().throwsException("No alert is present");
			}

			if (text.isEmpty() && !text.equalsIgnoreCase(fc.utobj().translateString("Please specify Title."))) {
				fc.utobj().throwsException("Alert text not verified");
			}
			fc.utobj().sendKeys(driver, pobj.title, title);
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().showAll(driver);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[contains(text () ,'" + title + "')]/ancestor::tr/td/input"));
			fc.utobj().clickElement(driver, pobj.archiveBottomBtn);
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().showAll(driver);

			fc.utobj().printTestStep("Archive Story");
			fc.utobj().clickElement(driver, pobj.archivedStoriesLink);
			fc.utobj().showAll(driver);

			fc.utobj().printTestStep("Restore Story");
			fc.utobj().actionImgOption(driver, title, "Restore");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify Restore Story");
			fc.utobj().clickElement(driver, pobj.archivedStoriesLink);
			fc.utobj().showAll(driver);
			boolean isTextPresent = fc.utobj().assertPageSource(driver, title);
			if (isTextPresent == true) {
				fc.utobj().throwsException("was not able to Restore Story :: At Archived page verification");
			}

			fc.utobj().clickElement(driver, pobj.storiesTab);
			fc.utobj().showAll(driver);
			boolean isTextPresent1 = fc.utobj().assertPageSource(driver, title);
			if (isTextPresent1 == false) {
				fc.utobj().throwsException("was not able to Restore Story :: At Stories Page Verification");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub","hub_stories" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Delete Story By Action Img Icon with Embed Video Option  At Admin > The Hub > Stories > Archived Stories", testCaseId = "TC_13_Delete_Story")
	private void deleteStoriesActionImgArchived() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminTheHubStoriesPage pobj = new AdminTheHubStoriesPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Stories Page");
			fc.utobj().printTestStep("Add Story");
			String title = fc.utobj().generateTestData(dataSet.get("title"));
			String summary = fc.utobj().generateTestData(dataSet.get("summary"));
			String editorText = fc.utobj().generateTestData(dataSet.get("editorText"));
			String accessTo = "All";
			String roleName = "";
			String mediaFile = "Embed Video";
			String currentDate = fc.utobj().getCurrentDateUSFormat();
			String futureDate = fc.utobj().getFutureDateUSFormat(1);
			addStories(driver, title, summary, editorText, accessTo, roleName, mediaFile, dataSet, currentDate,
					futureDate);
			fc.utobj().showAll(driver);

			fc.utobj().printTestStep("Archive Story");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[contains(text () ,'" + title + "')]/ancestor::tr/td/input"));
			fc.utobj().clickElement(driver, pobj.archiveBottomBtn);
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().showAll(driver);

			fc.utobj().clickElement(driver, pobj.archivedStoriesLink);
			fc.utobj().showAll(driver);

			fc.utobj().printTestStep("Delete Archive Story");
			fc.utobj().actionImgOption(driver, title, "Delete");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Delete Archive Story");
			fc.utobj().showAll(driver);
			boolean isTextPresent = fc.utobj().assertPageSource(driver, title);
			if (isTextPresent == true) {
				fc.utobj().throwsException("was not able to Delete Story :: At Archived page verification");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"thehub","hub_stories"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Restore Story By Bottom Button with Image Upload Option At Admin > The Hub > Stories > Archived Stories", testCaseId = "TC_14_Restore_Story")
	private void restoreStoriesBottomBtn() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminTheHubStoriesPage pobj = new AdminTheHubStoriesPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Stories Page");
			fc.utobj().printTestStep("Add Story");
			String title = fc.utobj().generateTestData(dataSet.get("title"));
			String summary = fc.utobj().generateTestData(dataSet.get("summary"));
			String editorText = fc.utobj().generateTestData(dataSet.get("editorText"));
			String accessTo = "All";
			String roleName = "";
			String mediaFile = "Image";
			String currentDate = fc.utobj().getCurrentDateUSFormat();
			String futureDate = fc.utobj().getFutureDateUSFormat(1);
			addStories(driver, title, summary, editorText, accessTo, roleName, mediaFile, dataSet, currentDate,
					futureDate);
			fc.utobj().showAll(driver);

			fc.utobj().printTestStep("Arcive Story");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[contains(text () ,'" + title + "')]/ancestor::tr/td/input"));
			fc.utobj().clickElement(driver, pobj.archiveBottomBtn);
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().showAll(driver);

			fc.utobj().printTestStep("Navigate to Archived Story Tab");
			fc.utobj().clickElement(driver, pobj.archivedStoriesLink);
			fc.utobj().showAll(driver);

			fc.utobj().printTestStep("Restore Archive Story");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[contains(text () ,'" + title + "')]/ancestor::tr/td/input"));
			fc.utobj().clickElement(driver, pobj.restoreBottomBtn);
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify Restore Story");
			fc.utobj().clickElement(driver, pobj.archivedStoriesLink);
			fc.utobj().showAll(driver);
			boolean isTextPresent = fc.utobj().assertPageSource(driver, title);
			if (isTextPresent == true) {
				fc.utobj().throwsException("was not able to Restore Story :: At Archived page verification");
			}

			fc.utobj().clickElement(driver, pobj.storiesTab);
			fc.utobj().showAll(driver);
			boolean isTextPresent1 = fc.utobj().assertPageSource(driver, title);
			if (isTextPresent1 == false) {
				fc.utobj().throwsException("was not able to Restore Story :: At Stories Page Verification");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"thehub","hub_stories"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Delete Story By Bottom Btn with Image Upload Option At Admin > The Hub > Stories > Archived Stories", testCaseId = "TC_15_Delete_Story")
	private void deleteStoriesBottomBtnArchived() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Stories Page");
			fc.utobj().printTestStep("Add Story");
			AdminTheHubStoriesPage pobj = new AdminTheHubStoriesPage(driver);
			String title = fc.utobj().generateTestData(dataSet.get("title"));
			String summary = fc.utobj().generateTestData(dataSet.get("summary"));
			String editorText = fc.utobj().generateTestData(dataSet.get("editorText"));
			String accessTo = "All";
			String roleName = "";
			String mediaFile = "Image";
			String currentDate = fc.utobj().getCurrentDateUSFormat();
			String futureDate = fc.utobj().getFutureDateUSFormat(1);
			addStories(driver, title, summary, editorText, accessTo, roleName, mediaFile, dataSet, currentDate,
					futureDate);
			fc.utobj().showAll(driver);

			fc.utobj().printTestStep("Archive Story");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[contains(text () ,'" + title + "')]/ancestor::tr/td/input"));
			fc.utobj().clickElement(driver, pobj.archiveBottomBtn);
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().showAll(driver);
			fc.utobj().printTestStep("Navigate To Archive Story Tab");
			fc.utobj().clickElement(driver, pobj.archivedStoriesLink);
			fc.utobj().showAll(driver);

			fc.utobj().printTestStep("Delete Story");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[contains(text () ,'" + title + "')]/ancestor::tr/td/input"));
			fc.utobj().clickElement(driver, pobj.deleteBottomBtn);
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Delete Story");
			fc.utobj().showAll(driver);
			boolean isTextPresent = fc.utobj().assertPageSource(driver, title);
			if (isTextPresent == true) {
				fc.utobj().throwsException("was not able to Delete Story :: At Archived page verification");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub","hub_stories"})
	@TestCase(createdOn = "2017-01-16", updatedOn = "2017-01-16", testCaseDescription = "To Verify the functionality of Home page View", testCaseId = "TC_Home_Page_View_Options")
	private void homePageView() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		
		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminTheHubStoriesPage pobj = new AdminTheHubStoriesPage(driver);
			fc.utobj().printTestStep("Navigate to Admin > The Hub > Story");
			fc.hub().hub_common().adminTheHubStoriesRSSFeedsandHomePageView(driver);
			fc.utobj().printTestStep("Add Story");
			String title = fc.utobj().generateTestData("Teststorytitle");
			String summary = fc.utobj().generateTestData("Teststorysummry");
			String editorText = fc.utobj().generateTestData("Teststoryeditortext");
			String accessTo = "All";
			String roleName = "";
			String mediaFile = "Image";
			String currentDate = fc.utobj().getCurrentDateUSFormat();
			String futureDate = fc.utobj().getFutureDateUSFormat(1);
			dataSet.put("uploadFile","pictureFile.jpg");
			addStories(driver, title, summary, editorText, accessTo, roleName, mediaFile, dataSet, currentDate, futureDate);
			
			fc.utobj().printTestStep(
					"Verify The Stories Tab should be appear as activated with stories summary page having header titles like below");
			fc.utobj().printTestStep("Title, Added By, Creation Date, Expiration Date, Action");

			boolean isTitleColumnPresent = fc.utobj().assertLinkText(driver, fc.utobj().translateString("Title"));
			boolean isAddedByColumnPresent = fc.utobj().assertLinkText(driver, fc.utobj().translateString("Added By"));
			boolean isCreationDateColumnPresent = fc.utobj().assertLinkText(driver,
					fc.utobj().translateString("Creation Date"));
			boolean isExpirationDateColumnPresent = fc.utobj().assertLinkText(driver,
					fc.utobj().translateString("Expiration Date"));
			boolean isActionTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text(),'Action')]");

			if (!isTitleColumnPresent) {
				fc.utobj().throwsException("Title column is not present at story page");
			}
			if (!isAddedByColumnPresent) {
				fc.utobj().throwsException("Added By column is not present at story page");
			}
			if (!isCreationDateColumnPresent) {
				fc.utobj().throwsException("Creation Date column is not present at story page");
			}
			if (!isExpirationDateColumnPresent) {
				fc.utobj().throwsException("Expiration Date column is not present at story page");
			}
			if (!isActionTextPresent) {
				fc.utobj().throwsException("Action column is not present at story page");
			}

			fc.utobj().printTestStep("Verify Define Top Story Sequence cbox options");
			fc.utobj().clickElement(driver, pobj.storiesTab);
			fc.utobj().printTestStep("Click over define top story sequence options and verify save and close Button");
			fc.utobj().clickElement(driver, pobj.defineTopStorySequenceLink);

			fc.commonMethods().switch_cboxIframe_frameId(driver);

			boolean isSaveBtnPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//input[@value='Save' and @name='button']");
			boolean isCloseBtnPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//input[@value='Close' and @name='close']");

			boolean isMoveToTopBtnPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a/img[@title='Move to Top']");
			boolean isMoveUpBtnPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a/img[@title='Move Up']");

			boolean isMoveDownPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a/img[@title='Move Down']");
			boolean isMoveToBottomBtnPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a/img[@title='Move to Bottom']");

			if (!isSaveBtnPresent) {
				fc.utobj().throwsException("Save Button is not present at cbox");
			}

			if (!isCloseBtnPresent) {
				fc.utobj().throwsException("Close Button is not present at cbox");
			}

			if (!isMoveToTopBtnPresent) {
				fc.utobj().throwsException("Move To Top Button is not present at cbox");
			}

			if (!isMoveUpBtnPresent) {
				fc.utobj().throwsException("Move Up Button is not present at cbox");
			}

			if (!isMoveDownPresent) {
				fc.utobj().throwsException("Move Down Button is not present at cbox");
			}

			if (!isMoveToBottomBtnPresent) {
				fc.utobj().throwsException("Move To Bottom Button is not present at cbox");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Click over Home Page View");
			fc.utobj().clickElement(driver, pobj.homePageView);

			fc.utobj().printTestStep(
					"Verify There should be 3 options with checkboxes to show the items on The Hub_Home_page");

			boolean isActiveStoryOptions = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text(), 'Active Stories on Home Page')]/parent::*/td/input[@name='homeActiveValue_S']");
			if (!isActiveStoryOptions) {
				fc.utobj().throwsException("Active Stories on Home Page Checkbox is not visible");
			}

			boolean ismultipleStoryOption = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text(), 'Select Display Style')]/following-sibling::td//td[contains(text(), 'Multiple Stories')]/preceding-sibling::td/input[@name='storyView']");
			if (!ismultipleStoryOption) {
				fc.utobj().throwsException("Multiple Stories radio button on Home Page is not visible");
			}

			boolean isSingleStoryOption = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text(), 'Select Display Style')]/following-sibling::td//td[contains(text(), 'Single Story')]/preceding-sibling::td/input[@name='storyView']");
			if (!isSingleStoryOption) {
				fc.utobj().throwsException("Multiple Stories radio button on Home Page is not visible");
			}

			boolean isFranbuzzOptions = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text(), 'FranBuzz Updates on Home Page')]/parent::*/td/input[@name='homeActiveValue_H']");
			if (!isFranbuzzOptions) {
				fc.utobj().throwsException("FranBuzz Updates on Home Page Checkbox is not visible");
			}

			boolean isActiveRSSOptions = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text(), 'Active RSS Feeds on Home Page')]/parent::*/td/input[@name='homeActiveValue_R']");
			if (!isActiveRSSOptions) {
				fc.utobj().throwsException("Active RSS Feeds on Home Page Checkbox is not visible");
			}

			boolean isselectRssFeedOptions = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[@id='ms-parentrssValue_1']/button");
			if (!isselectRssFeedOptions) {
				fc.utobj().throwsException("select rss feed multiselect drop down is not visible");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/*
	 * Configure Single and multiple story
	 */

	public void selectMultipleSingleStory(WebDriver driver, String storyView, String testCaseId) throws Exception {

		AdminTheHubStoriesPage story_page = new AdminTheHubStoriesPage(driver);

		fc.utobj().printTestStep("Go to Admin > The Hub > Home Page View");
		fc.hub().hub_common().adminTheHubStoriesRSSFeedsandHomePageView(driver);

		fc.utobj().printTestStep("Click on Home page view Tab and check the Stories checkbox");
		fc.utobj().clickElement(driver, story_page.homePageView);

		if (!fc.utobj().isSelected(driver,story_page.homeActiveCheckBox)) {
			fc.utobj().clickElement(driver, story_page.homeActiveCheckBox);
		}

		if (storyView.equalsIgnoreCase("Single")) {
			if (!fc.utobj().isSelected(driver, story_page.singleStoryRadioBtn)) {
				fc.utobj().clickElement(driver, story_page.singleStoryRadioBtn);
			}
		} else if (storyView.equalsIgnoreCase("Multiple")) {
			if (!fc.utobj().isSelected(driver, story_page.multipleStoryRadioBtn)) {
				fc.utobj().clickElement(driver, story_page.multipleStoryRadioBtn);
			}
		}
		fc.utobj().clickElement(driver, story_page.saveBtnAtHPV);
	}

	/*
	 * FranBuzz Updates on Home Page View
	 */

	public void franBuzzUpdatesAtHomePage(WebDriver driver, String testCaseId, boolean flag) throws Exception {
		AdminTheHubStoriesPage story_page = new AdminTheHubStoriesPage(driver);

		try {
			fc.utobj().printTestStep("Go to Admin > The Hub > Home Page View");
			fc.hub().hub_common().adminTheHubStoriesRSSFeedsandHomePageView(driver);

			fc.utobj().printTestStep("Click on Home page view Tab");
			fc.utobj().clickElement(driver, story_page.homePageView);

			if (flag == true) {
				if (!fc.utobj().isSelected(driver,story_page.franbuzzCheckBox)) {
					fc.utobj().clickElement(driver, story_page.franbuzzCheckBox);
				}
			} else if (flag == false) {
				if (fc.utobj().isSelected(driver,story_page.franbuzzCheckBox)) {
					fc.utobj().clickElement(driver, story_page.franbuzzCheckBox);
				}
			}
			fc.utobj().clickElement(driver, story_page.saveBtnAtHPV);
		} catch (Exception e) {
			fc.utobj().throwsException("Not able to ON FranBuzz Updates on Home Page");
		}
	}
}
