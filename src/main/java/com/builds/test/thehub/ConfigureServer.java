package com.builds.test.thehub;

public class ConfigureServer {

	private String FTPServerIP;
	private String FTPServerUN;
	private String FTPServerP;
	private String SFTPServerIP;
	private String SFTPServerUN;
	private String SFTPServerP;
	private String OneDriveUN;
	private String OneDriveP;
	private String OneDriveClientID;
	private String OneDriveClientSecretID;
	private String CustomerS3UN;
	private String CustomerS3P;
	private String CustomerS3AccessKey;
	private String CustomerS3SecretKey;
	private String GoogleDriveUN;
	private String GoogleDriveP;
	private String GoogleDriveClientID;
	private String GoogleDriveClientSecretID;
	
	public String getFTPServerIP() {
		return FTPServerIP;
	}
	public void setFTPServerIP(String fTPServerIP) {
		FTPServerIP = fTPServerIP;
	}
	public String getFTPServerUN() {
		return FTPServerUN;
	}
	public void setFTPServerUN(String fTPServerUN) {
		FTPServerUN = fTPServerUN;
	}
	public String getFTPServerP() {
		return FTPServerP;
	}
	public void setFTPServerP(String fTPServerP) {
		FTPServerP = fTPServerP;
	}
	public String getSFTPServerIP() {
		return SFTPServerIP;
	}
	public void setSFTPServerIP(String sFTPServerIP) {
		SFTPServerIP = sFTPServerIP;
	}
	public String getSFTPServerUN() {
		return SFTPServerUN;
	}
	public void setSFTPServerUN(String sFTPServerUN) {
		SFTPServerUN = sFTPServerUN;
	}
	public String getSFTPServerP() {
		return SFTPServerP;
	}
	public void setSFTPServerP(String sFTPServerP) {
		SFTPServerP = sFTPServerP;
	}
	public String getOneDriveUN() {
		return OneDriveUN;
	}
	public void setOneDriveUN(String oneDriveUN) {
		OneDriveUN = oneDriveUN;
	}
	public String getOneDriveP() {
		return OneDriveP;
	}
	public void setOneDriveP(String oneDriveP) {
		OneDriveP = oneDriveP;
	}
	public String getOneDriveClientID() {
		return OneDriveClientID;
	}
	public void setOneDriveClientID(String oneDriveClientID) {
		OneDriveClientID = oneDriveClientID;
	}
	public String getOneDriveClientSecretID() {
		return OneDriveClientSecretID;
	}
	public void setOneDriveClientSecretID(String oneDriveClientSecretID) {
		OneDriveClientSecretID = oneDriveClientSecretID;
	}
	public String getCustomerS3UN() {
		return CustomerS3UN;
	}
	public void setCustomerS3UN(String customerS3UN) {
		CustomerS3UN = customerS3UN;
	}
	public String getCustomerS3P() {
		return CustomerS3P;
	}
	public void setCustomerS3P(String customerS3P) {
		CustomerS3P = customerS3P;
	}
	public String getCustomerS3AccessKey() {
		return CustomerS3AccessKey;
	}
	public void setCustomerS3AccessKey(String customerS3AccessKey) {
		CustomerS3AccessKey = customerS3AccessKey;
	}
	public String getCustomerS3SecretKey() {
		return CustomerS3SecretKey;
	}
	public void setCustomerS3SecretKey(String customerS3SecretKey) {
		CustomerS3SecretKey = customerS3SecretKey;
	}
	public String getGoogleDriveUN() {
		return GoogleDriveUN;
	}
	public void setGoogleDriveUN(String googleDriveUN) {
		GoogleDriveUN = googleDriveUN;
	}
	public String getGoogleDriveP() {
		return GoogleDriveP;
	}
	public void setGoogleDriveP(String googleDriveP) {
		GoogleDriveP = googleDriveP;
	}
	public String getGoogleDriveClientID() {
		return GoogleDriveClientID;
	}
	public void setGoogleDriveClientID(String googleDriveClientID) {
		GoogleDriveClientID = googleDriveClientID;
	}
	public String getGoogleDriveClientSecretID() {
		return GoogleDriveClientSecretID;
	}
	public void setGoogleDriveClientSecretID(String googleDriveClientSecretID) {
		GoogleDriveClientSecretID = googleDriveClientSecretID;
	}
	
}
