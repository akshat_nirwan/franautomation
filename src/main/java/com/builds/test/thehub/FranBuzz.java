package com.builds.test.thehub;

public class FranBuzz {
	private String infoText;
	private String title;
	private String htmlText;
	private String shareTo;
	private String albumTitle;
	private String albumDescription;
	private String coverTitle;
	private String coverUploadPhoto;
	private String shareToValue;
	private String createdOnDate;
	private String day;
	private String month;
	private String time;
	private String videoTitle;
	private String textAreaVideo;
	private String typeOfPost;
	private String photoTitle;
	private String browsePhoto;
	private String photoDescription;
	private String commentText;
	private String documentTitle;
	private String documentFile;
	private String birthDayMessages;
	private String postTitle;
	private String linkText;
	
	
	public String getDocumentTitle() {
		return documentTitle;
	}

	public void setDocumentTitle(String documentTitle) {
		this.documentTitle = documentTitle;
	}

	public String getDocumentFile() {
		return documentFile;
	}

	public void setDocumentFile(String documentFile) {
		this.documentFile = documentFile;
	}

	public String getPhotoTitle() {
		return photoTitle;
	}

	public void setPhotoTitle(String photoTitle) {
		this.photoTitle = photoTitle;
	}

	public String getBrowsePhoto() {
		return browsePhoto;
	}

	public void setBrowsePhoto(String browsePhoto) {
		this.browsePhoto = browsePhoto;
	}

	public String getPhotoDescription() {
		return photoDescription;
	}

	public void setPhotoDescription(String photoDescription) {
		this.photoDescription = photoDescription;
	}
	
	public String getVideoTitle() {
		return videoTitle;
	}

	public void setVideoTitle(String videoTitle) {
		this.videoTitle = videoTitle;
	}

	public String getTextAreaVideo() {
		return textAreaVideo;
	}

	public void setTextAreaVideo(String textAreaVideo) {
		this.textAreaVideo = textAreaVideo;
	}

	public String getInfoText() {
		return infoText;
	}

	public void setInfoText(String infoText) {
		this.infoText = infoText;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getHtmlText() {
		return htmlText;
	}

	public void setHtmlText(String htmlText) {
		this.htmlText = htmlText;
	}

	public String getShareTo() {
		return shareTo;
	}

	public void setShareTo(String shareTo) {
		this.shareTo = shareTo;
	}

	public String getAlbumTitle() {
		return albumTitle;
	}

	public void setAlbumTitle(String albumTitle) {
		this.albumTitle = albumTitle;
	}

	public String getAlbumDescription() {
		return albumDescription;
	}

	public void setAlbumDescription(String albumDescription) {
		this.albumDescription = albumDescription;
	}

	public String getCoverTitle() {
		return coverTitle;
	}

	public void setCoverTitle(String coverTitle) {
		this.coverTitle = coverTitle;
	}

	public String getCoverUploadPhoto() {
		return coverUploadPhoto;
	}

	public void setCoverUploadPhoto(String coverUploadPhoto) {
		this.coverUploadPhoto = coverUploadPhoto;
	}

	public String getShareToValue() {
		return shareToValue;
	}

	public void setShareToValue(String shareToValue) {
		this.shareToValue = shareToValue;
	}

	public String getCreatedOnDate() {
		return createdOnDate;
	}

	public void setCreatedOnDate(String createdOnDate) {
		this.createdOnDate = createdOnDate;
	}


	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	public String getTypeOfPost() {
		return typeOfPost;
	}

	public void setTypeOfPost(String typeOfPost) {
		this.typeOfPost = typeOfPost;
	}

	public String getCommentText() {
		return commentText;
	}

	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}

	public String getBirthDayMessages() {
		return birthDayMessages;
	}

	public void setBirthDayMessages(String birthDayMessages) {
		this.birthDayMessages = birthDayMessages;
	}

	public String getPostTitle() {
		return postTitle;
	}

	public void setPostTitle(String postTitle) {
		this.postTitle = postTitle;
	}

	public String getLinkText() {
		return linkText;
	}

	public void setLinkText(String linkText) {
		this.linkText = linkText;
	}

}
