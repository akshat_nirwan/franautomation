package com.builds.test.thehub;

import java.io.File;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;

import com.builds.uimaps.thehub.AdminTheHubLibraryPage;
import com.builds.uimaps.thehub.AdminTheHubNewsPage;
import com.builds.uimaps.thehub.TheHubLibraryPage;
import com.builds.utilities.FranconnectUtil;


class HubCommonMethods {

	FranconnectUtil fc = new FranconnectUtil();

	public void selectRolesInNews(WebDriver driver, News news) throws Exception {

		try {
			WebElement element = null;
			if (news.getAccessTo().equalsIgnoreCase("Corporate")) {
				element = fc.utobj().getElementByXpath(driver,
						".//*[@id='roleDiv1']/tbody/tr[1]/td/following-sibling::td");
			} else if (news.getAccessTo().equalsIgnoreCase("Divisional")) {
				element = fc.utobj().getElementByXpath(driver,
						".//*[@id='roleDiv1']/tbody/tr[1]/td/following-sibling::td");
			} else if (news.getAccessTo().equalsIgnoreCase("Regional")) {
				element = fc.utobj().getElementByXpath(driver,
						".//*[@id='roleDiv1']/tbody/tr[1]/td/following-sibling::td");
			} else if (news.getAccessTo().equalsIgnoreCase("Franchise")) {
				element = fc.utobj().getElementByXpath(driver,
						".//*[@id='roleDiv1']/tbody/tr[1]/td/following-sibling::td");
			}

			String lines[] = fc.utobj().getText(driver, element).split("\\r?\\n");
			for (int i = 1; i <= lines.length; i++) {
				if (lines[i].equalsIgnoreCase(news.geRoleName())) {
					i = i + 1;
					WebElement inputElement = element.findElement(By.xpath("./input[" + i + "]"));
					fc.utobj().clickElement(driver, inputElement);
					break;
				}
			}
		} catch (Exception e) {
			Reporter.log(e.getMessage().toString());
			fc.utobj().throwsException("Not able to select role : " + news.geRoleName());
		}
	}
	
	void tryClickClose(WebDriver driver , AdminTheHubLibraryPage pobj) throws Exception{
		
		boolean isElementClick=false;
		for (int i = 0; i < 5; i++) {
			if (!isElementClick) {
				try {
					fc.utobj().sleep();
					fc.utobj().clickElement(driver, pobj.closeConfirmation);
					isElementClick=true;
					break;
				} catch (Exception e) {
					fc.utobj().sleep(60000);
					isElementClick=false;
				}
			}
		}
	}
	
	void searchAdminHubNewsItem(WebDriver driver,News news) throws Exception{
		AdminTheHubNewsPage pobj = new AdminTheHubNewsPage(driver);
		fc.utobj().sendKeys(driver, pobj.searchNewsTextBx, news.getNewsItemTitle());
		fc.utobj().clickElement(driver, pobj.searchNewsImg);
		fc.utobj().sleep();
	}
	
	void sortByRecentlyUploadedDocuments(WebDriver driver){
		try {
			AdminTheHubLibraryPage pobj=new AdminTheHubLibraryPage(driver);
			fc.utobj().selectDropDownByValue(driver, pobj.sortBy, "Recently Uploaded");
			fc.utobj().clickElement(driver, pobj.searchYellowBtn);
			fc.utobj().sleep();
		} catch (Exception e) {
			Reporter.log(e.getMessage());
		}
	}
	
	
	void sortDocumentsByFilter(WebDriver driver,Library library){
		try {
			AdminTheHubLibraryPage pobj=new AdminTheHubLibraryPage(driver);
			
			if (library.getDocumentType()!=null && library.getDocumentType().equalsIgnoreCase("Upload File")) {
				fc.utobj().selectDropDown(driver, pobj.docTypeSelect, "Documents");
			}else if (library.getDocumentType()==null) {
				fc.utobj().selectDropDown(driver, pobj.docTypeSelect, "All");
			}else{
				fc.utobj().selectDropDown(driver, pobj.docTypeSelect, library.getDocumentType());	
			}
			if (library.getDocumentSubType()!=null) {
				fc.utobj().selectDropDown(driver, pobj.docSubTypeSelect, library.getDocumentSubType());	
			}
			
			if (library.getSortBy()!=null) {
				fc.utobj().selectDropDownByValue(driver, pobj.sortBy, library.getSortBy());	
			}else {
				fc.utobj().selectDropDownByValue(driver, pobj.sortBy, "Recently Uploaded");	
			}
			fc.utobj().clickElement(driver, pobj.searchYellowBtn);
			fc.utobj().sleep();
		} catch (Exception e) {
			Reporter.log("Not able to filter documents by filter");
		}
	}
	
	
	public void TypeInField(WebDriver driver,WebElement element, String txt){
	    String val = txt; 
	    element.clear();
	    for (int i = 0; i < val.length(); i++){
	        char c = val.charAt(i);
	        String s = new StringBuilder().append(c).toString();
	        element.sendKeys(s);
	    }       
	}
	
	void searchAdminHubLibraryDocuments(WebDriver driver,Library library) throws Exception{
		AdminTheHubLibraryPage pobj=new AdminTheHubLibraryPage(driver);
		fc.utobj().sendKeys(driver, pobj.searchDoc, library.getDocumentTitle());
		fc.utobj().clickElement(driver, pobj.systemSearchBtn);
		fc.utobj().sleep();
	}
	
	void deleteDownloadedFile(Library library){
		try {
			String filePath=FranconnectUtil.config.get("downloadFilepath")+File.separator.concat(library.getFile());
			File file=new File(filePath);
			if (file.delete()) {
				Reporter.log("File Deleted Successfuly");
			}else {
				Reporter.log("File Not Deleted");
			}
		} catch (Exception e) {
			Reporter.log("Something went wrong while deleting file"+e.getMessage());
		}
	}
	
	void postCommentAtListView(WebDriver driver, AdminTheHubLibraryPage pobj,Library library) throws Exception{
		try {
			fc.utobj().clickElement(driver, pobj.listView);
			WebElement element=fc.utobj().getElementByXpath(driver, ".//a[@class='textTheme14 ' and .='"+library.getDocumentTitle()+"']/parent::*//a[.='Comment']");
			String text=fc.utobj().getAttributeValue(element, "onclick");
			text=text.substring(text.indexOf("'")+1, text.indexOf(",")-1);
			fc.utobj().clickElementByJS(driver, element);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//div[contains(@id,'"+text+"') and @style='display: block;']/textarea"), library.getCommentText());
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//div[contains(@id,'"+text+"')]/input[@id='btnReply']"));
		} catch (Exception e) {
			Reporter.log(e.getMessage().toString());
			fc.utobj().throwsException("Not able to post comment at List View");
		}
	}
	
	void searchThehubSearchDocument(WebDriver driver,Library library) throws Exception{
		try {
			TheHubLibraryPage pobj=new TheHubLibraryPage(driver);
			fc.utobj().sendKeys(driver, pobj.searchAllDocument, library.getDocumentTitle());
			fc.utobj().clickElement(driver, pobj.searchItem);
		} catch (Exception e) {
			Reporter.log(e.getMessage().toString());
			fc.utobj().throwsException("Not able to search document by document title : "+library.getDocumentTitle());
		}
	}
	
	//Library Global Search
	boolean searchItemByGlobalSearch(WebDriver driver,Library library){
		boolean isSearchTermPresent=false;
		try {
			TheHubLibraryPage pobj=new TheHubLibraryPage(driver);
			
			if (library.getSearchBy().equalsIgnoreCase("Folder")) {
				fc.utobj().sendKeys(driver, pobj.searchKeyTextBx, library.getFolderName());
			}else if (library.getSearchBy().equalsIgnoreCase("FolderSummary")) {
				fc.utobj().sendKeys(driver, pobj.searchKeyTextBx, library.getFolderSummary());
			}else if (library.getSearchBy().equalsIgnoreCase("Document Title")) {
				fc.utobj().sendKeys(driver, pobj.searchKeyTextBx, library.getDocumentTitle());
			}else if(library.getSearchBy().equalsIgnoreCase("Brief Summary")) {
				fc.utobj().sendKeys(driver, pobj.searchKeyTextBx, library.getBriefSummary());
			}
			
			fc.utobj().clickEnterOnElement(driver, pobj.searchKeyTextBx);
			int waitTime = 0;
			if (library.getWaitTime()==0) {
				waitTime=180;
			}else{
				waitTime=library.getWaitTime();
			}
			WebDriverWait wait=new WebDriverWait(driver, waitTime);
			
			//Click Over hub module
			fc.utobj().clickElement(driver, wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@class='link-style ellipsis ng-binding' and contains(text(),'The Hub')]"))));
			
			if (library.getSearchBy().equalsIgnoreCase("Folder") || library.getSearchBy().equalsIgnoreCase("FolderSummary")) {
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@class='ng-binding' and .='"+library.getFolderName()+"']")));
			}else if(library.getSearchBy().equalsIgnoreCase("Document Title") || library.getSearchBy().equalsIgnoreCase("Brief Summary")){
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@class='ng-binding' and .='"+library.getDocumentTitle()+"']")));
			}
			isSearchTermPresent=true;
			if (isSearchTermPresent) {
				pobj.searchKeyTextBx.sendKeys(Keys.ESCAPE);
			}
		} catch (Exception e) {
			Reporter.log("Not able to serch By Global Search : "+e.getMessage());
			isSearchTermPresent=false;
		}
		return isSearchTermPresent;
	}
	
	boolean searchItemByGlobalSearch(WebDriver driver,FranBuzz franbuzz){
		boolean isSearchTermPresent=false;
		try {
			TheHubLibraryPage pobj=new TheHubLibraryPage(driver);
			fc.utobj().sendKeys(driver, pobj.searchKeyTextBx, franbuzz.getInfoText());
			fc.utobj().clickEnterOnElement(driver, pobj.searchKeyTextBx);
			
			WebDriverWait wait=new WebDriverWait(driver, 120);
			fc.utobj().clickElement(driver, wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@class='link-style ellipsis ng-binding' and contains(text(),'The Hub')]"))));
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@class='ng-binding' and .='"+franbuzz.getInfoText()+"']")));
			isSearchTermPresent=true;
			if (isSearchTermPresent) {
				pobj.searchKeyTextBx.sendKeys(Keys.ESCAPE);
			}
		} catch (Exception e) {
			Reporter.log("Not able to serch By Global Search : "+e.getMessage());
			isSearchTermPresent=false;
		}
		return isSearchTermPresent;
	}
}
