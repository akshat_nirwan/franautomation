package com.builds.test.thehub;

public class Library {
	private String folderName;
	private String folderSummary; 
	private String summaryFormat;
	private String accessTo;
	private String htmlText;
	private String documentTitle;
	private String briefSummary;
	private String currentDate; 
	private String futureDate;
	private String documentType;
	private boolean recommendedDoc;
	private String documentSubType;
	private String file;
	private String linkUrl;
	private String editorText;
	private String sortBy;
	private String commentText;
	private String secondFutureDate;
	private String typeOfDocument;
	private String searchBy;
	private int waitTime;
	
	public void setFolderName(String folderName){
		this.folderName=folderName;
	}
	
	public String getFolderName() {
		return this.folderName;
	}
	
	public void setFolderSummary(String folderSummary){
		this.folderSummary=folderSummary;
	}
	
	public String getFolderSummary() {
		return this.folderSummary;
	}
	
	public void setSummaryFormat(String summaryFormat){
		this.summaryFormat=summaryFormat;
	}
	
	public String getSummaryFormat() {
		return this.summaryFormat;
	}
	
	public void setAccessTo(String accessTo){
		this.accessTo=accessTo;
	}
	
	public String getAccessTo() {
		return this.accessTo;
	}
	
	public void setHtmlText(String htmlText){
		this.htmlText=htmlText;
	}
	
	public String getHtmlText() {
		return this.htmlText;
	}
	
	public void setDocumentTitle(String documentTitle){
		this.documentTitle=documentTitle;
	}
	
	public String getDocumentTitle() {
		return this.documentTitle;
	}
	
	public void setBriefSummary(String briefSummary){
		this.briefSummary=briefSummary;
	}
	
	public String getBriefSummary() {
		return this.briefSummary;
	}
	
	public void setCurrentDate(String currentDate){
		this.currentDate=currentDate;
	}
	
	public String getCurrentDate() {
		return this.currentDate;
	}
	
	public void setFutureDate(String futureDate){
		this.futureDate=futureDate;
	}
	
	public String getFutureDate() {
		return this.futureDate;
	}
	
	public void setDocumentType(String documentType){
		this.documentType=documentType;
	}
	
	public String getDocumentType() {
		return this.documentType;
	}
	
	public void setRecommendedDoc(boolean recommendedDoc){
		this.recommendedDoc=recommendedDoc;
	}
	
	public boolean getRecommendedDoc() {
		return this.recommendedDoc;
	}
	
	public void setDocumentSubType(String documentSubType){
		this.documentSubType=documentSubType;
	}
	
	public String getDocumentSubType() {
		return this.documentSubType;
	}
	
	public void setFile(String file){
		this.file=file;
	}
	
	public String getFile() {
		return this.file;
	}
	
	public void setLinkUrl(String linkUrl){
		this.linkUrl=linkUrl;
	}
	
	public String getLinkUrl() {
		return this.linkUrl;
	}
	
	public void setEditorText(String editorText){
		this.editorText=editorText;
	}
	
	public String getEditorText() {
		return this.editorText;
	}

	public String getSortBy() {
		return sortBy;
	}

	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}

	public String getCommentText() {
		return commentText;
	}

	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}

	public String getSecondFutureDate() {
		return secondFutureDate;
	}

	public void setSecondFutureDate(String secondFutureDate) {
		this.secondFutureDate = secondFutureDate;
	}

	public String getTypeOfDocument() {
		return typeOfDocument;
	}

	public void setTypeOfDocument(String typeOfDocument) {
		this.typeOfDocument = typeOfDocument;
	}

	public String getSearchBy() {
		return searchBy;
	}

	public void setSearchBy(String searchBy) {
		this.searchBy = searchBy;
	}

	public int getWaitTime() {
		return waitTime;
	}

	public void setWaitTime(int waitTime) {
		this.waitTime = waitTime;
	}

}
