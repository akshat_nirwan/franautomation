package com.builds.test.thehub;

class Messages {

	private String subject;
	private String toEmail;
	private String messages;
	private String messagesType;
	private String recipientsUser;
	private String contactType;
	private String externalContactName;
	private String isAttachment;
	private String fileName;

	private String firstName;
	private String lastName;
	private String emailId;
	private String addressType;

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getSubject() {
		return this.subject;
	}

	public void setToEmail(String toEmail) {
		this.toEmail = toEmail;
	}

	public String getToEmail() {
		return this.toEmail;
	}

	public void setMessages(String messages) {
		this.messages = messages;
	}

	public String getMessages() {
		return this.messages;
	}

	public void setMessagesType(String messagesType) {
		this.messagesType = messagesType;
	}

	public String getMessagesType() {
		return this.messagesType;
	}

	public void setRecipientsUser(String recipientsUser) {
		this.recipientsUser = recipientsUser;
	}

	public String getRecipientsUser() {
		return this.recipientsUser;
	}

	public void setContactType(String contactType) {
		this.contactType = contactType;
	}

	public String getContactType() {
		return this.contactType;
	}

	public void setExternalContactName(String externalContactName) {
		this.externalContactName = externalContactName;
	}

	public String getExternalContactName() {
		return this.externalContactName;
	}

	public void setEmail(String emailId) {
		this.emailId = emailId;
	}

	public String getEmail() {
		return this.emailId;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setIsAttachment(String isAttachment) {
		this.isAttachment = isAttachment;
	}

	public String getIsAttachment() {
		return this.isAttachment;
	}

	public void setFile(String fileName) {
		this.fileName = fileName;
	}

	public String getFile() {
		return this.fileName;
	}

	public void setAddressType(String addressType) {
		this.addressType = addressType;
	}

	public String getAddressType() {
		return this.addressType;
	}

	public String getContactFullName() {

		if (getFirstName() != null && getLastName() != null) {
			return getFirstName() + " " + getLastName();
		}
		return null;
	}
}
