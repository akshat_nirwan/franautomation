package com.builds.test.thehub;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

import com.builds.uimaps.thehub.TheHubMessagesPage;
import com.builds.utilities.FranconnectUtil;

class MessagesTest {

	FranconnectUtil fc = new FranconnectUtil();

	public void selectRecipientsAddressBook(WebDriver driver, Messages hub_common) throws Exception {
		TheHubMessagesPage pobj = new TheHubMessagesPage(driver);

		if (hub_common.getAddressType() != null) {

			if (hub_common.getAddressType().equalsIgnoreCase("To")) {
				fc.utobj().clickElement(driver, pobj.addressBookTo);

			} else if (hub_common.getAddressType().equalsIgnoreCase("CC")) {

				fc.utobj().clickElement(driver, pobj.addressBookCC);

			} else if (hub_common.getAddressType().equalsIgnoreCase("BCC")) {
				fc.utobj().clickElement(driver, pobj.addressBookBCC);
			}
		} else if (hub_common.getAddressType() == null) {
			fc.utobj().clickElement(driver, pobj.addressBookTo);
		}
		fc.commonMethods().switch_cboxIframe_frameId(driver);

		if (hub_common.getContactType().equalsIgnoreCase("Internal")) {

			if (!fc.utobj().isSelected(driver, pobj.internalContact)) {
				fc.utobj().clickElement(driver, pobj.internalContact);
			}
			fc.utobj().setToDefault(driver, pobj.selectUsers);
			fc.utobj().selectValFromMultiSelect(driver, pobj.selectUsers, hub_common.getRecipientsUser());
			fc.utobj().clickElement(driver, pobj.submitOk);
			fc.utobj().sendKeys(driver, pobj.searchTextBx, hub_common.getToEmail());
			fc.utobj().clickElement(driver, pobj.searchImgBtn);

			fc.utobj().sleep();
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[contains(text () , '" + hub_common.getToEmail() + "')]/ancestor::tr/td/input"));
			fc.utobj().clickElement(driver, pobj.addUserBtn);
			fc.utobj().switchFrameToDefault(driver);
		} else if (hub_common.getContactType().equalsIgnoreCase("External")) {
			if (!fc.utobj().isSelected(driver,pobj.externalContact)) {
				fc.utobj().clickElement(driver, pobj.externalContact);
			}
			fc.utobj().showAll(driver);
			fc.utobj().sleep();
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[contains(text () , '" + hub_common.getContactFullName() + "')]/ancestor::tr/td/input"));
			fc.utobj().clickElement(driver, pobj.submitOk);
			fc.utobj().switchFrameToDefault(driver);
		}
	}

	public void composeMessages(WebDriver driver, Messages messages) throws Exception {

		try {
			TheHubMessagesPage pobj = new TheHubMessagesPage(driver);
			fc.hub().hub_common().theHubMessageSubModule(driver);
			fc.utobj().clickElement(driver, pobj.composeLink);

			selectRecipientsAddressBook(driver, messages);
			fc.utobj().sendKeys(driver, pobj.subject, messages.getSubject());

			if (messages.getIsAttachment() != null && messages.getIsAttachment().equalsIgnoreCase("Yes")) {
				new TheHubMessagesPageTest().closeTinyMCETrial(driver);
				
				fc.utobj().clickElement(driver, pobj.addAttachament);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				String filePath = fc.utobj().getFilePathFromTestData(messages.getFile());
				fc.utobj().sendKeys(driver, pobj.attachmentName, filePath);
				fc.utobj().clickElement(driver, pobj.attachBtn);
				fc.utobj().clickElement(driver, pobj.doneBtn);
			}
			if (messages.getMessagesType().equalsIgnoreCase("Text")) {

				fc.utobj().moveToElement(driver, pobj.texthtmlOption.get(0));
				if (!fc.utobj().isSelected(driver,pobj.texthtmlOption.get(0))) {
					fc.utobj().clickElement(driver, pobj.texthtmlOption.get(0));
				}
				fc.utobj().sendKeys(driver, pobj.messages, messages.getMessages());

			} else if (messages.getMessagesType().equalsIgnoreCase("Html")) {
				if (!fc.utobj().isSelected(driver,pobj.texthtmlOption.get(1))) {
					fc.utobj().clickElement(driver, pobj.texthtmlOption.get(1));
				}

				//Trial
				fc.utobj().printTestStep("Close the TinyMCE");
				new TheHubMessagesPageTest().closeTinyMCETrial(driver);
				
				fc.utobj().switchFrameById(driver, "mailText_ifr");
				Actions actions = new Actions(driver);
				actions.moveToElement(fc.utobj().getElement(driver, pobj.editorTextArea));
				actions.click();
				actions.sendKeys(messages.getMessages());
				actions.build().perform();

				fc.utobj().switchFrameToDefault(driver);
			}
			fc.utobj().clickElement(driver, pobj.sendBtn);
			fc.utobj().clickElement(driver, pobj.OkBtn);
		} catch (Exception e) {
			fc.utobj().throwsException(e.getMessage());
		}
	}

	public void addNewContact(WebDriver driver, Messages message_Page) throws Exception {

		try {
			TheHubMessagesPage pobj = new TheHubMessagesPage(driver);
			fc.hub().hub_common().theHubMessageSubModule(driver);
			fc.utobj().clickElement(driver, pobj.addressBookTab);
			fc.utobj().selectDropDown(driver, pobj.viewContactSelect, "External");

			fc.utobj().clickElement(driver, pobj.addNewContactLink);
			fc.utobj().sendKeys(driver, pobj.firstname, message_Page.getFirstName());
			fc.utobj().sendKeys(driver, pobj.lastName, message_Page.getLastName());
			fc.utobj().sendKeys(driver, pobj.nickName, "nickName");
			fc.utobj().sendKeys(driver, pobj.phoneNumber, "7744112255");
			fc.utobj().sendKeys(driver, pobj.phoneNumberExten, "12");
			fc.utobj().sendKeys(driver, pobj.faxNumber, "12457896321");
			fc.utobj().sendKeys(driver, pobj.mobileNumber, "1245789632");
			fc.utobj().sendKeys(driver, pobj.emailId, message_Page.getEmail());
			String otherEmailId = fc.utobj().generateRandomChar().concat("@hotmail.com");
			fc.utobj().sendKeys(driver, pobj.otherEmailId, otherEmailId);
			fc.utobj().sendKeys(driver, pobj.organization, "TestOrganization");
			fc.utobj().sendKeys(driver, pobj.address, "TestAddress");
			fc.utobj().sendKeys(driver, pobj.city, "TestCity");
			fc.utobj().selectDropDown(driver, pobj.country, "USA");
			fc.utobj().selectDropDown(driver, pobj.state, "Alaska");
			fc.utobj().sendKeys(driver, pobj.zip, "124578");
			fc.utobj().sendKeys(driver, pobj.otherInformation, "TestOtherInfo");
			fc.utobj().clickElement(driver, pobj.addConatactBtn);
		} catch (Exception e) {
			fc.utobj().throwsException(e.getMessage());
		}
	}
}
