package com.builds.test.thehub;

public class News {
	
	private String folderName;
	private String folderSummary;
	
	private String subFolderName;
	private String subFolderSummary;
	
	private String accessTo;
	private String roleName;
	
	private String newsItemTitle;
	private String briefSummary; 
	private String file;
	private String editorText;
	private boolean addNewsItemFolder;
	private String mediaType;
	private String embedCode;
	
	public void setFolderName(String folderName) {
		this.folderName = folderName;
	}

	public String getFolderName() {
		return this.folderName;
	}
	
	public void setFolderSummary(String folderSummary) {
		this.folderSummary = folderSummary;
	}

	public String getFolderSummary() {
		return this.folderSummary;
	}
	
	public void setSubFolderName(String subFolderName) {
		this.subFolderName = subFolderName;
	}

	public String getSubFolderName() {
		return this.subFolderName;
	}
	
	public void setSubFolderSummary(String subFolderSummary) {
		this.subFolderSummary = subFolderSummary;
	}

	public String getSubFolderSummary() {
		return this.subFolderSummary;
	}
	
	public void setAccessTo(String accessTo) {
		this.accessTo = accessTo;
	}
	
	public String getAccessTo() {
		return this.accessTo;
	}
	
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String geRoleName() {
		return this.roleName;
	}
	
	public void setNewsItemTitle(String newsItemTitle) {
		this.newsItemTitle = newsItemTitle;
	}

	public String getNewsItemTitle() {
		return this.newsItemTitle;
	}
	
	public void setNewsBriefSummary(String briefSummary) {
		this.briefSummary = briefSummary;
	}

	public String getNewsBriefSummary() {
		return this.briefSummary;
	}
	
	public void setFile(String file) {
		this.file = file;
	}

	public String getFile() {
		return this.file;
	}
	
	public void setNewsEditorText(String editorText) {
		this.editorText = editorText;
	}

	public String getNewsEditorText() {
		return this.editorText;
	}
	
	public void setAddNewsItemFolder(boolean addNewsItemFolder) {
		this.addNewsItemFolder = addNewsItemFolder;
	}

	public boolean getAddNewsItemFolder() {
		return this.addNewsItemFolder;
	}
	
	public void setMediaType(String mediaType) {
		this.mediaType = mediaType;
	}

	public String getMediaType() {
		return this.mediaType;
	}
	
	public void setNewsEmbedCode(String embedCode) {
		this.embedCode = embedCode;
	}
	public String getNewsEmbedCode() {
		return this.embedCode;
	}
}
