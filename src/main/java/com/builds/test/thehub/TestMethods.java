package com.builds.test.thehub;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class TestMethods {

	FranconnectUtil fc=new FranconnectUtil();
	
	@Test(groups = {"Test_Hub_0802"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Verify The Add Document At Admin > The Hub > Library >  Resource Library", testCaseId = "Test_Document_Add")
	private void addDocument() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);
			String folderName="TestFolder134";
			fc.hub().hub_common().adminTheHubLibraryPage(driver);
			
			for (int i = 0; i < 40; i++) {
				Library library=new Library();
				library.setFolderName(folderName);
				library.setDocumentSubType("PDF");
				library.setDocumentTitle(fc.utobj().generateTestData("TestDoc"));
				library.setDocumentType("Upload File");
				library.setBriefSummary("Test Brief summary");
				library.setFile("taskFile_01.pdf");
				library.setFutureDate(fc.utobj().getFutureDateUSFormat(2));
				library.setRecommendedDoc(false);
				new AdminTheHubLibraryPageTest().addDocument(driver, library);
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
}
