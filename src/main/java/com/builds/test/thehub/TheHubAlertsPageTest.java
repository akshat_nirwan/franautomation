package com.builds.test.thehub;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminAreaRegionAddAreaRegionPageTest;
import com.builds.test.admin.AdminDivisionAddDivisionPageTest;
import com.builds.test.admin.AdminFranchiseLocationAddFranchiseLocationPageTest;
import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.admin.AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest;
import com.builds.test.admin.AdminUsersManageManageFranchiseUsersPageTest;
import com.builds.test.admin.AdminUsersManageRegionalUsersAddRegionalUserPageTest;
import com.builds.test.admin.AdminUsersRolesAddNewRolePageTest;
import com.builds.test.admin.OptionsConfigureQuickLinkPageTest;
import com.builds.test.common.CorporateUser;
import com.builds.uimaps.admin.AdminUsersManageCorporateUsersAddCorporateUserPage;
import com.builds.uimaps.admin.AdminUsersManageManageFranchiseUsersPage;
import com.builds.uimaps.common.CommonUI;
import com.builds.uimaps.thehub.TheHubAlertsPage;
import com.builds.uimaps.thehub.TheHubDirectoryPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class TheHubAlertsPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "thehub" ,"hub_alerts"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Send Alert to Corporate User  :: The Hub > Alerts > Send Alerts ", testCaseId = "TC_136_Send_Alert_CU")
	private void sendAlertToCU() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubAlertsPage pobj = new TheHubAlertsPage(driver);

			fc.utobj().printTestStep("Navigate To The Hub > Messages > Signatures Page");
			fc.utobj().printTestStep("Add Signatures");

			String signatureTitle = fc.utobj().generateTestData("Testsignaturetitle");
			String signatureText = fc.utobj().generateTestData("Testsignaturetext");
			new TheHubMessagesPageTest().addSignatures(driver, signatureTitle, signatureText, config);

			fc.utobj().printTestStep("Add New Corporate Role Without The Hub > Can View Alerts Privilege");
			AdminUsersRolesAddNewRolePageTest roles_page = new AdminUsersRolesAddNewRolePageTest();
			String roleType = dataSet.get("roleType");
			String roleName = fc.utobj().generateTestData("TcCorporateRole");
			String moduleName = "The Hub";
			String emailId = "hubautomation@staffex.com";

			Map<String, String> privileges = new HashMap<String, String>();
			privileges.put("Can View Alerts", "No");
			roles_page.addRolesWithModulesPrivileges(driver, roleType, roleName, moduleName, privileges, testCaseId);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users > Add  Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUserPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser.setRole(roleName);
			corpUser = corporateUserPage.createDefaultUser(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");

			CorporateUser corpUser2 = new CorporateUser();
			corpUser2.setEmail(emailId);
			corpUser2 = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser2);
			corporateUserPage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser2);

			fc.utobj().printTestStep("Navigate To The Hub > Alerts");
			fc.utobj().printTestStep("Send Alert To Corporate User");
			fc.hub().hub_common().theHubAlertsSubModule(driver);
			fc.utobj().clickElement(driver, pobj.sendAlertLink);

			/*fc.utobj().printTestStep(
					"Verify Users who don't have Alerts privileges, should not appear through auto fill");
			new HubCommonMethods().TypeInField(driver, pobj.toEmailTextArea, corpUser.getFirstName());
			boolean isCorporateUserPresent = false;
			try {
				WebElement corElement= fc.utobj().getElementByXpath(driver,
						".//*[@id='theDiv']/div[contains(text(), '" + corpUser.getFirstName() + "')]");
				isCorporateUserPresent = corElement.isDisplayed();
			} catch (Exception e) {
				isCorporateUserPresent = false;
			}
			if (isCorporateUserPresent == true) {
				fc.utobj()
						.throwsException("User is visible in autofill even after don't have the permission of alerts");
			}*/

			fc.utobj().clickElement(driver, pobj.addressBook);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().setToDefault(driver, pobj.selectUsers);
			fc.utobj().selectValFromMultiSelect(driver, pobj.selectUsers, "Corporate Users");
			fc.utobj().clickElement(driver, pobj.submitOk);

			fc.utobj().printTestStep("Verify in address book");
			fc.utobj().sendKeys(driver, pobj.searchTxBx, corpUser.getuserFullName());
			fc.utobj().clickElement(driver, pobj.searchImgBtn);

			boolean isUserPresentAtAddressBook = fc.utobj().isElementPresent(driver, fc.utobj().getElementByXpath(driver, ".//*[contains(text(),'No records found')]"));

			if (isUserPresentAtAddressBook==false) {
				fc.utobj().throwsException(
						"User is visible in addressbook even after don't have the permission of alerts");
			}

			fc.utobj().sendKeys(driver, pobj.searchTxBx, corpUser2.getuserFullName());
			fc.utobj().clickElement(driver, pobj.searchImgBtn);

			WebElement element = fc.utobj().getElementByXpath(driver,
					".//*[contains(text () , '" + corpUser2.getuserFullName() + "')]/ancestor::tr/td/input");
			fc.utobj().clickElement(driver, element);
			fc.utobj().clickElement(driver, pobj.addBottmBtn);
			fc.utobj().switchFrameToDefault(driver);
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subject, subject);
			if (!fc.utobj().isSelected(driver,pobj.texthtmlOption.get(0))) {
				fc.utobj().clickElement(driver, pobj.texthtmlOption.get(0));
			}
			String alertMessages = fc.utobj().generateTestData(dataSet.get("alertMessages"));
			fc.utobj().sendKeys(driver, pobj.alertMessages, alertMessages);

			/*
			 * TC-9962
			 */
			fc.utobj().selectDropDown(driver, pobj.selectSignatures, signatureTitle);
			fc.utobj().clickElement(driver, pobj.sendBtn);

			String currentDate = fc.utobj().getCurrentDateUserDefineTimeZoneUSFormat("GMT-5:00");

			fc.utobj().printTestStep("Verify The Sent Alert");
			fc.utobj().clickElement(driver, pobj.viewAlertsLink);
			fc.utobj().selectDropDown(driver, pobj.viewSelect, "Sent");
			fc.utobj().selectDropDownByValue(driver, pobj.searchAlertWithSelect, "Both");
			fc.utobj().sendKeys(driver, pobj.searchAlertWithTxBx, subject);
			fc.utobj().clickElement(driver, pobj.searchBtn);
			fc.utobj().showAll(driver);
			fc.utobj().isTextDisplayed(driver, subject, "was not able to verify subject");

			/*
			 * TC-9957
			 */

			boolean isDateSentPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject + "')]/ancestor::tr/td[contains(text(),'" + currentDate + "')]");
			if (!isDateSentPresent) {
				fc.utobj().throwsException("Sent Date Of Alert is not present");
			}

			WebElement elementLink = fc.utobj().getElementByLinkText(driver, subject);
			fc.utobj().clickElement(driver, elementLink);

			fc.utobj().isTextDisplayed(driver, subject, "was not able to verify subject at alert detail Page");
			fc.utobj().isTextDisplayed(driver, "FranConnect Administrator", "was not able to verify FROM");
			fc.utobj().isTextDisplayed(driver, corpUser2.getuserFullName(), "was not able to verify TO");
			fc.utobj().switchFrameById(driver, "childframe");
			fc.utobj().isTextDisplayed(driver, alertMessages,
					"was not able to verify alert messages at Alert detail Page");
			fc.utobj().switchFrameToDefault(driver);

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With Corporate User");
			fc.loginpage().loginWithParameter(driver, corpUser2.getUserName(), corpUser2.getPassword());

			fc.utobj().printTestStep("Navigate To The Hub > Alerts");
			fc.hub().hub_common().theHubAlertsSubModule(driver);

			fc.utobj().printTestStep("Verify Alert");
			fc.utobj().clickElement(driver, pobj.viewAlertsLink);
			fc.utobj().selectDropDown(driver, pobj.viewSelect, "Received");
			fc.utobj().clickElement(driver, pobj.searchBtn);
			fc.utobj().showAll(driver);

			fc.utobj().isTextDisplayed(driver, subject, "was not able to verify subject");

			WebElement elementLink1 = fc.utobj().getElementByLinkText(driver, subject);
			fc.utobj().clickElement(driver, elementLink1);

			fc.utobj().isTextDisplayed(driver, subject, "was not able to verify subject at alert detail Page");
			fc.utobj().isTextDisplayed(driver, "FranConnect Administrator", "was not able to verify FROM");
			fc.utobj().isTextDisplayed(driver, corpUser2.getuserFullName(), "was not able to verify TO");
			fc.utobj().switchFrameById(driver, "childframe");
			fc.utobj().isTextDisplayed(driver, alertMessages,
					"was not able to verify alert messages at Alert detail Page");
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"thehub","hub_alerts"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Send Alert to Divisional User At The Hub > Alerts > Send Alerts", testCaseId = "TC_137_Send_Alert_DU")
	private void sendAlertToDU() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubAlertsPage pobj = new TheHubAlertsPage(driver);
			String emailId = "hubautomation@staffex.com";

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUserPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = corporateUserPage.createDefaultUser(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > Division > Add Division");
			fc.utobj().printTestStep("Add Division");

			AdminDivisionAddDivisionPageTest addDivPage = new AdminDivisionAddDivisionPageTest();
			String divisionName = fc.utobj().generateTestData(dataSet.get("divisionName"));
			addDivPage.addDivision(driver, divisionName);

			fc.utobj().printTestStep("Navigate To Admin > Users > Add Divisional User");
			fc.utobj().printTestStep("Add Divisional User");
			AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest divisionUserPage = new AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest();
			String divUserName = fc.utobj().generateTestData(dataSet.get("userName"));

			divisionUserPage.addDivisionalUser(driver, divUserName, corpUser.getPassword(), divisionName, emailId);
			String divUsetFullName = divUserName + " " + divUserName;

			fc.home_page().home_page().logout(driver);
			fc.utobj().printTestStep("Login By Newly Created Corporate User");
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());

			fc.utobj().printTestStep("Navigate To The Hub > Alerts");
			fc.utobj().printTestStep("Send Alert");
			fc.hub().hub_common().theHubAlertsSubModule(driver);
			fc.utobj().clickElement(driver, pobj.sendAlertLink);

			fc.utobj().printTestStep("Verify User should be appear through auto fill");
			new HubCommonMethods().TypeInField(driver, pobj.toEmailTextArea, divUserName);
			
			boolean isCorporateUserPresent = false;
			try {
				fc.utobj().getElementByXpath(driver,
						".//*[contains(text(), '" + divUserName + "')]");
				isCorporateUserPresent=true;
			} catch (Exception e) {
				isCorporateUserPresent = false;
			}

			if (!isCorporateUserPresent) {
				fc.utobj().throwsException("User is not visible in autofil");
			}

			fc.utobj().clickElement(driver, pobj.addressBook);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().setToDefault(driver, pobj.selectUsers);
			fc.utobj().selectValFromMultiSelect(driver, pobj.selectUsers, "Divisional Users");
			fc.utobj().clickElement(driver, pobj.submitOk);
			fc.utobj().sendKeys(driver, pobj.searchTxBx, divUsetFullName);
			fc.utobj().clickElement(driver, pobj.searchImgBtn);

			fc.utobj().sleep();
			WebElement element = fc.utobj().getElementByXpath(driver,
					".//*[contains(text () , '" + divUsetFullName + "')]/ancestor::tr/td/input");
			
			fc.utobj().clickElement(driver, element);
			fc.utobj().clickElement(driver, pobj.addBottmBtn);
			fc.utobj().switchFrameToDefault(driver);

			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subject, subject);
			if (!fc.utobj().isSelected(driver,pobj.texthtmlOption.get(0))) {
				fc.utobj().clickElement(driver, pobj.texthtmlOption.get(0));
			}
			String alertMessages = fc.utobj().generateTestData(dataSet.get("alertMessages"));
			fc.utobj().sendKeys(driver, pobj.alertMessages, alertMessages);
			fc.utobj().clickElement(driver, pobj.sendBtn);

			fc.utobj().printTestStep("Verify The Send Alert");
			fc.utobj().clickElement(driver, pobj.viewAlertsLink);
			// filter
			fc.utobj().selectDropDown(driver, pobj.viewSelect, "Sent");
			fc.utobj().clickElement(driver, pobj.searchBtn);
			fc.utobj().showAll(driver);

			fc.utobj().isTextDisplayed(driver, subject, "was not able to verify subject");

			WebElement elementLink = fc.utobj().getElementByLinkText(driver, subject);
			fc.utobj().clickElement(driver, elementLink);

			fc.utobj().isTextDisplayed(driver, subject, "was not able to verify subject at alert detail Page");
			fc.utobj().isTextDisplayed(driver, corpUser.getuserFullName(), "was not able to verify FROM");
			fc.utobj().isTextDisplayed(driver, divUsetFullName, "was not able to verify TO");
			fc.utobj().switchFrameById(driver, "childframe");
			fc.utobj().isTextDisplayed(driver, alertMessages,
					"was not able to verify alert messages at Alert detail Page");
			fc.utobj().switchFrameToDefault(driver);

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With Divisional User");
			fc.loginpage().loginWithParameter(driver, divUserName, corpUser.getPassword());
			fc.utobj().printTestStep("Navigate To The Hub > Alerts");
			fc.hub().hub_common().theHubAlertsSubModule(driver);

			fc.utobj().printTestStep("Verify The Alert");
			fc.utobj().clickElement(driver, pobj.viewAlertsLink);
			fc.utobj().selectDropDown(driver, pobj.viewSelect, "Received");
			fc.utobj().clickElement(driver, pobj.searchBtn);
			fc.utobj().showAll(driver);

			fc.utobj().isTextDisplayed(driver, subject, "was not able to verify subject");
			WebElement elementLink1 = fc.utobj().getElementByLinkText(driver, subject);
			fc.utobj().clickElement(driver, elementLink1);

			fc.utobj().isTextDisplayed(driver, subject, "was not able to verify subject at alert detail Page");
			fc.utobj().isTextDisplayed(driver, corpUser.getuserFullName(), "was not able to verify FROM");
			fc.utobj().isTextDisplayed(driver, divUsetFullName, "was not able to verify TO");
			fc.utobj().switchFrameById(driver, "childframe");
			fc.utobj().isTextDisplayed(driver, alertMessages,
					"was not able to verify alert messages at Alert detail Page");
			fc.utobj().switchFrameToDefault(driver);
			/*
			 * TC-9971
			 */
			fc.utobj().printTestStep("Delete The Searched Alert");
			fc.utobj().clickElement(driver, pobj.deleteAlertBottomBtn);
			fc.utobj().acceptAlertBox(driver);
			fc.utobj().selectDropDown(driver, pobj.viewSelect, "Received");
			fc.utobj().clickElement(driver, pobj.searchBtn);
			fc.utobj().showAll(driver);

			boolean isSubjectPresent = fc.utobj().assertPageSource(driver, subject);
			if (isSubjectPresent == true) {
				fc.utobj().throwsException("Alert is not deleting");
			}

			/*
			 * TC-9961
			 */

			/*fc.utobj().printTestStep("Verify Deleted Alert in Archived Reports Tab");
			fc.utobj().clickElement(driver, pobj.archiveAlertReportsLink);

			fc.utobj().printTestStep("Search Alert with filter in reports tab");
			fc.utobj().selectDropDownByValue(driver, pobj.selectSubject, "Subject");
			fc.utobj().sendKeys(driver, pobj.subjectFilter, subject);
			fc.utobj().selectDropDownByValue(driver, pobj.selectSendDate, "49");
			fc.utobj().clickElement(driver, pobj.searchBtn);

			boolean isAlertPresentArchivedReportsTab = fc.utobj().assertLinkPartialText(driver, subject);
			if (isAlertPresentArchivedReportsTab == false) {
				fc.utobj().throwsException("Deleted Alert is not present in Archived Reports Tab");
			}*/
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub" ,"hub_alerts"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Send Alert to Regional User At The Hub > Alerts > Send Alerts", testCaseId = "TC_138_Send_Alert_RU")
	private void sendAlertToRU() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubAlertsPage pobj = new TheHubAlertsPage(driver);
			String emailId = "hubautomation@staffex.com";

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUserPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = corporateUserPage.createDefaultUser(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > Add Area/Region");
			fc.utobj().printTestStep("Add Region");

			AdminAreaRegionAddAreaRegionPageTest addRegionPage = new AdminAreaRegionAddAreaRegionPageTest();
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			addRegionPage.addAreaRegion(driver, regionName);

			fc.utobj().printTestStep("Add Regional User");
			AdminUsersManageRegionalUsersAddRegionalUserPageTest regionalUserPage = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
			String userName = fc.utobj().generateTestData(dataSet.get("userName"));

			regionalUserPage.addRegionalUser(driver, userName, corpUser.getPassword(), regionName, emailId);
			String userName1 = userName + " " + userName;

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login By Newly Created Corporate User");
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());

			fc.utobj().printTestStep("Navigate To The Hub > Alerts");
			fc.utobj().printTestStep("Send Alert");

			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			String toEmail = userName1;
			String alertMessages = fc.utobj().generateTestData(dataSet.get("alertMessages"));
			String alertMessagesType = "Text";
			String recipientsUser = "Regional Users";
			sendAlert(driver, toEmail, subject, alertMessagesType, alertMessages, recipientsUser, config);

			fc.utobj().printTestStep("Verify The Sent Alert Details");
			fc.utobj().clickElement(driver, pobj.viewAlertsLink);
			fc.utobj().selectDropDown(driver, pobj.viewSelect, "Sent");
			fc.utobj().sendKeys(driver, pobj.searchByUser, corpUser.getuserFullName());
			fc.utobj().clickElement(driver, pobj.searchBtn);
			fc.utobj().showAll(driver);

			fc.utobj().isTextDisplayed(driver, subject, "was not able to verify subject");
			WebElement elementLink = fc.utobj().getElementByLinkText(driver, subject);
			fc.utobj().clickElement(driver, elementLink);

			fc.utobj().isTextDisplayed(driver, subject, "was not able to verify subject at alert detail Page");
			fc.utobj().isTextDisplayed(driver, corpUser.getuserFullName(), "was not able to verify FROM");
			fc.utobj().isTextDisplayed(driver, userName1, "was not able to verify TO");
			fc.utobj().switchFrameById(driver, "childframe");
			fc.utobj().isTextDisplayed(driver, alertMessages,
					"was not able to verify alert messages at Alert detail Page");
			fc.utobj().switchFrameToDefault(driver);

			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login With Regional User");
			fc.loginpage().loginWithParameter(driver, userName, corpUser.getPassword());
			fc.utobj().printTestStep("Navigate To The Hub > Alerts");
			fc.hub().hub_common().theHubAlertsSubModule(driver);

			fc.utobj().printTestStep("Verify The Alert");
			fc.utobj().clickElement(driver, pobj.viewAlertsLink);
			fc.utobj().showAll(driver);

			fc.utobj().isTextDisplayed(driver, subject, "was not able to verify subject");

			WebElement elementLink1 = fc.utobj().getElementByLinkText(driver, subject);
			fc.utobj().clickElement(driver, elementLink1);

			fc.utobj().isTextDisplayed(driver, subject, "was not able to verify subject at alert detail Page");
			fc.utobj().isTextDisplayed(driver, corpUser.getuserFullName(), "was not able to verify FROM");
			fc.utobj().isTextDisplayed(driver, userName1, "was not able to verify TO");
			fc.utobj().switchFrameById(driver, "childframe");
			fc.utobj().isTextDisplayed(driver, alertMessages,
					"was not able to verify alert messages at Alert detail Page");
			fc.utobj().switchFrameToDefault(driver);

			/*
			 * TC-9971
			 */
			fc.utobj().printTestStep("Delete The Searched Alert");
			fc.utobj().clickElement(driver, pobj.deleteAlertBottomBtn);
			fc.utobj().acceptAlertBox(driver);
			fc.utobj().selectDropDown(driver, pobj.searchAlertWithSelect, "Subject");
			fc.utobj().sendKeys(driver, pobj.searchAlertWithTxBx, subject);
			fc.utobj().selectDropDown(driver, pobj.selectSendDate, "All");
			fc.utobj().clickElement(driver, pobj.searchBtn);
			fc.utobj().showAll(driver);

			boolean isSubjectPresent = fc.utobj().assertPageSource(driver, subject);
			if (isSubjectPresent == true) {
				fc.utobj().throwsException("Alert is not deleting");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub" ,"hub_alerts"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Send Alert to Franchise User At The Hub > Alerts > Send Alerts", testCaseId = "TC_139_Send_Alert_FU")
	private void sendAlertToFU() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubAlertsPage pobj = new TheHubAlertsPage(driver);
			String emailId = "hubautomation@staffex.com";

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUserPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = corporateUserPage.createDefaultUser(driver, corpUser);

			fc.utobj().printTestStep("Add Franchise Location");
			String regionName = "Test127Region".concat(fc.utobj().generateRandomNumber());
			String storeType = "Test127StoreType".concat(fc.utobj().generateRandomNumber());
			String franchiseId = "Test127FranId".concat(fc.utobj().generateRandomNumber());

			AdminFranchiseLocationAddFranchiseLocationPageTest addFranchiseLocation = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			franchiseId = addFranchiseLocation.addFranchiseLocation_AllWithFName(driver, franchiseId, regionName,
					storeType, firstName);

			fc.utobj().printTestStep("Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest addFranchiseUser = new AdminUsersManageManageFranchiseUsersPageTest();
			String roleName = "Default Franchise Role";
			String userName = fc.utobj().generateTestData(dataSet.get("userName"));
			addFranchiseUser.addFranchiseUser(driver, userName, corpUser.getPassword(), franchiseId, roleName, emailId);
			String userName1 = firstName;

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login By Newly Created Corporate User");
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());

			fc.utobj().printTestStep("Navigate To The Hub > Alerts");
			fc.utobj().printTestStep("Send Alert");

			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			String toEmail = userName1;
			String alertMessagesType = "Text";
			String alertMessages = fc.utobj().generateTestData(dataSet.get("alertMessages"));
			String recipientsUser = "Franchise Users";
			sendAlert(driver, toEmail, subject, alertMessagesType, alertMessages, recipientsUser, config);

			fc.utobj().printTestStep("Verify The Sent Alert");
			fc.utobj().clickElement(driver, pobj.viewAlertsLink);
			fc.utobj().selectDropDown(driver, pobj.viewSelect, "Sent");
			fc.utobj().clickElement(driver, pobj.searchBtn);
			fc.utobj().showAll(driver);

			fc.utobj().isTextDisplayed(driver, subject, "was not able to verify subject");

			WebElement elementLink = fc.utobj().getElementByLinkText(driver, subject);
			fc.utobj().clickElement(driver, elementLink);

			fc.utobj().isTextDisplayed(driver, subject, "was not able to verify subject at alert detail Page");
			fc.utobj().isTextDisplayed(driver, corpUser.getuserFullName(), "was not able to verify FROM");
			fc.utobj().isTextDisplayed(driver, userName1, "was not able to verify TO");
			fc.utobj().switchFrameById(driver, "childframe");
			fc.utobj().isTextDisplayed(driver, alertMessages,
					"was not able to verify alert messages at Alert detail Page");
			fc.utobj().switchFrameToDefault(driver);
			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login With Franchise User");
			fc.loginpage().loginWithParameter(driver, userName, corpUser.getPassword());
			fc.utobj().printTestStep("Navigate To The Hub > Alerts");
			fc.hub().hub_common().theHubAlertsSubModule(driver);

			fc.utobj().printTestStep("Verify Alert");
			fc.utobj().clickElement(driver, pobj.viewAlertsLink);
			fc.utobj().showAll(driver);

			fc.utobj().isTextDisplayed(driver, subject, "was not able to verify subject");

			WebElement elementLink1 = fc.utobj().getElementByLinkText(driver, subject);
			fc.utobj().clickElement(driver, elementLink1);

			fc.utobj().isTextDisplayed(driver, subject, "was not able to verify subject at alert detail Page");
			fc.utobj().isTextDisplayed(driver, corpUser.getuserFullName(), "was not able to verify FROM");
			fc.utobj().isTextDisplayed(driver, userName1, "was not able to verify TO");
			fc.utobj().switchFrameById(driver, "childframe");
			fc.utobj().isTextDisplayed(driver, alertMessages,
					"was not able to verify alert messages at Alert detail Page");
			fc.utobj().switchFrameToDefault(driver);

			/*
			 * TC-9971
			 */
			fc.utobj().printTestStep("Delete The Searched Alert");
			fc.utobj().clickElement(driver, pobj.deleteAlertBottomBtn);
			fc.utobj().acceptAlertBox(driver);
			fc.utobj().selectDropDown(driver, pobj.searchAlertWithSelect, "Subject");
			fc.utobj().sendKeys(driver, pobj.searchAlertWithTxBx, subject);
			fc.utobj().selectDropDown(driver, pobj.selectSendDate, "All");
			fc.utobj().clickElement(driver, pobj.searchBtn);
			fc.utobj().showAll(driver);

			boolean isSubjectPresent = fc.utobj().assertPageSource(driver, subject);
			if (isSubjectPresent == true) {
				fc.utobj().throwsException("Alert is not deleting");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"thehub","hub_alerts"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Send Alert To Group At The Hub > Alerts > Send Alerts", testCaseId = "TC_140_Send_Alert_Group")
	private void sendAlertToGroups() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubAlertsPage pobj = new TheHubAlertsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUserPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "hubautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUserPage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To The Hub > Directory > Groups Page");
			fc.utobj().printTestStep("Create Group");
			TheHubDirectoryPageTest directoryPage = new TheHubDirectoryPageTest();
			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			directoryPage.createNewGroup(driver, groupName, corpUser.getuserFullName(), config);

			fc.utobj().printTestStep("Navigate To The Hub > Alerts");
			fc.utobj().printTestStep("Send Alert");
			fc.hub().hub_common().theHubAlertsSubModule(driver);
			fc.utobj().clickElement(driver, pobj.sendAlertLink);

			fc.utobj().clickElement(driver, pobj.addressBook);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().setToDefault(driver, pobj.selectUsers);
			fc.utobj().selectValFromMultiSelect(driver, pobj.selectUsers, "Groups");
			fc.utobj().clickElement(driver, pobj.submitOk);
			fc.utobj().sendKeys(driver, pobj.searchTxBx, groupName);
			fc.utobj().clickElement(driver, pobj.searchImgBtn);

			fc.utobj().sleep();	
			WebElement element = fc.utobj().getElementByXpath(driver,
					".//*[contains(text () , '" + groupName + "')]/ancestor::tr/td/input");
			fc.utobj().clickElement(driver, element);
			fc.utobj().clickElement(driver, pobj.addBottmBtn);
			fc.utobj().switchFrameToDefault(driver);

			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subject, subject);
			if (!fc.utobj().isSelected(driver,pobj.texthtmlOption.get(0))) {
				fc.utobj().clickElement(driver, pobj.texthtmlOption.get(0));
			}
			String alertMessages = fc.utobj().generateTestData(dataSet.get("alertMessages"));
			fc.utobj().sendKeys(driver, pobj.alertMessages, alertMessages);
			fc.utobj().clickElement(driver, pobj.sendBtn);

			fc.utobj().printTestStep("Verify The Sent Alert");
			fc.utobj().clickElement(driver, pobj.viewAlertsLink);
			// filter
			fc.utobj().selectDropDown(driver, pobj.viewSelect, "Sent");
			fc.utobj().clickElement(driver, pobj.searchBtn);
			fc.utobj().showAll(driver);

			fc.utobj().isTextDisplayed(driver, subject, "was not able to verify subject");

			WebElement elementLink = fc.utobj().getElementByLinkText(driver, subject);
			fc.utobj().clickElement(driver, elementLink);

			fc.utobj().isTextDisplayed(driver, subject, "was not able to verify subject at alert detail Page");
			fc.utobj().isTextDisplayed(driver, "FranConnect Administrator", "was not able to verify FROM");
			fc.utobj().isTextDisplayed(driver, corpUser.getuserFullName(), "was not able to verify TO");
			fc.utobj().switchFrameById(driver, "childframe");
			fc.utobj().isTextDisplayed(driver, alertMessages,
					"was not able to verify alert messages at Alert detail Page");
			fc.utobj().switchFrameToDefault(driver);
			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login With Corporate User");
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());
			fc.utobj().printTestStep("Navigate To The Hub > Alerts");
			fc.hub().hub_common().theHubAlertsSubModule(driver);

			fc.utobj().printTestStep("Verify The Alert");
			fc.utobj().clickElement(driver, pobj.viewAlertsLink);
			fc.utobj().selectDropDown(driver, pobj.viewSelect, "Received");
			fc.utobj().showAll(driver);

			fc.utobj().isTextDisplayed(driver, subject, "was not able to verify subject");

			WebElement elementLink2 = fc.utobj().getElementByLinkText(driver, subject);
			fc.utobj().clickElement(driver, elementLink2);

			fc.utobj().isTextDisplayed(driver, subject, "was not able to verify subject at alert detail Page");
			fc.utobj().isTextDisplayed(driver, "FranConnect Administrator", "was not able to verify FROM");
			fc.utobj().isTextDisplayed(driver, corpUser.getuserFullName(), "was not able to verify TO");
			fc.utobj().switchFrameById(driver, "childframe");
			fc.utobj().isTextDisplayed(driver, alertMessages,
					"was not able to verify alert messages at Alert detail Page");
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"thehub","hub_alerts"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Send Alert To Roles At The Hub > Alerts > Send Alerts", testCaseId = "TC_141_Send_Alert_Roles")
	private void sendAlertToRoles() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubAlertsPage pobj = new TheHubAlertsPage(driver);

			fc.utobj().printTestStep("Add Corporate Role");
			AdminUsersRolesAddNewRolePageTest rolePage = new AdminUsersRolesAddNewRolePageTest();
			String roleName = fc.utobj().generateTestData(dataSet.get("roleName"));
			rolePage.addCorporateRoles(driver, roleName);

			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "hubautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUserPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser.setRole(roleName);
			corpUser = corporateUserPage.createDefaultUser(driver, corpUser);

			fc.utobj().printTestStep("Navigate To The Hub > Alerts");
			fc.utobj().printTestStep("Send Alert");
			fc.hub().hub_common().theHubAlertsSubModule(driver);
			fc.utobj().clickElement(driver, pobj.sendAlertLink);

			fc.utobj().clickElement(driver, pobj.addressBook);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().setToDefault(driver, pobj.selectUsers);
			fc.utobj().selectValFromMultiSelect(driver, pobj.selectUsers, "Roles");
			fc.utobj().clickElement(driver, pobj.submitOk);
			fc.utobj().sendKeys(driver, pobj.searchTxBx, roleName);
			fc.utobj().clickElement(driver, pobj.searchImgBtn);
			
			fc.utobj().sleep();
			WebElement element = fc.utobj().getElementByXpath(driver,
					".//*[contains(text () , '" + roleName + "')]/ancestor::tr/td/input");
			fc.utobj().clickElement(driver, element);
			fc.utobj().clickElement(driver, pobj.addBottmBtn);
			fc.utobj().switchFrameToDefault(driver);

			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subject, subject);
			if (!fc.utobj().isSelected(driver,pobj.texthtmlOption.get(0))) {
				fc.utobj().clickElement(driver, pobj.texthtmlOption.get(0));
			}
			String alertMessages = fc.utobj().generateTestData(dataSet.get("alertMessages"));
			fc.utobj().sendKeys(driver, pobj.alertMessages, alertMessages);
			fc.utobj().clickElement(driver, pobj.sendBtn);

			fc.utobj().printTestStep("Verify The Sent Alert Details");
			fc.utobj().clickElement(driver, pobj.viewAlertsLink);
			fc.utobj().selectDropDown(driver, pobj.viewSelect, "Sent");
			fc.utobj().clickElement(driver, pobj.searchBtn);
			fc.utobj().showAll(driver);
			fc.utobj().isTextDisplayed(driver, subject, "was not able to verify subject");

			WebElement elementLink = fc.utobj().getElementByLinkText(driver, subject);
			fc.utobj().clickElement(driver, elementLink);

			fc.utobj().isTextDisplayed(driver, subject, "was not able to verify subject at alert detail Page");
			fc.utobj().isTextDisplayed(driver, "FranConnect Administrator", "was not able to verify FROM");
			fc.utobj().isTextDisplayed(driver, corpUser.getuserFullName(), "was not able to verify TO");
			fc.utobj().switchFrameById(driver, "childframe");
			fc.utobj().isTextDisplayed(driver, alertMessages,
					"was not able to verify alert messages at Alert detail Page");
			fc.utobj().switchFrameToDefault(driver);

			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login With Corporate User");
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());

			fc.utobj().printTestStep("Navigate To The Hub > Alerts");
			fc.hub().hub_common().theHubAlertsSubModule(driver);

			fc.utobj().printTestStep("Verify The Alert");
			fc.utobj().clickElement(driver, pobj.viewAlertsLink);
			fc.utobj().selectDropDown(driver, pobj.viewSelect, "Received");
			fc.utobj().showAll(driver);

			fc.utobj().isTextDisplayed(driver, subject, "was not able to verify subject");

			WebElement elementLink2 = fc.utobj().getElementByLinkText(driver, subject);
			fc.utobj().clickElement(driver, elementLink2);

			fc.utobj().isTextDisplayed(driver, subject, "was not able to verify subject at alert detail Page");
			fc.utobj().isTextDisplayed(driver, "FranConnect Administrator", "was not able to verify FROM");
			fc.utobj().isTextDisplayed(driver, corpUser.getuserFullName(), "was not able to verify TO");
			fc.utobj().switchFrameById(driver, "childframe");
			fc.utobj().isTextDisplayed(driver, alertMessages,
					"was not able to verify alert messages at Alert detail Page");
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"thehub","hub_alerts"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Delete Alert At The Hub > Alerts  > View Alerts", testCaseId = "TC_142_Delete_Alert")
	private void deleteAlert() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubAlertsPage pobj = new TheHubAlertsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUserPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "hubautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUserPage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To The Hub > Alerts");
			fc.utobj().printTestStep("Send Alert");
			String toEmail = corpUser.getuserFullName();
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			String alertMessages = fc.utobj().generateTestData(dataSet.get("alertMessages"));
			String alertMessagesType = dataSet.get("alertMessagesType");
			String recipientsUser = dataSet.get("recipientsUser");
			sendAlert(driver, toEmail, subject, alertMessagesType, alertMessages, recipientsUser, config);

			fc.utobj().clickElement(driver, pobj.viewAlertsLink);
			fc.utobj().selectDropDown(driver, pobj.viewSelect, "Sent");
			fc.utobj().selectDropDown(driver, pobj.searchAlertWithSelect, "Subject");
			fc.utobj().sendKeys(driver, pobj.searchAlertWithTxBx, subject);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			fc.utobj().printTestStep("Delete Alert");
			WebElement element = fc.utobj().getElementByXpath(driver,
					".//*[.='" + subject + "']/ancestor::tr/td/input");
			fc.utobj().clickElement(driver, element);
			fc.utobj().clickElement(driver, pobj.deleteAlertBottomBtn);
			fc.utobj().acceptAlertBox(driver);
			try {
				fc.utobj().acceptAlertBox(driver);
			} catch (Exception e) {
			}

			fc.utobj().printTestStep("Verify The Delete Alert");
			fc.utobj().selectDropDown(driver, pobj.viewSelect, "Sent");
			fc.utobj().selectDropDown(driver, pobj.searchAlertWithSelect, "Subject");
			fc.utobj().sendKeys(driver, pobj.searchAlertWithTxBx, subject);
			fc.utobj().clickElement(driver, pobj.searchBtn);
			fc.utobj().isTextDisplayed(driver, "No records found.", "was not able to delete Alert");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/*
	 * TC-9979
	 */

	@Test(groups = {"thehub" ,"hub_alerts"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Create Group At The Hub > Alerts > Groups ", testCaseId = "TC_143_Create_New_Group")
	private void createNewGroup() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			driver = fc.loginpage().login(driver);
			TheHubDirectoryPage pobj = new TheHubDirectoryPage(driver);

			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUserPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			String emailId = "hubautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUserPage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To The Hub Alerts > Groups Page");
			fc.utobj().printTestStep("Create Group");
			fc.hub().hub_common().theHubAlertsSubModule(driver);
			fc.utobj().clickElement(driver, pobj.groupsTab);
			fc.utobj().clickElement(driver, pobj.createNewGroupLink);

			fc.utobj().printTestStep("Verify External Address Book Radio Button");

			boolean isTextPresent = fc.utobj()
					.isElementPresent(driver, fc.utobj().getElement(driver, pobj.externalAddressBook));
			if (isTextPresent == false) {
				fc.utobj().throwsException("External Address Book Radio Button is not visible");
			}

			fc.utobj().clickElement(driver, pobj.internalAddressBook);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().sendKeys(driver, pobj.searchTxBxCU, corpUser.getuserFullName());
			fc.utobj().clickElement(driver, pobj.searchCUImgIcon);

			/*fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[contains(text () , '" + corpUser.getuserFullName() + "')]/ancestor::tr/td/input"));*/
			
			fc.utobj().clickElement(driver, pobj.selectcorporate);
			fc.utobj().clickElement(driver, pobj.selectUser);
			fc.utobj().switchFrameToDefault(driver);
			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			fc.utobj().sendKeys(driver, pobj.grouopName, groupName);
			fc.utobj().clickElement(driver, pobj.createBtn);
			fc.utobj().showAll(driver);

			fc.utobj().printTestStep("Verify The Create Group");
			fc.utobj().isTextDisplayed(driver, groupName, "was not able to create group");

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, groupName));

			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().isTextDisplayed(driver, corpUser.getuserFullName(),
					"was not able to verify Name of Corporate User");
			fc.utobj().isTextDisplayed(driver, "Corporate", "was not able to verify User Type");

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"thehub","hub_alerts"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Modify Group At The Hub > Alerts > Groups", testCaseId = "TC_144_Modify_Group")
	private void modifyGroupActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubDirectoryPage pobj = new TheHubDirectoryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Add Area/Region");
			fc.utobj().printTestStep("Add Region");

			AdminAreaRegionAddAreaRegionPageTest addRegionPage = new AdminAreaRegionAddAreaRegionPageTest();
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			addRegionPage.addAreaRegion(driver, regionName);

			AdminUsersManageRegionalUsersAddRegionalUserPageTest regionalUserPage = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
			String regUserName = fc.utobj().generateTestData(dataSet.get("regUserName"));
			String password = "t0n1ght123";
			String emailId = "hubautomation@staffex.com";
			regionalUserPage.addRegionalUser(driver, regUserName, password, regionName, emailId);

			regUserName = regUserName + " " + regUserName;

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUserPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUserPage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To The Hub > Directory > Groups Page");
			fc.utobj().printTestStep("Create Group");
			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			createNewGroupAtAlerts(driver, groupName, corpUser.getuserFullName(), config);

			fc.utobj().printTestStep("Modify Group");
			fc.utobj().printTestStep("Navigate To The Hub > Alerts > Groups Page");
			fc.hub().hub_common().theHubAlertsSubModule(driver);
			fc.utobj().clickElement(driver, pobj.groupsTab);
			fc.utobj().showAll(driver);
			fc.utobj().actionImgOption(driver, groupName, "Modify");
			String groupName1 = fc.utobj().generateTestData(dataSet.get("groupName"));
			fc.utobj().sendKeys(driver, pobj.grouopName, groupName1);
			fc.utobj().clickElement(driver, pobj.internalAddressBookLink);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.searchregionalUsers, regUserName);
			fc.utobj().clickElement(driver, pobj.searchRegUserImgIcon);

			fc.utobj().sleep();
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[contains(text () , '" + regUserName + "')]/ancestor::tr/td/input"));
			fc.utobj().clickElement(driver, pobj.selectUser);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().isTextDisplayed(driver, regUserName, "was not able to verify Second Contanct");
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("Verify The Modify Group");
			fc.utobj().showAll(driver);
			fc.utobj().isTextDisplayed(driver, groupName1, "was not able to modify group");
			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, groupName1));

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().isTextDisplayed(driver, corpUser.getuserFullName(),
					"was not able to verify Name of Corporate User");
			fc.utobj().isTextDisplayed(driver, "Corporate", "was not able to verify User Type of Corporate User");

			fc.utobj().isTextDisplayed(driver, regUserName, "was not able to verify Name of Regional User");
			fc.utobj().isTextDisplayed(driver, "Regional", "was not able to verify User Type of Regional User");
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"thehub","hub_alerts"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Delete Group At The Hub > Alerts > Groups ", testCaseId = "TC_145_Delete_Group")
	private void deleteGroupActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubDirectoryPage pobj = new TheHubDirectoryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUserPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "hubautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUserPage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To The Hub > Directory > Groups Page");
			fc.utobj().printTestStep("Create Group");
			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			createNewGroupAtAlerts(driver, groupName, corpUser.getuserFullName(), config);

			fc.utobj().printTestStep("Delete Group");

			fc.utobj().printTestStep("Navigate To The Hub > Alerts > Groups Page");
			fc.hub().hub_common().theHubAlertsSubModule(driver);
			fc.utobj().clickElement(driver, pobj.groupsTab);
			fc.utobj().showAll(driver);

			fc.utobj().actionImgOption(driver, groupName, "Delete");
			fc.utobj().acceptAlertBox(driver);
			fc.utobj().showAll(driver);

			fc.utobj().printTestStep("Modify The Delete Group");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, groupName);
			if (isTextPresent == true) {
				fc.utobj().throwsException("was not able to delete Group");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"thehub","hub_alerts"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Delete Group By Bottom Button At The Hub > Alerts > Groups", testCaseId = "TC_146_Delete_Group")
	private void deleteGroupBottomBtn() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubDirectoryPage pobj = new TheHubDirectoryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUserPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "hubautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUserPage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To The Hub > Directory > Groups Page");
			fc.utobj().printTestStep("Create Group");
			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			createNewGroupAtAlerts(driver, groupName, corpUser.getuserFullName(), config);

			fc.utobj().printTestStep("Navigate To The Hub > Alerts > Groups Page");
			fc.hub().hub_common().theHubAlertsSubModule(driver);
			fc.utobj().clickElement(driver, pobj.groupsTab);
			fc.utobj().showAll(driver);

			fc.utobj().printTestStep("Delete Group");
			WebElement element = fc.utobj().getElementByXpath(driver,
					".//*[.='" + groupName + "']/ancestor::tr/td/input");
			fc.utobj().clickElement(driver, element);
			fc.utobj().clickElement(driver, pobj.deleteBottomBtn);
			fc.utobj().acceptAlertBox(driver);
			fc.utobj().showAll(driver);

			fc.utobj().printTestStep("Verify The Delete Group");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, groupName);
			if (isTextPresent == true) {
				fc.utobj().throwsException("was not able to delete Group from bottom delete btn");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"thehub","hub_alerts"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Send Alert At The Hub > Alerts > Groups", testCaseId = "TC_147_Send_Alert")
	private void sendAlertActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubDirectoryPage pobj = new TheHubDirectoryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUserPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "hubautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUserPage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To The Hub > Directory > Groups Page");
			fc.utobj().printTestStep("Create Group");
			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			createNewGroupAtAlerts(driver, groupName, corpUser.getuserFullName(), config);

			fc.utobj().printTestStep("Navigate To The Hub > Alerts > Groups Page");
			fc.hub().hub_common().theHubAlertsSubModule(driver);
			fc.utobj().clickElement(driver, pobj.groupsTab);
			fc.utobj().showAll(driver);

			fc.utobj().printTestStep("Send Alert To Group");
			fc.utobj().actionImgOption(driver, groupName, "Send Alert");
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subjectAlert, subject);
			if (!fc.utobj().isSelected(driver,pobj.texthtmlOption.get(0))) {
				fc.utobj().clickElement(driver, pobj.texthtmlOption.get(0));
			}
			String alertMessages = fc.utobj().generateTestData(dataSet.get("alertMessages"));
			fc.utobj().sendKeys(driver, pobj.alertMessages, alertMessages);
			fc.utobj().clickElement(driver, pobj.sendBtn);

			fc.utobj().printTestStep("Verify The Alert");
			fc.utobj().clickElement(driver, pobj.viewAlertsLink);
			fc.utobj().selectDropDown(driver, pobj.viewSelect, "Sent");
			fc.utobj().clickElement(driver, pobj.searchBtn);
			fc.utobj().showAll(driver);
			fc.utobj().isTextDisplayed(driver, subject, "was not able to verify subject");
			
			WebElement elementLink = fc.utobj().getElementByLinkText(driver, subject);
			fc.utobj().clickElement(driver, elementLink);

			fc.utobj().isTextDisplayed(driver, subject, "was not able to verify subject at alert detail Page");
			fc.utobj().isTextDisplayed(driver, "FranConnect Administrator", "was not able to verify FROM");
			fc.utobj().isTextDisplayed(driver, corpUser.getuserFullName(), "was not able to verify TO");
			fc.utobj().switchFrameById(driver, "childframe");
			fc.utobj().isTextDisplayed(driver, alertMessages,
					"was not able to verify alert messages at Alert detail Page");
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/*
	 * TC-9985
	 */

	@Test(groups = {"thehub","hub_alerts"})
	@TestCase(createdOn = "2018-01-19", updatedOn = "2018-01-22", testCaseDescription = "Verify Deleted / Deactivated users should not be visible in Alerts Address book", testCaseId = "TC_Hub_Alerts_Address_Book_01")
	private void deactivateDeletedUser() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubAlertsPage pobj = new TheHubAlertsPage(driver);
			String emailId = "hubautomation@staffex.com";
			
			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			fc.adminpage().adminUsersManageCorporateUsersPage(driver);
			
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUserPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = corporateUserPage.createDefaultUser(driver, corpUser);
			
			fc.utobj().printTestStep("Search Added Corporate User");
			CommonUI cui = new CommonUI(driver);
			fc.utobj().sendKeys(driver, cui.Search_Button_ByID, corpUser.getuserFullName());
			fc.utobj().clickElement(driver, cui.Search_Input_ByID);

			fc.utobj().printTestStep("Deactivate the corporate user");
			fc.utobj().actionImgOption(driver, corpUser.getuserFullName(), "Deactivate");
			fc.utobj().acceptAlertBox(driver);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, cui.Close_Input_ByValue);

			fc.utobj().printTestStep("Navigate To Hub > Alerts");
			fc.hub().hub_common().theHubAlertsSubModule(driver);

			fc.utobj().clickElement(driver, pobj.sendAlertLink);

			fc.utobj().printTestStep("Verify Deactivated Users should not appear through auto fill");

			new HubCommonMethods().TypeInField(driver, pobj.toEmailTextArea, corpUser.getFirstName());
			
			boolean isCorporateUserPresent = false;
			try {
				WebElement element = fc.utobj().getElementByXpath(driver,
						".//*[@id='theDiv']/div[contains(text(), '" + corpUser.getFirstName() + "')]");
				isCorporateUserPresent = element.isDisplayed();
			} catch (Exception e) {
				isCorporateUserPresent = false;
			}

			if (isCorporateUserPresent) {
				fc.utobj().throwsException("Deactivated Users is visible in autofill");
			}

			fc.utobj().clickElement(driver, pobj.addressBook);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().setToDefault(driver, pobj.selectUsers);
			fc.utobj().selectValFromMultiSelect(driver, pobj.selectUsers, "Corporate Users");
			fc.utobj().clickElement(driver, pobj.submitOk);

			fc.utobj().printTestStep("Verify in address book");
			fc.utobj().sendKeys(driver, pobj.searchTxBx, corpUser.getuserFullName());
			fc.utobj().clickElement(driver, pobj.searchImgBtn);

			boolean isUserPresentAtAddressBook = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[@id='corpDiv']//td[contains(text(),'" + corpUser.getuserFullName() + "')]");

			if (isUserPresentAtAddressBook) {
				fc.utobj().throwsException("Deactivated User is visible in addressbook");
			}

			fc.utobj().clickElement(driver, cui.Close_Input_ByValue);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Navigate To Users > Corporate User Page");
			fc.adminpage().adminUsersManageCorporateUsersPage(driver);

			fc.utobj().clickElement(driver,
					new AdminUsersManageCorporateUsersAddCorporateUserPage(driver).deactivatedUsersTab);

			fc.utobj().printTestStep("Activate deactivated user and delete the same");
			fc.utobj().sendKeys(driver, cui.Search_Button_ByID, corpUser.getuserFullName());
			fc.utobj().clickElement(driver, cui.Search_Input_ByID);

			fc.utobj().actionImgOption(driver, corpUser.getuserFullName(), "Activate");
			fc.utobj().acceptAlertBox(driver);
			fc.utobj().clickElement(driver,
					new AdminUsersManageCorporateUsersAddCorporateUserPage(driver).activeUsersTab);
			fc.utobj().sendKeys(driver, cui.Search_Button_ByID, corpUser.getuserFullName());
			fc.utobj().clickElement(driver, cui.Search_Input_ByID);

			fc.utobj().printTestStep("Delete the corporate user");
			fc.utobj().actionImgOption(driver, corpUser.getuserFullName(), "Delete");
			fc.utobj().acceptAlertBox(driver);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, cui.Close_Input_ByValue);

			fc.utobj().printTestStep("Navigate To Hub > Alerts");
			fc.hub().hub_common().theHubAlertsSubModule(driver);

			fc.utobj().clickElement(driver, pobj.sendAlertLink);

			fc.utobj().printTestStep("Verify Deleted Users should not appear through auto fill");

			new HubCommonMethods().TypeInField(driver, pobj.toEmailTextArea, corpUser.getFirstName());
			
			boolean isCorporateUserPresent1 = false;
			try {
				WebElement element = fc.utobj().getElementByXpath(driver,
						".//*[@id='theDiv']/div[contains(text(), '" + corpUser.getFirstName() + "')]");
				isCorporateUserPresent1 = element.isDisplayed();
			} catch (Exception e) {
				isCorporateUserPresent1 = false;
			}

			if (isCorporateUserPresent1) {
				fc.utobj().throwsException("Deleted Users is visible in autofill");
			}

			fc.utobj().clickElement(driver, pobj.addressBook);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().setToDefault(driver, pobj.selectUsers);
			fc.utobj().selectValFromMultiSelect(driver, pobj.selectUsers, "Corporate Users");
			fc.utobj().clickElement(driver, pobj.submitOk);

			fc.utobj().printTestStep("Verify in address book");
			fc.utobj().sendKeys(driver, pobj.searchTxBx, corpUser.getuserFullName());
			fc.utobj().clickElement(driver, pobj.searchImgBtn);

			boolean isUserPresentAtAddressBook1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[@id='corpDiv']//td[contains(text(),'" + corpUser.getuserFullName() + "')]");

			if (isUserPresentAtAddressBook1) {
				fc.utobj().throwsException("Deleted User is visible in addressbook");
			}

			fc.utobj().clickElement(driver, cui.Close_Input_ByValue);
			fc.utobj().switchFrameToDefault(driver);

			/*
			 * Verify Divisional User
			 */

			fc.utobj().printTestStep("Navigate To Admin > Division > Add Division");
			fc.utobj().printTestStep("Add Division");

			AdminDivisionAddDivisionPageTest addDivPage = new AdminDivisionAddDivisionPageTest();
			String divisionName = fc.utobj().generateTestData("TestDiv");
			addDivPage.addDivision(driver, divisionName);

			fc.utobj().printTestStep("Navigate To Admin > Users > Add Divisional User");
			fc.utobj().printTestStep("Add Divisional User");
			AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest divisionUserPage = new AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest();
			String divUserName = fc.utobj().generateTestData("DivUser");
			String password = "t0n1ght123";
			divisionUserPage.addDivisionalUser(driver, divUserName, password, divisionName, emailId);

			String divUserFullName = divUserName + " " + divUserName;

			fc.utobj().printTestStep("Search Added Divisional User");
			fc.utobj().sendKeys(driver, cui.Search_Button_ByID, divUserName);
			fc.utobj().clickElement(driver, cui.Search_Input_ByID);

			fc.utobj().printTestStep("Deactivate the Divisional user");
			fc.utobj().actionImgOption(driver, divUserName, "Deactivate");
			fc.utobj().acceptAlertBox(driver);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, cui.Close_Input_ByValue);

			fc.utobj().printTestStep("Navigate To Hub > Alerts");
			fc.hub().hub_common().theHubAlertsSubModule(driver);

			fc.utobj().clickElement(driver, pobj.sendAlertLink);

			fc.utobj().printTestStep("Verify Deactivated Users should not appear through auto fill");

			/*fc.utobj().sendKeys(driver, pobj.toEmailTextArea, divUserFullName);*/
			new HubCommonMethods().TypeInField(driver, pobj.toEmailTextArea, divUserName);
			
			boolean isDivisionalUserPresent = false;
			try {
				WebElement element = fc.utobj().getElementByXpath(driver,
						".//*[@id='theDiv']/div[contains(text(), '" + divUserName + "')]");
				isDivisionalUserPresent = element.isDisplayed();
			} catch (Exception e) {
				isDivisionalUserPresent = false;
			}

			if (isDivisionalUserPresent) {
				fc.utobj().throwsException("Deactivated Users is visible in autofill");
			}

			fc.utobj().clickElement(driver, pobj.addressBook);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().setToDefault(driver, pobj.selectUsers);
			fc.utobj().selectValFromMultiSelect(driver, pobj.selectUsers, "Divisional Users");
			fc.utobj().clickElement(driver, pobj.submitOk);

			fc.utobj().printTestStep("Verify in address book");
			fc.utobj().sendKeys(driver, pobj.searchTxBx, divUserFullName);
			fc.utobj().clickElement(driver, pobj.searchImgBtn);

			boolean isUserPresentAtAddressBookDiv = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'No records found.')]");

			if (!isUserPresentAtAddressBookDiv) {
				fc.utobj().throwsException("Deactivated User is visible in addressbook");
			}

			fc.utobj().clickElement(driver, cui.Close_Input_ByValue);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Navigate To Users > Divisional User Page");
			fc.adminpage().openadminUsersManageDivisionalUsersLnk(driver);

			fc.utobj().clickElement(driver,
					new AdminUsersManageCorporateUsersAddCorporateUserPage(driver).deactivatedUsersTab);

			fc.utobj().sendKeys(driver, cui.Search_Button_ByID, divUserName);
			fc.utobj().clickElement(driver, cui.Search_Input_ByID);

			fc.utobj().actionImgOption(driver, divUserName, "Activate");
			fc.utobj().acceptAlertBox(driver);
			fc.utobj().clickElement(driver,
					new AdminUsersManageCorporateUsersAddCorporateUserPage(driver).activeUsersTab);

			fc.utobj().sendKeys(driver, cui.Search_Button_ByID, divUserName);
			fc.utobj().clickElement(driver, cui.Search_Input_ByID);

			fc.utobj().printTestStep("Delete the Divisional user");
			fc.utobj().actionImgOption(driver, divUserName, "Delete");
			fc.utobj().acceptAlertBox(driver);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, cui.Close_Input_ByValue);

			fc.utobj().printTestStep("Navigate To Hub > Alerts");
			fc.hub().hub_common().theHubAlertsSubModule(driver);

			fc.utobj().clickElement(driver, pobj.sendAlertLink);

			fc.utobj().printTestStep("Verify Deleted Users should not appear through auto fill");

			/*fc.utobj().sendKeys(driver, pobj.toEmailTextArea, divUserFullName);*/
			new HubCommonMethods().TypeInField(driver, pobj.toEmailTextArea, divUserName);
			
			boolean isDivUserPresent1 = false;
			try {
				WebElement element = fc.utobj().getElementByXpath(driver,
						".//*[@id='theDiv']/div[contains(text(), '" + divUserName + "')]");
				isDivUserPresent1 = element.isDisplayed();
			} catch (Exception e) {
				isDivUserPresent1 = false;
			}

			if (isDivUserPresent1) {
				fc.utobj().throwsException("Deleted Users is visible in autofill");
			}

			fc.utobj().clickElement(driver, pobj.addressBook);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().setToDefault(driver, pobj.selectUsers);
			fc.utobj().selectValFromMultiSelect(driver, pobj.selectUsers, "Divisional Users");
			fc.utobj().clickElement(driver, pobj.submitOk);

			fc.utobj().printTestStep("Verify in address book");
			fc.utobj().sendKeys(driver, pobj.searchTxBx, divUserFullName);
			fc.utobj().clickElement(driver, pobj.searchImgBtn);

			boolean isUserPresentAtAddressBook12 = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'No records found.')]");

			if (!isUserPresentAtAddressBook12) {
				fc.utobj().throwsException("Deleted User is visible in addressbook");
			}

			fc.utobj().clickElement(driver, cui.Close_Input_ByValue);
			fc.utobj().switchFrameToDefault(driver);

			/*
			 * Verify Regional User
			 */

			fc.utobj().printTestStep("Navigate To Admin > Add Area/Region");
			fc.utobj().printTestStep("Add Region");

			AdminAreaRegionAddAreaRegionPageTest addRegionPage = new AdminAreaRegionAddAreaRegionPageTest();
			String regionName = fc.utobj().generateTestData("Testreg");
			addRegionPage.addAreaRegion(driver, regionName);

			fc.utobj().printTestStep("Add Regional User");
			AdminUsersManageRegionalUsersAddRegionalUserPageTest regionalUserPage = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
			String regUserName = fc.utobj().generateTestData("Reguser");

			regionalUserPage.addRegionalUser(driver, regUserName, password, regionName, emailId);

			String regUserFullName = regUserName + " " + regUserName;

			fc.utobj().printTestStep("Search Added Regional User");
			fc.utobj().sendKeys(driver, cui.Search_Button_ByID, regUserName);
			fc.utobj().clickElement(driver, cui.Search_Input_ByID);

			fc.utobj().printTestStep("Deactivate the Regional user");
			fc.utobj().actionImgOption(driver, regUserName, "Deactivate");
			fc.utobj().acceptAlertBox(driver);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, cui.Close_Input_ByValue);

			fc.utobj().printTestStep("Navigate To Hub > Alerts");
			fc.hub().hub_common().theHubAlertsSubModule(driver);

			fc.utobj().clickElement(driver, pobj.sendAlertLink);

			fc.utobj().printTestStep("Verify Deactivated Users should not appear through auto fill");
			new HubCommonMethods().TypeInField(driver, pobj.toEmailTextArea, regUserName);
			
			boolean isRegionalUserPresent = false;
			try {
				WebElement element = fc.utobj().getElementByXpath(driver,
						".//*[@id='theDiv']/div[contains(text(), '" + regUserName + "')]");
				isRegionalUserPresent = element.isDisplayed();
			} catch (Exception e) {
				isRegionalUserPresent = false;
			}
			if (isRegionalUserPresent) {
				fc.utobj().throwsException("Deactivated Users is visible in autofill");
			}

			fc.utobj().clickElement(driver, pobj.addressBook);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().setToDefault(driver, pobj.selectUsers);
			fc.utobj().selectValFromMultiSelect(driver, pobj.selectUsers, "Regional Users");
			fc.utobj().clickElement(driver, pobj.submitOk);

			fc.utobj().printTestStep("Verify in address book");
			fc.utobj().sendKeys(driver, pobj.searchTxBx, regUserFullName);
			fc.utobj().clickElement(driver, pobj.searchImgBtn);

			boolean isUserPresentAtAddressBookReg = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'No records found.')]");

			if (!isUserPresentAtAddressBookReg) {
				fc.utobj().throwsException("Deactivated User is visible in addressbook");
			}

			fc.utobj().clickElement(driver, cui.Close_Input_ByValue);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Navigate To Users > Regional User Page");
			fc.adminpage().adminUsersManageRegionalUsersPage(driver);
			fc.utobj().clickElement(driver,
					new AdminUsersManageCorporateUsersAddCorporateUserPage(driver).deactivatedUsersTab);

			fc.utobj().sendKeys(driver, cui.Search_Button_ByID, regUserName);
			fc.utobj().clickElement(driver, cui.Search_Input_ByID);

			fc.utobj().actionImgOption(driver, regUserName, "Activate");
			fc.utobj().acceptAlertBox(driver);
			fc.utobj().clickElement(driver,
					new AdminUsersManageCorporateUsersAddCorporateUserPage(driver).activeUsersTab);

			fc.utobj().sendKeys(driver, cui.Search_Button_ByID, regUserName);
			fc.utobj().clickElement(driver, cui.Search_Input_ByID);

			fc.utobj().printTestStep("Delete the Regional user");
			fc.utobj().actionImgOption(driver, regUserName, "Delete");
			fc.utobj().acceptAlertBox(driver);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, cui.Close_Input_ByValue);

			fc.utobj().printTestStep("Navigate To Hub > Alerts");
			fc.hub().hub_common().theHubAlertsSubModule(driver);

			fc.utobj().clickElement(driver, pobj.sendAlertLink);

			fc.utobj().printTestStep("Verify Deleted Users should not appear through auto fill");

			new HubCommonMethods().TypeInField(driver, pobj.toEmailTextArea, regUserName);
			
			boolean isRegUserPresent1 = false;
			try {
				WebElement element = fc.utobj().getElementByXpath(driver,
						".//*[@id='theDiv']/div[contains(text(), '" + regUserName + "')]");
				isRegUserPresent1 = element.isDisplayed();
			} catch (Exception e) {
				isRegUserPresent1 = false;
			}
			if (isRegUserPresent1) {
				fc.utobj().throwsException("Deleted Users is visible in autofill");
			}

			fc.utobj().clickElement(driver, pobj.addressBook);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().setToDefault(driver, pobj.selectUsers);
			fc.utobj().selectValFromMultiSelect(driver, pobj.selectUsers, "Regional Users");
			fc.utobj().clickElement(driver, pobj.submitOk);

			fc.utobj().printTestStep("Verify in address book");
			fc.utobj().sendKeys(driver, pobj.searchTxBx, regUserFullName);
			fc.utobj().clickElement(driver, pobj.searchImgBtn);

			boolean isUserPresentAtAddressBook122 = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'No records found.')]");

			if (!isUserPresentAtAddressBook122) {
				fc.utobj().throwsException("Deleted User is visible in addressbook");
			}

			fc.utobj().clickElement(driver, cui.Close_Input_ByValue);
			fc.utobj().switchFrameToDefault(driver);

			/*
			 * Franchise User
			 */

			fc.utobj().printTestStep("Add Franchise Location");
			String storeType = "Test127StoreType".concat(fc.utobj().generateRandomNumber());
			String franchiseId = "Test127FranId".concat(fc.utobj().generateRandomNumber());

			AdminFranchiseLocationAddFranchiseLocationPageTest addFranchiseLocation = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String firstName = fc.utobj().generateTestData("Testfname");
			String franRegionName = fc.utobj().generateTestData("Testfranreg");
			franchiseId = addFranchiseLocation.addFranchiseLocation_AllWithFName(driver, franchiseId, franRegionName,
					storeType, firstName);

			fc.utobj().printTestStep("Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest addFranchiseUser = new AdminUsersManageManageFranchiseUsersPageTest();
			String roleName = "Default Franchise Role";
			String userNameFran = fc.utobj().generateTestData("Testfran");
			addFranchiseUser.addFranchiseUser(driver, userNameFran, password, franchiseId, roleName, emailId);
			String franUserName = firstName;
			String franUserFullName = firstName + " " + firstName;

			fc.utobj().printTestStep("Search Added Franchise User");
			fc.utobj().sendKeys(driver, cui.Search_Button_ByID, franUserName);
			fc.utobj().clickElement(driver, cui.Search_Input_ByID);

			fc.utobj().printTestStep("Deactivate the Franchise user");
			fc.utobj().actionImgOption(driver, franUserName, "Deactivate");
			fc.utobj().acceptAlertBox(driver);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			
			boolean isElementPresent=false;
			try {
				isElementPresent=fc.utobj().isElementPresent(driver, new AdminUsersManageManageFranchiseUsersPage(driver).deactivatedBtnAtCBox);
			} catch (Exception e) {
			}
			if (isElementPresent) {
				fc.utobj().clickElement(driver, new AdminUsersManageManageFranchiseUsersPage(driver).deactivatedBtnAtCBox);
			}
			fc.utobj().clickElement(driver, cui.Close_Input_ByValue);
			
			fc.utobj().printTestStep("Navigate To Hub > Alerts");
			fc.hub().hub_common().theHubAlertsSubModule(driver);

			fc.utobj().clickElement(driver, pobj.sendAlertLink);

			fc.utobj().printTestStep("Verify Deactivated Users should not appear through auto fill");
			new HubCommonMethods().TypeInField(driver, pobj.toEmailTextArea, firstName);
			
			boolean isFranchiseUserPresent = false;
			try {
				WebElement element = fc.utobj().getElementByXpath(driver,
						".//*[@id='theDiv']/div[contains(text(), '" + firstName + "')]");
				isFranchiseUserPresent = element.isDisplayed();
			} catch (Exception e) {
				isFranchiseUserPresent = false;
			}
			if (isFranchiseUserPresent) {
				fc.utobj().throwsException("Deactivated Users is visible in autofill");
			}

			fc.utobj().clickElement(driver, pobj.addressBook);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().setToDefault(driver, pobj.selectUsers);
			fc.utobj().selectValFromMultiSelect(driver, pobj.selectUsers, "Franchise Users");
			fc.utobj().clickElement(driver, pobj.submitOk);

			fc.utobj().printTestStep("Verify in address book");
			fc.utobj().sendKeys(driver, pobj.searchTxBx, franUserFullName);
			fc.utobj().clickElement(driver, pobj.searchImgBtn);

			boolean isUserPresentAtAddressBookFran = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'No records found.')]");

			if (!isUserPresentAtAddressBookFran) {
				fc.utobj().throwsException("Deactivated User is visible in addressbook");
			}

			fc.utobj().clickElement(driver, cui.Close_Input_ByValue);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Navigate To Users > Franchise User Page");
			fc.adminpage().adminUsersManageFranchiseUsersPage(driver);
			fc.utobj().clickElement(driver,
					new AdminUsersManageCorporateUsersAddCorporateUserPage(driver).deactivatedUsersTab);

			fc.utobj().sendKeys(driver, cui.Search_Button_ByID, franUserName);
			fc.utobj().clickElement(driver, cui.Search_Input_ByID);

			fc.utobj().actionImgOption(driver, franUserName, "Activate");
			fc.utobj().acceptAlertBox(driver);
			fc.utobj().clickElement(driver,
					new AdminUsersManageCorporateUsersAddCorporateUserPage(driver).activeUsersTab);

			fc.utobj().sendKeys(driver, cui.Search_Button_ByID, franUserName);
			fc.utobj().clickElement(driver, cui.Search_Input_ByID);

			fc.utobj().printTestStep("Delete the Franchise user");
			fc.utobj().actionImgOption(driver, franUserName, "Delete");
			fc.utobj().acceptAlertBox(driver);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			if (fc.utobj().isElementPresent(driver, new AdminUsersManageManageFranchiseUsersPage(driver).deleteBtnAtCbox)) {
				fc.utobj().clickElement(driver, new AdminUsersManageManageFranchiseUsersPage(driver).deleteBtnAtCbox);
			}
			fc.utobj().clickElement(driver, cui.Close_Input_ByValue);

			fc.utobj().printTestStep("Navigate To Hub > Alerts");
			fc.hub().hub_common().theHubAlertsSubModule(driver);
			fc.utobj().clickElement(driver, pobj.sendAlertLink);
			fc.utobj().printTestStep("Verify Deleted Users should not appear through auto fill");
			new HubCommonMethods().TypeInField(driver, pobj.toEmailTextArea, firstName);
			
			boolean isRegUserPresent12 = false;
			try {
				WebElement element = fc.utobj().getElementByXpath(driver,
						".//*[@id='theDiv']/div[contains(text(), '" + firstName + "')]");
				isRegUserPresent12 = element.isDisplayed();
			} catch (Exception e) {
				isRegUserPresent12 = false;
			}
			if (isRegUserPresent12) {
				fc.utobj().throwsException("Deleted Users is visible in autofill");
			}

			fc.utobj().clickElement(driver, pobj.addressBook);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().setToDefault(driver, pobj.selectUsers);
			fc.utobj().selectValFromMultiSelect(driver, pobj.selectUsers, "Franchise Users");
			fc.utobj().clickElement(driver, pobj.submitOk);

			fc.utobj().printTestStep("Verify in address book");
			fc.utobj().sendKeys(driver, pobj.searchTxBx, franUserFullName);
			fc.utobj().clickElement(driver, pobj.searchImgBtn);

			boolean isUserPresentAtAddressBook1223 = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'No records found.')]");
			if (!isUserPresentAtAddressBook1223) {
				fc.utobj().throwsException("Deleted User is visible in addressbook");
			}
			fc.utobj().clickElement(driver, cui.Close_Input_ByValue);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/*
	 * QuickLinkAlert
	 */

	@Test(groups = { "thehub","hub_alerts"})
	@TestCase(createdOn = "2018-01-19", updatedOn = "2018-01-19", testCaseDescription = "Verify Send Alert link under Quick Links", testCaseId = "TC_Hub_Quick_Link_Alert_01")
	private void quickLinkAlert() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubAlertsPage pobj = new TheHubAlertsPage(driver);

			String emailId = "hubautomation@staffex.com";
			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUserPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUserPage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Configure Quick Link Alerts");
			OptionsConfigureQuickLinkPageTest configure_notification = new OptionsConfigureQuickLinkPageTest();
			String linkToBeDisplayed = fc.utobj().translateString("Send Alert");
			configure_notification.configureQuickLnk(driver, linkToBeDisplayed, true);
			CommonUI coi = new CommonUI(driver);

			fc.utobj().printTestStep("Notification > Hub > Alerts");
			boolean isDisplayed = coi.showQuickLinks.isDisplayed();

			if (!isDisplayed) {
				fc.utobj().clickElement(driver, coi.showNotificationBar);
			}

			fc.utobj().clickElement(driver, coi.showQuickLinks);
			fc.utobj().clickElement(driver, coi.showAlertQuickLink);
			fc.utobj().clickElement(driver, pobj.addressBook);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().setToDefault(driver, pobj.selectUsers);
			fc.utobj().selectValFromMultiSelect(driver, pobj.selectUsers, "Corporate Users");
			fc.utobj().clickElement(driver, pobj.submitOk);

			fc.utobj().printTestStep("Verify in address book");
			fc.utobj().sendKeys(driver, pobj.searchTxBx, corpUser.getuserFullName());
			fc.utobj().clickElement(driver, pobj.searchImgBtn);

			fc.utobj().sleep();
			WebElement element = fc.utobj().getElementByXpath(driver,
					".//*[contains(text () , '" + corpUser.getuserFullName() + "')]/ancestor::tr/td/input");
			fc.utobj().clickElement(driver, element);
			fc.utobj().clickElement(driver, pobj.addBottmBtn);
			fc.utobj().switchFrameToDefault(driver);
			
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subject, subject);
			if (!fc.utobj().isSelected(driver,pobj.texthtmlOption.get(0))) {
				fc.utobj().clickElement(driver, pobj.texthtmlOption.get(0));
			}
			String alertMessages = fc.utobj().generateTestData(dataSet.get("alertMessages"));
			fc.utobj().sendKeys(driver, pobj.alertMessages, alertMessages);
			fc.utobj().clickElement(driver, pobj.sendBtn);

			fc.utobj().printTestStep("Verify The Sent Alert");
			fc.utobj().clickElement(driver, pobj.viewAlertsLink);

			fc.utobj().printTestStep("Filter Data By In Between Sent Date");
			fc.utobj().selectDropDown(driver, pobj.viewSelect, "Sent");
			fc.utobj().selectDropDownByValue(driver, pobj.selectSendDate, "47");
			fc.utobj().sendKeys(driver, pobj.fromDateD,
					fc.utobj().getCurrentDateUserDefineTimeZoneUSFormat("GMT-5:00"));
			fc.utobj().sendKeys(driver, pobj.toDateD, fc.utobj().getFutureDateUserDefineTimeZoneUSFormat("GMT-5:00", 1));
			fc.utobj().clickElement(driver, pobj.searchBtn);
			fc.utobj().showAll(driver);

			fc.utobj().isTextDisplayed(driver, subject, "was not able to verify subject");

			WebElement elementLink = fc.utobj().getElementByLinkText(driver, subject);
			fc.utobj().clickElement(driver, elementLink);

			fc.utobj().isTextDisplayed(driver, subject, "was not able to verify subject at alert detail Page");
			fc.utobj().isTextDisplayed(driver, "FranConnect Administrator", "was not able to verify FROM");
			fc.utobj().isTextDisplayed(driver, corpUser.getuserFullName(), "was not able to verify TO");
			fc.utobj().switchFrameById(driver, "childframe");
			fc.utobj().isTextDisplayed(driver, alertMessages,
					"was not able to verify alert messages at Alert detail Page");
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/*
	 * TC-9981 TC-9978
	 */

	@Test(groups = { "thehub","hub_alerts"})
	@TestCase(createdOn = "2018-01-19", updatedOn = "2018-01-19", testCaseDescription = "Verify Group action icon options functionality for other users except Corporate users", testCaseId = "TC_Hub_Alert_Groups_01")
	private void groupAlerts() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubDirectoryPage directory_page = new TheHubDirectoryPage(driver);
			String emailId = "hubautomation@staffex.com";
			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUserPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = corporateUserPage.createDefaultUser(driver, corpUser);
			/*
			 * Div User
			 */
			fc.utobj().printTestStep("Navigate To Admin > Division > Add Division");
			fc.utobj().printTestStep("Add Division");

			AdminDivisionAddDivisionPageTest addDivPage = new AdminDivisionAddDivisionPageTest();
			String divisionName = fc.utobj().generateTestData("testdiv");
			addDivPage.addDivision(driver, divisionName);

			fc.utobj().printTestStep("Navigate To Admin > Users > Add Divisional User");
			fc.utobj().printTestStep("Add Divisional User");
			AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest divisionUserPage = new AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest();
			String devUserName = fc.utobj().generateTestData("devuser");
			divisionUserPage.addDivisionalUser(driver, devUserName, corpUser.getPassword(), divisionName, emailId);

			/*
			 * Regional User
			 */
			fc.utobj().printTestStep("Navigate To Admin > Add Area/Region");
			fc.utobj().printTestStep("Add Region");

			AdminAreaRegionAddAreaRegionPageTest addRegionPage = new AdminAreaRegionAddAreaRegionPageTest();
			String regionName = fc.utobj().generateTestData("Testreg");
			addRegionPage.addAreaRegion(driver, regionName);

			fc.utobj().printTestStep("Add Regional User");
			AdminUsersManageRegionalUsersAddRegionalUserPageTest regionalUserPage = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
			String regUserName = fc.utobj().generateTestData("Testreguser");

			regionalUserPage.addRegionalUser(driver, regUserName, corpUser.getPassword(), regionName, emailId);

			/*
			 * Franchise User
			 */

			fc.utobj().printTestStep("Add Franchise Location");
			String storeType = "Test127StoreType".concat(fc.utobj().generateRandomNumber());
			String franchiseId = "Test127FranId".concat(fc.utobj().generateRandomNumber());

			AdminFranchiseLocationAddFranchiseLocationPageTest addFranchiseLocation = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String firstName = fc.utobj().generateTestData("Testfname");
			String franRegionName = fc.utobj().generateTestData("Testfreg");
			franchiseId = addFranchiseLocation.addFranchiseLocation_AllWithFName(driver, franchiseId, franRegionName,
					storeType, firstName);

			fc.utobj().printTestStep("Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest addFranchiseUser = new AdminUsersManageManageFranchiseUsersPageTest();
			String roleName = "Default Franchise Role";
			String userNameFran = fc.utobj().generateTestData("Testfranuser");
			addFranchiseUser.addFranchiseUser(driver, userNameFran, corpUser.getPassword(), franchiseId, roleName,
					emailId);

			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login by newly created Corporate User");
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());

			fc.utobj().printTestStep("Navigate To Hub > Alerts > Group");
			fc.utobj().printTestStep("Create Group");
			String groupName = fc.utobj().generateTestData("TestGroupName");
			createNewGroupAtAlerts(driver, groupName, corpUser.getuserFullName(), config);

			fc.utobj().actionImgOption(driver, groupName, "Turn this group Public");
			fc.utobj().acceptAlertBox(driver);

			fc.home_page().logout(driver);

			/*
			 * Divisional
			 */

			fc.utobj().printTestStep("Login With Newly Created Divisional User");
			fc.loginpage().loginWithParameter(driver, devUserName, corpUser.getPassword());

			fc.utobj().printTestStep("Verify Under Action icon of Groups, only Send Alerts option should come");

			fc.hub().hub_common().theHubAlertsSubModule(driver);
			fc.utobj().clickElement(driver, directory_page.groupsTab);
			boolean isOptionpresent = fc.utobj().optionPresentInActionMenu(driver, groupName, "Send Alert");

			if (isOptionpresent == false) {
				fc.utobj().throwsException("Under Action icon of Groups, Send Alerts option is not coming");
			}

			boolean isModifyOptionpresent = fc.utobj().optionPresentInActionMenu(driver, groupName, "Modify");
			boolean isDeleteOptionpresent = fc.utobj().optionPresentInActionMenu(driver, groupName, "Delete");
			boolean isTurnThisGroupPublicOptionpresent = fc.utobj().optionPresentInActionMenu(driver, groupName,
					"Turn this group Private");

			if (isModifyOptionpresent) {
				fc.utobj().throwsException("Under Action icon of Groups, Modify option is coming");
			}

			if (isDeleteOptionpresent) {
				fc.utobj().throwsException("Under Action icon of Groups, Delete option is coming");
			}
			if (isTurnThisGroupPublicOptionpresent) {
				fc.utobj().throwsException("Under Action icon of Groups, Turn this group Private option is coming");
			}

			boolean isDeleteButton2 = fc.utobj()
					.isElementPresent(driver, fc.utobj().getElement(driver, directory_page.deleteBottomBtn));
			if (isDeleteButton2 == false) {
				fc.utobj().throwsException("Delete Button is not coming");
			}

			/*
			 * Regional
			 */

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With Newly Created Regional User");
			fc.loginpage().loginWithParameter(driver, regUserName, corpUser.getPassword());
			
			fc.utobj().printTestStep("Verify Under Action icon of Groups, only Send Alerts option should come");
			fc.hub().hub_common().theHubAlertsSubModule(driver);
			
			boolean isRegGroupTab=fc.utobj().isElementPresent(driver, directory_page.groupsTab);
			if (isRegGroupTab) {
				fc.utobj().throwsException("Group Tab is visible to regional user");
			}

			/*
			 * Franchise
			 */
			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login With Newly Created Franchise User");
			fc.loginpage().loginWithParameter(driver, regUserName, corpUser.getPassword());

			fc.utobj().printTestStep("Verify Group tab should not visible for franchise user");
			fc.hub().hub_common().theHubAlertsSubModule(driver);
			
			boolean isGroupTabPresent=fc.utobj().isElementPresent(driver, directory_page.groupsTab);
			
			if (isGroupTabPresent) {
				fc.utobj().throwsException("Group Tab is visible to franchise user");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/*
	 * TC-9980
	 */

	@Test(groups = {"thehub","hub_alerts"})
	@TestCase(createdOn = "2018-01-22", updatedOn = "2018-01-22", testCaseDescription = "Verify Group action icon options functionality for Corporate users Under Alerts Section", testCaseId = "TC_Hub_Alert_Groups_02")
	private void groupsIconAlert() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubDirectoryPage directory_page = new TheHubDirectoryPage(driver);
			String emailId = "hubautomation@staffex.com";

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUserPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = corporateUserPage.createDefaultUser(driver, corpUser);

			fc.utobj().printTestStep("Naviagate To Hub > Alerts > Group");
			fc.utobj().printTestStep("Create Group");
			String groupName = fc.utobj().generateTestData("TestGroupName");
			createNewGroupAtAlerts(driver, groupName, corpUser.getuserFullName(), config);

			fc.utobj().printTestStep("Turn this group Public");
			fc.utobj().showAll(driver);
			fc.utobj().actionImgOption(driver, groupName, "Turn this group Public");
			fc.utobj().acceptAlertBox(driver);

			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login With Newly Created Corporate User Page");
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());

			fc.utobj().printTestStep("Navigate To Hub > Alerts > Groups Page");

			fc.hub().hub_common().theHubAlertsSubModule(driver);
			fc.utobj().clickElement(driver, directory_page.groupsTab);

			fc.utobj().showAll(driver);

			boolean isModifyOptionpresent2 = fc.utobj().optionPresentInActionMenu(driver, groupName, "Modify");
			boolean isDeleteOptionpresent2 = fc.utobj().optionPresentInActionMenu(driver, groupName, "Delete");
			boolean isTurnThisGroupPublicOptionpresent2 = fc.utobj().optionPresentInActionMenu(driver, groupName,
					"Turn this group Private");

			if (isModifyOptionpresent2 == false) {
				fc.utobj().throwsException("Under Action icon of Groups, Modify option is not coming");
			}

			if (isDeleteOptionpresent2 == false) {
				fc.utobj().throwsException("Under Action icon of Groups, Delete option is not coming");
			}
			if (isTurnThisGroupPublicOptionpresent2 == false) {
				fc.utobj().throwsException("Under Action icon of Groups, Turn this group Private option is not coming");
			}

			fc.utobj().printTestStep("Delete button at the bottom should be present");

			boolean isDeletePresent = fc.utobj()
					.isElementPresent(driver, fc.utobj().getElement(driver, directory_page.deleteBottomBtn));
			if (isDeletePresent == false) {
				fc.utobj().throwsException("Delete Button is not present");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/*
	 * TC-9974
	 * 
	 */

	@Test(groups = {"thehub","hub_alerts"})
	@TestCase(createdOn = "2018-01-19", updatedOn = "2018-01-19", testCaseDescription = "Verify Groups Sub Tab functionality under Alerts Tab for logged users based on privileged based", testCaseId = "TC_Hub_Alert_Messages_Directory_Groups_01")
	private void verifyCreatedGroupAlertMessagesDirectory() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			String emailId = "hubautomation@staffex.com";

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUserPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = corporateUserPage.createDefaultUser(driver, corpUser);

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login By newly Created Corporate User");
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());

			fc.utobj().printTestStep("Navigate To Hub > Messages > Groups");
			fc.utobj().printTestStep("Create Group");
			TheHubMessagesPageTest messages_test = new TheHubMessagesPageTest();
			String messageGroupName = fc.utobj().generateTestData("Testmsggroup");
			messages_test.createNewGroup(driver, messageGroupName, corpUser.getuserFullName());

			fc.utobj().printTestStep("Navigate To Hub > Directory > Groups");
			fc.utobj().printTestStep("Create Group");
			TheHubDirectoryPageTest directory_test = new TheHubDirectoryPageTest();

			String directoryGroupName = fc.utobj().generateTestData("Testdirecgroup");
			directory_test.createNewGroup(driver, directoryGroupName, corpUser.getuserFullName(), config);

			fc.utobj().printTestStep("Navigate To Hub > Alert > Groups");
			fc.utobj().printTestStep("Create Group");
			String alertGroupName = fc.utobj().generateTestData("Testalertgroup");
			createNewGroupAtAlerts(driver, alertGroupName, corpUser.getuserFullName(), config);
			fc.utobj().showAll(driver);

			boolean isMessageGroups = fc.utobj().assertPageSource(driver, messageGroupName);
			boolean isDirectoryGroups = fc.utobj().assertPageSource(driver, directoryGroupName);
			boolean isAlertGroups = fc.utobj().assertPageSource(driver, alertGroupName);

			if (isMessageGroups == false) {
				fc.utobj().throwsException("Message's groups is not visible at Alert's group");
			}

			if (isDirectoryGroups == false) {
				fc.utobj().throwsException("Directory's groups is not visible at Alert's group");
			}

			if (isAlertGroups == false) {
				fc.utobj().throwsException("Alert's groups is not visible at Alert's group");
			}

			boolean isMessageMember = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(), '" + messageGroupName + "')]/ancestor::tr/td[contains(text(),'1')]");
			boolean isDirectoryMember = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(), '" + directoryGroupName + "')]/ancestor::tr/td[contains(text(),'1')]");
			boolean isAlertMember = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(), '" + alertGroupName + "')]/ancestor::tr/td[contains(text(),'1')]");

			if (isMessageMember == false) {
				fc.utobj().throwsException("No of member is not present at Alert's groups page of Message's group");
			}

			if (isDirectoryMember == false) {
				fc.utobj().throwsException("No of member is not present at Alert's groups page of Directory's group");
			}

			if (isAlertMember == false) {
				fc.utobj().throwsException("No of member is not present at Alert's groups page of Alert's group");
			}

			boolean isMessageCreatedBy = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(), '" + messageGroupName
					+ "')]/ancestor::tr/td[contains(text(),'" + corpUser.getuserFullName() + "')]");
			boolean isDirectoryCreatedBy = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(), '" + directoryGroupName
					+ "')]/ancestor::tr/td[contains(text(),'" + corpUser.getuserFullName() + "')]");
			boolean isAlertCreatedBy = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(), '" + alertGroupName
					+ "')]/ancestor::tr/td[contains(text(),'" + corpUser.getuserFullName() + "')]");

			if (isMessageCreatedBy == false) {
				fc.utobj().throwsException("Created By is not present at Alert's groups page of Message's group");
			}

			if (isDirectoryCreatedBy == false) {
				fc.utobj().throwsException("Created By is not present at Alert's groups page of Directory's group");
			}

			if (isAlertCreatedBy == false) {
				fc.utobj().throwsException("Created By is not present at Alert's groups page of Alert's group");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/*
	 * TC-9973
	 */

	@Test(groups = { "thehub","hub_alerts"})
	@TestCase(createdOn = "2018-01-22", updatedOn = "2017-01-22", testCaseDescription = "Verify Archived Alerts functionality", testCaseId = "TC_Hub_Alert_Reports_01")
	private void reportsArchivedAlert() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubAlertsPage pobj = new TheHubAlertsPage(driver);
			String emailId = "hubautomation@staffex.com";

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUserPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = corporateUserPage.createDefaultUser(driver, corpUser);

			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login By newly Created Corporate User");
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());

			fc.utobj().printTestStep("Navigate To The Hub > Alerts");
			fc.utobj().printTestStep("Send Alert");
			String toEmail = corpUser.getuserFullName();
			String subject = fc.utobj().generateTestData("Testsubj");
			String alertMessages = fc.utobj().generateTestData("Testmsg");
			String alertMessagesType = "Text";
			String recipientsUser = "Corporate Users";
			sendAlert(driver, toEmail, subject, alertMessagesType, alertMessages, recipientsUser, config);

			fc.utobj().printTestStep("Navigate To Hub > Alerts > Reports");
			fc.utobj().clickElement(driver, pobj.reportsTab);

			fc.utobj().printTestStep("Search Alert with filter in reports tab");
			fc.utobj().selectDropDownByValue(driver, pobj.selectSubject, "Subject");
			fc.utobj().sendKeys(driver, pobj.subjectFilter, subject);
			fc.utobj().selectDropDownByValue(driver, pobj.selectSendDate, "49");
			fc.utobj().clickElement(driver, pobj.searchBtn);

			fc.utobj().printTestStep("Archive The Alert from Reports Tab");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text(),'" + subject + "')]/ancestor::tr/td/input[@name='alertno']"));
			fc.utobj().clickElement(driver, pobj.archiveBtn);
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify Archived Alert in Reports Tab");
			fc.utobj().selectDropDownByValue(driver, pobj.selectSubject, "Subject");
			fc.utobj().sendKeys(driver, pobj.subjectFilter, subject);
			fc.utobj().selectDropDownByValue(driver, pobj.selectSendDate, "49");
			fc.utobj().clickElement(driver, pobj.searchBtn);

			boolean isAlertPresentReports = fc.utobj().assertPageSource(driver, subject);
			if (isAlertPresentReports == true) {
				fc.utobj().throwsException("Alert is not archived");
			}

			fc.utobj().printTestStep("Archived Alert in Archived Alerts Tab");
			fc.utobj().clickElement(driver, pobj.archiveAlertLink);

			fc.utobj().printTestStep("Search Alert with filter in reports tab");
			fc.utobj().selectDropDownByValue(driver, pobj.selectSubject, "Subject");
			fc.utobj().sendKeys(driver, pobj.subjectFilter, subject);
			fc.utobj().selectDropDownByValue(driver, pobj.selectSendDate, "49");
			fc.utobj().selectDropDownByValue(driver, pobj.viewSelect, "1");
			fc.utobj().clickElement(driver, pobj.searchBtn);

			boolean isAlertPresentArchived = fc.utobj().assertPageSource(driver, subject);
			if (isAlertPresentArchived == false) {
				fc.utobj().throwsException("Alert is not present in Archived Alert Tabs");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/*
	 * TC-9972 Create alert by corporate user and send to divisional user > send
	 * reminder to div user Login by div user > verify reminder send to div
	 * 
	 */

	@Test(groups = { "thehub","hub_alerts"})
	@TestCase(createdOn = "2018-01-23", updatedOn = "2017-01-23", testCaseDescription = "Verify Send Reminder functionality for Alerts", testCaseId = "TC_Hub_Alert_Reports_02")
	private void reminderSendToDivUser() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubAlertsPage pobj = new TheHubAlertsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Division > Add Division");
			fc.utobj().printTestStep("Add Division");

			AdminDivisionAddDivisionPageTest addDivPage = new AdminDivisionAddDivisionPageTest();
			String divisionName = fc.utobj().generateTestData("TestDiv");
			addDivPage.addDivision(driver, divisionName);

			fc.utobj().printTestStep("Navigate To Admin > Users > Add Divisional User");
			fc.utobj().printTestStep("Add Divisional User");
			AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest divisionUserPage = new AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest();
			String divUserName = fc.utobj().generateTestData("Testdivuser");

			String emailId = "hubautomation@staffex.com";
			divisionUserPage.addDivisionalUser(driver, divUserName, "T0n1ght@123", divisionName, emailId);
			String divUserFullName = divUserName + " " + divUserName;

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUserPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = corporateUserPage.createDefaultUser(driver, corpUser);

			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login By newly Created Corporate User");
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());

			fc.utobj().printTestStep("Navigate To The Hub > Alerts");
			fc.utobj().printTestStep("Send Alert");

			String toEmail = divUserFullName;
			String subject = fc.utobj().generateTestData("Testsubj");
			String alertMessages = fc.utobj().generateTestData("Testmsg");
			String alertMessagesType = "Text";
			String recipientsUser = "Divisional Users";
			sendAlert(driver, toEmail, subject, alertMessagesType, alertMessages, recipientsUser, config);

			fc.utobj().printTestStep("Navigate To Hub > Alerts > Reports");
			fc.utobj().clickElement(driver, pobj.reportsTab);

			/*
			 * TC-9958
			 */

			fc.utobj().printTestStep("Verify Hub > Alerts > Reports Column");

			boolean isSubjectColumnPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//tr[@qat_maintable='withheader']//td[@class='thead headText12b']/a[contains(text(),'"
							+ fc.utobj().translateString("Subject") + "')]");
			boolean isDateSentColumnPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//tr[@qat_maintable='withheader']//td[@class='thead headText12b']/a[contains(text(),'"
							+ fc.utobj().translateString("Date Sent") + "')]");
			boolean isSenToColumnPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//tr[@qat_maintable='withheader']//td[@class='thead headText12b' and contains(text(),'"
							+ fc.utobj().translateString("Sent To") + "')]");
			boolean isOpenedColumnPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//tr[@qat_maintable='withheader']//td[@class='thead headText12b' and contains(text(),'"
							+ fc.utobj().translateString("Opened") + "')]");
			boolean isUnopenedColumnPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//tr[@qat_maintable='withheader']//td[@class='thead headText12b' and contains(text(),'"
							+ fc.utobj().translateString("Unopened") + "')]");

			if (isSubjectColumnPresent == false) {
				fc.utobj().throwsException("Subject column is not present");
			}

			if (isDateSentColumnPresent == false) {
				fc.utobj().throwsException("Date Sent column is not present");
			}

			if (isSenToColumnPresent == false) {
				fc.utobj().throwsException("Sen To column is not present");
			}

			if (isOpenedColumnPresent == false) {
				fc.utobj().throwsException("Opened column is not present");
			}

			if (isUnopenedColumnPresent == false) {
				fc.utobj().throwsException("Unopened column is not present");
			}

			fc.utobj().printTestStep("Search Alert with filter in reports tab");
			fc.utobj().selectDropDownByValue(driver, pobj.selectSubject, "Subject");
			fc.utobj().sendKeys(driver, pobj.subjectFilter, subject);
			fc.utobj().selectDropDownByValue(driver, pobj.selectSendDate, "49");
			fc.utobj().clickElement(driver, pobj.searchBtn);

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'" + subject + "')]"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.reminderBtn);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, pobj.sendBtn);

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login By newly created Divisional User");
			fc.loginpage().loginWithParameter(driver, divUserName, "T0n1ght@123");

			fc.utobj().printTestStep("Navigate To Hub > Alerts > Reports Tab");
			fc.hub().hub_common().theHubAlertsSubModule(driver);
			fc.utobj().clickElement(driver, pobj.reportsTab);

			fc.utobj().printTestStep("Search Alert with filter in reports tab");
			fc.utobj().selectDropDownByValue(driver, pobj.selectSubject, "Subject");
			fc.utobj().sendKeys(driver, pobj.subjectFilter, subject);
			fc.utobj().selectDropDownByValue(driver, pobj.selectSendDate, "49");
			fc.utobj().clickElement(driver, pobj.searchBtn);

			String xpath = "Reminder" + ": " + subject;

			boolean isAlertPresentArchived = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + xpath + "')]");
			if (!isAlertPresentArchived) {
				fc.utobj().throwsException("Reminder alert is not present in Reports Tab");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/*
	 * TC-9946
	 */

	@Test(groups = {"thehub","hub_alerts"})
	@TestCase(createdOn = "2018-01-24", updatedOn = "2018-01-24", testCaseDescription = "Verify the Hub View Alerts Section By Corporate User", testCaseId = "TC_Hub_Alerts_Filter_Value_01")
	private void verifyViewAlertFilterValue() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubAlertsPage pobj = new TheHubAlertsPage(driver);
			String emailId = "hubautomation@staffex.com";

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUserPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = corporateUserPage.createDefaultUser(driver, corpUser);

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With Newly Created Corporate User");
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());

			fc.utobj().printTestStep("Navigate To Hub > Alerts > View Alerts");
			fc.hub().hub_common().theHubAlertsSubModule(driver);
			fc.utobj().clickElement(driver, pobj.viewAlertsLink);

			fc.utobj().printTestStep("Verify The View Drop Down and Its default options");

			Select sl = new Select(fc.utobj().getElement(driver, pobj.viewSelect));
			List<WebElement> viewAllOptions = sl.getOptions();

			boolean b1[] = new boolean[4];

			for (WebElement webElement : viewAllOptions) {

				String options = webElement.getText();
				
				if (options!=null && !options.isEmpty()) {
					options=options.trim();
				}

				if (options.equalsIgnoreCase("Select")) {
					b1[0] = true;
				} else if (options.equalsIgnoreCase("Received")) {
					b1[1] = true;
				} else if (options.equalsIgnoreCase("Sent")) {
					b1[2] = true;
				} else if (options.equalsIgnoreCase("Deleted")) {
					b1[3] = true;
				}
			}

			if (b1[0] == false) {
				fc.utobj().throwsException("Select option is not present in view drop down");
			}
			if (b1[1] == false) {
				fc.utobj().throwsException("Received option is not present in view drop down");
			}
			if (b1[2] == false) {
				fc.utobj().throwsException("Sent option is not present in view drop down");
			}
			if (b1[3] == false) {
				fc.utobj().throwsException("Deleted option is not present in view drop down");
			}

			fc.utobj().printTestStep("Verify The Search By User Text box");
			boolean isSearchByUserPresent = fc.utobj().isElementPresent(driver, pobj.searchByUser);
			if (isSearchByUserPresent == false) {
				fc.utobj().throwsException("Search By User Text Box is not present");
			}

			fc.utobj().printTestStep("Verify The Search Alerts with drop down and its options");
			Select sl1 = new Select(pobj.searchAlertWithSelect);
			List<WebElement> viewAllOptions1 = sl1.getOptions();

			boolean b2[] = new boolean[3];

			for (WebElement webElement : viewAllOptions1) {

				String options = webElement.getText();
				
				if (options!=null && !options.isEmpty()) {
					options=options.trim();
				}
				

				if (options.equalsIgnoreCase(fc.utobj().translateString("Subject"))) {
					b2[0] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Text"))) {
					b2[1] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Both"))) {
					b2[2] = true;
				}
			}

			if (b2[0] == false) {
				fc.utobj().throwsException("Subject option is not present in Search Alerts With drop down");
			}
			if (b2[1] == false) {
				fc.utobj().throwsException("Text option is not present in Search Alerts With drop down");
			}
			if (b2[2] == false) {
				fc.utobj().throwsException("Both option is not present in Search Alerts With drop down");
			}

			boolean isSearchAlertsWithTextBox = fc.utobj().isElementPresent(driver, pobj.searchAlertWithTxBx);

			if (isSearchAlertsWithTextBox == false) {
				fc.utobj().throwsException("Search alert with text box is not present");
			}

			fc.utobj().printTestStep("Verify The Send Date drop down and its options");
			Select sl2 = new Select(pobj.selectSendDate);
			List<WebElement> viewAllOptions2 = sl2.getOptions();

			boolean b3[] = new boolean[15];

			for (WebElement webElement : viewAllOptions2) {

				String options = webElement.getText();
				
				if (options!=null && !options.isEmpty()) {
					options=options.trim();
				}

				if (options.equalsIgnoreCase(fc.utobj().translateString("After"))) {
					b3[0] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("All"))) {
					b3[1] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Before"))) {
					b3[2] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Current Month"))) {
					b3[3] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Current Year"))) {
					b3[4] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Is Between"))) {
					b3[5] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Last 12 Months"))) {
					b3[6] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Last 3 Months"))) {
					b3[7] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Last 30 Days"))) {
					b3[8] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Last 7 Days"))) {
					b3[9] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Last Month"))) {
					b3[10] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Last Year"))) {
					b3[11] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("On"))) {
					b3[12] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Today"))) {
					b3[13] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Yesterday"))) {
					b3[14] = true;
				}
			}

			if (b3[0] == false) {
				fc.utobj().throwsException("After option is not present in Send Date drop down");
			}
			if (b3[1] == false) {
				fc.utobj().throwsException("All option is not present in Send Date drop down");
			}
			if (b3[2] == false) {
				fc.utobj().throwsException("Before option is not present in Send Date drop down");
			}
			if (b3[3] == false) {
				fc.utobj().throwsException("Current Month option is not present in Send Date drop down");
			}
			if (b3[4] == false) {
				fc.utobj().throwsException("Current Year option is not present in Send Date drop down");
			}
			if (b3[5] == false) {
				fc.utobj().throwsException("Is Between option is not present in Send Date drop down");
			}
			if (b3[6] == false) {
				fc.utobj().throwsException("Last 12 Months option is not present in Send Date drop down");
			}
			if (b3[7] == false) {
				fc.utobj().throwsException("Last 3 Months option is not present in Send Date drop down");
			}
			if (b3[8] == false) {
				fc.utobj().throwsException("Last 30 Days option is not present in Send Date drop down");
			}
			if (b3[9] == false) {
				fc.utobj().throwsException("Last 7 Days option is not present in Send Date drop down");
			}
			if (b3[10] == false) {
				fc.utobj().throwsException("Last Month option is not present in Send Date drop down");
			}
			if (b3[11] == false) {
				fc.utobj().throwsException("Last Year option is not present in Send Date drop down");
			}
			if (b3[12] == false) {
				fc.utobj().throwsException("On option is not present in Send Date drop down");
			}
			if (b3[13] == false) {
				fc.utobj().throwsException("Today option is not present in Send Date drop down");
			}
			if (b3[14] == false) {
				fc.utobj().throwsException("Yesterday option is not present in Send Date drop down");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"thehub","hub_alerts"})
	@TestCase(createdOn = "2018-01-24", updatedOn = "2018-01-24", testCaseDescription = "Verify the Hub View Alerts Section By Divisional User", testCaseId = "TC_Hub_Alerts_Filter_Value_02")
	private void verifyViewAlertFilterValue01() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubAlertsPage pobj = new TheHubAlertsPage(driver);
			String emailId = "hubautomation@staffex.com";
			String password = "t0n1ght@123";
			/*
			 * Div User
			 */
			fc.utobj().printTestStep("Navigate To Admin > Division > Add Division");
			fc.utobj().printTestStep("Add Division");

			AdminDivisionAddDivisionPageTest addDivPage = new AdminDivisionAddDivisionPageTest();
			String divisionName = fc.utobj().generateTestData("testdiv");
			addDivPage.addDivision(driver, divisionName);

			fc.utobj().printTestStep("Navigate To Admin > Users > Add Divisional User");
			fc.utobj().printTestStep("Add Divisional User");
			AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest divisionUserPage = new AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest();
			String devUserName = fc.utobj().generateTestData("devuser");
			divisionUserPage.addDivisionalUser(driver, devUserName, password, divisionName, emailId);

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With Newly Created divisional User");
			fc.loginpage().loginWithParameter(driver, devUserName, password);

			fc.utobj().printTestStep("Navigate To Hub > Alerts > View Alerts");
			fc.hub().hub_common().theHubAlertsSubModule(driver);

			/*
			 * TC-9944
			 */

			fc.utobj().printTestStep("Default sub tab of alert should be present");

			fc.utobj().printTestStep("Verify The View Alerts Tabs should be present");
			boolean isViewAlertTab = fc.utobj()
					.isElementPresent(driver, fc.utobj().getElement(driver, pobj.viewAlertsLink));

			if (isViewAlertTab == false) {
				fc.utobj().throwsException("View Alerts tab is not present");
			}

			fc.utobj().printTestStep("Verify The Archive Alerts Tabs should be present");
			boolean isArchiveAlert = fc.utobj()
					.isElementPresent(driver, fc.utobj().getElement(driver, pobj.archiveAlertLink));

			if (isArchiveAlert == false) {
				fc.utobj().throwsException("Archive Alert tab is not present");
			}

			fc.utobj().printTestStep("Verify The Reports Tabs should be present");
			boolean isReportsTab = fc.utobj().isElementPresent(driver, fc.utobj().getElement(driver, pobj.reportsTab));

			if (isReportsTab == false) {
				fc.utobj().throwsException("Reports tab is not present");
			}

			fc.utobj().printTestStep("Verify The Send Alerts Tabs should be present");
			boolean isSendAlert = fc.utobj()
					.isElementPresent(driver, fc.utobj().getElement(driver, pobj.sendAlertLink));

			if (isSendAlert == false) {
				fc.utobj().throwsException("Archive Alert tab is not present");
			}

			fc.utobj().printTestStep("Verify The Archive Alert Reports Tabs should be present");
			boolean isArchivedReportsTab = fc.utobj()
					.isElementPresent(driver, fc.utobj().getElement(driver, pobj.archiveAlertReportsLink));

			if (isArchivedReportsTab == false) {
				fc.utobj().throwsException("Archive Alert Reports tab is not present");
			}

			fc.utobj().printTestStep("Verify Groups Tabs should be present");
			boolean isGroupsTab = fc.utobj()
					.isElementPresent(driver, fc.utobj().getElement(driver, new TheHubDirectoryPage(driver).groupsTab));

			if (isGroupsTab == false) {
				fc.utobj().throwsException("Groups tab is not present");
			}

			fc.utobj().clickElement(driver, pobj.viewAlertsLink);

			fc.utobj().printTestStep("Verify The View Drop Down and Its default options");

			Select sl = new Select(pobj.viewSelect);
			List<WebElement> viewAllOptions = sl.getOptions();

			boolean b1[] = new boolean[4];

			for (WebElement webElement : viewAllOptions) {

				String options = webElement.getText();
				
				if (options!=null && !options.isEmpty()) {
					options=options.trim();
				}

				if (options.equalsIgnoreCase(fc.utobj().translateString("Select"))) {
					b1[0] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Received"))) {
					b1[1] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Sent"))) {
					b1[2] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Deleted"))) {
					b1[3] = true;
				}
			}

			if (b1[0] == false) {
				fc.utobj().throwsException("Select option is not present in view drop down");
			}
			if (b1[1] == false) {
				fc.utobj().throwsException("Received option is not present in view drop down");
			}
			if (b1[2] == false) {
				fc.utobj().throwsException("Sent option is not present in view drop down");
			}
			if (b1[3] == false) {
				fc.utobj().throwsException("Deleted option is not present in view drop down");
			}

			fc.utobj().printTestStep("Verify The Search By User Text box");
			boolean isSearchByUserPresent = fc.utobj().isElementPresent(driver, pobj.searchByUser);
			if (isSearchByUserPresent == false) {
				fc.utobj().throwsException("Search By User Text Box is not present");
			}

			fc.utobj().printTestStep("Verify The Search Alerts with drop down and its options");
			Select sl1 = new Select(pobj.searchAlertWithSelect);
			List<WebElement> viewAllOptions1 = sl1.getOptions();

			boolean b2[] = new boolean[3];

			for (WebElement webElement : viewAllOptions1) {

				String options = webElement.getText();
				
				if (options!=null && !options.isEmpty()) {
					options=options.trim();
				}

				if (options.equalsIgnoreCase(fc.utobj().translateString("Subject"))) {
					b2[0] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Text"))) {
					b2[1] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Both"))) {
					b2[2] = true;
				}
			}

			if (b2[0] == false) {
				fc.utobj().throwsException("Subject option is not present in Search Alerts With drop down");
			}
			if (b2[1] == false) {
				fc.utobj().throwsException("Text option is not present in Search Alerts With drop down");
			}
			if (b2[2] == false) {
				fc.utobj().throwsException("Both option is not present in Search Alerts With drop down");
			}

			boolean isSearchAlertsWithTextBox = fc.utobj().isElementPresent(driver, pobj.searchAlertWithTxBx);

			if (isSearchAlertsWithTextBox == false) {
				fc.utobj().throwsException("Search alert with text box is not present");
			}

			fc.utobj().printTestStep("Verify The Send Date drop down and its options");
			Select sl2 = new Select(pobj.selectSendDate);
			List<WebElement> viewAllOptions2 = sl2.getOptions();

			boolean b3[] = new boolean[15];

			for (WebElement webElement : viewAllOptions2) {

				String options = webElement.getText();
				
				if (options!=null && !options.isEmpty()) {
					options=options.trim();
				}

				if (options.equalsIgnoreCase(fc.utobj().translateString("After"))) {
					b3[0] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("All"))) {
					b3[1] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Before"))) {
					b3[2] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Current Month"))) {
					b3[3] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Current Year"))) {
					b3[4] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Is Between"))) {
					b3[5] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Last 12 Months"))) {
					b3[6] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Last 3 Months"))) {
					b3[7] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Last 30 Days"))) {
					b3[8] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Last 7 Days"))) {
					b3[9] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Last Month"))) {
					b3[10] = true;
				} else if (options.equalsIgnoreCase("Last Year")) {
					b3[11] = true;
				} else if (options.equalsIgnoreCase("On")) {
					b3[12] = true;
				} else if (options.equalsIgnoreCase("Today")) {
					b3[13] = true;
				} else if (options.equalsIgnoreCase("Yesterday")) {
					b3[14] = true;
				}
			}

			if (b3[0] == false) {
				fc.utobj().throwsException("After option is not present in Send Date drop down");
			}
			if (b3[1] == false) {
				fc.utobj().throwsException("All option is not present in Send Date drop down");
			}
			if (b3[2] == false) {
				fc.utobj().throwsException("Before option is not present in Send Date drop down");
			}
			if (b3[3] == false) {
				fc.utobj().throwsException("Current Month option is not present in Send Date drop down");
			}
			if (b3[4] == false) {
				fc.utobj().throwsException("Current Year option is not present in Send Date drop down");
			}
			if (b3[5] == false) {
				fc.utobj().throwsException("Is Between option is not present in Send Date drop down");
			}
			if (b3[6] == false) {
				fc.utobj().throwsException("Last 12 Months option is not present in Send Date drop down");
			}
			if (b3[7] == false) {
				fc.utobj().throwsException("Last 3 Months option is not present in Send Date drop down");
			}
			if (b3[8] == false) {
				fc.utobj().throwsException("Last 30 Days option is not present in Send Date drop down");
			}
			if (b3[9] == false) {
				fc.utobj().throwsException("Last 7 Days option is not present in Send Date drop down");
			}
			if (b3[10] == false) {
				fc.utobj().throwsException("Last Month option is not present in Send Date drop down");
			}
			if (b3[11] == false) {
				fc.utobj().throwsException("Last Year option is not present in Send Date drop down");
			}
			if (b3[12] == false) {
				fc.utobj().throwsException("On option is not present in Send Date drop down");
			}
			if (b3[13] == false) {
				fc.utobj().throwsException("Today option is not present in Send Date drop down");
			}
			if (b3[14] == false) {
				fc.utobj().throwsException("Yesterday option is not present in Send Date drop down");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"thehub","hub_alerts"})
	@TestCase(createdOn = "2018-01-24", updatedOn = "2018-01-24", testCaseDescription = "Verify the Hub View Alerts Section By Regional User", testCaseId = "TC_Hub_Alerts_Filter_Value_03")
	private void verifyViewAlertFilterValue02() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubAlertsPage pobj = new TheHubAlertsPage(driver);
			String emailId = "hubautomation@staffex.com";
			String password = "t0n1ght@123";

			/*
			 * Regional User
			 */
			fc.utobj().printTestStep("Navigate To Admin > Add Area/Region");
			fc.utobj().printTestStep("Add Region");

			AdminAreaRegionAddAreaRegionPageTest addRegionPage = new AdminAreaRegionAddAreaRegionPageTest();
			String regionName = fc.utobj().generateTestData("Testreg");
			addRegionPage.addAreaRegion(driver, regionName);

			fc.utobj().printTestStep("Add Regional User");
			AdminUsersManageRegionalUsersAddRegionalUserPageTest regionalUserPage = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
			String regUserName = fc.utobj().generateTestData("Testreguser");
			regionalUserPage.addRegionalUser(driver, regUserName, password, regionName, emailId);

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With Newly Created Regional User");
			fc.loginpage().loginWithParameter(driver, regUserName, password);

			fc.utobj().printTestStep("Navigate To Hub > Alerts > View Alerts");
			fc.hub().hub_common().theHubAlertsSubModule(driver);

			/*
			 * TC-9945
			 */

			fc.utobj().printTestStep("Verify The View Alerts Tabs should be present");
			boolean isViewAlertTab = fc.utobj()
					.isElementPresent(driver, fc.utobj().getElement(driver, pobj.viewAlertsLink));

			if (isViewAlertTab == false) {
				fc.utobj().throwsException("View Alerts tab is not present");
			}

			fc.utobj().printTestStep("Verify The Archive Alerts Tabs should be present");
			boolean isArchiveAlert = fc.utobj()
					.isElementPresent(driver, fc.utobj().getElement(driver, pobj.archiveAlertLink));

			if (isArchiveAlert == false) {
				fc.utobj().throwsException("Archive Alert tab is not present");
			}

			fc.utobj().clickElement(driver, pobj.viewAlertsLink);

			fc.utobj().printTestStep("Verify The Search By User Text box");
			boolean isSearchByUserPresent = fc.utobj().isElementPresent(driver, pobj.searchByUser);
			if (isSearchByUserPresent == false) {
				fc.utobj().throwsException("Search By User Text Box is not present");
			}

			fc.utobj().printTestStep("Verify The Search Alerts with drop down and its options");
			Select sl1 = new Select(pobj.searchAlertWithSelect);
			List<WebElement> viewAllOptions1 = sl1.getOptions();

			boolean b2[] = new boolean[3];

			for (WebElement webElement : viewAllOptions1) {

				String options = webElement.getText();
				
				if (options!=null && !options.isEmpty()) {
					options=options.trim();
				}

				if (options.equalsIgnoreCase(fc.utobj().translateString("Subject"))) {
					b2[0] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Text"))) {
					b2[1] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Both"))) {
					b2[2] = true;
				}
			}

			if (b2[0] == false) {
				fc.utobj().throwsException("Subject option is not present in Search Alerts With drop down");
			}
			if (b2[1] == false) {
				fc.utobj().throwsException("Text option is not present in Search Alerts With drop down");
			}
			if (b2[2] == false) {
				fc.utobj().throwsException("Both option is not present in Search Alerts With drop down");
			}

			boolean isSearchAlertsWithTextBox = fc.utobj().isElementPresent(driver, pobj.searchAlertWithTxBx);

			if (isSearchAlertsWithTextBox == false) {
				fc.utobj().throwsException("Search alert with text box is not present");
			}

			fc.utobj().printTestStep("Verify The Send Date drop down and its options");
			Select sl2 = new Select(pobj.selectSendDate);
			List<WebElement> viewAllOptions2 = sl2.getOptions();

			boolean b3[] = new boolean[15];

			for (WebElement webElement : viewAllOptions2) {

				String options = webElement.getText();
				
				if (options!=null && !options.isEmpty()) {
					options=options.trim();
				}

				if (options.equalsIgnoreCase(fc.utobj().translateString("After"))) {
					b3[0] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("All"))) {
					b3[1] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Before"))) {
					b3[2] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Current Month"))) {
					b3[3] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Current Year"))) {
					b3[4] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Is Between"))) {
					b3[5] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Last 12 Months"))) {
					b3[6] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Last 3 Months"))) {
					b3[7] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Last 30 Days"))) {
					b3[8] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Last 7 Days"))) {
					b3[9] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Last Month"))) {
					b3[10] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Last Year"))) {
					b3[11] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("On"))) {
					b3[12] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Today"))) {
					b3[13] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Yesterday"))) {
					b3[14] = true;
				}
			}

			if (b3[0] == false) {
				fc.utobj().throwsException("After option is not present in Send Date drop down");
			}
			if (b3[1] == false) {
				fc.utobj().throwsException("All option is not present in Send Date drop down");
			}
			if (b3[2] == false) {
				fc.utobj().throwsException("Before option is not present in Send Date drop down");
			}
			if (b3[3] == false) {
				fc.utobj().throwsException("Current Month option is not present in Send Date drop down");
			}
			if (b3[4] == false) {
				fc.utobj().throwsException("Current Year option is not present in Send Date drop down");
			}
			if (b3[5] == false) {
				fc.utobj().throwsException("Is Between option is not present in Send Date drop down");
			}
			if (b3[6] == false) {
				fc.utobj().throwsException("Last 12 Months option is not present in Send Date drop down");
			}
			if (b3[7] == false) {
				fc.utobj().throwsException("Last 3 Months option is not present in Send Date drop down");
			}
			if (b3[8] == false) {
				fc.utobj().throwsException("Last 30 Days option is not present in Send Date drop down");
			}
			if (b3[9] == false) {
				fc.utobj().throwsException("Last 7 Days option is not present in Send Date drop down");
			}
			if (b3[10] == false) {
				fc.utobj().throwsException("Last Month option is not present in Send Date drop down");
			}
			if (b3[11] == false) {
				fc.utobj().throwsException("Last Year option is not present in Send Date drop down");
			}
			if (b3[12] == false) {
				fc.utobj().throwsException("On option is not present in Send Date drop down");
			}
			if (b3[13] == false) {
				fc.utobj().throwsException("Today option is not present in Send Date drop down");
			}
			if (b3[14] == false) {
				fc.utobj().throwsException("Yesterday option is not present in Send Date drop down");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"thehub","hub_alerts"})
	@TestCase(createdOn = "2018-01-24", updatedOn = "2018-01-24", testCaseDescription = "Verify the Hub View Alerts Section By Franchise User", testCaseId = "TC_Hub_Alerts_Filter_Value_04")
	private void verifyViewAlertFilterValue03() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubAlertsPage pobj = new TheHubAlertsPage(driver);
			String emailId = "hubautomation@staffex.com";
			String password = "t0n1ght@123";

			fc.utobj().printTestStep("Add Franchise Location");
			String storeType = "Test127StoreType".concat(fc.utobj().generateRandomNumber());
			String franchiseId = "Test127FranId".concat(fc.utobj().generateRandomNumber());

			AdminFranchiseLocationAddFranchiseLocationPageTest addFranchiseLocation = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String firstName = fc.utobj().generateTestData("Testfname");
			String franRegionName = fc.utobj().generateTestData("Testfreg");
			franchiseId = addFranchiseLocation.addFranchiseLocation_AllWithFName(driver, franchiseId, franRegionName,
					storeType, firstName);

			fc.utobj().printTestStep("Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest addFranchiseUser = new AdminUsersManageManageFranchiseUsersPageTest();
			String roleName = "Default Franchise Role";
			String userNameFran = fc.utobj().generateTestData("Testfranuser");
			addFranchiseUser.addFranchiseUser(driver, userNameFran, password, franchiseId, roleName, emailId);

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With Newly Created Franchise User");
			fc.loginpage().loginWithParameter(driver, userNameFran, password);

			fc.utobj().printTestStep("Navigate To Hub > Alerts > View Alerts");
			fc.hub().hub_common().theHubAlertsSubModule(driver);

			/*
			 * TC-9945
			 */

			fc.utobj().printTestStep("Verify The View Alerts Tabs should be present");
			boolean isViewAlertTab = fc.utobj()
					.isElementPresent(driver, fc.utobj().getElement(driver, pobj.viewAlertsLink));

			if (isViewAlertTab == false) {
				fc.utobj().throwsException("View Alerts tab is not present");
			}

			fc.utobj().printTestStep("Verify The Archive Alerts Tabs should be present");
			boolean isArchiveAlert = fc.utobj()
					.isElementPresent(driver, fc.utobj().getElement(driver, pobj.archiveAlertLink));

			if (isArchiveAlert == false) {
				fc.utobj().throwsException("Archive Alert tab is not present");
			}

			fc.utobj().clickElement(driver, pobj.viewAlertsLink);

			fc.utobj().printTestStep("Verify The Search By User Text box");
			boolean isSearchByUserPresent = fc.utobj().isElementPresent(driver, pobj.searchByUser);
			if (isSearchByUserPresent == false) {
				fc.utobj().throwsException("Search By User Text Box is not present");
			}

			fc.utobj().printTestStep("Verify The Search Alerts with drop down and its options");
			Select sl1 = new Select(pobj.searchAlertWithSelect);
			List<WebElement> viewAllOptions1 = sl1.getOptions();

			boolean b2[] = new boolean[3];

			for (WebElement webElement : viewAllOptions1) {

				String options = webElement.getText();
				
				if (options!=null && !options.isEmpty()) {
					options=options.trim();
				}

				if (options.equalsIgnoreCase(fc.utobj().translateString("Subject"))) {
					b2[0] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Text"))) {
					b2[1] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Both"))) {
					b2[2] = true;
				}
			}

			if (b2[0] == false) {
				fc.utobj().throwsException("Subject option is not present in Search Alerts With drop down");
			}
			if (b2[1] == false) {
				fc.utobj().throwsException("Text option is not present in Search Alerts With drop down");
			}
			if (b2[2] == false) {
				fc.utobj().throwsException("Both option is not present in Search Alerts With drop down");
			}

			boolean isSearchAlertsWithTextBox = fc.utobj().isElementPresent(driver, pobj.searchAlertWithTxBx);

			if (isSearchAlertsWithTextBox == false) {
				fc.utobj().throwsException("Search alert with text box is not present");
			}

			fc.utobj().printTestStep("Verify The Send Date drop down and its options");
			Select sl2 = new Select(pobj.selectSendDate);
			List<WebElement> viewAllOptions2 = sl2.getOptions();

			boolean b3[] = new boolean[15];

			for (WebElement webElement : viewAllOptions2) {

				String options = webElement.getText();
				
				if (options!=null && !options.isEmpty()) {
					options=options.trim();
				}

				if (options.equalsIgnoreCase(fc.utobj().translateString("After"))) {
					b3[0] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("All"))) {
					b3[1] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Before"))) {
					b3[2] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Current Month"))) {
					b3[3] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Current Year"))) {
					b3[4] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Is Between"))) {
					b3[5] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Last 12 Months"))) {
					b3[6] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Last 3 Months"))) {
					b3[7] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Last 30 Days"))) {
					b3[8] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Last 7 Days"))) {
					b3[9] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Last Month"))) {
					b3[10] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Last Year"))) {
					b3[11] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("On"))) {
					b3[12] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Today"))) {
					b3[13] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Yesterday"))) {
					b3[14] = true;
				}
			}

			if (b3[0] == false) {
				fc.utobj().throwsException("After option is not present in Send Date drop down");
			}
			if (b3[1] == false) {
				fc.utobj().throwsException("All option is not present in Send Date drop down");
			}
			if (b3[2] == false) {
				fc.utobj().throwsException("Before option is not present in Send Date drop down");
			}
			if (b3[3] == false) {
				fc.utobj().throwsException("Current Month option is not present in Send Date drop down");
			}
			if (b3[4] == false) {
				fc.utobj().throwsException("Current Year option is not present in Send Date drop down");
			}
			if (b3[5] == false) {
				fc.utobj().throwsException("Is Between option is not present in Send Date drop down");
			}
			if (b3[6] == false) {
				fc.utobj().throwsException("Last 12 Months option is not present in Send Date drop down");
			}
			if (b3[7] == false) {
				fc.utobj().throwsException("Last 3 Months option is not present in Send Date drop down");
			}
			if (b3[8] == false) {
				fc.utobj().throwsException("Last 30 Days option is not present in Send Date drop down");
			}
			if (b3[9] == false) {
				fc.utobj().throwsException("Last 7 Days option is not present in Send Date drop down");
			}
			if (b3[10] == false) {
				fc.utobj().throwsException("Last Month option is not present in Send Date drop down");
			}
			if (b3[11] == false) {
				fc.utobj().throwsException("Last Year option is not present in Send Date drop down");
			}
			if (b3[12] == false) {
				fc.utobj().throwsException("On option is not present in Send Date drop down");
			}
			if (b3[13] == false) {
				fc.utobj().throwsException("Today option is not present in Send Date drop down");
			}
			if (b3[14] == false) {
				fc.utobj().throwsException("Yesterday option is not present in Send Date drop down");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void createNewGroupAtAlerts(WebDriver driver, String groupName, String userName, Map<String, String> config)
			throws Exception {

		String testCaseId = "TC_Create_New_Group_The_Hub_Alert";

		if (fc.utobj().validate(testCaseId)) {
			try {
				TheHubDirectoryPage pobj = new TheHubDirectoryPage(driver);
				fc.hub().hub_common().theHubAlertsSubModule(driver);
				fc.utobj().clickElement(driver, pobj.groupsTab);
				fc.utobj().clickElement(driver, pobj.createNewGroupLink);
				fc.utobj().clickElement(driver, pobj.internalAddressBook);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().sendKeys(driver, pobj.searchTxBxCU, userName);
				fc.utobj().clickElement(driver, pobj.searchCUImgIcon);
				pobj = new TheHubDirectoryPage(driver);
				
				/*fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[contains(text () , '" + userName + "')]/ancestor::tr/td/input"));*/
				fc.utobj().sleep();
				fc.utobj().clickElement(driver, pobj.selectcorporate);
				fc.utobj().clickElement(driver, pobj.selectUser);
				fc.utobj().switchFrameToDefault(driver);
				fc.utobj().sendKeys(driver, pobj.grouopName, groupName);
				fc.utobj().clickElement(driver, pobj.createBtn);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to Create New Group :: The Hub > Directory");

		}
	}

	public void sendAlert(WebDriver driver, String toEmail, String subject, String alertMessagesType,
			String alertMessages, String recipientsUser, Map<String, String> config) throws Exception {

		String testCaseId = "TC_Send_Alert_The_Hub_Alert";

		if (fc.utobj().validate(testCaseId)) {
			try {
				TheHubAlertsPage pobj = new TheHubAlertsPage(driver);
				fc.hub().hub_common().theHubAlertsSubModule(driver);
				fc.utobj().clickElement(driver, pobj.sendAlertLink);

				fc.utobj().clickElement(driver, pobj.addressBook);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().setToDefault(driver, pobj.selectUsers);
				fc.utobj().selectValFromMultiSelect(driver, pobj.selectUsers, recipientsUser);
				fc.utobj().clickElement(driver, pobj.submitOk);
				fc.utobj().sendKeys(driver, pobj.searchTextBx, toEmail);
				fc.utobj().clickElement(driver, pobj.searchImgBtn);
				
				fc.utobj().sleep();
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[contains(text () , '" + toEmail + "')]/ancestor::tr/td/input"));
				fc.utobj().clickElement(driver, pobj.addUserBtn);
				fc.utobj().switchFrameToDefault(driver);

				fc.utobj().sendKeys(driver, pobj.subject, subject);

				if (alertMessagesType.equalsIgnoreCase("Text")) {

					fc.utobj().moveToElement(driver, pobj.texthtmlOption.get(0));
					if (!fc.utobj().isSelected(driver,pobj.texthtmlOption.get(0))) {
						fc.utobj().clickElement(driver, pobj.texthtmlOption.get(0));
					}
					fc.utobj().sendKeys(driver, pobj.alertMessages, alertMessages);
				} else if (alertMessagesType.equalsIgnoreCase("Html")) {

				}
				fc.utobj().clickElement(driver, pobj.sendBtn);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to send Alert Page");

		}
	}
}
