package com.builds.test.thehub;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.builds.uimaps.thehub.TheHubAlertsPage;
import com.builds.uimaps.thehub.TheHubCalendarPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class TheHubCalendarPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = {"thehub" ,"hub_calendar"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Create Appoinment At The Hub > Calendar", testCaseId = "TC_172_Create_Appointment")
	public void createAppointment() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubCalendarPage pobj = new TheHubCalendarPage(driver);

			fc.utobj().printTestStep("Navigate To The Hub > Calendar Page");
			fc.utobj().printTestStep("Create Appointment");
			fc.hub().hub_common().theHubCalendarSubModule(driver);
			fc.utobj().selectDropDown(driver, pobj.calendarUserSelect, "FranConnect Administrator");
			fc.utobj().clickElement(driver, pobj.createLink);
			fc.utobj().selectDropDown(driver, pobj.scheduleSelect, "Appointment");
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subject, subject);
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().sendKeys(driver, pobj.description, description);
			fc.utobj().sendKeys(driver, pobj.startDatetime, fc.utobj().getCurrentDateUSFormat());
			fc.utobj().sendKeys(driver, pobj.endDatetime, fc.utobj().getFutureDateUSFormat(2));
			fc.utobj().sendKeys(driver, pobj.location, "TestLocation");
			fc.utobj().selectDropDown(driver, pobj.priority, "Low");
			fc.utobj().selectDropDown(driver, pobj.scheduleScope, "Yes");
			fc.utobj().clickElement(driver, pobj.createBtn);

			fc.utobj().clickElement(driver, pobj.searchBtn);
			fc.utobj().selectDropDown(driver, pobj.scheduleType, "Appointment");
			fc.utobj().sendKeys(driver, pobj.searchWords, subject);
			if (!fc.utobj().isSelected(driver, pobj.lookInSubCheck)) {
				fc.utobj().clickElement(driver, pobj.lookInSubCheck);
			}
			fc.utobj().clickElement(driver, pobj.searchBtnAtSearch);

			fc.utobj().printTestStep("Verify The Create Appointment");
			boolean isSubjectPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + subject + "']");
			if (isSubjectPresent == false) {
				fc.utobj().throwsException("was not able to verify Create Appointment");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void createAppointment(WebDriver driver, String subject, String description) throws Exception {

		String testCaseId = "TC_Create_Appointment_TheHub_Calander";

		if (fc.utobj().validate(testCaseId)) {
			try {
				TheHubCalendarPage pobj = new TheHubCalendarPage(driver);
				fc.hub().hub_common().theHubCalendarSubModule(driver);
				// fc.utobj().selectDropDown(driver, pobj.calendarUserSelect,
				// "FranConnect Administrator");
				fc.utobj().clickElement(driver, pobj.createLink);
				fc.utobj().selectDropDown(driver, pobj.scheduleSelect, "Appointment");
				fc.utobj().sendKeys(driver, pobj.subject, subject);
				fc.utobj().sendKeys(driver, pobj.description, description);
				fc.utobj().sendKeys(driver, pobj.startDatetime,
						fc.utobj().getCurrentDateUserDefineTimeZoneUSFormat("GMT-5:00"));
				fc.utobj().sendKeys(driver, pobj.endDatetime, fc.utobj().getFutureDateUSFormat(2));
				fc.utobj().sendKeys(driver, pobj.location, "TestLocation");
				fc.utobj().selectDropDown(driver, pobj.priority, "Low");
				fc.utobj().selectDropDown(driver, pobj.scheduleScope, "Yes");
				fc.utobj().clickElement(driver, pobj.createBtn);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to Create Appointment");

		}
	}

	@Test(groups = {"thehub","thenewgui","hub_calendar"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Modify Appoinment At The Hub > Calendar", testCaseId = "TC_173_Modify_Appointment")
	private void modifyAppoinment() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubCalendarPage pobj = new TheHubCalendarPage(driver);

			fc.utobj().printTestStep("Navigate To The Hub > Calendar Page");
			fc.utobj().printTestStep("Create Appointment");
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			createAppointment(driver, subject, description);

			fc.utobj().clickElement(driver, pobj.searchBtn);
			fc.utobj().selectDropDown(driver, pobj.scheduleType, "Appointment");
			fc.utobj().sendKeys(driver, pobj.searchWords, subject);
			if (!fc.utobj().isSelected(driver, pobj.lookInSubCheck)) {
				fc.utobj().clickElement(driver, pobj.lookInSubCheck);
			}
			fc.utobj().clickElement(driver, pobj.searchBtnAtSearch);

			fc.utobj().printTestStep("Modify The Appointment");
			fc.utobj().actionImgOption(driver, subject, "Modify");

			subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subject, subject);
			description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().sendKeys(driver, pobj.description, description);
			fc.utobj().sendKeys(driver, pobj.startDatetime, fc.utobj().getCurrentDateUSFormat());
			fc.utobj().sendKeys(driver, pobj.endDatetime, fc.utobj().getFutureDateUSFormat(3));
			fc.utobj().sendKeys(driver, pobj.location, "TestLocation");
			fc.utobj().selectDropDown(driver, pobj.priority, "Low");
			fc.utobj().selectDropDown(driver, pobj.scheduleScope, "Yes");
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("Verify The Modify Appointment");

			fc.utobj().clickElement(driver, pobj.searchBtn);
			fc.utobj().selectDropDown(driver, pobj.scheduleType, "Appointment");
			fc.utobj().sendKeys(driver, pobj.searchWords, subject);
			if (!fc.utobj().isSelected(driver, pobj.lookInSubCheck)) {
				fc.utobj().clickElement(driver, pobj.lookInSubCheck);
			}
			fc.utobj().clickElement(driver, pobj.searchBtnAtSearch);

			boolean isSubjectPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + subject + "']");
			if (isSubjectPresent == false) {
				fc.utobj().throwsException("was not able to verify Modify Appointment");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"thehub","hub_calendar"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Create Meeting At The Hub > Calendar", testCaseId = "TC_174_Create_Meeting")
	private void createMeeting() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubCalendarPage pobj = new TheHubCalendarPage(driver);

			fc.utobj().printTestStep("Navigate To The Hub > Calendar Page");
			fc.utobj().printTestStep("Create Meeting");
			fc.hub().hub_common().theHubCalendarSubModule(driver);
			fc.utobj().selectDropDown(driver, pobj.calendarUserSelect, "FranConnect Administrator");
			fc.utobj().clickElement(driver, pobj.createLink);
			fc.utobj().selectDropDown(driver, pobj.scheduleSelect, "Meeting");
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subject, subject);
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().sendKeys(driver, pobj.description, description);
			fc.utobj().selectDropDown(driver, pobj.scheduleScope, "Private");
			fc.utobj().sendKeys(driver, pobj.startDatetime, fc.utobj().getCurrentDateUSFormat());
			fc.utobj().sendKeys(driver, pobj.endDatetime, fc.utobj().getFutureDateUSFormat(2));
			fc.utobj().sendKeys(driver, pobj.location, "TestLocation");
			fc.utobj().selectDropDown(driver, pobj.priority, "Low");
			fc.utobj().clickElement(driver, pobj.createBtn);

			fc.utobj().printTestStep("Verify The Create Meeting");

			fc.utobj().clickElement(driver, pobj.searchBtn);
			fc.utobj().selectDropDown(driver, pobj.scheduleType, "Meeting");
			fc.utobj().sendKeys(driver, pobj.searchWords, subject);
			if (!fc.utobj().isSelected(driver, pobj.lookInSubCheck)) {
				fc.utobj().clickElement(driver, pobj.lookInSubCheck);
			}
			fc.utobj().clickElement(driver, pobj.searchBtnAtSearch);

			boolean isSubjectPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + subject + "']");
			if (isSubjectPresent == false) {
				fc.utobj().throwsException("was not able to verify Create Meeting");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void createMeeting(WebDriver driver, String subject, String description, Map<String, String> dataSet)
			throws Exception {

		String testCaseId = "TC_Create_Meeting_TheHub_Calander";

		if (fc.utobj().validate(testCaseId)) {
			try {
				TheHubCalendarPage pobj = new TheHubCalendarPage(driver);
				fc.hub().hub_common().theHubCalendarSubModule(driver);
				fc.utobj().selectDropDown(driver, pobj.calendarUserSelect, "FranConnect Administrator");
				fc.utobj().clickElement(driver, pobj.createLink);
				fc.utobj().selectDropDown(driver, pobj.scheduleSelect, "Meeting");
				fc.utobj().sendKeys(driver, pobj.subject, subject);
				fc.utobj().sendKeys(driver, pobj.description, description);

				if (dataSet.get("scheduleScope") != null) {
					if (dataSet.get("scheduleScope").equalsIgnoreCase("Group")) {
						fc.utobj().selectDropDown(driver, pobj.scheduleScope, dataSet.get("scheduleScope"));
						fc.utobj().printTestStep("Select Participants");
						fc.utobj().clickElement(driver,
								fc.utobj().getElementByXpath(driver, ".//*[@id='Participantdivlabel']/a"));

						TheHubAlertsPage alert_page = new TheHubAlertsPage(driver);
						fc.commonMethods().switch_cboxIframe_frameId(driver);
						fc.utobj().setToDefault(driver, alert_page.selectUsers);
						fc.utobj().selectValFromMultiSelect(driver, alert_page.selectUsers, "Corporate Users");
						fc.utobj().clickElement(driver, alert_page.submitOk);
						fc.utobj().sendKeys(driver, alert_page.searchTxBx, dataSet.get("customUserName"));
						fc.utobj().clickElement(driver, alert_page.searchImgBtn);

						fc.utobj().sleep();
						WebElement element = fc.utobj().getElementByXpath(driver, ".//*[contains(text () , '"
								+ dataSet.get("customUserName") + "')]/ancestor::tr/td/input");
						fc.utobj().clickElement(driver, element);
						fc.utobj().clickElement(driver, alert_page.addUserBtn);
						fc.utobj().switchFrameToDefault(driver);
					} else if (dataSet.get("scheduleScope").equalsIgnoreCase("Public")
							|| dataSet.get("scheduleScope").equalsIgnoreCase("Private")) {
						fc.utobj().selectDropDown(driver, pobj.scheduleScope, dataSet.get("scheduleScope"));
					}
				}
				fc.utobj().sendKeys(driver, pobj.startDatetime, fc.utobj().getCurrentDateUSFormat());
				fc.utobj().sendKeys(driver, pobj.endDatetime, fc.utobj().getFutureDateUSFormat(2));
				fc.utobj().sendKeys(driver, pobj.location, "TestLocation");
				fc.utobj().selectDropDown(driver, pobj.priority, "Low");
				fc.utobj().clickElement(driver, pobj.createBtn);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to create Meeting");

		}
	}

	@Test(groups = {"thehub","hub_calendar"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Modify Meeting At The Hub > Calendar", testCaseId = "TC_175_Modify_Meeting")
	private void modifyMeeting() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubCalendarPage pobj = new TheHubCalendarPage(driver);

			fc.utobj().printTestStep("Navigate To The Hub > Calendar Page");
			fc.utobj().printTestStep("Create Meeting");
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			dataSet.put("scheduleScope", "Private");
			createMeeting(driver, subject, description, dataSet);

			fc.utobj().clickElement(driver, pobj.searchBtn);
			fc.utobj().selectDropDown(driver, pobj.scheduleType, "Meeting");
			fc.utobj().sendKeys(driver, pobj.searchWords, subject);
			if (!fc.utobj().isSelected(driver, pobj.lookInSubCheck)) {
				fc.utobj().clickElement(driver, pobj.lookInSubCheck);
			}
			fc.utobj().clickElement(driver, pobj.searchBtnAtSearch);

			fc.utobj().printTestStep("Modify Meeting");
			fc.utobj().actionImgOption(driver, subject, "Modify");

			subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subject, subject);
			description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().sendKeys(driver, pobj.description, description);
			fc.utobj().selectDropDown(driver, pobj.scheduleScope, "Private");
			fc.utobj().sendKeys(driver, pobj.startDatetime, fc.utobj().getCurrentDateUSFormat());
			fc.utobj().sendKeys(driver, pobj.endDatetime, fc.utobj().getFutureDateUSFormat(3));
			fc.utobj().sendKeys(driver, pobj.location, "TestLocation");
			fc.utobj().selectDropDown(driver, pobj.priority, "Low");
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("Verify Modify The Meeting");

			fc.utobj().clickElement(driver, pobj.searchBtn);
			fc.utobj().selectDropDown(driver, pobj.scheduleType, "Meeting");
			fc.utobj().sendKeys(driver, pobj.searchWords, subject);
			if (!fc.utobj().isSelected(driver, pobj.lookInSubCheck)) {
				fc.utobj().clickElement(driver, pobj.lookInSubCheck);
			}
			fc.utobj().clickElement(driver, pobj.searchBtnAtSearch);

			boolean isSubjectPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + subject + "']");
			if (isSubjectPresent == false) {
				fc.utobj().throwsException("was not able to verify Modify Meeting");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"thehub","hub_calendar"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Create Event At The Hub > Calendar", testCaseId = "TC_176_Create_Event")
	private void createEvent() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubCalendarPage pobj = new TheHubCalendarPage(driver);

			fc.utobj().printTestStep("Navigate To The Hub > Calendar Page");
			fc.utobj().printTestStep("Create Event");
			String option = "Event";
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			createEvent(driver, option, subject, description, dataSet);

			fc.utobj().printTestStep("Verify The Create Event");

			fc.utobj().clickElement(driver, pobj.searchBtn);
			fc.utobj().selectDropDown(driver, pobj.scheduleType, "Event");
			fc.utobj().sendKeys(driver, pobj.searchWords, subject);
			if (!fc.utobj().isSelected(driver, pobj.lookInSubCheck)) {
				fc.utobj().clickElement(driver, pobj.lookInSubCheck);
			}
			fc.utobj().clickElement(driver, pobj.searchBtnAtSearch);

			boolean isSubjectPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + subject + "']");
			if (isSubjectPresent == false) {
				fc.utobj().throwsException("was not able to verify Create Event");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"thehub","hub_calendar"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Modify Event At The Hub > Calendar", testCaseId = "TC_177_Modify_Event")
	private void modifyEvent() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubCalendarPage pobj = new TheHubCalendarPage(driver);

			fc.utobj().printTestStep("Navigate To The Hub > Calendar Page");
			fc.utobj().printTestStep("Create Event");
			String option = "Event";
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			createEvent(driver, option, subject, description, dataSet);

			fc.utobj().printTestStep("Modify Event");

			fc.utobj().clickElement(driver, pobj.searchBtn);
			fc.utobj().selectDropDown(driver, pobj.scheduleType, "Event");
			fc.utobj().sendKeys(driver, pobj.searchWords, subject);
			if (!fc.utobj().isSelected(driver, pobj.lookInSubCheck)) {
				fc.utobj().clickElement(driver, pobj.lookInSubCheck);
			}
			fc.utobj().clickElement(driver, pobj.searchBtnAtSearch);

			fc.utobj().actionImgOption(driver, subject, "Modify");

			fc.utobj().sendKeys(driver, pobj.subject, subject);
			fc.utobj().sendKeys(driver, pobj.description, description);

			if (option.equalsIgnoreCase("Event")) {
				fc.utobj().selectDropDown(driver, pobj.scheduleScope, dataSet.get("scheduleScope"));
				fc.utobj().sendKeys(driver, pobj.startDatetime, fc.utobj().getCurrentDateUSFormat());
				fc.utobj().sendKeys(driver, pobj.endDatetime, fc.utobj().getFutureDateUSFormat(2));
				fc.utobj().clickElement(driver, pobj.alldayEvent);
				fc.utobj().selectDropDown(driver, pobj.categorySelect, dataSet.get("category"));
				fc.utobj().selectDropDown(driver, pobj.priority, dataSet.get("priority"));
			}

			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("Verify The Modify Event");

			fc.utobj().clickElement(driver, pobj.searchBtn);
			fc.utobj().selectDropDown(driver, pobj.scheduleType, "Event");
			fc.utobj().sendKeys(driver, pobj.searchWords, subject);
			if (!fc.utobj().isSelected(driver, pobj.lookInSubCheck)) {
				fc.utobj().clickElement(driver, pobj.lookInSubCheck);
			}
			fc.utobj().clickElement(driver, pobj.searchBtnAtSearch);

			boolean isSubjectPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + subject + "']");
			if (isSubjectPresent == false) {
				fc.utobj().throwsException("was not able to verify Modify Event");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void createEvent(WebDriver driver, String option, String subject, String description,
			Map<String, String> dataSet) throws Exception {

		String testCaseId = "TC_Create_Event_TheHub_Calander";

		if (fc.utobj().validate(testCaseId)) {
			try {
				TheHubCalendarPage pobj = new TheHubCalendarPage(driver);
				fc.hub().hub_common().theHubCalendarSubModule(driver);
				fc.utobj().clickElement(driver, pobj.createLink);
				fc.utobj().selectDropDown(driver, pobj.scheduleSelect, option);
				fc.utobj().sendKeys(driver, pobj.subject, subject);
				fc.utobj().sendKeys(driver, pobj.description, description);

				if (option.equalsIgnoreCase("Event")) {

					if (dataSet.get("scheduleScope").equalsIgnoreCase("Group")) {
						fc.utobj().selectDropDown(driver, pobj.scheduleScope, dataSet.get("scheduleScope"));
						fc.utobj().printTestStep("Select Participants");
						
						fc.utobj().clickElement(driver,
								fc.utobj().getElementByXpath(driver, ".//*[@id='Participantdivlabel']/a"));

						TheHubAlertsPage alert_page = new TheHubAlertsPage(driver);
						fc.commonMethods().switch_cboxIframe_frameId(driver);
						fc.utobj().setToDefault(driver, alert_page.selectUsers);
						fc.utobj().selectValFromMultiSelect(driver, alert_page.selectUsers, "Corporate Users");
						fc.utobj().clickElement(driver, alert_page.submitOk);
						fc.utobj().sendKeys(driver, alert_page.searchTxBx, dataSet.get("customUserName"));
						fc.utobj().clickElement(driver, alert_page.searchImgBtn);

						fc.utobj().sleep();
						WebElement element = fc.utobj().getElementByXpath(driver, ".//*[contains(text () , '"
								+ dataSet.get("customUserName") + "')]/ancestor::tr/td/input");
						fc.utobj().clickElement(driver, element);
						fc.utobj().clickElement(driver, alert_page.addUserBtn);
						fc.utobj().switchFrameToDefault(driver);
					} else if (dataSet.get("scheduleScope").equalsIgnoreCase("Public")
							|| dataSet.get("scheduleScope").equalsIgnoreCase("Private")) {
						fc.utobj().selectDropDown(driver, pobj.scheduleScope, dataSet.get("scheduleScope"));
					}
					fc.utobj().sendKeys(driver, pobj.startDatetime,
							fc.utobj().getCurrentDateUserDefineTimeZoneUSFormat("GMT-5:00"));
					fc.utobj().sendKeys(driver, pobj.endDatetime, fc.utobj().getFutureDateUSFormat(2));
					fc.utobj().clickElement(driver, pobj.alldayEvent);
					fc.utobj().selectDropDown(driver, pobj.categorySelect, dataSet.get("category"));
					fc.utobj().selectDropDown(driver, pobj.priority, dataSet.get("priority"));
				}

				fc.utobj().clickElement(driver, pobj.createBtn);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to Create Event");

		}
	}
}
