package com.builds.test.thehub;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminAreaRegionAddAreaRegionPageTest;
import com.builds.test.admin.AdminDivisionAddDivisionPageTest;
import com.builds.test.admin.AdminFranchiseLocationAddFranchiseLocationPageTest;
import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.admin.AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest;
import com.builds.test.admin.AdminUsersManageManageFranchiseUsersPageTest;
import com.builds.test.admin.AdminUsersManageRegionalUsersAddRegionalUserPageTest;
import com.builds.test.admin.AdminUsersRolesAddNewRolePageTest;
import com.builds.test.admin.AdminUsersSupplierDetailsAddSupplierTest;
import com.builds.test.common.CorporateUser;
import com.builds.uimaps.common.CommonUI;
import com.builds.uimaps.thehub.TheHubDirectoryPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class TheHubDirectoryPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "thehub" ,"hub_directory"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Send Messages To Corporate User At The Hub > Directory", testCaseId = "TC_123_Send_Messages_CU")
	public void sendMessagesCU() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubDirectoryPage pobj = new TheHubDirectoryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "hubautomation@staffex.com";

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = corporateUser.createDefaultUser(driver, corpUser);

			fc.utobj().printTestStep("Navigate To The Hub > Directory Page");
			fc.utobj().printTestStep("Send Messages To Corporate User");
			fc.hub().hub_common().theHubDirectorySubModule(driver);
			fc.utobj().clickElement(driver, pobj.corporateUsersTab);
			fc.utobj().setToDefault(driver, pobj.selectByCountry);
			fc.utobj().sendKeys(driver, pobj.searchTxBx, corpUser.getuserFullName());
			fc.utobj().clickElement(driver, pobj.searchBtn);

			fc.utobj().printTestStep("Verify User details in pop up");
			fc.utobj().clickPartialLinkText(driver, corpUser.getuserFullName());
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			boolean isUserNamePresent = fc.utobj().assertPageSource(driver, corpUser.getuserFullName());
			if (isUserNamePresent == false) {
				fc.utobj().throwsException("user full name is not present at pop up");
			}
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[.='" + corpUser.getuserFullName() + "']/ancestor::tr/td/input"));
			fc.utobj().clickElement(driver, pobj.sendMessages);

			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			String messages = fc.utobj().generateTestData(dataSet.get("messages"));
			String messagesType = dataSet.get("messagesType");

			fc.utobj().sendKeys(driver, pobj.subject, subject);

			if (messagesType.equalsIgnoreCase("Text")) {

				fc.utobj().moveToElement(driver, pobj.texthtmlOption.get(0));
				if (!fc.utobj().isSelected(driver,pobj.texthtmlOption.get(0))) {
					fc.utobj().clickElement(driver, pobj.texthtmlOption.get(0));
				}
				fc.utobj().sendKeys(driver, pobj.messages, messages);
			} else if (messagesType.equalsIgnoreCase("Html")) {

			}
			fc.utobj().clickElement(driver, pobj.sendBtn);
			fc.utobj().clickElement(driver, pobj.OkBtn);

			boolean isDirectoryPagePresent = fc.utobj().isElementPresent(driver, pobj.searchTxBx);
			if (isDirectoryPagePresent == false) {
				fc.utobj().throwsException(
						"Not able to redirect after clicking over ok button at messages confirmation page");
			}

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With Newly Created Corporate User");
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());

			fc.utobj().printTestStep("Navigate To The Hub > Home Page");
			fc.hub().hub_common().theHubHome(driver);

			fc.utobj().printTestStep("Verify The Messages");
			String msgText = null;
			boolean status = false;
			List<WebElement> elements = driver.findElements(By.xpath(".//*[@id='messagesHomePageContent']//td//a"));

			if (elements.size() > 0) {
				for (int i = 0; i < elements.size(); i++) {

					msgText = elements.get(i).getText().trim();
					if (msgText.equalsIgnoreCase(subject)) {
						status = true;
						break;
					}
				}
			}

			if (status == false) {
				fc.utobj().throwsException("was not able to verify Message at Home Page in Messages Widgets");
			}

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + subject + "']"));
			fc.utobj().isTextDisplayed(driver, subject, "was not able to verify Subject Of the messages");
			fc.utobj().isTextDisplayed(driver, "FranConnect Administrator", "was not able to verify From");
			fc.utobj().isTextDisplayed(driver, corpUser.getuserFullName(), "was not abel to verify To");
			fc.utobj().isTextDisplayed(driver, fc.utobj().getCurrentDateUSFormat(), "was not able to verify date");
			fc.utobj().isTextDisplayed(driver, messages, "was not abel to verify messages content");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub"  ,"hub_directory"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add To Group Corporate User  At The Hub > Directory ", testCaseId = "TC_124_Add_To_Group")
	private void addToGroupCU() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubDirectoryPage pobj = new TheHubDirectoryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			String emailId = "hubautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To The Hub > Directory > Groups Page");
			fc.utobj().printTestStep("Create Group");
			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			createNewGroup(driver, groupName, "FranConnect Administrator", config);

			fc.utobj().printTestStep("Navigate To The Hub > Directory > Corporate Users Tab");
			fc.hub().hub_common().theHubDirectorySubModule(driver);
			fc.utobj().clickElement(driver, pobj.corporateUsersTab);
			fc.utobj().setToDefault(driver, pobj.selectByCountry);
			fc.utobj().sendKeys(driver, pobj.searchTxBx, corpUser.getuserFullName());
			fc.utobj().clickElement(driver, pobj.searchBtn);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[.='" + corpUser.getuserFullName() + "']/ancestor::tr/td/input"));
			fc.utobj().clickElement(driver, pobj.addToGroupBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[contains(text () , '" + groupName + "')]/ancestor::tr/td/input"));
			fc.utobj().clickElement(driver, pobj.addBtn);

			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, pobj.groupsTab);
			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, groupName));
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().printTestStep("Verify Add To Group Corporate User");
			fc.utobj().isTextDisplayed(driver, corpUser.getuserFullName(),
					"was not able to verify UserName Of CorporateUser");

			if (!fc.utobj().getElementByXpath(driver, ".//*[contains(text () ,'" + corpUser.getuserFullName()
					+ "')]/ancestor::tr/td[contains(text () , 'Corporate')]").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify UserType");
			}
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub"  ,"hub_directory"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Send Messages To Divisional User At The Hub > Directory", testCaseId = "TC_125_Send_Messages_DU")
	private void sendMessagesDU() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubDirectoryPage pobj = new TheHubDirectoryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Division > Add Division");
			AdminDivisionAddDivisionPageTest addDivPage = new AdminDivisionAddDivisionPageTest();
			String divisionName = fc.utobj().generateTestData(dataSet.get("divisionName"));
			addDivPage.addDivision(driver, divisionName);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Divisional Users");
			fc.utobj().printTestStep("Add Divisioanl User");
			AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest divisionUserPage = new AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest();
			String userName = fc.utobj().generateTestData(dataSet.get("userName"));
			String password = "t0n1ght123";

			String emailId = "hubautomation@staffex.com";
			divisionUserPage.addDivisionalUser(driver, userName, password, divisionName, emailId);
			String userName1 = userName + " " + userName;

			fc.utobj().printTestStep("Navigate To The Hub > Directory > Divisional Users Tab");
			fc.utobj().printTestStep("Send Message To Divisional User");
			fc.hub().hub_common().theHubDirectorySubModule(driver);
			fc.utobj().clickElement(driver, pobj.divisionalUserTab);
			fc.utobj().setToDefault(driver, pobj.selectByCountry);
			fc.utobj().sendKeys(driver, pobj.searchTxBx, userName1);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			fc.utobj().printTestStep("Verify User details in pop up");
			fc.utobj().clickPartialLinkText(driver, userName1);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			boolean isUserNamePresent = fc.utobj().assertPageSource(driver, userName1);
			if (isUserNamePresent == false) {
				fc.utobj().throwsException("user full name is not present at pop up");
			}
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[.='" + userName1 + "']/ancestor::tr/td/input"));
			fc.utobj().clickElement(driver, pobj.sendMessages);

			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			String messages = fc.utobj().generateTestData(dataSet.get("messages"));
			String messagesType = dataSet.get("messagesType");

			fc.utobj().sendKeys(driver, pobj.subject, subject);

			if (messagesType.equalsIgnoreCase("Text")) {

				fc.utobj().moveToElement(driver, pobj.texthtmlOption.get(0));
				if (!fc.utobj().isSelected(driver,pobj.texthtmlOption.get(0))) {
					fc.utobj().clickElement(driver, pobj.texthtmlOption.get(0));
				}
				fc.utobj().sendKeys(driver, pobj.messages, messages);
			} else if (messagesType.equalsIgnoreCase("Html")) {

			}
			fc.utobj().clickElement(driver, pobj.sendBtn);
			fc.utobj().clickElement(driver, pobj.OkBtn);

			boolean isDirectoryPagePresent = fc.utobj()
					.isElementPresent(driver, fc.utobj().getElement(driver, pobj.inboxTab));
			if (isDirectoryPagePresent == false) {
				fc.utobj().throwsException(
						"Not able to redirect after clicking over ok button at messages confirmation page");
			}
			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login With Newly Created Divisional User");
			fc.loginpage().loginWithParameter(driver, userName, password);

			fc.utobj().printTestStep("Navigate To The Hub > Home");
			fc.hub().hub_common().theHubHome(driver);

			String msgText = null;
			boolean status = false;
			List<WebElement> elements = driver.findElements(By.xpath(".//*[@id='messagesHomePageContent']//td//a"));

			if (elements.size() > 0) {
				for (int i = 0; i < elements.size(); i++) {

					msgText = elements.get(i).getText().trim();
					if (msgText.equalsIgnoreCase(subject)) {
						status = true;
						break;
					}
				}
			}

			if (status == false) {
				fc.utobj().throwsException("was not able to verify Message at Home Page in Messages Widgets");
			}

			fc.utobj().printTestStep("Verify The Send Message To Divisional User");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + subject + "']"));
			fc.utobj().isTextDisplayed(driver, subject, "was not able to verify Subject Of the messages");
			fc.utobj().isTextDisplayed(driver, "FranConnect Administrator", "was not able to verify From");
			fc.utobj().isTextDisplayed(driver, userName1, "was not abel to verify To");
			fc.utobj().isTextDisplayed(driver, fc.utobj().getCurrentDateUSFormat(), "was not able to verify date");
			fc.utobj().isTextDisplayed(driver, messages, "was not abel to verify messages content");

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"thehub" ,"hub_directory"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add To Group Divisional User At The Hub > Directory ", testCaseId = "TC_126_Add_To_Group_DU")
	private void addToGroupDU() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubDirectoryPage pobj = new TheHubDirectoryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Division > Add Division");
			AdminDivisionAddDivisionPageTest addDivPage = new AdminDivisionAddDivisionPageTest();
			String divisionName = fc.utobj().generateTestData(dataSet.get("divisionName"));
			addDivPage.addDivision(driver, divisionName);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Divisioanl User");
			fc.utobj().printTestStep("Add Divisional User");
			AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest divisionUserPage = new AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest();
			String userName = fc.utobj().generateTestData(dataSet.get("userName"));
			String password = "t0n1ght123";

			String emailId = "hubautomation@staffex.com";
			divisionUserPage.addDivisionalUser(driver, userName, password, divisionName, emailId);
			String userName1 = userName + " " + userName;

			fc.utobj().printTestStep("Navigate To The Hub > Directory > Groups Tab");
			fc.utobj().printTestStep("Create Group");
			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			createNewGroup(driver, groupName, "FranConnect Administrator", config);

			fc.utobj().printTestStep("Navigate To The Hub > Directory > Divisional User Tab");
			fc.utobj().printTestStep("Add To Group");
			fc.hub().hub_common().theHubDirectorySubModule(driver);
			fc.utobj().clickElement(driver, pobj.divisionalUserTab);
			fc.utobj().setToDefault(driver, pobj.selectByCountry);
			fc.utobj().sendKeys(driver, pobj.searchTxBx, userName1);
			fc.utobj().clickElement(driver, pobj.searchBtn);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[.='" + userName1 + "']/ancestor::tr/td/input"));
			fc.utobj().clickElement(driver, pobj.addToGroupBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[contains(text () , '" + groupName + "')]/ancestor::tr/td/input"));
			fc.utobj().clickElement(driver, pobj.addBtn);

			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, pobj.groupsTab);
			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, groupName));
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().printTestStep("Verify Add To Group Divisional User");
			fc.utobj().isTextDisplayed(driver, userName1, "was not able to verify UserName Of Divisional User");

			// need to be discuss
			if (!fc.utobj().getElementByXpath(driver,
					".//*[contains(text () ,'" + userName1 + "')]/ancestor::tr/td[contains(text () , 'Division')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify UserType");
			}
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub"  ,"hub_directory"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Send Messages To Franchise User At The Hub > Directory", testCaseId = "TC_127_Send_Messages_FU")
	private void sendMessagesFU() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubDirectoryPage pobj = new TheHubDirectoryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = "Test127Region".concat(fc.utobj().generateRandomNumber());
			String storeType = "Test127StoreType".concat(fc.utobj().generateRandomNumber());
			String franchiseId = "Test127FranId".concat(fc.utobj().generateRandomNumber());
			AdminFranchiseLocationAddFranchiseLocationPageTest addFranchiseLocation = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String ownerFirstName = fc.utobj().generateTestData("Fname");
			String ownerLastName = fc.utobj().generateTestData("Lname");
			addFranchiseLocation.addFranchiseLocation(driver, regionName, storeType, franchiseId, "Test Center",
					fc.utobj().getCurrentDateUSFormat(), ownerFirstName, ownerLastName, config);

			fc.utobj().printTestStep("Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest addFranchiseUser = new AdminUsersManageManageFranchiseUsersPageTest();
			String roleName = "Default Franchise Role";
			String userName = fc.utobj().generateTestData(dataSet.get("userName"));
			String password = "t0n1ght123";

			String emailId = "hubautomation@staffex.com";
			addFranchiseUser.addFranchiseUser(driver, userName, password, franchiseId, roleName, emailId);

			fc.utobj().printTestStep("Navigate To The Hub > Directory > Franchise Users Tab");
			fc.utobj().printTestStep("Send Messages To Franchise User");
			fc.hub().hub_common().theHubDirectorySubModule(driver);
			fc.utobj().clickElement(driver, pobj.franchiseUsersTab);

			fc.utobj().setToDefault(driver, pobj.searchByFranchiseId);
			fc.utobj().selectValFromMultiSelect(driver, pobj.searchByFranchiseId, franchiseId);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			fc.utobj().printTestStep("Verify User details in pop up");
			fc.utobj().clickPartialLinkText(driver, ownerFirstName);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			boolean isUserNamePresent = fc.utobj().assertPageSource(driver, ownerFirstName);
			if (isUserNamePresent == false) {
				fc.utobj().throwsException("user full name is not present at pop up");
			}
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text() , '" + ownerFirstName + "')]/ancestor::tr/td/input"));
			fc.utobj().clickElement(driver, pobj.sendMessages);

			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			String messages = fc.utobj().generateTestData(dataSet.get("messages"));
			String messagesType = dataSet.get("messagesType");

			fc.utobj().sendKeys(driver, pobj.subject, subject);

			if (messagesType.equalsIgnoreCase("Text")) {

				fc.utobj().moveToElement(driver, pobj.texthtmlOption.get(0));
				if (!fc.utobj().isSelected(driver,pobj.texthtmlOption.get(0))) {
					fc.utobj().clickElement(driver, pobj.texthtmlOption.get(0));
				}
				fc.utobj().sendKeys(driver, pobj.messages, messages);
			} else if (messagesType.equalsIgnoreCase("Html")) {

			}
			fc.utobj().clickElement(driver, pobj.sendBtn);
			fc.utobj().clickElement(driver, pobj.OkBtn);

			boolean isDirectoryPagePresent = fc.utobj().isElementPresent(driver, pobj.searchTxBx);
			if (isDirectoryPagePresent == false) {
				fc.utobj().throwsException(
						"Not able to redirect after clicking over ok button at messages confirmation page");
			}

			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login With Newly Created Franchise User");
			fc.loginpage().loginWithParameter(driver, userName, password);

			fc.utobj().printTestStep("Navigate To The Hub > Home Page");
			//fc.hub().hub_common().theHubHome(driver);

			String msgText = null;
			boolean status = false;
			List<WebElement> elements = driver.findElements(By.xpath(".//*[@id='messagesHomePageContent']//td//a"));

			if (elements.size() > 0) {
				for (int i = 0; i < elements.size(); i++) {

					msgText = elements.get(i).getText().trim();
					if (msgText.equalsIgnoreCase(subject)) {
						status = true;
						break;
					}
				}
			}

			if (status == false) {
				fc.utobj().throwsException("was not able to verify Message at Home Page in Messages Widgets");
			}

			fc.utobj().printTestStep("Verify The Send Messages To Franchise User");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + subject + "']"));
			fc.utobj().isTextDisplayed(driver, subject, "was not able to verify Subject Of the messages");
			fc.utobj().isTextDisplayed(driver, "FranConnect Administrator", "was not able to verify From");
			fc.utobj().isTextDisplayed(driver, ownerFirstName+" "+ownerLastName, "was not able to verify To");
			fc.utobj().isTextDisplayed(driver, fc.utobj().getCurrentDateUSFormat(), "was not able to verify date");
			fc.utobj().isTextDisplayed(driver, messages, "was not able to verify messages content");

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"thehub" ,"hub_directory"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add To group Franchise User At The Hub > Directory", testCaseId = "TC_128_Add_To_Group_FU")
	private void addToGroupFU() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubDirectoryPage pobj = new TheHubDirectoryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = "Test128Region".concat(fc.utobj().generateRandomNumber());
			String storeType = "Test128StoreType".concat(fc.utobj().generateRandomNumber());
			String franchiseId = "Test128FranId".concat(fc.utobj().generateRandomNumber());

			AdminFranchiseLocationAddFranchiseLocationPageTest addFranchiseLocation = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = addFranchiseLocation.addFranchiseLocation_All(driver, franchiseId, regionName, storeType);

			fc.utobj().printTestStep("Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest addFranchiseUser = new AdminUsersManageManageFranchiseUsersPageTest();
			String roleName = "Default Franchise Role";
			String userName = fc.utobj().generateTestData(dataSet.get("userName"));
			String password = "t0n1ght123";

			String emailId = "hubautomation@staffex.com";
			addFranchiseUser.addFranchiseUser(driver, userName, password, franchiseId, roleName, emailId);

			fc.utobj().printTestStep("Navigate To The Hub > Directory > Groups Page");
			fc.utobj().printTestStep("Create Group");
			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			createNewGroup(driver, groupName, "FranConnect Administrator", config);

			fc.utobj().printTestStep("Navigate To The Hub > Directory > Franchise Users Tab");
			fc.utobj().printTestStep("Add To Group");
			fc.hub().hub_common().theHubDirectorySubModule(driver);
			fc.utobj().clickElement(driver, pobj.franchiseUsersTab);

			fc.utobj().setToDefault(driver, pobj.searchByFranchiseId);
			fc.utobj().selectValFromMultiSelect(driver, pobj.searchByFranchiseId, franchiseId);
			fc.utobj().clickElement(driver, pobj.searchBtn);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[.='firstName lastName']/ancestor::tr/td/input"));

			fc.utobj().clickElement(driver, pobj.addToGroupBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[contains(text () , '" + groupName + "')]/ancestor::tr/td/input"));
			fc.utobj().clickElement(driver, pobj.addBtn);

			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify Add To Group");
			fc.utobj().clickElement(driver, pobj.groupsTab);
			fc.utobj().showAll(driver);
			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, groupName));
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().isTextDisplayed(driver, groupName, "Was not able to verify group Name");
			fc.utobj().isTextDisplayed(driver, "firstName lastName", "was not able to verify Name of Franchisee");
			fc.utobj().isTextDisplayed(driver, "Franchise (" + franchiseId + ")", "was not able to verify User Type");

			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub" ,"hub_directory" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Send Messages To Regional User At The Hub > Directory", testCaseId = "TC_129_Send_Messages_RU")
	private void sendMessagesRU() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubDirectoryPage pobj = new TheHubDirectoryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Add Area/Region");
			fc.utobj().printTestStep("Add Region");

			AdminAreaRegionAddAreaRegionPageTest addRegionPage = new AdminAreaRegionAddAreaRegionPageTest();
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			addRegionPage.addAreaRegion(driver, regionName);

			fc.utobj().printTestStep("Add Regional User");
			AdminUsersManageRegionalUsersAddRegionalUserPageTest regionalUserPage = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
			String userName = fc.utobj().generateTestData(dataSet.get("userName"));
			String password = "t0n1ght123";

			String emailId = "hubautomation@staffex.com";
			regionalUserPage.addRegionalUser(driver, userName, password, regionName, emailId);
			String userName1 = userName + " " + userName;

			fc.utobj().printTestStep("Navigate To The Hub > Directory > Regional User Tab");
			fc.utobj().printTestStep("Send Messages To Regional User");
			fc.hub().hub_common().theHubDirectorySubModule(driver);
			fc.utobj().clickElement(driver, pobj.regionalUserTab);
			fc.utobj().setToDefault(driver, pobj.selectByCountry);
			fc.utobj().sendKeys(driver, pobj.searchTxBx, userName1);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			fc.utobj().printTestStep("Verify User details in pop up");
			fc.utobj().clickPartialLinkText(driver, userName1);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			boolean isUserNamePresent = fc.utobj().assertPageSource(driver, userName1);
			if (isUserNamePresent == false) {
				fc.utobj().throwsException("user full name is not present at pop up");
			}
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[.='" + userName1 + "']/ancestor::tr/td/input"));
			fc.utobj().clickElement(driver, pobj.sendMessages);

			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			String messages = fc.utobj().generateTestData(dataSet.get("messages"));
			String messagesType = dataSet.get("messagesType");

			fc.utobj().sendKeys(driver, pobj.subject, subject);

			if (messagesType.equalsIgnoreCase("Text")) {

				fc.utobj().moveToElement(driver, pobj.texthtmlOption.get(0));
				if (!fc.utobj().isSelected(driver,pobj.texthtmlOption.get(0))) {
					fc.utobj().clickElement(driver, pobj.texthtmlOption.get(0));
				}
				fc.utobj().sendKeys(driver, pobj.messages, messages);
			} else if (messagesType.equalsIgnoreCase("Html")) {

			}
			fc.utobj().clickElement(driver, pobj.sendBtn);
			fc.utobj().clickElement(driver, pobj.OkBtn);

			boolean isDirectoryPagePresent = fc.utobj().isElementPresent(driver, pobj.searchTxBx);
			if (isDirectoryPagePresent == false) {
				fc.utobj().throwsException(
						"Not able to redirect after clicking over ok button at messages confirmation page");
			}

			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login With Newly Created Regional User");
			fc.loginpage().loginWithParameter(driver, userName, password);

			fc.utobj().printTestStep("Navigate To The Hub > Home Page");
			fc.hub().hub_common().theHubHome(driver);

			String msgText = null;
			boolean status = false;
			List<WebElement> elements = driver.findElements(By.xpath(".//*[@id='messagesHomePageContent']//td//a"));

			if (elements.size() > 0) {
				for (int i = 0; i < elements.size(); i++) {

					msgText = elements.get(i).getText().trim();
					if (msgText.equalsIgnoreCase(subject)) {
						status = true;
						break;
					}
				}
			}

			if (status == false) {
				fc.utobj().throwsException("was not able to verify Message at Home Page in Messages Widgets");
			}

			fc.utobj().printTestStep("Verify Send Message Detail");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + subject + "']"));
			fc.utobj().isTextDisplayed(driver, subject, "was not able to verify Subject Of the messages");
			fc.utobj().isTextDisplayed(driver, "FranConnect Administrator", "was not able to verify 'From'");
			fc.utobj().isTextDisplayed(driver, userName1, "was not abel to verify 'To'");
			fc.utobj().isTextDisplayed(driver, fc.utobj().getCurrentDateUSFormat(), "was not able to verify date");
			fc.utobj().isTextDisplayed(driver, messages, "was not abel to verify messages content");

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"thehub" ,"hub_directory"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add To Group Regional User At The Hub > Directory", testCaseId = "TC_130_Add_To_Group_RU")
	private void addToGroupRU() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubDirectoryPage pobj = new TheHubDirectoryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Add Area/Region");
			fc.utobj().printTestStep("Add Region");
			AdminAreaRegionAddAreaRegionPageTest addRegionPage = new AdminAreaRegionAddAreaRegionPageTest();
			String regionName = fc.utobj().generateTestData("Test130Regioname");
			addRegionPage.addAreaRegion(driver, regionName);

			fc.utobj().printTestStep("Add Regional User");
			AdminUsersManageRegionalUsersAddRegionalUserPageTest regionalUserPage = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
			String userName = fc.utobj().generateTestData(dataSet.get("userName"));
			String password = "t0n1ght123";

			String emailId = "hubautomation@staffex.com";
			regionalUserPage.addRegionalUser(driver, userName, password, regionName, emailId);
			String userName1 = userName + " " + userName;

			fc.utobj().printTestStep("Navigate To The Hub > Directory > Groups Page");
			fc.utobj().printTestStep("Create Group");
			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			createNewGroup(driver, groupName, "FranConnect Administrator", config);

			fc.utobj().printTestStep("Add To Group");
			fc.hub().hub_common().theHubDirectorySubModule(driver);
			fc.utobj().clickElement(driver, pobj.regionalUserTab);
			fc.utobj().setToDefault(driver, pobj.selectByCountry);
			fc.utobj().sendKeys(driver, pobj.searchTxBx, userName1);
			fc.utobj().clickElement(driver, pobj.searchBtn);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[.='" + userName1 + "']/ancestor::tr/td/input"));
			fc.utobj().clickElement(driver, pobj.addToGroupBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[contains(text () , '" + groupName + "')]/ancestor::tr/td/input"));
			fc.utobj().clickElement(driver, pobj.addBtn);

			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, pobj.groupsTab);
			fc.utobj().showAll(driver);
			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, groupName));
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().printTestStep("Verify Add To Group");
			fc.utobj().isTextDisplayed(driver, userName1, "was not able to verify UserName Of Regional User");

			if (!fc.utobj().getElementByXpath(driver,
					".//*[contains(text () ,'" + userName1 + "')]/ancestor::tr/td[contains(text () , 'Regional')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify UserType");
			}
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub","hub_directory"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Search At Suppliers At The Hub > Directory", testCaseId = "TC_131_Verify_Search_At_Suppliers")
	private void verifySearchAtSuppliers() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubDirectoryPage pobj = new TheHubDirectoryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Users > Supplier Details");
			fc.utobj().printTestStep("Add Supplier");
			AdminUsersSupplierDetailsAddSupplierTest suppliersPage = new AdminUsersSupplierDetailsAddSupplierTest();
			String supplierName = fc.utobj().generateTestData(dataSet.get("supplierName"));

			String emailId = "hubautomation@staffex.com";
			suppliersPage.addSupplier(driver, supplierName, config, emailId);

			fc.utobj().printTestStep("Verify Search Suppliers");
			fc.hub().hub_common().theHubDirectorySubModule(driver);
			fc.utobj().clickElement(driver, pobj.suppliers);
			fc.utobj().sendKeys(driver, pobj.suppliersName, supplierName);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			fc.utobj().isTextDisplayed(driver, supplierName, "was not able to verify supplier Name");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub","hub_directory"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-01-29", testCaseDescription = "Verify The Create Group  At The Hub > Directory", testCaseId = "TC_132_Create_New_Group")
	private void createNewGroup() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubDirectoryPage pobj = new TheHubDirectoryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUserPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			String emailId = "hubautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUserPage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To The Hub > Directory > Groups Tab");
			fc.utobj().printTestStep("Create New Group");

			fc.hub().hub_common().theHubDirectorySubModule(driver);
			fc.utobj().clickElement(driver, pobj.groupsTab);
			fc.utobj().clickElement(driver, pobj.createNewGroupLink);

			/*
			 * TC-9941
			 */
			boolean isElementPresent = fc.utobj()
					.isElementPresent(driver, fc.utobj().getElement(driver, pobj.externalLnkRadio));
			if (isElementPresent == false) {
				fc.utobj()
						.throwsException("External Address Book radio button is not present at Directory > Group page");
			}

			fc.utobj().clickElement(driver, pobj.internalAddressBook);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().sendKeys(driver, pobj.searchTxBxCU, corpUser.getuserFullName());
			try {
				fc.utobj().clickEnterOnElement(driver, pobj.searchCUImgIcon);
			} catch (Exception e) {
				fc.utobj().clickElement(driver, pobj.searchCUImgIcon);
			}
			
			pobj=new TheHubDirectoryPage(driver);
			fc.utobj().clickElement(driver, pobj.selectcorporate);
			pobj=new TheHubDirectoryPage(driver);
			fc.utobj().clickElement(driver, pobj.selectUser);
			fc.utobj().switchFrameToDefault(driver);
			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			fc.utobj().sendKeys(driver, pobj.grouopName, groupName);
			fc.utobj().clickElement(driver, pobj.createBtn);
			fc.utobj().showAll(driver);
			fc.utobj().isTextDisplayed(driver, groupName, "was not able to create group");

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, groupName));
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().printTestStep("Verify Create Group");
			fc.utobj().isTextDisplayed(driver, corpUser.getuserFullName(),
					"was not able to verify Name of Corporate User");
			fc.utobj().isTextDisplayed(driver, "Corporate", "was not able to verify User Type");

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub","hub_directory"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Modify Group At The Hub > Directory", testCaseId = "TC_133_Modify_Group")
	private void modifyGroupActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubDirectoryPage pobj = new TheHubDirectoryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Add Area/Region");
			fc.utobj().printTestStep("Add Region");

			AdminAreaRegionAddAreaRegionPageTest addRegionPage = new AdminAreaRegionAddAreaRegionPageTest();
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			addRegionPage.addAreaRegion(driver, regionName);

			fc.utobj().printTestStep("Add Regional User");
			AdminUsersManageRegionalUsersAddRegionalUserPageTest regionalUserPage = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
			String regUserName = fc.utobj().generateTestData(dataSet.get("regUserName"));
			String password = "t0n1ght123";
			String emailId = "hubautomation@staffex.com";
			regionalUserPage.addRegionalUser(driver, regUserName, password, regionName, emailId);
			regUserName = regUserName + " " + regUserName;

			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUserPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUserPage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To The Hub > Directory > Groups Page");
			fc.utobj().printTestStep("Create Group");
			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			createNewGroup(driver, groupName, corpUser.getuserFullName(), config);

			fc.utobj().printTestStep("Modify Group");
			fc.utobj().showAll(driver);
			fc.utobj().actionImgOption(driver, groupName, "Modify");
			String groupName1 = fc.utobj().generateTestData(dataSet.get("groupName"));
			fc.utobj().sendKeys(driver, pobj.grouopName, groupName1);
			fc.utobj().clickElement(driver, pobj.internalAddressBookLink);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.searchregionalUsers, regUserName);
			fc.utobj().clickElement(driver, pobj.searchRegUserImgIcon);

			fc.utobj().sleep();
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[contains(text () , '" + regUserName + "')]/ancestor::tr/td/input"));
			
			fc.utobj().clickElement(driver, pobj.selectUser);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().isTextDisplayed(driver, regUserName, "was not able to verify Second Contanct");
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().showAll(driver);

			fc.utobj().printTestStep("Verify The Modify Group");
			fc.utobj().isTextDisplayed(driver, groupName1, "was not able to modify group");
			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, groupName1));

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().isTextDisplayed(driver, corpUser.getuserFullName(),
					"was not able to verify Name of Corporate User");
			fc.utobj().isTextDisplayed(driver, "Corporate", "was not able to verify User Type of Corporate User");

			fc.utobj().isTextDisplayed(driver, regUserName, "was not able to verify Name of Regional User");
			fc.utobj().isTextDisplayed(driver, "Regional", "was not able to verify User Type of Regional User");
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"thehub","hub_directory"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Delete Group At The Hub > Directory ", testCaseId = "TC_134_Delete_Group")
	private void deleteGroupActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUserPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "hubautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUserPage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To The Hub > Directory > Groups Page");
			fc.utobj().printTestStep("Create Group");
			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			createNewGroup(driver, groupName, corpUser.getuserFullName(), config);
			fc.utobj().showAll(driver);

			fc.utobj().printTestStep("Delete Group");
			fc.utobj().actionImgOption(driver, groupName, "Delete");
			fc.utobj().acceptAlertBox(driver);
			fc.utobj().showAll(driver);

			fc.utobj().printTestStep("Verify The Delete Group");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, groupName);
			if (isTextPresent == true) {
				fc.utobj().throwsException("was not able to delete Group");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"thehub","hub_directory"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Delete Group By Bottom Button At The Hub > Directory", testCaseId = "TC_135_Delete_Group")
	private void deleteGroupBottomBtn() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubDirectoryPage pobj = new TheHubDirectoryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUserPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "hubautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUserPage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To The Hub > Directory > Groups Page");
			fc.utobj().printTestStep("Create Group");
			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			createNewGroup(driver, groupName, corpUser.getuserFullName(), config);
			fc.utobj().showAll(driver);

			fc.utobj().printTestStep("Delete Group");
			WebElement element = fc.utobj().getElementByXpath(driver,
					".//*[.='" + groupName + "']/ancestor::tr/td/input");
			fc.utobj().clickElement(driver, element);
			fc.utobj().clickElement(driver, pobj.deleteBottomBtn);
			fc.utobj().acceptAlertBox(driver);
			fc.utobj().showAll(driver);

			fc.utobj().printTestStep("Verify The Delete Group");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, groupName);
			if (isTextPresent == true) {
				fc.utobj().throwsException("was not able to delete Group from bottom delete btn");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/*
	 * TC-9943
	 */

	@Test(groups = { "thehub","hub_directory"})
	@TestCase(createdOn = "2018-01-29", updatedOn = "2018-01-29", testCaseDescription = "Verify Group action icon options functionality for other users except Corporate users", testCaseId = "TC_Hub_Directory_Groups_01")
	private void groupDirectory() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubDirectoryPage directory_page = new TheHubDirectoryPage(driver);
			String emailId = "hubautomation@staffex.com";

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUserPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = corporateUserPage.createDefaultUser(driver, corpUser);
			/*
			 * Div User
			 */
			fc.utobj().printTestStep("Navigate To Admin > Division > Add Division");
			fc.utobj().printTestStep("Add Division");

			AdminDivisionAddDivisionPageTest addDivPage = new AdminDivisionAddDivisionPageTest();
			String divisionName = fc.utobj().generateTestData("testdiv");
			addDivPage.addDivision(driver, divisionName);

			fc.utobj().printTestStep("Navigate To Admin > Users > Add Divisional User");
			fc.utobj().printTestStep("Add Divisional User");
			AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest divisionUserPage = new AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest();
			String devUserName = fc.utobj().generateTestData("devuser");
			divisionUserPage.addDivisionalUser(driver, devUserName, corpUser.getPassword(), divisionName, emailId);

			/*
			 * Regional User
			 */
			fc.utobj().printTestStep("Navigate To Admin > Add Area/Region");
			fc.utobj().printTestStep("Add Region");

			AdminAreaRegionAddAreaRegionPageTest addRegionPage = new AdminAreaRegionAddAreaRegionPageTest();
			String regionName = fc.utobj().generateTestData("Testreg");
			addRegionPage.addAreaRegion(driver, regionName);

			fc.utobj().printTestStep("Add Regional User");
			AdminUsersManageRegionalUsersAddRegionalUserPageTest regionalUserPage = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
			String regUserName = fc.utobj().generateTestData("Testreguser");

			regionalUserPage.addRegionalUser(driver, regUserName, corpUser.getPassword(), regionName, emailId);

			/*
			 * Franchise User
			 */

			fc.utobj().printTestStep("Add Franchise Location");
			String storeType = "Test127StoreType".concat(fc.utobj().generateRandomNumber());
			String franchiseId = "Test127FranId".concat(fc.utobj().generateRandomNumber());

			AdminFranchiseLocationAddFranchiseLocationPageTest addFranchiseLocation = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String firstName = fc.utobj().generateTestData("Testfname");
			String franRegionName = fc.utobj().generateTestData("Testfreg");
			franchiseId = addFranchiseLocation.addFranchiseLocation_AllWithFName(driver, franchiseId, franRegionName,
					storeType, firstName);

			fc.utobj().printTestStep("Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest addFranchiseUser = new AdminUsersManageManageFranchiseUsersPageTest();
			String roleName = "Default Franchise Role";
			String userNameFran = fc.utobj().generateTestData("Testfranuser");
			addFranchiseUser.addFranchiseUser(driver, userNameFran, corpUser.getPassword(), franchiseId, roleName,
					emailId);

			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login by newly created Corporate User");
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());

			fc.utobj().printTestStep("Navigate To Hub > Directory > Group");
			fc.utobj().printTestStep("Create Group");
			String groupName = fc.utobj().generateTestData("TestGroupName");
			createNewGroup(driver, groupName, corpUser.getuserFullName(), config);

			fc.utobj().actionImgOption(driver, groupName, "Turn this group Public");
			fc.utobj().acceptAlertBox(driver);
			/*
			 * TC-9942
			 */

			fc.utobj().printTestStep(
					"Verify All the options Modify,Delete Turn This group private should present in Action Icon");

			boolean isModifyOptionpresent = fc.utobj().optionPresentInActionMenu(driver, groupName, "Modify");
			boolean isDeleteOptionpresent = fc.utobj().optionPresentInActionMenu(driver, groupName, "Delete");
			boolean isTurnThisGroupPublicOptionpresent = fc.utobj().optionPresentInActionMenu(driver, groupName,
					"Turn this group Private");

			if (isModifyOptionpresent == false) {
				fc.utobj().throwsException("Under Action icon of Groups, Modify option is not coming");
			}

			if (isDeleteOptionpresent == false) {
				fc.utobj().throwsException("Under Action icon of Groups, Delete option is not coming");
			}
			if (isTurnThisGroupPublicOptionpresent == false) {
				fc.utobj().throwsException("Under Action icon of Groups, Turn this group Private option is not coming");
			}

			boolean isDeleteButton22 = fc.utobj()
					.isElementPresent(driver, fc.utobj().getElement(driver, directory_page.deleteBottomBtn));
			if (isDeleteButton22 == false) {
				fc.utobj().throwsException("Delete Button is not coming");
			}

			fc.home_page().logout(driver);

			/*
			 * Divisional
			 */

			fc.utobj().printTestStep("Login With Newly Created Divisional User");
			fc.loginpage().loginWithParameter(driver, devUserName, corpUser.getPassword());

			fc.utobj().printTestStep("Verify Only Delete Button should come at Directory > Groups");

			fc.hub().hub_common().theHubDirectorySubModule(driver);
			fc.utobj().clickElement(driver, directory_page.groupsTab);
			fc.utobj().showAll(driver);

			boolean isDeleteButton2 = fc.utobj()
					.isElementPresent(driver, fc.utobj().getElement(driver, directory_page.deleteBottomBtn));
			if (isDeleteButton2 == false) {
				fc.utobj().throwsException("Delete Button is not coming");
			}

			/*
			 * Regional
			 */

			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login With Newly Created Regional User");
			fc.loginpage().loginWithParameter(driver, regUserName, corpUser.getPassword());

			fc.utobj().printTestStep("Verify Only Delete Button Should Come");

			fc.hub().hub_common().theHubDirectorySubModule(driver);
			fc.utobj().clickElement(driver, directory_page.groupsTab);
			fc.utobj().showAll(driver);

			boolean isDeleteButton3 = fc.utobj()
					.isElementPresent(driver, fc.utobj().getElement(driver, directory_page.deleteBottomBtn));
			if (isDeleteButton3 == false) {
				fc.utobj().throwsException("Delete Button is not coming");
			}

			/*
			 * Franchise
			 */
			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login With Newly Created Franchise User");
			fc.loginpage().loginWithParameter(driver, regUserName, corpUser.getPassword());

			fc.utobj().printTestStep("Verify Only Delete Button Should Come");

			fc.hub().hub_common().theHubDirectorySubModule(driver);
			fc.utobj().clickElement(driver, directory_page.groupsTab);
			fc.utobj().showAll(driver);

			boolean isDeleteButton4 = fc.utobj()
					.isElementPresent(driver, fc.utobj().getElement(driver, directory_page.deleteBottomBtn));
			if (isDeleteButton4 == false) {
				fc.utobj().throwsException("Delete Button is not coming");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	/*
	 * TC-9939
	 */

	@Test(groups = { "thehub","hub_directory" })
	@TestCase(createdOn = "2018-01-29", updatedOn = "2018-01-29", testCaseDescription = "Verify Groups Sub Tab functionality under Directory Tab for logged users based on privileged based", testCaseId = "TC_Hub_Directory_Alert_Messages_Groups_01")
	private void verifyCreatedGroupAlertMessagesDirectory() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			String emailId = "hubautomation@staffex.com";

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUserPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = corporateUserPage.createDefaultUser(driver, corpUser);

			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login By newly Created Corporate User");
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());

			fc.utobj().printTestStep("Navigate To Hub > Messages > Groups");
			fc.utobj().printTestStep("Create Group");
			TheHubMessagesPageTest messages_test = new TheHubMessagesPageTest();

			String messageGroupName = fc.utobj().generateTestData("Testmsggroup");
			messages_test.createNewGroup(driver, messageGroupName, corpUser.getuserFullName());

			fc.utobj().printTestStep("Navigate To Hub > Alert > Groups");
			fc.utobj().printTestStep("Create Group");
			String alertGroupName = fc.utobj().generateTestData("Testalertgroup");
			new TheHubAlertsPageTest().createNewGroupAtAlerts(driver, alertGroupName, corpUser.getuserFullName(), config);

			fc.utobj().printTestStep("Navigate To Hub > Directory > Groups");
			fc.utobj().printTestStep("Create Group");
			TheHubDirectoryPageTest directory_test = new TheHubDirectoryPageTest();

			String directoryGroupName = fc.utobj().generateTestData("Testdirecgroup");
			directory_test.createNewGroup(driver, directoryGroupName, corpUser.getuserFullName(), config);
			fc.utobj().showAll(driver);

			boolean isMessageGroups = fc.utobj().assertPageSource(driver, messageGroupName);
			boolean isDirectoryGroups = fc.utobj().assertPageSource(driver, directoryGroupName);
			boolean isAlertGroups = fc.utobj().assertPageSource(driver, alertGroupName);

			if (isMessageGroups == false) {
				fc.utobj().throwsException("Message's groups is not visible at Alert's group");
			}

			if (isDirectoryGroups == false) {
				fc.utobj().throwsException("Directory's groups is not visible at Alert's group");
			}

			if (isAlertGroups == false) {
				fc.utobj().throwsException("Alert's groups is not visible at Alert's group");
			}

			boolean isMessageMember = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(), '" + messageGroupName + "')]/ancestor::tr/td[contains(text(),'1')]");
			boolean isDirectoryMember = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(), '" + directoryGroupName + "')]/ancestor::tr/td[contains(text(),'1')]");
			boolean isAlertMember = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(), '" + alertGroupName + "')]/ancestor::tr/td[contains(text(),'1')]");

			if (isMessageMember == false) {
				fc.utobj().throwsException("No of member is not present at Alert's groups page of Message's group");
			}

			if (isDirectoryMember == false) {
				fc.utobj().throwsException("No of member is not present at Alert's groups page of Directory's group");
			}

			if (isAlertMember == false) {
				fc.utobj().throwsException("No of member is not present at Alert's groups page of Alert's group");
			}

			boolean isMessageCreatedBy = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(), '" + messageGroupName
					+ "')]/ancestor::tr/td[contains(text(),'" + corpUser.getuserFullName() + "')]");
			boolean isDirectoryCreatedBy = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(), '" + directoryGroupName
					+ "')]/ancestor::tr/td[contains(text(),'" + corpUser.getuserFullName() + "')]");
			boolean isAlertCreatedBy = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(), '" + alertGroupName
					+ "')]/ancestor::tr/td[contains(text(),'" + corpUser.getuserFullName() + "')]");

			if (isMessageCreatedBy == false) {
				fc.utobj().throwsException("Created By is not present at Alert's groups page of Message's group");
			}

			if (isDirectoryCreatedBy == false) {
				fc.utobj().throwsException("Created By is not present at Alert's groups page of Directory's group");
			}

			if (isAlertCreatedBy == false) {
				fc.utobj().throwsException("Created By is not present at Alert's groups page of Alert's group");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/*
	 * TC-9933
	 */

	@Test(groups = { "thehub","hub_directory" })
	@TestCase(createdOn = "2018-01-30", updatedOn = "2018-01-30", testCaseDescription = "To verify the Supplier Details By Corporate User", testCaseId = "TC_Hub_Directory_Supplier_Corporate_User_01")
	public void supplierCorporate() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubDirectoryPage pobj = new TheHubDirectoryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "hubautomation@staffex.com";

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = corporateUser.createDefaultUser(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > Users > Suppliers Details > Add Supplier");
			AdminUsersSupplierDetailsAddSupplierTest supplier_page = new AdminUsersSupplierDetailsAddSupplierTest();
			String supplierName = fc.utobj().generateTestData("TestSupplierscor");
			supplier_page.addSupplier(driver, supplierName, config, emailId);

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With Newly Created Corporate User");
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());

			fc.utobj().printTestStep("Navigate To The Hub > Directory > Supplier Tab");
			fc.hub().hub_common().theHubDirectorySubModule(driver);
			fc.utobj().clickElement(driver, pobj.suppliers);

			fc.utobj().sendKeys(driver, pobj.suppliersName, supplierName);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			fc.utobj().clickPartialLinkText(driver, supplierName);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			boolean isSupplierNamePresent = fc.utobj().assertPageSource(driver, supplierName);

			if (isSupplierNamePresent == false) {
				fc.utobj().throwsException("Supplier name is not present at pop up");
			}

			boolean isPrintBtnPresent = fc.utobj().isElementPresent(driver, new CommonUI(driver).Print_Input_ByValue);
			if (isPrintBtnPresent == false) {
				fc.utobj().throwsException("Print button is not present at pop up");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub","hub_directory" })
	@TestCase(createdOn = "2018-01-30", updatedOn = "2018-01-30", testCaseDescription = "To verify the Supplier Details By Divisional User", testCaseId = "TC_Hub_Directory_Supplier_Divisional_User_01")
	private void supplierDivisional() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubDirectoryPage pobj = new TheHubDirectoryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Division > Add Division");
			AdminDivisionAddDivisionPageTest addDivPage = new AdminDivisionAddDivisionPageTest();
			String divisionName = fc.utobj().generateTestData(dataSet.get("divisionName"));
			addDivPage.addDivision(driver, divisionName);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Divisional Users");
			fc.utobj().printTestStep("Add Divisioanl User");
			AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest divisionUserPage = new AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest();
			String userName = fc.utobj().generateTestData(dataSet.get("userName"));
			String password = "t0n1ght123";

			String emailId = "hubautomation@staffex.com";
			divisionUserPage.addDivisionalUser(driver, userName, password, divisionName, emailId);

			fc.utobj().printTestStep("Navigate To Admin > Users > Suppliers Details > Add Supplier");
			AdminUsersSupplierDetailsAddSupplierTest supplier_page = new AdminUsersSupplierDetailsAddSupplierTest();
			String supplierName = fc.utobj().generateTestData("TestSupplierscor");
			supplier_page.addSupplier(driver, supplierName, config, emailId);

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With Newly Created Divisional User");
			fc.loginpage().loginWithParameter(driver, userName, password);

			fc.utobj().printTestStep("Navigate To The Hub > Directory > Supplier Tab");
			fc.hub().hub_common().theHubDirectorySubModule(driver);
			fc.utobj().clickElement(driver, pobj.suppliers);

			fc.utobj().sendKeys(driver, pobj.suppliersName, supplierName);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			fc.utobj().clickPartialLinkText(driver, supplierName);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			boolean isSupplierNamePresent = fc.utobj().assertPageSource(driver, supplierName);

			if (isSupplierNamePresent == false) {
				fc.utobj().throwsException("Supplier name is not present at pop up");
			}
			

			boolean isPrintBtnPresent = fc.utobj().isElementPresent(driver, new CommonUI(driver).Print_Input_ByValue);
			if (isPrintBtnPresent == false) {
				fc.utobj().throwsException("Print button is not present at pop up");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub","hub_directory"})
	@TestCase(createdOn = "2018-01-30", updatedOn = "2018-01-30", testCaseDescription = "To verify the Supplier Details By Franchise User", testCaseId = "TC_Hub_Directory_Supplier_Franchise_User_01")
	private void supplierFranchiseUser() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubDirectoryPage pobj = new TheHubDirectoryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = "Test127Region".concat(fc.utobj().generateRandomNumber());
			String storeType = "Test127StoreType".concat(fc.utobj().generateRandomNumber());
			String franchiseId = "Test127FranId".concat(fc.utobj().generateRandomNumber());
			AdminFranchiseLocationAddFranchiseLocationPageTest addFranchiseLocation = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String ownerFirstName = fc.utobj().generateTestData("Fname");
			String ownerLastName = fc.utobj().generateTestData("Lname");
			addFranchiseLocation.addFranchiseLocation(driver, regionName, storeType, franchiseId, "Test Center",
					fc.utobj().getCurrentDateUSFormat(), ownerFirstName, ownerLastName, config);

			fc.utobj().printTestStep("Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest addFranchiseUser = new AdminUsersManageManageFranchiseUsersPageTest();
			String roleName = "Default Franchise Role";
			String userName = fc.utobj().generateTestData(dataSet.get("userName"));
			String password = "t0n1ght123";

			String emailId = "hubautomation@staffex.com";
			addFranchiseUser.addFranchiseUser(driver, userName, password, franchiseId, roleName, emailId);

			fc.utobj().printTestStep("Navigate To Admin > Users > Suppliers Details > Add Supplier");
			AdminUsersSupplierDetailsAddSupplierTest supplier_page = new AdminUsersSupplierDetailsAddSupplierTest();
			String supplierName = fc.utobj().generateTestData("TestSupplierscor");
			supplier_page.addSupplier(driver, supplierName, config, emailId);

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With Newly Created Franchise User");
			fc.loginpage().loginWithParameter(driver, userName, password);

			fc.utobj().printTestStep("Navigate To The Hub > Directory > Supplier Tab");
			fc.hub().hub_common().theHubDirectorySubModule(driver);
			fc.utobj().clickElement(driver, pobj.suppliers);

			fc.utobj().sendKeys(driver, pobj.suppliersName, supplierName);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			fc.utobj().clickPartialLinkText(driver, supplierName);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			boolean isSupplierNamePresent = fc.utobj().assertPageSource(driver, supplierName);

			if (isSupplierNamePresent == false) {
				fc.utobj().throwsException("Supplier name is not present at pop up");
			}

			boolean isPrintBtnPresent = fc.utobj().isElementPresent(driver, new CommonUI(driver).Print_Input_ByValue);
			if (isPrintBtnPresent == false) {
				fc.utobj().throwsException("Print button is not present at pop up");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub","hub_directory"})
	@TestCase(createdOn = "2018-01-30", updatedOn = "2018-01-30", testCaseDescription = "To verify the Supplier Details By Regional User", testCaseId = "TC_Hub_Directory_Supplier_Regional_User_01")
	private void supplierRegionalUser() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubDirectoryPage pobj = new TheHubDirectoryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Add Area/Region");
			fc.utobj().printTestStep("Add Region");

			AdminAreaRegionAddAreaRegionPageTest addRegionPage = new AdminAreaRegionAddAreaRegionPageTest();
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			addRegionPage.addAreaRegion(driver, regionName);

			fc.utobj().printTestStep("Add Regional User");
			AdminUsersManageRegionalUsersAddRegionalUserPageTest regionalUserPage = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
			String userName = fc.utobj().generateTestData(dataSet.get("userName"));
			String password = "t0n1ght123";

			String emailId = "hubautomation@staffex.com";
			regionalUserPage.addRegionalUser(driver, userName, password, regionName, emailId);

			fc.utobj().printTestStep("Navigate To Admin > Users > Suppliers Details > Add Supplier");
			AdminUsersSupplierDetailsAddSupplierTest supplier_page = new AdminUsersSupplierDetailsAddSupplierTest();
			String supplierName = fc.utobj().generateTestData("TestSupplierscor");
			supplier_page.addSupplier(driver, supplierName, config, emailId);

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With Newly Created Regional User");
			fc.loginpage().loginWithParameter(driver, userName, password);

			fc.utobj().printTestStep("Navigate To The Hub > Directory > Supplier Tab");
			fc.hub().hub_common().theHubDirectorySubModule(driver);
			fc.utobj().clickElement(driver, pobj.suppliers);

			fc.utobj().sendKeys(driver, pobj.suppliersName, supplierName);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			fc.utobj().clickPartialLinkText(driver, supplierName);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			boolean isSupplierNamePresent = fc.utobj().assertPageSource(driver, supplierName);

			if (isSupplierNamePresent == false) {
				fc.utobj().throwsException("Supplier name is not present at pop up");
			}

			boolean isPrintBtnPresent = fc.utobj().isElementPresent(driver, new CommonUI(driver).Print_Input_ByValue);
			if (isPrintBtnPresent == false) {
				fc.utobj().throwsException("Print button is not present at pop up");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/*
	 * TC-9930
	 */
	@Test(groups = { "thehub" ,"hubfaile0724","hub_directory"})
	@TestCase(createdOn = "2018-01-30", updatedOn = "2018-01-30", testCaseDescription = "To Verify the Role based Sub tabs for divisional user under Hub > Directory", testCaseId = "TC_Hub_Directory_Privilege_Divisional_User_01")
	private void privilegeDivisionalUser() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubDirectoryPage pobj = new TheHubDirectoryPage(driver);

			fc.utobj().printTestStep(
					"Navigate To Add A Divisional User Type Role with Can View Corporate Users,Can View Franchise Users and Can View Suppliers as a deselectd");
			AdminUsersRolesAddNewRolePageTest roles_page = new AdminUsersRolesAddNewRolePageTest();
			String roleType = "Division Level";
			String roleName = fc.utobj().generateTestData("TcDivRole");
			String moduleName = "The Hub";
			String emailId = "hubautomation@staffex.com";
			String password = "t0n1ght@123";

			Map<String, String> privileges = new HashMap<String, String>();
			privileges.put("Can View Corporate Users", "No");
			privileges.put("Can View Franchise Users", "No");
			privileges.put("Can View Suppliers", "No");
			roles_page.addRolesWithModulesPrivileges(driver, roleType, roleName, moduleName, privileges, testCaseId);

			String divisionName = fc.utobj().generateTestData("Test12divName");
			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Divisional Users");
			fc.utobj().printTestStep("Add Divisioanl User");
			AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest divisionUserPage = new AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest();
			String userName = fc.utobj().generateTestData("Test21divuser");
			divisionUserPage.addDivisionalUserWithRole(driver, userName, divisionName, roleName, password, emailId);

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With Newly Created Divisional User");
			fc.loginpage().loginWithParameter(driver, userName, password);

			fc.utobj().printTestStep("Navigate To The Hub > Directory > Supplier Tab");
			fc.hub().hub_common().theHubDirectorySubModule(driver);

			boolean isCorporateUserTabPresent = fc.utobj()
					.isElementPresent(driver, fc.utobj().getElement(driver, pobj.corporateUsersTab));
			boolean isDivisionalUserTabPresent = fc.utobj()
					.isElementPresent(driver, fc.utobj().getElement(driver, pobj.divisionalUserTab));
			/*boolean isFranchiseUserTabPresent = fc.utobj()
					.isElementPresent(driver, fc.utobj().getElement(driver, pobj.franchiseUsersTab));*/
			boolean isRegionalUserTabPresent = fc.utobj()
					.isElementPresent(driver, fc.utobj().getElement(driver, pobj.regionalUserTab));
			/*boolean isSupplierUserTabPresent = fc.utobj()
					.isElementPresent(driver, fc.utobj().getElement(driver, pobj.suppliers));*/
			boolean isGroupsUserTabPresent = fc.utobj()
					.isElementPresent(driver, fc.utobj().getElement(driver, pobj.groupsTab));

			if (isCorporateUserTabPresent) {
				fc.utobj().throwsException("Corporate User Tab is present");
			}
			if (!isDivisionalUserTabPresent) {
				fc.utobj().throwsException("Divisional User Tab is not present");
			}
			/*if (isFranchiseUserTabPresent) {
				fc.utobj().throwsException("Franchise User Tab is not present");
			}*/
			if (!isRegionalUserTabPresent) {
				fc.utobj().throwsException("Regional User Tab is not present");
			}
			/*if (isSupplierUserTabPresent) {
				fc.utobj().throwsException("Supplier Tab is present");
			}*/
			if (!isGroupsUserTabPresent) {
				fc.utobj().throwsException("Groups Tab is not present");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub","hub_directory"})
	@TestCase(createdOn = "2018-01-30", updatedOn = "2018-01-30", testCaseDescription = "To Verify the Role based Sub tabs for Franchise User under Hub > Directory", testCaseId = "TC_Hub_Directory_Privilege_Franchise_User_01")
	private void privilegeFranchiseUser() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubDirectoryPage pobj = new TheHubDirectoryPage(driver);

			fc.utobj().printTestStep(
					"Navigate To Add A Franchise User Type Role with Can View Corporate Users,Can View Franchise Users and Can View Suppliers as a deselectd");
			AdminUsersRolesAddNewRolePageTest roles_page = new AdminUsersRolesAddNewRolePageTest();
			String roleType = "Franchise Level";
			String roleName = fc.utobj().generateTestData("TcFranRole");
			String moduleName = "The Hub";
			String emailId = "hubautomation@staffex.com";
			String password = "t0n1ght@123";

			Map<String, String> privileges = new HashMap<String, String>();
			privileges.put("Can View Corporate Users", "No");
			privileges.put("Can View Regional Users", "No");
			privileges.put("Can View Suppliers", "No");
			roles_page.addRolesWithModulesPrivileges(driver, roleType, roleName, moduleName, privileges, testCaseId);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = "Test127Region".concat(fc.utobj().generateRandomNumber());
			String storeType = "Test127StoreType".concat(fc.utobj().generateRandomNumber());
			String franchiseId = "Test127FranId".concat(fc.utobj().generateRandomNumber());
			AdminFranchiseLocationAddFranchiseLocationPageTest addFranchiseLocation = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String ownerFirstName = fc.utobj().generateTestData("Fname");
			String ownerLastName = fc.utobj().generateTestData("Lname");
			addFranchiseLocation.addFranchiseLocation(driver, regionName, storeType, franchiseId, "Test Center",
					fc.utobj().getCurrentDateUSFormat(), ownerFirstName, ownerLastName, config);

			fc.utobj().printTestStep("Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest addFranchiseUser = new AdminUsersManageManageFranchiseUsersPageTest();
			String userName = fc.utobj().generateTestData("Testfranuser");
			addFranchiseUser.addFranchiseUser(driver, userName, password, franchiseId, roleName, emailId);

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With Newly Created Franchise User");
			fc.loginpage().loginWithParameter(driver, userName, password);

			fc.utobj().printTestStep("Navigate To The Hub > Directory > Directory Tab");
			fc.hub().hub_common().theHubDirectorySubModule(driver);

			boolean isCorporateUserTabPresent = fc.utobj()
					.isElementPresent(driver, fc.utobj().getElement(driver, pobj.corporateUsersTab));
			boolean isDivisionalUserTabPresent = fc.utobj()
					.isElementPresent(driver, fc.utobj().getElement(driver, pobj.divisionalUserTab));
			boolean isFranchiseUserTabPresent = fc.utobj()
					.isElementPresent(driver, fc.utobj().getElement(driver, pobj.franchiseUsersTab));
			boolean isRegionalUserTabPresent = fc.utobj()
					.isElementPresent(driver, fc.utobj().getElement(driver, pobj.regionalUserTab));
			boolean isSupplierUserTabPresent = fc.utobj()
					.isElementPresent(driver, fc.utobj().getElement(driver, pobj.suppliers));
			boolean isGroupsUserTabPresent = fc.utobj()
					.isElementPresent(driver, fc.utobj().getElement(driver, pobj.groupsTab));

			if (isCorporateUserTabPresent) {
				fc.utobj().throwsException("Corporate User Tab is present");
			}
			if (isDivisionalUserTabPresent) {
				fc.utobj().throwsException("Divisional User Tab is present");
			}
			if (isFranchiseUserTabPresent == false) {
				fc.utobj().throwsException("Franchise User Tab is not present");
			}
			if (isRegionalUserTabPresent) {
				fc.utobj().throwsException("Regional User Tab is present");
			}
			if (isSupplierUserTabPresent) {
				fc.utobj().throwsException("Supplier Tab is present");
			}
			if (isGroupsUserTabPresent == false) {
				fc.utobj().throwsException("Groups Tab is not present");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub","hub_directory"})
	@TestCase(createdOn = "2018-01-30", updatedOn = "2018-01-30", testCaseDescription = "To Verify the Role based Sub tabs for Regional User under Hub > Directory", testCaseId = "TC_Hub_Directory_Privilege_Regional_User_01")
	private void privilegeRegionalUser() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubDirectoryPage pobj = new TheHubDirectoryPage(driver);

			fc.utobj().printTestStep(
					"Navigate To Add A Regional User Type Role with Can View Corporate Users,Can View Franchise Users and Can View Suppliers as a deselectd");
			AdminUsersRolesAddNewRolePageTest roles_page = new AdminUsersRolesAddNewRolePageTest();
			String roleType = "Regional Level";
			String roleName = fc.utobj().generateTestData("TcRegRole");
			String moduleName = "The Hub";
			String emailId = "hubautomation@staffex.com";
			String password = "t0n1ght@123";

			Map<String, String> privileges = new HashMap<String, String>();
			privileges.put("Can View Corporate Users", "No");
			privileges.put("Can View Franchise Users", "No");
			privileges.put("Can View Suppliers", "No");
			roles_page.addRolesWithModulesPrivileges(driver, roleType, roleName, moduleName, privileges, testCaseId);

			fc.utobj().printTestStep("Navigate To Admin > Add Area/Region");
			fc.utobj().printTestStep("Add Region");

			String regionName = fc.utobj().generateTestData("Testreg");

			fc.utobj().printTestStep("Add Regional User");
			AdminUsersManageRegionalUsersAddRegionalUserPageTest regionalUserPage = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
			String userName = fc.utobj().generateTestData("TestRegUser");
			regionalUserPage.addRegionalUserWithRole(driver, userName, password, regionName, roleName, emailId);

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With Newly Created Regional User");
			fc.loginpage().loginWithParameter(driver, userName, password);

			fc.utobj().printTestStep("Navigate To The Hub > Directory > Supplier Tab");
			fc.hub().hub_common().theHubDirectorySubModule(driver);

			boolean isCorporateUserTabPresent = fc.utobj()
					.isElementPresent(driver, fc.utobj().getElement(driver, pobj.corporateUsersTab));
			boolean isDivisionalUserTabPresent = fc.utobj()
					.isElementPresent(driver, fc.utobj().getElement(driver, pobj.divisionalUserTab));
			boolean isFranchiseUserTabPresent = fc.utobj()
					.isElementPresent(driver, fc.utobj().getElement(driver, pobj.franchiseUsersTab));
			boolean isRegionalUserTabPresent = fc.utobj()
					.isElementPresent(driver, fc.utobj().getElement(driver, pobj.regionalUserTab));
			boolean isSupplierUserTabPresent = fc.utobj()
					.isElementPresent(driver, fc.utobj().getElement(driver, pobj.suppliers));
			boolean isGroupsUserTabPresent = fc.utobj()
					.isElementPresent(driver, fc.utobj().getElement(driver, pobj.groupsTab));

			if (isCorporateUserTabPresent) {
				fc.utobj().throwsException("Corporate User Tab is present");
			}
			if (isDivisionalUserTabPresent) {
				fc.utobj().throwsException("Divisional User Tab is present");
			}
			if (isFranchiseUserTabPresent) {
				fc.utobj().throwsException("Franchise User Tab is present");
			}
			if (isRegionalUserTabPresent == false) {
				fc.utobj().throwsException("Regional User Tab is not present");
			}
			if (isSupplierUserTabPresent) {
				fc.utobj().throwsException("Supplier Tab is present");
			}
			if (isGroupsUserTabPresent == false) {
				fc.utobj().throwsException("Groups Tab is not present");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void createNewGroup(WebDriver driver, String groupName, String userName, Map<String, String> config)
			throws Exception {

		String testCaseId = "TC_Create_New_Group_The_Hub_Directory";

		if (fc.utobj().validate(testCaseId)) {
			try {
				TheHubDirectoryPage pobj = new TheHubDirectoryPage(driver);
				fc.hub().hub_common().theHubDirectorySubModule(driver);
				fc.utobj().clickElement(driver, pobj.groupsTab);
				fc.utobj().clickElement(driver, pobj.createNewGroupLink);
				fc.utobj().clickElement(driver, pobj.internalAddressBook);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().sendKeys(driver, pobj.searchTxBxCU, userName);
				fc.utobj().clickElement(driver, pobj.searchCUImgIcon);
				pobj = new TheHubDirectoryPage(driver);
				fc.utobj().sleep(3000);
				/*WebElement element=fc.utobj().getElementByXpath(driver,
						".//*[contains(text () , '" + userName + "')]/ancestor::tr/td/input");*/
				fc.utobj().sleep();
				fc.utobj().clickElement(driver, pobj.selectcorporate);
				fc.utobj().clickElement(driver, pobj.selectUser);
				fc.utobj().switchFrameToDefault(driver);
				fc.utobj().sendKeys(driver, pobj.grouopName, groupName);
				fc.utobj().clickElement(driver, pobj.createBtn);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to Create New Group :: The Hub > Directory");

		}
	}
}
