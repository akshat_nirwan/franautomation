package com.builds.test.thehub;

import java.io.File;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminConfigurationConfigureOffensiveWatchGreatKeywordsforForumandFranbuzzPageTest;
import com.builds.test.admin.AdminFranchiseLocationAddFranchiseLocationPageTest;
import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.admin.AdminUsersManageManageFranchiseUsersPageTest;
import com.builds.test.common.CorporateUser;
import com.builds.uimaps.admin.AdminUsersManageManageFranchiseUsersPage;
import com.builds.uimaps.thehub.TheHubFranBuzzPage;
import com.builds.uimaps.thehub.TheHubLibraryPage;
import com.builds.uimaps.thehub.TheHubNewsPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class TheHubFranBuzzPageTest {
	FranconnectUtil fc = new FranconnectUtil();
	HubCommonMethods hub_commonMethods = new HubCommonMethods();

	@Test(groups = { "thehub", "hub_franbuzz", "thehub_mail"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-16", testCaseDescription = "Share Information at Wall :: The Hub > FranBuzz > Wall", testCaseId = "TC_253_Share_Information")
	public void shareInformation() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubFranBuzzPage pobj = new TheHubFranBuzzPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest cPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "hubautomation@staffex.com";

			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			cPage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To The Hub > FranBuzz Page");
			fc.utobj().printTestStep("Share Information At Wall");
			fc.hub().hub_common().theHubFranBuzzSubModule(driver);
			fc.utobj().clickElement(driver, pobj.wallTab);

			// TC-76419
			fc.utobj().printTestStep("Verify Alert message after putting invalid data or black to share");
			fc.utobj().clickElement(driver, pobj.shareBtn);

			if (!"Please enter Text.".equals(fc.utobj().acceptAlertBox(driver))) {
				fc.utobj().throwsException("Not able to verify alert text message");
			}

			String infoText = fc.utobj().generateTestData(dataSet.get("infoText"));
			fc.utobj().sendKeys(driver, pobj.scrapText, infoText);
			fc.utobj().selectDropDown(driver, pobj.shareTo, "All Corporates");
			fc.utobj().clickElement(driver, pobj.shareBtn);

			fc.utobj().printTestStep("Verify The Share Information");
			fc.utobj().isTextDisplayed(driver, infoText, "was not able to post information");

			FranBuzz franbuzz = new FranBuzz();
			franbuzz.setInfoText(infoText);
			boolean isSharedInfoPresent = hub_commonMethods.searchItemByGlobalSearch(driver, franbuzz);
			if (!isSharedInfoPresent) {
				fc.utobj().throwsException("Shared Information at franbuzz not search by SOLR search");
			}

			// Verify the URL after placing enters through franbuzz posts
			fc.utobj().printTestStep("Share Information with url");
			FranBuzz franBuzz2 = new FranBuzz();
			String shareText = fc.utobj().generateTestData("Test");
			String url = "www.google.com";
			franBuzz2.setInfoText(shareText.concat(" ").concat(url));
			franBuzz2.setShareTo("All Corporates");
			shareInformation(driver, franBuzz2);

			fc.utobj().printTestStep("Verify the URL after placing enters through franbuzz posts");
			if (!fc.utobj().isElementPresent(driver,
					".//div[contains(text(),'" + shareText + "')]/a[@href='http://" + url + "']")) {
				fc.utobj().throwsException("not able to verify posted url");

			}

			// To verify the html code link under franbuzz
			fc.utobj().printTestStep("To verify the html code link under franbuzz");
			FranBuzz franBuzz23 = new FranBuzz();
			String temp = fc.utobj().generateTestData("Click Here Google");
			String sText = "<a href=\"http://www.google.com\">" + temp + "</a>";
			franBuzz23.setInfoText(sText);
			franBuzz23.setShareTo("All Corporates");
			shareInformation(driver, franBuzz23);
			fc.utobj().printTestStep("Verify After clicking over posted url it should be redirected to given URL");
			franBuzz23.setInfoText(temp);
			franBuzz23.setTypeOfPost("Information");
			searchWallPosts(driver, franBuzz23);
			fc.utobj().clickLink(driver, temp);

			if (!fc.utobj().isElementPresent(driver, pobj.lnkVerify)) {
				fc.utobj().throwsException("Not redirected to inserted url");
			}
			fc.utobj().back(driver);

			fc.utobj().printTestStep("Verify The Mail After The Share Information");
			Map<String, String> mailInfo = fc.utobj().readMailBox("Franbuzz Post", infoText, emailId, "sdg@1a@Hfs");
			if (mailInfo.size() == 0 || !mailInfo.get("mailBody").contains(infoText)) {
				fc.utobj().throwsException("was not able to verify The Mail After Post");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_franbuzz"})
	@TestCase(createdOn = "2018-10-08", updatedOn = "2018-10-08", testCaseDescription = "Verify Franbuzz video tab is working or not", testCaseId = "TC_Hub_Franbuzz_Video_01")
	public void shareVideo() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubFranBuzzPage pobj = new TheHubFranBuzzPage(driver);

			FranBuzz franBuzz = new FranBuzz();
			franBuzz.setVideoTitle(fc.utobj().generateTestData(dataSet.get("videoTitle")));
			franBuzz.setTextAreaVideo(dataSet.get("textAreaVideo"));
			franBuzz.setTypeOfPost("Video");

			fc.utobj().printTestStep("Navigate To The Hub > FranBuzz Page");
			fc.utobj().printTestStep("Share Video At Wall");
			fc.hub().hub_common().theHubFranBuzzSubModule(driver);
			fc.utobj().clickElement(driver, pobj.wallTab);
			fc.utobj().clickElement(driver, pobj.videoLnk);
			fc.utobj().sendKeys(driver, pobj.videoTitle, franBuzz.getVideoTitle());
			fc.utobj().sendKeys(driver, pobj.textAreaVideo, franBuzz.getTextAreaVideo());
			fc.utobj().selectDropDown(driver, pobj.shareTo, "All Corporates");
			fc.utobj().clickElement(driver, pobj.ShareVideoBtn);

			fc.utobj().printTestStep("Search posted video by title");
			searchWallPosts(driver, franBuzz);

			if (!fc.utobj().assertPageSource(driver, franBuzz.getVideoTitle())) {
				fc.utobj().throwsException("Video Title is not present");
			}

			boolean isVideoResponse = false;
			try {
				String url = fc.utobj().getAttributeValue(fc.utobj().getElementByXpath(driver,
						".//*[.='" + franBuzz.getVideoTitle() + "']/parent::*/iframe"), "src");
				int code = fc.codeUtil().getBuildResponseCode(url);
				if (code != 200) {
					isVideoResponse = false;
				} else {
					isVideoResponse = true;
				}
			} catch (Exception e) {
				Reporter.log(e.getMessage());
			}

			if (!isVideoResponse) {
				fc.utobj().throwsException("Video is not uploaded successfully");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "hub_franbuzz", "thehub"})
	@TestCase(createdOn = "2018-10-08", updatedOn = "2017-10-08", testCaseDescription = "Verify Franbuzz upload photo is working or not on the photo tab", testCaseId = "TC_Hub_Franbuzz_Image_01")
	public void sharePhoto() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubFranBuzzPage pobj = new TheHubFranBuzzPage(driver);

			fc.utobj().printTestStep("Upload jpeg File");
			FranBuzz franbuzz = new FranBuzz();
			franbuzz.setPhotoTitle(fc.utobj().generateTestData(dataSet.get("photoTitle")));
			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("browsePhoto"));
			franbuzz.setBrowsePhoto(filePath);
			franbuzz.setPhotoDescription(fc.utobj().generateTestData(dataSet.get("photoDescription")));
			franbuzz.setShareTo("All Corporates");
			franbuzz.setShareToValue("All Corporates");
			franbuzz.setTypeOfPost("Photo");
			sharePhoto(driver, franbuzz);

			fc.utobj().printTestStep("Verify Added jpeg file's Title");
			franbuzz.setTypeOfPost("Photo");
			searchWallPosts(driver, franbuzz);

			if (!fc.utobj().assertPageSource(driver, franbuzz.getPhotoTitle())) {
				fc.utobj().throwsException("Not able to verify jpeg file's Title");
			}

			fc.utobj().printTestStep("Verify Uploaded jpeg file");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[@title='" + franbuzz.getPhotoTitle() + "']/img"));
			if (!getUploadedPhotoResponse(driver, franbuzz)) {
				fc.utobj().throwsException("Not able to verify added jpeg file");
			}

			fc.utobj().printTestStep("Upload gif File's Title");
			franbuzz.setPhotoTitle(fc.utobj().generateTestData(dataSet.get("photoTitle")));
			filePath = fc.utobj().getFilePathFromTestData("GIF_01.gif");
			franbuzz.setBrowsePhoto(filePath);
			sharePhoto(driver, franbuzz);

			fc.utobj().printTestStep("Verify Added gif file's Title");
			searchWallPosts(driver, franbuzz);

			if (!fc.utobj().assertPageSource(driver, franbuzz.getPhotoTitle())) {
				fc.utobj().throwsException("Not able to verify gif file's Title");
			}

			fc.utobj().printTestStep("Verify Uploaded gif file");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[@title='" + franbuzz.getPhotoTitle() + "']/img"));
			if (!getUploadedPhotoResponse(driver, franbuzz)) {
				fc.utobj().throwsException("Not able to verify added gif file");
			}

			fc.utobj().printTestStep("Upload png File's Title");
			franbuzz.setPhotoTitle(fc.utobj().generateTestData(dataSet.get("photoTitle")));
			filePath = fc.utobj().getFilePathFromTestData("Png_01.png");
			franbuzz.setBrowsePhoto(filePath);
			sharePhoto(driver, franbuzz);

			fc.utobj().printTestStep("Verify Added png file's Title");
			searchWallPosts(driver, franbuzz);

			if (!fc.utobj().assertPageSource(driver, franbuzz.getPhotoTitle())) {
				fc.utobj().throwsException("Not able to verify png file's Title");
			}

			fc.utobj().printTestStep("Verify Uploaded png file");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[@title='" + franbuzz.getPhotoTitle() + "']/img"));
			if (!getUploadedPhotoResponse(driver, franbuzz)) {
				fc.utobj().throwsException("Not able to verify added png file");
			}

			// Verify Franbuzz in the photo and album without data share
			fc.utobj().printTestStep("Verify Franbuzz in the photo Enter any data");
			fc.utobj().clickElement(driver, pobj.wallTab);
			fc.utobj().clickElement(driver, pobj.photoLink);
			fc.utobj().clickElement(driver, pobj.uploadPhoto);
			fc.utobj().clickElement(driver, pobj.shareSinglePhoto);
			if (!"Please enter photo title.".equals(fc.utobj().acceptAlertBox(driver))) {
				fc.utobj().throwsException("Not able to verify alert and alert messages");
			}

			fc.utobj().printTestStep("Enable FranBuzz Updates");
			new AdminTheHubStoriesPageTest().franBuzzUpdatesAtHomePage(driver, testCaseId, true);

			fc.utobj().printTestStep("Navigate To Hub > Home Page");
			fc.hub().hub_common().theHubHome(driver);

			fc.utobj().printTestStep("Verify The FranBuzz Update at Hub Home Page");
			boolean isInfoTextPresent = fc.utobj().assertPageSource(driver, franbuzz.getPhotoTitle());
			if (isInfoTextPresent == false) {
				try {
					fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
							".//*[@id='franBuzzWall']//td/a[contains(text(),'View More')]"));

					fc.utobj().printTestStep("Search Photo title by search wall post search");
					searchWallPosts(driver, franbuzz);

					isInfoTextPresent = fc.utobj().assertPageSource(driver, franbuzz.getPhotoTitle());
				} catch (Exception e) {
					isInfoTextPresent = false;
				}
			}
			if (isInfoTextPresent == false) {
				fc.utobj().throwsException("Franbuzz update is not visible at Hub > Home Page");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_franbuzz" })
	@TestCase(createdOn = "2018-10-10", updatedOn = "2018-10-11", testCaseDescription = "Verify Creation of album from gallery", testCaseId = "TC_Hub_Franbuzz_Gallery_01")
	private void createAlbumFromGallery() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubFranBuzzPage pobj = new TheHubFranBuzzPage(driver);
			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest cPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "hubautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			cPage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Logged Out from the build and login by Corporate user");
			fc.home_page().logout(driver);
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());

			FranBuzz franbuzz = new FranBuzz();
			franbuzz.setAlbumTitle(fc.utobj().generateTestData(dataSet.get("albumTitle")));
			franbuzz.setAlbumDescription(fc.utobj().generateTestData(dataSet.get("albumDescription")));
			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("coverUploadPhoto"));
			franbuzz.setCoverTitle(fc.utobj().generateTestData(dataSet.get("coverTitle")));
			franbuzz.setCoverUploadPhoto(filePath);

			fc.utobj().printTestStep("Go To Hub > Franbuzz > Gallery > Create Your Album");
			createAlbumFromGallery(driver, franbuzz);

			fc.utobj().printTestStep("Add Comment In Album");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@title='" + franbuzz.getAlbumTitle() + "']"));
			fc.utobj().clickElement(driver, pobj.addCommentLink);
			franbuzz.setCommentText(fc.utobj().generateTestData("Test"));
			fc.utobj().sendKeys(driver, pobj.commentTextArea, franbuzz.getCommentText());
			fc.utobj().clickElement(driver, pobj.addCommentBtn);

			if (!fc.utobj().getText(driver, fc.utobj().getElementByXpath(driver, ".//*[@class='titleN']"))
					.equals(franbuzz.getCommentText())) {
				fc.utobj().throwsException("Not able to verify the added comment in Gallery Album");
			}

			fc.utobj().printTestStep("Verify Franbuzz back to album button is working or not in the album");
			fc.utobj().clickElement(driver, pobj.backToAlbumsBtn);

			if (!fc.utobj().isElementPresent(driver, ".//*[@title='" + franbuzz.getAlbumTitle() + "']")) {
				fc.utobj().throwsException("Back To Albums button is not working");
			}

			fc.utobj().printTestStep("Add Photo in created album");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@title='" + franbuzz.getAlbumTitle() + "']"));
			fc.utobj().clickElement(driver, pobj.addPhotosLnk);

			fc.utobj().printTestStep("Add first photo");
			String title0 = fc.utobj().generateTestData("Test");
			fc.utobj().sendKeys(driver, pobj.apTitle0, title0);
			fc.utobj().sendKeys(driver, pobj.apBrowse0, fc.utobj().getFilePathFromTestData("Apple_01.jpg"));

			String title1 = fc.utobj().generateTestData("Test");
			fc.utobj().sendKeys(driver, pobj.apTitle1, title1);
			fc.utobj().sendKeys(driver, pobj.apBrowse1, fc.utobj().getFilePathFromTestData("Apple_02.jpg"));
			fc.utobj().clickElement(driver, pobj.apUploadBtn);

			if (!fc.utobj().assertPageSource(driver, title0)) {
				fc.utobj().throwsException("not able to add first photo in album");
			}

			if (!fc.utobj().assertPageSource(driver, title1)) {
				fc.utobj().throwsException("not able to add second photo in album");
			}

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[@title='" + franbuzz.getCoverTitle() + "']/img"));

			fc.utobj().printTestStep("Verify Click on album then on single photo from roll strips");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@title='" + title1 + "']"));
			if (!getUploadedPhotoResponse(driver, franbuzz)) {
				fc.utobj().throwsException("Not able to verify the second added photo in album");
			}

			fc.utobj().printTestStep("Verify Gallery's Album should not visible to another user's wall");
			fc.utobj().printTestStep("Logged Out from the build and login by Adm user");
			fc.home_page().logout(driver);
			fc.loginpage().loginWithParameter(driver, FranconnectUtil.config.get("userName"),
					FranconnectUtil.config.get("password"));

			fc.utobj().printTestStep("Go To Hub > Franbuzz");
			fc.hub().hub_common().theHubFranBuzzSubModule(driver);

			franbuzz.setTypeOfPost("Album");
			fc.utobj().printTestStep(
					"Verify Alert message should come to enter data for Search field if we left as blank");
			fc.utobj().clickElement(driver, pobj.searchWallPostsIcon);
			if (!"Please enter a Search Parameter.".equalsIgnoreCase(fc.utobj().acceptAlertBox(driver))) {

			}
			searchWallPosts(driver, franbuzz);

			if (fc.utobj().assertLinkText(driver, franbuzz.getAlbumTitle())) {
				fc.utobj().throwsException("Other user's post is coming in user wall");
			}

			fc.utobj().printTestStep("Search User By UserName");
			searchUserByUserFullName(driver, corpUser);

			fc.utobj().printTestStep("Verify Created Album should appear only under users gallery.");
			fc.utobj().clickElement(driver, pobj.userPhotosTab);
			fc.utobj().clickElement(driver, pobj.albumTab);

			if (!fc.utobj().assertPageSource(driver, franbuzz.getAlbumTitle())) {
				fc.utobj().throwsException("not able to verify the title of Created Album in user's Album tab");
			}

			fc.utobj().printTestStep("Verify The Uploaded Cover Photo");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//img[@title='" + franbuzz.getAlbumTitle() + "']"));
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[@title='" + franbuzz.getCoverTitle() + "']/img"));
			if (!getUploadedPhotoResponse(driver, franbuzz)) {
				fc.utobj().throwsException("not able to verify the uploaded cover photo in Album in user's Album tab");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_franbuzz" })
	@TestCase(createdOn = "2018-10-10", updatedOn = "2018-10-11", testCaseDescription = "Verify Creation of album from gallery", testCaseId = "TC_Hub_Franbuzz_Gallery_04")
	private void createAlbumFromGalleryUDM() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubFranBuzzPage pobj = new TheHubFranBuzzPage(driver);

			FranBuzz franbuzz = new FranBuzz();
			franbuzz.setAlbumTitle(fc.utobj().generateTestData(dataSet.get("albumTitle")));
			franbuzz.setAlbumDescription(fc.utobj().generateTestData(dataSet.get("albumDescription")));
			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("coverUploadPhoto"));
			franbuzz.setCoverTitle(fc.utobj().generateTestData(dataSet.get("coverTitle")));
			franbuzz.setCoverUploadPhoto(filePath);

			fc.utobj().printTestStep("Go To Hub > Franbuzz > Gallery > Create Your Album");
			franbuzz = createAlbumFromGallery(driver, franbuzz);

			fc.utobj().printTestStep("Verify Created Album In Gallery");
			if (!fc.utobj().assertPageSource(driver, franbuzz.getAlbumTitle())) {
				fc.utobj().throwsException("Not able to verify created ");
			}

			fc.utobj().printTestStep("Verify The Date of Created Album");
			String cFormat = changeFormat("MM/dd/yyyy hh:mm aa", "MMMM dd yyyy", franbuzz.getCreatedOnDate());
			String ar[] = cFormat.split(" ");
			for (int i = 0; i < ar.length; i++) {
				if (i == 1) {
					ar[i] = " " + ar[i].concat(",");
				} else if (i == 2) {
					ar[i] = " " + ar[i];
				}
			}
			String temp = "";
			for (String string : ar) {
				temp += string;
			}
			String date = fc.utobj().getText(driver, fc.utobj().getElementByXpath(driver,
					".//*[contains(text(),'" + franbuzz.getAlbumTitle() + "')]/following-sibling::p/i"));

			if (!temp.equalsIgnoreCase(date)) {
				fc.utobj().throwsException("Not able to verify the Created Album's date in Gallery");
			}

			fc.utobj().printTestStep("Update The Album Title in Gallery");
			fc.utobj().clickElementByJS(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(@onclick,'" + franbuzz.getAlbumTitle() + "')]/i[@class='editIcon']"));
			franbuzz.setAlbumTitle(fc.utobj().generateTestData(dataSet.get("albumTitle")));
			fc.utobj().sendKeys(driver, pobj.uPhotoTitleTxBx1, franbuzz.getAlbumTitle());
			fc.utobj().clickElement(driver, pobj.uPhotoTitleUpdateBtn);

			fc.utobj().printTestStep("Verify The Updated Album Title");
			if (!fc.utobj().assertPageSource(driver, franbuzz.getAlbumTitle())) {
				fc.utobj().throwsException("Not able to verify the Updated Album's title in gallery");
			}

			fc.utobj().printTestStep("Delete The Album from Gallery");
			fc.utobj().clickElementByJS(driver, fc.utobj().getElementByXpath(driver, ".//a[contains(@onclick,'"
					+ franbuzz.getAlbumTitle() + "')]/following-sibling::a/i[@class='deleteIcon']"));
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Deleted Album");
			if (fc.utobj().assertPageSource(driver, franbuzz.getAlbumTitle())) {
				fc.utobj().throwsException("Not able to verify the Deleted Album title in gallery");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_franbuzz" })
	@TestCase(createdOn = "2018-10-10", updatedOn = "2018-10-11", testCaseDescription = "To Verify Franbuzz the Photos for self created", testCaseId = "TC_Hub_Franbuzz_Gallery_02")
	private void uploadPhotoInGallery() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubFranBuzzPage pobj = new TheHubFranBuzzPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest cPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "hubautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			cPage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Logged Out from the build and login by Corporate user");
			fc.home_page().logout(driver);
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());

			FranBuzz franbuzz = new FranBuzz();
			franbuzz.setPhotoTitle(fc.utobj().generateTestData(dataSet.get("photoTitle")));
			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("photoUpload"));
			franbuzz.setBrowsePhoto(filePath);

			fc.utobj().printTestStep("Go To Hub > Franbuzz > Gallery > Upload Photo");
			uploadPhotoInGallery(driver, franbuzz);

			fc.utobj().printTestStep("Verify The Uploaded Photo");
			if (!fc.utobj().assertPageSource(driver, franbuzz.getPhotoTitle())) {
				fc.utobj().throwsException("Not able to verify the uploaded photo in gallery");
			}

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//img[@title='" + franbuzz.getPhotoTitle() + "']"));
			if (!getUploadedPhotoResponse(driver, franbuzz)) {
				fc.utobj().throwsException("Not able to verify the added photo in gallery");
			}

			fc.utobj().printTestStep("Verify Gallery photo should not visible to another user's wall");

			fc.utobj().printTestStep("Logged Out from the build and login by Adm user");
			fc.home_page().logout(driver);
			fc.loginpage().loginWithParameter(driver, FranconnectUtil.config.get("userName"),
					FranconnectUtil.config.get("password"));

			fc.utobj().printTestStep("Go To Hub > Franbuzz");
			fc.hub().hub_common().theHubFranBuzzSubModule(driver);

			franbuzz.setTypeOfPost("Photo");
			searchWallPosts(driver, franbuzz);

			if (fc.utobj().assertLinkText(driver, franbuzz.getPhotoTitle())) {
				fc.utobj().throwsException("Other user's post is coming in user wall");
			}

			fc.utobj().printTestStep("Search User By UserName");
			searchUserByUserFullName(driver, corpUser);

			fc.utobj().printTestStep("Verify Created Photo should appear only under users gallery.");
			fc.utobj().clickElement(driver, pobj.userPhotosTab);

			if (!fc.utobj().assertPageSource(driver, franbuzz.getPhotoTitle())) {
				fc.utobj().throwsException("not able to verify the title of uploaded photo in user's photo tab");
			}

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//img[@title='" + franbuzz.getPhotoTitle() + "']"));

			if (!getUploadedPhotoResponse(driver, franbuzz)) {
				fc.utobj().throwsException("not able to verify the uploaded photo in user's photo tab");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_franbuzz" })
	@TestCase(createdOn = "2018-10-10", updatedOn = "2018-10-11", testCaseDescription = "To Verify Franbuzz the Photos for self created", testCaseId = "TC_Hub_Franbuzz_Gallery_03")
	private void uploadPhotoInGalleryUDM() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubFranBuzzPage pobj = new TheHubFranBuzzPage(driver);

			FranBuzz franbuzz = new FranBuzz();
			franbuzz.setPhotoTitle(fc.utobj().generateTestData(dataSet.get("photoTitle")));
			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("photoUpload"));
			franbuzz.setBrowsePhoto(filePath);

			fc.utobj().printTestStep("Go To Hub > Franbuzz > Gallery > Upload Photo");
			franbuzz = uploadPhotoInGallery(driver, franbuzz);

			fc.utobj().printTestStep("Verify The Uploaded Photo's title");
			if (!fc.utobj().assertPageSource(driver, franbuzz.getPhotoTitle())) {
				fc.utobj().throwsException("Not able to verify the uploaded photo's title in gallery");
			}

			fc.utobj().printTestStep("Verify The Date of Uploaded Photo");
			String cFormat = changeFormat("MM/dd/yyyy hh:mm aa", "MMMM dd yyyy", franbuzz.getCreatedOnDate());
			String ar[] = cFormat.split(" ");
			for (int i = 0; i < ar.length; i++) {
				if (i == 1) {
					ar[i] = " " + ar[i].concat(",");
				} else if (i == 2) {
					ar[i] = " " + ar[i];
				}
			}
			String temp = "";
			for (String string : ar) {
				temp += string;
			}
			String date = fc.utobj().getText(driver, fc.utobj().getElementByXpath(driver,
					".//*[.='" + franbuzz.getPhotoTitle() + "']/following-sibling::p/i"));

			if (!temp.equalsIgnoreCase(date)) {
				fc.utobj().throwsException("Not able to verify the added photo's date in Gallery");
			}

			fc.utobj().printTestStep("Update The Photo Title in Gallery");
			fc.utobj().clickElementByJS(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(@onclick,'" + franbuzz.getPhotoTitle() + "')]/i[@class='editIcon']"));
			franbuzz.setPhotoTitle(fc.utobj().generateTestData(dataSet.get("photoTitle")));
			fc.utobj().sendKeys(driver, pobj.uPhotoTitleTxBx, franbuzz.getPhotoTitle());
			fc.utobj().clickElement(driver, pobj.uPhotoTitleUpdateBtn);

			fc.utobj().printTestStep("Verify The Updated Photo");
			if (!fc.utobj().assertPageSource(driver, franbuzz.getPhotoTitle())) {
				fc.utobj().throwsException("Not able to verify the Updated photo title in gallery");
			}

			fc.utobj().printTestStep("Delete The Photo from Gallery");
			fc.utobj().clickElementByJS(driver, fc.utobj().getElementByXpath(driver, ".//a[contains(@onclick,'"
					+ franbuzz.getPhotoTitle() + "')]/following-sibling::a/i[@class='deleteIcon']"));
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Deleted Photo");
			if (fc.utobj().assertPageSource(driver, franbuzz.getPhotoTitle())) {
				fc.utobj().throwsException("Not able to verify the Deleted photo title in gallery");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_franbuzz"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Leave Comment At The Hub > FranBuzz > Wall", testCaseId = "TC_Leave_Comment_At_Wall")
	private void leaveYourComment() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To The Hub > FranBuzz Page");
			fc.utobj().printTestStep("Share Information At Wall");
			String infoText = fc.utobj().generateTestData(dataSet.get("infoText"));
			FranBuzz franbuzz = new FranBuzz();
			franbuzz.setInfoText(infoText);
			franbuzz.setShareTo("All Corporates");
			franbuzz.setTypeOfPost("Information");
			shareInformation(driver, franbuzz);

			fc.utobj().printTestStep("Search Post by search wall post");
			searchWallPosts(driver, franbuzz);

			fc.utobj().printTestStep(
					"Verify Alert messages should come after clicking over post button without entering text");
			String tempText = fc.utobj()
					.getElementByXpath(driver,
							".//*[contains(text () , '" + franbuzz.getInfoText() + "')]/parent::div[@class='fbz_post']")
					.getAttribute("onmouseover");
			tempText = tempText.substring(tempText.indexOf("('") + 2, tempText.indexOf("')"));
			WebElement element = fc.utobj().getElementByID(driver, "pcomment" + tempText);
			fc.utobj().clickElement(driver, element);
			WebElement postElement = fc.utobj().getElementByID(driver, "btnReply" + tempText);
			fc.utobj().clickElement(driver, postElement);
			if (!"Please Enter Comment.".equalsIgnoreCase(fc.utobj().acceptAlertBox(driver))) {
				fc.utobj().throwsException("Not able to verify alert messages");
			}
			fc.utobj().printTestStep("Leave Comment");
			franbuzz.setCommentText(fc.utobj().generateTestData(dataSet.get("comment")));
			leaveYourComment(driver, franbuzz);

			fc.utobj().printTestStep("Search Post by search wall post");
			searchWallPosts(driver, franbuzz);

			fc.utobj().printTestStep("Verify The Comment over shared Information");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, franbuzz.getCommentText());
			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to verify Comment Text over wall post");
			}

			fc.utobj().printTestStep("Leave Comment second time");
			franbuzz.setCommentText(fc.utobj().generateTestData(dataSet.get("comment")));
			leaveYourComment(driver, franbuzz);

			fc.utobj().printTestStep("Leave Comment third time");
			franbuzz.setCommentText(fc.utobj().generateTestData(dataSet.get("comment")));
			franbuzz = leaveYourComment(driver, franbuzz);

			fc.utobj().printTestStep("Verify More Link");
			TheHubFranBuzzPage pobj = new TheHubFranBuzzPage(driver);
			fc.utobj().clickElement(driver, pobj.moreCommentLink);

			fc.utobj().printTestStep("Verify The Comment over shared Information");
			boolean isTextPresent1 = fc.utobj().assertPageSource(driver, franbuzz.getCommentText());
			if (isTextPresent1 == false) {
				fc.utobj().throwsException(
						"was not able to verify Comment Text over wall post after clicking over More Link");
			}
			// Verify Every Comment should have proper date
			// 16 October at 08:57 AM

			String cFormat = changeFormat("MM/dd/yyyy hh:mm aa", "dd MMMM yyyy hh:mm aa", franbuzz.getCreatedOnDate());
			String ar[] = cFormat.split(" ");
			Map<String, String> map = new HashMap<String, String>();
			try {
				map.put("day", ar[0]);
				map.put("month", ar[1]);
				map.put("time", ar[3]);
				map.put("ampm", ar[4]);
			} catch (Exception e) {
				Reporter.log(e.getMessage());
			}

			String s1 = map.get("day") + " " + map.get("month");
			if (!fc.utobj().isElementPresent(driver, ".//*[contains(text(),'" + franbuzz.getCommentText()
					+ "')]/preceding-sibling::div/div[@class='fbz_userName']/div[contains(text(),'" + s1 + "')]")) {
				fc.utobj().throwsException("Created Date is not present");
			}
			String s2 = map.get("time") + " " + map.get("ampm");
			if (!fc.utobj().isElementPresent(driver, ".//*[contains(text(),'" + franbuzz.getCommentText()
					+ "')]/preceding-sibling::div/div[@class='fbz_userName']/div[contains(text(),'" + s2 + "')]")) {
				fc.utobj().throwsException("Created Time is not present");
			}

			fc.utobj().printTestStep("Verify Franbuzz comment delete button is working or not");
			fc.utobj().clickElementByJS(driver, fc.utobj().getElementByXpath(driver, ".//*[contains(text(),'"
					+ franbuzz.getCommentText() + "')]/preceding-sibling::div//a[@class='fbz_closeBtn']"));

			fc.utobj().acceptAlertBox(driver);
			if (fc.utobj().assertPageSource(driver, franbuzz.getCommentText())) {
				fc.utobj().throwsException("Not able to delete posted comment");
			}

			fc.utobj().printTestStep("Verify after clicking over user name who posted the comment");
			fc.utobj()
					.clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[contains(text(),'"
							+ franbuzz.getInfoText()
							+ "')]/preceding-sibling::div/div[@class='fbz_userName']//a[@href='franOnlineHome']"));

			if (!fc.utobj().isElementPresent(driver, pobj.userProfileLink)) {
				fc.utobj().throwsException("Not redirecting to user profile");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_franbuzz", "serverhub" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Delete Share Information at Wall At The Hub > FranBuzz > Wall", testCaseId = "TC_254_Delete_Information")
	private void deleteInformation() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubFranBuzzPage pobj = new TheHubFranBuzzPage(driver);

			fc.utobj().printTestStep("Navigate To The Hub > FranBuzz Page");
			fc.utobj().printTestStep("Share Information At Wall");
			String infoText = fc.utobj().generateTestData(dataSet.get("infoText"));
			FranBuzz franbuzz = new FranBuzz();
			franbuzz.setInfoText(infoText);
			franbuzz.setShareTo("All Corporates");
			shareInformation(driver, franbuzz);

			boolean isInfoTextPresent = false;
			for (int i = 0; i < 10; i++) {
				try {
					WebElement element = fc.utobj().getElementByXpath(driver,
							".//*[contains(text () , '" + infoText + "')]");
					element.isDisplayed();
					isInfoTextPresent = true;
					break;
				} catch (Exception e) {
					isInfoTextPresent = false;
					JavascriptExecutor jse = (JavascriptExecutor) driver;
					jse.executeScript("window.scrollBy(0,250)", "");
				}
			}

			if (isInfoTextPresent == false) {
				fc.utobj().throwsException("Not able to share information at Wall");
			}

			fc.utobj().printTestStep("Delete Information");
			Actions ac = new Actions(driver);
			WebElement element = fc.utobj().getElementByXpath(driver, ".//*[contains(text () , '" + infoText + "')]");
			ac.moveToElement(element).build().perform();
			WebElement deleteElement = fc.utobj().getElementByXpath(driver,
					".//div[@class='fbz_postContent' and contains(text () , '" + infoText
							+ "')]/parent::div//div[@class='fbz_controlR']/div[contains(@id , 'closeBtn')]/a[@title='Delete']");
			ac.moveToElement(deleteElement).build().perform();
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", deleteElement);
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Delete Information At Wall");
			fc.utobj().clickElement(driver, pobj.wallTab);
			boolean isTextPresent = fc.utobj().assertPageSource(driver, infoText);
			if (isTextPresent == true) {
				fc.utobj().throwsException("was not able to verify Delete Information Text");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "thehub_mail", "hub_franbuzz"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The New POst At The Hub > FranBuzz > Community", testCaseId = "TC_255_New_Post")
	private void newPost() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubFranBuzzPage pobj = new TheHubFranBuzzPage(driver);
			FranBuzz franbuzz=new FranBuzz();
			
			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest cPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "hubautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			cPage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To The Hub >Franbuzz Page");
			fc.utobj().printTestStep("Add New Post At Community Tab");
			fc.hub().hub_common().theHubFranBuzzSubModule(driver);
			fc.utobj().clickElement(driver, pobj.communityTab);
			fc.utobj().selectDropDown(driver, pobj.selectPost, "All");
			fc.utobj().clickElement(driver, pobj.newPost);
			String title = fc.utobj().generateTestData(dataSet.get("title"));
			fc.utobj().sendKeys(driver, pobj.title, title);

			String htmlText = fc.utobj().generateTestData(dataSet.get("htmlText"));
			fc.utobj().switchFrameById(driver, "ta_ifr");
			Actions actions = new Actions(driver);
			actions.moveToElement(fc.utobj().getElement(driver, pobj.editorTextArea));
			actions.click();
			actions.sendKeys(htmlText);
			fc.utobj().logReportMsg("Entered Text", htmlText);
			actions.build().perform();
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().printTestStep("Verify The Add Post");
			fc.utobj().clickElement(driver, pobj.postBtn);

			fc.utobj().isTextDisplayed(driver, title, "was not able to verify Story Title");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[contains(text () ,'" + title + "')]/../span/a"));
			fc.utobj().isTextDisplayed(driver, htmlText, "was not able to verify complete story");
			
			fc.utobj().printTestStep("Verify The Added Post at wall");
			fc.utobj().clickElement(driver, pobj.wallTab);
			franbuzz.setPostTitle(title);
			franbuzz.setTypeOfPost("Post");
			searchWallPosts(driver, franbuzz);
			
			if (!fc.utobj().assertPageSource(driver, franbuzz.getPostTitle())) {
				fc.utobj().throwsException("Added community is not visible at wall");
			}
			
			fc.utobj().printTestStep("Verify The Mail After The Post");
			Map<String, String> mailInfo = fc.utobj().readMailBox("FranBuzz Post", title, emailId, "sdg@1a@Hfs");
			if (mailInfo.size() == 0 || !mailInfo.get("mailBody").contains(title)) {
				fc.utobj().throwsException("was not able to verify The Mail After Post");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(groups = { "thehub", "hub_franbuzz"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Modify Post At The Hub > FranBuzz > Community", testCaseId = "TC_256_Modify_Post")
	private void modifyPost() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubFranBuzzPage pobj = new TheHubFranBuzzPage(driver);

			fc.utobj().printTestStep("Navigate To The Hub >Franbuzz Page");
			fc.utobj().printTestStep("Add New Post");
			String title = fc.utobj().generateTestData(dataSet.get("title"));
			String htmlText = fc.utobj().generateTestData(dataSet.get("htmlText"));
			FranBuzz franbuzz = new FranBuzz();
			franbuzz.setTitle(title);
			franbuzz.setHtmlText(htmlText);
			newPost(driver, franbuzz);

			fc.utobj().printTestStep("Modify Post");
			fc.utobj().selectDropDown(driver, pobj.selectPost, "All");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[contains(text () , '" + title + "')]/../following-sibling::div/a[@title='Modify']"));

			title = fc.utobj().generateTestData(dataSet.get("title"));
			title=title.concat(fc.utobj().generateRandomSpecialChar());
			fc.utobj().sendKeys(driver, pobj.title, title);

			htmlText = fc.utobj().generateTestData(dataSet.get("htmlText"));
			fc.utobj().switchFrameById(driver, "ta_ifr");
			Actions actions = new Actions(driver);
			actions.moveToElement(fc.utobj().getElement(driver, pobj.editorTextArea));
			actions.click();
			actions.sendKeys(htmlText);
			fc.utobj().logReportMsg("Entered Text", htmlText);
			actions.build().perform();
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, pobj.addImage);
			fc.utobj().sendKeys(driver, pobj.fcImageUpload, fc.utobj().getFilePathFromTestData("Apple_07.jpg"));
			fc.utobj().clickElement(driver, pobj.modifyBtn);
			
			fc.utobj().printTestStep("Verify The Modify Post");
			fc.utobj().selectDropDownByValue(driver, pobj.selectPost, "messages");
			fc.utobj().isTextDisplayed(driver, title, "was not able to verify Story Title");
			
			fc.utobj().printTestStep("To verify the functionality of Franbuzz Community Post on The Hub_Home_page icon button");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[contains(text () , '" + title + "')]/../following-sibling::div/a[@title='Post on The Hub Home page']"));
			fc.utobj().sendKeys(driver, pobj.startDate, fc.utobj().getCurrentDateUSFormat());
			fc.utobj().sendKeys(driver, pobj.expDate, fc.utobj().getFutureDateUSFormat(2));
			fc.utobj().clickElement(driver, pobj.uPhotoTitleUpdateBtn);
			
			fc.utobj().printTestStep("Verify At hub home page");
			fc.hub().hub_common().theHubHome(driver);
			if (!fc.utobj().assertPageSource(driver, title)) {
				fc.utobj().throwsException("Not able to verify at hub home page");
			}
			
			fc.utobj().printTestStep("Verify At Hub > Top Stories");
			fc.hub().hub_common().theHubNewsSubModule(driver);
			fc.utobj().clickElement(driver, new TheHubNewsPage(driver).topStoriesTab);
			
			if (!fc.utobj().assertPageSource(driver, title)) {
				fc.utobj().throwsException("Not able to verify at hub News > Top Stories Folder");
			}
			
			fc.utobj().printTestStep("Verify At Admin > hub > Stories");
			fc.hub().hub_common().adminTheHubStoriesRSSFeedsandHomePageView(driver);
			fc.utobj().showAll(driver);
			
			if (!fc.utobj().assertPageSource(driver, title)) {
				fc.utobj().throwsException("Not able to verify at Admin > hub > Stories");
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(groups = { "thehub", "hub_franbuzz"})
	@TestCase(createdOn = "2018-10-24", updatedOn = "2018-10-24", testCaseDescription = "Verify added comment on post At The Hub > FranBuzz > Community", testCaseId = "TC_Hub_Franbuzz_Community_01")
	private void lYCCommunity() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubFranBuzzPage pobj = new TheHubFranBuzzPage(driver);

			fc.utobj().printTestStep("Navigate To The Hub >Franbuzz Page");
			fc.utobj().printTestStep("Add New Post");
			String title = fc.utobj().generateTestData(dataSet.get("title"));
			String htmlText = fc.utobj().generateTestData(dataSet.get("htmlText"));
			FranBuzz franbuzz = new FranBuzz();
			franbuzz.setTitle(title);
			franbuzz.setHtmlText(htmlText);
			franbuzz.setCommentText(fc.utobj().generateRandomSpecialChar());
			newPost(driver, franbuzz);
			
			fc.utobj().printTestStep("leave your comment on post");
			String tempText = fc.utobj()
					.getElementByXpath(driver,
							".//*[contains(text () , '"+title+"')]/ancestor::div[@class='fbz-community-inner']")
					.getAttribute("onmouseover");
			tempText = tempText.substring(tempText.indexOf("('") + 2, tempText.indexOf("')"));

			WebElement element = fc.utobj().getElementByID(driver, "pcomment" + tempText);
			fc.utobj().sendKeys(driver, element, franbuzz.getCommentText());
			WebElement postElement = fc.utobj().getElementByID(driver, "btnReply" + tempText);
			fc.utobj().clickElement(driver, postElement);
			
			if (!fc.utobj().assertPageSource(driver, franbuzz.getCommentText())) {
				fc.utobj().throwsException("Not able to verify comment having spacial charcter");
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_franbuzz" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Delete Post :: The Hub > FranBuzz > Community", testCaseId = "TC_257_Delete_Post")
	private void deletePost() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubFranBuzzPage pobj = new TheHubFranBuzzPage(driver);

			fc.utobj().printTestStep("Navigate To The Hub >Franbuzz Page");
			fc.utobj().printTestStep("Add New Post");
			String title = fc.utobj().generateTestData(dataSet.get("title"));
			String htmlText = fc.utobj().generateTestData(dataSet.get("htmlText"));
			FranBuzz franbuzz = new FranBuzz();
			franbuzz.setTitle(title);
			franbuzz.setHtmlText(htmlText);
			newPost(driver, franbuzz);

			fc.utobj().printTestStep("Delete Post");
			fc.utobj().selectDropDown(driver, pobj.selectPost, "All");

			Actions ac = new Actions(driver);
			ac.moveToElement(fc.utobj().getElementByXpath(driver, ".//*[contains(text () , '" + title + "')]")).build()
					.perform();
			WebElement element = fc.utobj().getElementByXpath(driver,
					".//*[contains(text () , '" + title + "')]/../following-sibling::div/div/a[@title='Delete']");

			ac.moveToElement(element).build().perform();
			element.click();

			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Delete Post");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, title);
			if (isTextPresent == true) {
				fc.utobj().throwsException("was not able to delete new Post");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_franbuzz" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Create Group At The Hub > FranBuzz > Create New Group", testCaseId = "TC_258_Create_Group")
	private void createGroup() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubFranBuzzPage pobj = new TheHubFranBuzzPage(driver);

			fc.utobj().printTestStep("Navigate To The Hub > FranBuzz > Groups Page");
			fc.utobj().printTestStep("Create Group");
			fc.hub().hub_common().theHubFranBuzzSubModule(driver);
			fc.utobj().clickElement(driver, pobj.createGroupLink);
			String groupTitle = fc.utobj().generateTestData(dataSet.get("groupTitle"));
			fc.utobj().sendKeys(driver, pobj.groupTitle, groupTitle);
			String groupDescription = fc.utobj().generateTestData(dataSet.get("groupDescription"));
			fc.utobj().sendKeys(driver, pobj.groupDescription, groupDescription);
			if (!fc.utobj().isSelected(driver, pobj.publicGroup)) {
				fc.utobj().clickElement(driver, pobj.publicGroup);
			}

			String file = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			fc.utobj().sendKeys(driver, pobj.groupImgeUpload, file);

			fc.utobj().clickElement(driver, pobj.selectAllCorporateUser);
			fc.utobj().clickElement(driver, pobj.createGroupBtn);

			fc.utobj().printTestStep("Verify Create Group");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, groupTitle);
			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to add group");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_franbuzz" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Modify Group At The Hub > FranBuzz > Create New Group", testCaseId = "TC_259_Modify_Group")
	private void modifyGroup() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubFranBuzzPage pobj = new TheHubFranBuzzPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");

			fc.utobj().printTestStep("Navigate To The Hub > FranBuzz > Groups Page");
			fc.utobj().printTestStep("Create Group");
			String groupTitle = fc.utobj().generateTestData(dataSet.get("groupTitle"));
			String groupDescription = fc.utobj().generateTestData(dataSet.get("groupDescription"));
			createGroup(driver, groupTitle, groupDescription, config);

			fc.utobj().printTestStep("Modify Group");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//li[@title='" + groupTitle + "']"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[.='" + groupTitle + "']/../div[@class='fbz_messageR']/a[@class='fbz_editIcon']"));

			groupTitle = fc.utobj().generateTestData(dataSet.get("groupTitle"));
			fc.utobj().sendKeys(driver, pobj.groupTitle, groupTitle);
			groupDescription = fc.utobj().generateTestData(dataSet.get("groupDescription"));
			fc.utobj().sendKeys(driver, pobj.groupDescription, groupDescription);
			fc.utobj().clickElement(driver, pobj.modifyGroupBtn);

			fc.utobj().printTestStep("Verify The Modify Group");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, groupTitle);
			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to modify group");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_franbuzz" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Delete Group At The Hub > FranBuzz > Create New Group", testCaseId = "TC_260_Delete_Group")
	private void deleteGroup() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");

			fc.utobj().printTestStep("Navigate To The Hub > FranBuzz > Groups Page");
			fc.utobj().printTestStep("Create Group");
			String groupTitle = fc.utobj().generateTestData(dataSet.get("groupTitle"));
			String groupDescription = fc.utobj().generateTestData(dataSet.get("groupDescription"));
			createGroup(driver, groupTitle, groupDescription, config);

			fc.utobj().printTestStep("Delete Group");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//li[@title='" + groupTitle + "']"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[.='" + groupTitle + "']/../div[@class='fbz_messageR']/a[@class='fbz_closeBtn']"));
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Delete Group");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, groupTitle);
			if (isTextPresent == true) {
				fc.utobj().throwsException("was not able to delete group");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "thehub_mail", "hub_franbuzz" })
	@TestCase(createdOn = "2017-11-08", updatedOn = "2017-11-08", testCaseDescription = "Verify Offensive keyword mail is sent to added user", testCaseId = "TC_TheHub_Franbuzz_Post_001")
	private void verifyMailOffensiveWord() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			AdminConfigurationConfigureOffensiveWatchGreatKeywordsforForumandFranbuzzPageTest offensive_Keyword = new AdminConfigurationConfigureOffensiveWatchGreatKeywordsforForumandFranbuzzPageTest();
			fc.utobj().printTestStep("Navigate To Admin > Configuration > Email Contacts");
			fc.utobj().printTestStep("Add New Contact For New User");
			String emailId = "hubautomation@staffex.com";
			offensive_Keyword.addEmailContactUser(driver, emailId);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Offensive KeyWords");
			fc.utobj().printTestStep("Add Offensive Keyword");

			String offensiveKeyword = fc.utobj().generateTestData(dataSet.get("offensiveKeyword"));
			offensive_Keyword.addOffensiveWord(driver, offensiveKeyword);

			fc.utobj().printTestStep("Verify The Mail When Offensive Keyword configuration enable");
			offensive_Keyword.enableAnddisableConfigurationOffensiveKeyword(driver, true);

			fc.utobj().printTestStep("Navigate To The Hub > FranBuzz Page");
			fc.utobj().printTestStep("Share Information At Wall");
			FranBuzz franbuzz = new FranBuzz();
			franbuzz.setInfoText(offensiveKeyword);
			franbuzz.setShareTo("All Corporates");
			shareInformation(driver, franbuzz);

			fc.utobj().printTestStep("Verify The mail which is sent by system after posting Offensive Word");

			String expectedSubject = "Wall Post Detection Mail";
			String expectedMessageBody = offensiveKeyword;
			Map<String, String> mailDataMap = fc.utobj().readMailBox(expectedSubject, expectedMessageBody, emailId,
					"sdg@1a@Hfs");

			if (!mailDataMap.get("mailBody").contains(offensiveKeyword)) {
				fc.utobj().throwsException(
						"not able to verify mail which is sent by system after detecting offensive keyword");
			}

			fc.utobj().printTestStep("Verify The Mail When Offensive Keyword configuration disable");
			offensive_Keyword.enableAnddisableConfigurationOffensiveKeyword(driver, false);

			offensiveKeyword = fc.utobj().generateTestData("TestK");

			fc.utobj().printTestStep("Navigate To The Hub > FranBuzz Page");
			fc.utobj().printTestStep("Share Information At Wall");
			franbuzz.setInfoText(offensiveKeyword);
			shareInformation(driver, franbuzz);

			fc.utobj().printTestStep("Verify The mail which is sent by system after posting Offensive Word");

			expectedMessageBody = offensiveKeyword;
			Map<String, String> mailDataMap1 = fc.utobj().readMailBoxForNoMail(expectedSubject, expectedMessageBody,
					emailId, "sdg@1a@Hfs");

			if (mailDataMap1.get("flag").equalsIgnoreCase("false")) {
				fc.utobj().throwsException("Not able to verify the offensive keyword disable configuration");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "thehub_mail", "hub_franbuzz" })
	@TestCase(createdOn = "2017-11-09", updatedOn = "2017-11-09", testCaseDescription = "Verify Watch keyword mail is sent to added user", testCaseId = "TC_TheHub_Franbuzz_Post_002")
	private void verifyMailWatchWord() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			AdminConfigurationConfigureOffensiveWatchGreatKeywordsforForumandFranbuzzPageTest offensive_Keyword = new AdminConfigurationConfigureOffensiveWatchGreatKeywordsforForumandFranbuzzPageTest();
			fc.utobj().printTestStep("Navigate To Admin > Configuration > Email Contacts");
			fc.utobj().printTestStep("Add New Contact For New User");
			String emailId = "hubautomation@staffex.com";
			offensive_Keyword.addEmailContactUser(driver, emailId);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Offensive KeyWords");
			fc.utobj().printTestStep("Add Watch Keyword");

			String watchKeyword = fc.utobj().generateTestData(dataSet.get("watchKeyword"));
			offensive_Keyword.watchWord(driver, watchKeyword);

			fc.utobj().printTestStep("Verify The Mail When Watch Keyword configuration enable");
			offensive_Keyword.enableAnddisableConfigurationWatchKeyword(driver, true);

			fc.utobj().printTestStep("Navigate To The Hub > FranBuzz Page");
			fc.utobj().printTestStep("Share Information At Wall");
			FranBuzz franbuzz = new FranBuzz();
			franbuzz.setInfoText(watchKeyword);
			franbuzz.setShareTo("All Corporates");
			shareInformation(driver, franbuzz);

			fc.utobj().printTestStep("Verify The mail which is sent by system after posting watch KeyWord");

			String expectedSubject = "Wall Post Detection Mail";
			String expectedMessageBody = watchKeyword;
			Map<String, String> mailDataMap = fc.utobj().readMailBox(expectedSubject, expectedMessageBody, emailId,
					"sdg@1a@Hfs");

			if (!mailDataMap.get("mailBody").contains(watchKeyword)) {
				fc.utobj().throwsException(
						"not able to verify mail which is sent by system after detecting watch keyword");
			}

			fc.utobj().printTestStep("Verify The Mail When Offensive Keyword configuration disable");
			offensive_Keyword.enableAnddisableConfigurationWatchKeyword(driver, false);

			watchKeyword = fc.utobj().generateTestData("Testw");

			fc.utobj().printTestStep("Navigate To The Hub > FranBuzz Page");
			fc.utobj().printTestStep("Share Information At Wall");
			franbuzz.setInfoText(watchKeyword);
			shareInformation(driver, franbuzz);

			fc.utobj().printTestStep("Verify The mail which is sent by system after posting Watch Word");

			expectedMessageBody = watchKeyword;
			Map<String, String> mailDataMap1 = fc.utobj().readMailBoxForNoMail(expectedSubject, expectedMessageBody,
					emailId, "sdg@1a@Hfs");

			if (mailDataMap1.get("flag").equalsIgnoreCase("false")) {
				fc.utobj().throwsException("Not able to verify the Watch keyword disable configuration");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "thehub_mail", "hub_franbuzz" })
	@TestCase(createdOn = "2017-11-09", updatedOn = "2017-11-09", testCaseDescription = "Verify Great keyword mail is sent to added user", testCaseId = "TC_TheHub_Franbuzz_Post_003")
	private void verifyMailGreatWord() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			AdminConfigurationConfigureOffensiveWatchGreatKeywordsforForumandFranbuzzPageTest offensive_Keyword = new AdminConfigurationConfigureOffensiveWatchGreatKeywordsforForumandFranbuzzPageTest();
			fc.utobj().printTestStep("Navigate To Admin > Configuration > Email Contacts");
			fc.utobj().printTestStep("Add New Contact For New User");
			String emailId = "hubautomation@staffex.com";
			offensive_Keyword.addEmailContactUser(driver, emailId);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Offensive KeyWords");
			fc.utobj().printTestStep("Add Great Keyword");

			String greatKeyword = fc.utobj().generateTestData(dataSet.get("greatKeyword"));
			offensive_Keyword.greatWord(driver, greatKeyword);

			fc.utobj().printTestStep("Verify The Mail When Great Keyword configuration enable");
			offensive_Keyword.enableAnddisableConfigurationGreatKeyword(driver, true);

			fc.utobj().printTestStep("Navigate To The Hub > FranBuzz Page");
			fc.utobj().printTestStep("Share Information At Wall");
			FranBuzz franbuzz = new FranBuzz();
			franbuzz.setInfoText(greatKeyword);
			franbuzz.setShareTo("All Corporates");
			shareInformation(driver, franbuzz);

			fc.utobj().printTestStep("Verify The mail which is sent by system after posting Great KeyWord");

			String expectedSubject = "Wall Post Detection Mail";
			String expectedMessageBody = greatKeyword;
			Map<String, String> mailDataMap = fc.utobj().readMailBox(expectedSubject, expectedMessageBody, emailId,
					"sdg@1a@Hfs");

			if (!mailDataMap.get("mailBody").contains(greatKeyword)) {
				fc.utobj().throwsException(
						"not able to verify mail which is sent by system after detecting Great keyword");
			}

			fc.utobj().printTestStep("Verify The Mail When Great Keyword configuration disable");
			offensive_Keyword.enableAnddisableConfigurationGreatKeyword(driver, false);

			greatKeyword = fc.utobj().generateTestData("Testw");
			fc.utobj().printTestStep("Navigate To The Hub > FranBuzz Page");
			fc.utobj().printTestStep("Share Information At Wall");
			franbuzz.setInfoText(greatKeyword);
			shareInformation(driver, franbuzz);

			fc.utobj().printTestStep("Verify The mail which is sent by system after posting Great Word");

			expectedMessageBody = greatKeyword;
			Map<String, String> mailDataMap1 = fc.utobj().readMailBoxForNoMail(expectedSubject, expectedMessageBody,
					emailId, "sdg@1a@Hfs");

			if (mailDataMap1.get("flag").equalsIgnoreCase("false")) {
				fc.utobj().throwsException("Not able to verify the Great keyword disable configuration");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_franbuzz"})
	@TestCase(createdOn = "2018-01-10", updatedOn = "2017-01-10", testCaseDescription = "Verify Album Created On date after searching by SOLR search", testCaseId = "TC_Hub_Franbuzz_Solr_01")
	private void solrSearch01() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Verify Franbuzz in the album without entering any data");
			fc.utobj().printTestStep("Navigate To The Hub > FranBuzz Page");
			TheHubFranBuzzPage pobjFranB = new TheHubFranBuzzPage(driver);
			fc.hub().hub_common().theHubFranBuzzSubModule(driver);
			fc.utobj().clickElement(driver, pobjFranB.photoLink);
			fc.utobj().clickElement(driver, pobjFranB.createAlbum);
			fc.utobj().clickElement(driver, pobjFranB.createBtn);

			if (!"Please enter album title.".equals(fc.utobj().acceptAlertBox(driver))) {
				fc.utobj().throwsException("Not able to verify alert and alert messages for album");
			}
			fc.utobj().clickElement(driver, pobjFranB.cancleBtn);

			FranBuzz franbuzz = new FranBuzz();
			franbuzz.setAlbumTitle(fc.utobj().generateTestData(dataSet.get("albumTitle")));
			franbuzz.setAlbumDescription(fc.utobj().generateTestData(dataSet.get("albumDescription")));
			franbuzz.setShareTo("All Corporate");
			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("coverUploadPhoto"));
			franbuzz.setCoverTitle(fc.utobj().generateTestData(dataSet.get("coverTitle")));
			franbuzz.setCoverUploadPhoto(filePath);
			fc.utobj().printTestStep("Go To Hub > Franbuzz > Create Your Album");

			createYourAlbum(driver, franbuzz);
			fc.utobj().printTestStep("Search Created album by search wall post");
			franbuzz.setTypeOfPost("Album");
			searchWallPosts(driver, franbuzz);

			fc.utobj().clickLink(driver, franbuzz.getAlbumTitle());
			fc.utobj().clickElement(driver, pobjFranB.addPhotosLnk);
			String title0 = fc.utobj().generateTestData("Test");
			fc.utobj().sendKeys(driver, pobjFranB.apTitle0, title0);
			fc.utobj().sendKeys(driver, pobjFranB.apBrowse0, fc.utobj().getFilePathFromTestData("Apple_01.jpg"));
			fc.utobj().clickElement(driver, pobjFranB.apUploadBtn);

			if (!fc.utobj().assertPageSource(driver, title0)) {
				fc.utobj().throwsException("not able to add photo in album");
			}

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[@title='" + title0 + "']/img"));
			if (!getUploadedPhotoResponse(driver, franbuzz)) {
				fc.utobj().throwsException("Not able to verify the added photo in album");
			}

			fc.utobj().printTestStep("Verify Created on date should be display after Solr search");
			TheHubLibraryPage pobj = new TheHubLibraryPage(driver);
			fc.utobj().sendKeys(driver, pobj.searchKeyTextBx, franbuzz.getAlbumTitle());
			fc.utobj().clickEnterOnElement(driver, pobj.searchKeyTextBx);
			WebDriverWait wait = new WebDriverWait(driver, 120);
			fc.utobj().clickElement(driver, wait.until(ExpectedConditions.visibilityOfElementLocated(
					By.xpath(".//*[@class='link-style ellipsis ng-binding' and contains(text(),'The Hub')]"))));
			fc.utobj().moveToElementThroughAction(driver, fc.utobj().getElementByXpath(driver,
					".//*[@class='ng-binding' and .='" + franbuzz.getAlbumTitle() + "']"));

			boolean isCreatedOnPresent = fc.utobj().isElementPresent(driver,
					".//*[.='Created On ']/following-sibling::div[contains(text(),'" + franbuzz.getCreatedOnDate()
							+ "')]");
			if (!isCreatedOnPresent) {
				fc.utobj().throwsException("Created On is not present after searching by Solr search");
			}
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@class='ng-binding' and .='" + franbuzz.getAlbumTitle() + "']"));

			String cFormat = changeFormat("MM/dd/yyyy hh:mm aa", "dd MMMM yyyy hh:mm aa", franbuzz.getCreatedOnDate());
			String ar[] = cFormat.split(" ");
			Map<String, String> map = new HashMap<String, String>();
			try {
				map.put("day", ar[0]);
				map.put("month", ar[1]);
				map.put("time", ar[3]);
				map.put("ampm", ar[4]);
			} catch (Exception e) {
				Reporter.log(e.getMessage());
			}

			String s1 = map.get("day") + " " + map.get("month");
			if (!fc.utobj().isElementPresent(driver, ".//div[contains(text(),'" + s1 + "')]")) {
				fc.utobj().throwsException("Created Date is not present");
			}
			String s2 = map.get("time") + " " + map.get("ampm");
			if (!fc.utobj().isElementPresent(driver, ".//div[contains(text(),'" + s2 + "')]")) {
				fc.utobj().throwsException("Created Time is not present");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(groups = { "thehub", "hub_franbuzz"})
	@TestCase(createdOn = "2018-01-10", updatedOn = "2017-01-10", testCaseDescription = "To Verify Franbuzz the Documents functionality on franbuzz home page", testCaseId = "TC_Hub_Franbuzz_Documents_01")
	private void shareDocuments() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To The Hub > FranBuzz Page");
			TheHubFranBuzzPage pobj = new TheHubFranBuzzPage(driver);
			fc.hub().hub_common().theHubFranBuzzSubModule(driver);
			
			FranBuzz franbuzz=new FranBuzz();
			franbuzz.setDocumentTitle(fc.utobj().generateTestData(dataSet.get("documentTitle")));
			String file=dataSet.get("documentFile");
			franbuzz.setDocumentFile(fc.utobj().getFilePathFromTestData(file));
			fc.utobj().printTestStep("Share Documents");
			fc.utobj().clickElement(driver, pobj.documentLink);
			fc.utobj().sendKeys(driver, pobj.documentTitle, franbuzz.getDocumentTitle());
			fc.utobj().sendKeys(driver, pobj.uploadDocumentTxBx, franbuzz.getDocumentFile());
			fc.utobj().selectDropDown(driver, pobj.docShareTo, "All Corporates");
			fc.utobj().clickElement(driver, pobj.shareDocumentBtn);
			
			fc.utobj().printTestStep("Search added documents");
			franbuzz.setTypeOfPost("Documents");
			searchWallPosts(driver, franbuzz);
			
			fc.utobj().printTestStep("Verify added document's title");
			if (!fc.utobj().assertPageSource(driver, franbuzz.getDocumentTitle())) {
				fc.utobj().throwsException("Not able to verify the added document's title");
			}
			
			fc.utobj().printTestStep("Verify The Added Document file");
			fc.utobj().clickLink(driver, franbuzz.getDocumentTitle());
			
			String text="";
			try {
				text=fc.utobj().getAttributeValue(fc.utobj().getElementByLinkText(driver, franbuzz.getDocumentTitle()), "href");
				text=text.substring(text.indexOf('\'')+1, text.indexOf(',')-1);
			} catch (Exception e) {
				Reporter.log("Not able to get file name :"+e.getMessage());
			}
			
			if (!fc.utobj().isFileFound(new File(FranconnectUtil.config.get("downloadFilepath")), text)) {
				fc.utobj().throwsException("Not able to verify added document");
			}
			
			fc.utobj().printTestStep("Delete the added document");
			Actions ac = new Actions(driver);
			WebElement element = fc.utobj().getElementByXpath(driver, ".//*[contains(text () , '" + franbuzz.getDocumentTitle() + "')]");
			ac.moveToElement(element).build().perform();
			WebElement deleteElement = fc.utobj().getElementByXpath(driver,
					".//div[@class='fbz_postContent']/a[contains(text () , '"+franbuzz.getDocumentTitle()+"')]/ancestor::div[@class='fbz_post']//div/a[@title='Delete']");
			ac.moveToElement(deleteElement).build().perform();
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", deleteElement);
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Deleted Document");
			fc.utobj().clickElement(driver, pobj.wallTab);
			boolean isTextPresent = fc.utobj().assertPageSource(driver, franbuzz.getDocumentTitle());
			if (isTextPresent == true) {
				fc.utobj().throwsException("not able to delete the added document");
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(groups = { "thehub", "hub_franbuzz"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-16", testCaseDescription = "Verify Birthday Calendar", testCaseId = "TC_Hub_Franbuzz_Calander_01")
	public void birthdayCalender01() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubFranBuzzPage pobj = new TheHubFranBuzzPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest cPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "hubautomation@staffex.com";

			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser.setFirstName("Fn");
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			cPage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			CorporateUser corpUser1 = new CorporateUser();
			corpUser1.setEmail(emailId);
			corpUser1.setFirstName("Fsn");
			corpUser1 = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser1);
			cPage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser1);
			
			fc.utobj().printTestStep("Navigate To The Hub > FranBuzz Page");
			fc.hub().hub_common().theHubFranBuzzSubModule(driver);

			fc.utobj().printTestStep("update added corporate user's birthday");
			searchUserByUserFullName(driver, corpUser);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@class='memberLinks']/a[contains(text(),'"+corpUser.getFirstName()+"')]"));
			
			fc.utobj().clickElement(driver, pobj.editUserProfile);
			Map<String, String> map=new HashMap<>();
			try {
				String date=fc.utobj().getCurrentTimeUDF("MM/dd/yyyy", "GMT-4:00");
				String ar[]=date.split("/");
				for (int i = 0; i < ar.length; i++) {
					if (i==0) {
						int a=Integer.parseInt(ar[i]);
						a=a-1;
						map.put("month", String.valueOf(a));
					}else if(i==1){
						map.put("day", ar[i]);
					}
				}
			} catch (Exception e) {
				Reporter.log("Not able to parse date : "+e.getMessage());
			}
			fc.utobj().selectDropDownByValue(driver, pobj.birthMonth, Integer.parseInt(map.get("month"))+"");
			fc.utobj().selectDropDownByValue(driver, pobj.birthDate, Integer.parseInt(map.get("day"))+"");
			fc.utobj().clickElement(driver, pobj.saveChangesBtn);
			
			fc.utobj().printTestStep("update second added corporate user's birthday");
			searchUserByUserFullName(driver, corpUser1);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@class='memberLinks']/a[contains(text(),'"+corpUser1.getFirstName()+"')]"));
			
			fc.utobj().clickElement(driver, pobj.editUserProfile);
			Map<String, String> map1=new HashMap<>();
			try {
				String date=fc.utobj().getCurrentTimeUDF("MM/dd/yyyy", "GMT-4:00");
				String ar[]=date.split("/");
				for (int i = 0; i < ar.length; i++) {
					if (i==0) {
						int a=Integer.parseInt(ar[i]);
						a=a-1;
						map1.put("month", String.valueOf(a));
					}else if(i==1){
						map1.put("day", ar[i]);
					}
				}
			} catch (Exception e) {
				Reporter.log("Not able to parse date : "+e.getMessage());
			}
			fc.utobj().selectDropDownByValue(driver, pobj.birthMonth, Integer.parseInt(map1.get("month"))+"");
			fc.utobj().selectDropDownByValue(driver, pobj.birthDate, Integer.parseInt(map1.get("day"))+"");
			fc.utobj().clickElement(driver, pobj.saveChangesBtn);
			
			fc.utobj().printTestStep("upcoming members birthday should be display Under Birthday calendar");
			if (!fc.utobj().isElementPresent(driver, ".//*[@id='birthdayList']//a[contains(text(),'"+corpUser.getFirstName()+"')]")) {
				fc.utobj().throwsException("upcoming members birthday is not displaying");
			}
			
			if (!fc.utobj().isElementPresent(driver, ".//*[@id='birthdayList']//a[contains(text(),'"+corpUser1.getFirstName()+"')]")) {
				fc.utobj().throwsException("second upcoming members birthday is not displaying");
			}
			
			fc.utobj().printTestStep("Birthday calendar should be highlighted");
			fc.utobj().clickElement(driver, pobj.birthDateCalendarLink);
			if (!fc.utobj().isElementPresent(driver, ".//*[@class='currentBday']/*[.='"+map.get("day")+"']")) {
				fc.utobj().throwsException("Birthday is not coming in calendar");
			}
			
			fc.utobj().printTestStep("Verify users in birthday calendar");
			if (!fc.utobj().isElementPresent(driver, ".//*[@original-title='"+corpUser.getuserFullName()+"']")) {
				fc.utobj().throwsException("First user's birthday is not coming in calendar");
			}
			if (!fc.utobj().isElementPresent(driver, ".//*[@original-title='"+corpUser1.getuserFullName()+"']")) {
				fc.utobj().throwsException("Second user's birthday is not coming in calendar");
			}
			
			fc.utobj().printTestStep("Verify Franbuzz on the calendar page month scroll button is working or not");
			String month=fc.utobj().getText(driver, pobj.currentMonth);
			
			Map<String, String> mapMonth=getMonths(month);
			fc.utobj().clickElement(driver, pobj.nextArrow);
			if (!fc.utobj().isElementPresent(driver, ".//*[@class='subTitleText' and .='"+mapMonth.get("nextmonth")+"']")) {
				fc.utobj().throwsException("Not able to verify next month after click over next arrow");
			}
			fc.utobj().clickElement(driver, pobj.prevArrow);
			fc.utobj().clickElement(driver, pobj.prevArrow);
			if (!fc.utobj().isElementPresent(driver, ".//*[@class='subTitleText' and .='"+mapMonth.get("prevmonth")+"']")) {
				fc.utobj().throwsException("Not able to verify previous month after click over previous arrow");
			}
			
			fc.utobj().printTestStep("Send Wishes");
			fc.utobj().clickElement(driver, pobj.wallTab);
			fc.utobj().clickElement(driver, ".//a[contains(text(),'"+corpUser.getFirstName()+"')]/following-sibling::label[.='Send Wishes']");
			FranBuzz franBuzz=new FranBuzz();
			franBuzz.setBirthDayMessages(fc.utobj().generateTestData("Birth"));
			fc.utobj().sendKeys(driver, pobj.birthDayTextField, franBuzz.getBirthDayMessages());
			fc.utobj().clickElement(driver, pobj.sendBWishesBtn);
			
			fc.utobj().printTestStep("Logged Out from the build and login by Corporate user");
			fc.home_page().logout(driver);
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());
			
			fc.utobj().printTestStep("Navigate To The Hub > FranBuzz Page");
			fc.hub().hub_common().theHubFranBuzzSubModule(driver);
			fc.utobj().clickElement(driver, pobj.inboxTab);
			
			if (!fc.utobj().assertPageSource(driver, franBuzz.getBirthDayMessages())) {
				fc.utobj().throwsException("Sent Message in not visible in user's inbox");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(groups = { "thehub", "hub_franbuzz" ,"hubdev1023"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-16", testCaseDescription = "Verify Birthday Calendar", testCaseId = "TC_Hub_Franbuzz_Calander_02")
	public void birthdayCalender02() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubFranBuzzPage pobj = new TheHubFranBuzzPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = "Test127Region".concat(fc.utobj().generateRandomNumber());
			String storeType = "Test127StoreType".concat(fc.utobj().generateRandomNumber());
			String franchiseId = "Test127FranId".concat(fc.utobj().generateRandomNumber());
			AdminFranchiseLocationAddFranchiseLocationPageTest addFranchiseLocation = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String ownerFirstName = fc.utobj().generateTestData("Ffn");
			String ownerLastName = fc.utobj().generateTestData("Lname");
			addFranchiseLocation.addFranchiseLocation(driver, regionName, storeType, franchiseId, "Test Center",
					fc.utobj().getCurrentDateUSFormat(), ownerFirstName, ownerLastName, null);

			fc.utobj().printTestStep("Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest addFranchiseUser = new AdminUsersManageManageFranchiseUsersPageTest();
			String roleName = "Default Franchise Role";
			String userName = fc.utobj().generateTestData("Franusr");
			String password = "t0n1ght123";
			String emailId = "hubautomation@staffex.com";
			addFranchiseUser.addFranchiseUser(driver, userName, password, franchiseId, roleName, emailId);
			AdminUsersManageManageFranchiseUsersPage franchise_pobj = new AdminUsersManageManageFranchiseUsersPage(driver);
			fc.utobj().printTestStep("update added Franchise user's birthday");
			fc.utobj().selectValFromMultiSelect(driver, franchise_pobj.Franchise_IDMulti, franchiseId);
			fc.utobj().clickElement(driver, franchise_pobj.searchButton);
			fc.utobj().actionImgOption(driver, franchiseId, "Modify");
			
			Map<String, String> map=new HashMap<>();
			try {
				String date=fc.utobj().getCurrentTimeUDF("MM/dd/yyyy", "GMT-4:00");
				String ar[]=date.split("/");
				for (int i = 0; i < ar.length; i++) {
					if (i==0) {
						int a=Integer.parseInt(ar[i]);
						a=a-1;
						map.put("month", String.valueOf(a));
					}else if(i==1){
						map.put("day", ar[i]);
					}
				}
			} catch (Exception e) {
				Reporter.log("Not able to parse date : "+e.getMessage());
			}
			fc.utobj().selectDropDownByValue(driver, pobj.birthMonth, Integer.parseInt(map.get("month"))+"");
			fc.utobj().selectDropDownByValue(driver, pobj.birthDate, Integer.parseInt(map.get("day"))+"");
			fc.utobj().clickElement(driver, franchise_pobj.submit);
			
			fc.utobj().printTestStep("Navigate To The Hub > FranBuzz Page");
			fc.hub().hub_common().theHubFranBuzzSubModule(driver);
			
			fc.utobj().printTestStep("upcoming members birthday should be display Under Birthday calendar");
			if (!fc.utobj().isElementPresent(driver, ".//*[@id='birthdayList']//a[contains(text(),'"+ownerFirstName+"')]")) {
				fc.utobj().throwsException("upcoming members birthday is not displaying");
			}
			
			fc.utobj().printTestStep("Verify under recent activities on Wall");
			if(fc.utobj().isElementPresent(driver, 
					".//*[@id='profileList']//a[contains(text(),'"+ownerFirstName+"')]/following-sibling::div[contains(text(),'Birth Date')]")){
				fc.utobj().throwsException("No recent activity is created after updating the user's birthday");
			}
			
			fc.utobj().printTestStep("Birthday calendar should be highlighted");
			fc.utobj().clickElement(driver, pobj.birthDateCalendarLink);
			if (!fc.utobj().isElementPresent(driver, ".//*[@class='currentBday']/*[.='"+map.get("day")+"']")) {
				fc.utobj().throwsException("Birthday is not coming in calendar");
			}
			
			fc.utobj().printTestStep("Verify users in birthday calendar");
			String fullName=ownerFirstName+" "+ownerLastName;
			if (!fc.utobj().isElementPresent(driver, ".//*[@original-title='"+fullName+"']")) {
				fc.utobj().throwsException("Franchise user's birthday is not coming in calendar");
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(groups = { "thehub", "hub_franbuzz"})
	@TestCase(createdOn = "2018-10-24", updatedOn = "2018-10-24", testCaseDescription = "Verify Shared Link", testCaseId = "TC_Hub_Franbuzz_Link_01")
	public void shareLink() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Go to Hub > Frannbuzz and share Link");
			FranBuzz franbuzz=new FranBuzz();
			franbuzz.setLinkText(dataSet.get("linkText"));
			franbuzz.setShareTo("All Corporate");
			
			TheHubFranBuzzPage pobj = new TheHubFranBuzzPage(driver);
			fc.hub().hub_common().theHubFranBuzzSubModule(driver);
			fc.utobj().clickElement(driver, pobj.wallTab);
			fc.utobj().clickElement(driver, pobj.linkTab);
			
			fc.utobj().sendKeys(driver, pobj.linkTxBx, "google");
			fc.utobj().clickElement(driver, pobj.shareLinkBtn);
			
			if (!"Please enter valid URL.".equals(fc.utobj().acceptAlertBox(driver))) {
				fc.utobj().throwsException("Not able to verify alert messages");
			}
			
			fc.utobj().sendKeys(driver, pobj.linkTxBx, franbuzz.getLinkText());
			fc.utobj().selectDropDown(driver, pobj.shareToLinkDD, franbuzz.getShareTo());
			fc.utobj().clickElement(driver, pobj.shareLinkBtn);
			
			fc.utobj().printTestStep("Search Link by search");
			franbuzz.setTypeOfPost("Link");
			searchWallPosts(driver, franbuzz);
			fc.utobj().printTestStep("Verify The shared link");
			
			if (!fc.utobj().assertLinkPartialText(driver, franbuzz.getLinkText())) {
				fc.utobj().throwsException("Shared Link is not present at wall");
			}
			String parentWindow=driver.getWindowHandle();
			fc.utobj().clickPartialLinkText(driver, franbuzz.getLinkText());
			Set<String> allWindow=driver.getWindowHandles();
			for (String window : allWindow) {
				if (!window.equals(parentWindow)) {
					driver.switchTo().window(window);
					if (!driver.getCurrentUrl().contains(franbuzz.getLinkText())) {
						fc.utobj().throwsException("Shared Link is not working");
						driver.close();
						break;
					}
				}
			}
			driver.switchTo().window(parentWindow);
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	
	
	
	Map<String, String> getMonths(String month){
		
		Map<String, String> map=new HashMap<>();
		
		if (month.equalsIgnoreCase("January")) {
			map.put("prevmonth", "December");
			map.put("nextmonth", "February");
			
		}else if(month.equalsIgnoreCase("February")){
			map.put("prevmonth", "January");
			map.put("nextmonth", "March");
			
		}else if(month.equalsIgnoreCase("March")){
			map.put("prevmonth", "February");
			map.put("nextmonth", "April");
			
		}else if(month.equalsIgnoreCase("April")){
			map.put("prevmonth", "March");
			map.put("nextmonth", "May");
			
		}else if(month.equalsIgnoreCase("May")){
			map.put("prevmonth", "April");
			map.put("nextmonth", "June");
		}else if(month.equalsIgnoreCase("June")){
			map.put("prevmonth", "May");
			map.put("nextmonth", "July");
		}else if(month.equalsIgnoreCase("July")){
			map.put("prevmonth", "June");
			map.put("nextmonth", "August");
			
		}else if(month.equalsIgnoreCase("August")){
			map.put("prevmonth", "July");
			map.put("nextmonth", "September");
			
		}else if(month.equalsIgnoreCase("September")){
			map.put("prevmonth", "August");
			map.put("nextmonth", "October");
			
		}else if(month.equalsIgnoreCase("October")){
			map.put("prevmonth", "September");
			map.put("nextmonth", "November");
			
		}else if(month.equalsIgnoreCase("November")){
			map.put("prevmonth", "October");
			map.put("nextmonth", "December");
			
		}else if(month.equalsIgnoreCase("December")){
			map.put("prevmonth", "November");
			map.put("nextmonth", "January");
		}
		return map;
	}
	
	
	

	void createYourAlbum(WebDriver driver, FranBuzz franbuzz) throws Exception {
		fc.utobj().printTestStep("Navigate To The Hub > FranBuzz Page");
		TheHubFranBuzzPage pobj = new TheHubFranBuzzPage(driver);
		fc.hub().hub_common().theHubFranBuzzSubModule(driver);
		fc.utobj().printTestStep("Create Your Album");
		fc.utobj().clickElement(driver, pobj.photoLink);
		fc.utobj().clickElement(driver, pobj.createAlbum);
		fc.utobj().sendKeys(driver, pobj.albumtitle, franbuzz.getAlbumTitle());
		fc.utobj().sendKeys(driver, pobj.albumDescription, franbuzz.getAlbumDescription());
		if ("All".equalsIgnoreCase(franbuzz.getShareTo())) {
			fc.utobj().selectDropDown(driver, pobj.shareTo, franbuzz.getShareTo());
		} else if ("All Corporates".equalsIgnoreCase(franbuzz.getShareTo())) {
			fc.utobj().selectDropDown(driver, pobj.shareTo, franbuzz.getShareTo());
		} else if ("Groups".equalsIgnoreCase(franbuzz.getShareTo())) {
			fc.utobj().selectDropDown(driver, pobj.shareTo, franbuzz.getShareToValue());
		}
		fc.utobj().sendKeys(driver, pobj.cPhotoTitle, franbuzz.getCoverTitle());
		fc.utobj().sendKeys(driver, pobj.cPhotoUpload, franbuzz.getCoverUploadPhoto());
		fc.utobj().clickElement(driver, pobj.createBtn);
		franbuzz.setCreatedOnDate(fc.utobj().getCurrentTimeUDF("MM/dd/yyyy hh:mm aa", "GMT-5:00"));
		Reporter.log("Created On Date : " + franbuzz.getCreatedOnDate(), true);

	}

	
	
	
	FranBuzz createAlbumFromGallery(WebDriver driver, FranBuzz franbuzz) throws Exception {
		try {
			fc.utobj().printTestStep("Navigate To The Hub > FranBuzz Page");
			TheHubFranBuzzPage pobj = new TheHubFranBuzzPage(driver);
			fc.hub().hub_common().theHubFranBuzzSubModule(driver);
			fc.utobj().printTestStep("Go To Gallery Tab and create a new Album");
			fc.utobj().clickElement(driver, pobj.galleryTab);
			fc.utobj().clickElement(driver, pobj.createAnAlbum);
			fc.utobj().sendKeys(driver, pobj.cNewAlbumTitle, franbuzz.getAlbumTitle());
			fc.utobj().sendKeys(driver, pobj.albumDescription, franbuzz.getAlbumDescription());
			fc.utobj().sendKeys(driver, pobj.cPhotoTitle, franbuzz.getCoverTitle());
			fc.utobj().sendKeys(driver, pobj.cPhotoUpload, franbuzz.getCoverUploadPhoto());
			fc.utobj().clickElement(driver, pobj.createBtn);
			franbuzz.setCreatedOnDate(fc.utobj().getCurrentTimeUDF("MM/dd/yyyy hh:mm aa", "GMT-4:00"));
		} catch (Exception e) {
			fc.utobj().throwsException("Not able to created album in Gallery :" + e.getMessage());
		}
		return franbuzz;
	}

	String changeFormat(String oFormat, String tFormat, String stringDate) throws ParseException {
		DateFormat originalFormat = new SimpleDateFormat(oFormat, Locale.ENGLISH);
		DateFormat targetFormat = new SimpleDateFormat(tFormat);
		Date date = originalFormat.parse(stringDate);
		String formattedDate = targetFormat.format(date);
		return formattedDate;
	}

	void searchWallPosts(WebDriver driver, FranBuzz franbuzz) {
		try {
			TheHubFranBuzzPage pobj = new TheHubFranBuzzPage(driver);
			fc.utobj().clickElement(driver, pobj.wallTab);
			if ("Information".equalsIgnoreCase(franbuzz.getTypeOfPost())) {
				fc.utobj().sendKeys(driver, pobj.searchWallPosts, franbuzz.getInfoText());
			} else if ("Photo".equalsIgnoreCase(franbuzz.getTypeOfPost())) {
				fc.utobj().sendKeys(driver, pobj.searchWallPosts, franbuzz.getPhotoTitle());
			} else if ("Video".equalsIgnoreCase(franbuzz.getTypeOfPost())) {
				fc.utobj().sendKeys(driver, pobj.searchWallPosts, franbuzz.getVideoTitle());
			} else if ("Album".equalsIgnoreCase(franbuzz.getTypeOfPost())) {
				fc.utobj().sendKeys(driver, pobj.searchWallPosts, franbuzz.getAlbumTitle());
			}else if ("Documents".equalsIgnoreCase(franbuzz.getTypeOfPost())) {
				fc.utobj().sendKeys(driver, pobj.searchWallPosts, franbuzz.getDocumentTitle());
			}else if ("Post".equalsIgnoreCase(franbuzz.getTypeOfPost())) {
				fc.utobj().sendKeys(driver, pobj.searchWallPosts, franbuzz.getPostTitle());
			}else if ("Link".equalsIgnoreCase(franbuzz.getTypeOfPost())) {
				fc.utobj().sendKeys(driver, pobj.searchWallPosts, franbuzz.getPostTitle());
			}
			try {
				fc.utobj().clickElement(driver, pobj.searchWallPostsIcon);
			} catch (Exception e) {
				fc.utobj().clickEnterOnElement(driver, pobj.searchWallPosts);
			}
		} catch (Exception e) {
			Reporter.log("Not able to search wall posts by search : " + e.getMessage());
		}
	}

	boolean getUploadedPhotoResponse(WebDriver driver, FranBuzz franbuzz) {
		boolean isImagePresent = false;
		try {
			String url = fc.utobj()
					.getAttributeValue(fc.utobj().getElementByXpath(driver, ".//div[@class='ad-image']/img"), "src");
			fc.utobj().sleep();
			int code = 0;
			for (int i = 0; i < 5; i++) {
				try {
					URL url1 = new URL(url);
					HttpURLConnection connection = (HttpURLConnection) url1.openConnection();
					connection.setRequestMethod("GET");
					connection.connect();
					code = connection.getResponseCode();
					Reporter.log("code :" + code, true);
					if (code == 200) {
						isImagePresent = true;
						break;
					} else {
						fc.utobj().sleep(3000);
					}
				} catch (Exception e1) {
					Reporter.log(e1.getMessage());
				}
			}
		} catch (Exception e) {
			Reporter.log(e.getMessage());
		}
		return isImagePresent;
	}

	FranBuzz leaveYourComment(WebDriver driver, FranBuzz franbuzz) throws Exception {
		try {
			String infoText = "";
			if ("Information".equalsIgnoreCase(franbuzz.getTypeOfPost())) {
				infoText = franbuzz.getInfoText();
			} else if ("Photo".equalsIgnoreCase(franbuzz.getTypeOfPost())) {
				infoText = franbuzz.getPhotoTitle();
			} else if ("Video".equalsIgnoreCase(franbuzz.getTypeOfPost())) {
				infoText = franbuzz.getVideoTitle();
			} else if ("Link".equalsIgnoreCase(franbuzz.getTypeOfPost())) {
				infoText = "";
			} else if ("Document".equalsIgnoreCase(franbuzz.getTypeOfPost())) {
				infoText = "";
			}

			String tempText = fc.utobj()
					.getElementByXpath(driver,
							".//*[contains(text () , '" + infoText + "')]/parent::div[@class='fbz_post']")
					.getAttribute("onmouseover");
			tempText = tempText.substring(tempText.indexOf("('") + 2, tempText.indexOf("')"));

			WebElement element = fc.utobj().getElementByID(driver, "pcomment" + tempText);
			fc.utobj().sendKeys(driver, element, franbuzz.getCommentText());
			WebElement postElement = fc.utobj().getElementByID(driver, "btnReply" + tempText);
			fc.utobj().clickElement(driver, postElement);
			franbuzz.setCreatedOnDate(fc.utobj().getCurrentTimeUDF("MM/dd/yyyy hh:mm aa", "GMT-5:00"));

		} catch (Exception e) {
			fc.utobj().throwsException("Not able to post comment over post :" + e.getMessage());
		}
		return franbuzz;
	}

	FranBuzz uploadPhotoInGallery(WebDriver driver, FranBuzz franbuzz) throws Exception {
		try {
			fc.utobj().printTestStep("Navigate To The Hub > FranBuzz Page");
			TheHubFranBuzzPage pobj = new TheHubFranBuzzPage(driver);
			fc.hub().hub_common().theHubFranBuzzSubModule(driver);
			fc.utobj().printTestStep("Go To Gallery Tab and Upload Photo");
			fc.utobj().clickElement(driver, pobj.galleryTab);
			fc.utobj().clickElement(driver, pobj.uploadPhotosInGallery);
			fc.utobj().sendKeys(driver, pobj.photoTitle, franbuzz.getPhotoTitle());
			fc.utobj().sendKeys(driver, pobj.singlePhotoUpload1, franbuzz.getBrowsePhoto());
			fc.utobj().clickElement(driver, pobj.apUploadBtn);

			// Get Day Month and Year
			franbuzz.setCreatedOnDate(fc.utobj().getCurrentTimeUDF("MM/dd/yyyy hh:mm aa", "GMT-4:00"));
		} catch (Exception e) {
			fc.utobj().throwsException("Not able to upload photos in Gallery :" + e.getMessage());
		}
		return franbuzz;
	}

	void searchUserByUserFullName(WebDriver driver, CorporateUser corpUser) throws Exception {
		try {
			TheHubFranBuzzPage pobj = new TheHubFranBuzzPage(driver);

			fc.utobj().sendKeys(driver, pobj.searchString, corpUser.getuserFullName());
			fc.utobj().clickEnterOnElement(driver, pobj.searchString);
			fc.utobj().sleep(1000);
			fc.utobj().clickElement(driver, fc.utobj().getElementByPartialLinkText(driver, corpUser.getFirstName()));
		} catch (Exception e) {
			fc.utobj().throwsException("Not able to search member :" + e.getMessage());
		}
	}

	public void createGroup(WebDriver driver, String groupTitle, String groupDescription, Map<String, String> config)
			throws Exception {
		try {
			TheHubFranBuzzPage pobj = new TheHubFranBuzzPage(driver);
			fc.hub().hub_common().theHubFranBuzzSubModule(driver);
			fc.utobj().clickElement(driver, pobj.createGroupLink);
			fc.utobj().sendKeys(driver, pobj.groupTitle, groupTitle);
			fc.utobj().sendKeys(driver, pobj.groupDescription, groupDescription);
			if (!fc.utobj().isSelected(driver, pobj.publicGroup)) {
				fc.utobj().clickElement(driver, pobj.publicGroup);
			}
			fc.utobj().clickElement(driver, pobj.selectAllCorporateUser);
			fc.utobj().clickElement(driver, pobj.createGroupBtn);
		} catch (Exception e) {
		}
	}

	void sharePhoto(WebDriver driver, FranBuzz franbuzz) throws Exception {
		try {
			TheHubFranBuzzPage pobj = new TheHubFranBuzzPage(driver);
			fc.hub().hub_common().theHubFranBuzzSubModule(driver);
			fc.utobj().clickElement(driver, pobj.wallTab);
			fc.utobj().clickElement(driver, pobj.photoLink);
			fc.utobj().clickElement(driver, pobj.uploadPhoto);
			fc.utobj().sendKeys(driver, pobj.photoTitle, franbuzz.getPhotoTitle());
			fc.utobj().sendKeys(driver, pobj.browsePhoto, franbuzz.getBrowsePhoto());
			fc.utobj().sendKeys(driver, pobj.photoDescription, franbuzz.getPhotoDescription());
			if ("All".equalsIgnoreCase(franbuzz.getShareTo())) {
				fc.utobj().selectDropDown(driver, pobj.photoShareTo, franbuzz.getShareTo());
			} else if ("All Corporates".equalsIgnoreCase(franbuzz.getShareTo())) {
				fc.utobj().selectDropDown(driver, pobj.photoShareTo, franbuzz.getShareTo());

			} else if ("Groups".equalsIgnoreCase(franbuzz.getShareTo())) {
				fc.utobj().selectDropDown(driver, pobj.photoShareTo, franbuzz.getShareToValue());
			}
			fc.utobj().clickElement(driver, pobj.shareSinglePhoto);
		} catch (Exception e) {
			Reporter.log("Not able to Upload Single Photo :" + e.getMessage());
			fc.utobj().throwsException("Not able to Upload Single Photo :" + e.getMessage());
		}
	}

	public void shareInformation(WebDriver driver, FranBuzz franbuzz) throws Exception {
		try {
			TheHubFranBuzzPage pobj = new TheHubFranBuzzPage(driver);
			fc.hub().hub_common().theHubFranBuzzSubModule(driver);
			fc.utobj().clickElement(driver, pobj.wallTab);
			fc.utobj().sendKeys(driver, pobj.scrapText, franbuzz.getInfoText());
			if (franbuzz.getShareTo() != null) {
				if (franbuzz.getShareTo().equalsIgnoreCase("All Corporates")) {
					fc.utobj().selectDropDown(driver, pobj.shareTo, franbuzz.getShareTo());
				} else if (franbuzz.getShareTo().equalsIgnoreCase("Everyone")) {
					fc.utobj().selectDropDown(driver, pobj.shareTo, franbuzz.getShareTo());
				} else if (franbuzz.getShareTo().equalsIgnoreCase("Groups")) {
					fc.utobj().selectDropDown(driver, pobj.shareTo, franbuzz.getShareToValue());
				}
			}
			fc.utobj().clickElement(driver, pobj.shareBtn);
		} catch (Exception e) {
			Reporter.log("Not able to share information : " + e.getMessage());
			fc.utobj().throwsException("Not able to share information : " + e.getMessage());
		}
	}
	
	public void shareLink(WebDriver driver, FranBuzz franbuzz) throws Exception {
		try {
			TheHubFranBuzzPage pobj = new TheHubFranBuzzPage(driver);
			fc.hub().hub_common().theHubFranBuzzSubModule(driver);
			fc.utobj().clickElement(driver, pobj.wallTab);
			fc.utobj().clickElement(driver, pobj.linkTab);
			fc.utobj().sendKeys(driver, pobj.linkTxBx, franbuzz.getLinkText());
			
			if (franbuzz.getShareTo() != null) {
				if (franbuzz.getShareTo().equalsIgnoreCase("All Corporates")) {
					fc.utobj().selectDropDown(driver, pobj.shareToLinkDD, franbuzz.getShareTo());
				} else if (franbuzz.getShareTo().equalsIgnoreCase("Everyone")) {
					fc.utobj().selectDropDown(driver, pobj.shareToLinkDD, franbuzz.getShareTo());
				} else if (franbuzz.getShareTo().equalsIgnoreCase("Groups")) {
					fc.utobj().selectDropDown(driver, pobj.shareToLinkDD, franbuzz.getShareToValue());
				}
			}
			fc.utobj().clickElement(driver, pobj.shareLinkBtn);
		} catch (Exception e) {
			Reporter.log("Not able to share Link : " + e.getMessage());
			fc.utobj().throwsException("Not able to share Link : " + e.getMessage());
		}
	}

	public void newPost(WebDriver driver, FranBuzz franbuzz) throws Exception {
		try {
			TheHubFranBuzzPage pobj = new TheHubFranBuzzPage(driver);
			fc.hub().hub_common().theHubFranBuzzSubModule(driver);
			fc.utobj().clickElement(driver, pobj.communityTab);
			fc.utobj().selectDropDown(driver, pobj.selectPost, "All");
			fc.utobj().clickElement(driver, pobj.newPost);
			fc.utobj().sendKeys(driver, pobj.title, franbuzz.getTitle());

			fc.utobj().switchFrameById(driver, "ta_ifr");
			Actions actions = new Actions(driver);
			actions.moveToElement(fc.utobj().getElement(driver, pobj.editorTextArea));
			actions.click();
			actions.sendKeys(franbuzz.getHtmlText());
			fc.utobj().logReportMsg("Entered Text", franbuzz.getHtmlText());
			actions.build().perform();
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, pobj.postBtn);
		} catch (Exception e) {
			Reporter.log("Not able to post the at franbuzz : " + e.getMessage());
			fc.utobj().throwsException("Not able to post the at franbuzz : " + e.getMessage());
		}
	}
}
