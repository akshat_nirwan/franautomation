package com.builds.test.thehub;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminConfigurationConfigureSingleSignOnPageTest;
import com.builds.test.admin.AdminDivisionAddDivisionPageTest;
import com.builds.test.admin.AdminFranchiseLocationAddFranchiseLocationPageTest;
import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.admin.AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest;
import com.builds.test.admin.AdminUsersManageManageFranchiseUsersPageTest;
import com.builds.test.admin.AdminUsersManageRegionalUsersAddRegionalUserPageTest;
import com.builds.test.admin.AdminUsersRolesAddNewRolePageTest;
import com.builds.test.common.CorporateUser;
import com.builds.test.support.AdminSupportManageDepartmentPageTest;
import com.builds.uimaps.common.FCHomePage;
import com.builds.uimaps.fs.SearchUI;
import com.builds.uimaps.support.SupportTicketsPage;
import com.builds.uimaps.thehub.TheHubHomePage;
import com.builds.uimaps.thehub.TheHubNewsPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class TheHubHomePageTest {
	FranconnectUtil fc = new FranconnectUtil();
	HubCommonMethods hub_common_method=new HubCommonMethods();
	
	
	@Test(groups = { "thehub","hub_home"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Messages At The Hub > Home", testCaseId = "TC_117_Verify_Messages")
	public void verifyMessages() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubHomePage pobj = new TheHubHomePage(driver);

			fc.utobj().printTestStep("Navigate To The Hub > Messages Page");

			MessagesTest messagesPage = new MessagesTest();
			Messages hub_common = new Messages();
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			hub_common.setSubject(subject);
			String messages = fc.utobj().generateTestData(dataSet.get("messages"));
			hub_common.setMessages(messages);
			String messagesType = dataSet.get("messagesType");
			hub_common.setMessagesType(messagesType);
			String recipientsUser = dataSet.get("recipientsUser");
			hub_common.setRecipientsUser(recipientsUser);
			hub_common.setToEmail("FranConnect Administrator");
			hub_common.setContactType("Internal");

			fc.utobj().printTestStep("Compose Message And Send Message");
			messagesPage.composeMessages(driver, hub_common);

			fc.utobj().printTestStep("Navigate To The Hub > Home Page");
			fc.utobj().printTestStep("Verify The Messages");
			fc.utobj().clickElement(driver, pobj.homeLink);
			fc.utobj().clickElement(driver, pobj.manageWidgetsLink);
			if (!fc.utobj().isSelected(driver,pobj.messagesHomePageWidgets)) {
				fc.utobj().clickElement(driver, pobj.messagesHomePageWidgets);
				fc.utobj().clickElement(driver, pobj.saveBtn);
			}
			try {
				fc.utobj().clickElement(driver, pobj.cancelBtn);
			} catch (Exception e) {
			}

			String msgText = null;
			boolean status = false;
			List<WebElement> elements = driver.findElements(By.xpath(".//*[@id='messagesHomePageContent']//td//a"));

			if (elements.size() > 0) {
				for (int i = 0; i < elements.size(); i++) {

					msgText = elements.get(i).getText().trim();
					if (msgText.equalsIgnoreCase(subject)) {
						status = true;
						break;
					}
				}
			}

			if (status == false) {
				fc.utobj().throwsException("was not able to verify Message at Home Page in Messages Widgets");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"thehub","hub_home"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Alert At The Hub > Home", testCaseId = "TC_118_Verify_Alert")
	public void verifyAlert() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubHomePage pobj = new TheHubHomePage(driver);

			fc.utobj().printTestStep("Navigate To The Hub > Alert Page");
			TheHubAlertsPageTest alertPage = new TheHubAlertsPageTest();

			String toEmail = "FranConnect Administrator";
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			String alertMessages = fc.utobj().generateTestData(dataSet.get("alertMessages"));
			String alertMessagesType = dataSet.get("alertMessagesType");
			String recipientsUser = dataSet.get("recipientsUser");

			fc.utobj().printTestStep("Send Alert");
			alertPage.sendAlert(driver, toEmail, subject, alertMessagesType, alertMessages, recipientsUser, config);

			fc.utobj().printTestStep("Navigate To The Hub > Home Page");

			fc.utobj().clickElement(driver, pobj.homeLink);
			fc.utobj().clickElement(driver, pobj.manageWidgetsLink);

			fc.utobj().printTestStep("Verify The Alert");

			if (!fc.utobj().isSelected(driver, pobj.alertsHomePagec)) {
				fc.utobj().clickElement(driver, pobj.alertsHomePagec);
				fc.utobj().clickElement(driver, pobj.saveBtn);
			}
			try {
				fc.utobj().clickElement(driver, pobj.cancelBtn);
			} catch (Exception e) {
			}

			String alertMsgText = null;
			boolean status = false;
			List<WebElement> elements = driver.findElements(By.xpath(".//*[@id='alertsHomePage']//a"));

			if (elements.size() > 0) {
				for (int i = 0; i < elements.size(); i++) {

					alertMsgText = elements.get(i).getText().trim();
					if (alertMsgText.equalsIgnoreCase(subject)) {
						status = true;
						break;
					}
				}
			}

			if (status == false) {
				fc.utobj()
						.throwsException("was not able to verify Alert Message at Home Page in Alert Message Widgets");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub" ,"hub_home"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Top Stories At The Hub > Home", testCaseId = "TC_120_Verify_Top_Stories")
	private void verifyTopStories() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubHomePage pobj = new TheHubHomePage(driver);

			AdminTheHubStoriesPageTest story_page = new AdminTheHubStoriesPageTest();
			story_page.selectMultipleSingleStory(driver, "Single", testCaseId);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Top Stories Page ");
			fc.utobj().printTestStep("Add Top Stories");
			AdminTheHubStoriesPageTest storiesPage = new AdminTheHubStoriesPageTest();
			String title = fc.utobj().generateTestData(dataSet.get("title"));
			String summary = fc.utobj().generateTestData(dataSet.get("summary"));
			String editorText = fc.utobj().generateTestData(dataSet.get("editorText"));
			String accessTo = "All";
			String roleName = null;
			String mediaFileType = "Image";
			String currentDate = fc.utobj().getCurrentDateUSFormat();
			String futureDate = fc.utobj().getFutureDateUSFormat(1);
			storiesPage.addStories(driver, title, summary, editorText, accessTo, roleName, mediaFileType, dataSet,
					currentDate, futureDate);

			fc.utobj().printTestStep("Navigate To The Hub > Home Page");
			fc.hub().hub_common().theHubHome(driver);

			fc.utobj().printTestStep("Verify The Stories");

			boolean isStoryPresnt = false;
			boolean isSingleStoryPresent = false;

			try {
				List<WebElement> nextElement = driver
						.findElements(By.xpath(".//*[@id='currentStory' and contains(text(),'of')]"));

				String totalStory = null;

				if (nextElement.size() == 1) {
					totalStory = nextElement.get(0).getText();
					totalStory = totalStory.substring(totalStory.lastIndexOf("f") + 1);
					if (totalStory != null && totalStory.length() > 0) {
						totalStory = totalStory.trim();
					}

					for (int i = 0; i < Integer.parseInt(totalStory); i++) {
						fc.utobj().clickElementWithoutMove(driver, fc.utobj().getElement(driver, pobj.nextStoryLnk));
						List<WebElement> element = driver
								.findElements(By.xpath(".//*[contains(text(),'" + title + "')]"));
						if (element.size() == 1) {
							isStoryPresnt = true;
							isSingleStoryPresent = false;
							break;
						}
					}
				} else {
					isSingleStoryPresent = true;
					fc.utobj().isTextDisplayed(driver, title, "was not able to verify title of Top Story");
					fc.utobj().isTextDisplayed(driver, editorText, "was not able to verify summary of Top Story");
				}
			} catch (Exception e) {
			}

			if (isStoryPresnt == false && isSingleStoryPresent == false) {
				fc.utobj().throwsException("Not able to verify title of Top Story");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	//@Test(groups ={"thehub","hub_0806"},dependsOnMethods="verifyTopStories",alwaysRun=true)
	@TestCase(createdOn = "2017-01-16", updatedOn = "2017-01-16", testCaseDescription = "Verify Multiple Stories At The Hub > Home", testCaseId = "TC_Verify_Hub_Multiple_Top_Stories")
	private void multipleStory() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubHomePage pobj=new TheHubHomePage(driver);
			AdminTheHubStoriesPageTest storiesPage = new AdminTheHubStoriesPageTest();
			storiesPage.selectMultipleSingleStory(driver, "Multiple", testCaseId);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Top Stories Page ");
			fc.utobj().printTestStep("Add Multiple - 2 Top Stories");

			String title = fc.utobj().generateTestData(dataSet.get("title"));
			String title1 = fc.utobj().generateTestData(dataSet.get("title"));
			String title2 = fc.utobj().generateTestData(dataSet.get("title"));
			String title3 = fc.utobj().generateTestData(dataSet.get("title"));

			for (int i = 0; i < 4; i++) {
				String summary = fc.utobj().generateTestData(dataSet.get("summary"));
				String editorText = fc.utobj().generateTestData(dataSet.get("editorText"));
				String accessTo = "All";
				String roleName = null;
				String mediaFileType = "Image";
				String currentDate = fc.utobj().getCurrentDateUSFormat();
				String futureDate = fc.utobj().getFutureDateUSFormat(1);
				storiesPage.addStories(driver, title, summary, editorText, accessTo, roleName, mediaFileType, dataSet,
						currentDate, futureDate);
				title = title1;
				if (i == 1) {
					title = title2;
				} else if (i == 2) {
					title = title3;
				}
			}

			fc.utobj().printTestStep("Navigate To The Hub > Home Page");
			fc.hub().hub_common().theHubHome(driver);

			boolean isTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[@class='bText12b' and contains(text(),'" + title + "')]");
			
			if (!isTextPresent) {
				try {
					fc.utobj().clickElement(driver, pobj.homeLink);
					fc.utobj().clickElement(driver, pobj.nextMultiStory);
					isTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
							".//*[@class='bText12b' and contains(text(),'" + title + "')]");
				} catch (Exception e) {
				}
			}
			
			if (!isTextPresent) {
				fc.utobj().throwsException("First story is not visible at Hub Home page");
			}
			boolean isTextPresent1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[@class='bText12b' and contains(text(),'" + title1 + "')]");
			
			if (!isTextPresent1) {
				try {
					fc.utobj().clickElement(driver, pobj.homeLink);
					fc.utobj().clickElement(driver, pobj.nextMultiStory);
					isTextPresent1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
							".//*[@class='bText12b' and contains(text(),'" + title1 + "')]");
				} catch (Exception e) {
				}
			}
			
			if (!isTextPresent1) {
				fc.utobj().throwsException("Second story is not visible at Hub Home page");
			}
			boolean isTextPresent2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[@class='bText12b' and contains(text(),'" + title2 + "')]");
			
			if (!isTextPresent2) {
				try {
					fc.utobj().clickElement(driver, pobj.homeLink);
					fc.utobj().clickElement(driver, pobj.nextMultiStory);
					isTextPresent2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
							".//*[@class='bText12b' and contains(text(),'" + title2 + "')]");
				} catch (Exception e) {
				}
			}
			
			if (!isTextPresent2) {
				fc.utobj().throwsException("Third story is not visible at Hub Home page");
			}
			
			boolean isTextPresent3 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[@class='bText12b' and contains(text(),'" + title3 + "')]");

			if (!isTextPresent3) {
				try {
					fc.utobj().clickElement(driver, pobj.homeLink);
					fc.utobj().clickElement(driver, pobj.nextMultiStory);
					isTextPresent3 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
							".//*[@class='bText12b' and contains(text(),'" + title3 + "')]");
				} catch (Exception e) {
				}
			}
			
			if (!isTextPresent3) {
				fc.utobj().throwsException("Fourth story is not visible at Hub Home page");
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub" ,"hub_home"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Epoll At The Hub > Home", testCaseId = "TC_122_Verify_Epoll")
	private void verifyEPoll() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubHomePage pobj = new TheHubHomePage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Epoll Page");
			fc.utobj().printTestStep("Post New Epoll");
			AdminTheHubEPOllPageTest epollPage = new AdminTheHubEPOllPageTest();
			String epollTitle = fc.utobj().generateTestData(dataSet.get("epollTitle"));
			String startDate = fc.utobj().getCurrentDateUSFormat();
			String endDate = fc.utobj().getFutureDateUSFormat(2);
			String epollQuestion = fc.utobj().generateTestData(dataSet.get("epollQuestion"));
			String option1 = fc.utobj().generateTestData(dataSet.get("option1"));
			String option2 = fc.utobj().generateTestData(dataSet.get("option2"));
			String epollQuestionType = dataSet.get("epollQuestionType");
			String canTakePart = "All";
			String canViewResult = "No";
			epollPage.postNewPoll(driver, epollTitle, startDate, endDate, epollQuestion, epollQuestionType, option1,
					option2, canTakePart, canViewResult, config);

			fc.utobj().printTestStep("Navigate To The Hub > Home Page");
			fc.hub().hub_common().theHubHome(driver);

			fc.utobj().printTestStep("Verify The New Epoll");
			fc.utobj().clickElement(driver, pobj.allOngoingPoll);
			fc.utobj().isTextDisplayed(driver, epollTitle, "was not able to verify Epoll Title");
			fc.utobj().isTextDisplayed(driver, epollQuestion, "was not able to verify Epoll Question");

			List<WebElement> elements = driver
					.findElements(By.xpath(".//td[contains(text () , '" + epollQuestion + "')]/select/option"));

			if (!elements.get(1).getText().trim().equalsIgnoreCase(option1)) {
				fc.utobj().throwsException("was not able to verify options 1 of the question");
			}
			if (!elements.get(2).getText().trim().equalsIgnoreCase(option2)) {
				fc.utobj().throwsException("was not able to verify options 2 of the question");
			}

			/*
			 * TC-9903 : Drop Down List
			 */

			fc.utobj().selectDropDown(driver,
					fc.utobj().getElementByXpath(driver, ".//td[contains(text(),'" + epollQuestion + "')]/select"),
					option1);
			String text = fc.utobj().getAttributeValue(
					fc.utobj().getElementByXpath(driver, ".//td[contains(text(),'" + epollQuestion + "')]/select"),
					"name");
			text = text.trim();
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//input[@name='Vote' and contains(@onclick ,'" + text + "')]"));

			fc.utobj().printTestStep("Verify After submitting response vote button should be disable");
			String disableText = fc.utobj().getAttributeValue(fc.utobj().getElementByXpath(driver,
					".//input[@name='Vote' and contains(@onclick ,'" + text + "')]"), "disabled");

			if (disableText == null) {
				fc.utobj().throwsException("Button is not going to disable after submitting response");
			}

			fc.utobj().printTestStep("Navigate To The Hub > Home Page");
			fc.hub().hub_common().theHubHome(driver);

			fc.utobj().printTestStep("Verify Previous Results");
			fc.utobj().clickElement(driver, pobj.previousResults);

			boolean isTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text(),'Previous Polls')]");
			if (isTextPresent == false) {
				fc.utobj().throwsException(
						"On clicking over Previous Results , It is not redirecting to respective sections");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_role","hub_home" })
	@TestCase(createdOn = "2018-01-04", updatedOn = "2018-01-04", testCaseDescription = "Verify On deselecting Can View What's New Privilege , New Since Last Login section and Whats New Tab should not appear under the HUB Module By Corporate User", testCaseId = "TC_Hub_WhatsNew_Privilege_01")
	private void WhatsNew01() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep(
					"Go to admin > Roles > Create Corporate Role with Can View What's New as deselected");
			TheHubHomePage pobj = new TheHubHomePage(driver);

			AdminUsersRolesAddNewRolePageTest roles_page = new AdminUsersRolesAddNewRolePageTest();
			String roleType = dataSet.get("roleType");
			String roleName = fc.utobj().generateTestData("TcCorporateRole");
			String moduleName = "The Hub";
			String emailId = "hubautomation@staffex.com";

			Map<String, String> privileges = new HashMap<String, String>();
			privileges.put("Can View What's New", "No");
			roles_page.addRolesWithModulesPrivileges(driver, roleType, roleName, moduleName, privileges, testCaseId);

			fc.utobj().printTestStep(
					"Navigate To Admin > Users > Manage Corporate Users > Add  Corporate User with added role");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporate_user = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser.setRole(roleName);
			corpUser = corporate_user.createDefaultUser(driver, corpUser);

			fc.utobj().printTestStep("Logged out from ADM User");
			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Log In with newly created Corporate user");
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());

			fc.utobj().printTestStep("Navigate To The Hub Module");
			fc.hub().hub_common().theHubHome(driver);

			fc.utobj().printTestStep("Restore default setting for Manage Widgets");
			fc.utobj().clickElement(driver, pobj.manageWidgetsLink);
			fc.utobj().clickElement(driver, pobj.restoredefaultsettings);

			fc.utobj().printTestStep("Verify New Since Last Login should not visible under hub > home");

			boolean isTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[@id='newSinceLastLoginHeading']");

			if (isTextPresent) {
				fc.utobj().throwsException("New Since Last Login is visible under hub > home");
			}

			fc.utobj().printTestStep("Verify Whats New sub module should not visible under hub");

			boolean isSubTabPresent = false;

			List<WebElement> listElement = driver.findElements(By.xpath(".//*[@id='module_intranet']/ul/li/a"));

			if (listElement.size() == 0) {
				fc.utobj().throwsException("No Sub module is present under hub module");
			} else if (listElement.size() > 0) {

				String[] arrayString = new String[listElement.size()];

				for (int i = 0; i < listElement.size(); i++) {
					arrayString[i] = listElement.get(i).getText().trim();
				}

				for (String string : arrayString) {

					if (string.equalsIgnoreCase(fc.utobj().translateString("What's New"))) {
						isSubTabPresent = true;
					}
				}
			}

			if (isSubTabPresent) {
				fc.utobj().throwsException("What's New sub module is visible under hub");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_role" ,"hub_home"})
	@TestCase(createdOn = "2018-01-04", updatedOn = "2018-01-04", testCaseDescription = "Verify On deselecting Can View What's New Privilege , New Since Last Login section and Whats New Tab should not appear under the HUB Module By Divisional User", testCaseId = "TC_Hub_WhatsNew_Privilege_02")
	private void WhatsNew02() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep(
					"Go to admin > Roles > Create Divisional Role with Can View What's New as deselected");
			TheHubHomePage pobj = new TheHubHomePage(driver);

			AdminUsersRolesAddNewRolePageTest roles_page = new AdminUsersRolesAddNewRolePageTest();
			String roleType = dataSet.get("roleType");
			String roleName = fc.utobj().generateTestData("TcDivisionalRole");
			String moduleName = "The Hub";
			String emailId = "hubautomation@staffex.com";
			String userName = fc.utobj().generateTestData("du");
			String password = "t0n1ght@123";
			String divisionName = fc.utobj().generateTestData("Testdiv");

			Map<String, String> privileges = new HashMap<String, String>();
			privileges.put("Can View What's New", "No");
			roles_page.addRolesWithModulesPrivileges(driver, roleType, roleName, moduleName, privileges, testCaseId);

			fc.utobj().printTestStep(
					"Navigate To Admin > Users > Manage Divisional Users > Add  Divisional User with added role");
			AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest divisional_user = new AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest();
			divisional_user.addDivisionalUserWithRole(driver, userName, divisionName, roleName, password, emailId);

			fc.utobj().printTestStep("Logged out from ADM User");
			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Log In with newly created Divisional user");
			fc.loginpage().loginWithParameter(driver, userName, password);

			fc.utobj().printTestStep("Navigate To The Hub Module");
			fc.hub().hub_common().theHubHome(driver);

			fc.utobj().printTestStep("Restore default setting for Manage Widgets");
			fc.utobj().clickElement(driver, pobj.manageWidgetsLink);
			fc.utobj().clickElement(driver, pobj.restoredefaultsettings);

			fc.utobj().printTestStep("Verify New Since Last Login should not visible under hub > home");

			boolean isTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[@id='newSinceLastLoginHeading']");

			if (isTextPresent) {
				fc.utobj().throwsException("New Since Last Login is visible under hub > home");
			}

			fc.utobj().printTestStep("Verify Whats New sub module should not visible under hub");

			boolean isSubTabPresent = false;

			List<WebElement> listElement = driver.findElements(By.xpath(".//*[@id='module_intranet']/ul/li/a"));

			if (listElement.size() == 0) {
				fc.utobj().throwsException("No Sub module is present under hub module");
			} else if (listElement.size() > 0) {

				String[] arrayString = new String[listElement.size()];

				for (int i = 0; i < listElement.size(); i++) {
					arrayString[i] = listElement.get(i).getText().trim();
				}

				for (String string : arrayString) {

					if (string.equalsIgnoreCase(fc.utobj().translateString("What's New"))) {
						isSubTabPresent = true;
					}
				}
			}

			if (isSubTabPresent) {
				fc.utobj().throwsException("What's New sub module is visible under hub");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_role" ,"hub_home"})
	@TestCase(createdOn = "2018-01-04", updatedOn = "2018-01-04", testCaseDescription = "Verify On deselecting Can View What's New Privilege , New Since Last Login section and Whats New Tab should not appear under the HUB Module By Regional User", testCaseId = "TC_Hub_WhatsNew_Privilege_03")
	private void WhatsNew03() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj()
					.printTestStep("Go to admin > Roles > Create Regional Role with Can View What's New as deselected");
			TheHubHomePage pobj = new TheHubHomePage(driver);

			AdminUsersRolesAddNewRolePageTest roles_page = new AdminUsersRolesAddNewRolePageTest();
			String roleType = dataSet.get("roleType");
			String roleName = fc.utobj().generateTestData("TcRegionalRole");
			String moduleName = "The Hub";
			String emailId = "hubautomation@staffex.com";
			String userName = fc.utobj().generateTestData("ru");
			String password = "t0n1ght@123";
			String regionName = fc.utobj().generateTestData("TestReg");

			Map<String, String> privileges = new HashMap<String, String>();
			privileges.put("Can View What's New", "No");
			roles_page.addRolesWithModulesPrivileges(driver, roleType, roleName, moduleName, privileges, testCaseId);

			fc.utobj().printTestStep(
					"Navigate To Admin > Users > Manage Regional Users > Add  Regional User with added role");
			AdminUsersManageRegionalUsersAddRegionalUserPageTest region_page = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
			region_page.addRegionalUserWithRole(driver, userName, password, regionName, roleName, emailId);

			fc.utobj().printTestStep("Logged out from ADM User");
			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Log In with newly created Regional user");
			fc.loginpage().loginWithParameter(driver, userName, password);

			fc.utobj().printTestStep("Navigate To The Hub Module");
			fc.hub().hub_common().theHubHome(driver);

			fc.utobj().printTestStep("Restore default setting for Manage Widgets");
			fc.utobj().clickElement(driver, pobj.manageWidgetsLink);
			fc.utobj().clickElement(driver, pobj.restoredefaultsettings);

			fc.utobj().printTestStep("Verify New Since Last Login should not visible under hub > home");

			boolean isTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[@id='newSinceLastLoginHeading']");

			if (isTextPresent) {
				fc.utobj().throwsException("New Since Last Login is visible under hub > home");
			}

			fc.utobj().printTestStep("Verify Whats New sub module should not visible under hub");

			boolean isSubTabPresent = false;

			List<WebElement> listElement = driver.findElements(By.xpath(".//*[@id='module_intranet']/ul/li/a"));

			if (listElement.size() == 0) {
				fc.utobj().throwsException("No Sub module is present under hub module");
			} else if (listElement.size() > 0) {

				String[] arrayString = new String[listElement.size()];

				for (int i = 0; i < listElement.size(); i++) {
					arrayString[i] = listElement.get(i).getText().trim();
				}

				for (String string : arrayString) {

					if (string.equalsIgnoreCase(fc.utobj().translateString("What's New"))) {
						isSubTabPresent = true;
					}
				}
			}

			if (isSubTabPresent) {
				fc.utobj().throwsException("What's New sub module is visible under hub");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_role","hub_home"})
	@TestCase(createdOn = "2018-01-04", updatedOn = "2018-01-04", testCaseDescription = "Verify On deselecting Can View What's New Privilege , New Since Last Login section and Whats New Tab should not appear under the HUB Module By Franchise User", testCaseId = "TC_Hub_WhatsNew_Privilege_04")
	private void whatsNew04() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep(
					"Go to admin > Roles > Create franchise Role with Can View What's New as deselected");
			TheHubHomePage pobj = new TheHubHomePage(driver);

			AdminUsersRolesAddNewRolePageTest roles_page = new AdminUsersRolesAddNewRolePageTest();
			String roleType = dataSet.get("roleType");
			String roleName = fc.utobj().generateTestData("TcFranchiseRole");
			String moduleName = "The Hub";
			String emailId = "hubautomation@staffex.com";
			String userName = fc.utobj().generateTestData("fu");
			String password = "t0n1ght@123";
			String regionName = fc.utobj().generateTestData("TestReg");
			String storeType = fc.utobj().generateTestData("TestStore");
			String franchiseName = fc.utobj().generateTestData("TestFran");
			String centerName = fc.utobj().generateTestData("center");
			String openingDate = fc.utobj().getCurrentDateUSFormat();
			String ownerFirstName = fc.utobj().generateTestData("Testfname");
			String ownerLastName = fc.utobj().generateTestData("Testlname");

			Map<String, String> privileges = new HashMap<String, String>();
			privileges.put("Can View What's New", "No");
			roles_page.addRolesWithModulesPrivileges(driver, roleType, roleName, moduleName, privileges, testCaseId);

			fc.utobj().printTestStep(
					"Navigate To Admin > Franchise Location > Manage Franchise Locations > Add Franchise Location");
			AdminFranchiseLocationAddFranchiseLocationPageTest location_page = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String franchiseId = location_page.addFranchiseLocation(driver, regionName, storeType, franchiseName,
					centerName, openingDate, ownerFirstName, ownerLastName, config);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Franchise Users > Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest franchise_page = new AdminUsersManageManageFranchiseUsersPageTest();
			franchise_page.addFranchiseUser(driver, userName, password, franchiseId, roleName, emailId);

			fc.utobj().printTestStep("Logged out from ADM User");
			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Log In with newly created franchise user");
			fc.loginpage().loginWithParameter(driver, userName, password);

			fc.utobj().printTestStep("Navigate To The Hub Module");
			fc.hub().hub_common().theHubHome(driver);

			fc.utobj().printTestStep("Restore default setting for Manage Widgets");
			fc.utobj().clickElement(driver, pobj.manageWidgetsLink);
			fc.utobj().clickElement(driver, pobj.restoredefaultsettings);

			fc.utobj().printTestStep("Verify New Since Last Login should not visible under hub > home");

			boolean isTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[@id='newSinceLastLoginHeading']");

			if (isTextPresent) {
				fc.utobj().throwsException("New Since Last Login is visible under hub > home");
			}

			fc.utobj().printTestStep("Verify Whats New sub module should not visible under hub");

			boolean isSubTabPresent = false;

			List<WebElement> listElement = driver.findElements(By.xpath(".//*[@id='module_intranet']/ul/li/a"));

			if (listElement.size() == 0) {
				fc.utobj().throwsException("No Sub module is present under hub module");
			} else if (listElement.size() > 0) {

				String[] arrayString = new String[listElement.size()];

				for (int i = 0; i < listElement.size(); i++) {
					arrayString[i] = listElement.get(i).getText().trim();
				}

				for (String string : arrayString) {

					if (string.equalsIgnoreCase(fc.utobj().translateString("What's New"))) {
						isSubTabPresent = true;
					}
				}
			}

			if (isSubTabPresent) {
				fc.utobj().throwsException("What's New sub module is visible under hub");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_role", "hubepoll0720","hub_home"})
	@TestCase(createdOn = "2018-01-05", updatedOn = "2018-01-05", testCaseDescription = "Verify On deselecting Can View EPoll Privilege , Epoll section should not appear under the HUB Module By Corporate User", testCaseId = "TC_Hub_Epoll_Privilege_01")
	private void epollPrivileges01() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Go to admin > Roles > Create Corporate Role with Can View EPoll as deselected");
			TheHubHomePage pobj = new TheHubHomePage(driver);

			AdminUsersRolesAddNewRolePageTest roles_page = new AdminUsersRolesAddNewRolePageTest();
			String roleType = dataSet.get("roleType");
			String roleName = fc.utobj().generateTestData("TcCorporateRole");
			String moduleName = "The Hub";
			String emailId = "hubautomation@staffex.com";

			Map<String, String> privileges = new HashMap<String, String>();
			privileges.put("Can View EPoll", "No");
			roles_page.addRolesWithModulesPrivileges(driver, roleType, roleName, moduleName, privileges, testCaseId);

			fc.utobj().printTestStep(
					"Navigate To Admin > Users > Manage Corporate Users > Add  Corporate User with added role");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporate_user = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser.setRole(roleName);
			corpUser = corporate_user.createDefaultUser(driver, corpUser);

			fc.utobj().printTestStep("Logged out from ADM User");
			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Log In with newly created Corporate user");
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());

			fc.utobj().printTestStep("Navigate To The Hub Module");
			fc.hub().hub_common().theHubHome(driver);

			fc.utobj().printTestStep("Restore default setting for Manage Widgets");
			fc.utobj().clickElement(driver, pobj.manageWidgetsLink);
			fc.utobj().clickElement(driver, pobj.restoredefaultsettings);

			fc.utobj().printTestStep("Verify Epoll section should not visible under hub > home");

			boolean isTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[@id='epollHomePageHeading']");

			if (isTextPresent) {
				fc.utobj().throwsException("Epoll section is visible under hub > home");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_role","hub_home" })
	@TestCase(createdOn = "2018-01-03", updatedOn = "2018-01-03", testCaseDescription = "Verify On deselecting Can View EPoll Privilege , Epoll section should not appear under the HUB Module By Divisional User", testCaseId = "TC_Hub_Epoll_Privilege_02")
	private void epollPrivileges02() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Go to admin > Roles > Create Divisional Role with Can View EPoll as deselected");
			TheHubHomePage pobj = new TheHubHomePage(driver);

			AdminUsersRolesAddNewRolePageTest roles_page = new AdminUsersRolesAddNewRolePageTest();
			String roleType = dataSet.get("roleType");
			String roleName = fc.utobj().generateTestData("TcDivisionalRole");
			String moduleName = "The Hub";
			String emailId = "hubautomation@staffex.com";
			String userName = fc.utobj().generateTestData("du");
			String password = "t0n1ght@123";
			String divisionName = fc.utobj().generateTestData("Testdiv");

			Map<String, String> privileges = new HashMap<String, String>();
			privileges.put("Can View EPoll", "No");
			roles_page.addRolesWithModulesPrivileges(driver, roleType, roleName, moduleName, privileges, testCaseId);

			fc.utobj().printTestStep(
					"Navigate To Admin > Users > Manage Divisional Users > Add  Divisional User with added role");
			AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest divisional_user = new AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest();
			divisional_user.addDivisionalUserWithRole(driver, userName, divisionName, roleName, password, emailId);

			fc.utobj().printTestStep("Logged out from ADM User");
			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Log In with newly created Divisional user");
			fc.loginpage().loginWithParameter(driver, userName, password);

			fc.utobj().printTestStep("Navigate To The Hub Module");
			fc.hub().hub_common().theHubHome(driver);

			fc.utobj().printTestStep("Restore default setting for Manage Widgets");
			fc.utobj().clickElement(driver, pobj.manageWidgetsLink);
			fc.utobj().clickElement(driver, pobj.restoredefaultsettings);

			fc.utobj().printTestStep("Verify Epoll section should not visible under hub > home");

			boolean isTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[@id='epollHomePageHeading']");

			if (isTextPresent) {
				fc.utobj().throwsException("Epoll section is visible under hub > home");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_role" ,"hub_home"})
	@TestCase(createdOn = "2018-01-03", updatedOn = "2018-01-03", testCaseDescription = "Verify On deselecting Can View EPoll Privilege , Epoll section should not appear under the HUB Module By Regional User", testCaseId = "TC_Hub_Epoll_Privilege_03")
	private void epollPrivileges03() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Go to admin > Roles > Create Regional Role with Can View EPoll as deselected");
			TheHubHomePage pobj = new TheHubHomePage(driver);

			AdminUsersRolesAddNewRolePageTest roles_page = new AdminUsersRolesAddNewRolePageTest();
			String roleType = dataSet.get("roleType");
			String roleName = fc.utobj().generateTestData("TcRegionalRole");
			String moduleName = "The Hub";
			String emailId = "hubautomation@staffex.com";
			String userName = fc.utobj().generateTestData("ru");
			String password = "t0n1ght@123";
			String regionName = fc.utobj().generateTestData("TestReg");

			Map<String, String> privileges = new HashMap<String, String>();
			privileges.put("Can View EPoll", "No");
			roles_page.addRolesWithModulesPrivileges(driver, roleType, roleName, moduleName, privileges, testCaseId);

			fc.utobj().printTestStep(
					"Navigate To Admin > Users > Manage Regional Users > Add  Regional User with added role");
			AdminUsersManageRegionalUsersAddRegionalUserPageTest region_page = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
			region_page.addRegionalUserWithRole(driver, userName, password, regionName, roleName, emailId);

			fc.utobj().printTestStep("Logged out from ADM User");
			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Log In with newly created Regional user");
			fc.loginpage().loginWithParameter(driver, userName, password);

			fc.utobj().printTestStep("Navigate To The Hub Module");
			fc.hub().hub_common().theHubHome(driver);

			fc.utobj().printTestStep("Restore default setting for Manage Widgets");
			fc.utobj().clickElement(driver, pobj.manageWidgetsLink);
			fc.utobj().clickElement(driver, pobj.restoredefaultsettings);

			fc.utobj().printTestStep("Verify Epoll section should not visible under hub > home");

			boolean isTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[@id='epollHomePageHeading']");

			if (isTextPresent) {
				fc.utobj().throwsException("Epoll section is visible under hub > home");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_role" ,"hub_home"})
	@TestCase(createdOn = "2018-01-03", updatedOn = "2018-01-03", testCaseDescription = "Verify On deselecting Can View EPoll Privilege , Epoll section should not appear under the HUB Module By Franchise User", testCaseId = "TC_Hub_Epoll_Privilege_04")
	private void epollPrivileges04() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Go to admin > Roles > Create franchise Role with Can View EPoll as deselected");
			TheHubHomePage pobj = new TheHubHomePage(driver);

			AdminUsersRolesAddNewRolePageTest roles_page = new AdminUsersRolesAddNewRolePageTest();
			String roleType = dataSet.get("roleType");
			String roleName = fc.utobj().generateTestData("TcFranchiseRole");
			String moduleName = "The Hub";
			String emailId = "hubautomation@staffex.com";
			String userName = fc.utobj().generateTestData("fu");
			String password = "t0n1ght@123";
			String regionName = fc.utobj().generateTestData("TestReg");
			String storeType = fc.utobj().generateTestData("TestStore");
			String franchiseName = fc.utobj().generateTestData("TestFran");
			String centerName = fc.utobj().generateTestData("center");
			String openingDate = fc.utobj().getCurrentDateUSFormat();
			String ownerFirstName = fc.utobj().generateTestData("Testfname");
			String ownerLastName = fc.utobj().generateTestData("Testlname");

			Map<String, String> privileges = new HashMap<String, String>();
			privileges.put("Can View EPoll", "No");
			roles_page.addRolesWithModulesPrivileges(driver, roleType, roleName, moduleName, privileges, testCaseId);

			fc.utobj().printTestStep(
					"Navigate To Admin > Franchise Location > Manage Franchise Locations > Add Franchise Location");
			AdminFranchiseLocationAddFranchiseLocationPageTest location_page = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String franchiseId = location_page.addFranchiseLocation(driver, regionName, storeType, franchiseName,
					centerName, openingDate, ownerFirstName, ownerLastName, config);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Franchise Users > Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest franchise_page = new AdminUsersManageManageFranchiseUsersPageTest();
			franchise_page.addFranchiseUser(driver, userName, password, franchiseId, roleName, emailId);

			fc.utobj().printTestStep("Logged out from ADM User");
			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Log In with newly created franchise user");
			fc.loginpage().loginWithParameter(driver, userName, password);

			fc.utobj().printTestStep("Navigate To The Hub Module");
			fc.hub().hub_common().theHubHome(driver);

			fc.utobj().printTestStep("Restore default setting for Manage Widgets");
			fc.utobj().clickElement(driver, pobj.manageWidgetsLink);
			fc.utobj().clickElement(driver, pobj.restoredefaultsettings);

			fc.utobj().printTestStep("Verify Epoll section should not visible under hub > home");

			boolean isTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[@id='epollHomePageHeading']");

			if (isTextPresent) {
				fc.utobj().throwsException("Epoll section is visible under hub > home");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_role","hub_home" })
	@TestCase(createdOn = "2018-01-03", updatedOn = "2018-01-03", testCaseDescription = "Verify On deselecting can view Related Links Privilege , Related Links section should not appear under the HUB Module By Corporate User", testCaseId = "TC_Hub_Related_Link_Privilege_01")
	private void relatedLnkPrivileges01() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep(
					"Go to admin > Roles > Create Corporate Role with Can View Related Links as deselected");

			AdminUsersRolesAddNewRolePageTest roles_page = new AdminUsersRolesAddNewRolePageTest();
			String roleType = dataSet.get("roleType");
			String roleName = fc.utobj().generateTestData("TcCorporateRole");
			String moduleName = "The Hub";
			String emailId = "hubautomation@staffex.com";

			Map<String, String> privileges = new HashMap<String, String>();
			privileges.put("Can View Related Links", "No");
			roles_page.addRolesWithModulesPrivileges(driver, roleType, roleName, moduleName, privileges, testCaseId);

			fc.utobj().printTestStep(
					"Navigate To Admin > Users > Manage Corporate Users > Add  Corporate User with added role");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporate_user = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser.setRole(roleName);
			corpUser = corporate_user.createDefaultUser(driver, corpUser);

			fc.utobj().printTestStep("Logged out from ADM User");
			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Log In with newly created Corporate user");
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());

			fc.utobj().printTestStep("Navigate To The Hub Module");
			fc.hub().hub_common().theHubModule(driver);

			fc.utobj().printTestStep("Verify Related Links sub module should not visible under hub");

			boolean isSubTabPresent = false;

			List<WebElement> listElement = driver.findElements(By.xpath(".//*[@id='module_intranet']/ul/li/a"));

			if (listElement.size() == 0) {
				fc.utobj().throwsException("No Sub module is present under hub module");
			} else if (listElement.size() > 0) {

				String[] arrayString = new String[listElement.size()];

				for (int i = 0; i < listElement.size(); i++) {
					arrayString[i] = listElement.get(i).getText().trim();
				}

				for (String string : arrayString) {

					if (string.equalsIgnoreCase(fc.utobj().translateString("Related Links"))) {
						isSubTabPresent = true;
					}
				}
			}

			if (isSubTabPresent) {
				fc.utobj().throwsException("Related Links sub module is visible under hub");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_role" ,"hub_home"})
	@TestCase(createdOn = "2018-01-03", updatedOn = "2018-01-03", testCaseDescription = "Verify On deselecting can view Related Links Privilege , Related Links section should not appear under the HUB Module By Divisional User", testCaseId = "TC_Hub_Related_Link_Privilege_02")
	private void relatedLnkPrivileges02() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep(
					"Go to admin > Roles > Create Divisional Role with Can View Related Links as deselected");

			AdminUsersRolesAddNewRolePageTest roles_page = new AdminUsersRolesAddNewRolePageTest();
			String roleType = dataSet.get("roleType");
			String roleName = fc.utobj().generateTestData("TcDivisionalRole");
			String moduleName = "The Hub";
			String emailId = "hubautomation@staffex.com";
			String userName = fc.utobj().generateTestData("du");
			String password = "t0n1ght@123";
			String divisionName = fc.utobj().generateTestData("Testdiv");

			Map<String, String> privileges = new HashMap<String, String>();
			privileges.put("Can View Related Links", "No");
			roles_page.addRolesWithModulesPrivileges(driver, roleType, roleName, moduleName, privileges, testCaseId);

			fc.utobj().printTestStep(
					"Navigate To Admin > Users > Manage Divisional Users > Add  Divisional User with added role");
			AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest divisional_user = new AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest();
			divisional_user.addDivisionalUserWithRole(driver, userName, divisionName, roleName, password, emailId);

			fc.utobj().printTestStep("Logged out from ADM User");
			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Log In with newly created Divisional user");
			fc.loginpage().loginWithParameter(driver, userName, password);

			fc.utobj().printTestStep("Navigate To The Hub Module");
			fc.hub().hub_common().theHubModule(driver);

			fc.utobj().printTestStep("Verify Related Links sub module should not visible under hub");

			boolean isSubTabPresent = false;

			List<WebElement> listElement = driver.findElements(By.xpath(".//*[@id='module_intranet']/ul/li/a"));

			if (listElement.size() == 0) {
				fc.utobj().throwsException("No Sub module is present under hub module");
			} else if (listElement.size() > 0) {

				String[] arrayString = new String[listElement.size()];

				for (int i = 0; i < listElement.size(); i++) {
					arrayString[i] = listElement.get(i).getText().trim();
				}

				for (String string : arrayString) {

					if (string.equalsIgnoreCase(fc.utobj().translateString("Related Links"))) {
						isSubTabPresent = true;
					}
				}
			}

			if (isSubTabPresent) {
				fc.utobj().throwsException("Related Links sub module is visible under hub");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_role","hub_home"})
	@TestCase(createdOn = "2018-01-03", updatedOn = "2018-01-03", testCaseDescription = "Verify On deselecting can view Related Links Privilege , Related Links section should not appear under the HUB Module By Regional User", testCaseId = "TC_Hub_Related_Link_Privilege_03")
	private void relatedLnkPrivileges03() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep(
					"Go to admin > Roles > Create Regional Role with Can View Related Links as deselected");

			AdminUsersRolesAddNewRolePageTest roles_page = new AdminUsersRolesAddNewRolePageTest();
			String roleType = dataSet.get("roleType");
			String roleName = fc.utobj().generateTestData("TcRegionalRole");
			String moduleName = "The Hub";
			String emailId = "hubautomation@staffex.com";
			String userName = fc.utobj().generateTestData("ru");
			String password = "t0n1ght@123";
			String regionName = fc.utobj().generateTestData("TestReg");

			Map<String, String> privileges = new HashMap<String, String>();
			privileges.put("Can View Related Links", "No");
			roles_page.addRolesWithModulesPrivileges(driver, roleType, roleName, moduleName, privileges, testCaseId);

			fc.utobj().printTestStep(
					"Navigate To Admin > Users > Manage Regional Users > Add  Regional User with added role");
			AdminUsersManageRegionalUsersAddRegionalUserPageTest region_page = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
			region_page.addRegionalUserWithRole(driver, userName, password, regionName, roleName, emailId);

			fc.utobj().printTestStep("Logged out from ADM User");
			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Log In with newly created Regional user");
			fc.loginpage().loginWithParameter(driver, userName, password);

			fc.utobj().printTestStep("Navigate To The Hub Module");
			fc.hub().hub_common().theHubModule(driver);

			fc.utobj().printTestStep("Verify Related Links sub module should not visible under hub");

			boolean isSubTabPresent = false;

			List<WebElement> listElement = driver.findElements(By.xpath(".//*[@id='module_intranet']/ul/li/a"));

			if (listElement.size() == 0) {
				fc.utobj().throwsException("No Sub module is present under hub module");
			} else if (listElement.size() > 0) {

				String[] arrayString = new String[listElement.size()];

				for (int i = 0; i < listElement.size(); i++) {
					arrayString[i] = listElement.get(i).getText().trim();
				}

				for (String string : arrayString) {

					if (string.equalsIgnoreCase(fc.utobj().translateString("Related Links"))) {
						isSubTabPresent = true;
					}
				}
			}

			if (isSubTabPresent) {
				fc.utobj().throwsException("Related Links sub module is visible under hub");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_role","hub_home" })
	@TestCase(createdOn = "2018-01-03", updatedOn = "2018-01-03", testCaseDescription = "Verify On deselecting can view Related Links Privilege , Related Links section should not appear under the HUB Module By Franchise User", testCaseId = "TC_Hub_Related_Link_Privilege_04")
	private void relatedLnkPrivileges04() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep(
					"Go to admin > Roles > Create franchise Role with can view Related Links as deselected");

			AdminUsersRolesAddNewRolePageTest roles_page = new AdminUsersRolesAddNewRolePageTest();
			String roleType = dataSet.get("roleType");
			String roleName = fc.utobj().generateTestData("TcFranchiseRole");
			String moduleName = "The Hub";
			String emailId = "hubautomation@staffex.com";
			String userName = fc.utobj().generateTestData("fu");
			String password = "t0n1ght@123";
			String regionName = fc.utobj().generateTestData("TestReg");
			String storeType = fc.utobj().generateTestData("TestStore");
			String franchiseName = fc.utobj().generateTestData("TestFran");
			String centerName = fc.utobj().generateTestData("center");
			String openingDate = fc.utobj().getCurrentDateUSFormat();
			String ownerFirstName = fc.utobj().generateTestData("Testfname");
			String ownerLastName = fc.utobj().generateTestData("Testlname");

			Map<String, String> privileges = new HashMap<String, String>();
			privileges.put("Can View Related Links", "No");
			roles_page.addRolesWithModulesPrivileges(driver, roleType, roleName, moduleName, privileges, testCaseId);

			fc.utobj().printTestStep(
					"Navigate To Admin > Franchise Location > Manage Franchise Locations > Add Franchise Location");
			AdminFranchiseLocationAddFranchiseLocationPageTest location_page = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String franchiseId = location_page.addFranchiseLocation(driver, regionName, storeType, franchiseName,
					centerName, openingDate, ownerFirstName, ownerLastName, config);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Franchise Users > Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest franchise_page = new AdminUsersManageManageFranchiseUsersPageTest();
			franchise_page.addFranchiseUser(driver, userName, password, franchiseId, roleName, emailId);

			fc.utobj().printTestStep("Logged out from ADM User");
			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Log In with newly created franchise user");
			fc.loginpage().loginWithParameter(driver, userName, password);

			fc.utobj().printTestStep("Navigate To The Hub Module");
			fc.hub().hub_common().theHubModule(driver);

			fc.utobj().printTestStep("Verify Related Links sub module should not visible under hub");

			boolean isSubTabPresent = false;

			List<WebElement> listElement = driver.findElements(By.xpath(".//*[@id='module_intranet']/ul/li/a"));

			if (listElement.size() == 0) {
				fc.utobj().throwsException("No Sub module is present under hub module");
			} else if (listElement.size() > 0) {

				String[] arrayString = new String[listElement.size()];

				for (int i = 0; i < listElement.size(); i++) {
					arrayString[i] = listElement.get(i).getText().trim();
				}

				for (String string : arrayString) {

					if (string.equalsIgnoreCase(fc.utobj().translateString("Related Links"))) {
						isSubTabPresent = true;
					}
				}
			}

			if (isSubTabPresent) {
				fc.utobj().throwsException("Related Links sub module is visible under hub");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/*
	 * Alert Section Verify At Home : TC-10900 :
	 */

	@Test(groups = { "thehub", "hub_role","hub_home" })
	@TestCase(createdOn = "2018-01-09", updatedOn = "2018-01-09", testCaseDescription = "Verify On deselecting can view alert Privilege , Alert section should not appear under the Hub Module By Corporate User", testCaseId = "TC_Hub_Alert_Privilege_01")
	private void alertPrivileges01() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Go to admin > Roles > Create Corporate Role with can view alert as deselected");
			TheHubHomePage pobj = new TheHubHomePage(driver);

			AdminUsersRolesAddNewRolePageTest roles_page = new AdminUsersRolesAddNewRolePageTest();
			String roleType = dataSet.get("roleType");
			String roleName = fc.utobj().generateTestData("TcCorporateRole");
			String moduleName = "The Hub";
			String emailId = "hubautomation@staffex.com";

			Map<String, String> privileges = new HashMap<String, String>();
			privileges.put("Can View Alerts", "No");
			roles_page.addRolesWithModulesPrivileges(driver, roleType, roleName, moduleName, privileges, testCaseId);

			fc.utobj().printTestStep(
					"Navigate To Admin > Users > Manage Corporate Users > Add  Corporate User with added role");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporate_user = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser.setRole(roleName);
			corpUser = corporate_user.createDefaultUser(driver, corpUser);

			fc.utobj().printTestStep("Logged out from ADM User");
			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Log In with newly created Corporate user");
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());

			fc.utobj().printTestStep("Navigate To The Hub Module");
			fc.hub().hub_common().theHubHome(driver);

			fc.utobj().printTestStep("Restore default setting for Manage Widgets");
			fc.utobj().clickElement(driver, pobj.manageWidgetsLink);
			fc.utobj().clickElement(driver, pobj.restoredefaultsettings);

			fc.utobj().printTestStep("Verify Alert section should not visible under hub > home");

			boolean isTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[@id='alertsHomePageHeading']");

			if (isTextPresent) {
				fc.utobj().throwsException("Alert section is visible under hub > home");
			}

			fc.utobj().printTestStep("Navigate To The Hub Module");
			fc.hub().hub_common().theHubModule(driver);

			fc.utobj().printTestStep("Verify Alerts sub module should not visible under hub");

			boolean isSubTabPresent = false;

			List<WebElement> listElement = driver.findElements(By.xpath(".//*[@id='module_intranet']/ul/li/a"));

			if (listElement.size() == 0) {
				fc.utobj().throwsException("No Sub module is present under hub module");
			} else if (listElement.size() > 0) {

				String[] arrayString = new String[listElement.size()];

				for (int i = 0; i < listElement.size(); i++) {
					arrayString[i] = listElement.get(i).getText().trim();
				}

				for (String string : arrayString) {

					if (string.equalsIgnoreCase(fc.utobj().translateString("Alerts"))) {
						isSubTabPresent = true;
					}
				}
			}

			if (isSubTabPresent) {
				fc.utobj().throwsException("Alerts sub module is visible under hub");
			}

			fc.utobj().printTestStep(
					"Verify Click on Logged User Name then select Search link from drop down list, then Alert Tab should not appear");
			FCHomePage home_page = new FCHomePage(driver);
			fc.utobj().clickElement(driver, home_page.userOptions);
			fc.utobj().clickElement(driver, home_page.search);

			boolean isAlertTabPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[@qat_tabname='Alerts']");
			if (isAlertTabPresent) {
				fc.utobj().throwsException("Alert Tab is visible");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_role","hub_home" })
	@TestCase(createdOn = "2018-01-09", updatedOn = "2018-01-09", testCaseDescription = "Verify On deselecting can view alert Privilege , Alert section should not appear under the HUB Module By Divisional User", testCaseId = "TC_Hub_Alert_Privilege_02")
	private void alertPrivileges02() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Go to admin > Roles > Create Divisional Role with Can View Alerts as deselected");
			TheHubHomePage pobj = new TheHubHomePage(driver);

			AdminUsersRolesAddNewRolePageTest roles_page = new AdminUsersRolesAddNewRolePageTest();
			String roleType = dataSet.get("roleType");
			String roleName = fc.utobj().generateTestData("TcDivisionalRole");
			String moduleName = "The Hub";
			String emailId = "hubautomation@staffex.com";
			String userName = fc.utobj().generateTestData("du");
			String password = "t0n1ght@123";
			String divisionName = fc.utobj().generateTestData("Testdiv");

			Map<String, String> privileges = new HashMap<String, String>();
			privileges.put("Can View Alerts", "No");
			roles_page.addRolesWithModulesPrivileges(driver, roleType, roleName, moduleName, privileges, testCaseId);

			fc.utobj().printTestStep(
					"Navigate To Admin > Users > Manage Divisional Users > Add  Divisional User with added role");
			AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest divisional_user = new AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest();
			divisional_user.addDivisionalUserWithRole(driver, userName, divisionName, roleName, password, emailId);

			fc.utobj().printTestStep("Logged out from ADM User");
			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Log In with newly created Divisional user");
			fc.loginpage().loginWithParameter(driver, userName, password);

			fc.utobj().printTestStep("Navigate To The Hub Module");
			fc.hub().hub_common().theHubHome(driver);

			fc.utobj().printTestStep("Restore default setting for Manage Widgets");
			fc.utobj().clickElement(driver, pobj.manageWidgetsLink);
			fc.utobj().clickElement(driver, pobj.restoredefaultsettings);

			fc.utobj().printTestStep("Verify Alert section should not visible under hub > home");

			boolean isTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[@id='alertsHomePageHeading']");

			if (isTextPresent) {
				fc.utobj().throwsException("Alert section is visible under hub > home");
			}

			fc.utobj().printTestStep("Navigate To The Hub Module");
			fc.hub().hub_common().theHubModule(driver);

			fc.utobj().printTestStep("Verify Alerts sub module should not visible under hub");

			boolean isSubTabPresent = false;

			List<WebElement> listElement = driver.findElements(By.xpath(".//*[@id='module_intranet']/ul/li/a"));

			if (listElement.size() == 0) {
				fc.utobj().throwsException("No Sub module is present under hub module");
			} else if (listElement.size() > 0) {

				String[] arrayString = new String[listElement.size()];

				for (int i = 0; i < listElement.size(); i++) {
					arrayString[i] = listElement.get(i).getText().trim();
				}

				for (String string : arrayString) {

					if (string.equalsIgnoreCase(fc.utobj().translateString("Alerts"))) {
						isSubTabPresent = true;
					}
				}
			}

			if (isSubTabPresent) {
				fc.utobj().throwsException("Alerts sub module is visible under hub");
			}

			fc.utobj().printTestStep(
					"Verify Click on Logged User Name then select Search link from drop down list, then Alert Tab should not appear");
			FCHomePage home_page = new FCHomePage(driver);
			fc.utobj().clickElement(driver, home_page.userOptions);
			fc.utobj().clickElement(driver, home_page.search);

			boolean isAlertTabPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[@qat_tabname='Alerts']");
			if (isAlertTabPresent) {
				fc.utobj().throwsException("Alert Tab is visible");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_role" ,"hub_home" })
	@TestCase(createdOn = "2018-01-09", updatedOn = "2018-01-09", testCaseDescription = "Verify On deselecting can view alert Privilege , Alert section should not appear under the HUB Module By Regional User", testCaseId = "TC_Hub_Alert_Privilege_03")
	private void alertPrivileges03() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Go to admin > Roles > Create Regional Role with Can View Alerts as deselected");
			TheHubHomePage pobj = new TheHubHomePage(driver);

			AdminUsersRolesAddNewRolePageTest roles_page = new AdminUsersRolesAddNewRolePageTest();
			String roleType = dataSet.get("roleType");
			String roleName = fc.utobj().generateTestData("TcRegionalRole");
			String moduleName = "The Hub";
			String emailId = "hubautomation@staffex.com";
			String userName = fc.utobj().generateTestData("ru");
			String password = "t0n1ght@123";
			String regionName = fc.utobj().generateTestData("TestReg");

			Map<String, String> privileges = new HashMap<String, String>();
			privileges.put("Can View Alerts", "No");
			roles_page.addRolesWithModulesPrivileges(driver, roleType, roleName, moduleName, privileges, testCaseId);

			fc.utobj().printTestStep(
					"Navigate To Admin > Users > Manage Regional Users > Add  Regional User with added role");
			AdminUsersManageRegionalUsersAddRegionalUserPageTest region_page = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
			region_page.addRegionalUserWithRole(driver, userName, password, regionName, roleName, emailId);

			fc.utobj().printTestStep("Logged out from ADM User");
			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Log In with newly created Regional user");
			fc.loginpage().loginWithParameter(driver, userName, password);

			fc.utobj().printTestStep("Navigate To The Hub Module");
			fc.hub().hub_common().theHubHome(driver);

			fc.utobj().printTestStep("Restore default setting for Manage Widgets");
			fc.utobj().clickElement(driver, pobj.manageWidgetsLink);
			fc.utobj().clickElement(driver, pobj.restoredefaultsettings);

			fc.utobj().printTestStep("Verify Alert section should not visible under hub > home");

			boolean isTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[@id='alertsHomePageHeading']");

			if (isTextPresent) {
				fc.utobj().throwsException("Alert section is visible under hub > home");
			}

			fc.utobj().printTestStep("Navigate To The Hub Module");
			fc.hub().hub_common().theHubModule(driver);

			fc.utobj().printTestStep("Verify Alerts sub module should not visible under hub");

			boolean isSubTabPresent = false;

			List<WebElement> listElement = driver.findElements(By.xpath(".//*[@id='module_intranet']/ul/li/a"));

			if (listElement.size() == 0) {
				fc.utobj().throwsException("No Sub module is present under hub module");
			} else if (listElement.size() > 0) {

				String[] arrayString = new String[listElement.size()];

				for (int i = 0; i < listElement.size(); i++) {
					arrayString[i] = listElement.get(i).getText().trim();
				}

				for (String string : arrayString) {

					if (string.equalsIgnoreCase(fc.utobj().translateString("Alerts"))) {
						isSubTabPresent = true;
					}
				}
			}

			if (isSubTabPresent) {
				fc.utobj().throwsException("Alerts sub module is visible under hub");
			}

			fc.utobj().printTestStep(
					"Verify Click on Logged User Name then select Search link from drop down list, then Alert Tab should not appear");
			FCHomePage home_page = new FCHomePage(driver);
			fc.utobj().clickElement(driver, home_page.userOptions);
			fc.utobj().clickElement(driver, home_page.search);

			boolean isAlertTabPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[@qat_tabname='Alerts']");
			if (isAlertTabPresent) {
				fc.utobj().throwsException("Alert Tab is visible");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_role","hub_home"})
	@TestCase(createdOn = "2018-01-09", updatedOn = "2018-01-09", testCaseDescription = "Verify On deselecting can view alert Privilege , Alert section should not appear under the HUB Module By Franchise User", testCaseId = "TC_Hub_Alert_Privilege_04")
	private void alertPrivileges04() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Go to admin > Roles > Create franchise Role with Can View Alerts as deselected");
			TheHubHomePage pobj = new TheHubHomePage(driver);

			AdminUsersRolesAddNewRolePageTest roles_page = new AdminUsersRolesAddNewRolePageTest();
			String roleType = dataSet.get("roleType");
			String roleName = fc.utobj().generateTestData("TcFranchiseRole");
			String moduleName = "The Hub";
			String emailId = "hubautomation@staffex.com";
			String userName = fc.utobj().generateTestData("fu");
			String password = "t0n1ght@123";
			String regionName = fc.utobj().generateTestData("TestReg");
			String storeType = fc.utobj().generateTestData("TestStore");
			String franchiseName = fc.utobj().generateTestData("TestFran");
			String centerName = fc.utobj().generateTestData("center");
			String openingDate = fc.utobj().getCurrentDateUSFormat();
			String ownerFirstName = fc.utobj().generateTestData("Testfname");
			String ownerLastName = fc.utobj().generateTestData("Testlname");

			Map<String, String> privileges = new HashMap<String, String>();
			privileges.put("Can View Alerts", "No");
			roles_page.addRolesWithModulesPrivileges(driver, roleType, roleName, moduleName, privileges, testCaseId);

			fc.utobj().printTestStep(
					"Navigate To Admin > Franchise Location > Manage Franchise Locations > Add Franchise Location");
			AdminFranchiseLocationAddFranchiseLocationPageTest location_page = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String franchiseId = location_page.addFranchiseLocation(driver, regionName, storeType, franchiseName,
					centerName, openingDate, ownerFirstName, ownerLastName, config);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Franchise Users > Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest franchise_page = new AdminUsersManageManageFranchiseUsersPageTest();
			franchise_page.addFranchiseUser(driver, userName, password, franchiseId, roleName, emailId);

			fc.utobj().printTestStep("Logged out from ADM User");
			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Log In with newly created franchise user");
			fc.loginpage().loginWithParameter(driver, userName, password);

			fc.utobj().printTestStep("Navigate To The Hub Module");
			fc.hub().hub_common().theHubHome(driver);

			fc.utobj().printTestStep("Restore default setting for Manage Widgets");
			fc.utobj().clickElement(driver, pobj.manageWidgetsLink);
			fc.utobj().clickElement(driver, pobj.restoredefaultsettings);

			fc.utobj().printTestStep("Verify Alert section should not visible under hub > home");

			boolean isTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[@id='alertsHomePageHeading']");

			if (isTextPresent) {
				fc.utobj().throwsException("Alert section is visible under hub > home");
			}

			fc.utobj().printTestStep("Navigate To The Hub Module");
			fc.hub().hub_common().theHubModule(driver);

			fc.utobj().printTestStep("Verify Alerts sub module should not visible under hub");

			boolean isSubTabPresent = false;

			List<WebElement> listElement = driver.findElements(By.xpath(".//*[@id='module_intranet']/ul/li/a"));

			if (listElement.size() == 0) {
				fc.utobj().throwsException("No Sub module is present under hub module");
			} else if (listElement.size() > 0) {

				String[] arrayString = new String[listElement.size()];

				for (int i = 0; i < listElement.size(); i++) {
					arrayString[i] = listElement.get(i).getText().trim();
				}

				for (String string : arrayString) {

					if (string.equalsIgnoreCase(fc.utobj().translateString("Alerts"))) {
						isSubTabPresent = true;
					}
				}
			}

			if (isSubTabPresent) {
				fc.utobj().throwsException("Alerts sub module is visible under hub");
			}

			fc.utobj().printTestStep(
					"Verify Click on Logged User Name then select Search link from drop down list, then Alert Tab should not appear");
			FCHomePage home_page = new FCHomePage(driver);
			fc.utobj().clickElement(driver, home_page.userOptions);
			fc.utobj().clickElement(driver, home_page.search);

			boolean isAlertTabPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[@qat_tabname='Alerts']");
			if (isAlertTabPresent) {
				fc.utobj().throwsException("Alert Tab is visible");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/*
	 * News Section should not visible at Hub Home
	 */

	@Test(groups = { "thehub", "hub_role","hub_home"})
	@TestCase(createdOn = "2018-01-10", updatedOn = "2018-01-10", testCaseDescription = "Verify On deselecting can view News Privilege , News section should not appear under the HUB Module By Corporate User", testCaseId = "TC_Hub_News_Privilege_01")
	private void newsSubTab01() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminTheHubNewsPageTest newsPage = new AdminTheHubNewsPageTest();
			TheHubNewsPage pobj = new TheHubNewsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > News Page");
			fc.utobj().printTestStep("Add News Item");

			String newsItemTitle = fc.utobj().generateTestData("TestNewsItem");
			String briefSummary = fc.utobj().generateTestData("TestBriefSum");
			String file = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));

			String editorText = fc.utobj().generateTestData("TestEditorText");
			fc.hub().hub_common().adminTheHubNewsPage(driver);
			fc.utobj().clickElement(driver, pobj.topStoriesTab);
			
			News news=new News();
			news.setNewsItemTitle(newsItemTitle);
			news.setNewsBriefSummary(briefSummary);
			news.setNewsEditorText(editorText);
			news.setFile(file);
			newsPage.addNewsItems(driver,news);

			fc.utobj().printTestStep("Go to admin > Roles > Create Corporate Role with Can View News as deselected");
			AdminUsersRolesAddNewRolePageTest roles_page = new AdminUsersRolesAddNewRolePageTest();
			String roleType = dataSet.get("roleType");
			String roleName = fc.utobj().generateTestData("TcCorporateRole");
			String moduleName = "The Hub";
			String emailId = "hubautomation@staffex.com";

			Map<String, String> privileges = new HashMap<String, String>();
			privileges.put("Can View News", "No");
			roles_page.addRolesWithModulesPrivileges(driver, roleType, roleName, moduleName, privileges, testCaseId);

			fc.utobj().printTestStep(
					"Navigate To Admin > Users > Manage Corporate Users > Add  Corporate User with added role");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporate_user = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser.setRole(roleName);
			corpUser = corporate_user.createDefaultUser(driver, corpUser);

			fc.utobj().printTestStep("Logged out from ADM User");
			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Log In with newly created Corporate user");
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());

			fc.utobj().printTestStep("Navigate To The Hub Module");
			fc.hub().hub_common().theHubModule(driver);

			fc.utobj().printTestStep("Verify News sub module should not visible under hub");

			boolean isSubTabPresent = false;

			List<WebElement> listElement = driver.findElements(By.xpath(".//*[@id='module_intranet']/ul/li/a"));

			if (listElement.size() == 0) {
				fc.utobj().throwsException("No Sub module is present under hub module");
			} else if (listElement.size() > 0) {

				String[] arrayString = new String[listElement.size()];

				for (int i = 0; i < listElement.size(); i++) {
					arrayString[i] = listElement.get(i).getText().trim();
				}

				for (String string : arrayString) {

					if (string.equalsIgnoreCase(fc.utobj().translateString("News"))) {
						isSubTabPresent = true;
					}
				}
			}

			if (isSubTabPresent) {
				fc.utobj().throwsException("News sub module is visible under hub");
			}

			SearchUI search_page = new SearchUI(driver);
			fc.utobj().printTestStep("Verify News should not visible in Top Search");
			boolean isSearchTrue = false;
			for (int i = 0; i < 3; i++) {
				if (isSearchTrue == false) {
					fc.utobj().sendKeys(driver, search_page.topSearchField, newsItemTitle);
					Thread.sleep(2000);
					if (i == 2) {
						Thread.sleep(2000);
					}
					fc.utobj().clickEnterOnElement(driver, search_page.topSearchField);

					try {
						isSearchTrue = fc.utobj().isElementPresent(driver
								.findElements(By.xpath(".//custom[contains(text () , '" + newsItemTitle + "')]")));
					} catch (Exception e) {
						Reporter.log(e.getMessage());
						System.out.println(e.getMessage());
					}
				}
			}

			if (isSearchTrue) {
				fc.utobj().throwsException("News Item is present at top search");
			}
			fc.utobj().refresh(driver);

			fc.utobj().printTestStep("News Item should not visible in What's New Subtab");

			fc.hub().hub_common().theHubWhatsNew(driver);

			boolean isAssert = fc.utobj().assertPageSource(driver, newsItemTitle);

			if (isAssert) {
				fc.utobj().throwsException("News items is visible at Whats New Subtab");
			}

			fc.utobj().printTestStep(
					"Verify News Item should not visible search by Search All The Hub Items search filter");
			fc.utobj().sendKeys(driver, pobj.searchAllDocument, newsItemTitle);
			fc.utobj().clickElement(driver, pobj.searchItem);
			pobj.searchAllDocument.clear();

			boolean isAssertNews = fc.utobj().assertPageSource(driver, newsItemTitle);

			if (isAssertNews) {
				fc.utobj().throwsException("News Item is visible search by Search All The Hub Items search filter");
			}

			/*fc.utobj().printTestStep("Verify News Item at Admin >News Logs");
			fc.hub().hub_common().adminTheHubNewsLogsPage(driver);
			fc.utobj().showAll(driver);

			boolean isText = fc.utobj().assertPageSource(driver, newsItemTitle);
			if (isText) {
				fc.utobj().throwsException("News Item is visible at Admin > News Logs");
			}*/

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_role","hub_home"})
	@TestCase(createdOn = "2018-01-10", updatedOn = "2018-01-10", testCaseDescription = "Verify On deselecting can view News Privilege , News section should not appear under the Hub Module By Divisional User", testCaseId = "TC_Hub_News_Privilege_02")
	private void newsSubTab02() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			AdminTheHubNewsPageTest newsPage = new AdminTheHubNewsPageTest();
			TheHubNewsPage pobj = new TheHubNewsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > News Page");
			fc.utobj().printTestStep("Add News Item");

			String newsItemTitle = fc.utobj().generateTestData("TestNewsItem");
			String briefSummary = fc.utobj().generateTestData("TestBriefSum");
			String file = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));

			String editorText = fc.utobj().generateTestData("TestEditorText");
			fc.hub().hub_common().adminTheHubNewsPage(driver);
			fc.utobj().clickElement(driver, pobj.topStoriesTab);
			
			News news=new News();
			news.setNewsItemTitle(newsItemTitle);
			news.setNewsBriefSummary(briefSummary);
			news.setNewsEditorText(editorText);
			news.setFile(file);
			
			newsPage.addNewsItems(driver,news);

			fc.utobj().printTestStep("Go to admin > Roles > Create Divisional Role with Can View News as deselected");

			AdminUsersRolesAddNewRolePageTest roles_page = new AdminUsersRolesAddNewRolePageTest();
			String roleType = dataSet.get("roleType");
			String roleName = fc.utobj().generateTestData("TcDivisionalRole");
			String moduleName = "The Hub";
			String emailId = "hubautomation@staffex.com";
			String userName = fc.utobj().generateTestData("du");
			String password = "t0n1ght@123";
			String divisionName = fc.utobj().generateTestData("Testdiv");

			Map<String, String> privileges = new HashMap<String, String>();
			privileges.put("Can View News", "No");
			roles_page.addRolesWithModulesPrivileges(driver, roleType, roleName, moduleName, privileges, testCaseId);

			fc.utobj().printTestStep(
					"Navigate To Admin > Users > Manage Divisional Users > Add  Divisional User with added role");
			AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest divisional_user = new AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest();
			divisional_user.addDivisionalUserWithRole(driver, userName, divisionName, roleName, password, emailId);

			fc.utobj().printTestStep("Logged out from ADM User");
			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Log In with newly created Divisional user");
			fc.loginpage().loginWithParameter(driver, userName, password);

			fc.utobj().printTestStep("Navigate To The Hub Module");
			fc.hub().hub_common().theHubModule(driver);

			fc.utobj().printTestStep("Verify News sub module should not visible under hub");

			boolean isSubTabPresent = false;

			List<WebElement> listElement = driver.findElements(By.xpath(".//*[@id='module_intranet']/ul/li/a"));

			if (listElement.size() == 0) {
				fc.utobj().throwsException("No Sub module is present under hub module");
			} else if (listElement.size() > 0) {

				String[] arrayString = new String[listElement.size()];

				for (int i = 0; i < listElement.size(); i++) {
					arrayString[i] = listElement.get(i).getText().trim();
				}

				for (String string : arrayString) {

					if (string.equalsIgnoreCase(fc.utobj().translateString("News"))) {
						isSubTabPresent = true;
					}
				}
			}

			if (isSubTabPresent) {
				fc.utobj().throwsException("News sub module is visible under hub");
			}

			SearchUI search_page = new SearchUI(driver);
			fc.utobj().printTestStep("Verify News should not visible in Top Search");
			boolean isSearchTrue = false;
			for (int i = 0; i < 3; i++) {
				if (isSearchTrue == false) {
					fc.utobj().sendKeys(driver, search_page.topSearchField, newsItemTitle);
					Thread.sleep(2000);
					if (i == 2) {
						Thread.sleep(2000);
					}
					fc.utobj().clickEnterOnElement(driver, search_page.topSearchField);

					try {
						isSearchTrue = fc.utobj().isElementPresent(driver
								.findElements(By.xpath(".//custom[contains(text () , '" + newsItemTitle + "')]")));
					} catch (Exception e) {
						Reporter.log(e.getMessage());
						System.out.println(e.getMessage());
					}
				}
			}

			if (isSearchTrue) {
				fc.utobj().throwsException("News Item is present at top search");
			}
			fc.utobj().refresh(driver);

			fc.utobj().printTestStep("News Item should not visible in What's New Subtab");

			fc.hub().hub_common().theHubWhatsNew(driver);

			boolean isAssert = fc.utobj().assertPageSource(driver, newsItemTitle);

			if (isAssert) {
				fc.utobj().throwsException("News items is visible at Whats New Subtab");
			}

			fc.utobj().printTestStep(
					"Verify News Item should not visible search by Search All The Hub Items search filter");
			fc.utobj().sendKeys(driver, pobj.searchAllDocument, newsItemTitle);
			fc.utobj().clickElement(driver, pobj.searchItem);
			pobj.searchAllDocument.clear();

			boolean isAssertNews = fc.utobj().assertPageSource(driver, newsItemTitle);

			if (isAssertNews) {
				fc.utobj().throwsException("News Item is visible search by Search All The Hub Items search filter");
			}

			/*fc.utobj().printTestStep("Verify News Item at Admin >News Logs");
			fc.hub().hub_common().adminTheHubNewsLogsPage(driver);
			fc.utobj().showAll(driver);

			boolean isText = fc.utobj().assertPageSource(driver, newsItemTitle);
			if (isText) {
				fc.utobj().throwsException("News Item is visible at Admin > News Logs");
			}*/

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_role","hub_home"})
	@TestCase(createdOn = "2018-01-10", updatedOn = "2018-01-10", testCaseDescription = "Verify On deselecting can view News Privilege , News section should not appear under the HUB Module By Regional User", testCaseId = "TC_Hub_News_Privilege_03")
	private void newsSubTab03() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminTheHubNewsPageTest newsPage = new AdminTheHubNewsPageTest();
			TheHubNewsPage pobj = new TheHubNewsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > News Page");
			fc.utobj().printTestStep("Add News Item");

			String newsItemTitle = fc.utobj().generateTestData("TestNewsItem");
			String briefSummary = fc.utobj().generateTestData("TestBriefSum");
			String file = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			String editorText = fc.utobj().generateTestData("TestEditorText");
			
			News news=new News();
			news.setNewsItemTitle(newsItemTitle);
			news.setNewsBriefSummary(briefSummary);
			news.setNewsEditorText(editorText);
			news.setFile(file);
			
			
			fc.hub().hub_common().adminTheHubNewsPage(driver);
			fc.utobj().clickElement(driver, pobj.topStoriesTab);
			
			newsPage.addNewsItems(driver,news);

			fc.utobj().printTestStep("Go to admin > Roles > Create Regional Role with Can View News as deselected");

			AdminUsersRolesAddNewRolePageTest roles_page = new AdminUsersRolesAddNewRolePageTest();
			String roleType = dataSet.get("roleType");
			String roleName = fc.utobj().generateTestData("TcRegionalRole");
			String moduleName = "The Hub";
			String emailId = "hubautomation@staffex.com";
			String userName = fc.utobj().generateTestData("ru");
			String password = "t0n1ght@123";
			String regionName = fc.utobj().generateTestData("TestReg");

			Map<String, String> privileges = new HashMap<String, String>();
			privileges.put("Can View News", "No");
			roles_page.addRolesWithModulesPrivileges(driver, roleType, roleName, moduleName, privileges, testCaseId);

			fc.utobj().printTestStep(
					"Navigate To Admin > Users > Manage Regional Users > Add  Regional User with added role");
			AdminUsersManageRegionalUsersAddRegionalUserPageTest region_page = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
			region_page.addRegionalUserWithRole(driver, userName, password, regionName, roleName, emailId);

			fc.utobj().printTestStep("Logged out from ADM User");
			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Log In with newly created Regional user");
			fc.loginpage().loginWithParameter(driver, userName, password);

			fc.utobj().printTestStep("Navigate To The Hub Module");
			fc.hub().hub_common().theHubModule(driver);

			fc.utobj().printTestStep("Verify News sub module should not visible under hub");

			boolean isSubTabPresent = false;

			List<WebElement> listElement = driver.findElements(By.xpath(".//*[@id='module_intranet']/ul/li/a"));

			if (listElement.size() == 0) {
				fc.utobj().throwsException("No Sub module is present under hub module");
			} else if (listElement.size() > 0) {

				String[] arrayString = new String[listElement.size()];

				for (int i = 0; i < listElement.size(); i++) {
					arrayString[i] = listElement.get(i).getText().trim();
				}

				for (String string : arrayString) {

					if (string.equalsIgnoreCase(fc.utobj().translateString("News"))) {
						isSubTabPresent = true;
					}
				}
			}

			if (isSubTabPresent) {
				fc.utobj().throwsException("News sub module is visible under hub");
			}

			SearchUI search_page = new SearchUI(driver);
			fc.utobj().printTestStep("Verify News should not visible in Top Search");
			boolean isSearchTrue = false;
			for (int i = 0; i < 3; i++) {
				if (isSearchTrue == false) {
					fc.utobj().sendKeys(driver, search_page.topSearchField, newsItemTitle);
					Thread.sleep(2000);
					if (i == 2) {
						Thread.sleep(2000);
					}
					fc.utobj().clickEnterOnElement(driver, search_page.topSearchField);

					try {
						isSearchTrue = fc.utobj().isElementPresent(driver
								.findElements(By.xpath(".//custom[contains(text () , '" + newsItemTitle + "')]")));
					} catch (Exception e) {
						Reporter.log(e.getMessage());
						System.out.println(e.getMessage());
					}
				}
			}

			if (isSearchTrue) {
				fc.utobj().throwsException("News Item is present at top search");
			}
			fc.utobj().refresh(driver);

			fc.utobj().printTestStep("News Item should not visible in What's New Subtab");

			fc.hub().hub_common().theHubWhatsNew(driver);

			boolean isAssert = fc.utobj().assertPageSource(driver, newsItemTitle);

			if (isAssert) {
				fc.utobj().throwsException("News items is visible at Whats New Subtab");
			}

			fc.utobj().printTestStep(
					"Verify News Item should not visible search by Search All The Hub Items search filter");
			fc.utobj().sendKeys(driver, pobj.searchAllDocument, newsItemTitle);
			fc.utobj().clickElement(driver, pobj.searchItem);
			pobj.searchAllDocument.clear();

			boolean isAssertNews = fc.utobj().assertPageSource(driver, newsItemTitle);

			if (isAssertNews) {
				fc.utobj().throwsException("News Item is visible search by Search All The Hub Items search filter");
			}

			/*fc.utobj().printTestStep("Verify News Item at Admin >News Logs");
			fc.hub().hub_common().adminTheHubNewsLogsPage(driver);
			fc.utobj().showAll(driver);

			boolean isText = fc.utobj().assertPageSource(driver, newsItemTitle);
			if (isText) {
				fc.utobj().throwsException("News Item is visible at Admin > News Logs");
			}*/

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_role","hub_home"})
	@TestCase(createdOn = "2018-01-10", updatedOn = "2018-01-10", testCaseDescription = "Verify On deselecting can view News Privilege , News section should not appear under the HUB Module By Franchise User", testCaseId = "TC_Hub_News_Privilege_04")
	private void newsSubTab04() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminTheHubNewsPageTest newsPage = new AdminTheHubNewsPageTest();
			TheHubNewsPage pobj = new TheHubNewsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > The Hub > News Page");
			fc.utobj().printTestStep("Add News Item");

			String newsItemTitle = fc.utobj().generateTestData("TestNewsItem");
			String briefSummary = fc.utobj().generateTestData("TestBriefSum");
			String file = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			String editorText = fc.utobj().generateTestData("TestEditorText");

			News news=new News();
			news.setNewsItemTitle(newsItemTitle);
			news.setNewsBriefSummary(briefSummary);
			news.setNewsEditorText(editorText);
			news.setFile(file);
			
			fc.hub().hub_common().adminTheHubNewsPage(driver);
			fc.utobj().clickElement(driver, pobj.topStoriesTab);
			newsPage.addNewsItems(driver,news);

			fc.utobj().printTestStep("Go to admin > Roles > Create franchise Role with Can View News as deselected");

			AdminUsersRolesAddNewRolePageTest roles_page = new AdminUsersRolesAddNewRolePageTest();
			String roleType = dataSet.get("roleType");
			String roleName = fc.utobj().generateTestData("TcFranchiseRole");
			String moduleName = "The Hub";
			String emailId = "hubautomation@staffex.com";
			String userName = fc.utobj().generateTestData("fu");
			String password = "t0n1ght@123";
			String regionName = fc.utobj().generateTestData("TestReg");
			String storeType = fc.utobj().generateTestData("TestStore");
			String franchiseName = fc.utobj().generateTestData("TestFran");
			String centerName = fc.utobj().generateTestData("center");
			String openingDate = fc.utobj().getCurrentDateUSFormat();
			String ownerFirstName = fc.utobj().generateTestData("Testfname");
			String ownerLastName = fc.utobj().generateTestData("Testlname");

			Map<String, String> privileges = new HashMap<String, String>();
			privileges.put("Can View News", "No");
			roles_page.addRolesWithModulesPrivileges(driver, roleType, roleName, moduleName, privileges, testCaseId);

			fc.utobj().printTestStep(
					"Navigate To Admin > Franchise Location > Manage Franchise Locations > Add Franchise Location");
			AdminFranchiseLocationAddFranchiseLocationPageTest location_page = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String franchiseId = location_page.addFranchiseLocation(driver, regionName, storeType, franchiseName,
					centerName, openingDate, ownerFirstName, ownerLastName, config);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Franchise Users > Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest franchise_page = new AdminUsersManageManageFranchiseUsersPageTest();
			franchise_page.addFranchiseUser(driver, userName, password, franchiseId, roleName, emailId);

			fc.utobj().printTestStep("Logged out from ADM User");
			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Log In with newly created franchise user");
			fc.loginpage().loginWithParameter(driver, userName, password);

			fc.utobj().printTestStep("Navigate To The Hub Module");
			fc.hub().hub_common().theHubModule(driver);

			fc.utobj().printTestStep("Verify News sub module should not visible under hub");

			boolean isSubTabPresent = false;

			List<WebElement> listElement = driver.findElements(By.xpath(".//*[@id='module_intranet']/ul/li/a"));

			if (listElement.size() == 0) {
				fc.utobj().throwsException("No Sub module is present under hub module");
			} else if (listElement.size() > 0) {

				String[] arrayString = new String[listElement.size()];

				for (int i = 0; i < listElement.size(); i++) {
					arrayString[i] = listElement.get(i).getText().trim();
				}

				for (String string : arrayString) {

					if (string.equalsIgnoreCase(fc.utobj().translateString("News"))) {
						isSubTabPresent = true;
					}
				}
			}

			if (isSubTabPresent) {
				fc.utobj().throwsException("News sub module is visible under hub");
			}

			SearchUI search_page = new SearchUI(driver);
			fc.utobj().printTestStep("Verify News should not visible in Top Search");
			boolean isSearchTrue = false;
			for (int i = 0; i < 3; i++) {
				if (isSearchTrue == false) {
					fc.utobj().sendKeys(driver, search_page.topSearchField, newsItemTitle);
					Thread.sleep(2000);
					if (i == 2) {
						Thread.sleep(2000);
					}
					fc.utobj().clickEnterOnElement(driver, search_page.topSearchField);

					try {
						isSearchTrue = fc.utobj().isElementPresent(driver
								.findElements(By.xpath(".//custom[contains(text () , '" + newsItemTitle + "')]")));
					} catch (Exception e) {
						Reporter.log(e.getMessage());
						System.out.println(e.getMessage());
					}
				}
			}

			if (isSearchTrue) {
				fc.utobj().throwsException("News Item is present at top search");
			}
			fc.utobj().refresh(driver);

			fc.utobj().printTestStep("News Item should not visible in What's New Subtab");

			fc.hub().hub_common().theHubWhatsNew(driver);

			boolean isAssert = fc.utobj().assertPageSource(driver, newsItemTitle);

			if (isAssert) {
				fc.utobj().throwsException("News items is visible at Whats New Subtab");
			}

			fc.utobj().printTestStep(
					"Verify News Item should not visible search by Search All The Hub Items search filter");
			fc.utobj().sendKeys(driver, pobj.searchAllDocument, newsItemTitle);
			fc.utobj().clickElement(driver, pobj.searchItem);
			pobj.searchAllDocument.clear();

			boolean isAssertNews = fc.utobj().assertPageSource(driver, newsItemTitle);

			if (isAssertNews) {
				fc.utobj().throwsException("News Item is visible search by Search All The Hub Items search filter");
			}

			/*fc.utobj().printTestStep("Verify News Item at Admin >News Logs");
			fc.hub().hub_common().adminTheHubNewsLogsPage(driver);
			fc.utobj().showAll(driver);

			boolean isText = fc.utobj().assertPageSource(driver, newsItemTitle);
			if (isText) {
				fc.utobj().throwsException("News Item is visible at Admin > News Logs");
			}*/
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	/*
	 * TC-9915
	 */

	@Test(groups = { "thehub","hub_home"})
	@TestCase(createdOn = "2018-01-31", updatedOn = "2018-01-31", testCaseDescription = "To Verify the Franbuzz section on home page", testCaseId = "TC_Hub_Home_Franbuzz_01")
	private void franBuzzUpdate() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Hub > Franbuzz > Share Information");
			String infoText = fc.utobj().generateTestData(dataSet.get("infoText"));
			FranBuzz franbuzz=new FranBuzz();
			franbuzz.setInfoText(infoText);
			franbuzz.setShareTo("All Corporates");
			new TheHubFranBuzzPageTest().shareInformation(driver, franbuzz);

			fc.utobj().printTestStep("Enable FranBuzz Updates");
			new AdminTheHubStoriesPageTest().franBuzzUpdatesAtHomePage(driver, testCaseId, true);

			fc.utobj().printTestStep("Navigate To Hub > Home Page");
			fc.hub().hub_common().theHubHome(driver);

			fc.utobj().printTestStep("Verify The FranBuzz Update at Hub Home Page");
			boolean isInfoTextPresent = fc.utobj().assertPageSource(driver, infoText);
			if (isInfoTextPresent == false) {
				try {
					fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
							".//*[@id='franBuzzWall']//td/a[contains(text(),'View More')]"));
					isInfoTextPresent = fc.utobj().assertPageSource(driver, infoText);
				} catch (Exception e) {
					isInfoTextPresent = false;
				}
			}
			if (isInfoTextPresent == false) {
				fc.utobj().throwsException("Franbuzz update is not visible at Hub > Home Page");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/*
	 * TC-9882
	 */

	@Test(groups = { "thehub","hub_home"})
	@TestCase(createdOn = "2018-01-31", updatedOn = "2018-01-31", testCaseDescription = "To Verify Manage Widget Link Feature", testCaseId = "TC_Hub_Home_Manage_Widget_01")
	private void manageWidget() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubHomePage pobj = new TheHubHomePage(driver);
			fc.utobj().printTestStep("Navigate To Hub > Home Page");
			fc.hub().hub_common().theHubHome(driver);

			fc.utobj().clickElement(driver, pobj.manageWidgetsLink);

			List<WebElement> listElement = driver.findElements(
					By.xpath(".//*[@id='manage_widgets']//tr/td/input[@name='widgets']/following-sibling::label"));
			boolean b1[] = new boolean[listElement.size()];

			for (WebElement webElement : listElement) {
				String text = fc.utobj().getText(driver, webElement);

				if (text.equalsIgnoreCase(fc.utobj().translateString("Alerts"))) {
					b1[0] = true;
				} else if (text.equalsIgnoreCase(fc.utobj().translateString("EPoll"))) {
					b1[1] = true;
				} else if (text.equalsIgnoreCase(fc.utobj().translateString("Event Calendar"))) {
					b1[2] = true;
				} else if (text.equalsIgnoreCase(fc.utobj().translateString("Library Documents"))) {
					b1[3] = true;
				} else if (text.equalsIgnoreCase(fc.utobj().translateString("Messages"))) {
					b1[4] = true;
				} else if (text.equalsIgnoreCase(fc.utobj().translateString("New Since Last Login"))) {
					b1[5] = true;
				} else if (text.equalsIgnoreCase(fc.utobj().translateString("Tickets"))) {
					b1[6] = true;
				} else if (text.equalsIgnoreCase(fc.utobj().translateString("Today's Events"))) {
					b1[7] = true;
				}
			}

			if (b1[0] == false) {
				fc.utobj().throwsException("Alerts item is not present in widget");
			}
			if (b1[1] == false) {
				fc.utobj().throwsException("EPoll item is not present in widget");
			}
			if (b1[2] == false) {
				fc.utobj().throwsException("Event Calendar item is not present in widget");
			}
			if (b1[3] == false) {
				fc.utobj().throwsException("Library Documents item is not present in widget");
			}
			if (b1[4] == false) {
				fc.utobj().throwsException("Messages item is not present in widget");
			}
			if (b1[5] == false) {
				fc.utobj().throwsException("New Since Last Login item is not present in widget");
			}
			if (b1[6] == false) {
				fc.utobj().throwsException("Tickets item is not present in widget");
			}
			if (b1[7] == false) {
				fc.utobj().throwsException("Today's Events item is not present in widget");
			}

			fc.utobj().printTestStep("Verify Library user can hide library section from the home page");
			
			TheHubHomePage hubhome_Page=new TheHubHomePage(driver);
			if (fc.utobj().isSelected(driver, hubhome_Page.libraryDocumentsc)) {
				fc.utobj().clickElement(driver, hubhome_Page.libraryDocumentsc);
			}
			fc.utobj().clickElement(driver, hubhome_Page.saveBtn);
			
			boolean isElementPresent=false;
			
			try {
				WebElement element=fc.utobj().getElementByXpath(driver, ".//div[@id ='libraryDocuments' and @style='display:']");
				isElementPresent=element.isDisplayed();
			} catch (Exception e) {
				isElementPresent=false;
			}
			if (isElementPresent) {
				fc.utobj().throwsException("library section is visible on the home screen though removed from manage widget");
			}
			
			fc.utobj().clickElement(driver, pobj.manageWidgetsLink);
			if (!fc.utobj().isSelected(driver, hubhome_Page.libraryDocumentsc)) {
				fc.utobj().clickElement(driver, hubhome_Page.libraryDocumentsc);
			}
			fc.utobj().clickElement(driver, hubhome_Page.saveBtn);
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/*
	 * TC-9895
	 */

	@Test(groups = { "thehub","hub_home"})
	@TestCase(createdOn = "2018-01-31", updatedOn = "2018-02-01", testCaseDescription = "To Verify the Today's Events widgets on home page", testCaseId = "TC_Hub_Home_Today_Event_01")
	public void verifyEvents() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubHomePage pobj = new TheHubHomePage(driver);
			String emailId = "hubautomation@staffex.com";
			String divPassword = "t0n1ght@123";
			String divUserName = fc.utobj().generateTestData("TestDivuser");
			String divisionName = fc.utobj().generateTestData("TestDiv");

			fc.utobj().printTestStep("Add Division");
			AdminDivisionAddDivisionPageTest p1 = new AdminDivisionAddDivisionPageTest();
			p1.addDivision(driver, divisionName);

			fc.utobj().printTestStep(
					"Navigate To Admin > Users > Manage Divisional Users > Add  Divisional User with added role");
			AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest divisional_user = new AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest();
			divisional_user.addDivisionalUser(driver, divUserName, divPassword, divisionName, emailId);

			fc.utobj().printTestStep(
					"Navigate To Admin > Users > Manage Corporate Users > Add  Corporate User with added role");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporate_user = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = corporate_user.createDefaultUser(driver, corpUser);

			dataSet.put("customUserName", corpUser.getuserFullName());
			fc.utobj().printTestStep("Navigate To The Hub > Calender Page");
			TheHubCalendarPageTest calender_page = new TheHubCalendarPageTest();
			fc.utobj().printTestStep("Create An Event");
			String option = "Event";
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			calender_page.createEvent(driver, option, subject, description, dataSet);

			fc.utobj().printTestStep("Create An Appointment");
			String appSubject = fc.utobj().generateTestData("Testapp");
			String appDescription = fc.utobj().generateTestData("Testappdes");
			calender_page.createAppointment(driver, appSubject, appDescription);

			fc.utobj().printTestStep("Create An Meeting");
			String meeSubject = fc.utobj().generateTestData("Testmee");
			String meeDescription = fc.utobj().generateTestData("Testdes");
			calender_page.createMeeting(driver, meeSubject, meeDescription, dataSet);

			fc.home_page().logout(driver);
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());

			fc.utobj().printTestStep("Navigate To The Hub > Home Page");
			fc.hub().hub_common().theHubHome(driver);
			/*
			 * TC-9900
			 */

			fc.utobj().clickElement(driver, pobj.manageWidgetsLink);
			if (!fc.utobj().isSelected(driver, pobj.todaysEventc)) {
				fc.utobj().clickElement(driver, pobj.todaysEventc);
			}
			if (!fc.utobj().isSelected(driver, pobj.eventCalenderc)) {
				fc.utobj().clickElement(driver, pobj.eventCalenderc);
			}
			fc.utobj().clickElement(driver, pobj.saveBtn);
			try {
				fc.utobj().clickElement(driver, pobj.cancelBtn);
			} catch (Exception e) {

			}
			fc.utobj().printTestStep(
					"Verify Event calendar widget on home page should be displayed with selected current date");

			String currentDate = fc.utobj().getCurrentDateUserDefineTimeZoneUSFormat("GMT-5:00");

			currentDate = currentDate.substring(3, currentDate.indexOf("/") + 3);
			if (currentDate.startsWith("0")) {
				currentDate = currentDate.substring(1, 2);
			}
			String selectedDate = fc.utobj().getText(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='homeCalendarDiv']/form//a/span"));

			if (!currentDate.contains(selectedDate)) {
				fc.utobj().throwsException("current date is not selected ");
			}

			fc.utobj().printTestStep("Verify Today Event At Hub Home Page");

			boolean isEventPresent = fc.utobj().assertPageSource(driver, subject);
			
			boolean isMoreLinkPresent=false;
			if (!isEventPresent) {
				try {
					fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
							".//*[@id='todaysEvent']//td//a/span[contains(text(),'More')]"));
					isMoreLinkPresent=true;
				} catch (Exception e) {
					isMoreLinkPresent=false;
				}
			}
			if (isMoreLinkPresent) {
				isEventPresent = fc.utobj().assertPageSource(driver, subject);
			}
			
			if (!isEventPresent) {
				fc.utobj().throwsException("Event is not present at Home Page");
			}

			if (isMoreLinkPresent) {
				fc.utobj().printTestStep("Navigate To The Hub > Home Page");
				fc.hub().hub_common().theHubHome(driver);
			}
			
			fc.utobj().printTestStep("Verify Meetting At Hub Home Page");
			boolean isMeetingPresent = fc.utobj().assertPageSource(driver, meeSubject);
			
			//check for more link
			isMoreLinkPresent=false;
			
			if (!isMeetingPresent) {
				try {
					fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
							".//*[@id='todaysEvent']//td//a/span[contains(text(),'More')]"));
					isMoreLinkPresent=true;
				} catch (Exception e) {
					isMoreLinkPresent=false;
				}
			}
			
			if (isMoreLinkPresent) {
				isMeetingPresent = fc.utobj().assertPageSource(driver, meeSubject);
			}
			if (isMeetingPresent = false) {
				fc.utobj().throwsException("Meeting is not present at Home Page");
			}

			if (isMoreLinkPresent) {
				fc.utobj().printTestStep("Navigate To The Hub > Home Page");
				fc.hub().hub_common().theHubHome(driver);
			}

			fc.utobj().printTestStep("Verify Appointment At Hub Home Page");

			boolean isAppointmentPresent = fc.utobj().assertPageSource(driver, appSubject);
			
			isMoreLinkPresent=false;
			if (!isAppointmentPresent) {
				try {
					fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
							".//*[@id='todaysEvent']//td//a/span[contains(text(),'More')]"));
					isMoreLinkPresent=true;
				} catch (Exception e) {
					isMoreLinkPresent=false;
				}
			}
			
			if (isMoreLinkPresent) {
				isAppointmentPresent = fc.utobj().assertPageSource(driver, appSubject);
			}
			if (isAppointmentPresent = false) {
				fc.utobj().throwsException("Appointment is not present at Home Page");
			}

			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login With Newly Created Divisional User");
			fc.loginpage().loginWithParameter(driver, divUserName, divPassword);

			fc.utobj().printTestStep("Verify Today Event At Hub Home Page");
			fc.hub().hub_common().theHubHome(driver);

			boolean isEventPresentDiv = fc.utobj().assertPageSource(driver, subject);
			
			isMoreLinkPresent=false;
			
			if (!isEventPresentDiv) {
				try {
					fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
							".//*[@id='todaysEvent']//td//a/span[contains(text(),'More')]"));
					isMoreLinkPresent=true;
				} catch (Exception e) {
					isMoreLinkPresent=false;
				}
			}
			if (isMoreLinkPresent) {
				isEventPresentDiv = fc.utobj().assertPageSource(driver, subject);
			}
			
			if (isEventPresentDiv) {
				fc.utobj().throwsException("Event is present at Home Page logged in by Divisional User");
			}

			if (isMoreLinkPresent) {
				fc.utobj().printTestStep("Navigate To The Hub > Home Page");
				fc.hub().hub_common().theHubHome(driver);
			}
			fc.utobj().printTestStep("Verify Meeting At Hub Home Page");

			boolean isMeetingPresentDiv = fc.utobj().assertPageSource(driver, meeSubject);
			isMoreLinkPresent=false;
			
			if (!isMeetingPresentDiv) {
				try {
					fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
							".//*[@id='todaysEvent']//td//a/span[contains(text(),'More')]"));
					isMoreLinkPresent=true;
				} catch (Exception e) {
					isMoreLinkPresent=false;
				}
			}
			
			if (isMoreLinkPresent) {
				isMeetingPresentDiv = fc.utobj().assertPageSource(driver, meeSubject);
			}
			if (isMeetingPresentDiv) {
				fc.utobj().throwsException("Meeting is present at Home Page logged in by Divisional User");
			}

			if (isMoreLinkPresent) {
				fc.utobj().printTestStep("Navigate To The Hub > Home Page");
				fc.hub().hub_common().theHubHome(driver);
			}
			
			fc.utobj().printTestStep("Verify Appointment At Hub Home Page");

			boolean isAppointmentPresentDiv = fc.utobj().assertPageSource(driver, appSubject);
			isMoreLinkPresent=false;
			
			if (!isAppointmentPresentDiv) {
				try {
					fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
							".//*[@id='todaysEvent']//td//a/span[contains(text(),'More')]"));
					isMoreLinkPresent=true;
				} catch (Exception e) {
					isMoreLinkPresent=false;
				}
			}
			if (isMoreLinkPresent) {
				isAppointmentPresentDiv = fc.utobj().assertPageSource(driver, appSubject);
			}
			if (isAppointmentPresentDiv = false) {
				fc.utobj().throwsException("Appointment is not present at Home Page");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/*
	 * TC-9888
	 */
	@Test(groups = { "thehub" ,"hub_home"})
	@TestCase(createdOn = "2018-02-01", updatedOn = "2018-02-01", testCaseDescription = "To Verify the Ask Corporate section through Franchisee, Regional users", testCaseId = "TC_Hub_Home_Ask_Corporate_01")
	public void verifyAskCorporate() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate to Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "hubautomation@staffex.com";

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest cu = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			cu.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			String userNameDeprat = corpUser.getuserFullName();

			fc.utobj().printTestStep("Navigate To Admin > Support > Manage Department");
			fc.utobj().printTestStep("Add Department");
			String departmentName = fc.utobj().generateTestData(dataSet.get("departmentName"));
			AdminSupportManageDepartmentPageTest departmentPage = new AdminSupportManageDepartmentPageTest();
			departmentPage.addDepartments(driver, departmentName, userNameDeprat);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUserPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser2 = new CorporateUser();
			corpUser2 = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser2);
			corpUser2.setEmail(emailId);
			corpUser2 = corporateUserPage.createDefaultUser(driver, corpUser2);

			fc.utobj().printTestStep("Add Franchise Location");
			String regionName = "Test127Region".concat(fc.utobj().generateRandomNumber());
			String storeType = "Test127StoreType".concat(fc.utobj().generateRandomNumber());
			String franchiseId = "Test127FranId".concat(fc.utobj().generateRandomNumber());

			AdminFranchiseLocationAddFranchiseLocationPageTest addFranchiseLocation = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String firstName = fc.utobj().generateTestData("Testfirstname");
			franchiseId = addFranchiseLocation.addFranchiseLocation_AllWithFName(driver, franchiseId, regionName,
					storeType, firstName);

			fc.utobj().printTestStep("Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest addFranchiseUser = new AdminUsersManageManageFranchiseUsersPageTest();
			String roleName = "Default Franchise Role";
			String userName = fc.utobj().generateTestData("Test");
			addFranchiseUser.addFranchiseUser(driver, userName, corpUser.getUserName(), franchiseId, roleName, emailId);

			fc.utobj().printTestStep("Navigate To Admin > Add Area/Region");
			fc.utobj().printTestStep("Add Region");

			AdminUsersManageRegionalUsersAddRegionalUserPageTest regionalUserPage = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
			String regUserName = fc.utobj().generateTestData("Testreguser");
			regionalUserPage.addRegionalUser(driver, regUserName, corpUser.getUserName(), regionName, emailId);

			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login By Newly Created Regional User");
			fc.loginpage().loginWithParameter(driver, regUserName, corpUser.getUserName());

			TheHubHomePage pobj = new TheHubHomePage(driver);

			fc.utobj().printTestStep("Navigate To Hub > Home Page");
			fc.hub().hub_common().theHubHome(driver);

			fc.utobj().clickElement(driver, pobj.manageWidgetsLink);
			if (!fc.utobj().isSelected(driver, pobj.askCorporatec)) {
				fc.utobj().clickElement(driver, pobj.askCorporatec);
			}
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().selectDropDown(driver, pobj.departmentId, departmentName);
			String askCorSubject = fc.utobj().generateTestData("Testticketsub");
			fc.utobj().sendKeys(driver, pobj.askCorporateSubject, askCorSubject);
			String askCordesc = fc.utobj().generateTestData("Testticketdes");
			fc.utobj().sendKeys(driver, pobj.askCorporateDescription, askCordesc);
			fc.utobj().clickElement(driver, pobj.addButton);
			SupportTicketsPage ticket_page = new SupportTicketsPage(driver);
			fc.utobj().clickElement(driver, ticket_page.okBtn);

			fc.utobj().printTestStep("Verify Ask Corporate Tikcet");
			fc.support().support_common().supportAskCorporate(driver);

			fc.utobj().clickElement(driver, ticket_page.ticketTab);

			boolean isTicketPresent = fc.utobj().assertPageSource(driver, askCorSubject);
			if (isTicketPresent == false) {
				fc.utobj().throwsException("Support Ticket is Not Created");
			}

			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login By Newly Created Franchise User");
			fc.loginpage().loginWithParameter(driver, userName, corpUser.getUserName());

			fc.utobj().printTestStep("Navigate To Hub > Home Page");
			fc.hub().hub_common().theHubHome(driver);

			fc.utobj().clickElement(driver, pobj.manageWidgetsLink);
			if (!fc.utobj().isSelected(driver, pobj.askCorporatec)) {
				fc.utobj().clickElement(driver, pobj.askCorporatec);
			}
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().selectDropDown(driver, pobj.departmentId, departmentName);
			String askCorSubjectDiv = fc.utobj().generateTestData("Testticketsub");
			fc.utobj().sendKeys(driver, pobj.askCorporateSubject, askCorSubjectDiv);
			String askCordescDiv = fc.utobj().generateTestData("Testticketdes");
			fc.utobj().sendKeys(driver, pobj.askCorporateDescription, askCordescDiv);
			fc.utobj().clickElement(driver, pobj.addButton);
			fc.utobj().clickElement(driver, ticket_page.okBtn);

			fc.utobj().printTestStep("Verify Ask Corporate Tikcet");
			fc.support().support_common().supportAskCorporate(driver);

			fc.utobj().clickElement(driver, ticket_page.ticketTab);

			boolean isTicketPresentDiv = fc.utobj().assertPageSource(driver, askCorSubjectDiv);
			if (isTicketPresentDiv == false) {
				fc.utobj().throwsException("Support Ticket is Not Created By Divisional User");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	/*
	 * 
	 */

	@Test(groups = { "thehub","hub_home" })
	@TestCase(createdOn = "2018-02-01", updatedOn = "2018-02-01", testCaseDescription = "To Verify Ticket Widget feature on home page ", testCaseId = "TC_Hub_Home_Tickets_01")
	public void VerifyTickets() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			String emailId = "hubautomation@staffex.com";

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUserPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = corporateUserPage.createDefaultUser(driver, corpUser);
			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login By Newly Created Corporate User");
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());

			fc.utobj().printTestStep("Navigate To Hub > Home Page");
			fc.hub().hub_common().theHubHome(driver);
			TheHubHomePage pobj = new TheHubHomePage(driver);

			fc.utobj().clickElement(driver, pobj.manageWidgetsLink);
			if (!fc.utobj().isSelected(driver,pobj.troubleTicketsHomePagec)) {
				fc.utobj().clickElement(driver, pobj.troubleTicketsHomePagec);
			}
			fc.utobj().clickElement(driver, pobj.saveBtn);

			boolean isTicketSectionPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[@id='troubleTicketsHomePageHeading']");
			if (isTicketSectionPresent == false) {
				fc.utobj().throwsException("Ticket Section is Present for Corporate User");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/*
	 * TC-9914
	 */

	@Test(groups = { "thehub" ,"hub_home"})
	@TestCase(createdOn = "2018-02-01", updatedOn = "2018-02-01", testCaseDescription = "To verify the the authenticated SSO links feature", testCaseId = "TC_Hub_Home_SSO_01")
	public void modifySSO() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			String emailId = "hubautomation@staffex.com";

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUserPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = corporateUserPage.createDefaultUser(driver, corpUser);

			fc.utobj().printTestStep("Go to Admin > Single Sign On");
			String linkTitle = fc.utobj().generateTestData("Testlnktitle");
			String linkdescription = fc.utobj().generateTestData("Testlnkdescription");
			String fileName = "xperia_z_ultra-wallpaper-1366x768.jpg";
			String targetUrl = "https://www.google.com";
			String urlTocken1 = fc.utobj().generateTestData("urlTocken1");
			String urlTocken2 = fc.utobj().generateTestData("urlTocken2");

			new AdminConfigurationConfigureSingleSignOnPageTest().LinkSSO(driver, linkTitle, linkdescription, config,
					fileName, targetUrl, urlTocken1, urlTocken2, corpUser.getuserFullName());

			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login By Newly Created Corporate User");
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());

			fc.utobj().printTestStep("Navigate To Hub > Home Page");
			fc.hub().hub_common().theHubHome(driver);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='nullContent']//a[contains(text(),'" + linkdescription + "')]/parent::td/a/img"));
			boolean isToken1Present = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text(),'" + urlTocken1 + "')]/parent::tr/td/input");
			boolean isToken2Present = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text(),'" + urlTocken2 + "')]/parent::tr/td/input");
			boolean isRelatedLinkPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[@id='siteMainTable']//*[contains(text(),'Related Links')]");

			if (isToken1Present == false) {
				fc.utobj().throwsException("First Token text box is not present");
			}
			if (isToken2Present == false) {
				fc.utobj().throwsException("Second Token text box is not present");
			}
			if (isRelatedLinkPresent == false) {
				fc.utobj().throwsException("Not redirected to Related Link Page");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(groups = {"thehub","hub_download","hub_home"})
	@TestCase(createdOn = "2018-05-31", updatedOn = "2018-05-31", testCaseDescription = "Verify download the document from the library section on the Hub_Home_page", testCaseId = "TC_Hub_Home_Library_Document_Download_01")
	private void verifyDownloadRecentlyDocument() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			AdminTheHubLibraryPageTest libraryPage = new AdminTheHubLibraryPageTest();
			TheHubHomePage pobj=new TheHubHomePage(driver);
			
			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";
			
			Library library=new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			libraryPage.addFolder(driver,library);
			fc.utobj().printTestStep("Add Document In Folder");

			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String currentDate = fc.utobj().getCurrentDateUSFormat();
			String afterDate = dataSet.get("afterDate");
			int val = Integer.parseInt(afterDate);
			String futureDate = fc.utobj().getFutureDateUSFormat(val);
			
			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setCurrentDate(currentDate);
			library.setFutureDate(futureDate);
			library.setDocumentSubType(dataSet.get("documentSubType"));
			library.setFile(dataSet.get("fileName"));
			library.setDocumentType(dataSet.get("documentType"));
			library.setRecommendedDoc(true);
			libraryPage.addDocument(driver,library);

			fc.utobj().printTestStep("Verify Recently Uploaded should be downloaded from Hub > Home Page");
			fc.hub().hub_common().theHubHome(driver);

			TheHubHomePage hubhome_Page = new TheHubHomePage(driver);
			fc.utobj().clickElement(driver, hubhome_Page.manageWidgetsLink);
			if (!fc.utobj().isSelected(driver, hubhome_Page.libraryDocumentsc)) {
				fc.utobj().clickElement(driver, hubhome_Page.libraryDocumentsc);
			}
			fc.utobj().clickElement(driver, hubhome_Page.saveBtn);
			
			fc.utobj().clickLink(driver, documentTitle);
			fc.utobj().printTestStep("Verify The downloaded Recently Uploaded file");

			boolean isFilePresent=fc.utobj().isFileFound(new File(FranconnectUtil.config.get("downloadFilepath")), library.getFile());
			if (!isFilePresent) {
				fc.utobj().throwsException("Not able to able to download recently uploaded library document");
			}
			hub_common_method.deleteDownloadedFile(library);
			
			fc.utobj().printTestStep("Again Download The file to make this most downloaded file");
			fc.utobj().clickLink(driver, documentTitle);
			hub_common_method.deleteDownloadedFile(library);
			fc.utobj().clickLink(driver, documentTitle);
			hub_common_method.deleteDownloadedFile(library);
			
			/*fc.utobj().printTestStep("Verify The downloaded Recomnded file");
			fc.utobj().clickLink(driver, "Recommended");
			fc.utobj().sleep(1000);
			fc.utobj().clickLink(driver, documentTitle);
			
			boolean isFilePresentRecomnded=fc.utobj().isFileFound(new File(FranconnectUtil.config.get("downloadFilepath")), library.getFile());
			if (!isFilePresentRecomnded) {
				fc.utobj().throwsException("Not able to able to download recomnded library document");
			}
			hub_common_method.deleteDownloadedFile(library);*/
			
			fc.utobj().refresh(driver);
			
			fc.utobj().printTestStep("Verify The downloaded Most Downloaded file");
			//fc.utobj().clickLink(driver, "Most Downloaded");
			fc.utobj().clickElement(driver, pobj.mostDownloadedLink);
			fc.utobj().sleep(2000);
			fc.utobj().clickElement(driver, ".//*[@id='downloadedDocumentsDisplay']//a[.='"+documentTitle+"']");
			
			boolean isFilePresentMost=fc.utobj().isFileFound(new File(FranconnectUtil.config.get("downloadFilepath")), library.getFile());
			if (!isFilePresentMost) {
				fc.utobj().throwsException("Not able to able to download Most Downloade library document");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
}
