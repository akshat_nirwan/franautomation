package com.builds.test.thehub;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminAccessControlSearchedLogsPageTest;
import com.builds.test.admin.AdminAreaRegionAddAreaRegionPageTest;
import com.builds.test.admin.AdminFranchiseLocationAddFranchiseLocationPageTest;
import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.admin.AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest;
import com.builds.test.admin.AdminUsersManageManageFranchiseUsersPageTest;
import com.builds.test.admin.AdminUsersManageRegionalUsersAddRegionalUserPageTest;
import com.builds.test.admin.AdminUsersRolesAddNewRolePageTest;
import com.builds.test.admin.OptionsConfigureQuickLinkPageTest;
import com.builds.test.admin.SearchedLogs;
import com.builds.test.common.CorporateUser;
import com.builds.uimaps.common.CommonUI;
import com.builds.uimaps.fs.SearchUI;
import com.builds.uimaps.thehub.AdminTheHubLibraryPage;
import com.builds.uimaps.thehub.TheHubLibraryPage;
import com.builds.uimaps.thehub.TheHubNewsPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class TheHubLibraryPageTest {
	FranconnectUtil fc = new FranconnectUtil();
	HubCommonMethods hub_common_method = new HubCommonMethods();

	@Test(groups = { "thehub", "hub_library" ,"hub_download"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Verify The Add Document At The Hub > Library", testCaseId = "TC_218_Verify_Recently_Uploaded")
	private void verifyRecentlyUploaded() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			SearchUI search_page = new SearchUI(driver);
			AdminTheHubLibraryPageTest libraryPage = new AdminTheHubLibraryPageTest();

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "Regional";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			libraryPage.addFolder(driver, library);

			fc.utobj().printTestStep("Add Document In Folder");
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String currentDate = fc.utobj().getCurrentDateUSFormat();
			String afterDate = dataSet.get("afterDate");
			int val = Integer.parseInt(afterDate);
			String futureDate = fc.utobj().getFutureDateUSFormat(val);

			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setCurrentDate(currentDate);
			library.setFutureDate(futureDate);
			library.setLinkUrl(dataSet.get("linkUrl"));
			library.setDocumentType(dataSet.get("documentType"));
			library.setRecommendedDoc(true);
			libraryPage.addDocument(driver, library);

			boolean isSearchTrue = false;
			for (int i = 0; i < 3; i++) {
				if (isSearchTrue == false) {
					fc.utobj().sendKeys(driver, search_page.topSearchField, documentTitle);
					Thread.sleep(2000);
					if (i == 2) {
						Thread.sleep(2000);
					}
					fc.utobj().clickEnterOnElement(driver, search_page.topSearchField);

					try {
						isSearchTrue = fc.utobj().isElementPresent(driver
								.findElements(By.xpath(".//custom[contains(text () , '" + documentTitle + "')]")));
					} catch (Exception e) {
						Reporter.log(e.getMessage());
					}
				}
			}

			if (isSearchTrue == false) {
				fc.utobj().throwsException("was not able to search Document By Top Search");
			}
			fc.utobj().refresh(driver);

			fc.utobj().printTestStep("Navigate To The Hub > Library > Home");
			fc.hub().hub_common().theHubLibrarySubModule(driver);

			fc.utobj().printTestStep("Verify Recently Uploaded");
			fc.utobj().isTextDisplayed(driver, documentTitle, "was not able to verify Recent Uploaded document");

			fc.utobj().printTestStep("Verify All the label is present at libray page");

			boolean isHomeTabPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='Home' and @class='catelist bText12bl']");
			if (!isHomeTabPresent) {
				fc.utobj().throwsException("Home label is not peresent");
			}

			boolean isIndexTabPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='Index' and @class='catelist bText12bl']");
			if (!isIndexTabPresent) {
				fc.utobj().throwsException("Index label is not peresent");
			}

			boolean isResourceTabPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[.='Resource Library']");
			if (!isResourceTabPresent) {
				fc.utobj().throwsException("Resource Library label is not present");
			}

			boolean isRecentlyUploadedTabPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[@class='top-space botBorder' and contains(text(),'Recently Uploaded')]");
			if (!isRecentlyUploadedTabPresent) {
				fc.utobj().throwsException("Recently Uploaded label is not peresent");
			}

			boolean isRecommendedTabPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[@class='top-space botBorder' and contains(text(),'Recommended')]");
			if (!isRecommendedTabPresent) {
				fc.utobj().throwsException("Recommended label is not peresent");
			}

			boolean isMostDownloadedTabPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[@class='top-space botBorder' and contains(text(),'Most Downloaded')]");
			if (!isMostDownloadedTabPresent) {
				fc.utobj().throwsException("Most Downloaded label is not peresent");
			}

			boolean isSearchDocFieldPresent = fc.utobj().verifyCaseID(driver, "searchAllDocument");
			if (!isSearchDocFieldPresent) {
				fc.utobj().throwsException("Search field is not present");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library","hubf1121"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Verify The Report Of Document At The Hub > Library > Index [Detail View]", testCaseId = "TC_219_Report_Document")
	private void reportDocumentActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			TheHubLibraryPage pobj = new TheHubLibraryPage(driver);
			AdminTheHubLibraryPageTest libraryPage = new AdminTheHubLibraryPageTest();

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			libraryPage.addFolder(driver, library);

			fc.utobj().printTestStep("Add Document In Folder");

			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String currentDate = fc.utobj().getCurrentDateUSFormat();
			String afterDate = dataSet.get("afterDate");
			int val = Integer.parseInt(afterDate);
			String futureDate = fc.utobj().getFutureDateUSFormat(val);

			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setCurrentDate(currentDate);
			library.setFutureDate(futureDate);
			library.setDocumentSubType(dataSet.get("documentSubType"));
			library.setFile(dataSet.get("fileName"));
			library.setDocumentType(dataSet.get("documentType"));
			library.setRecommendedDoc(false);
			libraryPage.addDocument(driver, library);

			fc.utobj().printTestStep("Navigate To The Hub  > Library > Index");
			fc.hub().hub_common().theHubLibrarySubModule(driver);
			fc.utobj().clickElement(driver, pobj.indexTab);

			fc.utobj().printTestStep("Verify The Detail View Of Document");
			fc.utobj().clickElement(driver, pobj.detailView);
			fc.utobj().clickLink(driver, documentTitle);

			if (!fc.utobj()
					.getElementByXpath(driver, ".//*[@id='DtlsInner']//a[contains(text () ,'" + documentTitle + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document Title At Detail View Page");
			}
			if (!fc.utobj()
					.getElementByXpath(driver, ".//*[@id='DtlsInner']//*[contains(text () ,'" + briefSummary + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document's Brief Summary");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//*[@id='DtlsInner']//*[contains(text () ,'" + documentTitle
					+ "')]/..//following-sibling::td[.='" + folderName + "']").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Parent Directory");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//*[@id='DtlsInner']//*[contains(text () ,'" + documentTitle
					+ "')]/..//following-sibling::td[.='FranConnect Administrator']").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Parent Directory");
			}

			fc.utobj().printTestStep("Verify Report of Recomnded Document");
			libraryPage.actionImgIconDetailsView(driver, documentTitle, "Report");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().isTextDisplayed(driver, documentTitle, "Was not able to verify document Title");

			if (!fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , 'Brief Summary :')]/following-sibling::td[.='" + briefSummary + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Brief Summary :");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Uploaded By :')]/following-sibling::td[.='FranConnect Administrator']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Uploaded By :");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Uploaded On :')]/following-sibling::td[.='" + currentDate + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Uploaded Date :");
			}

			if (!fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , 'Start Display Date :')]/following-sibling::td[.='" + currentDate + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Start Display Date :");
			}

			if (!fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , 'Expiration Date :')]/following-sibling::td[.='" + futureDate + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Start Display Date :");
			}
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			//verify Report option from document Comment link from Index > List View
			fc.utobj().printTestStep("Verify Report option from document Action Menu at Index > List View");
			
			fc.utobj().clickElement(driver, pobj.listView);
			libraryPage.actionImgIconListView(driver, documentTitle, "Report");
			
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().isTextDisplayed(driver, documentTitle, "Was not able to verify document Title");

			if (!fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , 'Brief Summary :')]/following-sibling::td[.='" + briefSummary + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Brief Summary :");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Uploaded By :')]/following-sibling::td[.='FranConnect Administrator']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Uploaded By :");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Uploaded On :')]/following-sibling::td[.='" + currentDate + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Uploaded Date :");
			}

			if (!fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , 'Start Display Date :')]/following-sibling::td[.='" + currentDate + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Start Display Date :");
			}

			if (!fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , 'Expiration Date :')]/following-sibling::td[.='" + futureDate + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Start Display Date :");
			}
			
			fc.utobj().printTestStep("Verify the document download details");
			boolean isDocDownloadDetailsPresent=fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text(),'User Name')]/parent::*/following-sibling::tr/td[contains(text(),'FranConnect Administrator')]/preceding-sibling::td[contains(text(),'1')]");
			if (!isDocDownloadDetailsPresent) {
				fc.utobj().throwsException("Document Details is not present");
			}
			
			boolean isPrintButtonPresent=fc.utobj().isElementPresent(driver, pobj.adminLibraryPage(driver).printButton);
			if (!isPrintButtonPresent) {
				fc.utobj().throwsException("Print button is not present at Report cbox");
			}
			boolean isExportAsExcelButtonPresent=fc.utobj().isElementPresent(driver, pobj.adminLibraryPage(driver).exportAsExcelButton);
			if (!isExportAsExcelButtonPresent) {
				fc.utobj().throwsException("Export As Excel Button is not present at Report cbox");
			}
			boolean isCloseButtonPresent=fc.utobj().isElementPresent(driver, pobj.closeBtn);
			if (!isCloseButtonPresent) {
				fc.utobj().throwsException("Close Button is not present at Report cbox");
			}
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library","hubf1121"})
	@TestCase(createdOn = "2018-06-12", updatedOn = "2018-06-12", testCaseDescription = "Verify The Added Document At Admin > The Hub > Library", testCaseId = "TC_The_Hub_Add_Document_01")
	private void addDocumentWithHtmlPageHub() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubLibraryPage pobj=new TheHubLibraryPage(driver);
			AdminTheHubLibraryPageTest admin_library = new AdminTheHubLibraryPageTest();
			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			admin_library.addFolder(driver, library);

			fc.utobj().printTestStep("Add Document");
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setDocumentType(dataSet.get("documentType"));
			library.setEditorText(dataSet.get("editorText"));
			library.setRecommendedDoc(false);
			addFolderDocument(driver, library);

			fc.utobj().printTestStep("Verify The Added Document");
			fc.utobj().isTextDisplayed(driver, library.getDocumentTitle(), "was not able to verify document title after search");
			fc.utobj().isTextDisplayed(driver, library.getBriefSummary(),
					"was not able to verify document's brief summary after search");

			fc.utobj().printTestStep("Verify The Entered Html Text");
			fc.utobj().clickLink(driver, library.getDocumentTitle());

			boolean isEditorTextPresent = fc.utobj().assertPageSource(driver, library.getEditorText());
			if (!isEditorTextPresent) {
				fc.utobj().throwsException("Entered Html Text is not present");
			}
			
			//TC-64814 : Verify On downloading html from archived junk characters are coming
			String oldDocumentTitle=library.getDocumentTitle();
			fc.utobj().printTestStep("Modify The Added Html Document");
			fc.utobj().clickLink(driver, library.getFolderName());
			hub_common_method.sortByRecentlyUploadedDocuments(driver);
			admin_library.actionImgIconListView(driver, documentTitle, "Modify");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String newDocTitle=fc.utobj().generateTestData("TestNewDocTitle"); 
			library.setDocumentTitle(newDocTitle);
			fc.utobj().sendKeys(driver, pobj.documentTitle, library.getDocumentTitle());
			fc.utobj().clickElement(driver, pobj.documentType.get(1));
			String linkUrl="https://in.yahoo.com/";
			fc.utobj().sendKeys(driver, pobj.linkUrl, linkUrl);
			fc.commonMethods().Click_Save_Input_ByValue(driver);
			fc.commonMethods().Click_Close_Input_ByValue(driver);
			fc.utobj().switchFrameToDefault(driver);
			
			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library");
			fc.hub().hub_common().adminTheHubLibraryPage(driver);
			fc.utobj().printTestStep("Click Over Archived Document Tab");
			fc.utobj().clickElement(driver, pobj.adminLibraryPage(driver).archivedDocumentsTab);
			hub_common_method.sortByRecentlyUploadedDocuments(driver);
			fc.utobj().clickLink(driver, oldDocumentTitle);
			
			if(!fc.utobj().assertPageSource(driver, library.getEditorText())){
				fc.utobj().throwsException("Html Editor text is not present at Archived Documents Tab List View");
			}
			
			String parentWindow=driver.getWindowHandle();
			fc.utobj().clickElement(driver, pobj.adminLibraryPage(driver).archivedDocumentsTab);
			hub_common_method.sortByRecentlyUploadedDocuments(driver);
			
			fc.utobj().printTestStep("Verify The Html Document after clicking over Document url from properties cbox");
			admin_library.actionImgIconListView(driver, oldDocumentTitle, "Document Properties");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver,
					".//td[contains(text () , 'Document URL')]/following-sibling::td/a[contains(@href,'"
							+ FranconnectUtil.config.get("buildUrl") + "')]");

			fc.utobj().printTestStep("Verify Html Document window should be opened from Archived > List View");
			boolean isHtmlEditortTextPresentArchD = false;
			Set<String> windowsHandlesArchD = driver.getWindowHandles();
			for (String window : windowsHandlesArchD) {
				if (!window.equalsIgnoreCase(parentWindow)) {
					driver.switchTo().window(window);
					if (driver.getCurrentUrl().contains("htmlPageView.jsp")) {
						isHtmlEditortTextPresentArchD = fc.utobj().assertPageSource(driver, library.getEditorText());
						driver.close();
						break;
					}
				}
			}
			driver.switchTo().window(parentWindow);
			if (!isHtmlEditortTextPresentArchD) {
				fc.utobj().throwsException("HTML documents is not visible");
			}
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library" ,"hub_download","hubf1121"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Verify The Modify Document At The Hub > Library > Index [Detail View]", testCaseId = "TC_220_Modify_Document")
	private void modifyDocumentActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			TheHubLibraryPage pobj = new TheHubLibraryPage(driver);
			AdminTheHubLibraryPageTest libraryPage = new AdminTheHubLibraryPageTest();

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			libraryPage.addFolder(driver, library);

			fc.utobj().printTestStep("Add Document");
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String currentDate = fc.utobj().getCurrentDateUSFormat();
			String afterDate = dataSet.get("afterDate");
			int val = Integer.parseInt(afterDate);
			String futureDate = fc.utobj().getFutureDateUSFormat(val);

			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setCurrentDate(currentDate);
			library.setFutureDate(futureDate);
			library.setDocumentSubType(dataSet.get("documentSubType"));
			library.setFile(dataSet.get("fileName"));
			library.setDocumentType(dataSet.get("documentType"));
			library.setRecommendedDoc(false);
			libraryPage.addDocument(driver, library);

			String tempDocTitle = library.getDocumentTitle();
			String tempBriefSummary=library.getBriefSummary();

			fc.utobj().printTestStep("Navigate To The Hub  > Library > Index Page");
			fc.hub().hub_common().theHubLibrarySubModule(driver);
			fc.utobj().clickElement(driver, pobj.indexTab);
			fc.utobj().clickElement(driver, pobj.detailView);

			fc.utobj().printTestStep("Modify Document");
			libraryPage.actionImgIconDetailsView(driver, documentTitle, "Modify");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			fc.utobj().sendKeys(driver, pobj.documentTitle, documentTitle);
			briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			fc.utobj().sendKeys(driver, pobj.briefSummary, briefSummary);

			if (!fc.utobj().isSelected(driver, pobj.documentType.get(0))) {
				fc.utobj().clickElement(driver, pobj.documentType.get(0));
			}

			fc.utobj().selectDropDown(driver, pobj.documentSubType, dataSet.get("documentSubType"));

			String file = fc.utobj().getFilePathFromTestData(dataSet.get("fileName"));
			fc.utobj().moveToElement(driver, pobj.fileClick);
			pobj.fileClick.sendKeys(file);

			if (!fc.utobj().isSelected(driver, pobj.thumbFileUploaded.get(1))) {
				fc.utobj().clickElement(driver, pobj.thumbFileUploaded.get(1));
			}

			if (!fc.utobj().isSelected(driver, pobj.accessibilityPublic)) {
				fc.utobj().clickElement(driver, pobj.accessibilityPublic);
			}

			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Modify Document Details");
			fc.utobj().clickElement(driver, pobj.detailView);

			if (!fc.utobj()
					.getElementByXpath(driver, ".//*[@id='DtlsInner']//a[contains(text () ,'" + documentTitle + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document Title At Detail View Page");
			}
			if (!fc.utobj()
					.getElementByXpath(driver, ".//*[@id='DtlsInner']//*[contains(text () ,'" + briefSummary + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document's Brief Summary");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//*[@id='DtlsInner']//*[contains(text () ,'" + documentTitle
					+ "')]/..//following-sibling::td[.='" + folderName + "']").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Parent Directory");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//*[@id='DtlsInner']//*[contains(text () ,'" + documentTitle
					+ "')]/..//following-sibling::td[.='FranConnect Administrator']").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Parent Directory");
			}

			fc.utobj()
					.printTestStep("Verify previous documents should be moved to Archived section after modification");
			fc.hub().hub_common().adminTheHubLibraryPage(driver);
			fc.utobj().clickElement(driver, new AdminTheHubLibraryPage(driver).archivedDocumentsTab);
			new HubCommonMethods().sortByRecentlyUploadedDocuments(driver);

			//verify Report option from document action icon from Archived > List View
			fc.utobj().clickElement(driver, pobj.listView);
			fc.utobj().clickElement(driver, ".//a[contains(@class,'textTheme14 ') and .='"+tempDocTitle+"']");
			libraryPage.actionImgIconListView(driver, tempDocTitle, "Report");
			
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().isTextDisplayed(driver, tempDocTitle, "Was not able to verify document Title");

			if (!fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , 'Brief Summary :')]/following-sibling::td[.='" + tempBriefSummary + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Brief Summary :");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Uploaded By :')]/following-sibling::td[.='FranConnect Administrator']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Uploaded By :");
			}
			
			fc.utobj().printTestStep("Verify the document download details");
			boolean isDocDownloadDetailsPresent=fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text(),'User Name')]/parent::*/following-sibling::tr/td[contains(text(),'FranConnect Administrator')]/preceding-sibling::td[contains(text(),'1')]");
			if (!isDocDownloadDetailsPresent) {
				fc.utobj().throwsException("Document Details is not present");
			}
			
			boolean isPrintButtonPresent=fc.utobj().isElementPresent(driver, pobj.adminLibraryPage(driver).printButton);
			if (!isPrintButtonPresent) {
				fc.utobj().throwsException("Print button is not present at Report cbox");
			}
			boolean isExportAsExcelButtonPresent=fc.utobj().isElementPresent(driver, pobj.adminLibraryPage(driver).exportAsExcelButton);
			if (!isExportAsExcelButtonPresent) {
				fc.utobj().throwsException("Export As Excel Button is not present at Report cbox");
			}
			boolean isCloseButtonPresent=fc.utobj().isElementPresent(driver, pobj.closeBtn);
			if (!isCloseButtonPresent) {
				fc.utobj().throwsException("Close Button is not present at Report cbox");
			}
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);
			
			//Verify Report option from document action icon from Archived >Details View
			fc.utobj().clickElement(driver, pobj.detailView);
			libraryPage.actionImgIconDetailsView(driver, tempDocTitle, "Report");
			
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().isTextDisplayed(driver, tempDocTitle, "Was not able to verify document Title");

			if (!fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , 'Brief Summary :')]/following-sibling::td[.='" + tempBriefSummary + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Brief Summary :");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Uploaded By :')]/following-sibling::td[.='FranConnect Administrator']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Uploaded By :");
			}
			
			fc.utobj().printTestStep("Verify the document download details");
			boolean isDocDownloadDetailsPresentDetails=fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text(),'User Name')]/parent::*/following-sibling::tr/td[contains(text(),'FranConnect Administrator')]/preceding-sibling::td[contains(text(),'1')]");
			if (!isDocDownloadDetailsPresentDetails) {
				fc.utobj().throwsException("Document Details is not present");
			}
			
			boolean isPrintButtonPresentDetails=fc.utobj().isElementPresent(driver, pobj.adminLibraryPage(driver).printButton);
			if (!isPrintButtonPresentDetails) {
				fc.utobj().throwsException("Print button is not present at Report cbox");
			}
			boolean isExportAsExcelButtonPresentDetails=fc.utobj().isElementPresent(driver, pobj.adminLibraryPage(driver).exportAsExcelButton);
			if (!isExportAsExcelButtonPresentDetails) {
				fc.utobj().throwsException("Export As Excel Button is not present at Report cbox");
			}
			boolean isCloseButtonPresentDetails=fc.utobj().isElementPresent(driver, pobj.closeBtn);
			if (!isCloseButtonPresentDetails) {
				fc.utobj().throwsException("Close Button is not present at Report cbox");
			}
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			boolean isDocumentTitlePresent = fc.utobj().assertPageSource(driver, tempDocTitle);
			if (!isDocumentTitlePresent) {
				fc.utobj().throwsException("Document is not present at archived documents tab");
			}

			fc.utobj().printTestStep("Verify version history details");
			libraryPage.actionImgIconDetailsView(driver, tempDocTitle, "Version History");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			boolean isDocTitleVerisonHistory = fc.utobj().assertPageSource(driver, tempDocTitle);
			if (!isDocTitleVerisonHistory) {
				fc.utobj().throwsException("Document is not present at version history cbox iframe");
			}

			boolean isDateLinkPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					"//*[@id='versionHistorytable']//td[contains(text(),'FranConnect Administrator')]/preceding-sibling::td/a");
			if (!isDateLinkPresent) {
				fc.utobj().throwsException("version history with dates link is not present");
			}

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='versionHistorytable']//td[contains(text(),'FranConnect Administrator')]/preceding-sibling::td/a[contains(text(),'_01')]"));

			boolean isFileFound2 = fc.utobj().isFileFound(new File(FranconnectUtil.config.get("downloadFilepath")),
					library.getFile());
			if (!isFileFound2) {
				fc.utobj().throwsException("Not able to download file from version history iframe");
			}

			fc.commonMethods().Click_Close_Input_ByValue(driver);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify Document should be downloaded from thumb view");
			fc.utobj().printTestStep("Go to Thumbnail view");
			fc.utobj().clickElement(driver, pobj.thumbnailView);
			libraryPage.thumbnailOption(driver, tempDocTitle, "Version History");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			boolean isDocTitleVerisonHistoryThumb = fc.utobj().assertPageSource(driver, tempDocTitle);
			if (!isDocTitleVerisonHistoryThumb) {
				fc.utobj().throwsException("Document is not present at version history cbox iframe");
			}

			boolean isDateLinkPresentThumb = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					"//*[@id='versionHistorytable']//td[contains(text(),'FranConnect Administrator')]/preceding-sibling::td/a");
			if (!isDateLinkPresentThumb) {
				fc.utobj().throwsException("version history with dates link is not present");
			}

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='versionHistorytable']//td[contains(text(),'FranConnect Administrator')]/preceding-sibling::td/a[contains(text(),'_01')]"));

			boolean isFileFound1 = fc.utobj().isFileFound(new File(FranconnectUtil.config.get("downloadFilepath")),
					library.getFile());
			/*if (!isFileFound1) {
				fc.utobj().throwsException("Not able to download file from version history iframe");
			}*/
			fc.commonMethods().Click_Close_Input_ByValue(driver);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Delete Document from archived list view");
			fc.utobj().clickElement(driver, pobj.detailView);
			new HubCommonMethods().sortByRecentlyUploadedDocuments(driver);
			libraryPage.actionImgIconDetailsView(driver, tempDocTitle, "Delete");

			fc.utobj().acceptAlertBox(driver);
			fc.utobj().printTestStep("Verify The Deleted Document from archived list view");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, tempDocTitle);
			if (isTextPresent == true) {
				fc.utobj().throwsException("was not able to delete document");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library","hub_0806" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Verify The Delete Document At The Hub > Library > Index[Detail View]", testCaseId = "TC_221_Delete_Document")
	private void deleteDocumentActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			TheHubLibraryPage pobj = new TheHubLibraryPage(driver);
			AdminTheHubLibraryPageTest libraryPage = new AdminTheHubLibraryPageTest();

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "Franchise";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			libraryPage.addFolder(driver, library);

			fc.utobj().printTestStep("Add Document");
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String currentDate = fc.utobj().getCurrentDateUSFormat();
			String afterDate = dataSet.get("afterDate");
			int val = Integer.parseInt(afterDate);
			String futureDate = fc.utobj().getFutureDateUSFormat(val);

			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setCurrentDate(currentDate);
			library.setFutureDate(futureDate);
			library.setLinkUrl(dataSet.get("linkUrl"));
			library.setDocumentType(dataSet.get("documentType"));
			library.setRecommendedDoc(false);

			libraryPage.addDocument(driver, library);

			fc.utobj().printTestStep("Navigate To The Hub  > Library > Index Page");
			fc.hub().hub_common().theHubLibrarySubModule(driver);
			fc.utobj().clickElement(driver, pobj.indexTab);
			fc.utobj().clickElement(driver, pobj.detailView);

			fc.utobj().printTestStep("Delete Document");
			libraryPage.actionImgIconDetailsView(driver, documentTitle, "Delete");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Delete Document");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, documentTitle);
			if (isTextPresent == true) {
				fc.utobj().throwsException("was not able to delete document");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library","hubfailed0928_1","hubf1121"})
	@TestCase(createdOn = "2017-06-22", updatedOn = "2017-06-22", testCaseDescription = "Verify The Delete Document At The Hub > Library > Index[List View]", testCaseId = "TC_The_Hub_List_View_Delete_Document_01")
	private void deleteDocumentActionImgListView() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);
			TheHubLibraryPage pobj = new TheHubLibraryPage(driver);
			AdminTheHubLibraryPageTest libraryPage = new AdminTheHubLibraryPageTest();

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "Franchise";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			libraryPage.addFolder(driver, library);

			fc.utobj().printTestStep("Add Document");
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String currentDate = fc.utobj().getCurrentDateUSFormat();
			String afterDate = dataSet.get("afterDate");
			int val = Integer.parseInt(afterDate);
			String futureDate = fc.utobj().getFutureDateUSFormat(val);

			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setCurrentDate(currentDate);
			library.setFutureDate(futureDate);
			library.setLinkUrl(dataSet.get("linkUrl"));
			library.setDocumentType(dataSet.get("documentType"));
			library.setRecommendedDoc(false);
			libraryPage.addDocument(driver, library);

			fc.utobj().printTestStep("Navigate To The Hub  > Library > Index Page");
			fc.hub().hub_common().theHubLibrarySubModule(driver);
			fc.utobj().clickElement(driver, pobj.indexTab);
			fc.utobj().clickElement(driver, pobj.listView);

			fc.utobj().printTestStep("Modify The Document");
			new HubCommonMethods().sortByRecentlyUploadedDocuments(driver);
			libraryPage.actionImgIconListView(driver, documentTitle, "Modify");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			fc.utobj().sendKeys(driver, pobj.documentTitle, documentTitle);
			briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			fc.utobj().sendKeys(driver, pobj.briefSummary, briefSummary);
			
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Search Modified Document by Search All The Hub Items");
			library.setDocumentTitle(documentTitle);
			hub_common_method.searchThehubSearchDocument(driver, library);
			fc.utobj().clickElement(driver, pobj.listView);
			fc.utobj().printTestStep("Verify The Modified Document");

			boolean isModifiedDocPresent = fc.utobj().assertPageSource(driver, library.getDocumentTitle());
			if (!isModifiedDocPresent) {
				fc.utobj().printTestStep("Not able to modify the document");
			}

			fc.utobj().printTestStep("Delete Document");
			libraryPage.actionImgIconListView(driver, documentTitle, "Delete");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Delete Document");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, documentTitle);
			if (isTextPresent == true) {
				fc.utobj().throwsException("was not able to delete document");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library" ,"hubdev0709" ,"hubDownload0725"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Verify The Document Properties At The Hub > Library >  Index[Detail View]", testCaseId = "TC_222_Document_Properties")
	private void documentPropertiesActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			TheHubLibraryPage pobj = new TheHubLibraryPage(driver);
			AdminTheHubLibraryPageTest libraryPage = new AdminTheHubLibraryPageTest();

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			libraryPage.addFolder(driver, library);
			fc.utobj().printTestStep("Add Document");

			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String currentDate = fc.utobj().getCurrentDateUSFormat();
			String afterDate = dataSet.get("afterDate");
			int val = Integer.parseInt(afterDate);
			String futureDate = fc.utobj().getFutureDateUSFormat(val);
			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setCurrentDate(currentDate);
			library.setFutureDate(futureDate);
			library.setDocumentSubType(dataSet.get("documentSubType"));
			library.setFile(dataSet.get("fileName"));
			library.setDocumentType(dataSet.get("documentType"));
			library.setRecommendedDoc(false);
			libraryPage.addDocument(driver, library);

			fc.utobj().printTestStep("Navigate To The Hub > Library > Index Page ");
			fc.hub().hub_common().theHubLibrarySubModule(driver);
			fc.utobj().clickElement(driver, pobj.indexTab);
			fc.utobj().clickElement(driver, pobj.detailView);

			new HubCommonMethods().sortByRecentlyUploadedDocuments(driver);
			libraryPage.actionImgIconDetailsView(driver, documentTitle, "Document Properties");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			if (!fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , 'Document Title')]/following-sibling::td[.='" + documentTitle + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document Title");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Summary')]/following-sibling::td[.='" + briefSummary + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document's Brief Summary");
			}

			if (!fc.utobj().isElementPresent(driver, fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Document URL')]/following-sibling::td/a[contains(@href,'"+FranconnectUtil.config.get("buildUrl")+"')]"))) {
				fc.utobj().throwsException("Document URL is not present");
				
			}
			
			if (!fc.utobj().isElementPresent(driver, fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Active From')]/following-sibling::td[contains(text(),'"+library.getCurrentDate()+"')]"))) {
				fc.utobj().throwsException("Acttive From Label is not present");
				
			}
			
			if (!fc.utobj().isElementPresent(driver, fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Creation Date')]/following-sibling::td[contains(text(),'"+library.getCurrentDate()+"')]"))) {
				fc.utobj().throwsException("Acttive From Label is not present");
				
			}
			
			if (!fc.utobj().isElementPresent(driver, fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Last Modified')]/following-sibling::td[contains(text(),'"+library.getCurrentDate()+"')]"))) {
				fc.utobj().throwsException("Acttive From Label is not present");
				
			}
			
			if (!fc.utobj().isElementPresent(driver, fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Document Size ')]/following-sibling::td[contains(text(),'KB')]"))) {
				fc.utobj().throwsException("Acttive From Label is not present");
				
			}
			
			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Document Type')]/following-sibling::td[contains(text () , '"
									+ dataSet.get("documentType") + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document Type");
			}
			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Document Subtype')]/following-sibling::td[contains(text () , '"
									+ dataSet.get("documentSubType") + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify DocumentSub Type");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Uploaded By')]/following-sibling::td[contains(text () , 'FranConnect Administrator')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Uploaded By");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Recommended')]/following-sibling::td[contains(text () , '"
									+ dataSet.get("recommendedDoc") + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Recommended");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);
			
			//To verify Document Properties option from document Action Icon from Index > List View
			fc.utobj().printTestStep("Verify Document Properties option from document Action Icon from Index > List View");
			fc.utobj().clickElement(driver, pobj.listView);
			new HubCommonMethods().sortByRecentlyUploadedDocuments(driver);
			libraryPage.actionImgIconListView(driver, documentTitle, "Document Properties");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			if (!fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , 'Document Title')]/following-sibling::td[.='" + documentTitle + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document Title");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Summary')]/following-sibling::td[.='" + briefSummary + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document's Brief Summary");
			}

			if (!fc.utobj().isElementPresent(driver, fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Document URL')]/following-sibling::td/a[contains(@href,'"+FranconnectUtil.config.get("buildUrl")+"')]"))) {
				fc.utobj().throwsException("Document URL is not present");
				
			}
			
			if (!fc.utobj().isElementPresent(driver, fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Active From')]/following-sibling::td[contains(text(),'"+library.getCurrentDate()+"')]"))) {
				fc.utobj().throwsException("Acttive From Label is not present");
				
			}
			
			if (!fc.utobj().isElementPresent(driver, fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Creation Date')]/following-sibling::td[contains(text(),'"+library.getCurrentDate()+"')]"))) {
				fc.utobj().throwsException("Acttive From Label is not present");
				
			}
			
			if (!fc.utobj().isElementPresent(driver, fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Last Modified')]/following-sibling::td[contains(text(),'"+library.getCurrentDate()+"')]"))) {
				fc.utobj().throwsException("Acttive From Label is not present");
				
			}
			
			if (!fc.utobj().isElementPresent(driver, fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Document Size ')]/following-sibling::td[contains(text(),'KB')]"))) {
				fc.utobj().throwsException("Acttive From Label is not present");
				
			}
			
			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Document Type')]/following-sibling::td[contains(text () , '"
									+ dataSet.get("documentType") + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document Type");
			}
			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Document Subtype')]/following-sibling::td[contains(text () , '"
									+ dataSet.get("documentSubType") + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify DocumentSub Type");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Uploaded By')]/following-sibling::td[contains(text () , 'FranConnect Administrator')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Uploaded By");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Recommended')]/following-sibling::td[contains(text () , '"
									+ dataSet.get("recommendedDoc") + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Recommended");
			}

			
			fc.utobj().printTestStep("Verify clicking on URL document should be downloaded successfully");
			fc.utobj().clickElement(driver, ".//td[contains(text () , 'Document URL')]/following-sibling::td/a[contains(@href,'"+FranconnectUtil.config.get("buildUrl")+"')]");
			
			/*if(!fc.utobj().isFileFound(new File(FranconnectUtil.config.get("downloadFilepath")), library.getFile())){
				fc.utobj().throwsException("Not able to download file from on clicking Document url");
			}*/
			
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library","hub_0806","hubf1121"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Post Comment At The Hub > Library >  Index[Detail View]", testCaseId = "TC_223_Post_Comment")
	private void postCommentActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubLibraryPage pobj = new TheHubLibraryPage(driver);
			AdminTheHubLibraryPageTest libraryPage = new AdminTheHubLibraryPageTest();

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			libraryPage.addFolder(driver, library);

			fc.utobj().printTestStep("Add Document");

			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String currentDate = fc.utobj().getCurrentDateUSFormat();
			String afterDate = dataSet.get("afterDate");
			int val = Integer.parseInt(afterDate);
			String futureDate = fc.utobj().getFutureDateUSFormat(val);

			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setCurrentDate(currentDate);
			library.setFutureDate(futureDate);
			library.setDocumentSubType(dataSet.get("documentSubType"));
			library.setFile(dataSet.get("fileName"));
			library.setDocumentType(dataSet.get("documentType"));
			library.setRecommendedDoc(false);

			libraryPage.addDocument(driver, library);

			fc.utobj().printTestStep("Navigate To The Hub > Library > Index Page");
			fc.hub().hub_common().theHubLibrarySubModule(driver);
			fc.utobj().clickElement(driver, pobj.indexTab);
			fc.utobj().clickElement(driver, pobj.detailView);

			fc.utobj().printTestStep("Post Comment");
			libraryPage.postCommentActionImg(driver, documentTitle, "Post Comment");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().isTextDisplayed(driver, documentTitle, "was not able to verify Document Title");
			fc.utobj().isTextDisplayed(driver, briefSummary, "was not able to verify Brief Summary");

			fc.utobj().clickElement(driver, pobj.comment);
			String commentText = fc.utobj().generateTestData(dataSet.get("commentText"));
			WebElement element12 = fc.utobj().getElementByXpath(driver, ".//*[contains(@id , 'ta')]");
			fc.utobj().sendKeys(driver, element12, commentText);
			fc.utobj().clickElement(driver, pobj.post);
			fc.utobj().printTestStep("Verify The Post Comment");
			fc.utobj().isTextDisplayed(driver, commentText, "was not able to verify Comment Text");
			
			
			fc.commonMethods().Click_Close_Input_ByValue(driver);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify Post Comment option from document Comment link from Index > List View");
			String commentTextList=fc.utobj().generateTestData("Test Comment Text");
			library.setCommentText(commentTextList);
			hub_common_method.postCommentAtListView(driver, new AdminTheHubLibraryPage(driver), library);
			boolean isCommentTextPresent=fc.utobj().assertPageSource(driver, library.getCommentText());
			if (!isCommentTextPresent) {
				fc.utobj().throwsException("Not able to verify added Comment at List View");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library", "hub0528" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Verify The Delete Comment At The Hub > Library >  Index[Detail View]", testCaseId = "TC_224_Delete_Comment")
	private void deleteComment() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			TheHubLibraryPage pobj = new TheHubLibraryPage(driver);
			AdminTheHubLibraryPageTest libraryPage = new AdminTheHubLibraryPageTest();

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			libraryPage.addFolder(driver, library);
			fc.utobj().printTestStep("Add Document");

			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String currentDate = fc.utobj().getCurrentDateUSFormat();
			String afterDate = dataSet.get("afterDate");
			int val = Integer.parseInt(afterDate);
			String futureDate = fc.utobj().getFutureDateUSFormat(val);
			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setCurrentDate(currentDate);
			library.setFutureDate(futureDate);
			library.setDocumentSubType(dataSet.get("documentSubType"));
			library.setFile(dataSet.get("fileName"));
			library.setDocumentType(dataSet.get("documentType"));
			library.setRecommendedDoc(false);
			libraryPage.addDocument(driver, library);

			fc.utobj().printTestStep("Navigate To The Hub > Library > Index Page");

			fc.hub().hub_common().theHubLibrarySubModule(driver);
			fc.utobj().clickElement(driver, pobj.indexTab);
			fc.utobj().clickElement(driver, pobj.detailView);

			fc.utobj().printTestStep("Post Comment");
			libraryPage.postCommentActionImg(driver, documentTitle, "Post Comment");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().isTextDisplayed(driver, documentTitle, "was not able to verify Document Title");
			fc.utobj().isTextDisplayed(driver, briefSummary, "was not able to verify Brief Summary");

			fc.utobj().clickElement(driver, pobj.comment);
			String commentText = fc.utobj().generateTestData(dataSet.get("commentText"));
			WebElement element12 = fc.utobj().getElementByXpath(driver, ".//*[contains(@id , 'ta')]");
			fc.utobj().sendKeys(driver, element12, commentText);
			fc.utobj().clickElement(driver, pobj.post);
			fc.utobj().printTestStep("Delete Comment");
			fc.utobj().clickElement(driver, pobj.deleteComment);
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Deleted Comment");
			boolean isTextPresent = fc.utobj().assertNotInPageSource(driver, commentText);
			if (isTextPresent) {
				fc.utobj().throwsException("was not able to delete Comment");
			}
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library" ,"hub_download" ,"hubfailed1018","hubf1121"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Verify The Version History At The Hub > Library >  Index[Detail View]", testCaseId = "TC_225_Version_History")
	private void versionHistory() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			TheHubLibraryPage pobj = new TheHubLibraryPage(driver);
			AdminTheHubLibraryPageTest libraryPage = new AdminTheHubLibraryPageTest();

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			libraryPage.addFolder(driver, library);

			fc.utobj().printTestStep("Add Document");

			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String currentDate = fc.utobj().getCurrentDateUSFormat();
			String afterDate = dataSet.get("afterDate");
			int val = Integer.parseInt(afterDate);
			String futureDate = fc.utobj().getFutureDateUSFormat(val);

			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setCurrentDate(currentDate);
			library.setFutureDate(futureDate);
			library.setDocumentSubType(dataSet.get("documentSubType"));
			library.setFile(dataSet.get("fileName"));
			library.setDocumentType(dataSet.get("documentType"));
			library.setRecommendedDoc(false);

			libraryPage.addDocument(driver, library);

			fc.utobj().printTestStep("Navigate To The Hub > Library > Index Page");
			fc.hub().hub_common().theHubLibrarySubModule(driver);
			fc.utobj().clickElement(driver, pobj.indexTab);
			fc.utobj().clickElement(driver, pobj.detailView);
			
			fc.utobj().printTestStep("Verify The Version History");
			libraryPage.actionImgIconDetailsView(driver, documentTitle, "Version History");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Document Title')]/following-sibling::td[contains(text () , '"
									+ documentTitle + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document Title");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Brief Summary')]/following-sibling::td[contains(text () , '"
									+ briefSummary + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document's Brief Summary");
			}

			String text = fc.utobj().getText(driver,
					fc.utobj().getElementByXpath(driver,
							".//td[contains(text(),' FranConnect Administrator')]/preceding-sibling::td/a[contains(text(),'"
									+ fc.utobj().getCurrentYear() + "')]"));

			String pattern = "yyyyMMdd";
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
			String date = simpleDateFormat.format(new Date());
			date = date.concat("_01");
			if (!text.equalsIgnoreCase(date)) {
				fc.utobj().throwsException("not able to verify document link format at version history frame");
			}

			fc.utobj().clickElement(driver,
					".//td[contains(text(),' FranConnect Administrator')]/preceding-sibling::td/a[contains(text(),'"
							+ fc.utobj().getCurrentYear() + "')]");

			boolean isFileFound = fc.utobj().isFileFound(new File(FranconnectUtil.config.get("downloadFilepath")),
					library.getFile());
			if (!isFileFound) {
				fc.utobj().throwsException("Not able to download version history document");
			}

			hub_common_method.deleteDownloadedFile(library);

			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library" ,"thehubtest0702"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Verify The Delete Document At The Hub > Library >  Index[Detail View]", testCaseId = "TC_226_Delete_Document")
	private void deleteDocument() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			TheHubLibraryPage pobj = new TheHubLibraryPage(driver);
			AdminTheHubLibraryPageTest libraryPage = new AdminTheHubLibraryPageTest();

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			libraryPage.addFolder(driver, library);

			fc.utobj().printTestStep("Add Document");

			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String currentDate = fc.utobj().getCurrentDateUSFormat();
			String afterDate = dataSet.get("afterDate");
			int val = Integer.parseInt(afterDate);
			String futureDate = fc.utobj().getFutureDateUSFormat(val);

			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setCurrentDate(currentDate);
			library.setFutureDate(futureDate);
			library.setDocumentSubType(dataSet.get("documentSubType"));
			library.setFile(dataSet.get("fileName"));
			library.setDocumentType(dataSet.get("documentType"));
			library.setRecommendedDoc(false);

			libraryPage.addDocument(driver, library);

			fc.utobj().printTestStep("Navigate To The Hub > Library > Index Page");

			fc.hub().hub_common().theHubLibrarySubModule(driver);
			fc.utobj().clickElement(driver, pobj.indexTab);
			fc.utobj().clickElement(driver, pobj.detailView);

			fc.utobj().printTestStep("Delete Document");
			fc.utobj().clickElement(driver, pobj.deleteLink);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().showAll(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[.='" + documentTitle + "']/ancestor::tr/td/input"));
			fc.utobj().clickElementByJS(driver, fc.utobj().getElement(driver, pobj.deleteBtn));
			fc.utobj().acceptAlertBox(driver);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, pobj.detailView);

			fc.utobj().printTestStep("Verify The Delete Document");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, documentTitle);

			if (isTextPresent == true) {
				fc.utobj().throwsException("was not able to delete document");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Verify The Report Document At The Hub > Library >  Index[Thumbnail View]", testCaseId = "TC_227_Report_Document")
	private void reportDocument() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			TheHubLibraryPage pobj = new TheHubLibraryPage(driver);
			AdminTheHubLibraryPageTest libraryPage = new AdminTheHubLibraryPageTest();

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			libraryPage.addFolder(driver, library);

			fc.utobj().printTestStep("Add Document");

			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String currentDate = fc.utobj().getCurrentDateUSFormat();
			String afterDate = dataSet.get("afterDate");
			int val = Integer.parseInt(afterDate);
			String futureDate = fc.utobj().getFutureDateUSFormat(val);

			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setCurrentDate(currentDate);
			library.setFutureDate(futureDate);
			library.setDocumentSubType(dataSet.get("documentSubType"));
			library.setFile(dataSet.get("fileName"));
			library.setDocumentType(dataSet.get("documentType"));
			library.setRecommendedDoc(false);

			libraryPage.addDocument(driver, library);

			fc.utobj().printTestStep("Navigate To The Hub > Library > Index Page");

			fc.hub().hub_common().theHubLibrarySubModule(driver);
			fc.utobj().clickElement(driver, pobj.indexTab);
			fc.utobj().clickElement(driver, pobj.thumbnailView);

			fc.utobj().printTestStep("Verify The Report Of Document");
			if (!fc.utobj().getElementByXpath(driver, ".//*[@id='thums']//a[.='" + documentTitle + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document Title At Thumbnail View Page");
			}

			libraryPage.thumbnailOption(driver, documentTitle, "Report");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().isTextDisplayed(driver, documentTitle, "Was not able to verify document Title");

			if (!fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , 'Brief Summary :')]/following-sibling::td[.='" + briefSummary + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Brief Summary :");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Uploaded By :')]/following-sibling::td[.='FranConnect Administrator']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Uploaded By :");
			}
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Verify The Document Properties At The Hub > Library >  Index[Thumbnail View]", testCaseId = "TC_228_Document_Properties")
	private void documentProperties() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			TheHubLibraryPage pobj = new TheHubLibraryPage(driver);
			AdminTheHubLibraryPageTest libraryPage = new AdminTheHubLibraryPageTest();

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			libraryPage.addFolder(driver, library);

			fc.utobj().printTestStep("Add Document");

			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String currentDate = fc.utobj().getCurrentDateUSFormat();
			String afterDate = dataSet.get("afterDate");
			int val = Integer.parseInt(afterDate);
			String futureDate = fc.utobj().getFutureDateUSFormat(val);

			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setCurrentDate(currentDate);
			library.setFutureDate(futureDate);
			library.setDocumentSubType(dataSet.get("documentSubType"));
			library.setFile(dataSet.get("fileName"));
			library.setDocumentType(dataSet.get("documentType"));
			library.setRecommendedDoc(false);

			libraryPage.addDocument(driver, library);

			fc.utobj().printTestStep("Navigate To The Hub > Library > Index Page");

			fc.hub().hub_common().theHubLibrarySubModule(driver);
			fc.utobj().clickElement(driver, pobj.indexTab);
			fc.utobj().clickElement(driver, pobj.thumbnailView);

			fc.utobj().printTestStep("Verify The Document Properties");
			libraryPage.thumbnailOption(driver, documentTitle, "Document Properties");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			if (!fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , 'Document Title')]/following-sibling::td[.='" + documentTitle + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document Title");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Summary')]/following-sibling::td[.='" + briefSummary + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document's Brief Summary");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Document Type')]/following-sibling::td[contains(text () , '"
									+ dataSet.get("documentType") + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document Type");
			}
			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Document Subtype')]/following-sibling::td[contains(text () , '"
									+ dataSet.get("documentSubType") + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify DocumentSub Type");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Uploaded By')]/following-sibling::td[contains(text () , 'FranConnect Administrator')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify DocumentSub Type");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Recommended')]/following-sibling::td[contains(text () , '"
									+ dataSet.get("recommendedDoc") + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Recommended");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Post Comment At The Hub > Library >  Index[Thumbnail View]", testCaseId = "TC_229_Post_Comment")
	private void postComment() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			TheHubLibraryPage pobj = new TheHubLibraryPage(driver);
			AdminTheHubLibraryPageTest libraryPage = new AdminTheHubLibraryPageTest();

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			libraryPage.addFolder(driver, library);

			fc.utobj().printTestStep("Add Document");

			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String currentDate = fc.utobj().getCurrentDateUSFormat();
			String afterDate = dataSet.get("afterDate");
			int val = Integer.parseInt(afterDate);
			String futureDate = fc.utobj().getFutureDateUSFormat(val);

			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setCurrentDate(currentDate);
			library.setFutureDate(futureDate);
			library.setDocumentSubType(dataSet.get("documentSubType"));
			library.setFile(dataSet.get("fileName"));
			library.setDocumentType(dataSet.get("documentType"));
			library.setRecommendedDoc(false);

			libraryPage.addDocument(driver, library);

			fc.utobj().printTestStep("Navigate To The Hub > Library > Index Page");

			fc.hub().hub_common().theHubLibrarySubModule(driver);
			fc.utobj().clickElement(driver, pobj.indexTab);
			fc.utobj().clickElement(driver, pobj.thumbnailView);

			fc.utobj().printTestStep("Post Comment");
			libraryPage.thumbnailOption(driver, documentTitle, "Post Comment");

			fc.utobj().printTestStep("Verify The Post Comment");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().isTextDisplayed(driver, documentTitle, "was not able to verify Document Title");
			fc.utobj().isTextDisplayed(driver, briefSummary, "was not able to verify Brief Summary");

			fc.utobj().clickElement(driver, pobj.comment);
			String commentText = fc.utobj().generateTestData(dataSet.get("commentText"));
			WebElement element12 = fc.utobj().getElementByXpath(driver, ".//*[contains(@id , 'ta')]");
			System.out.println(element12);
			fc.utobj().sendKeys(driver, element12, commentText);
			fc.utobj().clickElement(driver, pobj.post);

			fc.utobj().isTextDisplayed(driver, commentText, "was not able to verify Comment Text");

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library" ,"hubf1121"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Veriyf The Delete Comment At The Hub > Library >  Index[Thumbnail View]", testCaseId = "TC_230_Delete_Comment")
	private void deleteComment01() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			TheHubLibraryPage pobj = new TheHubLibraryPage(driver);
			AdminTheHubLibraryPageTest libraryPage = new AdminTheHubLibraryPageTest();

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			libraryPage.addFolder(driver, library);

			fc.utobj().printTestStep("Add Document");
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String currentDate = fc.utobj().getCurrentDateUSFormat();
			String afterDate = dataSet.get("afterDate");
			int val = Integer.parseInt(afterDate);
			String futureDate = fc.utobj().getFutureDateUSFormat(val);

			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setCurrentDate(currentDate);
			library.setFutureDate(futureDate);
			library.setDocumentSubType(dataSet.get("documentSubType"));
			library.setFile(dataSet.get("fileName"));
			library.setDocumentType(dataSet.get("documentType"));
			library.setRecommendedDoc(false);
			libraryPage.addDocument(driver, library);

			fc.utobj().printTestStep("Navigate To The Hub > Library > Index Page");
			fc.hub().hub_common().theHubLibrarySubModule(driver);
			fc.utobj().clickElement(driver, pobj.indexTab);
			fc.utobj().clickElement(driver, pobj.thumbnailView);

			fc.utobj().printTestStep("Post Comment");
			libraryPage.thumbnailOption(driver, documentTitle, "Post Comment");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().isTextDisplayed(driver, documentTitle, "was not able to verify Document Title");
			fc.utobj().isTextDisplayed(driver, briefSummary, "was not able to verify Brief Summary");

			fc.utobj().printTestStep("Delete Comment");
			fc.utobj().clickElement(driver, pobj.comment);
			String commentText = fc.utobj().generateTestData(dataSet.get("commentText"));
			WebElement element12 = fc.utobj().getElementByXpath(driver, ".//*[contains(@id , 'ta')]");
			fc.utobj().sendKeys(driver, element12, commentText);
			fc.utobj().clickElement(driver, pobj.post);
			fc.utobj().clickElement(driver, pobj.deleteComment);
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Delete Comment");
			boolean isTextPresent = fc.utobj().assertNotInPageSource(driver, commentText);
			if (isTextPresent) {
				fc.utobj().throwsException("was not able to delete Comment");
			}
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library" ,"hub_0806"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Verify The Version History At The Hub > Library >  Index[Thumbnail View]", testCaseId = "TC_231_Version_History")
	private void versionHistory01() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			TheHubLibraryPage pobj = new TheHubLibraryPage(driver);
			AdminTheHubLibraryPageTest libraryPage = new AdminTheHubLibraryPageTest();

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			libraryPage.addFolder(driver, library);

			fc.utobj().printTestStep("Add Document");

			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String currentDate = fc.utobj().getCurrentDateUSFormat();
			String afterDate = dataSet.get("afterDate");
			int val = Integer.parseInt(afterDate);
			String futureDate = fc.utobj().getFutureDateUSFormat(val);

			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setCurrentDate(currentDate);
			library.setFutureDate(futureDate);
			library.setDocumentSubType(dataSet.get("documentSubType"));
			library.setFile(dataSet.get("fileName"));
			library.setDocumentType(dataSet.get("documentType"));
			library.setRecommendedDoc(false);

			libraryPage.addDocument(driver, library);

			fc.utobj().printTestStep("Navigate To The Hub > Library > Index Page");

			fc.hub().hub_common().theHubLibrarySubModule(driver);
			fc.utobj().clickElement(driver, pobj.indexTab);
			fc.utobj().clickElement(driver, pobj.thumbnailView);

			fc.utobj().printTestStep("Verify The Version History");
			libraryPage.thumbnailOption(driver, documentTitle, "Version History");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Document Title')]/following-sibling::td[contains(text () , '"
									+ documentTitle + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document Title");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Brief Summary')]/following-sibling::td[contains(text () , '"
									+ briefSummary + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document's Brief Summary");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library","hub_0806","hubf1121" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Verify The Delete Document At The Hub > Library >  Index[Thumbnail View]", testCaseId = "TC_232_Delete_Document")
	private void deleteDocumentBtn() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			TheHubLibraryPage pobj = new TheHubLibraryPage(driver);
			AdminTheHubLibraryPageTest libraryPage = new AdminTheHubLibraryPageTest();

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			libraryPage.addFolder(driver, library);

			fc.utobj().printTestStep("Add Document");

			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String currentDate = fc.utobj().getCurrentDateUSFormat();
			String afterDate = dataSet.get("afterDate");
			int val = Integer.parseInt(afterDate);
			String futureDate = fc.utobj().getFutureDateUSFormat(val);

			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setCurrentDate(currentDate);
			library.setFutureDate(futureDate);
			library.setDocumentSubType(dataSet.get("documentSubType"));
			library.setFile(dataSet.get("fileName"));
			library.setDocumentType(dataSet.get("documentType"));
			library.setRecommendedDoc(false);

			libraryPage.addDocument(driver, library);

			fc.utobj().printTestStep("Navigate To The Hub > Library > Index Page");

			fc.hub().hub_common().theHubLibrarySubModule(driver);
			fc.utobj().clickElement(driver, pobj.indexTab);
			fc.utobj().clickElement(driver, pobj.thumbnailView);

			fc.utobj().printTestStep("Delete Document");

			WebElement element = fc.utobj().getElementByXpath(driver, ".//*[@id='thums']//*[.='" + documentTitle
					+ "']//following::table[@class='topLine pt5'][1]//td/a[contains(@onclick, 'deletedocument')]");
			fc.utobj().clickElementByJS(driver, element);

			fc.utobj().acceptAlertBox(driver);
			fc.utobj().clickElement(driver, pobj.thumbnailView);

			fc.utobj().printTestStep("Verify The Delete Document");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, documentTitle);

			if (isTextPresent) {
				fc.utobj().throwsException("was not able to delete Library Document");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library","hubf1121"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Verify The Modify Document At The Hub > Library >  Index[Thumbnail View]", testCaseId = "TC_233_Modify_Document")
	private void modifyDocumentBtn() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			TheHubLibraryPage pobj = new TheHubLibraryPage(driver);
			AdminTheHubLibraryPageTest libraryPage = new AdminTheHubLibraryPageTest();

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			libraryPage.addFolder(driver, library);
			fc.utobj().printTestStep("Add Document");

			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String currentDate = fc.utobj().getCurrentDateUSFormat();
			String afterDate = dataSet.get("afterDate");
			int val = Integer.parseInt(afterDate);
			String futureDate = fc.utobj().getFutureDateUSFormat(val);

			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setCurrentDate(currentDate);
			library.setFutureDate(futureDate);
			library.setDocumentSubType(dataSet.get("documentSubType"));
			library.setFile(dataSet.get("fileName"));
			library.setDocumentType(dataSet.get("documentType"));
			library.setRecommendedDoc(false);
			libraryPage.addDocument(driver, library);

			String tempDocTitle = library.getDocumentTitle();
			String tempBriefSummary=library.getBriefSummary();

			fc.utobj().printTestStep("Navigate To The Hub > Library > Index Page");
			fc.hub().hub_common().theHubLibrarySubModule(driver);
			fc.utobj().clickElement(driver, pobj.indexTab);
			fc.utobj().clickElement(driver, pobj.thumbnailView);

			fc.utobj().printTestStep("Modify Document");
			WebElement element = fc.utobj().getElementByXpath(driver, ".//*[@id='thums']//*[.='" + documentTitle + "']"
					+ "//following::table[@class='topLine pt5'][1]//td/a/img[@title='Modify']");
			fc.utobj().clickElement(driver, element);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			fc.utobj().sendKeys(driver, pobj.documentTitle, documentTitle);
			briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			fc.utobj().sendKeys(driver, pobj.briefSummary, briefSummary);

			if (!fc.utobj().isSelected(driver, pobj.documentType.get(0))) {
				fc.utobj().clickElement(driver, pobj.documentType.get(0));
			}

			fc.utobj().selectDropDown(driver, pobj.documentSubType, dataSet.get("documentSubType"));

			String file = fc.utobj().getFilePathFromTestData(dataSet.get("fileName"));

			fc.utobj().moveToElement(driver, pobj.fileClick);
			pobj.fileClick.sendKeys(file);

			if (!fc.utobj().isSelected(driver, pobj.thumbFileUploaded.get(1))) {
				fc.utobj().clickElement(driver, pobj.thumbFileUploaded.get(1));
			}

			if (!fc.utobj().isSelected(driver, pobj.accessibilityPublic)) {
				fc.utobj().clickElement(driver, pobj.accessibilityPublic);
			}

			fc.utobj().clickElement(driver, pobj.saveBtn);
			new HubCommonMethods().tryClickClose(driver, new AdminTheHubLibraryPage(driver));
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Modify Document Detail");
			fc.utobj().clickElement(driver, pobj.thumbnailView);

			if (!fc.utobj().getElementByXpath(driver, ".//*[@id='thums']//a[.='" + documentTitle + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document Title At Thumbnail View Page");
			}

			libraryPage.thumbnailOption(driver, documentTitle, "Report");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().isTextDisplayed(driver, documentTitle, "Was not able to verify document Title");

			if (!fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , 'Brief Summary :')]/following-sibling::td[.='" + briefSummary + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Brief Summary :");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Uploaded By :')]/following-sibling::td[.='FranConnect Administrator']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Uploaded By :");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj()
					.printTestStep("Verify previous documents should be moved to Archived section after modification");
			fc.hub().hub_common().adminTheHubLibraryPage(driver);
			fc.utobj().clickElement(driver, new AdminTheHubLibraryPage(driver).archivedDocumentsTab);
			
			//To verify Report option from document action icon from Archived >Thumb View
			fc.utobj().printTestStep("Verify Report option from document action icon from Archived > Thumb View");
			fc.utobj().clickElement(driver, pobj.thumbnailView);
			fc.utobj().clickElement(driver, ".//a[contains(@class,'rowText12 wordicn') and .='"+tempDocTitle+"']");
			libraryPage.thumbnailOption(driver, tempDocTitle, "Report");
			
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().isTextDisplayed(driver, tempDocTitle, "Was not able to verify document Title");

			if (!fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , 'Brief Summary :')]/following-sibling::td[.='" + tempBriefSummary + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Brief Summary :");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Uploaded By :')]/following-sibling::td[.='FranConnect Administrator']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Uploaded By :");
			}
			
			fc.utobj().printTestStep("Verify the document download details");
			boolean isDocDownloadDetailsPresent=fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text(),'User Name')]/parent::*/following-sibling::tr/td[contains(text(),'FranConnect Administrator')]/preceding-sibling::td[contains(text(),'1')]");
			if (!isDocDownloadDetailsPresent) {
				fc.utobj().throwsException("Document Details is not present");
			}
			
			boolean isPrintButtonPresent=fc.utobj().isElementPresent(driver, pobj.adminLibraryPage(driver).printButton);
			if (!isPrintButtonPresent) {
				fc.utobj().throwsException("Print button is not present at Report cbox");
			}
			boolean isExportAsExcelButtonPresent=fc.utobj().isElementPresent(driver, pobj.adminLibraryPage(driver).exportAsExcelButton);
			if (!isExportAsExcelButtonPresent) {
				fc.utobj().throwsException("Export As Excel Button is not present at Report cbox");
			}
			boolean isCloseButtonPresent=fc.utobj().isElementPresent(driver, pobj.closeBtn);
			if (!isCloseButtonPresent) {
				fc.utobj().throwsException("Close Button is not present at Report cbox");
			}
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);
			
			fc.utobj().clickElement(driver, pobj.thumbnailView);
			new HubCommonMethods().sortByRecentlyUploadedDocuments(driver);

			boolean isDocumentTitlePresent = fc.utobj().assertPageSource(driver, tempDocTitle);
			if (!isDocumentTitlePresent) {
				fc.utobj().throwsException("Document is not present at archived documents tab");
			}

			fc.utobj().printTestStep("Verify version history details");
			libraryPage.thumbnailOption(driver, tempDocTitle, "Version History");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			boolean isDocTitleVerisonHistory = fc.utobj().assertPageSource(driver, tempDocTitle);
			if (!isDocTitleVerisonHistory) {
				fc.utobj().throwsException("Document is not present at version history cbox iframe");
			}

			boolean isDateLinkPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					"//*[@id='versionHistorytable']//td[contains(text(),'FranConnect Administrator')]/preceding-sibling::td/a");
			if (!isDateLinkPresent) {
				fc.utobj().throwsException("version history with dates link is not present");
			}

			fc.commonMethods().Click_Close_Input_ByValue(driver);
			fc.utobj().switchFrameToDefault(driver);
			
			fc.utobj().printTestStep("Delete The Archived Document");
			new HubCommonMethods().sortByRecentlyUploadedDocuments(driver);
			WebElement deleteElement = fc.utobj().getElementByXpath(driver, ".//*[@id='thums']//*[.='" + tempDocTitle + "']"
					+ "//following::table[@class='topLine pt5'][1]//td/a[contains(@onclick, 'deletedocument')]");
			fc.utobj().clickElement(driver, deleteElement);
			fc.utobj().acceptAlertBox(driver);
			
			fc.utobj().printTestStep("Verify The Delete Document");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, tempDocTitle);
			if (isTextPresent) {
				fc.utobj().throwsException("was not able to delete document at Archived > Thumbnail view");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library" ,"hubf1121"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Verify The Add Document At The Hub > Library >  Folder", testCaseId = "TC_234_Add_Document")
	private void addDocument01() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubLibraryPage pobj = new TheHubLibraryPage(driver);
			AdminTheHubLibraryPageTest libraryPage = new AdminTheHubLibraryPageTest();

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			libraryPage.addFolder(driver, library);

			fc.utobj().printTestStep("Navigate To The Hub > Library > Folder");

			fc.hub().hub_common().theHubLibrarySubModule(driver);
			libraryPage.clickOverMoreLink(driver, folderName);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='folderDiv']//*[contains(text() , '" + folderName + "')]"));

			fc.utobj().printTestStep("Add Document");
			fc.utobj().clickElement(driver, pobj.addDocument);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			if (!fc.utobj().isSelected(driver, pobj.multipleDoc.get(1))) {
				fc.utobj().clickElement(driver, pobj.multipleDoc.get(1));
			}

			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			fc.utobj().sendKeys(driver, pobj.documentTitle, documentTitle);
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			fc.utobj().sendKeys(driver, pobj.briefSummary, briefSummary);

			if (!fc.utobj().isSelected(driver, pobj.documentType.get(0))) {
				fc.utobj().clickElement(driver, pobj.documentType.get(0));
			}

			fc.utobj().selectDropDown(driver, pobj.documentSubType, dataSet.get("subType"));

			String file = fc.utobj().getFilePathFromTestData(dataSet.get("fileName"));

			fc.utobj().moveToElement(driver, pobj.fileClick);
			pobj.fileClick.sendKeys(file);

			if (!fc.utobj().isSelected(driver, pobj.thumbFileUploaded.get(1))) {
				fc.utobj().clickElement(driver, pobj.thumbFileUploaded.get(1));
			}

			if (!fc.utobj().isSelected(driver, pobj.accessibilityPublic)) {
				fc.utobj().clickElement(driver, pobj.accessibilityPublic);
			}

			fc.utobj().clickElementByJS(driver, pobj.addBtn);
			new HubCommonMethods().tryClickClose(driver, new AdminTheHubLibraryPage(driver));
			fc.utobj().switchFrameToDefault(driver);

			libraryPage.clickOverMoreLink(driver, folderName);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='folderDiv']//*[contains(text() , '" + folderName + "')]"));

			fc.utobj().printTestStep("Verify The Add Document");
			fc.utobj().clickElement(driver, pobj.listView);

			if (!fc.utobj()
					.getElementByXpath(driver, ".//*[@id='thumDtls']//a[contains(text () ,'" + documentTitle + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document title Folder List View");
			}

			if (!fc.utobj()
					.getElementByXpath(driver, ".//*[@id='thumDtls']//span[contains(text () , '" + briefSummary + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Brief Summary Of document at List View");
			}

			fc.utobj().clickElement(driver, pobj.thumbnailView);

			if (!fc.utobj()
					.getElementByXpath(driver, ".//*[@id='thums']//a[contains(text () ,'" + documentTitle + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document at List View");
			}
			fc.utobj().clickElement(driver, pobj.detailView);
			fc.utobj().isTextDisplayed(driver, documentTitle, "was not able to verify Document at List View");

			fc.utobj().printTestStep(
					"Verify Library search should be working after searching by on both the way exact search or normal search");

			if (!fc.utobj().isSelected(driver, pobj.searchExact)) {
				fc.utobj().clickElement(driver, pobj.searchExact);
			}
			fc.utobj().sendKeys(driver, pobj.searchAllDocument, documentTitle);

			boolean isExactSearchPresent = fc.utobj().assertLinkText(driver, documentTitle);
			if (!isExactSearchPresent) {
				fc.utobj().throwsException("Document is not searchable by exact search");
			}

			if (fc.utobj().isSelected(driver, pobj.searchExact)) {
				fc.utobj().clickElement(driver, pobj.searchExact);
			}
			fc.utobj().sendKeys(driver, pobj.searchAllDocument, documentTitle);

			boolean isNormalSearchPresent = fc.utobj().assertLinkText(driver, documentTitle);
			if (!isNormalSearchPresent) {
				fc.utobj().throwsException("Document is not searchable by normal search");
			}
			
			fc.utobj().printTestStep("Go to > Admin > Access Control > The Hub Searched Logs");
			SearchedLogs searchedLogs=new SearchedLogs();
			searchedLogs.setSearchedText(documentTitle);
			searchedLogs.setSearchedDate("Today");
			new AdminAccessControlSearchedLogsPageTest().searchedLogs(driver, searchedLogs);
			
			if (!fc.utobj().assertPageSource(driver, documentTitle)) {
				fc.utobj().throwsException("not able to search searched-logs");
			}
			String password="t0n1ght@123";
			String emailId="hubautomation@staffex.com";
			
			//Add Regional User
			AdminAreaRegionAddAreaRegionPageTest addRegionPage = new AdminAreaRegionAddAreaRegionPageTest();
			String regionName = fc.utobj().generateTestData("Test50regname");
			addRegionPage.addAreaRegion(driver, regionName);

			fc.utobj().printTestStep("Navigate To Admin > Users > Mange Regional Users Page");
			fc.utobj().printTestStep("Add Regional User");
			AdminUsersManageRegionalUsersAddRegionalUserPageTest regionalUserPage = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
			String userNameReg = fc.utobj().generateTestData("Test50reguser");
			regionalUserPage.addRegionalUser(driver, userNameReg, password, regionName, emailId);
			
			//Add Franchise User
			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			String regionNameFran = "Test50Region".concat(fc.utobj().generateRandomNumber());
			String storeType = "Test50StoreType".concat(fc.utobj().generateRandomNumber());
			String franchiseId = "Test50FranId".concat(fc.utobj().generateRandomNumber());

			AdminFranchiseLocationAddFranchiseLocationPageTest addFranchiseLocation = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String firstName = fc.utobj().generateTestData("Test50");
			franchiseId = addFranchiseLocation.addFranchiseLocation_AllWithFName(driver, franchiseId, regionNameFran,
					storeType, firstName);

			fc.utobj().printTestStep("Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest addFranchiseUser = new AdminUsersManageManageFranchiseUsersPageTest();
			String roleName = "Default Franchise Role";
			String userNameFran = fc.utobj().generateTestData(dataSet.get("userName"));
			addFranchiseUser.addFranchiseUser(driver, userNameFran, password, franchiseId, roleName, emailId);
			
			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login with Regional user");
			fc.loginpage().loginWithParameter(driver, userNameReg, password);
			
			fc.utobj().printTestStep("Verify Library document should search successfully from Exact search while login regional users");
			fc.hub().hub_common().theHubLibrarySubModule(driver);
			
			if (!fc.utobj().isSelected(driver, pobj.searchExact)) {
				fc.utobj().clickElement(driver, pobj.searchExact);
			}
			fc.utobj().sendKeys(driver, pobj.searchAllDocument, documentTitle);
			fc.utobj().clickElement(driver, pobj.searchItem);
			
			boolean isExactSearchPresentRegional = fc.utobj().assertLinkText(driver, documentTitle);
			if (!isExactSearchPresentRegional) {
				fc.utobj().throwsException("Document is not searchable by exact search login by regional user");
			}
			
			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login with Franchise user");
			fc.loginpage().loginWithParameter(driver, userNameFran, password);
			
			fc.utobj().printTestStep("Verify Library document should search successfully from Exact search while login franchise users");
			fc.hub().hub_common().theHubLibrarySubModule(driver);
			
			if (!fc.utobj().isSelected(driver, pobj.searchExact)) {
				fc.utobj().clickElement(driver, pobj.searchExact);
			}
			fc.utobj().sendKeys(driver, pobj.searchAllDocument, documentTitle);
			fc.utobj().clickElement(driver, pobj.searchItem);
			
			boolean isExactSearchPresentFranchise = fc.utobj().assertLinkText(driver, documentTitle);
			if (!isExactSearchPresentFranchise) {
				fc.utobj().throwsException("Document is not searchable by exact search login by Franchise user");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library","hub_download","hubf1121"})
	@TestCase(createdOn = "2018-06-13", updatedOn = "2017-06-13", testCaseDescription = "Verify the modified document at Folder [List view]", testCaseId = "TC_The_Hub_Library_Modify_Document_01")
	private void modifyDocuments01() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubLibraryPage pobj = new TheHubLibraryPage(driver);
			AdminTheHubLibraryPageTest libraryPage = new AdminTheHubLibraryPageTest();

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			libraryPage.addFolder(driver, library);

			fc.utobj().printTestStep("Navigate To The Hub > Library > Folder");
			fc.utobj().printTestStep("Add Document In Folder");
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setDocumentSubType(dataSet.get("documentSubType"));
			library.setFile(dataSet.get("fileName"));
			library.setDocumentType(dataSet.get("documentType"));
			library.setRecommendedDoc(false);
			addFolderDocument(driver,library);
			String tempDocTitle = documentTitle;

			fc.utobj().printTestStep("Modify Document At List View");
			fc.utobj().clickElement(driver, pobj.listView);
			libraryPage.actionImgIconListView(driver, documentTitle, "Modify");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			fc.utobj().sendKeys(driver, pobj.documentTitle, documentTitle);
			briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			fc.utobj().sendKeys(driver, pobj.briefSummary, briefSummary);
			if (!fc.utobj().isSelected(driver, pobj.documentType.get(0))) {
				fc.utobj().clickElement(driver, pobj.documentType.get(0));
			}
			fc.utobj().selectDropDown(driver, pobj.documentSubType, library.getDocumentSubType());
			String file = fc.utobj().getFilePathFromTestData("taskFile_03.pdf");
			fc.utobj().moveToElement(driver, pobj.fileClick);
			pobj.fileClick.sendKeys(file);

			if (!fc.utobj().isSelected(driver, pobj.thumbFileUploaded.get(1))) {
				fc.utobj().clickElement(driver, pobj.thumbFileUploaded.get(1));
			}

			if (!fc.utobj().isSelected(driver, pobj.accessibilityPublic)) {
				fc.utobj().clickElement(driver, pobj.accessibilityPublic);
			}

			fc.utobj().clickElement(driver, pobj.saveBtn);
			new HubCommonMethods().tryClickClose(driver, new AdminTheHubLibraryPage(driver));
			fc.utobj().switchFrameToDefault(driver);

			libraryPage.clickOverMoreLink(driver, folderName);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='folderDiv']//*[contains(text() , '" + folderName + "')]"));

			fc.utobj().printTestStep("Verify The Modified Document");
			fc.utobj().clickElement(driver, pobj.listView);

			if (!fc.utobj()
					.getElementByXpath(driver, ".//*[@id='thumDtls']//a[contains(text () ,'" + documentTitle + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document title Folder List View");
			}

			if (!fc.utobj()
					.getElementByXpath(driver, ".//*[@id='thumDtls']//span[contains(text () , '" + briefSummary + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Brief Summary Of document at List View");
			}

			fc.utobj().printTestStep("Verify version history details");
			libraryPage.actionImgIconListView(driver, documentTitle, "Version History");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			boolean isDocTitleVerisonHistory01 = fc.utobj().assertPageSource(driver, documentTitle);
			if (!isDocTitleVerisonHistory01) {
				fc.utobj().throwsException("Document is not present at version history cbox iframe");
			}

			boolean isDateLinkPresent01 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[@id='versionHistorytable']//td[contains(text(),'FranConnect Administrator')]/preceding-sibling::td/a[contains(text(),'_01')]");
			if (!isDateLinkPresent01) {
				fc.utobj().throwsException("version history with dates link is not present");
			}

			fc.utobj().clickElement(driver,
					".//*[@id='versionHistorytable']//td[contains(text(),'FranConnect Administrator')]/preceding-sibling::td/a[contains(text(),'_01')]");
			fc.utobj().printTestStep("Verify Document should be downloaded successfully from list view");
			boolean isFileFound01 = fc.utobj().isFileFound(new File(FranconnectUtil.config.get("downloadFilepath")),
					library.getFile());
			if (!isFileFound01) {
				fc.utobj().throwsException("Not able to download the file");
			}
			hub_common_method.deleteDownloadedFile(library);
			fc.commonMethods().Click_Close_Input_ByValue(driver);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj()
					.printTestStep("Verify previous documents should be moved to Archived section after modification");
			fc.hub().hub_common().adminTheHubLibraryPage(driver);
			fc.utobj().clickElement(driver, new AdminTheHubLibraryPage(driver).archivedDocumentsTab);
			new HubCommonMethods().sortByRecentlyUploadedDocuments(driver);

			boolean isDocumentTitlePresent = fc.utobj().assertPageSource(driver, tempDocTitle);
			if (!isDocumentTitlePresent) {
				fc.utobj().throwsException("Document is not present at archived documents tab");
			}

			fc.utobj().printTestStep("Verify version history details");
			libraryPage.actionImgIconListView(driver, tempDocTitle, "Version History");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			boolean isDocTitleVerisonHistory = fc.utobj().assertPageSource(driver, tempDocTitle);
			if (!isDocTitleVerisonHistory) {
				fc.utobj().throwsException("Document is not present at version history cbox iframe");
			}

			boolean isDateLinkPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					"//*[@id='versionHistorytable']//td[contains(text(),'FranConnect Administrator')]/preceding-sibling::td/a[contains(text(),'_01')]");
			if (!isDateLinkPresent) {
				fc.utobj().throwsException("version history with dates link is not present");
			}

			fc.commonMethods().Click_Close_Input_ByValue(driver);
			fc.utobj().switchFrameToDefault(driver);
			new HubCommonMethods().sortByRecentlyUploadedDocuments(driver);
			libraryPage.actionImgIconListView(driver, tempDocTitle, "Delete");

			fc.utobj().acceptAlertBox(driver);
			fc.utobj().printTestStep("Verify The Delete Document");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, documentTitle);
			if (isTextPresent == true) {
				fc.utobj().throwsException("was not able to delete document");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(groups = { "thehub", "hub_library","hubDownload0725","hubf1121"})
	@TestCase(createdOn = "2018-07-31", updatedOn = "2017-07-31", testCaseDescription = "Verify the .ai file type upload through library", testCaseId = "TC_The_Hub_Download_AI_File_01")
	private void addDocumentWithAIFileType() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminTheHubLibraryPageTest libraryPage = new AdminTheHubLibraryPageTest();

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";
			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			libraryPage.addFolder(driver, library);

			fc.utobj().printTestStep("Navigate To The Hub > Library > Folder");
			fc.utobj().printTestStep("Add Document In Folder");
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setDocumentSubType(dataSet.get("documentSubType"));
			library.setFile(dataSet.get("fileName"));
			library.setDocumentType(dataSet.get("documentType"));
			library.setRecommendedDoc(false);
			addFolderDocument(driver,library);
			
			fc.utobj().printTestStep("Search Added Document By Search All The Hub Items");
			hub_common_method.searchThehubSearchDocument(driver, library);
			fc.utobj().clickLink(driver, library.getDocumentTitle());
			
			/*if (!fc.utobj().isFileFound(new File(FranconnectUtil.config.get("downloadFilepath")), dataSet.get("fileName"))) {
				fc.utobj().throwsException("Not able to download .ai type document");
			}*/
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Verify The Modify Folder At The Hub > Library >  Folder ", testCaseId = "TC_235_Modify_Folder")
	private void modifyFolder() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubLibraryPage pobj = new TheHubLibraryPage(driver);
			AdminTheHubLibraryPageTest libraryPage = new AdminTheHubLibraryPageTest();

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			libraryPage.addFolder(driver, library);

			fc.utobj().printTestStep("Navigate To The Hub > Library > Folder");
			fc.hub().hub_common().theHubLibrarySubModule(driver);
			libraryPage.clickOverMoreLink(driver, folderName);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='folderDiv']//*[contains(text() , '" + folderName + "')]"));

			fc.utobj().printTestStep("Modify Folder");
			fc.utobj().clickElement(driver, pobj.modifyFolderLink);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.folderName, folderName);
			String htmlText = fc.utobj().generateTestData(dataSet.get("htmlText"));

			if (!fc.utobj().isSelected(driver, pobj.summaryForamte.get(1))) {
				fc.utobj().clickElement(driver, pobj.summaryForamte.get(1));
			}

			fc.utobj().switchFrameById(driver, "ta_ifr");

			Actions actions = new Actions(driver);
			actions.moveToElement(fc.utobj().getElement(driver, pobj.editorTextArea));
			actions.click();
			actions.sendKeys(htmlText);
			fc.utobj().logReportMsg("Entered Text", htmlText);
			actions.build().perform();
			fc.utobj().switchFrameToDefault(driver);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();",
					fc.utobj().getElementByXpath(driver, ".//*[@id='startRecords']"));

			if (!fc.utobj().isSelected(driver, pobj.roleBase.get(1))) {
				fc.utobj().clickElement(driver, pobj.roleBase.get(1));
			}

			fc.utobj().clickElement(driver, pobj.defaultRegionalRole);

			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			libraryPage.clickOverMoreLink(driver, folderName);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='folderDiv']//*[contains(text() , '" + folderName + "')]"));

			fc.utobj().printTestStep("Verify The Modify Folder");
			fc.utobj().isTextDisplayed(driver, folderName, "was not able to verify Folder Name");
			fc.utobj().isTextDisplayed(driver, htmlText, "was not able to verify Folder Summary");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Verify The Delete Folder At The Hub > Library >  Folder", testCaseId = "TC_236_Delete_Folder")
	private void deleteFolder() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubLibraryPage pobj = new TheHubLibraryPage(driver);
			AdminTheHubLibraryPageTest libraryPage = new AdminTheHubLibraryPageTest();

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			libraryPage.addFolder(driver, library);

			fc.utobj().printTestStep("Navigate To The Hub > Library > Folder");
			fc.hub().hub_common().theHubLibrarySubModule(driver);
			libraryPage.clickOverMoreLink(driver, folderName);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='folderDiv']//*[contains(text() , '" + folderName + "')]"));

			fc.utobj().printTestStep("Delete Folder");
			fc.utobj().clickElement(driver, pobj.deleteFolderLink);
			fc.utobj().acceptAlertBox(driver);

			libraryPage.clickOverMoreLink(driver, folderName);

			fc.utobj().printTestStep("Verify The Delete Folder");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, folderName);
			if (isTextPresent == true) {
				fc.utobj().throwsException("was not able to delete Folder");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library","hubf1121"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Verify The Add Sub Folder At The Hub > Library > Folder", testCaseId = "TC_237_Add_Sub_Folder")
	private void addSubFolder() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubLibraryPage pobj = new TheHubLibraryPage(driver);
			AdminTheHubLibraryPageTest libraryPage = new AdminTheHubLibraryPageTest();

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			libraryPage.addFolder(driver, library);

			fc.utobj().printTestStep("Navigate To The Hub > Library > Folder");
			fc.hub().hub_common().theHubLibrarySubModule(driver);
			libraryPage.clickOverMoreLink(driver, folderName);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='folderDiv']//*[contains(text() , '" + folderName + "')]"));

			fc.utobj().printTestStep("Add Sub Folder");
			WebElement element1 = fc.utobj().getElementByXpath(driver, ".//*[contains(text () , 'More Actions')]");
			fc.utobj().moveToElement(driver, element1);
			Actions builder = new Actions(driver);
			fc.utobj().logReport("Move To Element", element1);
			builder.moveToElement(element1).build().perform();
			fc.utobj().getElement(driver, pobj.addSubFolder).click();

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String folderName1 = fc.utobj().generateTestData(dataSet.get("folderName"));
			fc.utobj().sendKeys(driver, pobj.folderName, folderName1);
			String htmlText = fc.utobj().generateTestData(dataSet.get("htmlText"));

			if (!fc.utobj().isSelected(driver, pobj.summaryForamte.get(1))) {
				fc.utobj().clickElement(driver, pobj.summaryForamte.get(1));
			}

			fc.utobj().switchFrameById(driver, "ta_ifr");

			Actions actions = new Actions(driver);
			actions.moveToElement(fc.utobj().getElement(driver, pobj.editorTextArea));
			actions.click();
			actions.sendKeys(htmlText);
			fc.utobj().logReportMsg("Entered Text", htmlText);
			actions.build().perform();
			fc.utobj().switchFrameToDefault(driver);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='startRecords']"));

			if (!fc.utobj().isSelected(driver, pobj.roleBase.get(1))) {
				fc.utobj().clickElement(driver, pobj.roleBase.get(1));
			}

			fc.utobj().clickElementByJS(driver, fc.utobj().getElementByXpath(driver, ".//input[@value='4']"));
			fc.utobj().clickElement(driver, pobj.addBtn1);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Add Sub Folder");
			libraryPage.clickOverMoreLink(driver, folderName);

			WebElement element = fc.utobj().getElementByXpath(driver,
					".//*[@id='folderDiv']//*[contains(text() , '" + folderName + "')]");
			fc.utobj().clickElement(driver, element);
			fc.utobj().isTextDisplayed(driver, folderName, "was not able to verify Parent Folder");
			fc.utobj().isTextDisplayed(driver, folderName1, "was not able to verify child Folder");
			fc.utobj().isTextDisplayed(driver, folderSummary, "was not able to verify Summary of Parent Folder");

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='librarybody']//a[.='" + folderName1 + "']"));
			fc.utobj().isTextDisplayed(driver, folderName, "was not able to verify Parent Folder");
			fc.utobj().isTextDisplayed(driver, folderName1, "was not able to verify child Folder");
			fc.utobj().isTextDisplayed(driver, htmlText, "was not able to verify Summary of Child Folder");

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void addFolderDocument(WebDriver driver, Library library) throws Exception {

		try {
			TheHubLibraryPage pobj = new TheHubLibraryPage(driver);
			AdminTheHubLibraryPageTest libraryPage = new AdminTheHubLibraryPageTest();

			fc.hub().hub_common().theHubLibrarySubModule(driver);
			libraryPage.clickOverMoreLink(driver, library.getFolderName());
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='folderDiv']//*[contains(text() , '" + library.getFolderName() + "')]"));

			fc.utobj().clickElement(driver, pobj.addDocument);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			if (!fc.utobj().isSelected(driver, pobj.multipleDoc.get(1))) {
				fc.utobj().clickElement(driver, pobj.multipleDoc.get(1));
			}

			fc.utobj().sendKeys(driver, pobj.documentTitle, library.getDocumentTitle());
			fc.utobj().sendKeys(driver, pobj.briefSummary, library.getBriefSummary());

			String documentType = library.getDocumentType();

			if (documentType.equalsIgnoreCase("Upload File")) {
				fc.utobj().selectDropDown(driver, pobj.documentSubType, library.getDocumentSubType());

				String file = fc.utobj().getFilePathFromTestData(library.getFile());
				if (!fc.utobj().isSelected(driver, pobj.documentType.get(0))) {
					fc.utobj().clickElement(driver, pobj.documentType.get(0));
				}
				fc.utobj().moveToElement(driver, pobj.fileClick);
				pobj.fileClick.sendKeys(file);
			} else if (documentType.equalsIgnoreCase("Web Link")) {
				if (!fc.utobj().isSelected(driver, pobj.documentType.get(1))) {
					fc.utobj().clickElement(driver, pobj.documentType.get(1));
				}
				fc.utobj().sendKeys(driver, pobj.linkUrl, library.getLinkUrl());
			} else if (documentType.equalsIgnoreCase("HTML Page")) {

				if (!fc.utobj().isSelected(driver, pobj.documentType.get(2))) {
					fc.utobj().clickElement(driver, pobj.documentType.get(2));
				}
				fc.utobj().switchFrameById(driver, "htmlSummary_ifr");
				Actions actions = new Actions(driver);
				actions.moveToElement(fc.utobj().getElement(driver, pobj.editorTextArea));
				actions.click();
				actions.sendKeys(library.getEditorText());
				actions.build().perform();
				fc.utobj().switchFrameToDefault(driver);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
			}
			if (!fc.utobj().isSelected(driver, pobj.thumbFileUploaded.get(1))) {
				fc.utobj().clickElement(driver, pobj.thumbFileUploaded.get(1));
			}
			if (/*dataSet.get("recommendedDoc")*/library.getRecommendedDoc()==true) {
				fc.utobj().clickElement(driver, pobj.recommmendedDocumnet);
				fc.utobj().sendKeys(driver, pobj.recommendedExpDate, fc.utobj().getFutureDateUSFormat(4));
			} else if (/*dataSet.get("recommendedDoc").equalsIgnoreCase("No")*/library.getRecommendedDoc()==false) {
				if (fc.utobj().isSelected(driver, pobj.recommmendedDocumnet)) {
					fc.utobj().clickElement(driver, pobj.recommmendedDocumnet);
				}
			}
			if (!fc.utobj().isSelected(driver, pobj.accessibilityPublic)) {
				fc.utobj().clickElement(driver, pobj.accessibilityPublic);
			}
			fc.utobj().clickElementByJS(driver, pobj.addBtn);
			new HubCommonMethods().tryClickClose(driver, new AdminTheHubLibraryPage(driver));
			fc.utobj().switchFrameToDefault(driver);
			libraryPage.clickOverMoreLink(driver, library.getFolderName());
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='folderDiv']//*[contains(text() , '" + library.getFolderName() + "')]"));

		} catch (Exception e) {
			Reporter.log("Not able to add document in folder hub> library :"+e.getMessage());
			fc.utobj().throwsException("Not able to add document in folder hub> library :"+e.getMessage());
		}
	}

	@Test(groups = { "thehub", "hub_library", "hubdev0706","thehubdevTest" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Verify The Report Of Document At The Hub > Library >  Folder [Detail View]", testCaseId = "TC_238_Report_Document")
	private void reportDocumentActionImg01() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			TheHubLibraryPage pobj = new TheHubLibraryPage(driver);
			AdminTheHubLibraryPageTest libraryPage = new AdminTheHubLibraryPageTest();

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			libraryPage.addFolder(driver, library);

			fc.utobj().printTestStep("Navigate To The Hub > Library > Folder");
			fc.utobj().printTestStep("Add Document In Folder");
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setDocumentType(dataSet.get("documentType"));
			library.setDocumentSubType(dataSet.get("documentSubType"));
			library.setRecommendedDoc(false);
			library.setFile(dataSet.get("fileName"));
			addFolderDocument(driver,library);

			fc.utobj().printTestStep("Verify Document Details At Detail View");
			fc.utobj().clickElement(driver, pobj.detailView);
			fc.utobj().clickLink(driver, documentTitle);

			if (!fc.utobj()
					.getElementByXpath(driver, ".//*[@id='DtlsInner']//a[contains(text () ,'" + documentTitle + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document Title At Detail View Page");
			}
			if (!fc.utobj()
					.getElementByXpath(driver, ".//*[@id='DtlsInner']//*[contains(text () ,'" + briefSummary + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document's Brief Summary");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//*[@id='DtlsInner']//*[contains(text () ,'" + documentTitle
									+ "')]/..//following-sibling::td[.='" + fc.utobj().getCurrentDateUSFormat() + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Update Date");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//*[@id='DtlsInner']//*[contains(text () ,'" + documentTitle
					+ "')]/..//following-sibling::td[.='FranConnect Administrator']").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Parent Directory");
			}

			fc.utobj().printTestStep("Verify Report Of Document");
			libraryPage.actionImgIconDetailsView(driver, documentTitle, "Report");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().isTextDisplayed(driver, documentTitle, "Was not able to verify document Title");

			if (!fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , 'Brief Summary :')]/following-sibling::td[.='" + briefSummary + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Brief Summary :");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Uploaded By :')]/following-sibling::td[.='FranConnect Administrator']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Uploaded By :");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);
			
			
			fc.utobj().printTestStep("Verify Report option from document Action Menu at Index > List View");
			
			fc.utobj().clickElement(driver, pobj.listView);
			libraryPage.actionImgIconListView(driver, documentTitle, "Report");
			
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().isTextDisplayed(driver, documentTitle, "Was not able to verify document Title");

			if (!fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , 'Brief Summary :')]/following-sibling::td[.='" + briefSummary + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Brief Summary :");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Uploaded By :')]/following-sibling::td[.='FranConnect Administrator']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Uploaded By :");
			}
			
			fc.utobj().printTestStep("Verify the document download details");
			boolean isDocDownloadDetailsPresent=fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text(),'User Name')]/parent::*/following-sibling::tr/td[contains(text(),'FranConnect Administrator')]/preceding-sibling::td[contains(text(),'1')]");
			if (!isDocDownloadDetailsPresent) {
				fc.utobj().throwsException("Document Details is not present");
			}
			
			boolean isPrintButtonPresent=fc.utobj().isElementPresent(driver, pobj.adminLibraryPage(driver).printButton);
			if (!isPrintButtonPresent) {
				fc.utobj().throwsException("Print button is not present at Report cbox");
			}
			boolean isExportAsExcelButtonPresent=fc.utobj().isElementPresent(driver, pobj.adminLibraryPage(driver).exportAsExcelButton);
			if (!isExportAsExcelButtonPresent) {
				fc.utobj().throwsException("Export As Excel Button is not present at Report cbox");
			}
			boolean isCloseButtonPresent=fc.utobj().isElementPresent(driver, pobj.closeBtn);
			if (!isCloseButtonPresent) {
				fc.utobj().throwsException("Close Button is not present at Report cbox");
			}
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Verify The Modify Document At The Hub > Library >  Folder [Detail View]", testCaseId = "TC_239_Modify_Document")
	private void modifyDocumentActionImg01() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			TheHubLibraryPage pobj = new TheHubLibraryPage(driver);
			AdminTheHubLibraryPageTest libraryPage = new AdminTheHubLibraryPageTest();

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			libraryPage.addFolder(driver, library);

			fc.utobj().printTestStep("Navigate To The Hub > Library > Folder");
			fc.utobj().printTestStep("Add Document In Folder");
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String currentDate = fc.utobj().getCurrentDateUSFormat();
			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setDocumentType(dataSet.get("documentType"));
			library.setDocumentSubType(dataSet.get("documentSubType"));
			library.setFile(dataSet.get("fileName"));
			library.setRecommendedDoc(false);
			addFolderDocument(driver,library);

			fc.utobj().printTestStep("Modify Document At Detail View");
			fc.utobj().clickElement(driver, pobj.detailView);
			libraryPage.actionImgIconDetailsView(driver, documentTitle, "Modify");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			fc.utobj().sendKeys(driver, pobj.documentTitle, documentTitle);
			briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			fc.utobj().sendKeys(driver, pobj.briefSummary, briefSummary);

			if (!fc.utobj().isSelected(driver, pobj.documentType.get(0))) {
				fc.utobj().clickElement(driver, pobj.documentType.get(0));
			}

			fc.utobj().selectDropDown(driver, pobj.documentSubType, dataSet.get("documentSubType"));

			String file =fc.utobj().getFilePathFromTestData(dataSet.get("fileName"));

			fc.utobj().moveToElement(driver, pobj.fileClick);
			pobj.fileClick.sendKeys(file);

			if (!fc.utobj().isSelected(driver, pobj.thumbFileUploaded.get(1))) {
				fc.utobj().clickElement(driver, pobj.thumbFileUploaded.get(1));
			}

			if (!fc.utobj().isSelected(driver, pobj.accessibilityPublic)) {
				fc.utobj().clickElement(driver, pobj.accessibilityPublic);
			}

			fc.utobj().clickElement(driver, pobj.saveBtn);
			new HubCommonMethods().tryClickClose(driver, new AdminTheHubLibraryPage(driver));
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Modify Documenet At Detail View");

			fc.utobj().clickElement(driver, pobj.detailView);

			if (!fc.utobj()
					.getElementByXpath(driver, ".//*[@id='DtlsInner']//a[contains(text () ,'" + documentTitle + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document Title At Detail View Page");
			}
			if (!fc.utobj()
					.getElementByXpath(driver, ".//*[@id='DtlsInner']//*[contains(text () ,'" + briefSummary + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document's Brief Summary");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//*[@id='DtlsInner']//*[contains(text () ,'" + documentTitle
					+ "')]/..//following-sibling::td[.='" + currentDate + "']").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Update Date");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//*[@id='DtlsInner']//*[contains(text () ,'" + documentTitle
					+ "')]/..//following-sibling::td[.='FranConnect Administrator']").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Parent Directory");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Verify The Delete Document At The Hub > Library >  Folder [Detail View]", testCaseId = "TC_240_Delete_Document")
	private void deleteDocumentActionImg01() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			TheHubLibraryPage pobj = new TheHubLibraryPage(driver);
			AdminTheHubLibraryPageTest libraryPage = new AdminTheHubLibraryPageTest();

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "FranchiseUsers";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			libraryPage.addFolder(driver, library);

			fc.utobj().printTestStep("Navigate To The Hub > Library > Folder");
			fc.utobj().printTestStep("Add Document In Folder");
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setDocumentType(dataSet.get("documentType"));
			library.setLinkUrl(dataSet.get("linkUrl"));
			library.setRecommendedDoc(false);
			addFolderDocument(driver,library);

			fc.utobj().printTestStep("Delete Document At Detail View");
			fc.utobj().clickElement(driver, pobj.detailView);
			libraryPage.actionImgIconDetailsView(driver, documentTitle, "Delete");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Delete Document");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, documentTitle);
			if (isTextPresent == true) {
				fc.utobj().throwsException("was not able to delete document");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library" })
	@TestCase(createdOn = "2017-06-22", updatedOn = "2017-06-22", testCaseDescription = "Verify The Delete Document At The Hub > Library >  Folder [List View]", testCaseId = "TC_The_Hub_Folder_List_View_Delete_Document_01")
	private void deleteDocumentActionImgListView01() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			TheHubLibraryPage pobj = new TheHubLibraryPage(driver);
			AdminTheHubLibraryPageTest libraryPage = new AdminTheHubLibraryPageTest();

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "FranchiseUsers";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			libraryPage.addFolder(driver, library);

			fc.utobj().printTestStep("Navigate To The Hub > Library > Folder");
			fc.utobj().printTestStep("Add Document In Folder");
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setDocumentType(dataSet.get("documentType"));
			library.setLinkUrl(dataSet.get("linkUrl"));
			library.setRecommendedDoc(false);
			addFolderDocument(driver,library);

			fc.utobj().printTestStep("Delete Document At Detail View");
			fc.utobj().clickElement(driver, pobj.detailView);
			libraryPage.actionImgIconDetailsView(driver, documentTitle, "Delete");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Delete Document");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, documentTitle);
			if (isTextPresent == true) {
				fc.utobj().throwsException("was not able to delete document");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library","hub_0806" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Veriyf The Document Properties At The Hub > Library >  Folder [Detail View]", testCaseId = "TC_241_Document_Properties")
	private void documentPropertiesActionImg01() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			TheHubLibraryPage pobj = new TheHubLibraryPage(driver);
			AdminTheHubLibraryPageTest libraryPage = new AdminTheHubLibraryPageTest();

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			libraryPage.addFolder(driver, library);

			fc.utobj().printTestStep("Navigate To The Hub > Library > Folder");
			fc.utobj().printTestStep("Add Document In Folder");
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setDocumentType(dataSet.get("documentType"));
			library.setDocumentSubType(dataSet.get("documentSubType"));
			library.setFile(dataSet.get("fileName"));
			library.setRecommendedDoc(false);
			addFolderDocument(driver,library);

			fc.utobj().printTestStep("Verify The Document Properties At Detail View");
			fc.utobj().clickElement(driver, pobj.detailView);
			libraryPage.actionImgIconDetailsView(driver, documentTitle, "Document Properties");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			if (!fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , 'Document Title')]/following-sibling::td[.='" + documentTitle + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document Title");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Summary')]/following-sibling::td[.='" + briefSummary + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document's Brief Summary");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Document Type')]/following-sibling::td[contains(text () , '"
									+ dataSet.get("documentType") + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document Type");
			}
			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Document Subtype')]/following-sibling::td[contains(text () , '"
									+ dataSet.get("documentSubType") + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify DocumentSub Type");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Uploaded By')]/following-sibling::td[contains(text () , 'FranConnect Administrator')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify DocumentSub Type");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Recommended')]/following-sibling::td[contains(text () , '"
									+ dataSet.get("recommendedDoc") + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Recommended");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library" ,"thehubdev0703" ,"thehubdevTest","hub_0806","hubf1121"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Veriyf The Post Comment At The Hub > Library > Folder", testCaseId = "TC_242_Post_Comment")
	private void postCommentActionImg01() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			TheHubLibraryPage pobj = new TheHubLibraryPage(driver);
			AdminTheHubLibraryPageTest libraryPage = new AdminTheHubLibraryPageTest();

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			libraryPage.addFolder(driver, library);

			fc.utobj().printTestStep("Navigate To The Hub > Library > Folder");
			fc.utobj().printTestStep("Add Document In Folder");
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setDocumentTitle(documentTitle);
			library.setDocumentSubType(dataSet.get("documentSubType"));
			library.setDocumentType(dataSet.get("documentType"));
			library.setFile(dataSet.get("fileName"));
			library.setRecommendedDoc(false);
			addFolderDocument(driver, library);

			//To verify Delete Comment option from document Comment link from Folder > List View
			fc.utobj().printTestStep("verify Delete Comment option from document Comment link from Folder > List View");
			fc.utobj().clickElement(driver, pobj.listView);
			new HubCommonMethods().sortByRecentlyUploadedDocuments(driver);
			fc.utobj().printTestStep("Post Comment");
			String commentTextList = fc.utobj().generateTestData("Test Comment Text");
			library.setCommentText(commentTextList);
			hub_common_method.postCommentAtListView(driver, new AdminTheHubLibraryPage(driver), library);
			boolean isCommentTextPresent = fc.utobj().assertPageSource(driver, library.getCommentText());
			if (!isCommentTextPresent) {
				fc.utobj().throwsException("Not able to verify added Comment at List View");
			}
			
			fc.utobj().printTestStep("Delete Comment from List View");
			fc.utobj().clickElement(driver, pobj.deleteComment);
			fc.utobj().acceptAlertBox(driver);
			if(fc.utobj().assertNotInPageSource(driver, library.getCommentText())){
				fc.utobj().throwsException("Not able to delete posted comment from list view");
			}
			
			fc.utobj().printTestStep("Post Comment At Detail View");
			fc.utobj().clickElement(driver, pobj.detailView);
			fc.utobj().sleep();

			libraryPage.postCommentActionImg(driver, documentTitle, "Post Comment");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().isTextDisplayed(driver, documentTitle, "was not able to verify Document Title");
			fc.utobj().isTextDisplayed(driver, briefSummary, "was not able to verify Brief Summary");

			fc.utobj().clickElement(driver, pobj.comment);
			String commentText = fc.utobj().generateTestData(dataSet.get("commentText"));
			WebElement element12 = fc.utobj().getElementByXpath(driver, ".//*[contains(@id , 'ta')]");
			System.out.println(element12);
			fc.utobj().sendKeys(driver, element12, commentText);
			fc.utobj().clickElement(driver, pobj.post);

			fc.utobj().printTestStep("Verify The Post Comment");
			fc.utobj().isTextDisplayed(driver, commentText, "was not able to verify Comment Text");

			fc.commonMethods().Click_Close_Input_ByValue(driver);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify Post Comment option from document Comment link from Folder > List View");
			String commentTextList12=fc.utobj().generateTestData("Test Comment Text");
			library.setCommentText(commentTextList12);
			hub_common_method.postCommentAtListView(driver, new AdminTheHubLibraryPage(driver), library);
			
			if (!fc.utobj().assertPageSource(driver, library.getCommentText())) {
				fc.utobj().throwsException("Not able to verify comment text at folder list view");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "thehub0524", "hub_library","hubf1121"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Veriyf The Delete Comment AT The Hub > Library >  Folder [Detail View]", testCaseId = "TC_243_Delete_Comment")
	private void deleteComment02() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			TheHubLibraryPage pobj = new TheHubLibraryPage(driver);
			AdminTheHubLibraryPageTest libraryPage = new AdminTheHubLibraryPageTest();

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			libraryPage.addFolder(driver, library);

			fc.utobj().printTestStep("Navigate To The Hub > Library > Folder");
			fc.utobj().printTestStep("Add Document In Folder");
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setDocumentSubType(dataSet.get("documentSubType"));
			library.setDocumentType(dataSet.get("documentType"));
			library.setFile(dataSet.get("fileName"));
			library.setRecommendedDoc(false);
			
			addFolderDocument(driver,library);

			fc.utobj().printTestStep("Post Comment At Detail View");
			fc.utobj().clickElement(driver, pobj.detailView);
			libraryPage.postCommentActionImg(driver, documentTitle, "Post Comment");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().isTextDisplayed(driver, documentTitle, "was not able to verify Document Title");
			fc.utobj().isTextDisplayed(driver, briefSummary, "was not able to verify Brief Summary");

			fc.utobj().printTestStep("Delete Comment");
			fc.utobj().clickElement(driver, pobj.comment);
			String commentText = fc.utobj().generateTestData(dataSet.get("commentText"));
			WebElement element12 = fc.utobj().getElementByXpath(driver, ".//*[contains(@id , 'ta')]");
			System.out.println(element12);
			fc.utobj().sendKeys(driver, element12, commentText);
			fc.utobj().clickElement(driver, pobj.post);
			fc.utobj().clickElement(driver, pobj.deleteComment);
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Delete Comment");
			boolean isTextPresent = fc.utobj().assertNotInPageSource(driver, commentText);
			if (isTextPresent) {
				fc.utobj().throwsException("was not able to delete Comment");
			}
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Verify The Version History At The Hub > Library >  Folder [Detail View]", testCaseId = "TC_244_Version_History")
	public void versionHistory02() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			TheHubLibraryPage pobj = new TheHubLibraryPage(driver);
			AdminTheHubLibraryPageTest libraryPage = new AdminTheHubLibraryPageTest();

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			libraryPage.addFolder(driver, library);

			fc.utobj().printTestStep("Navigate To The Hub > Library > Folder");
			fc.utobj().printTestStep("Add Document In Folder");
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setDocumentSubType(dataSet.get("documentSubType"));
			library.setDocumentType(dataSet.get("documentType"));
			library.setFile(dataSet.get("fileName"));
			library.setRecommendedDoc(false);
			addFolderDocument(driver,library);

			fc.utobj().printTestStep("Verify The Version History At Detail View");
			fc.utobj().clickElement(driver, pobj.detailView);
			libraryPage.actionImgIconDetailsView(driver, documentTitle, "Version History");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Document Title')]/following-sibling::td[contains(text () , '"
									+ documentTitle + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document Title");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Brief Summary')]/following-sibling::td[contains(text () , '"
									+ briefSummary + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document's Brief Summary");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Verify The Delete Document At The Hub > Library >  Folder [Detail View]", testCaseId = "TC_245_Delete_Document")
	private void deleteDocument02() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			TheHubLibraryPage pobj = new TheHubLibraryPage(driver);
			AdminTheHubLibraryPageTest libraryPage = new AdminTheHubLibraryPageTest();

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			libraryPage.addFolder(driver, library);

			fc.utobj().printTestStep("Navigate To The Hub > Library > Folder");
			fc.utobj().printTestStep("Add Document In Folder");
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setDocumentSubType(dataSet.get("documentSubType"));
			library.setDocumentType(dataSet.get("documentType"));
			library.setFile(dataSet.get("fileName"));
			library.setRecommendedDoc(false);
			addFolderDocument(driver,library);

			fc.utobj().printTestStep("Delete Document At Detail View");
			fc.utobj().clickElement(driver, pobj.detailView);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='librarybody']//span[.='Delete']"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[.='" + documentTitle + "']/ancestor::tr/td/input"));
			fc.utobj().clickElement(driver, pobj.deleteBtn);
			fc.utobj().acceptAlertBox(driver);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Delete Document");
			libraryPage.clickOverMoreLink(driver, folderName);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='folderDiv']//*[contains(text() , '" + folderName + "')]"));

			fc.utobj().clickElement(driver, pobj.detailView);
			if (!fc.utobj().getElementByXpath(driver, ".//*[contains(text () , 'No documents found')]").isDisplayed()) {
				fc.utobj().throwsException("was not able to delete document");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library" ,"hubf1121"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Verify The Report Document The Hub > Library >  Folder [Thumbnail View]", testCaseId = "TC_246_Report_Document")
	private void reportDocument02() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			TheHubLibraryPage pobj = new TheHubLibraryPage(driver);
			AdminTheHubLibraryPageTest libraryPage = new AdminTheHubLibraryPageTest();

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			libraryPage.addFolder(driver, library);

			fc.utobj().printTestStep("Navigate To The Hub > Library > Folder");
			fc.utobj().printTestStep("Add Document In Folder");
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setDocumentSubType(dataSet.get("documentSubType"));
			library.setDocumentType(dataSet.get("documentType"));
			library.setFile(dataSet.get("fileName"));
			library.setRecommendedDoc(false);
			addFolderDocument(driver,library);
			fc.utobj().clickElement(driver, pobj.thumbnailView);

			if (!fc.utobj().getElementByXpath(driver, ".//*[@id='thums']//a[.='" + documentTitle + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document Title At Thumbnail View Page");
			}

			fc.utobj().printTestStep("Verify The Report Document At Thumbnail View");
			libraryPage.thumbnailOption(driver, documentTitle, "Report");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().isTextDisplayed(driver, documentTitle, "Was not able to verify document Title");

			if (!fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , 'Brief Summary :')]/following-sibling::td[.='" + briefSummary + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Brief Summary :");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Uploaded By :')]/following-sibling::td[.='FranConnect Administrator']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Uploaded By :");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library" ,"hubdev0709" ,"thehubdevTest"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Verify The Document Properties At The Hub > Library >  Folder [Thumbnail View]", testCaseId = "TC_247_Document_Properties")
	private void documentProperties02() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			TheHubLibraryPage pobj = new TheHubLibraryPage(driver);
			AdminTheHubLibraryPageTest libraryPage = new AdminTheHubLibraryPageTest();

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			libraryPage.addFolder(driver, library);

			fc.utobj().printTestStep("Navigate To The Hub > Library > Folder");
			fc.utobj().printTestStep("Add Document In Folder");
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setDocumentSubType(dataSet.get("documentSubType"));
			library.setDocumentType(dataSet.get("documentType"));
			library.setFile(dataSet.get("fileName"));
			library.setRecommendedDoc(false);
			String currentDate=fc.utobj().getCurrentDateUSFormat();
			library.setCurrentDate(currentDate);
			addFolderDocument(driver,library);
			fc.utobj().printTestStep("Verify The Document Properties At Thumbnail View");

			fc.utobj().clickElement(driver, pobj.thumbnailView);
			libraryPage.thumbnailOption(driver, documentTitle, "Document Properties");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			if (!fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , 'Document Title')]/following-sibling::td[.='" + documentTitle + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document Title");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Summary')]/following-sibling::td[.='" + briefSummary + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document's Brief Summary");
			}

			if (!fc.utobj().isElementPresent(driver, fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Document URL')]/following-sibling::td/a[contains(@href,'"+FranconnectUtil.config.get("buildUrl")+"')]"))) {
				fc.utobj().throwsException("Document URL is not present");
				
			}
			
			if (!fc.utobj().isElementPresent(driver, fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Active From')]/following-sibling::td[contains(text(),'"+library.getCurrentDate()+"')]"))) {
				fc.utobj().throwsException("Acttive From Label is not present");
				
			}
			
			if (!fc.utobj().isElementPresent(driver, fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Creation Date')]/following-sibling::td[contains(text(),'"+library.getCurrentDate()+"')]"))) {
				fc.utobj().throwsException("Acttive From Label is not present");
				
			}
			
			if (!fc.utobj().isElementPresent(driver, fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Last Modified')]/following-sibling::td[contains(text(),'"+library.getCurrentDate()+"')]"))) {
				fc.utobj().throwsException("Acttive From Label is not present");
				
			}
			
			if (!fc.utobj().isElementPresent(driver, fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Document Size ')]/following-sibling::td[contains(text(),'KB')]"))) {
				fc.utobj().throwsException("Acttive From Label is not present");
				
			}
			
			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Document Type')]/following-sibling::td[contains(text () , '"
									+ dataSet.get("documentType") + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document Type");
			}
			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Document Subtype')]/following-sibling::td[contains(text () , '"
									+ dataSet.get("documentSubType") + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify DocumentSub Type");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Uploaded By')]/following-sibling::td[contains(text () , 'FranConnect Administrator')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify DocumentSub Type");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Recommended')]/following-sibling::td[contains(text () , '"
									+ dataSet.get("recommendedDoc") + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Recommended");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Veriyf The Post Comment At The Hub > Library >  Folder [Thumbnail View]", testCaseId = "TC_248_Post_Comment")
	private void postComment05() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			TheHubLibraryPage pobj = new TheHubLibraryPage(driver);
			AdminTheHubLibraryPageTest libraryPage = new AdminTheHubLibraryPageTest();

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			libraryPage.addFolder(driver, library);

			fc.utobj().printTestStep("Navigate To The Hub > Library > Folder");
			fc.utobj().printTestStep("Add Document In Folder");
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setDocumentSubType(dataSet.get("documentSubType"));
			library.setDocumentType(dataSet.get("documentType"));
			library.setFile(dataSet.get("fileName"));
			library.setRecommendedDoc(false);
			addFolderDocument(driver,library);

			fc.utobj().printTestStep("Post Comment At Thumbnail View");
			fc.utobj().clickElement(driver, pobj.thumbnailView);
			libraryPage.thumbnailOption(driver, documentTitle, "Post Comment");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().isTextDisplayed(driver, documentTitle, "was not able to verify Document Title");
			fc.utobj().isTextDisplayed(driver, briefSummary, "was not able to verify Brief Summary");

			fc.utobj().clickElement(driver, pobj.comment);
			String commentText = fc.utobj().generateTestData(dataSet.get("commentText"));
			WebElement element12 = fc.utobj().getElementByXpath(driver, ".//*[contains(@id , 'ta')]");
			System.out.println(element12);
			fc.utobj().sendKeys(driver, element12, commentText);
			fc.utobj().clickElement(driver, pobj.post);

			fc.utobj().printTestStep("Verify The Post Comment");
			fc.utobj().isTextDisplayed(driver, commentText, "was not able to verify Comment Text");

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Verify The Delete Comment At The Hub > Library >  Folder [Thumbnail View]", testCaseId = "TC_249_Delete_Comment")
	public void deleteComment03() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			TheHubLibraryPage pobj = new TheHubLibraryPage(driver);
			AdminTheHubLibraryPageTest libraryPage = new AdminTheHubLibraryPageTest();

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			libraryPage.addFolder(driver, library);

			fc.utobj().printTestStep("Navigate To The Hub > Library > Folder");
			fc.utobj().printTestStep("Add Document In Folder");
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setDocumentSubType(dataSet.get("documentSubType"));
			library.setDocumentType(dataSet.get("documentType"));
			library.setFile(dataSet.get("fileName"));
			library.setRecommendedDoc(false);
			addFolderDocument(driver,library);

			fc.utobj().printTestStep("Post Comment At Thumbnail View");
			fc.utobj().clickElement(driver, pobj.thumbnailView);
			libraryPage.thumbnailOption(driver, documentTitle, "Post Comment");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().isTextDisplayed(driver, documentTitle, "was not able to verify Document Title");
			fc.utobj().isTextDisplayed(driver, briefSummary, "was not able to verify Brief Summary");

			fc.utobj().printTestStep("Delete Comment");
			fc.utobj().clickElement(driver, pobj.comment);
			String commentText = fc.utobj().generateTestData(dataSet.get("commentText"));
			WebElement element12 = fc.utobj().getElementByXpath(driver, ".//*[contains(@id , 'ta')]");
			System.out.println(element12);
			fc.utobj().sendKeys(driver, element12, commentText);
			fc.utobj().clickElement(driver, pobj.post);
			fc.utobj().clickElement(driver, pobj.deleteComment);
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Delete Comment");
			boolean isTextPresent = fc.utobj().assertNotInPageSource(driver, commentText);
			if (isTextPresent) {
				fc.utobj().throwsException("was not able to delete Comment");
			}
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Verify The Version History At The Hub > Library >  Folder [Thumbnail View].", testCaseId = "TC_250_Version_History")
	private void versionHistory03() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			TheHubLibraryPage pobj = new TheHubLibraryPage(driver);
			AdminTheHubLibraryPageTest libraryPage = new AdminTheHubLibraryPageTest();

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			libraryPage.addFolder(driver, library);

			fc.utobj().printTestStep("Navigate To The Hub > Library > Folder");
			fc.utobj().printTestStep("Add Document In Folder");
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setDocumentSubType(dataSet.get("documentSubType"));
			library.setDocumentType(dataSet.get("documentType"));
			library.setFile(dataSet.get("fileName"));
			library.setRecommendedDoc(false);
			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setDocumentSubType(dataSet.get("documentSubType"));
			library.setDocumentType(dataSet.get("documentType"));
			library.setFile(dataSet.get("fileName"));
			library.setRecommendedDoc(false);
			addFolderDocument(driver,library);

			fc.utobj().printTestStep("Verify The Version History At Thumbnail View");
			fc.utobj().clickElement(driver, pobj.thumbnailView);
			libraryPage.thumbnailOption(driver, documentTitle, "Version History");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Document Title')]/following-sibling::td[contains(text () , '"
									+ documentTitle + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document Title");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Brief Summary')]/following-sibling::td[contains(text () , '"
									+ briefSummary + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document's Brief Summary");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hubfail0420", "hub_library","hubf1121" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Verify The Delete Document At The Hub > Library >  Folder [Thumbnail View]", testCaseId = "TC_251_Delete_Document")
	private void deleteDocumentBtn02() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			TheHubLibraryPage pobj = new TheHubLibraryPage(driver);
			AdminTheHubLibraryPageTest libraryPage = new AdminTheHubLibraryPageTest();

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			libraryPage.addFolder(driver, library);

			fc.utobj().printTestStep("Navigate To The Hub > Library > Folder");
			fc.utobj().printTestStep("Add Document In Folder");
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setDocumentSubType(dataSet.get("documentSubType"));
			library.setDocumentType(dataSet.get("documentType"));
			library.setFile(dataSet.get("fileName"));
			library.setRecommendedDoc(false);
			addFolderDocument(driver,library);

			fc.utobj().printTestStep("Delete Comment At Thumbnail View");
			fc.utobj().clickElement(driver, pobj.thumbnailView);
			WebElement element = fc.utobj().getElementByXpath(driver, ".//*[@id='thums']//*[.='" + documentTitle + "']"
					+ "//following::table[@class='topLine pt5'][1]//td/a[contains(@onclick, 'deletedocument')]");
			fc.utobj().clickElement(driver, element);
			fc.utobj().acceptAlertBox(driver);

			libraryPage.clickOverMoreLink(driver, folderName);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='folderDiv']//*[contains(text() , '" + folderName + "')]"));
			fc.utobj().clickElement(driver, pobj.thumbnailView);

			fc.utobj().printTestStep("Verify The Delete Comment");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, documentTitle);
			if (isTextPresent == true) {
				fc.utobj().throwsException("was not able to delete Library Document");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-31", testCaseDescription = "Verify The Modify Document At The Hub > Library >  Folder [Thumbnail View]", testCaseId = "TC_252_Modify_Document")
	private void modifyDocumentBtn02() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			TheHubLibraryPage pobj = new TheHubLibraryPage(driver);
			AdminTheHubLibraryPageTest libraryPage = new AdminTheHubLibraryPageTest();

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			libraryPage.addFolder(driver, library);

			fc.utobj().printTestStep("Navigate To The Hub > Library > Folder");
			fc.utobj().printTestStep("Add Document In Folder");
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setDocumentSubType(dataSet.get("documentSubType"));
			library.setDocumentType(dataSet.get("documentType"));
			library.setFile(dataSet.get("fileName"));
			library.setRecommendedDoc(false);
			addFolderDocument(driver,library);

			fc.utobj().printTestStep("Modify The Document At Thumbnail View");
			fc.utobj().clickElement(driver, pobj.thumbnailView);

			WebElement element = fc.utobj().getElementByXpath(driver, ".//*[@id='thums']//*[.='" + documentTitle + "']"
					+ "//following::table[@class='topLine pt5'][1]//td/a/img[@title='Modify']");
			fc.utobj().clickElement(driver, element);

			fc.commonMethods().switch_cboxIframe_frameId(driver);

			documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			fc.utobj().sendKeys(driver, pobj.documentTitle, documentTitle);
			briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			fc.utobj().sendKeys(driver, pobj.briefSummary, briefSummary);

			if (!fc.utobj().isSelected(driver, pobj.documentType.get(0))) {
				fc.utobj().clickElement(driver, pobj.documentType.get(0));
			}

			fc.utobj().selectDropDown(driver, pobj.documentSubType, dataSet.get("documentSubType"));
			String file = fc.utobj().getFilePathFromTestData(dataSet.get("fileName"));

			fc.utobj().moveToElement(driver, pobj.fileClick);
			pobj.fileClick.sendKeys(file);

			if (!fc.utobj().isSelected(driver, pobj.thumbFileUploaded.get(1))) {
				fc.utobj().clickElement(driver, pobj.thumbFileUploaded.get(1));
			}

			if (!fc.utobj().isSelected(driver, pobj.accessibilityPublic)) {
				fc.utobj().clickElement(driver, pobj.accessibilityPublic);
			}

			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			libraryPage.clickOverMoreLink(driver, folderName);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='folderDiv']//*[contains(text() , '" + folderName + "')]"));

			fc.utobj().clickElement(driver, pobj.thumbnailView);
			if (!fc.utobj().getElementByXpath(driver, ".//*[@id='thums']//a[.='" + documentTitle + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document Title At Thumbnail View Page");
			}

			fc.utobj().printTestStep("Verify The Modify Document");
			libraryPage.thumbnailOption(driver, documentTitle, "Report");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().isTextDisplayed(driver, documentTitle, "Was not able to verify document Title");

			if (!fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , 'Brief Summary :')]/following-sibling::td[.='" + briefSummary + "']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Brief Summary :");
			}

			if (!fc.utobj()
					.getElementByXpath(driver,
							".//td[contains(text () , 'Uploaded By :')]/following-sibling::td[.='FranConnect Administrator']")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Uploaded By :");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehubTest22221", "hub_role", "hub_library","hubf1121" })
	@TestCase(createdOn = "2018-01-03", updatedOn = "2018-01-03", testCaseDescription = "Verify On deselecting Can view Library Privilege , Library section should not appear under the HUB Module By Corporate User", testCaseId = "TC_Hub_Library_Privilege_01")
	private void libraryPrivileges01() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			SearchUI search_page = new SearchUI(driver);
			AdminTheHubLibraryPageTest libraryPage = new AdminTheHubLibraryPageTest();

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "Regional";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			libraryPage.addFolder(driver, library);

			fc.utobj().printTestStep("Add Document In Folder");
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String currentDate = fc.utobj().getCurrentDateUSFormat();
			String afterDate = dataSet.get("afterDate");
			int val = Integer.parseInt(afterDate);
			String futureDate = fc.utobj().getFutureDateUSFormat(val);

			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setCurrentDate(currentDate);
			library.setFutureDate(futureDate);
			library.setLinkUrl(dataSet.get("linkUrl"));
			library.setDocumentType(dataSet.get("documentType"));
			library.setRecommendedDoc(true);

			libraryPage.addDocument(driver, library);

			fc.utobj().printTestStep("Go to admin > Roles > Create Corporate Role with Can View Library as deselected");

			AdminUsersRolesAddNewRolePageTest roles_page = new AdminUsersRolesAddNewRolePageTest();
			String roleType = dataSet.get("roleType");
			String roleName = fc.utobj().generateTestData("TcCorporateRole");
			String moduleName = "The Hub";
			String emailId = "hubautomation@staffex.com";

			Map<String, String> privileges = new HashMap<String, String>();
			privileges.put("Can View Library", "No");
			roles_page.addRolesWithModulesPrivileges(driver, roleType, roleName, moduleName, privileges, testCaseId);

			fc.utobj().printTestStep(
					"Navigate To Admin > Users > Manage Corporate Users > Add  Corporate User with added role");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporate_user = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser.setRole(roleName);
			corpUser = corporate_user.createDefaultUser(driver, corpUser);

			fc.utobj().printTestStep("Logged out from ADM User");
			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Log In with newly created Corporate user");
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());

			fc.utobj().printTestStep("Navigate To The Hub Module");
			fc.hub().hub_common().theHubModule(driver);

			fc.utobj().printTestStep("Verify Library sub module should not visible under hub");

			boolean isSubTabPresent = false;

			List<WebElement> listElement = driver.findElements(By.xpath(".//*[@id='module_intranet']/ul/li/a"));

			if (listElement.size() == 0) {
				fc.utobj().throwsException("No Sub module is present under hub module");
			} else if (listElement.size() > 0) {

				String[] arrayString = new String[listElement.size()];

				for (int i = 0; i < listElement.size(); i++) {
					arrayString[i] = listElement.get(i).getText().trim();
				}

				for (String string : arrayString) {

					if (string.equalsIgnoreCase(fc.utobj().translateString("Library"))) {
						isSubTabPresent = true;
					}
				}
			}

			if (isSubTabPresent) {
				fc.utobj().throwsException("Library sub module is visible under hub");
			}

			fc.utobj().printTestStep("Verify Library Document Should not be visible in Solr Search Result");

			boolean isSearchTrue = false;
			for (int i = 0; i < 3; i++) {
				if (isSearchTrue == false) {
					fc.utobj().sendKeys(driver, search_page.topSearchField, documentTitle);
					Thread.sleep(2000);
					if (i == 2) {
						Thread.sleep(2000);
					}
					fc.utobj().clickEnterOnElement(driver, search_page.topSearchField);

					try {
						isSearchTrue = fc.utobj().isElementPresent(driver
								.findElements(By.xpath(".//custom[contains(text () , '" + documentTitle + "')]")));
					} catch (Exception e) {
						Reporter.log(e.getMessage());
						System.out.println(e.getMessage());
					}
				}
			}

			if (isSearchTrue) {
				fc.utobj().throwsException("was not able to search Document By Top Search");
			}
			fc.utobj().refresh(driver);

			fc.utobj().printTestStep("Library Document should not visible in What's New Subtab");
			fc.hub().hub_common().theHubWhatsNew(driver);

			boolean isAssert = fc.utobj().assertPageSource(driver, documentTitle);

			if (isAssert) {
				fc.utobj().throwsException("Library Document is visible at Whats New Subtab");
			}

			TheHubNewsPage pobj = new TheHubNewsPage(driver);
			fc.utobj().printTestStep(
					"Verify Library Document should not visible search by Search All The Hub Items search filter");
			fc.utobj().sendKeys(driver, pobj.searchAllDocument, documentTitle);
			fc.utobj().clickElement(driver, pobj.searchItem);
			pobj.searchAllDocument.clear();

			boolean isAssertNews = fc.utobj().assertPageSource(driver, documentTitle);

			if (isAssertNews) {
				fc.utobj().throwsException(
						"Library Document is visible search by Search All The Hub Items search filter");
			}

			fc.utobj().printTestStep("Varify Library Document should not visible at Admin > Library Logs Page");
			fc.hub().hub_common().adminTheHubLibraryLogsPage(driver);

			fc.utobj().sendKeys(driver, pobj.searchCriteria, documentTitle);
			fc.utobj().clickElement(driver, pobj.searchButton);
			pobj.searchCriteria.clear();

			boolean isLibrary = fc.utobj().assertPageSource(driver, documentTitle);
			if (isLibrary) {
				fc.utobj().throwsException("Library Document is visible at Admin > Library Logs Page");
			}

			fc.utobj().printTestStep(
					"Verify Library Document should not visible At Recent/Most/Recommended on home page");
			fc.hub().hub_common().theHubHome(driver);

			boolean isLib = fc.utobj().assertPageSource(driver, documentTitle);

			if (isLib) {
				fc.utobj().throwsException("Document is visible at cent/Most/Recommended on home page");
			}

			fc.utobj().printTestStep("Library Link Should not visible At Admin Page");

			boolean isLnkPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[@id='Intranet']//a[@qat_adminlink='Library']");

			if (isLnkPresent) {
				fc.utobj().throwsException("Library Link is visible At Admin Page");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehubTest22221", "hub_role", "hub_library","hubf1121" })
	@TestCase(createdOn = "2018-01-03", updatedOn = "2018-01-03", testCaseDescription = "Verify On deselecting Can view Library Privilege , Library section should not appear under the HUB Module By Divisional User", testCaseId = "TC_Hub_Library_Privilege_02")
	private void libraryPrivileges02() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			SearchUI search_page = new SearchUI(driver);
			AdminTheHubLibraryPageTest libraryPage = new AdminTheHubLibraryPageTest();

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "Regional";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			libraryPage.addFolder(driver, library);

			fc.utobj().printTestStep("Add Document In Folder");
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String currentDate = fc.utobj().getCurrentDateUSFormat();
			String afterDate = dataSet.get("afterDate");
			int val = Integer.parseInt(afterDate);
			String futureDate = fc.utobj().getFutureDateUSFormat(val);

			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setCurrentDate(currentDate);
			library.setFutureDate(futureDate);
			library.setLinkUrl(dataSet.get("linkUrl"));
			library.setDocumentType(dataSet.get("documentType"));
			library.setRecommendedDoc(true);

			libraryPage.addDocument(driver, library);
			fc.utobj()
					.printTestStep("Go to admin > Roles > Create Divisional Role with Can View Library as deselected");

			AdminUsersRolesAddNewRolePageTest roles_page = new AdminUsersRolesAddNewRolePageTest();
			String roleType = dataSet.get("roleType");
			String roleName = fc.utobj().generateTestData("TcDivisionalRole");
			String moduleName = "The Hub";
			String emailId = "hubautomation@staffex.com";
			String userName = fc.utobj().generateTestData("du");
			String password = "t0n1ght@123";
			String divisionName = fc.utobj().generateTestData("Testdiv");

			Map<String, String> privileges = new HashMap<String, String>();
			privileges.put("Can View Library", "No");
			roles_page.addRolesWithModulesPrivileges(driver, roleType, roleName, moduleName, privileges, testCaseId);

			fc.utobj().printTestStep(
					"Navigate To Admin > Users > Manage Divisional Users > Add  Divisional User with added role");
			AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest divisional_user = new AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest();
			divisional_user.addDivisionalUserWithRole(driver, userName, divisionName, roleName, password, emailId);

			fc.utobj().printTestStep("Logged out from ADM User");
			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Log In with newly created Divisional user");
			fc.loginpage().loginWithParameter(driver, userName, password);

			fc.utobj().printTestStep("Navigate To The Hub Module");
			fc.hub().hub_common().theHubModule(driver);

			fc.utobj().printTestStep("Verify Library sub module should not visible under hub");

			boolean isSubTabPresent = false;

			List<WebElement> listElement = driver.findElements(By.xpath(".//*[@id='module_intranet']/ul/li/a"));

			if (listElement.size() == 0) {
				fc.utobj().throwsException("No Sub module is present under hub module");
			} else if (listElement.size() > 0) {

				String[] arrayString = new String[listElement.size()];

				for (int i = 0; i < listElement.size(); i++) {
					arrayString[i] = listElement.get(i).getText().trim();
				}

				for (String string : arrayString) {

					if (string.equalsIgnoreCase(fc.utobj().translateString("Library"))) {
						isSubTabPresent = true;
					}
				}
			}

			if (isSubTabPresent) {
				fc.utobj().throwsException("Library sub module is visible under hub");
			}

			fc.utobj().printTestStep("Verify Library Document Should not be visible in Solr Search Result");

			boolean isSearchTrue = false;
			for (int i = 0; i < 3; i++) {
				if (isSearchTrue == false) {
					fc.utobj().sendKeys(driver, search_page.topSearchField, documentTitle);
					Thread.sleep(2000);
					if (i == 2) {
						Thread.sleep(2000);
					}
					fc.utobj().clickEnterOnElement(driver, search_page.topSearchField);

					try {
						isSearchTrue = fc.utobj().isElementPresent(driver
								.findElements(By.xpath(".//custom[contains(text () , '" + documentTitle + "')]")));
					} catch (Exception e) {
						Reporter.log(e.getMessage());
						System.out.println(e.getMessage());
					}
				}
			}

			if (isSearchTrue) {
				fc.utobj().throwsException("was not able to search Document By Top Search");
			}
			fc.utobj().refresh(driver);

			fc.utobj().printTestStep("Library Document should not visible in What's New Subtab");
			fc.hub().hub_common().theHubWhatsNew(driver);

			boolean isAssert = fc.utobj().assertPageSource(driver, documentTitle);

			if (isAssert) {
				fc.utobj().throwsException("Library Document is visible at Whats New Subtab");
			}

			TheHubNewsPage pobj = new TheHubNewsPage(driver);
			fc.utobj().printTestStep(
					"Verify Library Document should not visible search by Search All The Hub Items search filter");
			fc.utobj().sendKeys(driver, pobj.searchAllDocument, documentTitle);
			fc.utobj().clickElement(driver, pobj.searchItem);
			pobj.searchAllDocument.clear();

			boolean isAssertNews = fc.utobj().assertPageSource(driver, documentTitle);

			if (isAssertNews) {
				fc.utobj().throwsException(
						"Library Document is visible search by Search All The Hub Items search filter");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehubTest22221", "hub_role", "hub_library","hubf1121" })
	@TestCase(createdOn = "2018-01-03", updatedOn = "2018-01-03", testCaseDescription = "Verify On deselecting Can view Library Privilege , Library section should not appear under the HUB Module By Regional User", testCaseId = "TC_Hub_Library_Privilege_03")
	private void libraryPrivileges03() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			SearchUI search_page = new SearchUI(driver);
			AdminTheHubLibraryPageTest libraryPage = new AdminTheHubLibraryPageTest();

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "Regional";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			libraryPage.addFolder(driver, library);

			fc.utobj().printTestStep("Add Document In Folder");
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String currentDate = fc.utobj().getCurrentDateUSFormat();
			String afterDate = dataSet.get("afterDate");
			int val = Integer.parseInt(afterDate);
			String futureDate = fc.utobj().getFutureDateUSFormat(val);

			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setCurrentDate(currentDate);
			library.setFutureDate(futureDate);
			library.setLinkUrl(dataSet.get("linkUrl"));
			library.setDocumentType(dataSet.get("documentType"));
			library.setRecommendedDoc(true);

			libraryPage.addDocument(driver, library);

			fc.utobj().printTestStep("Go to admin > Roles > Create Regional Role with Can View Library as deselected");

			AdminUsersRolesAddNewRolePageTest roles_page = new AdminUsersRolesAddNewRolePageTest();
			String roleType = dataSet.get("roleType");
			String roleName = fc.utobj().generateTestData("TcRegionalRole");
			String moduleName = "The Hub";
			String emailId = "hubautomation@staffex.com";
			String userName = fc.utobj().generateTestData("ru");
			String password = "t0n1ght@123";
			String regionName = fc.utobj().generateTestData("TestReg");

			Map<String, String> privileges = new HashMap<String, String>();
			privileges.put("Can View Library", "No");
			roles_page.addRolesWithModulesPrivileges(driver, roleType, roleName, moduleName, privileges, testCaseId);

			fc.utobj().printTestStep(
					"Navigate To Admin > Users > Manage Regional Users > Add  Regional User with added role");
			AdminUsersManageRegionalUsersAddRegionalUserPageTest region_page = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
			region_page.addRegionalUserWithRole(driver, userName, password, regionName, roleName, emailId);

			fc.utobj().printTestStep("Logged out from ADM User");
			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Log In with newly created Regional user");
			fc.loginpage().loginWithParameter(driver, userName, password);

			fc.utobj().printTestStep("Navigate To The Hub Module");
			fc.hub().hub_common().theHubModule(driver);

			fc.utobj().printTestStep("Verify Library sub module should not visible under hub");

			boolean isSubTabPresent = false;

			List<WebElement> listElement = driver.findElements(By.xpath(".//*[@id='module_intranet']/ul/li/a"));

			if (listElement.size() == 0) {
				fc.utobj().throwsException("No Sub module is present under hub module");
			} else if (listElement.size() > 0) {

				String[] arrayString = new String[listElement.size()];

				for (int i = 0; i < listElement.size(); i++) {
					arrayString[i] = listElement.get(i).getText().trim();
				}

				for (String string : arrayString) {

					if (string.equalsIgnoreCase(fc.utobj().translateString("Library"))) {
						isSubTabPresent = true;
					}
				}
			}

			if (isSubTabPresent) {
				fc.utobj().throwsException("Library sub module is visible under hub");
			}

			fc.utobj().printTestStep("Verify Library Document Should not be visible in Solr Search Result");

			boolean isSearchTrue = false;
			for (int i = 0; i < 3; i++) {
				if (isSearchTrue == false) {
					fc.utobj().sendKeys(driver, search_page.topSearchField, documentTitle);
					Thread.sleep(2000);
					if (i == 2) {
						Thread.sleep(2000);
					}
					fc.utobj().clickEnterOnElement(driver, search_page.topSearchField);

					try {
						isSearchTrue = fc.utobj().isElementPresent(driver
								.findElements(By.xpath(".//custom[contains(text () , '" + documentTitle + "')]")));
					} catch (Exception e) {
						Reporter.log(e.getMessage());
						System.out.println(e.getMessage());
					}
				}
			}

			if (isSearchTrue) {
				fc.utobj().throwsException("was not able to search Document By Top Search");
			}
			fc.utobj().refresh(driver);

			fc.utobj().printTestStep("Library Document should not visible in What's New Subtab");
			fc.hub().hub_common().theHubWhatsNew(driver);

			boolean isAssert = fc.utobj().assertPageSource(driver, documentTitle);

			if (isAssert) {
				fc.utobj().throwsException("Library Document is visible at Whats New Subtab");
			}

			TheHubNewsPage pobj = new TheHubNewsPage(driver);
			fc.utobj().printTestStep(
					"Verify Library Document should not visible search by Search All The Hub Items search filter");
			fc.utobj().sendKeys(driver, pobj.searchAllDocument, documentTitle);
			fc.utobj().clickElement(driver, pobj.searchItem);
			pobj.searchAllDocument.clear();

			boolean isAssertNews = fc.utobj().assertPageSource(driver, documentTitle);

			if (isAssertNews) {
				fc.utobj().throwsException(
						"Library Document is visible search by Search All The Hub Items search filter");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehubTest22221", "hub_role", "hub_library" ,"hubf1121"})
	@TestCase(createdOn = "2018-01-03", updatedOn = "2018-01-03", testCaseDescription = "Verify On deselecting Can view Library Privilege , Library section should not appear under the HUB Module By Franchise User", testCaseId = "TC_Hub_Library_Privilege_04")
	private void libraryPrivileges04() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			SearchUI search_page = new SearchUI(driver);
			AdminTheHubLibraryPageTest libraryPage = new AdminTheHubLibraryPageTest();

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "Regional";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			libraryPage.addFolder(driver, library);

			fc.utobj().printTestStep("Add Document In Folder");
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String currentDate = fc.utobj().getCurrentDateUSFormat();
			String afterDate = dataSet.get("afterDate");
			int val = Integer.parseInt(afterDate);
			String futureDate = fc.utobj().getFutureDateUSFormat(val);

			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setCurrentDate(currentDate);
			library.setFutureDate(futureDate);
			library.setLinkUrl(dataSet.get("linkUrl"));
			library.setDocumentType(dataSet.get("documentType"));
			library.setRecommendedDoc(true);

			libraryPage.addDocument(driver, library);

			fc.utobj().printTestStep("Go to admin > Roles > Create franchise Role with Can View Library as deselected");

			AdminUsersRolesAddNewRolePageTest roles_page = new AdminUsersRolesAddNewRolePageTest();
			String roleType = dataSet.get("roleType");
			String roleName = fc.utobj().generateTestData("TcFranchiseRole");
			String moduleName = "The Hub";
			String emailId = "hubautomation@staffex.com";
			String userName = fc.utobj().generateTestData("fu");
			String password = "t0n1ght@123";
			String regionName = fc.utobj().generateTestData("TestReg");
			String storeType = fc.utobj().generateTestData("TestStore");
			String franchiseName = fc.utobj().generateTestData("TestFran");
			String centerName = fc.utobj().generateTestData("center");
			String openingDate = fc.utobj().getCurrentDateUSFormat();
			String ownerFirstName = fc.utobj().generateTestData("Testfname");
			String ownerLastName = fc.utobj().generateTestData("Testlname");

			Map<String, String> privileges = new HashMap<String, String>();
			privileges.put("Can View Library", "No");
			roles_page.addRolesWithModulesPrivileges(driver, roleType, roleName, moduleName, privileges, testCaseId);

			fc.utobj().printTestStep(
					"Navigate To Admin > Franchise Location > Manage Franchise Locations > Add Franchise Location");
			AdminFranchiseLocationAddFranchiseLocationPageTest location_page = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String franchiseId = location_page.addFranchiseLocation(driver, regionName, storeType, franchiseName,
					centerName, openingDate, ownerFirstName, ownerLastName, FranconnectUtil.config);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Franchise Users > Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest franchise_page = new AdminUsersManageManageFranchiseUsersPageTest();
			franchise_page.addFranchiseUser(driver, userName, password, franchiseId, roleName, emailId);

			fc.utobj().printTestStep("Logged out from ADM User");
			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Log In with newly created franchise user");
			fc.loginpage().loginWithParameter(driver, userName, password);

			fc.utobj().printTestStep("Navigate To The Hub Module");
			fc.hub().hub_common().theHubModule(driver);

			fc.utobj().printTestStep("Verify Library sub module should not visible under hub");

			boolean isSubTabPresent = false;

			List<WebElement> listElement = driver.findElements(By.xpath(".//*[@id='module_intranet']/ul/li/a"));

			if (listElement.size() == 0) {
				fc.utobj().throwsException("No Sub module is present under hub module");
			} else if (listElement.size() > 0) {

				String[] arrayString = new String[listElement.size()];

				for (int i = 0; i < listElement.size(); i++) {
					arrayString[i] = listElement.get(i).getText().trim();
				}

				for (String string : arrayString) {

					if (string.equalsIgnoreCase(fc.utobj().translateString("Library"))) {
						isSubTabPresent = true;
					}
				}
			}

			if (isSubTabPresent) {
				fc.utobj().throwsException("Library sub module is visible under hub");
			}

			fc.utobj().printTestStep("Verify Library Document Should not be visible in Solr Search Result");

			boolean isSearchTrue = false;
			for (int i = 0; i < 3; i++) {
				if (isSearchTrue == false) {
					fc.utobj().sendKeys(driver, search_page.topSearchField, documentTitle);
					Thread.sleep(2000);
					if (i == 2) {
						Thread.sleep(2000);
					}
					fc.utobj().clickEnterOnElement(driver, search_page.topSearchField);

					try {
						isSearchTrue = fc.utobj().isElementPresent(driver
								.findElements(By.xpath(".//custom[contains(text () , '" + documentTitle + "')]")));
					} catch (Exception e) {
						Reporter.log(e.getMessage());
						System.out.println(e.getMessage());
					}
				}
			}

			if (isSearchTrue) {
				fc.utobj().throwsException("was not able to search Document By Top Search");
			}
			fc.utobj().refresh(driver);

			fc.utobj().printTestStep("Library Document should not visible in What's New Subtab");
			fc.hub().hub_common().theHubWhatsNew(driver);

			boolean isAssert = fc.utobj().assertPageSource(driver, documentTitle);

			if (isAssert) {
				fc.utobj().throwsException("Library Document is visible at Whats New Subtab");
			}

			TheHubNewsPage pobj = new TheHubNewsPage(driver);
			fc.utobj().printTestStep(
					"Verify Library Document should not visible search by Search All The Hub Items search filter");
			fc.utobj().sendKeys(driver, pobj.searchAllDocument, documentTitle);
			fc.utobj().clickElement(driver, pobj.searchItem);
			pobj.searchAllDocument.clear();

			boolean isAssertNews = fc.utobj().assertPageSource(driver, documentTitle);

			if (isAssertNews) {
				fc.utobj().throwsException(
						"Library Document is visible search by Search All The Hub Items search filter");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_library","hub_download","hubf1121"})
	@TestCase(createdOn = "2018-05-03", updatedOn = "2018-05-03", testCaseDescription = "Verify to upload eps file under Library", testCaseId = "TC_The_Hub_Library_Upload_File_01")
	private void uploadEpsFile() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);
			Library library = new Library();
			AdminTheHubLibraryPageTest libraryPage = new AdminTheHubLibraryPageTest();
			library.setFile(dataSet.get("fileName"));
			File downloadPath = new File(FranconnectUtil.config.get("downloadFilepath"));

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			libraryPage.addFolder(driver, library);

			fc.utobj().printTestStep("Add Document");
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String currentDate = fc.utobj().getCurrentDateUSFormat();
			String futureDate = fc.utobj().getFutureDateUSFormat(2);
			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setCurrentDate(currentDate);
			library.setFutureDate(futureDate);
			library.setDocumentType(dataSet.get("documentType"));
			library.setRecommendedDoc(false);
			libraryPage.addDocument(driver, library);

			fc.utobj().clickLink(driver, documentTitle);
			fc.utobj().printTestStep("Verify The Downloaded File");

			boolean isFileFound = fc.utobj().isFileFound(downloadPath, library.getFile());
			if (!isFileFound) {
				fc.utobj().throwsException("Not able to download File");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(groups = { "thehub", "hub_library","hubf1121"})
	@TestCase(createdOn = "2018-06-12", updatedOn = "2018-06-12", testCaseDescription = "Verify The Added Document At The Hub > Library", testCaseId = "TC_The_Hub_Add_Document_02")
	private void documentPropertiesFolder01() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			AdminTheHubLibraryPageTest library_page=new AdminTheHubLibraryPageTest();
			AdminTheHubLibraryPage pobj = new AdminTheHubLibraryPage(driver);
			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";

			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			library_page.addFolder(driver, library);

			fc.utobj().printTestStep("Add Document");
			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String currentDate = fc.utobj().getCurrentDateUSFormat();
			String futureDate = fc.utobj().getFutureDateUSFormat(2);

			library.setDocumentTitle(documentTitle);
			library.setBriefSummary(briefSummary);
			library.setCurrentDate(currentDate);
			library.setFutureDate(futureDate);
			library.setDocumentType(dataSet.get("documentType"));
			library.setEditorText(fc.utobj().generateTestData("Testeditortext"));
			library.setRecommendedDoc(false);
			addFolderDocument(driver, library);

			fc.utobj().printTestStep("Verify Document Properties option from document Option from Folder > List View");
			library_page.clickOverMoreLink(driver, folderName);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//span[contains(text(),'" + folderName + "')]"));
			fc.utobj().clickElement(driver, pobj.listView);
			hub_common_method.sortByRecentlyUploadedDocuments(driver);
			String parentWindow = driver.getWindowHandle();
			library_page.actionImgIconListView(driver, documentTitle, "Document Properties");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver,
					".//td[contains(text () , 'Document URL')]/following-sibling::td/a[contains(@href,'"
							+ FranconnectUtil.config.get("buildUrl") + "')]");

			fc.utobj().printTestStep("Verify Html Document window should be opened");

			boolean isHtmlEditortTextPresent = false;
			Set<String> windowsHandles = driver.getWindowHandles();
			for (String window : windowsHandles) {
				if (!window.equalsIgnoreCase(parentWindow)) {
					driver.switchTo().window(window);
					if (driver.getCurrentUrl().contains("htmlPageView.jsp")) {
						isHtmlEditortTextPresent = fc.utobj().assertPageSource(driver, library.getEditorText());
						driver.close();
						break;
					}
				}
			}
			driver.switchTo().window(parentWindow);
			if (!isHtmlEditortTextPresent) {
				fc.utobj().throwsException("HTML documents is not visible");
			}
			
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			//Modify And Set Upload Document
			library_page.clickOverMoreLink(driver, folderName);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//span[contains(text(),'" + folderName + "')]"));
			fc.utobj().clickElement(driver, pobj.listView);
			fc.utobj().printTestStep("Modify The Document and changed document type to upload");
			library_page.actionImgIconListView(driver, documentTitle, "Modify");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			if (!fc.utobj().isSelected(driver, pobj.documentType.get(0))) {
				fc.utobj().clickElement(driver, pobj.documentType.get(0));
			}

			fc.utobj().selectDropDown(driver, pobj.documentSubType, "PDF");
			fc.utobj().moveToElement(driver, pobj.fileClick);

			String fileName = "taskFile_07.pdf";
			String filePath=fc.utobj().getFilePathFromTestData(fileName);
			
			pobj.fileClick.sendKeys(filePath);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify After clicking over Document url document should be downloaded");
			hub_common_method.sortByRecentlyUploadedDocuments(driver);
			library_page.actionImgIconListView(driver, library.getDocumentTitle(), "Document Properties");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver,
					".//td[contains(text () , 'Document URL')]/following-sibling::td/a[contains(@href,'"
							+ FranconnectUtil.config.get("buildUrl") + "')]");

			fc.utobj().printTestStep("Verify The Downloaded File");
			boolean isFileFound = fc.utobj().isFileFound(new File(FranconnectUtil.config.get("downloadFilepath")),
					fileName);
			if (!isFileFound) {
				fc.utobj().throwsException("Document not downled from document properties url");
			}
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			//Modify And Set Link Document
			fc.utobj().printTestStep("Verify After clicking over Document url Link Url window should be open");
			library_page.clickOverMoreLink(driver, folderName);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//span[contains(text(),'" + folderName + "')]"));
			fc.utobj().clickElement(driver, pobj.listView);

			fc.utobj().printTestStep("Modify The Document and changed document type to Web Url");
			library_page.actionImgIconListView(driver, documentTitle, "Modify");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			if (!fc.utobj().isSelected(driver, pobj.documentType.get(1))) {
				fc.utobj().clickElement(driver, pobj.documentType.get(1));
			}

			String lnkUrl = "https://in.yahoo.com/";
			fc.utobj().sendKeys(driver, pobj.linkUrl, lnkUrl);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Test Web Url");
			library_page.clickOverMoreLink(driver, folderName);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//span[contains(text(),'" + folderName + "')]"));
			fc.utobj().clickElement(driver, pobj.listView);

			hub_common_method.sortByRecentlyUploadedDocuments(driver);
			library_page.actionImgIconListView(driver, library.getDocumentTitle(), "Document Properties");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver,
					".//td[contains(text () , 'Document URL')]/following-sibling::td/a[contains(@href,'"
							+ FranconnectUtil.config.get("buildUrl") + "')]");

			boolean isLinkUrlWIndowOpen = false;
			Set<String> windowsHandlesLink = driver.getWindowHandles();
			for (String window : windowsHandlesLink) {
				if (!window.equalsIgnoreCase(parentWindow)) {
					driver.switchTo().window(window);
					if (driver.getCurrentUrl().toLowerCase().contains("yahoo")) {
						isLinkUrlWIndowOpen = true;
						driver.close();
						break;
					}
				}
			}
			driver.switchTo().window(parentWindow);
			if (!isLinkUrlWIndowOpen) {
				fc.utobj().throwsException("Link is not opening");
			}
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	/*
	 * TC-76197
	 */

	/*
	 * QuickLinkGroup
	 */

	@Test(groups = { "thehub","hub_library","hubdev0828"})
	@TestCase(createdOn = "2018-08-28", updatedOn = "2018-08-28", testCaseDescription = "Verify the Add Library Document link through Quick Links at bottom of page bar button", testCaseId = "TC_Hub_Library_Quick_Link_01")
	private void quickLinkAddDocument() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			
			TheHubLibraryPage pobj = new TheHubLibraryPage(driver);
			AdminTheHubLibraryPageTest libraryPage = new AdminTheHubLibraryPageTest();

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Library Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String summaryFormat = dataSet.get("summaryFormat");
			String accessTo = "All";
			Library library = new Library();
			library.setFolderName(folderName);
			library.setFolderSummary(folderSummary);
			library.setSummaryFormat(summaryFormat);
			library.setAccessTo(accessTo);
			libraryPage.addFolder(driver, library);
			
			fc.utobj().printTestStep("Configure Quick Link Add Document");
			OptionsConfigureQuickLinkPageTest configure_notification = new OptionsConfigureQuickLinkPageTest();
			configure_notification.configureQuickLnk(driver, "Add Library Document", true);
			CommonUI coi = new CommonUI(driver);

			fc.utobj().printTestStep("Notification > Hub > Add Library Document");
			boolean isDisplayed = coi.showQuickLinks.isDisplayed();

			if (!isDisplayed) {
				fc.utobj().clickElement(driver, coi.showNotificationBar);
			}
			fc.utobj().clickElement(driver, coi.showQuickLinks);
			fc.utobj().clickElement(driver, coi.addLibraryDocumentLink);
			
			libraryPage.clickOverMoreLink(driver, folderName);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='folderDiv']//*[contains(text() , '" + folderName + "')]"));

			fc.utobj().printTestStep("Add Document");
			fc.utobj().clickElement(driver, pobj.addDocument);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			if (!fc.utobj().isSelected(driver, pobj.multipleDoc.get(1))) {
				fc.utobj().clickElement(driver, pobj.multipleDoc.get(1));
			}

			String documentTitle = fc.utobj().generateTestData(dataSet.get("documentTittle"));
			fc.utobj().sendKeys(driver, pobj.documentTitle, documentTitle);
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			fc.utobj().sendKeys(driver, pobj.briefSummary, briefSummary);

			if (!fc.utobj().isSelected(driver, pobj.documentType.get(0))) {
				fc.utobj().clickElement(driver, pobj.documentType.get(0));
			}

			fc.utobj().selectDropDown(driver, pobj.documentSubType, dataSet.get("subType"));

			String file = fc.utobj().getFilePathFromTestData(dataSet.get("fileName"));

			fc.utobj().moveToElement(driver, pobj.fileClick);
			pobj.fileClick.sendKeys(file);

			if (!fc.utobj().isSelected(driver, pobj.thumbFileUploaded.get(1))) {
				fc.utobj().clickElement(driver, pobj.thumbFileUploaded.get(1));
			}

			if (!fc.utobj().isSelected(driver, pobj.accessibilityPublic)) {
				fc.utobj().clickElement(driver, pobj.accessibilityPublic);
			}

			fc.utobj().clickElementByJS(driver, pobj.addBtn);
			new HubCommonMethods().tryClickClose(driver, new AdminTheHubLibraryPage(driver));
			fc.utobj().switchFrameToDefault(driver);

			libraryPage.clickOverMoreLink(driver, folderName);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='folderDiv']//*[contains(text() , '" + folderName + "')]"));

			fc.utobj().printTestStep("Verify The Add Document");
			fc.utobj().clickElement(driver, pobj.listView);

			if (!fc.utobj()
					.getElementByXpath(driver, ".//*[@id='thumDtls']//a[contains(text () ,'" + documentTitle + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Document title Folder List View");
			}

			if (!fc.utobj()
					.getElementByXpath(driver, ".//*[@id='thumDtls']//span[contains(text () , '" + briefSummary + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Brief Summary Of document at List View");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
}
