package com.builds.test.thehub;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminAreaRegionAddAreaRegionPageTest;
import com.builds.test.admin.AdminDivisionAddDivisionPageTest;
import com.builds.test.admin.AdminFranchiseLocationAddFranchiseLocationPageTest;
import com.builds.test.admin.AdminHiddenLinksConfigureNewHierarchyLevelPageTest;
import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.admin.AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest;
import com.builds.test.admin.AdminUsersManageManageFranchiseUsersPageTest;
import com.builds.test.admin.AdminUsersManageRegionalUsersAddRegionalUserPageTest;
import com.builds.test.admin.AdminUsersRolesAddNewRolePageTest;
import com.builds.test.admin.OptionsConfigureQuickLinkPageTest;
import com.builds.test.common.CorporateUser;
import com.builds.test.infomgr.FranchiseesCommonMethods;
import com.builds.uimaps.admin.AdminUsersManageCorporateUsersPage;
import com.builds.uimaps.admin.AdminUsersManageDivisionalUsersAddDivisionalUsersPage;
import com.builds.uimaps.common.CommonUI;
import com.builds.uimaps.common.FCHomePage;
import com.builds.uimaps.infomgr.InfoMgrFranchiseesPage;
import com.builds.uimaps.thehub.TheHubDirectoryPage;
import com.builds.uimaps.thehub.TheHubHomePage;
import com.builds.uimaps.thehub.TheHubMessagesPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class TheHubMessagesPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "thehub", "thehub_mail" ,"hub_messages" ,"hubmsgTest" ,"hubf1121"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Sent Messsages To Corporate User at The Hub > Messages > Compose", testCaseId = "TC_148_Compose_Messages_Send_CU")
	public void composeMessagesSendToCU() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		
		try {
			driver = fc.loginpage().login(driver);
			TheHubMessagesPage pobj = new TheHubMessagesPage(driver);
			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users Page");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUserPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "hubautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser.setState("Alabama");
			corporateUserPage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To The Hub > Messages Page");
			fc.utobj().printTestStep("Compose Messages");

			fc.hub().hub_common().theHubMessageSubModule(driver);
			fc.utobj().clickElement(driver, pobj.composeLink);
			fc.utobj().clickElement(driver, pobj.addressBookTo);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			if (!fc.utobj().isSelected(driver, pobj.internalContact)) {
				fc.utobj().clickElement(driver, pobj.internalContact);
			}
			fc.utobj().setToDefault(driver, pobj.selectUsers);
			fc.utobj().selectValFromMultiSelect(driver, pobj.selectUsers, "Corporate Users");
			fc.utobj().clickElement(driver, pobj.submitOk);
			fc.utobj().sendKeys(driver, pobj.searchTxBx, corpUser.getuserFullName());
			fc.utobj().clickElement(driver, pobj.searchImgBtn);

			fc.utobj().sleep();
			WebElement element = fc.utobj().getElementByXpath(driver,
					".//*[contains(text () , '" + corpUser.getuserFullName() + "')]/ancestor::tr/td/input");
			fc.utobj().clickElement(driver, element);
			fc.utobj().clickElement(driver, pobj.addBtnAtTop);
			fc.utobj().switchFrameToDefault(driver);

			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subject, subject);

			if (!fc.utobj().isSelected(driver, pobj.texthtmlOption.get(1))) {
				fc.utobj().clickElement(driver, pobj.texthtmlOption.get(1));
			}

			fc.utobj().printTestStep("Send Messages");
			String messages = fc.utobj().generateTestData(dataSet.get("messages"));

			/*
			 * TC-10019
			 */
			fc.utobj().printTestStep("Insert Keyword");
			fc.utobj().clickElement(driver, pobj.keywordsbutton);
			fc.utobj().selectDropDown(driver, pobj.viewContactSelect, "User State");

			List<WebElement> listElements = driver.findElements(By.xpath(".//*[@id='select']/option"));
			String keywordText = null;

			for (WebElement webElement : listElements) {
				keywordText = webElement.getAttribute("value");

				if (keywordText.equalsIgnoreCase("$USER_STATE$")) {
					break;
				}
			}

			//Trial
			fc.utobj().printTestStep("Close the TinyMCE");
			closeTinyMCETrial(driver);
			
			fc.utobj().switchFrameById(driver, "mailText_ifr");
			Actions actions = new Actions(driver);
			actions.moveToElement(fc.utobj().getElement(driver, pobj.editorTextArea));
			actions.click();
			actions.sendKeys(messages);
			actions.sendKeys(fc.utobj().getElement(driver, pobj.editorTextArea), Keys.ENTER);
			actions.sendKeys(keywordText);
			actions.build().perform();

			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify A checkbox should be seen labelled as Send Email to Users");
			boolean isCheckBoxPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text(),'Send Email to Users')]/ancestor::tr/td/input[@id='isSentMail' and @type='checkbox']");

			if (isCheckBoxPresent == false) {
				fc.utobj()
						.throwsException("Not able to Verify checkbox should be seen labelled as Send Email to Users");
			}

			fc.utobj().clickElement(driver, pobj.sendBtn);

			fc.utobj().printTestStep("Verify Sent Message at Sent Message Confirmation page");
			fc.utobj().isTextDisplayed(driver, corpUser.getuserFullName(), "was not able to verify TO");
			fc.utobj().isTextDisplayed(driver, subject, "was not able to verify Subject");
			fc.utobj().isTextDisplayed(driver, messages, "was not able to verify Subject");
			fc.utobj().clickElement(driver, pobj.OkBtn);

			fc.utobj().printTestStep("Verify Sent Message in sent box");
			fc.utobj().selectDropDown(driver, pobj.currentFolderSelect, "Sent");

			fc.utobj().selectDropDown(driver, pobj.viewSelect, "All");

			fc.utobj().showAll(driver);
			fc.utobj().isTextDisplayed(driver, subject, "was not able to verify Subject in sent box");

			/*
			 * TC-10068
			 */

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.adminpage().adminUsersManageCorporateUsersPage(driver);

			fc.utobj().printTestStep("Search Corporate User and send messages by bottom button");

			AdminUsersManageCorporateUsersPage corporate_userPage = new AdminUsersManageCorporateUsersPage(driver);
			fc.utobj().sendKeys(driver, corporate_userPage.search, corpUser.getuserFullName());
			fc.utobj().clickElement(driver, corporate_userPage.searchButton);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'"
					+ corpUser.getuserFullName() + "')]/ancestor::tr/td/input[@name='contactidcorp']"));
			fc.utobj().clickElement(driver, new AdminUsersManageCorporateUsersPage(driver).sendMessagesBtn);

			String subjectAtUser = fc.utobj().generateTestData("subjectAtUser");
			fc.utobj().sendKeys(driver, pobj.subject, subjectAtUser);

			if (!fc.utobj().isSelected(driver, pobj.texthtmlOption.get(0))) {
				fc.utobj().clickElement(driver, pobj.texthtmlOption.get(0));
			}

			fc.utobj().printTestStep("Send Messages");
			String messagesAtUser = fc.utobj().generateTestData("messagesAtUser");
			fc.utobj().sendKeys(driver, pobj.messages, messagesAtUser);
			fc.utobj().clickElement(driver, pobj.sendBtn);
			fc.utobj().clickElement(driver, pobj.OkBtn);

			/*
			 * TC-10062
			 */

			fc.utobj().printTestStep("Navigate To The Hub > Messages > Search Page");
			fc.hub().hub_common().theHubMessageSubModule(driver);

			fc.utobj().printTestStep("Search Message By Search Tab");
			fc.utobj().clickElement(driver, pobj.searchTab);
			fc.utobj().selectDropDown(driver, pobj.searchSelectFolder, "Sent");
			fc.utobj().sendKeys(driver, pobj.searchTo, corpUser.getuserFullName());
			fc.utobj().sendKeys(driver, pobj.searchSubject, subject);
			fc.utobj().sendKeys(driver, pobj.searchText, messages);
			fc.utobj().sendKeys(driver, pobj.searchFromDate,
					fc.utobj().getCurrentDateUserDefineTimeZoneUSFormat("GMT-5:00"));
			fc.utobj().sendKeys(driver, pobj.searchToDate,
					fc.utobj().getCurrentDateUserDefineTimeZoneUSFormat("GMT-5:00"));
			fc.utobj().clickElement(driver, pobj.submitOk);

			boolean isMessagesPresent = fc.utobj().assertPageSource(driver, subject);
			if (isMessagesPresent == false) {
				fc.utobj().throwsException("Not able to search message By Search");
			}

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With Corporate User");
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());

			fc.utobj().printTestStep("Navigate To The Hub > Messages Page");
			fc.hub().hub_common().theHubMessageSubModule(driver);

			fc.utobj().printTestStep("Verify Messages In Inbox");
			fc.utobj().clickElement(driver, pobj.inboxTab);
			fc.utobj().selectDropDown(driver, pobj.currentFolderSelect, "Inbox");

			fc.utobj().selectDropDown(driver, pobj.viewSelect, "All");

			fc.utobj().showAll(driver);

			fc.utobj().printTestStep("Verify Messages Send from Users > Manage Corporate User");
			boolean isMessagesUser = fc.utobj().assertPageSource(driver, subjectAtUser);
			if (isMessagesUser == false) {
				fc.utobj().throwsException("Message is not present");
			}

			WebElement element1 = fc.utobj().getElementByLinkText(driver, subject);
			fc.utobj().clickElement(driver, element1);

			fc.utobj().isTextDisplayed(driver, subject,
					"was not able to verify Subject at Messages Detail at Corporate User Page");
			fc.utobj().isTextDisplayed(driver, "FranConnect Administrator",
					"was not able to verify FROM at corporate User Page");
			fc.utobj().isTextDisplayed(driver, corpUser.getuserFullName(),
					"was not able to verify TO at corporate User Page");
			fc.utobj().isTextDisplayed(driver, messages, "was not able to verify Subject");

			fc.utobj().printTestStep("Verify Email");

			Map<String, String> mailInfo = fc.utobj().readMailBox(subject, messages, emailId, "sdg@1a@Hfs");

			if (mailInfo.size() == 0 || !mailInfo.get("mailBody").contains(messages)) {
				fc.utobj().throwsException("was not able to verify email");
			}

			if (mailInfo.size() == 0 || !mailInfo.get("mailBody").contains(corpUser.getState())) {
				fc.utobj().throwsException("was not able to Verify Keyword In Email");
			}

			if (mailInfo.size() == 0 || mailInfo.get("mailBody").contains("$USER_STATE$") == true) {
				fc.utobj().throwsException("Keyword is not replacing In Email");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub" ,"hub_messages"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Sent Messsages To Divisionasl User At The Hub > Messages > Compose", testCaseId = "TC_149_Compose_Messages_Send_DU")
	private void composeMessagesSendToDU() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubMessagesPage pobj = new TheHubMessagesPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Division > Add Division Page");
			AdminDivisionAddDivisionPageTest addDivPage = new AdminDivisionAddDivisionPageTest();
			String divisionName = fc.utobj().generateTestData(dataSet.get("divisionName"));
			addDivPage.addDivision(driver, divisionName);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Divisional Users Page");
			fc.utobj().printTestStep("Add Divisional User");
			AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest divisionUserPage = new AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest();
			String userName = fc.utobj().generateTestData(dataSet.get("userName"));
			String password = "t0n1ght123";
			String emailId = "hubautomation@staffex.com";
			divisionUserPage.addDivisionalUser(driver, userName, password, divisionName, emailId);
			String userName1 = userName + " " + userName;

			fc.utobj().printTestStep("Navigate To The Hub > Messages Page");
			fc.utobj().printTestStep("Compose Messages");
			fc.hub().hub_common().theHubMessageSubModule(driver);
			fc.utobj().clickElement(driver, pobj.composeLink);
			fc.utobj().clickElement(driver, pobj.addressBookTo);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			if (!fc.utobj().isSelected(driver, pobj.internalContact)) {
				fc.utobj().clickElement(driver, pobj.internalContact);
			}
			fc.utobj().setToDefault(driver, pobj.selectUsers);
			fc.utobj().selectValFromMultiSelect(driver, pobj.selectUsers, "Divisional Users");
			fc.utobj().clickElement(driver, pobj.submitOk);
			fc.utobj().sendKeys(driver, pobj.searchTxBx, userName1);
			fc.utobj().clickElement(driver, pobj.searchImgBtn);

			fc.utobj().sleep();
			WebElement element = fc.utobj().getElementByXpath(driver,
					".//*[contains(text () , '" + userName1 + "')]/ancestor::tr/td/input");
			fc.utobj().clickElement(driver, element);
			fc.utobj().clickElement(driver, pobj.addBtnAtTop);
			fc.utobj().switchFrameToDefault(driver);

			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subject, subject);

			if (!fc.utobj().isSelected(driver, pobj.texthtmlOption.get(0))) {
				fc.utobj().clickElement(driver, pobj.texthtmlOption.get(0));
			}
			String messages = fc.utobj().generateTestData(dataSet.get("messages"));
			fc.utobj().sendKeys(driver, pobj.messages, messages);
			fc.utobj().clickElement(driver, pobj.sendBtn);

			fc.utobj().printTestStep("Verify The Send Messages");
			fc.utobj().isTextDisplayed(driver, userName1, "was not able to verify TO");
			fc.utobj().isTextDisplayed(driver, subject, "was not able to verify Subject");
			fc.utobj().isTextDisplayed(driver, messages, "was not able to verify Subject");
			fc.utobj().clickElement(driver, pobj.OkBtn);
			fc.utobj().selectDropDown(driver, pobj.currentFolderSelect, "Sent");

			fc.utobj().selectDropDown(driver, pobj.viewSelect, "All");

			fc.utobj().showAll(driver);

			fc.utobj().isTextDisplayed(driver, subject, "was not able to verify Subject At Sent Box");

			/*
			 * TC-10068
			 */

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Divisional User");
			fc.adminpage().openadminUsersManageDivisionalUsersLnk(driver);
			fc.utobj().printTestStep("Search Divisional User and send messages by bottom button");

			AdminUsersManageCorporateUsersPage corporate_userPage = new AdminUsersManageCorporateUsersPage(driver);

			fc.utobj().printTestStep("Search Divisional User");
			fc.utobj().sendKeys(driver, corporate_userPage.search, userName1);
			fc.utobj().clickElement(driver, corporate_userPage.searchButton);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text(),'" + userName1 + "')]/ancestor::tr/td/input[@name='contactidcorp']"));
			fc.utobj().clickElement(driver, new AdminUsersManageCorporateUsersPage(driver).sendMessagesBtn);

			String subjectAtUser = fc.utobj().generateTestData("subjectAtUser");
			fc.utobj().sendKeys(driver, pobj.subject, subjectAtUser);

			if (!fc.utobj().isSelected(driver, pobj.texthtmlOption.get(0))) {
				fc.utobj().clickElement(driver, pobj.texthtmlOption.get(0));
			}

			fc.utobj().printTestStep("Send Messages");
			String messagesAtUser = fc.utobj().generateTestData("messagesAtUser");
			fc.utobj().sendKeys(driver, pobj.messages, messagesAtUser);
			fc.utobj().clickElement(driver, pobj.sendBtn);
			fc.utobj().clickElement(driver, pobj.OkBtn);

			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login With Divisional User");
			fc.loginpage().loginWithParameter(driver, userName, password);

			fc.utobj().printTestStep("Navigate To The Hub > Messages Page");
			fc.hub().hub_common().theHubMessageSubModule(driver);

			/*
			 * TC-9991
			 */

			fc.utobj().printTestStep("Verify The Sub tabs of messages Sub module");

			List<WebElement> subTabs = driver.findElements(By.xpath(".//table[@class='subTabs']//td//span"));

			boolean b3[] = new boolean[10];

			for (WebElement webElement : subTabs) {

				String options = fc.utobj().getText(driver, webElement);

				if (options.equalsIgnoreCase(fc.utobj().translateString("Inbox"))) {
					b3[0] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Compose"))) {
					b3[1] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Folders"))) {
					b3[2] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Address Book"))) {
					b3[3] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Groups"))) {
					b3[4] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Audit Report"))) {
					b3[5] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Archived"))) {
					b3[6] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Search"))) {
					b3[7] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Export"))) {
					b3[8] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("My Signatures"))) {
					b3[9] = true;
				}
			}

			fc.utobj().printTestStep(
					"The following Sub tabs should be appeared under Messages Section except Audit Report");

			if (b3[0] == false) {
				fc.utobj().throwsException("Inbox subtabs is not present under messages submodule");
			}
			if (b3[1] == false) {
				fc.utobj().throwsException("Compose subtabs is not present under messages submodule");
			}
			if (b3[2] == false) {
				fc.utobj().throwsException("Folders subtabs is not present under messages submodule");
			}
			if (b3[3] == false) {
				fc.utobj().throwsException("Address Book subtabs is not present under messages submodule");
			}
			if (b3[4] == false) {
				fc.utobj().throwsException("Groups subtabs is not present under messages submodule");
			}
			if (b3[5] == true) {
				fc.utobj().throwsException("Audit Report subtabs is present under messages submodule");
			}
			if (b3[6] == false) {
				fc.utobj().throwsException("Archived subtabs is not present under messages submodule");
			}
			if (b3[7] == false) {
				fc.utobj().throwsException("Search subtabs is not present under messages submodule");
			}
			if (b3[8] == false) {
				fc.utobj().throwsException("Export subtabs is not present under messages submodule");
			}
			if (b3[9] == false) {
				fc.utobj().throwsException("My Signatures subtabs is not present under messages submodule");
			}

			fc.utobj().clickElement(driver, pobj.inboxTab);
			fc.utobj().selectDropDown(driver, pobj.currentFolderSelect, "Inbox");

			fc.utobj().selectDropDown(driver, pobj.viewSelect, "All");

			fc.utobj().showAll(driver);

			fc.utobj().printTestStep("Verify Messages Send from Users > Manage Divisional User");
			boolean isMessagesUser = fc.utobj().assertPageSource(driver, subjectAtUser);
			if (isMessagesUser == false) {
				fc.utobj().throwsException("Message is not present");
			}

			fc.utobj().printTestStep("Verify Messages Detail At Inbox");
			WebElement element1 = fc.utobj().getElementByLinkText(driver, subject);
			fc.utobj().clickElement(driver, element1);
			fc.utobj().isTextDisplayed(driver, subject,
					"was not able to verify Subject at Messages Detail at Divisional User Page");
			fc.utobj().isTextDisplayed(driver, "FranConnect Administrator",
					"was not able to verify FROM at Divisional User Page");
			fc.utobj().isTextDisplayed(driver, userName1, "was not able to verify TO at Divisional User Page");
			fc.utobj().isTextDisplayed(driver, messages, "was not able to verify Subject");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub" ,"hub_messages"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Sent Messsages To Regional User At The Hub > Messages > Compose ", testCaseId = "TC_150_Compose_Messages_Send_RU")
	private void composeMessagesSendToRU() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubMessagesPage pobj = new TheHubMessagesPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Add Area/Region");
			fc.utobj().printTestStep("Add Region");

			AdminAreaRegionAddAreaRegionPageTest addRegionPage = new AdminAreaRegionAddAreaRegionPageTest();
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			addRegionPage.addAreaRegion(driver, regionName);

			fc.utobj().printTestStep("Navigate To Admin > Users > Mange Regional Users Page");
			fc.utobj().printTestStep("Add Regional User");
			AdminUsersManageRegionalUsersAddRegionalUserPageTest regionalUserPage = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
			String userName = fc.utobj().generateTestData(dataSet.get("userName"));
			String password = "t0n1ght123";

			String emailId = "hubautomation@staffex.com";
			regionalUserPage.addRegionalUser(driver, userName, password, regionName, emailId);
			String userName1 = userName + " " + userName;

			fc.utobj().printTestStep("Navigate To The Hub > Messages Page");
			fc.utobj().printTestStep("Compose Messages");
			fc.hub().hub_common().theHubMessageSubModule(driver);
			fc.utobj().clickElement(driver, pobj.composeLink);
			fc.utobj().clickElement(driver, pobj.addressBookTo);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			if (!fc.utobj().isSelected(driver, pobj.internalContact)) {
				fc.utobj().clickElement(driver, pobj.internalContact);
			}
			fc.utobj().setToDefault(driver, pobj.selectUsers);
			fc.utobj().selectValFromMultiSelect(driver, pobj.selectUsers, "Regional Users");
			fc.utobj().clickElement(driver, pobj.submitOk);
			fc.utobj().sendKeys(driver, pobj.searchTxBx, userName1);
			fc.utobj().clickElement(driver, pobj.searchImgBtn);

			fc.utobj().sleep();
			WebElement element = fc.utobj().getElementByXpath(driver,
					".//*[contains(text () , '" + userName1 + "')]/ancestor::tr/td/input");
			fc.utobj().clickElement(driver, element);
			fc.utobj().clickElement(driver, pobj.addBtnAtTop);
			fc.utobj().switchFrameToDefault(driver);

			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subject, subject);

			if (!fc.utobj().isSelected(driver, pobj.texthtmlOption.get(0))) {
				fc.utobj().clickElement(driver, pobj.texthtmlOption.get(0));
			}
			String messages = fc.utobj().generateTestData(dataSet.get("messages"));
			fc.utobj().sendKeys(driver, pobj.messages, messages);
			fc.utobj().clickElement(driver, pobj.sendBtn);

			fc.utobj().printTestStep("Verify The Sent Message Details");
			fc.utobj().isTextDisplayed(driver, userName1, "was not able to verify TO");
			fc.utobj().isTextDisplayed(driver, subject, "was not able to verify Subject");
			fc.utobj().isTextDisplayed(driver, messages, "was not able to verify Subject");
			fc.utobj().clickElement(driver, pobj.OkBtn);
			fc.utobj().selectDropDown(driver, pobj.currentFolderSelect, "Sent");

			fc.utobj().selectDropDown(driver, pobj.viewSelect, "All");

			fc.utobj().showAll(driver);
			fc.utobj().isTextDisplayed(driver, subject, "was not able to verify Subject at Inbox");

			/*
			 * TC-10068
			 */

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Regional User");
			fc.adminpage().adminUsersManageRegionalUsersPage(driver);
			fc.utobj().printTestStep("Search Regional User and send messages by bottom button");

			AdminUsersManageCorporateUsersPage corporate_userPage = new AdminUsersManageCorporateUsersPage(driver);
			fc.utobj().sendKeys(driver, corporate_userPage.search, userName1);
			fc.utobj().clickElement(driver, corporate_userPage.searchButton);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text(),'" + userName1 + "')]/ancestor::tr/td/input[@name='contactidcorp']"));
			fc.utobj().clickElement(driver, new AdminUsersManageCorporateUsersPage(driver).sendMessagesBtn);

			String subjectAtUser = fc.utobj().generateTestData("subjectAtUser");
			fc.utobj().sendKeys(driver, pobj.subject, subjectAtUser);

			if (!fc.utobj().isSelected(driver, pobj.texthtmlOption.get(0))) {
				fc.utobj().clickElement(driver, pobj.texthtmlOption.get(0));
			}

			fc.utobj().printTestStep("Send Messages");
			String messagesAtUser = fc.utobj().generateTestData("messagesAtUser");
			fc.utobj().sendKeys(driver, pobj.messages, messagesAtUser);
			fc.utobj().clickElement(driver, pobj.sendBtn);
			fc.utobj().clickElement(driver, pobj.OkBtn);

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With Regionl User");
			fc.loginpage().loginWithParameter(driver, userName, password);

			fc.utobj().printTestStep("Navigate To The Hub > Messages Page");
			fc.hub().hub_common().theHubMessageSubModule(driver);

			/*
			 * TC-9991
			 */

			fc.utobj().printTestStep("Verify The Sub tabs of messages Sub module");

			List<WebElement> subTabs = driver.findElements(By.xpath(".//table[@class='subTabs']//td//span"));

			boolean b3[] = new boolean[10];

			for (WebElement webElement : subTabs) {

				String options = fc.utobj().getText(driver, webElement);

				if (options.equalsIgnoreCase(fc.utobj().translateString("Inbox"))) {
					b3[0] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Compose"))) {
					b3[1] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Folders"))) {
					b3[2] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Address Book"))) {
					b3[3] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Groups"))) {
					b3[4] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Audit Report"))) {
					b3[5] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Archived"))) {
					b3[6] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Search"))) {
					b3[7] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Export"))) {
					b3[8] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("My Signatures"))) {
					b3[9] = true;
				}
			}

			fc.utobj().printTestStep(
					"The following Sub tabs should be appeared under Messages Section except Audit Report");

			if (b3[0] == false) {
				fc.utobj().throwsException("Inbox subtabs is not present under messages submodule");
			}
			if (b3[1] == false) {
				fc.utobj().throwsException("Compose subtabs is not present under messages submodule");
			}
			if (b3[2] == false) {
				fc.utobj().throwsException("Folders subtabs is not present under messages submodule");
			}
			if (b3[3] == false) {
				fc.utobj().throwsException("Address Book subtabs is not present under messages submodule");
			}
			if (b3[4] == false) {
				fc.utobj().throwsException("Groups subtabs is not present under messages submodule");
			}
			if (b3[5] == true) {
				fc.utobj().throwsException("Audit Report subtabs is present under messages submodule");
			}
			if (b3[6] == false) {
				fc.utobj().throwsException("Archived subtabs is not present under messages submodule");
			}
			if (b3[7] == false) {
				fc.utobj().throwsException("Search subtabs is not present under messages submodule");
			}
			if (b3[8] == false) {
				fc.utobj().throwsException("Export subtabs is not present under messages submodule");
			}
			if (b3[9] == false) {
				fc.utobj().throwsException("My Signatures subtabs is not present under messages submodule");
			}

			fc.utobj().clickElement(driver, pobj.inboxTab);
			fc.utobj().selectDropDown(driver, pobj.currentFolderSelect, "Inbox");

			fc.utobj().selectDropDown(driver, pobj.viewSelect, "All");

			fc.utobj().showAll(driver);

			fc.utobj().printTestStep("Verify Messages Send from Users > Manage Regional User");
			boolean isMessagesUser = fc.utobj().assertPageSource(driver, subjectAtUser);
			if (isMessagesUser == false) {
				fc.utobj().throwsException("Message is not present");
			}

			fc.utobj().printTestStep("Verify Messages Detail");
			WebElement element1 = fc.utobj().getElementByLinkText(driver, subject);
			fc.utobj().clickElement(driver, element1);
			fc.utobj().isTextDisplayed(driver, subject,
					"was not able to verify Subject at Messages Detail at Regional User Page");
			fc.utobj().isTextDisplayed(driver, "FranConnect Administrator",
					"was not able to verify FROM at Regional User Page");
			fc.utobj().isTextDisplayed(driver, userName1, "was not able to verify TO at Regional User Page");
			fc.utobj().isTextDisplayed(driver, messages, "was not able to verify Subject");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub","hub_messages"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Sent Messsages To Franchise User  At The Hub > Messages > Compose ", testCaseId = "TC_151_Compose_Messages_Send_FU")
	private void composeMessagesSendToFU() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubMessagesPage pobj = new TheHubMessagesPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			String regionName = "Test127Region".concat(fc.utobj().generateRandomNumber());
			String storeType = "Test127StoreType".concat(fc.utobj().generateRandomNumber());
			String franchiseId = "Test127FranId".concat(fc.utobj().generateRandomNumber());

			AdminFranchiseLocationAddFranchiseLocationPageTest addFranchiseLocation = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			franchiseId = addFranchiseLocation.addFranchiseLocation_AllWithFName(driver, franchiseId, regionName,
					storeType, firstName);

			fc.utobj().printTestStep("Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest addFranchiseUser = new AdminUsersManageManageFranchiseUsersPageTest();
			String roleName = "Default Franchise Role";
			String userName = fc.utobj().generateTestData(dataSet.get("userName"));
			String password = "t0n1ght123";
			String emailId = "hubautomation@staffex.com";
			addFranchiseUser.addFranchiseUser(driver, userName, password, franchiseId, roleName, emailId);
			String userName1 = firstName;

			fc.utobj().printTestStep("Navigate To The Hub > Messages Page");
			fc.utobj().printTestStep("Compose Messages And Send Messages");
			fc.hub().hub_common().theHubMessageSubModule(driver);
			fc.utobj().clickElement(driver, pobj.composeLink);

			fc.utobj().printTestStep("Select To User");
			Messages hub_common22 = new Messages();
			hub_common22.setAddressType("To");
			hub_common22.setContactType("Internal");
			hub_common22.setRecipientsUser("Franchise Users");
			hub_common22.setToEmail(userName1);
			new MessagesTest().selectRecipientsAddressBook(driver, hub_common22);

			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subject, subject);

			if (!fc.utobj().isSelected(driver, pobj.texthtmlOption.get(0))) {
				fc.utobj().clickElement(driver, pobj.texthtmlOption.get(0));
			}
			String messages = fc.utobj().generateTestData(dataSet.get("messages"));
			fc.utobj().sendKeys(driver, pobj.messages, messages);
			fc.utobj().clickElement(driver, pobj.sendBtn);

			fc.utobj().printTestStep("Verify The Sent Messages Details");
			fc.utobj().isTextDisplayed(driver, userName1, "was not able to verify TO");
			fc.utobj().isTextDisplayed(driver, subject, "was not able to verify Subject");
			fc.utobj().isTextDisplayed(driver, messages, "was not able to verify Subject");
			fc.utobj().clickElement(driver, pobj.OkBtn);

			fc.utobj().selectDropDown(driver, pobj.currentFolderSelect, "Sent");

			fc.utobj().selectDropDown(driver, pobj.viewSelect, "All");

			fc.utobj().showAll(driver);
			fc.utobj().isTextDisplayed(driver, subject, "was not able to verify Subject at Inbox");

			/*
			 * TC-10068
			 */

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Franchise User");
			fc.adminpage().adminUsersManageFranchiseUsersPage(driver);
			fc.utobj().printTestStep("Search Franchise User and send messages by bottom button");

			AdminUsersManageCorporateUsersPage corporate_userPage = new AdminUsersManageCorporateUsersPage(driver);
			fc.utobj().sendKeys(driver, corporate_userPage.search, userName1);
			fc.utobj().clickElement(driver, corporate_userPage.searchButton);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text(),'" + userName1 + "')]/ancestor::tr/td/input[@name='contactidcorp']"));
			fc.utobj().clickElement(driver, new AdminUsersManageCorporateUsersPage(driver).sendMessagesBtn);

			String subjectAtUser = fc.utobj().generateTestData("subjectAtUser");
			fc.utobj().sendKeys(driver, pobj.subject, subjectAtUser);

			if (!fc.utobj().isSelected(driver, pobj.texthtmlOption.get(0))) {
				fc.utobj().clickElement(driver, pobj.texthtmlOption.get(0));
			}

			fc.utobj().printTestStep("Send Messages");
			String messagesAtUser = fc.utobj().generateTestData("messagesAtUser");
			fc.utobj().sendKeys(driver, pobj.messages, messagesAtUser);
			fc.utobj().clickElement(driver, pobj.sendBtn);
			fc.utobj().clickElement(driver, pobj.OkBtn);

			/*
			 * TC-10027
			 */

			fc.utobj().printTestStep(
					"Verify the sent message should appear under Info Mgr's Message History section of location which was sent from Hub Messages section");
			fc.infomgr().infomgr_common().InfoMgrFranchisees(driver);

			fc.utobj().printTestStep("Search Franchise Location By Filter");
			new FranchiseesCommonMethods().searchFranchise(driver, franchiseId);
			fc.utobj().clickLink(driver, franchiseId);

			fc.utobj().clickElement(driver,
					new InfoMgrFranchiseesPage(driver).getContactHistoryPage().lnkContactHistory);
			boolean isMessagesPresent = fc.utobj().assertPageSource(driver, subject);
			if (isMessagesPresent == false) {
				fc.utobj().throwsException("sent message subject is not present at Info Mgr > Contact History Page");
			}

			/*
			 * TC-10024
			 */

			fc.utobj().printTestStep(
					"Verify It should be display The Hub > Messages > inbox Page having all TO, CC and BCC address fields");
			fc.utobj().clickElement(driver, pobj.sendMessageBtn);

			fc.utobj().printTestStep("Select To User");
			new MessagesTest().selectRecipientsAddressBook(driver, hub_common22);

			fc.utobj().printTestStep("Select CC User");
			Messages hub_common = new Messages();
			hub_common.setAddressType("CC");
			hub_common.setContactType("Internal");
			hub_common.setRecipientsUser("Franchise Users");
			hub_common.setToEmail(userName1);
			new MessagesTest().selectRecipientsAddressBook(driver, hub_common);

			fc.utobj().printTestStep("Select BCC User");
			Messages hub_common11 = new Messages();
			hub_common11.setAddressType("BCC");
			hub_common11.setContactType("Internal");
			hub_common11.setRecipientsUser("Franchise Users");
			hub_common11.setToEmail(userName1);
			new MessagesTest().selectRecipientsAddressBook(driver, hub_common11);

			if (pobj.toEmailTextArea.isEnabled() == false) {
				fc.utobj().throwsException("To Mail Area is not present");
			}

			if (pobj.ccMailTextArea.isEnabled() == false) {
				fc.utobj().throwsException("CC Mail Area is not present");
			}

			if (pobj.bccMailTextArea.isEnabled() == false) {
				fc.utobj().throwsException("BCC Mail Area is not present");
			}

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With Franchise User");
			fc.loginpage().loginWithParameter(driver, userName, password);

			fc.utobj().printTestStep("Navigate To The Hub > Messages Page");
			fc.hub().hub_common().theHubMessageSubModule(driver);

			/*
			 * TC-9991
			 */

			fc.utobj().printTestStep("Verify The Sub tabs of messages Sub module");

			List<WebElement> subTabs = driver.findElements(By.xpath(".//table[@class='subTabs']//td//span"));

			boolean b3[] = new boolean[10];

			for (WebElement webElement : subTabs) {

				String options = fc.utobj().getText(driver, webElement);

				if (options.equalsIgnoreCase(fc.utobj().translateString("Inbox"))) {
					b3[0] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Compose"))) {
					b3[1] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Folders"))) {
					b3[2] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Address Book"))) {
					b3[3] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Groups"))) {
					b3[4] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Audit Report"))) {
					b3[5] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Archived"))) {
					b3[6] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Search"))) {
					b3[7] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Export"))) {
					b3[8] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("My Signatures"))) {
					b3[9] = true;
				}
			}

			fc.utobj().printTestStep(
					"The following Sub tabs should be appeared under Messages Section except Audit Report");

			if (b3[0] == false) {
				fc.utobj().throwsException("Inbox subtabs is not present under messages submodule");
			}
			if (b3[1] == false) {
				fc.utobj().throwsException("Compose subtabs is not present under messages submodule");
			}
			if (b3[2] == false) {
				fc.utobj().throwsException("Folders subtabs is not present under messages submodule");
			}
			if (b3[3] == false) {
				fc.utobj().throwsException("Address Book subtabs is not present under messages submodule");
			}
			if (b3[4] == false) {
				fc.utobj().throwsException("Groups subtabs is not present under messages submodule");
			}
			if (b3[5] == true) {
				fc.utobj().throwsException("Audit Report subtabs is present under messages submodule");
			}
			if (b3[6] == false) {
				fc.utobj().throwsException("Archived subtabs is not present under messages submodule");
			}
			if (b3[7] == false) {
				fc.utobj().throwsException("Search subtabs is not present under messages submodule");
			}
			if (b3[8] == false) {
				fc.utobj().throwsException("Export subtabs is not present under messages submodule");
			}
			if (b3[9] == false) {
				fc.utobj().throwsException("My Signatures subtabs is not present under messages submodule");
			}

			fc.utobj().clickElement(driver, pobj.inboxTab);
			fc.utobj().selectDropDown(driver, pobj.currentFolderSelect, "Inbox");

			fc.utobj().selectDropDown(driver, pobj.viewSelect, "All");

			fc.utobj().showAll(driver);

			fc.utobj().printTestStep("Verify Messages Send from Users > Manage Franchise User");
			boolean isMessagesUser = fc.utobj().assertPageSource(driver, subjectAtUser);
			if (isMessagesUser == false) {
				fc.utobj().throwsException("Message is not present");
			}

			WebElement element1 = fc.utobj().getElementByLinkText(driver, subject);
			fc.utobj().clickElement(driver, element1);

			fc.utobj().printTestStep("Verify Messages In Inbox");
			fc.utobj().isTextDisplayed(driver, subject,
					"was not able to verify Subject at Messages Detail at Franchise User Page");
			fc.utobj().isTextDisplayed(driver, "FranConnect Administrator",
					"was not able to verify FROM at Franchise User Page");
			fc.utobj().isTextDisplayed(driver, userName1, "was not able to verify TO at Franchise User Page");
			fc.utobj().isTextDisplayed(driver, messages, "was not able to verify Subject");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub" ,"hub_messages"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add Folder At The Hub > Messages > Folders", testCaseId = "TC_152_Add_Folder")
	private void addFolder() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubMessagesPage pobj = new TheHubMessagesPage(driver);

			fc.utobj().printTestStep("Navigate To The Hub > Messages > Folders Page");
			fc.utobj().printTestStep("Add Folder");
			fc.hub().hub_common().theHubMessageSubModule(driver);
			fc.utobj().clickElement(driver, pobj.foldersLink);
			fc.utobj().clickElement(driver, pobj.addFolder);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			fc.utobj().sendKeys(driver, pobj.folderTxBx, folderName);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().showAll(driver);

			fc.utobj().printTestStep("Verify The Add Folder");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, folderName);
			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to add Folder Name");
			}

			fc.utobj().clickElement(driver, pobj.inboxTab);
			boolean isValFound = fc.utobj().searchInSelectBoxSingleValue(driver, pobj.currentFolderSelect, folderName);

			if (isValFound == false) {
				fc.utobj().throwsException("was not able to verify Folder at Inbox Page");
			}

			fc.utobj().printTestStep("Verify Default Folder should be there in Folder");
			fc.utobj().clickElement(driver, pobj.foldersLink);

			List<WebElement> listElements = driver.findElements(By.xpath(
					".//tr[@qat_maintable='withoutheader']/td/a[contains(@href , 'messages') and not(contains(@href , 'folder_name'))]"));

			boolean b1[] = new boolean[5];

			for (int i = 0; i < listElements.size(); i++) {
				String text = listElements.get(i).getText().trim();

				if (text.equalsIgnoreCase("Inbox")) {
					b1[0] = true;
				} else if (text.equalsIgnoreCase("Sent")) {
					b1[1] = true;
				} else if (text.equalsIgnoreCase("Draft")) {
					b1[2] = true;
				} else if (text.equalsIgnoreCase("Archived")) {
					b1[3] = true;
				} else if (text.equalsIgnoreCase("Deleted")) {
					b1[4] = true;
				}
			}

			if (b1[0] == false) {
				fc.utobj().throwsException("Not able to verify Inbox Folder is folders page");
			}
			if (b1[1] == false) {
				fc.utobj().throwsException("Not able to verify Sent Folder is folders page");
			}
			if (b1[2] == false) {
				fc.utobj().throwsException("Not able to verify Draft Folder is folders page");
			}
			if (b1[3] == false) {
				fc.utobj().throwsException("Not able to verify Archived Folder is folders page");
			}
			if (b1[4] == false) {
				fc.utobj().throwsException("Not able to verify Deleted Folder is folders page");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void addFolder(WebDriver driver, String folderName, Map<String, String> config) throws Exception {

		String testCaseId = "TC_Add_Folder_TheHub_Messgaes";

		if (fc.utobj().validate(testCaseId)) {
			try {
				TheHubMessagesPage pobj = new TheHubMessagesPage(driver);
				fc.hub().hub_common().theHubMessageSubModule(driver);
				fc.utobj().clickElement(driver, pobj.foldersLink);
				fc.utobj().clickElement(driver, pobj.addFolder);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().sendKeys(driver, pobj.folderTxBx, folderName);
				fc.utobj().clickElement(driver, pobj.saveBtn);
				fc.utobj().switchFrameToDefault(driver);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Was not able to add Folder :: The Hub > Messages");

		}
	}

	@Test(groups = {"thehub","hub_messages"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Modify Folder At The Hub > Messages > Folders", testCaseId = "TC_153_Modify_Folder")
	private void modifyFolder() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubMessagesPage pobj = new TheHubMessagesPage(driver);

			fc.utobj().printTestStep("Navigate To The Hub > Messages > Folders Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			addFolder(driver, folderName, config);
			fc.utobj().showAll(driver);

			fc.utobj().printTestStep("Modify Folder");
			fc.utobj().actionImgOption(driver, folderName, "Modify");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			fc.utobj().sendKeys(driver, pobj.folderTxBx, folderName);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().showAll(driver);

			fc.utobj().printTestStep("Verify The Modify Folder");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, folderName);
			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to Modify Folder Name");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"thehub","hub_messages"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Delete Folder At The Hub > Messages > Folders", testCaseId = "TC_154_Delete_Folder")
	private void deleteFolder() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To The Hub > Messages > Folders Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			addFolder(driver, folderName, config);
			fc.utobj().showAll(driver);

			fc.utobj().printTestStep("Delete Folder");
			fc.utobj().actionImgOption(driver, folderName, "Delete");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().showAll(driver);

			fc.utobj().printTestStep("Verify The Delete Folder");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, folderName);
			if (isTextPresent == true) {
				fc.utobj().throwsException("was not able to Delete Folder Name");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"thehub","hub_messages"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add New Contact At The Hub > Messages > Address [External] ", testCaseId = "TC_155_Add_New_Contact")
	private void addNewContact() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubMessagesPage pobj = new TheHubMessagesPage(driver);

			fc.utobj().printTestStep("Navigate To The Hub > Messages Page");
			fc.utobj().printTestStep("Add New Contact In Address Book");
			fc.hub().hub_common().theHubMessageSubModule(driver);
			fc.utobj().clickElement(driver, pobj.addressBookTab);
			fc.utobj().selectDropDown(driver, pobj.viewContactSelect, "External");

			fc.utobj().clickElement(driver, pobj.addNewContactLink);
			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			fc.utobj().sendKeys(driver, pobj.firstname, firstName);
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			fc.utobj().sendKeys(driver, pobj.lastName, lastName);
			fc.utobj().sendKeys(driver, pobj.nickName, "nickName");
			fc.utobj().sendKeys(driver, pobj.phoneNumber, "7744112255");
			fc.utobj().sendKeys(driver, pobj.phoneNumberExten, "12");
			fc.utobj().sendKeys(driver, pobj.faxNumber, "12457896321");
			fc.utobj().sendKeys(driver, pobj.mobileNumber, "1245789632");
			String emailId = fc.utobj().generateRandomChar().concat("@gmail.com");
			fc.utobj().sendKeys(driver, pobj.emailId, emailId);
			String otherEmailId = fc.utobj().generateRandomChar().concat("@hotmail.com");
			fc.utobj().sendKeys(driver, pobj.otherEmailId, otherEmailId);
			fc.utobj().sendKeys(driver, pobj.organization, "TestOrganization");
			fc.utobj().sendKeys(driver, pobj.address, "TestAddress");
			fc.utobj().sendKeys(driver, pobj.city, "TestCity");
			fc.utobj().selectDropDown(driver, pobj.country, "USA");
			fc.utobj().selectDropDown(driver, pobj.state, "Alaska");
			fc.utobj().sendKeys(driver, pobj.zip, "124578");
			fc.utobj().sendKeys(driver, pobj.otherInformation, "TestOtherInfo");
			fc.utobj().clickElement(driver, pobj.addConatactBtn);
			fc.utobj().showAll(driver);

			fc.utobj().printTestStep("Verify The Add New Contact");
			fc.utobj().isTextDisplayed(driver, firstName + " " + lastName,
					"was not able to verify Name Of the contact");
			fc.utobj().isTextDisplayed(driver, emailId, "was not able to verify Email Of the contact");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub","hub_messages" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Modify Contact At The Hub > Messages > Address [External]", testCaseId = "TC_156_Modify_Contact")
	private void modifyContactActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubMessagesPage pobj = new TheHubMessagesPage(driver);

			fc.utobj().printTestStep("Navigate To The Hub > Messages Page");
			fc.utobj().printTestStep("Add Contact In Address Book");
			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String emailId = fc.utobj().generateRandomChar().concat("@gmail.com");

			Messages message_Page = new Messages();
			message_Page.setFirstName(firstName);
			message_Page.setLastName(lastName);
			message_Page.setEmail(emailId);

			new MessagesTest().addNewContact(driver, message_Page);

			fc.utobj().selectDropDown(driver, pobj.viewContactSelect, "External");

			fc.utobj().showAll(driver);

			fc.utobj().printTestStep("Modify Contact");
			fc.utobj().actionImgOption(driver, firstName + " " + lastName, "Modify");

			firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			fc.utobj().sendKeys(driver, pobj.firstname, firstName);
			lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			fc.utobj().sendKeys(driver, pobj.lastName, lastName);
			fc.utobj().sendKeys(driver, pobj.nickName, "nickName");
			fc.utobj().sendKeys(driver, pobj.phoneNumber, "7744112255");
			fc.utobj().sendKeys(driver, pobj.phoneNumberExten, "12");
			fc.utobj().sendKeys(driver, pobj.faxNumber, "12457896321");
			fc.utobj().sendKeys(driver, pobj.mobileNumber, "1245789632");
			emailId = fc.utobj().generateRandomChar().concat("@gmail.com");
			fc.utobj().sendKeys(driver, pobj.emailId, emailId);
			String otherEmailId = fc.utobj().generateRandomChar().concat("@hotmail.com");
			fc.utobj().sendKeys(driver, pobj.otherEmailId, otherEmailId);
			fc.utobj().sendKeys(driver, pobj.organization, "TestOrganization");
			fc.utobj().sendKeys(driver, pobj.address, "TestAddress");
			fc.utobj().sendKeys(driver, pobj.city, "TestCity");
			fc.utobj().selectDropDown(driver, pobj.country, "USA");
			fc.utobj().selectDropDown(driver, pobj.state, "Alaska");
			fc.utobj().sendKeys(driver, pobj.zip, "124578");
			fc.utobj().sendKeys(driver, pobj.otherInformation, "TestOtherInfo");
			fc.utobj().clickElement(driver, pobj.saveContactBtn);
			fc.utobj().showAll(driver);

			fc.utobj().printTestStep("Verify The Modify Contact");
			fc.utobj().isTextDisplayed(driver, firstName + " " + lastName,
					"was not able to verify Name Of the contact");
			fc.utobj().isTextDisplayed(driver, emailId, "was not able to verify Email Of the contact");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"thehub","hub_messages"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Delete Contact At The Hub > Messages > Address [External]", testCaseId = "TC_157_Delete_Contact")
	private void deleteContactActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubMessagesPage pobj = new TheHubMessagesPage(driver);

			fc.utobj().printTestStep("Navigate To The Hub > Messages Page");
			fc.utobj().printTestStep("Add New Contact In Address Book");

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String emailId = fc.utobj().generateRandomChar().concat("@gmail.com");

			Messages message_Page = new Messages();
			message_Page.setFirstName(firstName);
			message_Page.setLastName(lastName);
			message_Page.setEmail(emailId);
			new MessagesTest().addNewContact(driver, message_Page);

			fc.utobj().printTestStep("Delete Contact");
			fc.utobj().selectDropDown(driver, pobj.viewContactSelect, "External");

			fc.utobj().showAll(driver);
			fc.utobj().actionImgOption(driver, firstName + " " + lastName, "Delete");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().selectDropDown(driver, pobj.viewContactSelect, "External");

			fc.utobj().showAll(driver);

			fc.utobj().printTestStep("Verify The Delete Contact");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, firstName + " " + lastName);

			if (isTextPresent == true) {
				fc.utobj().throwsException("was not able to delete Contact");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"thehub","hub_messages"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Delete Contact By Bottom Button At The Hub > Messages > Address [External]", testCaseId = "TC_158_Delete_Contact")
	private void deleteContactBottomBtn() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubMessagesPage pobj = new TheHubMessagesPage(driver);

			fc.utobj().printTestStep("Navigate To The Hub > Messages Page");
			fc.utobj().printTestStep("Add Contact In Address Book");
			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String emailId = fc.utobj().generateRandomChar().concat("@gmail.com");

			Messages message_Page = new Messages();
			message_Page.setFirstName(firstName);
			message_Page.setLastName(lastName);
			message_Page.setEmail(emailId);

			new MessagesTest().addNewContact(driver, message_Page);

			fc.utobj().selectDropDown(driver, pobj.viewContactSelect, "External");

			fc.utobj().showAll(driver);

			fc.utobj().printTestStep("Delete Contact");
			WebElement element = fc.utobj().getElementByXpath(driver,
					".//*[contains(text () , '" + firstName + " " + lastName + "')]/ancestor::tr/td/input");
			fc.utobj().clickElement(driver, element);
			fc.utobj().clickElement(driver, pobj.deleteBtn);
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().selectDropDown(driver, pobj.viewContactSelect, "External");

			fc.utobj().showAll(driver);

			fc.utobj().printTestStep("Verify The Delete Contact");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, firstName + " " + lastName);

			if (isTextPresent == true) {
				fc.utobj().throwsException("was not able to delete Contact");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "thehub_mail","hub_messages"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Send Email By Botton Button At The Hub > Messages > Address [External]", testCaseId = "TC_159_Send_Email")
	private void sendEmailBottomBtn() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubMessagesPage pobj = new TheHubMessagesPage(driver);

			fc.utobj().printTestStep("Navigate To The Hub > Messages Page");
			fc.utobj().printTestStep("Add New Contact In Address Book");
			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String emailId = "hubautomation@staffex.com";

			Messages message_Page = new Messages();
			message_Page.setFirstName(firstName);
			message_Page.setLastName(lastName);
			message_Page.setEmail(emailId);

			new MessagesTest().addNewContact(driver, message_Page);

			fc.utobj().selectDropDown(driver, pobj.viewContactSelect, "External");

			fc.utobj().showAll(driver);

			fc.utobj().printTestStep("Send Email");
			WebElement element = fc.utobj().getElementByXpath(driver,
					".//*[contains(text () , '" + firstName + " " + lastName + "')]/ancestor::tr/td/input");
			fc.utobj().clickElement(driver, element);
			fc.utobj().clickElement(driver, pobj.sendEmailBtn);

			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subject, subject);

			fc.utobj().moveToElement(driver, pobj.texthtmlOption.get(0));
			if (!fc.utobj().isSelected(driver, pobj.texthtmlOption.get(0))) {
				fc.utobj().clickElement(driver, pobj.texthtmlOption.get(0));
			}
			String messages = fc.utobj().generateTestData(dataSet.get("messages"));
			fc.utobj().sendKeys(driver, pobj.messages, messages);

			fc.utobj().clickElement(driver, pobj.sendBtn);

			fc.utobj().printTestStep("Verify Send Email Details");
			fc.utobj().isTextDisplayed(driver, firstName + " " + lastName, "Was not able to verify TO");
			fc.utobj().isTextDisplayed(driver, subject, "Was not able to verify Subject");
			fc.utobj().isTextDisplayed(driver, messages, "Was not able to verify Messages Content");
			fc.utobj().clickElement(driver, pobj.OkBtn);

			fc.utobj().printTestStep("Verify The Sent Email In Sent Email Folder");
			fc.utobj().clickElement(driver, pobj.inboxTab);
			fc.utobj().selectDropDown(driver, pobj.currentFolderSelect, "Sent");
			fc.utobj().selectDropDown(driver, pobj.viewSelect, "All");

			fc.utobj().showAll(driver);
			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, subject));
			fc.utobj().isTextDisplayed(driver, firstName + " " + lastName, "Was not able to verify TO");
			fc.utobj().isTextDisplayed(driver, "FranConnect Administrator", "Was not able to verify TO");
			fc.utobj().isTextDisplayed(driver, subject, "Was not able to verify Subject");
			fc.utobj().isTextDisplayed(driver, messages, "Was not able to verify Messages Content");

			fc.utobj().printTestStep("Verify Email At Mail Box");

			Map<String, String> mailInfo = fc.utobj().readMailBox(subject, messages, emailId, "sdg@1a@Hfs");

			if (mailInfo.size() == 0 || !mailInfo.get("mailBody").contains(messages)) {

				fc.utobj().throwsException("was not able to verify The Email At Mail Box");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "thehub_mail" ,"hub_messages"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Sent Messsages To Corporate User  At The Hub > Messages > Address [Internal ]", testCaseId = "TC_160_Send_Messages_CU")
	private void sendMessagesToCU() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubMessagesPage pobj = new TheHubMessagesPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUserPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "hubautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUserPage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To The Hub > Messages Page");
			fc.utobj().printTestStep("Send Messgaes To Corporate User");
			fc.hub().hub_common().theHubMessageSubModule(driver);
			fc.utobj().clickElement(driver, pobj.addressBookTab);
			fc.utobj().selectDropDown(driver, pobj.viewContactSelect, "Internal");
			fc.utobj().selectDropDown(driver, pobj.userTypeSelect, "Corporate Users");
			fc.utobj().selectDropDown(driver, pobj.corpResultsPerPage, "500");

			List<WebElement> listElement = null;
			for (int i = 0; i < 50; i++) {
				listElement = driver.findElements(By.xpath(".//*[.='" + corpUser.getuserFullName() + "']"));
				if (listElement.size() > 0) {
					fc.utobj().sleep();
					WebElement element = fc.utobj().getElementByXpath(driver,
							".//*[.='" + corpUser.getuserFullName() + "']/ancestor::tr/td/input");
					fc.utobj().clickElement(driver, element);
					fc.utobj().clickElement(driver, pobj.sendMessageBtn);
					break;
				} else {
					try {
						fc.utobj().clickElement(driver, pobj.nextLink);
					} catch (Exception e) {
						fc.utobj().throwsException("was not able to find Added Corporate User");
					}
				}
			}

			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subject, subject);

			if (!fc.utobj().isSelected(driver, pobj.texthtmlOption.get(0))) {
				fc.utobj().clickElement(driver, pobj.texthtmlOption.get(0));
			}
			String messages = fc.utobj().generateTestData(dataSet.get("messages"));
			fc.utobj().sendKeys(driver, pobj.messages, messages);
			fc.utobj().clickElement(driver, pobj.sendBtn);

			fc.utobj().printTestStep("Verify The Sent Messages Details");
			fc.utobj().printTestStep("Verify Messages Detail");
			fc.utobj().isTextDisplayed(driver, corpUser.getuserFullName(), "was not able to verify TO");
			fc.utobj().isTextDisplayed(driver, subject, "was not able to verify Subject");
			fc.utobj().isTextDisplayed(driver, messages, "was not able to verify Subject");
			fc.utobj().clickElement(driver, pobj.OkBtn);

			fc.utobj().printTestStep("Verify The Sent Messages In Sent Folder");
			fc.utobj().clickElement(driver, pobj.inboxTab);
			fc.utobj().selectDropDown(driver, pobj.currentFolderSelect, "Sent");

			fc.utobj().selectDropDown(driver, pobj.viewSelect, "All");

			fc.utobj().showAll(driver);
			fc.utobj().isTextDisplayed(driver, subject, "was not able to verify Subject at Inbox");

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With Corporate User");
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());

			fc.utobj().printTestStep("Navigate To The Hub > Messages Page");
			fc.hub().hub_common().theHubMessageSubModule(driver);
			fc.utobj().clickElement(driver, pobj.inboxTab);
			fc.utobj().selectDropDown(driver, pobj.currentFolderSelect, "Inbox");

			fc.utobj().selectDropDown(driver, pobj.viewSelect, "All");

			fc.utobj().showAll(driver);

			fc.utobj().printTestStep("Verify Messages Detail In Inbox Folder");
			WebElement element1 = fc.utobj().getElementByLinkText(driver, subject);
			fc.utobj().clickElement(driver, element1);
			fc.utobj().isTextDisplayed(driver, subject,
					"was not able to verify Subject at Messages Detail at Corporate User Page");
			fc.utobj().isTextDisplayed(driver, "FranConnect Administrator",
					"was not able to verify FROM at corporate User Page");
			fc.utobj().isTextDisplayed(driver, corpUser.getuserFullName(),
					"was not able to verify TO at corporate User Page");
			fc.utobj().isTextDisplayed(driver, messages, "was not able to verify Subject");

			fc.utobj().printTestStep("Verify Email At Mail Box");

			Map<String, String> mailInfo = fc.utobj().readMailBox(subject, messages, emailId, "sdg@1a@Hfs");

			if (mailInfo.size() == 0 || !mailInfo.get("mailBody").contains(messages)) {

				fc.utobj().throwsException("was not able to verify The Email At Mail Box");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "thehub_mail","hub_messages","hubf1121"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Sent Messsages To Divisionasl User At The Hub > Messages > Address [Internal]", testCaseId = "TC_161_Send_Messages_DU")
	private void sendMessagesToDU() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubMessagesPage pobj = new TheHubMessagesPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Division > Add Division");
			AdminDivisionAddDivisionPageTest addDivPage = new AdminDivisionAddDivisionPageTest();
			String divisionName = fc.utobj().generateTestData(dataSet.get("divisionName"));
			addDivPage.addDivision(driver, divisionName);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Divisional Users");
			AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest divisionUserPage = new AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest();
			String userName = fc.utobj().generateTestData(dataSet.get("userName"));
			String password = "t0n1ght123";

			String emailId = "hubautomation@staffex.com";
			divisionUserPage.addDivisionalUser(driver, userName, password, divisionName, emailId);
			String divUserFullName = userName + " " + userName;

			fc.utobj().printTestStep("Navigate To The Hub > Messages Page");
			fc.utobj().printTestStep("Send Messages To Divisional User");
			fc.hub().hub_common().theHubMessageSubModule(driver);
			fc.utobj().clickElement(driver, pobj.addressBookTab);
			fc.utobj().selectDropDown(driver, pobj.viewContactSelect, "Internal");
			fc.utobj().selectDropDown(driver, pobj.userTypeSelect, "Divisional Users");
			fc.utobj().selectDropDown(driver, pobj.divResultsPerPage, "500");

			for (int i = 0; i < 50; i++) {
				if (fc.utobj().isElementPresent(driver, ".//*[contains(text(),'" + divUserFullName + "')]")) {
					WebElement element=fc.utobj().getElementByXpath(driver,
							".//a[contains(text(),'" + divUserFullName + "')]/ancestor::tr/td/input");
					fc.utobj().clickElement(driver, element);
					fc.utobj().clickElement(driver, pobj.sendMessageBtn);
					break;
				} else {
					try {
						fc.utobj().clickElement(driver, pobj.nextLink);
					} catch (Exception e) {
						fc.utobj().throwsException("was not able to find Added Divisional User");
					}
				}
			}

			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subject, subject);

			if (!fc.utobj().isSelected(driver, pobj.texthtmlOption.get(0))) {
				fc.utobj().clickElement(driver, pobj.texthtmlOption.get(0));
			}
			String messages = fc.utobj().generateTestData(dataSet.get("messages"));
			fc.utobj().sendKeys(driver, pobj.messages, messages);
			fc.utobj().clickElement(driver, pobj.sendBtn);

			fc.utobj().printTestStep("Verify The Messages Detail");
			fc.utobj().isTextDisplayed(driver, divUserFullName, "was not able to verify TO");
			fc.utobj().isTextDisplayed(driver, subject, "was not able to verify Subject");
			fc.utobj().isTextDisplayed(driver, messages, "was not able to verify Subject");
			fc.utobj().clickElement(driver, pobj.OkBtn);

			fc.utobj().printTestStep("Verify The Messages ");
			fc.utobj().clickElement(driver, pobj.inboxTab);
			fc.utobj().selectDropDown(driver, pobj.currentFolderSelect, "Sent");

			fc.utobj().selectDropDown(driver, pobj.viewSelect, "All");

			fc.utobj().showAll(driver);

			fc.utobj().isTextDisplayed(driver, subject, "was not able to verify Subject at Inbox");

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With Divisional User");
			fc.loginpage().loginWithParameter(driver, userName, password);

			fc.utobj().printTestStep("Navigate To The Hub  > Messages Page");
			fc.hub().hub_common().theHubMessageSubModule(driver);
			fc.utobj().clickElement(driver, pobj.inboxTab);
			fc.utobj().selectDropDown(driver, pobj.currentFolderSelect, "Inbox");

			fc.utobj().selectDropDown(driver, pobj.viewSelect, "All");

			fc.utobj().showAll(driver);
			WebElement element1 = fc.utobj().getElementByLinkText(driver, subject);
			fc.utobj().clickElement(driver, element1);

			fc.utobj().printTestStep("Verify Messages Detail In Inbox Folder");
			fc.utobj().isTextDisplayed(driver, subject,
					"was not able to verify Subject at Messages Detail at Divisional User Page");
			fc.utobj().isTextDisplayed(driver, "FranConnect Administrator",
					"was not able to verify FROM at Divisional User Page");
			fc.utobj().isTextDisplayed(driver, divUserFullName, "was not able to verify TO at Divisional User Page");
			fc.utobj().isTextDisplayed(driver, messages, "was not able to verify Subject");

			fc.utobj().printTestStep("Verify Email At Mail Box");

			Map<String, String> mailInfo = fc.utobj().readMailBox(subject, messages, emailId, "sdg@1a@Hfs");

			if (mailInfo.size() == 0 || !mailInfo.get("mailBody").contains(messages)) {

				fc.utobj().throwsException("was not able to verify The Email At Mail Box");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "thehub_mail" ,"hub_messages"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Sent Messsages To Regional User At The Hub > Messages > Address [Internal ]", testCaseId = "TC_162_Send_Messages_RU")
	private void sendMessagesToRU() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubMessagesPage pobj = new TheHubMessagesPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Add Area/Region");
			fc.utobj().printTestStep("Add Region");

			AdminAreaRegionAddAreaRegionPageTest addRegionPage = new AdminAreaRegionAddAreaRegionPageTest();
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			addRegionPage.addAreaRegion(driver, regionName);

			fc.utobj().printTestStep("Add Regional User");
			AdminUsersManageRegionalUsersAddRegionalUserPageTest regionalUserPage = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
			String userName = fc.utobj().generateTestData(dataSet.get("userName"));
			String password = "t0n1ght123";

			String emailId = "hubautomation@staffex.com";
			regionalUserPage.addRegionalUser(driver, userName, password, regionName, emailId);
			String userName1 = userName + " " + userName;

			fc.utobj().printTestStep("Navigate To The Hub  > Messages Page");
			fc.utobj().printTestStep("Send Messages");
			fc.hub().hub_common().theHubMessageSubModule(driver);
			fc.utobj().clickElement(driver, pobj.addressBookTab);
			fc.utobj().selectDropDown(driver, pobj.viewContactSelect, "Internal");

			fc.utobj().selectDropDown(driver, pobj.userTypeSelect, "Regional Users");

			fc.utobj().selectDropDown(driver, pobj.regResultsPerPage, "500");

			List<WebElement> listElement = null;

			for (int i = 0; i < 50; i++) {

				listElement = driver.findElements(By.xpath(".//*[.='" + userName1 + "']"));

				if (listElement.size() > 0) {
					fc.utobj().sleep();
					WebElement element = fc.utobj().getElementByXpath(driver,
							".//*[.='" + userName1 + "']/ancestor::tr/td/input");
					fc.utobj().clickElement(driver, element);
					fc.utobj().clickElement(driver, pobj.sendMessageBtn);
					break;
				} else {
					try {
						fc.utobj().clickElement(driver, pobj.nextLink);
					} catch (Exception e) {
						fc.utobj().throwsException("was not able to find Added Regional User");
					}
				}
			}

			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subject, subject);

			if (!fc.utobj().isSelected(driver, pobj.texthtmlOption.get(0))) {
				fc.utobj().clickElement(driver, pobj.texthtmlOption.get(0));
			}
			String messages = fc.utobj().generateTestData(dataSet.get("messages"));
			fc.utobj().sendKeys(driver, pobj.messages, messages);
			fc.utobj().clickElement(driver, pobj.sendBtn);

			fc.utobj().printTestStep("Verify Messages Details");
			fc.utobj().isTextDisplayed(driver, userName1, "was not able to verify TO");
			fc.utobj().isTextDisplayed(driver, subject, "was not able to verify Subject");
			fc.utobj().isTextDisplayed(driver, messages, "was not able to verify Subject");
			fc.utobj().clickElement(driver, pobj.OkBtn);

			fc.utobj().clickElement(driver, pobj.inboxTab);
			fc.utobj().selectDropDown(driver, pobj.currentFolderSelect, "Sent");

			fc.utobj().selectDropDown(driver, pobj.viewSelect, "All");

			fc.utobj().showAll(driver);
			fc.utobj().isTextDisplayed(driver, subject, "was not able to verify Subject at Inbox");
			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login With Regional User");
			fc.loginpage().loginWithParameter(driver, userName, password);

			fc.utobj().printTestStep("Navigate To The Hub  > Messages Page");
			fc.hub().hub_common().theHubMessageSubModule(driver);
			fc.utobj().clickElement(driver, pobj.inboxTab);
			fc.utobj().selectDropDown(driver, pobj.currentFolderSelect, "Inbox");

			fc.utobj().selectDropDown(driver, pobj.viewSelect, "All");

			fc.utobj().showAll(driver);
			WebElement element1 = fc.utobj().getElementByLinkText(driver, subject);
			fc.utobj().clickElement(driver, element1);

			fc.utobj().printTestStep("Verify Messages Details");
			fc.utobj().isTextDisplayed(driver, subject,
					"was not able to verify Subject at Messages Detail at Regional User Page");
			fc.utobj().isTextDisplayed(driver, "FranConnect Administrator",
					"was not able to verify FROM at Regional User Page");
			fc.utobj().isTextDisplayed(driver, userName1, "was not able to verify TO at Regional User Page");
			fc.utobj().isTextDisplayed(driver, messages, "was not able to verify Subject");

			fc.utobj().printTestStep("Verify Email At Mail Box");

			Map<String, String> mailInfo = fc.utobj().readMailBox(subject, messages, emailId, "sdg@1a@Hfs");

			if (mailInfo.size() == 0 || !mailInfo.get("mailBody").contains(messages)) {

				fc.utobj().throwsException("was not able to verify The Email At Mail Box");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "thehub_mail","hub_messages"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Sent Messsages To Franchise User At The Hub > Messages > Address [Internal ]", testCaseId = "TC_163_Send_Messages_FU")
	private void sendMessagesToFU() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubMessagesPage pobj = new TheHubMessagesPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			String regionName = "Test127Region".concat(fc.utobj().generateRandomNumber());
			String storeType = "Test127StoreType".concat(fc.utobj().generateRandomNumber());
			String franchiseId = "Test127FranId".concat(fc.utobj().generateRandomNumber());

			AdminFranchiseLocationAddFranchiseLocationPageTest addFranchiseLocation = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			franchiseId = addFranchiseLocation.addFranchiseLocation_AllWithFName(driver, franchiseId, regionName,
					storeType, firstName);

			fc.utobj().printTestStep("Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest addFranchiseUser = new AdminUsersManageManageFranchiseUsersPageTest();
			String roleName = "Default Franchise Role";
			String userName = fc.utobj().generateTestData(dataSet.get("userName"));
			String password = "t0n1ght123";
			String emailId = "hubautomation@staffex.com";
			addFranchiseUser.addFranchiseUser(driver, userName, password, franchiseId, roleName, emailId);
			String userName1 = firstName + " " + firstName;

			fc.utobj().printTestStep("Navigate To The Hub > Messages Page");
			fc.hub().hub_common().theHubMessageSubModule(driver);

			fc.utobj().printTestStep("Send Messages To Franchise User");
			fc.utobj().clickElement(driver, pobj.addressBookTab);
			fc.utobj().selectDropDown(driver, pobj.viewContactSelect, "Internal");
			fc.utobj().selectDropDown(driver, pobj.userTypeSelect, "Franchise Users");
			fc.utobj().selectDropDown(driver, pobj.franResultsPerPage, "500");
			fc.utobj().sleep();

			List<WebElement> listElement = null;
			for (int i = 0; i < 50; i++) {

				listElement = driver.findElements(By.xpath(".//*[.='" + userName1 + "']"));
				if (listElement.size() > 0) {
					WebElement element = fc.utobj().getElementByXpath(driver,
							".//*[.='" + userName1 + "']/ancestor::tr/td/input");
					fc.utobj().clickElement(driver, element);
					fc.utobj().clickElement(driver, pobj.sendMessageBtn);
					break;
				} else {
					try {
						fc.utobj().clickElement(driver, pobj.nextLink);
					} catch (Exception e) {
						fc.utobj().throwsException("was not able to find Added Franchise User");
					}
				}
			}

			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subject, subject);

			if (!fc.utobj().isSelected(driver, pobj.texthtmlOption.get(0))) {
				fc.utobj().clickElement(driver, pobj.texthtmlOption.get(0));
			}
			String messages = fc.utobj().generateTestData(dataSet.get("messages"));
			fc.utobj().sendKeys(driver, pobj.messages, messages);
			fc.utobj().clickElement(driver, pobj.sendBtn);

			fc.utobj().printTestStep("Verify The Messages Details");
			fc.utobj().isTextDisplayed(driver, userName1, "was not able to verify TO");
			fc.utobj().isTextDisplayed(driver, subject, "was not able to verify Subject");
			fc.utobj().isTextDisplayed(driver, messages, "was not able to verify Subject");
			fc.utobj().clickElement(driver, pobj.OkBtn);

			fc.utobj().clickElement(driver, pobj.inboxTab);
			fc.utobj().selectDropDown(driver, pobj.currentFolderSelect, "Sent");

			fc.utobj().selectDropDown(driver, pobj.viewSelect, "All");

			fc.utobj().showAll(driver);
			fc.utobj().isTextDisplayed(driver, subject, "was not able to verify Subject at Inbox");

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With Franchise User");
			fc.loginpage().loginWithParameter(driver, userName, password);

			fc.utobj().printTestStep("Navigate To The Hub > Messgaes Page");
			fc.hub().hub_common().theHubMessageSubModule(driver);
			fc.utobj().clickElement(driver, pobj.inboxTab);
			fc.utobj().selectDropDown(driver, pobj.currentFolderSelect, "Inbox");

			fc.utobj().selectDropDown(driver, pobj.viewSelect, "All");

			fc.utobj().showAll(driver);
			WebElement element1 = fc.utobj().getElementByLinkText(driver, subject);
			fc.utobj().clickElement(driver, element1);

			fc.utobj().printTestStep("Verify Messages Details In Inbox Folder");
			fc.utobj().isTextDisplayed(driver, subject,
					"was not able to verify Subject at Messages Detail at Franchise User Page");
			fc.utobj().isTextDisplayed(driver, "FranConnect Administrator",
					"was not able to verify FROM at Franchise User Page");
			fc.utobj().isTextDisplayed(driver, userName1, "was not able to verify TO at Franchise User Page");
			fc.utobj().isTextDisplayed(driver, messages, "was not able to verify Subject");

			fc.utobj().printTestStep("Verify Email At Mail Box");

			Map<String, String> mailInfo = fc.utobj().readMailBox(subject, messages, emailId, "sdg@1a@Hfs");

			if (mailInfo.size() == 0 || !mailInfo.get("mailBody").contains(messages)) {

				fc.utobj().throwsException("was not able to verify The Email At Mail Box");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub","hub_messages"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Create Group at The Hub > Messages > Groups", testCaseId = "TC_164_Create_New_Group")
	private void createNewGroup() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubDirectoryPage pobj = new TheHubDirectoryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUserPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "hubautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUserPage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To The Hub > Messages > Groups Page");
			fc.hub().hub_common().theHubMessageSubModule(driver);

			fc.utobj().printTestStep("Create New Group With Internal Contacts");
			fc.utobj().clickElement(driver, pobj.groupsTab);
			fc.utobj().clickElement(driver, pobj.createNewGroupLink);
			fc.utobj().clickElement(driver, pobj.internalAddressBook);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().sendKeys(driver, pobj.searchTxBxCU, corpUser.getuserFullName());
			fc.utobj().clickElement(driver, pobj.searchCUImgIcon);

			fc.utobj().sleep();
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[contains(text () , '" + corpUser.getuserFullName() + "')]/ancestor::tr/td/input"));
			fc.utobj().clickElement(driver, pobj.selectUser);
			fc.utobj().switchFrameToDefault(driver);
			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			fc.utobj().sendKeys(driver, pobj.grouopName, groupName);
			fc.utobj().clickElement(driver, pobj.createBtn);
			fc.utobj().showAll(driver);
			fc.utobj().isTextDisplayed(driver, groupName, "was not able to create group");

			fc.utobj().printTestStep("Verify The Created Group");
			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, groupName));

			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().isTextDisplayed(driver, corpUser.getuserFullName(),
					"was not able to verify Name of Corporate User");
			fc.utobj().isTextDisplayed(driver, "Corporate", "was not able to verify User Type");
			fc.utobj().clickElement(driver, pobj.closeBtn);

			fc.utobj().printTestStep("Create A new External Contact");
			String firstName = fc.utobj().generateTestData("Fname");
			String lastName = fc.utobj().generateTestData("Lname");

			Messages message_Page = new Messages();
			message_Page.setFirstName(firstName);
			message_Page.setLastName(lastName);
			message_Page.setEmail(emailId);

			new MessagesTest().addNewContact(driver, message_Page);

			String fullName = firstName + " " + lastName;

			fc.utobj().printTestStep("Create New Groups With External Contact");
			fc.utobj().clickElement(driver, pobj.groupsTab);
			fc.utobj().clickElement(driver, pobj.createNewGroupLink);
			fc.utobj().clickElement(driver, pobj.externalLnkRadio);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().showAll(driver);

			WebElement element = fc.utobj().getElementByXpath(driver,
					".//td[contains(text() , '" + fullName + "')]/ancestor::tr/td/input[@name='userToPC']");
			fc.utobj().clickElement(driver, element);
			fc.utobj().clickElement(driver, pobj.selectUser);
			fc.utobj().switchFrameToDefault(driver);

			String externalGroupName = fc.utobj().generateTestData("Testgroupname");
			fc.utobj().sendKeys(driver, pobj.grouopName, externalGroupName);
			fc.utobj().clickElement(driver, pobj.createBtn);
			fc.utobj().showAll(driver);
			fc.utobj().isTextDisplayed(driver, externalGroupName, "was not able to create group");

			fc.utobj().printTestStep("Verify The Created External Group");
			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, externalGroupName));

			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().isTextDisplayed(driver, fullName, "was not able to verify Name of Corporate User");
			fc.utobj().clickElement(driver, pobj.closeBtn);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void createNewGroup(WebDriver driver, String groupName, String userName) throws Exception {
		try {
			TheHubDirectoryPage pobj = new TheHubDirectoryPage(driver);

			fc.hub().hub_common().theHubMessageSubModule(driver);
			fc.utobj().clickElement(driver, pobj.groupsTab);
			fc.utobj().clickElement(driver, pobj.createNewGroupLink);
			fc.utobj().clickElement(driver, pobj.internalAddressBook);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.searchTxBxCU, userName);
			fc.utobj().clickElement(driver, pobj.searchCUImgIcon);
			pobj = new TheHubDirectoryPage(driver);
			
			/*fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[contains(text () , '" + userName + "')]/ancestor::tr/td/input"));*/
			fc.utobj().sleep();
			fc.utobj().clickElement(driver, pobj.selectcorporate);
			fc.utobj().clickElement(driver, pobj.selectUser);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().sendKeys(driver, pobj.grouopName, groupName);
			fc.utobj().clickElement(driver, pobj.createBtn);

		} catch (Exception e) {
			Reporter.log(e.getMessage());
			fc.utobj().throwsException("Not able to add group");
		}
	}

	@Test(groups = { "thehub","hub_messages"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Modify Group At The Hub > Messages > Groups", testCaseId = "TC_165_Modify_Group")
	private void modifyGroupActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubDirectoryPage pobj = new TheHubDirectoryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Add Area/Region");
			fc.utobj().printTestStep("Add Region");

			AdminAreaRegionAddAreaRegionPageTest addRegionPage = new AdminAreaRegionAddAreaRegionPageTest();
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			addRegionPage.addAreaRegion(driver, regionName);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Regional Users");
			fc.utobj().printTestStep("Add Regional User");
			AdminUsersManageRegionalUsersAddRegionalUserPageTest regionalUserPage = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
			String regUserName = fc.utobj().generateTestData(dataSet.get("regUserName"));
			String password = "t0n1ght123";
			String emailId = "hubautomation@staffex.com";
			regionalUserPage.addRegionalUser(driver, regUserName, password, regionName, emailId);
			regUserName = regUserName + " " + regUserName;

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUserPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUserPage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));

			fc.utobj().printTestStep("Navigate To The Hub > Messages > Groups Page");
			fc.utobj().printTestStep("Create Group");
			createNewGroup(driver, groupName, corpUser.getuserFullName());

			fc.utobj().printTestStep("Modify Group");
			fc.utobj().showAll(driver);
			fc.utobj().actionImgOption(driver, groupName, "Modify");

			//Handle Bar :
			try {
				CommonUI coi = new CommonUI(driver);
				fc.utobj().isElementPresent(driver, coi.showQuickLinks);
				fc.utobj().clickElement(driver, coi.hideNotificationBar);
			} catch (Exception e) {
				Reporter.log(e.getMessage());
			}
			String groupName1 = fc.utobj().generateTestData(dataSet.get("groupName"));
			fc.utobj().sendKeys(driver, pobj.grouopName, groupName1);
			fc.utobj().clickElement(driver, pobj.internalAddressBookLink);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.searchregionalUsers, regUserName);
			fc.utobj().clickElement(driver, pobj.searchRegUserImgIcon);

			pobj = new TheHubDirectoryPage(driver);
			fc.utobj().sleep();
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[contains(text () , '" + regUserName + "')]/ancestor::tr/td/input"));
			
			fc.utobj().clickElement(driver, pobj.selectUser);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().isTextDisplayed(driver, regUserName, "was not able to verify Second Contanct");
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().showAll(driver);
			fc.utobj().isTextDisplayed(driver, groupName1, "was not able to modify group");
			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, groupName1));

			fc.utobj().printTestStep("Verify The Modify Group");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().isTextDisplayed(driver, corpUser.getuserFullName(),
					"was not able to verify Name of Corporate User");
			fc.utobj().isTextDisplayed(driver, "Corporate", "was not able to verify User Type of Corporate User");

			fc.utobj().isTextDisplayed(driver, regUserName, "was not able to verify Name of Regional User");
			fc.utobj().isTextDisplayed(driver, "Regional", "was not able to verify User Type of Regional User");
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"thehub","hub_messages"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Delete Group At The Hub > Messages > Groups", testCaseId = "TC_166_Delete_Group")
	private void deleteGroupActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUserPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "hubautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUserPage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To The Hub > Messages > Groups Page");
			fc.utobj().printTestStep("Create Group");
			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			createNewGroup(driver, groupName, corpUser.getuserFullName());
			fc.utobj().showAll(driver);

			fc.utobj().printTestStep("Delete Group");
			fc.utobj().actionImgOption(driver, groupName, "Delete");
			fc.utobj().acceptAlertBox(driver);
			fc.utobj().showAll(driver);

			fc.utobj().printTestStep("Verify The Delete Group");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, groupName);
			if (isTextPresent == true) {
				fc.utobj().throwsException("was not able to delete Group");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "thehub_mail" ,"hub_messages","hubf1121"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Send Messages To Groups Contacts At The Hub > Messages > Groups ", testCaseId = "TC_167_Send_Message")
	private void sendMessagesActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubDirectoryPage pobj = new TheHubDirectoryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUserPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "hubautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUserPage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To The Hub > Messages > Groups Page");
			fc.utobj().printTestStep("Create Group");
			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			createNewGroup(driver, groupName, corpUser.getuserFullName());

			fc.utobj().printTestStep("Send Messgaes");
			fc.utobj().showAll(driver);
			fc.utobj().actionImgOption(driver, groupName, "Send Message");
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subject, subject);

			if (!fc.utobj().isSelected(driver, pobj.texthtmlOption.get(0))) {
				fc.utobj().clickElement(driver, pobj.texthtmlOption.get(0));
			}
			String messages = fc.utobj().generateTestData(dataSet.get("messages"));
			fc.utobj().sendKeys(driver, pobj.messages, messages);
			fc.utobj().clickElement(driver, pobj.sendBtn);

			fc.utobj().isTextDisplayed(driver, corpUser.getuserFullName(), "was not able to verify TO");
			fc.utobj().isTextDisplayed(driver, subject, "was not able to verify Subject");
			fc.utobj().isTextDisplayed(driver, messages, "was not able to verify Subject");
			fc.utobj().clickElement(driver, pobj.OkBtn);

			fc.utobj().printTestStep("Verify The Messages In Sent Folder");
			fc.utobj().clickElement(driver, pobj.inboxTab);
			fc.utobj().selectDropDown(driver, pobj.folderSelect, "Sent");

			fc.utobj().selectDropDown(driver, pobj.viewSelectAtSentPage, "All");

			fc.utobj().showAll(driver);
			fc.utobj().isTextDisplayed(driver, subject, "was not able to verify Subject Sent Folder");

			fc.utobj().printTestStep("Verify Sent Messages In Mail");
			Map<String, String> mailInfo = fc.utobj().readMailBox(subject, messages, emailId, "sdg@1a@Hfs");

			if (mailInfo.size() == 0 || !mailInfo.get("mailBody").contains(messages)) {
				fc.utobj().throwsException("was not able to verify email");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub","hub_messages"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Delete Group By Bottom Button Option At The Hub > Messages > Groups", testCaseId = "TC_168_Delete_Group")
	private void deleteGroupBottomBtn() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubDirectoryPage pobj = new TheHubDirectoryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUserPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "hubautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUserPage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To The Hub > Messages > Groups Page");
			fc.utobj().printTestStep("Create Group");
			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			createNewGroup(driver, groupName, corpUser.getuserFullName());
			fc.utobj().showAll(driver);

			fc.utobj().printTestStep("Delete Group");
			WebElement element = fc.utobj().getElementByXpath(driver,
					".//*[.='" + groupName + "']/ancestor::tr/td/input");
			fc.utobj().clickElement(driver, element);
			fc.utobj().clickElement(driver, pobj.deleteBottomBtn);
			fc.utobj().acceptAlertBox(driver);
			fc.utobj().showAll(driver);

			fc.utobj().printTestStep("Verify The Delete Group");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, groupName);
			if (isTextPresent == true) {
				fc.utobj().throwsException("was not able to delete Group from bottom delete btn");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"thehub","hub_messages"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add Signatures At The Hub > Messagess > Signatures", testCaseId = "TC_169_Add_Signatures")
	private void addSignatures() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubMessagesPage pobj = new TheHubMessagesPage(driver);

			fc.utobj().printTestStep("Navigate To The Hub > Messages > Signatures Page");
			fc.utobj().printTestStep("Add Signatures");
			fc.hub().hub_common().theHubMessageSubModule(driver);
			fc.utobj().clickElement(driver, pobj.mySignaturesTab);
			fc.utobj().clickElement(driver, pobj.addSignatureLink);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String signatureTitle = fc.utobj().generateTestData(dataSet.get("signatureTitle"));
			fc.utobj().sendKeys(driver, pobj.signaturesTitle, signatureTitle);

			String signatureText = fc.utobj().generateTestData(dataSet.get("signatureText"));
			fc.utobj().switchFrameById(driver, "ta_ifr");
			Actions actions = new Actions(driver);
			actions.moveToElement(fc.utobj().getElement(driver, pobj.editorTextArea));
			actions.click();
			actions.sendKeys(signatureText);
			fc.utobj().logReportMsg("Entered Text", signatureText);
			actions.build().perform();
			fc.utobj().switchFrameToDefault(driver);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.addSignaturebtn);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().showAll(driver);

			fc.utobj().printTestStep("Verify Signatures");
			fc.utobj().isTextDisplayed(driver, signatureTitle, "was not able to verify signature");
			fc.utobj().isTextDisplayed(driver, signatureText, "was not able to verify signature");

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void addSignatures(WebDriver driver, String signatureTitle, String signatureText, Map<String, String> config)
			throws Exception {

		String testCaseId = "TC_Add_Signatures_Messages";

		if (fc.utobj().validate(testCaseId)) {
			try {
				TheHubMessagesPage pobj = new TheHubMessagesPage(driver);
				fc.hub().hub_common().theHubMessageSubModule(driver);
				fc.utobj().clickElement(driver, pobj.mySignaturesTab);
				fc.utobj().clickElement(driver, pobj.addSignatureLink);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().sendKeys(driver, pobj.signaturesTitle, signatureTitle);
				fc.utobj().switchFrameById(driver, "ta_ifr");
				Actions actions = new Actions(driver);
				actions.moveToElement(fc.utobj().getElement(driver, pobj.editorTextArea));
				actions.click();
				actions.sendKeys(signatureText);
				fc.utobj().logReportMsg("Entered Text", signatureText);
				actions.build().perform();
				fc.utobj().switchFrameToDefault(driver);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().clickElement(driver, pobj.addSignaturebtn);
				fc.utobj().switchFrameToDefault(driver);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to Add Signature :: The Hub > Messages");

		}
	}

	@Test(groups = { "thehub","hub_messages" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Modify Signatures At The Hub > Messagess > Signatures ", testCaseId = "TC_170_Modify_Signatures")
	private void modifySignatures() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubMessagesPage pobj = new TheHubMessagesPage(driver);

			fc.utobj().printTestStep("Navigate To The Hub > Messages > Signatures Page");
			fc.utobj().printTestStep("Add Signatures");
			String signatureTitle = fc.utobj().generateTestData(dataSet.get("signatureTitle"));
			String signatureText = fc.utobj().generateTestData(dataSet.get("signatureText"));
			addSignatures(driver, signatureTitle, signatureText, config);

			fc.utobj().printTestStep("Modify Signatures");
			fc.utobj().showAll(driver);
			WebElement element = fc.utobj().getElementByXpath(driver, ".//*[contains(text () , '" + signatureTitle
					+ "')]/ancestor::tr/td[@class='thead']//td/a[.='Modify']");
			fc.utobj().clickElement(driver, element);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			signatureTitle = fc.utobj().generateTestData(dataSet.get("signatureTitle"));
			fc.utobj().sendKeys(driver, pobj.signaturesTitle, signatureTitle);
			String signatureText1 = fc.utobj().generateTestData(dataSet.get("signatureText"));
			fc.utobj().switchFrameById(driver, "ta_ifr");
			Actions actions = new Actions(driver);
			actions.moveToElement(fc.utobj().getElement(driver, pobj.editorTextArea));
			actions.click();
			actions.sendKeys(signatureText1);
			fc.utobj().logReportMsg("Entered Text", signatureText1);
			actions.build().perform();

			fc.utobj().switchFrameToDefault(driver);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().printTestStep("Verify The Modify Signatures");
			fc.utobj().clickElement(driver, pobj.saveSignatureBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().showAll(driver);
			fc.utobj().isTextDisplayed(driver, signatureTitle, "was not able to verify signature Title");
			fc.utobj().isTextDisplayed(driver, signatureText + signatureText1, "was not able to verify signatureText");

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"thehub","hub_messages"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Delete Signatures At The Hub > Messagess > Signatures", testCaseId = "TC_171_Delete_Signatures")
	private void deleteSignatures() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To The Hub > Messages > Signatures Page");
			fc.utobj().printTestStep("Add Signatures");
			String signatureTitle = fc.utobj().generateTestData(dataSet.get("signatureTitle"));
			String signatureText = fc.utobj().generateTestData(dataSet.get("signatureText"));
			addSignatures(driver, signatureTitle, signatureText, config);

			fc.utobj().printTestStep("Delete Signatures");
			fc.utobj().showAll(driver);
			WebElement element = fc.utobj().getElementByXpath(driver, ".//*[contains(text () , '" + signatureTitle
					+ "')]/ancestor::tr/td[@class='thead']//td/a[.='Delete']");
			fc.utobj().clickElement(driver, element);
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Delete Signatures");
			fc.utobj().showAll(driver);
			boolean isTextPresent = fc.utobj().assertPageSource(driver, signatureTitle);
			if (isTextPresent == true) {
				fc.utobj().throwsException("was not able to delete signature");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_role","hub_messages"})
	@TestCase(createdOn = "2017-12-27", updatedOn = "2017-12-27", testCaseDescription = "Verify the functionality of Can view Messages Privilege By Corporate User", testCaseId = "TC_Hub_Can_View_Privilege_01")
	private void roleCanViewPrivilege() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Add New Corporate Role With The Hub > Can View Messages Privilege");
			AdminUsersRolesAddNewRolePageTest roles_page = new AdminUsersRolesAddNewRolePageTest();
			String roleType = dataSet.get("roleType");
			String roleName = fc.utobj().generateTestData("TcCorporateRole");
			String moduleName = "The Hub";
			String emailId = "hubautomation@staffex.com";

			Map<String, String> privileges = new HashMap<String, String>();
			privileges.put("Can View Messages", "Yes");
			roles_page.addRolesWithModulesPrivileges(driver, roleType, roleName, moduleName, privileges, testCaseId);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users > Add  Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporate_user = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser.setRole(roleName);
			corpUser = corporate_user.createDefaultUser(driver, corpUser);

			fc.utobj().printTestStep("Logged out from ADM User");
			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Log In with newly created Corporate user");
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());

			fc.utobj().printTestStep("Navigate To The Hub > Home Page");
			TheHubHomePage hub_homePage = new TheHubHomePage(driver);
			fc.hub().hub_common().theHubHome(driver);

			fc.utobj().printTestStep("Verify The Messages Widget At Home Page");
			fc.utobj().clickElement(driver, hub_homePage.manageWidgetsLink);

			List<WebElement> listElementWidget = driver
					.findElements(By.xpath(".//*[@id='manage_widgets']/div//tr/td/label"));
			boolean isWidgetMessages = false;

			for (WebElement webElement : listElementWidget) {

				String widgetOption = webElement.getText();

				if (widgetOption != null) {
					widgetOption = widgetOption.trim();

					if (widgetOption.equalsIgnoreCase(fc.utobj().translateString("Messages"))) {
						isWidgetMessages = true;
						break;
					}
				}
			}

			if (isWidgetMessages == false) {
				fc.utobj().throwsException("Not able to verify messages label in manage widget options");
			}

			if (isWidgetMessages) {
				if (!fc.utobj().isSelected(driver, hub_homePage.messagesHomePageWidgets)) {
					fc.utobj().clickElement(driver, hub_homePage.messagesHomePageWidgets);
					fc.utobj().clickElement(driver, hub_homePage.saveBtn);
				}

				fc.utobj().clickElement(driver, hub_homePage.cancelBtn);
			}

			boolean isMessagesSpanPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[@id='messagesHomePageHeading']");
			if (isMessagesSpanPresent == false) {
				fc.utobj().throwsException("Not able to verify the Messages Span at The Hub Home Page");
			}

			fc.utobj().printTestStep("Navigate To The Hub > Directory Page");
			fc.hub().hub_common().theHubDirectorySubModule(driver);

			fc.utobj().printTestStep("Click over Corporate User Tab");
			fc.utobj().clickElement(driver, new TheHubDirectoryPage(driver).corporateUsersTab);

			fc.utobj().printTestStep("Verify The Send Messages Button At Directory Page");
			WebElement element = fc.utobj().getElement(driver, new TheHubDirectoryPage(driver).sendMessages);

			boolean isElementPresent = fc.utobj().isElementPresent(driver, element);

			if (isElementPresent == false) {
				fc.utobj().throwsException("Not able to verify The Send Messages Button At Directory Page");
			}

			fc.utobj().printTestStep("Navigate To The Hub > Messages > Compose");
			fc.hub().hub_common().theHubMessageSubModule(driver);

			fc.utobj().printTestStep("Click over Compose Tab");
			fc.utobj().clickElement(driver, new TheHubMessagesPage(driver).composeLink);

			boolean isComposeElementPresent = fc.utobj().isElementPresent(driver,
					new TheHubMessagesPage(driver).toEmailTextArea);

			if (isComposeElementPresent == false) {
				fc.utobj().throwsException("Not able to verify Compose Tab At The Hub > Messages Page");
			}

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users Page");
			fc.adminpage().adminUsersManageCorporateUsersPage(driver);

			fc.utobj().printTestStep("Verify The Send Message Button");
			AdminUsersManageCorporateUsersPage corporate_userPage = new AdminUsersManageCorporateUsersPage(driver);

			boolean isTextPresent = fc.utobj().isElementPresent(driver,
					fc.utobj().getElement(driver, corporate_userPage.sendMessagesBtn));

			if (isTextPresent == false) {
				fc.utobj().throwsException("Not able to verify the send message button at Manage Corporate User Page");
			}

			fc.utobj().printTestStep("Verify The Send Message menu option at Action Image button");

			fc.utobj().sendKeys(driver, corporate_userPage.search, corpUser.getuserFullName());
			fc.utobj().clickElement(driver, corporate_userPage.searchButton);

			boolean isSendMessagePresent = false;

			try {
				fc.utobj().actionImgOption(driver, corpUser.getuserFullName(),
						fc.utobj().translateString("Send Message"));
				isSendMessagePresent = true;

			} catch (Exception e) {
				isSendMessagePresent = false;
			}

			if (isSendMessagePresent == false) {
				fc.utobj().throwsException("Not able to verify The Send Message menu option at Action Image button");
			}

			fc.utobj().printTestStep("Click Logged User name > Search > Search Users > Send Message");
			FCHomePage home_page = new FCHomePage(driver);
			fc.utobj().clickElement(driver, home_page.userOptions);
			fc.utobj().clickElement(driver, home_page.search);
			fc.utobj().clickElement(driver, home_page.searchUserTab);
			fc.utobj().sendKeys(driver, home_page.userId, corpUser.getUserName());
			fc.utobj().clickElement(driver, home_page.submitBtn);

			fc.utobj().printTestStep("Verify Send Message option in action menu list");
			boolean isOptionPresent = false;

			try {
				fc.utobj().actionImgOption(driver, corpUser.getuserFullName(),
						fc.utobj().translateString("Send Message"));
				isOptionPresent = true;
			} catch (Exception e) {
				isOptionPresent = false;
			}

			if (isOptionPresent == false) {
				fc.utobj().throwsException("Not able to verrify the Send Message option in action menu list");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_role","hub_messages" })
	@TestCase(createdOn = "2017-12-27", updatedOn = "2017-12-27", testCaseDescription = "Verify the functionality of Can view Messages Privilege By Divisional User", testCaseId = "TC_Hub_Can_View_Privilege_02")
	private void roleCanViewPrivilege02() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Add New Divisional Role With The Hub > Can View Messages Privilege");
			AdminUsersRolesAddNewRolePageTest roles_page = new AdminUsersRolesAddNewRolePageTest();
			String roleType = dataSet.get("roleType");
			String roleName = fc.utobj().generateTestData("TcDivRole");
			String moduleName = "The Hub";
			String emailId = "hubautomation@staffex.com";
			String userName = fc.utobj().generateTestData("du");
			String password = "t0n1ght@123";
			String divisionName = fc.utobj().generateTestData("TestDiv");
			Map<String, String> privileges = new HashMap<String, String>();
			privileges.put("Can View Messages", "Yes");
			roles_page.addRolesWithModulesPrivileges(driver, roleType, roleName, moduleName, privileges, testCaseId);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Divisional Users > Add Divisional User");
			AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest division_user = new AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest();
			division_user.addDivisionalUserWithRole(driver, userName, divisionName, roleName, password, emailId);

			fc.utobj().printTestStep("Logged out from ADM User");
			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Log In with newly created Divisional user");
			fc.loginpage().loginWithParameter(driver, userName, password);

			fc.utobj().printTestStep("Navigate To The Hub > Home Page");
			TheHubHomePage hub_homePage = new TheHubHomePage(driver);
			fc.hub().hub_common().theHubHome(driver);

			fc.utobj().printTestStep("Verify The Messages Widget At Home Page");
			fc.utobj().clickElement(driver, hub_homePage.manageWidgetsLink);

			List<WebElement> listElementWidget = driver
					.findElements(By.xpath(".//*[@id='manage_widgets']/div//tr/td/label"));
			boolean isWidgetMessages = false;

			for (WebElement webElement : listElementWidget) {

				String widgetOption = webElement.getText();

				if (widgetOption != null) {
					widgetOption = widgetOption.trim();

					if (widgetOption.equalsIgnoreCase(fc.utobj().translateString("Messages"))) {
						isWidgetMessages = true;
						break;
					}
				}
			}

			if (isWidgetMessages == false) {
				fc.utobj().throwsException("Not able to verify messages label in manage widget options");
			}

			if (isWidgetMessages) {
				if (!fc.utobj().isSelected(driver, hub_homePage.messagesHomePageWidgets)) {
					fc.utobj().clickElement(driver, hub_homePage.messagesHomePageWidgets);
					fc.utobj().clickElement(driver, hub_homePage.saveBtn);
				}

				fc.utobj().clickElement(driver, hub_homePage.cancelBtn);
			}

			boolean isMessagesSpanPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[@id='messagesHomePageHeading']");
			if (isMessagesSpanPresent == false) {
				fc.utobj().throwsException("Not able to verify the Messages Span at The Hub Home Page");
			}

			fc.utobj().printTestStep("Navigate To The Hub > Directory Page");
			fc.hub().hub_common().theHubDirectorySubModule(driver);

			fc.utobj().printTestStep("Click over Divisional User Tab");
			fc.utobj().clickElement(driver, new TheHubDirectoryPage(driver).divisionalUserTab);

			fc.utobj().printTestStep("Verify The Send Messages Button At Directory Page");
			WebElement element = fc.utobj().getElement(driver, new TheHubDirectoryPage(driver).sendMessages);

			boolean isElementPresent = fc.utobj().isElementPresent(driver, element);

			if (isElementPresent == false) {
				fc.utobj().throwsException("Not able to verify The Send Messages Button At Directory Page");
			}

			fc.utobj().printTestStep("Navigate To The Hub > Messages > Compose");
			fc.hub().hub_common().theHubMessageSubModule(driver);

			fc.utobj().printTestStep("Click over Compose Tab");
			fc.utobj().clickElement(driver, new TheHubMessagesPage(driver).composeLink);

			boolean isComposeElementPresent = fc.utobj().isElementPresent(driver,
					new TheHubMessagesPage(driver).toEmailTextArea);

			if (isComposeElementPresent == false) {
				fc.utobj().throwsException("Not able to verify Compose Tab At The Hub > Messages Page");
			}

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Divisional Users Page");
			fc.adminpage().openadminUsersManageDivisionalUsersLnk(driver);

			fc.utobj().printTestStep("Verify The Send Message Button");
			AdminUsersManageDivisionalUsersAddDivisionalUsersPage division_userPage = new AdminUsersManageDivisionalUsersAddDivisionalUsersPage(
					driver);

			boolean isTextPresent = fc.utobj().isElementPresent(driver,
					fc.utobj().getElement(driver, division_userPage.sendMessagesBtn));

			if (isTextPresent == false) {
				fc.utobj().throwsException("Not able to verify the send message button at Manage Divisional User Page");
			}

			fc.utobj().printTestStep("Verify The Send Message menu option at Action Image button");

			fc.utobj().sendKeys(driver, division_userPage.search, userName);
			fc.utobj().clickElement(driver, division_userPage.searchButton);

			boolean isSendMessagePresent = false;

			try {
				fc.utobj().actionImgOption(driver, userName, fc.utobj().translateString("Send Message"));
				isSendMessagePresent = true;

			} catch (Exception e) {
				isSendMessagePresent = false;
			}

			if (isSendMessagePresent == false) {
				fc.utobj().throwsException("Not able to verify The Send Message menu option at Action Image button");
			}

			fc.utobj().printTestStep("Click Logged User name > Search > Search Users > Send Message");
			FCHomePage home_page = new FCHomePage(driver);
			fc.utobj().clickElement(driver, home_page.userOptions);
			fc.utobj().clickElement(driver, home_page.search);
			fc.utobj().clickElement(driver, home_page.searchUserTab);
			fc.utobj().sendKeys(driver, home_page.userId, userName);
			fc.utobj().clickElement(driver, home_page.submitBtn);

			fc.utobj().printTestStep("Verify Send Message option in action menu list");
			boolean isOptionPresent = false;

			try {
				fc.utobj().actionImgOption(driver, userName, fc.utobj().translateString("Send Message"));
				isOptionPresent = true;
			} catch (Exception e) {
				isOptionPresent = false;
			}

			if (isOptionPresent == false) {
				fc.utobj().throwsException("Not able to verrify the Send Message option in action menu list");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_role" ,"hub_messages"})
	@TestCase(createdOn = "2017-12-27", updatedOn = "2017-12-27", testCaseDescription = "Verify the functionality of Can view Messages Privilege By Regional User", testCaseId = "TC_Hub_Can_View_Privilege_03")
	private void roleCanViewPrivilege03() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Add New Regional Role With The Hub > Can View Messages Privilege");
			AdminUsersRolesAddNewRolePageTest roles_page = new AdminUsersRolesAddNewRolePageTest();
			String roleType = dataSet.get("roleType");
			String roleName = fc.utobj().generateTestData("TcRegionalRole");
			String moduleName = "The Hub";
			String emailId = "hubautomation@staffex.com";
			String userName = fc.utobj().generateTestData("ru");
			String password = "t0n1ght@123";
			String regionName = fc.utobj().generateTestData("TestReg");
			Map<String, String> privileges = new HashMap<String, String>();
			privileges.put("Can View Messages", "Yes");

			roles_page.addRolesWithModulesPrivileges(driver, roleType, roleName, moduleName, privileges, testCaseId);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Regional Users > Add Regional User");
			AdminUsersManageRegionalUsersAddRegionalUserPageTest region_page = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
			region_page.addRegionalUserWithRole(driver, userName, password, regionName, roleName, emailId);

			fc.utobj().printTestStep("Logged out from ADM User");
			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Log In with newly created Regional user");
			fc.loginpage().loginWithParameter(driver, userName, password);

			fc.utobj().printTestStep("Navigate To The Hub > Home Page");
			TheHubHomePage hub_homePage = new TheHubHomePage(driver);
			fc.hub().hub_common().theHubHome(driver);

			fc.utobj().printTestStep("Verify The Messages Widget At Home Page");
			fc.utobj().clickElement(driver, hub_homePage.manageWidgetsLink);

			List<WebElement> listElementWidget = driver
					.findElements(By.xpath(".//*[@id='manage_widgets']/div//tr/td/label"));
			boolean isWidgetMessages = false;

			for (WebElement webElement : listElementWidget) {

				String widgetOption = webElement.getText();

				if (widgetOption != null) {
					widgetOption = widgetOption.trim();

					if (widgetOption.equalsIgnoreCase(fc.utobj().translateString("Messages"))) {
						isWidgetMessages = true;
						break;
					}
				}
			}

			if (isWidgetMessages == false) {
				fc.utobj().throwsException("Not able to verify messages label in manage widget options");
			}

			if (isWidgetMessages) {
				if (!fc.utobj().isSelected(driver, hub_homePage.messagesHomePageWidgets)) {
					fc.utobj().clickElement(driver, hub_homePage.messagesHomePageWidgets);
					fc.utobj().clickElement(driver, hub_homePage.saveBtn);
				}

				fc.utobj().clickElement(driver, hub_homePage.cancelBtn);
			}

			boolean isMessagesSpanPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[@id='messagesHomePageHeading']");
			if (isMessagesSpanPresent == false) {
				fc.utobj().throwsException("Not able to verify the Messages Span at The Hub Home Page");
			}

			fc.utobj().printTestStep("Navigate To The Hub > Directory Page");
			fc.hub().hub_common().theHubDirectorySubModule(driver);

			fc.utobj().printTestStep("Click over Regional User Tab");
			fc.utobj().clickElement(driver, new TheHubDirectoryPage(driver).regionalUserTab);

			fc.utobj().printTestStep("Verify The Send Messages Button At Directory Page");
			WebElement element = fc.utobj().getElement(driver, new TheHubDirectoryPage(driver).sendMessages);

			boolean isElementPresent = fc.utobj().isElementPresent(driver, element);

			if (isElementPresent == false) {
				fc.utobj().throwsException("Not able to verify The Send Messages Button At Directory Page");
			}

			fc.utobj().printTestStep("Navigate To The Hub > Messages > Compose");
			fc.hub().hub_common().theHubMessageSubModule(driver);

			fc.utobj().printTestStep("Click over Compose Tab");
			fc.utobj().clickElement(driver, new TheHubMessagesPage(driver).composeLink);

			boolean isComposeElementPresent = fc.utobj().isElementPresent(driver,
					new TheHubMessagesPage(driver).toEmailTextArea);

			if (isComposeElementPresent == false) {
				fc.utobj().throwsException("Not able to verify Compose Tab At The Hub > Messages Page");
			}

			fc.utobj().printTestStep("Click Logged User name > Search > Search Users > Send Message");
			FCHomePage home_page = new FCHomePage(driver);
			fc.utobj().clickElement(driver, home_page.userOptions);
			fc.utobj().clickElement(driver, home_page.search);
			fc.utobj().clickElement(driver, home_page.searchUserTab);
			fc.utobj().sendKeys(driver, home_page.userId, userName);
			fc.utobj().clickElement(driver, home_page.submitBtn);

			fc.utobj().printTestStep("Verify Send Message option in action menu list");
			boolean isOptionPresent = false;

			try {
				fc.utobj().actionImgOption(driver, userName, fc.utobj().translateString("Send Message"));
				isOptionPresent = true;
			} catch (Exception e) {
				isOptionPresent = false;
			}

			if (isOptionPresent == false) {
				fc.utobj().throwsException("Not able to verrify the Send Message option in action menu list");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_role" ,"hub_messages"})
	@TestCase(createdOn = "2017-12-28", updatedOn = "2017-12-28", testCaseDescription = "Verify the functionality of Can view Messages Privilege By Franchise User", testCaseId = "TC_Hub_Can_View_Privilege_04")
	private void roleCanViewPrivilege04() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Add New Franchise Role With The Hub > Can View Messages Privilege");
			AdminUsersRolesAddNewRolePageTest roles_page = new AdminUsersRolesAddNewRolePageTest();
			String roleType = dataSet.get("roleType");
			String roleName = fc.utobj().generateTestData("TcFranRole");
			String moduleName = "The Hub";
			String emailId = "hubautomation@staffex.com";
			String userName = fc.utobj().generateTestData("fu");
			String password = "t0n1ght@123";
			String regionName = fc.utobj().generateTestData("TestReg");
			String storeType = fc.utobj().generateTestData("TestStore");
			String franchiseName = fc.utobj().generateTestData("TestFran");
			String centerName = fc.utobj().generateTestData("center");
			String openingDate = fc.utobj().getCurrentDateUSFormat();
			String ownerFirstName = fc.utobj().generateTestData("Testfname");
			String ownerLastName = fc.utobj().generateTestData("Testlname");

			Map<String, String> privileges = new HashMap<String, String>();
			privileges.put("Can View Messages", "Yes");

			roles_page.addRolesWithModulesPrivileges(driver, roleType, roleName, moduleName, privileges, testCaseId);

			fc.utobj().printTestStep(
					"Navigate To Admin > Franchise Location > Manage Franchise Locations > Add Franchise Location");
			AdminFranchiseLocationAddFranchiseLocationPageTest location_page = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String franchiseId = location_page.addFranchiseLocation(driver, regionName, storeType, franchiseName,
					centerName, openingDate, ownerFirstName, ownerLastName, config);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Franchise Users > Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest franchise_page = new AdminUsersManageManageFranchiseUsersPageTest();
			franchise_page.addFranchiseUser(driver, userName, password, franchiseId, roleName, emailId);

			fc.utobj().printTestStep("Logged out from ADM User");
			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Log In with newly created Franchise user");
			fc.loginpage().loginWithParameter(driver, userName, password);

			fc.utobj().printTestStep("Navigate To The Hub > Home Page");
			TheHubHomePage hub_homePage = new TheHubHomePage(driver);
			fc.hub().hub_common().theHubHome(driver);

			fc.utobj().printTestStep("Verify The Messages Widget At Home Page");
			fc.utobj().clickElement(driver, hub_homePage.manageWidgetsLink);

			List<WebElement> listElementWidget = driver
					.findElements(By.xpath(".//*[@id='manage_widgets']/div//tr/td/label"));
			boolean isWidgetMessages = false;

			for (WebElement webElement : listElementWidget) {

				String widgetOption = webElement.getText();

				if (widgetOption != null) {
					widgetOption = widgetOption.trim();

					if (widgetOption.equalsIgnoreCase(fc.utobj().translateString("Messages"))) {
						isWidgetMessages = true;
						break;
					}
				}
			}

			if (isWidgetMessages == false) {
				fc.utobj().throwsException("Not able to verify messages label in manage widget options");
			}

			if (isWidgetMessages) {
				if (!fc.utobj().isSelected(driver, hub_homePage.messagesHomePageWidgets)) {
					fc.utobj().clickElement(driver, hub_homePage.messagesHomePageWidgets);
					fc.utobj().clickElement(driver, hub_homePage.saveBtn);
				}

				fc.utobj().clickElement(driver, hub_homePage.cancelBtn);
			}

			boolean isMessagesSpanPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[@id='messagesHomePageHeading']");
			if (isMessagesSpanPresent == false) {
				fc.utobj().throwsException("Not able to verify the Messages Span at The Hub Home Page");
			}

			fc.utobj().printTestStep("Navigate To The Hub > Directory Page");
			fc.hub().hub_common().theHubDirectorySubModule(driver);

			fc.utobj().printTestStep("Click over Franchise User Tab");
			fc.utobj().clickElement(driver, new TheHubDirectoryPage(driver).franchiseUsersTab);

			fc.utobj().printTestStep("Verify The Send Messages Button At Directory Page");
			WebElement element = fc.utobj().getElement(driver, new TheHubDirectoryPage(driver).sendMessages);

			boolean isElementPresent = fc.utobj().isElementPresent(driver, element);

			if (isElementPresent == false) {
				fc.utobj().throwsException("Not able to verify The Send Messages Button At Directory Page");
			}

			fc.utobj().printTestStep("Navigate To The Hub > Messages > Compose");
			fc.hub().hub_common().theHubMessageSubModule(driver);

			fc.utobj().printTestStep("Click over Compose Tab");
			fc.utobj().clickElement(driver, new TheHubMessagesPage(driver).composeLink);

			boolean isComposeElementPresent = fc.utobj().isElementPresent(driver,
					new TheHubMessagesPage(driver).toEmailTextArea);

			if (isComposeElementPresent == false) {
				fc.utobj().throwsException("Not able to verify Compose Tab At The Hub > Messages Page");
			}

			fc.utobj().printTestStep("Click Logged User name > Search > Search Users > Send Message");
			FCHomePage home_page = new FCHomePage(driver);
			fc.utobj().clickElement(driver, home_page.userOptions);
			fc.utobj().clickElement(driver, home_page.search);
			fc.utobj().clickElement(driver, home_page.searchUserTab);
			fc.utobj().sendKeys(driver, home_page.userId, userName);
			fc.utobj().clickElement(driver, home_page.submitBtn);

			fc.utobj().printTestStep("Verify Send Message option in action menu list");
			boolean isOptionPresent = false;

			try {
				fc.utobj().actionImgOption(driver, ownerFirstName, fc.utobj().translateString("Send Message"));
				isOptionPresent = true;
			} catch (Exception e) {
				isOptionPresent = false;
			}

			if (isOptionPresent == false) {
				fc.utobj().throwsException("Not able to verrify the Send Message option in action menu list");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_role","hub_messages" })
	@TestCase(createdOn = "2017-12-28", updatedOn = "2017-12-28", testCaseDescription = "Verify the functionality of Can view Messages Privilege By Corporate User", testCaseId = "TC_Hub_Can_View_Privilege_05")
	private void roleCanViewPrivilege05() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj()
					.printTestStep("Go to admin > Roles > Create Corporate Role with Can View Messages as deselected");

			AdminUsersRolesAddNewRolePageTest roles_page = new AdminUsersRolesAddNewRolePageTest();
			String roleType = dataSet.get("roleType");
			String roleName = fc.utobj().generateTestData("TcCorporateRole");
			String moduleName = "The Hub";
			String emailId = "hubautomation@staffex.com";

			Map<String, String> privileges = new HashMap<String, String>();
			privileges.put("Can View Messages", "No");
			roles_page.addRolesWithModulesPrivileges(driver, roleType, roleName, moduleName, privileges, testCaseId);

			fc.utobj().printTestStep(
					"Navigate To Admin > Users > Manage Corporate Users > Add  Corporate User with added role");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporate_user = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser.setRole(roleName);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser = corporate_user.createDefaultUser(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users > Add  Corporate User");
			CorporateUser corpUser1 = new CorporateUser();
			corpUser1 = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser1);
			corpUser1.setEmail(emailId);
			corpUser1 = corporate_user.createDefaultUser(driver, corpUser1);

			fc.utobj().printTestStep("Logged out from ADM User");
			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Log In with newly created Corporate user");
			fc.loginpage().loginWithParameter(driver, corpUser1.getUserName(), corpUser1.getPassword());

			fc.utobj().printTestStep("Navigate To The Hub > Directory Page");
			fc.hub().hub_common().theHubDirectorySubModule(driver);

			fc.utobj().printTestStep("Click over Corporate User Tab");
			TheHubDirectoryPage directory_page = new TheHubDirectoryPage(driver);

			fc.utobj().clickElement(driver, directory_page.corporateUsersTab);

			fc.utobj().printTestStep("Search by user name");
			fc.utobj().sendKeys(driver, directory_page.searchTxBx, corpUser.getuserFullName());
			fc.utobj().clickElement(driver, directory_page.searchBtn);

			fc.utobj()
					.printTestStep("Verify The Corporate user's privilegs that not having can view messages privilege");

			boolean isCheckBoxDisable = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text() , '"
					+ corpUser.getuserFullName() + "')]/ancestor::tr/td/input[@name='contactidcorp_noUse']");

			if (isCheckBoxDisable == false) {
				fc.utobj().throwsException(
						"Not able to verify the can view messages privilege at the hub directory page");
			}
			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users Page");
			fc.adminpage().adminUsersManageCorporateUsersPage(driver);

			fc.utobj().printTestStep("Verify The Send Message Button");
			AdminUsersManageCorporateUsersPage corporate_userPage = new AdminUsersManageCorporateUsersPage(driver);

			fc.utobj().printTestStep("Search Corporate User");
			fc.utobj().sendKeys(driver, corporate_userPage.search, corpUser.getuserFullName());
			fc.utobj().clickElement(driver, corporate_userPage.searchButton);
			fc.utobj().printTestStep(
					"Verify the check box should be deselected and while clicking over send message button alert should come");

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'"
					+ corpUser.getuserFullName() + "')]/ancestor::tr/td/input[@name='contactidcorp']"));
			fc.utobj().clickElement(driver, corporate_userPage.sendMessagesBtn);

			boolean isAlertPresent = fc.utobj().isAlertPresent(driver);
			String alertText = null;
			if (isAlertPresent) {

				alertText = fc.utobj().acceptAlertBox(driver);

				if (alertText != null) {
					alertText = alertText.trim();

					if (!alertText.contains("Following user(s) do not have message privilege")
							&& alertText.contains(corpUser.getuserFullName())) {
						fc.utobj().throwsException("Not able to verify alert text for privileges");
					}
				}
			} else if (isAlertPresent == false) {
				fc.utobj().throwsException("No alert present for privileges");
			}

			fc.utobj().printTestStep("Click Logged User name > Search > Search Users > Send Message");
			FCHomePage home_page = new FCHomePage(driver);
			fc.utobj().clickElement(driver, home_page.userOptions);
			fc.utobj().clickElement(driver, home_page.search);
			fc.utobj().clickElement(driver, home_page.searchUserTab);
			fc.utobj().sendKeys(driver, home_page.userId, corpUser.getUserName());
			fc.utobj().clickElement(driver, home_page.submitBtn);

			fc.utobj().printTestStep("Verify Send Message option in action menu list");
			boolean isOptionPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + corpUser.getuserFullName() + "')]/ancestor::tr/td/div[@id='menuBar']");
			if (isOptionPresent) {
				fc.utobj().throwsException(
						"Not able to verify User name > Search > Search Users > Send Message option in action menu option");
			}

			// Need To Be Discussed with PankajK

			/*
			 * fc.utobj().printTestStep(
			 * "Navigate To Hub > Messages > Compose Page");
			 * fc.hub().hub_common().theHubMessageSubModule(driver);
			 * TheHubMessagesPage messages_page = new
			 * TheHubMessagesPage(driver); CommonUI coui = new CommonUI(driver);
			 * fc.utobj().clickElement(driver, messages_page.composeLink);
			 * 
			 * fc.utobj().printTestStep(
			 * "Verify The Users who don't have Messages access, should not be listed into To, CC and Bcc address links"
			 * ); fc.utobj().clickElement(driver,
			 * messages_page.addressBookToLnk);
			 * fc.commonMethods().switch_cboxIframe_frameId(driver);
			 * fc.utobj().sendKeys(driver, directory_page.searchTxBxCU,
			 * corpUser.getuserFullName()); fc.utobj().clickElement(driver,
			 * directory_page.searchCUImgIcon);
			 * 
			 * boolean isToAddressBook = fc.utobj().assertPageSource(driver,
			 * corpUser.getuserFullName()); if (isToAddressBook) {
			 * fc.utobj().throwsException(
			 * "Users who don't have Messages access, is showing in To"); }
			 * fc.utobj().clickElement(driver, coui.Close_Input_ByValue);
			 * fc.utobj().switchFrameToDefault(driver);
			 * 
			 * fc.utobj().clickElement(driver, messages_page.addressBookCCLnk);
			 * fc.commonMethods().switch_cboxIframe_frameId(driver);
			 * fc.utobj().sendKeys(driver, directory_page.searchTxBxCU,
			 * corpUser.getuserFullName()); fc.utobj().clickElement(driver,
			 * directory_page.searchCUImgIcon);
			 * 
			 * boolean isCCAddressBook = fc.utobj().assertPageSource(driver,
			 * corpUser.getuserFullName()); if (isCCAddressBook == true) {
			 * fc.utobj().throwsException(
			 * "Users who don't have Messages access, is showing in CC"); }
			 * fc.utobj().clickElement(driver, coui.Close_Input_ByValue);
			 * fc.utobj().switchFrameToDefault(driver);
			 * 
			 * fc.utobj().clickElement(driver, messages_page.addressBookBCCLnk);
			 * fc.commonMethods().switch_cboxIframe_frameId(driver);
			 * fc.utobj().sendKeys(driver, directory_page.searchTxBxCU,
			 * corpUser.getuserFullName()); fc.utobj().clickElement(driver,
			 * directory_page.searchCUImgIcon);
			 * 
			 * boolean isBCCAddressBook = fc.utobj().assertPageSource(driver,
			 * corpUser.getuserFullName()); if (isBCCAddressBook == true) {
			 * fc.utobj().throwsException(
			 * "Users who don't have Messages access, is showing in BCC"); }
			 * fc.utobj().clickElement(driver, coui.Close_Input_ByValue);
			 * fc.utobj().switchFrameToDefault(driver);
			 */

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_role" ,"hub_messages"})
	@TestCase(createdOn = "2017-12-28", updatedOn = "2017-12-28", testCaseDescription = "Verify the functionality of Can view Messages Privilege By Divisional User", testCaseId = "TC_Hub_Can_View_Privilege_06")
	private void roleCanViewPrivilege06() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj()
					.printTestStep("Go to admin > Roles > Create Divisional Role with Can View Messages as deselected");

			AdminUsersRolesAddNewRolePageTest roles_page = new AdminUsersRolesAddNewRolePageTest();
			String roleType = dataSet.get("roleType");
			String roleName = fc.utobj().generateTestData("TcDivisionalRole");
			String moduleName = "The Hub";
			String emailId = "hubautomation@staffex.com";
			String userName = fc.utobj().generateTestData("Du");
			String password = "t0n1ght@123";
			String divisionName = fc.utobj().generateTestData("Testdiv");

			Map<String, String> privileges = new HashMap<String, String>();
			privileges.put("Can View Messages", "No");
			roles_page.addRolesWithModulesPrivileges(driver, roleType, roleName, moduleName, privileges, testCaseId);

			fc.utobj().printTestStep(
					"Navigate To Admin > Users > Manage Divisional Users > Add  Divisional User with added role");
			AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest divisional_user = new AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest();
			String roleUserFullName = divisional_user.addDivisionalUserWithRole(driver, userName, divisionName,
					roleName, password, emailId);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Divisional Users > Add  Divisional User");
			String loginUserName = fc.utobj().generateTestData("testdu");

			divisional_user.addDivisionalUser(driver, loginUserName, password, divisionName, emailId);

			fc.utobj().printTestStep("Logged out from ADM User");
			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Log In with newly created Divisional user");
			fc.loginpage().loginWithParameter(driver, loginUserName, password);

			fc.utobj().printTestStep("Navigate To The Hub > Directory Page");
			fc.hub().hub_common().theHubDirectorySubModule(driver);

			fc.utobj().printTestStep("Click over Divisional User Tab");
			TheHubDirectoryPage directory_page = new TheHubDirectoryPage(driver);

			fc.utobj().clickElement(driver, directory_page.divisionalUserTab);

			fc.utobj().printTestStep("Search by user name");
			fc.utobj().sendKeys(driver, directory_page.searchTxBx, userName);
			fc.utobj().clickElement(driver, directory_page.searchBtn);

			fc.utobj().printTestStep(
					"Verify The Divisional user's privilegs that not having can view messages privilege");

			boolean isCheckBoxDisable = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text() , '"
					+ roleUserFullName + "')]/ancestor::tr/td/input[@name='contactiddiv_noUse']");

			if (isCheckBoxDisable == false) {
				fc.utobj().throwsException(
						"Not able to verify the can view messages privilege at the hub directory page");
			}

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Divisional Users Page");
			fc.adminpage().openadminUsersManageDivisionalUsersLnk(driver);

			fc.utobj().printTestStep("Verify The Send Message Button");
			AdminUsersManageDivisionalUsersAddDivisionalUsersPage division_userPage = new AdminUsersManageDivisionalUsersAddDivisionalUsersPage(
					driver);

			fc.utobj().printTestStep("Search Divisional User");
			fc.utobj().sendKeys(driver, division_userPage.search, userName);
			fc.utobj().clickElement(driver, division_userPage.searchButton);

			fc.utobj().printTestStep(
					"Verify the check box should be deselected and while clicking over send message button alert should come");

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text(),'" + roleUserFullName + "')]/ancestor::tr/td/input[@name='contactidcorp']"));
			fc.utobj().clickElement(driver, division_userPage.sendMessagesBtn);

			boolean isAlertPresent = fc.utobj().isAlertPresent(driver);
			String alertText = null;
			if (isAlertPresent) {

				alertText = fc.utobj().acceptAlertBox(driver);

				if (alertText != null) {
					alertText = alertText.trim();

					if (!alertText.contains("Following user(s) do not have message privilege")
							&& alertText.contains(roleUserFullName)) {
						fc.utobj().throwsException("Not able to verify alert text for privileges");
					}
				}
			} else if (isAlertPresent == false) {
				fc.utobj().throwsException("No alert present for privileges");
			}

			fc.utobj().printTestStep("Click Logged User name > Search > Search Users > Send Message");
			FCHomePage home_page = new FCHomePage(driver);
			fc.utobj().clickElement(driver, home_page.userOptions);
			fc.utobj().clickElement(driver, home_page.search);
			fc.utobj().clickElement(driver, home_page.searchUserTab);
			fc.utobj().sendKeys(driver, home_page.userId, userName);
			fc.utobj().clickElement(driver, home_page.submitBtn);

			fc.utobj().printTestStep("Verify Send Message option in action menu list");
			boolean isOptionPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + roleUserFullName + "')]/ancestor::tr/td/div[@id='menuBar']");

			if (isOptionPresent) {
				fc.utobj().throwsException(
						"Not able to verify User name > Search > Search Users > Send Message option in action menu option");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_role" ,"hub_messages"})
	@TestCase(createdOn = "2018-01-02", updatedOn = "2018-01-02", testCaseDescription = "Verify the functionality of Can view Messages Privilege By Regional User", testCaseId = "TC_Hub_Can_View_Privilege_07")
	private void roleCanViewPrivilege07() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Go to admin > Roles > Create Regional Role with Can View Messages as deselected");

			AdminUsersRolesAddNewRolePageTest roles_page = new AdminUsersRolesAddNewRolePageTest();
			String roleType = dataSet.get("roleType");
			String roleName = fc.utobj().generateTestData("TcRegionalRole");
			String moduleName = "The Hub";
			String emailId = "hubautomation@staffex.com";
			String userName = fc.utobj().generateTestData("Ru");
			String password = "t0n1ght@123";
			String regionName = fc.utobj().generateTestData("TestReg");

			Map<String, String> privileges = new HashMap<String, String>();
			privileges.put("Can View Messages", "No");
			roles_page.addRolesWithModulesPrivileges(driver, roleType, roleName, moduleName, privileges, testCaseId);

			fc.utobj().printTestStep(
					"Navigate To Admin > Users > Manage Regional Users > Add  Regional User with added role");
			AdminUsersManageRegionalUsersAddRegionalUserPageTest region_page = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
			String roleUserFullName = region_page.addRegionalUserWithRole(driver, userName, password, regionName,
					roleName, emailId);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Regional Users > Add  Regional User");
			String loginUserName = fc.utobj().generateTestData("testru");

			region_page.addRegionalUser(driver, loginUserName, password, regionName, emailId);

			fc.utobj().printTestStep("Logged out from ADM User");
			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Log In with newly created Regional user");
			fc.loginpage().loginWithParameter(driver, loginUserName, password);

			fc.utobj().printTestStep("Navigate To The Hub > Directory Page");
			fc.hub().hub_common().theHubDirectorySubModule(driver);

			fc.utobj().printTestStep("Click over Regional User Tab");
			TheHubDirectoryPage directory_page = new TheHubDirectoryPage(driver);

			fc.utobj().clickElement(driver, directory_page.regionalUserTab);

			fc.utobj().printTestStep("Search by user name");
			fc.utobj().sendKeys(driver, directory_page.searchTxBx, userName);
			fc.utobj().clickElement(driver, directory_page.searchBtn);

			fc.utobj()
					.printTestStep("Verify The Regional user's privilegs that not having can view messages privilege");

			boolean isCheckBoxDisable = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text() , '"
					+ roleUserFullName + "')]/ancestor::tr/td/input[@name='contactidreg_noUse']");

			if (isCheckBoxDisable == false) {
				fc.utobj().throwsException(
						"Not able to verify the can view messages privilege at the hub directory page");
			}

			fc.utobj().printTestStep("Click Logged User name > Search > Search Users > Send Message");
			FCHomePage home_page = new FCHomePage(driver);
			fc.utobj().clickElement(driver, home_page.userOptions);
			fc.utobj().clickElement(driver, home_page.search);
			fc.utobj().clickElement(driver, home_page.searchUserTab);
			fc.utobj().sendKeys(driver, home_page.userId, userName);
			fc.utobj().clickElement(driver, home_page.submitBtn);

			fc.utobj().printTestStep("Verify Send Message option in action menu list");
			boolean isOptionPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + roleUserFullName + "')]/ancestor::tr/td/div[@id='menuBar']");

			if (isOptionPresent) {
				fc.utobj().throwsException(
						"Not able to verify User name > Search > Search Users > Send Message option in action menu option");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_role" ,"hub_messages"})
	@TestCase(createdOn = "2018-01-02", updatedOn = "2018-01-02", testCaseDescription = "Verify the functionality of Can view Messages Privilege By Franchise User", testCaseId = "TC_Hub_Can_View_Privilege_08")
	private void roleCanViewPrivilege08() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj()
					.printTestStep("Go to admin > Roles > Create Franchise Role with Can View Messages as deselected");

			AdminUsersRolesAddNewRolePageTest roles_page = new AdminUsersRolesAddNewRolePageTest();
			String roleType = dataSet.get("roleType");
			String roleName = fc.utobj().generateTestData("TcFranchiseRole");
			String moduleName = "The Hub";
			String emailId = "hubautomation@staffex.com";
			String userName = fc.utobj().generateTestData("Fu");
			String password = "t0n1ght@123";
			String regionName = fc.utobj().generateTestData("TestRegf");
			String storeType = fc.utobj().generateTestData("TestStore");
			String franchiseName = fc.utobj().generateTestData("TestFran");
			String centerName = fc.utobj().generateTestData("center");
			String openingDate = fc.utobj().getCurrentDateUSFormat();
			String ownerFirstName = fc.utobj().generateTestData("Testfname");
			String ownerLastName = fc.utobj().generateTestData("Testlname");

			Map<String, String> privileges = new HashMap<String, String>();
			privileges.put("Can View Messages", "No");
			roles_page.addRolesWithModulesPrivileges(driver, roleType, roleName, moduleName, privileges, testCaseId);

			fc.utobj().printTestStep(
					"Navigate To Admin > Franchise Location > Manage Franchise Locations > Add Franchise Location");
			AdminFranchiseLocationAddFranchiseLocationPageTest location_page = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String franchiseId = location_page.addFranchiseLocation(driver, regionName, storeType, franchiseName,
					centerName, openingDate, ownerFirstName, ownerLastName, config);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Franchise Users > Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest franchise_page = new AdminUsersManageManageFranchiseUsersPageTest();
			franchise_page.addFranchiseUser(driver, userName, password, franchiseId, roleName, emailId);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Franchise Users > Add  Franchise User");
			String loginUserName = fc.utobj().generateTestData("testfu");
			franchise_page.addFranchiseUserEmployee(driver, loginUserName, password, franchiseId,
					fc.utobj().translateString("Default Franchise Role"), config, emailId);

			fc.utobj().printTestStep("Logged out from ADM User");
			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Log In with newly created Franchise user");
			fc.loginpage().loginWithParameter(driver, loginUserName, password);

			fc.utobj().printTestStep("Navigate To The Hub > Directory Page");
			fc.hub().hub_common().theHubDirectorySubModule(driver);

			fc.utobj().printTestStep("Click over Franchise User Tab");
			TheHubDirectoryPage directory_page = new TheHubDirectoryPage(driver);

			fc.utobj().clickElement(driver, directory_page.franchiseUsersTab);

			fc.utobj().printTestStep("Search by user name");
			fc.utobj().sendKeys(driver, directory_page.searchTxBx, ownerFirstName);
			fc.utobj().clickElement(driver, directory_page.searchBtn);

			String ownerFullName = ownerFirstName + " " + ownerLastName;

			fc.utobj()
					.printTestStep("Verify The Franchise user's privilegs that not having can view messages privilege");

			boolean isCheckBoxDisable = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text() , '"
					+ ownerFullName + "')]/ancestor::tr/td/input[@name='contactidreg_noUse']");

			if (isCheckBoxDisable == false) {
				fc.utobj().throwsException(
						"Not able to verify the can view messages privilege at the hub directory page");
			}

			fc.utobj().printTestStep("Click Logged User name > Search > Search Users > Send Message");
			FCHomePage home_page = new FCHomePage(driver);
			fc.utobj().clickElement(driver, home_page.userOptions);
			fc.utobj().clickElement(driver, home_page.search);
			fc.utobj().clickElement(driver, home_page.searchUserTab);
			fc.utobj().sendKeys(driver, home_page.userId, userName);
			fc.utobj().clickElement(driver, home_page.submitBtn);

			fc.utobj().printTestStep("Verify Send Message option in action menu list");
			boolean isOptionPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + ownerFullName + "')]/ancestor::tr/td/div[@id='menuBar']");

			if (isOptionPresent) {
				fc.utobj().throwsException(
						"Not able to verify User name > Search > Search Users > Send Message option in action menu option");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/*
	 * TC-10067
	 */

	/*
	 * QuickLinkGroup
	 */

	@Test(groups = { "thehub","hub_messages","mailtest0910"})
	@TestCase(createdOn = "2018-01-19", updatedOn = "2018-01-19", testCaseDescription = "To verify Create Group functionality through Quick links", testCaseId = "TC_Hub_Quick_Link_Create_Group_01")
	private void quickLinkCreateGroup() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubDirectoryPage pobj = new TheHubDirectoryPage(driver);

			String emailId = "hubautomation@staffex.com";
			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUserPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUserPage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Configure Quick Link Create Group");
			OptionsConfigureQuickLinkPageTest configure_notification = new OptionsConfigureQuickLinkPageTest();
			String linkToBeDisplayed = fc.utobj().translateString("Create New Group");
			configure_notification.configureQuickLnk(driver, linkToBeDisplayed, true);
			CommonUI coi = new CommonUI(driver);

			fc.utobj().printTestStep("Notification > Hub > Create Group");
			boolean isDisplayed = coi.showQuickLinks.isDisplayed();

			if (!isDisplayed) {
				fc.utobj().clickElement(driver, coi.showNotificationBar);
			}

			fc.utobj().clickElement(driver, coi.showQuickLinks);

			fc.utobj().clickElement(driver, coi.createNewGroupQuickLink);
			fc.utobj().clickElement(driver, pobj.internalAddressBook);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().sendKeys(driver, pobj.searchTxBxCU, corpUser.getuserFullName());
			fc.utobj().clickElement(driver, pobj.searchCUImgIcon);

			fc.utobj().sleep();
			/*fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[contains(text () , '" + corpUser.getuserFullName() + "')]/ancestor::tr/td/input"));*/
			fc.utobj().clickElement(driver, pobj.selectcorporate);
			fc.utobj().clickElement(driver, pobj.selectUser);
			fc.utobj().switchFrameToDefault(driver);
			
			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			fc.utobj().sendKeys(driver, pobj.grouopName, groupName);
			fc.utobj().clickElement(driver, pobj.createBtn);
			fc.utobj().showAll(driver);
			fc.utobj().isTextDisplayed(driver, groupName, "was not able to create group");

			fc.utobj().printTestStep("Verify The Create Group");
			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, groupName));
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().isTextDisplayed(driver, corpUser.getuserFullName(),
					"was not able to verify Name of Corporate User");
			fc.utobj().isTextDisplayed(driver, "Corporate", "was not able to verify User Type");
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			/*
			 * TC-10063
			 */

			fc.utobj().printTestStep("Verify the Export Tab Options under Messages");
			fc.utobj().clickElement(driver, new TheHubMessagesPage(driver).exportTab);

			List<WebElement> listElement = driver.findElements(By.xpath(".//input[@name='exportDataCategory']/.."));
			boolean b1[] = new boolean[3];

			for (int i = 0; i < listElement.size(); i++) {

				String text = listElement.get(i).getText().trim();

				if (text.equalsIgnoreCase("All Contacts")) {
					b1[0] = true;
				} else if (text.equalsIgnoreCase("All Personal Contacts")) {
					b1[1] = true;
				} else if (text.equalsIgnoreCase("All Global Contacts")) {
					b1[2] = true;
				}
			}

			if (b1[0] == false) {
				fc.utobj().throwsException("All Contacts Radio Button is not present");
			}
			if (b1[1] == false) {
				fc.utobj().throwsException("All Personal Contacts Radio Button is not present");
			}
			if (b1[2] == false) {
				fc.utobj().throwsException("All Global Contacts Radio Button is not present");
			}

			List<WebElement> listElementCorCheckBox = driver
					.findElements(By.xpath(".//input[@name='userTypeCorp']/.."));

			for (int i = 0; i < listElementCorCheckBox.size(); i++) {

				String text = listElementCorCheckBox.get(i).getText().trim();

				if (text.equalsIgnoreCase("All Corporate Contacts") == false) {

					fc.utobj().throwsException("All Corporate Contacts Check Box is not present");

				}
			}

			List<WebElement> listElementDivCheckBox = driver.findElements(By.xpath(".//input[@name='userTypeDiv']/.."));

			for (int i = 0; i < listElementDivCheckBox.size(); i++) {

				String text = listElementDivCheckBox.get(i).getText().trim();

				if (text.equalsIgnoreCase("All Division Contacts") == false) {

					fc.utobj().throwsException("All Division Contacts Check Box is not present");

				}
			}

			List<WebElement> listElementFranCheckBox = driver
					.findElements(By.xpath(".//input[@name='userTypeFran']/.."));

			for (int i = 0; i < listElementFranCheckBox.size(); i++) {

				String text = listElementFranCheckBox.get(i).getText().trim();

				if (text.equalsIgnoreCase("All Franchise Contacts") == false) {

					fc.utobj().throwsException("All Franchise Contacts Check Box is not present");

				}
			}

			List<WebElement> listElementRegCheckBox = driver.findElements(By.xpath(".//input[@name='userTypeReg']/.."));

			for (int i = 0; i < listElementRegCheckBox.size(); i++) {

				String text = listElementRegCheckBox.get(i).getText().trim();

				if (text.equalsIgnoreCase("All Regional Contacts") == false) {

					fc.utobj().throwsException("All Regional Contacts Check Box is not present");

				}
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/*
	 * TC-10059
	 */
	@Test(groups = { "thehub","hub_messages"})
	@TestCase(createdOn = "2018-01-19", updatedOn = "2018-01-19", testCaseDescription = "Verify Group action icon options functionality for other users except Corporate users", testCaseId = "TC_Hub_Messages_Groups_01")
	private void groupMessages() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubDirectoryPage directory_page = new TheHubDirectoryPage(driver);
			String emailId = "hubautomation@staffex.com";

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUserPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = corporateUserPage.createDefaultUser(driver, corpUser);
			/*
			 * Div User
			 */
			fc.utobj().printTestStep("Navigate To Admin > Division > Add Division");
			fc.utobj().printTestStep("Add Division");

			AdminDivisionAddDivisionPageTest addDivPage = new AdminDivisionAddDivisionPageTest();
			String divisionName = fc.utobj().generateTestData("testdiv");
			addDivPage.addDivision(driver, divisionName);

			fc.utobj().printTestStep("Navigate To Admin > Users > Add Divisional User");
			fc.utobj().printTestStep("Add Divisional User");
			AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest divisionUserPage = new AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest();
			String devUserName = fc.utobj().generateTestData("devuser");
			divisionUserPage.addDivisionalUser(driver, devUserName, corpUser.getPassword(), divisionName, emailId);

			/*
			 * Regional User
			 */
			fc.utobj().printTestStep("Navigate To Admin > Add Area/Region");
			fc.utobj().printTestStep("Add Region");

			AdminAreaRegionAddAreaRegionPageTest addRegionPage = new AdminAreaRegionAddAreaRegionPageTest();
			String regionName = fc.utobj().generateTestData("Testreg");
			addRegionPage.addAreaRegion(driver, regionName);

			fc.utobj().printTestStep("Add Regional User");
			AdminUsersManageRegionalUsersAddRegionalUserPageTest regionalUserPage = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
			String regUserName = fc.utobj().generateTestData("Testreguser");

			regionalUserPage.addRegionalUser(driver, regUserName, corpUser.getPassword(), regionName, emailId);

			/*
			 * Franchise User
			 */

			fc.utobj().printTestStep("Add Franchise Location");
			String storeType = "Test127StoreType".concat(fc.utobj().generateRandomNumber());
			String franchiseId = "Test127FranId".concat(fc.utobj().generateRandomNumber());

			AdminFranchiseLocationAddFranchiseLocationPageTest addFranchiseLocation = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String firstName = fc.utobj().generateTestData("Testfname");
			String franRegionName = fc.utobj().generateTestData("Testfreg");
			franchiseId = addFranchiseLocation.addFranchiseLocation_AllWithFName(driver, franchiseId, franRegionName,
					storeType, firstName);

			fc.utobj().printTestStep("Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest addFranchiseUser = new AdminUsersManageManageFranchiseUsersPageTest();
			String roleName = "Default Franchise Role";
			String userNameFran = fc.utobj().generateTestData("Testfranuser");
			addFranchiseUser.addFranchiseUser(driver, userNameFran, corpUser.getPassword(), franchiseId, roleName,
					emailId);

			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login by newly created Corporate User");
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());

			fc.utobj().printTestStep("Navigate To Hub > Message > Group");
			fc.utobj().printTestStep("Create Group");
			String groupName = fc.utobj().generateTestData("TestGroupName");
			createNewGroup(driver, groupName, corpUser.getuserFullName());

			fc.utobj().actionImgOption(driver, groupName, "Turn this group Public");
			fc.utobj().acceptAlertBox(driver);

			fc.home_page().logout(driver);

			/*
			 * Divisional
			 */

			fc.utobj().printTestStep("Login With Newly Created Divisional User");
			fc.loginpage().loginWithParameter(driver, devUserName, corpUser.getPassword());

			fc.utobj().printTestStep("Verify Under Action icon of Groups, only Send Message option should come");

			fc.hub().hub_common().theHubMessageSubModule(driver);
			fc.utobj().clickElement(driver, directory_page.groupsTab);
			boolean isOptionpresent = fc.utobj().optionPresentInActionMenu(driver, groupName, "Send Message");

			if (isOptionpresent == false) {
				fc.utobj().throwsException("Under Action icon of Groups, Send Message option is not coming");
			}

			boolean isModifyOptionpresent = fc.utobj().optionPresentInActionMenu(driver, groupName, "Modify");
			boolean isDeleteOptionpresent = fc.utobj().optionPresentInActionMenu(driver, groupName, "Delete");
			boolean isTurnThisGroupPublicOptionpresent = fc.utobj().optionPresentInActionMenu(driver, groupName,
					"Turn this group Private");

			if (isModifyOptionpresent) {
				fc.utobj().throwsException("Under Action icon of Groups, Modify option is coming");
			}

			if (isDeleteOptionpresent) {
				fc.utobj().throwsException("Under Action icon of Groups, Delete option is coming");
			}
			if (isTurnThisGroupPublicOptionpresent) {
				fc.utobj().throwsException("Under Action icon of Groups, Turn this group Private option is coming");
			}

			boolean isDeleteButton2 = fc.utobj().isElementPresent(driver,
					fc.utobj().getElement(driver, directory_page.deleteBottomBtn));
			if (isDeleteButton2 == false) {
				fc.utobj().throwsException("Delete Button is not coming");
			}

			/*
			 * Regional
			 */

			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login With Newly Created Regional User");
			fc.loginpage().loginWithParameter(driver, regUserName, corpUser.getPassword());

			fc.utobj().printTestStep("Verify Under Action icon of Groups, only Send Message option should come");

			fc.hub().hub_common().theHubMessageSubModule(driver);
			fc.utobj().clickElement(driver, directory_page.groupsTab);
			boolean isOptionpresent1 = fc.utobj().optionPresentInActionMenu(driver, groupName, "Send Message");

			if (isOptionpresent1 == false) {
				fc.utobj().throwsException("Under Action icon of Groups, Send Message option is not coming");
			}

			boolean isModifyOptionpresent1 = fc.utobj().optionPresentInActionMenu(driver, groupName, "Modify");
			boolean isDeleteOptionpresent1 = fc.utobj().optionPresentInActionMenu(driver, groupName, "Delete");
			boolean isTurnThisGroupPublicOptionpresent1 = fc.utobj().optionPresentInActionMenu(driver, groupName,
					"Turn this group Private");

			if (isModifyOptionpresent1) {
				fc.utobj().throwsException("Under Action icon of Groups, Modify option is coming");
			}

			if (isDeleteOptionpresent1) {
				fc.utobj().throwsException("Under Action icon of Groups, Delete option is coming");
			}
			if (isTurnThisGroupPublicOptionpresent1) {
				fc.utobj().throwsException("Under Action icon of Groups, Turn this group Private option is coming");
			}

			boolean isDeleteButton3 = fc.utobj().isElementPresent(driver,
					fc.utobj().getElement(driver, directory_page.deleteBottomBtn));
			if (isDeleteButton3 == false) {
				fc.utobj().throwsException("Delete Button is not coming");
			}

			/*
			 * Franchise
			 */
			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login With Newly Created Franchise User");
			fc.loginpage().loginWithParameter(driver, regUserName, corpUser.getPassword());

			fc.utobj().printTestStep("Verify Under Action icon of Groups, only Send Message option should come");

			fc.hub().hub_common().theHubMessageSubModule(driver);
			fc.utobj().clickElement(driver, directory_page.groupsTab);
			boolean isOptionpresent2 = fc.utobj().optionPresentInActionMenu(driver, groupName, "Send Message");

			if (isOptionpresent2 == false) {
				fc.utobj().throwsException("Under Action icon of Groups, Send Message option is not coming");
			}

			boolean isModifyOptionpresent2 = fc.utobj().optionPresentInActionMenu(driver, groupName, "Modify");
			boolean isDeleteOptionpresent2 = fc.utobj().optionPresentInActionMenu(driver, groupName, "Delete");
			boolean isTurnThisGroupPublicOptionpresent2 = fc.utobj().optionPresentInActionMenu(driver, groupName,
					"Turn this group Private");

			if (isModifyOptionpresent2) {
				fc.utobj().throwsException("Under Action icon of Groups, Modify option is coming");
			}

			if (isDeleteOptionpresent2) {
				fc.utobj().throwsException("Under Action icon of Groups, Delete option is coming");
			}
			if (isTurnThisGroupPublicOptionpresent2) {
				fc.utobj().throwsException("Under Action icon of Groups, Turn this group Private option is coming");
			}

			boolean isDeleteButton4 = fc.utobj().isElementPresent(driver,
					fc.utobj().getElement(driver, directory_page.deleteBottomBtn));
			if (isDeleteButton4 == false) {
				fc.utobj().throwsException("Delete Button is not coming");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/*
	 * TC-10022
	 */

	@Test(groups = { "thehub","hub_messages"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Sent Html Type Messsages To Corporate User at The Hub > Messages > Compose", testCaseId = "TC_Compose_Messages_Html_Type_01")
	public void composeMessages() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubMessagesPage pobj = new TheHubMessagesPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users Page");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUserPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "hubautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = corporateUserPage.createDefaultUser(driver, corpUser);

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With Newly Created Coporate User");
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());

			fc.utobj().printTestStep("Navigate To The Hub > Messages Page");
			fc.hub().hub_common().theHubMessageSubModule(driver);
			/*
			 * TC-9993
			 */

			fc.utobj().printTestStep("To Verify the default view of Current Folder and View under messages sub module");
			String selectedValue = fc.utobj().getSelectedDropDownValue(driver, pobj.currentFolderSelect);
			if (selectedValue.equalsIgnoreCase("Inbox") == false) {
				fc.utobj().throwsException("Not able to verify Default selected value in Current Folder");
			}

			String selectedValueInView = fc.utobj().getSelectedDropDownValue(driver, pobj.viewSelect);
			if (selectedValueInView.equalsIgnoreCase("All") == false) {
				fc.utobj().throwsException("Not able to verify Default selected value in View dropdown");
			}

			String userMessages = fc.utobj().generateTestData("Testusermessages");
			fc.utobj().printTestStep("Configure Auto Reply for Out of Office");
			fc.utobj().clickElement(driver, pobj.autoReplyForOutOfOffice);
			CommonUI cui = new CommonUI(driver);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			if (!fc.utobj().isSelected(driver, pobj.outOfOfficeStatusOut)) {
				fc.utobj().clickElement(driver, pobj.outOfOfficeStatusOut);
			}
			fc.utobj().sendKeys(driver, pobj.userMessage, userMessages);
			fc.utobj().clickElement(driver, pobj.submitAutoReplyPage);

			fc.utobj().clickElement(driver, cui.Close_Input_ByValue);
			fc.utobj().switchFrameToDefault(driver);

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With Adm User");
			fc.loginpage().loginWithParameter(driver, config.get("userName"), config.get("password"));

			fc.utobj().printTestStep("Navigate To The Hub > Messages Page");
			fc.utobj().printTestStep("Compose Messages");

			Messages hub_common = new Messages();
			String subject = fc.utobj().generateTestData("TestSubject");
			hub_common.setSubject(subject);
			String messages = fc.utobj().generateTestData("TestMessages");
			hub_common.setMessages(messages);
			String messagesType = "Html";
			hub_common.setMessagesType(messagesType);
			String recipientsUser = "Corporate Users";
			hub_common.setRecipientsUser(recipientsUser);
			hub_common.setToEmail(corpUser.getuserFullName());
			hub_common.setContactType("Internal");
			new MessagesTest().composeMessages(driver, hub_common);

			// TC-9995

			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData("Testfolder");
			addFolder(driver, folderName, config);

			fc.utobj().printTestStep("Verify Sent Message in sent box");
			fc.utobj().clickElement(driver, pobj.inboxTab);
			fc.utobj().selectDropDown(driver, pobj.currentFolderSelect, "Sent");

			fc.utobj().selectDropDown(driver, pobj.viewSelect, "All");

			fc.utobj().showAll(driver);
			fc.utobj().isTextDisplayed(driver, subject, "was not able to verify Subject in sent box");

			fc.utobj().printTestStep("Verify The Auto Reply In Inbox Folder");
			fc.utobj().selectDropDown(driver, pobj.currentFolderSelect, "Inbox");

			fc.utobj().clickLink(driver, "Out of Office");
			boolean isuserMessages = fc.utobj().assertPageSource(driver, userMessages);

			if (isuserMessages == false) {
				fc.utobj().throwsException("Auto Reply for Out of Office is not working");
			}
			/*
			 * TC-9992
			 */

			fc.utobj().printTestStep("search messages on Basis of subject by search Messages with options");
			fc.utobj().clickElement(driver, pobj.inboxTab);
			fc.utobj().selectDropDown(driver, pobj.currentFolderSelect, "Sent");
			fc.utobj().selectDropDownByValue(driver, pobj.searchMessageswithSelect, "subject");
			fc.utobj().sendKeys(driver, pobj.searchMessagesWithTextBx, subject);
			fc.utobj().clickElement(driver, new CommonUI(driver).ok_Input_ByValue);

			fc.utobj().isTextDisplayed(driver, subject, "Not able to search messages by subject");

			fc.utobj().printTestStep("search messages on Basis of Text by search Messages with options");
			fc.utobj().selectDropDownByValue(driver, pobj.searchMessageswithSelect, "Text");
			fc.utobj().sendKeys(driver, pobj.searchMessagesWithTextBx, messages);
			fc.utobj().clickElement(driver, new CommonUI(driver).ok_Input_ByValue);

			fc.utobj().isTextDisplayed(driver, subject, "Not able to search messages by Text");

			fc.utobj()
					.printTestStep("search messages on Basis of Both subject and text by search Messages with options");
			fc.utobj().selectDropDownByValue(driver, pobj.searchMessageswithSelect, "both");
			fc.utobj().sendKeys(driver, pobj.searchMessagesWithTextBx, subject);
			fc.utobj().clickElement(driver, new CommonUI(driver).ok_Input_ByValue);

			fc.utobj().isTextDisplayed(driver, subject,
					"Not able to search messages by Selecting both and enter subject");

			fc.utobj().selectDropDownByValue(driver, pobj.searchMessageswithSelect, "both");
			fc.utobj().sendKeys(driver, pobj.searchMessagesWithTextBx, messages);
			fc.utobj().clickElement(driver, new CommonUI(driver).ok_Input_ByValue);

			fc.utobj().isTextDisplayed(driver, subject, "Not able to search messages by Selecting both and enter Text");

			/*
			 * TC-9995
			 */

			fc.utobj().printTestStep("Move Messages To Folder");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[contains(text(), '" + subject + "')]/ancestor::tr/td/input[@name='messageno']"));
			fc.utobj().selectDropDown(driver, pobj.moveToFolder, folderName);
			fc.utobj().clickElement(driver, pobj.OKBtn);

			fc.utobj().printTestStep("Verify The Moved Messages in sent folder");

			boolean isMessagesPresent = fc.utobj().assertNotInPageSource(driver, subject);
			if (isMessagesPresent == true) {
				fc.utobj()
						.throwsException("Messages is showing in sent folder even after moving it into another folder");
			}

			fc.utobj().printTestStep("Verify The Moved Messages In folder");
			fc.utobj().selectDropDown(driver, pobj.currentFolderSelect, folderName);

			boolean isMessagesPresentInfolder = fc.utobj().assertNotInPageSource(driver, subject);
			if (isMessagesPresentInfolder == false) {
				fc.utobj().throwsException(
						"Messages is not showing in moved folder even after moving it from sent folder");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/*
	 * TC-10045
	 */

	@Test(groups = {"thehub","hub_messages"})
	@TestCase(createdOn = "2018-02-13", updatedOn = "2018-02-13", testCaseDescription = "Verify Groups Sub Tab functionality under Message Tab for logged users based on privileged based", testCaseId = "TC_Hub_Messages_Alerts_Directory_Groups_01")
	private void verifyCreatedGroupAlertMessagesDirectory() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			String emailId = "hubautomation@staffex.com";
			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUserPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = corporateUserPage.createDefaultUser(driver, corpUser);
			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login By newly Created Corporate User");
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());

			fc.utobj().printTestStep("Navigate To Hub > Directory > Groups");
			fc.utobj().printTestStep("Create Group");
			TheHubDirectoryPageTest directory_test = new TheHubDirectoryPageTest();

			String directoryGroupName = fc.utobj().generateTestData("Testdirecgroup");
			directory_test.createNewGroup(driver, directoryGroupName, corpUser.getuserFullName(), config);

			fc.utobj().printTestStep("Navigate To Hub > Alert > Groups");
			fc.utobj().printTestStep("Create Group");
			String alertGroupName = fc.utobj().generateTestData("Testalertgroup");
			new TheHubAlertsPageTest().createNewGroupAtAlerts(driver, alertGroupName, corpUser.getuserFullName(),
					config);

			fc.utobj().printTestStep("Navigate To Hub > Messages > Groups");
			fc.utobj().printTestStep("Create Group");
			String messageGroupName = fc.utobj().generateTestData("Testmsggroup");
			createNewGroup(driver, messageGroupName, corpUser.getuserFullName());
			fc.utobj().showAll(driver);

			boolean isMessageGroups = fc.utobj().assertPageSource(driver, messageGroupName);
			boolean isDirectoryGroups = fc.utobj().assertPageSource(driver, directoryGroupName);
			boolean isAlertGroups = fc.utobj().assertPageSource(driver, alertGroupName);

			if (isMessageGroups == false) {
				fc.utobj().throwsException("Message's groups is not visible at Message's group");
			}

			if (isDirectoryGroups == false) {
				fc.utobj().throwsException("Directory's groups is not visible at Message's group");
			}

			if (isAlertGroups == false) {
				fc.utobj().throwsException("Alert's groups is not visible at Message's group");
			}

			boolean isMessageMember = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(), '" + messageGroupName + "')]/ancestor::tr/td[contains(text(),'1')]");
			boolean isDirectoryMember = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(), '" + directoryGroupName + "')]/ancestor::tr/td[contains(text(),'1')]");
			boolean isAlertMember = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(), '" + alertGroupName + "')]/ancestor::tr/td[contains(text(),'1')]");

			if (isMessageMember == false) {
				fc.utobj().throwsException("No of member is not present at Message's groups page of Message's group");
			}

			if (isDirectoryMember == false) {
				fc.utobj().throwsException("No of member is not present at Message's groups page of Directory's group");
			}

			if (isAlertMember == false) {
				fc.utobj().throwsException("No of member is not present at Message's groups page of Alert's group");
			}

			boolean isMessageCreatedBy = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(), '"
					+ messageGroupName + "')]/ancestor::tr/td[contains(text(),'" + corpUser.getuserFullName() + "')]");
			boolean isDirectoryCreatedBy = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(), '" + directoryGroupName + "')]/ancestor::tr/td[contains(text(),'"
							+ corpUser.getuserFullName() + "')]");
			boolean isAlertCreatedBy = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(), '"
					+ alertGroupName + "')]/ancestor::tr/td[contains(text(),'" + corpUser.getuserFullName() + "')]");

			if (isMessageCreatedBy == false) {
				fc.utobj().throwsException("Created By is not present at Message's groups page of Message group");
			}

			if (isDirectoryCreatedBy == false) {
				fc.utobj().throwsException("Created By is not present at Message's groups page of Directory group");
			}

			if (isAlertCreatedBy == false) {
				fc.utobj().throwsException("Created By is not present at Message's groups page of Alert group");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/*
	 * TC-10017
	 */

	@Test(groups = { "thehub" ,"hub_messages"})
	@TestCase(createdOn = "2018-02-13", updatedOn = "2018-02-13", testCaseDescription = "To Verify messages > Compose Message and save as draft feature", testCaseId = "TC_Compose_Messages_Draft_01")
	public void saveAsDraft() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubMessagesPage pobj = new TheHubMessagesPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users Page");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUserPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "hubautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUserPage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To The Hub > Messages Page");
			fc.utobj().printTestStep("Save Messages As a draft");

			String subject = fc.utobj().generateTestData("TestSubject");
			String recipientsUser = "Corporate Users";

			fc.hub().hub_common().theHubMessageSubModule(driver);
			fc.utobj().clickElement(driver, pobj.composeLink);

			fc.utobj().clickElement(driver, pobj.addressBookTo);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().setToDefault(driver, pobj.selectUsers);
			fc.utobj().selectValFromMultiSelect(driver, pobj.selectUsers, recipientsUser);
			fc.utobj().clickElement(driver, pobj.submitOk);
			fc.utobj().sendKeys(driver, pobj.searchTextBx, corpUser.getuserFullName());
			fc.utobj().clickElement(driver, pobj.searchImgBtn);

			fc.utobj().sleep();
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[contains(text () , '" + corpUser.getuserFullName() + "')]/ancestor::tr/td/input"));
			fc.utobj().clickElement(driver, pobj.addUserBtn);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().sendKeys(driver, pobj.subject, subject);

			fc.utobj().clickElement(driver, pobj.saveAsDraft);
			fc.utobj().showAll(driver);

			fc.utobj().printTestStep("Verify Message Saved as a draft");
			boolean isSubjectPresent = fc.utobj().assertPageSource(driver, subject);

			if (isSubjectPresent == false) {
				fc.utobj().throwsException("Message is not saved as a draft");
			}

			fc.utobj().clickLink(driver, subject);
			String getSubject = fc.utobj().getAttributeValue(pobj.subject, "value");
			if (!getSubject.equalsIgnoreCase(subject)) {
				fc.utobj().throwsException("Not able to verify subject at message compose page");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/*
	 * TC-9990
	 */

	@Test(groups = { "thehub","hub_messages" })
	@TestCase(createdOn = "2018-01-24", updatedOn = "2018-01-24", testCaseDescription = "To Verify the Messages sub Tabs through corporate users based on privilege based through admin roles", testCaseId = "TC_Hub_Messages_SubTabs_01")
	private void verifyMessagesSubTabs() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubMessagesPage pobj = new TheHubMessagesPage(driver);

			String emailId = "hubautomation@staffex.com";
			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUserPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = corporateUserPage.createDefaultUser(driver, corpUser);

			Messages hub_common = new Messages();
			String subject = fc.utobj().generateTestData("TestSubject");
			hub_common.setSubject(subject);
			String messages = fc.utobj().generateTestData("TestMessages");
			hub_common.setMessages(messages);
			String messagesType = "Text";
			hub_common.setMessagesType(messagesType);
			String recipientsUser = "Corporate Users";
			hub_common.setRecipientsUser(recipientsUser);
			hub_common.setToEmail(corpUser.getuserFullName());
			hub_common.setContactType("Internal");

			fc.utobj().printTestStep("Compose Messages");
			new MessagesTest().composeMessages(driver, hub_common);

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With Newly Created Corporate User");
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());

			fc.utobj().printTestStep("Navigate To Hub > Messages");
			fc.hub().hub_common().theHubMessageSubModule(driver);

			fc.utobj().printTestStep("Verify The Sub tabs of messages Sub module");
			List<WebElement> subTabs = driver.findElements(By.xpath(".//table[@class='subTabs']//td//span"));

			boolean b3[] = new boolean[10];

			for (WebElement webElement : subTabs) {

				String options = fc.utobj().getText(driver, webElement);

				if (options.equalsIgnoreCase(fc.utobj().translateString("Inbox"))) {
					b3[0] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Compose"))) {
					b3[1] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Folders"))) {
					b3[2] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Address Book"))) {
					b3[3] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Groups"))) {
					b3[4] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Audit Report"))) {
					b3[5] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Archived"))) {
					b3[6] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Search"))) {
					b3[7] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("Export"))) {
					b3[8] = true;
				} else if (options.equalsIgnoreCase(fc.utobj().translateString("My Signatures"))) {
					b3[9] = true;
				}
			}

			if (b3[0] == false) {
				fc.utobj().throwsException("Inbox subtabs is not present under messages submodule");
			}
			if (b3[1] == false) {
				fc.utobj().throwsException("Compose subtabs is not present under messages submodule");
			}
			if (b3[2] == false) {
				fc.utobj().throwsException("Folders subtabs is not present under messages submodule");
			}
			if (b3[3] == false) {
				fc.utobj().throwsException("Address Book subtabs is not present under messages submodule");
			}
			if (b3[4] == false) {
				fc.utobj().throwsException("Groups subtabs is not present under messages submodule");
			}
			if (b3[5] == false) {
				fc.utobj().throwsException("Audit Report subtabs is not present under messages submodule");
			}
			if (b3[6] == false) {
				fc.utobj().throwsException("Archived subtabs is not present under messages submodule");
			}
			if (b3[7] == false) {
				fc.utobj().throwsException("Search subtabs is not present under messages submodule");
			}
			if (b3[8] == false) {
				fc.utobj().throwsException("Export subtabs is not present under messages submodule");
			}
			if (b3[9] == false) {
				fc.utobj().throwsException("My Signatures subtabs is not present under messages submodule");
			}

			/*
			 * TC-9995
			 */
			fc.utobj().printTestStep("search messages on Basis of subject by search Messages with options");
			fc.utobj().selectDropDownByValue(driver, pobj.searchMessageswithSelect, "subject");
			fc.utobj().sendKeys(driver, pobj.searchMessagesWithTextBx, subject);
			fc.utobj().clickElement(driver, new CommonUI(driver).ok_Input_ByValue);

			fc.utobj().printTestStep("Mark Message As Read");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[contains(text(), '" + subject + "')]/ancestor::tr/td/input[@name='messageno']"));
			fc.utobj().selectDropDownByValue(driver, pobj.markSelect, "1");

			boolean isReadImgPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text(),'" + subject + "')]/ancestor::tr/td/img[contains(@src , 'read_mail_t.gif')]");
			if (isReadImgPresent == false) {
				fc.utobj().throwsException("Not able to verify Mark Message As Read");
			}

			fc.utobj().printTestStep("Mark Message As Unread");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[contains(text(), '" + subject + "')]/ancestor::tr/td/input[@name='messageno']"));
			fc.utobj().selectDropDownByValue(driver, pobj.markSelect, "2");

			boolean isUnreadImgPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'"
					+ subject + "')]/ancestor::tr/td/img[contains(@src , 'unread_mail_t.gif')]");
			if (isUnreadImgPresent == false) {
				fc.utobj().throwsException("Not able to verify Mark Message As Unread");
			}

			fc.utobj().printTestStep("Delete Messages By Delete Button");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[contains(text(), '" + subject + "')]/ancestor::tr/td/input[@name='messageno']"));
			fc.utobj().clickElement(driver, pobj.deleteButtonInbox);
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Deleted Messages");

			fc.utobj().printTestStep("search messages on Basis of subject by search Messages with options");
			fc.utobj().selectDropDownByValue(driver, pobj.searchMessageswithSelect, "subject");
			fc.utobj().sendKeys(driver, pobj.searchMessagesWithTextBx, subject);
			fc.utobj().clickElement(driver, new CommonUI(driver).ok_Input_ByValue);

			boolean isSubjectPresent = fc.utobj().assertPageSource(driver, subject);
			if (isSubjectPresent == true) {
				fc.utobj().throwsException("Messages is not deleting");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/*
	 * TC-10020
	 */

	@Test(groups = { "thehubTest", "thehub_mail" ,"hub_messages"})
	@TestCase(createdOn = "2018-02-20", updatedOn = "2017-02-20", testCaseDescription = "", testCaseId = "TC_Compose_Messages_External_Contact_01")
	public void composeMessagesSendExternal() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			driver = fc.loginpage().login(driver);
			MessagesTest messages_test = new MessagesTest();

			fc.utobj().printTestStep("Navigate To Hub > Messages > Address Book > Add New Contact");
			String firstName = fc.utobj().generateTestData("Testfname");
			String lastName = fc.utobj().generateTestData("Testlname");
			String emailId = "hubautomation@staffex.com";

			Messages hub_common = new Messages();
			hub_common.setFirstName(firstName);
			hub_common.setLastName(lastName);
			hub_common.setEmail(emailId);
			new MessagesTest().addNewContact(driver, hub_common);

			String subject = fc.utobj().generateTestData("TestSubject");
			hub_common.setSubject(subject);
			String messages = fc.utobj().generateTestData("TestMessages");
			hub_common.setMessages(messages);
			String messagesType = "Html";
			hub_common.setMessagesType(messagesType);
			hub_common.setContactType("External");

			fc.utobj().printTestStep("Navigate To > The Hub > Messages  > Compose");
			messages_test.composeMessages(driver, hub_common);

			fc.utobj().printTestStep("Verify The sent messages in mail box");

			Map<String, String> mailInfo = fc.utobj().readMailBox(hub_common.getSubject(), hub_common.getMessages(),
					hub_common.getEmail(), "sdg@1a@Hfs");
			if (mailInfo.get("mailBody").contains(hub_common.getMessages()) == false) {
				fc.utobj().throwsException("Not able to verify messages in mail");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/*
	 * TC-9997
	 * 
	 */

	@Test(groups = { "thehub","hub_messages","hub_download"})
	@TestCase(createdOn = "2018-02-23", updatedOn = "2018-02-23", testCaseDescription = "To Verify the Message Details page", testCaseId = "TC_Verify_Messages_Details_01")
	public void verifyMessagesDetailsAtDetailsPage() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubMessagesPage pobj = new TheHubMessagesPage(driver);
			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users Page");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUserPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "hubautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser.setState("Alabama");
			corporateUserPage.createDefaultUser(driver, corpUser);

			fc.utobj().printTestStep("Navigate To The Hub > Messages Page");
			fc.utobj().printTestStep("Compose Messages");

			Messages hub_common = new Messages();
			String subject = fc.utobj().generateTestData("TestSubject");
			hub_common.setSubject(subject);
			String messages = fc.utobj().generateTestData("TestMessages");
			hub_common.setMessages(messages);
			String messagesType = "Html";
			hub_common.setMessagesType(messagesType);
			String recipientsUser = "Corporate Users";
			hub_common.setRecipientsUser(recipientsUser);
			hub_common.setToEmail(corpUser.getuserFullName());
			hub_common.setContactType("Internal");
			hub_common.setIsAttachment("Yes");
			String fileName = dataSet.get("fileName");
			hub_common.setFile(fileName);
			new MessagesTest().composeMessages(driver, hub_common);

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login By Newly Created Corporate User");
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());

			fc.utobj().printTestStep("Navigate To The Hub > Messages Page");
			fc.hub().hub_common().theHubMessageSubModule(driver);

			fc.utobj().printTestStep("Verify Messages In Inbox");
			fc.utobj().clickElement(driver, pobj.inboxTab);
			fc.utobj().selectDropDown(driver, pobj.currentFolderSelect, "Inbox");
			fc.utobj().selectDropDown(driver, pobj.viewSelect, "All");

			fc.utobj().clickLink(driver, hub_common.getSubject());

			fc.utobj().printTestStep("Verify To Label");
			boolean isToPresent = fc.utobj().assertPageSource(driver, corpUser.getuserFullName());

			boolean isAttachementPresent = fc.utobj().assertPageSource(driver,
					fileName.substring(0, fileName.lastIndexOf("_") + 3));

			String fileNameInBuild = fc.utobj().getText(driver, fc.utobj().getElementByXpath(driver,
					".//*[contains(text(),'" + fileName.substring(0, fileName.lastIndexOf("_") + 3) + "')]"));

			boolean isMessagesPresent = fc.utobj().assertPageSource(driver, hub_common.getMessages());

			fc.utobj().clickElement(driver, pobj.saveFileBtn);
			fc.utobj().printTestStep("Verify The Downloaded File");

			boolean isFileFound = false;
			File folder = new File(FranconnectUtil.config.get("downloadFilepath"));
			isFileFound = fc.utobj().isFileFound(folder, fileNameInBuild);

			
			 if (!isFileFound) { fc.utobj().throwsException(
			 "Not able to download attached file"); }
			 

			if (!isMessagesPresent) {
				fc.utobj().throwsException("Not able to verify messages text");
			}

			if (!isToPresent) {
				fc.utobj().throwsException("Not able to verify To");
			}

			if (!isAttachementPresent) {
				fc.utobj().throwsException("Attachment is not present in the build");
			}

			fc.utobj().printTestStep("Verify The Reply Button Function");

			fc.utobj().clickElement(driver, pobj.replyBtn);

			fc.utobj().printTestStep("Enter messages body");
			String relpySubject = fc.utobj().generateTestData("relpySubject");
			fc.utobj().sendKeys(driver, pobj.subject, relpySubject);
			String replyMsg = fc.utobj().generateTestData("Testrplymsg");
			
			//Trial
			fc.utobj().printTestStep("Close the TinyMCE");
			closeTinyMCETrial(driver);
			
			fc.utobj().switchFrameById(driver, "mailText_ifr");
			Actions actions = new Actions(driver);
			actions.moveToElement(fc.utobj().getElement(driver, pobj.editorTextArea));
			actions.click();
			actions.sendKeys(replyMsg);
			actions.build().perform();

			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, pobj.sendBtn);
			fc.utobj().clickElement(driver, pobj.OkBtn);

			fc.utobj().clickLink(driver, hub_common.getSubject());

			fc.utobj().printTestStep("Verify The Reply All Button Function");
			fc.utobj().clickElement(driver, pobj.replyAllBtn);
			fc.utobj().printTestStep("Enter messages body");
			String relpyAllSubject = fc.utobj().generateTestData("relpyAllSubject");
			fc.utobj().sendKeys(driver, pobj.subject, relpyAllSubject);
			String replyAllMsg = fc.utobj().generateTestData("TestrplyAllmsg");
			
			//Trial
			fc.utobj().printTestStep("Close the TinyMCE");
			closeTinyMCETrial(driver);
			
			fc.utobj().switchFrameById(driver, "mailText_ifr");
			Actions actions1 = new Actions(driver);
			actions1.moveToElement(fc.utobj().getElement(driver, pobj.editorTextArea));
			actions1.click();
			actions1.sendKeys(replyAllMsg);
			actions1.build().perform();

			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, pobj.sendBtn);
			fc.utobj().clickElement(driver, pobj.OkBtn);

			fc.utobj().clickLink(driver, hub_common.getSubject());

			fc.utobj().printTestStep("Verify The Forward Button Function");
			fc.utobj().clickElement(driver, pobj.forwardBtn);
			fc.utobj().printTestStep("Enter messages body");
			String forwardSubject = fc.utobj().generateTestData("forwardSubject");
			hub_common.setContactType("Internal");
			hub_common.setRecipientsUser("Corporate Users");
			hub_common.setToEmail(corpUser.getuserFullName());
			new MessagesTest().selectRecipientsAddressBook(driver, hub_common);
			fc.utobj().sendKeys(driver, pobj.subject, forwardSubject);
			String forwardMsg = fc.utobj().generateTestData("forwardmsg");
			
			//Trial
			fc.utobj().printTestStep("Close the TinyMCE");
			closeTinyMCETrial(driver);
			
			fc.utobj().switchFrameById(driver, "mailText_ifr");
			Actions actions12 = new Actions(driver);
			actions12.moveToElement(fc.utobj().getElement(driver, pobj.editorTextArea));
			actions12.click();
			actions12.sendKeys(forwardMsg);
			actions12.build().perform();

			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, pobj.sendBtn);
			fc.utobj().clickElement(driver, pobj.OkBtn);

			fc.utobj().clickLink(driver, hub_common.getSubject());

			fc.utobj().printTestStep("Verify The Add Sender Button Function");
			fc.utobj().clickElement(driver, pobj.addSenderBtn);
			fc.utobj().clickElement(driver, pobj.addConatactBtn);
			boolean isAddedContactPresent = fc.utobj().assertPageSource(driver, corpUser.getuserFullName());
			if (isAddedContactPresent == false) {
				fc.utobj().throwsException("Not able to verify added Sender");
			}

			fc.utobj().printTestStep("Verify Close Button Functionality");
			fc.utobj().clickElement(driver, pobj.inboxTab);
			fc.utobj().clickLink(driver, hub_common.getSubject());

			fc.utobj().clickElement(driver, pobj.commonUi(driver).Close_Input_ByValue);
			boolean isSubjectPresent = fc.utobj().assertPageSource(driver, hub_common.getSubject());

			if (isSubjectPresent == false) {
				fc.utobj().throwsException("close button is not working properly");
			}

			boolean isForwardMessagesPresent = fc.utobj().assertPageSource(driver, forwardSubject);
			if (isForwardMessagesPresent == false) {
				fc.utobj().throwsException("Forwarded messages is not present in inbox");
			}

			fc.utobj().printTestStep("Verify Delete Button Functionaliity");
			fc.utobj().clickLink(driver, hub_common.getSubject());
			fc.utobj().clickElement(driver, pobj.deleteBtn);
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().selectDropDownByValue(driver, pobj.searchMessageswithSelect, "subject");
			fc.utobj().sendKeys(driver, pobj.searchMessagesWithTextBx, hub_common.getSubject());
			fc.utobj().clickElement(driver, new CommonUI(driver).ok_Input_ByValue);

			boolean isdeletedSubjectPresent = fc.utobj().assertPageSource(driver, hub_common.getSubject());
			if (isdeletedSubjectPresent == true) {
				fc.utobj().throwsException("Not able to delete messages");
			}
			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With Adm User");
			fc.loginpage().loginWithParameter(driver, FranconnectUtil.config.get("userName"),
					FranconnectUtil.config.get("password"));

			fc.utobj().printTestStep("Navigate To The Hub > Messages Page");
			fc.hub().hub_common().theHubMessageSubModule(driver);

			fc.utobj().printTestStep("Verify Messages In Inbox");
			fc.utobj().clickElement(driver, pobj.inboxTab);
			fc.utobj().selectDropDown(driver, pobj.currentFolderSelect, "Inbox");
			fc.utobj().selectDropDown(driver, pobj.viewSelect, "All");
			fc.utobj().showAll(driver);

			boolean isReplyMessagesPresent = fc.utobj().assertPageSource(driver, relpySubject);
			boolean isReplyAllMessagesPresent = fc.utobj().assertPageSource(driver, relpyAllSubject);

			if (isReplyMessagesPresent == false) {
				fc.utobj().throwsException("Reply messages is not present in inbox");
			}

			if (isReplyAllMessagesPresent == false) {
				fc.utobj().throwsException("Reply All messages is not present in inbox");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/*
	 * TC-10016
	 */
	@Test(groups = { "thehub","hub_messages"})
	@TestCase(createdOn = "2018-02-27", updatedOn = "2018-02-27", testCaseDescription = "Verify Verify messages > Compose > Address book for duplicate or invalid users", testCaseId = "TC_Hub_Messages_Duplicate_01")
	private void sendMessagesToGroup() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubMessagesPage pobj = new TheHubMessagesPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUserPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "hubautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUserPage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Add Messages Group");
			String groupName = fc.utobj().generateTestData("Testgroups");
			createNewGroup(driver, groupName, corpUser.getuserFullName());

			fc.utobj().printTestStep("Navigate To The Hub > Messages Page");
			fc.hub().hub_common().theHubMessageSubModule(driver);
			fc.utobj().clickElement(driver, pobj.composeLink);

			fc.utobj().printTestStep("Send Messgaes To Corporate User And Group having same user");
			Messages hub_common = new Messages();
			hub_common.setAddressType("To");
			hub_common.setContactType("Internal");
			hub_common.setRecipientsUser("Corporate Users");
			hub_common.setToEmail(corpUser.getuserFullName());
			new MessagesTest().selectRecipientsAddressBook(driver, hub_common);

			hub_common.setRecipientsUser("Groups");
			hub_common.setToEmail(groupName);
			new MessagesTest().selectRecipientsAddressBook(driver, hub_common);

			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subject, subject);

			if (!fc.utobj().isSelected(driver, pobj.texthtmlOption.get(0))) {
				fc.utobj().clickElement(driver, pobj.texthtmlOption.get(0));
			}
			String messages = fc.utobj().generateTestData(dataSet.get("messages"));
			fc.utobj().sendKeys(driver, pobj.messages, messages);
			fc.utobj().clickElement(driver, pobj.sendBtn);
			fc.utobj().clickElement(driver, pobj.OkBtn);

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With Corporate User");
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());

			fc.utobj().printTestStep("Navigate To The Hub > Messages Page");
			fc.hub().hub_common().theHubMessageSubModule(driver);
			fc.utobj().clickElement(driver, pobj.inboxTab);
			fc.utobj().selectDropDown(driver, pobj.currentFolderSelect, "Inbox");
			fc.utobj().showAll(driver);

			List<WebElement> listElement = driver.findElements(By.linkText(subject));

			if (listElement.size() != 1) {
				fc.utobj().throwsException("Not able to send messages to user and group having same user");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/*
	 * TC-10009
	 */

	@Test(groups = { "thehub", "thehub_mail","hub_messages"})
	@TestCase(createdOn = "2018-02-26", updatedOn = "2018-02-26", testCaseDescription = "To Verify messages > Compose > Send Message to New Level Users", testCaseId = "TC_Hub_Compose_Messages_New_User_Level_01")
	public void composeMessagesNewUserLevel() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubMessagesPage pobj = new TheHubMessagesPage(driver);

			fc.utobj().printTestStep("Admin  > Hidden Links >  Configure New Hierarchy Level");
			fc.utobj().printTestStep("Add A New User Level");

			String configurationLabel = fc.utobj().generateTestData("Testnulevel");
			String configurationAbbreviation = fc.utobj().getRandomChar().concat(fc.utobj().getRandomChar());
			configurationAbbreviation = configurationAbbreviation.toUpperCase();
			new AdminHiddenLinksConfigureNewHierarchyLevelPageTest().configureAdditionalUserLevel(driver,
					configurationLabel, configurationAbbreviation);

			String divisionName = fc.utobj().generateTestData("Testdiv");
			AdminDivisionAddDivisionPageTest p1 = new AdminDivisionAddDivisionPageTest();
			p1.addDivisionName(driver, divisionName);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			String regionName = "Test127Region".concat(fc.utobj().generateRandomNumber());
			String storeType = "Test127StoreType".concat(fc.utobj().generateRandomNumber());
			String franchiseId = "Test127FranId".concat(fc.utobj().generateRandomNumber());

			AdminFranchiseLocationAddFranchiseLocationPageTest addFranchiseLocation = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = addFranchiseLocation.addFranchiseLocationWithDivision(driver, config, franchiseId, regionName,
					storeType, divisionName);

			fc.utobj().printTestStep("Add A user into newly added user level");
			fc.adminpage().adminPage(driver);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='Users']//a[@qat_adminlink='" + configurationLabel + "']"));
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//input[@onclick='subDivFormJS();']"));
			AdminUsersManageDivisionalUsersAddDivisionalUsersPage divisional_user = new AdminUsersManageDivisionalUsersAddDivisionalUsersPage(
					driver);

			String userName = fc.utobj().generateTestData("Testnuuser");
			String password = "t0n1ght@123";

			String emailId = "hubautomation@staffex.com";

			fc.utobj().sendKeys(driver, divisional_user.userName, userName);
			fc.utobj().sendKeys(driver, divisional_user.password, password);
			fc.utobj().sendKeys(driver, divisional_user.confirmPassword, password);
			fc.utobj().selectValFromMultiSelect(driver, divisional_user.roleMultiSelectBtn, "Default Division Role");
			fc.utobj().selectDropDown(driver, divisional_user.timeZone, "GMT -05:00 US/Canada/Eastern");
			fc.utobj().selectValFromMultiSelect(driver, divisional_user.multiselectDivision, divisionName);
			fc.utobj().selectValFromMultiSelect(driver, divisional_user.selectFranchiseId, franchiseId);

			fc.utobj().sendKeys(driver, divisional_user.firstName, userName);
			fc.utobj().sendKeys(driver, divisional_user.lastName, userName);
			fc.utobj().sendKeys(driver, divisional_user.city, "11");
			fc.utobj().selectDropDown(driver, divisional_user.country, "USA");
			fc.utobj().selectDropDown(driver, divisional_user.state, "Alabama");
			fc.utobj().sendKeys(driver, divisional_user.phone1, "1234567890");
			fc.utobj().sendKeys(driver, divisional_user.phoneExt1, "2");
			fc.utobj().sendKeys(driver, divisional_user.email, emailId);
			fc.utobj().clickElement(driver, divisional_user.submit);
			String userFullName = userName + " " + userName;

			fc.utobj().printTestStep("Navigate To The Hub > Messages Page");
			fc.utobj().printTestStep("Compose Messages");

			Messages hub_common = new Messages();
			String subject = fc.utobj().generateTestData("TestSubject");
			hub_common.setSubject(subject);
			String messages = fc.utobj().generateTestData("TestMessages");
			hub_common.setMessages(messages);
			String recipientsUser = "Divisional Users";
			hub_common.setRecipientsUser(recipientsUser);
			hub_common.setToEmail(userFullName);
			hub_common.setContactType("Internal");

			fc.hub().hub_common().theHubMessageSubModule(driver);
			fc.utobj().clickElement(driver, pobj.composeLink);
			new MessagesTest().selectRecipientsAddressBook(driver, hub_common);
			fc.utobj().sendKeys(driver, pobj.subject, hub_common.getSubject());

			if (!fc.utobj().isSelected(driver, pobj.texthtmlOption.get(1))) {
				fc.utobj().clickElement(driver, pobj.texthtmlOption.get(1));
			}

			fc.utobj().clickElement(driver, pobj.keywordsbutton);
			fc.utobj().selectDropDown(driver, pobj.keyWordSelect, "User State");
			String keyword = fc.utobj().getAttributeValue(pobj.keyWordSelect, "value");
			
			//Trial
			fc.utobj().printTestStep("Close the TinyMCE");
			closeTinyMCETrial(driver);
			
			fc.utobj().switchFrameById(driver, "mailText_ifr");
			Actions actions = new Actions(driver);
			actions.moveToElement(fc.utobj().getElement(driver, pobj.editorTextArea));
			actions.click();
			actions.sendKeys(hub_common.getMessages());
			actions.sendKeys("\n" + keyword);
			actions.build().perform();
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, pobj.sendBtn);
			fc.utobj().clickElement(driver, pobj.OkBtn);

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With Newly Created New User Level User");
			fc.loginpage().loginWithParameter(driver, userName, password);

			fc.utobj().printTestStep("Navigate To Hub > Messages Page");
			fc.hub().hub_common().theHubMessageSubModule(driver);

			fc.utobj().printTestStep("Verify Messages Send In Inbox Tab");
			fc.utobj().clickElement(driver, pobj.inboxTab);
			fc.utobj().selectDropDown(driver, pobj.currentFolderSelect, "Inbox");
			fc.utobj().showAll(driver);

			boolean isMessagesUser = fc.utobj().assertPageSource(driver, subject);
			if (isMessagesUser == false) {
				fc.utobj().throwsException("Message is not present");
			}

			WebElement element1 = fc.utobj().getElementByLinkText(driver, subject);
			fc.utobj().clickElement(driver, element1);

			fc.utobj().isTextDisplayed(driver, subject,
					"was not able to verify Subject at Messages Detail at Corporate User Page");
			fc.utobj().isTextDisplayed(driver, "FranConnect Administrator",
					"was not able to verify FROM at corporate User Page");
			fc.utobj().isTextDisplayed(driver, userFullName, "was not able to verify TO at corporate User Page");
			fc.utobj().isTextDisplayed(driver, messages, "was not able to verify Subject");

			fc.utobj().printTestStep("Verify Email");

			Map<String, String> mailInfo = fc.utobj().readMailBox(subject, messages, emailId, "sdg@1a@Hfs");

			if (mailInfo.size() == 0 || !mailInfo.get("mailBody").contains(messages)) {
				fc.utobj().throwsException("was not able to verify email");
			}

			if (mailInfo.size() == 0 || !mailInfo.get("mailBody").contains("Alabama")) {
				fc.utobj().throwsException("was not able to verify Keyword In Email");
			}

			if (mailInfo.size() == 0 || mailInfo.get("mailBody").contains("$USER_STATE$")) {
				fc.utobj().throwsException("Keyword is not replacing In Email");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/*
	 * TC-10008
	 */

	@Test(groups = { "thehub", "thehub_mail","hub_messages"})
	@TestCase(createdOn = "2018-03-12", updatedOn = "2018-03-12", testCaseDescription = "Verify Send Messages To Added Role", testCaseId = "TC_Hub_Messages_Role_01")
	private void sendMessagesToRoles() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			driver = fc.loginpage().login(driver);
			TheHubMessagesPage pobj = new TheHubMessagesPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Admin > Access Control > Roles");
			fc.utobj().printTestStep("Add New Corporate User");
			AdminUsersRolesAddNewRolePageTest roles_page = new AdminUsersRolesAddNewRolePageTest();
			String roleName = fc.utobj().generateTestData("Testrolemessages");
			roles_page.addCorporateRoles(driver, roleName);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUserPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "hubautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser.setRole(roleName);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUserPage.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To The Hub > Messages Page");
			fc.hub().hub_common().theHubMessageSubModule(driver);
			fc.utobj().clickElement(driver, pobj.composeLink);

			fc.utobj().printTestStep("Send Messgaes Added Role Contacts");
			Messages hub_common = new Messages();
			hub_common.setAddressType("To");
			hub_common.setContactType("Internal");
			hub_common.setRecipientsUser("Roles");
			hub_common.setToEmail(roleName);
			hub_common.setMessagesType("Text");
			String messages = fc.utobj().generateTestData("TestMessages");
			hub_common.setMessages(messages);
			String subject = fc.utobj().generateTestData("TestSubject");
			hub_common.setSubject(subject);
			new MessagesTest().composeMessages(driver, hub_common);

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With Corporate User");
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());

			fc.utobj().printTestStep("Navigate To The Hub > Messages Page");
			fc.hub().hub_common().theHubMessageSubModule(driver);
			fc.utobj().clickElement(driver, pobj.inboxTab);
			fc.utobj().selectDropDown(driver, pobj.currentFolderSelect, "Inbox");
			fc.utobj().showAll(driver);

			fc.utobj().printTestStep("Verify The Messages In Inbox");
			boolean isMessagesUser = fc.utobj().assertPageSource(driver, subject);
			if (isMessagesUser == false) {
				fc.utobj().throwsException("Message is not present");
			}

			WebElement element1 = fc.utobj().getElementByLinkText(driver, subject);
			fc.utobj().clickElement(driver, element1);

			fc.utobj().isTextDisplayed(driver, subject,
					"was not able to verify Subject at Messages Detail at Corporate User Page");
			fc.utobj().isTextDisplayed(driver, "FranConnect Administrator",
					"was not able to verify FROM at corporate User Page");
			fc.utobj().isTextDisplayed(driver, corpUser.getuserFullName(),
					"was not able to verify TO at corporate User Page");
			fc.utobj().isTextDisplayed(driver, messages, "was not able to verify Subject");

			fc.utobj().printTestStep("Verify Email");

			Map<String, String> mailInfo = fc.utobj().readMailBox(subject, messages, emailId, "sdg@1a@Hfs");

			if (mailInfo.size() == 0 || !mailInfo.get("mailBody").contains(messages)) {
				fc.utobj().throwsException("was not able to verify email");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	/*
	 * TC-9999
	 */

	@Test(groups = { "thehub" ,"hub_messages"})
	@TestCase(createdOn = "2018-03-29", updatedOn = "2018-03-29", testCaseDescription = "To Verify the Compose Tab functionality", testCaseId = "TC_Compose_Messages_Tab_01")
	public void verifyCompletedComposeMessages() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubMessagesPage pobj = new TheHubMessagesPage(driver);

			fc.utobj().printTestStep("Navigate To The Hub > Messages Page");
			fc.hub().hub_common().theHubMessageSubModule(driver);
			fc.utobj().clickElement(driver, pobj.composeLink);

			fc.utobj().printTestStep("Verify To Field");
			boolean isToPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[@id='theText']/ancestor::td/preceding-sibling::td[contains(text(),'To')]");
			if (isToPresent == false) {
				fc.utobj().throwsException("To Field is not present");
			}

			fc.utobj().printTestStep("Verify CC Field");
			boolean isCCPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[@id='sCcAutoUser']/ancestor::td/preceding-sibling::td[contains(text(),'Cc')]");
			if (isCCPresent == false) {
				fc.utobj().throwsException("Cc Field is not present");
			}

			fc.utobj().printTestStep("Verify BCC Field");
			boolean isBCCPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[@id='sBCCAutoUser']/ancestor::td/preceding-sibling::td[contains(text(),'Bcc')]");
			if (isBCCPresent == false) {
				fc.utobj().throwsException("Bcc Field is not present");
			}

			fc.utobj().printTestStep("Verify Importance Field");
			boolean isImpPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text(),'Importance')]/preceding-sibling::td/input[@id='priority' and @type='checkbox']");
			if (isImpPresent == false) {
				fc.utobj().throwsException("Importance CheckBox is not present");
			}

			fc.utobj().printTestStep("Verify Read Receipt Field");
			boolean isReadRecepientPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text(),'Read Receipt')]/preceding-sibling::td/input[@id='readReceipt' and @type='checkbox']");
			if (isReadRecepientPresent == false) {
				fc.utobj().throwsException("Read Receipt CheckBox is not present");
			}

			fc.utobj().printTestStep("Verify To Address Book Link");
			boolean isToAddressBookPresent = fc.utobj().isElementPresent(driver, pobj.addressBookToLnk);
			if (isToAddressBookPresent == false) {
				fc.utobj().throwsException("To Address Book Link is not present");
			}

			fc.utobj().printTestStep("Verify Cc Address Book Link");
			boolean isCcAddressBookPresent = fc.utobj().isElementPresent(driver, pobj.addressBookCCLnk);
			if (isCcAddressBookPresent == false) {
				fc.utobj().throwsException("Cc Address Book Link is not present");
			}

			fc.utobj().printTestStep("Verify Bcc Address Book Link");
			boolean isBccAddressBookPresent = fc.utobj().isElementPresent(driver, pobj.addressBookBCCLnk);
			if (isBccAddressBookPresent == false) {
				fc.utobj().throwsException("Bcc Address Book Link is not present");
			}

			fc.utobj().printTestStep("Verify Subject Field");
			boolean isSubjectPresent = fc.utobj().isElementPresent(driver, pobj.subject);
			if (isSubjectPresent == false) {
				fc.utobj().throwsException("Subject Field is not present");
			}

			fc.utobj().printTestStep("Verify Add Attachement Field");
			boolean isAttPresent = fc.utobj().isElementPresent(driver, pobj.addAttachament);
			if (isAttPresent == false) {
				fc.utobj().throwsException("Subject Field is not present");
			}

			fc.utobj().printTestStep("Verify Text Radio Button");
			boolean isTextPresent = fc.utobj().isElementPresent(driver, pobj.texthtmlOption.get(0));
			if (isTextPresent == false) {
				fc.utobj().throwsException("Text Redio Button is not present");
			}

			fc.utobj().printTestStep("Verify Html Radio Button");
			boolean isHtmlPresent = fc.utobj().isElementPresent(driver, pobj.texthtmlOption.get(1));
			if (isHtmlPresent == false) {
				fc.utobj().throwsException("Html Redio Button is not present");
			}

			fc.utobj().printTestStep("Verify Signature checkbox Button");
			boolean isSigPresent = fc.utobj().isElementPresent(driver, pobj.isSignatured);
			if (isSigPresent == false) {
				fc.utobj().throwsException("Signature checkbox is not present");
			}

			fc.utobj().printTestStep("Verify Save a copy of this message in folder checkbox Button");
			boolean isSavedPresent = fc.utobj().isElementPresent(driver, pobj.isSavedCheck);
			if (isSavedPresent == false) {
				fc.utobj().throwsException("Save a copy of this message in folder checkbox is not present");
			}

			fc.utobj().printTestStep("Verify Send Email to Users checkbox Button");
			boolean isSentMailPresent = fc.utobj().isElementPresent(driver, pobj.isSentMailCheck);
			if (isSentMailPresent == false) {
				fc.utobj().throwsException("Send Email to Users checkbox is not present");
			}

			fc.utobj().printTestStep("Verify Send Button");
			boolean isSendBtnPresent = fc.utobj().isElementPresent(driver, fc.utobj().getElement(driver, pobj.sendBtn));
			if (isSendBtnPresent == false) {
				fc.utobj().throwsException("Send Button is not present");
			}

			fc.utobj().printTestStep("Verify Save As Draft Button");
			boolean isSADBtnPresent = fc.utobj().isElementPresent(driver, fc.utobj().getElement(driver, pobj.sendBtn));
			if (isSADBtnPresent == false) {
				fc.utobj().throwsException("Save As Draft Button is not present");
			}

			fc.utobj().printTestStep("Verify Spell Check Button");
			boolean isSpellBtnPresent = fc.utobj().isElementPresent(driver,
					fc.utobj().getElement(driver, pobj.sendBtn));
			if (isSpellBtnPresent == false) {
				fc.utobj().throwsException("Spell Check Button is not present");
			}

			fc.utobj().printTestStep("Verify Cancel Button");
			boolean isCancelBtnPresent = fc.utobj().isElementPresent(driver,
					fc.utobj().getElement(driver, pobj.sendBtn));
			if (isCancelBtnPresent == false) {
				fc.utobj().throwsException("Cancel Button is not present");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/*
	 * TC-10008
	 */
	@Test(groups = { "thehub", "thehub_mail","hub_messages"})
	@TestCase(createdOn = "2018-03-29", updatedOn = "2018-03-29", testCaseDescription = "Verify Send Messages To Added Role With TO , CC , BCC", testCaseId = "TC_Hub_Messages_Role_02")
	private void sendMessagesToRolesCCBcc() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			driver = fc.loginpage().login(driver);
			TheHubMessagesPage pobj = new TheHubMessagesPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Admin > Access Control > Roles");
			fc.utobj().printTestStep("Add New Corporate User");
			AdminUsersRolesAddNewRolePageTest roles_page = new AdminUsersRolesAddNewRolePageTest();
			String roleName = fc.utobj().generateTestData("Testrolemessages");
			roles_page.addCorporateRoles(driver, roleName);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUserPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "hubautomation@staffex.com";

			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser.setRole(roleName);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUserPage.createDefaultUser(driver, corpUser);

			fc.utobj().printTestStep("Add another corporate user for cc");

			CorporateUser corpUserCC = new CorporateUser();
			corpUserCC.setEmail(emailId);
			corpUserCC.setRole(roleName);
			corpUserCC = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUserCC);
			corporateUserPage.createDefaultUser(driver, corpUserCC);

			fc.utobj().printTestStep("Add another corporate user for bcc");
			CorporateUser corpUserBCC = new CorporateUser();
			corpUserBCC.setEmail(emailId);
			corpUserBCC.setRole(roleName);
			corpUserBCC = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUserBCC);
			corporateUserPage.createDefaultUser(driver, corpUserBCC);

			fc.utobj().printTestStep("Navigate To The Hub > Messages Page");
			fc.hub().hub_common().theHubMessageSubModule(driver);
			fc.utobj().clickElement(driver, pobj.composeLink);

			fc.utobj().printTestStep("Send Messgaes Added Role Contacts");
			fc.utobj().clickElement(driver, pobj.addressBookTo);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			if (!fc.utobj().isSelected(driver, pobj.internalContact)) {
				fc.utobj().clickElement(driver, pobj.internalContact);
			}
			fc.utobj().setToDefault(driver, pobj.selectUsers);
			fc.utobj().selectValFromMultiSelect(driver, pobj.selectUsers, "Roles");
			fc.utobj().clickElement(driver, pobj.submitOk);
			fc.utobj().sendKeys(driver, pobj.searchTextBx, roleName);
			fc.utobj().clickElement(driver, pobj.searchImgBtn);

			fc.utobj().sleep();
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[contains(text () , '" + roleName + "')]/ancestor::tr/td/input"));

			fc.utobj().clickElement(driver, pobj.addUserBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, pobj.addressBookCC);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			if (!fc.utobj().isSelected(driver, pobj.internalContact)) {
				fc.utobj().clickElement(driver, pobj.internalContact);
			}
			fc.utobj().setToDefault(driver, pobj.selectUsers);
			fc.utobj().selectValFromMultiSelect(driver, pobj.selectUsers, "Roles");
			fc.utobj().clickElement(driver, pobj.submitOk);
			fc.utobj().sendKeys(driver, pobj.searchTextBx, roleName);
			fc.utobj().clickElement(driver, pobj.searchImgBtn);

			fc.utobj().sleep();
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[contains(text () , '" + roleName + "')]/ancestor::tr/td/input"));

			fc.utobj().clickElement(driver, pobj.addUserBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, pobj.addressBookBCC);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			if (!fc.utobj().isSelected(driver, pobj.internalContact)) {
				fc.utobj().clickElement(driver, pobj.internalContact);
			}
			fc.utobj().setToDefault(driver, pobj.selectUsers);
			fc.utobj().selectValFromMultiSelect(driver, pobj.selectUsers, "Roles");
			fc.utobj().clickElement(driver, pobj.submitOk);
			fc.utobj().sendKeys(driver, pobj.searchTextBx, roleName);
			fc.utobj().clickElement(driver, pobj.searchImgBtn);

			fc.utobj().sleep();
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[contains(text () , '" + roleName + "')]/ancestor::tr/td/input"));

			fc.utobj().clickElement(driver, pobj.addUserBtn);
			fc.utobj().switchFrameToDefault(driver);

			String subject = fc.utobj().generateTestData("Test02Subject");
			String messages = fc.utobj().generateTestData("Test02Message");

			fc.utobj().sendKeys(driver, pobj.subject, subject);
			fc.utobj().moveToElement(driver, pobj.texthtmlOption.get(0));
			if (!fc.utobj().isSelected(driver, pobj.texthtmlOption.get(0))) {
				fc.utobj().clickElement(driver, pobj.texthtmlOption.get(0));
			}
			fc.utobj().sendKeys(driver, pobj.messages, messages);
			fc.utobj().clickElement(driver, pobj.sendBtn);
			fc.utobj().clickElement(driver, pobj.OkBtn);

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With Corporate User");
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());

			fc.utobj().printTestStep("Navigate To The Hub > Messages Page");
			fc.hub().hub_common().theHubMessageSubModule(driver);
			fc.utobj().clickElement(driver, pobj.inboxTab);
			fc.utobj().selectDropDown(driver, pobj.currentFolderSelect, "Inbox");
			fc.utobj().showAll(driver);

			fc.utobj().printTestStep("Verify The Messages In Inbox");
			boolean isSubjectPresent = fc.utobj().assertPageSource(driver, subject);
			if (!isSubjectPresent) {
				fc.utobj().throwsException("Messages is not able to verify in To inbox");
			}

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login with CC contact corporate cser");
			fc.loginpage().loginWithParameter(driver, corpUserCC.getUserName(), corpUserCC.getPassword());

			fc.utobj().printTestStep("Navigate To The Hub > Messages Page");
			fc.hub().hub_common().theHubMessageSubModule(driver);
			fc.utobj().clickElement(driver, pobj.inboxTab);
			fc.utobj().selectDropDown(driver, pobj.currentFolderSelect, "Inbox");
			fc.utobj().showAll(driver);

			fc.utobj().printTestStep("Verify Messages for cc contact user");
			fc.utobj().printTestStep("Verify The Messages In Inbox");
			boolean isSubjectPresentCC = fc.utobj().assertPageSource(driver, subject);

			if (!isSubjectPresentCC) {
				fc.utobj().throwsException("Messages is not able to verify in CC inbox");
			}

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login with BCC contact corporate cser");
			fc.loginpage().loginWithParameter(driver, corpUserBCC.getUserName(), corpUserBCC.getPassword());

			fc.utobj().printTestStep("Navigate To The Hub > Messages Page");
			fc.hub().hub_common().theHubMessageSubModule(driver);
			fc.utobj().clickElement(driver, pobj.inboxTab);
			fc.utobj().selectDropDown(driver, pobj.currentFolderSelect, "Inbox");
			fc.utobj().showAll(driver);

			fc.utobj().printTestStep("Verify Messages for BCC contact user");
			fc.utobj().printTestStep("Verify The Messages In Inbox");
			boolean isSubjectPresentBCC = fc.utobj().assertPageSource(driver, subject);

			if (!isSubjectPresentBCC) {
				fc.utobj().throwsException("Messages is not able to verify in BCC inbox");
			}

			fc.utobj().printTestStep("Verify Email");
			Map<String, String> mailInfo = fc.utobj().readMailBox(subject, messages, emailId, "sdg@1a@Hfs");

			if (mailInfo.size() == 0 || !mailInfo.get("mailBody").contains(messages)) {
				fc.utobj().throwsException("was not able to verify email");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	void closeTinyMCETrial(WebDriver driver){
		//Trial
		try {
			List<WebElement> listEle=fc.utobj().getElementListByXpath(driver, ".//*[@class='mce-close']");
			for (WebElement webElement : listEle) {
				fc.utobj().clickElement(driver, webElement);
			}
		} catch (Exception e) {
			Reporter.log(e.getMessage());
		}
	}
}
