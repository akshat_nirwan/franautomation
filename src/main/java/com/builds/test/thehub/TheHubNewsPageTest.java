package com.builds.test.thehub;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.admin.AdminUsersRolesAddNewRolePageTest;
import com.builds.test.admin.OptionsConfigureQuickLinkPageTest;
import com.builds.test.common.CorporateUser;
import com.builds.uimaps.common.CommonUI;
import com.builds.uimaps.fs.SearchUI;
import com.builds.uimaps.thehub.AdminTheHubNewsPage;
import com.builds.uimaps.thehub.TheHubNewsPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class TheHubNewsPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "thehub" ,"hub_news" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Recently Uploaded News Item At The Hub > News > Home", testCaseId = "TC_178_Verify_Recently_Uploaded")
	private void verifyRecentlyUploaded() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubNewsPage pobj = new TheHubNewsPage(driver);

			AdminTheHubNewsPageTest newsPage = new AdminTheHubNewsPageTest();

			fc.utobj().printTestStep("Navigate To Admin > The Hub > News Page");
			fc.utobj().printTestStep("Add News Item");

			String newsItemTitle = fc.utobj().generateTestData(dataSet.get("newsItemTitle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String file = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));

			String editorText = fc.utobj().generateTestData(dataSet.get("editorText"));
			fc.hub().hub_common().adminTheHubNewsPage(driver);
			fc.utobj().clickElement(driver, pobj.topStoriesTab);
			
			News news=new News();
			news.setNewsItemTitle(newsItemTitle);
			news.setNewsBriefSummary(briefSummary);
			news.setNewsEditorText(editorText);
			news.setFile(file);
			
			newsPage.addNewsItems(driver,news);

			fc.utobj().printTestStep("Navigate To The Hub > News Page");
			fc.hub().hub_common().theHubNewsSubModule(driver);

			fc.utobj().printTestStep("Verify News Feeds Link At Home > News");
			boolean isLinkPresent = fc.utobj().assertLinkText(driver, "News Feeds");
			if (isLinkPresent == false) {
				fc.utobj().throwsException("News Feeds Link is not present at The Hub > News Feeds");
			}

			fc.utobj().clickElement(driver, pobj.homeTab);

			fc.utobj().printTestStep("Verify News Item At Home Page Recently Uploaded Page");
			fc.utobj().isTextDisplayed(driver, newsItemTitle,
					"was not able to verify Recently Uploaded News's Title at The Hub News Page");
			fc.utobj().isTextDisplayed(driver, briefSummary,
					"was not able to verify Recently Uploaded News's Brief Summary at The Hub News Page");

			fc.utobj().printTestStep("Navigate To The Hub > Top Stories Page");

			fc.utobj().clickElement(driver, pobj.topStoriesTab);
			fc.utobj().printTestStep("Archive The News Story");
			fc.utobj().moveToElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + newsItemTitle + "']"));

			fc.utobj().actionImgOption(driver, newsItemTitle, "Archive");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Archive News At Archived Tab");
			fc.utobj().clickElement(driver, pobj.archivedNewsTab);

			WebElement element = fc.utobj().getElementByXpath(driver,
					".//*[contains(text () ,'" + newsItemTitle + "')]");
			fc.utobj().moveToElement(driver, element);
			if (!element.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify News Item Title");
			}

			WebElement element1 = fc.utobj().getElementByXpath(driver,
					".//*[contains(text () ,'" + briefSummary + "')]");
			fc.utobj().moveToElement(driver, element1);
			if (!element1.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify News Item's Summary");
			}

			WebElement element2 = fc.utobj().getElementByXpath(driver,
					".//*[contains(text () , 'FranConnect Administrator')]");
			fc.utobj().moveToElement(driver, element2);
			if (!element2.isDisplayed()) {
				fc.utobj().throwsException("was not able to added By");
			}

			fc.utobj().printTestStep("Verify The News Details At pop up ");

			fc.utobj().clickElementByJS(driver, fc.utobj().getElementByLinkText(driver, newsItemTitle));
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			WebElement element3 = fc.utobj().getElementByXpath(driver,
					".//td[contains(text () ,'" + newsItemTitle + "')]");
			fc.utobj().moveToElement(driver, element3);
			if (!element3.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify News Item Title");
			}

			WebElement element4 = fc.utobj().getElementByXpath(driver,
					".//td[contains(text () ,'" + briefSummary + "')]");
			fc.utobj().moveToElement(driver, element4);
			if (!element4.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify News Item's Summary");
			}

			WebElement element5 = fc.utobj().getElementByXpath(driver, ".//*[contains(text () ,'" + editorText + "')]");
			fc.utobj().moveToElement(driver, element5);
			if (!element5.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify EditorText");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub","hub_news" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add News Item In Top Stories At The Hub > News > Top Stories", testCaseId = "TC_179_Add_New_Item")
	private void addNewsItemTopStories() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubNewsPage pobj = new TheHubNewsPage(driver);

			fc.utobj().printTestStep("Navigate To The Hub > News Page");
			fc.utobj().printTestStep("Add News Item In Top Stories");
			fc.hub().hub_common().theHubNewsSubModule(driver);
			fc.utobj().clickElement(driver, pobj.topStoriesTab);

			fc.utobj().clickElement(driver, pobj.addNewsItemLink);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			String newsItemTitle = fc.utobj().generateTestData(dataSet.get("newsItemTitle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			
			String editorText =/*fc.utobj().generateTestData(dataSet.get("editorText"));*/
					"A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart. "
					+ "I am alone, and feel the charm of existence in this spot, which was created for the bliss of souls like mine. I am so happy, "
					+ "my dear friend, so absorbed in the exquisite sense of mere tranquil existence, that I neglect my talents. I should be incapable "
					+ "of drawing a single stroke at the present moment; and yet I feel that I never was a greater artist than now. When, while the lovely "
					+ "valley teems with vapour around me, and the meridian sun strikes the upper surface of the impenetrable foliage of my trees, and but "
					+ "a few stray gleams steal into the inner sanctuary, I throw myself down among the tall grass by the trickling stream; and, as I lie "
					+ "close to the earth, a thousand unknown plants are noticed by me: when I hear the buzz of the little world among the stalks, and grow "
					+ "familiar with the countless indescribable forms of the insects and flies, then I feel the presence of the Almighty, who formed us in "
					+ "his own image, and the breath of that universal love which bears and sustains us, as it floats around us in an eternity of bliss; and "
					+ "then, my friend, when darkness overspreads my eyes, and heaven and earth seem to dwell in my soul and absorb its power, like the form "
					+ "of a beloved mistress, then I often think with longing, Oh, would I could describe these conceptions, could impress upon paper all "
					+ "that is living so full and warm within me, that it might be the mirror of my soul, as my soul is the mirror of the infinite God! O "
					+ "my friend -- but it is too much for my strength -- I sink under the weight of the splendour of these visions! A wonderful serenity "
					+ "has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart. I am alone, and feel "
					+ "the charm of existence in this spot, which was created for the bliss of souls like mine. I am so happy, my dear friend, so absorbed "
					+ "in the exquisite sense of mere tranquil existence, that I neglect my talents. I should be incapable of drawing a single stroke at "
					+ "the present moment; and yet I feel that I never was a greater artist than now. When, while the lovely valley teems with vapour "
					+ "around me, and the meridian sun strikes the upper surface of the impenetrable foliage of my trees, and but a few stray gleams "
					+ "steal into the inner sanctuary, I throw myself down among the tall grass by the trickling stream; and, as I lie close to the "
					+ "earth, a thousand unknown plants are noticed by me: when I hear the buzz of the little world among the stalks, and grow "
					+ "familiar with the";
			
			String file = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			fc.utobj().sendKeys(driver, pobj.newsItemTitle, newsItemTitle);
			fc.utobj().sendKeys(driver, pobj.briefSummary, briefSummary);
			if (!fc.utobj().isSelected(driver,pobj.mediaType.get(0))) {
				fc.utobj().clickElement(driver, pobj.mediaType.get(0));
			}
			fc.utobj().sendKeys(driver, pobj.newsItemFile, file);
			fc.utobj().switchFrameById(driver, "completeNews_ifr");
			Actions actions = new Actions(driver);
			actions.moveToElement(fc.utobj().getElement(driver, pobj.editorTextArea));
			actions.click();
			actions.sendKeys(editorText);
			fc.utobj().logReportMsg("Entered Text", editorText);
			actions.build().perform();
			fc.utobj().switchFrameToDefault(driver);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().printTestStep("Verify The News Item Preview");
			fc.utobj().clickElement(driver, pobj.previewBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			boolean isEditorTextDisplayed=fc.utobj().assertPageSource(driver, editorText);
			if (isEditorTextDisplayed==false) {
				fc.utobj().throwsException("Complete Story is not visible at preview window");
			}
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify News Item In Top Stories");
			fc.utobj().clickElement(driver, pobj.topStoriesTab);
			
			if (!fc.utobj().getElementByXpath(driver, ".//a[.='" + newsItemTitle + "']").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify add News Item Story");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//*[.='" + briefSummary + "']").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify add News Item Story's Summary");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//td/span[contains(text () , 'FranConnect Administrator')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify add News Item Story's Summary");
			}

			fc.utobj().printTestStep("Verify The News At Pop up");

			fc.utobj().clickElementByJS(driver, fc.utobj().getElementByLinkText(driver, newsItemTitle));
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			WebElement element = fc.utobj().getElementByXpath(driver,
					".//*[contains(text () ,'" + newsItemTitle + "')]");
			fc.utobj().moveToElement(driver, element);
			if (!element.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify News Item Title");
			}

			WebElement element1 = fc.utobj().getElementByXpath(driver,
					".//*[contains(text () ,'" + briefSummary + "')]");
			fc.utobj().moveToElement(driver, element1);
			if (!element1.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify News Item's Summary");
			}

			WebElement element3 = fc.utobj().getElementByXpath(driver, ".//*[contains(text () ,'" + editorText + "')]");
			fc.utobj().moveToElement(driver, element3);
			if (!element3.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify EditorText");
			}
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void addNewsItemInTopStories(WebDriver driver, String newsItemTitle, String briefSummary, String editorText,
			String file, String mediaType, Map<String, String> dataSet) throws Exception {

		String testCaseId = "TC_Add_News_Item_In_Top_Stories";

		if (fc.utobj().validate(testCaseId)) {
			try {
				TheHubNewsPage pobj = new TheHubNewsPage(driver);

				fc.hub().hub_common().theHubNewsSubModule(driver);
				fc.utobj().clickElement(driver, pobj.topStoriesTab);

				fc.utobj().clickElement(driver, pobj.addNewsItemLink);
				fc.commonMethods().switch_cboxIframe_frameId(driver);

				fc.utobj().sendKeys(driver, pobj.newsItemTitle, newsItemTitle);
				fc.utobj().sendKeys(driver, pobj.briefSummary, briefSummary);

				if (mediaType.equalsIgnoreCase("Image")) {

					if (!fc.utobj().isSelected(driver, pobj.mediaType.get(0))) {
						fc.utobj().clickElement(driver, pobj.mediaType.get(0));
					}

					fc.utobj().sendKeys(driver, pobj.newsItemFile, file);

				} else if (mediaType.equalsIgnoreCase("Video")) {

					if (!fc.utobj().isSelected(driver, pobj.mediaType.get(1))) {
						fc.utobj().clickElement(driver, pobj.mediaType.get(1));
					}

					fc.utobj().sendKeys(driver, pobj.videoFile, file);

				} else if (mediaType.equalsIgnoreCase("Embed Code")) {

					if (!fc.utobj().isSelected(driver, pobj.mediaType.get(2))) {
						fc.utobj().clickElement(driver, pobj.mediaType.get(2));
					}

					fc.utobj().sendKeys(driver, pobj.embedCode, dataSet.get("embedCode"));
				}

				fc.utobj().switchFrameById(driver, "completeNews_ifr");

				Actions actions = new Actions(driver);
				actions.moveToElement(fc.utobj().getElement(driver, pobj.editorTextArea));
				actions.click();

				actions.sendKeys(editorText);
				fc.utobj().logReportMsg("Entered Text", editorText);
				actions.build().perform();
				fc.utobj().switchFrameToDefault(driver);

				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().clickElement(driver, pobj.addBtn);
				fc.utobj().clickElement(driver, pobj.closeBtn);
				fc.utobj().switchFrameToDefault(driver);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Was not able to add News Items Into Story :: The Hub > News ");

		}
	}

	@Test(groups = { "thehub","hub_news"  })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Modify Folder At The Hub > News > Top Stories", testCaseId = "TC_181_Modify_Top_Stories_Folder")
	private void modifyFolderTopStories() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubNewsPage pobj = new TheHubNewsPage(driver);

			fc.utobj().printTestStep("Navigate To The Hub > Top Stories Page");
			fc.hub().hub_common().theHubNewsSubModule(driver);

			fc.utobj().printTestStep("Modify Top Stories Folder");

			fc.utobj().clickElement(driver, pobj.topStoriesTab);

			fc.utobj().clickElement(driver, pobj.modifyFolder);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().sendKeys(driver, pobj.folderName, "Top Stories");
			String folderSummary = dataSet.get("folderSummary");
			fc.utobj().sendKeys(driver, pobj.folderSummary, folderSummary);

			if (!fc.utobj().isSelected(driver, pobj.accessibleToAll.get(0))) {
				fc.utobj().clickElement(driver, pobj.accessibleToAll.get(0));
			}

			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Modify Top Story Folder");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, "Top Stories");
			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to Modify Folder");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"thehub","hub_news" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Report of News Item In Top Stories At The Hub > News > Top Stories", testCaseId = "TC_182_Report_Stories")
	private void reportNewsItemActionImgTopStories() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubNewsPage pobj = new TheHubNewsPage(driver);

			fc.utobj().printTestStep("Navigate To The Hub > News Page");
			fc.hub().hub_common().theHubNewsSubModule(driver);
			String newsItemTitle = fc.utobj().generateTestData(dataSet.get("newsItemTitle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String editorText = fc.utobj().generateTestData(dataSet.get("editorText"));
			String mediaType = "Video";
			String file = FranconnectUtil.config.get("inputDirectory") + "\\testData\\media\\"
					+ dataSet.get("videoFile");

			fc.utobj().printTestStep("Add News Item In Top Stories");
			addNewsItemInTopStories(driver, newsItemTitle, briefSummary, editorText, file, mediaType, dataSet);

			fc.utobj().printTestStep("Report News Item");
			fc.utobj().actionImgOption(driver, newsItemTitle, "Report");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().printTestStep("Verify News Item Info");
			WebElement element = fc.utobj().getElementByXpath(driver,
					".//*[contains(text () ,'" + newsItemTitle + "')]");
			fc.utobj().moveToElement(driver, element);
			if (!element.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify News Item Title");
			}

			WebElement element1 = fc.utobj().getElementByXpath(driver,
					".//*[contains(text () ,'" + briefSummary + "')]");
			fc.utobj().moveToElement(driver, element1);
			if (!element1.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify News Item's Summary");
			}

			WebElement element2 = fc.utobj().getElementByXpath(driver,
					".//*[contains(text () , 'FranConnect Administrator')]");
			fc.utobj().moveToElement(driver, element2);
			if (!element2.isDisplayed()) {
				fc.utobj().throwsException("was not able to added By");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub" ,"hub_news" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Modify News Item In Top Stories The Hub > News > Top Stories", testCaseId = "TC_183_Modify_Stories")
	private void modifyNewsItemActionImgTopStories() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubNewsPage pobj = new TheHubNewsPage(driver);

			fc.utobj().printTestStep("Navigate To The Hub > News Page");
			String newsItemTitle = fc.utobj().generateTestData(dataSet.get("newsItemTitle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String editorText = fc.utobj().generateTestData(dataSet.get("editorText"));
			String mediaType = "Video";
			String file = FranconnectUtil.config.get("inputDirectory") + "\\testData\\media\\"
					+ dataSet.get("videoFile");

			fc.utobj().printTestStep("Add News Item In Top Stories");
			addNewsItemInTopStories(driver, newsItemTitle, briefSummary, editorText, file, mediaType, dataSet);

			fc.utobj().printTestStep("Modify News Item");
			fc.utobj().actionImgOption(driver, newsItemTitle, "Modify");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			newsItemTitle = fc.utobj().generateTestData(dataSet.get("newsItemTitle"));
			briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String editorText1 = fc.utobj().generateTestData(dataSet.get("editorText"));
			String file1 = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));

			fc.utobj().sendKeys(driver, pobj.newsItemTitle, newsItemTitle);
			fc.utobj().sendKeys(driver, pobj.briefSummary, briefSummary);

			if (!fc.utobj().isSelected(driver, pobj.mediaType.get(0))) {
				fc.utobj().clickElement(driver, pobj.mediaType.get(0));
			}

			fc.utobj().sendKeys(driver, pobj.newsItemFile, file1);

			fc.utobj().switchFrameById(driver, "completeNews_ifr");

			Actions actions = new Actions(driver);
			actions.moveToElement(fc.utobj().getElement(driver, pobj.editorTextArea));
			actions.click();

			actions.sendKeys(editorText1);
			actions.build().perform();
			fc.utobj().switchFrameToDefault(driver);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Modify News Item");
			WebElement element = fc.utobj().getElementByXpath(driver,
					".//*[contains(text () ,'" + newsItemTitle + "')]");
			fc.utobj().moveToElement(driver, element);
			if (!element.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify News Item Title");
			}

			WebElement element1 = fc.utobj().getElementByXpath(driver,
					".//*[contains(text () ,'" + briefSummary + "')]");
			fc.utobj().moveToElement(driver, element1);
			if (!element1.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify News Item's Summary");
			}

			WebElement element2 = fc.utobj().getElementByXpath(driver,
					".//*[contains(text () , 'FranConnect Administrator')]");
			fc.utobj().moveToElement(driver, element2);
			if (!element2.isDisplayed()) {
				fc.utobj().throwsException("was not able to added By");
			}

			fc.utobj().printTestStep("Verify The News Item At Pop Up");

			fc.utobj().clickElementByJS(driver, fc.utobj().getElementByLinkText(driver, newsItemTitle));
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			WebElement element3 = fc.utobj().getElementByXpath(driver,
					".//*[contains(text () ,'" + newsItemTitle + "')]");
			fc.utobj().moveToElement(driver, element3);
			if (!element3.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify News Item Title");
			}

			WebElement element4 = fc.utobj().getElementByXpath(driver,
					".//*[contains(text () ,'" + briefSummary + "')]");
			fc.utobj().moveToElement(driver, element4);
			if (!element4.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify News Item's Summary");
			}

			String text = editorText.concat(editorText1);
			System.out.println(text);
			WebElement element5 = fc.utobj().getElementByXpath(driver, ".//*[contains(text () ,'" + text + "')]");
			fc.utobj().moveToElement(driver, element5);
			if (!element5.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify EditorText");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub" ,"hub_news" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Archive News Item In Top Stories At The Hub > News > Top Stories", testCaseId = "TC_184_Archive_Stories")
	private void archiveNewsItemActionImgTopStories() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubNewsPage pobj = new TheHubNewsPage(driver);

			fc.utobj().printTestStep("Navigate To The Hub > News Page");
			String newsItemTitle = fc.utobj().generateTestData(dataSet.get("newsItemTitle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String editorText = fc.utobj().generateTestData(dataSet.get("editorText"));
			String mediaType = "Video";
			String file = FranconnectUtil.config.get("inputDirectory") + "\\testData\\media\\"
					+ dataSet.get("videoFile");

			fc.utobj().printTestStep("Add News Item In Top Stories");
			addNewsItemInTopStories(driver, newsItemTitle, briefSummary, editorText, file, mediaType, dataSet);

			fc.utobj().printTestStep("Archive News Item");
			fc.utobj().actionImgOption(driver, newsItemTitle, "Archive");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify Archive News Item At Archived Tab");
			fc.utobj().clickElement(driver, pobj.archivedNewsTab);

			WebElement element = fc.utobj().getElementByXpath(driver,
					".//*[contains(text () ,'" + newsItemTitle + "')]");
			fc.utobj().moveToElement(driver, element);
			if (!element.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify News Item Title");
			}

			WebElement element1 = fc.utobj().getElementByXpath(driver,
					".//*[contains(text () ,'" + briefSummary + "')]");
			fc.utobj().moveToElement(driver, element1);
			if (!element1.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify News Item's Summary");
			}

			WebElement element2 = fc.utobj().getElementByXpath(driver,
					".//*[contains(text () , 'FranConnect Administrator')]");
			fc.utobj().moveToElement(driver, element2);
			if (!element2.isDisplayed()) {
				fc.utobj().throwsException("was not able to added By");
			}

			fc.utobj().printTestStep("Verify The News Item Details At Pop Up");

			WebElement elementCLik = fc.utobj().getElementByLinkText(driver, newsItemTitle);
			fc.utobj().clickElementByJS(driver, elementCLik);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			WebElement element3 = fc.utobj().getElementByXpath(driver,
					".//td[contains(text () ,'" + newsItemTitle + "')]");
			fc.utobj().moveToElement(driver, element3);
			if (!element3.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify News Item Title");
			}

			WebElement element4 = fc.utobj().getElementByXpath(driver,
					".//td[contains(text () ,'" + briefSummary + "')]");
			fc.utobj().moveToElement(driver, element4);
			if (!element4.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify News Item's Summary");
			}

			WebElement element5 = fc.utobj().getElementByXpath(driver, ".//*[contains(text () ,'" + editorText + "')]");
			fc.utobj().moveToElement(driver, element5);
			if (!element5.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify EditorText");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"thehub","hub_news" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Delete News Item In Top Stories At The Hub > News > Top Stories", testCaseId = "TC_185_Delete_Stories")
	private void deleteNewsItemActionImgTopStories() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To The Hub > News Page");
			String newsItemTitle = fc.utobj().generateTestData(dataSet.get("newsItemTitle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String editorText = fc.utobj().generateTestData(dataSet.get("editorText"));
			String mediaType = "Embed Code";

			String file = dataSet.get("embedCode");
			fc.utobj().printTestStep("Add News Item In Top Stories");
			addNewsItemInTopStories(driver, newsItemTitle, briefSummary, editorText, file, mediaType, dataSet);

			fc.utobj().printTestStep("Delete News Item");
			fc.utobj().actionImgOption(driver, newsItemTitle, "Delete");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Delete News Item");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, newsItemTitle);
			if (isTextPresent == true) {
				fc.utobj().throwsException("was not able to delete News Item at Top Story Page");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"thehub","hub_news" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add Sub Folder In Top Stories Folder At The Hub > News > Top Stories", testCaseId = "TC_186_Add_Sub_Folder")
	private void addSubFolderTopStories() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubNewsPage pobj = new TheHubNewsPage(driver);

			fc.utobj().printTestStep("Navigate To The Hub > News Page");
			fc.hub().hub_common().theHubNewsSubModule(driver);
			fc.utobj().printTestStep("Add Sub Folder In Top Stories Folder");
			fc.utobj().clickElement(driver, pobj.topStoriesTab);

			Actions builder = new Actions(driver);
			builder.moveToElement(fc.utobj().getElement(driver, pobj.moreActions))
					.click(fc.utobj().getElement(driver, pobj.moreActions));
			builder.perform();
			fc.utobj().getElement(driver, pobj.addSubFolder).click();

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			fc.utobj().sendKeys(driver, pobj.folderName, folderName);
			fc.utobj().sendKeys(driver, pobj.folderSummary, folderSummary);

			if (!fc.utobj().isSelected(driver, pobj.accessibleToAll.get(0))) {
				fc.utobj().clickElement(driver, pobj.accessibleToAll.get(0));
			}

			if (!fc.utobj().isSelected(driver, pobj.addNewsItemFolder.get(1))) {
				fc.utobj().clickElement(driver, pobj.addNewsItemFolder.get(1));
			}
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify Add Sub Folder In Top Stories");
			builder.moveToElement(fc.utobj().getElement(driver, pobj.topStoriesTab))
					.click(fc.utobj().getElement(driver, pobj.topStoriesTab));
			builder.perform();

			WebElement element1 = fc.utobj().getElementByXpath(driver, ".//a[.='" + folderName + "']");
			element1.click();

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + folderName + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to add Sub folder");
			}

			
			fc.utobj().printTestStep("Verify All The Options Link Listed under More Action Icon Of Top Stories");
			fc.utobj().clickElement(driver, pobj.topStoriesTab);
			Actions builder21 = new Actions(driver);
			builder21.moveToElement(fc.utobj().getElement(driver, pobj.moreActions))
					.click(fc.utobj().getElement(driver, pobj.moreActions));
			builder21.perform();
			boolean isfolderPresent = fc.utobj().assertLinkText(driver, "Add Sub Folder");
			if (isfolderPresent == false) {
				fc.utobj().throwsException("Add Sub Folder Link is not present under More Action");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void addSubFolderTopStories(WebDriver driver, String folderName, String folderSummary) throws Exception {

		String testCaseId = "TC_Add_Sub_Folder_Top_Stories";

		if (fc.utobj().validate(testCaseId)) {
			try {
				TheHubNewsPage pobj = new TheHubNewsPage(driver);
				fc.hub().hub_common().theHubNewsSubModule(driver);
				fc.utobj().clickElement(driver, pobj.topStoriesTab);

				Actions builder = new Actions(driver);
				builder.moveToElement(fc.utobj().getElement(driver, pobj.moreActions))
						.click(fc.utobj().getElement(driver, pobj.moreActions));
				builder.perform();
				fc.utobj().getElement(driver, pobj.addSubFolder).click();

				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().sendKeys(driver, pobj.folderName, folderName);
				fc.utobj().sendKeys(driver, pobj.folderSummary, folderSummary);

				/*if (!fc.utobj().isSelected(driver, pobj.accessibleToAll.get(0))) {
					fc.utobj().clickElement(driver, pobj.accessibleToAll.get(0));
				}*/

				if (!fc.utobj().isSelected(driver, pobj.addNewsItemFolder.get(1))) {
					fc.utobj().clickElement(driver, pobj.addNewsItemFolder.get(1));
				}
				fc.utobj().clickElement(driver, pobj.addBtn);
				fc.utobj().clickElement(driver, pobj.closeBtn);
				fc.utobj().switchFrameToDefault(driver);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to add Sub Folder Top Stories :: The Hub > News");

		}
	}
	
	public void addSubFolderInFolder(WebDriver driver, String folderName, String subFolderName, String subFolderSummary) throws Exception {

		try {
			TheHubNewsPage pobj = new TheHubNewsPage(driver);
			fc.utobj().clickElementWithActions(driver, fc.utobj().getElementByLinkText(driver, folderName));

			/*Actions builder = new Actions(driver);
			WebElement element=fc.utobj().getElement(driver, pobj.moreActions);
			builder.moveToElement(element)
					.click(element).build().perform();*/
			fc.utobj().clickElementWithActions(driver, pobj.moreActions);
			
			
			fc.utobj().getElement(driver, pobj.addSubFolder).click();

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.folderName, subFolderName);
			fc.utobj().sendKeys(driver, pobj.folderSummary, subFolderSummary);

			if (!fc.utobj().isSelected(driver, pobj.accessibleToAll.get(0))) {
				fc.utobj().clickElement(driver, pobj.accessibleToAll.get(0));
			}

			if (!fc.utobj().isSelected(driver, pobj.addNewsItemFolder.get(1))) {
				fc.utobj().clickElement(driver, pobj.addNewsItemFolder.get(1));
			}
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);
		} catch (Exception e) {
			fc.utobj().throwsException("Not able to add Sub Folder");
		}
	}

	private void addNewsItemInTopStoriesSubFolder(WebDriver driver, String newsItemTitle, String briefSummary,
			String editorText, String file, String mediaType, Map<String, String> dataSet) throws Exception {

		String testCaseId = "TC_Add_News_Item_In_Top_Stories_SubFolder";

		if (fc.utobj().validate(testCaseId)) {
			try {
				TheHubNewsPage pobj = new TheHubNewsPage(driver);

				fc.utobj().clickElement(driver, pobj.addNewsItemLink);

				fc.commonMethods().switch_cboxIframe_frameId(driver);

				fc.utobj().sendKeys(driver, pobj.newsItemTitle, newsItemTitle);
				fc.utobj().sendKeys(driver, pobj.briefSummary, briefSummary);

				if (mediaType.equalsIgnoreCase("Image")) {

					if (!fc.utobj().isSelected(driver, pobj.mediaType.get(0))) {
						fc.utobj().clickElement(driver, pobj.mediaType.get(0));
					}

					fc.utobj().sendKeys(driver, pobj.newsItemFile, file);

				} else if (mediaType.equalsIgnoreCase("Video")) {

					if (!fc.utobj().isSelected(driver, pobj.mediaType.get(1))) {
						fc.utobj().clickElement(driver, pobj.mediaType.get(1));
					}

					fc.utobj().sendKeys(driver, pobj.videoFile, file);

				} else if (mediaType.equalsIgnoreCase("Embed Code")) {

					if (!fc.utobj().isSelected(driver, pobj.mediaType.get(2))) {
						fc.utobj().clickElement(driver, pobj.mediaType.get(2));
					}

					fc.utobj().sendKeys(driver, pobj.embedCode, dataSet.get("embedCode"));
				}

				fc.utobj().switchFrameById(driver, "completeNews_ifr");

				Actions actions = new Actions(driver);
				actions.moveToElement(fc.utobj().getElement(driver, pobj.editorTextArea));
				actions.click();

				actions.sendKeys(editorText);
				fc.utobj().logReportMsg("Entered Text", editorText);
				actions.build().perform();
				fc.utobj().switchFrameToDefault(driver);

				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().clickElement(driver, pobj.addBtn);
				fc.utobj().sleep();
				fc.utobj().clickElement(driver, pobj.closeBtn);
				fc.utobj().switchFrameToDefault(driver);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to Add New Item In Top Storied Sub Folder :: The Hub  > News");

		}
	}

	@Test(groups = { "thehub","hub_news" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add News Item In Top Stories's Sub Folder At The Hub > News > Top Stories > Folder", testCaseId = "TC_187_Add_News_In_Folder")
	private void addNewsItemInFolder() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubNewsPage pobj = new TheHubNewsPage(driver);

			fc.utobj().printTestStep("Navigate To The Hub > News Page");
			fc.utobj().printTestStep("Add Sub Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			addSubFolderTopStories(driver, folderName, folderSummary);

			Actions builder = new Actions(driver);
			builder.moveToElement(fc.utobj().getElement(driver, pobj.moreActions))
					.click(fc.utobj().getElement(driver, pobj.moreActions));
			builder.perform();
			builder.moveToElement(fc.utobj().getElement(driver, pobj.topStoriesTab))
					.click(fc.utobj().getElement(driver, pobj.topStoriesTab));
			builder.perform();

			WebElement element1 = fc.utobj().getElementByXpath(driver, ".//a[.='" + folderName + "']");
			element1.click();

			fc.utobj().printTestStep("Add News Item In Top Stories's Sub Folder");
			String newsItemTitle = fc.utobj().generateTestData(dataSet.get("newsItemTitle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String editorText = fc.utobj().generateTestData(dataSet.get("editorText"));
			String file = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			String mediaType = "Image";
			addNewsItemInTopStoriesSubFolder(driver, newsItemTitle, briefSummary, editorText, file, mediaType, dataSet);

			fc.utobj().printTestStep("Verify The News Item In Sub Folder Of Top Stories");

			if (!fc.utobj().getElementByXpath(driver, ".//a[.='" + newsItemTitle + "']").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify add News Item Story");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//*[.='" + briefSummary + "']").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify add News Item Story's Summary");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//td/span[contains(text () , 'FranConnect Administrator')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify add News Item Story's Summary");
			}

			fc.utobj().printTestStep("Verify The News Item detail at pop up");
			fc.utobj().clickElementByJS(driver, fc.utobj().getElementByLinkText(driver, newsItemTitle));

			fc.commonMethods().switch_cboxIframe_frameId(driver);

			WebElement element = fc.utobj().getElementByXpath(driver,
					".//*[contains(text () ,'" + newsItemTitle + "')]");
			fc.utobj().moveToElement(driver, element);
			if (!element.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify News Item Title");
			}

			WebElement element3 = fc.utobj().getElementByXpath(driver,
					".//*[contains(text () ,'" + briefSummary + "')]");
			fc.utobj().moveToElement(driver, element3);
			if (!element3.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify News Item's Summary");
			}

			WebElement element4 = fc.utobj().getElementByXpath(driver, ".//*[contains(text () ,'" + editorText + "')]");
			fc.utobj().moveToElement(driver, element4);
			if (!element4.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify EditorText");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"thehub","hub_news" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Report of Top Stories's sub Folder At The Hub > News > Top Stories > Folder", testCaseId = "TC_188_Report_Stories")
	private void reportStoriesActionImgTopStories() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubNewsPage pobj = new TheHubNewsPage(driver);

			fc.utobj().printTestStep("Navigate To The Hub > News Page");
			fc.utobj().printTestStep("Add Sub Folder In Top Stories Page");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			addSubFolderTopStories(driver, folderName, folderSummary);

			Actions builder = new Actions(driver);
			builder.moveToElement(fc.utobj().getElement(driver, pobj.moreActions))
					.click(fc.utobj().getElement(driver, pobj.moreActions));
			builder.perform();
			builder.moveToElement(fc.utobj().getElement(driver, pobj.topStoriesTab))
					.click(fc.utobj().getElement(driver, pobj.topStoriesTab));
			builder.perform();

			WebElement element1 = fc.utobj().getElementByXpath(driver, ".//a[.='" + folderName + "']");
			element1.click();

			fc.utobj().printTestStep("Add News Item In Top Stories's Sub Folder");
			String newsItemTitle = fc.utobj().generateTestData(dataSet.get("newsItemTitle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String editorText = fc.utobj().generateTestData(dataSet.get("editorText"));
			// String file=fc.utobj().getFilePathFromTestData(config,
			// dataSet.get("uploadFile"));
			String file = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			String mediaType = "Image";
			addNewsItemInTopStoriesSubFolder(driver, newsItemTitle, briefSummary, editorText, file, mediaType, dataSet);

			fc.utobj().printTestStep("Verify Report of News Item");
			fc.utobj().actionImgOption(driver, newsItemTitle, "Report");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			WebElement element = fc.utobj().getElementByXpath(driver,
					".//td[contains(text () ,'" + newsItemTitle + "')]");
			fc.utobj().moveToElement(driver, element);
			if (!element.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify News Item Title");
			}

			WebElement element4 = fc.utobj().getElementByXpath(driver,
					".//td[contains(text () ,'" + briefSummary + "')]");
			fc.utobj().moveToElement(driver, element4);
			if (!element4.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify News Item's Summary");
			}

			WebElement element3 = fc.utobj().getElementByXpath(driver,
					".//td[contains(text () ,'FranConnect Administrator')]");
			fc.utobj().moveToElement(driver, element3);
			if (!element3.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify User[By Add]");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub","hub_news"  })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Modify News Item By Action Img Icon Option at The Hub > News > Folder", testCaseId = "TC_189_Modify_Stories")
	private void modifyNewsItemActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubNewsPage pobj = new TheHubNewsPage(driver);

			fc.utobj().printTestStep("Navigate To The Hub > News Page");
			fc.utobj().printTestStep("Add Sub Folder In Top Stories");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			addSubFolderTopStories(driver, folderName, folderSummary);

			Actions builder = new Actions(driver);
			builder.moveToElement(fc.utobj().getElement(driver, pobj.moreActions))
					.click(fc.utobj().getElement(driver, pobj.moreActions));
			builder.perform();
			builder.moveToElement(fc.utobj().getElement(driver, pobj.topStoriesTab))
					.click(fc.utobj().getElement(driver, pobj.topStoriesTab));
			builder.perform();

			WebElement element1 = fc.utobj().getElementByXpath(driver, ".//a[.='" + folderName + "']");
			element1.click();

			fc.utobj().printTestStep("Add News Item In Top Stories's Sub Folder");
			String newsItemTitle = fc.utobj().generateTestData(dataSet.get("newsItemTitle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String editorText = fc.utobj().generateTestData(dataSet.get("editorText"));
			String file = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			String mediaType = "Image";
			addNewsItemInTopStoriesSubFolder(driver, newsItemTitle, briefSummary, editorText, file, mediaType, dataSet);

			fc.utobj().printTestStep("Modify News Item");
			fc.utobj().actionImgOption(driver, newsItemTitle, "Modify");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			newsItemTitle = fc.utobj().generateTestData(dataSet.get("newsItemTitle"));
			briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String editorText1 = fc.utobj().generateTestData(dataSet.get("editorText"));

			fc.utobj().sendKeys(driver, pobj.newsItemTitle, newsItemTitle);
			fc.utobj().sendKeys(driver, pobj.briefSummary, briefSummary);
			fc.utobj().sendKeys(driver, pobj.newsItemFile, file);
			fc.utobj().switchFrameById(driver, "completeNews_ifr");

			Actions actions = new Actions(driver);
			actions.moveToElement(fc.utobj().getElement(driver, pobj.editorTextArea));
			actions.click();

			actions.sendKeys(editorText1);
			fc.utobj().logReportMsg("Entered Text", editorText1);
			actions.build().perform();
			fc.utobj().switchFrameToDefault(driver);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Modify News Item");
			if (!fc.utobj().getElementByXpath(driver, ".//a[.='" + newsItemTitle + "']").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify add News Item Story");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//*[.='" + briefSummary + "']").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify add News Item Story's Summary");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//td/span[contains(text () , 'FranConnect Administrator')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify add News Item Story's Summary");
			}

			fc.utobj().printTestStep("Verify The News Item details at pop up");

			fc.utobj().clickElementByJS(driver, fc.utobj().getElementByLinkText(driver, newsItemTitle));
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			WebElement element = fc.utobj().getElementByXpath(driver,
					".//td[contains(text () ,'" + newsItemTitle + "')]");
			fc.utobj().moveToElement(driver, element);
			if (!element.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify News Item Title");
			}

			WebElement element2 = fc.utobj().getElementByXpath(driver,
					".//td[contains(text () ,'" + briefSummary + "')]");
			fc.utobj().moveToElement(driver, element2);
			if (!element2.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify News Item's Summary");
			}

			String text = editorText.concat(editorText1);
			System.out.println(text);
			WebElement element3 = fc.utobj().getElementByXpath(driver, ".//*[contains(text () ,'" + text + "')]");
			fc.utobj().moveToElement(driver, element3);
			if (!element3.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify EditorText");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub" ,"hub_news"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Archive News Item Action Image Icon At The Hub > News > Top Stories > Folder ", testCaseId = "TC_190_Archive_Stories")
	private void archiveStoriesActionImgTopStories() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubNewsPage pobj = new TheHubNewsPage(driver);

			fc.utobj().printTestStep("Navigate To The Hub > News Page");
			fc.utobj().printTestStep("Add Sub Folder In Top Stories");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			addSubFolderTopStories(driver, folderName, folderSummary);

			Actions builder = new Actions(driver);
			builder.moveToElement(fc.utobj().getElement(driver, pobj.moreActions))
					.click(fc.utobj().getElement(driver, pobj.moreActions));
			builder.perform();
			builder.moveToElement(fc.utobj().getElement(driver, pobj.topStoriesTab))
					.click(fc.utobj().getElement(driver, pobj.topStoriesTab));
			builder.perform();

			WebElement element1 = fc.utobj().getElementByXpath(driver, ".//a[.='" + folderName + "']");
			element1.click();

			fc.utobj().printTestStep("Add News Item In Top Stories Sub Folder");
			String newsItemTitle = fc.utobj().generateTestData(dataSet.get("newsItemTitle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String editorText = fc.utobj().generateTestData(dataSet.get("editorText"));
			String file = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			String mediaType = "Image";
			addNewsItemInTopStoriesSubFolder(driver, newsItemTitle, briefSummary, editorText, file, mediaType, dataSet);

			fc.utobj().printTestStep("Archive News Item");
			Actions builder1 = new Actions(driver);
			builder1.moveToElement(fc.utobj().getElement(driver, pobj.moreActions))
					.click(fc.utobj().getElement(driver, pobj.moreActions));
			builder1.perform();
			builder1.moveToElement(fc.utobj().getElement(driver, pobj.topStoriesTab))
					.click(fc.utobj().getElement(driver, pobj.topStoriesTab));
			builder1.perform();

			WebElement element12 = fc.utobj().getElementByXpath(driver, ".//a[.='" + folderName + "']");
			//element12.click();
			fc.utobj().clickElementWithActions(driver, element12);
			fc.utobj().sleep();
			fc.utobj().actionImgOption(driver, newsItemTitle, "Archive");
			fc.utobj().acceptAlertBox(driver);

			boolean isTextPresent = fc.utobj().assertPageSource(driver, newsItemTitle);
			if (isTextPresent == true) {
				fc.utobj().throwsException("was not able to Archive News Item :: Verification At News Item Page");
			}

			fc.utobj().printTestStep("Verify Archive News Item At Archived News Item Tab");
			fc.utobj().clickElement(driver, pobj.archivedNewsTab);

			if (!fc.utobj().getElementByXpath(driver, ".//a[.='" + newsItemTitle + "']").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify add News Item Story");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//*[.='" + briefSummary + "']").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify add News Item Story's Summary");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//td/span[contains(text () , 'FranConnect Administrator')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify add News Item Story's Summary");
			}

			fc.utobj().printTestStep("Verify The News Item Details At Pop up");

			WebElement elementCLik = fc.utobj().getElementByLinkText(driver, newsItemTitle);
			fc.utobj().clickElementByJS(driver, elementCLik);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			WebElement element = fc.utobj().getElementByXpath(driver,
					".//td[contains(text () ,'" + newsItemTitle + "')]");
			fc.utobj().moveToElement(driver, element);
			if (!element.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify News Item Title");
			}

			WebElement element2 = fc.utobj().getElementByXpath(driver,
					".//td[contains(text () ,'" + briefSummary + "')]");
			fc.utobj().moveToElement(driver, element2);
			if (!element2.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify News Item's Summary");
			}

			WebElement element3 = fc.utobj().getElementByXpath(driver, ".//*[contains(text () ,'" + editorText + "')]");
			fc.utobj().moveToElement(driver, element3);
			if (!element3.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify EditorText");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"thehub","hub_news"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Delete Story By Action Image Icon at The Hub > News > Folder ", testCaseId = "TC_191_Delete_Stories")
	private void deleteStoriesActionImgTopStories() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubNewsPage pobj = new TheHubNewsPage(driver);

			fc.utobj().printTestStep("Navigate To The Hub > News Page");
			fc.utobj().printTestStep("Add Sub Folder In Top Stories");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			addSubFolderTopStories(driver, folderName, folderSummary);

			Actions builder = new Actions(driver);
			builder.moveToElement(fc.utobj().getElement(driver, pobj.moreActions))
					.click(fc.utobj().getElement(driver, pobj.moreActions));
			builder.perform();
			builder.moveToElement(fc.utobj().getElement(driver, pobj.topStoriesTab))
					.click(fc.utobj().getElement(driver, pobj.topStoriesTab));
			builder.perform();

			WebElement element1 = fc.utobj().getElementByXpath(driver, ".//a[.='" + folderName + "']");
			element1.click();

			fc.utobj().printTestStep("Add News Item In Top Stories Sub Folder");
			String newsItemTitle = fc.utobj().generateTestData(dataSet.get("newsItemTitle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String editorText = fc.utobj().generateTestData(dataSet.get("editorText"));
			// String file=fc.utobj().getFilePathFromTestData(config,
			// dataSet.get("uploadFile"));
			String file = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			String mediaType = "Image";
			addNewsItemInTopStoriesSubFolder(driver, newsItemTitle, briefSummary, editorText, file, mediaType, dataSet);

			fc.utobj().printTestStep("Verify The Delete News Item");
			fc.utobj().actionImgOption(driver, newsItemTitle, "Delete");
			fc.utobj().acceptAlertBox(driver);

			boolean isTextPresent = fc.utobj().assertPageSource(driver, newsItemTitle);

			if (isTextPresent == true) {
				fc.utobj().throwsException("was not able to Delete News Item :: Verification At News Item Page");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups ={"thehub","hub_news" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Modify Sub Folder of Top Stories At The Hub > News > Top Stories", testCaseId = "TC_192_Modify_Sub_Folder")
	private void modifySubFolderTopStories() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubNewsPage pobj = new TheHubNewsPage(driver);

			fc.utobj().printTestStep("Navigate To The Hub > News Page");
			fc.utobj().printTestStep("Add Sub Folder In Top Stories");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			addSubFolderTopStories(driver, folderName, folderSummary);
			
			//fc.utobj().clickElementWithActions(driver, pobj.topStoriesTab);
			fc.utobj().moveToElementThroughAction(driver, pobj.topStoriesTab);
			fc.utobj().clickElementWithActions(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + folderName + "']"));
			
			fc.utobj().printTestStep("Modify Folder");
			fc.utobj().clickElement(driver, pobj.modifyFolder);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			fc.utobj().sendKeys(driver, pobj.folderName, folderName);
			fc.utobj().sendKeys(driver, pobj.folderSummary, folderSummary);

			if (!fc.utobj().isSelected(driver, pobj.accessibleToAll.get(0))) {
				fc.utobj().clickElement(driver, pobj.accessibleToAll.get(0));
			}

			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Modify Folder");
			fc.utobj().clickElementWithActions(driver, pobj.topStoriesTab);
			fc.utobj().clickElementWithActions(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + folderName + "']"));

			fc.utobj().isTextDisplayed(driver, folderName, "was not able to verify Folder Name after Modify");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"thehub","hub_news" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Delete Sub Folder of Top Stories At The Hub > News > Top Stories", testCaseId = "TC_193_Delete_Sub_Folder")
	private void deleteSubFolderTopStories() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubNewsPage pobj = new TheHubNewsPage(driver);

			fc.utobj().printTestStep("Navigate To The Hub > News Page");
			fc.utobj().printTestStep("Add Sub Folder In Top Stories");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			addSubFolderTopStories(driver, folderName, folderSummary);

			Actions builder = new Actions(driver);
			builder.moveToElement(fc.utobj().getElement(driver, pobj.moreActions))
					.click(fc.utobj().getElement(driver, pobj.moreActions));
			builder.perform();
			builder.moveToElement(fc.utobj().getElement(driver, pobj.topStoriesTab))
					.click(fc.utobj().getElement(driver, pobj.topStoriesTab));
			builder.perform();

			WebElement element1 = fc.utobj().getElementByXpath(driver, ".//a[.='" + folderName + "']");
			element1.click();

			fc.utobj().printTestStep("Delete Sub Folder");
			fc.utobj().clickElement(driver, pobj.deleteTab);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.okBtn);
			fc.utobj().switchFrameToDefault(driver);

			builder.moveToElement(fc.utobj().getElement(driver, pobj.moreActions))
					.click(fc.utobj().getElement(driver, pobj.moreActions));
			builder.perform();
			builder.moveToElement(fc.utobj().getElement(driver, pobj.topStoriesTab))
					.click(fc.utobj().getElement(driver, pobj.topStoriesTab));
			builder.perform();

			fc.utobj().printTestStep("Verify The Delete Sub Folder");

			List<WebElement> listElement = driver
					.findElements(By.xpath(".//li[@class='haschild hide']/ul[contains(@id , 'folderList')]/li/a"));

			for (int i = 0; i < listElement.size(); i++) {

				String text = listElement.get(1).getText().trim();

				if (text.equalsIgnoreCase(folderName)) {
					fc.utobj().throwsException("was not able to delete sub folder of Top Stories");
					break;
				}
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"thehub","hub_news" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Archive Sub Folder of Top Stories At The Hub > News > Top Stories > Folder", testCaseId = "TC_194_Archive_Sub_Folder")
	private void archiveSubFolderTopStories() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubNewsPage pobj = new TheHubNewsPage(driver);

			fc.utobj().printTestStep("Navigate To The Hub > News Page");
			fc.utobj().printTestStep("Add Sub Folder In Top Stories");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			addSubFolderTopStories(driver, folderName, folderSummary);

			Actions builder = new Actions(driver);
			builder.moveToElement(fc.utobj().getElement(driver, pobj.moreActions))
					.click(fc.utobj().getElement(driver, pobj.moreActions));
			builder.perform();
			builder.moveToElement(fc.utobj().getElement(driver, pobj.topStoriesTab))
					.click(fc.utobj().getElement(driver, pobj.topStoriesTab));
			builder.perform();

			WebElement element1 = fc.utobj().getElementByXpath(driver, ".//a[.='" + folderName + "']");
			element1.click();

			fc.utobj().printTestStep("Archive Sub Folder");

			builder.moveToElement(fc.utobj().getElement(driver, pobj.moreActions))
					.click(fc.utobj().getElement(driver, pobj.moreActions));
			builder.perform();

			fc.utobj().getElement(driver, pobj.archiveLink).click();

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.okBtn);
			fc.utobj().switchFrameToDefault(driver);

			builder.moveToElement(fc.utobj().getElement(driver, pobj.archivedNewsTab))
					.click(fc.utobj().getElement(driver, pobj.archivedNewsTab));
			builder.perform();
			WebElement element = fc.utobj().getElementByXpath(driver, ".//a[.='" + folderName + "']");
			fc.utobj().logReport("Clicking on Element", element);
			element.click();

			fc.utobj().printTestStep("Verify Archive Sub Folder");

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + folderName + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to archive folder");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"thehub","hub_news" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add Sub Folder In Folder of Top Stories At The Hub > News > Top Stories > Folder", testCaseId = "TC_195_Add_Sub_Folder")
	private void addSubFolderInFolderTopStories() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubNewsPage pobj = new TheHubNewsPage(driver);

			fc.utobj().printTestStep("Navigate To The Hub > News Page");
			fc.utobj().printTestStep("Add Sub Folder In Top Stories");
			String folderName1 = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			addSubFolderTopStories(driver, folderName1, folderSummary);

			fc.utobj().refresh(driver);
			Actions builder = new Actions(driver);
			builder.moveToElement(fc.utobj().getElement(driver, pobj.moreActions))
					.click(fc.utobj().getElement(driver, pobj.moreActions));
			builder.perform();
			builder.moveToElement(fc.utobj().getElement(driver, pobj.topStoriesTab))
					.click(fc.utobj().getElement(driver, pobj.topStoriesTab));
			builder.perform();

			WebElement element1 = fc.utobj().getElementByXpath(driver, ".//a[.='" + folderName1 + "']");
			element1.click();

			fc.utobj().printTestStep("Add Sub Folder");

			builder.moveToElement(fc.utobj().getElement(driver, pobj.moreActions))
					.click(fc.utobj().getElement(driver, pobj.moreActions));
			builder.perform();
			fc.utobj().getElement(driver, pobj.addSubFolder).click();

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String folderName2 = fc.utobj().generateTestData(dataSet.get("folderName"));
			folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			fc.utobj().sendKeys(driver, pobj.folderName, folderName2);
			fc.utobj().sendKeys(driver, pobj.folderSummary, folderSummary);

			if (!fc.utobj().isSelected(driver, pobj.accessibleToAll.get(0))) {
				fc.utobj().clickElement(driver, pobj.accessibleToAll.get(0));
			}

			if (!fc.utobj().isSelected(driver, pobj.addNewsItemFolder.get(1))) {
				fc.utobj().clickElement(driver, pobj.addNewsItemFolder.get(1));
			}
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			builder.moveToElement(fc.utobj().getElement(driver, pobj.topStoriesTab))
					.click(fc.utobj().getElement(driver, pobj.topStoriesTab));
			builder.perform();

			fc.utobj().printTestStep("Verify Sub Folder");

			WebElement element2 = fc.utobj().getElementByXpath(driver,
					".//*[@id='siteMainTable']//a[contains(text () ,'" + folderName1 + "')]");
			element2.click();

			if (!fc.utobj().getElementByXpath(driver, ".//a[.='" + folderName2 + "']").isDisplayed()) {
				fc.utobj().throwsException("was not able to add Sub folder");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"thehub","hub_news" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add News Item In Folder At The Hub > News > Folder ", testCaseId = "TC_196_Add_News_Item")
	private void addNewsItemInFolder01() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminTheHubNewsPage pobj = new AdminTheHubNewsPage(driver);
			AdminTheHubNewsPageTest newsPage = new AdminTheHubNewsPageTest();

			fc.utobj().printTestStep("Navigate To Admin > The Hub > News Page");
			
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String accessTo = "All";
			
			News news=new News();
			news.setFolderName(folderName);
			news.setFolderSummary(folderSummary);
			news.setAccessTo(accessTo);
			
			fc.utobj().printTestStep("Add Folder");
			newsPage.addFolder(driver, news);

			fc.utobj().printTestStep("Navigate To The Hub > News Page");
			fc.hub().hub_common().theHubNewsSubModule(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + folderName + "')]"));

			fc.utobj().printTestStep("Add News Item In Folder");

			fc.utobj().clickElement(driver, pobj.addNewsItemLink);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String newsItemTitle = fc.utobj().generateTestData(dataSet.get("newsItemTitle"));
			fc.utobj().sendKeys(driver, pobj.newsItemTitle, newsItemTitle);
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			fc.utobj().sendKeys(driver, pobj.briefSummary, briefSummary);

			String file = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			fc.utobj().sendKeys(driver, pobj.newsItemFile, file);

			String editorText = "'single quote'";
			fc.utobj().switchFrameById(driver, "completeNews_ifr");

			Actions actions = new Actions(driver);
			actions.moveToElement(fc.utobj().getElement(driver, pobj.editorTextArea));
			actions.click();
			actions.sendKeys(editorText);
			fc.utobj().logReportMsg("Entered Text", editorText);
			actions.build().perform();
			fc.utobj().switchFrameToDefault(driver);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify News Item In Folder");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + folderName + "')]"));

			if (!fc.utobj().getElementByXpath(driver, ".//a[.='" + newsItemTitle + "']").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify add News Item Story");
			}

			fc.utobj().printTestStep("Verify Editor text having single quote");
			//fc.utobj().clickElement(driver, ".//*[@id='news']//a[contains(text() , '"+newsItemTitle+"')]");
			//fc.utobj().clickLink(driver, newsItemTitle);
			fc.utobj().clickElementByJS(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='news']//a[contains(text() , '"+newsItemTitle+"')]"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			boolean isassertPageSource=fc.utobj().assertPageSource(driver, editorText);
			if (!isassertPageSource) {
				fc.utobj().throwsException("Single quote data not verified");
			}
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void addNewsItems(WebDriver driver, String newsItemTitle, String briefSummary, String file,
			String editorText) throws Exception {

		String testCaseId = "TC_Add_News_Items_The_Hub";

		if (fc.utobj().validate(testCaseId)) {
			try {
				TheHubNewsPage pobj = new TheHubNewsPage(driver);
				fc.utobj().clickElement(driver, pobj.addNewsItemLink);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().sendKeys(driver, pobj.newsItemTitle, newsItemTitle);
				fc.utobj().sendKeys(driver, pobj.briefSummary, briefSummary);
				fc.utobj().sendKeys(driver, pobj.newsItemFile, file);
				fc.utobj().switchFrameById(driver, "completeNews_ifr");

				Actions actions = new Actions(driver);
				actions.moveToElement(fc.utobj().getElement(driver, pobj.editorTextArea));
				actions.click();

				actions.sendKeys(editorText);
				fc.utobj().logReportMsg("Entered Text", editorText);
				actions.build().perform();
				fc.utobj().switchFrameToDefault(driver);

				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().clickElement(driver, pobj.addBtn);
				fc.utobj().sleep();
				fc.utobj().clickElement(driver, pobj.closeBtn);
				fc.utobj().switchFrameToDefault(driver);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to add New Items :: The Hub > News");

		}
	}

	@Test(groups = {"thehub","hub_news"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Modify Folder At The Hub > News > Folder", testCaseId = "TC_197_Modify_Folder")
	private void modifyFolder01() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminTheHubNewsPage pobj = new AdminTheHubNewsPage(driver);
			AdminTheHubNewsPageTest newsPage = new AdminTheHubNewsPageTest();

			fc.utobj().printTestStep("Navigate To Admin > The Hub > News Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String accessTo = "All";
			
			News news=new News();
			news.setFolderName(folderName);
			news.setFolderSummary(folderSummary);
			news.setAccessTo(accessTo);
			
			newsPage.addFolder(driver, news);

			fc.utobj().printTestStep("Navigate To The Hub > News Page");
			fc.hub().hub_common().theHubNewsSubModule(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + folderName + "')]"));

			fc.utobj().printTestStep("Modify Folder");
			fc.utobj().clickElement(driver, pobj.modifyFolder);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			fc.utobj().sendKeys(driver, pobj.folderName, folderName);
			fc.utobj().sendKeys(driver, pobj.folderSummary, folderSummary);
			if (!fc.utobj().isSelected(driver, pobj.accessibleToAll.get(0))) {
				fc.utobj().clickElement(driver, pobj.accessibleToAll.get(0));
			}
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Modify Folder");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + folderName + "')]"));

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + folderName + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to modify Folder Name");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"thehub","hub_news" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Delete Folder At The Hub > News > Folder", testCaseId = "TC_198_Delete_Folder")
	private void deleteFolder01() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminTheHubNewsPage pobj = new AdminTheHubNewsPage(driver);
			AdminTheHubNewsPageTest newsPage = new AdminTheHubNewsPageTest();

			fc.utobj().printTestStep("Navigate To Admin > The Hub > News Page");
			
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String accessTo = "All";
			
			News news=new News();
			news.setFolderName(folderName);
			news.setFolderSummary(folderSummary);
			news.setAccessTo(accessTo);
			
			fc.utobj().printTestStep("Add Folder");
			newsPage.addFolder(driver, news);

			fc.utobj().printTestStep("Navigate To The Hub > News Page");
			fc.hub().hub_common().theHubNewsSubModule(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + folderName + "')]"));

			fc.utobj().printTestStep("Delete The Folder");
			fc.utobj().clickElement(driver, pobj.deleteFolder);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.okayBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Delete The Folder");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, folderName);
			if (isTextPresent == true) {
				fc.utobj().throwsException("was not able to delete folder");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"thehub","hub_news"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Archive Folder At The Hub > News > Folder", testCaseId = "TC_199_Archive_Folder")
	private void archiveFolder01() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminTheHubNewsPage pobj = new AdminTheHubNewsPage(driver);
			AdminTheHubNewsPageTest newsPage = new AdminTheHubNewsPageTest();

			fc.utobj().printTestStep("Navigate To Admin > The Hub > News Page");
			
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String accessTo = "All";
			
			News news=new News();
			news.setFolderName(folderName);
			news.setFolderSummary(folderSummary);
			news.setAccessTo(accessTo);
			
			fc.utobj().printTestStep("Add Folder");
			newsPage.addFolder(driver, news);

			fc.utobj().printTestStep("Navigate To The Hub > News Page");
			fc.hub().hub_common().theHubNewsSubModule(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + folderName + "')]"));

			fc.utobj().printTestStep("Archive Folder");

			Actions builder = new Actions(driver);
			builder.moveToElement(fc.utobj().getElement(driver, pobj.moreActions))
					.click(fc.utobj().getElement(driver, pobj.moreActions));
			builder.perform();
			fc.utobj().getElement(driver, pobj.archiveLink).click();

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.okayBtn);
			fc.utobj().switchFrameToDefault(driver);

			builder.moveToElement(fc.utobj().getElement(driver, pobj.archivedNewsTab))
					.click(fc.utobj().getElement(driver, pobj.archivedNewsTab));
			builder.perform();
			WebElement element = fc.utobj().getElementByXpath(driver, ".//a[.='" + folderName + "']");
			fc.utobj().logReport("Clicking on Element", element);
			element.click();

			fc.utobj().printTestStep("Verify Archive Folder At Archive Tab");
			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + folderName + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to archive folder");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub" ,"hub_news"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add Sub Folder In Folder At The Hub > News > Folder", testCaseId = "TC_200_Add_Sub_Folder")
	private void addSubFolder01() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminTheHubNewsPage pobj = new AdminTheHubNewsPage(driver);
			AdminTheHubNewsPageTest newsPage = new AdminTheHubNewsPageTest();

			fc.utobj().printTestStep("Navigate To Admin > The Hub > News Page");
			
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String accessTo = "All";
			
			News news=new News();
			news.setFolderName(folderName);
			news.setFolderSummary(folderSummary);
			news.setAccessTo(accessTo);
			
			fc.utobj().printTestStep("Add Folder");
			newsPage.addFolder(driver, news);

			fc.utobj().printTestStep("Navigate To The Hub > News Page");
			fc.hub().hub_common().theHubNewsSubModule(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + folderName + "')]"));

			fc.utobj().printTestStep("Add Sub Folder");
			Actions builder = new Actions(driver);

			builder.moveToElement(fc.utobj().getElement(driver, pobj.moreActions))
					.click(fc.utobj().getElement(driver, pobj.moreActions));
			builder.perform();

			fc.utobj().getElement(driver, pobj.addSubFolder).click();

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String subFolderName = fc.utobj().generateTestData(dataSet.get("subFolderName"));
			String subFolderSummary = fc.utobj().generateTestData(dataSet.get("subFolderSummary"));
			fc.utobj().sendKeys(driver, pobj.folderName, subFolderName);
			fc.utobj().sendKeys(driver, pobj.folderSummary, subFolderSummary);
			if (!fc.utobj().isSelected(driver, pobj.accessibleToAll.get(0))) {
				fc.utobj().clickElement(driver, pobj.accessibleToAll.get(0));
			}
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Add Sub Folder In Folder");
			WebElement element = fc.utobj().getElementByXpath(driver, ".//a[.='" + folderName + "']");
			fc.utobj().logReport("Move To Element", element);
			builder.moveToElement(element).click(element);
			builder.perform();

			WebElement element1 = fc.utobj().getElementByXpath(driver, ".//a[.='" + subFolderName + "']");
			fc.utobj().logReport("Clicking on Element", element1);
			element1.click();

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + subFolderName + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to add Sub folder");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"thehub","hub_news"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Report Of News Item At The Hub > News > Folder", testCaseId = "TC_201_Report_News_Item")
	public void reportNewsItemActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminTheHubNewsPage pobj = new AdminTheHubNewsPage(driver);
			AdminTheHubNewsPageTest newsPage = new AdminTheHubNewsPageTest();

			fc.utobj().printTestStep("Navigate To Admin > The Hub > News Page");
			
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String accessTo = "All";
			
			News news=new News();
			news.setFolderName(folderName);
			news.setFolderSummary(folderSummary);
			news.setAccessTo(accessTo);
			
			fc.utobj().printTestStep("Add Folder");
			newsPage.addFolder(driver, news);

			fc.utobj().printTestStep("Navigate To The Hub > News Page");
			fc.hub().hub_common().theHubNewsSubModule(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + folderName + "')]"));

			fc.utobj().printTestStep("Add News Item");
			String newsItemTitle = fc.utobj().generateTestData(dataSet.get("newsItemTitle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String editorText = fc.utobj().generateTestData(dataSet.get("editorText"));
			String file = FranconnectUtil.config.get("inputDirectory") + "\\testData\\document\\"
					+ dataSet.get("uploadFile");
			addNewsItems(driver, newsItemTitle, briefSummary, file, editorText);

			fc.utobj().printTestStep("Report Of News Item");
			fc.utobj().actionImgOption(driver, newsItemTitle, "Report");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().printTestStep("Verify News Item Report");
			WebElement element = fc.utobj().getElementByXpath(driver,
					".//td[contains(text () ,'" + newsItemTitle + "')]");
			fc.utobj().moveToElement(driver, element);
			if (!element.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify News Item Title");
			}

			WebElement element1 = fc.utobj().getElementByXpath(driver,
					".//td[contains(text () ,'" + briefSummary + "')]");
			fc.utobj().moveToElement(driver, element1);
			if (!element1.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify News Item's Summary");
			}

			WebElement element3 = fc.utobj().getElementByXpath(driver,
					".//td[contains(text () ,'FranConnect Administrator')]");
			fc.utobj().moveToElement(driver, element3);
			if (!element3.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify User[By Add]");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub" ,"hub_news"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Modify News Item By Action Image Icon At The Hub > News > Folder", testCaseId = "TC_202_Modify_News_Item")
	private void modifyNewsItemActionImg01() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminTheHubNewsPage pobj = new AdminTheHubNewsPage(driver);
			AdminTheHubNewsPageTest newsPage = new AdminTheHubNewsPageTest();

			fc.utobj().printTestStep("Navigate To Admin > The Hub > News Page");
			
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String accessTo = "All";
			
			News news=new News();
			news.setFolderName(folderName);
			news.setFolderSummary(folderSummary);
			news.setAccessTo(accessTo);
			
			fc.utobj().printTestStep("Add Folder");
			newsPage.addFolder(driver,news);

			fc.utobj().printTestStep("Navigate To The Hub > News Page");
			fc.hub().hub_common().theHubNewsSubModule(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + folderName + "')]"));

			fc.utobj().printTestStep("Add News Item In Folder");
			String newsItemTitle = fc.utobj().generateTestData(dataSet.get("newsItemTitle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String editorText = fc.utobj().generateTestData(dataSet.get("editorText"));
			String file = FranconnectUtil.config.get("inputDirectory") + "\\testData\\document\\"
					+ dataSet.get("uploadFile");

			addNewsItems(driver, newsItemTitle, briefSummary, file, editorText);

			fc.utobj().printTestStep("Modify News Item");
			fc.utobj().actionImgOption(driver, newsItemTitle, "Modify");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			newsItemTitle = fc.utobj().generateTestData(dataSet.get("newsItemTitle"));
			briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String editorText1 = fc.utobj().generateTestData(dataSet.get("editorText"));

			fc.utobj().sendKeys(driver, pobj.newsItemTitle, newsItemTitle);
			fc.utobj().sendKeys(driver, pobj.briefSummary, briefSummary);
			fc.utobj().sendKeys(driver, pobj.newsItemFile, file);
			fc.utobj().switchFrameById(driver, "completeNews_ifr");

			Actions actions = new Actions(driver);
			actions.moveToElement(fc.utobj().getElement(driver, pobj.editorTextArea));
			actions.click();
			actions.sendKeys(editorText1);
			fc.utobj().logReportMsg("Entered Text", editorText1);
			actions.build().perform();
			fc.utobj().switchFrameToDefault(driver);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Modify News Item");
			if (!fc.utobj().getElementByXpath(driver, ".//a[.='" + newsItemTitle + "']").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify add News Item Story");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//*[.='" + briefSummary + "']").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify add News Item Story's Summary");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//td/span[contains(text () , 'FranConnect Administrator')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify add News Item Story's Summary");
			}

			fc.utobj().printTestStep("Verify The News Item At Pop up");

			fc.utobj().clickElementByJS(driver, fc.utobj().getElementByLinkText(driver, newsItemTitle));

			fc.commonMethods().switch_cboxIframe_frameId(driver);

			WebElement element = fc.utobj().getElementByXpath(driver,
					".//td[contains(text () ,'" + newsItemTitle + "')]");
			fc.utobj().moveToElement(driver, element);
			if (!element.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify News Item Title");
			}

			WebElement element1 = fc.utobj().getElementByXpath(driver,
					".//td[contains(text () ,'" + briefSummary + "')]");
			fc.utobj().moveToElement(driver, element1);
			if (!element1.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify News Item's Summary");
			}

			String text = editorText.concat(editorText1);
			System.out.println(text);
			WebElement element3 = fc.utobj().getElementByXpath(driver, ".//*[contains(text () ,'" + text + "')]");
			fc.utobj().moveToElement(driver, element3);
			if (!element3.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify EditorText");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub" , "hub0530","hub_news"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Archive News Item By Action Img Icon at The Hub > News > Folder", testCaseId = "TC_203_Archive_News_Item")
	private void archiveNewsItemActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminTheHubNewsPage pobj = new AdminTheHubNewsPage(driver);
			AdminTheHubNewsPageTest newsPage = new AdminTheHubNewsPageTest();

			fc.utobj().printTestStep("Navigate To Admin > The Hub > News Page");
			
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String accessTo = "All";
			
			News news=new News();
			news.setFolderName(folderName);
			news.setFolderSummary(folderSummary);
			news.setAccessTo(accessTo);
			
			fc.utobj().printTestStep("Add Folder");
			newsPage.addFolder(driver, news);

			fc.utobj().printTestStep("Navigate To The Hub > News Page");
			fc.hub().hub_common().theHubNewsSubModule(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + folderName + "')]"));

			fc.utobj().printTestStep("Add News Item In Folder");
			String newsItemTitle = fc.utobj().generateTestData(dataSet.get("newsItemTitle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String editorText = fc.utobj().generateTestData(dataSet.get("editorText"));
			String file = FranconnectUtil.config.get("inputDirectory") + "\\testData\\document\\"
					+ dataSet.get("uploadFile");

			addNewsItems(driver, newsItemTitle, briefSummary, file, editorText);

			fc.utobj().printTestStep("Archive News Item");
			fc.utobj().actionImgOption(driver, newsItemTitle, "Archive");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Archive News Item");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, newsItemTitle);
			if (isTextPresent == true) {
				fc.utobj().throwsException("was not able to Archive News Item :: Verification At News Item Page");
			}

			fc.utobj().printTestStep("Navigate To Archived News Tab");
			fc.utobj().printTestStep("Verify The Archived News Item Details");
			fc.utobj().clickElement(driver, pobj.archivedNewsTab);

			if (!fc.utobj().getElementByXpath(driver, ".//a[.='" + newsItemTitle + "']").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify add News Item Story");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//*[.='" + briefSummary + "']").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify add News Item Story's Summary");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//td/span[contains(text () , 'FranConnect Administrator')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify add News Item Story's Summary");
			}

			fc.utobj().printTestStep("Verify The Archived News Item Details At Pop Up");

			WebElement elementCLik = fc.utobj().getElementByLinkText(driver, newsItemTitle);

			fc.utobj().clickElementByJS(driver, elementCLik);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			WebElement element = fc.utobj().getElementByXpath(driver,
					".//td[contains(text () ,'" + newsItemTitle + "')]");
			fc.utobj().moveToElement(driver, element);
			if (!element.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify News Item Title");
			}

			WebElement element1 = fc.utobj().getElementByXpath(driver,
					".//td[contains(text () ,'" + briefSummary + "')]");
			fc.utobj().moveToElement(driver, element1);
			if (!element1.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify News Item's Summary");
			}

			WebElement element3 = fc.utobj().getElementByXpath(driver, ".//*[contains(text () ,'" + editorText + "')]");
			fc.utobj().moveToElement(driver, element3);
			if (!element3.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify EditorText");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"thehub","hub_news"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Delete News Item By Action Image Icon At The Hub > News > Folder", testCaseId = "TC_204_Delete_News_Item")
	private void deleteNewsItemActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminTheHubNewsPageTest newsPage = new AdminTheHubNewsPageTest();

			fc.utobj().printTestStep("Navigate To Admin > The Hub > News Page");
			
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String accessTo = "All";
			
			News news=new News();
			news.setFolderName(folderName);
			news.setFolderSummary(folderSummary);
			news.setAccessTo(accessTo);
			
			fc.utobj().printTestStep("Add Folder");
			newsPage.addFolder(driver, news);

			fc.utobj().printTestStep("Navigate To The Hub > News Page");
			fc.hub().hub_common().theHubNewsSubModule(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + folderName + "')]"));

			fc.utobj().printTestStep("Add News Item in folder");
			String newsItemTitle = fc.utobj().generateTestData(dataSet.get("newsItemTitle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String editorText = fc.utobj().generateTestData(dataSet.get("editorText"));
			String file = FranconnectUtil.config.get("inputDirectory") + "\\testData\\document\\"
					+ dataSet.get("uploadFile");
			addNewsItems(driver, newsItemTitle, briefSummary, file, editorText);

			fc.utobj().printTestStep("Delete The News Item");
			fc.utobj().actionImgOption(driver, newsItemTitle, "Delete");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Delete News Item");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, newsItemTitle);

			if (isTextPresent == true) {
				fc.utobj().throwsException("was not able to Delete News Item :: Verification At News Item Page");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"thehub","hub_news"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Report of News Item At The Hub > News > Archive News", testCaseId = "TC_205_Report_News_Item")
	private void reportNewsItemActionImgArchivedNews() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminTheHubNewsPage pobj = new AdminTheHubNewsPage(driver);
			AdminTheHubNewsPageTest newsPage = new AdminTheHubNewsPageTest();

			fc.utobj().printTestStep("Navigate To Admin > The Hub > News Page");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String accessTo = "All";
			
			
			News news=new News();
			news.setFolderName(folderName);
			news.setFolderSummary(folderSummary);
			news.setAccessTo(accessTo);
			
			newsPage.addFolder(driver, news);

			fc.utobj().printTestStep("Navigate To The Hub > News Page");
			fc.hub().hub_common().theHubNewsSubModule(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + folderName + "')]"));

			fc.utobj().printTestStep("Add News Item In Folder");
			String newsItemTitle = fc.utobj().generateTestData(dataSet.get("newsItemTitle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String editorText = fc.utobj().generateTestData(dataSet.get("editorText"));
			String file = FranconnectUtil.config.get("inputDirectory") + "\\testData\\document\\"
					+ dataSet.get("uploadFile");
			addNewsItems(driver, newsItemTitle, briefSummary, file, editorText);

			fc.utobj().printTestStep("Archive News Item");
			fc.utobj().actionImgOption(driver, newsItemTitle, "Archive");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Navigate To Archived News Item Tab");
			fc.utobj().clickElement(driver, pobj.archivedNewsTab);

			fc.utobj().printTestStep("Verify The Report of Archive News Item");
			fc.utobj().actionImgOption(driver, newsItemTitle, "Report");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			WebElement element = fc.utobj().getElementByXpath(driver,
					".//*[contains(text () ,'" + newsItemTitle + "')]");
			fc.utobj().moveToElement(driver, element);
			if (!element.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify News Item Title");
			}

			WebElement element1 = fc.utobj().getElementByXpath(driver,
					".//*[contains(text () ,'" + briefSummary + "')]");
			fc.utobj().moveToElement(driver, element1);
			if (!element1.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify News Item's Summary");
			}

			WebElement element3 = fc.utobj().getElementByXpath(driver,
					".//*[contains(text () ,'FranConnect Administrator')]");
			fc.utobj().moveToElement(driver, element3);
			if (!element3.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify User[By Add]");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"thehub","hub_news"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Delete News Item By Action Image Icon at The Hub > News > Archive News", testCaseId = "TC_206_Delete_News_Item")
	private void deleteNewsItemActionImgArchivedNews() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminTheHubNewsPage pobj = new AdminTheHubNewsPage(driver);
			AdminTheHubNewsPageTest newsPage = new AdminTheHubNewsPageTest();

			fc.utobj().printTestStep("Navigate To Admin > The Hub > News Page");
			
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String accessTo = "All";
			
			News news=new News();
			news.setFolderName(folderName);
			news.setFolderSummary(folderSummary);
			news.setAccessTo(accessTo);
			
			fc.utobj().printTestStep("Add Folder");
			newsPage.addFolder(driver, news);

			fc.utobj().printTestStep("Navigate To The Hub > News Page");
			fc.hub().hub_common().theHubNewsSubModule(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + folderName + "')]"));

			fc.utobj().printTestStep("Add News Item In Folder");
			String newsItemTitle = fc.utobj().generateTestData(dataSet.get("newsItemTitle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String editorText = fc.utobj().generateTestData(dataSet.get("editorText"));
			String file = FranconnectUtil.config.get("inputDirectory") + "\\testData\\document\\"
					+ dataSet.get("uploadFile");
			addNewsItems(driver, newsItemTitle, briefSummary, file, editorText);

			fc.utobj().printTestStep("Archive News Item");
			fc.utobj().actionImgOption(driver, newsItemTitle, "Archive");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Navigate To Archived News Item Tab");
			fc.utobj().clickElement(driver, pobj.archivedNewsTab);

			fc.utobj().printTestStep("Delete The Archive News Item");
			fc.utobj().actionImgOption(driver, newsItemTitle, "Delete");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().clickElement(driver, pobj.archivedNewsTab);

			fc.utobj().printTestStep("Verify The Delete Archive News Item");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, newsItemTitle);

			if (isTextPresent == true) {
				fc.utobj().throwsException("was not able to Delete News Item :: Verification At Archived News Page");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub","hub_news" })
	@TestCase(createdOn = "2018-04-06", updatedOn = "2018-06-14", testCaseDescription = "Verify the Add News item link through Quick Links at bottom of page bar button", testCaseId = "TC_Quick_Link_News_Item_01")
	private void quickLinkNews() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Configure Add News In Quick Link");
			OptionsConfigureQuickLinkPageTest quick_link = new OptionsConfigureQuickLinkPageTest();
			quick_link.configureQuickLnk(driver, "Add News", true);

			fc.utobj().printTestStep("Click Over Quick Link Bar");
			CommonUI coi = new CommonUI(driver);

			fc.utobj().printTestStep("Notification > Hub > Add News");
			boolean isDisplayed = coi.showQuickLinks.isDisplayed();

			if (!isDisplayed) {
				fc.utobj().clickElement(driver, coi.showNotificationBar);
			}
			fc.utobj().clickElement(driver, coi.showQuickLinks);
			fc.utobj().clickElement(driver, coi.addNewsQuickLink);

			boolean isNewsHomePage = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[.='Folder']");

			if (isNewsHomePage == false) {
				fc.utobj().throwsException(
						"Not able to redirect to News Home Page After clicking over Add News Quick Link");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub","hub_news"})
	@TestCase(createdOn = "2018-04-09", updatedOn = "2018-04-09", testCaseDescription = "Verify Top Stories Folder privileges", testCaseId = "TC_Hub_Top_Stories_Privileges_01")
	private void topStoriesFolderPrivileges() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubNewsPage pobj = new TheHubNewsPage(driver);
			String emailId = "hubautomation@staffex.com";
			
			fc.utobj().printTestStep("Add Corporate Role");
			String roleName = fc.utobj().generateTestData("TcCorNews");
			new AdminUsersRolesAddNewRolePageTest().addCorporateRoles(driver, roleName);
			
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporate_user = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			String roleName1 = fc.utobj().generateTestData("TcCorNews");
			new AdminUsersRolesAddNewRolePageTest().addCorporateRoles(driver, roleName1);
			
			fc.utobj().printTestStep(
					"Navigate To Admin > Users > Manage Corporate Users > Add  Corporate User with added role");
			CorporateUser corpUser1 = new CorporateUser();
			corpUser1 = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser1);
			corpUser1.setEmail(emailId);
			corpUser1.setRole(roleName1);
			corpUser1 = corporate_user.createDefaultUser(driver, corpUser1);
			
			fc.utobj().printTestStep("Navigate To The Hub > News Page");
			fc.hub().hub_common().theHubNewsSubModule(driver);

			fc.utobj().clickElement(driver, pobj.topStoriesTab);
			fc.utobj().clickElement(driver, pobj.modifyFolder);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			if (!fc.utobj().isSelected(driver, pobj.accessibleToAll.get(1))) {
				fc.utobj().clickElement(driver, pobj.accessibleToAll.get(1));
			}

			WebElement element = fc.utobj().getElementByXpath(driver, ".//*[@id='roleDiv1']/tbody/tr[1]/td[2]");
			String lines[] = fc.utobj().getText(driver, element).split("\\r?\\n");

			for (int i = 1; i <= lines.length; i++) {

				if (lines[i].equalsIgnoreCase(roleName)) {
					System.out.println(lines[i]);
					i = i + 1;
					WebElement inputElement = element.findElement(By.xpath("./input[" + i + "]"));
					fc.utobj().clickElement(driver, inputElement);
					break;
				}
			}
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Add News Item In Top Story Folder");
			String newsItemTitle = fc.utobj().generateTestData(dataSet.get("newsItemTitle"));
			String briefSummary = fc.utobj().generateTestData(dataSet.get("briefSummary"));
			String editorText = fc.utobj().generateTestData(dataSet.get("editorText"));
			String file = fc.utobj().getFilePathFromTestData(dataSet.get("uploadFile"));
			String mediaType = "Image";
			addNewsItemInTopStories(driver, newsItemTitle, briefSummary, editorText, file, mediaType, dataSet);

			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login By Newly Added Corporate User");
			fc.loginpage().loginWithParameter(driver, corpUser1.getUserName(), corpUser1.getPassword());

			fc.utobj().printTestStep("Navigate To The Hub > News Page");
			fc.hub().hub_common().theHubNewsSubModule(driver);

			fc.utobj().printTestStep("Verify News Item Should not visible at News > Home");
			fc.utobj().clickElement(driver, pobj.homeTab);

			boolean isNewsItemPresentAtHome = fc.utobj().assertPageSource(driver, newsItemTitle);
			if (isNewsItemPresentAtHome) {
				fc.utobj().throwsException("Please check News Item is visible at News > Home");
			}

			fc.utobj().sendKeys(driver, pobj.searchAllDocument, newsItemTitle);
			fc.utobj().clickElement(driver, pobj.searchItem);

			boolean oiSearchByTopSearch = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text() , 'No Records Found.')]");
			if (oiSearchByTopSearch==false) {
				fc.utobj().throwsException("News Item is searchable by Search All Hub Items even after not privilege");
			}

			fc.utobj().sendKeys(driver, new SearchUI(driver).topSearchField, newsItemTitle);
			fc.utobj().clickEnterOnElement(driver, new SearchUI(driver).topSearchField);
			
			boolean isTestPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[@id='searchResult']//*[contains(text () ,'"+newsItemTitle+"') and contains(text() , 'did not match any items')]");
			if (isTestPresent == false) {
				fc.utobj().throwsException("News Item is searchable by Solr even after not privilege");
			}
			fc.utobj().refresh(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(groups = { "thehub","hub_news"})
	@TestCase(createdOn = "2018-04-19", updatedOn = "2018-04-19", testCaseDescription = "Verify inaccessible News items and folders through Global Solr search", testCaseId = "TC_Hub_Folder_NewsItem_Privileges_02")
	private void folderPrivileges() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubNewsPage pobj = new TheHubNewsPage(driver);
			String emailId = "hubautomation@staffex.com";
			
			fc.utobj().printTestStep("Add Corporate Role");
			String roleName = fc.utobj().generateTestData("TcCorNews");
			new AdminUsersRolesAddNewRolePageTest().addCorporateRoles(driver, roleName);
			
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporate_user = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			String roleName1 = fc.utobj().generateTestData("TcCorNews");
			new AdminUsersRolesAddNewRolePageTest().addCorporateRoles(driver, roleName1);
			
			fc.utobj().printTestStep(
					"Navigate To Admin > Users > Manage Corporate Users > Add  Corporate User with added role");
			CorporateUser corpUser1 = new CorporateUser();
			corpUser1 = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser1);
			corpUser1.setEmail(emailId);
			corpUser1.setRole(roleName1);
			corpUser1 = corporate_user.createDefaultUser(driver, corpUser1);
			
			fc.utobj().printTestStep("Navigate To Admin > The Hub > News Page");
			fc.hub().hub_common().adminTheHubNewsPage(driver);

			AdminTheHubNewsPageTest admin_news=new AdminTheHubNewsPageTest();
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderSummary = fc.utobj().generateTestData(dataSet.get("folderSummary"));
			String accessTo = "All";
			
			News news=new News();
			news.setFolderName(folderName);
			news.setFolderSummary(folderSummary);
			news.setAccessTo(accessTo);
			
			fc.utobj().printTestStep("Add Folder");
			admin_news.addFolder(driver, news);
			
			fc.utobj().printTestStep("Navigate To The Hub > News Page");
			fc.hub().hub_common().theHubNewsSubModule(driver);
			
			fc.utobj().printTestStep("Add first sub folder");
			String subFolderName=fc.utobj().generateTestData("Testsubfoldername");
			String subFolderSummary=fc.utobj().generateTestData("Test sub folder summary");
			addSubFolderInFolder(driver, folderName, subFolderName, subFolderSummary);
			
			fc.utobj().printTestStep("Add second sub folder");
			fc.utobj().clickElementWithActions(driver, fc.utobj().getElementByLinkText(driver, folderName));
			
			String subFolderName2=fc.utobj().generateTestData("Testsubfoldername");
			String subFolderSummary2=fc.utobj().generateTestData("Test sub folder summary");
			addSubFolderInFolder(driver, subFolderName, subFolderName2, subFolderSummary2);
			
			fc.utobj().clickElementWithActions(driver, fc.utobj().getElementByLinkText(driver, folderName));
			fc.utobj().clickElementWithActions(driver, fc.utobj().getElementByLinkText(driver, subFolderName));
			
			fc.utobj().printTestStep("Add third sub folder");
			String subFolderName3=fc.utobj().generateTestData("Testsubfoldername");
			String subFolderSummary3=fc.utobj().generateTestData("Test sub folder summary");
			addSubFolderInFolder(driver, subFolderName2, subFolderName3, subFolderSummary3);
			
			fc.utobj().printTestStep("Add News Item in third sub Folder");
			fc.utobj().clickElementWithActions(driver, fc.utobj().getElementByLinkText(driver, folderName));
			fc.utobj().clickElementWithActions(driver, fc.utobj().getElementByLinkText(driver, subFolderName));
			fc.utobj().clickElementWithActions(driver, fc.utobj().getElementByLinkText(driver, subFolderName2));
			fc.utobj().clickElementWithActions(driver, fc.utobj().getElementByLinkText(driver, subFolderName3));
			
			String newsItemTitle=fc.utobj().generateTestData("Testnewsitemtitle");
			String briefSummary=fc.utobj().generateTestData("Test news item brief summary");
			String file=fc.utobj().getFilePathFromTestData(dataSet.get("fileName"));
			String editorText=fc.utobj().generateTestData("Test news item editor text");
			addNewsItems(driver, newsItemTitle, briefSummary, file, editorText);
			
			fc.utobj().clickElementWithActions(driver, fc.utobj().getElementByLinkText(driver, folderName));
			fc.utobj().clickElementWithActions(driver, fc.utobj().getElementByLinkText(driver, subFolderName));
			
			fc.utobj().clickElement(driver, pobj.modifyFolder);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			if (!fc.utobj().isSelected(driver, pobj.accessibleToAll.get(1))) {
				fc.utobj().clickElement(driver, pobj.accessibleToAll.get(1));
			}

			fc.utobj().sleep();
			news.setAccessTo("Corporate");
			news.setRoleName(roleName);
			new HubCommonMethods().selectRolesInNews(driver, news);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login By Newly Added Corporate User");
			fc.loginpage().loginWithParameter(driver, corpUser1.getUserName(), corpUser1.getPassword());

			fc.utobj().printTestStep("Navigate To The Hub > News Page");
			fc.hub().hub_common().theHubNewsSubModule(driver);
			
			fc.utobj().printTestStep("Verify the inaccessible news folder items on News Home page");
			fc.utobj().clickElement(driver, pobj.homeTab);
			
			boolean isNewsPresent=fc.utobj().assertPageSource(driver, newsItemTitle);
			if (isNewsPresent) {
				fc.utobj().throwsException("News Item is visible at News > Home Page");
			}

			fc.utobj().sendKeys(driver, new SearchUI(driver).topSearchField, newsItemTitle);
			fc.utobj().clickEnterOnElement(driver, new SearchUI(driver).topSearchField);

			boolean isTestPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[@id='searchResult']//*[contains(text () ,'"+newsItemTitle+"') and contains(text() , 'did not match any items')]");
			if (isTestPresent == false) {
				fc.utobj().throwsException("News Item is searchable by Solr even after not privilege");
			}
			
			fc.utobj().sendKeys(driver, new SearchUI(driver).topSearchField, subFolderName);
			fc.utobj().clickEnterOnElement(driver, new SearchUI(driver).topSearchField);

			boolean isFolderAccessable = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[@id='searchResult']//*[contains(text () ,'"+subFolderName+"') and contains(text() , 'did not match any items')]");
			if (isFolderAccessable == false) {
				fc.utobj().throwsException("Sub folder is searchable by Solr even after not privilege");
			}
			fc.utobj().refresh(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
}
