package com.builds.test.thehub;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import org.xml.sax.SAXException;

import com.builds.uimaps.infomgr.InfoMgrPagination;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class TheHubPaginationTest {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "thehub" ,"hub_pagination"})

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_The_Hub_Paging_Report_Filter_Directory", testCaseDescription = "This test case will verify Directory Paging all the summary of  The Hub Module ", reference = {
			"" })
	public void VerifyTheHubSummaryPagingDirectory() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("verify Paging for  Directory Corporate Users  ");
			showPagingTheHubModule(config, driver, "Directory", 20);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub"  ,"hub_pagination"})

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_The_Hub_Paging_Report_Filter_DirectoryDivisional", testCaseDescription = "This test case will verify DirectoryDivisional Paging all the summary of  The Hub Module ", reference = {
			"" })
	public void VerifyTheHubSummaryPagingDirectoryDivisional() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("verify Paging for  Directory Divisional Users ");
			showPagingTheHubModule(config, driver, "DirectoryDivisional", 20);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub"  ,"hub_pagination"})

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_The_Hub_Paging_Report_Filter_DiDirectoryFranchise", testCaseDescription = "This test case will verify DirectoryFranchise Paging all the summary of  The Hub Module ", reference = {
			"" })
	public void VerifyTheHubSummaryPagingDirectoryFranchise() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("verify Paging for  Directory Franchise Users ");
			showPagingTheHubModule(config, driver, "DirectoryFranchise", 20);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub" ,"hub_pagination" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_The_Hub_Paging_Report_Filter_DirectoryRegional", testCaseDescription = "This test case will verify DirectoryRegional Paging all the summary of  The Hub Module ", reference = {
			"" })
	public void VerifyTheHubSummaryPagingDirectoryRegional() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("verify Paging for  Directory Regional Users ");
			showPagingTheHubModule(config, driver, "DirectoryRegional", 20);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub" ,"hub_pagination" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_The_Hub_Paging_Report_Filter_Suppliers", testCaseDescription = "This test case will verify Suppliers Paging all the summary of  The Hub Module ", reference = {
			"" })
	public void VerifyTheHubSummaryPagingSuppliers() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("verify Paging for  Directory Suppliers Users ");
			showPagingTheHubModule(config, driver, "Suppliers", 20);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub" ,"hub_pagination" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_The_Hub_Paging_Report_Filter_Groups", testCaseDescription = "This test case will verify Groups Paging all the summary of  The Hub Module ", reference = {
			"" })
	public void VerifyTheHubSummaryPagingGroups() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("verify Paging for  Directory Groups ");
			showPagingTheHubModule(config, driver, "Groups", 20);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub" ,"hub_pagination" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_The_Hub_Paging_Report_Filter_Alerts", testCaseDescription = "This test case will verify Alerts Paging all the summary of  The Hub Module ", reference = {
			"" })
	public void VerifyTheHubSummaryPagingAlerts() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("verify Paging for  Alerts ");
			showPagingTheHubModule(config, driver, "Alerts", 20);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub" ,"hub_pagination" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_The_Hub_Paging_Report_Filter_messages", testCaseDescription = "This test case will verify messages Paging all the summary of  The Hub Module ", reference = {
			"" })
	public void VerifyTheHubSummaryPagingmessages() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("verify Paging for  Messages ");
			showPagingTheHubModule(config, driver, "messages", 20);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub" ,"hub_pagination" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_The_Hub_Paging_Report_Filter_Tasks", testCaseDescription = "This test case will verify Tasks Paging all the summary of  The Hub Module ", reference = {
			"" })
	public void VerifyTheHubSummaryPagingTasks() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("verify Paging for Tasks ");
			showPagingTheHubModule(config, driver, "Tasks", 20);

			// Related Links Left need to implement

			// End Lead Summary paging

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void showPagingTheHubModule(Map<String, String> config, WebDriver driver, String subModuleName,
			int viewPerPage)
			throws ParserConfigurationException, SAXException, IOException, InterruptedException, Exception

	{
		String testCaseId = "TC_The_Hub_Paging_Report_Filter";
		InfoMgrPagination InfoMgrPagination = new InfoMgrPagination(driver);

		if (subModuleName == null)
			subModuleName = "Directory";
		List<WebElement> records = null;
		fc.utobj().printTestStep("Search " + subModuleName + " and click");

		if (subModuleName != null && "Directory".equals(subModuleName)) {

			try {
				fc.hub().hub_common().theHubDirectorySubModule(driver);
				records = driver.findElements(By.xpath(".//*[@id='siteMainTable']/tbody/tr/td/table/tbody/tr[5]"));
				// Thread.sleep(1000);
			} catch (Exception e) {
				// e.printStackTrace();
				fc.utobj().printTestStep(subModuleName + " Not opernational in proper.");
			}
		} else if (subModuleName != null && "DirectoryDivisional".equals(subModuleName)) {

			try {
				fc.hub().hub_common().theHubDirectorySubModule(driver);
				fc.utobj().clickElement(driver, InfoMgrPagination.DivisnalUserTab);
				records = driver.findElements(By.xpath(".//*[@id='siteMainTable']/tbody/tr/td/table/tbody/tr[5]"));

				// Thread.sleep(1000);
			} catch (Exception e) {
				// e.printStackTrace();
				fc.utobj().printTestStep(subModuleName + " Not opernational in proper.");
			}
		} else if (subModuleName != null && "DirectoryFranchise".equals(subModuleName)) {

			try {
				fc.hub().hub_common().theHubDirectorySubModule(driver);
				fc.utobj().clickElement(driver, InfoMgrPagination.FranchiseUsersUserTab);
				records = driver.findElements(By.xpath(".//*[@id='siteMainTable']/tbody/tr/td/table/tbody/tr[5]"));

				// Thread.sleep(1000);
			} catch (Exception e) {
				// e.printStackTrace();
				fc.utobj().printTestStep(subModuleName + " Not opernational in proper.");
			}
		} else if (subModuleName != null && "DirectoryRegional".equals(subModuleName)) {

			try {
				fc.hub().hub_common().theHubDirectorySubModule(driver);
				fc.utobj().clickElement(driver, InfoMgrPagination.RegionalUsersUserTab);
				records = driver.findElements(By.xpath(".//*[@id='siteMainTable']/tbody/tr/td/table/tbody/tr[5]"));
				// Thread.sleep(1000);
			} catch (Exception e) {
				// e.printStackTrace();
				fc.utobj().printTestStep(subModuleName + " Not opernational in proper.");
			}
		} else if (subModuleName != null && "Suppliers".equals(subModuleName)) {

			try {
				fc.hub().hub_common().theHubDirectorySubModule(driver);
				fc.utobj().clickElement(driver, InfoMgrPagination.SuppliersUsersUserTab);
				records = driver.findElements(By.xpath(".//*[@id='siteMainTable']/tbody/tr/td/table/tbody/tr[5]"));
				// Thread.sleep(1000);
			} catch (Exception e) {
				// e.printStackTrace();
				fc.utobj().printTestStep(subModuleName + " Not opernational in proper.");
			}
		} else if (subModuleName != null && "Groups".equals(subModuleName)) {

			try {
				fc.hub().hub_common().theHubDirectorySubModule(driver);
				fc.utobj().clickElement(driver, InfoMgrPagination.GroupsUsersUserTab);
				records = driver.findElements(By.xpath(".//*[@id='siteMainTable']/tbody/tr/td/table/tbody/tr[5]"));
				// Thread.sleep(1000);
			} catch (Exception e) {
				// e.printStackTrace();
				fc.utobj().printTestStep(subModuleName + " Not opernational in proper.");
			}
		} else if (subModuleName != null && "Alerts".equals(subModuleName)) {

			try {
				fc.hub().hub_common().theHubAlertsSubModule(driver);
				records = driver.findElements(By.xpath(".//*[@id='siteMainTable']/tbody/tr/td/table/tbody/tr[7]"));
				// Thread.sleep(1000);
			} catch (Exception e) {
				// e.printStackTrace();
				fc.utobj().printTestStep(subModuleName + " Not opernational in proper.");
			}
		} else if (subModuleName != null && "messages".equals(subModuleName)) {

			try {
				fc.hub().hub_common().theHubMessageSubModule(driver);
				records = driver.findElements(By.xpath(".//*[@id='siteMainTable']/tbody/tr/td/table/tbody/tr[3]"));
				// Thread.sleep(1000);
			} catch (Exception e) {
				// e.printStackTrace();
				fc.utobj().printTestStep(subModuleName + " Not opernational in proper.");
			}
		} else if (subModuleName != null && "Tasks".equals(subModuleName)) {

			try {
				fc.hub().hub_common().theHubTaskSubModule(driver);
				records = driver.findElements(By.xpath(".//*[@id='siteMainTable']/tbody/tr/td/table/tbody/tr[3]"));
				// Thread.sleep(1000);
			} catch (Exception e) {
				// e.printStackTrace();
				fc.utobj().printTestStep(subModuleName + " Not opernational in proper.");
			}
		}

		String header;
		int totalRecordCount = 0;
		int totalrecordPerPage = 0;
		String opnerPageingCheckList = "pageid";
		try {
			header = fc.utobj().getElementByXpath(driver, ".//td[@id='pageid']").getText();
			String options[] = header.split(" ");
			if (options != null) {
				totalRecordCount = Integer.valueOf(options[5]);
				totalrecordPerPage = Integer.valueOf(options[3]);
			}
			fc.utobj().printTestStep(subModuleName + " ToTal Record " + totalRecordCount);

		} catch (Exception E) {
			fc.utobj().printTestStep("Pagination not found in " + subModuleName);
			// System.out.println("Pagination not found");
			totalRecordCount = 0;
		}

		if (totalRecordCount > 0 && totalRecordCount > viewPerPage) {
			fc.utobj().printTestStep("Pagination exists in " + subModuleName);

			int clickCount = totalRecordCount / viewPerPage;
			String optionsShoud[] = new String[clickCount + 1];
			;
			int inLastRerord = totalRecordCount - (viewPerPage * clickCount);
			for (int cnt = 0; cnt <= clickCount; cnt++) {

				optionsShoud[cnt] = viewPerPage + "";
				if (cnt == clickCount) {
					optionsShoud[cnt] = inLastRerord + "";
				}

			}

			int foundRecordCount = 0;
			int counter = 1;
			try {
				if (optionsShoud != null && optionsShoud.length > 0) {
					for (int i = 0; i < (optionsShoud.length); i++) {
						// System.out.println("===Val=="+lstOptions.get(i)
						// +"==New=="+lstOptions.get(i).getText()
						// +"===clickCount=="+clickCount+"optionsShoud=="+optionsShoud[i]);
						if (true) {

							if (i == 1 && i != 0) {
								fc.utobj().clickElement(driver,
										fc.utobj().getElementByXpath(driver, ".//*[@id='pageid']/a[" + (i) + "]/u"));
								counter = i;
							} else if (i > 1 && i != 0) {
								fc.utobj().clickElement(driver,
										fc.utobj().getElementByXpath(driver, ".//*[@id='pageid']/a[" + (i) + "]/u"));
								counter = i + 1;

							}
							foundRecordCount = pagingRecordsTheHub(driver, records, subModuleName).size();
							if (foundRecordCount > 0) {

								if (foundRecordCount == Integer.parseInt(optionsShoud[i])) {
									// System.out.println("found record
									// "+foundRecordCount+"= Test Case Pass for
									// the page "+(i));
									fc.utobj().printTestStep(+foundRecordCount + " records for page = " + counter);
								} else {

									if (foundRecordCount <= Integer.parseInt(optionsShoud[i])) {
										// System.out.println("found record
										// "+foundRecordCount+"= Test Case Pass
										// for the Last page "+(i));
										fc.utobj().printTestStep(+foundRecordCount + " records for page = " + counter);
									} else {
										// System.out.println("found record
										// "+foundRecordCount+"= Test Case Fail
										// for the page "+(i));
										// fc.utobj().printTestStep(testCaseId,
										// "found record "+foundRecordCount+"=
										// Test Case Fail for the page "+(i));
										fc.utobj().throwsException("found record " + foundRecordCount
												+ "= Test Case Fail for the page Almost Last Page " + counter);
									}
								}
							} else {
								fc.utobj().throwsException(
										"" + foundRecordCount + " records for page " + counter + " Test case fails");
							}
							// Check for paging after sort function
							if (subModuleName != null && "Alerts".equals(subModuleName)) {
								fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
										".//table[@class='summaryTblex']/tbody/tr[1]/td[4]//a[not(*)]"));
							} else if (subModuleName != null && "messages".equals(subModuleName)) {
								fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
										".//table[@class='summaryTblex']/tbody/tr[1]/td[7]//a[not(*)]"));
							} else {
								fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
										".//table[@class='summaryTblex']/tbody/tr[1]/td[2]//a[not(*)]"));
							}
							foundRecordCount = pagingRecordsTheHub(driver, records, subModuleName).size();
							if (foundRecordCount > 0) {

								if (foundRecordCount == Integer.parseInt(optionsShoud[i])) {
									// System.out.println("found record
									// "+foundRecordCount+"= Test Case Pass for
									// the page "+(i));
									fc.utobj().printTestStep(
											"After Sort record " + foundRecordCount + " for the page " + counter);
								} else {

									if (foundRecordCount <= Integer.parseInt(optionsShoud[i])) {
										// System.out.println("found record
										// "+foundRecordCount+"= Test Case Pass
										// for the Last page "+(i));
										fc.utobj().printTestStep(
												"After Sort record " + foundRecordCount + " for the page " + counter);
									} else {
										// System.out.println("found record
										// "+foundRecordCount+"= Test Case Fail
										// for the page "+(i));
										// fc.utobj().printTestStep(testCaseId,
										// "After Sort record
										// "+foundRecordCount+"= Test Case Fail
										// for the page "+(i));
										// fc.utobj().throwsException("After
										// Sort record "+foundRecordCount+"=
										// Test Case Fail for the page "+(i));
										fc.utobj().throwsException("After Sort record " + foundRecordCount
												+ "= Test Case Fail for the page Almost Last Page " + (i + 1));
									}

								}
							} else {
								fc.utobj().throwsException("" + foundRecordCount + " records for page " + counter
										+ " Test case fails on click sort");
							}
							//

						}
					}
				}

			} catch (Exception e) {
				e.printStackTrace();
			}

			try {
				// fc.utobj().printTestStep(testCaseId, "Reset our page to the
				// page 1 to check the results Per Page verification.");
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='pageid']/a[1]/u"));
				fc.utobj().selectDropDown(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='resultsPerPage']"),
						"100");
				foundRecordCount = pagingRecordsTheHub(driver, records, subModuleName).size();
				if (foundRecordCount > 0) {

				}
				if (foundRecordCount <= 100) {
					// System.out.println("found record in Result Per page all
					// "+foundRecordCount+" test case passed at View Per Page");
					fc.utobj().printTestStep("View Per Page " + foundRecordCount + " records.");

				} else {
					if (foundRecordCount > 100) {
						fc.utobj().throwsException("View Per Page " + foundRecordCount + " records.");
					}
				}
				// Check for paging after click on Sort function
				if (subModuleName != null && "Alerts".equals(subModuleName)) {
					fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
							".//table[@class='summaryTblex']/tbody/tr[1]/td[4]/a"));
				} else if (subModuleName != null && "messages".equals(subModuleName)) {
					fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
							".//table[@class='summaryTblex']/tbody/tr[1]/td[7]/a"));
				} else {
					fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
							".//table[@class='summaryTblex']/tbody/tr[1]/td[2]/a"));
				}
				foundRecordCount = pagingRecordsTheHub(driver, records, subModuleName).size();
				if (foundRecordCount > 0) {

				}
				if (foundRecordCount <= 100) {
					// System.out.println("found record in Result Per page all
					// "+foundRecordCount+" test case passed at View Per Page");
					fc.utobj().printTestStep("After Sort found record in Result Per page  " + foundRecordCount
							+ " test case passed at View Per Page");

				} else {
					if (foundRecordCount > 100) {
						fc.utobj().throwsException("View Per Page " + foundRecordCount + " records.");
					}
				}
				fc.utobj().selectDropDown(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='resultsPerPage']"),
						"20");

			} catch (Exception E) {
				// System.out.println("resultsPerPage Field Not Found!");
				fc.utobj().printTestStep("View Per Page  Field Not Found! in " + subModuleName);

				// totalRecordCount = 0;
			}
			try {

				fc.utobj().clickLink(driver, "Show All");

				// System.out.println("totalRecordCount===in show
				// all"+totalRecordCount);
				fc.utobj().printTestStep("totalRecordCount in show all " + subModuleName + " = " + totalRecordCount);
				foundRecordCount = pagingRecordsTheHub(driver, records, subModuleName).size();
				if (foundRecordCount > 0) {

				}
				if (totalRecordCount == foundRecordCount) {
					// System.out.println("found record in show all
					// "+foundRecordCount+" test case passed");
					fc.utobj().printTestStep("found record in show all " + foundRecordCount + " test case passed");
				} else {
					if (foundRecordCount > 0) {
						if (totalRecordCount != foundRecordCount) {
							fc.utobj()
									.throwsException("Show All " + foundRecordCount + " records not working correct.");
						}
					}
				}
				// System.out.println("found record "+foundRecordCount+" in
				// "+lstOptions.get(i).getText()+" th Page");

			} catch (Exception E) {
				// System.out.println("Show All Field Not Found!");
				fc.utobj().printTestStep("Show All field not found!");
				// totalRecordCount = 0;
			}

		} else {
			// System.out.println("Pagination Not Exists");
			fc.utobj().printTestStep("Pagination Not Exists in " + subModuleName);
		}

		// sortedList
		try {
			boolean isSorttable = sortedListTheHub(driver, config, records, subModuleName);
			if (isSorttable == true) {
				fc.utobj().printTestStep("Sorting function working for  " + subModuleName);
			} else {
				fc.utobj().printTestStep("Sorting function not Working for  " + subModuleName + " OR not applicable.");
			}

		} catch (Exception e) {
			// e.printStackTrace();
			fc.utobj().printTestStep("Eception in Sorting function which Working for  " + subModuleName);
		}

		// End sortedList

		fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
	}

	public boolean sortedListTheHub(WebDriver driver, Map<String, String> config, List<WebElement> records,
			String subModuleName) {

		boolean isSorttable = false;
		try {
			records = pagingRecordsTheHub(driver, records, subModuleName);
			ArrayList<String> obtainedList = new ArrayList<>();
			ArrayList<String> sortedList = new ArrayList<>();
			List<WebElement> elementList = records; // driver.findElements(By.xpath(YourLocator));
			for (WebElement we : elementList) {
				obtainedList.add(we.getText());
			}
			if (subModuleName != null && "Alerts".equals(subModuleName)) {
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//table[@class='summaryTblex']/tbody/tr[1]/td[4]//a[not(*)]"));
			} else if (subModuleName != null && "messages".equals(subModuleName)) {
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//table[@class='summaryTblex']/tbody/tr[1]/td[7]//a[not(*)]"));
			} else {
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//table[@class='summaryTblex']/tbody/tr[1]/td[2]//a[not(*)]"));
			}

			records = pagingRecordsTheHub(driver, records, subModuleName);
			for (WebElement we : records) {
				sortedList.add(we.getText());
			}
			try {
				if (sortedList != null) {
					for (int i = 0; i < sortedList.size(); i++) {
						// System.out.println(sortedList.get(i));
						if (sortedList.get(0) != obtainedList.get(0)) {
							// System.out.println(sortedList.get(0)+"======="+obtainedList.get(0));
							isSorttable = true;
							break;
						}
					}
				}

			} catch (Exception e) {
				isSorttable = false;

			}

		} catch (Exception e) {
			isSorttable = false;
			// e.printStackTrace();
		}
		return isSorttable;
	}

	public List<WebElement> pagingRecordsTheHub(WebDriver driver, List<WebElement> records, String subModuleName) {

		if (subModuleName == null)
			subModuleName = "LeadSummary";
		List<WebElement> listofElements = null;
		int rc = 0;
		if (subModuleName != null && "Directory".equals(subModuleName) || "DirectoryDivisional".equals(subModuleName)
				|| "DirectoryFranchise".equals(subModuleName) || "DirectoryRegional".equals(subModuleName)
				|| "Suppliers".equals(subModuleName) || "Groups".equals(subModuleName)
				|| "Tasks".equals(subModuleName)) {
			listofElements = driver.findElements(
					By.xpath(".//table[@class='summaryTblex']/tbody/tr/td[@class='botBorder colPadding'][2]"));
		} else if (subModuleName != null && "Alerts".equals(subModuleName)) {
			listofElements = driver.findElements(
					By.xpath(".//table[@class='summaryTblex']/tbody/tr/td[@class='botBorder colPadding'][4]"));
		} else if (subModuleName != null && "messages".equals(subModuleName)) {
			listofElements = driver.findElements(
					By.xpath(".//table[@class='summaryTblex']/tbody/tr/td[@class='botBorder colPadding'][7]"));
		}
		rc = listofElements.size();
		return listofElements;

	}
}
