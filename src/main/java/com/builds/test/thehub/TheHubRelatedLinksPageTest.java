package com.builds.test.thehub;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminHiddenLinksConfigureNewHierarchyLevelPageTest;
import com.builds.uimaps.thehub.TheHubRelatedLinksPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class TheHubRelatedLinksPageTest {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "thehub" ,"hub_relatedLink"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Genearal Link At The Hub > Related Links Page", testCaseId = "TC_261_Verify_General_Link")
	private void verifyGeneralLink() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubRelatedLinksPage pobj = new TheHubRelatedLinksPage(driver);
			String parentWindow = driver.getWindowHandle();

			fc.utobj().printTestStep("Navigate To Admin > The Hub > Related Links Page");
			fc.utobj().printTestStep("Add New Link");

			AdminTheHubRelatedLinksPageTest relatedLinkPage = new AdminTheHubRelatedLinksPageTest();
			String linkTitle = fc.utobj().generateTestData(dataSet.get("linkTitle"));
			String text = fc.utobj().generateRandomNumber();
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			String accessTo = "CorporateUsers";
			String linkUrl = relatedLinkPage.addNewLink(driver, text, linkTitle, description, accessTo);

			fc.utobj().printTestStep("Navigate To The Hub > Related Links Page");

			fc.hub().hub_common().theHubRelatedLinksSubModule(driver);
			String urlText = null;
			boolean status = false;

			fc.utobj().printTestStep("Verify New Link");
			
			for (int i = 0; i < 10; i++) {
				try {
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
					List<WebElement> listElement = driver.findElements(By.xpath(".//*[.='" + linkTitle + "']"));
					
					if (listElement.size()>0) {
						listElement.get(0).click();
						break;
					}else {
						fc.utobj().clickElement(driver, pobj.generalLinkNext);
					}
				} catch (Exception e) {
				}finally {
					driver.manage().timeouts().implicitlyWait(45, TimeUnit.SECONDS);
				}
			}
			
			Set<String> allWindow = driver.getWindowHandles();
			for (String wId : allWindow) {
				if (!wId.equalsIgnoreCase(parentWindow)) {
					try {
						driver.switchTo().window(wId);
						if (driver.getCurrentUrl().contains(linkUrl)) {
							urlText=driver.getCurrentUrl();
							status=true;
						}
					} catch (Exception e) {
						status=false;
						driver.close();
					}
				}
				driver.switchTo().window(parentWindow);
			}
			if (status) {
				urlText = urlText.replace("http://", "");
				urlText = urlText.replace("/", "");
				if (!urlText.contains(linkUrl)) {
					fc.utobj().throwsException("was not able to verify Link Url");
				}
			}else {
				fc.utobj().throwsException("was not able to verify Link Url");
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"thehub" ,"hub_relatedLink"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Add New Link at The Hub > Related Links > My Links", testCaseId = "TC_262_Add_New_Link")
	public void addNewLink() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubRelatedLinksPage pobj = new TheHubRelatedLinksPage(driver);

			fc.utobj().printTestStep("Navigate To The Hub > Related Links Page");
			fc.utobj().printTestStep("Add New Link");
			fc.hub().hub_common().theHubRelatedLinksSubModule(driver);
			fc.utobj().clickElement(driver, pobj.addNewLink);
			String text = fc.utobj().generateRandomNumber();
			String linkUrl = "www." + text + ".com";
			fc.utobj().sendKeys(driver, pobj.linkUrl, linkUrl);
			String linkTitle = fc.utobj().generateTestData(dataSet.get("linkTitle"));
			fc.utobj().sendKeys(driver, pobj.linkTitle, linkTitle);
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().sendKeys(driver, pobj.description, description);
			fc.utobj().clickElement(driver, pobj.addBtn);

			fc.utobj().printTestStep("Verify Add New Link");
			fc.utobj().isTextDisplayed(driver, linkTitle, "was not able to verify Title of the Link");
			fc.utobj().isTextDisplayed(driver, linkTitle, "was not able to verify Title of the Link");
			WebElement element = fc.utobj().getElementByXpath(driver, ".//*[.='" + linkTitle
					+ "']/ancestor::tr/td[contains(text () , '" + fc.utobj().getCurrentDateUserDefineTimeZoneUSFormat("GMT-5:00") + "')]");
			fc.utobj().moveToElement(driver, element);
			if (!element.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Added On Date");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addNewLink(WebDriver driver, String text, String linkTitle, String description) throws Exception {

		String testCaseId = "TC_Add_New_Link_The_Hub";
		String linkUrl = null;

		if (fc.utobj().validate(testCaseId)) {
			try {
				TheHubRelatedLinksPage pobj = new TheHubRelatedLinksPage(driver);
				fc.hub().hub_common().theHubRelatedLinksSubModule(driver);
				fc.utobj().clickElement(driver, pobj.addNewLink);
				linkUrl = "www." + text + ".com";
				fc.utobj().sendKeys(driver, pobj.linkUrl, linkUrl);
				fc.utobj().sendKeys(driver, pobj.linkTitle, linkTitle);
				fc.utobj().sendKeys(driver, pobj.description, description);
				fc.utobj().clickElement(driver, pobj.addBtn);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to add New LInk :: The Hub > Related Links");

		}
		return linkUrl;
	}

	@Test(groups = {"thehub" ,"hub_relatedLink"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Modify New Link By Action Image Option At The Hub > Related Links > My Links", testCaseId = "TC_263_Modify_Link")
	private void modifyLink() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubRelatedLinksPage pobj = new TheHubRelatedLinksPage(driver);

			fc.utobj().printTestStep("Navigate To The Hub > Related Links Page");
			fc.utobj().printTestStep("Add New Link");
			String linkTitle = fc.utobj().generateTestData(dataSet.get("linkTitle"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			String text = fc.utobj().generateRandomNumber();
			addNewLink(driver, text, linkTitle, description);

			fc.utobj().printTestStep("Modify Link");
			fc.utobj().actionImgOption(driver, linkTitle, "Modify");

			text = fc.utobj().generateRandomNumber();
			String linkUrl = "www." + text + ".com";
			fc.utobj().sendKeys(driver, pobj.linkUrl, linkUrl);
			linkTitle = fc.utobj().generateTestData(dataSet.get("linkTitle"));
			fc.utobj().sendKeys(driver, pobj.linkTitle, linkTitle);
			description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().sendKeys(driver, pobj.description, description);
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("Verify The Modify Link");
			fc.utobj().isTextDisplayed(driver, linkTitle, "was not able to verify Title of the Link");
			fc.utobj().isTextDisplayed(driver, linkTitle, "was not able to verify Title of the Link");
			WebElement element = fc.utobj().getElementByXpath(driver, ".//*[.='" + linkTitle
					+ "']/ancestor::tr/td[contains(text () , '" + fc.utobj().getCurrentDateUserDefineTimeZoneUSFormat("GMT-5:00") + "')]");
			fc.utobj().moveToElement(driver, element);
			if (!element.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Added On Date");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"thehub" ,"hub_relatedLink"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Delete New Link By Action Image Option At The Hub > Related Links > My Links", testCaseId = "TC_264_Delete_Link")
	private void deleteLink() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminHiddenLinksConfigureNewHierarchyLevelPageTest level = new AdminHiddenLinksConfigureNewHierarchyLevelPageTest();
			level.ConfigureNewHierarchyLevel(driver);

			fc.utobj().printTestStep("Navigate To The Hub > Related Links Page");
			fc.utobj().printTestStep("Add Link");
			String linkTitle = fc.utobj().generateTestData(dataSet.get("linkTitle"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			String text = fc.utobj().generateRandomNumber();
			addNewLink(driver, text, linkTitle, description);

			fc.utobj().printTestStep("Delete Link");
			fc.utobj().actionImgOption(driver, linkTitle, "Delete");
			fc.utobj().acceptAlertBox(driver);
			fc.utobj().showAll(driver);

			fc.utobj().printTestStep("Verify Delete Link");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, linkTitle);
			if (isTextPresent == true) {
				fc.utobj().throwsException("was not able to delete Link");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
}
