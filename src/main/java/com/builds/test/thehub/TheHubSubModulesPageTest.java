package com.builds.test.thehub;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.thehub.TheHubSubModulesPage;
import com.builds.utilities.FranconnectUtil;

public class TheHubSubModulesPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	public void openHomePage(WebDriver driver) throws Exception {

		TheHubSubModulesPage pobj = new TheHubSubModulesPage(driver);

		fc.utobj().clickElement(driver, pobj.homePage);
	}

	public void openDirectoryPage(WebDriver driver) throws Exception {

		TheHubSubModulesPage pobj = new TheHubSubModulesPage(driver);

		fc.utobj().clickElement(driver, pobj.directoryPage);
	}

	public void openAlertsPage(WebDriver driver) throws Exception {

		TheHubSubModulesPage pobj = new TheHubSubModulesPage(driver);

		fc.utobj().clickElement(driver, pobj.alertsPage);
	}

	public void openMessagesPage(WebDriver driver) throws Exception {

		TheHubSubModulesPage pobj = new TheHubSubModulesPage(driver);

		fc.utobj().clickElement(driver, pobj.messagesPage);
	}

	public void openCalendarPage(WebDriver driver) throws Exception {

		TheHubSubModulesPage pobj = new TheHubSubModulesPage(driver);

		try {
			fc.utobj().clickElement(driver, pobj.calendarPage);
			fc.utobj().clickElement(driver, pobj.calendarPage);
		} catch (Exception e) {

		}

	}

	public void openNewsPage(WebDriver driver) throws Exception {

		TheHubSubModulesPage pobj = new TheHubSubModulesPage(driver);

		fc.utobj().clickElement(driver, pobj.newsPage);
	}

	public void openLibraryPage(WebDriver driver) throws Exception {

		TheHubSubModulesPage pobj = new TheHubSubModulesPage(driver);

		fc.utobj().clickElement(driver, pobj.libraryPage);
	}

	public void openForumsPage(WebDriver driver) throws Exception {

		TheHubSubModulesPage pobj = new TheHubSubModulesPage(driver);

		fc.utobj().clickElement(driver, pobj.forumsPage);
	}

	public void openFranBuzzPage(WebDriver driver) throws Exception {

		TheHubSubModulesPage pobj = new TheHubSubModulesPage(driver);

		fc.utobj().clickElement(driver, pobj.franBuzzPage);
	}

	public void openRelatedLinksPage(WebDriver driver) throws Exception {

		TheHubSubModulesPage pobj = new TheHubSubModulesPage(driver);

		fc.utobj().clickElement(driver, pobj.relatedLinksPage);
	}

	public void openWhatsNewPage(WebDriver driver) throws Exception {

		TheHubSubModulesPage pobj = new TheHubSubModulesPage(driver);

		fc.utobj().clickElement(driver, pobj.whatsNewPage);
	}
}
