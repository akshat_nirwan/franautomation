package com.builds.test.thehub;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminConfigurationConfigureTaskEmailsPageTest;
import com.builds.test.admin.AdminFranchiseLocationAddFranchiseLocationPageTest;
import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.common.CorporateUser;
import com.builds.uimaps.thehub.TheHubTasksPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class TheHubTasksPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "thehub", "thehubemail" ,"mailTest123" , "hub_task"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add Task at The Hub Tasks Page", testCaseId = "TC_207_Add_Task")
	private void addTask() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubTasksPage pobj = new TheHubTasksPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String franchiseId = fc.utobj().generateTestData(dataSet.get("franchiseId"));

			AdminFranchiseLocationAddFranchiseLocationPageTest addFranchiseLocation = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = addFranchiseLocation.addFranchiseLocation_All(driver, franchiseId, regionName, storeType);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "hubautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To The Hub > Tasks Page");
			fc.hub().hub_common().theHubTaskSubModule(driver);

			fc.utobj().printTestStep("Add Task");
			fc.utobj().clickElement(driver, pobj.addTaskLink);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().selectDropDown(driver, pobj.franchiseeId, franchiseId);
			fc.utobj().selectValFromMultiSelect(driver, pobj.assignToSelect, corpUser.getuserFullName());
			fc.utobj().selectDropDown(driver, pobj.statusSelect, dataSet.get("status"));
			fc.utobj().selectDropDown(driver, pobj.taskTypSelect, dataSet.get("taskType"));
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subject, subject);

			if (fc.utobj().isSelected(driver, pobj.timelessTaskIdCheck)) {
				fc.utobj().clickElement(driver, pobj.timelessTaskIdCheck);
			}

			fc.utobj().selectDropDown(driver, pobj.sTime, "1");
			fc.utobj().selectDropDown(driver, pobj.sMinute, "00 Min");
			fc.utobj().selectDropDown(driver, pobj.sAPM, "AM");
			fc.utobj().selectDropDown(driver, pobj.eTime, "2");
			fc.utobj().selectDropDown(driver, pobj.eMinute, "00 Min");
			fc.utobj().selectDropDown(driver, pobj.eAPM, "AM");

			fc.utobj().selectDropDown(driver, pobj.prioritySelect, dataSet.get("priority"));
			fc.utobj().sendKeys(driver, pobj.startDate, fc.utobj().getCurrentDateUSFormat());
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			fc.utobj().sendKeys(driver, pobj.comments, comment);
			if (!fc.utobj().isSelected(driver,pobj.schduleTime.get(0))) {
				fc.utobj().clickElement(driver, pobj.schduleTime.get(0));
			}
			fc.utobj().clickElement(driver, pobj.createBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Filter Record By Subject");

			filterRecordBySubject(driver, subject);
			fc.utobj().printTestStep("Verify The Add Task");

			fc.utobj().isTextDisplayed(driver, subject, "was not able to verify subject of the task");
			fc.utobj().isTextDisplayed(driver, franchiseId, "was not able to verify Associated With");
			fc.utobj().isTextDisplayed(driver, fc.utobj().getCurrentDateUSFormat(),
					"was not able to verify start Date");
			fc.utobj().isTextDisplayed(driver, dataSet.get("status"), "task status");
			fc.utobj().isTextDisplayed(driver, dataSet.get("taskType"), "was not able to verify");
			fc.utobj().isTextDisplayed(driver, corpUser.getuserFullName(), "was not able to verify AssignTo");

			fc.utobj().printTestStep("Verify Task Creation Mail");
			Map<String, String> mailInfo = fc.utobj().readMailBox("New Task Added", subject, emailId, "sdg@1a@Hfs");

			if (mailInfo.size() == 0 || !mailInfo.get("mailBody").contains(subject)) {
				fc.utobj().throwsException("was not able to verify Mail After Task Added");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void filterRecordBySubject(WebDriver driver, String subject) throws Exception {

		TheHubTasksPage pobj = new TheHubTasksPage(driver);
		fc.utobj().clickElement(driver, pobj.showFilter);
		fc.utobj().selectDropDown(driver, pobj.categorySelect, "The Hub");
		fc.utobj().sendKeys(driver, pobj.subject, subject);
		fc.utobj().setToDefault(driver, pobj.taskStatus);
		fc.utobj().sendKeys(driver, pobj.startDate, "");
		fc.utobj().sendKeys(driver, pobj.endDate, "");
		fc.utobj().selectDropDown(driver, pobj.reminderSelect, "Select");
		fc.utobj().setToDefault(driver, pobj.prioritySelectFilter);
		fc.utobj().setToDefault(driver, pobj.viewFilterUser);
		fc.utobj().setToDefault(driver, pobj.taskTypeFilter);
		fc.utobj().clickElement(driver, pobj.searchBtn);
	}

	public void addTask(WebDriver driver, String franchiseId, String userName, String subject,
			Map<String, String> dataSet, String comment, Map<String, String> config) throws Exception {

		String testCaseId = "TC_Add_Task_The_Hub";

		if (fc.utobj().validate(testCaseId)) {
			try {
				TheHubTasksPage pobj = new TheHubTasksPage(driver);

				fc.hub().hub_common().theHubTaskSubModule(driver);
				fc.utobj().clickElement(driver, pobj.addTaskLink);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().selectDropDown(driver, pobj.franchiseeId, franchiseId);

				fc.utobj().selectValFromMultiSelect(driver, pobj.assignToSelect, userName);
				fc.utobj().selectDropDown(driver, pobj.statusSelect, dataSet.get("status"));
				fc.utobj().selectDropDown(driver, pobj.taskTypSelect, dataSet.get("taskType"));
				fc.utobj().sendKeys(driver, pobj.subject, subject);
				if (fc.utobj().isSelected(driver, pobj.timelessTaskIdCheck)) {
					fc.utobj().clickElement(driver, pobj.timelessTaskIdCheck);
				}

				fc.utobj().selectDropDown(driver, pobj.sTime, "1");
				fc.utobj().selectDropDown(driver, pobj.sMinute, "00 Min");
				fc.utobj().selectDropDown(driver, pobj.sAPM, "AM");
				fc.utobj().selectDropDown(driver, pobj.eTime, "2");
				fc.utobj().selectDropDown(driver, pobj.eMinute, "00 Min");
				fc.utobj().selectDropDown(driver, pobj.eAPM, "AM");

				fc.utobj().selectDropDown(driver, pobj.prioritySelect, dataSet.get("priority"));
				fc.utobj().sendKeys(driver, pobj.startDate, fc.utobj().getCurrentDateUSFormat());
				fc.utobj().sendKeys(driver, pobj.comments, comment);
				if (!fc.utobj().isSelected(driver,pobj.schduleTime.get(0))) {
					fc.utobj().clickElement(driver, pobj.schduleTime.get(0));
				}
				fc.utobj().clickElement(driver, pobj.createBtn);
				fc.utobj().sleep();
				fc.utobj().switchFrameToDefault(driver);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to add Task at The Hub Page:: The Hub > Tasks");

		}
	}

	@Test(groups = { "thehub","hub_task"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-07-24", testCaseDescription = "Modify Task By Action Img Option at The Hub > Tasks Page", testCaseId = "TC_208_Modify_Task")
	private void modifyTaskActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubTasksPage pobj = new TheHubTasksPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String franchiseId = fc.utobj().generateTestData(dataSet.get("franchiseId"));

			AdminFranchiseLocationAddFranchiseLocationPageTest addFranchiseLocation = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = addFranchiseLocation.addFranchiseLocation_All(driver, franchiseId, regionName, storeType);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "hubautomation@staffex.com";

			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");
			String regionName1 = fc.utobj().generateTestData(dataSet.get("regionName"));
			String storeType1 = fc.utobj().generateTestData(dataSet.get("storeType"));
			String franchiseId1 = fc.utobj().generateTestData(dataSet.get("franchiseId"));
			String franchiseId12 = addFranchiseLocation.addFranchiseLocation_All(driver, franchiseId1, regionName1,
					storeType1);
			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");

			CorporateUser corpUser2 = new CorporateUser();
			corpUser2 = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser2);
			corpUser2.setEmail(emailId);
			corpUser2 = corporateUser.createDefaultUser(driver, corpUser2);

			fc.utobj().printTestStep("Navigate To The Hub > Tasks Page");
			fc.utobj().printTestStep("Add Task");
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			addTask(driver, franchiseId, corpUser.getuserFullName(), subject, dataSet, comment, config);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Filter Record By Subject");

			showFilter(driver, subject);
			fc.utobj().clickElement(driver, pobj.searchBtn);
			fc.utobj().printTestStep("Modify Task");
			fc.utobj().actionImgOption(driver, subject, "Modify");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().selectDropDown(driver, pobj.franchiseeId, franchiseId12);
			fc.utobj().selectValFromMultiSelect(driver, pobj.assignToSelect, corpUser2.getuserFullName());
			fc.utobj().selectDropDown(driver, pobj.statusSelect, dataSet.get("status1"));
			fc.utobj().selectDropDown(driver, pobj.taskTypSelect, dataSet.get("taskType"));

			subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subject, subject);
			if (fc.utobj().isSelected(driver, pobj.timelessTaskIdCheck)) {
				fc.utobj().clickElement(driver, pobj.timelessTaskIdCheck);
			}
			fc.utobj().selectDropDown(driver, pobj.sTime, "1");
			fc.utobj().selectDropDown(driver, pobj.sMinute, "00 Min");
			fc.utobj().selectDropDown(driver, pobj.sAPM, "AM");
			fc.utobj().selectDropDown(driver, pobj.eTime, "2");
			fc.utobj().selectDropDown(driver, pobj.eMinute, "00 Min");
			fc.utobj().selectDropDown(driver, pobj.eAPM, "AM");

			fc.utobj().selectDropDown(driver, pobj.prioritySelect, dataSet.get("priority1"));
			fc.utobj().sendKeys(driver, pobj.startDate, fc.utobj().getCurrentDateUSFormat());
			comment = fc.utobj().generateTestData(dataSet.get("comment"));
			fc.utobj().sendKeys(driver, pobj.comments, comment);
			if (!fc.utobj().isSelected(driver,pobj.schduleTime.get(0))) {
				fc.utobj().clickElement(driver, pobj.schduleTime.get(0));
			}
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Filter Record By Subject");

			showFilter(driver, subject);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			fc.utobj().printTestStep("Verify The Modify Task");

			fc.utobj().isTextDisplayed(driver, subject, "was not able to verify subject of the task");
			fc.utobj().isTextDisplayed(driver, franchiseId12, "was not able to verify Associated With");
			fc.utobj().isTextDisplayed(driver, fc.utobj().getCurrentDateUSFormat(),
					"was not able to verify start Date");
			fc.utobj().isTextDisplayed(driver, dataSet.get("status1"), "task status");
			fc.utobj().isTextDisplayed(driver, dataSet.get("taskType"), "was not able to verify");
			fc.utobj().isTextDisplayed(driver, corpUser2.getuserFullName(), "was not able to verify AssignTo");

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void showFilter(WebDriver driver, String subject) throws Exception {
		TheHubTasksPage pobj = new TheHubTasksPage(driver);
		boolean isShowFilter = false;
		try {
			fc.utobj().getElement(driver, pobj.hideFilters).isDisplayed();
			fc.utobj().sendKeys(driver, pobj.subject, subject);
			isShowFilter = true;
		} catch (Exception e) {
			fc.utobj().clickElement(driver, pobj.showFilter);
			fc.utobj().sendKeys(driver, pobj.subject, subject);
			isShowFilter = true;
		}

		if (isShowFilter = false) {
			try {
				fc.utobj().clickElement(driver, pobj.showFilter);
				fc.utobj().sendKeys(driver, pobj.subject, subject);
			} catch (Exception e) {

			}
		}

	}

	@Test(groups = { "thehub","hub_task"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Delete Task By Action Image Option At The Hub > Tasks", testCaseId = "TC_209_Delete_Task")
	private void deleteTaskActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubTasksPage pobj = new TheHubTasksPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("Test209regionname");
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String franchiseId = fc.utobj().generateTestData(dataSet.get("franchiseId"));

			AdminFranchiseLocationAddFranchiseLocationPageTest addFranchiseLocation = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = addFranchiseLocation.addFranchiseLocation_All(driver, franchiseId, regionName, storeType);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "hubautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To The Hub > Tasks Page");
			fc.utobj().printTestStep("Add Task");
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			addTask(driver, franchiseId, corpUser.getuserFullName(), subject, dataSet, comment, config);

			fc.utobj().printTestStep("Filter Record By Subject");
			filterRecordBySubject(driver, subject);

			fc.utobj().printTestStep("Delete Task");
			fc.utobj().actionImgOption(driver, subject, "Delete");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Filter Record By Subject");
			fc.utobj().sendKeys(driver, pobj.subject, subject);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			fc.utobj().printTestStep("Verify The Delete Task");

			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , 'No records found.')]").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Delete Task");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub" ,"hub_task"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "View Task By Action Image Option At The Hub > Tasks", testCaseId = "TC_210_View_Task")
	private void viewTaskActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubTasksPage pobj = new TheHubTasksPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String franchiseId = fc.utobj().generateTestData(dataSet.get("franchiseId"));

			AdminFranchiseLocationAddFranchiseLocationPageTest addFranchiseLocation = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = addFranchiseLocation.addFranchiseLocation_All(driver, franchiseId, regionName, storeType);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "hubautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To The Hub > Tasks Page");
			fc.utobj().printTestStep("Add Task");
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			addTask(driver, franchiseId, corpUser.getuserFullName(), subject, dataSet, comment, config);

			fc.utobj().printTestStep("Filter Record By Subject");
			filterRecordBySubject(driver, subject);

			/*
			 * //search by subject fc.utobj().clickElement(driver,
			 * pobj.showFilter); fc.utobj().sendKeys(driver, pobj.subject,
			 * subject); fc.utobj().clickElement(driver, pobj.searchBtn);
			 * fc.utobj().clickElement(driver, pobj.hideFilters);
			 */

			fc.utobj().printTestStep("View Task");

			fc.utobj().actionImgOption(driver, subject, "View");

			fc.utobj().printTestStep("Verify View Task");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().isTextDisplayed(driver, subject, "was not able to verify subject");
			fc.utobj().isTextDisplayed(driver, franchiseId, "was not able to verify Associated To");
			fc.utobj().isTextDisplayed(driver, corpUser.getuserFullName(), "was not able to verify Assigned To");
			fc.utobj().isTextDisplayed(driver, dataSet.get("status"), "was not able to verify Status");
			fc.utobj().isTextDisplayed(driver, "FranConnect Administrator", "was not able to verify Added By");
			fc.utobj().isTextDisplayed(driver, fc.utobj().getCurrentDateUSFormat(),
					"was not able to verify Start Date Time");
			fc.utobj().isTextDisplayed(driver, dataSet.get("priority"), "was not able to verify Priority");
			fc.utobj().isTextDisplayed(driver, dataSet.get("taskType"), "was not able to verify Task Type");
			fc.utobj().isTextDisplayed(driver, comment, "was not able to verify Comment");
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub","hub_task"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Process Task By Action Image Option At The Hub > Tasks", testCaseId = "TC_211_Process_Task")
	private void processTasksActionImg() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubTasksPage pobj = new TheHubTasksPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String franchiseId = fc.utobj().generateTestData(dataSet.get("franchiseId"));

			AdminFranchiseLocationAddFranchiseLocationPageTest addFranchiseLocation = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = addFranchiseLocation.addFranchiseLocation_All(driver, franchiseId, regionName, storeType);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "hubautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultUser(driver, corpUser);

			fc.utobj().printTestStep("Navigate To The Hub > Tasks Page");
			fc.utobj().printTestStep("Add Task");
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			addTask(driver, franchiseId, corpUser.getuserFullName(), subject, dataSet, comment, config);

			fc.utobj().printTestStep("Filter Record By Subject");
			filterRecordBySubject(driver, subject);

			fc.utobj().printTestStep("Process The Task");

			fc.utobj().actionImgOption(driver, subject, "Process");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().isTextDisplayed(driver, subject, "was not able to verify subject");
			fc.utobj().isTextDisplayed(driver, franchiseId, "was not able to verify Associated To");
			fc.utobj().isTextDisplayed(driver, corpUser.getuserFullName(), "was not able to verify Assigned To");
			fc.utobj().isTextDisplayed(driver, fc.utobj().getCurrentDateUSFormat(),
					"was not able to verify Start Date Time");
			fc.utobj().isTextDisplayed(driver, dataSet.get("priority"), "was not able to verify Priority");
			fc.utobj().isTextDisplayed(driver, comment, "was not able to verify Comment");
			fc.utobj().selectDropDown(driver, pobj.statusSelect, dataSet.get("status1"));
			comment = fc.utobj().generateTestData(dataSet.get("comment"));
			fc.utobj().sendKeys(driver, pobj.comments, comment);
			fc.utobj().clickElement(driver, pobj.processBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Filter Record By Subject");
			fc.utobj().sendKeys(driver, pobj.subject, subject);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			fc.utobj().printTestStep("Verify The Process Task");

			fc.utobj().isTextDisplayed(driver, subject, "was not able to verify subject of the task");
			fc.utobj().isTextDisplayed(driver, franchiseId, "was not able to verify Associated With");
			fc.utobj().isTextDisplayed(driver, fc.utobj().getCurrentDateUSFormat(),
					"was not able to verify start Date");
			fc.utobj().isTextDisplayed(driver, dataSet.get("status1"), "task status");
			fc.utobj().isTextDisplayed(driver, dataSet.get("taskType"), "was not able to verify");
			fc.utobj().isTextDisplayed(driver, corpUser.getuserFullName(), "was not able to verify AssignTo");

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub","hub_task"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-07-24", testCaseDescription = "Modify Task At Pop Up Frame At The Hub > Tasks", testCaseId = "TC_212_Modify_Task")
	private void modifyTaskCboxIframe() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubTasksPage pobj = new TheHubTasksPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("TC222Region");
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String franchiseId = fc.utobj().generateTestData(dataSet.get("franchiseId"));

			AdminFranchiseLocationAddFranchiseLocationPageTest addFranchiseLocation = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = addFranchiseLocation.addFranchiseLocation_All(driver, franchiseId, regionName, storeType);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "hubautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName1 = fc.utobj().generateTestData(dataSet.get("regionName"));
			String storeType1 = fc.utobj().generateTestData(dataSet.get("storeType"));
			String franchiseId1 = fc.utobj().generateTestData(dataSet.get("franchiseId"));
			String franchiseId12 = addFranchiseLocation.addFranchiseLocation_All(driver, franchiseId1, regionName1,
					storeType1);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");

			CorporateUser corpUser2 = new CorporateUser();
			corpUser2 = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser2);
			corpUser2.setEmail(emailId);
			corpUser2 = corporateUser.createDefaultUser(driver, corpUser2);

			fc.utobj().printTestStep("Navigate To The Hub > Tasks Page");
			fc.utobj().printTestStep("Add Task");
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			addTask(driver, franchiseId, corpUser.getuserFullName(), subject, dataSet, comment, config);

			fc.utobj().printTestStep("Filter Record By Subject");
			filterRecordBySubject(driver, subject);

			fc.utobj().printTestStep("Modify Task At Cbox");

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, subject));
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.modifyLink);
			fc.utobj().selectDropDown(driver, pobj.franchiseeId, franchiseId12);
			fc.utobj().selectValFromMultiSelect(driver, pobj.assignToSelect, corpUser2.getuserFullName());
			fc.utobj().selectDropDown(driver, pobj.statusSelect, dataSet.get("status1"));
			fc.utobj().selectDropDown(driver, pobj.taskTypSelect, dataSet.get("taskType"));
			subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subject, subject);
			if (fc.utobj().isSelected(driver, pobj.timelessTaskIdCheck)) {
				fc.utobj().clickElement(driver, pobj.timelessTaskIdCheck);
			}

			fc.utobj().selectDropDown(driver, pobj.sTime, "1");
			fc.utobj().selectDropDown(driver, pobj.sMinute, "00 Min");
			fc.utobj().selectDropDown(driver, pobj.sAPM, "AM");
			fc.utobj().selectDropDown(driver, pobj.eTime, "2");
			fc.utobj().selectDropDown(driver, pobj.eMinute, "00 Min");
			fc.utobj().selectDropDown(driver, pobj.eAPM, "AM");

			fc.utobj().selectDropDown(driver, pobj.prioritySelect, dataSet.get("priority1"));
			fc.utobj().sendKeys(driver, pobj.startDate, fc.utobj().getCurrentDateUSFormat());
			comment = fc.utobj().generateTestData(dataSet.get("comment"));
			fc.utobj().sendKeys(driver, pobj.comments, comment);
			if (!fc.utobj().isSelected(driver,pobj.schduleTime.get(0))) {
				fc.utobj().clickElement(driver, pobj.schduleTime.get(0));
			}
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Filter Record By Subject");
			fc.utobj().sendKeys(driver, pobj.subject, subject);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			fc.utobj().printTestStep("Verify The Modify The Task");

			fc.utobj().isTextDisplayed(driver, subject, "was not able to verify subject of the task");
			fc.utobj().isTextDisplayed(driver, franchiseId12, "was not able to verify Associated With");
			fc.utobj().isTextDisplayed(driver, fc.utobj().getCurrentDateUSFormat(),
					"was not able to verify start Date");
			fc.utobj().isTextDisplayed(driver, dataSet.get("status1"), "task status");
			fc.utobj().isTextDisplayed(driver, dataSet.get("taskType"), "was not able to verify");
			fc.utobj().isTextDisplayed(driver, corpUser2.getuserFullName(), "was not able to verify AssignTo");

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub","hub_task"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-11-13", testCaseDescription = "Complete Taks at Pop up Iframe At The Hub > Tasks", testCaseId = "TC_213_Complete_Task")
	private void completeTaskCboxIframe() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubTasksPage pobj = new TheHubTasksPage(driver);

			AdminConfigurationConfigureTaskEmailsPageTest task_email = new AdminConfigurationConfigureTaskEmailsPageTest();

			fc.utobj().printTestStep("NaVigate To Admin > Configuration > Configure Task Emails");
			fc.utobj().printTestStep("Configure Task Completion Emails");
			String emailSubject = task_email.configureTaskCompletionEmail(driver);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "hubautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String franchiseId = fc.utobj().generateTestData(dataSet.get("franchiseId"));

			AdminFranchiseLocationAddFranchiseLocationPageTest addFranchiseLocation = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = addFranchiseLocation.addFranchiseLocation_All(driver, franchiseId, regionName, storeType);

			fc.utobj().printTestStep("Navigate To The Hub > Tasks Page");
			fc.utobj().printTestStep("Add Task");
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			addTask(driver, franchiseId, corpUser.getuserFullName(), subject, dataSet, comment, config);

			fc.utobj().printTestStep("Filter Record By Subject");
			filterRecordBySubject(driver, subject);

			fc.utobj().printTestStep("Complete Task");

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, subject));
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.completeLink);

			String comments = fc.utobj().generateTestData("Test");

			fc.utobj().sendKeys(driver, pobj.comments, comments);

			fc.utobj().clickElement(driver, pobj.processBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Filter Record By Subject");
			fc.utobj().sendKeys(driver, pobj.subject, subject);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			fc.utobj().printTestStep("Verify The Complete Task");

			fc.utobj().isTextDisplayed(driver, subject, "was not able to verify subject of the task");
			fc.utobj().isTextDisplayed(driver, franchiseId, "was not able to verify Associated With");
			fc.utobj().isTextDisplayed(driver, fc.utobj().getCurrentDateUSFormat(),
					"was not able to verify start Date");
			fc.utobj().isTextDisplayed(driver, "Complete", "task status");
			fc.utobj().isTextDisplayed(driver, dataSet.get("taskType"), "was not able to verify");
			fc.utobj().isTextDisplayed(driver, corpUser.getuserFullName(), "was not able to verify AssignTo");

			fc.utobj().printTestStep("Verify The Mail After Task Completion");

			Map<String, String> mailIinfo = fc.utobj().readMailBox(emailSubject, comments, emailId, "sdg@1a@Hfs");

			if (mailIinfo.size() == 0 || !mailIinfo.get("mailBody").contains(subject)) {
				fc.utobj().throwsException("was not able to verify Task Completion Mail");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub" ,"hub_task"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Change Status By Action Button Option At The Hub > Tasks", testCaseId = "TC_214_Change_Status")
	private void changeStatusActionBtn() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubTasksPage pobj = new TheHubTasksPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "hubautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String franchiseId = fc.utobj().generateTestData(dataSet.get("franchiseId"));

			AdminFranchiseLocationAddFranchiseLocationPageTest addFranchiseLocation = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = addFranchiseLocation.addFranchiseLocation_All(driver, franchiseId, regionName, storeType);

			fc.utobj().printTestStep("Navigate To The Hub > Tasks Page");
			fc.utobj().printTestStep("Add Task");
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			addTask(driver, franchiseId, corpUser.getuserFullName(), subject, dataSet, comment, config);

			fc.utobj().printTestStep("Filter Record By Subject");
			filterRecordBySubject(driver, subject);

			fc.utobj().printTestStep("Change Status");
			WebElement element = fc.utobj().getElementByXpath(driver,
					".//a[.='" + subject + "']/ancestor::tr/td/input[@name='checkb']");
			fc.utobj().clickElement(driver, element);
			fc.utobj().selectActionMenuItems(driver, "Change Status");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().selectDropDown(driver, pobj.statusSelect, dataSet.get("status1"));
			comment = fc.utobj().generateTestData(dataSet.get("comment"));
			fc.utobj().sendKeys(driver, pobj.comments, comment);
			fc.utobj().clickElement(driver, pobj.changeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Filter Record By Subject");
			// search by subject
			fc.utobj().sendKeys(driver, pobj.subject, subject);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			fc.utobj().printTestStep("Verify The Change Status");
			fc.utobj().isTextDisplayed(driver, subject, "was not able to verify subject of the task");
			fc.utobj().isTextDisplayed(driver, franchiseId, "was not able to verify Associated With");
			fc.utobj().isTextDisplayed(driver, fc.utobj().getCurrentDateUSFormat(),
					"was not able to verify start Date");
			fc.utobj().isTextDisplayed(driver, dataSet.get("status1"), "task status");
			fc.utobj().isTextDisplayed(driver, dataSet.get("taskType"), "was not able to verify");
			fc.utobj().isTextDisplayed(driver, corpUser.getuserFullName(), "was not able to verify AssignTo");

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub" ,"hub_task"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Delete Task By Action Button Option At The Hub > Tasks ", testCaseId = "TC_215_Delete_Task")
	private void deleteTaskActionBtn() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubTasksPage pobj = new TheHubTasksPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String franchiseId = fc.utobj().generateTestData(dataSet.get("franchiseId"));

			AdminFranchiseLocationAddFranchiseLocationPageTest addFranchiseLocation = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = addFranchiseLocation.addFranchiseLocation_All(driver, franchiseId, regionName, storeType);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "hubautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To The Hub > Tasks Page");
			fc.utobj().printTestStep("Add Task");
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			addTask(driver, franchiseId, corpUser.getuserFullName(), subject, dataSet, comment, config);

			fc.utobj().printTestStep("Filter Record By Subject");
			filterRecordBySubject(driver, subject);

			/*
			 * //search by subject fc.utobj().clickElement(driver,
			 * pobj.showFilter); fc.utobj().sendKeys(driver, pobj.subject,
			 * subject); fc.utobj().clickElement(driver, pobj.searchBtn);
			 * fc.utobj().clickElement(driver, pobj.hideFilters);
			 */

			fc.utobj().printTestStep("Delete The Task");
			WebElement element = fc.utobj().getElementByXpath(driver,
					".//a[.='" + subject + "']/ancestor::tr/td/input[@name='checkb']");
			fc.utobj().clickElement(driver, element);
			fc.utobj().selectActionMenuItems(driver, "Delete");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Filter Record By Subject");
			// search by subject
			fc.utobj().sendKeys(driver, pobj.subject, subject);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			fc.utobj().printTestStep("Verify The Delete Task");
			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , 'No records found.')]").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Delete Task");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub" ,"hub_task"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Change Status By Bottom Button At The Hub > Tasks", testCaseId = "TC_216_Change_Status")
	private void changeStatusBottomBtn() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubTasksPage pobj = new TheHubTasksPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "hubautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String franchiseId = fc.utobj().generateTestData(dataSet.get("franchiseId"));

			AdminFranchiseLocationAddFranchiseLocationPageTest addFranchiseLocation = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = addFranchiseLocation.addFranchiseLocation_All(driver, franchiseId, regionName, storeType);

			fc.utobj().printTestStep("Navigate To The Hub > Tasks Page");
			fc.utobj().printTestStep("Add Task");
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			addTask(driver, franchiseId, corpUser.getuserFullName(), subject, dataSet, comment, config);

			fc.utobj().printTestStep("Filter Record By Subject");
			filterRecordBySubject(driver, subject);

			fc.utobj().printTestStep("Change Status");
			WebElement element = fc.utobj().getElementByXpath(driver,
					".//a[.='" + subject + "']/ancestor::tr/td/input[@name='checkb']");
			fc.utobj().clickElement(driver, element);
			fc.utobj().clickElement(driver, pobj.statusChangedBtmBtn);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().selectDropDown(driver, pobj.statusSelect, dataSet.get("status1"));
			comment = fc.utobj().generateTestData(dataSet.get("comment"));
			fc.utobj().sendKeys(driver, pobj.comments, comment);
			fc.utobj().clickElement(driver, pobj.changeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Filter Record By Subject");

			fc.utobj().sendKeys(driver, pobj.subject, subject);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			fc.utobj().printTestStep("Verify Change Status");
			fc.utobj().isTextDisplayed(driver, subject, "was not able to verify subject of the task");
			fc.utobj().isTextDisplayed(driver, franchiseId, "was not able to verify Associated With");
			fc.utobj().isTextDisplayed(driver, fc.utobj().getCurrentDateUSFormat(),
					"was not able to verify start Date");
			fc.utobj().isTextDisplayed(driver, dataSet.get("status1"), "task status");
			fc.utobj().isTextDisplayed(driver, dataSet.get("taskType"), "was not able to verify");
			fc.utobj().isTextDisplayed(driver, corpUser.getuserFullName(), "was not able to verify AssignTo");

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub" ,"hub_task"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Delete Task By Bottom Button At The Hub > Tasks ", testCaseId = "TC_217_Delete_Task")
	private void deleteTaskBottomBtn() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubTasksPage pobj = new TheHubTasksPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String franchiseId = fc.utobj().generateTestData(dataSet.get("franchiseId"));

			AdminFranchiseLocationAddFranchiseLocationPageTest addFranchiseLocation = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = addFranchiseLocation.addFranchiseLocation_All(driver, franchiseId, regionName, storeType);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "hubautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To The Hub > Tasks Page");
			fc.utobj().printTestStep("Add Task");
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			addTask(driver, franchiseId, corpUser.getuserFullName(), subject, dataSet, comment, config);

			fc.utobj().printTestStep("Filter Record By Subject");
			filterRecordBySubject(driver, subject);

			/*
			 * //search by subject fc.utobj().clickElement(driver,
			 * pobj.showFilter); fc.utobj().sendKeys(driver, pobj.subject,
			 * subject); fc.utobj().clickElement(driver, pobj.searchBtn);
			 * fc.utobj().clickElement(driver, pobj.hideFilters);
			 */

			fc.utobj().printTestStep("Delete Task");
			WebElement element = fc.utobj().getElementByXpath(driver,
					".//a[.='" + subject + "']/ancestor::tr/td/input[@name='checkb']");
			fc.utobj().clickElement(driver, element);
			fc.utobj().clickElement(driver, pobj.deleteBottomBtn);
			fc.utobj().acceptAlertBox(driver);

			// search by subject
			fc.utobj().printTestStep("Filter Record By Subject");
			fc.utobj().sendKeys(driver, pobj.subject, subject);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			fc.utobj().printTestStep("Verify The Delete Task");
			if (!fc.utobj().getElementByXpath(driver, ".//td[contains(text () , 'No records found.')]").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Delete Task");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub" ,"hub_task"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Reassign Task At The Hub > Tasks", testCaseId = "TC_265_Reassign_Task")
	private void reassignTheTask() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("thehub", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			TheHubTasksPage pobj = new TheHubTasksPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String franchiseId = fc.utobj().generateTestData(dataSet.get("franchiseId"));

			AdminFranchiseLocationAddFranchiseLocationPageTest addFranchiseLocation = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = addFranchiseLocation.addFranchiseLocation_All(driver, franchiseId, regionName, storeType);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "hubautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");

			CorporateUser corpUser2 = new CorporateUser();
			corpUser2 = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser2);
			corpUser2.setEmail(emailId);
			corpUser2 = corporateUser.createDefaultUser(driver, corpUser2);

			fc.utobj().printTestStep("Navigate To The Hub > Tasks Page");
			fc.utobj().printTestStep("Add Task");
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			addTask(driver, franchiseId, corpUser.getuserFullName(), subject, dataSet, comment, config);

			fc.utobj().printTestStep("Filter Record By Subject");
			filterRecordBySubject(driver, subject);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Delete User");
			fc.adminpage().adminUsersManageCorporateUsersPage(driver);
			fc.utobj().sendKeys(driver, pobj.searchCorUser, corpUser.getuserFullName());
			fc.utobj().clickElement(driver, pobj.searchBtnAtCorPage);
			fc.utobj().actionImgOption(driver, corpUser.getuserFullName(), "Delete");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Reassign The Task To Another User");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.reassignBtn);
			fc.utobj().selectDropDown(driver, pobj.intranetTaskCountlist, corpUser2.getuserFullName());
			fc.utobj().clickElement(driver, pobj.reassignBtn);
			fc.utobj().clickElement(driver, pobj.closeBtnAtCPage);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Navigate To The Hub > Tasks Page");
			fc.hub().hub_common().theHubTaskSubModule(driver);

			fc.utobj().printTestStep("Filter Record By Subject");

			try {
				fc.utobj().clickElement(driver, pobj.showFilter);
			} catch (Exception e) {
			}
			fc.utobj().sendKeys(driver, pobj.subject, subject);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			fc.utobj().printTestStep("Verify The Reassign Task");
			fc.utobj().isTextDisplayed(driver, subject, "was not able to verify subject of the task");
			fc.utobj().isTextDisplayed(driver, franchiseId, "was not able to verify Associated With");
			fc.utobj().isTextDisplayed(driver, dataSet.get("status"), "task status");
			fc.utobj().isTextDisplayed(driver, dataSet.get("taskType"), "was not able to verify");
			fc.utobj().isTextDisplayed(driver, corpUser2.getuserFullName(), "was not able to verify AssignTo");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
}
