package com.builds.test.thehub;

import org.openqa.selenium.WebDriver;
import org.testng.Reporter;

import com.builds.test.admin.AdminPageTest;
import com.builds.test.common.FCHomePageTest;
import com.builds.uimaps.common.FCHomePage;
import com.builds.utilities.FranconnectUtil;

public class TheHub_Common extends FranconnectUtil {

	public void theHubModule(WebDriver driver) throws Exception {
		Reporter.log("Navigating to theHubModule...");
		// Opens The Hub Page

		home_page().openTheHubModule(driver);
	}

	public void theHubTaskSubModule(WebDriver driver) throws Exception {
		Reporter.log("Navigating to theHubTaskSubModule...");
		// Opens The Hub Page
		theHubModule(driver);

		hub().hub_common().openTheHubTaskSubModule(home_page(), driver);
	}

	public void theHubWhatsNew(WebDriver driver) throws Exception {
		Reporter.log("Navigating to theHubWhatsNew...");
		// Opens The Hub Page
		theHubModule(driver);

		hub().hub_common().openTheHubWhatsNew(home_page(), driver);
	}

	public void theHubHome(WebDriver driver) throws Exception {
		Reporter.log("Navigating to theHubHome...");
		// Opens The Hub Page
		theHubModule(driver);
		hub().hub_common().openTheHubHome(home_page(), driver);
	}

	public void intranetModule(WebDriver driver) throws Exception {
		Reporter.log("Navigating to intranetModule...");
		// Opens Intranet Page

		home_page().openIntranetModule(driver);
	}

	public void intranetHomeSubModule(WebDriver driver) throws Exception {
		Reporter.log("Navigating to intranetHomeSubModule...");
		// Intranet > Home Page
		intranetModule(driver);
		TheHubSubModulesPageTest p1 = new TheHubSubModulesPageTest();
		p1.openHomePage(driver);
	}

	public void theHubDirectorySubModule(WebDriver driver) throws Exception {
		Reporter.log("Navigating to theHubDirectorySubModule...");
		// Intranet > Directory
		intranetModule(driver);
		TheHubSubModulesPageTest p1 = new TheHubSubModulesPageTest();
		p1.openDirectoryPage(driver);

	}

	public void theHubAlertsSubModule(WebDriver driver) throws Exception {
		Reporter.log("Navigating to theHubAlertsSubModule...");
		// Intranet > Alerts
		intranetModule(driver);
		TheHubSubModulesPageTest p1 = new TheHubSubModulesPageTest();
		p1.openAlertsPage(driver);

	}

	public void theHubMessageSubModule(WebDriver driver) throws Exception {
		Reporter.log("Navigating to thehubMessageSubModule...");
		// Intranet > Message
		intranetModule(driver);
		TheHubSubModulesPageTest p1 = new TheHubSubModulesPageTest();
		p1.openMessagesPage(driver);

	}

	public void intranetMessageGroupSubModule(WebDriver driver) throws Exception {
		Reporter.log("Navigating to intranetMessageGroupSubModule...");
		// Intranet > Message > Groups
		theHubMessageSubModule(driver);
		Thread.sleep(2000);

	}

	public void intranetMessageFolderSubModule(WebDriver driver) throws Exception {
		Reporter.log("Navigating to intranetMessageFolderSubModule...");
		// Intranet > Message > Folders
		theHubMessageSubModule(driver);
		Thread.sleep(2000);
	}

	public void theHubCalendarSubModule(WebDriver driver) throws Exception {
		Reporter.log("Navigating to theHubCalendarSubModule...");
		// Intranet > Calender
		intranetModule(driver);
		TheHubSubModulesPageTest p1 = new TheHubSubModulesPageTest();
		p1.openCalendarPage(driver);
	}

	public void theHubNewsSubModule(WebDriver driver) throws Exception {
		Reporter.log("Navigating to intranetNewsSubModule...");
		// Intranet > News
		intranetModule(driver);
		TheHubSubModulesPageTest p1 = new TheHubSubModulesPageTest();
		p1.openNewsPage(driver);

	}

	public void theHubLibrarySubModule(WebDriver driver) throws Exception {
		Reporter.log("Navigating to intranetLibrarySubModule...");
		// Intranet > Library
		intranetModule(driver);
		TheHubSubModulesPageTest p1 = new TheHubSubModulesPageTest();
		p1.openLibraryPage(driver);

	}

	public void intranetForumsSubModule(WebDriver driver) throws Exception {
		Reporter.log("Navigating to intranetForumsSubModule...");
		// Intranet > Library
		intranetModule(driver);
		TheHubSubModulesPageTest p1 = new TheHubSubModulesPageTest();
		p1.openForumsPage(driver);

	}

	public void theHubFranBuzzSubModule(WebDriver driver) throws Exception {
		Reporter.log("Navigating to theHubFranBuzzSubModule...");
		// Intranet > Library
		intranetModule(driver);
		TheHubSubModulesPageTest p1 = new TheHubSubModulesPageTest();
		p1.openFranBuzzPage(driver);
	}

	public void theHubRelatedLinksSubModule(WebDriver driver) throws Exception {
		Reporter.log("Navigating to theHubRelatedLinksSubModule...");
		// Intranet > RelatedLinks
		intranetModule(driver);
		TheHubSubModulesPageTest p1 = new TheHubSubModulesPageTest();
		p1.openRelatedLinksPage(driver);

	}

	public void intranetWhatsNewSubModule(WebDriver driver) throws Exception {
		Reporter.log("Navigating to intranetWhatsNewSubModule...");
		// Intranet > WhatsNew
		intranetModule(driver);
		TheHubSubModulesPageTest p1 = new TheHubSubModulesPageTest();
		p1.openWhatsNewPage(driver);

	}

	public void adminTheHubStoriesRSSFeedsandHomePageView(WebDriver driver) throws Exception {
		adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openStoriesRSSFeedsandHomePageViewLnk(driver);
		Reporter.log("Navigating to adminTheHubStoriesRSSFeedsandHomePageView...");
	}

	public void adminTheHubNewsPage(WebDriver driver) throws Exception {

		adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openTheHubNewsLnk(driver);
		Reporter.log("Navigating to adminTheHubNewsPage...");
	}

	public void adminTheHubLibraryPage(WebDriver driver) throws Exception {
		adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openIntranetFolderLnk(driver);
		Reporter.log("Navigating to adminIntranetLibraryPage...");
	}

	public void adminTheHubLibraryLogsPage(WebDriver driver) throws Exception {

		adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openHubLibraryLogsLnk(driver);
		Reporter.log("Navigating to Admin The Hub Library Logs...");
	}

	public void adminTheHubEPollNewEPollPage(WebDriver driver) throws Exception {

		adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openIntranetEPollLnk(driver);
		Reporter.log("Navigating to adminIntranetEPollNewEPollPage...");
	}

	public void adminTheHubRelatedLinksPage(WebDriver driver) throws Exception {
		adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openRelatedLinksLnk(driver);
		Reporter.log("Navigating to adminTheHubRelatedLinksPage...");
	}

	public void adminTheHubArchiveAlertsPage(WebDriver driver) throws Exception {
		adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openArchiveAlertsLnk(driver);
		Reporter.log("Navigating to adminTheHubArchiveAlertsLinksPage...");
	}

	public void adminTheHubArchiveMessagesPage(WebDriver driver) throws Exception {
		adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openArchiveMessagesLnk(driver);
		Reporter.log("Navigating to adminTheHubArchiveMessagesLinksPage...");
	}
	
	public void adminTheHubConfigureServerPage(WebDriver driver) throws Exception {
		adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openConfigureServerLnk(driver);
		Reporter.log("Navigating to adminTheHubConfigureServerPage...");
	}

	public void adminTheHubNewsLogsPage(WebDriver driver) throws Exception {
		adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openNewsLogsLnk(driver);
		Reporter.log("Navigating to Admin The Hub News Logs...");
	}

	public void adminTheHubConfigureFranBuzzNamePage(WebDriver driver) throws Exception {
		adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openConfigureFranBuzzNameLnk(driver);
		Reporter.log("Navigating to adminTheHubConfigureFranBuzzNamePage...");
	}

	public void adminTheHubConfigureSingleSignOnPage(WebDriver driver) throws Exception {
		adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openConfigureSingleSignOnLnk(driver);
		Reporter.log("Navigating to adminTheHubConfigureSingleSignOnPage...");
	}

	public void openTheHubHome(FCHomePageTest fcHomePageTest, WebDriver driver) throws Exception {

		FCHomePage pobj = new FCHomePage(driver);
		fcHomePageTest.utobj().sleep(5000);
		fcHomePageTest.utobj().clickElementByJS(driver, pobj.theHubHome);
		Reporter.log("Navigating to The Hub Home Page...");
	}

	public void openTheHubWhatsNew(FCHomePageTest fcHomePageTest, WebDriver driver) throws Exception {

		FCHomePage pobj = new FCHomePage(driver);
		fcHomePageTest.utobj().sleep(5000);
		fcHomePageTest.utobj().clickElement(driver, pobj.theHubWhatsNew);
		Reporter.log("Navigating to The Hub Whats New Page...");
	}

	public void openTheHubTaskSubModule(FCHomePageTest fcHomePageTest, WebDriver driver) throws Exception {

		FCHomePage pobj = new FCHomePage(driver);
		fcHomePageTest.utobj().sleep(5000);
		fcHomePageTest.utobj().clickElement(driver, pobj.theHubTask);
		Reporter.log("Navigating to The Hub Task Page...");
	}

}
