package com.builds.test.training;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminDivisionAddDivisionPageTest;
import com.builds.uimaps.training.AdminTrainingPlanAndCertificatePage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;
import com.hp.hpl.jena.sparql.engine.Plan;

public class AdminPlanAndCertificateNewFile {
	FranconnectUtil fc = new FranconnectUtil();
	// hell

	@Test(groups = { "training", "trainingsmoke"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Create Plan ", testCaseId = "TC_Create_Plan_Before_Date")
	private void CreateCPlan() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("training", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			driver = fc.loginpage().login(driver);
			AdminTrainingPlanAndCertificatePage pobj = new AdminTrainingPlanAndCertificatePage(driver);
			fc.utobj().printTestStep("Navigating to Admin > Training > Question Library Page");
			fc.training().training_common().adminTraingPlanAndCertificatesPage(driver);
			fc.utobj().clickElement(driver, pobj.Create);
			fc.utobj().clickElement(driver, pobj.CreatePlan);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			String planName = dataSet.get("PlanName") + fc.utobj().generateRandomNumber();
			fc.utobj().sendKeys(driver, pobj.planName, planName);
			fc.utobj().sendKeys(driver, pobj.Description, dataSet.get("Description"));
			
			fc.utobj().printTestStep("Enter Start date less than current date");
			fc.utobj().clickElement(driver, pobj.startdate);
			DateFormat dateFormat2 = new SimpleDateFormat("dd");
			Date date2 = new Date();
			int day = Integer.parseInt(dateFormat2.format(date2));
			day++;
			String today = String.valueOf(day);
			int dayis = Integer.parseInt(today);// ravi
			dayis = dayis - 4;
			if (dayis > 0) {
				// find the calendar
				WebElement dateWidget = fc.utobj().getElementByID(driver, "fc-datepicker-div");
				List<WebElement> columns = dateWidget.findElements(By.tagName("a"));

				// comparing the text of cell with today's date and clicking it.
				for (WebElement cell : columns) {
					if (cell.getText().equals(String.valueOf(dayis))) {
						cell.click();
						break;
					}
				}
			}
			
			fc.utobj().printTestStep("Enter End date");
			fc.utobj().clickElement(driver, pobj.enddate);
			dateFormat2 = new SimpleDateFormat("dd");
			date2 = new Date();

			day = Integer.parseInt(dateFormat2.format(date2));
			day++;
			today = String.valueOf(day);
			dayis = Integer.parseInt(today);
			if (dayis > 0 && dayis < 31) {

				// find the calendar
				WebElement dateWidget = fc.utobj().getElementByID(driver, "fc-datepicker-div");
				List<WebElement> columns1 = dateWidget.findElements(By.tagName("a"));

				// comparing the text of cell with today's date and clicking it.
				for (WebElement cell : columns1) {
					if (cell.getText().equals(String.valueOf(dayis))) {
						cell.click();
						break;
					}
				}
			}

			
			fc.utobj().clickElement(driver, pobj.savePlan);
			boolean check = fc.utobj().assertPageSource(driver, "Start date can not be less than current date");
			if (check) {
				fc.utobj().clickElement(driver, pobj.cancelPlan);
				fc.utobj().switchFrameToDefault(driver);
				fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			} else {
				throw new Exception("msg is not appering for backdate");
			}

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "training", "trainingsmoke" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Create Plan ", testCaseId = "TC_Create_Plan_future_Date")
	private void CreateFPlan() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("training", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			driver = fc.loginpage().login(driver);
			AdminDivisionAddDivisionPageTest p1 = new AdminDivisionAddDivisionPageTest();
			AdminTrainingPlanAndCertificatePage pobj = new AdminTrainingPlanAndCertificatePage(driver);
			fc.utobj().printTestStep("Navigating to Admin > Training > Question Library Page");
			fc.training().training_common().adminTraingPlanAndCertificatesPage(driver);
			fc.utobj().clickElement(driver, pobj.Create);
			fc.utobj().clickElement(driver, pobj.CreatePlan);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			String planName = dataSet.get("PlanName") + fc.utobj().generateRandomNumber();
			fc.utobj().sendKeys(driver, pobj.planName, planName);
			fc.utobj().sendKeys(driver, pobj.Description, dataSet.get("Description"));
			fc.utobj().clickElement(driver, pobj.startdate);
			DateFormat dateFormat2 = new SimpleDateFormat("dd");
			Date date2 = new Date();

			int day = Integer.parseInt(dateFormat2.format(date2));
			day++;
			String today = String.valueOf(day);
			int dayis = Integer.parseInt(today);
			dayis += 2;

			// find the calendar
			WebElement dateWidget = fc.utobj().getElementByID(driver, "fc-datepicker-div");
			List<WebElement> columns = dateWidget.findElements(By.tagName("a"));

			// comparing the text of cell with today's date and clicking it.
			for (WebElement cell : columns) {
				if (cell.getText().equals(String.valueOf(dayis))) {
					cell.click();
					break;
				}
			}

			fc.utobj().clickElement(driver, pobj.enddate);
			dateFormat2 = new SimpleDateFormat("dd");
			date2 = new Date();

			day = Integer.parseInt(dateFormat2.format(date2));
			day++;
			today = String.valueOf(day);
			dayis = Integer.parseInt(today);
			dayis += 3;

			// find the calendar
			dateWidget = fc.utobj().getElementByID(driver, "fc-datepicker-div");
			List<WebElement> columns1 = dateWidget.findElements(By.tagName("a"));

			// comparing the text of cell with today's date and clicking it.
			for (WebElement cell : columns1) {
				if (cell.getText().equals(String.valueOf(dayis))) {
					cell.click();
					break;
				}
			}

			fc.utobj().clickElement(driver, pobj.savePlan);
			fc.training().training_common().adminTraingPlanAndCertificatesPage(driver);
			fc.utobj().clickElement(driver, pobj.viewperpage);
			fc.utobj().clickElement(driver, ".//a[contains(text(),'500')]");
			fc.utobj().clickElement(driver, pobj.FilterBtn);
			fc.utobj().sendKeys(driver, pobj.planName, planName);
			fc.utobj().clickElement(driver, pobj.applyFilters);
			fc.utobj().clickPartialLinkText(driver, planName);
			boolean check = fc.utobj().assertPageSource(driver, dataSet.get("Description"));
			if (!check) {
				throw new Exception("while clicking on Plan name description not came");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "training", "trainingsmoke" ,"trainingFailed0924"})
	@TestCase(createdOn = "2018-06-28", updatedOn = "2018-06-28", testCaseDescription = "Verify the Create Plan ", testCaseId = "TC_Create_Plan_with_User")
	private void CreatePlan() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("training", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			driver = fc.loginpage().login(driver);
			AdminDivisionAddDivisionPageTest p1 = new AdminDivisionAddDivisionPageTest();
			String divisionName = fc.utobj().generateTestData("divisionName");
			String zipCode = fc.utobj().generateTestData("zipCode");
			p1.addDivisionByGeographyZipCode(driver, divisionName, zipCode, config);
			AdminTrainingPlanAndCertificatePage pobj = new AdminTrainingPlanAndCertificatePage(driver);
			fc.utobj().printTestStep("Navigating to Admin > Training > Question Library Page");
			com.builds.test.training.Plan c = new com.builds.test.training.Plan();
			c.setCategoryCorporateRole(driver, config);
			c.setCategoryFranchiseRole(driver, config);
			c.setCategoryDivisionRole(driver, config);
			c.setCategoryRegionalRole(driver, config);
			fc.training().training_common().adminTraingPlanAndCertificatesPage(driver);
			fc.utobj().clickElement(driver, pobj.Create);
			fc.utobj().clickElement(driver, pobj.CreatePlan);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			boolean check = fc.utobj().assertPageSource(driver, "Fields marked with");
			if (!check) {
				throw new Exception("Mandatory field value is not found ");
			}
			String planName = dataSet.get("PlanName") + fc.utobj().generateRandomNumber();
			fc.utobj().sendKeys(driver, pobj.planName, planName);
			fc.utobj().clickElement(driver, ".//*//label[@data-target='.configure-roles-contaione']");
			boolean invalidrole = false;

			if (!invalidrole) {
				try {
					fc.utobj().clickElement(driver,
							fc.utobj().getElementByXpath(driver, ".//*[@id='fc-drop-parentcorRoles']/button"));
					fc.utobj().clickElement(driver, ".//*[@id='fc-drop-parentcorRoles']/div/ul//span[contains(text(), '"
							+ c.getCategoryCorporateRole() + "')]");

				} catch (Exception e) {
					invalidrole = true;
					fc.utobj().printTestStep("Searching for invalid corp role doesn't work");
				}

				fc.utobj().sleep(1000);
			}
			if (!invalidrole) {

				try {
					fc.utobj().clickElement(driver,
							fc.utobj().getElementByXpath(driver, ".//*[@id='fc-drop-parentregionalRoles']/button"));
					fc.utobj().clickElement(driver,
							".//*[@id='fc-drop-parentregionalRoles']/div/ul//span[contains(text(), '"
									+ c.getCategoryRegionalRole() + "')]");
					fc.utobj().sleep(1000);

				} catch (Exception e) {
					invalidrole = true;
					fc.utobj().printTestStep("Searching for invalid Regional role doesn't work");

				}
				fc.utobj().sleep(1000);
			}
			if (!invalidrole) {
				try {
					fc.utobj().clickElement(driver,
							fc.utobj().getElementByXpath(driver, ".//*[@id='fc-drop-parentdivisionalRoles']/button"));
					fc.utobj().clickElement(driver,
							".//*[@id='fc-drop-parentdivisionalRoles']/div/ul//span[contains(text(), '"
									+ c.getCategoryDivisionRole() + "')]");
				} catch (Exception e) {
					invalidrole = true;
					fc.utobj().printTestStep("Searching for invalid role doesn't work");
				}
				fc.utobj().sleep(1000);
			}
			if (!invalidrole) {

				try {
					fc.utobj().clickElement(driver,
							fc.utobj().getElementByXpath(driver, ".//*[@id='fc-drop-parentroles']/button"));
					fc.utobj().clickElement(driver, ".//*[@id='fc-drop-parentroles']/div/ul//span[contains(text(), '"
							+ c.getCategoryFranchiseRole() + "')]");

				} catch (Exception e) {
					fc.utobj().printTestStep("Searching for invalid role doesn't work");
				}

				fc.utobj().sleep(1000);
			}

			if (invalidrole) {
				throw new Exception("invalid role got selected");
			}

			fc.utobj().sendKeys(driver, pobj.Description, dataSet.get("Description"));

			// List<WebElement> li = new LinkedList<WebElement>();
			// li.add(pobj.startdate);
			// li.add(pobj.enddate);
			// fc.utobj().isElementPresent(li);
			fc.utobj().clickElement(driver, pobj.savePlan);
			fc.training().training_common().adminTraingPlanAndCertificatesPage(driver);
			fc.utobj().clickElement(driver, pobj.viewperpage);
			fc.utobj().clickElement(driver, ".//a[contains(text(),'500')]");
			fc.utobj().clickElement(driver, pobj.FilterBtn);
			fc.utobj().sendKeys(driver, pobj.planName, planName);
			fc.utobj().clickElement(driver, pobj.applyFilters);
			fc.utobj().clickPartialLinkText(driver, planName);
			check = fc.utobj().assertPageSource(driver, dataSet.get("Description"));
			if (!check) {
				throw new Exception("while clicking on Plan name description not came");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
}
