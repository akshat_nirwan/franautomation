package com.builds.test.training;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

import com.builds.uimaps.training.AdminTrainingConfigureInviteEmailPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminTrainingConfigureInviteEmailPageTest {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = "training")

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Modification Of Configure Invite Email With options Same as From Email ", testCaseId = "TC_38_Configure_Invite_Email_Modify_Same_As_From_Email")
	private void inviteEmailSameAsFromEmail() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("training", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminTrainingConfigureInviteEmailPage pobj = new AdminTrainingConfigureInviteEmailPage(driver);

			fc.training().training_common().adminTraingConfigureInviteEmailPage(driver);
			fc.utobj().sendKeys(driver, pobj.fromMail, dataSet.get("fromMail"));

			if (!fc.utobj().isSelected(driver, pobj.returnPath)) {
				fc.utobj().clickElement(driver, pobj.returnPath);
			}

			String mailSubject = fc.utobj().generateTestData(dataSet.get("emailSubject"));

			fc.utobj().sendKeys(driver, pobj.mailSubject, mailSubject);
			try {

				fc.utobj().switchFrameById(driver, "mailText_ifr");

				Actions actions = new Actions(driver);
				actions.moveToElement(fc.utobj().getElement(driver, pobj.editorTextArea));
				actions.click();
				fc.utobj().getElement(driver, pobj.editorTextArea).clear();
				actions.sendKeys(
						"Hello $RECIPIENT_FIRST_NAME$ $RECIPIENT_LAST_NAME$,\n A course has been recently added for training.\n\n Course Details:\n 1. Course Title : $COURSE_TITLE$\n 2. Course Instructor : $COURSE_INSTRUCTOR$\n 3. Course Objective : $COURSE_OBJECTIVE$\n 4. Course Instructions : $COURSE_INSTRUCTIONS$\n\n We would appreciate your effort for taking the course.\n \n Thank You.");
				actions.build().perform();
				fc.utobj().switchFrameToDefault(driver);

			} catch (Exception ex) {
			}

			fc.utobj().clickElement(driver, pobj.modify);

			fc.utobj().clickElement(driver, ".//button[contains(@onclick,'closeSubmit()')]");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	
}
