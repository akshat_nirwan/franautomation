package com.builds.test.training;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import com.builds.uimaps.training.AdminTrainingCoureseManagementPage;
import com.builds.uimaps.training.AdminTrainingCourseManagementAddSectionrPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminTrainingCourseManagementAddSectionPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	//

	@Test(groups = { "training" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Add Section Regular Type At Admin Training Course Management Page", testCaseId = "TC_Verify_Lesson_Dependency")
	private void AddLessonDependancy() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("training", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigating to Admin > Training > Course Management Page");
			AdminTrainingCourseManagementAddSectionrPage pobj = new AdminTrainingCourseManagementAddSectionrPage(
					driver);
			AdminTrainingCoursesManagementPageTest courseManagement = new AdminTrainingCoursesManagementPageTest();
			fc.utobj().printTestStep("Add Category");
			String categoryname = courseManagement.addCategory(driver, dataSet);
			fc.utobj().printTestStep("Add Course");
			courseManagement.addCourse(driver, dataSet, categoryname, testCaseId);

			fc.utobj().printTestStep("Create Lesson");
			fc.utobj().clickElement(driver, pobj.addLesson);

			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//button[contains(text(),'Cancel')]"));
			driver.switchTo().defaultContent();
			fc.utobj().printTestStep("Create Lesson");
			fc.utobj().clickElement(driver, pobj.addLesson);

			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[contains(text(),'Create Lesson')]"));
			boolean test = fc.utobj().assertPageSource(driver, "This field is required.");
			if (!test) {
				fc.utobj().throwsException("Mandatory field msg is not coming ");
			} 
			boolean check = false;
			try {
				String fileName = fc.utobj()
						.getFilePathFromTestData("'Calypso'_Panorama_of_Spirit's_View_from_'Troy'.png");
				fc.utobj().sendKeys(driver, pobj.uploadcoverimage, fileName);
				check = true;

			} catch (Exception ex) {

			}
			if (check) {
				fc.utobj().throwsException("cover image with special char not uploaded");
			}
			try {
				String fileName = fc.utobj().getFilePathFromTestData("ShinChan####.png");
				fc.utobj().sendKeys(driver, pobj.uploadcoverimage, fileName);
				check = true;

			} catch (Exception ex) {

			}
			if (!check) {
				fc.utobj().throwsException("cover image not uploaded");
			}
			String sectionTitle1 = fc.utobj()
					.generateTestData(dataSet.get("sectionTitle") + fc.utobj().generateRandomNumber6Digit());
			fc.utobj().sendKeys(driver, pobj.sectionTitle, sectionTitle1);
			String summary = fc.utobj().generateTestData(dataSet.get("summary"));
			fc.utobj().sendKeys(driver, pobj.summary, summary);
			fc.utobj().clickElement(driver, pobj.addLesson);

			driver.switchTo().defaultContent();
			fc.utobj().clickElement(driver, pobj.uploadFile);

			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			// fc.utobj().clickElement(driver,
			// fc.utobj().getElementByXpath(driver,".//*[@id='fileUploader']//div[contains(text(),
			// 'Drop your files here')]")));
			String fileName = fc.utobj().getFilePathFromTestData("taskFile.pdf");
			fc.utobj().sendKeys(driver, pobj.fileuploader, fileName);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='saveButton']"));

			fc.utobj().printTestStep("Create Lesson");
			fc.utobj().clickElement(driver, pobj.addLesson);

			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			String sectionTitle2 = fc.utobj().generateTestData(dataSet.get("sectionTitle"));
			fc.utobj().sendKeys(driver, pobj.sectionTitle, sectionTitle2);
			summary = fc.utobj().generateTestData(dataSet.get("summary"));
			fc.utobj().sendKeys(driver, pobj.summary, summary);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					(".//*[@id='sectionCombo']/option[contains(text(),'" + sectionTitle1 + "')]")));
			fc.utobj().clickElement(driver, pobj.addLesson);

			driver.switchTo().defaultContent();
			fc.utobj().clickElement(driver, pobj.uploadFile);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			// fc.utobj().clickElement(driver,
			// fc.utobj().getElementByXpath(driver,".//*[@id='fileUploader']//div[contains(text(),
			// 'Drop your files here')]")));
			fileName = fc.utobj().getFilePathFromTestData("taskFile.pdf");

			fc.utobj().sendKeys(driver, pobj.fileuploader, fileName);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='saveButton']"));
			fc.utobj().printTestStep("Verify Add Section Regular Type");
			driver.switchTo().defaultContent();
			boolean isTextPresent = fc.utobj().assertPageSource(driver, sectionTitle1);
			if (isTextPresent == false) {
				fc.utobj().throwsException("Was not able to add Section!!!");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}

	@Test(groups = { "training", "trainingsanity", "TrainingCourse123" , "FailedTestCases"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Add Section Regular Type At Admin Training Course Management Page", testCaseId = "TC_08_Add_Section_Regular")
	private void addSectionRegular() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("training", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigating to Admin > Training > Course Management Page");
			AdminTrainingCourseManagementAddSectionrPage pobj = new AdminTrainingCourseManagementAddSectionrPage(
					driver);
			AdminTrainingCoursesManagementPageTest courseManagement = new AdminTrainingCoursesManagementPageTest();
			fc.utobj().printTestStep("Add Category");
			String categoryname = courseManagement.addCategory(driver, dataSet);
			fc.utobj().printTestStep("Add Course");
			courseManagement.addCourse(driver, dataSet, categoryname, testCaseId);
			fc.utobj().printTestStep("Create Lesson");
			fc.utobj().clickElement(driver, pobj.addLesson);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			fc.utobj().clickElement(driver, ".//*[@id='siteMainDiv']//button[contains(text(),'Cancel')]");
			driver.switchTo().defaultContent();
			fc.utobj().clickElement(driver, pobj.addLesson);

			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			String sectionTitle = fc.utobj().generateTestData(dataSet.get("sectionTitle"));
			fc.utobj().sendKeys(driver, pobj.sectionTitle, sectionTitle);
			String summary = fc.utobj().generateTestData(dataSet.get("summary"));
			fc.utobj().sendKeys(driver, pobj.summary, summary);
			fc.utobj().clickElement(driver, pobj.addLesson);

			driver.switchTo().defaultContent();
			fc.utobj().clickElement(driver, pobj.uploadFile);
			fc.utobj().printTestStep("Checking for large file size default more then 25MB");
			boolean check = false;
			try {
				fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
				String fileName = fc.utobj().getFilePathFromTestData("HeavyFileUpload30MB.pdf");
				fc.utobj().sendKeys(driver, pobj.fileuploader, fileName);
				fc.utobj().clickElement(driver, pobj.createQuizButton);
				check = fc.utobj().assertPageSource(driver, "File size must be less than25 MB");
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			if (!check) {
				fc.utobj().throwsException("More then 25 Mb file got uploaded please check default limit");
			}

			String fileName = fc.utobj().getFilePathFromTestData("taskFile.pdf");
			fc.utobj().sendKeys(driver, pobj.fileuploader, fileName);

			check = fc.utobj().isElementPresent(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='preview']"));
			if (!check) {
				fc.utobj().throwsException("Preview of uploaded file not found");
			}
			addQuizBeforeCreatingLesson(driver, sectionTitle, dataSet, config, testCaseId);
			boolean isTextPresent = fc.utobj().assertPageSource(driver, sectionTitle);
			if (isTextPresent == false) {
				fc.utobj().throwsException("Was not able to add Section!!!");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}

	public String addQuizBeforeCreatingLesson(WebDriver driver, String sectionTitle, Map<String, String> dataSet,
			Map<String, String> config, String testCaseId) throws Exception {
		String quizname = "NewQuiz" + fc.utobj().generateRandomNumber();
		try {
			AdminTrainingCourseManagementAddSectionrPage pobj = new AdminTrainingCourseManagementAddSectionrPage(
					driver);
			fc.utobj().clickElement(driver, pobj.createQuizButton);

			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);

			fc.utobj().sendKeys(driver, pobj.quizName, quizname);
			fc.utobj().sendKeys(driver, pobj.quizSummary, "This is a Quiz");
			fc.utobj().clickElement(driver, pobj.addquiz);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, pobj.AddNewQuestion);
			fc.utobj().clickElement(driver, pobj.ResponseTypeDate);
			fc.utobj().sendKeys(driver, pobj.text1, "Which date is today ??");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='questionMarks1']/option[@value=\"2\"]"));
			fc.utobj().clickElement(driver, pobj.saveQuiz);
			fc.utobj().clickElement(driver, pobj.back);
		} catch (Exception ex) {
			fc.utobj().throwsException("Quiz not added");
		}
		return quizname;
	}

	public String addQuizBeforeCreatingLesson(WebDriver driver, String sectionTitle, Map<String, String> config,
			String quizquestion) throws Exception {
		String quizname = "NewQuiz" + fc.utobj().generateRandomNumber();
		try {
			AdminTrainingCourseManagementAddSectionrPage pobj = new AdminTrainingCourseManagementAddSectionrPage(
					driver);
			fc.utobj().clickElement(driver, pobj.createQuizButton);

			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);

			fc.utobj().sendKeys(driver, pobj.quizName, quizname);
			fc.utobj().sendKeys(driver, pobj.quizSummary, "This is a Quiz");
			fc.utobj().clickElement(driver, pobj.addquiz);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, pobj.AddNewQuestion);
			fc.utobj().clickElement(driver, pobj.ResponseTypeDate);
			fc.utobj().clickElement(driver, pobj.AddtoLibrary);
			fc.utobj().sendKeys(driver, pobj.text1, quizquestion);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='questionMarks1']/option[@value=\"2\"]"));
			fc.utobj().clickElement(driver, ".//*[contains(text(),'Save & Add')]");
			fc.utobj().clickElement(driver,
					".//*[@id=\"questionNos\" and contains(text(),'02')]/ancestor::form//*[@data-role=\"ico_Delete\"]");
			fc.utobj().clickElement(driver, ".//*[contains(text(),'Add from Question Library  ')]");
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			fc.utobj().clickElement(driver, ".//*[@data-role=\"ico_Filter\"]");
			fc.utobj().sendKeys(driver, pobj.QuestionName, quizquestion);
			fc.utobj().clickElement(driver, pobj.search);
			fc.utobj().clickElement(driver, pobj.checkAll);
			fc.utobj().clickElement(driver, pobj.select);
			fc.utobj().clickElement(driver, pobj.saveQuiz);

		} catch (Exception ex) {
			fc.utobj().throwsException("Quiz not added");
		}
		return quizname;
	}

	public void DeleteQuiz(WebDriver driver, String sectionTitle, Map<String, String> config, String quizquestion)
			throws Exception {
		try {
			fc.utobj().clickElement(driver,
					".//*[contains(text(),'" + quizquestion + "')]/following-sibling::td//*[@id='detailActionMenu']");
			fc.utobj().clickElement(driver, ".//*[contains(text(),'" + quizquestion
					+ "')]/following-sibling::td//*[@id='detailActionMenu']/following-sibling::ul/li/a[contains(text() , 'Delete')]");
			fc.utobj().acceptAlertBox(driver);

		} catch (Exception ex) {
			fc.utobj().throwsException("Quiz not added");
		}
	}

	public void ModifyQuiz(WebDriver driver, String sectionTitle, String quizquestion, Map<String, String> config)
			throws Exception {
		try {
			AdminTrainingCourseManagementAddSectionrPage pobj = new AdminTrainingCourseManagementAddSectionrPage(
					driver);
			fc.utobj().clickElement(driver,
					".//*[contains(text(),'" + quizquestion + "')]/following-sibling::td//*[@id='detailActionMenu']");
			fc.utobj().clickElement(driver, ".//*[contains(text(),'" + quizquestion
					+ "')]/following-sibling::td//*[@id='detailActionMenu']/following-sibling::ul/li/a[contains(text() , 'Modify')]");
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			fc.utobj().sendKeys(driver, pobj.question, "Modified" + quizquestion);
			fc.utobj().clickElement(driver, pobj.ModifyLesson);// please if
																// webelement
																// failed try to
																// create new
																// one it is
																// being used by
																// multiple
																// location
		} catch (Exception ex) {
			fc.utobj().throwsException("Quiz not added");
		}
	}

	@Test(groups = { "training", "trainingsanity", "TrainingCourse123" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Delete lesson by Action Icon", testCaseId = "TC_Delete_Lesson_By_Action_Icon_Option")
	private void DeleteLessonByActionIcon() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("training", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigating to Admin > Training > Course Management Page");
			AdminTrainingCourseManagementAddSectionrPage pobj = new AdminTrainingCourseManagementAddSectionrPage(
					driver);
			AdminTrainingCoursesManagementPageTest courseManagement = new AdminTrainingCoursesManagementPageTest();
			fc.utobj().printTestStep("Add Category");
			String categoryname = courseManagement.addCategory(driver, dataSet);
			fc.utobj().printTestStep("Add Course");
			String courseTitle = courseManagement.addCourse(driver, dataSet, categoryname, testCaseId);

			fc.utobj().printTestStep("Create Lesson");
			fc.utobj().clickElement(driver, pobj.addLesson);

			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			String LessonName = fc.utobj().generateTestData(dataSet.get("sectionTitle"));
			fc.utobj().sendKeys(driver, pobj.sectionTitle, LessonName);
			String summary = fc.utobj().generateTestData(dataSet.get("summary"));
			fc.utobj().sendKeys(driver, pobj.summary, summary);
			fc.utobj().clickElement(driver, pobj.addLesson);

			driver.switchTo().defaultContent();
			fc.utobj().clickElement(driver, pobj.uploadFile);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			String fileName = fc.utobj().getFilePathFromTestData("taskFile.pdf");
			fc.utobj().sendKeys(driver, pobj.fileuploader, fileName);

			fc.utobj().clickElement(driver, pobj.SaveAdvanceEditorButton);
			fc.utobj().printTestStep("Verify Add Section Regular Type");
			driver.switchTo().defaultContent();
			DeleteLesson(driver, dataSet, categoryname, courseTitle, LessonName, config, testCaseId);
			boolean isTextPresent = fc.utobj().assertPageSource(driver, LessonName);
			if (isTextPresent) {
				fc.utobj().throwsException("Was not able to Delete Lesson!!!");
			} else {

			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}

	private void DeleteLesson(WebDriver driver, Map<String, String> dataSet, String categoryname, String courseTitle,
			String lessonName, Map<String, String> config, String testCaseId) throws Exception {
		try {
			fc.loginpage().login(driver);
			fc.training().training_common().adminTraingCourseManagementPage(driver);
			AdminTrainingCourseManagementAddSectionrPage pobj = new AdminTrainingCourseManagementAddSectionrPage(
					driver);
			// Modify Course:::
			fc.utobj().printTestStep("Delete Lesson");

			// course modify Action Img
			/*fc.utobj().clickElement(driver, pobj.GeneralCategory);

			fc.utobj().sendKeys(driver, pobj.input, categoryname);
			//fc.utobj().clickElement(driver, ".//*[@id='mCSB_1_container']//a[contains(text(),'" + categoryname + "')]");
			fc.utobj().clickLink(driver, categoryname);*/
			
			fc.utobj().printTestStep("Search Category");
			new AdminTrainingCoursesManagementPageTest().searchCategory(driver, new AdminTrainingCoureseManagementPage(driver), categoryname);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//div[@data-role='ico_Filter']"));
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='searchCourse']"), courseTitle);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='search']"));

			fc.utobj().clickPartialLinkText(driver, courseTitle);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//div[@data-role='ico_Filter']"));

			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='searchLesson']"), lessonName);

			fc.utobj().clickElement(driver, ".//*[@id='search']");

			String xpath = ".//*[contains(text(),'" + lessonName
					+ "')]/ancestor::div/following-sibling::div//div[@data-role='ico_ThreeDots']";
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, xpath));

			fc.utobj().clickElement(driver, pobj.delete);

			fc.utobj().acceptAlertBox(driver);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//div[@data-role='ico_Filter']"));

			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='searchLesson']"), lessonName);

			fc.utobj().clickElement(driver, ".//*[@id='search']");

			boolean val = fc.utobj().assertPageSource(driver, lessonName);
			if (!val)
				fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			else {
				fc.utobj().throwsException("Lesson Name found on View Page " + testCaseId);
			}
		} catch (Exception e) {
			fc.utobj().throwsException("Unable to Delete Lesson " + testCaseId);
		}

	}

	@Test(groups = { "training", "trainingsanity", "TrainingCourse123" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Modify lesson by Action Icon", testCaseId = "TC_Modify_Lesson_By_Action_Icon_Option")
	private void ModifyLessonByActionIcon() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("training", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigating to Admin > Training > Course Management Page");
			AdminTrainingCourseManagementAddSectionrPage pobj = new AdminTrainingCourseManagementAddSectionrPage(
					driver);
			AdminTrainingCoursesManagementPageTest courseManagement = new AdminTrainingCoursesManagementPageTest();
			fc.utobj().printTestStep("Add Category");
			String categoryname = courseManagement.addCategory(driver, dataSet);
			fc.utobj().printTestStep("Add Course");
			String courseTitle = courseManagement.addCourse(driver, dataSet, categoryname, testCaseId);

			fc.utobj().printTestStep("Create Lesson");

			fc.utobj().clickElement(driver, pobj.addLesson);

			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			String LessonName = fc.utobj().generateTestData(dataSet.get("sectioadmnTitle"));
			fc.utobj().sendKeys(driver, pobj.sectionTitle, LessonName);
			String summary = fc.utobj().generateTestData(dataSet.get("summary"));
			fc.utobj().sendKeys(driver, pobj.summary, summary);
			fc.utobj().clickElement(driver, pobj.addLesson);

			driver.switchTo().defaultContent();
			fc.utobj().clickElement(driver, pobj.uploadFile);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			String fileName = fc.utobj().getFilePathFromTestData("taskFile.pdf");

			fc.utobj().sendKeys(driver, pobj.fileuploader, fileName);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='saveButton']"));
			fc.utobj().printTestStep("Verify Modify Section Regular Type");
			driver.switchTo().defaultContent();
			ModifyLesson(driver, dataSet, categoryname, courseTitle, LessonName, config, testCaseId);
			boolean isTextPresent = fc.utobj().assertPageSource(driver, LessonName);
			if (isTextPresent) {
				fc.utobj().throwsException("Was not able to Modify Lesson!!!");
			} else {

			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}

	private void ModifyLesson(WebDriver driver, Map<String, String> dataSet, String categoryname, String courseTitle,
			String LessonName, Map<String, String> config, String testCaseId) throws Exception {
		try {
			fc.loginpage().login(driver);
			fc.training().training_common().adminTraingCourseManagementPage(driver);
			AdminTrainingCourseManagementAddSectionrPage pobj = new AdminTrainingCourseManagementAddSectionrPage(
					driver);
			// Modify Course:::
			fc.utobj().printTestStep("Modify Lesson");

			/*// course modify Action Img
			fc.utobj().clickElement(driver, pobj.GeneralCategory);

			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='dialogV']//input"),
					categoryname);
			//fc.utobj().clickElement(driver, ".//*[@id='mCSB_1_container']//a[contains(text(),'" + categoryname + "')]");
			fc.utobj().clickLink(driver, categoryname);*/
			
			fc.utobj().printTestStep("Search Category");
			new AdminTrainingCoursesManagementPageTest().searchCategory(driver, new AdminTrainingCoureseManagementPage(driver), categoryname);
			

			String nameOfCourseToBeModify = courseTitle;
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//div[@data-role='ico_Filter']"));
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='searchCourse']"), courseTitle);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='search']"));

			fc.utobj().clickPartialLinkText(driver, courseTitle);
			String xpath = ".//*[contains(text () , '" + LessonName
					+ "')]/ancestor::div/following-sibling::div//div[@data-role='ico_ThreeDots']";
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, xpath));

			fc.utobj().clickElement(driver, pobj.modify);

			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			String sectionTitle = "Modified Lesson " + fc.utobj().generateRandomNumber6Digit();
			fc.utobj().sendKeys(driver, pobj.sectionTitle, sectionTitle);
			fc.utobj().clickElement(driver, pobj.ModifyLesson);

			driver.switchTo().defaultContent();
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//div[@data-role='ico_Filter']"));

			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='searchLesson']"), sectionTitle);

			fc.utobj().clickElement(driver, ".//*[@id='search']");

			boolean val = fc.utobj().assertPageSource(driver, sectionTitle);
			if (!val) {
				fc.utobj().throwsException("Modified lesson not found ");
			}
		} catch (Exception e) {
			fc.utobj().throwsException("Unable to modify Lesson " + testCaseId);
		}

	}

	public String addSectionRegular(WebDriver driver, Map<String, String> dataSet) throws Exception {
		String testCaseId = "TC_addSectionRegular_01";
		String sectionTitle = null;

		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminTrainingCourseManagementAddSectionrPage pobj = new AdminTrainingCourseManagementAddSectionrPage(
						driver);

				fc.utobj().clickElement(driver, pobj.addLesson);

				sectionTitle = fc.utobj().generateTestData(dataSet.get("sectionTitle"));
				fc.utobj().sendKeys(driver, pobj.sectionTitle, sectionTitle);
				fc.utobj().clickElement(driver, pobj.contentOptionRegular);
				File file = new File(
						FranconnectUtil.config.get("testDataPath").concat("/") + dataSet.get("uploadFile"));
				fc.utobj().sendKeys(driver, pobj.uploadFile, file.getAbsolutePath());
				String summary = fc.utobj().generateTestData(dataSet.get("summary"));
				fc.utobj().sendKeys(driver, pobj.summary, summary);
				fc.utobj().clickElement(driver, pobj.addLesson);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to Add Section Regular");

		}
		return sectionTitle;
	}

	@Test(groups = { "training", "trainingsanity", "TrainingCourse123" , "Check1234Test"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Add Section with option Advance Editor", testCaseId = "TC_09_Add_Section_Use_Advance_Editor")
	private void addSectionUseAdvanceEditor() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("training", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigating to Admin > Training > Course Management Page");
			AdminTrainingCourseManagementAddSectionrPage pobj = new AdminTrainingCourseManagementAddSectionrPage(
					driver);
			AdminTrainingCoursesManagementPageTest courseManagement = new AdminTrainingCoursesManagementPageTest();
			fc.utobj().printTestStep("Add Category");
			String categoryname = courseManagement.addCategory(driver, dataSet);
			fc.utobj().printTestStep("Add Course");
			courseManagement.addCourse(driver, dataSet, categoryname, testCaseId);
			// Adding first lesson
			fc.utobj().printTestStep("Create Lesson");
			boolean check = fc.utobj().assertPageSource(driver, "No Records Found");
			if (!check) {
				fc.utobj().throwsException("No records found message is not shown on lesson summary page");
			}
			fc.utobj().clickElement(driver, pobj.addLesson);

			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			fc.utobj().assertPageSource(driver, "Name");
			fc.utobj().assertPageSource(driver, "Summary");
			fc.utobj().assertPageSource(driver,
					"Fields marked with <span class=\"mandatory-input\"></span>  are mandatory.");
			String sectionTitle = fc.utobj().generateTestData(dataSet.get("sectionTitle"));
			fc.utobj().sendKeys(driver, pobj.sectionTitle, sectionTitle);
			String summary = fc.utobj().generateTestData(dataSet.get("summary"));
			fc.utobj().sendKeys(driver, pobj.summary, summary);
			fc.utobj().clickElement(driver, pobj.addLesson);

			driver.switchTo().defaultContent();

			fc.utobj().clickElement(driver, pobj.useAdvanceEditor);

			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			// fc.utobj().clickElement(driver,
			// fc.utobj().getElementByXpath(driver,".//*[@id='editor']//iframe[@id='ta_ifr']")));
			try {
				driver.switchTo()
						.frame(fc.utobj().getElementByXpath(driver, ".//*[@id='editor']//iframe[@id='ta_ifr']"));

				WebElement body = fc.utobj().getElementByCssSelector(driver, "body");
				body.sendKeys("hello");

			} catch (Exception ex) {
			}
			driver.switchTo().defaultContent();
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			fc.utobj().clickElement(driver, pobj.SaveAdvanceEditorButton);

			fc.utobj().switchFrameToDefault(driver);
//			addQuizAfterCreatingLesson(driver, sectionTitle, dataSet, config, testCaseId);
			boolean isTextPresent = fc.utobj().assertPageSource(driver, sectionTitle);
			if (isTextPresent == false) {
				fc.utobj().throwsException("Was not able to add Section!!!");
			}

			// adding dependent lesson
			fc.utobj().clickElement(driver, pobj.addLesson);

			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			String sectionTitle1 = fc.utobj().generateTestData(dataSet.get("sectionTitle"));
			fc.utobj().sendKeys(driver, pobj.sectionTitle, sectionTitle1);
			String summary1 = fc.utobj().generateTestData(dataSet.get("summary"));
			fc.utobj().sendKeys(driver, pobj.summary, summary1);
			String isChecked = fc.utobj().getElementByID(driver, "daysCheck").getAttribute("checked");
			if (isChecked == "true") {
				fc.utobj().throwsException("Restart lesson is already checked");
			}
			fc.utobj().clickElement(driver, ".//*[@data-target=\"#sectionIncompelete\"]");
			Select select = new Select(fc.utobj().getElementByID(driver, "sectionCombo"));
			select.selectByVisibleText(sectionTitle);
			fc.utobj().clickElement(driver, pobj.addLesson);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='days']"), "3");
			fc.utobj().clickElement(driver, pobj.addLesson);

			driver.switchTo().defaultContent();

			fc.utobj().clickElement(driver, pobj.useAdvanceEditor);

			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			try {
				driver.switchTo()
						.frame(fc.utobj().getElementByXpath(driver, ".//*[@id='editor']//iframe[@id='ta_ifr']"));
				WebElement body = fc.utobj().getElementByCssSelector(driver, "body");
				body.sendKeys("hello");

			} catch (Exception ex) {
			}
			driver.switchTo().defaultContent();
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			fc.utobj().clickElement(driver, pobj.SaveAdvanceEditorButton);

			fc.utobj().switchFrameToDefault(driver);
			try
			{
			addQuizAfterCreatingLesson(driver, sectionTitle1, dataSet, config, testCaseId);
			isTextPresent = fc.utobj().assertPageSource(driver, sectionTitle1);
			if (isTextPresent == false) {
				fc.utobj().throwsException("Was not able to add Section!!!");
			}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
			

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addSectionAdvanceEditor(WebDriver driver, Map<String, String> dataSet) throws Exception {

		String testCaseId = "TC_addSectionAdvanceEditor_01";
		String sectionTitle = null;

		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminTrainingCourseManagementAddSectionrPage pobj = new AdminTrainingCourseManagementAddSectionrPage(
						driver);
				fc.utobj().clickElement(driver, pobj.addLesson);

				sectionTitle = fc.utobj().generateTestData(dataSet.get("sectionTitle"));
				fc.utobj().sendKeys(driver, pobj.sectionTitle, sectionTitle);

				fc.utobj().clickElement(driver, pobj.useAdvanceEditor);

				fc.utobj().switchFrame(driver, pobj.Iframe);

				fc.utobj().sendKeys(driver, pobj.editorContent, dataSet.get("sectionContent"));

				driver.switchTo().parentFrame();

				fc.utobj().sendKeys(driver, pobj.summary, dataSet.get("summary"));

				fc.utobj().clickElement(driver, pobj.addLesson);
				/* fc.utobj().acceptAlertBox(driver); */
				fc.utobj().switchFrameToDefault(driver);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to add Section Advance Editor");

		}
		return sectionTitle;
	}

	@Test(groups = { "training", "trainingsanity", "ValidateTraining" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Add Embed Video Type Section At Admin Training Course Management Page ", testCaseId = "TC_10_Add_Section_Embed_Video")
	private void addSectionEmbedVideo() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("training", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigating to Admin > Training > Course Management Page");
			AdminTrainingCourseManagementAddSectionrPage pobj = new AdminTrainingCourseManagementAddSectionrPage(
					driver);
			AdminTrainingCoursesManagementPageTest courseManagement = new AdminTrainingCoursesManagementPageTest();
			fc.utobj().printTestStep("Add Category");
			String categoryname = courseManagement.addCategory(driver, dataSet);
			fc.utobj().printTestStep("Add Course");
			courseManagement.addCourse(driver, dataSet, categoryname, testCaseId);

			fc.utobj().printTestStep("Create Lesson");
			fc.utobj().clickElement(driver, pobj.addLesson);

			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			String sectionTitle = fc.utobj().generateTestData(dataSet.get("sectionTitle"));
			fc.utobj().sendKeys(driver, pobj.sectionTitle, sectionTitle);
			String summary = fc.utobj().generateTestData(dataSet.get("summary"));
			fc.utobj().sendKeys(driver, pobj.summary, summary);
			fc.utobj().clickElement(driver, pobj.addLesson);

			driver.switchTo().defaultContent();

			fc.utobj().clickElement(driver, pobj.embedVideo);

			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			try {
				fc.utobj().clickElement(driver, ".//*[@id='saveButton']");
				fc.utobj().getElementByXpath(driver, ".//*[@id='subSectionContent']")
						.sendKeys(dataSet.get("embedCode"));
				fc.utobj().clickElement(driver, ".//*[@id='saveButton']");
			} catch (Exception ex) {
			}

			fc.utobj().printTestStep("Verify Add Emebed Type Section");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//div[@data-role='ico_Filter']"));

			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='searchLesson']"), sectionTitle);

			fc.utobj().clickElement(driver, ".//*[@id='search']");

			addQuizAfterCreatingLesson(driver, sectionTitle, dataSet, config, testCaseId);
			boolean isTextPresent = fc.utobj().assertPageSource(driver, sectionTitle);
			if (isTextPresent == false) {

				fc.utobj().throwsException("Was not able to add Section!!!");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void addQuizAfterCreatingLesson(WebDriver driver, String sectionTitle, Map<String, String> dataSet,
			Map<String, String> config, String testCaseId) throws Exception {
		try {
			AdminTrainingCourseManagementAddSectionrPage pobj = new AdminTrainingCourseManagementAddSectionrPage(
					driver);
			String xpath = ".//*[contains(text () , '" + sectionTitle
					+ "')]/ancestor::div/following-sibling::div//div[@data-role='ico_ThreeDots']";
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, xpath));

			fc.utobj().clickElement(driver, pobj.addquiz2);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			fc.utobj().sendKeys(driver, pobj.quizName, "Quiz" + fc.utobj().generateRandomNumber());
			fc.utobj().sendKeys(driver, pobj.quizSummary, "This is a Quiz");
			fc.utobj().clickElement(driver, pobj.addquiz);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, pobj.AddNewQuestion);
			fc.utobj().clickElement(driver, pobj.ResponseTypeDate);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='text1']"),
					"Which date is today ??");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='questionMarks1']/option[@value=\"2\"]"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='saveQuiz']"));
			xpath = ".//*[contains(text () , '" + sectionTitle
					+ "')]/ancestor::div/following-sibling::div//div[@data-role='ico_ThreeDots']";
			fc.utobj().clickElement(driver, pobj.back);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, xpath));

			try
			{
				fc.utobj().clickElement(driver, pobj.viewquiz1);
			}
			catch(Exception rc)
			{
				fc.utobj().clickElement(driver, pobj.viewquiz2nd);
			}
			

			xpath = ".//*[contains(text () , '" + sectionTitle
					+ "')]/ancestor::div/following-sibling::div//div[@data-role='ico_ThreeDots']";
			fc.utobj().clickElement(driver, pobj.back);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, xpath));

			fc.utobj().clickElement(driver, pobj.copyurl);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			fc.utobj().clickElement(driver, ".//*[contains(text(),'Close')]");
		} catch (Exception ex) {
//			fc.utobj().throwsException("Quiz not added");
		}
	}

	public String addSectionEmbedVideo(WebDriver driver, Map<String, String> dataSet) throws Exception {

		String testCaseId = "TC_addSectionEmbedVideo_01";
		String sectionTitle = null;

		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminTrainingCourseManagementAddSectionrPage pobj = new AdminTrainingCourseManagementAddSectionrPage(
						driver);

				fc.utobj().clickElement(driver, pobj.addLesson);

				sectionTitle = fc.utobj().generateTestData(dataSet.get("sectionTitle"));
				fc.utobj().sendKeys(driver, pobj.sectionTitle, sectionTitle);

				fc.utobj().clickElement(driver, pobj.contentOptionEmbed);

				fc.utobj().sendKeys(driver, pobj.embedCode, dataSet.get("embedCode"));
				fc.utobj().sendKeys(driver, pobj.summary, dataSet.get("summary"));

				fc.utobj().clickElement(driver, pobj.addLesson);
				/* fc.utobj().acceptAlertBox(driver); */

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to Add Section Embed Video");

		}
		return sectionTitle;
	}

	@Test(groups = { "training", "trainingsanity", "TrainingCourse123" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Add SCORM Complient Content Type Section At Admin Training Course Management", testCaseId = "TC_11_Add_Section_SCORM_Compliant_Content")
	private void addSectionScormCompliant() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("training", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigating to Admin > Training > Course Management Page");
			AdminTrainingCourseManagementAddSectionrPage pobj = new AdminTrainingCourseManagementAddSectionrPage(
					driver);
			AdminTrainingCoursesManagementPageTest courseManagement = new AdminTrainingCoursesManagementPageTest();
			fc.utobj().printTestStep("Add Category");
			String categoryname = courseManagement.addCategory(driver, dataSet);
			fc.utobj().printTestStep("Add Course");
			courseManagement.addCourse(driver, dataSet, categoryname, testCaseId);
			fc.utobj().printTestStep("Create Lesson");
			fc.utobj().clickElement(driver, pobj.addLesson);

			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			String sectionTitle = fc.utobj().generateTestData(dataSet.get("sectionTitle"));
			fc.utobj().sendKeys(driver, pobj.sectionTitle, sectionTitle);
			String summary = fc.utobj().generateTestData(dataSet.get("summary"));
			fc.utobj().sendKeys(driver, pobj.summary, summary);
			fc.utobj().clickElement(driver, pobj.addLesson);

			driver.switchTo().defaultContent();

			fc.utobj().clickElement(driver, pobj.scormContent);

			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			String fileName = fc.utobj().getFilePathFromTestData("taskFile.pdf");
			fc.utobj().sendKeys(driver, pobj.fileuploader, fileName);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='saveButton']"));

			boolean check = fc.utobj().assertPageSource(driver,
					"Only zip format is supported in case of scrom content");
			if (!check) {
				fc.utobj().throwsException("exception msg is not coming");
			}
			fileName = fc.utobj().getFilePathFromTestData("taskFile.zip");
			fc.utobj().sendKeys(driver, pobj.fileuploader, fileName);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='saveButton']"));

			fc.utobj().printTestStep("Verify Add SCORM Complient Type Section");
			fc.utobj().switchFrameToDefault(driver);
			addQuizAfterCreatingLesson(driver, sectionTitle, dataSet, config, testCaseId);

			boolean isTextPresent = fc.utobj().assertPageSource(driver, sectionTitle);
			if (isTextPresent == false) {
				fc.utobj().throwsException("Was not able to add Section!!!");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addSection(WebDriver driver, Map<String, String> dataSet, Map<String, String> config)
			throws Exception {

		String testCaseId = "TC_addSection_02";
		String sectionTitle = null;

		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminTrainingCourseManagementAddSectionrPage pobj = new AdminTrainingCourseManagementAddSectionrPage(
						driver);

				fc.utobj().clickElement(driver, pobj.addLesson);

				sectionTitle = fc.utobj().generateTestData(dataSet.get("sectionTitle"));
				fc.utobj().sendKeys(driver, pobj.sectionTitle, sectionTitle);

				fc.utobj().clickElement(driver, pobj.contentOptionScorm);

				// it support only .zip file format
				File file = new File(config.get("testDataPath").concat("/") + dataSet.get("uploadFile"));
				fc.utobj().sendKeys(driver, pobj.uploadFile, file.getAbsolutePath());

				String summary = fc.utobj().generateTestData(dataSet.get("summary"));

				fc.utobj().sendKeys(driver, pobj.summary, summary);

				fc.utobj().clickElement(driver, pobj.addLesson);
				/* fc.utobj().acceptAlertBox(driver); */

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to add Section");

		}
		return sectionTitle;
	}

	@Test(groups = { "training", "Testing123" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Add Articulate Content Type Section At Admin Training Course Management", testCaseId = "TC_12_Add_Section_Articulate_Content")
	private void addSectionArticulateContent() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("training", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigating to Admin > Training > Course Management Page");
			AdminTrainingCourseManagementAddSectionrPage pobj = new AdminTrainingCourseManagementAddSectionrPage(
					driver);
			AdminTrainingCoursesManagementPageTest courseManagement = new AdminTrainingCoursesManagementPageTest();
			fc.utobj().printTestStep("Add Category");
			String categoryname = courseManagement.addCategory(driver, dataSet);
			fc.utobj().printTestStep("Add Course");
			courseManagement.addCourse(driver, dataSet, categoryname, testCaseId);
			fc.utobj().printTestStep("Create Lesson");
			fc.utobj().clickElement(driver, pobj.addLesson);

			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			String sectionTitle = fc.utobj().generateTestData(dataSet.get("sectionTitle"));
			fc.utobj().sendKeys(driver, pobj.sectionTitle, sectionTitle);
			String summary = fc.utobj().generateTestData(dataSet.get("summary"));
			fc.utobj().sendKeys(driver, pobj.summary, summary);
			fc.utobj().clickElement(driver, pobj.addLesson);

			driver.switchTo().defaultContent();
			fc.utobj().clickElement(driver, pobj.articulateContent);

			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);

			String fileName = fc.utobj().getFilePathFromTestData("taskfile.html");
			boolean check = false;
			try {
				String fileName1 = fc.utobj().getFilePathFromTestData("taskFile.zip");
				fc.utobj().sendKeys(driver, pobj.fileuploader, fileName1);

				check = true;

			} catch (Exception ex) {
				ex.printStackTrace();
			}
			if (!check) {
				fc.utobj().throwsException("other then zip file uploaded in articulate content");
			}
			check = fc.utobj().assertPageSource(driver, "This field is required.");
			if (!check) {
				fc.utobj().throwsException("error msg not came while uploading wrong file  in articulate content");
			}
			String fileName1 = fc.utobj().getFilePathFromTestData("taskFile.zip");
			fc.utobj().sendKeys(driver, pobj.fileuploader, fileName1);

			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='mainFile']"), fileName);
			// fc.utobj().searchInSelectBoxSingleValue(driver,
			// fc.utobj().getElementByXpath(driver,".//*[@id='mainFile']")),
			// dataSet.get("mainFile"));
			check = fc.utobj().isElementPresent(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='preview']"));
			if (!check) {
				fc.utobj().throwsException("Preview of uploaded file not found");
			}

			fc.utobj().clickElement(driver, ".//*[@id='saveButton']");
			fc.utobj().switchFrameToDefault(driver);

			addQuizAfterCreatingLesson(driver, sectionTitle, dataSet, config, testCaseId);
			fc.utobj().printTestStep("Verify Add Articulate ");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, sectionTitle);
			if (isTextPresent == false) {
				fc.utobj().throwsException("Was not able to add Section!!!");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addSectionArticulate(WebDriver driver, Map<String, String> dataSet, Map<String, String> config)
			throws Exception {

		String testCaseId = "TC_addSectionArticulate_01";
		String sectionTitle = null;

		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminTrainingCourseManagementAddSectionrPage pobj = new AdminTrainingCourseManagementAddSectionrPage(
						driver);
				fc.utobj().clickElement(driver, pobj.addLesson);

				sectionTitle = fc.utobj().generateTestData(dataSet.get("sectionTitle"));
				fc.utobj().sendKeys(driver, pobj.sectionTitle, sectionTitle);

				fc.utobj().clickElement(driver, pobj.contentOptionArticulate);

				File file = new File(config.get("testDataPath").concat("/") + dataSet.get("uploadFile"));
				fc.utobj().sendKeys(driver, pobj.uploadFile, file.getAbsolutePath());

				File file1 = new File(config.get("testDataPath").concat("/") + dataSet.get("mainFile"));
				fc.utobj().sendKeys(driver, pobj.mainFile, file1.getAbsolutePath());

				String summary = fc.utobj().generateTestData(dataSet.get("summary"));

				fc.utobj().sendKeys(driver, pobj.summary, summary);

				fc.utobj().clickElement(driver, pobj.addLesson);
				/* fc.utobj().acceptAlertBox(driver); */

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to add Section Articulate");

		}
		return sectionTitle;
	}

	@Test(groups = { "training_old", "trainingsanity" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Modification of Regular Content Type Section At Admin Training Course Management", testCaseId = "TC_13_Modify_Regular_Content_Section")
	private void modifyRegularContentSection() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("training", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigating to Admin > Training > Course Management Page");
			AdminTrainingCourseManagementAddSectionrPage pobj = new AdminTrainingCourseManagementAddSectionrPage(
					driver);
			AdminTrainingCoursesManagementPageTest courseManagement = new AdminTrainingCoursesManagementPageTest();
			fc.utobj().printTestStep("Add Category");
			String categoryname = courseManagement.addCategory(driver, dataSet);
			fc.utobj().printTestStep("Add Course");
			courseManagement.addCourse(driver, dataSet, categoryname, testCaseId);
			fc.utobj().printTestStep("Add Section");
			String sectionTitle = addSectionRegular(driver, dataSet);

			fc.utobj().printTestStep("Modify Section");
			String sectionTitle1 = sectionTitle;
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, "//td[contains(text()," + "'"
					+ sectionTitle1 + "'" + ")]/following-sibling::td/div[@id='menuBar']/layer/a/img"));

			// click over Modify section
			String str1 = fc.utobj().getElementByXpath(driver, "//td[contains(text()," + "'" + sectionTitle1 + "'"
					+ ")]/following-sibling::td/div[@id='menuBar']/layer").getAttribute("id");

			String alteredText = str1.replace("Actions_dynamicmenu", "");
			alteredText = alteredText.replace("Bar", "");

			String option = "Modify";
			List<String> linkArray = fc.utobj().translate(option);
			if (!(linkArray.size() < 1)) {
				for (int i = 0; i < linkArray.size(); i++) {
					try {
						option = linkArray.get(i);
						fc.utobj().clickElement(driver,
								fc.utobj().getElementByXpath(driver, "//div/div[@id='Actions_dynamicmenu" + alteredText
										+ "Menu" + "']/span[contains(text(), '" + option + "')]"));
						break;
					} catch (Exception e) {
					}
				}
			} else {
				fc.utobj().clickElement(driver,
						fc.utobj().getElementByXpath(driver, "//div/div[@id='Actions_dynamicmenu" + alteredText + "Menu"
								+ "']/span[contains(text(), '" + option + "')]"));
			}

			String sectionTitleM = fc.utobj().generateTestData(dataSet.get("sectionTitleM"));
			fc.utobj().sendKeys(driver, pobj.sectionTitle, sectionTitleM);

			fc.utobj().clickElement(driver, pobj.contentOptionRegular);

			File file = new File(config.get("testDataPath").concat("/") + dataSet.get("uploadFileM"));
			fc.utobj().sendKeys(driver, pobj.uploadFile, file.getAbsolutePath());

			String summary = fc.utobj().generateTestData(dataSet.get("summaryM"));

			fc.utobj().sendKeys(driver, pobj.summary, summary);

			fc.utobj().clickElement(driver, pobj.addLesson);
			/* fc.utobj().acceptAlertBox(driver); */

			fc.utobj().printTestStep("Verify Modification of Regular Content Type Action");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, sectionTitleM);
			if (isTextPresent == false) {
				fc.utobj().throwsException("Was not able to modify Section!!!");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}

	@Test(groups = { "training_old", "trainingsanity" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Deletion Of Regular Content Type Section", testCaseId = "TC_14_Delete_Regular_Content_Section")
	private void deleteRegularContentSection() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("training", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigating to Admin > Training > Course Management Page");
			fc.utobj().printTestStep("Add Category");
			AdminTrainingCoursesManagementPageTest CourseManagement = new AdminTrainingCoursesManagementPageTest();
			String categoryname = CourseManagement.addCategory(driver, dataSet);
			fc.utobj().printTestStep("Add Course");
			CourseManagement.addCourse(driver, dataSet, categoryname, testCaseId);
			fc.utobj().printTestStep("Add Section Regular Type");
			String sectionTitle = addSectionRegular(driver, dataSet);
			fc.utobj().printTestStep("Delete Section");
			String sectionTitle1 = sectionTitle;
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, "//td[contains(text()," + "'"
					+ sectionTitle1 + "'" + ")]/following-sibling::td/div[@id='menuBar']/layer/a/img"));

			String str1 = fc.utobj().getElementByXpath(driver, "//td[contains(text()," + "'" + sectionTitle1 + "'"
					+ ")]/following-sibling::td/div[@id='menuBar']/layer").getAttribute("id");
			String alteredText = str1.replace("Actions_dynamicmenu", "");
			alteredText = alteredText.replace("Bar", "");

			String option = "Delete";
			List<String> linkArray = fc.utobj().translate(option);
			if (!(linkArray.size() < 1)) {
				for (int i = 0; i < linkArray.size(); i++) {
					try {
						option = linkArray.get(i);
						fc.utobj().clickElement(driver,
								fc.utobj().getElementByXpath(driver, "//div/div[@id='Actions_dynamicmenu" + alteredText
										+ "Menu" + "']/span[contains(text(), '" + option + "')]"));
						break;
					} catch (Exception e) {
					}
				}
			} else {
				fc.utobj().clickElement(driver,
						fc.utobj().getElementByXpath(driver, "//div/div[@id='Actions_dynamicmenu" + alteredText + "Menu"
								+ "']/span[contains(text(), '" + option + "')]"));
			}

			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify Deletion Of Regular Content Type Section");

			boolean isTextPresent = fc.utobj().assertPageSource(driver, sectionTitle);
			if (isTextPresent == true) {

				fc.utobj().throwsException("was not able to delete Section!!");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void addQuizAllTypeQuestionBeforeCreatingLesson(WebDriver driver, String sectionTitle,
			Map<String, String> config, String quizquestion) throws Exception {
		String quizname = "NewQuiz" + fc.utobj().generateRandomNumber();
		try {
			AdminTrainingCourseManagementAddSectionrPage pobj = new AdminTrainingCourseManagementAddSectionrPage(
					driver);
			fc.utobj().clickElement(driver, pobj.createQuizButton);

			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);

			fc.utobj().sendKeys(driver, pobj.quizName, quizname);
			fc.utobj().sendKeys(driver, pobj.quizSummary, "This is a Quiz");
			fc.utobj().clickElement(driver, pobj.addquiz);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, pobj.AddNewQuestion);
			fc.utobj().clickElement(driver, pobj.ResponseTypeDate);
			fc.utobj().clickElement(driver, pobj.AddtoLibrary);
			fc.utobj().sendKeys(driver, pobj.text1, quizquestion);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='questionMarks1']/option[@value=\"2\"]"));
			fc.utobj().clickElement(driver, ".//*[contains(text(),'Save & Add')]");
			fc.utobj().clickElement(driver,
					".//*[@id=\"questionNos\" and contains(text(),'02')]/ancestor::form//*[@data-role=\"ico_Delete\"]");
			fc.utobj().clickElement(driver, ".//*[contains(text(),'Add from Question Library  ')]");
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			fc.utobj().clickElement(driver, ".//*[@data-role=\"ico_Filter\"]");
			fc.utobj().sendKeys(driver, pobj.QuestionName, quizquestion);
			fc.utobj().clickElement(driver, pobj.search);
			fc.utobj().clickElement(driver, pobj.checkAll);
			fc.utobj().clickElement(driver, pobj.select);
			fc.utobj().clickElement(driver, pobj.saveQuiz);

		} catch (Exception ex) {
			fc.utobj().throwsException("Quiz not added");
		}

	}
}
