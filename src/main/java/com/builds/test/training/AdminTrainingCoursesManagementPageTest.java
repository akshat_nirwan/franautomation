package com.builds.test.training;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminUsersRolesAddNewRolePageTest;
import com.builds.uimaps.admin.AdminUsersManageCorporateUsersAddCorporateUserPage;
import com.builds.uimaps.training.AdminTrainingCoureseManagementPage;
import com.builds.uimaps.training.AdminTrainingCourseManagementAddSectionrPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminTrainingCoursesManagementPageTest {

	FranconnectUtil fc = new FranconnectUtil();
	private Map<String, String> UniqueKey = new HashMap<String, String>();

	@Test(groups = { "training","Trainingcategory" })
	@TestCase(createdOn = "2018-01-03", updatedOn = "2017-01*03", testCaseDescription = "Verify The validation of Category At Admin Training Course Management Page", testCaseId = "TC_01_Add_Category")
	private void validateCategory() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("training", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			fc.utobj().printTestStep("Entering the credential for validate user");
			driver = fc.loginpage().login(driver);
			AdminTrainingCoureseManagementPage pobj = new AdminTrainingCoureseManagementPage(driver);
			fc.utobj().printTestStep("Navigate To Admin Training Course Management Page");
			fc.training().training_common().adminTraingCourseManagementPage(driver);
			try {
				fc.utobj().clickElement(driver, pobj.GeneralCategory);

			} catch (Exception e) {
				fc.utobj().throwsException("In Course ManageMent Page General Category not Found");
			}

			fc.utobj().printTestStep("Add Category");
			try {
				fc.utobj().clickElement(driver, pobj.manageCategory);
			} catch (Exception e) {
				fc.utobj().throwsException("Course ManageMent Page not Found");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "training", "trainingsmoke", "Training1234" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add Category At Admin Training Course Management Page", testCaseId = "TC_02_Add_Category")
	private void addCategory() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("training", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			fc.utobj().printTestStep("Login and Adding new role");
			driver = fc.loginpage().login(driver);
			String CorpRole = "Corp" + fc.utobj().generateRandomNumber();
			String RegRole = "Reg" + fc.utobj().generateRandomNumber();
			String FranRole = "Fran" + fc.utobj().generateRandomNumber();
			String DivRole = "Div" + fc.utobj().generateRandomNumber();
			AdminUsersRolesAddNewRolePageTest RolePage = new AdminUsersRolesAddNewRolePageTest();
			RolePage.addCorporateRoles(driver, CorpRole);

			RolePage.addRegionalRoleWithOnlyTrainingModule(driver, config, RegRole);

			RolePage.addFranchiselRoleWithOnlyTrainingModule(driver, config, FranRole);

			RolePage.addDivisionalRoleWithOnlyTrainingModule(driver, config, DivRole);

			driver = fc.loginpage().login(driver);
			AdminTrainingCoureseManagementPage pobj = new AdminTrainingCoureseManagementPage(driver);
			fc.utobj().printTestStep("Navigate To Admin Training Course Management Page");
			fc.training().training_common().adminTraingCourseManagementPage(driver);

			fc.utobj().printTestStep("Add Category");
			try {
				fc.utobj().clickElement(driver, pobj.manageCategory);
			} catch (Exception e) {
				fc.utobj().throwsException("Course ManageMent Page not Found");
			}
			boolean verify = fc.utobj().assertPageSource(driver, "Manage Courses");
			if (!verify) {
				fc.utobj().throwsException("Manage Course link not found");
			}
			fc.utobj().clickElement(driver, pobj.addCategory);

			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);

			String testData1 = fc.utobj().generateTestData(dataSet.get("categoryName"));
			Thread.sleep(1000);
			fc.utobj().clipBoardData(testData1);
			Thread.sleep(1000);
			Actions actions = new Actions(driver);
			Thread.sleep(1000);
			pobj.categoryName.clear();
			Thread.sleep(1000);
			actions.moveToElement(pobj.categoryName).build().perform();
			Thread.sleep(1000);
			actions.keyDown(Keys.CONTROL).sendKeys(String.valueOf('\u0076')).perform();
			Thread.sleep(1000);
			actions.keyUp(Keys.CONTROL);
			Thread.sleep(1000);

			String testData2 = fc.utobj().generateTestData(dataSet.get("categoryDescription"));
			Thread.sleep(1000);
			fc.utobj().clipBoardData(testData2);
			Thread.sleep(1000);
			Actions actions1 = new Actions(driver);
			Thread.sleep(1000);
			pobj.categoryDescription.clear();
			Thread.sleep(1000);
			actions1.moveToElement(pobj.categoryDescription).build().perform();
			Thread.sleep(1000);
			actions1.keyDown(Keys.CONTROL).sendKeys(testData2).perform();
			Thread.sleep(1000);
			actions1.keyUp(Keys.CONTROL);
			Thread.sleep(1000);

			fc.utobj().clickElement(driver, pobj.allowRoleBasedAccess);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='fc-drop-parentcorRoles']/button"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					(".//*[@id='fc-drop-parentcorRoles']/div/ul//span[contains(text(), '" + CorpRole + "')]")));

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='fc-drop-parentregionalRoles']/button"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='fc-drop-parentregionalRoles']/div/ul//span[contains(text(), '" + RegRole + "')]"));

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='fc-drop-parentfranchiseeRoles']/button"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='fc-drop-parentfranchiseeRoles']/div/ul//span[contains(text(), '" + FranRole + "')]"));

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='fc-drop-parentdivisionalRoles']/button"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='fc-drop-parentdivisionalRoles']/div/ul//span[contains(text(), '" + DivRole + "')]"));

			fc.utobj().clickElement(driver, pobj.addCategoryButton);

			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Add Category");
			try
			{
			fc.utobj().clickElement(driver, ".//*[@data-role='ico_Filter']");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='categoryName']"), testData1);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='search']"));

			// Confirmation Data

			List<String> listItems = new LinkedList<String>();
			listItems.add(testData1);
			listItems.add(testData2);
			boolean isTextPresent = fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);
			if (isTextPresent == false) {
				fc.utobj().throwsException("Was not able to add Category !!!");
			}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addCorporateUser(WebDriver driver, Map<String, String> config, Map<String, String> dataSet,
			String testCaseId, String userName) throws Exception {
		try {
			fc.loginpage().login(driver);
			fc.adminpage().adminUsersManageCorporateUsersAddCorporateUserPage(driver);
			AdminUsersManageCorporateUsersAddCorporateUserPage pobj = new AdminUsersManageCorporateUsersAddCorporateUserPage(
					driver);
			userName = dataSet.get("corporateUser") + fc.utobj().generateRandomNumber();
			fc.utobj().sendKeys(driver, pobj.userName, userName);
			fc.utobj().sendKeys(driver, pobj.password, "T0n1ght1");
			fc.utobj().sendKeys(driver, pobj.confirmPassword, "T0n1ght1");
			fc.utobj().selectDropDown(driver, pobj.type, "Normal User");

			fc.utobj().clickElement(driver, pobj.rolesBtn);
			fc.utobj().sendKeys(driver, pobj.searchRoles, "Corporate Administrator");
			fc.utobj().clickElement(driver, pobj.selectAll);

			fc.utobj().selectDropDown(driver, pobj.timeZone, "GMT -06:00 US/Canada/Central");

			fc.utobj().sendKeys(driver, pobj.firstName, userName + "Fname");
			fc.utobj().sendKeys(driver, pobj.lastName, userName + "Lname");

			fc.utobj().sendKeys(driver, pobj.city, "11");
			fc.utobj().selectDropDown(driver, pobj.country, "USA");
			fc.utobj().selectDropDown(driver, pobj.state, "Colorado");

			fc.utobj().sendKeys(driver, pobj.phone1, "1234567890");
			fc.utobj().sendKeys(driver, pobj.phoneExt1, "2");
			fc.utobj().sendKeys(driver, pobj.email, "trainingautomation@staffex.com");

			try {
				if (fc.utobj().isSelected(driver, pobj.consultant)) {
					// do nothing
				} else {
					fc.utobj().clickElement(driver, pobj.consultant);
				}
			} catch (Exception e) {

			}

			fc.utobj().clickElement(driver, pobj.submit);
			fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			return userName + "Fname " + userName + "Lname";

		} catch (Exception ex) {
			fc.utobj().quitBrowserOnCatch(driver, ex, testCaseId);
			return null;
		}

	}

	public String addCategory(WebDriver driver, Map<String, String> dataSet) throws Exception {

		String testCaseId = "TC_Add_Category_Training";
		String categoryName = null;

		if (fc.utobj().validate(testCaseId)) {
			try {
				fc.training().training_common().adminTraingCourseManagementPage(driver);

				AdminTrainingCoureseManagementPage pobj = new AdminTrainingCoureseManagementPage(driver);
				fc.utobj().clickElement(driver, pobj.manageCategory);
				fc.utobj().clickElement(driver, pobj.addCategory);
				fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
				categoryName = fc.utobj().generateTestData(dataSet.get("categoryName"));
				fc.utobj().sendKeys(driver, pobj.categoryName, categoryName);
				String categoryDescription = fc.utobj().generateTestData(dataSet.get("categoryDescription"));
				fc.utobj().sendKeys(driver, pobj.categoryDescription, categoryDescription);
				fc.utobj().clickElement(driver, pobj.addCategoryButton);
				driver.switchTo().defaultContent();
				fc.utobj().clickElement(driver, ".//div[@data-role='ico_Filter']");
				fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='categoryName']"),
						categoryName);
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='search']"));
				boolean isTextPresent = fc.utobj().assertPageSource(driver, categoryName);
				if (isTextPresent == false) {
					fc.utobj().throwsException("Was not able to add Category !!!");
				}
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to add Category Training");
		}
		return categoryName;
	}

	@Test(groups = { "training", "ValidateTraining" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify modification of the Category At Admin Training Course Management Page", testCaseId = "TC_03_Modify_Category")
	private void modifyCategory() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("training", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminTrainingCoureseManagementPage pobj = new AdminTrainingCoureseManagementPage(driver);
			fc.utobj().printTestStep("Navigate To Admin Training Course Management Page");
			fc.utobj().printTestStep("Add Category");
			String categoryName = addCategory(driver, dataSet);

			fc.utobj().printTestStep("Modify Category");

			String nameOfCategoryModify = categoryName;
			fc.utobj().clickElement(driver, ".//div[@data-role='ico_Filter']");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='categoryName']"), categoryName);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='search']"));
			String xpath = ".//*[contains(text () , '" + categoryName
					+ "')]/ancestor::div/following-sibling::div//div[@data-role='ico_ThreeDots']";

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, xpath));

			fc.utobj().clickElement(driver, ".//*/li[contains(text () , 'Modify')]");

			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			String categoryNameModify = fc.utobj().generateTestData(dataSet.get("categoryNameModify"));
			fc.utobj().sendKeys(driver, pobj.categoryName, categoryNameModify);

			String categoryDescriptionModify = fc.utobj().generateTestData(dataSet.get("categoryDescriptionModify"));
			fc.utobj().sendKeys(driver, pobj.categoryDescription, categoryDescriptionModify);
			fc.utobj().clickElement(driver, pobj.saveCategoryButtonInModify);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, ".//div[@data-role='ico_Filter']");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='categoryName']"),
					categoryNameModify);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='search']"));

			fc.utobj().printTestStep("Verify The Modify Category");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, categoryNameModify);
			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to modify category!!!");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "training")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Deletion of the Category At Admin Training Course Management Page", testCaseId = "TC_04_Delete_Category")
	private void deleteCategory() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("training", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminTrainingCoureseManagementPage pobj = new AdminTrainingCoureseManagementPage(driver);
			fc.utobj().printTestStep("Navigate To Admin Training Course Management Page");
			fc.utobj().printTestStep("Add Category");
			String categoryName = addCategory(driver, dataSet);

			fc.utobj().printTestStep("Delete Category");

			String nameOfCategoryModify = categoryName;
			fc.utobj().clickElement(driver, ".//div[@data-role='ico_Filter']");

			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='categoryName']"), categoryName);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='search']"));

			String xpath = ".//*[contains(text () , '" + categoryName
					+ "')]/ancestor::div/following-sibling::div//div[@data-role='ico_ThreeDots']";
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, xpath));

			fc.utobj().clickElement(driver, pobj.delete);

			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Category has been Deleted");

			fc.utobj().clickElement(driver, ".//div[@data-role='ico_Filter']");

			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='categoryName']"), categoryName);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='search']"));

			boolean isTextPresent = fc.utobj().assertPageSource(driver, categoryName);
			if (isTextPresent) {
				fc.utobj().throwsException("was not able to Delete category!!!");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}

	@Test(groups = { "training", "trainingsmoke","TrainingCourse123" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Add Course At Admin Training Course Management Page", testCaseId = "TC_05_Add_Course")
	private void addCourse() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("training", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			String CorpRole = "Corp" + fc.utobj().generateRandomNumber();
			String RegRole = "Reg" + fc.utobj().generateRandomNumber();
			String FranRole = "Fran" + fc.utobj().generateRandomNumber();
			String DivRole = "Div" + fc.utobj().generateRandomNumber();
			AdminUsersRolesAddNewRolePageTest RolePage = new AdminUsersRolesAddNewRolePageTest();
			RolePage.addCorporateRoles(driver, CorpRole);

			RolePage.addRegionalRoleWithOnlyTrainingModule(driver, config, RegRole);

			RolePage.addFranchiselRoleWithOnlyTrainingModule(driver, config, FranRole);

			RolePage.addDivisionalRoleWithOnlyTrainingModule(driver, config, DivRole);

			AdminTrainingCoureseManagementPage pobj = new AdminTrainingCoureseManagementPage(driver);
			fc.utobj().printTestStep("Navigate To Admin Training Course Management Page");
			fc.utobj().printTestStep("Add Category");
			String categoryName = addCategory(driver, dataSet);

			fc.utobj().clickElement(driver, ".//div[@data-role='ico_Filter']");

			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='categoryName']"), categoryName);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='search']"));
			String xpath = ".//*[contains(text () , '" + categoryName
					+ "')]/ancestor::div/following-sibling::div//div[@data-role='ico_ThreeDots']";

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, xpath));

			fc.utobj().clickElement(driver, pobj.addCourse);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			String fileName = fc.utobj().getFilePathFromTestData("logo.jpg");
			try {
				fc.utobj().sendKeys(driver, pobj.uploadcoverimage, fileName);
			} catch (Exception e) {
				pobj.uploadcoverimage.sendKeys(fileName);
			}
			String testData21 = fc.utobj().generateTestData(dataSet.get("courseName"));
			fc.utobj().sendKeys(driver, pobj.courseName, testData21);

			String testData22 = fc.utobj().generateTestData(dataSet.get("courseObjective"));
			fc.utobj().sendKeys(driver, pobj.courseObjective, testData22);

			fc.utobj().printTestStep("Testing for different");
			WebElement element = driver.findElement(By.xpath(".//*[@id='fc-drop-parentuserCombo']/div/div/input"));
			fc.utobj().clickElement(driver, ".//*[@id='fc-drop-parentuserCombo']/button");
			fc.utobj().sendKeys(driver, element, "FranConnect Administrator");
			Thread.sleep(5000);
			element.sendKeys(Keys.ENTER);
			Thread.sleep(1000);
			fc.utobj().clickElement(driver,".//*[@id='fc-drop-parentuserCombo']/div/ul/li/.//*[contains(text(),'FranConnect Administrator')]");
			fc.utobj().sendKeys(driver, pobj.instructions, dataSet.get("instructions"));		
			fc.utobj().clickElement(driver, pobj.allowRoleBasedAccess);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='fc-drop-parentcorRoles']/button"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					(".//*[@id='fc-drop-parentcorRoles']/div/ul//span[contains(text(), '" + CorpRole + "')]")));

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='fc-drop-parentfranchiseeRoles']/button"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='fc-drop-parentfranchiseeRoles']/div/ul//span[contains(text(), '" + FranRole + "')]"));

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='fc-drop-parentdivisionalRoles']/button"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='fc-drop-parentdivisionalRoles']/div/ul//span[contains(text(), '" + DivRole + "')]"));

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='fc-drop-parentregionalRoles']/button"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='fc-drop-parentregionalRoles']/div/ul//span[contains(text(), '" + RegRole + "')]"));

			fc.utobj().clickElement(driver, pobj.addCourseButton);

			fc.utobj().printTestStep("Verify The Add Course");

			// data confirm

			List<String> listItems = new LinkedList<String>();
			listItems.add(testData21);
			listItems.add(testData22);
			listItems.add(CorpRole);
			listItems.add(FranRole);
			listItems.add(DivRole);
			listItems.add(RegRole);
			boolean isTextPresent = fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);
			if (isTextPresent == false) {
				fc.utobj().throwsException("Was not able to add Course!!!");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addCourse(WebDriver driver, Map<String, String> dataSet, String categoryName, String testCaseId)
			throws Exception {

		String courseTitle = null;

		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminTrainingCoureseManagementPage pobj = new AdminTrainingCoureseManagementPage(driver);

				fc.training().training_common().adminTraingCourseManagementPage(driver);

				// add course into name of Category

				String nameOfCategory = categoryName;
				fc.utobj().clickElement(driver, pobj.manageCategory);
				driver.switchTo().defaultContent();
				fc.utobj().clickElement(driver,
						".//div[@data-role='ico_Filter' or @ data-target=\"#FilterContainer\"]");
				fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='categoryName']"),
						categoryName);
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='search']"));
				Thread.sleep(4000);
				String xpath = ".//*[contains(text () , '" + categoryName
						+ "')]/ancestor::div/following-sibling::div//div[@data-role='ico_ThreeDots']";
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, xpath));

				fc.utobj().clickElement(driver, pobj.addCourse);

				fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
				courseTitle = fc.utobj().generateTestData(dataSet.get("courseName"));
				fc.utobj().sendKeys(driver, pobj.courseName, courseTitle);

				String testData22 = fc.utobj().generateTestData(dataSet.get("courseObjective"));
				fc.utobj().sendKeys(driver, pobj.courseObjective, testData22);

				WebElement element = driver.findElement(By.xpath(".//*[@id='fc-drop-parentuserCombo']/div/div/input"));
				fc.utobj().clickElement(driver, ".//*[@id='fc-drop-parentuserCombo']/button");
				fc.utobj().sendKeys(driver, element, "FranConnect Administrator");
				Thread.sleep(5000);
				element.sendKeys(Keys.ENTER);
				Thread.sleep(1000);
				fc.utobj().clickElement(driver,".//*[@id='fc-drop-parentuserCombo']/div/ul/li/.//*[contains(text(),'FranConnect Administrator')]");
				
				
				fc.utobj().sendKeys(driver, pobj.instructions, "Test Instruction");

				fc.utobj().clickElement(driver, pobj.addCourseButton);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to add Course");

		}
		return courseTitle;
	}

	@Test(groups = { "training", "TrainingCourse123", "StarFailedTC" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Modification of Course At Admin Training Course Management Page", testCaseId = "TC_06_Modify_Course")
	private void modifyCourse() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("training", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminTrainingCoureseManagementPage pobj = new AdminTrainingCoureseManagementPage(driver);
			fc.utobj().printTestStep("Navigate To Admin Training Course Management Page");
			fc.utobj().printTestStep("Add Category");
			String categoryname = addCategory(driver, dataSet);
			fc.utobj().printTestStep("Add Course");
			String courseTitle = addCourse(driver, dataSet, categoryname, testCaseId);
			fc.training().training_common().adminTraingCourseManagementPage(driver);
			fc.utobj().printTestStep("Modify Course");
			
			/*fc.utobj().clickElement(driver, pobj.GeneralCategory);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='dialogV']//input"),
					categoryname);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					(".//*[@id='mCSB_1_container']//a[contains(text (), '" + categoryname + "')]")));*/
			
			fc.utobj().printTestStep("Search Category");
			searchCategory(driver, pobj, categoryname);
			
			String nameOfCourseToBeModify = courseTitle;
			fc.utobj().clickElement(driver, ".//div[@data-role='ico_Filter']");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='searchCourse']"), courseTitle);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='search']"));
			String xpath = ".//*[contains(text () , '" + courseTitle
					+ "')]/ancestor::div/following-sibling::div//div[@data-role='ico_ThreeDots']";
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, xpath));
			fc.utobj().clickElement(driver, pobj.modify);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			courseTitle = fc.utobj().generateTestData(dataSet.get("courseName"));
			fc.utobj().sendKeys(driver, pobj.courseName, courseTitle);
			String testData22 = fc.utobj().generateTestData(dataSet.get("courseObjective"));
			fc.utobj().sendKeys(driver, pobj.courseObjective, testData22);
			fc.utobj().printTestStep("Testing for different");
			WebElement element = driver.findElement(By.xpath(".//*[@id='fc-drop-parentuserCombo']/div/div/input"));
			fc.utobj().clickElement(driver, ".//*[@id='fc-drop-parentuserCombo']/button");
			fc.utobj().sendKeys(driver, element, "FranConnect Administrator");
			Thread.sleep(5000);
			element.sendKeys(Keys.ENTER);
			Thread.sleep(1000);
			fc.utobj().clickElement(driver,".//*[@id='fc-drop-parentuserCombo']/div/ul/li/.//*[contains(text(),'FranConnect Administrator')]");
			fc.utobj().sendKeys(driver, pobj.instructions, dataSet.get("instructions"));		
			fc.utobj().clickElement(driver, pobj.modifycourse);

			fc.utobj().printTestStep("Verify The Add Course");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"training","TrainingCourse123"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-15", testCaseDescription = "Verify Deletion of the Course At Admin Training Course Management Page", testCaseId = "TC_07_Delete_Course")
	private void deleteCourse() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("training", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminTrainingCoureseManagementPage pobj = new AdminTrainingCoureseManagementPage(driver);
			fc.utobj().printTestStep("Navigate To Admin Training Course Management Page");
			fc.utobj().printTestStep("Add Category");
			String categoryname = addCategory(driver, dataSet);
			fc.utobj().printTestStep("Add Course");
			String courseTitle = addCourse(driver, dataSet, categoryname, testCaseId);
			;

			fc.training().training_common().adminTraingCourseManagementPage(driver);

			// Modify Course:::
			fc.utobj().printTestStep("Modify Course");

			// course modify Action Img
			/*fc.utobj().clickElement(driver, ".//*[@id='dialogV']//span[contains(@class,'ellipsis')]");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='dialogV']//input"),
					categoryname);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					(".//*[@id='mCSB_1_container']//a[contains(text (), '" + categoryname + "')]")));*/
			
			fc.utobj().printTestStep("Search Category");
			searchCategory(driver, pobj, categoryname);

			String nameOfCourseToBeDelete = courseTitle;
			fc.utobj().clickElement(driver, ".//div[@data-role='ico_Filter']");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='searchCourse']"), courseTitle);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='search']"));
			String xpath = ".//*[contains(text () , '" + courseTitle
					+ "')]/ancestor::div/following-sibling::div//div[@data-role='ico_ThreeDots']";

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, xpath));

			fc.utobj().clickElement(driver, pobj.delete1);

			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Delete Course");

			boolean isTextPresent = fc.utobj().assertPageSource(driver, nameOfCourseToBeDelete);
			if (isTextPresent == true) {
				fc.utobj().throwsException("was not able to delete Course!!");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "training", "TrainingCourse123" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify CopyUrl of the Course At Admin Training Course Management Page", testCaseId = "TC_Verify_Copy_URL")
	private void CopyUrl() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("training", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminTrainingCoureseManagementPage pobj = new AdminTrainingCoureseManagementPage(driver);
			fc.utobj().printTestStep("Navigate To Admin Training Course Management Page");
			fc.utobj().printTestStep("Add Category");
			String categoryname = addCategory(driver, dataSet);
			fc.utobj().printTestStep("Add Course");
			String courseTitle = addCourse(driver, dataSet, categoryname, testCaseId);
			;

			fc.training().training_common().adminTraingCourseManagementPage(driver);

			// Modify Course:::
			fc.utobj().printTestStep("Modify Course");

			// course modify Action Img
			/*fc.utobj().clickElement(driver, pobj.GeneralCategory);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='dialogV']//input"),
					categoryname);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					(".//*[@id='mCSB_1_container']//a[contains(text (), '" + categoryname + "')]")));*/
			fc.utobj().printTestStep("Search Category");
			searchCategory(driver, pobj, categoryname);
			
			String nameOfCourseToBeDelete = courseTitle;
			fc.utobj().clickElement(driver, ".//div[@data-role='ico_Filter']");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='searchCourse']"), courseTitle);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='search']"));
			String xpath = ".//*[contains(text () , '" + courseTitle
					+ "')]/ancestor::div/following-sibling::div//div[@data-role='ico_ThreeDots']";
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, xpath));

			fc.utobj().clickElement(driver, pobj.copyUrlLinkText1);
			try {

				fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
				fc.utobj().clickElement(driver, ".//*[@id='copyButton']");
				fc.utobj().clickElement(driver, ".//*[@id='siteMainDiv1111']//button[contains(text(), ' Close ')]");

			} catch (Exception ex) {
				fc.commonMethods().switch_cboxIframe_frameId(driver);
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "training")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Participants of the Course At Admin Training Course Management Page", testCaseId = "TC_Verify_Paricipants_Of_Course")
	private void Participants() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("training", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminTrainingCoureseManagementPage pobj = new AdminTrainingCoureseManagementPage(driver);
			fc.utobj().printTestStep("Navigate To Admin Training Course Management Page");
			fc.utobj().printTestStep("Add Category");
			String categoryname = addCategory(driver, dataSet);
			fc.utobj().printTestStep("Add Course");
			String courseTitle = addCourse(driver, dataSet, categoryname, testCaseId);
			;

			fc.training().training_common().adminTraingCourseManagementPage(driver);

			// Modify Course:::
			fc.utobj().printTestStep("Modify Course");

			// course modify Action Img
			/*fc.utobj().clickElement(driver, pobj.GeneralCategory);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='dialogV']//input"),
					categoryname);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					(".//*[@id='mCSB_1_container']//a[contains(text (), '" + categoryname + "')]")));*/
			
			fc.utobj().printTestStep("Search Category");
			searchCategory(driver, pobj, categoryname);

			String nameOfCourseToBeDelete = courseTitle;
			fc.utobj().clickElement(driver, ".//div[@data-role='ico_Filter']");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='searchCourse']"), courseTitle);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='search']"));
			String xpath = ".//*[contains(text () , '" + courseTitle
					+ "')]/ancestor::div/following-sibling::div//div[@data-role='ico_ThreeDots']";
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, xpath));

			fc.utobj().clickElement(driver, ".//*[@id='dialogV']//a[contains(text(),'Participants')]");

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "training", "TrainingCourse123" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Send Invite of the Course At Admin Training Course Management Page", testCaseId = "TC_Invite_Users_For_Course")
	private void InviteUser() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("training", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			String corpuser = addCorporateUser(driver, config, dataSet, testCaseId,
					"CorpUser" + fc.utobj().generateRandomNumber());
			fc.utobj().printTestStep("Navigating to Admin > Training > Course Management Page");
			AdminTrainingCourseManagementAddSectionrPage pobj = new AdminTrainingCourseManagementAddSectionrPage(
					driver);
			AdminTrainingCoursesManagementPageTest courseManagement = new AdminTrainingCoursesManagementPageTest();
			fc.utobj().printTestStep("Add Category");
			String categoryname = courseManagement.addCategory(driver, dataSet);
			fc.utobj().printTestStep("Add Course");
			String courseTitle = courseManagement.addCourse(driver, dataSet, categoryname, testCaseId);

			fc.utobj().printTestStep("Create Lesson");
			fc.utobj().clickElement(driver, pobj.addLesson);

			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			String sectionTitle = fc.utobj().generateTestData(dataSet.get("sectionTitle"));
			fc.utobj().sendKeys(driver, pobj.sectionTitle, sectionTitle);
			String summary = fc.utobj().generateTestData(dataSet.get("summary"));
			fc.utobj().sendKeys(driver, pobj.summary, summary);
			fc.utobj().clickElement(driver, pobj.addLesson);

			driver.switchTo().defaultContent();
			fc.utobj().clickElement(driver, pobj.uploadFile);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			String fileName = fc.utobj().getFilePathFromTestData("taskFile.pdf");
			fc.utobj().sendKeys(driver, pobj.fileuploader, fileName);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='saveButton']"));
			fc.utobj().printTestStep("Verify Add Section Regular Type");
			driver.switchTo().defaultContent();
			boolean isTextPresent = fc.utobj().assertPageSource(driver, sectionTitle);
			if (isTextPresent == false) {
				fc.utobj().throwsException("Was not able to add Section!!!");
			}
			AdminTrainingPlanAndCertificateTest apt = new AdminTrainingPlanAndCertificateTest();
			apt.publishCourse(categoryname, courseTitle, driver, config, testCaseId);
			fc.training().training_common().adminTraingCourseManagementPage(driver);
			fc.utobj().clickElement(driver, ".//*[@id='dialogV']//span[contains(text (), 'General Category')]");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='dialogV']//input"),
					categoryname);
			/*fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					(".//*[@id='mCSB_1_container']//a[contains(text (), '" + categoryname + "')]")));*/
			fc.utobj().clickLink(driver, categoryname);

			String nameOfCourseToBeDelete = courseTitle;
			fc.utobj().clickElement(driver, ".//div[@data-role='ico_Filter']");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='searchCourse']"), courseTitle);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='search']"));
			String xpath = ".//*[contains(text () , '" + courseTitle
					+ "')]/ancestor::div/following-sibling::div//div[@data-role='ico_ThreeDots']";
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, xpath));

			fc.utobj().clickElement(driver, pobj.inviteUser);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='addressLinkDiv']"));
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='searchCorpUsers']"), corpuser);

			fc.utobj().clickElement(driver, pobj.searchCorpUser);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='corpCheckbox']"));

			fc.utobj().clickElement(driver, pobj.Add);

			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='MailSubject']"),
					"Invitation to enrol for a course");

			fc.utobj().clickElement(driver, pobj.sendInvite);
			fc.utobj().printTestStep("Mail recieved is not check on Corporate User End");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	void searchCategory(WebDriver driver,AdminTrainingCoureseManagementPage pobj,String categoryname) throws Exception{
		try {
			fc.utobj().clickElement(driver, pobj.searchCategoryDropArrow);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='dialogV']//input"),
					categoryname);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[contains(text(),'"+categoryname+"')]"));
		} catch (Exception e) {
			Reporter.log("Not able to search category : "+e.getMessage());
			fc.utobj().throwsException("Not able to search category");
		}
	}
}
