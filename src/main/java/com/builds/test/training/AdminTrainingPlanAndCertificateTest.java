package com.builds.test.training;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminDivisionAddDivisionPageTest;
import com.builds.test.admin.AdminFranchiseLocationAddFranchiseLocationPageTest;
import com.builds.test.admin.AdminUsersManageManageFranchiseUsersPageTest;
import com.builds.test.admin.AdminUsersRolesAddNewRolePageTest;
import com.builds.uimaps.training.AdminTrainingCoureseManagementPage;
import com.builds.uimaps.training.AdminTrainingCourseManagementAddSectionrPage;
import com.builds.uimaps.training.AdminTrainingPlanAndCertificatePage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminTrainingPlanAndCertificateTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "training", "trainingsmoke", "Checktest"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Create Plan ", testCaseId = "TC_Create_Plan")
	private void CreatePlan() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("training", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			driver = fc.loginpage().login(driver);
			AdminDivisionAddDivisionPageTest p1 = new AdminDivisionAddDivisionPageTest();
			String divisionName = fc.utobj().generateTestData("divisionName");
			String zipCode = fc.utobj().generateTestData("zipCode");
			p1.addDivisionByGeographyZipCode(driver, divisionName, zipCode, config);
			AdminTrainingPlanAndCertificatePage pobj = new AdminTrainingPlanAndCertificatePage(driver);
			fc.utobj().printTestStep("Navigating to Admin > Training > Question Library Page");
			fc.training().training_common().adminTraingPlanAndCertificatesPage(driver);
			fc.utobj().clickElement(driver, pobj.Create);
			fc.utobj().clickElement(driver, pobj.CreatePlan);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			boolean check = fc.utobj().assertPageSource(driver, "Fields marked with");
			if (!check) {
				throw new Exception("Mandatory field value is not found ");
			}
			String planName = dataSet.get("PlanName") + fc.utobj().generateRandomNumber();
			fc.utobj().sendKeys(driver, pobj.planName, planName);

			try {
				fc.utobj().clickElement(driver, ".//*[@id='fc-drop-parentbrands']/button");
				fc.utobj().sendKeys(driver, pobj.divisionName, divisionName);
				Thread.sleep(5000);
				pobj.divisionName.sendKeys(Keys.ENTER);
				Thread.sleep(1000);
				fc.utobj().clickElement(driver, ".//*[@id='fc-drop-parentbrands']/div/ul/li[1]/label/span");
			} catch (Exception ex) {
				fc.utobj().printTestStep("Not able to add Division while creating plan");
			}
			fc.utobj().sendKeys(driver, pobj.Description, dataSet.get("Description"));
			List<WebElement> li = new LinkedList<WebElement>();
			li.add(pobj.startdate);
			li.add(pobj.enddate);
			fc.utobj().isElementPresent(li);
			fc.utobj().clickElement(driver, pobj.savePlan);
			fc.training().training_common().adminTraingPlanAndCertificatesPage(driver);
			fc.utobj().clickElement(driver, pobj.viewperpage);
			fc.utobj().clickElement(driver, ".//a[contains(text(),'500')]");
			fc.utobj().clickElement(driver, pobj.FilterBtn);
			fc.utobj().sendKeys(driver, pobj.planName, planName);
			fc.utobj().clickElement(driver, pobj.applyFilters);
			fc.utobj().clickPartialLinkText(driver, planName);
			check = fc.utobj().assertPageSource(driver, dataSet.get("Description"));
			if (!check) {
				throw new Exception("while clicking on Plan name description not came");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "training", "trainingsmoke" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Create Plan ", testCaseId = "TC_Create_Incomplete_Plan")
	private void CreateIncompletePlan() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("training", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			driver = fc.loginpage().login(driver);
			AdminTrainingPlanAndCertificatePage pobj = new AdminTrainingPlanAndCertificatePage(driver);
			fc.utobj().printTestStep("Navigating to Admin > Training > Question Library Page");
			fc.training().training_common().adminTraingPlanAndCertificatesPage(driver);
			fc.utobj().clickElement(driver, pobj.Create);
			fc.utobj().clickElement(driver, pobj.CreatePlan);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			boolean check = fc.utobj().assertPageSource(driver, "Fields marked with");
			if (!check) {
				throw new Exception("Mandatory field value is not found ");
			}
			String planName = dataSet.get("PlanName") + fc.utobj().generateRandomNumber();
			fc.utobj().sendKeys(driver, pobj.planName, planName);
			fc.utobj().sendKeys(driver, pobj.Description, dataSet.get("Description"));
			fc.utobj().clickElement(driver, ".//*[@id='SendAllRecipients']/label");
			fc.utobj().sendKeys(driver, pobj.days, "2");
			fc.utobj().clickElement(driver, pobj.savePlan);
			fc.training().training_common().adminTraingPlanAndCertificatesPage(driver);
			fc.utobj().clickElement(driver, pobj.viewperpage);
			fc.utobj().clickElement(driver, ".//a[contains(text(),'500')]");
			fc.utobj().clickElement(driver, pobj.FilterBtn);
			fc.utobj().sendKeys(driver, pobj.planName, planName);
			fc.utobj().clickElement(driver, pobj.applyFilters);
			fc.utobj().clickPartialLinkText(driver, planName);
			check = fc.utobj().assertPageSource(driver, dataSet.get("Description"));
			if (!check) {
				throw new Exception("while clicking on Plan name description not came");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "training", "trainingsmoke", "Checktest" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Create Plan ", testCaseId = "TC_Create_Plan_with_Blank_Field")
	private void CreatePlanWithBlankField() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("training", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);
			AdminTrainingPlanAndCertificatePage pobj = new AdminTrainingPlanAndCertificatePage(driver);
			fc.utobj().printTestStep("Navigating to Admin > Training > Question Library Page");
			fc.training().training_common().adminTraingPlanAndCertificatesPage(driver);
			fc.utobj().clickElement(driver, pobj.Create);
			fc.utobj().clickElement(driver, pobj.CreatePlan);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			boolean check = fc.utobj().assertPageSource(driver, "Fields marked with");
			if (!check) {
				throw new Exception("Mandatory field value is not found ");
			}
			String planName = dataSet.get("PlanName") + fc.utobj().generateRandomNumber();
			fc.utobj().sendKeys(driver, pobj.planName, "");
			fc.utobj().sendKeys(driver, pobj.Description, "");
			fc.utobj().clickElement(driver, pobj.savePlan);
			check = fc.utobj().assertPageSource(driver, "This field is required.");//
			if (!check) {
				throw new Exception("please check some problem while adding with blank field ");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}

	@Test(groups = { "training", "trainingsmoke" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Create Plan ", testCaseId = "TC_Verify_Differents_Status_Of_Plans")
	private void VerifyDifferentStatusOfPlan() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("training", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			driver = fc.loginpage().login(driver);
			String parentWindowHandle = driver.getWindowHandle();
			fc.utobj().printTestStep("Navigating to Admin > Training > Course Management Page");
			AdminTrainingCourseManagementAddSectionrPage pobj = new AdminTrainingCourseManagementAddSectionrPage(
					driver);
			AdminTrainingPlanAndCertificatePage pobjpnc = new AdminTrainingPlanAndCertificatePage(driver);
			AdminTrainingCoursesManagementPageTest courseManagement = new AdminTrainingCoursesManagementPageTest();
			fc.utobj().printTestStep("Add Category");
			String categoryname = courseManagement.addCategory(driver, dataSet);
			fc.utobj().printTestStep("Add Course");
			String coursename = courseManagement.addCourse(driver, dataSet, categoryname, testCaseId);
			fc.utobj().printTestStep("Create Lesson");
			fc.utobj().clickElement(driver, pobj.addLesson);

			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			String sectionTitle = fc.utobj().generateTestData(dataSet.get("sectionTitle"));
			fc.utobj().sendKeys(driver, pobj.sectionTitle, sectionTitle);
			String summary = fc.utobj().generateTestData(dataSet.get("summary"));
			fc.utobj().sendKeys(driver, pobj.summary, summary);
			fc.utobj().clickElement(driver, pobj.addLesson);

			driver.switchTo().defaultContent();
			fc.utobj().clickElement(driver, pobj.uploadFile);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			String fileName = fc.utobj().getFilePathFromTestData("taskFile.pdf");
			fc.utobj().sendKeys(driver, pobj.fileuploader, fileName);

			AdminTrainingCourseManagementAddSectionPageTest objadd = new AdminTrainingCourseManagementAddSectionPageTest();
			objadd.addQuizBeforeCreatingLesson(driver, sectionTitle, dataSet, config, testCaseId);

			publishCourse(categoryname, coursename, driver, config, testCaseId);
			String Certificate = CreateCertificate_indieMethod(driver, config, dataSet, testCaseId, pobjpnc);
			String planName = CreatePlanwithCourseAndCertiicate(driver, config, dataSet, testCaseId, pobjpnc,
					coursename, Certificate);
			publishplan(driver, config, dataSet, testCaseId, pobjpnc, coursename, planName);
			fc.training().training_common().trainingPlans(driver);
			fc.utobj().clickElement(driver, pobjpnc.availablePlan);
			fc.utobj().clickElement(driver, pobjpnc.FilterBtn);
			fc.utobj().sendKeys(driver, pobjpnc.planName, planName);
			fc.utobj().clickElement(driver, pobjpnc.applyFilters);
			fc.utobj().clickElement(driver, pobjpnc.subscribe);
			fc.utobj().clickElement(driver, pobjpnc.start);
			fc.utobj().clickElement(driver, pobjpnc.startLearningN);
			System.out.println();
			Set<String> allWindowHandles = driver.getWindowHandles();
			System.out.println(allWindowHandles.size());
			for (String currentWindowHandle : allWindowHandles) {
				if (!currentWindowHandle.equals(parentWindowHandle)) {
					driver.switchTo().window(currentWindowHandle);

					try {
						fc.utobj().getElementByXpath(driver, ".//p[contains(text () , '" + sectionTitle + "')]")
								.isDisplayed();
						fc.utobj().moveToElement(driver, fc.utobj().getElement(driver, pobjpnc.closequiz));
						fc.utobj().clickElement(driver, pobjpnc.closequiz);
					} catch (Exception e) {
						System.out.println("we reached here");
					}
					driver.switchTo().window(parentWindowHandle);
				}
			}
			fc.training().training_common().trainingPlans(driver);
			fc.utobj().clickElement(driver, pobjpnc.FilterBtn);
			fc.utobj().sendKeys(driver, pobjpnc.planName, planName);
			fc.utobj().clickElement(driver, pobjpnc.applyFilters);
			fc.utobj().clickElement(driver, pobjpnc.resume);
			fc.utobj().clickElement(driver, pobjpnc.resume);
			fc.utobj().clickElement(driver, pobjpnc.startLearningN);
			Set<String> allWindowHandles1 = driver.getWindowHandles();
			System.out.println(allWindowHandles.size());
			for (String currentWindowHandle : allWindowHandles1) {
				if (!currentWindowHandle.equals(parentWindowHandle)) {
					driver.switchTo().window(currentWindowHandle);

					try {
						fc.utobj().getElementByXpath(driver, ".//p[contains(text () , '" + sectionTitle + "')]")
								.isDisplayed();
						fc.utobj().moveToElement(driver, fc.utobj().getElement(driver, pobjpnc.startquiz));
						fc.utobj().clickElement(driver, pobjpnc.startquiz);
					} catch (Exception e) {
						System.out.println("we reached here");
					}
					driver.switchTo().window(parentWindowHandle);
				}
			}
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			fc.utobj().clickElement(driver, pobjpnc.startquiz);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, pobjpnc.insertDate);
			DateFormat dateFormat2 = new SimpleDateFormat("dd");
			Date date2 = new Date();
			String today = dateFormat2.format(date2);
			WebElement dateWidget = fc.utobj().getElementByID(driver, "fc-datepicker-div");
			List<WebElement> columns = dateWidget.findElements(By.tagName("a"));
			for (WebElement cell : columns) {
				if (cell.getText().equals(today)) {
					cell.click();
					break;
				}
			}
			fc.utobj().clickElement(driver, pobjpnc.finishQuiz);
			fc.utobj().clickElement(driver, pobjpnc.SubmitQuiz);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			fc.utobj().clickElement(driver, pobjpnc.Close);

			fc.training().training_common().trainingPlans(driver);
			fc.utobj().isElementPresent(driver, pobjpnc.InProgressPlan);
			fc.utobj().isElementPresent(driver, pobjpnc.Expired);
			fc.utobj().clickElement(driver, pobjpnc.CompletedPlan);
			fc.utobj().clickElement(driver, pobjpnc.FilterBtn);
			fc.utobj().sendKeys(driver, pobjpnc.planName, planName);
			fc.utobj().clickElement(driver, pobjpnc.applyFilters);
			boolean found = fc.utobj().assertPageSource(driver, planName);
			if (found)
				fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void publishplan(WebDriver driver, Map<String, String> config, Map<String, String> dataSet,
			String testCaseId, AdminTrainingPlanAndCertificatePage pobjpnc, String coursename, String plan)
			throws Exception {
		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigating to Admin > Training > Question Library Page");
			fc.training().training_common().adminTraingPlanAndCertificatesPage(driver);
			fc.utobj().clickElement(driver, pobjpnc.planTab);
			fc.utobj().clickElement(driver, pobjpnc.FilterBtn);
			fc.utobj().sendKeys(driver, pobjpnc.planfieldtabinsearch, plan);
			fc.utobj().clickElement(driver, pobjpnc.searchfilter);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@data-role=\"ico_ThreeDots\"]"));
			fc.utobj().clickElement(driver, pobjpnc.Publish);

			fc.utobj().clickElement(driver, pobjpnc.Confirm);
		} catch (Exception ex) {
			fc.utobj().throwsException("Not able to create Plan with course and certifcate");
		}
	}

	public String CreatePlanwithCourseAndCertiicate(WebDriver driver, Map<String, String> config,
			Map<String, String> dataSet, String testCaseId, AdminTrainingPlanAndCertificatePage pobjpnc,
			String coursename, String certificate) throws Exception {
		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigating to Admin > Training > Question Library Page");
			fc.training().training_common().adminTraingPlanAndCertificatesPage(driver);
			fc.utobj().clickElement(driver, pobjpnc.Create);
			fc.utobj().clickElement(driver, pobjpnc.CreatePlan);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			String planName = dataSet.get("PlanName") + fc.utobj().generateRandomNumber();
			fc.utobj().sendKeys(driver, pobjpnc.planName, planName);
			fc.utobj().sendKeys(driver, pobjpnc.Description, dataSet.get("Description"));
			fc.utobj().clickElement(driver, pobjpnc.startdate);
			DateFormat dateFormat2 = new SimpleDateFormat("dd");
			Date date2 = new Date();

			int day = Integer.parseInt(dateFormat2.format(date2));
			day++;
			String today = String.valueOf(day);
			int dayis = Integer.parseInt(today);
			dayis=dayis-1;

			// find the calendar
			WebElement dateWidget = fc.utobj().getElementByID(driver, "fc-datepicker-div");
			List<WebElement> columns = dateWidget.findElements(By.tagName("a"));

			// comparing the text of cell with today's date and clicking it.
			for (WebElement cell : columns) {
				if (cell.getText().equals(String.valueOf(dayis))) {
					cell.click();
					break;
				}
			}

			fc.utobj().clickElement(driver, pobjpnc.enddate);
			// find the calendar
			dateWidget = fc.utobj().getElementByID(driver, "fc-datepicker-div");
			List<WebElement> columns1 = dateWidget.findElements(By.tagName("a"));

			// comparing the text of cell with today's date and clicking it.
			for (WebElement cell : columns1) {
				if (cell.getText().equals(String.valueOf(dayis))) {
					cell.click();
					break;
				}
			}

			fc.utobj().clickElement(driver, pobjpnc.savePlan);
			fc.utobj().clickElement(driver, pobjpnc.addCourses);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			fc.utobj().clickElement(driver, pobjpnc.FilterBtn);
			fc.utobj().sendKeys(driver, pobjpnc.courseNamefilter, coursename);
			fc.utobj().clickElement(driver, pobjpnc.searchfilter);
			fc.utobj().clickElement(driver, pobjpnc.selectallChkbx);
			fc.utobj().clickElement(driver, pobjpnc.addinCourse);

			fc.utobj().clickElement(driver, pobjpnc.addCertificate);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			fc.utobj().clickElement(driver, pobjpnc.FilterBtn);
			fc.utobj().sendKeys(driver, pobjpnc.certificateName, certificate);
			fc.utobj().clickElement(driver, pobjpnc.applyFilters);
			fc.utobj().clickElement(driver, pobjpnc.selectallChkbx);
			fc.utobj().clickElement(driver, pobjpnc.addinCourse);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, pobjpnc.saveCourse);
			return planName;
		} catch (Exception ex) {
			fc.utobj().throwsException("Not able to create Plan with course and certifcate");
		}
		return null;
	}

	public String CreateCertificate_indieMethod(WebDriver driver, Map<String, String> config,
			Map<String, String> dataSet, String testCaseId, AdminTrainingPlanAndCertificatePage pobjpnc)
			throws Exception {
		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigating to Admin > Training > Question Library Page");
			fc.training().training_common().adminTraingPlanAndCertificatesPage(driver);
			fc.utobj().clickElement(driver, pobjpnc.Create);
			fc.utobj().clickElement(driver, pobjpnc.CreateCertificate);
			String CertificateName = dataSet.get("CertificateName") + fc.utobj().generateRandomNumber();
			String CertificateHeading = dataSet.get("CertificateHeading") + fc.utobj().generateRandomNumber();
			String CertificateSubHeading = dataSet.get("CertificateSubHeading") + fc.utobj().generateRandomNumber();
			String CertificateMeassage = dataSet.get("CertificateMeassage") + fc.utobj().generateRandomNumber();
			fc.utobj().sendKeys(driver, pobjpnc.certificateName, CertificateName);
			fc.utobj().sendKeys(driver, pobjpnc.Description, dataSet.get("Description"));
			Actions action = new Actions(driver);
			action.moveToElement(pobjpnc.Template1).build().perform();

			fc.utobj().clickElement(driver, pobjpnc.certificateTemplate);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			String logo = fc.utobj().getFilePathFromTestData("logo");
			fc.utobj().sendKeys(driver, pobjpnc.UploadcertificateLogo, logo);
			fc.utobj().sendKeys(driver, pobjpnc.certificateHeading, CertificateHeading);
			fc.utobj().sendKeys(driver, pobjpnc.certificateSubHeading, CertificateSubHeading);
			fc.utobj().sendKeys(driver, pobjpnc.certificateMessage, CertificateMeassage);
			String Signature = fc.utobj().getFilePathFromTestData("sign");
			fc.utobj().sendKeys(driver, pobjpnc.UploadcertificateSignature, Signature);
			fc.utobj().clickElement(driver, pobjpnc.saveCertificate);
			Thread.sleep(5000);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, pobjpnc.save);
			fc.training().training_common().adminTraingPlanAndCertificatesPage(driver);
			fc.utobj().clickElement(driver, pobjpnc.CertificateTab);
			fc.utobj().clickElement(driver, pobjpnc.FilterBtn);
			fc.utobj().sendKeys(driver, pobjpnc.certificateName, CertificateName);
			fc.utobj().clickElement(driver, pobjpnc.applyFilters);
			fc.utobj().clickPartialLinkText(driver, CertificateName);
			return CertificateName;
		} catch (Exception ex) {
			fc.utobj().throwsException("Not Able to Create Certificate");
		}
		return null;
	}

	@Test(groups = { "training" ,"failedTraining" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify all status of Course ", testCaseId = "TC_Verify_All_Status_Of_Course")
	private void VerifyAllStatusOfCourse() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("training", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);
			String parentWindowHandle = driver.getWindowHandle();

			fc.utobj().printTestStep("Navigating to Admin > Training > Course Management Page");
			AdminTrainingCourseManagementAddSectionrPage pobj = new AdminTrainingCourseManagementAddSectionrPage(
					driver);

			AdminTrainingPlanAndCertificatePage pobjpnc = new AdminTrainingPlanAndCertificatePage(driver);
			AdminTrainingCoursesManagementPageTest courseManagement = new AdminTrainingCoursesManagementPageTest();
			fc.utobj().printTestStep("Add Category");
			String categoryname = courseManagement.addCategory(driver, dataSet);
			fc.utobj().printTestStep("Add Course");
			String coursename = courseManagement.addCourse(driver, dataSet, categoryname, testCaseId);

			fc.utobj().printTestStep("Create Lesson");
			fc.utobj().clickElement(driver, pobj.addLesson);

			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			String sectionTitle = fc.utobj().generateTestData(dataSet.get("sectionTitle"));
			fc.utobj().sendKeys(driver, pobj.sectionTitle, sectionTitle);
			String summary = fc.utobj().generateTestData(dataSet.get("summary"));
			fc.utobj().sendKeys(driver, pobj.summary, summary);
			fc.utobj().clickElement(driver, pobj.addLesson);

			driver.switchTo().defaultContent();
			fc.utobj().clickElement(driver, pobj.uploadFile);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			String fileName = fc.utobj().getFilePathFromTestData("taskFile.pdf");
			fc.utobj().sendKeys(driver, pobj.fileuploader, fileName);

			AdminTrainingCourseManagementAddSectionPageTest objadd = new AdminTrainingCourseManagementAddSectionPageTest();
			String quizname = objadd.addQuizBeforeCreatingLesson(driver, sectionTitle, dataSet, config, testCaseId);

			driver.switchTo().defaultContent();
			publishCourse(categoryname, coursename, driver, config, testCaseId);
			fc.training().training_common().trainingCourse(driver);
			fc.utobj().clickElement(driver, pobjpnc.availablePlan);
			fc.utobj().clickElement(driver, pobjpnc.FilterBtn);
			fc.utobj().sendKeys(driver, pobjpnc.coursename, coursename);
			fc.utobj().clickElement(driver, pobjpnc.applyFilters);
			fc.utobj().clickElement(driver, pobjpnc.start);
			try {
				fc.utobj().clickElement(driver, pobj.mynotes);
				fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
				fc.utobj().clickElement(driver, pobj.addnotes);
				fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
				fc.utobj().sendKeys(driver, pobj.txtarea, "Hello Testing purpose only");
				fc.utobj().clickElement(driver, pobj.add);
				fc.utobj().switchFrameToDefault(driver);
				fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
				fc.utobj().clickElement(driver, pobj.deletenote);
				fc.utobj().acceptAlertBox(driver);
				fc.utobj().clickElement(driver, pobj.close);
				fc.utobj().switchFrameToDefault(driver);
				fc.utobj().clickElement(driver, pobj.viewperpage);
				fc.utobj().clickElement(driver, ".//a[contains(text(),'500')]");
				String option = "Start Learning";
				List<String> linkArray = fc.utobj().translate(option);
				if (!(linkArray.size() < 1)) {
					for (int i = 0; i < linkArray.size(); i++) {
						try {
							option = linkArray.get(i);
							// .//text()[normalize-space()="Start Learning"]
							fc.utobj().clickElement(driver,
									".//*/text()[normalize-space()=\"" + option + "\"]/parent::*");
							break;
						} catch (Exception e) {
						}
					}
				} else {
					fc.utobj().clickElement(driver, ".//*/text()[normalize-space()=\"" + option + "\"]/parent::*");
				}

			} catch (Exception e) {
				fc.utobj().clickElement(driver, pobjpnc.startLearning);
			}

			System.out.println();

			Set<String> allWindowHandles = driver.getWindowHandles();

			System.out.println(allWindowHandles.size());

			for (String currentWindowHandle : allWindowHandles) {
				if (!currentWindowHandle.equals(parentWindowHandle)) {
					driver.switchTo().window(currentWindowHandle);

					try {
						fc.utobj().getElementByXpath(driver, ".//p[contains(text () , '" + sectionTitle + "')]")
								.isDisplayed();
						fc.utobj().moveToElement(driver, fc.utobj().getElement(driver, pobjpnc.closequiz));
						fc.utobj().clickElement(driver, pobjpnc.closequiz);
					} catch (Exception e) {
						System.out.println("we reached here");
					}
					driver.switchTo().window(parentWindowHandle);
				}
			}

			fc.training().training_common().trainingCourse(driver);
			fc.utobj().clickElement(driver, pobjpnc.FilterBtn);
			fc.utobj().sendKeys(driver, pobjpnc.coursename, coursename);
			fc.utobj().clickElement(driver, pobjpnc.applyFilters);
			fc.utobj().clickElement(driver, pobjpnc.resume);
			// fc.utobj().clickElement(driver, pobjpnc.startLearning);

			Set<String> allWindowHandles1 = driver.getWindowHandles();

			System.out.println(allWindowHandles.size());

			for (String currentWindowHandle : allWindowHandles1) {
				if (!currentWindowHandle.equals(parentWindowHandle)) {
					driver.switchTo().window(currentWindowHandle);

					try {
						fc.utobj().getElementByXpath(driver, ".//p[contains(text () , '" + sectionTitle + "')]")
								.isDisplayed();
						fc.utobj().moveToElement(driver, fc.utobj().getElement(driver, pobjpnc.startquiz));
						fc.utobj().clickElement(driver, pobjpnc.startquiz);
					} catch (Exception e) {
						System.out.println("we reached here");
					}
					driver.switchTo().window(parentWindowHandle);
				}
			}
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[contains(text(),'Start Quiz')]"));

			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			fc.utobj().clickElement(driver, pobjpnc.startquiz);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, pobjpnc.insertDate);

			DateFormat dateFormat2 = new SimpleDateFormat("dd");
			Date date2 = new Date();

			String today = dateFormat2.format(date2);

			// find the calendar
			WebElement dateWidget = fc.utobj().getElementByID(driver, "fc-datepicker-div");
			List<WebElement> columns = dateWidget.findElements(By.tagName("a"));

			// comparing the text of cell with today's date and clicking it.
			for (WebElement cell : columns) {
				if (cell.getText().equals(today)) {
					cell.click();
					break;
				}
			}
			// fc.utobj().sendKeys(driver, pobjpnc.insertDate, "03/03/2017");
			fc.utobj().clickElement(driver, pobjpnc.finishQuiz);
			fc.utobj().clickElement(driver, pobjpnc.SubmitQuiz);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			fc.utobj().clickElement(driver, pobjpnc.Close);

			fc.training().training_common().trainingCourse(driver);
			fc.utobj().clickElement(driver, pobjpnc.CompletedPlan);
			fc.utobj().clickElement(driver, pobjpnc.FilterBtn);
			fc.utobj().sendKeys(driver, pobjpnc.coursename, coursename);
			fc.utobj().clickElement(driver, pobjpnc.applyFilters);

			// Working on Assessment from here
			fc.training().training_common().trainingAssessment(driver);
			fc.utobj().clickElement(driver, pobj.viewperpage);
			fc.utobj().clickElement(driver, ".//a[contains(text(),'500')]");
			fc.utobj().clickElement(driver, pobjpnc.FilterBtn);

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='fc-drop-parentsectionId']/button"));

			fc.utobj().clickElement(driver, ".//*[@id='fc-drop-parentsectionId']//span[contains(text(),'Select All')]");

			fc.utobj().sendKeys(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='fc-drop-parentsectionId']/div/div/input"),
					"" + sectionTitle + "");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					(".//*[@id='fc-drop-parentsectionId']//span[contains(text(),'" + sectionTitle + "')]")));
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='fc-drop-parentsectionId']/button"));
			fc.utobj().clickElement(driver, pobjpnc.applyFilters);
			fc.utobj().clickPartialLinkText(driver, quizname);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			fc.utobj().assertPageSource(driver, quizname);
			fc.utobj().assertPageSource(driver, "Cancel");
			fc.utobj().clickElement(driver, pobjpnc.startevaluation);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					(".//td[contains(text(),'FranConnect Administrator')]/following-sibling::td[2]/button")));

			Actions action = new Actions(driver);
			action.click(fc.utobj().getElement(driver, pobjpnc.slider)).build().perform();
			Thread.sleep(1000);
			for (int i = 0; i < 2; i++) {
				action.sendKeys(Keys.ARROW_RIGHT).build().perform();
				Thread.sleep(200);
			}
			fc.utobj().clickElement(driver, pobjpnc.reviewScoring);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			fc.utobj().clickElement(driver, pobjpnc.submitassessment);
			fc.utobj().clickElement(driver, pobjpnc.CompletedPlan);
			fc.utobj().clickElement(driver, pobjpnc.FilterBtn);

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='fc-drop-parentsectionId']/button"));

			fc.utobj().clickElement(driver, ".//*[@id='fc-drop-parentsectionId']//span[contains(text(),'Select All')]");

			fc.utobj().sendKeys(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='fc-drop-parentsectionId']/div/div/input"),
					"" + sectionTitle + "");

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					(".//*[@id='fc-drop-parentsectionId']//span[contains(text(),'" + sectionTitle + "')]")));

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='fc-drop-parentsectionId']/button"));
			fc.utobj().clickElement(driver, pobjpnc.applyFilters);

			// working on My Results
			fc.training().training_common().trainingMyResults(driver);
			String date = fc.utobj().generateCurrentDatewithformat("MM/dd/yyyy");
			boolean found1 = fc.utobj().assertPageSource(driver, date);
			fc.utobj().clickElement(driver, pobj.viewperpage);
			fc.utobj().clickElement(driver, ".//a[contains(text(),'500')]");
			fc.utobj().clickElement(driver, pobjpnc.FilterBtn);
			fc.utobj().sendKeys(driver, pobjpnc.quizName, quizname);
			fc.utobj().clickElement(driver, pobjpnc.applyFilters);
			fc.utobj().clickElement(driver, ".//span[contains(text(),\"Pass\")]");
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			fc.utobj().isElementPresent(driver, pobjpnc.print);
			boolean found2 = fc.utobj().assertPageSource(driver, "100.00%");
			boolean found3 = fc.utobj().assertPageSource(driver, "Pass");
			boolean found4 = fc.utobj().assertPageSource(driver, "1");
			boolean found5 = fc.utobj().assertPageSource(driver, "Which date is today ??");
			boolean found6 = fc.utobj().assertPageSource(driver, quizname);
			boolean found7 = fc.utobj().assertPageSource(driver, date);
			if (!found1 && !found2 && !found3 && !found4 && !found5 && !found6 && !found7)
				fc.utobj().throwsException("Please check some points not found on Result view page");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "training", "trainingsmoke", "plan1" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify all status of Course ", testCaseId = "TC_Verify_All_Status_Of_Result_for_Fran_user")
	private void VerifyAllStatusResultForFranchiseUser() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("training", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);
			String parentWindowHandle = driver.getWindowHandle();
			fc.utobj().printTestStep("Navigating to Admin > Training > Course Management Page");
			AdminTrainingCourseManagementAddSectionrPage pobj = new AdminTrainingCourseManagementAddSectionrPage(
					driver);
			AdminTrainingPlanAndCertificatePage pobjpnc = new AdminTrainingPlanAndCertificatePage(driver);
			AdminTrainingCoursesManagementPageTest courseManagement = new AdminTrainingCoursesManagementPageTest();
			fc.utobj().printTestStep("Add Category");
			String categoryname = courseManagement.addCategory(driver, dataSet);
			fc.utobj().printTestStep("Add Course");
			String coursename = courseManagement.addCourse(driver, dataSet, categoryname, testCaseId);

			fc.utobj().printTestStep("Create Lesson");
			fc.utobj().clickElement(driver, pobj.addLesson);

			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			String sectionTitle = fc.utobj().generateTestData(dataSet.get("sectionTitle"));
			fc.utobj().sendKeys(driver, pobj.sectionTitle, sectionTitle);
			String summary = fc.utobj().generateTestData(dataSet.get("summary"));
			fc.utobj().sendKeys(driver, pobj.summary, summary);
			fc.utobj().clickElement(driver, pobj.addLesson);

			driver.switchTo().defaultContent();
			fc.utobj().clickElement(driver, pobj.uploadFile);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			String fileName = fc.utobj().getFilePathFromTestData("taskFile.pdf");
			fc.utobj().sendKeys(driver, pobj.fileuploader, fileName);

			AdminTrainingCourseManagementAddSectionPageTest objadd = new AdminTrainingCourseManagementAddSectionPageTest();
			String quizname = objadd.addQuizBeforeCreatingLesson(driver, sectionTitle, dataSet, config, testCaseId);

			driver.switchTo().defaultContent();
			publishCourse(categoryname, coursename, driver, config, testCaseId);
			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			AdminFranchiseLocationAddFranchiseLocationPageTest franchiseLocation = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String franchiseId = fc.utobj().generateTestData("Testfranid");
			String storeType = fc.utobj().generateTestData("Teststorytype");
			String franRegionName = fc.utobj().generateTestData("Testregname");
			franchiseLocation.addFranchiseLocation_All(driver, franchiseId, franRegionName, storeType);

			fc.utobj().printTestStep("Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest franchiseUser = new AdminUsersManageManageFranchiseUsersPageTest();
			String franUserName = fc.utobj().generateTestData("Testfuser");
			String password = "T0night123";
			String emailId = "automationteam@franconnect.com";
			franchiseUser.addFranchiseUser(driver, franUserName, password, franchiseId, "Default Franchise Role",
					emailId);
			fc.loginpage().loginWithParameter(driver, franUserName, password);
			fc.training().training_common().trainingCourse(driver);
			fc.utobj().clickElement(driver, pobjpnc.availablePlan);
			fc.utobj().clickElement(driver, pobjpnc.FilterBtn);
			fc.utobj().sendKeys(driver, pobjpnc.coursename, coursename);
			fc.utobj().clickElement(driver, pobjpnc.applyFilters);
			fc.utobj().clickElement(driver, pobjpnc.start);
			try {
				fc.utobj().clickElement(driver, pobj.mynotes);
				fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
				fc.utobj().clickElement(driver, pobj.addnotes);
				fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
				fc.utobj().sendKeys(driver, pobj.txtarea, "Hello Testing purpose only");
				fc.utobj().clickElement(driver, pobj.add);
				fc.utobj().switchFrameToDefault(driver);
				fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
				fc.utobj().clickElement(driver, pobj.deletenote);
				fc.utobj().acceptAlertBox(driver);
				fc.utobj().clickElement(driver, pobj.close);
				fc.utobj().switchFrameToDefault(driver);
				fc.utobj().clickElement(driver, pobj.viewperpage);
				fc.utobj().clickElement(driver, ".//a[contains(text(),'500')]");
				String option = "Start Learning";
				List<String> linkArray = fc.utobj().translate(option);
				if (!(linkArray.size() < 1)) {
					for (int i = 0; i < linkArray.size(); i++) {
						try {
							option = linkArray.get(i);
							// .//text()[normalize-space()="Start Learning"]
							fc.utobj().clickElement(driver,
									".//*/text()[normalize-space()=\"" + option + "\"]/parent::*");
							break;
						} catch (Exception e) {
						}
					}
				} else {
					fc.utobj().clickElement(driver, ".//*/text()[normalize-space()=\"" + option + "\"]/parent::*");
				}

			} catch (Exception e) {
				fc.utobj().clickElement(driver, pobjpnc.startLearning);
			}

			System.out.println();

			Set<String> allWindowHandles = driver.getWindowHandles();

			System.out.println(allWindowHandles.size());

			for (String currentWindowHandle : allWindowHandles) {
				if (!currentWindowHandle.equals(parentWindowHandle)) {
					driver.switchTo().window(currentWindowHandle);

					try {
						fc.utobj().getElementByXpath(driver, ".//p[contains(text () , '" + sectionTitle + "')]")
								.isDisplayed();
						fc.utobj().moveToElement(driver, fc.utobj().getElement(driver, pobjpnc.closequiz));
						fc.utobj().clickElement(driver, pobjpnc.closequiz);
					} catch (Exception e) {
						System.out.println("we reached here");
					}
					driver.switchTo().window(parentWindowHandle);
				}
			}

			fc.training().training_common().trainingCourse(driver);
			fc.utobj().clickElement(driver, pobjpnc.FilterBtn);
			fc.utobj().sendKeys(driver, pobjpnc.coursename, coursename);
			fc.utobj().clickElement(driver, pobjpnc.applyFilters);
			fc.utobj().clickElement(driver, pobjpnc.resume);
			// fc.utobj().clickElement(driver, pobjpnc.startLearning);

			Set<String> allWindowHandles1 = driver.getWindowHandles();

			System.out.println(allWindowHandles.size());

			for (String currentWindowHandle : allWindowHandles1) {
				if (!currentWindowHandle.equals(parentWindowHandle)) {
					driver.switchTo().window(currentWindowHandle);

					try {
						fc.utobj().getElementByXpath(driver, ".//p[contains(text () , '" + sectionTitle + "')]")
								.isDisplayed();
						fc.utobj().moveToElement(driver, fc.utobj().getElement(driver, pobjpnc.startquiz));
						fc.utobj().clickElement(driver, pobjpnc.startquiz);
					} catch (Exception e) {
						System.out.println("we reached here");
					}
					driver.switchTo().window(parentWindowHandle);
				}
			}
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[contains(text(),'Start Quiz')]"));

			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			fc.utobj().clickElement(driver, pobjpnc.startquiz);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, pobjpnc.insertDate);

			DateFormat dateFormat2 = new SimpleDateFormat("dd");
			Date date2 = new Date();

			String today = dateFormat2.format(date2);

			// find the calendar
			WebElement dateWidget = fc.utobj().getElementByID(driver, "fc-datepicker-div");
			List<WebElement> columns = dateWidget.findElements(By.tagName("a"));

			// comparing the text of cell with today's date and clicking it.
			for (WebElement cell : columns) {
				if (cell.getText().equals(today)) {
					cell.click();
					break;
				}
			}
			// fc.utobj().sendKeys(driver, pobjpnc.insertDate, "03/03/2017");
			fc.utobj().clickElement(driver, pobjpnc.finishQuiz);
			fc.utobj().clickElement(driver, pobjpnc.SubmitQuiz);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			fc.utobj().clickElement(driver, pobjpnc.Close);

			fc.training().training_common().trainingCourse(driver);
			fc.utobj().clickElement(driver, pobjpnc.CompletedPlan);
			fc.utobj().clickElement(driver, pobjpnc.FilterBtn);
			fc.utobj().sendKeys(driver, pobjpnc.coursename, coursename);
			fc.utobj().clickElement(driver, pobjpnc.applyFilters);

			/*
			 * // Working on Assessment from here
			 * fc.training().training_common().trainingAssessment(driver);
			 * fc.utobj().clickElement(driver, pobj.viewperpage);
			 * fc.utobj().clickElement(driver, ".//a[contains(text(),'500')]");
			 * fc.utobj().clickElement(driver, pobjpnc.FilterBtn);
			 * 
			 * fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
			 * ".//*[@id='fc-drop-parentsectionId']/button"));
			 * 
			 * fc.utobj().clickElement(driver,
			 * ".//*[@id='fc-drop-parentsectionId']//span[contains(text(),'Select All')]");
			 * 
			 * fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver,
			 * ".//*[@id='fc-drop-parentsectionId']/div/div/input"), "" + sectionTitle +
			 * ""); fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
			 * (".//*[@id='fc-drop-parentsectionId']//span[contains(text(),'" + sectionTitle
			 * + "')]"))); fc.utobj().clickElement(driver,
			 * fc.utobj().getElementByXpath(driver,
			 * ".//*[@id='fc-drop-parentsectionId']/button"));
			 * fc.utobj().clickElement(driver, pobjpnc.applyFilters);
			 * fc.utobj().clickPartialLinkText(driver, quizname);
			 * fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			 * fc.utobj().assertPageSource(driver, quizname);
			 * fc.utobj().assertPageSource(driver, "Cancel");
			 * fc.utobj().clickElement(driver, pobjpnc.startevaluation);
			 * fc.utobj().switchFrameToDefault(driver);
			 * 
			 * fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
			 * (".//td[contains(text(),'FranConnect Administrator')]/following-sibling::td[2]/button"
			 * )));
			 * 
			 * Actions action = new Actions(driver);
			 * action.click(fc.utobj().getElement(driver,
			 * pobjpnc.slider)).build().perform(); Thread.sleep(1000); for (int i = 0; i <
			 * 2; i++) { action.sendKeys(Keys.ARROW_RIGHT).build().perform();
			 * Thread.sleep(200); } fc.utobj().clickElement(driver, pobjpnc.reviewScoring);
			 * fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			 * fc.utobj().clickElement(driver, pobjpnc.submitassessment);
			 * fc.utobj().clickElement(driver, pobjpnc.CompletedPlan);
			 * fc.utobj().clickElement(driver, pobjpnc.FilterBtn);
			 * 
			 * fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
			 * ".//*[@id='fc-drop-parentsectionId']/button"));
			 * 
			 * fc.utobj().clickElement(driver,
			 * ".//*[@id='fc-drop-parentsectionId']//span[contains(text(),'Select All')]");
			 * 
			 * fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver,
			 * ".//*[@id='fc-drop-parentsectionId']/div/div/input"), "" + sectionTitle +
			 * "");
			 * 
			 * fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
			 * (".//*[@id='fc-drop-parentsectionId']//span[contains(text(),'" + sectionTitle
			 * + "')]")));
			 * 
			 * fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
			 * ".//*[@id='fc-drop-parentsectionId']/button"));
			 * fc.utobj().clickElement(driver, pobjpnc.applyFilters);
			 * 
			 * // working on My Results
			 * fc.training().training_common().trainingMyResults(driver); String date =
			 * fc.utobj().generateCurrentDatewithformat("MM/dd/yyyy"); boolean found1 =
			 * fc.utobj().assertPageSource(driver, date); fc.utobj().clickElement(driver,
			 * pobj.viewperpage); fc.utobj().clickElement(driver,
			 * ".//a[contains(text(),'500')]"); fc.utobj().clickElement(driver,
			 * pobjpnc.FilterBtn);
			 * 
			 * fc.utobj().sendKeys(driver, pobjpnc.quizName, quizname);
			 * fc.utobj().clickElement(driver, pobjpnc.applyFilters);
			 * fc.utobj().clickElement(driver, ".\\*[contains(text(),'pass')]");
			 * fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			 * fc.utobj().clickElement(driver, ".\\*[contains(text(),'Print')]"); boolean
			 * found2 = fc.utobj().assertPageSource(driver, "100.00%"); boolean found3 =
			 * fc.utobj().assertPageSource(driver, "Pass"); boolean found4 =
			 * fc.utobj().assertPageSource(driver, "1"); boolean found5 =
			 * fc.utobj().assertPageSource(driver, "Which date is today ??"); boolean found6
			 * = fc.utobj().assertPageSource(driver, quizname); boolean found7 =
			 * fc.utobj().assertPageSource(driver, date); if (!found1 && !found2 && !found3
			 * && !found4 && !found5 && !found6 && !found7) fc.utobj().
			 * throwsException("Please check some points not found on Result view page");
			 */
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void publishCourse(String categoryname, String coursename, WebDriver driver, Map<String, String> config,
			String testCaseId) throws Exception {
		try {
			fc.training().training_common().adminTraingCourseManagementPage(driver);
			AdminTrainingCourseManagementAddSectionrPage pobj = new AdminTrainingCourseManagementAddSectionrPage(
					driver);
			
			/*fc.utobj().clickElement(driver, pobj.GeneralCategory);
			fc.utobj().sendKeys(driver, pobj.input, categoryname);
			//fc.utobj().clickElement(driver, ".//*[@id='mCSB_1_container']//a[contains(text(),'" + categoryname + "')]");
			fc.utobj().clickLink(driver, categoryname);*/
			
			fc.utobj().printTestStep("Search Category");
			new AdminTrainingCoursesManagementPageTest().searchCategory(driver, new AdminTrainingCoureseManagementPage(driver), categoryname);

			fc.utobj().clickElement(driver, ".//div[@data-role='ico_Filter']");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='searchCourse']"), coursename);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='search']"));

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='detailActionMenu']/div"));

			fc.utobj().clickElement(driver, pobj.Publish);

			fc.utobj().clickElement(driver, pobj.Confirm);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}

	@Test(groups = { "training", "trainingsmoke", "demotest" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Create Plan with Lock Functionality", testCaseId = "TC_Create_Plan_With_Lock_Functionality")
	private void CreatePlanWithLockFunctionality() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("training", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			driver = fc.loginpage().login(driver);
			AdminTrainingPlanAndCertificatePage pobj = new AdminTrainingPlanAndCertificatePage(driver);
			fc.utobj().printTestStep("Navigating to Admin > Training > Question Library Page");
			fc.training().training_common().adminTraingPlanAndCertificatesPage(driver);
			fc.utobj().clickElement(driver, pobj.Create);
			fc.utobj().clickElement(driver, pobj.CreatePlan);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			String planName = dataSet.get("PlanName") + fc.utobj().generateRandomNumber();
			fc.utobj().sendKeys(driver, pobj.planName, planName);
			fc.utobj().sendKeys(driver, pobj.Description, dataSet.get("Description"));
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='changeForm']//label[@for=\"LockPlan\"]"));
			fc.utobj().clickElement(driver, pobj.savePlan);
			fc.training().training_common().adminTraingPlanAndCertificatesPage(driver);
			fc.utobj().clickElement(driver, pobj.FilterBtn);
			fc.utobj().sendKeys(driver, pobj.planName, planName);
			fc.utobj().clickElement(driver, pobj.applyFilters);
			fc.utobj().clickPartialLinkText(driver, planName);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "training", "trainingsmoke"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Create Plan with Lock Functionality and Role based Access", testCaseId = "TC_Create_Plan_With_Role_Based_Access")
	private void CreatePlanWithRoleBasedAccess() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("training", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			driver = fc.loginpage().login(driver);
			String CorpRole = "Corp" + fc.utobj().generateRandomNumber();
			String RegRole = "Reg" + fc.utobj().generateRandomNumber();
			String FranRole = "Fran" + fc.utobj().generateRandomNumber();
			String DivRole = "Div" + fc.utobj().generateRandomNumber();
			
			AdminUsersRolesAddNewRolePageTest RolePage = new AdminUsersRolesAddNewRolePageTest();
			RolePage.addCorporateRoles(driver, CorpRole);
			RolePage.addRegionalRoleWithOnlyTrainingModule(driver, config, RegRole);
			RolePage.addFranchiselRoleWithOnlyTrainingModule(driver, config, FranRole);
			RolePage.addDivisionalRoleWithOnlyTrainingModule(driver, config, DivRole);
			
			AdminTrainingPlanAndCertificatePage pobj = new AdminTrainingPlanAndCertificatePage(driver);
			fc.utobj().printTestStep("Navigating to Admin > Training > Question Library Page");
			fc.training().training_common().adminTraingPlanAndCertificatesPage(driver);
			fc.utobj().clickElement(driver, pobj.Create);
			fc.utobj().clickElement(driver, pobj.CreatePlan);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			String planName = dataSet.get("PlanName") + fc.utobj().generateRandomNumber();
			fc.utobj().sendKeys(driver, pobj.planName, planName);
			fc.utobj().sendKeys(driver, pobj.Description, dataSet.get("Description"));
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='changeForm']//label[@for=\"LockPlan\"]"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					(".//*[@id='SendAllRecipients']/label[@data-target=\".configure-roles-contaione\"]")));
			
			fc.utobj().printTestStep("Select Corporate Role");
			fc.training().training_common().selectRole(driver, CorpRole, "Corporate");
			
			fc.utobj().printTestStep("Select Regional Role");
			fc.training().training_common().selectRole(driver, RegRole, "Regional");
			
			fc.utobj().printTestStep("Select Franchise Role");
			fc.training().training_common().selectRole(driver, FranRole, "Franchise");
			
			fc.utobj().printTestStep("Select Divisional Role");
			fc.training().training_common().selectRole(driver, DivRole, "Divisional");
			
			fc.utobj().clickElement(driver, pobj.savePlan);
			fc.training().training_common().adminTraingPlanAndCertificatesPage(driver);
			fc.utobj().clickElement(driver, pobj.FilterBtn);
			fc.utobj().sendKeys(driver, pobj.planName, planName);
			fc.utobj().clickElement(driver, pobj.applyFilters);
			fc.utobj().clickElement(driver, pobj.threedots);
			fc.utobj().clickElement(driver, ".//a[.='"+planName+"']/ancestor::div/following-sibling::div//li/a[.='Modify']");
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			planName = dataSet.get("PlanName") + fc.utobj().generateRandomNumber();
			fc.utobj().sendKeys(driver, pobj.planName, planName);
			fc.utobj().clickElement(driver, pobj.savePlan);
			fc.training().training_common().adminTraingPlanAndCertificatesPage(driver);
			fc.utobj().clickElement(driver, pobj.FilterBtn);
			fc.utobj().sendKeys(driver, pobj.planName, planName);
			fc.utobj().clickElement(driver, pobj.applyFilters);
			fc.utobj().clickElement(driver, pobj.threedots);
			fc.utobj().clickElement(driver, ".//a[.='"+planName+"']/ancestor::div/following-sibling::div//li/a[.='Modify']");
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			planName = dataSet.get("PlanName") + fc.utobj().generateRandomNumber();
			fc.utobj().sendKeys(driver, pobj.planName, planName);
			fc.utobj().clickElement(driver, pobj.savePlan);			
			fc.utobj().clickPartialLinkText(driver, planName);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "training", "trainingsmoke", "Validation" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Create Certificate", testCaseId = "TC_Create_Certificate")
	private void CreateCertificate() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("training", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			driver = fc.loginpage().login(driver);
			AdminTrainingPlanAndCertificatePage pobj = new AdminTrainingPlanAndCertificatePage(driver);
			fc.utobj().printTestStep("Navigating to Admin > Training > Question Library Page");
			fc.training().training_common().adminTraingPlanAndCertificatesPage(driver);
			fc.utobj().clickElement(driver, pobj.Create);
			fc.utobj().clickElement(driver, pobj.CreateCertificate);
			String CertificateName = dataSet.get("CertificateName") + fc.utobj().generateRandomNumber();
			fc.utobj().clickElement(driver, "//label[@for='certificateCorporateApprovalcheckbox']");
			fc.utobj().clickElement(driver, "//label[@for='certificateNeedRenewalcheckbox']");
			String CertificateHeading = dataSet.get("CertificateHeading") + fc.utobj().generateRandomNumber();
			String CertificateSubHeading = dataSet.get("CertificateSubHeading") + fc.utobj().generateRandomNumber();
			String CertificateMeassage = dataSet.get("CertificateMeassage") + fc.utobj().generateRandomNumber();
			fc.utobj().sendKeys(driver, pobj.certificateName, CertificateName);
			fc.utobj().sendKeys(driver, pobj.Description, dataSet.get("Description"));
			fc.utobj().printTestStep("opening a template");
			Actions action = new Actions(driver);
			action.moveToElement(pobj.Template1).build().perform();
			fc.utobj().clickElement(driver, pobj.certificateTemplate);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			String logo = fc.utobj().getFilePathFromTestData("logo");
			fc.utobj().sendKeys(driver, pobj.UploadcertificateLogo, logo);
			fc.utobj().sendKeys(driver, pobj.certificateHeading, CertificateHeading);
			fc.utobj().sendKeys(driver, pobj.certificateSubHeading, CertificateSubHeading);
			fc.utobj().sendKeys(driver, pobj.certificateMessage, CertificateMeassage);
			String Signature = fc.utobj().getFilePathFromTestData("sign");
			fc.utobj().sendKeys(driver, pobj.UploadcertificateSignature, Signature);
			fc.utobj().clickElement(driver, pobj.saveCertificate);
			Thread.sleep(5000);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, pobj.save);
			fc.training().training_common().adminTraingPlanAndCertificatesPage(driver);
			fc.utobj().clickElement(driver, pobj.CertificateTab);
			fc.utobj().clickElement(driver, pobj.FilterBtn);
			fc.utobj().sendKeys(driver, pobj.certificateName, CertificateName);
			fc.utobj().clickElement(driver, pobj.applyFilters);
			fc.utobj().clickPartialLinkText(driver, CertificateName);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "training", "trainingsmoke" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Create Certificate", testCaseId = "TC_Delete_Certificates_In_Batch")
	private void DeleteCertificateInBatch() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("training", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			driver = fc.loginpage().login(driver);
			AdminTrainingPlanAndCertificatePage pobj = new AdminTrainingPlanAndCertificatePage(driver);
			fc.utobj().printTestStep("Navigating to Admin > Training > Question Library Page");
			fc.training().training_common().adminTraingPlanAndCertificatesPage(driver);
			fc.utobj().clickElement(driver, pobj.Create);
			fc.utobj().clickElement(driver, pobj.CreateCertificate);
			String CertificateName = dataSet.get("CertificateName") + fc.utobj().generateRandomNumber();
			String CertificateHeading = dataSet.get("CertificateHeading") + fc.utobj().generateRandomNumber();
			String CertificateSubHeading = dataSet.get("CertificateSubHeading") + fc.utobj().generateRandomNumber();
			String CertificateMeassage = dataSet.get("CertificateMeassage") + fc.utobj().generateRandomNumber();
			fc.utobj().sendKeys(driver, pobj.certificateName, CertificateName);
			fc.utobj().sendKeys(driver, pobj.Description, dataSet.get("Description"));
			Actions action = new Actions(driver);
			action.moveToElement(pobj.Template1).build().perform();
			fc.utobj().clickElement(driver, pobj.certificateTemplate);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			String logo = fc.utobj().getFilePathFromTestData("logo");
			fc.utobj().sendKeys(driver, pobj.UploadcertificateLogo, logo);
			fc.utobj().sendKeys(driver, pobj.certificateHeading, CertificateHeading);
			fc.utobj().sendKeys(driver, pobj.certificateSubHeading, CertificateSubHeading);
			fc.utobj().sendKeys(driver, pobj.certificateMessage, CertificateMeassage);
			String Signature = fc.utobj().getFilePathFromTestData("sign");
			fc.utobj().sendKeys(driver, pobj.UploadcertificateSignature, Signature);
			fc.utobj().clickElement(driver, pobj.saveCertificate);
			Thread.sleep(5000);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, pobj.save);
			fc.training().training_common().adminTraingPlanAndCertificatesPage(driver);
			fc.utobj().clickElement(driver, pobj.CertificateTab);
			fc.utobj().clickElement(driver, pobj.FilterBtn);
			fc.utobj().sendKeys(driver, pobj.certificateName, CertificateName);
			try {
				fc.utobj().clickElement(driver, ".//*[@name=\"date-box1\"]");
				fc.utobj().clickElement(driver, "//span[contains(@class,'single-date')][contains(text(),'All')]");
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			fc.utobj().clickElement(driver, pobj.applyFilters);
			fc.utobj().clickElement(driver, pobj.deleteall);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//div[@data-role='ico_Filter']/following-sibling::div"));
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//div[@data-role='ico_Filter']/following-sibling::div/ul"));
			fc.utobj().acceptAlertBox(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "training", "trainingsmoke" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Create Certificate", testCaseId = "TC_Manage_Certificates")
	private void ManageCertificatewithApproval() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("training", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigating to Admin > Training > Course Management Page");
			AdminTrainingCourseManagementAddSectionrPage pobj = new AdminTrainingCourseManagementAddSectionrPage(
					driver);
			AdminTrainingCoursesManagementPageTest courseManagement = new AdminTrainingCoursesManagementPageTest();
			fc.utobj().printTestStep("Add Category");
			String categoryname = courseManagement.addCategory(driver, dataSet);
			fc.utobj().printTestStep("Add Course");
			String coursetitle = courseManagement.addCourse(driver, dataSet, categoryname, testCaseId);
			fc.utobj().printTestStep("Create Lesson");
			fc.utobj().clickElement(driver, pobj.addLesson);

			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			String sectionTitle = fc.utobj().generateTestData(dataSet.get("sectionTitle"));
			fc.utobj().sendKeys(driver, pobj.sectionTitle, sectionTitle);
			String summary = fc.utobj().generateTestData(dataSet.get("summary"));
			fc.utobj().sendKeys(driver, pobj.summary, summary);
			fc.utobj().clickElement(driver, pobj.addLesson);

			driver.switchTo().defaultContent();

			fc.utobj().clickElement(driver, pobj.embedVideo);

			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			try {
				fc.utobj().getElementByXpath(driver, ".//*[@id='subSectionContent']")
						.sendKeys(dataSet.get("embedCode"));
				fc.utobj().clickElement(driver, ".//*[@id='saveButton']");
			} catch (Exception ex) {
			}

			String str = "ravi";
			fc.utobj().printTestStep("Verify Add Emebed Type Section");

			fc.utobj().clickElement(driver, ".//div[@data-role='ico_Filter']");

			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='searchLesson']"), sectionTitle);

			fc.utobj().clickElement(driver, ".//*[@id='search']");

			AdminTrainingCourseManagementAddSectionPageTest objtag = new AdminTrainingCourseManagementAddSectionPageTest();
			objtag.addQuizAfterCreatingLesson(driver, sectionTitle, dataSet, config, testCaseId);

			boolean isTextPresent = fc.utobj().assertPageSource(driver, sectionTitle);
			if (isTextPresent == false) {

				fc.utobj().throwsException("Was not able to add Section!!!");
			}
			publishCourse(categoryname, coursetitle, driver, config, testCaseId);
			driver = fc.loginpage().login(driver);
			AdminTrainingPlanAndCertificatePage pobj1 = new AdminTrainingPlanAndCertificatePage(driver);
			fc.utobj().printTestStep("Navigating to Admin > Training > Question Library Page");
			fc.training().training_common().adminTraingPlanAndCertificatesPage(driver);
			fc.utobj().clickElement(driver, pobj1.Create);
			fc.utobj().clickElement(driver, pobj1.CreateCertificate);
			String CertificateName = dataSet.get("CertificateName") + fc.utobj().generateRandomNumber();
			String CertificateHeading = dataSet.get("CertificateHeading") + fc.utobj().generateRandomNumber();
			String CertificateSubHeading = dataSet.get("CertificateSubHeading") + fc.utobj().generateRandomNumber();
			String CertificateMeassage = dataSet.get("CertificateMeassage") + fc.utobj().generateRandomNumber();
			fc.utobj().sendKeys(driver, pobj1.certificateName, CertificateName);
			fc.utobj().sendKeys(driver, pobj1.Description, dataSet.get("Description"));
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='SendAllRecipients']/label"));
			Actions action = new Actions(driver);
			action.moveToElement(pobj.Template1).build().perform();
			fc.utobj().clickElement(driver, pobj1.certificateTemplate);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			String logo = fc.utobj().getFilePathFromTestData("logo");
			fc.utobj().sendKeys(driver, pobj1.UploadcertificateLogo, logo);
			fc.utobj().sendKeys(driver, pobj1.certificateHeading, CertificateHeading);
			fc.utobj().sendKeys(driver, pobj1.certificateSubHeading, CertificateSubHeading);
			fc.utobj().sendKeys(driver, pobj1.certificateMessage, CertificateMeassage);
			String Signature = fc.utobj().getFilePathFromTestData("sign");
			fc.utobj().sendKeys(driver, pobj1.UploadcertificateSignature, Signature);
			fc.utobj().clickElement(driver, pobj1.saveCertificate);
			// Thread.sleep(5000);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, pobj1.save);
			fc.training().training_common().adminTraingPlanAndCertificatesPage(driver);
			fc.utobj().clickElement(driver, pobj1.CertificateTab);
			fc.utobj().clickElement(driver, pobj1.FilterBtn);
			fc.utobj().sendKeys(driver, pobj1.certificateName, CertificateName);
			fc.utobj().clickElement(driver, pobj1.applyFilters);
			fc.utobj().clickPartialLinkText(driver, CertificateName);
			driver = fc.loginpage().login(driver);
			AdminTrainingPlanAndCertificatePage pobj2 = new AdminTrainingPlanAndCertificatePage(driver);
			fc.utobj().printTestStep("Navigating to Admin > Training > Question Library Page");
			fc.training().training_common().adminTraingPlanAndCertificatesPage(driver);
			fc.utobj().clickElement(driver, pobj2.Create);
			fc.utobj().clickElement(driver, pobj2.CreatePlan);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			String planName = dataSet.get("PlanName") + fc.utobj().generateRandomNumber();
			fc.utobj().sendKeys(driver, pobj2.planName, planName);
			fc.utobj().sendKeys(driver, pobj2.Description, dataSet.get("Description"));
			fc.utobj().clickElement(driver, pobj2.savePlan);
			fc.utobj().clickElement(driver, "//button[@id='addCourses']");
			// fc.training().training_common().adminTraingPlanAndCertificatesPage(driver);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			fc.utobj().clickElement(driver, ".//div[@data-role='ico_Filter']");
			fc.utobj().sendKeys(driver, pobj2.courseName1, coursetitle);
			fc.utobj().clickElement(driver, pobj2.applyFilters);
			fc.utobj().clickElement(driver, ".//*//label[@for='check']");
			fc.utobj().clickElement(driver, ".//*[@id='addButton']");

			fc.utobj().clickElement(driver, "//button[@id='addCertificates']");
			// fc.training().training_common().adminTraingPlanAndCertificatesPage(driver);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			fc.utobj().clickElement(driver, ".//div[@data-role='ico_Filter']");
			fc.utobj().sendKeys(driver, pobj2.certificateName, CertificateName);
			fc.utobj().clickElement(driver, pobj2.applyFilters);
			fc.utobj().clickElement(driver, ".//*//label[@for='check']");
			fc.utobj().clickElement(driver, ".//*[@id='addButton']");
			fc.utobj().clickElement(driver, "//button[@id='saveButton']");
			fc.utobj().clickElement(driver, pobj2.FilterBtn);
			fc.utobj().sendKeys(driver, pobj2.planName, planName);
			fc.utobj().clickElement(driver, pobj2.applyFilters);
			fc.utobj().clickPartialLinkText(driver, planName);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "training", "trainingsmoke" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Create Certificate", testCaseId = "TC_Manage_Certificates_01")
	private void ManageCertificatewithoutApproval() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("training", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigating to Admin > Training > Course Management Page");
			AdminTrainingCourseManagementAddSectionrPage pobj = new AdminTrainingCourseManagementAddSectionrPage(
					driver);
			AdminTrainingCoursesManagementPageTest courseManagement = new AdminTrainingCoursesManagementPageTest();
			fc.utobj().printTestStep("Add Category");
			String categoryname = courseManagement.addCategory(driver, dataSet);
			fc.utobj().printTestStep("Add Course");
			courseManagement.addCourse(driver, dataSet, categoryname, testCaseId);

			fc.utobj().printTestStep("Create Lesson");
			fc.utobj().clickElement(driver, pobj.addLesson);

			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			String sectionTitle = fc.utobj().generateTestData(dataSet.get("sectionTitle"));
			fc.utobj().sendKeys(driver, pobj.sectionTitle, sectionTitle);
			String summary = fc.utobj().generateTestData(dataSet.get("summary"));
			fc.utobj().sendKeys(driver, pobj.summary, summary);
			fc.utobj().clickElement(driver, pobj.addLesson);

			driver.switchTo().defaultContent();

			fc.utobj().clickElement(driver, pobj.embedVideo);

			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			// fc.utobj().clickElement(driver,
			// fc.utobj().getElementByXpath(driver,".//*[@id='editor']//iframe[@id='ta_ifr']")));
			try {
				fc.utobj().getElementByXpath(driver, ".//*[@id='subSectionContent']")
						.sendKeys(dataSet.get("embedCode"));
				fc.utobj().clickElement(driver, ".//*[@id='saveButton']");
			} catch (Exception ex) {
			}

			fc.utobj().printTestStep("Verify Add Emebed Type Section");

			fc.utobj().clickElement(driver, ".//div[@data-role='ico_Filter']");

			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='searchLesson']"), sectionTitle);

			fc.utobj().clickElement(driver, ".//*[@id='search']");

			AdminTrainingCourseManagementAddSectionPageTest objtag = new AdminTrainingCourseManagementAddSectionPageTest();
			objtag.addQuizAfterCreatingLesson(driver, sectionTitle, dataSet, config, testCaseId);

			boolean isTextPresent = fc.utobj().assertPageSource(driver, sectionTitle);
			if (isTextPresent == false) {

				fc.utobj().throwsException("Was not able to add Section!!!");
			}
			driver = fc.loginpage().login(driver);
			AdminTrainingPlanAndCertificatePage pobj1 = new AdminTrainingPlanAndCertificatePage(driver);
			fc.utobj().printTestStep("Navigating to Admin > Training > Question Library Page");
			fc.training().training_common().adminTraingPlanAndCertificatesPage(driver);

			fc.utobj().clickElement(driver, pobj1.Create);

			fc.utobj().clickElement(driver, pobj1.CreateCertificate);
			String CertificateName = dataSet.get("CertificateName") + fc.utobj().generateRandomNumber();
			String CertificateHeading = dataSet.get("CertificateHeading") + fc.utobj().generateRandomNumber();
			String CertificateSubHeading = dataSet.get("CertificateSubHeading") + fc.utobj().generateRandomNumber();
			String CertificateMeassage = dataSet.get("CertificateMeassage") + fc.utobj().generateRandomNumber();

			fc.utobj().sendKeys(driver, pobj1.certificateName, CertificateName);

			fc.utobj().sendKeys(driver, pobj1.Description, dataSet.get("Description"));

			Actions action = new Actions(driver);
			action.moveToElement(pobj.Template1).build().perform();

			fc.utobj().clickElement(driver, pobj1.certificateTemplate);

			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			String logo = fc.utobj().getFilePathFromTestData("logo.png");
			fc.utobj().sendKeys(driver, pobj1.UploadcertificateLogo, logo);

			fc.utobj().sendKeys(driver, pobj1.certificateHeading, CertificateHeading);

			fc.utobj().sendKeys(driver, pobj1.certificateSubHeading, CertificateSubHeading);

			fc.utobj().sendKeys(driver, pobj1.certificateMessage, CertificateMeassage);

			String Signature = fc.utobj().getFilePathFromTestData("sign.jpg");
			fc.utobj().sendKeys(driver, pobj1.UploadcertificateSignature, Signature);

			fc.utobj().clickElement(driver, pobj1.saveCertificate);
			Thread.sleep(5000);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, pobj1.save);

			fc.training().training_common().adminTraingPlanAndCertificatesPage(driver);

			fc.utobj().clickElement(driver, pobj1.CertificateTab);

			fc.utobj().clickElement(driver, pobj1.FilterBtn);

			fc.utobj().sendKeys(driver, pobj1.certificateName, CertificateName);

			fc.utobj().clickElement(driver, pobj1.applyFilters);

			fc.utobj().clickPartialLinkText(driver, CertificateName);

			driver = fc.loginpage().login(driver);

			AdminTrainingPlanAndCertificatePage pobj2 = new AdminTrainingPlanAndCertificatePage(driver);
			fc.utobj().printTestStep("Navigating to Admin > Training > Question Library Page");

			fc.training().training_common().adminTraingPlanAndCertificatesPage(driver);

			fc.utobj().clickElement(driver, pobj2.Create);

			fc.utobj().clickElement(driver, pobj2.CreatePlan);

			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			String planName = dataSet.get("PlanName") + fc.utobj().generateRandomNumber();

			fc.utobj().sendKeys(driver, pobj2.planName, planName);

			fc.utobj().sendKeys(driver, pobj2.Description, dataSet.get("Description"));

			fc.utobj().clickElement(driver, pobj2.savePlan);

			fc.training().training_common().adminTraingPlanAndCertificatesPage(driver);

			fc.utobj().clickElement(driver, pobj2.FilterBtn);

			fc.utobj().sendKeys(driver, pobj2.planName, planName);

			fc.utobj().clickElement(driver, pobj2.applyFilters);

			fc.utobj().clickPartialLinkText(driver, planName);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "training", "trainingsmoke" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Create Certificate", testCaseId = "TC_Verify_Events")
	private void AddEvent() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("training", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			driver = fc.loginpage().login(driver);
			AdminTrainingPlanAndCertificatePage pobj = new AdminTrainingPlanAndCertificatePage(driver);
			fc.utobj().printTestStep("Navigating to Training > Event");
			fc.training().training_common().trainingEvents(driver);
			fc.utobj().clickElement(driver, pobj.addevent);
			// Thread.sleep(1000);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			String eventName = dataSet.get("eventname") + fc.utobj().generateRandomNumber();
			fc.utobj().sendKeys(driver, pobj.eventname, eventName);
			fc.utobj().sendKeys(driver, pobj.eventdiscription,
					dataSet.get("eventdiscription") + fc.utobj().generateRandomNumber());
			fc.utobj().sendKeys(driver, pobj.eventlocation,
					dataSet.get("eventlocation") + fc.utobj().generateRandomNumber());
			fc.utobj().clickElement(driver, pobj.eventdateScheduled);

			DateFormat dateFormat2 = new SimpleDateFormat("dd");
			Date date2 = new Date();

			int day = Integer.parseInt(dateFormat2.format(date2));
			day++;
			String today = String.valueOf(day);

			// find the calendar
			WebElement dateWidget = fc.utobj().getElementByID(driver, "fc-datepicker-div");
			List<WebElement> columns = dateWidget.findElements(By.tagName("a"));

			// comparing the text of cell with today's date and clicking it.
			for (WebElement cell : columns) {
				if (cell.getText().equals(today)) {
					cell.click();
					break;
				}
			}
			fc.utobj().clickElement(driver, pobj.createevent);
			fc.utobj().switchFrameToDefault(driver);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			fc.utobj().clickElement(driver, pobj.Close);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'" + eventName
					+ "')]/parent::td/following-sibling::*//div[contains(@data-role,'ico_ThreeDots')]"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'" + eventName
					+ "')]/parent::td/following-sibling::*//a[contains(text(),'Modify')]"));
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			eventName = dataSet.get("eventname") + fc.utobj().generateRandomNumber();
			fc.utobj().sendKeys(driver, pobj.eventname, eventName);
			fc.utobj().sendKeys(driver, pobj.eventdiscription,
					dataSet.get("eventdiscription") + fc.utobj().generateRandomNumber());
			fc.utobj().sendKeys(driver, pobj.eventlocation,
					dataSet.get("eventlocation") + fc.utobj().generateRandomNumber());
			fc.utobj().clickElement(driver, pobj.eventdateScheduled);

			dateFormat2 = new SimpleDateFormat("dd");
			date2 = new Date();

			day = Integer.parseInt(dateFormat2.format(date2));
			day = day + 1;
			today = String.valueOf(day);

			// find the calendar
			dateWidget = fc.utobj().getElementByID(driver, "fc-datepicker-div");
			List<WebElement> cols = dateWidget.findElements(By.tagName("a"));

			// comparing the text of cell with today's date and clicking it.
			for (WebElement cell : cols) {
				if (cell.getText().equals(today)) {
					cell.click();
					break;
				}
			}
			fc.utobj().clickElement(driver, pobj.modify);
			fc.utobj().clickElement(driver, pobj.Close);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'" + eventName
					+ "')]/parent::td/following-sibling::*//div[contains(@data-role,'ico_ThreeDots')]"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'" + eventName
					+ "')]/parent::td/following-sibling::*//a[contains(text(),'Delete')]"));
			fc.utobj().acceptAlertBox(driver);
			boolean found = fc.utobj().assertPageSource(driver, eventName);
			if (found)
				fc.utobj().throwsException("Event not got deleted");

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
}
