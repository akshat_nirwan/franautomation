package com.builds.test.training;

import java.util.Map;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import com.builds.uimaps.training.AdminTrainingCourseManagementAddSectionrPage;
import com.builds.uimaps.training.AdminTrainingQuestionLibraryPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminTrainingQuestionLibraryPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "training", "trainingsmoke","QuestionLibrary" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Add Group At Admin Training Question Library Page", testCaseId = "TC_29_Question_Library_Add_Group")
	private void addGroup() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("training", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			driver = fc.loginpage().login(driver);
			AdminTrainingQuestionLibraryPage pobj = new AdminTrainingQuestionLibraryPage(driver);
			fc.utobj().printTestStep("Navigating to Admin > Training > Question Library Page");
			fc.training().training_common().adminTraingQuestionLibraryPage(driver);

			fc.utobj().clickElement(driver, pobj.manageGroup);
			fc.utobj().printTestStep("Add Group");
			fc.utobj().clickElement(driver, pobj.addGroup);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			fc.utobj().sendKeys(driver, pobj.groupName, groupName);
			fc.utobj().sendKeys(driver, pobj.groupDescription, dataSet.get("groupDescription"));
			fc.utobj().clickElement(driver, pobj.save);

			fc.utobj().switchFrameToDefault(driver);
			boolean check = fc.utobj().assertPageSource(driver, groupName);
			fc.utobj().clickElement(driver, pobj.viewperpage);
			fc.utobj().clickElement(driver, ".//a[contains(text(),'500')]");

			// WebElement records500 = fc.utobj().getElementByXpath(driver,
			// ".//a[contains(text(),'500')]");
			// records500.click();

			if (!check) {
				fc.utobj().throwsException("Added group is not being displayed on pop list ");
			}
			fc.utobj().printTestStep("Verify Add Group");
			fc.utobj().clickElement(driver, ".//*[contains(text(),'" + groupName + "')]/parent::td");
			fc.utobj().clickElement(driver, ".//*[contains(text(),'" + groupName + "')]/parent::tr/td[5]/div");
			fc.utobj().clickElement(driver, ".//*[contains(text(),'" + groupName + "')]/parent::tr/td[5]/div//li[3]/a");
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			AdminTrainingCourseManagementAddSectionrPage pobj1 = new AdminTrainingCourseManagementAddSectionrPage(
					driver);

			fc.utobj().clickElement(driver, " .//*[@id='responseType']/option[@value='text']");
			String quizquestion = "what is your name";
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='question']"), quizquestion);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='questionMarks']/option[@value=\"2\"]"));
			fc.utobj().clickElement(driver, ".//*[@id='siteMainDiv']/div/div/div[3]/button[2]");
			fc.utobj().clickElement(driver, ".//*[contains(text(),'" + groupName
					+ "')]/parent::td[1]/following-sibling::td[1]/a[contains(text(),'1')]");
			check = fc.utobj().assertPageSource(driver, quizquestion);
			if (!check) {
				fc.utobj().throwsException("Added question not found on page");
			}
			fc.utobj().clickElement(driver, ".//*[@data-role=\"ico_ThreeDots\"]");
			fc.utobj().clickElement(driver, ".//*//a[contains(text(),'Modify')]");
			quizquestion += " check";
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='question']"), quizquestion);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='questionMarks']/option[@value=\"2\"]"));
			fc.utobj().clickElement(driver, ".//*[@id='siteMainDiv']/div/div/div[3]/button[2]");

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addGroup(WebDriver driver, Map<String, String> dataSet) throws Exception {

		String testCaseId = "TC_addGroupTraining_01";
		String groupName = null;

		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminTrainingQuestionLibraryPage pobj = new AdminTrainingQuestionLibraryPage(driver);
				fc.training().training_common().adminTraingQuestionLibraryPage(driver);
				fc.utobj().clickElement(driver, pobj.manageGroup);
				fc.utobj().clickElement(driver, pobj.addGroup);
				fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
				groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
				fc.utobj().sendKeys(driver, pobj.groupName, groupName);
				fc.utobj().clickElement(driver, pobj.save);
				boolean check = fc.utobj().assertPageSource(driver, "This field is required.");
				if (!check) {
					fc.utobj().throwsException("without mandatory field group got added");
				}
				fc.utobj().sendKeys(driver, pobj.groupDescription, dataSet.get("groupDescription"));
				fc.utobj().clickElement(driver, pobj.save);
				fc.utobj().switchFrameToDefault(driver);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Was not able to Add Group");

		}
		return groupName;
	}

	@Test(groups = { "training","QuestionLibrary" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Add and Modify Question In Library At Admin Training Question Library Page", testCaseId = "TC_32_Question_Library_Group_Add_Question")
	private void addQuestionInGroup() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("training", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigating to Admin > Training > Question Library Page");
			AdminTrainingQuestionLibraryPage pobj = new AdminTrainingQuestionLibraryPage(driver);
			fc.utobj().printTestStep("Add Group");
			String groupName = addGroup(driver, dataSet);
//			fc.utobj().showAll(driver);

			fc.utobj().printTestStep("Add Question");

			fc.utobj().clickElement(driver, pobj.viewperpage);

			fc.utobj().clickElement(driver, pobj.recordsperpage);

			try {
				fc.utobj().clickElement(driver, ".//*[contains (text () ,'" + groupName
						+ "')]/following-sibling::td//div[@data-role='ico_ThreeDots']");
			} catch (Exception ex) {
				JavascriptExecutor jse = (JavascriptExecutor) driver;
				jse.executeScript("window.scrollBy(0,1050)", "");
				fc.utobj().clickElement(driver, ".//*[contains (text () ,'" + groupName
						+ "')]/following-sibling::td//div[@data-role='ico_ThreeDots']");
			}
			fc.utobj().clickElement(driver,
					(".//td[contains (text () ,'" + groupName + "')]/following-sibling::td//ul/li[3]"));
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			fc.utobj().selectDropDown(driver, pobj.groupNameId, groupName);
			String questionText = fc.utobj().generateTestData(dataSet.get("questionText"));
			fc.utobj().sendKeys(driver, pobj.questionText, questionText);
			fc.utobj().selectDropDown(driver, pobj.responseType, dataSet.get("responseType"));
			WebElement mySelectElement = fc.utobj().getElementByID(driver, "questionMarks");
			Select dropdown = new Select(mySelectElement);
			dropdown.selectByVisibleText("0");
			fc.utobj().clickElement(driver, pobj.resoponseMandatory);
			fc.utobj().selectDropDown(driver, pobj.questionMark, dataSet.get("marks"));
			fc.utobj().clickElement(driver, pobj.saveQuestion);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, pobj.questionLibraryAfterQuestionAdd);
			fc.utobj().clickElement(driver, ".//td[contains(text(),'" + questionText + "')]/following-sibling::td[4]");
			fc.utobj().clickElement(driver,
					(".//td[contains(text(),'" + questionText + "')]/following-sibling::td[4]//li[1]"));

			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			fc.utobj().selectDropDown(driver, pobj.groupNameId, groupName);
			String oldquestiontext=questionText;
			questionText = fc.utobj().generateTestData(dataSet.get("questionText"));
			fc.utobj().sendKeys(driver, pobj.questionText, questionText);
			fc.utobj().clickElement(driver, pobj.saveQuestion);
			fc.utobj().switchFrameToDefault(driver);
			boolean isTextPresent = fc.utobj().assertPageSource(driver, oldquestiontext);
			if (isTextPresent == true) {
				fc.utobj().throwsException("Was not able to add question in group!!!");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "training", "FailedTestCases" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Add Question In Library At Admin Training Question Library Page", testCaseId = "TC_75_Question_Library_Group_Add_Question_All_Type")
	private void addAllTypeQuestionInGroup() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("training", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigating to Admin > Training > Question Library Page");
			AdminTrainingQuestionLibraryPage pobj = new AdminTrainingQuestionLibraryPage(driver);
			fc.utobj().printTestStep("Add Group");
			String groupName = addGroup(driver, dataSet);

			fc.utobj().printTestStep("Add Question");
			fc.utobj().clickElement(driver, pobj.viewperpage);

			fc.utobj().clickElement(driver, ".//a[contains(text(),'500')]");

			try {
				fc.utobj().clickElement(driver, ".//*[contains (text () ,'" + groupName
						+ "')]/following-sibling::td//div[@data-role='ico_ThreeDots']");
			} catch (Exception ex) {
				JavascriptExecutor jse = (JavascriptExecutor) driver;
				jse.executeScript("window.scrollBy(0,1050)", "");
				fc.utobj().clickElement(driver, ".//*[contains (text () ,'" + groupName
						+ "')]/following-sibling::td//div[@data-role='ico_ThreeDots']");
			}
			fc.utobj().clickElement(driver,
					(".//*[contains (text () ,'" + groupName + "')]/following-sibling::td//ul/li[3]"));

			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			fc.utobj().selectDropDown(driver, pobj.groupNameId, groupName);
			String questionText = fc.utobj().generateTestData(dataSet.get("questionText"));
			fc.utobj().sendKeys(driver, pobj.questionText, questionText);
			fc.utobj().selectDropDown(driver, pobj.responseType, dataSet.get("responseType1"));
			WebElement mySelectElement = fc.utobj().getElementByID(driver, "questionMarks");
			Select dropdown = new Select(mySelectElement);
			dropdown.selectByVisibleText("0");
			fc.utobj().clickElement(driver, pobj.resoponseMandatory);
			fc.utobj().selectDropDown(driver, pobj.questionMark, dataSet.get("marks"));
			fc.utobj().clickElement(driver, pobj.saveandAddnextQuestion);

			fc.utobj().selectDropDown(driver, pobj.groupNameId, groupName);
			questionText = fc.utobj().generateTestData(dataSet.get("questionText2"));
			fc.utobj().sendKeys(driver, pobj.questionText, questionText);
			fc.utobj().selectDropDown(driver, pobj.responseType, dataSet.get("responseType2"));
			mySelectElement = fc.utobj().getElementByID(driver, "questionMarks");
			dropdown = new Select(mySelectElement);
			dropdown.selectByVisibleText("0");
			fc.utobj().clickElement(driver, pobj.resoponseMandatory);
			fc.utobj().selectDropDown(driver, pobj.questionMark, dataSet.get("marks"));
			fc.utobj().clickElement(driver, pobj.saveandAddnextQuestion);

			fc.utobj().selectDropDown(driver, pobj.groupNameId, groupName);
			questionText = fc.utobj().generateTestData(dataSet.get("questionText3"));
			fc.utobj().sendKeys(driver, pobj.questionText, questionText);
			fc.utobj().selectDropDown(driver, pobj.responseType, dataSet.get("responseType3"));
			mySelectElement = fc.utobj().getElementByID(driver, "questionMarks");
			dropdown = new Select(mySelectElement);
			dropdown.selectByVisibleText("0");
			fc.utobj().clickElement(driver, pobj.resoponseMandatory);
			fc.utobj().selectDropDown(driver, pobj.questionMark, dataSet.get("marks"));

			fc.utobj().sendKeys(driver, pobj.minRating, "3");
			fc.utobj().sendKeys(driver, pobj.maxRating, "9");
			fc.utobj().clickElement(driver, pobj.saveandAddnextQuestion);

			fc.utobj().selectDropDown(driver, pobj.groupNameId, groupName);
			questionText = fc.utobj().generateTestData(dataSet.get("questionText4"));
			fc.utobj().sendKeys(driver, pobj.questionText, questionText);
			Select dd = new Select(pobj.responseType);
			dd.selectByValue("radio");
			mySelectElement = fc.utobj().getElementByID(driver, "questionMarks");
			dropdown = new Select(mySelectElement);
			dropdown.selectByVisibleText("0");
			fc.utobj().clickElement(driver, pobj.resoponseMandatory);
			fc.utobj().selectDropDown(driver, pobj.questionMark, dataSet.get("marks"));
			int Options = Integer.parseInt(dataSet.get("NumOfRadioOptions"));
			int start = 1;
			while (Options > 0) {
				fc.utobj().sendKeys(driver,
						fc.utobj().getElementByXpath(driver,
								".//*[@id='answers_radio" + String.valueOf(start) + "']//*[@id='optionradio1']"),
						dataSet.get("Option" + String.valueOf(start)));
				start++;
				Options--;
			}

			fc.utobj().clickElement(driver, pobj.saveandAddnextQuestion);

			fc.utobj().selectDropDown(driver, pobj.groupNameId, groupName);
			questionText = fc.utobj().generateTestData(dataSet.get("questionText5"));
			fc.utobj().sendKeys(driver, pobj.questionText, questionText);
			dd = new Select(pobj.responseType);
			dd.selectByValue("checkbox");
			mySelectElement = fc.utobj().getElementByID(driver, "questionMarks");
			dropdown = new Select(mySelectElement);
			dropdown.selectByVisibleText("0");
			fc.utobj().clickElement(driver, pobj.resoponseMandatory);
			fc.utobj().selectDropDown(driver, pobj.questionMark, dataSet.get("marks"));
			Options = Integer.parseInt(dataSet.get("NumOfRadioOptions"));
			start = 1;
			while (Options > 0) {

				fc.utobj().sendKeys(driver,
						fc.utobj().getElementByXpath(driver,
								".//*[@id='answers_checkbox" + String.valueOf(start) + "']//*[@id='optioncheckbox1']"),
						dataSet.get("Option" + String.valueOf(start)));
				if (start == 1) {
					fc.utobj().clickElement(driver, ".//*[@id='answers_checkbox" + String.valueOf(start) + "']//label");
				}
				start++;
				Options--;
			}
			fc.utobj().clickElement(driver, pobj.saveQuestion);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, pobj.questionLibraryAfterQuestionAdd);
			fc.utobj().clickElement(driver, ".//td[contains(text(),'" + questionText + "')]/following-sibling::td[4]");
			fc.utobj().clickElement(driver,
					(".//td[contains(text(),'" + questionText + "')]/following-sibling::td[4]//li[1]"));
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			fc.utobj().selectDropDown(driver, pobj.groupNameId, groupName);
			questionText = fc.utobj().generateTestData(dataSet.get("questionText"));
			fc.utobj().sendKeys(driver, pobj.questionText, questionText);
			fc.utobj().clickElement(driver, pobj.saveQuestion);
			fc.utobj().switchFrameToDefault(driver);
			// fc.utobj().clickElement(driver,
			// pobj.questionLibraryAfterQuestionAdd);
			fc.utobj().clickElement(driver, ".//td[contains(text(),'" + questionText + "')]/following-sibling::td[4]");
			fc.utobj().clickElement(driver,
					(".//td[contains(text(),'" + questionText + "')]/following-sibling::td[4]//li[2]"));
			fc.utobj().acceptAlertBox(driver);
			boolean isTextPresent = fc.utobj().assertPageSource(driver, questionText);
			if (isTextPresent == true) {
				fc.utobj().throwsException("Was not able to add question in group!!!");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addQuestion(WebDriver driver, Map<String, String> dataSet, String groupName) throws Exception {

		String testCaseId = "TC_addQuestionTraining_01";
		String questionText = null;

		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminTrainingQuestionLibraryPage pobj = new AdminTrainingQuestionLibraryPage(driver);

				fc.utobj().clickElement(driver, pobj.questionLibraryButton);
				fc.utobj().clickElement(driver, pobj.addQuestion);

				fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
				fc.utobj().selectDropDown(driver, pobj.groupNameId, groupName);

				questionText = fc.utobj().generateTestData(dataSet.get("questionText"));

				fc.utobj().sendKeys(driver, pobj.questionText, questionText);

				fc.utobj().selectDropDown(driver, pobj.responseType, dataSet.get("responseType"));

				if (pobj.questionMarks.get(0).isSelected() == true) {
					// do nothing
				} else {
					fc.utobj().clickElement(driver, pobj.questionMarks.get(0));
				}

				fc.utobj().clickElement(driver, pobj.resoponseMandatory);
				fc.utobj().selectDropDown(driver, pobj.questionMark, dataSet.get("marks"));

				fc.utobj().clickElement(driver, pobj.saveQuestion);
				fc.utobj().switchFrameToDefault(driver);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to Add Question");

		}
		return questionText;

	}

	@Test(groups = { "training", "trainingsanity","QuestionLibrary" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Varify Deletion Of Question At Admin Training Question Library Page ", testCaseId = "TC_35_Question_Library_Delete_Question")
	private void deleteQuestion() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("training", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigating to Admin > Training > Question Library Page");
			fc.utobj().printTestStep("Add Group");
			String groupName = addGroup(driver, dataSet);
			fc.utobj().printTestStep("Add Question");
			String questionText = addQuestion(driver, dataSet, groupName);
			fc.utobj().printTestStep("Delete Question");
			fc.utobj().showAll(driver);

			try {
				fc.utobj().clickElement(driver, ".//*[contains (text () ,'" + groupName
						+ "')]/following-sibling::td//div[@data-role='ico_ThreeDots']");
			} catch (Exception ex) {
				JavascriptExecutor jse = (JavascriptExecutor) driver;
				jse.executeScript("window.scrollBy(0,1050)", "");
				fc.utobj().clickElement(driver, ".//*[contains (text () ,'" + groupName
						+ "')]/following-sibling::td//div[@data-role='ico_ThreeDots']");
			}
			fc.utobj().clickElement(driver, ".//td[contains (text ()," + "'" + groupName + "'"
					+ ")]/following-sibling::td//li/a[contains(text(),'Delete')]");
			fc.utobj().acceptAlertBox(driver);
			fc.utobj().showAll(driver);

			fc.utobj().printTestStep("Verify Deletion of Group");

			boolean isTextPresent = fc.utobj().assertPageSource(driver, questionText);
			if (isTextPresent == true) {
				fc.utobj().throwsException("Was not able to delete question!!!");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "training", "trainingsanity", "trainingsmoketest","QuestionLibrary" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Archive Question At Admin Training Question Library Page", testCaseId = "TC_36_Question_Library_Archive_Question")
	private void archiveQuestion() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("training", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigating to Admin > Training > Question Library Page");
			AdminTrainingQuestionLibraryPage pobj = new AdminTrainingQuestionLibraryPage(driver);
			fc.utobj().printTestStep("Add Group");
			String groupName = addGroup(driver, dataSet);
			fc.utobj().showAll(driver);

			fc.utobj().printTestStep("Add Question");

			fc.utobj().clickElement(driver, pobj.viewperpage);

			fc.utobj().clickElement(driver, pobj.recordsperpage);

			try {
				fc.utobj().clickElement(driver, ".//*[contains (text () ,'" + groupName
						+ "')]/following-sibling::td//div[@data-role='ico_ThreeDots']");
			} catch (Exception ex) {
				JavascriptExecutor jse = (JavascriptExecutor) driver;
				jse.executeScript("window.scrollBy(0,1050)", "");
				fc.utobj().clickElement(driver, ".//*[contains (text () ,'" + groupName
						+ "')]/following-sibling::td//div[@data-role='ico_ThreeDots']");
			}
			fc.utobj().clickElement(driver,
					(".//td[contains (text () ,'" + groupName + "')]/following-sibling::td//ul/li[3]"));

			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			fc.utobj().selectDropDown(driver, pobj.groupNameId, groupName);
			String questionText = fc.utobj().generateTestData(dataSet.get("questionText"));
			fc.utobj().sendKeys(driver, pobj.questionText, fc.utobj().translateString(questionText));// need
																										// traslate

			fc.utobj().selectDropDownByValue(driver, pobj.responseType, fc.utobj().translateString("text"));
			WebElement mySelectElement = fc.utobj().getElementByID(driver, "questionMarks");

			Select dropdown = new Select(mySelectElement);
			dropdown.selectByVisibleText("0");

			fc.utobj().clickElement(driver, pobj.resoponseMandatory);

			fc.utobj().selectDropDown(driver, pobj.questionMark, dataSet.get("marks"));

			fc.utobj().clickElement(driver, pobj.saveQuestion);

			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, pobj.questionLibraryAfterQuestionAdd);

			fc.utobj().clickElement(driver, ".//td[contains(text(),'" + questionText + "')]/following-sibling::td[4]");
			fc.utobj().clickElement(driver,
					(".//td[contains(text(),'" + questionText + "')]/following-sibling::td[4]//li[3]"));

			fc.utobj().acceptAlertBox(driver);

			fc.utobj().clickElement(driver, ".//td[contains(text(),'" + questionText + "')]/following-sibling::td[4]");
			fc.utobj().clickElement(driver,
					(".//td[contains(text(),'" + questionText + "')]/following-sibling::td[4]//li[3]"));

			fc.utobj().acceptAlertBox(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void archiveQuestion(WebDriver driver, Map<String, String> dataSet, String groupName) throws Exception {

		String testCaseId = "TC_archiveQuestion_01";

		if (fc.utobj().validate(testCaseId)) {
			try {
				fc.utobj().showAll(driver);

				String text = fc.utobj().getElementByXpath(driver, ".//td[contains (text () ," + "'" + groupName + "'"
						+ ")]/following-sibling::td/div[@id='menuBar']/layer").getAttribute("id");
				String alterText = text.replace("Actions_dynamicmenu", "");
				alterText = alterText.replace("Bar", "");
				System.out.println(alterText);

				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//td[contains (text ()," + "'"
						+ groupName + "'" + ")]/following-sibling::td/div[@id='menuBar']/layer/a/img"));

				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, "//div[@id='Actions_dynamicmenu"
						+ alterText + "Menu" + "']/span[contains(text () , 'Archive')]")); // Archive

				fc.utobj().acceptAlertBox(driver);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Was not able to Archive Question");

		}
	}

}
