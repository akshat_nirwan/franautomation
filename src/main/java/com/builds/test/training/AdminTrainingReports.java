package com.builds.test.training;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.uimaps.training.AdminTrainingCourseManagementAddSectionrPage;
import com.builds.uimaps.training.AdminTrainingQuestionLibraryPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminTrainingReports {
	FranconnectUtil fc = new FranconnectUtil();
	
	@Test(groups = { "training", "trainingsmoke","QuestionLibrary" })

	@TestCase(createdOn = "2018-06-14", updatedOn = "2018-06-14", testCaseDescription = "Verify reports on Training page", testCaseId = "TC_Verify_Training_Reports")
	private void addGroup() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("training", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			driver = fc.loginpage().login(driver);
			AdminTrainingQuestionLibraryPage pobj = new AdminTrainingQuestionLibraryPage(driver);
			fc.utobj().printTestStep("Navigating to Admin > Training > Question Library Page");
			fc.training().training_common().trainingReports(driver);
			fc.utobj().clickElement(driver, ".//*[@id='siteMainTable']/tbody/tr/td/table/tbody/tr/td/table[2]/tbody/tr/td/table/tbody/tr/td[1]/table[2]/tbody/tr[2]/td[2]/ul/li[2]/a");
			fc.utobj().assertPageSource(driver, "Summary by Courses ");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

}
