package com.builds.test.training;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import com.builds.test.admin.AdminUsersRolesAddNewRolePageTest;
import com.builds.test.admin.Roles;
import com.builds.uimaps.training.AdminTrainingCoureseManagementPage;
import com.builds.utilities.FranconnectUtil;

public class Category {
	FranconnectUtil fc = new FranconnectUtil();
	private String CategoryName;
	private String UploadCoverImage;
	private String CategoryDesc;
	private boolean RolebasedAccessCategory;
	private String CategoryCorporateRole = "Select All";
	private String CategoryRegionalRole = "Select All";
	private String CategoryFranchiseRole = "Select All";
	private String CategoryDivisionRole = "Select All";
	private List<Course> courseArray = new LinkedList<Course>();
	private Course CourseName;
	private AdminUsersRolesAddNewRolePageTest role = new AdminUsersRolesAddNewRolePageTest();

	public List<Course> getCourseArray() {
		return courseArray;
	}

	public void setCourseArray(Course course) {
		this.courseArray.add(course);
	}

	public void setCategoryCorporateRole(String categoryCorporateRole) {
		CategoryCorporateRole = categoryCorporateRole;
	}

	public void setCategoryRegionalRole(String categoryRegionalRole) {
		CategoryRegionalRole = categoryRegionalRole;
	}

	public void setCategoryFranchiseRole(String categoryFranchiseRole) {
		CategoryFranchiseRole = categoryFranchiseRole;
	}

	public void setCategoryDivisionRole(String categoryDivisionRole) {
		CategoryDivisionRole = categoryDivisionRole;
	}

	public String getCategoryName() {
		return CategoryName;
	}

	public void setCategoryName(String categoryName) {
		CategoryName = categoryName;
	}

	public String getCategoryDesc() {
		return CategoryDesc;
	}

	public void setCategoryDesc(String categoryDesc) {
		CategoryDesc = categoryDesc;
	}

	public boolean isRolebasedAccessCategory() {
		return RolebasedAccessCategory;
	}

	public void setRolebasedAccessCategory(boolean rolebasedAccessCategory) {
		RolebasedAccessCategory = rolebasedAccessCategory;
	}

	public String getCategoryCorporateRole() {
		return CategoryCorporateRole;
	}

	public void setCategoryCorporateRole(WebDriver driver, Map<String, String> config) throws Exception {
		CategoryCorporateRole = /*"Corp" + fc.utobj().generateRandomNumber();*/fc.utobj().generateTestData("Corpr");
		//role.addCorporateRoles(driver, CategoryCorporateRole);
		fc.utobj().printTestStep("Add Corporate Role");
		AdminUsersRolesAddNewRolePageTest roles_page=new AdminUsersRolesAddNewRolePageTest();
		Roles roles=new Roles();
		roles.setRoleType("Corporate");
		roles.setRoleName(CategoryCorporateRole);
		roles_page.addRoles(driver, roles);

	}

	public String getCategoryRegionalRole() {
		return CategoryRegionalRole;
	}

	public void setCategoryRegionalRole(WebDriver driver, Map<String, String> config) throws Exception {
		CategoryRegionalRole = /*"Reg" + fc.utobj().generateRandomNumber();*/fc.utobj().generateTestData("Reg");
		//role.addRegionalRoleWithOnlyTrainingModule(driver, config, CategoryRegionalRole);
		fc.utobj().printTestStep("Add Regional Role");
		AdminUsersRolesAddNewRolePageTest roles_page=new AdminUsersRolesAddNewRolePageTest();
		Roles roles=new Roles();
		roles.setRoleType("Regional");
		roles.setRoleName(CategoryRegionalRole);
		roles_page.addRoles(driver, roles);
	}

	public String getCategoryFranchiseRole() {
		return CategoryFranchiseRole;
	}

	public void setCategoryFranchiseRole(WebDriver driver, Map<String, String> config) throws Exception {
		CategoryFranchiseRole = /*"Fran" + fc.utobj().generateRandomNumber();*/fc.utobj().generateTestData("Fran");
		//role.addFranchiselRoleWithOnlyTrainingModule(driver, config, CategoryFranchiseRole);
		
		fc.utobj().printTestStep("Add Franchise Role");
		AdminUsersRolesAddNewRolePageTest roles_page=new AdminUsersRolesAddNewRolePageTest();
		Roles roles=new Roles();
		roles.setRoleType("Franchise");
		roles.setRoleName(CategoryFranchiseRole);
		roles_page.addRoles(driver, roles);
	}

	public String getCategoryDivisionRole() {
		return CategoryDivisionRole;
	}

	public void setCategoryDivisionRole(WebDriver driver, Map<String, String> config) throws Exception {
		CategoryDivisionRole =fc.utobj().generateTestData("Div");
		//role.addDivisionalRoleWithOnlyTrainingModule(driver, config, CategoryDivisionRole);
		fc.utobj().printTestStep("Add Divisional Role");
		AdminUsersRolesAddNewRolePageTest roles_page=new AdminUsersRolesAddNewRolePageTest();
		Roles roles=new Roles();
		roles.setRoleType("Division");
		roles.setRoleName(CategoryDivisionRole);
		roles_page.addRoles(driver, roles);
	}

	public Course getCourseName() {
		return CourseName;
	}

	public void setCourseName(Course courseName) {
		CourseName = courseName;
	}

	public Category() {
	}

	Course addCouse(WebDriver driver, Map<String, String> config, Course course) throws Exception {

		try {
			fc.training().training_common().adminTraingCourseManagementPage(driver);
			AdminTrainingCoureseManagementPage pobj = new AdminTrainingCoureseManagementPage(driver);
			fc.utobj().clickElement(driver, pobj.manageCategory);
			Thread.sleep(1000);
			fc.utobj().clickElement(driver, ".//div[@data-role='ico_Filter']");
			Thread.sleep(1000);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='categoryName']"),
					this.getCategoryName());
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='search']"));
			String xpath = ".//*[contains(text () , '" + this.getCategoryName()
					+ "')]/ancestor::div/following-sibling::div//div[@data-role='ico_ThreeDots']";
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, xpath));
			Thread.sleep(1000);
			fc.utobj().clickElement(driver, pobj.addCourse);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			Thread.sleep(1000);
			if (fc.utobj().getElementByXpath(driver, ".//*[@id='LockCourse']").getAttribute("checked") != null) {
				fc.utobj().throwsException("Default lock the course button is enabled");
			}
			boolean testimageformat = false;
			try {
				String fileName = fc.utobj().getFilePathFromTestData("taskFile.pdf");
				fc.utobj().sendKeys(driver, pobj.uploadcoverimage, fileName);
				fc.utobj().clickElement(driver, pobj.addCourseButton);
				testimageformat = fc.utobj().assertPageSource(driver, "please upload jpg,jpeg,png type file");
				fc.utobj().clickElement(driver, ".//*[contains(text(),'Cancel')]");
			} catch (Exception e) {
				fc.utobj().printTestStep("Warning: Image is not getting loaded");
			}
			if (!testimageformat) {
				throw new Exception("image with pdf format added in category popup");
			}
			;

			fc.utobj().clickElement(driver, ".//div[@data-role='ico_Filter']");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='categoryName']"),
					this.getCategoryName());
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='search']"));
			xpath = ".//*[contains(text () , '" + this.getCategoryName()
					+ "')]/ancestor::div/following-sibling::div//div[@data-role='ico_ThreeDots']";
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, xpath));
			fc.utobj().clickElement(driver, pobj.addCourse);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			//fc.utobj().assertPageSource(driver, "Fields marked with <span class=\"mandatory-input\"/> are mandatory. ");
			boolean check = fc.utobj().isElementPresent(driver, ".//div[contains(text(),'Fields marked with')]");
			if (!check) {
				//throw new Exception("Mandatory field value is not found");
				fc.utobj().throwsException("Mandatory field value is not found");
			}
			String fileName = fc.utobj().getFilePathFromTestData("logo.jpg");
			course.setAllowrolebasedaccess(this.isRolebasedAccessCategory());
			try {
				fc.utobj().sendKeys(driver, pobj.uploadcoverimage, fileName);
			} catch (Exception e) {
				pobj.uploadcoverimage.sendKeys(fileName);
			}
			;
			fc.utobj().sendKeys(driver, pobj.courseName, course.getCoursename());
			;
			fc.utobj().sendKeys(driver, pobj.courseObjective, course.getCourseobjective());
			;
			fc.utobj().clickElement(driver, ".//*[@id='fc-drop-parentuserCombo']/button");
			;
			if (course.getInstrutor() != null)
			{
				JavascriptExecutor jse = (JavascriptExecutor)driver;
				WebElement element = driver.findElement(By.xpath(".//*[@id='fc-drop-parentuserCombo']/div/div/input"));
				try
				{
				jse.executeScript("arguments[0].value='FranConnect Administrator'", element);
				Actions actions = new Actions(driver);
				Thread.sleep(1000);
				Thread.sleep(1000);
				actions.moveToElement(element);
				actions.sendKeys(Keys.ENTER).click(element).build().perform();
				actions.release().perform();
				actions.sendKeys(Keys.ENTER).click(element).keyUp(Keys.ENTER).build().perform();
				Thread.sleep(1000);
				}
				catch(Exception e)
				{
					fc.utobj().clickElement(driver, ".//*[@id='fc-drop-parentuserCombo']/button");
					fc.utobj().sendKeys(driver, element, "FranConnect Administrator");
					Thread.sleep(5000);
					element.sendKeys(Keys.ENTER);
					Thread.sleep(1000);
					
				}
				
				
				
				fc.utobj()
						.clickElement(driver,
								".//*[@id='fc-drop-parentuserCombo']/div/ul/li/.//*[contains(text(),'"
										+ course.getInstrutor() + "')]");
			}
			;
			fc.utobj().sendKeys(driver, pobj.instructions, course.getCourseinstruction());
			;
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='LockThisCourse']/label"));
			;

			if (this.RolebasedAccessCategory) {

				fc.utobj().clickElement(driver, pobj.divisionCourse);
				;
				if (this.getCategoryDivisionRole() != "" || this.getCategoryDivisionRole() != null)
					fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
							".//*[@id='fc-drop-parentbrands']/div/ul/li/label/span[contains(text(),'Select All')]"));
				;
				if (course.getCoursedependency() != null) {
					fc.utobj().clickElement(driver, ".//*[@id='fc-drop-parentcourseCombo']/button");
					fc.utobj().clickElement(driver,
							".//*[@id='fc-drop-parentcourseCombo']/div/ul//span[contains(text(), '"
											+ course.getCoursedependency().getCoursename() + "')]");

				}

				boolean invalidrole = false;

				if (!invalidrole) {
					try {
						fc.utobj().clickElement(driver,
								fc.utobj().getElementByXpath(driver, ".//*[@id='fc-drop-parentcorRoles']/button"));
						fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
								".//*[@id='fc-drop-parentcorRoles']/div/ul//label/span[contains(text(), 'invalid role')]"));
						Thread.sleep(1000);
						invalidrole = true;

					} catch (Exception e) {
						fc.utobj().printTestStep("Searching for invalid corp role doesn't work");
					}
				}

				fc.utobj().clickElement(driver,
						".//*[@id='fc-drop-parentcorRoles']/div/ul/li/label/span[contains(text(),'"
										+ this.getCategoryCorporateRole() + "')]");
				;

				if (!invalidrole) {
					try {
						fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
								".//*[@id='fc-drop-parentfranchiseeRoles']/button"));
						fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
								".//*[@id='fc-drop-parentfranchiseeRoles']/div/ul//label/span[contains(text(), 'invalid role')]"));
						;
						invalidrole = true;

					} catch (Exception e) {
						fc.utobj().printTestStep("Searching for invalid role doesn't work");
					}

				}

				fc.utobj().clickElement(driver,
						".//*[@id='fc-drop-parentfranchiseeRoles']/div/ul//label/span[contains(text(), '"
										+ this.getCategoryFranchiseRole() + "')]");
				;

				if (!invalidrole) {
					try {
						fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
								".//*[@id='fc-drop-parentdivisionalRoles']/button"));
						fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
								".//*[@id='fc-drop-parentdivisionalRoles']/div/ul//label/span[contains(text(), 'invalid role')]"));
						;
						invalidrole = true;
					} catch (Exception e) {
						fc.utobj().printTestStep("Searching for invalid role doesn't work");
					}
				}
				fc.utobj().clickElement(driver,
						".//*[@id='fc-drop-parentdivisionalRoles']/div/ul//label/span[contains(text(), '"
										+ this.getCategoryDivisionRole() + "')]");
				;

				if (!invalidrole) {

					try {
						fc.utobj().clickElement(driver,
								fc.utobj().getElementByXpath(driver, ".//*[@id='fc-drop-parentregionalRoles']/button"));
						fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
								".//*[@id='fc-drop-parentregionalRoles']/div/ul//label/span[contains(text(), 'invalid role')]"));
						;
						invalidrole = true;

					} catch (Exception e) {
						fc.utobj().printTestStep("Searching for invalid Regional role doesn't work");
					}
					fc.utobj().clickElement(driver,
							".//*[@id='fc-drop-parentregionalRoles']/div/ul//label/span[contains(text(), '"
											+ this.getCategoryRegionalRole() + "')]");

				}
			}

			;
			fc.utobj().clickElement(driver, pobj.addCourseButton);
			boolean found = fc.utobj().assertPageSource(driver, "Course Title already exists.");
			if (found) {
				fc.utobj().clickElement(driver, ".//*[contains(text(),'Cancel')]");
				fc.utobj().throwsException("Trying to add duplicate records which doesn't work");
			}
			;
			if (course.getCoursename().equals("") || course.getCourseobjective().equals("")) {
				fc.utobj().assertPageSource(driver, "This field is required");
				fc.utobj().clickElement(driver, ".//*[contains(text(),'Cancel')]");
				fc.utobj().throwsException("Course not get added as per mandatory fields were not present");
			}
			if (course.getInstrutor().equals("")) {
				fc.utobj().assertPageSource(driver, "Please select atleast one field.");
				fc.utobj().clickElement(driver, ".//*[contains(text(),'Cancel')]");
				fc.utobj().throwsException("Course not get added as per mandatory fields were not present");
			}
			return course;

		} catch (Exception e) {
			e.printStackTrace();
			course = null;
		}
		return course;

	}

	public boolean verifyUnPublishedCourse(WebDriver driver, Map<String, String> config, Course course) {
		try {
			AdminTrainingCoureseManagementPage pobj = new AdminTrainingCoureseManagementPage(driver);
			fc.training().training_common().adminTraingCourseManagementPage(driver);
			;

			// course modify Action Img
			fc.utobj().clickElement(driver, pobj.GeneralCategory);
			;
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='dialogV']//input"),
					this.getCategoryName());
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					(".//*[@id='mCSB_1_container']//a[contains(text (), '" + this.getCategoryName() + "')]")));
			fc.utobj().sleep(500);
			fc.utobj().clickPartialLinkText(driver, course.getCoursename());
			List<String> listItems = new LinkedList<String>();
			listItems.add(course.getCoursename());
			listItems.add(course.getCourseobjective());
			listItems.add(course.getCourseinstruction());
			listItems.add(course.getInstrutor());
			if (course.isAllowrolebasedaccess()) {
				listItems.add(course.getInstrutor());
			}
			boolean isTextPresent = fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);
			if (isTextPresent == false) {
				fc.utobj().throwsException("Was not able to add course !!!");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return false;

	}

	public String getUploadCoverImage() {
		return UploadCoverImage;
	}

	public void setUploadCoverImage(String uploadCoverImage) {
		UploadCoverImage = uploadCoverImage;
	}

}
