package com.builds.test.training;

import java.util.Map;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import com.builds.uimaps.training.AdminTrainingCoureseManagementPage;
import com.builds.uimaps.training.AdminTrainingCourseManagementAddSectionrPage;
import com.builds.utilities.FranconnectUtil;

public class Course extends FranconnectUtil {
	FranconnectUtil fc = new FranconnectUtil();
	private String coursename = "";
	private String instrutor = "";
	private String courseobjective = "";
	private String courseinstruction;
	private Course coursedependency;
	private String coursedivision;
	private boolean lockthiscourse;
	private boolean allowrolebasedaccess;
	private String uploadcoverimage;
	private String CategoryCorporateRole = "Select All";
	private String CategoryRegionalRole = "Select All";
	private String CategoryFranchiseRole = "Select All";
	private String CategoryDivisionRole = "Select All";

	public Course() {
	}

	public String getCoursename() {
		return coursename;
	}

	public void setCoursename(String coursename) {
		this.coursename = coursename;
	}

	public String getInstrutor() {
		return instrutor;
	}

	public void setInstrutor(String instrutor) {
		this.instrutor = instrutor;
	}

	public String getCourseobjective() {
		return courseobjective;
	}

	public void setCourseobjective(String courseobjective) {
		this.courseobjective = courseobjective;
	}

	public String getCourseinstruction() {
		return courseinstruction;
	}

	public void setCourseinstruction(String courseinstruction) {
		this.courseinstruction = courseinstruction;
	}

	public Course getCoursedependency() {
		return coursedependency;
	}

	public void setCoursedependency(Course course) {
		this.coursedependency = course;
	}

	public String getCoursedivision() {
		return coursedivision;
	}

	public void setCoursedivision(String coursedivision) {
		this.coursedivision = coursedivision;
	}

	public boolean isLockthiscourse() {
		return lockthiscourse;
	}

	public void setLockthiscourse(boolean lockthiscourse) {
		this.lockthiscourse = lockthiscourse;
	}

	public boolean isAllowrolebasedaccess() {
		return allowrolebasedaccess;
	}

	public void setAllowrolebasedaccess(boolean allowrolebasedaccess) {
		this.allowrolebasedaccess = allowrolebasedaccess;
	}

	public String getUploadcoverimage() {
		return uploadcoverimage;
	}

	public void setUploadcoverimage(String uploadcoverimage) {
		this.uploadcoverimage = uploadcoverimage;
	}

	public String getCategoryCorporateRole() {
		return CategoryCorporateRole;
	}

	public void setCategoryCorporateRole(String categoryCorporateRole) {
		CategoryCorporateRole = categoryCorporateRole;
	}

	public String getCategoryRegionalRole() {
		return CategoryRegionalRole;
	}

	public void setCategoryRegionalRole(String categoryRegionalRole) {
		CategoryRegionalRole = categoryRegionalRole;
	}

	public String getCategoryFranchiseRole() {
		return CategoryFranchiseRole;
	}

	public void setCategoryFranchiseRole(String categoryFranchiseRole) {
		CategoryFranchiseRole = categoryFranchiseRole;
	}

	public String getCategoryDivisionRole() {
		return CategoryDivisionRole;
	}

	public void setCategoryDivisionRole(String categoryDivisionRole) {
		CategoryDivisionRole = categoryDivisionRole;
	}

	public Lesson addLesson(WebDriver driver, Category c, Lesson lesson) throws Exception {
		try {
			// check this out
			AdminTrainingCoureseManagementPage pobj = new AdminTrainingCoureseManagementPage(driver);
			training().training_common().adminTraingCourseManagementPage(driver);
			
			
			/*fc.utobj().clickElement(driver, pobj.GeneralCategory);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='dialogV']//input"),
					c.getCategoryName());
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,(".//*[@id='mCSB_1_container']//a[contains(text (), '" + c.getCategoryName() + "')]")));
			*/
			
			fc.utobj().printTestStep("Search Category");
			new AdminTrainingCoursesManagementPageTest().searchCategory(driver, pobj, c.getCategoryName());
			
			
			fc.utobj().clickElement(driver, ".//*[contains(text(),\"" + this.coursename + "\")]");
			fc.utobj().printTestStep("Create Lesson");
			fc.utobj().clickElement(driver, pobj.addLesson);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			String sectionTitle = fc.utobj().generateTestData("sectionTitle");
			lesson.setLessonName(sectionTitle);
			fc.utobj().sendKeys(driver, pobj.sectionTitle, sectionTitle);
			String summary = fc.utobj().generateTestData("summary");
			lesson.setLessonSummary(summary);
			fc.utobj().sendKeys(driver, pobj.summary, summary);
			fc.utobj().clickElement(driver, pobj.addLesson);
			driver.switchTo().defaultContent();
			fc.utobj().clickElement(driver, pobj.uploadFile);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			String fileName = fc.utobj().getFilePathFromTestData("taskFile.pdf");
			fc.utobj().sendKeys(driver, pobj.fileuploader, fileName);
			AdminTrainingCourseManagementAddSectionPageTest Objacp = new AdminTrainingCourseManagementAddSectionPageTest();
			String quizquestion="Which date is today ??";
			AdminTrainingCourseManagementAddSectionrPage pobj2 = new AdminTrainingCourseManagementAddSectionrPage(
					driver);
 			fc.utobj().clickElement(driver, pobj2.createQuizButton);
 			fc.utobj().switchFrameToDefault(driver);

			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			fc.utobj().clickElement(driver, ".//button[contains(text(),'Cancel')]");
//			Objacp.addQuizBeforeCreatingLesson(driver, sectionTitle, config,quizquestion);
//			fc.utobj().clickElement(driver, pobj.back);
			boolean isTextPresent = fc.utobj().assertPageSource(driver, sectionTitle);
			if (isTextPresent == false) {
				fc.utobj().throwsException("Was not able to add Section!!!");
			}
		} catch (Exception ex) {
			utobj().throwsException("Lesson not get added");
		}
		return lesson;

	}
	public Lesson addLessonModifyQuiz(WebDriver driver, Category c, Lesson lesson) throws Exception {
		try {
			// check this out
			AdminTrainingCoureseManagementPage pobj = new AdminTrainingCoureseManagementPage(driver);
			training().training_common().adminTraingCourseManagementPage(driver);
			fc.utobj().clickElement(driver, pobj.GeneralCategory);
			;
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='dialogV']//input"),
					c.getCategoryName());
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,(".//*[@id='mCSB_1_container']//a[contains(text (), '" + c.getCategoryName() + "')]")));
			fc.utobj().sleep(500);
			fc.utobj().clickElement(driver, ".//*[contains(text(),'" + this.coursename + "')]");
			fc.utobj().printTestStep("Create Lesson");
			JavascriptExecutor js = ((JavascriptExecutor) driver);
			js.executeScript("window.scrollTo(0, 0)");
			System.out.println(this.coursename);
			fc.utobj().clickElement(driver, pobj.addLesson);
			;
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			String sectionTitle = fc.utobj().generateTestData("sectionTitle");
			fc.utobj().sendKeys(driver, pobj.sectionTitle, sectionTitle);
			String summary = fc.utobj().generateTestData("summary");
			fc.utobj().sendKeys(driver, pobj.summary, summary);
			fc.utobj().clickElement(driver, pobj.addLesson);
			;
			driver.switchTo().defaultContent();
			fc.utobj().clickElement(driver, pobj.uploadFile);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			String fileName = fc.utobj().getFilePathFromTestData("taskFile.pdf");
			;
			fc.utobj().sendKeys(driver, pobj.fileuploader, fileName);
			;
			AdminTrainingCourseManagementAddSectionPageTest Objacp = new AdminTrainingCourseManagementAddSectionPageTest();
			String quizquestion="Which date is today ??"+fc.utobj().generateRandomChar();
			Objacp.addQuizBeforeCreatingLesson(driver, sectionTitle, config,quizquestion);
			Objacp.ModifyQuiz(driver, sectionTitle,quizquestion, config);
			fc.utobj().clickElement(driver, ".//*[@for='allQuestionID']");
			fc.utobj().clickElement(driver, ".//*[@id='dialogV']/div[3]/div/div/form/div/div/div[1]/div[2]/div/div/div");
			fc.utobj().clickElement(driver, ".//*[@id='dialogV']/div[3]/div/div/form/div/div/div[1]/div[2]/div/ul/li[2]/a");
			fc.utobj().acceptAlertBox(driver);
			boolean check=utobj().assertPageSource(driver, "View Per Page");
			if (!check) {
				throw new Exception("view per page not found");
			}
			check = utobj().verifyElementOnVisible_ByXpath(driver, ".//*[@data-role=\"ico_ArrowBackward\"]");
			if (!check) {
				throw new Exception("Backward Arrow not found");
			}
			check = utobj().verifyElementOnVisible_ByXpath(driver, ".//*[@data-role=\"ico_ArrowLeft\"]");
			if (!check) {
				throw new Exception("Left Arrow not found");
			}
			check = utobj().verifyElementOnVisible_ByXpath(driver, ".//*[@data-role=\"ico_ArrowRight\"]");
			if (!check) {
				throw new Exception("Right Arrow not found");
			}
			check = utobj().verifyElementOnVisible_ByXpath(driver, ".//*[@data-role=\"ico_ArrowForward\"]");
			if (!check) {
				throw new Exception("Forward Arrow not found");
			}
			fc.utobj().isElementPresent(driver, pobj.print);
			
			fc.utobj().clickElement(driver, pobj.back);
			boolean isTextPresent = fc.utobj().assertPageSource(driver, sectionTitle);
			if (isTextPresent == false) {
				fc.utobj().throwsException("Was not able to add Section!!!");
			}
			
			System.out.println("Hello");
		} catch (Exception ex) {
			utobj().throwsException("Lesson not get added");
		}
		return lesson;
		
	}
	public Lesson addLesson_QuizWithAllTypeOfQuestion(WebDriver driver, Category c, Lesson lesson) throws Exception {
		try {
			AdminTrainingCoureseManagementPage pobj = new AdminTrainingCoureseManagementPage(driver);
			training().training_common().adminTraingCourseManagementPage(driver);
			
			/*fc.utobj().clickElement(driver, pobj.GeneralCategory);
			;
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='dialogV']//input"),
					c.getCategoryName());
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,(".//*[@id='mCSB_1_container']//a[contains(text (), '" + c.getCategoryName() + "')]")));
			*/
			
			fc.utobj().printTestStep("Search Category");
			new AdminTrainingCoursesManagementPageTest().searchCategory(driver, pobj, c.getCategoryName());
			
			
			fc.utobj().sleep(500);
			fc.utobj().clickElement(driver, ".//*[contains(text(),'" + this.coursename + "')]");
			fc.utobj().printTestStep("Create Lesson");
			JavascriptExecutor js = ((JavascriptExecutor) driver);
			js.executeScript("window.scrollTo(0, 0)");
			System.out.println(this.coursename);
			fc.utobj().clickElement(driver, pobj.addLesson);
			;
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			String sectionTitle = fc.utobj().generateTestData("sectionTitle");
			fc.utobj().sendKeys(driver, pobj.sectionTitle, sectionTitle);
			String summary = fc.utobj().generateTestData("summary");
			fc.utobj().sendKeys(driver, pobj.summary, summary);
			fc.utobj().clickElement(driver, pobj.addLesson);
			;
			driver.switchTo().defaultContent();
			fc.utobj().clickElement(driver, pobj.uploadFile);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			String fileName = fc.utobj().getFilePathFromTestData("taskFile.pdf");
			;
			fc.utobj().sendKeys(driver, pobj.fileuploader, fileName);
			;
			AdminTrainingCourseManagementAddSectionPageTest Objacp = new AdminTrainingCourseManagementAddSectionPageTest();
			String quizquestion="Which date is today ??"+fc.utobj().generateRandomChar();
			Objacp.addQuizAllTypeQuestionBeforeCreatingLesson(driver, sectionTitle, config,quizquestion);
			Objacp.ModifyQuiz(driver, sectionTitle,quizquestion, config);
			System.out.println("hello how are you");
			fc.utobj().clickElement(driver, ".//*[@id='dialogV']/div[3]/div/div/form/div/div/div[3]/div[2]/table/tbody/tr[1]/td[1]/label");
			fc.utobj().clickElement(driver, ".//*[@id='dialogV']/div[3]/div/div/form/div/div/div[1]/div[2]/div/div/div");
			fc.utobj().clickElement(driver, ".//*[@id='dialogV']/div[3]/div/div/form/div/div/div[1]/div[2]/div/ul/li[3]/a");
			fc.utobj().acceptAlertBox(driver);
			boolean check=utobj().assertPageSource(driver, "View Per Page");
			if (!check) {
				throw new Exception("view per page not found");
			}
			check = utobj().verifyElementOnVisible_ByXpath(driver, ".//*[@data-role=\"ico_ArrowBackward\"]");
			if (!check) {
				throw new Exception("Backward Arrow not found");
			}
			check = utobj().verifyElementOnVisible_ByXpath(driver, ".//*[@data-role=\"ico_ArrowLeft\"]");
			if (!check) {
				throw new Exception("Left Arrow not found");
			}
			check = utobj().verifyElementOnVisible_ByXpath(driver, ".//*[@data-role=\"ico_ArrowRight\"]");
			if (!check) {
				throw new Exception("Right Arrow not found");
			}
			check = utobj().verifyElementOnVisible_ByXpath(driver, ".//*[@data-role=\"ico_ArrowForward\"]");
			if (!check) {
				throw new Exception("Forward Arrow not found");
			}
			fc.utobj().isElementPresent(driver, pobj.print);
			
			fc.utobj().clickElement(driver, pobj.back);
			boolean isTextPresent = fc.utobj().assertPageSource(driver, sectionTitle);
			if (isTextPresent == false) {
				fc.utobj().throwsException("Was not able to add Section!!!");
			}
			
			System.out.println("Hello");
		} catch (Exception ex) {
			utobj().throwsException("Lesson not get added");
		}
		return lesson;
		
	}
	public Lesson addLessonDeletequiz(WebDriver driver, Category c, Lesson lesson) throws Exception {
		try {
			// check this out
			AdminTrainingCoureseManagementPage pobj = new AdminTrainingCoureseManagementPage(driver);
			training().training_common().adminTraingCourseManagementPage(driver);
			/*fc.utobj().clickElement(driver, pobj.GeneralCategory);
			;
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='dialogV']//input"),
					c.getCategoryName());
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,(".//*[@id='mCSB_1_container']//a[contains(text (), '" + c.getCategoryName() + "')]")));*/
			
			fc.utobj().printTestStep("Search Category");
			new AdminTrainingCoursesManagementPageTest().searchCategory(driver, pobj, c.getCategoryName());
			
			fc.utobj().sleep(500);
			fc.utobj().clickElement(driver, ".//*[contains(text(),\"" + this.coursename + "\")]");
			fc.utobj().printTestStep("Create Lesson");
			fc.utobj().clickElement(driver, pobj.addLesson);
			;
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			String sectionTitle = fc.utobj().generateTestData("sectionTitle");
			lesson.setLessonName(sectionTitle);
			fc.utobj().sendKeys(driver, pobj.sectionTitle, sectionTitle);
			String summary = fc.utobj().generateTestData("summary");
			lesson.setLessonSummary(summary);
			fc.utobj().sendKeys(driver, pobj.summary, summary);
			fc.utobj().clickElement(driver, pobj.addLesson);
			;
			driver.switchTo().defaultContent();
			fc.utobj().clickElement(driver, pobj.uploadFile);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			String fileName = fc.utobj().getFilePathFromTestData("taskFile.pdf");
			;
			fc.utobj().sendKeys(driver, pobj.fileuploader, fileName);
			;
			AdminTrainingCourseManagementAddSectionPageTest Objacp = new AdminTrainingCourseManagementAddSectionPageTest();
			String quizquestion="Which date is today ??";
			Objacp.addQuizBeforeCreatingLesson(driver, sectionTitle, config, quizquestion);
			Objacp.DeleteQuiz(driver, sectionTitle, config,quizquestion);
			fc.utobj().isElementPresent(driver, pobj.print);
			fc.utobj().clickElement(driver, pobj.back);
			boolean isTextPresent = fc.utobj().assertPageSource(driver, sectionTitle);
			if (isTextPresent == false) {
				fc.utobj().throwsException("Was not able to add Section!!!");
			}
			
			System.out.println("Hello");
		} catch (Exception ex) {
			utobj().throwsException("Lesson not get added");
		}
		return lesson;
		
	}

	public void verifyLesson() {
		// TODO Auto-generated method stub

	}

	public void VerifyActionButton(WebDriver driver, Map<String, String> config, Category category) {
		try {
			AdminTrainingCoureseManagementPage pobj = new AdminTrainingCoureseManagementPage(driver);
			fc.training().training_common().adminTraingCourseManagementPage(driver);
			;

			// course modify Action Img check
			fc.utobj().clickElement(driver, pobj.GeneralCategory);
			;
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='dialogV']//input"),
					category.getCategoryName());
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='mCSB_1_container']//a[contains(text (), '" + category.getCategoryName() + "')]"));
			fc.utobj().sleep(500);
			String nameOfCourseToBeDelete = this.getCoursename();
			fc.utobj().clickElement(driver, ".//div[@data-role='ico_Filter']");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='searchCourse']"),
					nameOfCourseToBeDelete);
			fc.utobj().sleep(2500);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='search']"));
			String xpath = ".//*[contains(text () , '" + nameOfCourseToBeDelete
					+ "')]/ancestor::div/following-sibling::div//div[@data-role='ico_ThreeDots']";
			fc.utobj().sleep(2500);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, xpath));
			;
			fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[@id='dialogV']//a[contains(text(),'Delete')]");
			fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[@id='dialogV']//a[contains(text(),'Copy & Customize')]");
			fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[@id='dialogV']//a[contains(text(),'Participants')]");
			fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[@id='dialogV']//a[contains(text(),'Copy URL')]");
			fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[@id='dialogV']//a[contains(text(),'Modify')]");

		} catch (Exception ex) {

		}

	}

	public void VefityCopyandCustomizeTab(WebDriver driver, Map<String, String> config, Category category) {
		try {
			AdminTrainingCoureseManagementPage pobj = new AdminTrainingCoureseManagementPage(driver);
			fc.training().training_common().adminTraingCourseManagementPage(driver);
			;

			// course modify Action Img check
			fc.utobj().clickElement(driver, pobj.GeneralCategory);
			;
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='dialogV']//input"),
					category.getCategoryName());
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='mCSB_1_container']//a[contains(text (), '" + category.getCategoryName() + "')]"));
			fc.utobj().sleep(500);
			String nameOfCourseToBeDelete = this.getCoursename();
			fc.utobj().clickElement(driver, ".//div[@data-role='ico_Filter']");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='searchCourse']"),
					nameOfCourseToBeDelete);
			fc.utobj().sleep(2500);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='search']"));
			String xpath = ".//*[contains(text () , '" + nameOfCourseToBeDelete
					+ "')]/ancestor::div/following-sibling::div//div[@data-role='ico_ThreeDots']";
			fc.utobj().sleep(2500);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, xpath));
			;
			fc.utobj().clickElement(driver, ".//*[@id='dialogV']//a[contains(text(),'Copy & Customize')]");
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			fc.utobj().sendKeys(driver, pobj.CopyandCustomizeCourseName, this.getCoursename());
			fc.utobj().clickElement(driver, ".//*[contains(text(),'Confirm')]");
			boolean check = fc.utobj().assertPageSource(driver, "Course Title already exists.");
			if (!check) {
				fc.utobj().throwsException("Duplicate course got added");
			}
			String coursename = fc.utobj().generateTestData("Customizecourse");
			this.setCoursename(coursename);
			fc.utobj().sendKeys(driver, pobj.CopyandCustomizeCourseName, this.getCoursename());
			fc.utobj().clickElement(driver, ".//*[contains(text(),'Confirm')]");

		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

}
