package com.builds.test.training;

public class Lesson {
	private String LessonName = "";
	private String LessonSummary = "";
	private String LessonDependancy = "";

	public String getLessonName() {
		return LessonName;
	}

	public void setLessonName(String lessonName) {
		LessonName = lessonName;
	}

	public String getLessonSummary() {
		return LessonSummary;
	}

	public void setLessonSummary(String lessonSummary) {
		LessonSummary = lessonSummary;
	}

	public String getLessonDependancy() {
		return LessonDependancy;
	}

	public void setLessonDependancy(String lessonDependancy) {
		LessonDependancy = lessonDependancy;
	}

}
