package com.builds.test.training;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminDivisionAddDivisionPageTest;
import com.builds.uimaps.training.AdminTrainingCoureseManagementPage;
import com.builds.uimaps.training.AdminTrainingCourseManagementAddSectionrPage;
import com.builds.uimaps.training.AdminTrainingPlanAndCertificatePage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class New1808_plan extends FranconnectUtil{
	
	FranconnectUtil fc=new FranconnectUtil();
	
	@Test(groups = { "training","Minee1"})
	@TestCase(createdOn = "2018-01-03", updatedOn = "2017-01-03", testCaseDescription = "Verify the Plan with roles", testCaseId = "Tc_Verify_Plan_Got_Deleted")
	private void validateQuizFunctionalityPreview() throws Exception {
		String testCaseId = utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = utobj().readTestData("training", testCaseId);
		WebDriver driver = commonMethods().browsers().openBrowser();
		try {
			driver = loginpage().login(driver);
			AdminDivisionAddDivisionPageTest p1 = new AdminDivisionAddDivisionPageTest();
			String divisionName = utobj().generateTestData("divisionName");
			String zipCode = utobj().generateTestData("zipCode");
			p1.addDivisionByGeographyZipCode(driver, divisionName, zipCode, config);
			AdminTrainingPlanAndCertificatePage pobj = new AdminTrainingPlanAndCertificatePage(driver);
			utobj().printTestStep("Navigating to Admin > Training > Question Library Page");
			Training T= new Training();
			Category c = new Category();
			c.setCategoryCorporateRole(driver, config);
			c.setCategoryFranchiseRole(driver, config);
			c.setCategoryDivisionRole(driver, config);
			c.setCategoryRegionalRole(driver, config);
			c.setCategoryDesc(utobj().generateTestData("descsription"));
			c.setCategoryName(utobj().generateTestData("categoryname"));
			c.setUploadCoverImage(utobj().getFilePathFromTestData("logo.jpg"));
			c.setRolebasedAccessCategory(false);
			c = T.AddCategory(driver, config, c);
			if (c == null) {
				utobj().throwsException("Category not get added kindly check screen shot");
			}
			Course course = new Course();
			course.setCourseobjective(utobj().generateTestData("courseobjective"));
			course.setInstrutor("FranConnect Administrator");
			course.setCourseinstruction("This is the sameple instruction");
			course.setAllowrolebasedaccess(c.isRolebasedAccessCategory());
			course.setCoursename(utobj().generateTestData("CourseName"));
			course = c.addCouse(driver, config, course);
			Lesson lesson = new Lesson();
			lesson = course.addLessonDeletequiz(driver, c, lesson);
			
			utobj().switchFrameToDefault(driver);
			utobj().clickElement(driver, ".//a[.='"+lesson.getLessonName()+"']/ancestor::div[@class='grid-cell']/following-sibling::div//button");
			training().training_common().adminTraingPlanAndCertificatesPage(driver);
			utobj().clickElement(driver, pobj.Create);
			utobj().clickElement(driver, pobj.CreatePlan);
			commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			boolean check = utobj().isElementPresent(driver, ".//div[contains(text(),'Fields marked with')]");
			if (!check) {
				throw new Exception("Mandatory field value is not found ");
			}
			String planName = dataSet.get("PlanName") + utobj().generateRandomNumber();
			utobj().sendKeys(driver, pobj.planName, planName);
			utobj().clickElement(driver, ".//*//label[@data-target='.configure-roles-contaione']");
			boolean invalidrole = false;

			if (!invalidrole) {
				try {
					WebElement element=utobj().getElementByID(driver, "fc-drop-parentcorRoles");
					utobj().clickElement(driver, element);
					WebElement element1=element.findElement(By.xpath("./button/parent::div/div//input[@class='searchInputMultiple']"));
					utobj().sendKeys(driver, element1, c.getCategoryCorporateRole());
					utobj().sleep(2000);
					utobj().clickElement(driver, ".//input[@class='search-btn on']");
					utobj().clickElement(driver, ".//*[@id='fc-drop-parentcorRoles']/div/ul//span[contains(text(), '"
							+ c.getCategoryCorporateRole() + "')]");

				} catch (Exception e) {
					invalidrole = true;
					utobj().printTestStep("Searching for invalid corp role doesn't work");
				}

				utobj().sleep(1000);
			}
			if (!invalidrole) {

				try {
					WebElement element=utobj().getElementByID(driver, "fc-drop-parentregionalRoles");
					utobj().clickElement(driver, element);
					WebElement element1=element.findElement(By.xpath("./button/parent::div/div//input[@class='searchInputMultiple']"));
					utobj().sendKeys(driver, element1, c.getCategoryRegionalRole());
					utobj().sleep(2000);
					utobj().clickElement(driver, ".//input[@class='search-btn on']");
					utobj().clickElement(driver,
							".//*[@id='fc-drop-parentregionalRoles']/div/ul//span[contains(text(), '"
									+ c.getCategoryRegionalRole() + "')]");
					utobj().sleep(1000);
				} catch (Exception e) {
					invalidrole = true;
					utobj().printTestStep("Searching for invalid Regional role doesn't work");

				}
				utobj().sleep(1000);
			}
			if (!invalidrole) {
				try {
					WebElement element=fc.utobj().getElementByID(driver, "fc-drop-parentdivisionalRoles");
					fc.utobj().clickElement(driver, element);
					WebElement element1=element.findElement(By.xpath("./button/parent::div/div//input[@class='searchInputMultiple']"));
					fc.utobj().sendKeys(driver, element1, c.getCategoryDivisionRole());
					fc.utobj().sleep(2000);
					fc.utobj().clickElement(driver, ".//input[@class='search-btn on']");
					utobj().clickElement(driver,
							".//*[@id='fc-drop-parentdivisionalRoles']/div/ul//span[contains(text(), '"
									+ c.getCategoryDivisionRole() + "')]");
				} catch (Exception e) {
					invalidrole = true;
					utobj().printTestStep("Searching for invalid role doesn't work");
				}
				utobj().sleep(1000);
			}
			if (!invalidrole) {
				try {
					WebElement element=fc.utobj().getElementByID(driver, "fc-drop-parentroles");
					fc.utobj().clickElement(driver, element);
					WebElement element1=element.findElement(By.xpath("./button/parent::div/div//input[@class='searchInputMultiple']"));
					fc.utobj().sendKeys(driver, element1, c.getCategoryFranchiseRole());
					fc.utobj().sleep(2000);
					fc.utobj().clickElement(driver, ".//input[@class='search-btn on']");
					utobj().clickElement(driver, ".//*[@id='fc-drop-parentroles']/div/ul//span[contains(text(), '"
							+ c.getCategoryFranchiseRole() + "')]");

				} catch (Exception e) {
					utobj().printTestStep("Searching for invalid role doesn't work");
				}

				utobj().sleep(1000);
			}

			if (invalidrole) {
				throw new Exception("invalid role got selected");
			}

			utobj().sendKeys(driver, pobj.Description, dataSet.get("Description"));
			utobj().clickElement(driver, pobj.savePlan);
			training().training_common().adminTraingPlanAndCertificatesPage(driver);
			utobj().clickElement(driver, pobj.viewperpage);
			utobj().clickElement(driver, ".//a[contains(text(),'500')]");
			utobj().clickElement(driver, pobj.FilterBtn);
			utobj().sendKeys(driver, pobj.planName, planName);
			utobj().clickElement(driver, pobj.applyFilters);
			utobj().clickElement(driver, "//*[@viewBox='0 0 12 24']");
			utobj().clickElement(driver, "//a[contains(text(),'Delete')]");
			fc.utobj().acceptAlertBox(driver);
			training().training_common().adminTraingPlanAndCertificatesPage(driver);
			utobj().clickElement(driver, "//input[@placeholder='Search for Plan, Course or Lesson by Title/Objective']");
			utobj().clickElement(driver, "//input[@id='systemSearchBtn']");
			boolean chk=utobj().assertPageSource(driver, planName);
			if(chk)
			{
				utobj().throwsException("Plan not deleted");
			}
			utobj().logoutAndQuitBrowser(driver, testCaseId);
			
		} catch (Exception e) {
			utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	
	@Test(groups = { "training","Dashboard"})
	@TestCase(createdOn = "2018-07-03", updatedOn = "2018-07-03", testCaseDescription = "Verify The validation of Certificate with plan", testCaseId = "TC_plan_certificate1")
	private void validateDashboard3() throws Exception {
		String testCaseId = utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = utobj().readTestData("training", testCaseId);
		WebDriver driver = commonMethods().browsers().openBrowser();
		try {

			driver = loginpage().login(driver);
			String parentWindowHandle = driver.getWindowHandle();

			utobj().printTestStep("Navigating to Admin > Training > Course Management Page");
			AdminTrainingCourseManagementAddSectionrPage pobj = new AdminTrainingCourseManagementAddSectionrPage(
					driver);

			AdminTrainingPlanAndCertificatePage pobjpnc = new AdminTrainingPlanAndCertificatePage(driver);
			AdminTrainingCoursesManagementPageTest courseManagement = new AdminTrainingCoursesManagementPageTest();
			utobj().printTestStep("Add Category");
			String categoryname = courseManagement.addCategory(driver, dataSet);
			utobj().printTestStep("Add Course");
			String coursename = courseManagement.addCourse(driver, dataSet, categoryname, testCaseId);

			utobj().printTestStep("Create Lesson");
			utobj().clickElement(driver, pobj.addLesson);

			commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			String sectionTitle = utobj().generateTestData(dataSet.get("sectionTitle"));
			utobj().sendKeys(driver, pobj.sectionTitle, sectionTitle);
			String summary = utobj().generateTestData(dataSet.get("summary"));
			utobj().sendKeys(driver, pobj.summary, summary);
			utobj().clickElement(driver, pobj.addLesson);

			driver.switchTo().defaultContent();
			utobj().clickElement(driver, pobj.uploadFile);
			commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			String fileName = utobj().getFilePathFromTestData("taskFile.pdf");
			utobj().sendKeys(driver, pobj.fileuploader, fileName);

			AdminTrainingCourseManagementAddSectionPageTest objadd = new AdminTrainingCourseManagementAddSectionPageTest();
			String quizname = objadd.addQuizBeforeCreatingLesson(driver, sectionTitle, dataSet, config, testCaseId);

			driver.switchTo().defaultContent();
			AdminTrainingPlanAndCertificateTest obj= new AdminTrainingPlanAndCertificateTest();
			obj.publishCourse(categoryname, coursename, driver, config, testCaseId);
			training().training_common().trainingCourse(driver);
			utobj().clickElement(driver, pobjpnc.availablePlan);
			utobj().clickElement(driver, pobjpnc.FilterBtn);
			utobj().sendKeys(driver, pobjpnc.coursename, coursename);
			utobj().clickElement(driver, pobjpnc.applyFilters);
			utobj().clickElement(driver, pobjpnc.start);
			try {
				utobj().clickElement(driver, pobj.mynotes);
				commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
				utobj().clickElement(driver, pobj.addnotes);
				commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
				utobj().sendKeys(driver, pobj.txtarea, "Hello Testing purpose only");
				utobj().clickElement(driver, pobj.add);
				utobj().switchFrameToDefault(driver);
				commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
				utobj().clickElement(driver, pobj.deletenote);
				utobj().acceptAlertBox(driver);
				utobj().clickElement(driver, pobj.close);
				utobj().switchFrameToDefault(driver);
				utobj().clickElement(driver, pobj.viewperpage);
				utobj().clickElement(driver, ".//a[contains(text(),'500')]");
				String option = "Start Learning";
				List<String> linkArray = utobj().translate(option);
				if (!(linkArray.size() < 1)) {
					for (int i = 0; i < linkArray.size(); i++) {
						try {
							option = linkArray.get(i);
							// .//text()[normalize-space()="Start Learning"]
							utobj().clickElement(driver,
									".//*/text()[normalize-space()=\"" + option + "\"]/parent::*");
							break;
						} catch (Exception e) {
						}
					}
				} else {
					utobj().clickElement(driver, ".//*/text()[normalize-space()=\"" + option + "\"]/parent::*");
				}

			} catch (Exception e) {
				utobj().clickElement(driver, pobjpnc.startLearning);
			}

			System.out.println();

			Set<String> allWindowHandles = driver.getWindowHandles();

			System.out.println(allWindowHandles.size());

			for (String currentWindowHandle : allWindowHandles) {
				if (!currentWindowHandle.equals(parentWindowHandle)) {
					driver.switchTo().window(currentWindowHandle);

					try {
						utobj().getElementByXpath(driver, ".//p[contains(text () , '" + sectionTitle + "')]")
								.isDisplayed();
						utobj().moveToElement(driver, utobj().getElement(driver, pobjpnc.closequiz));
						utobj().clickElement(driver, pobjpnc.closequiz);
					} catch (Exception e) {
						System.out.println("we reached here");
					}
					driver.switchTo().window(parentWindowHandle);
				}
			}

			training().training_common().trainingCourse(driver);
			utobj().clickElement(driver, pobjpnc.FilterBtn);
			utobj().sendKeys(driver, pobjpnc.coursename, coursename);
			utobj().clickElement(driver, pobjpnc.applyFilters);
			utobj().clickElement(driver, pobjpnc.resume);
			fc.utobj().clickElement(driver, pobjpnc.startTAgain);

			Set<String> allWindowHandles1 = driver.getWindowHandles();

			System.out.println(allWindowHandles.size());

			for (String currentWindowHandle : allWindowHandles1) {
				if (!currentWindowHandle.equals(parentWindowHandle)) {
					driver.switchTo().window(currentWindowHandle);

					try {
						utobj().getElementByXpath(driver, ".//p[contains(text () , '" + sectionTitle + "')]")
								.isDisplayed();
						utobj().moveToElement(driver, utobj().getElement(driver, pobjpnc.startquiz));
						utobj().clickElement(driver, pobjpnc.startquiz);
					} catch (Exception e) {
						System.out.println("we reached here");
					}
					driver.switchTo().window(parentWindowHandle);
				}
			}
			/*utobj().clickElement(driver,
					utobj().getElementByXpath(driver, ".//*[contains(text(),'Start Quiz')]"));*/

			commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			utobj().clickElement(driver, pobjpnc.startquiz);
			utobj().switchFrameToDefault(driver);
			utobj().clickElement(driver, pobjpnc.insertDate);

			DateFormat dateFormat2 = new SimpleDateFormat("dd");
			Date date2 = new Date();

			String today = dateFormat2.format(date2);

			// find the calendar
			WebElement dateWidget = utobj().getElementByID(driver, "fc-datepicker-div");
			List<WebElement> columns = dateWidget.findElements(By.tagName("a"));

			// comparing the text of cell with today's date and clicking it.
			for (WebElement cell : columns) {
				if (cell.getText().equals(today)) {
					cell.click();
					break;
				}
			}
			// utobj().sendKeys(driver, pobjpnc.insertDate, "03/03/2017");
			utobj().clickElement(driver, pobjpnc.finishQuiz);
			utobj().clickElement(driver, pobjpnc.SubmitQuiz);
			commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			utobj().clickElement(driver, pobjpnc.Close);
			fc.utobj().switchFrameToDefault(driver);

			training().training_common().trainingCourse(driver);
			utobj().clickElement(driver, pobjpnc.CompletedPlan);
			utobj().clickElement(driver, pobjpnc.FilterBtn);
			utobj().sendKeys(driver, pobjpnc.coursename, coursename);
			utobj().clickElement(driver, pobjpnc.applyFilters);

			// Working on Assessment from here
			training().training_common().trainingAssessment(driver);
			utobj().clickElement(driver, pobj.viewperpage);
			utobj().clickElement(driver, ".//a[contains(text(),'500')]");
			utobj().clickElement(driver, pobjpnc.FilterBtn);

			utobj().clickElement(driver,
					utobj().getElementByXpath(driver, ".//*[@id='fc-drop-parentsectionId']/button"));

			utobj().clickElement(driver, ".//*[@id='fc-drop-parentsectionId']//span[contains(text(),'Select All')]");

			utobj().sendKeys(driver,
					utobj().getElementByXpath(driver, ".//*[@id='fc-drop-parentsectionId']/div/div/input"),
					"" + sectionTitle + "");
			utobj().clickElement(driver, utobj().getElementByXpath(driver,
					(".//*[@id='fc-drop-parentsectionId']//span[contains(text(),'" + sectionTitle + "')]")));
			utobj().clickElement(driver,
					utobj().getElementByXpath(driver, ".//*[@id='fc-drop-parentsectionId']/button"));
			utobj().clickElement(driver, pobjpnc.applyFilters);
			utobj().clickPartialLinkText(driver, quizname);
			commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			utobj().assertPageSource(driver, quizname);
			utobj().assertPageSource(driver, "Cancel");
			utobj().clickElement(driver, pobjpnc.startevaluation);
			utobj().switchFrameToDefault(driver);

			utobj().clickElement(driver, utobj().getElementByXpath(driver,
					(".//td[contains(text(),'FranConnect Administrator')]/following-sibling::td[2]/button")));

			Actions action = new Actions(driver);
			action.click(utobj().getElement(driver, pobjpnc.slider)).build().perform();
			Thread.sleep(1000);
			for (int i = 0; i < 2; i++) {
				action.sendKeys(Keys.ARROW_RIGHT).build().perform();
				Thread.sleep(200);
			}
			utobj().clickElement(driver, pobjpnc.reviewScoring);
			commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			utobj().clickElement(driver, pobjpnc.submitassessment);
			utobj().clickElement(driver, pobjpnc.CompletedPlan);
			utobj().clickElement(driver, pobjpnc.FilterBtn);

			utobj().clickElement(driver,
					utobj().getElementByXpath(driver, ".//*[@id='fc-drop-parentsectionId']/button"));

			utobj().clickElement(driver, ".//*[@id='fc-drop-parentsectionId']//span[contains(text(),'Select All')]");

			utobj().sendKeys(driver,
					utobj().getElementByXpath(driver, ".//*[@id='fc-drop-parentsectionId']/div/div/input"),
					"" + sectionTitle + "");

			utobj().clickElement(driver, utobj().getElementByXpath(driver,
					(".//*[@id='fc-drop-parentsectionId']//span[contains(text(),'" + sectionTitle + "')]")));

			utobj().clickElement(driver,
					utobj().getElementByXpath(driver, ".//*[@id='fc-drop-parentsectionId']/button"));
			utobj().clickElement(driver, pobjpnc.applyFilters);

			// working on My Results
			training().training_common().trainingMyResults(driver);
			String date = utobj().generateCurrentDatewithformat("MM/dd/yyyy");
			boolean found1 = utobj().assertPageSource(driver, date);
			utobj().clickElement(driver, pobj.viewperpage);
			utobj().clickElement(driver, ".//a[contains(text(),'500')]");
			utobj().clickElement(driver, pobjpnc.FilterBtn);
			utobj().sendKeys(driver, pobjpnc.quizName, quizname);
			utobj().clickElement(driver, pobjpnc.applyFilters);
			utobj().clickElement(driver, ".//span[contains(text(),\"Pass\")]");
			commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			utobj().isElementPresent(driver, pobjpnc.print);
			boolean found2 = utobj().assertPageSource(driver, "100.00%");
			boolean found3 = utobj().assertPageSource(driver, "Pass");
			boolean found4 = utobj().assertPageSource(driver, "1");
			boolean found5 = utobj().assertPageSource(driver, "Which date is today ??");
			boolean found6 = utobj().assertPageSource(driver, quizname);
			boolean found7 = utobj().assertPageSource(driver, date);
			if (!found1 && !found2 && !found3 && !found4 && !found5 && !found6 && !found7)
				utobj().throwsException("Please check some points not found on Result view page");
			utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
}