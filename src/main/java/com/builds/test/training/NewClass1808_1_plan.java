package com.builds.test.training;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminDivisionAddDivisionPageTest;
import com.builds.uimaps.training.AdminTrainingCourseManagementAddSectionrPage;
import com.builds.uimaps.training.AdminTrainingPlanAndCertificatePage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class NewClass1808_1_plan extends FranconnectUtil{
	
	@Test(groups = { "training","Minee" ,"trainingFailed0924"})
	@TestCase(createdOn = "2018-01-03", updatedOn = "2017-01*03", testCaseDescription = "Verify the Plan with roles", testCaseId = "Tc_Verify_Plan_with_Roles")
	private void validateQuizFunctionalityPreview() throws Exception {
		String testCaseId = utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = utobj().readTestData("training", testCaseId);
		WebDriver driver = commonMethods().browsers().openBrowser();
		try {
			driver = loginpage().login(driver);
			AdminDivisionAddDivisionPageTest p1 = new AdminDivisionAddDivisionPageTest();
			String divisionName = utobj().generateTestData("divisionName");
			String zipCode = utobj().generateTestData("zipCode");
			p1.addDivisionByGeographyZipCode(driver, divisionName, zipCode, config);
			AdminTrainingPlanAndCertificatePage pobj = new AdminTrainingPlanAndCertificatePage(driver);
			utobj().printTestStep("Navigating to Admin > Training > Question Library Page");
			Training T= new Training();
			Category c = new Category();
			c.setCategoryCorporateRole(driver, config);
			c.setCategoryFranchiseRole(driver, config);
			c.setCategoryDivisionRole(driver, config);
			c.setCategoryRegionalRole(driver, config);
			c.setCategoryDesc(utobj().generateTestData("descsription"));
			c.setCategoryName(utobj().generateTestData("categoryname"));
			c.setUploadCoverImage(utobj().getFilePathFromTestData("logo.jpg"));
			c.setRolebasedAccessCategory(false);
			c = T.AddCategory(driver, config, c);
			if (c == null) {
				utobj().throwsException("Category not get added kindly check screen shot");
			}
			Course course = new Course();
			course.setCourseobjective(utobj().generateTestData("courseobjective"));
			course.setInstrutor("FranConnect Administrator");
			course.setCourseinstruction("This is the sameple instruction");
			course.setAllowrolebasedaccess(c.isRolebasedAccessCategory());
			course.setCoursename(utobj().generateTestData("CourseName"));
			course = c.addCouse(driver, config, course);
			Lesson lesson = new Lesson();
			lesson = course.addLessonDeletequiz(driver, c, lesson);
			
			utobj().switchFrameToDefault(driver);
			utobj().clickElement(driver, ".//a[.='"+lesson.getLessonName()+"']/ancestor::div[@class='grid-cell']/following-sibling::div//button");
			training().training_common().adminTraingPlanAndCertificatesPage(driver);
			utobj().clickElement(driver, pobj.Create);
			utobj().clickElement(driver, pobj.CreatePlan);
			commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			boolean check = utobj().assertPageSource(driver, "Fields marked with");
			if (!check) {
				throw new Exception("Mandatory field value is not found ");
			}
			String planName = dataSet.get("PlanName") + utobj().generateRandomNumber();
			utobj().sendKeys(driver, pobj.planName, planName);
			utobj().clickElement(driver, ".//*//label[@data-target='.configure-roles-contaione']");
			boolean invalidrole = false;

			if (!invalidrole) {
				try {
					utobj().clickElement(driver,
							utobj().getElementByXpath(driver, ".//*[@id='fc-drop-parentcorRoles']/button"));
					utobj().clickElement(driver, ".//*[@id='fc-drop-parentcorRoles']/div/ul//span[contains(text(), '"
							+ c.getCategoryCorporateRole() + "')]");

				} catch (Exception e) {
					invalidrole = true;
					utobj().printTestStep("Searching for invalid corp role doesn't work");
				}

				utobj().sleep(1000);
			}
			if (!invalidrole) {

				try {
					utobj().clickElement(driver,
							utobj().getElementByXpath(driver, ".//*[@id='fc-drop-parentregionalRoles']/button"));
					utobj().clickElement(driver,
							".//*[@id='fc-drop-parentregionalRoles']/div/ul//span[contains(text(), '"
									+ c.getCategoryRegionalRole() + "')]");
					utobj().sleep(1000);

				} catch (Exception e) {
					invalidrole = true;
					utobj().printTestStep("Searching for invalid Regional role doesn't work");

				}
				utobj().sleep(1000);
			}
			if (!invalidrole) {
				try {
					utobj().clickElement(driver,
							utobj().getElementByXpath(driver, ".//*[@id='fc-drop-parentdivisionalRoles']/button"));
					utobj().clickElement(driver,
							".//*[@id='fc-drop-parentdivisionalRoles']/div/ul//span[contains(text(), '"
									+ c.getCategoryDivisionRole() + "')]");
				} catch (Exception e) {
					invalidrole = true;
					utobj().printTestStep("Searching for invalid role doesn't work");
				}
				utobj().sleep(1000);
			}
			if (!invalidrole) {
				try {
					utobj().clickElement(driver,
							utobj().getElementByXpath(driver, ".//*[@id='fc-drop-parentroles']/button"));
					utobj().clickElement(driver, ".//*[@id='fc-drop-parentroles']/div/ul//span[contains(text(), '"
							+ c.getCategoryFranchiseRole() + "')]");

				} catch (Exception e) {
					utobj().printTestStep("Searching for invalid role doesn't work");
				}

				utobj().sleep(1000);
			}

			if (invalidrole) {
				throw new Exception("invalid role got selected");
			}

			utobj().sendKeys(driver, pobj.Description, dataSet.get("Description"));
			utobj().clickElement(driver, pobj.savePlan);
			training().training_common().adminTraingPlanAndCertificatesPage(driver);
			utobj().clickElement(driver, pobj.viewperpage);
			utobj().clickElement(driver, ".//a[contains(text(),'500')]");
			utobj().clickElement(driver, pobj.FilterBtn);
			utobj().sendKeys(driver, pobj.planName, planName);
			utobj().clickElement(driver, pobj.applyFilters);
			utobj().clickPartialLinkText(driver, planName);
			check = utobj().assertPageSource(driver, dataSet.get("Description"));
			if (!check) {
				throw new Exception("while clicking on Plan name description not came");
			}
			utobj().logoutAndQuitBrowser(driver, testCaseId);
			
		} catch (Exception e) {
			
			utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "training", "Testing123" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Add Articulate Content Type Section At Admin Training Course Management", testCaseId = "TC_new_tc_lessonCourse")
	private void addSectionArticulateContent() throws Exception {
		String testCaseId = utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = utobj().readTestData("training", testCaseId);
		WebDriver driver = commonMethods().browsers().openBrowser();
		try {
			driver = loginpage().login(driver);
			utobj().printTestStep("Navigating to Admin > Training > Course Management Page");
			AdminTrainingCourseManagementAddSectionrPage pobj = new AdminTrainingCourseManagementAddSectionrPage(
					driver);
			AdminTrainingCoursesManagementPageTest courseManagement = new AdminTrainingCoursesManagementPageTest();
			utobj().printTestStep("Add Category");
			String categoryname = courseManagement.addCategory(driver, dataSet);
			utobj().printTestStep("Add Course");
			courseManagement.addCourse(driver, dataSet, categoryname, testCaseId);
			utobj().printTestStep("Create Lesson");
			utobj().clickElement(driver, pobj.addLesson);

			commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			String sectionTitle = utobj().generateTestData(dataSet.get("sectionTitle"));
			utobj().sendKeys(driver, pobj.sectionTitle, sectionTitle);
			String summary = utobj().generateTestData(dataSet.get("summary"));
			utobj().sendKeys(driver, pobj.summary, summary);
			utobj().clickElement(driver, pobj.addLesson);

			driver.switchTo().defaultContent();
			utobj().clickElement(driver, pobj.articulateContent);

			commonMethods().switch_frameClass_newLayoutcboxIframe(driver);

			String fileName = utobj().getFilePathFromTestData("taskfile.html");
			boolean check = false;
			try {
				String fileName1 = utobj().getFilePathFromTestData("taskFile.zip");
				utobj().sendKeys(driver, pobj.fileuploader, fileName1);

				check = true;

			} catch (Exception ex) {
				ex.printStackTrace();
			}
			if (!check) {
				utobj().throwsException("other then zip file uploaded in articulate content");
			}
			check = utobj().assertPageSource(driver, "This field is required.");
			if (!check) {
				utobj().throwsException("error msg not came while uploading wrong file  in articulate content");
			}
			String fileName1 = utobj().getFilePathFromTestData("taskFile.zip");
			utobj().sendKeys(driver, pobj.fileuploader, fileName1);

			utobj().sendKeys(driver, utobj().getElementByXpath(driver, ".//*[@id='mainFile']"), fileName);
			// utobj().searchInSelectBoxSingleValue(driver,
			// utobj().getElementByXpath(driver,".//*[@id='mainFile']")),
			// dataSet.get("mainFile"));
			check = utobj().isElementPresent(driver, utobj().getElementByXpath(driver, ".//*[@id='preview']"));
			if (!check) {
				utobj().throwsException("Preview of uploaded file not found");
			}

			utobj().clickElement(driver, ".//*[@id='saveButton']");
			utobj().switchFrameToDefault(driver);
			AdminTrainingCourseManagementAddSectionPageTest obj= new AdminTrainingCourseManagementAddSectionPageTest();
			obj.addQuizAfterCreatingLesson(driver, sectionTitle, dataSet, config, testCaseId);
			utobj().printTestStep("Verify Add Articulate ");
			boolean isTextPresent = utobj().assertPageSource(driver, sectionTitle);
			if (isTextPresent == false) {
				utobj().throwsException("Was not able to add Section!!!");
			}

			utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
}
