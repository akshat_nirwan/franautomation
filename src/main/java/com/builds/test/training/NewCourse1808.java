package com.builds.test.training;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.builds.uimaps.training.AdminTrainingCourseManagementAddSectionrPage;
import com.builds.uimaps.training.AdminTrainingPlanAndCertificatePage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class NewCourse1808 {
	FranconnectUtil fc = new FranconnectUtil();
	
	@Test(groups = { "training", "trainingsanity", "Ban" })
	@TestCase(createdOn = "2018-07-25", updatedOn = "2018-07-25", testCaseDescription = "Verify the Add Embed Video Type Section At Admin Training Course Management Page ", testCaseId = "TC_Video_verify_in_Course")
	private void addSectionEmbedVideo() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("training", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigating to Admin > Training > Course Management Page");
			AdminTrainingCourseManagementAddSectionrPage pobj = new AdminTrainingCourseManagementAddSectionrPage(
					driver);
			AdminTrainingCoursesManagementPageTest courseManagement = new AdminTrainingCoursesManagementPageTest();
			fc.utobj().printTestStep("Add Category");
			String categoryname = courseManagement.addCategory(driver, dataSet);
			fc.utobj().printTestStep("Add Course");
			courseManagement.addCourse(driver, dataSet, categoryname, testCaseId);

			fc.utobj().printTestStep("Create Lesson");
			fc.utobj().clickElement(driver, pobj.addLesson);

			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			String sectionTitle = fc.utobj().generateTestData(dataSet.get("sectionTitle"));
			fc.utobj().sendKeys(driver, pobj.sectionTitle, sectionTitle);
			String summary = fc.utobj().generateTestData(dataSet.get("summary"));
			fc.utobj().sendKeys(driver, pobj.summary, summary);
			fc.utobj().clickElement(driver, pobj.addLesson);

			driver.switchTo().defaultContent();

			fc.utobj().clickElement(driver, pobj.embedVideo);

			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			try {
				fc.utobj().clickElement(driver, ".//*[@id='saveButton']");
				fc.utobj().getElementByXpath(driver, ".//*[@id='subSectionContent']")
						.sendKeys(dataSet.get("embedCode"));
				fc.utobj().clickElement(driver, ".//*[@id='saveButton']");
			} catch (Exception ex) {
			}

			fc.utobj().printTestStep("Verify Add Emebed Type Section");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//div[@data-role='ico_Filter']"));

			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='searchLesson']"), sectionTitle);

			fc.utobj().clickElement(driver, ".//*[@id='search']");

			addQuizAfterCreatingLesson(driver, sectionTitle, dataSet, config, testCaseId);
			fc.utobj().clickElement(driver, "//button[@class='btn-style btn-border-style WindowPopup no-margin']");
			String parentWindowHandle = driver.getWindowHandle();

			Set<String> allWindowHandles = driver.getWindowHandles();

			System.out.println(allWindowHandles.size());

			for (String currentWindowHandle : allWindowHandles) {
				if (!currentWindowHandle.equals(parentWindowHandle)) {
					driver.switchTo().window(currentWindowHandle);

					try {
						
						fc.utobj().clickElement(driver, "//div[@class='text-center']");
					} catch (Exception e) {
						System.out.println("we reached here");
					}
					driver.switchTo().window(parentWindowHandle);
				}
			}
			
			
			
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	public void addQuizAfterCreatingLesson(WebDriver driver, String sectionTitle, Map<String, String> dataSet,
			Map<String, String> config, String testCaseId) throws Exception {
		try {
			AdminTrainingCourseManagementAddSectionrPage pobj = new AdminTrainingCourseManagementAddSectionrPage(
					driver);
			String xpath = ".//*[contains(text () , '" + sectionTitle
					+ "')]/ancestor::div/following-sibling::div//div[@data-role='ico_ThreeDots']";
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, xpath));

			fc.utobj().clickElement(driver, pobj.addquiz2);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			fc.utobj().sendKeys(driver, pobj.quizName, "Quiz" + fc.utobj().generateRandomNumber());
			fc.utobj().sendKeys(driver, pobj.quizSummary, "This is a Quiz");
			fc.utobj().clickElement(driver, pobj.addquiz);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, pobj.AddNewQuestion);
			fc.utobj().clickElement(driver, pobj.ResponseTypeDate);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='text1']"),
					"Which date is today ??");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='questionMarks1']/option[@value=\"2\"]"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='saveQuiz']"));
			xpath = ".//*[contains(text () , '" + sectionTitle
					+ "')]/ancestor::div/following-sibling::div//div[@data-role='ico_ThreeDots']";
			fc.utobj().clickElement(driver, pobj.back);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, xpath));

			try
			{
				fc.utobj().clickElement(driver, pobj.viewquiz1);
			}
			catch(Exception rc)
			{
				fc.utobj().clickElement(driver, pobj.viewquiz2nd);
			}
			

			xpath = ".//*[contains(text () , '" + sectionTitle
					+ "')]/ancestor::div/following-sibling::div//div[@data-role='ico_ThreeDots']";
			fc.utobj().clickElement(driver, pobj.back);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, xpath));

			fc.utobj().clickElement(driver, pobj.copyurl);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			fc.utobj().clickElement(driver, ".//*[contains(text(),'Close')]");
		} catch (Exception ex) {
			fc.utobj().throwsException("Quiz not added");
		}
	}
	@Test(groups = { "training","Dashboard", "CheckTraining" })
	@TestCase(createdOn = "2018-07-03", updatedOn = "2018-07-03", testCaseDescription = "Verify The validation of Category At Admin Training Course Management Page", testCaseId = "TC_plan_certificate")
	private void validateDashboard3() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("training", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			fc.utobj().printTestStep("Entering the credential for validate user");
			driver = fc.loginpage().login(driver);
			String parentWindowHandle = driver.getWindowHandle();
			fc.utobj().printTestStep("Navigating to Admin > Training > Course Management Page");
			AdminTrainingCourseManagementAddSectionrPage pobj = new AdminTrainingCourseManagementAddSectionrPage(
					driver);
			AdminTrainingPlanAndCertificatePage pobjpnc = new AdminTrainingPlanAndCertificatePage(driver);
			AdminTrainingCoursesManagementPageTest courseManagement = new AdminTrainingCoursesManagementPageTest();
			fc.utobj().printTestStep("Add Category");
			String categoryname = courseManagement.addCategory(driver, dataSet);
			fc.utobj().printTestStep("Add Course");
			String coursename = courseManagement.addCourse(driver, dataSet, categoryname, testCaseId);
			fc.utobj().printTestStep("Create Lesson");
			fc.utobj().clickElement(driver, pobj.addLesson);
			
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			String sectionTitle = fc.utobj().generateTestData(dataSet.get("sectionTitle"));
			fc.utobj().sendKeys(driver, pobj.sectionTitle, sectionTitle);
			String summary = fc.utobj().generateTestData(dataSet.get("summary"));
			fc.utobj().sendKeys(driver, pobj.summary, summary);
			fc.utobj().clickElement(driver, pobj.addLesson);
			
			driver.switchTo().defaultContent();
			fc.utobj().clickElement(driver, pobj.uploadFile);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			String fileName = fc.utobj().getFilePathFromTestData("taskFile.pdf");
			fc.utobj().sendKeys(driver, pobj.fileuploader, fileName);
			
			AdminTrainingCourseManagementAddSectionPageTest objadd = new AdminTrainingCourseManagementAddSectionPageTest();
			objadd.addQuizBeforeCreatingLesson(driver, sectionTitle, dataSet, config, testCaseId);
			AdminTrainingPlanAndCertificateTest t1= new AdminTrainingPlanAndCertificateTest();
			
			t1.publishCourse(categoryname, coursename, driver, config, testCaseId);
			AdminTrainingPlanAndCertificateTest tb= new AdminTrainingPlanAndCertificateTest();
			String Certificate = t1.CreateCertificate_indieMethod(driver, config, dataSet, testCaseId, pobjpnc);
			String planName = tb.CreatePlanwithCourseAndCertiicate(driver, config, dataSet, testCaseId, pobjpnc,
					coursename, Certificate);
			tb.publishplan(driver, config, dataSet, testCaseId, pobjpnc, coursename, planName);
			fc.training().training_common().trainingPlans(driver);
			fc.utobj().clickElement(driver, pobjpnc.availablePlan);
			fc.utobj().clickElement(driver, pobjpnc.FilterBtn);
			fc.utobj().sendKeys(driver, pobjpnc.planName, planName);
			fc.utobj().clickElement(driver, pobjpnc.applyFilters);
			fc.utobj().clickElement(driver, pobjpnc.subscribe);
			fc.utobj().clickElement(driver, pobjpnc.start);
			fc.utobj().clickElement(driver, pobjpnc.startLearningN);
			System.out.println();
			Set<String> allWindowHandles = driver.getWindowHandles();
			System.out.println(allWindowHandles.size());
			for (String currentWindowHandle : allWindowHandles) {
				if (!currentWindowHandle.equals(parentWindowHandle)) {
					driver.switchTo().window(currentWindowHandle);

					try {
						fc.utobj().getElementByXpath(driver, ".//p[contains(text () , '" + sectionTitle + "')]")
								.isDisplayed();
						fc.utobj().moveToElement(driver, fc.utobj().getElement(driver, pobjpnc.closequiz));
						fc.utobj().clickElement(driver, pobjpnc.closequiz);
					} catch (Exception e) {
						System.out.println("we reached here");
					}
					driver.switchTo().window(parentWindowHandle);
				}
			}
			fc.training().training_common().trainingPlans(driver);
			fc.utobj().clickElement(driver, pobjpnc.FilterBtn);
			fc.utobj().sendKeys(driver, pobjpnc.planName, planName);
			fc.utobj().clickElement(driver, pobjpnc.applyFilters);
			fc.utobj().clickElement(driver, pobjpnc.resume);
			fc.utobj().clickElement(driver, pobjpnc.resume);
			fc.utobj().clickElement(driver, pobjpnc.startLearningN);
			Set<String> allWindowHandles1 = driver.getWindowHandles();
			System.out.println(allWindowHandles.size());
			for (String currentWindowHandle : allWindowHandles1) {
				if (!currentWindowHandle.equals(parentWindowHandle)) {
					driver.switchTo().window(currentWindowHandle);

					try {
						fc.utobj().getElementByXpath(driver, ".//p[contains(text () , '" + sectionTitle + "')]")
								.isDisplayed();
						fc.utobj().moveToElement(driver, fc.utobj().getElement(driver, pobjpnc.startquiz));
						fc.utobj().clickElement(driver, pobjpnc.startquiz);
					} catch (Exception e) {
						System.out.println("we reached here");
					}
					driver.switchTo().window(parentWindowHandle);
				}
			}
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			fc.utobj().clickElement(driver, pobjpnc.startquiz);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, pobjpnc.insertDate);
			DateFormat dateFormat2 = new SimpleDateFormat("dd");
			Date date2 = new Date();
			String today = dateFormat2.format(date2);
			WebElement dateWidget = fc.utobj().getElementByID(driver, "fc-datepicker-div");
			List<WebElement> columns = dateWidget.findElements(By.tagName("a"));
			for (WebElement cell : columns) {
				if (cell.getText().equals(today)) {
					cell.click();
					break;
				}
			}
			fc.utobj().clickElement(driver, pobjpnc.finishQuiz);
			fc.utobj().clickElement(driver, pobjpnc.SubmitQuiz);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			fc.utobj().clickElement(driver, pobjpnc.Close);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

}
