package com.builds.test.training;

import java.util.Map;

import org.openqa.selenium.WebDriver;

import com.builds.test.admin.AdminUsersRolesAddNewRolePageTest;
import com.builds.utilities.FranconnectUtil;

public class Plan {
	FranconnectUtil fc = new FranconnectUtil();
	private String CategoryCorporateRole = "Select All";
	private String CategoryRegionalRole = "Select All";
	private String CategoryFranchiseRole = "Select All";
	private String CategoryDivisionRole = "Select All";
	private AdminUsersRolesAddNewRolePageTest role = new AdminUsersRolesAddNewRolePageTest();
	
	public String getCategoryCorporateRole() {
		return CategoryCorporateRole;
	}

	public void setCategoryCorporateRole(WebDriver driver, Map<String, String> config) throws Exception {
		CategoryCorporateRole = "Corp" + fc.utobj().generateRandomNumber();
		role.addCorporateRoles(driver, CategoryCorporateRole);

	}

	public String getCategoryRegionalRole() {
		return CategoryRegionalRole;
	}

	public void setCategoryRegionalRole(WebDriver driver, Map<String, String> config) throws Exception {
		CategoryRegionalRole = "Reg" + fc.utobj().generateRandomNumber();
		role.addRegionalRoleWithOnlyTrainingModule(driver, config, CategoryRegionalRole);
	}

	public String getCategoryFranchiseRole() {
		return CategoryFranchiseRole;
	}

	public void setCategoryFranchiseRole(WebDriver driver, Map<String, String> config) throws Exception {
		CategoryFranchiseRole = "Fran" + fc.utobj().generateRandomNumber();
		role.addFranchiselRoleWithOnlyTrainingModule(driver, config, CategoryFranchiseRole);
	}

	public String getCategoryDivisionRole() {
		return CategoryDivisionRole;
	}

	public void setCategoryDivisionRole(WebDriver driver, Map<String, String> config) throws Exception {
		CategoryDivisionRole = "Div" + fc.utobj().generateRandomNumber();
		role.addDivisionalRoleWithOnlyTrainingModule(driver, config, CategoryDivisionRole);
	}

}
