package com.builds.test.training;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.builds.uimaps.training.AdminTrainingQuestionLibraryPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class QuizAdminTraining extends FranconnectUtil {

	@Test(groups = { "training","TrainingNewOne"})
	@TestCase(createdOn = "2018-01-03", updatedOn = "2017-01*03", testCaseDescription = "All added questions of the question library should be displayed on Question Library page.", testCaseId = "TC-9572")
	private void validateLesson() throws Exception {
		String testCaseId = utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = commonMethods().browsers().openBrowser();
		try {
			driver = loginpage().login(driver);
			Training T = new Training();
			Category c = new Category();
			c.setCategoryDesc(utobj().generateTestData("descsription"));
			c.setCategoryName(utobj().generateTestData("categoryname"));
			c.setUploadCoverImage(utobj().getFilePathFromTestData("logo.jpg"));
			c.setRolebasedAccessCategory(false);
			c = T.AddCategory(driver, config, c);
			if (c == null) {
				utobj().throwsException("Category not get added kindly check screen shot");
			}
			Course course = new Course();
			course.setCourseobjective(utobj().generateTestData("courseobjective"));
			course.setInstrutor("FranConnect Administrator");
			course.setCourseinstruction("This is the sameple instruction");
			course.setAllowrolebasedaccess(c.isRolebasedAccessCategory());
			course.setCoursename(utobj().generateTestData("CourseName"));
			course = c.addCouse(driver, config, course);
			Lesson lesson = new Lesson();
			lesson = course.addLessonModifyQuiz(driver, c, lesson);
			utobj().switchFrameToDefault(driver);
//			VerifyQuestionInQuestionLibrary(driver, "Which date is today ??");
			utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	@Test(groups = { "training","TrainingNewOne"})
	@TestCase(createdOn = "2018-01-03", updatedOn = "2017-01*03", testCaseDescription = "All added questions of the question library should be displayed on Question Library page.", testCaseId = "ADD_ALL_TYPE_QUESTION")
	private void addLesson_QuizWithAllTypeOfQuestion() throws Exception {
		String testCaseId = utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = commonMethods().browsers().openBrowser();
		try {
			driver = loginpage().login(driver);
			Training T = new Training();
			Category c = new Category();
			c.setCategoryDesc(utobj().generateTestData("descsription"));
			c.setCategoryName(utobj().generateTestData("categoryname"));
			c.setUploadCoverImage(utobj().getFilePathFromTestData("logo.jpg"));
			c.setRolebasedAccessCategory(false);
			c = T.AddCategory(driver, config, c);
			if (c == null) {
				utobj().throwsException("Category not get added kindly check screen shot");
			}
			Course course = new Course();
			course.setCourseobjective(utobj().generateTestData("courseobjective"));
			course.setInstrutor("FranConnect Administrator");
			course.setCourseinstruction("This is the sameple instruction");
			course.setAllowrolebasedaccess(c.isRolebasedAccessCategory());
			course.setCoursename(utobj().generateTestData("CourseName"));
			course = c.addCouse(driver, config, course);
			Lesson lesson = new Lesson();
			lesson = course.addLesson_QuizWithAllTypeOfQuestion(driver, c, lesson);
			utobj().switchFrameToDefault(driver);
			VerifyQuestionInQuestionLibrary(driver, "Which date is today ??");
			
		} catch (Exception e) {
			
			utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	@Test(groups = { "training","TrainingCourse123" })
	@TestCase(createdOn = "2018-01-03", updatedOn = "2017-01*03", testCaseDescription = "Verify the quiz functionality in its preview", testCaseId = "Tc_Verify_quiz_functionality_preview")
	private void validateQuizFunctionalityPreview() throws Exception {
		String testCaseId = utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = commonMethods().browsers().openBrowser();
		try {
			driver = loginpage().login(driver);
			Training T = new Training();
			Category c = new Category();
			c.setCategoryDesc(utobj().generateTestData("descsription"));
			c.setCategoryName(utobj().generateTestData("categoryname"));
			c.setUploadCoverImage(utobj().getFilePathFromTestData("logo.jpg"));
			c.setRolebasedAccessCategory(false);
			c = T.AddCategory(driver, config, c);
			if (c == null) {
				utobj().throwsException("Category not get added kindly check screen shot");
			}
			Course course = new Course();
			course.setCourseobjective(utobj().generateTestData("courseobjective"));
			course.setInstrutor("FranConnect Administrator");
			course.setCourseinstruction("This is the sameple instruction");
			course.setAllowrolebasedaccess(c.isRolebasedAccessCategory());
			course.setCoursename(utobj().generateTestData("CourseName"));
			course = c.addCouse(driver, config, course);
			Lesson lesson = new Lesson();
			lesson = course.addLessonDeletequiz(driver, c, lesson);
			
			utobj().switchFrameToDefault(driver);
			utobj().clickElement(driver, ".//a[.='"+lesson.getLessonName()+"']/ancestor::div[@class='grid-cell']/following-sibling::div//button");
			utobj().logoutAndQuitBrowser(driver, testCaseId);
			
		} catch (Exception e) {
			
			utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	private void VerifyQuestionInQuestionLibrary(WebDriver driver, String string) throws Exception {
		training().training_common().adminTraingQuestionLibraryPage(driver);
		AdminTrainingQuestionLibraryPage pobj = new AdminTrainingQuestionLibraryPage(driver);

		utobj().printTestStep("Add Question");
		utobj().clickElement(driver, pobj.viewperpage);

		utobj().clickElement(driver, ".//a[contains(text(),'500')]");
		boolean check = utobj().assertPageSource(driver, string);
		if (!check) {
			utobj().throwsException("question not found");
		}

	}

}
