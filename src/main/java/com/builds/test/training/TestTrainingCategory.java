package com.builds.test.training;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminDivisionAddDivisionPageTest;
import com.builds.test.admin.AdminFranchiseLocationAddFranchiseLocationPageTest;
import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.admin.AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest;
import com.builds.test.admin.AdminUsersManageManageFranchiseUsersPageTest;
import com.builds.test.admin.AdminUsersManageRegionalUsersAddRegionalUserPageTest;
import com.builds.test.admin.AdminUsersRolesAddNewRolePageTest;
import com.builds.test.admin.AdminUsersRolesPageTest;
import com.builds.test.admin.Roles;
import com.builds.test.common.CorporateUser;
import com.builds.uimaps.training.AdminTrainingCoureseManagementPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class TestTrainingCategory extends FranconnectUtil {

	FranconnectUtil fc=new FranconnectUtil();
	
	@Test(groups = { "training", "TrainingNewPattern" ,"trainingFailed0925" })
	@TestCase(createdOn = "2018-01-03", updatedOn = "2017-01*03", testCaseDescription = "Verify The validation of Category With Large amount of data", testCaseId = "TC_09_Add_Category")
	private void NormalDeselectionCategory() throws Exception {
		String testCaseId = utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = commonMethods().browsers().openBrowser();
		try {
			driver = loginpage().login(driver);
			Category c = new Category();
			c.setCategoryDesc(utobj().generateTestData(
					"this is the large amount of data providing for category Description as description"));
			c.setCategoryName(utobj()
					.generateTestData("this is the large amount of data providing for category name as categoryname"));
			c.setUploadCoverImage(utobj().getFilePathFromTestData("logo.jpg"));
			c.setRolebasedAccessCategory(true);
			c.setCategoryCorporateRole(driver, config);
			c.setCategoryFranchiseRole(driver, config);
			c.setCategoryDivisionRole(driver, config);
			c.setCategoryRegionalRole(driver, config);
			AdminTrainingCoureseManagementPage pobj = new AdminTrainingCoureseManagementPage(driver);
			training().training_common().adminTraingCourseManagementPage(driver);
			utobj().clickElement(driver, pobj.manageCategory);
			utobj().clickElement(driver, pobj.addCategory);
			Thread.sleep(1000);
			commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			String fileName = utobj().getFilePathFromTestData("logo.jpeg");
			try {
				utobj().sendKeys(driver, pobj.uploadcoverimage, fileName);
			} catch (Exception e) {
				pobj.uploadcoverimage.sendKeys(fileName);
			}
			utobj().sendKeys(driver, pobj.categoryName, c.getCategoryName());
			utobj().sendKeys(driver, pobj.categoryDescription, c.getCategoryDesc());
			if (c.isRolebasedAccessCategory()) {
				utobj().clickElement(driver, pobj.allowRoleBasedAccess);
				boolean invalidrole = false;

				if (!invalidrole) {
					try {
						utobj().clickElement(driver,
								utobj().getElementByXpath(driver, ".//*[@id='fc-drop-parentcorRoles']/button"));
						utobj().clickElement(driver,
								".//*[@id='fc-drop-parentcorRoles']/div/ul//span[contains(text(), '"
										+ c.getCategoryCorporateRole() + "')]");
						Thread.sleep(1000);
						utobj().clickElement(driver,
								".//*[@id='fc-drop-parentcorRoles']/div/ul//span[contains(text(), '"
										+ c.getCategoryCorporateRole() + "')]");
						Thread.sleep(1000);

					} catch (Exception e) {
						invalidrole = true;
						utobj().printTestStep("Searching for invalid corp role doesn't work");
					}

					utobj().sleep(1000);
				}
				if (!invalidrole) {

					try {
						utobj().clickElement(driver,
								utobj().getElementByXpath(driver, ".//*[@id='fc-drop-parentregionalRoles']/button"));
						utobj().clickElement(driver,
								".//*[@id='fc-drop-parentregionalRoles']/div/ul//span[contains(text(), '"
										+ c.getCategoryRegionalRole() + "')]");
						utobj().sleep(1000);
						utobj().clickElement(driver,
								".//*[@id='fc-drop-parentregionalRoles']/div/ul//span[contains(text(), '"
										+ c.getCategoryRegionalRole() + "')]");
						utobj().sleep(1000);

					} catch (Exception e) {
						invalidrole = true;
						utobj().printTestStep("Searching for invalid Regional role doesn't work");

					}
					utobj().sleep(1000);
				}

				if (!invalidrole) {

					try {
						utobj().clickElement(driver,
								utobj().getElementByXpath(driver, ".//*[@id='fc-drop-parentfranchiseeRoles']/button"));
						utobj().clickElement(driver,
								".//*[@id='fc-drop-parentfranchiseeRoles']/div/ul//span[contains(text(), '"
										+ c.getCategoryFranchiseRole() + "')]");
						utobj().sleep(1000);
						utobj().clickElement(driver,
								".//*[@id='fc-drop-parentfranchiseeRoles']/div/ul//span[contains(text(), '"
										+ c.getCategoryFranchiseRole() + "')]");

					} catch (Exception e) {
						invalidrole = true;
						utobj().printTestStep("Searching for invalid role doesn't work");
					}

					utobj().sleep(1000);
				}
				if (!invalidrole) {
					try {
						utobj().clickElement(driver,
								utobj().getElementByXpath(driver, ".//*[@id='fc-drop-parentdivisionalRoles']/button"));
						utobj().clickElement(driver,
								".//*[@id='fc-drop-parentdivisionalRoles']/div/ul//span[contains(text(), '"
										+ c.getCategoryDivisionRole() + "')]");
						utobj().sleep(1000);
						utobj().clickElement(driver,
								".//*[@id='fc-drop-parentdivisionalRoles']/div/ul//span[contains(text(), '"
										+ c.getCategoryDivisionRole() + "')]");
						utobj().sleep(1000);

					} catch (Exception e) {
						invalidrole = true;
						utobj().printTestStep("Searching for invalid role doesn't work");
					}
					utobj().sleep(1000);
				}

				if (invalidrole) {
					throw new Exception("invalid role got selected");
				}
			}
			utobj().clickElement(driver, pobj.allowRoleBasedAccess);
			utobj().clickElement(driver, pobj.addCategoryButton);
			utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "training", "TrainingNewPattern" })
	@TestCase(createdOn = "2018-01-03", updatedOn = "2017-01*03", testCaseDescription = "Verify The validation of Category with special character", testCaseId = "TC_06_Add_Category")
	private void TestCategory() throws Exception {
		String testCaseId = utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = commonMethods().browsers().openBrowser();
		try {
			driver = loginpage().login(driver);
			Training T = new Training();
			Category c = new Category();
			c.setCategoryDesc(utobj().generateTestData("descsription"));
			c.setCategoryName(utobj().generateTestData("categoryname"));
			c.setUploadCoverImage(utobj().getFilePathFromTestData("logo.jpg"));
			c.setRolebasedAccessCategory(false);
			training().training_common().adminTraingCourseManagementPage(driver);
			T.AddCategory(driver, config, c);
			T.verifyCategory(driver, c);
			T.DeleteCategory(driver, c);
			c.setCategoryName("!@#$%");
			c = T.AddCategory(driver, config, c);
			if (c != null) {
				utobj().throwsException("Category add with special Character");
			}

			utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "training"})
	@TestCase(createdOn = "2018-01-03", updatedOn = "2017-01*03", testCaseDescription = "Verify The validation of Category At Admin Training Course Management Page", testCaseId = "TC_07_Add_Category")
	private void TestUserWithCategory() throws Exception {
		String testCaseId = utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = commonMethods().browsers().openBrowser();
		try {
			driver = loginpage().login(driver);
			Training T = new Training();
			Category c = new Category();
			c.setCategoryDesc(utobj().generateTestData("descsription"));
			c.setCategoryName(utobj().generateTestData("categoryname"));
			c.setUploadCoverImage(utobj().getFilePathFromTestData("logo.jpg"));
			c.setRolebasedAccessCategory(true);
			
			c.setCategoryCorporateRole(driver, config);
			c.setCategoryFranchiseRole(driver, config);
			c.setCategoryDivisionRole(driver, config);
			c.setCategoryRegionalRole(driver, config);
			
			T.AddCategory(driver, config, c);
			T.verifyCategory(driver, c);

			driver = loginpage().login(driver);
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser = commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail("trainingautomation@staffex.com");
			corpUser.setRole(c.getCategoryCorporateRole());
			corpUser.setPassword("123456789");
			corpUser = corpTest.createDefaultUser(driver, corpUser);
			T.verifyCategorywithRoleUser(driver, c, corpUser.getUserName(), "123456789");

			/*driver = loginpage().login(driver);
			AdminUsersManageRegionalUsersAddRegionalUserPageTest Rgoal = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
			String Region = utobj().generateTestData("Region");
			String RegionalUser = utobj().generateTestData("RegionalUser");
			Rgoal.addRegionalUserWithRoleState(driver, RegionalUser, "123456789", Region, c.getCategoryRegionalRole(),
					config, "trainingautomation@staffex.com", "Angus");
			T.verifyCategorywithRoleUser(driver, c, RegionalUser, "123456789");

			driver = loginpage().login(driver);
			String FranchiseUser = utobj().generateTestData("FranchiseUser");
			AdminUsersManageManageFranchiseUsersPageTest addFranUser = new AdminUsersManageManageFranchiseUsersPageTest();
			utobj().printTestStep("Add Franchise Location");
			String franchiseId = utobj().generateTestData(dataSet.get("franchiseId"));
			String regionName = utobj().generateTestData(dataSet.get("regionName"));
			String firstName = utobj().generateTestData(dataSet.get("firstName"));
			String lastName = utobj().generateTestData(dataSet.get("lastName"));
			AdminFranchiseLocationAddFranchiseLocationPageTest franchiseLocationPage = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseLocationPage.addFranchiseLocationForMkt(driver, franchiseId, regionName, firstName, lastName);
			addFranUser.addFranchiseUser(driver, FranchiseUser, "123456789", franchiseId, c.getCategoryFranchiseRole(),
					"trainingautomation@staffex.com");
			T.verifyCategorywithRoleUser(driver, c, FranchiseUser, "123456789");

			driver = loginpage().login(driver);
			String DivisionlUser = utobj().generateTestData("DivisionlUser");
			AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest addDivUser = new AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest();
			
			String divisionName=utobj().generateTestData("Tudivision");
			utobj().printTestStep("Add Division");
			AdminDivisionAddDivisionPageTest p1 = new AdminDivisionAddDivisionPageTest();
			p1.addDivision(driver, divisionName);
			
			addDivUser.addDivisionalUser(driver, DivisionlUser, "123456789", divisionName,
					"trainingautomation@staffex.com");
			T.verifyCategorywithRoleUser(driver, c, DivisionlUser, "123456789");
			
			c.setCategoryCorporateRole(driver, config);
			driver = loginpage().login(driver);
			CorporateUser = utobj().generateTestData("CorporateUser");
			corpTest = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			CorporateUser corpUser2 = new CorporateUser();
			corpUser2 = commonMethods().fillDefaultDataFor_CorporateUser(corpUser2);
			corpUser2.setEmail("trainingautomation@staffex.com");
			corpUser2 = corpTest.createDefaultUser(driver, corpUser2);
			T.verifyCategorywithRoleUser(driver, c, CorporateUser, corpUser2.getPassword());*/

			utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "training", "TrainingNewPattern" })
	@TestCase(createdOn = "2018-01-03", updatedOn = "2017-01*03", testCaseDescription = "Verify The validation of Category At Admin Training Course Management Page", testCaseId = "TC_03_Add_Category")
	private void NormalCategory() throws Exception {
		String testCaseId = utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = commonMethods().browsers().openBrowser();
		try {
			driver = loginpage().login(driver);
			Training T = new Training();
			Category c = new Category();
			c.setCategoryDesc(utobj().generateTestData(
					"this is the large amount of data providing for category Description as description"));
			c.setCategoryName(utobj()
					.generateTestData("this is the large amount of data providing for category name as categoryname"));
			c.setUploadCoverImage(utobj().getFilePathFromTestData("logo.jpg"));
			c.setRolebasedAccessCategory(true);
			c.setCategoryCorporateRole(driver, config);
			c.setCategoryFranchiseRole(driver, config);
			c.setCategoryDivisionRole(driver, config);
			c.setCategoryRegionalRole(driver, config);
			training().training_common().adminTraingCourseManagementPage(driver);
			T.AddCategory(driver, config, c);
			T.verifyCategory(driver, c);
			c = T.AddCategory(driver, config, c);
			if (c != null) {
				utobj().throwsException("Duplicate Category got added");
			}
			utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "training", "TrainingNewPattern" })
	@TestCase(createdOn = "2018-01-03", updatedOn = "2017-01*03", testCaseDescription = "Verify The validation of Category At Admin Training Course Management Page", testCaseId = "TC_08_Add_Category")
	private void NormalCategoryLongTextnoSpace() throws Exception {
		String testCaseId = utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = commonMethods().browsers().openBrowser();
		try {
			driver = loginpage().login(driver);
			Training T = new Training();
			Category c = new Category();
			c.setCategoryDesc(utobj().generateTestData(
					"AllegoryofVanity,avanitascompletedbyAntoniCandaules,KingofLydia,ShewshisWifebyStealthtoGyges,OneofhisMinisters"
							+ ",asSheGoestoBedisapaintingbyEnglishartistWilliamEtty,firstexhibitedin1830.ItshowsascenefromtheHistoriesbyHerodotus,inwhichCandaules,kingofLydia,"
							+ "inviteshisbodyguardGygestohideinthecouple'sbedroomandwatchhiswifeNyssiaundress.AfterNyssianoticesGyges,hekillsCandaulesandtakeshisplaceasking.Thepa"
							+ "intingshowsthemomentatwhichNyssia,unawareofGyges,removesthelastofherclothes.Ettyhopedtoimpartthemoralthatwomenarenotchattelsandthatmenviolatingtheir"
							+ "rightsshouldbepunished,buthemadelittleefforttoexplainthistoaudiences.Thepaintingwasimmediatelycontroversial,seenasacynicalcombinationofpornographyand"
							+ "aviolentunpleasantnarrative,andcriticscondemneditasanimmoralworkofthetypetheywouldnotexpectfromaBritishartist.In1929itwasamongseveralartworkstransfer"
							+ "redtothenewlyexpandedTateGallery,whereitremainsodePeredabetween1632and1636.Worksinthiscategoryofsymbolicart,especiallyassociatedwithstilllifepainting"
							+ "sof16th-and17th-centuryFlandersandtheNetherlands,refertothetraditionalChristianviewofearthlylifeandtheworthlessnatureofallearthlygoodsandpursuits.TheL"
							+ "atinnounvanĭtāsmeans\"emptiness\"andderivesitsprominencefromEcclesiastes.Commonsymbolsinvanitasincludeskulls,rottenfruit;bubbles;smoke,watches,hourgla"
							+ "sses,andmusicalinstruments."));
			c.setCategoryName(utobj().generateTestData(
					"AllegoryofVanity,avanitascompletedbyAntoniodePeredabetween1632and1636.Worksinthiscategoryofsymbolicart,especiallyassociatedwithstilllifepaintingsof"
							+ "16th-and17th-centuryFlandersandtheNetherlands,refertothetraditionalChristianviewofearthlylifeandtheworthlessnatureofallearthlygoodsandpursuits.The"
							+ "Latinnounvanĭtāsmeans\"emptiness\"andderivesitsprominencefromEcclesiastes.Commonsymbolsinvanitasincludeskulls,rottenfruit;bubbles;smoke,watches,hour"
							+ "lasses,andmusicalinstruments."));
			c.setUploadCoverImage(utobj().getFilePathFromTestData("logo.jpg"));
			c.setRolebasedAccessCategory(true);
			c.setCategoryCorporateRole(driver, config);
			c.setCategoryFranchiseRole(driver, config);
			c.setCategoryDivisionRole(driver, config);
			c.setCategoryRegionalRole(driver, config);
			training().training_common().adminTraingCourseManagementPage(driver);
			T.AddCategory(driver, config, c);
			T.verifyCategory(driver, c);
			c = T.AddCategory(driver, config, c);
			if (c != null) {
				utobj().throwsException("Duplicate Category got added");
			}
			utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "training", "TrainingNewPattern" })
	@TestCase(createdOn = "2018-01-03", updatedOn = "2017-01*03", testCaseDescription = "Verify The validation of Category At Admin Training Course Management Page", testCaseId = "TC_01_Modifying_Category")
	private void ModifyCategory() throws Exception {
		String testCaseId = utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = commonMethods().browsers().openBrowser();
		try {
			driver = loginpage().login(driver);
			Training T = new Training();
			Category c = new Category();
			c.setCategoryDesc(utobj().generateTestData(
					"this is the large amount of data providing for category Description as description"));
			c.setCategoryName(utobj()
					.generateTestData("this is the large amount of data providing for category name as categoryname"));
			c.setUploadCoverImage(utobj().getFilePathFromTestData("logo.jpg"));
			c.setRolebasedAccessCategory(false);
			// c.setCategoryCorporateRole(driver, config);
			// c.setCategoryFranchiseRole(driver, config);
			// c.setCategoryDivisionRole(driver, config);
			// c.setCategoryRegionalRole(driver, config);
			training().training_common().adminTraingCourseManagementPage(driver);
			T.AddCategory(driver, config, c);
			String categoryname = null;
			if (c != null)
				categoryname = c.getCategoryName();
			c.setCategoryName(utobj().generateTestData("Modified_Category"));
			T.ModifyCategory(driver, config, c, categoryname);
			T.verifyCategory(driver, c);
			utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "training", "TrainingNewPattern" })
	@TestCase(createdOn = "2018-01-03", updatedOn = "2017-01*03", testCaseDescription = "Verify The validation of Category At Admin Training Course Management Page", testCaseId = "TC_06_Add_Category")
	private void SelectAllROleCategory() throws Exception {
		String testCaseId = utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = commonMethods().browsers().openBrowser();
		try {
			driver = loginpage().login(driver);
			Training T = new Training();
			Category c = new Category();
			c.setCategoryDesc(utobj().generateTestData("descsription"));
			c.setCategoryName(utobj().generateTestData("categoryname"));
			c.setUploadCoverImage(utobj().getFilePathFromTestData("logo.jpg"));
			c.setRolebasedAccessCategory(true);
			training().training_common().adminTraingCourseManagementPage(driver);
			T.AddCategory(driver, config, c);
			T.verifyCategory(driver, c);
			utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "training", "TrainingNewPattern" })
	@TestCase(createdOn = "2018-01-03", updatedOn = "2017-01*03", testCaseDescription = "Verify The validation of Category At Admin Training Course Management Page", testCaseId = "TC_04_Add_Category")
	private void viaEnterCategory() throws Exception {
		String testCaseId = utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = commonMethods().browsers().openBrowser();
		try {
			driver = loginpage().login(driver);
			Category c = new Category();
			c.setCategoryName("");
			c.setCategoryDesc("");
			training().training_common().adminTraingCourseManagementPage(driver);
			AdminTrainingCoureseManagementPage pobj = new AdminTrainingCoureseManagementPage(driver);
			training().training_common().adminTraingCourseManagementPage(driver);
			utobj().clickElement(driver, pobj.manageCategory);
			Exception verify = utobj().assertPageSource(driver, "Manage Courses") ? null
					: utobj().throwsException1("Manage Courses link not found");
			utobj().clickElement(driver, pobj.addCategory);
			commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			Robot r = new Robot();
			r.keyPress(KeyEvent.VK_ENTER);
			r.keyRelease(KeyEvent.VK_ENTER);
			utobj().clickElement(driver, ".//*[contains(text(),'Close')]");
			Thread.sleep(5000);
			utobj().clickElement(driver, pobj.addCategory);
			commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			c.setCategoryName(" ");
			c.setCategoryDesc(" ");
			utobj().sendKeys(driver, pobj.categoryName, c.getCategoryName());
			utobj().sendKeys(driver, pobj.categoryDescription, c.getCategoryDesc());
			r = new Robot();
			r.keyPress(KeyEvent.VK_ENTER);
			r.keyRelease(KeyEvent.VK_ENTER);
			utobj().assertPageSource(driver, "This field is required");
			utobj().clickElement(driver, ".//*[contains(text(),'Close')]");

			utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "training", "TrainingNewPattern" })
	@TestCase(createdOn = "2018-01-03", updatedOn = "2017-01*03", testCaseDescription = "Verify The validation of Category At Admin Training Course Management Page", testCaseId = "TC_05_Add_Category")
	private void blankCategory() throws Exception {
		String testCaseId = utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = utobj().readTestData("training", testCaseId);
		WebDriver driver = commonMethods().browsers().openBrowser();
		try {
			driver = loginpage().login(driver);
			Training T = new Training();
			Category c = new Category();
			c.setCategoryName("");
			c.setCategoryDesc("");
			try {
				c = T.AddCategory(driver, config, c);
				if (c == null) {
					utobj().throwsException("Category not added");
				}
				T.verifyCategory(driver, c);
			} catch (Exception e) {

				try {
					c = new Category();
					c.setCategoryDesc(utobj().generateTestData("descsription"));
					c = T.AddCategory(driver, config, c);
					if (c == null) {
						utobj().throwsException("Category not added");
					}
					T.verifyCategory(driver, c);
				} catch (Exception ex) {
					try {
						c = new Category();
						c.setCategoryDesc("");
						c.setCategoryName(utobj().generateTestData("categoryname"));
						c = T.AddCategory(driver, config, c);
						if (c != null) {
							utobj().throwsException("Category not added");
						}
						T.verifyCategory(driver, c);
					} catch (Exception exa) {
						try {
							c = new Category();
							c.setCategoryDesc("");
							c.setCategoryName("!@#$$");
							c = T.AddCategory(driver, config, c);
							if (c != null) {
								utobj().throwsException("Category not added");
							}
							T.verifyCategory(driver, c);
						} catch (Exception exacs) {
							c = new Category();
							c.setCategoryDesc("  ");
							c.setCategoryName("  ");
							c = T.AddCategory(driver, config, c);
							if (c != null) {
								utobj().throwsException("Category not added");
							}
							T.verifyCategory(driver, c);
						}

					}
				}
			}
			if (c != null) {
				utobj().throwsException("category added with blank field");
			}
			utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

}
