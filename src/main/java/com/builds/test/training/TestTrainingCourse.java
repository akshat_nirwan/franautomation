package com.builds.test.training;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.admin.AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest;
import com.builds.test.admin.AdminUsersManageManageFranchiseUsersPageTest;
import com.builds.test.admin.AdminUsersManageRegionalUsersAddRegionalUserPageTest;
import com.builds.test.common.CorporateUser;
import com.builds.uimaps.training.AdminTrainingCoureseManagementPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class TestTrainingCourse extends FranconnectUtil {

	@Test(groups = { "training", "TrainingNewPattern", "TrainingCourse" })
	@TestCase(createdOn = "2018-01-03", updatedOn = "2017-01*03", testCaseDescription = "Verify The validation of course details on lesson summary page", testCaseId = "TC_05_Add_Course_new")
	private void validateCourse() throws Exception {
		String testCaseId = utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = commonMethods().browsers().openBrowser();
		try {
			driver = loginpage().login(driver);
			Training T = new Training();
			Category c = new Category();
			c.setCategoryDesc(utobj().generateTestData("descsription"));
			c.setCategoryName(utobj().generateTestData("categoryname"));
			boolean check = false;
			c.setUploadCoverImage(utobj().getFilePathFromTestData("logo.jpg"));
			c.setRolebasedAccessCategory(false);
			// utobj().printTestStep("Creating the corporate role");
			// c.setCategoryCorporateRole(driver, config);
			// utobj().printTestStep("Creating the Franchise role");
			// c.setCategoryFranchiseRole(driver, config);
			// utobj().printTestStep("Creating the Regional role");
			// c.setCategoryRegionalRole(driver, config);
			// utobj().printTestStep("Creating the Division role");
			// c.setCategoryDivisionRole(driver, config);
			// utobj().printTestStep("Creating Category with Above role");
			c = T.AddCategory(driver, config, c);
			Course course = new Course();
			course.setCoursename(utobj().generateTestData("CourseName"));
			System.out.println(course.getCoursename());
			course.setCourseobjective(utobj().generateTestData("courseobjective"));
			course.setInstrutor("FranConnect Administrator");
			course.setCourseinstruction("This is the sameple instruction");
			course.setAllowrolebasedaccess(c.isRolebasedAccessCategory());
			utobj().printTestStep("Creating the course with above role");
			Course tempcourse = course = c.addCouse(driver, config, course);
			if (course == null) {
				utobj().throwsException("Delete option came while having course");
			}
			tempcourse.setCoursename(utobj().generateTestData("CourseName"));
			course = tempcourse;
			utobj().printTestStep("Creating the course with above role but with differenct name");
			course = c.addCouse(driver, config, course);
			if (course == null) {
				utobj().throwsException("Delete option came while having course");
			}
			utobj().printTestStep("Creating the course with above role but with different objective name");
			tempcourse.setCourseobjective(utobj().generateTestData("courseobjective"));
			course = tempcourse;
			course = c.addCouse(driver, config, course);
			if (course != null) {
				utobj().throwsException("Duplicate course got added");
			}
			course = tempcourse;
			course.setCourseinstruction("This is the sameple instruction");
			course.setAllowrolebasedaccess(c.isRolebasedAccessCategory());
			utobj().printTestStep("Creating the course with above role but with different instruction");
			course = c.addCouse(driver, config, course);
			if (course != null) {
				utobj().throwsException("Delete option came while having course");
			}
			course = tempcourse;
			course.setCoursename(utobj().generateTestData("new course name"));
			utobj().printTestStep("Creating the course with above role but with different name");
			course = c.addCouse(driver, config, course);
			if (course != null) {
				utobj().printTestStep("Verifying the above category and Unpublished course");
				T.verifyCategory(driver, c).verifyUnPublishedCourse(driver, config, course);
			}
			try {
				utobj().printTestStep("Try to delete the Category without deleting the course");
				T.DeleteCategory(driver, c);
				check = true;
			} catch (Exception ex) {

			}
			if (check) {
				utobj().throwsException("Delete option came while having course");
			}
			utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "training", "TrainingNewPattern" })
	@TestCase(createdOn = "2018-01-03", updatedOn = "2017-01*03", testCaseDescription = "Verify The validation of Category and course  At Admin Training Course Management Page with all user", testCaseId = "TC_06_Add_Course")
	private void validateCoursespecialchar() throws Exception {
		String testCaseId = utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = commonMethods().browsers().openBrowser();
		try {
			driver = loginpage().login(driver);
			Training T = new Training();
			Category c = new Category();
			c.setCategoryDesc(utobj().generateTestData("descsription"));
			c.setCategoryName(utobj().generateTestData("categoryname"));
			Course course = new Course();
			c.setUploadCoverImage(utobj().getFilePathFromTestData("logo.jpg"));
			c.setRolebasedAccessCategory(true);
			c.setCategoryCorporateRole(driver, config);
			c.setCategoryFranchiseRole(driver, config);
			c.setCategoryRegionalRole(driver, config);
			c.setCategoryDivisionRole(driver, config);
			training().training_common().adminTraingCourseManagementPage(driver);
			c = T.AddCategory(driver, config, c);
			course.setCoursename(utobj().generateTestData("Coursename"));
			course.setCourseobjective(utobj().generateTestData("courseobjective"));
			course.setInstrutor("FranConnect Administrator");
			course.setCourseinstruction("This is the sameple instruction");
			System.out.println("Hello");
			course.setAllowrolebasedaccess(true);
			Course tempcourse = new Course();
			tempcourse = course = c.addCouse(driver, config, course);
			course.setCoursename(utobj().generateTestData(
					"lose the file by calling open() andclose(), but if a bug in your programprevents theclose() statement from being executed, the file may neverclose. This may seem trivial, b"));
			course.setCourseobjective(utobj().generateTestData(
					"lose the file by calling open() andclose(), but if a bug in your programprevents theclose() statement from being executed, the file may neverclose. This may seem trivial, b"));
			course.setCourseinstruction(utobj().generateTestData(
					"lose the file by calling open() andclose(), but if a bug in your programprevents theclose() statement from being executed, the file may neverclose. This may seem trivial, b"));
			course = c.addCouse(driver, config, course);
			course = tempcourse;
			if (course != null)
				c.verifyUnPublishedCourse(driver, config, course);
			course.setCoursename(utobj().generateTestData("CourseName"));
			course = c.addCouse(driver, config, course);
			course = tempcourse;
			if (course != null)
				c.verifyUnPublishedCourse(driver, config, course);

			try{
				if(c.isRolebasedAccessCategory())
				{
				driver = loginpage().login(driver);
				String CorporateUser = utobj().generateTestData("CorporateUser");
				AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
				CorporateUser corpUser = new CorporateUser();

				corpUser = commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
				corpUser.setEmail("trainingautomation@staffex.com");
				corpUser.setRole(c.getCategoryCorporateRole());
				corpUser = corpTest.createDefaultUser(driver, corpUser);
				T.verifyCategorywithRoleUser(driver, c, corpUser.getUserName(), "123456789");
				c.verifyUnPublishedCourse(driver, config, course);
				driver = loginpage().login(driver);
				AdminUsersManageRegionalUsersAddRegionalUserPageTest Rgoal = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
				String Region = utobj().generateTestData("Region");
				String RegionalUser = utobj().generateTestData("RegionalUser");
				Rgoal.addRegionalUserWithRoleState(driver, RegionalUser, "123456789", Region,
						c.getCategoryRegionalRole(), config, "trainingautomation@staffex.com", "Angus");
//				T.verifyCategorywithRoleUser(driver, c, RegionalUser, "123456789");
//				c.verifyUnPublishedCourse(driver, config, course);
				training().training_common().trainingCourse(driver);
				utobj().assertPageSource(driver, course.getCoursename());
				driver = loginpage().login(driver);
				String FranchiseUser = utobj().generateTestData("FranchiseUser");
				AdminUsersManageManageFranchiseUsersPageTest addFranUser = new AdminUsersManageManageFranchiseUsersPageTest();
				addFranUser.addFranchiseUser(driver, FranchiseUser, "123456789", "110", c.getCategoryFranchiseRole(),
						"trainingautomation@staffex.com");
//				T.verifyCategorywithRoleUser(driver, c, FranchiseUser, "123456789");
//				c.verifyUnPublishedCourse(driver, config, course);
				training().training_common().trainingCourse(driver);
				utobj().assertPageSource(driver, course.getCoursename());
				driver = loginpage().login(driver);
				String DivisionlUser = utobj().generateTestData("DivisionlUser");
				AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest addDivUser = new AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest();
				String divisionName = "Division";
				addDivUser.addDivisionalUser(driver, DivisionlUser, "123456789", divisionName,
						"trainingautomation@staffex.com");
//				T.verifyCategorywithRoleUser(driver, c, DivisionlUser, "123456789");
//				c.verifyUnPublishedCourse(driver, config, course);
				training().training_common().trainingCourse(driver);
				utobj().assertPageSource(driver, course.getCoursename());
				c.setCategoryCorporateRole(driver, config);
				driver = loginpage().login(driver);
				CorporateUser = utobj().generateTestData("CorporateUser");
				corpTest = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

				CorporateUser corpUser2 = new CorporateUser();
				corpUser2 = commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
				corpUser2.setEmail("trainingautomation@staffex.com");
				corpUser2 = corpTest.createDefaultUser(driver, corpUser2);
				T.verifyCategorywithRoleUser(driver, c, CorporateUser, "123456789");
				if (c != null) {
					c.verifyUnPublishedCourse(driver, config, course);
				}
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
			utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "training", "TrainingCourse" })
	@TestCase(createdOn = "2018-01-03", updatedOn = "2017-01*03", testCaseDescription = "Verify The validation of Category At Admin Training Course Management Page", testCaseId = "TC_07_Add_Course")
	private void validateCourseSelectAll() throws Exception {
		String testCaseId = utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = commonMethods().browsers().openBrowser();
		try {
			driver = loginpage().login(driver);
			Training T = new Training();
			Category c = new Category();
			c.setCategoryDesc(utobj().generateTestData("descsription"));
			c.setCategoryName(utobj().generateTestData("categoryname"));
			Course course = new Course();
			c.setUploadCoverImage(utobj().getFilePathFromTestData("logo.jpg"));
			c.setRolebasedAccessCategory(false);
			c = T.AddCategory(driver, config, c);
			course.setCourseobjective(utobj().generateTestData("courseobjective"));
			course.setInstrutor("FranConnect Administrator");
			course.setCourseinstruction("This is the sameple instruction");
			course.setAllowrolebasedaccess(c.isRolebasedAccessCategory());

			course.setCoursename(utobj().generateTestData("CourseName"));
			course = c.addCouse(driver, config, course);
			if (course == null)
				utobj().throwsException("Course does not get added kindly check");
			course.VerifyActionButton(driver, config, c);
			course.VefityCopyandCustomizeTab(driver, config, c);
			c.verifyUnPublishedCourse(driver, config, course);

			utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "training", "TrainingCourse123" })
	@TestCase(createdOn = "2018-01-03", updatedOn = "2017-01*03", testCaseDescription = "Verify The validation of Category At Admin Training Course Management Page", testCaseId = "TC_08_Add_Course")
	private void validateCourseDeSelectAll() throws Exception {
		String testCaseId = utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = commonMethods().browsers().openBrowser();
		try {
			driver = loginpage().login(driver);
			Training T = new Training();
			Category c = new Category();
			c.setCategoryDesc(utobj().generateTestData("descsription"));
			c.setCategoryName(utobj().generateTestData("categoryname"));
			c.setUploadCoverImage(utobj().getFilePathFromTestData("logo.jpg"));
			c.setRolebasedAccessCategory(false);
			training().training_common().adminTraingCourseManagementPage(driver);
			c = T.AddCategory(driver, config, c);
			if (c == null) {
				utobj().throwsException("Category not get added kindly check screen shot");
			}
			Course course = new Course();
			course.setCourseobjective(utobj().generateTestData("courseobjective"));
			course.setInstrutor("FranConnect Administrator");
			course.setCourseinstruction("This is the sameple instruction");
			course.setAllowrolebasedaccess(c.isRolebasedAccessCategory());
			course.setCoursename(utobj().generateTestData("CourseName"));
			course = c.addCouse(driver, config, course);
			Lesson lesson = new Lesson();
			lesson = course.addLesson(driver, c, lesson);
			utobj().switchFrameToDefault(driver);
			AdminTrainingCoureseManagementPage pobj = new AdminTrainingCoureseManagementPage(driver);
			training().training_common().adminTraingCourseManagementPage(driver);
			utobj().clickElement(driver, pobj.GeneralCategory);
			utobj().sleep(3000);
			utobj().sendKeys(driver, utobj().getElementByXpath(driver, ".//*[@id='dialogV']//input"),
					c.getCategoryName());
			utobj().clickElement(driver, utobj().getElementByXpath(driver,
					(".//*[@id='mCSB_1_container']//a[contains(text (), '" + c.getCategoryName() + "')]")));
			utobj().sleep(500);

			utobj().clickElement(driver,
					utobj().getElementByXpath(driver, ".//*[@class=\"row\"]/div[1]//*[@data-role=\"ico_ThreeDots\"]"));
			utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[@class=\"row\"]/div[1]//*[@data-role=\"ico_ThreeDots\"]/parent::div/following-sibling::ul//*[contains(text(),'Change Courses Sequence')]");
			utobj().clickElement(driver, utobj().getElementByXpath(driver,
					".//*[@class=\"row\"]/div[1]//*[@data-role=\"ico_ThreeDots\"]/parent::div/following-sibling::ul//*[contains(text(),'Move Courses')]"));
			commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			utobj().sleep(3000);
			if (!utobj().verifyElementOnVisible_ByXpath(driver, ".//*[@id='txtList']"))
				utobj().throwsException("element not found ");
			else
				utobj().printTestStep("Text list verified");

			if (!utobj().verifyElementOnVisible_ByXpath(driver, ".//*[@placeholder=\"Search Category\"]"))
				utobj().throwsException("element not found ");
			else
				utobj().printTestStep("Text list verified");

			if (!utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'Close')]"))
				utobj().throwsException("element not found ");
			else
				utobj().printTestStep("Text list verified");

			if (!utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'Save')]"))
				utobj().throwsException("element not found ");
			else
				utobj().printTestStep("Text list verified");
			draganddropperform(driver);
			utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "training", "TrainingCourse" })
	@TestCase(createdOn = "2018-01-03", updatedOn = "2017-01-03", testCaseDescription = "Verify The validation of Category At Admin Training Course Management Page", testCaseId = "Tc_9546")
	private void validateLesson() throws Exception {
		String testCaseId = utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = commonMethods().browsers().openBrowser();
		try {
			driver = loginpage().login(driver);
			Training T = new Training();
			Category c = new Category();
			c.setCategoryDesc(utobj().generateTestData("descsription"));
			c.setCategoryName(utobj().generateTestData("categoryname"));
			c.setUploadCoverImage(utobj().getFilePathFromTestData("logo.jpg"));
			c.setRolebasedAccessCategory(false);
			training().training_common().adminTraingCourseManagementPage(driver);
			c = T.AddCategory(driver, config, c);
			if (c == null) {
				utobj().throwsException("Category not get added kindly check screen shot");
			}
			Course course = new Course();
			course.setCourseobjective(utobj().generateTestData("courseobjective"));
			course.setInstrutor("FranConnect Administrator");
			course.setCourseinstruction("This is the sameple instruction");
			course.setAllowrolebasedaccess(c.isRolebasedAccessCategory());
			course.setCoursename(utobj().generateTestData("CourseName"));
			course = c.addCouse(driver, config, course);
			Lesson lesson = new Lesson();
			lesson = course.addLesson(driver, c, lesson);
			utobj().switchFrameToDefault(driver);
			boolean check = utobj().assertPageSource(driver, "View Per Page");
			if (!check) {
				throw new Exception("view per page not found");
			}
			check = utobj().verifyElementOnVisible_ByXpath(driver, ".//*[@data-role=\"ico_ArrowBackward\"]");
			if (!check) {
				throw new Exception("Backward Arrow not found");
			}
			check = utobj().verifyElementOnVisible_ByXpath(driver, ".//*[@data-role=\"ico_ArrowLeft\"]");
			if (!check) {
				throw new Exception("Left Arrow not found");
			}
			check = utobj().verifyElementOnVisible_ByXpath(driver, ".//*[@data-role=\"ico_ArrowRight\"]");
			if (!check) {
				throw new Exception("Right Arrow not found");
			}
			check = utobj().verifyElementOnVisible_ByXpath(driver, ".//*[@data-role=\"ico_ArrowForward\"]");
			if (!check) {
				throw new Exception("Forward Arrow not found");
			}
		} catch (Exception e) {

			utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	private void draganddropperform(WebDriver driver) {
		try {
			WebElement elt1 = utobj().getElementByXpath(driver, ".//*[@data-role=\"ico_Drag\"]");
			WebElement elt2 = utobj().getElementByXpath(driver, ".//*[@id='category_1']");
			Actions builder = new Actions(driver);
			Action dragAndDrop = builder.clickAndHold(elt1).moveToElement(elt2).release(elt2).build();
			dragAndDrop.perform();
			utobj().verifyElementOnVisible_ByXpath(driver, ".//*[@data-role=\"ico_Delete\"]");
			utobj().clickElement(driver, ".//*[contains(text(),'Save')]");
			utobj().clickElement(driver, ".//*[contains(text(),'Close')]");
			System.out.println("drag and drop done");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

}
