package com.builds.test.training;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.admin.AdminUsersManageCorporateUsersPageTest;
import com.builds.test.common.CorporateUser;
import com.builds.uimaps.training.AdminTrainingCoureseManagementPage;
import com.builds.uimaps.training.AdminTrainingCourseManagementAddSectionrPage;
import com.builds.uimaps.training.AdminTrainingPlanAndCertificatePage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class TestTrainingTopSearch extends FranconnectUtil {

	@Test(groups = { "training", "topsearch" })
	@TestCase(createdOn = "2018-07-11", updatedOn = "2018-07-11", testCaseDescription = "Verify The validation of course in Top Search", testCaseId = "TC_Top_Search_01")
	private void validateCourse() throws Exception {
		String testCaseId = utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = commonMethods().browsers().openBrowser();
		try {
			driver = loginpage().login(driver);
			AdminTrainingCoureseManagementPage pobj = new AdminTrainingCoureseManagementPage(driver);
			Training T = new Training();
			Category c = new Category();
			c.setCategoryDesc(utobj().generateTestData("descsription"));
			c.setCategoryName(utobj().generateTestData("categoryname"));
			c.setUploadCoverImage(utobj().getFilePathFromTestData("logo.jpg"));
			c.setRolebasedAccessCategory(false);
			training().training_common().adminTraingCourseManagementPage(driver);
			c = T.AddCategory(driver, config, c);
			if (c == null) {
				utobj().throwsException("Category not get added kindly check screen shot");
			}
			Course course = new Course();
			course.setCourseobjective(utobj().generateTestData("courseobjective"));
			course.setInstrutor("FranConnect Administrator");
			course.setCourseinstruction("This is the sameple instruction");
			course.setAllowrolebasedaccess(c.isRolebasedAccessCategory());
			course.setCoursename(utobj().generateTestData("CourseName"));
			course = c.addCouse(driver, config, course);
			Lesson lesson = new Lesson();
			lesson = course.addLesson(driver, c, lesson);
			utobj().switchFrameToDefault(driver);
			training().training_common().adminTraingCourseManagementPage(driver);
			utobj().sendKeys(driver, pobj.Search, course.getCoursename());
			utobj().clickElement(driver, "//input[@id='systemSearchBtn']");
			boolean check = utobj().assertPageSource(driver, "View Per Page");
			utobj().clickElement(driver, "//a[contains(text(),'" + course.getCoursename() + "')]");
			utobj().assertPageSource(driver, course.getCourseinstruction());
			utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "training", "topsearch"})
	@TestCase(createdOn = "2018-07-11", updatedOn = "2018-07-11", testCaseDescription = "Verify The validation of course in Top Search", testCaseId = "TC_Top_Search_12")
	private void validateCourse1() throws Exception {
		String testCaseId = utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = commonMethods().browsers().openBrowser();
		try {
			driver = loginpage().login(driver);
			utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser = commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail("trainingautomation@staffex.com");
			corpUser.setPassword("123456789");
			corpUser = corpTest.createDefaultUser(driver, corpUser);
			
			AdminTrainingCoureseManagementPage pobj = new AdminTrainingCoureseManagementPage(driver);
			Training T = new Training();
			Category c = new Category();
			c.setCategoryDesc(utobj().generateTestData("descsription"));
			c.setCategoryName(utobj().generateTestData("categoryname"));
			c.setUploadCoverImage(utobj().getFilePathFromTestData("logo.jpg"));
			c.setRolebasedAccessCategory(false);
			training().training_common().adminTraingCourseManagementPage(driver);
			c = T.AddCategory(driver, config, c);
			if (c == null) {
				utobj().throwsException("Category not get added kindly check screen shot");
			}
			Course course = new Course();
			course.setCourseobjective(utobj().generateTestData("courseobjective"));
			course.setInstrutor("FranConnect Administrator");
			course.setCourseinstruction("This is the sameple instruction");
			course.setAllowrolebasedaccess(c.isRolebasedAccessCategory());
			course.setCoursename(utobj().generateTestData("CourseName"));
			course = c.addCouse(driver, config, course);
			Lesson lesson = new Lesson();
			lesson = course.addLesson(driver, c, lesson);
			utobj().switchFrameToDefault(driver);
			training().training_common().adminTraingCourseManagementPage(driver);
			utobj().sendKeys(driver, pobj.Search, course.getCoursename());
			utobj().clickElement(driver, "//input[@id='systemSearchBtn']");
			utobj().clickElement(driver, "//*[@viewBox='0 0 12 24']");
			utobj().clickElement(driver, "//a[contains(text(),'Publish')]");
			utobj().clickElement(driver, pobj.publishPopupConfirm);
			utobj().clickElement(driver, "//div[@id='detailActionMenu']//*[@viewBox='0 0 12 24']");
			utobj().clickElement(driver, "//a[contains(text(),'Invite Users')]");
			utobj().clickElement(driver, pobj.addressLink);
			commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			utobj().sendKeys(driver, pobj.searchCorpUsers, corpUser.getuserFullName());
			utobj().clickEnterOnElement(driver, pobj.searchCorpUsers);
			utobj().clickElement(driver, pobj.corpCheckBox);
			utobj().clickElement(driver, pobj.addUser);
			utobj().switchFrameToDefault(driver);
			utobj().clickElement(driver, pobj.sendInviteButton);
			utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(groups = { "training", "topsearch"})
	@TestCase(createdOn = "2018-07-11", updatedOn = "2018-07-11", testCaseDescription = "Verify The validation lesson in Top Search", testCaseId = "TC_Top_Search_02")
	private void validateLesson() throws Exception {
		String testCaseId = utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = commonMethods().browsers().openBrowser();
		try {
			driver = loginpage().login(driver);
			AdminTrainingCoureseManagementPage pobj = new AdminTrainingCoureseManagementPage(driver);
			Training T = new Training();
			Category c = new Category();
			c.setCategoryDesc(utobj().generateTestData("descsription"));
			c.setCategoryName(utobj().generateTestData("categoryname"));
			c.setUploadCoverImage(utobj().getFilePathFromTestData("logo.jpg"));
			c.setRolebasedAccessCategory(false);
			training().training_common().adminTraingCourseManagementPage(driver);
			c = T.AddCategory(driver, config, c);
			if (c == null) {
				utobj().throwsException("Category not get added kindly check screen shot");
			}
			Course course = new Course();
			course.setCourseobjective(utobj().generateTestData("courseobjective"));
			course.setInstrutor("FranConnect Administrator");
			course.setCourseinstruction("This is the sameple instruction");
			course.setAllowrolebasedaccess(c.isRolebasedAccessCategory());
			course.setCoursename(utobj().generateTestData("CourseName"));
			course = c.addCouse(driver, config, course);
			Lesson lesson = new Lesson();
			lesson = course.addLesson(driver, c, lesson);
			utobj().switchFrameToDefault(driver);
			training().training_common().adminTraingCourseManagementPage(driver);
			utobj().sendKeys(driver, pobj.Search, lesson.getLessonName());
			utobj().clickElement(driver, "//input[@id='systemSearchBtn']");
			utobj().clickElement(driver, "//a[contains(text(),'" + lesson.getLessonName() + "')]");
			commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			utobj().assertPageSource(driver, lesson.getLessonSummary());

			utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "training", "topsearch", "FailedTestCasesToday" })
	@TestCase(createdOn = "2018-07-11", updatedOn = "2018-07-11", testCaseDescription = "Verify The validation of course_at_frontend in Top Search", testCaseId = "TC_Top_Search_03")
	private void validateCourse_at_frontend() throws Exception {
		String testCaseId = utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = commonMethods().browsers().openBrowser();
		try {
			driver = loginpage().login(driver);
			AdminTrainingCoureseManagementPage pobj = new AdminTrainingCoureseManagementPage(driver);
			Training T = new Training();
			Category c = new Category();
			c.setCategoryDesc(utobj().generateTestData("descsription"));
			c.setCategoryName(utobj().generateTestData("categoryname"));
			c.setUploadCoverImage(utobj().getFilePathFromTestData("logo.jpg"));
			c.setRolebasedAccessCategory(false);
			training().training_common().adminTraingCourseManagementPage(driver);
			c = T.AddCategory(driver, config, c);
			if (c == null) {
				utobj().throwsException("Category not get added kindly check screen shot");
			}
			Course course = new Course();
			course.setCourseobjective(utobj().generateTestData("courseobjective"));
			course.setInstrutor("FranConnect Administrator");
			course.setCourseinstruction("This is the sameple instruction");
			course.setAllowrolebasedaccess(c.isRolebasedAccessCategory());
			course.setCoursename(utobj().generateTestData("CourseName"));
			course = c.addCouse(driver, config, course);
			Lesson lesson = new Lesson();
			lesson = course.addLesson(driver, c, lesson);
			verifyUnPublishedCourse(driver, config, course, c);
			utobj().switchFrameToDefault(driver);
			publishCourse1(c.getCategoryName(), course.getCoursename(), driver, config, testCaseId);
			training().training_common().trainingHome(driver);
			utobj().sendKeys(driver, pobj.Search, course.getCoursename());
			utobj().clickElement(driver, "//input[@id='systemSearchBtn']");
			utobj().clickElement(driver, "//a[contains(text(),'" + course.getCoursename() + "')]");
			utobj().assertPageSource(driver, course.getCourseinstruction());
			utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void publishCourse1(String categoryname, String coursename, WebDriver driver, Map<String, String> config,
			String testCaseId) throws Exception {
		try {
			training().training_common().adminTraingCourseManagementPage(driver);
			AdminTrainingCourseManagementAddSectionrPage pobj = new AdminTrainingCourseManagementAddSectionrPage(
					driver);
			// course modify Action Img
			utobj().clickElement(driver, pobj.GeneralCategory);

			utobj().sendKeys(driver, pobj.input, categoryname);
			utobj().clickElement(driver, ".//*[@id='mCSB_1_container']//a[contains(text(),'" + categoryname + "')]");

			utobj().clickElement(driver, ".//div[@data-role='ico_Filter']");
			utobj().sendKeys(driver, utobj().getElementByXpath(driver, ".//*[@id='searchCourse']"), coursename);
			utobj().clickElement(driver, utobj().getElementByXpath(driver, ".//*[@id='search']"));

			utobj().clickElement(driver, utobj().getElementByXpath(driver, ".//*[@id='detailActionMenu']/div"));

			utobj().clickElement(driver, pobj.Publish);

			utobj().clickElement(driver, pobj.Confirm);

		} catch (Exception e) {
			utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}

	@Test(groups = { "training", "topsearch", "FailedTestCasesToday" })
	@TestCase(createdOn = "2018-07-11", updatedOn = "2018-07-11", testCaseDescription = "Verify The validation lesson_on_front in Top Search", testCaseId = "TC_Top_Search_04")
	private void validateLesson_on_front() throws Exception {
		String testCaseId = utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = commonMethods().browsers().openBrowser();
		try {
			driver = loginpage().login(driver);
			AdminTrainingCoureseManagementPage pobj = new AdminTrainingCoureseManagementPage(driver);
			Training T = new Training();
			Category c = new Category();
			c.setCategoryDesc(utobj().generateTestData("descsription"));
			c.setCategoryName(utobj().generateTestData("categoryname"));
			c.setUploadCoverImage(utobj().getFilePathFromTestData("logo.jpg"));
			c.setRolebasedAccessCategory(false);
			training().training_common().adminTraingCourseManagementPage(driver);
			c = T.AddCategory(driver, config, c);
			if (c == null) {
				utobj().throwsException("Category not get added kindly check screen shot");
			}
			Course course = new Course();
			course.setCourseobjective(utobj().generateTestData("courseobjective"));
			course.setInstrutor("FranConnect Administrator");
			course.setCourseinstruction("This is the sameple instruction");
			course.setAllowrolebasedaccess(c.isRolebasedAccessCategory());
			course.setCoursename(utobj().generateTestData("CourseName"));
			course = c.addCouse(driver, config, course);
			Lesson lesson = new Lesson();
			lesson = course.addLesson(driver, c, lesson);
			utobj().switchFrameToDefault(driver);
			verifyUnPublishedCourse(driver, config, course, c);
			utobj().switchFrameToDefault(driver);
			publishCourse1(c.getCategoryName(), course.getCoursename(), driver, config, testCaseId);
			training().training_common().trainingHome(driver);
			utobj().sendKeys(driver, pobj.Search, lesson.getLessonName());
			utobj().clickElement(driver, "//input[@id='systemSearchBtn']");
			boolean check = utobj().assertPageSource(driver, "View Per Page");
			utobj().clickElement(driver, "//a[contains(text(),'" + lesson.getLessonName() + "')]");
			commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			utobj().assertPageSource(driver, lesson.getLessonSummary());
			utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "training"})
	@TestCase(createdOn = "2018-07-11", updatedOn = "2018-07-11", testCaseDescription = "Verify The validation lesson_on_front in Top Search", testCaseId = "TC_Top_Search_05")
	private void validateQ() throws Exception {
		String testCaseId = utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = utobj().readTestData("training", testCaseId);
		WebDriver driver = commonMethods().browsers().openBrowser();
		try {
			
			driver = loginpage().login(driver); // ravi
			String parentWindowHandle = driver.getWindowHandle();
			utobj().printTestStep("Navigating to Admin > Training > Course Management Page");
			AdminTrainingCourseManagementAddSectionrPage pobj = new AdminTrainingCourseManagementAddSectionrPage(
					driver);
			AdminTrainingPlanAndCertificatePage pobjpnc = new AdminTrainingPlanAndCertificatePage(driver);
			AdminTrainingCoursesManagementPageTest courseManagement = new AdminTrainingCoursesManagementPageTest();
			utobj().printTestStep("Add Category");
			String categoryname = courseManagement.addCategory(driver, dataSet);
			utobj().printTestStep("Add Course");
			String coursename = courseManagement.addCourse(driver, dataSet, categoryname, testCaseId);
			utobj().printTestStep("Create Lesson");
			utobj().clickElement(driver, pobj.addLesson);

			commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			String sectionTitle = utobj().generateTestData(dataSet.get("sectionTitle"));
			utobj().sendKeys(driver, pobj.sectionTitle, sectionTitle);
			String summary = utobj().generateTestData(dataSet.get("summary"));
			utobj().sendKeys(driver, pobj.summary, summary);
			utobj().clickElement(driver, pobj.addLesson);
			driver.switchTo().defaultContent();
			utobj().clickElement(driver, pobj.uploadFile);
			commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			String fileName = utobj().getFilePathFromTestData("taskFile.pdf");
			utobj().sendKeys(driver, pobj.fileuploader, fileName);
			AdminTrainingCourseManagementAddSectionPageTest objadd = new AdminTrainingCourseManagementAddSectionPageTest();
			objadd.addQuizBeforeCreatingLesson(driver, sectionTitle, dataSet, config, testCaseId);
			publishCourse1(categoryname, coursename, driver, config, testCaseId);
			training().training_common().trainingHome(driver);
			AdminTrainingCoureseManagementPage pobj1 = new AdminTrainingCoureseManagementPage(driver);
			utobj().sendKeys(driver, pobj1.Search, sectionTitle);
			utobj().clickEnterOnElement(driver, pobj1.Search);
			utobj().clickElement(driver, "//a[contains(text(),'" + sectionTitle + "')]");
			commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			utobj().clickElement(driver, pobj.startLearningBtn);
			utobj().switchFrameToDefault(driver);
			
			Set<String> allWindowHandles = driver.getWindowHandles();
			for (String currentWindowHandle : allWindowHandles) {
				if (!currentWindowHandle.equals(parentWindowHandle)) {
					driver.switchTo().window(currentWindowHandle);

					try {
						utobj().getElementByXpath(driver, ".//p[contains(text () , '" + sectionTitle + "')]")
								.isDisplayed();
						utobj().moveToElement(driver, utobj().getElement(driver, pobjpnc.startquiz));
						utobj().clickElement(driver, pobjpnc.startquiz);
					} catch (Exception e) {
						System.out.println("we reached here");
					}
					driver.switchTo().window(parentWindowHandle);
				}
			}
			commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			utobj().clickElement(driver, pobjpnc.startquiz);
			utobj().switchFrameToDefault(driver);
			utobj().clickElement(driver, pobjpnc.insertDate);
			DateFormat dateFormat2 = new SimpleDateFormat("dd");
			Date date2 = new Date();
			String today = dateFormat2.format(date2);
			WebElement dateWidget = utobj().getElementByID(driver, "fc-datepicker-div");
			List<WebElement> columns = dateWidget.findElements(By.tagName("a"));
			for (WebElement cell : columns) {
				if (cell.getText().equals(today)) {
					cell.click();
					break;
				}
			}
			utobj().clickElement(driver, pobjpnc.finishQuiz);
			utobj().clickElement(driver, pobjpnc.SubmitQuiz);
			commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			utobj().clickElement(driver, pobjpnc.Close);
			
			training().training_common().trainingHome(driver);
			utobj().sendKeys(driver, pobj1.Search, sectionTitle);
			try {
				utobj().clickElement(driver, "//input[@id='systemSearchBtn']");
			} catch (Exception e) {
				utobj().clickEnterOnElement(driver, pobj1.Search);
			}
			
			boolean check = utobj().assertPageSource(driver, "View Per Page");
			utobj().clickElement(driver, "//a[contains(text(),'" + sectionTitle + "')]");
			commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			utobj().assertPageSource(driver, sectionTitle);
			utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void publishCourse(String categoryname, String coursename, WebDriver driver, Map<String, String> config,
			String testCaseId) throws Exception {
		try {
			training().training_common().adminTraingCourseManagementPage(driver);
			AdminTrainingCourseManagementAddSectionrPage pobj = new AdminTrainingCourseManagementAddSectionrPage(
					driver);
			
			utobj().clickElement(driver, pobj.GeneralCategory);

			utobj().sendKeys(driver, pobj.input, categoryname);
			utobj().clickElement(driver, ".//*[@id='mCSB_1_container']//a[contains(text(),'" + categoryname + "')]");

			utobj().clickElement(driver, ".//div[@data-role='ico_Filter']");
			utobj().sendKeys(driver, utobj().getElementByXpath(driver, ".//*[@id='searchCourse']"), coursename);
			utobj().clickElement(driver, utobj().getElementByXpath(driver, ".//*[@id='search']"));

			utobj().clickElement(driver, utobj().getElementByXpath(driver, ".//*[@id='detailActionMenu']/div"));

			utobj().clickElement(driver, pobj.Publish);

			utobj().clickElement(driver, pobj.Confirm);

		} catch (Exception e) {
			utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}

	public boolean verifyUnPublishedCourse(WebDriver driver, Map<String, String> config, Course course, Category cat) {
		try {
			AdminTrainingCoureseManagementPage pobj = new AdminTrainingCoureseManagementPage(driver);
			training().training_common().adminTraingCourseManagementPage(driver);
			;

			// course modify Action Img
			/*utobj().clickElement(driver, pobj.GeneralCategory);
			utobj().sendKeys(driver, utobj().getElementByXpath(driver, ".//*[@id='dialogV']//input"),
					cat.getCategoryName());
			utobj().clickElement(driver, utobj().getElementByXpath(driver,
					(".//*[@id='mCSB_1_container']//a[contains(text (), '" + cat.getCategoryName() + "')]")));*/
			
			utobj().printTestStep("Search Category");
			new AdminTrainingCoursesManagementPageTest().searchCategory(driver, pobj, cat.getCategoryName());
			
			utobj().sleep(500);
			utobj().clickPartialLinkText(driver, course.getCoursename());
			List<String> listItems = new LinkedList<String>();
			listItems.add(course.getCoursename());
			listItems.add(course.getCourseobjective());
			listItems.add(course.getCourseinstruction());
			listItems.add(course.getInstrutor());
			if (course.isAllowrolebasedaccess()) {
				listItems.add(course.getInstrutor());
			}
			boolean isTextPresent = utobj().assertPageSourceWithMultipleRecords(driver, listItems);
			if (isTextPresent == false) {
				utobj().throwsException("Was not able to add course !!!");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return false;

	}

}
