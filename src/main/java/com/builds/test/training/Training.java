package com.builds.test.training;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.builds.uimaps.training.AdminTrainingCoureseManagementPage;
import com.builds.utilities.FranconnectUtil;

public class Training extends FranconnectUtil {

	FranconnectUtil fc=new FranconnectUtil();
	
	public Category AddCategory(WebDriver driver, Map<String, String> config, Category c) {
		try {
			AdminTrainingCoureseManagementPage pobj = new AdminTrainingCoureseManagementPage(driver);
			training().training_common().adminTraingCourseManagementPage(driver);
			utobj().clickElement(driver, pobj.manageCategory);
			Exception verify = utobj().assertPageSource(driver, "Manage Courses") ? null
					: utobj().throwsException1("Manage Courses link not found");
			utobj().clickElement(driver, pobj.addCategory);
			commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			boolean testimageformat = false;
			try {
				String fileName = utobj().getFilePathFromTestData("taskFile.pdf");
				utobj().sendKeys(driver, pobj.uploadcoverimage, fileName);
				utobj().clickElement(driver, pobj.addCategoryButton);
				testimageformat = utobj().assertPageSource(driver, "please upload jpg,jpeg,png type file");
				utobj().clickElement(driver, ".//*[contains(text(),'Close')]");
			} catch (Exception e) {
				utobj().printTestStep("Warning: Image is not getting loaded through automation");
			}
			if (!testimageformat) {
				throw new Exception("image with pdf format added in category popup");
			}
			utobj().switchFrameToDefault(driver);
			utobj().clickElement(driver, pobj.addCategory);
			commonMethods().switch_frameClass_newLayoutcboxIframe(driver);

			try {
				String fileName = utobj().getFilePathFromTestData("logo.jpeg");
				utobj().sendKeys(driver, pobj.uploadcoverimage, fileName);
				fileName = utobj().getFilePathFromTestData("logo.jpg");
				utobj().sendKeys(driver, pobj.uploadcoverimage, fileName);
				fileName = utobj().getFilePathFromTestData("logo.png");
				utobj().sendKeys(driver, pobj.uploadcoverimage, fileName);
			} catch (Exception e) {
				utobj().throwsException("Warning: Image is not getting loaded through automation");
			}

			utobj().sendKeys(driver, pobj.categoryName, c.getCategoryName());
			utobj().sendKeys(driver, pobj.categoryDescription, c.getCategoryDesc());
			if (c.isRolebasedAccessCategory()) {
				utobj().clickElement(driver, pobj.allowRoleBasedAccess);
				boolean invalidrole = false;

				if (!invalidrole) {
					try {
						utobj().clickElement(driver,
								utobj().getElementByXpath(driver, ".//*[@id='fc-drop-parentcorRoles']/button"));
						utobj().clickElement(driver, utobj().getElementByXpath(driver,
								".//*[@id='fc-drop-parentcorRoles']/div/ul//span[contains(text(), 'invalid role')]"));
						Thread.sleep(1000);
						invalidrole = true;

					} catch (Exception e) {
						utobj().printTestStep("Searching for invalid corp role doesn't work");
					}

					WebElement element = fc.utobj().getElementByID(driver, "fc-drop-parentcorRoles");
					WebElement element1 = element
							.findElement(By.xpath("./button/parent::div/div//input[@class='searchInputMultiple']"));
					fc.utobj().sendKeys(driver, element1, c.getCategoryCorporateRole());
					fc.utobj().sleep(2000);
					fc.utobj().clickEnterOnElement(driver, element1);
					fc.utobj().clickElementWithoutMove(driver, element.findElement(By.xpath(".//input[@id='selectAll']/parent::*")));
					
					/*utobj().clickElement(driver,
							".//*[@id='fc-drop-parentcorRoles']/div/ul//span[contains(text(), '"
											+ c.getCategoryCorporateRole() + "')]");*/
					utobj().sleep(1000);
				}
				if (!invalidrole) {

					try {
						utobj().clickElement(driver,
								utobj().getElementByXpath(driver, ".//*[@id='fc-drop-parentregionalRoles']/button"));
						utobj().clickElement(driver, utobj().getElementByXpath(driver,
								".//*[@id='fc-drop-parentregionalRoles']/div/ul//span[contains(text(), 'invalid role')]"));
						utobj().sleep(1000);
						invalidrole = true;

					} catch (Exception e) {
						utobj().printTestStep("Searching for invalid Regional role doesn't work");
					}
					
					
					WebElement element = fc.utobj().getElementByID(driver, "fc-drop-parentregionalRoles");
					WebElement element1 = element
							.findElement(By.xpath("./button/parent::div/div//input[@class='searchInputMultiple']"));
					fc.utobj().sendKeys(driver, element1, c.getCategoryRegionalRole());
					fc.utobj().sleep(2000);
					//fc.utobj().clickElement(driver, ".//input[@class='search-btn on']");
					fc.utobj().clickEnterOnElement(driver, element1);
					fc.utobj().clickElementWithoutMove(driver, element.findElement(By.xpath(".//input[@id='selectAll']/parent::*")));
					
					/*utobj().clickElement(driver,
							".//*[@id='fc-drop-parentregionalRoles']/div/ul//span[contains(text(), '"
											+ c.getCategoryRegionalRole() + "')]");*/
					utobj().sleep(1000);
				}

				if (!invalidrole) {
					try {
						utobj().clickElement(driver,
								utobj().getElementByXpath(driver, ".//*[@id='fc-drop-parentfranchiseeRoles']/button"));
						utobj().clickElement(driver, utobj().getElementByXpath(driver,
								".//*[@id='fc-drop-parentfranchiseeRoles']/div/ul//span[contains(text(), 'invalid role')]"));
						utobj().sleep(1000);
						invalidrole = true;

					} catch (Exception e) {
						utobj().printTestStep("Searching for invalid role doesn't work");
					}
					
					WebElement element = fc.utobj().getElementByID(driver, "fc-drop-parentfranchiseeRoles");
					WebElement element1 = element
							.findElement(By.xpath("./button/parent::div/div//input[@class='searchInputMultiple']"));
					fc.utobj().sendKeys(driver, element1, c.getCategoryFranchiseRole());
					fc.utobj().sleep(2000);
					fc.utobj().clickEnterOnElement(driver, element1);
					fc.utobj().clickElementWithoutMove(driver, element.findElement(By.xpath(".//input[@id='selectAll']/parent::*")));
					
					
					/*utobj().clickElement(driver,".//*[@id='fc-drop-parentfranchiseeRoles']/div/ul//span[contains(text(), '"
											+ c.getCategoryFranchiseRole() + "')]");*/
					utobj().sleep(1000);
				}

				if (!invalidrole) {
					try {
						utobj().clickElement(driver,
								utobj().getElementByXpath(driver, ".//*[@id='fc-drop-parentdivisionalRoles']/button"));
						utobj().clickElement(driver, utobj().getElementByXpath(driver,
								".//*[@id='fc-drop-parentdivisionalRoles']/div/ul//span[contains(text(), 'invalid role')]"));
						utobj().sleep(1000);
						invalidrole = true;
					} catch (Exception e) {
						utobj().printTestStep("Searching for invalid role doesn't work");
					}
					
					/*utobj().clickElement(driver,
							".//*[@id='fc-drop-parentdivisionalRoles']/div/ul//span[contains(text(), '"
											+ c.getCategoryDivisionRole() + "')]");*/
					WebElement element = fc.utobj().getElementByID(driver, "fc-drop-parentdivisionalRoles");
					WebElement element1 = element
							.findElement(By.xpath("./button/parent::div/div//input[@class='searchInputMultiple']"));
					fc.utobj().sendKeys(driver, element1, c.getCategoryDivisionRole());
					fc.utobj().sleep(2000);
					fc.utobj().clickEnterOnElement(driver, element1);
					fc.utobj().sleep();
					fc.utobj().clickElementWithoutMove(driver, element.findElement(By.xpath(".//input[@id='selectAll']/parent::*")));
					utobj().sleep(1000);
				}
				if (invalidrole) {
					fc.utobj().throwsException("invalid role got selected");
				}

			}
			utobj().clickElement(driver, pobj.addCategoryButton);
			Pattern p = Pattern.compile("[^A-Za-z0-9 ]");
			Matcher m = p.matcher(c.getCategoryName());
			boolean b = m.find();
			if ((c.getCategoryName() == "" || c.getCategoryDesc() == "")) {
				try {
					utobj().assertPageSource(driver, "This field is required");
					utobj().assertPageSource(driver,
							"Fields marked with <span class=\"mandatory-input\"/> are mandatory. ");
				} catch (Exception exce) {
					utobj().throwsException("Some message is missing from add category pop up");
				}
				utobj().clickElement(driver, ".//*[contains(text(),'Close')]");
				utobj().throwsException("Category not added");
			} else if (b == true) {
				utobj().assertPageSource(driver,
						"Please remove special characters from the Category name Category Description");
				utobj().clickElement(driver, ".//*[contains(text(),'Close')]");
				utobj().throwsException("Category not added");
			}
			boolean checkduplicate = utobj().assertPageSource(driver, "Category name already exist");
			if (checkduplicate) {
				c = null;
			}

			utobj().switchFrameToDefault(driver);
			
			return c;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;

	}

	public Category verifyCategory(WebDriver driver, Category c) {
		try {
			AdminTrainingCoureseManagementPage pobj = new AdminTrainingCoureseManagementPage(driver);
			training().training_common().adminTraingCourseManagementPage(driver);
			utobj().clickElement(driver, pobj.manageCategory);
			boolean check = utobj().verifyElementOnVisible_ByXpath(driver, ".//*[@data-role=\"ico_ArrowBackward\"]");
			if (!check) {
				throw new Exception("Backward Arrow not found");
			}
			check = utobj().verifyElementOnVisible_ByXpath(driver, ".//*[@data-role=\"ico_ArrowLeft\"]");
			if (!check) {
				throw new Exception("Left Arrow not found");
			}
			check = utobj().verifyElementOnVisible_ByXpath(driver, ".//*[@data-role=\"ico_ArrowRight\"]");
			if (!check) {
				throw new Exception("Right Arrow not found");
			}
			check = utobj().verifyElementOnVisible_ByXpath(driver, ".//*[@data-role=\"ico_ArrowForward\"]");
			if (!check) {
				throw new Exception("Forward Arrow not found");
			}
			utobj().printTestStep("Verify The Add Category");
			utobj().clickElement(driver, ".//div[@data-role='ico_Filter']");
			utobj().sendKeys(driver, utobj().getElementByXpath(driver, ".//*[@id='categoryName']"),
					c.getCategoryName());
			if (c.isRolebasedAccessCategory()) {
				utobj().clickElement(driver, pobj.LastModifiedDate);
				Thread.sleep(1000);
				utobj().clickElement(driver, pobj.LastModifiedDateAll);
				Select DrpDwnRoleBasedAccess = new Select(utobj().getElementByID(driver, "flagPrivileged"));
				DrpDwnRoleBasedAccess.selectByValue("0");
			}
			utobj().clickElement(driver, utobj().getElementByXpath(driver, ".//*[@id='search']"));
			List<String> listItems = new LinkedList<String>();
			listItems.add(c.getCategoryName());
			listItems.add(c.getCategoryDesc());
			listItems.add("Last Modify Date");
			listItems.add("Total Course");
			listItems.add("Role Based Access");
			if (c.isRolebasedAccessCategory()) {
				listItems.add("Yes");
			} else {
				listItems.add("No");
			}
			String count = String.valueOf(c.getCourseArray().size());
			listItems.add(count);

			boolean isTextPresent = utobj().assertPageSourceWithMultipleRecords(driver, listItems);
			if (isTextPresent == false) {
				utobj().throwsException("Was not able to add Category !!!");
			}
			System.out.println("Category Getting added");

		} catch (Exception e) {
			e.printStackTrace();

		}

		// c.verifyCourse(c.getCourseName());

		return c;
		// TODO Auto-generated method stub

	}

	public void verifyCategorywithRoleUser(WebDriver driver, Category c, String userName, String password)
			throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		loginpage().loginWithParameter(driver, userName, password);
		this.verifyCategory(driver, c);
	}

	public Category ModifyCategory(WebDriver driver, Map<String, String> config, Category c, String categoryname) {
		try {
			AdminTrainingCoureseManagementPage pobj = new AdminTrainingCoureseManagementPage(driver);
			training().training_common().adminTraingCourseManagementPage(driver);
			utobj().clickElement(driver, pobj.manageCategory);
			utobj().clickElement(driver, ".//div[@data-role='ico_Filter']");
			utobj().sendKeys(driver, utobj().getElementByXpath(driver, ".//*[@id='categoryName']"), categoryname);
			utobj().sleep(2000);
			utobj().clickElement(driver, utobj().getElementByXpath(driver, ".//*[@id='search']"));
			String xpath = ".//*[contains(text () , '" + categoryname
					+ "')]/ancestor::div/following-sibling::div//div[@data-role='ico_ThreeDots']";
			utobj().sleep(2000);
			utobj().clickElement(driver, utobj().getElementByXpath(driver, xpath));
			utobj().sleep(2000);
			utobj().clickElement(driver, ".//*/li[contains(text () , 'Modify')]");
			utobj().sleep(2000);
			commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			utobj().sendKeys(driver, pobj.categoryName, c.getCategoryName());

			utobj().sendKeys(driver, pobj.categoryDescription, c.getCategoryDesc());
			utobj().clickElement(driver, pobj.saveCategoryButtonInModify);
			utobj().switchFrameToDefault(driver);
			return c;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;

	}

	public Category DeleteCategory(WebDriver driver, Category c) throws Exception {
		try {
			AdminTrainingCoureseManagementPage pobj = new AdminTrainingCoureseManagementPage(driver);
			training().training_common().adminTraingCourseManagementPage(driver);
			utobj().clickElement(driver, pobj.manageCategory);
			utobj().printTestStep("Verify The Add Category");
			utobj().sleep(3000);
			utobj().clickElement(driver, ".//div[@data-role='ico_Filter']");
			utobj().sleep(1000);
			utobj().sendKeys(driver, utobj().getElementByXpath(driver, ".//*[@id='categoryName']"),
					c.getCategoryName());
			utobj().sleep(1000);
			utobj().clickElement(driver, utobj().getElementByXpath(driver, ".//*[@id='search']"));
			utobj().sleep(1000);
			String xpath = ".//*[contains(text () , '" + c.getCategoryName()
					+ "')]/ancestor::div/following-sibling::div//div[@data-role='ico_ThreeDots']";
			utobj().clickElement(driver, utobj().getElementByXpath(driver, xpath));
			utobj().sleep(1000);
			utobj().clickElement(driver, pobj.delete);
			utobj().sleep(2000);
			utobj().sleep(1000);
			utobj().dismissAlertBox(driver);
			xpath = ".//*[contains(text () , '" + c.getCategoryName()
					+ "')]/ancestor::div/following-sibling::div//div[@data-role='ico_ThreeDots']";
			utobj().clickElement(driver, utobj().getElementByXpath(driver, xpath));
			utobj().sleep(1000);
			utobj().clickElement(driver, pobj.delete);
			utobj().sleep(2000);
			utobj().sleep(1000);
			utobj().acceptAlertBox(driver);
			utobj().sleep(1000);
		} catch (Exception ex) {
			utobj().throwsException("Exception caught while deleting category ");

		}
		return c;

	}

}
