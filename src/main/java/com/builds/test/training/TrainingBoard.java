package com.builds.test.training;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.uimaps.training.AdminTrainingCoureseManagementPage;
import com.builds.uimaps.training.AdminTrainingCourseManagementAddSectionrPage;
import com.builds.uimaps.training.AdminTrainingPlanAndCertificatePage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class TrainingBoard {
	
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "training","Dashboard" })
	@TestCase(createdOn = "2018-07-03", updatedOn = "2018-07-03", testCaseDescription = "Verify The validation of Category At Admin Training Course Management Page", testCaseId = "TC_01_Training_DashBoard")
	private void validateDashboard() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("training", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			fc.utobj().printTestStep("Entering the credential for validate user");
			driver = fc.loginpage().login(driver);
			AdminTrainingCoureseManagementPage pobj = new AdminTrainingCoureseManagementPage(driver);		
			fc.utobj().printTestStep("Navigate To Training FrontEnd Page");
			fc.training().training_common().trainingHome(driver);
			fc.utobj().isElementPresent(driver, pobj.OverAllDashBoard);
			fc.utobj().isElementPresent(driver, pobj.MyDashBoard);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	@Test(groups = { "training","Dashboard" })
	@TestCase(createdOn = "2018-07-03", updatedOn = "2018-07-03", testCaseDescription = "Verify The validation of Category At Admin Training Course Management Page", testCaseId = "TC_11_Training_DashBoard")
	private void validateDashboard10() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("training", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			fc.utobj().printTestStep("Entering the credential for validate user");
			driver = fc.loginpage().login(driver);
			AdminTrainingCoureseManagementPage pobj = new AdminTrainingCoureseManagementPage(driver);		
			fc.utobj().printTestStep("Navigate To Training FrontEnd Page");
			fc.training().training_common().trainingHome(driver);
			fc.utobj().clickElement(driver, pobj.OverAllDashBoard);
			fc.utobj().assertPageSource(driver, "Weekly Analytics");
			fc.utobj().assertPageSource(driver, "Monthly Analytics");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			
		} catch (Exception e) {
			
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	@Test(groups = { "training","Dashboard123" })
	@TestCase(createdOn = "2018-07-03", updatedOn = "2018-07-03", testCaseDescription = "Verify The validation of Category At Admin Training Course Management Page", testCaseId = "TC_02_Training_DashBoard")
	private void validateDashboard1() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("training", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			fc.utobj().printTestStep("Entering the credential for validate user");
			driver = fc.loginpage().login(driver);
			String parentWindowHandle = driver.getWindowHandle();
			fc.utobj().printTestStep("Navigating to Admin > Training > Course Management Page");
			AdminTrainingCourseManagementAddSectionrPage pobj = new AdminTrainingCourseManagementAddSectionrPage(
					driver);
			AdminTrainingPlanAndCertificatePage pobjpnc = new AdminTrainingPlanAndCertificatePage(driver);
			AdminTrainingCoursesManagementPageTest courseManagement = new AdminTrainingCoursesManagementPageTest();
			fc.utobj().printTestStep("Add Category");
			String categoryname = courseManagement.addCategory(driver, dataSet);
			fc.utobj().printTestStep("Add Course");
			String coursename = courseManagement.addCourse(driver, dataSet, categoryname, testCaseId);
			fc.utobj().printTestStep("Create Lesson");
			fc.utobj().clickElement(driver, pobj.addLesson);

			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			String sectionTitle = fc.utobj().generateTestData(dataSet.get("sectionTitle"));
			fc.utobj().sendKeys(driver, pobj.sectionTitle, sectionTitle);
			String summary = fc.utobj().generateTestData(dataSet.get("summary"));
			fc.utobj().sendKeys(driver, pobj.summary, summary);
			fc.utobj().clickElement(driver, pobj.addLesson);

			driver.switchTo().defaultContent();
			fc.utobj().clickElement(driver, pobj.uploadFile);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			String fileName = fc.utobj().getFilePathFromTestData("taskFile.pdf");
			fc.utobj().sendKeys(driver, pobj.fileuploader, fileName);

			AdminTrainingCourseManagementAddSectionPageTest objadd = new AdminTrainingCourseManagementAddSectionPageTest();
			objadd.addQuizBeforeCreatingLesson(driver, sectionTitle, dataSet, config, testCaseId);
			AdminTrainingPlanAndCertificateTest t1= new AdminTrainingPlanAndCertificateTest();

			t1.publishCourse(categoryname, coursename, driver, config, testCaseId);
			String Certificate = t1.CreateCertificate_indieMethod(driver, config, dataSet, testCaseId, pobjpnc);
			String planName = t1.CreatePlanwithCourseAndCertiicate(driver, config, dataSet, testCaseId, pobjpnc,
					coursename, Certificate);
			t1.publishplan(driver, config, dataSet, testCaseId, pobjpnc, coursename, planName);
			fc.training().training_common().trainingPlans(driver);
			fc.utobj().clickElement(driver, pobjpnc.availablePlan);
			fc.utobj().clickElement(driver, pobjpnc.FilterBtn);
			fc.utobj().sendKeys(driver, pobjpnc.planName, planName);
			fc.utobj().clickElement(driver, pobjpnc.applyFilters);
			fc.utobj().clickElement(driver, pobjpnc.subscribe);
			fc.utobj().clickElement(driver, pobjpnc.start);
			fc.utobj().clickElement(driver, pobjpnc.startLearningN);
			System.out.println();
			Set<String> allWindowHandles = driver.getWindowHandles();
			System.out.println(allWindowHandles.size());
			for (String currentWindowHandle : allWindowHandles) {
				if (!currentWindowHandle.equals(parentWindowHandle)) {
					driver.switchTo().window(currentWindowHandle);

					try {
						fc.utobj().getElementByXpath(driver, ".//p[contains(text () , '" + sectionTitle + "')]")
								.isDisplayed();
						fc.utobj().moveToElement(driver, fc.utobj().getElement(driver, pobjpnc.closequiz));
						fc.utobj().clickElement(driver, pobjpnc.closequiz);
					} catch (Exception e) {
						System.out.println("we reached here");
					}
					driver.switchTo().window(parentWindowHandle);
				}
			}
			AdminTrainingCoureseManagementPage pobj1 = new AdminTrainingCoureseManagementPage(driver);
			fc.utobj().printTestStep("Navigate To Admin Training Course Management Page");
			boolean check= fc.utobj().assertPageSource(driver, sectionTitle);
			if(!check)
			{
				throw new Exception("Lesson not found in <<<<<< Continure from where you left >>>>>>");
			}
			fc.training().training_common().trainingHome(driver);
			fc.utobj().clickPartialLinkText(driver, "In Progress Courses");
			check= fc.utobj().assertPageSource(driver, coursename);
			if(!check)
			{
				throw new Exception("Course not found in <<<<<< In Progress Course >>>>>>");
			}
			fc.training().training_common().trainingHome(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			
		} catch (Exception e) {
			
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	@Test(groups = { "training","Dashboard", "FailedTestCases132" })
	@TestCase(createdOn = "2018-07-03", updatedOn = "2018-07-03", testCaseDescription = "Verify The validation of Category At Admin Training Course Management Page", testCaseId = "TC_04_Training_DashBoard")
	private void validateDashboard3() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("training", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			fc.utobj().printTestStep("Entering the credential for validate user");
			driver = fc.loginpage().login(driver);
			String parentWindowHandle = driver.getWindowHandle();
			fc.utobj().printTestStep("Navigating to Admin > Training > Course Management Page");
			AdminTrainingCourseManagementAddSectionrPage pobj = new AdminTrainingCourseManagementAddSectionrPage(
					driver);
			AdminTrainingPlanAndCertificatePage pobjpnc = new AdminTrainingPlanAndCertificatePage(driver);
			AdminTrainingCoursesManagementPageTest courseManagement = new AdminTrainingCoursesManagementPageTest();
			fc.utobj().printTestStep("Add Category");
			String categoryname = courseManagement.addCategory(driver, dataSet);
			fc.utobj().printTestStep("Add Course");
			String coursename = courseManagement.addCourse(driver, dataSet, categoryname, testCaseId);
			fc.utobj().printTestStep("Create Lesson");
			fc.utobj().clickElement(driver, pobj.addLesson);
			
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			String sectionTitle = fc.utobj().generateTestData(dataSet.get("sectionTitle"));
			fc.utobj().sendKeys(driver, pobj.sectionTitle, sectionTitle);
			String summary = fc.utobj().generateTestData(dataSet.get("summary"));
			fc.utobj().sendKeys(driver, pobj.summary, summary);
			fc.utobj().clickElement(driver, pobj.addLesson);
			
			driver.switchTo().defaultContent();
			fc.utobj().clickElement(driver, pobj.uploadFile);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			String fileName = fc.utobj().getFilePathFromTestData("taskFile.pdf");
			fc.utobj().sendKeys(driver, pobj.fileuploader, fileName);
			
			AdminTrainingCourseManagementAddSectionPageTest objadd = new AdminTrainingCourseManagementAddSectionPageTest();
			objadd.addQuizBeforeCreatingLesson(driver, sectionTitle, dataSet, config, testCaseId);
			AdminTrainingPlanAndCertificateTest t1= new AdminTrainingPlanAndCertificateTest();
			
			t1.publishCourse(categoryname, coursename, driver, config, testCaseId);
			String Certificate = t1.CreateCertificate_indieMethod(driver, config, dataSet, testCaseId, pobjpnc);
			String planName = t1.CreatePlanwithCourseAndCertiicate(driver, config, dataSet, testCaseId, pobjpnc,
					coursename, Certificate);
			t1.publishplan(driver, config, dataSet, testCaseId, pobjpnc, coursename, planName);
			fc.training().training_common().trainingPlans(driver);
			fc.utobj().clickElement(driver, pobjpnc.availablePlan);
			fc.utobj().clickElement(driver, pobjpnc.FilterBtn);
			fc.utobj().sendKeys(driver, pobjpnc.planName, planName);
			fc.utobj().clickElement(driver, pobjpnc.applyFilters);
			fc.utobj().clickElement(driver, pobjpnc.subscribe);
			fc.utobj().clickElement(driver, pobjpnc.start);
			fc.utobj().clickElement(driver, pobjpnc.startLearningN);
			System.out.println();
			Set<String> allWindowHandles = driver.getWindowHandles();
			System.out.println(allWindowHandles.size());
			for (String currentWindowHandle : allWindowHandles) {
				if (!currentWindowHandle.equals(parentWindowHandle)) {
					driver.switchTo().window(currentWindowHandle);
					
					try {
						fc.utobj().getElementByXpath(driver, ".//p[contains(text () , '" + sectionTitle + "')]")
						.isDisplayed();
						fc.utobj().moveToElement(driver, fc.utobj().getElement(driver, pobjpnc.closequiz));
						fc.utobj().clickElement(driver, pobjpnc.closequiz);
					} catch (Exception e) {
						System.out.println("we reached here");
					}
					driver.switchTo().window(parentWindowHandle);
				}
			}
			AdminTrainingCoureseManagementPage pobj1 = new AdminTrainingCoureseManagementPage(driver);
			fc.utobj().printTestStep("Navigate To Admin Training Course Management Page");
			boolean check= fc.utobj().assertPageSource(driver, sectionTitle);
			if(!check)
			{
				throw new Exception("Lesson not found in <<<<<< Continure from where you left >>>>>>");
			}
			try
			{
			fc.training().training_common().trainingHome(driver);
			fc.utobj().clickPartialLinkText(driver, "In Progress Courses");
			check= fc.utobj().assertPageSource(driver, coursename);
			if(!check)
			{
				throw new Exception("Course not found in <<<<<< In Progress Course >>>>>>");
			}
			fc.training().training_common().trainingHome(driver);
			fc.utobj().isElementPresent(driver,pobj.mostPopular);
			fc.utobj().isElementPresent(driver,pobj.InProgress);
			}
			catch(Exception ex)
			{
				
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			
		} catch (Exception e) {
			
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	@Test(groups = { "training","Dashboard" })
	@TestCase(createdOn = "2018-07-03", updatedOn = "2018-07-03", testCaseDescription = "Verify The validation of Category At Admin Training Course Management Page", testCaseId = "TC_05_Training_DashBoard")
	private void validateDashboard4() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("training", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			fc.utobj().printTestStep("Entering the credential for validate user");
			driver = fc.loginpage().login(driver);
			String parentWindowHandle = driver.getWindowHandle();
			fc.utobj().printTestStep("Navigating to Admin > Training > Course Management Page");
			AdminTrainingCourseManagementAddSectionrPage pobj = new AdminTrainingCourseManagementAddSectionrPage(
					driver);
			AdminTrainingPlanAndCertificatePage pobjpnc = new AdminTrainingPlanAndCertificatePage(driver);
			AdminTrainingCoursesManagementPageTest courseManagement = new AdminTrainingCoursesManagementPageTest();
			fc.utobj().printTestStep("Add Category");
			String categoryname = courseManagement.addCategory(driver, dataSet);
			fc.utobj().printTestStep("Add Course");
			String coursename = courseManagement.addCourse(driver, dataSet, categoryname, testCaseId);
			fc.utobj().printTestStep("Create Lesson");
			fc.utobj().clickElement(driver, pobj.addLesson);
			
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			String sectionTitle = fc.utobj().generateTestData(dataSet.get("sectionTitle"));
			fc.utobj().sendKeys(driver, pobj.sectionTitle, sectionTitle);
			String summary = fc.utobj().generateTestData(dataSet.get("summary"));
			fc.utobj().sendKeys(driver, pobj.summary, summary);
			fc.utobj().clickElement(driver, pobj.addLesson);
			
			driver.switchTo().defaultContent();
			fc.utobj().clickElement(driver, pobj.uploadFile);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			String fileName = fc.utobj().getFilePathFromTestData("taskFile.pdf");
			fc.utobj().sendKeys(driver, pobj.fileuploader, fileName);
			
			AdminTrainingCourseManagementAddSectionPageTest objadd = new AdminTrainingCourseManagementAddSectionPageTest();
			objadd.addQuizBeforeCreatingLesson(driver, sectionTitle, dataSet, config, testCaseId);
			AdminTrainingPlanAndCertificateTest t1= new AdminTrainingPlanAndCertificateTest();
			
			t1.publishCourse(categoryname, coursename, driver, config, testCaseId);
			String Certificate = t1.CreateCertificate_indieMethod(driver, config, dataSet, testCaseId, pobjpnc);
			String planName = t1.CreatePlanwithCourseAndCertiicate(driver, config, dataSet, testCaseId, pobjpnc,
					coursename, Certificate);
			t1.publishplan(driver, config, dataSet, testCaseId, pobjpnc, coursename, planName);
			fc.training().training_common().trainingPlans(driver);
			fc.utobj().clickElement(driver, pobjpnc.availablePlan);
			fc.utobj().clickElement(driver, pobjpnc.FilterBtn);
			fc.utobj().sendKeys(driver, pobjpnc.planName, planName);
			fc.utobj().clickElement(driver, pobjpnc.applyFilters);
			fc.utobj().clickElement(driver, pobjpnc.subscribe);
			fc.utobj().clickElement(driver, pobjpnc.start);
			fc.utobj().clickElement(driver, pobjpnc.startLearningN);
			System.out.println();
			Set<String> allWindowHandles = driver.getWindowHandles();
			System.out.println(allWindowHandles.size());
			for (String currentWindowHandle : allWindowHandles) {
				if (!currentWindowHandle.equals(parentWindowHandle)) {
					driver.switchTo().window(currentWindowHandle);
					
					try {
						fc.utobj().getElementByXpath(driver, ".//p[contains(text () , '" + sectionTitle + "')]")
						.isDisplayed();
						fc.utobj().moveToElement(driver, fc.utobj().getElement(driver, pobjpnc.closequiz));
						fc.utobj().clickElement(driver, pobjpnc.closequiz);
					} catch (Exception e) {
						System.out.println("we reached here");
					}
					driver.switchTo().window(parentWindowHandle);
				}
			}
			AdminTrainingCoureseManagementPage pobj1 = new AdminTrainingCoureseManagementPage(driver);
			fc.utobj().printTestStep("Navigate To Admin Training Course Management Page");
			boolean check= fc.utobj().assertPageSource(driver, sectionTitle);
			if(!check)
			{
				throw new Exception("Lesson not found in <<<<<< Continure from where you left >>>>>>");
			}
			fc.training().training_common().trainingHome(driver);
			fc.utobj().clickPartialLinkText(driver, "In Progress Courses");
			check= fc.utobj().assertPageSource(driver, coursename);
			if(!check)
			{
				throw new Exception("Course not found in <<<<<< In Progress Course >>>>>>");
			}
			fc.training().training_common().trainingHome(driver);
			fc.utobj().isElementPresent(driver,pobj.recentlyAddedPlan);
			fc.utobj().isElementPresent(driver,pobj.plantobeExpired);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			
		} catch (Exception e) {
			
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	@Test(groups = { "training","Dashboard" })
	@TestCase(createdOn = "2018-07-03", updatedOn = "2018-07-03", testCaseDescription = "Verify The validation of Category At Admin Training Course Management Page", testCaseId = "TC_03_Training_DashBoard")
	private void validateDashboard2() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("training", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			fc.utobj().printTestStep("Entering the credential for validate user");
			driver = fc.loginpage().login(driver);
			String parentWindowHandle = driver.getWindowHandle();
			fc.utobj().printTestStep("Navigating to Admin > Training > Course Management Page");
			AdminTrainingCourseManagementAddSectionrPage pobj = new AdminTrainingCourseManagementAddSectionrPage(
					driver);
			AdminTrainingPlanAndCertificatePage pobjpnc = new AdminTrainingPlanAndCertificatePage(driver);
			AdminTrainingCoursesManagementPageTest courseManagement = new AdminTrainingCoursesManagementPageTest();
			fc.utobj().printTestStep("Add Category");
			String categoryname = courseManagement.addCategory(driver, dataSet);
			fc.utobj().printTestStep("Add Course");
			String coursename = courseManagement.addCourse(driver, dataSet, categoryname, testCaseId);
			fc.utobj().printTestStep("Create Lesson");
			fc.utobj().clickElement(driver, pobj.addLesson);
			
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			String sectionTitle = fc.utobj().generateTestData(dataSet.get("sectionTitle"));
			fc.utobj().sendKeys(driver, pobj.sectionTitle, sectionTitle);
			String summary = fc.utobj().generateTestData(dataSet.get("summary"));
			fc.utobj().sendKeys(driver, pobj.summary, summary);
			fc.utobj().clickElement(driver, pobj.addLesson);
			
			driver.switchTo().defaultContent();
			fc.utobj().clickElement(driver, pobj.uploadFile);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			String fileName = fc.utobj().getFilePathFromTestData("taskFile.pdf");
			fc.utobj().sendKeys(driver, pobj.fileuploader, fileName);
			
			AdminTrainingCourseManagementAddSectionPageTest objadd = new AdminTrainingCourseManagementAddSectionPageTest();
			objadd.addQuizBeforeCreatingLesson(driver, sectionTitle, dataSet, config, testCaseId);
			AdminTrainingPlanAndCertificateTest t1= new AdminTrainingPlanAndCertificateTest();
			
			t1.publishCourse(categoryname, coursename, driver, config, testCaseId);
			String Certificate = t1.CreateCertificate_indieMethod(driver, config, dataSet, testCaseId, pobjpnc);
			String planName = t1.CreatePlanwithCourseAndCertiicate(driver, config, dataSet, testCaseId, pobjpnc,
					coursename, Certificate);
			t1.publishplan(driver, config, dataSet, testCaseId, pobjpnc, coursename, planName);
			fc.training().training_common().trainingPlans(driver);
			fc.utobj().clickElement(driver, pobjpnc.availablePlan);
			fc.utobj().clickElement(driver, pobjpnc.FilterBtn);
			fc.utobj().sendKeys(driver, pobjpnc.planName, planName);
			fc.utobj().clickElement(driver, pobjpnc.applyFilters);
			fc.utobj().clickElement(driver, pobjpnc.subscribe);
			fc.utobj().clickElement(driver, pobjpnc.start);
			fc.utobj().clickElement(driver, pobjpnc.startLearningN);
			System.out.println();
			Set<String> allWindowHandles = driver.getWindowHandles();
			System.out.println(allWindowHandles.size());
			for (String currentWindowHandle : allWindowHandles) {
				if (!currentWindowHandle.equals(parentWindowHandle)) {
					driver.switchTo().window(currentWindowHandle);
					
					try {
						fc.utobj().getElementByXpath(driver, ".//p[contains(text () , '" + sectionTitle + "')]")
						.isDisplayed();
						fc.utobj().moveToElement(driver, fc.utobj().getElement(driver, pobjpnc.closequiz));
						fc.utobj().clickElement(driver, pobjpnc.closequiz);
					} catch (Exception e) {
						System.out.println("we reached here");
					}
					driver.switchTo().window(parentWindowHandle);
				}
			}
			AdminTrainingCoureseManagementPage pobj1 = new AdminTrainingCoureseManagementPage(driver);
			fc.utobj().printTestStep("Navigate To Admin Training Course Management Page");
			boolean check= fc.utobj().assertPageSource(driver, sectionTitle);
			if(!check)
			{
				throw new Exception("Lesson not found in <<<<<< Continure from where you left >>>>>>");
			}
			fc.training().training_common().trainingHome(driver);
			fc.utobj().clickPartialLinkText(driver, "In Progress Courses");
			check= fc.utobj().assertPageSource(driver, coursename);
			if(!check)
			{
				throw new Exception("Course not found in <<<<<< In Progress Course >>>>>>");
			}
			fc.training().training_common().trainingHome(driver);
			fc.utobj().isElementPresent(driver, pobj1.resume);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			
		} catch (Exception e) {
			
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	@Test(groups = { "training","Dashboard" , "FailedTestCases"})
	@TestCase(createdOn = "2018-07-03", updatedOn = "2018-07-03", testCaseDescription = "Verify The validation of Category At Admin Training Course Management Page", testCaseId = "TC_06_Training_DashBoard")
	private void validateDashboard5() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("training", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			fc.utobj().printTestStep("Entering the credential for validate user");
			driver = fc.loginpage().login(driver);
			String parentWindowHandle = driver.getWindowHandle();
			fc.utobj().printTestStep("Navigating to Admin > Training > Course Management Page");
			AdminTrainingCourseManagementAddSectionrPage pobj = new AdminTrainingCourseManagementAddSectionrPage(
					driver);
			AdminTrainingPlanAndCertificatePage pobjpnc = new AdminTrainingPlanAndCertificatePage(driver);
			AdminTrainingCoursesManagementPageTest courseManagement = new AdminTrainingCoursesManagementPageTest();
			fc.utobj().printTestStep("Add Category");
			String categoryname = courseManagement.addCategory(driver, dataSet);
			fc.utobj().printTestStep("Add Course");
			String coursename = courseManagement.addCourse(driver, dataSet, categoryname, testCaseId);
			fc.utobj().printTestStep("Create Lesson");
			fc.utobj().clickElement(driver, pobj.addLesson);
			
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			String sectionTitle = fc.utobj().generateTestData(dataSet.get("sectionTitle"));
			fc.utobj().sendKeys(driver, pobj.sectionTitle, sectionTitle);
			String summary = fc.utobj().generateTestData(dataSet.get("summary"));
			fc.utobj().sendKeys(driver, pobj.summary, summary);
			fc.utobj().clickElement(driver, pobj.addLesson);
			
			driver.switchTo().defaultContent();
			fc.utobj().clickElement(driver, pobj.uploadFile);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			String fileName = fc.utobj().getFilePathFromTestData("taskFile.pdf");
			fc.utobj().sendKeys(driver, pobj.fileuploader, fileName);
			
			AdminTrainingCourseManagementAddSectionPageTest objadd = new AdminTrainingCourseManagementAddSectionPageTest();
			objadd.addQuizBeforeCreatingLesson(driver, sectionTitle, dataSet, config, testCaseId);
			AdminTrainingPlanAndCertificateTest t1= new AdminTrainingPlanAndCertificateTest();
			
			t1.publishCourse(categoryname, coursename, driver, config, testCaseId);
			String Certificate = t1.CreateCertificate_indieMethod(driver, config, dataSet, testCaseId, pobjpnc);
			String planName = t1.CreatePlanwithCourseAndCertiicate(driver, config, dataSet, testCaseId, pobjpnc,
					coursename, Certificate);
			t1.publishplan(driver, config, dataSet, testCaseId, pobjpnc, coursename, planName);
			fc.training().training_common().trainingPlans(driver);
			fc.utobj().clickElement(driver, pobjpnc.availablePlan);
			fc.utobj().clickElement(driver, pobjpnc.FilterBtn);
			fc.utobj().sendKeys(driver, pobjpnc.planName, planName);
			fc.utobj().clickElement(driver, pobjpnc.applyFilters);
			fc.utobj().clickElement(driver, pobjpnc.subscribe);
			fc.utobj().clickElement(driver, pobjpnc.start);
			fc.utobj().clickElement(driver, pobjpnc.startLearningN);
			System.out.println();
			Set<String> allWindowHandles = driver.getWindowHandles();
			System.out.println(allWindowHandles.size());
			for (String currentWindowHandle : allWindowHandles) {
				if (!currentWindowHandle.equals(parentWindowHandle)) {
					driver.switchTo().window(currentWindowHandle);
					
					try {
						fc.utobj().getElementByXpath(driver, ".//p[contains(text () , '" + sectionTitle + "')]")
						.isDisplayed();
						fc.utobj().moveToElement(driver, fc.utobj().getElement(driver, pobjpnc.closequiz));
						fc.utobj().clickElement(driver, pobjpnc.closequiz);
					} catch (Exception e) {
						System.out.println("we reached here");
					}
					driver.switchTo().window(parentWindowHandle);
				}
			}
			AdminTrainingCoureseManagementPage pobj1 = new AdminTrainingCoureseManagementPage(driver);
			fc.utobj().printTestStep("Navigate To Admin Training Course Management Page");
			boolean check= fc.utobj().assertPageSource(driver, sectionTitle);
			if(!check)
			{
				throw new Exception("Lesson not found in <<<<<< Continure from where you left >>>>>>");
			}
			try
			{
			fc.training().training_common().trainingHome(driver);
			fc.utobj().clickElement(driver, "//span[@class='ellipsis'][contains(text(),'"+coursename+"')]/parent::td//following-sibling::td/button");
			}
			catch(Exception ex)
			{
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			
		} catch (Exception e) {
			
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	@Test(groups = { "training","Dashboard" })
	@TestCase(createdOn = "2018-07-03", updatedOn = "2018-07-03", testCaseDescription = "Verify The validation of Category At Admin Training Course Management Page", testCaseId = "TC_07_Training_DashBoard")
	private void validateDashboard6() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("training", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			fc.utobj().printTestStep("Entering the credential for validate user");
			driver = fc.loginpage().login(driver);
			String parentWindowHandle = driver.getWindowHandle();
			fc.utobj().printTestStep("Navigating to Admin > Training > Course Management Page");
			AdminTrainingCourseManagementAddSectionrPage pobj = new AdminTrainingCourseManagementAddSectionrPage(
					driver);
			AdminTrainingPlanAndCertificatePage pobjpnc = new AdminTrainingPlanAndCertificatePage(driver);
			AdminTrainingCoursesManagementPageTest courseManagement = new AdminTrainingCoursesManagementPageTest();
			fc.utobj().printTestStep("Add Category");
			String categoryname = courseManagement.addCategory(driver, dataSet);
			fc.utobj().printTestStep("Add Course");
			String coursename = courseManagement.addCourse(driver, dataSet, categoryname, testCaseId);
			fc.utobj().printTestStep("Create Lesson");
			fc.utobj().clickElement(driver, pobj.addLesson);
			
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			String sectionTitle = fc.utobj().generateTestData(dataSet.get("sectionTitle"));
			fc.utobj().sendKeys(driver, pobj.sectionTitle, sectionTitle);
			String summary = fc.utobj().generateTestData(dataSet.get("summary"));
			fc.utobj().sendKeys(driver, pobj.summary, summary);
			fc.utobj().clickElement(driver, pobj.addLesson);
			
			driver.switchTo().defaultContent();
			fc.utobj().clickElement(driver, pobj.uploadFile);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			String fileName = fc.utobj().getFilePathFromTestData("taskFile.pdf");
			fc.utobj().sendKeys(driver, pobj.fileuploader, fileName);
			
			AdminTrainingCourseManagementAddSectionPageTest objadd = new AdminTrainingCourseManagementAddSectionPageTest();
			objadd.addQuizBeforeCreatingLesson(driver, sectionTitle, dataSet, config, testCaseId);
			AdminTrainingPlanAndCertificateTest t1= new AdminTrainingPlanAndCertificateTest();
			
			t1.publishCourse(categoryname, coursename, driver, config, testCaseId);
			String Certificate = t1.CreateCertificate_indieMethod(driver, config, dataSet, testCaseId, pobjpnc);
			String planName = t1.CreatePlanwithCourseAndCertiicate(driver, config, dataSet, testCaseId, pobjpnc,
					coursename, Certificate);
			t1.publishplan(driver, config, dataSet, testCaseId, pobjpnc, coursename, planName);
			fc.training().training_common().trainingPlans(driver);
			fc.utobj().clickElement(driver, pobjpnc.availablePlan);
			fc.utobj().clickElement(driver, pobjpnc.FilterBtn);
			fc.utobj().sendKeys(driver, pobjpnc.planName, planName);
			fc.utobj().clickElement(driver, pobjpnc.applyFilters);
			fc.utobj().clickElement(driver, pobjpnc.subscribe);
			fc.utobj().clickElement(driver, pobjpnc.start);
			fc.utobj().clickElement(driver, pobjpnc.startLearningN);
			System.out.println();
			Set<String> allWindowHandles = driver.getWindowHandles();
			System.out.println(allWindowHandles.size());
			for (String currentWindowHandle : allWindowHandles) {
				if (!currentWindowHandle.equals(parentWindowHandle)) {
					driver.switchTo().window(currentWindowHandle);
					
					try {
						fc.utobj().getElementByXpath(driver, ".//p[contains(text () , '" + sectionTitle + "')]")
						.isDisplayed();
						fc.utobj().moveToElement(driver, fc.utobj().getElement(driver, pobjpnc.closequiz));
						fc.utobj().clickElement(driver, pobjpnc.closequiz);
					} catch (Exception e) {
						System.out.println("we reached here");
					}
					driver.switchTo().window(parentWindowHandle);
				}
			}
			AdminTrainingCoureseManagementPage pobj1 = new AdminTrainingCoureseManagementPage(driver);
			fc.utobj().printTestStep("Navigate To Admin Training Course Management Page");
			boolean check= fc.utobj().assertPageSource(driver, sectionTitle);
			if(!check)
			{
				throw new Exception("Lesson not found in <<<<<< Continure from where you left >>>>>>");
			}
			fc.training().training_common().trainingHome(driver);
			fc.utobj().clickPartialLinkText(driver, "In Progress Courses");
			check= fc.utobj().assertPageSource(driver, coursename);
			fc.training().training_common().trainingHome(driver);
			fc.utobj().clickElement(driver, pobj.InProgress);
			fc.utobj().clickElement(driver, "//span[@class='ellipsis'][contains(text(),'"+coursename+"')]/parent::td//following-sibling::td/button");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			
		} catch (Exception e) {
			
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	@Test(groups = { "training","Dashboard" })
	@TestCase(createdOn = "2018-07-03", updatedOn = "2018-07-03", testCaseDescription = "Verify The validation of Category At Admin Training Course Management Page", testCaseId = "TC_08_Training_DashBoard")
	private void validateDashboard7() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("training", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			fc.utobj().printTestStep("Entering the credential for validate user");
			driver = fc.loginpage().login(driver);
			String parentWindowHandle = driver.getWindowHandle();
			fc.utobj().printTestStep("Navigating to Admin > Training > Course Management Page");
			AdminTrainingCourseManagementAddSectionrPage pobj = new AdminTrainingCourseManagementAddSectionrPage(
					driver);
			AdminTrainingPlanAndCertificatePage pobjpnc = new AdminTrainingPlanAndCertificatePage(driver);
			AdminTrainingCoursesManagementPageTest courseManagement = new AdminTrainingCoursesManagementPageTest();
			fc.utobj().printTestStep("Add Category");
			String categoryname = courseManagement.addCategory(driver, dataSet);
			fc.utobj().printTestStep("Add Course");
			String coursename = courseManagement.addCourse(driver, dataSet, categoryname, testCaseId);
			fc.utobj().printTestStep("Create Lesson");
			fc.utobj().clickElement(driver, pobj.addLesson);
			
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			String sectionTitle = fc.utobj().generateTestData(dataSet.get("sectionTitle"));
			fc.utobj().sendKeys(driver, pobj.sectionTitle, sectionTitle);
			String summary = fc.utobj().generateTestData(dataSet.get("summary"));
			fc.utobj().sendKeys(driver, pobj.summary, summary);
			fc.utobj().clickElement(driver, pobj.addLesson);
			
			driver.switchTo().defaultContent();
			fc.utobj().clickElement(driver, pobj.uploadFile);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			String fileName = fc.utobj().getFilePathFromTestData("taskFile.pdf");
			fc.utobj().sendKeys(driver, pobj.fileuploader, fileName);
			
			AdminTrainingCourseManagementAddSectionPageTest objadd = new AdminTrainingCourseManagementAddSectionPageTest();
			objadd.addQuizBeforeCreatingLesson(driver, sectionTitle, dataSet, config, testCaseId);
			AdminTrainingPlanAndCertificateTest t1= new AdminTrainingPlanAndCertificateTest();
			
			t1.publishCourse(categoryname, coursename, driver, config, testCaseId);
			String Certificate = t1.CreateCertificate_indieMethod(driver, config, dataSet, testCaseId, pobjpnc);
			String planName = t1.CreatePlanwithCourseAndCertiicate(driver, config, dataSet, testCaseId, pobjpnc,
					coursename, Certificate);
			t1.publishplan(driver, config, dataSet, testCaseId, pobjpnc, coursename, planName);
			fc.training().training_common().trainingHome(driver);
			try
			{
			fc.utobj().clickElement(driver, ".//td[contains(text () , 'planName')]/following-sibling::*/button");
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			
		} catch (Exception e) {
			
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	@Test(groups = { "training","Dashboard" })
	@TestCase(createdOn = "2018-07-03", updatedOn = "2018-07-03", testCaseDescription = "Verify The validation of Category At Admin Training Course Management Page", testCaseId = "TC_09_Training_DashBoard")
	private void validateDashboard8() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("training", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			fc.utobj().printTestStep("Entering the credential for validate user");
			driver = fc.loginpage().login(driver);
			String parentWindowHandle = driver.getWindowHandle();
			fc.utobj().printTestStep("Navigating to Admin > Training > Course Management Page");
			AdminTrainingCourseManagementAddSectionrPage pobj = new AdminTrainingCourseManagementAddSectionrPage(
					driver);
			AdminTrainingPlanAndCertificatePage pobjpnc = new AdminTrainingPlanAndCertificatePage(driver);
			AdminTrainingCoursesManagementPageTest courseManagement = new AdminTrainingCoursesManagementPageTest();
			fc.utobj().printTestStep("Add Category");
			String categoryname = courseManagement.addCategory(driver, dataSet);
			fc.utobj().printTestStep("Add Course");
			String coursename = courseManagement.addCourse(driver, dataSet, categoryname, testCaseId);
			fc.utobj().printTestStep("Create Lesson");
			fc.utobj().clickElement(driver, pobj.addLesson);
			
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			String sectionTitle = fc.utobj().generateTestData(dataSet.get("sectionTitle"));
			fc.utobj().sendKeys(driver, pobj.sectionTitle, sectionTitle);
			String summary = fc.utobj().generateTestData(dataSet.get("summary"));
			fc.utobj().sendKeys(driver, pobj.summary, summary);
			fc.utobj().clickElement(driver, pobj.addLesson);
			
			driver.switchTo().defaultContent();
			fc.utobj().clickElement(driver, pobj.uploadFile);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			String fileName = fc.utobj().getFilePathFromTestData("taskFile.pdf");
			fc.utobj().sendKeys(driver, pobj.fileuploader, fileName);
			
			AdminTrainingCourseManagementAddSectionPageTest objadd = new AdminTrainingCourseManagementAddSectionPageTest();
			objadd.addQuizBeforeCreatingLesson(driver, sectionTitle, dataSet, config, testCaseId);
			AdminTrainingPlanAndCertificateTest t1= new AdminTrainingPlanAndCertificateTest();
			
			t1.publishCourse(categoryname, coursename, driver, config, testCaseId);
			String Certificate = t1.CreateCertificate_indieMethod(driver, config, dataSet, testCaseId, pobjpnc);
			String planName = t1.CreatePlanwithCourseAndCertiicate(driver, config, dataSet, testCaseId, pobjpnc,
					coursename, Certificate);
			t1.publishplan(driver, config, dataSet, testCaseId, pobjpnc, coursename, planName);
			fc.training().training_common().trainingHome(driver);
			try
			{
				fc.utobj().clickElement(driver, "//a[contains(text(),'Plans to be Expired')]");
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			
		} catch (Exception e) {
			
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	@Test(groups = { "training","Dashboard" })
	@TestCase(createdOn = "2018-07-03", updatedOn = "2018-07-03", testCaseDescription = "Verify The validation of Category At Admin Training Course Management Page", testCaseId = "TC_10_Training_DashBoard")
	private void validateDashboard9() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("training", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			fc.utobj().printTestStep("Entering the credential for validate user");
			driver = fc.loginpage().login(driver);
			String parentWindowHandle = driver.getWindowHandle();
			fc.utobj().printTestStep("Navigating to Admin > Training > Course Management Page");
			AdminTrainingCourseManagementAddSectionrPage pobj = new AdminTrainingCourseManagementAddSectionrPage(
					driver);
			AdminTrainingPlanAndCertificatePage pobjpnc = new AdminTrainingPlanAndCertificatePage(driver);
			AdminTrainingCoursesManagementPageTest courseManagement = new AdminTrainingCoursesManagementPageTest();
			fc.utobj().printTestStep("Add Category");
			String categoryname = courseManagement.addCategory(driver, dataSet);
			fc.utobj().printTestStep("Add Course");
			String coursename = courseManagement.addCourse(driver, dataSet, categoryname, testCaseId);
			fc.utobj().printTestStep("Create Lesson");
			fc.utobj().clickElement(driver, pobj.addLesson);
			
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			String sectionTitle = fc.utobj().generateTestData(dataSet.get("sectionTitle"));
			fc.utobj().sendKeys(driver, pobj.sectionTitle, sectionTitle);
			String summary = fc.utobj().generateTestData(dataSet.get("summary"));
			fc.utobj().sendKeys(driver, pobj.summary, summary);
			fc.utobj().clickElement(driver, pobj.addLesson);
			
			driver.switchTo().defaultContent();
			fc.utobj().clickElement(driver, pobj.uploadFile);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			String fileName = fc.utobj().getFilePathFromTestData("taskFile.pdf");
			fc.utobj().sendKeys(driver, pobj.fileuploader, fileName);
			
			AdminTrainingCourseManagementAddSectionPageTest objadd = new AdminTrainingCourseManagementAddSectionPageTest();
			objadd.addQuizBeforeCreatingLesson(driver, sectionTitle, dataSet, config, testCaseId);
			AdminTrainingPlanAndCertificateTest t1= new AdminTrainingPlanAndCertificateTest();
			
			t1.publishCourse(categoryname, coursename, driver, config, testCaseId);
			String Certificate = t1.CreateCertificate_indieMethod(driver, config, dataSet, testCaseId, pobjpnc);
			String planName = t1.CreatePlanwithCourseAndCertiicate(driver, config, dataSet, testCaseId, pobjpnc,
					coursename, Certificate);
			t1.publishplan(driver, config, dataSet, testCaseId, pobjpnc, coursename, planName);
			fc.training().training_common().trainingHome(driver);
			try
			{
				fc.utobj().isElementPresent(driver, pobj.certificate);
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			
		} catch (Exception e) {
			
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

}
