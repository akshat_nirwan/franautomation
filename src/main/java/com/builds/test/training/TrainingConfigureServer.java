package com.builds.test.training;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.builds.uimaps.training.AdminTrainingCoureseManagementPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class TrainingConfigureServer extends FranconnectUtil {
	
	@Test(groups = { "training","topsearch" })
	@TestCase(createdOn = "2018-07-11", updatedOn = "2018-07-11", testCaseDescription = "Verify The validation of course in Top Search", testCaseId = "TC_Configure_Server_01")
	private void validateCourse() throws Exception {
		String testCaseId = utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = commonMethods().browsers().openBrowser();
		try {
			driver = loginpage().login(driver);
			AdminTrainingCoureseManagementPage pobj = new AdminTrainingCoureseManagementPage(driver);
			training().training_common().adminTraingConfigureServerPage(driver);	
			utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	@Test(groups = { "training","topsearch" })
	@TestCase(createdOn = "2018-07-11", updatedOn = "2018-07-11", testCaseDescription = "Verify The validation of course in Top Search", testCaseId = "TC_Configure_Server_02")
	private void validateCourse1() throws Exception {
		String testCaseId = utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = commonMethods().browsers().openBrowser();
		try {
			driver = loginpage().login(driver);
			AdminTrainingCoureseManagementPage pobj = new AdminTrainingCoureseManagementPage(driver);
			training().training_common().adminTraingConfigureServerPage(driver);	
			
			List<WebElement> list= driver.findElements(By.name("flag"));
			String str=list.get(0).getAttribute("checked");
			if (str.equalsIgnoreCase("true"))
			{
			    System.out.println("Checkbox selected");
			}
			utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			
			utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	@Test(groups = { "training" })
	@TestCase(createdOn = "2018-07-11", updatedOn = "2018-07-11", testCaseDescription = "Verify The validation of course in Top Search", testCaseId = "TC_Configure_Server_03")
	private void validateCourse2() throws Exception {
		String testCaseId = utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = commonMethods().browsers().openBrowser();
		try {
			driver = loginpage().login(driver);
			AdminTrainingCoureseManagementPage pobj = new AdminTrainingCoureseManagementPage(driver);
			training().training_common().adminTraingConfigureServerPage(driver);	
			List<String> list = new LinkedList<String>();
			list.add("FranConnect S3");
			list.add("FTP server");
			list.add("SFTP server");
			list.add("OneDrive");
			list.add("Customer S3");
			list.add("Google Drive");
			utobj().assertPageSourceWithMultipleRecords(driver, list);
			utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			
			utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

}
