package com.builds.test.training;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.uimaps.training.AdminTrainingCourseManagementAddSectionrPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class TrainingCourse {
	FranconnectUtil fc = new FranconnectUtil();
	
	@Test(groups = { "training" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Add Section Regular Type At Admin Training Course Management Page", testCaseId = "TC_Verify_Lesson_Dependency1")
	private void AddLessonDependancy() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("training", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigating to Admin > Training > Course Management Page");
			AdminTrainingCourseManagementAddSectionrPage pobj = new AdminTrainingCourseManagementAddSectionrPage(
					driver);
			AdminTrainingCoursesManagementPageTest courseManagement = new AdminTrainingCoursesManagementPageTest();
			fc.utobj().printTestStep("Add Category");
			String categoryname = courseManagement.addCategory(driver, dataSet);
			fc.utobj().printTestStep("Add Course");
			courseManagement.addCourse(driver, dataSet, categoryname, testCaseId);

			fc.utobj().printTestStep("Create Lesson");
			fc.utobj().clickElement(driver, pobj.addLesson);

			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//button[contains(text(),'Cancel')]"));
			driver.switchTo().defaultContent();
			fc.utobj().printTestStep("Create Lesson");
			fc.utobj().clickElement(driver, pobj.addLesson);

			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[contains(text(),'Create Lesson')]"));
			boolean test = fc.utobj().assertPageSource(driver, "This field is required.");
			if (!test) {
				fc.utobj().throwsException("Mandatory field msg is not coming ");
			} 
			boolean check = false;
			try {
				String fileName = fc.utobj()
						.getFilePathFromTestData("'Calypso'_Panorama_of_Spirit's_View_from_'Troy'.png");
				fc.utobj().sendKeys(driver, pobj.uploadcoverimage, fileName);
				check = true;

			} catch (Exception ex) {

			}
			if (check) {
				fc.utobj().throwsException("cover image with special char not uploaded");
			}
			try {
				String fileName = fc.utobj().getFilePathFromTestData("ShinChan####.png");
				fc.utobj().sendKeys(driver, pobj.uploadcoverimage, fileName);
				check = true;

			} catch (Exception ex) {

			}
			if (!check) {
				fc.utobj().throwsException("cover image not uploaded");
			}
			String sectionTitle1 = fc.utobj()
					.generateTestData(dataSet.get("sectionTitle") + fc.utobj().generateRandomNumber6Digit());
			fc.utobj().sendKeys(driver, pobj.sectionTitle, sectionTitle1);
			String summary = fc.utobj().generateTestData(dataSet.get("summary"));
			fc.utobj().sendKeys(driver, pobj.summary, summary);
			fc.utobj().clickElement(driver, pobj.addLesson);

			driver.switchTo().defaultContent();
			fc.utobj().clickElement(driver, pobj.uploadFile);

			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			// fc.utobj().clickElement(driver,
			// fc.utobj().getElementByXpath(driver,".//*[@id='fileUploader']//div[contains(text(),
			// 'Drop your files here')]")));
			String fileName = fc.utobj().getFilePathFromTestData("taskFile.pdf");
			fc.utobj().sendKeys(driver, pobj.fileuploader, fileName);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='saveButton']"));

			fc.utobj().printTestStep("Create Lesson");
			fc.utobj().clickElement(driver, pobj.addLesson);

			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			String sectionTitle2 = fc.utobj().generateTestData(dataSet.get("sectionTitle"));
			fc.utobj().sendKeys(driver, pobj.sectionTitle, sectionTitle2);
			summary = fc.utobj().generateTestData(dataSet.get("summary"));
			fc.utobj().sendKeys(driver, pobj.summary, summary);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					(".//*[@id='sectionCombo']/option[contains(text(),'" + sectionTitle1 + "')]")));
			fc.utobj().clickElement(driver, pobj.addLesson);

			driver.switchTo().defaultContent();
			fc.utobj().clickElement(driver, pobj.uploadFile);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			// fc.utobj().clickElement(driver,
			// fc.utobj().getElementByXpath(driver,".//*[@id='fileUploader']//div[contains(text(),
			// 'Drop your files here')]")));
			fileName = fc.utobj().getFilePathFromTestData("taskFile.pdf");

			fc.utobj().sendKeys(driver, pobj.fileuploader, fileName);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='saveButton']"));
			fc.utobj().printTestStep("Verify Add Section Regular Type");
			driver.switchTo().defaultContent();
			boolean isTextPresent = fc.utobj().assertPageSource(driver, sectionTitle1);
			if (isTextPresent == false) {
				fc.utobj().throwsException("Was not able to add Section!!!");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}

}
