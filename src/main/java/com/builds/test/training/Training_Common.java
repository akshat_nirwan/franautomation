package com.builds.test.training;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.builds.test.admin.AdminPageTest;
import com.builds.utilities.FranconnectUtil;

public class Training_Common {
	FranconnectUtil fc = new FranconnectUtil();

	public void adminTraingCourseManagementPage(WebDriver driver) throws Exception {
		// Reporter.log("Navigating to adminTraingCourseManagementPage...");
		fc.adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openCourseManagementLnk(driver);
	}

	public void adminTraingPlanAndCertificatesPage(WebDriver driver) throws Exception {
		// Reporter.log("Navigating to adminTraingCourseManagementPage...");
		fc.adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openPlanandCertificateLnk(driver);
	}

	public void adminTraingQuestionLibraryPage(WebDriver driver) throws Exception {
		// Reporter.log("Navigating to adminTraingQuestionLibraryPage...");
		fc.adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openQuestionLibraryTrainingLnk(driver);
	}

	public void adminTraingConfigureServerPage(WebDriver driver) throws Exception {
		// Reporter.log("Navigating to adminTraingConfigureServerPage...");
		fc.adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openConfigureServerTrainginLnk(driver);
	}

	public void adminTraingConfigureInviteEmailPage(WebDriver driver) throws Exception {
		// Reporter.log("Navigating to adminTraingConfigureInviteEmailPage...");
		fc.adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openConfigureInviteEmail(driver);
	}

	public void trainingModule(WebDriver driver) throws Exception {

		fc.home_page().openTrainingModule(driver);
	}

	public void trainingHome(WebDriver driver) throws Exception {
		// Reporter.log("Navigating to trainingHomeModule...");
		trainingModule(driver);

		fc.utobj().sleep(30000);
		fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, "Dashboard"));
	}

	public void trainingPlans(WebDriver driver) throws Exception {
		// Reporter.log("Navigating to trainingHomeModule...");
		trainingModule(driver);

		fc.utobj().sleep(30000);
		fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, "Plans"));
	}

	public void trainingReports(WebDriver driver) throws Exception {
		// Reporter.log("Navigating to trainingHomeModule...");
		trainingModule(driver);
		fc.utobj().sleep(30000);
		fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, "Reports"));
	}

	public void trainingCourse(WebDriver driver) throws Exception {
		// Reporter.log("Navigating to trainingHomeModule...");
		trainingModule(driver);

		fc.utobj().sleep(30000);
		fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, "Courses"));
	}

	public void trainingAssessment(WebDriver driver) throws Exception {
		// Reporter.log("Navigating to trainingHomeModule...");
		trainingModule(driver);

		fc.utobj().sleep(30000);
		fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, "Assessments"));
	}

	public void trainingMyResults(WebDriver driver) throws Exception {
		// Reporter.log("Navigating to trainingHomeModule...");
		trainingModule(driver);

		fc.utobj().sleep(30000);
		fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, "My Results"));
	}

	public void trainingEvents(WebDriver driver) throws Exception {
		// Reporter.log("Navigating to trainingEventsModule...");
		trainingModule(driver);

		fc.utobj().sleep(30000);
		fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, "Events"));
	}

	// select ROle In Role Based Acess

	void selectRole(WebDriver driver, String roleName, String typeOfRole) throws Exception {
		if (typeOfRole.equalsIgnoreCase("Corporate")) {
			WebElement element = fc.utobj().getElementByID(driver, "fc-drop-parentcorRoles");
			fc.utobj().clickElement(driver, element);
			WebElement element1 = element
					.findElement(By.xpath("./button/parent::div/div//input[@class='searchInputMultiple']"));
			fc.utobj().sendKeys(driver, element1, roleName);
			fc.utobj().sleep(2000);
			fc.utobj().clickElement(driver, ".//input[@class='search-btn on']");
			fc.utobj().clickElementWithoutMove(driver, element.findElement(By.xpath(".//input[@id='selectAll']/parent::*")));
			fc.utobj().clickElement(driver, element);
		} else if (typeOfRole.equalsIgnoreCase("Divisional")) {
			WebElement element = fc.utobj().getElementByID(driver, "fc-drop-parentdivisionalRoles");
			fc.utobj().clickElement(driver, element);
			WebElement element1 = element
					.findElement(By.xpath("./button/parent::div/div//input[@class='searchInputMultiple']"));
			fc.utobj().sendKeys(driver, element1, roleName);
			fc.utobj().sleep(2000);
			fc.utobj().clickElement(driver, ".//input[@class='search-btn on']");
			fc.utobj().clickElementWithoutMove(driver, element.findElement(By.xpath(".//input[@id='selectAll']/parent::*")));
			fc.utobj().clickElement(driver, element);
		} else if (typeOfRole.equalsIgnoreCase("Regional")) {
			WebElement element = fc.utobj().getElementByID(driver, "fc-drop-parentregionalRoles");
			fc.utobj().clickElement(driver, element);
			WebElement element1 = element
					.findElement(By.xpath("./button/parent::div/div//input[@class='searchInputMultiple']"));
			fc.utobj().sendKeys(driver, element1, roleName);
			fc.utobj().sleep(2000);
			fc.utobj().clickElement(driver, ".//input[@class='search-btn on']");
			fc.utobj().clickElementWithoutMove(driver, element.findElement(By.xpath(".//input[@id='selectAll']/parent::*")));
			fc.utobj().clickElement(driver, element);
		} else if (typeOfRole.equalsIgnoreCase("Franchise")) {
			WebElement element = fc.utobj().getElementByID(driver, "fc-drop-parentroles");
			fc.utobj().clickElement(driver, element);
			WebElement element1 = element
					.findElement(By.xpath("./button/parent::div/div//input[@class='searchInputMultiple']"));
			fc.utobj().sendKeys(driver, element1, roleName);
			fc.utobj().sleep(2000);
			fc.utobj().clickElement(driver, ".//input[@class='search-btn on']");
			fc.utobj().clickElementWithoutMove(driver, element.findElement(By.xpath(".//input[@id='selectAll']/parent::*")));
			fc.utobj().clickElement(driver, element);
		}
	}

}
