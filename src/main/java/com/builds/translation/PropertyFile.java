package com.builds.translation;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class PropertyFile {

	@Test
	@Parameters({ "folderPath", "destFolderPath" })
	public void parsePropertyFile(@Optional String folderPath, @Optional String destFolderPath) throws Exception {
		File folder = new File(folderPath);

		File destFolder = new File(destFolderPath);
		if (destFolder.exists() == false) {
			destFolder.mkdirs();
		}

		if (folder.exists() == true) {
			File[] files = folder.listFiles();
			for (File propFile : files) {
				if (propFile.getAbsolutePath().endsWith(".properties")) {
					String dest = propFile.getAbsolutePath().replace(folder.getAbsolutePath(), "");
					dest = dest.replace(".properties", "");
					printPropertyFileToExcel(propFile, destFolderPath + "\\" + dest + ".xls");
				}
			}
		}
	}

	public void printPropertyFileToExcel(File source, String dest) throws Exception {

		FileOutputStream fout = new FileOutputStream(dest);
		Workbook wb = new HSSFWorkbook();
		Sheet sh = wb.createSheet("Language");
		Row row = sh.createRow(0);
		row.createCell(0).setCellValue("Key");
		row.createCell(1).setCellValue("Value");

		try (BufferedReader br = new BufferedReader(new FileReader(source))) {
			String sCurrentLine;
			while ((sCurrentLine = br.readLine()) != null) {
				if (sCurrentLine.contains("=") && !sCurrentLine.startsWith("#")) {
					String[] keyval = sCurrentLine.split("=");
					row = sh.createRow(sh.getLastRowNum() + 1);
					row.createCell(0).setCellValue(keyval[0].replace("\\", ""));
					row.createCell(1).setCellValue(keyval[1]);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		sh.autoSizeColumn(0);
		sh.autoSizeColumn(1);
		wb.write(fout);
		wb.close();
		fout.close();
		fout.close();

	}
}
