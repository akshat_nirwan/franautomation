package com.builds.translation;

import java.io.File;
import java.io.FileInputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

public class XMLFile {

	@Test
	@Parameters({ "folderName", "loopUpFile" })
	public void readAllFilesInFolder(@Optional String folderName, @Optional String loopUpFile) throws Exception {
		File folder = new File(folderName);
		if (folder.exists() == true) {
			getAllFiles(folder, loopUpFile);
		}
	}

	public void getAllFiles(File folder, String loopUpFile) throws Exception {
		File[] files = folder.listFiles();
		for (File tempFiles : files) {
			if (tempFiles.isDirectory()) {
				getAllFiles(tempFiles, loopUpFile);
			}
			if (tempFiles.getAbsolutePath().contains(".xml")) {
				readXMLDisplayFieldColumn(tempFiles, loopUpFile);
			}
		}
	}

	public void readXMLDisplayFieldColumn(File fileName, String loopUpFile) throws Exception {
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(fileName);
		doc.getDocumentElement().normalize();

		try {
			NodeList nList = doc.getElementsByTagName("display-name");
			// System.out.println(fileName.getAbsolutePath() +" :
			// "+nList.getLength());

			for (int x = 0; x < nList.getLength(); x++) {
				// System.out.println("Display Name :
				// "+nList.item(x).getTextContent());
				verifyTheDisplayNameIsInXLS(fileName.getAbsolutePath(), nList.item(x).getTextContent(), loopUpFile);
			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public void verifyTheDisplayNameIsInXLS(String xmlFile, String displayName, String lookUpFiles) throws Exception {

		File folder = new File(lookUpFiles);
		File[] files = folder.listFiles();
		boolean isTranslationFound = false;
		for (File f : files) {
			if (f.getAbsolutePath().endsWith(".xls")) {
				FileInputStream fis = new FileInputStream(f.getAbsolutePath());
				Workbook wb = new HSSFWorkbook(fis);
				Sheet sh = wb.getSheetAt(0);
				Row row = null;
				Cell cell = null;

				for (int x = 1; x < sh.getLastRowNum(); x++) {
					row = sh.getRow(x);
					cell = row.getCell(0);
					if (cell.getStringCellValue().trim().equalsIgnoreCase(displayName.trim())) {
						isTranslationFound = true;
					}
				}
				wb.close();
				fis.close();
			}
		}

		if (isTranslationFound == false) {
			System.out.println(xmlFile + " -> " + displayName + " -> " + lookUpFiles);
		}
	}
}
