package com.builds.uimaps.admin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AddCorporateUser_Testing_UI {

	@FindBy(id="userName")
	public WebElement userNameTxt;
	
	@FindBy(xpath=".//input[@id='password' and @type='password']")
	public WebElement passwordTxt;
	
	@FindBy(id="confirmPassword")
	public WebElement confirmPassword_Txt;
	
	@FindBy(id="firstName")
	public WebElement firstNameTxt;
	
	@FindBy(id="lastName")
	public WebElement lastNameTxt;
	
	@FindBy(id="city")
	public WebElement cityTxt;
	
	@FindBy(id="country")
	public WebElement countryDrp;
	
	@FindBy(id="state")
	public WebElement stateDrp;
	
	@FindBy(id="phone1")
	public WebElement phone1;
	
	@FindBy(id="email")
	public WebElement email;
	
	@FindBy(id="Submit")
	public WebElement SubmitButton;
	
	public AddCorporateUser_Testing_UI(WebDriver driver)
	{
		PageFactory.initElements(driver, this);
	}
	
}
