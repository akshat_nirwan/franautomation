package com.builds.uimaps.admin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.builds.utilities.FranconnectUtil;

public class AddNewRoleUI {
	
	/*
	 *    Akshat
	 */
	
	@FindBy(name="roleType")
	public WebElement roleType_DropDown;
	
	@FindBy(xpath=".//input[@type='text' and @name='roleName']")
	public WebElement roleName_TextBox;
	
	@FindBy(xpath=".//input[@type='checkbox' and @name='privilege223']")
	public WebElement canAccessAllLeads_checkBox;
	
	public WebElement checkBoxAgainst_PrivilegeDescription(WebDriver driver, String privilegeDescription) throws Exception
	{
		FranconnectUtil fc = new FranconnectUtil();
	//	WebElement checkbox = fc.utobj().getElementByXpath(driver, ".//td[@class='bText12' and contains(text(),'"+privilegeDescription+"')]/ancestor::td[@class='botBorder']/following-sibling::td/input");
		WebElement checkbox = fc.utobj().getElementByXpath(driver, ".//td[.='Can Access Sales Information']/ancestor::tr[@class='thead bText12b']/following-sibling::tr//td[.='"+privilegeDescription+"']/ancestor::td[@class='botBorder']/following-sibling::td/input");
		return checkbox;
	}
	
	@FindBy(xpath=".//input[@type='button' and @name='Submit']")
	public WebElement submit_Btn;
	
	public AddNewRoleUI(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
