package com.builds.uimaps.admin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminAccessControlDeletedLogsPage {

	@FindBy(id = "module")
	public WebElement recordTypeName;

	@FindBy(id = "searchButton")
	public WebElement searchBtn;

	public AdminAccessControlDeletedLogsPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
