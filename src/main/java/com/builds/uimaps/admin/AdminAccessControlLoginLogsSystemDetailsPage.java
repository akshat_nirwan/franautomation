package com.builds.uimaps.admin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminAccessControlLoginLogsSystemDetailsPage {

	/*** Users Logged In ****/
	@FindBy(linkText = "Users Logged In")
	public WebElement usersLoggedIn ;

	@FindBy(id = "fromDate")
	public WebElement fromDate;

	@FindBy(id = "lastDate")
	public WebElement toDate;

	@FindBy(id = "userType")
	public WebElement userType;

	@FindBy(id = "userdetails")
	public WebElement userdetails;

	@FindBy(id = "searchButton")
	public WebElement searchBtn;

	@FindBy(linkText = "Show All")
	public WebElement showAllLnk ;

	@FindBy(xpath = ".//a[@qat_tabname='Users not Logged In']")
	public WebElement usersNotLoggedInTab ;

	@FindBy(xpath = ".//a[@qat_tabname='Current Logged In Users']")
	public WebElement currentLoggedUserTab ;

	@FindBy(xpath = ".//a[@qat_tabname='Users Logged In']")
	public WebElement userLoggedInTab ;

	public AdminAccessControlLoginLogsSystemDetailsPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
