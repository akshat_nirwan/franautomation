package com.builds.uimaps.admin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminAccessControlSearchedLogsPage {

	@FindBy(id="searchText")
	public WebElement searchedText;
	
	@FindBy(id="matchType")
	public WebElement searchedDate;
	
	@FindBy(id="search")
	public WebElement searchBtn;
	
	public AdminAccessControlSearchedLogsPage(WebDriver driver){
		PageFactory.initElements(driver, this);
	}
	
	
	
}
