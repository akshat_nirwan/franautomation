package com.builds.uimaps.admin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminAccessControlUserAccountSummaryPage {

	public AdminAccessControlUserAccountSummaryPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "matchType")
	public WebElement enquiryDateSelect;

	@FindBy(xpath = ".//a[contains(@href,'acloginDetailActiveUsers?user=0')]")
	public WebElement corporateUserCount ;

	@FindBy(xpath = ".//a[contains(@href,'acloginDetailActiveUsers?user=2')]")
	public WebElement regionalUserCount ;

	@FindBy(xpath = ".//a[contains(@href,'acloginDetailActiveUsers?user=6')]")
	public WebElement divisionalUserCount ;

	@FindBy(xpath = ".//a[contains(@href,'acloginDetailActiveUsers?user=1')]")
	public WebElement franchiseUserCount ;

	@FindBy(name = "user")
	public WebElement userSelect;

	@FindBy(id = "search")
	public WebElement viewReport;

}
