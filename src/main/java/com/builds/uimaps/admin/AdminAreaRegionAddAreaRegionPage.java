package com.builds.uimaps.admin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

public class AdminAreaRegionAddAreaRegionPage {
	WebDriver driver;

	@FindBy(id = "category")
	public WebElement category;

	@FindBy(name = "regioname")
	public WebElement aregRegionName;

	@FindBy(name = "groupBy")
	public WebElement groupBy;

	@FindBy(id = "all")
	public WebElement all;

	@FindBy(xpath = ".//*[@id='domestic']/a[@title='Show All']/u")
	public WebElement showAll;

	@FindBy(xpath = ".//input[@name='zipmode' and @value='1']")
	public WebElement commaSeparated ;

	@FindBy(name = "ziplist")
	public WebElement ziplist;

	@FindBy(name = "button")
	public WebElement Submit;

	@FindBy(name = "regionKey")
	public WebElement regionDrp;

	@FindBy(xpath = ".//*[@id='searchButton']")
	public WebElement searchBtn ;

	@FindBy(xpath = ".//input[@type='submit' and contains(@value , 'Proceed')]")
	public WebElement proceedBtn ;

	@FindBy(xpath = ".//input[@type='button' and contains(@value,'Next')]")
	public WebElement nextBtn ;

	@FindBy(xpath = ".//input[@value='Back']")
	public WebElement backBtn ;

	@FindBy(xpath = ".//td[contains(text(),'The following Zip Codes overlap with other')]")
	public WebElement displayBtn ;

	@FindBy(xpath = ".//*[@id='0_1']")
	public WebElement firstStateChkBox;

	public AdminAreaRegionAddAreaRegionPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

}
