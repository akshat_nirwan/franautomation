package com.builds.uimaps.admin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminAreaRegionManageAreaRegionPage {
	@FindBy(name = "regionKey")
	public WebElement areaRegionDrp;

	@FindBy(id = "searchButton")
	public WebElement searchBtn;

	@FindBy(xpath = ".//*[@title='Show All']")
	public WebElement lnkShowAll ;

	public AdminAreaRegionManageAreaRegionPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
