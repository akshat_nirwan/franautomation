package com.builds.uimaps.admin;

import org.openqa.selenium.WebDriver;

public class AdminAreaRegionManagementPages {

	WebDriver driver;

	public AdminAreaRegionManagementPages(WebDriver driver) {
		this.driver = driver;
	}

	public AdminAreaRegionAddAreaRegionPage getAddAreaRegionPage() {
		return new AdminAreaRegionAddAreaRegionPage(driver);
	}

	public AdminAreaRegionManageAreaRegionPage getManageAreaRegionPage() {
		return new AdminAreaRegionManageAreaRegionPage(driver);
	}

}
