package com.builds.uimaps.admin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminConfigurationConfigureCalendarStartDayPage {

	@FindBy(xpath = ".//input[@id='startDay' and @value='M']")
	public WebElement monday ;

	@FindBy(name = "Save")
	public WebElement saveBtn;

	public AdminConfigurationConfigureCalendarStartDayPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
