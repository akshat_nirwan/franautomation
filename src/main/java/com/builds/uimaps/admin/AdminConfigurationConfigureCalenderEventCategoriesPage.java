package com.builds.uimaps.admin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminConfigurationConfigureCalenderEventCategoriesPage {

	@FindBy(xpath = ".//input[@value='Add Category']")
	public WebElement addCategoryBtn ;

	@FindBy(id = "scheduleCategoryName")
	public WebElement categoryName;

	@FindBy(xpath = ".//input[@value='Add']")
	public WebElement addBtn ;

	public AdminConfigurationConfigureCalenderEventCategoriesPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
