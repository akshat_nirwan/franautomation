package com.builds.uimaps.admin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminConfigurationConfigureCallStatusPage {

	@FindBy(xpath = ".//input[@value='Add Call Status']")
	public WebElement addCallStatus ;

	@FindBy(name = "templateOrder")
	public WebElement listing;

	@FindBy(name = "callStatusName")
	public WebElement callStatusName;

	/*
	 * @FindBy(name="Add") public WebElement addBtn;
	 */

	@FindBy(xpath = ".//input[@name='Add']")
	public WebElement addBtn ;

	@FindBy(xpath = ".//input[@value='Modify']")
	public WebElement modifyBtn ;

	@FindBy(xpath = ".//input[@value='Delete']")
	public WebElement deleteBtn ;

	@FindBy(xpath = ".//input[@value='Close']")
	public WebElement closeBtn ;

	@FindBy(xpath = ".//img[@title='Move Up']")
	public WebElement moveUp ;

	@FindBy(xpath = ".//input[@value='Change Status Sequence']")
	public WebElement changeTypeSequence ;

	public AdminConfigurationConfigureCallStatusPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
