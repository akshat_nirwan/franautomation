package com.builds.uimaps.admin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminConfigurationConfigureDecimalPlacePage {

	@FindBy(xpath = ".//input[@id='dataValue' and @value='1']")
	public WebElement decimal1 ;

	@FindBy(xpath = ".//input[@id='dataValue' and @value='2']")
	public WebElement decimal2 ;

	@FindBy(xpath = ".//input[@id='dataValue' and @value='3']")
	public WebElement decimal3 ;

	@FindBy(xpath = ".//input[@id='dataValue' and @value='4']")
	public WebElement decimal4 ;

	@FindBy(name = "Save2")
	public WebElement updateBtn;

	public AdminConfigurationConfigureDecimalPlacePage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
