package com.builds.uimaps.admin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminConfigurationConfigureDefaultFromReturnPathforEmailsPage {
	@FindBy(id = "mailFrom")
	public WebElement fromEmail;

	@FindBy(xpath = ".//input[@id='isSame' and @value='0']")
	public WebElement returnPathSameAsFromEmail ;

	@FindBy(xpath = ".//input[@id='isSame' and @value='1']")
	public WebElement returnPathCustom ;

	@FindBy(id = "newFooter_ifr")
	public WebElement mailBodyFrame;

	@FindBy(xpath = "html/body")
	public WebElement mailText ;

	@FindBy(name = "submit")
	public WebElement modifyBtn;

	public AdminConfigurationConfigureDefaultFromReturnPathforEmailsPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
