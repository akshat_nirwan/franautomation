package com.builds.uimaps.admin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminConfigurationConfigureDictionaryPage {
	@FindBy(xpath = ".//*[@id='form1']/table/tbody/tr[1]/td/table/tbody/tr/td[2]/input")
	public WebElement addNewWords ;

	@FindBy(id = "wordName")
	public WebElement wordName;

	/*
	 * @FindBy(name="Add") public WebElement addBtn;
	 */

	@FindBy(xpath = ".//input[@name='Add']")
	public WebElement addBtn ;

	@FindBy(xpath = ".//input[@value='Close']")
	public WebElement closeBtn ;

	@FindBy(id = "search")
	public WebElement searchField;

	@FindBy(id = "searchButton")
	public WebElement searchButton;

	@FindBy(name = "delete")
	public WebElement delete;

	public AdminConfigurationConfigureDictionaryPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
