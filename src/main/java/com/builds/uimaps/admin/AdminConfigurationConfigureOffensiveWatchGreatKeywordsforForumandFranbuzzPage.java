package com.builds.uimaps.admin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminConfigurationConfigureOffensiveWatchGreatKeywordsforForumandFranbuzzPage {

	@FindBy(xpath = ".//a[contains(@href , 'configureOffensiveKeywords')]")
	public WebElement offensiveWord ;

	@FindBy(xpath = ".//select[@id='templateOrder1']/parent::td/table//td/a[@class='actBtn1' and contains(text(),'Add')]")
	public WebElement addOffensiveWord ;

	@FindBy(xpath = ".//select[@id='templateOrder1']/parent::td/table//td/a[@class='actBtn2' and contains(text(),'Modify')]")
	public WebElement modifyOffensiveWord ;

	@FindBy(xpath = ".//select[@id='templateOrder1']/parent::td/table//td/a[@class='actBtn3' and contains(text(),'Remove')]")
	public WebElement removeyOffensiveWord ;

	@FindBy(xpath = ".//select[@id='templateOrder2']/parent::td/table//td/a[@class='actBtn1' and contains(text(),'Add')]")
	public WebElement addWatchWord ;

	@FindBy(xpath = ".//select[@id='templateOrder2']/parent::td/table//td/a[@class='actBtn2' and contains(text(),'Modify')]")
	public WebElement modifyWatchWord ;

	@FindBy(xpath = ".//select[@id='templateOrder2']/parent::td/table//td/a[@class='actBtn3' and contains(text(),'Remove')]")
	public WebElement removeyWatchWord ;

	@FindBy(xpath = ".//select[@id='templateOrder3']/parent::td/table//td/a[@class='actBtn1' and contains(text(),'Add')]")
	public WebElement addGreatWord ;

	@FindBy(xpath = ".//select[@id='templateOrder3']/parent::td/table//td/a[@class='actBtn2' and contains(text(),'Modify')]")
	public WebElement modifyGreatWord ;

	@FindBy(xpath = ".//select[@id='templateOrder3']/parent::td/table//td/a[@class='actBtn3' and contains(text(),'Remove')]")
	public WebElement removeyGreatWord ;

	@FindBy(id = "o_enable")
	public WebElement oEnable;

	@FindBy(id = "w_enable")
	public WebElement wEnable;

	@FindBy(id = "g_enable")
	public WebElement gEnable;

	@FindBy(id = "templateOrder1")
	public WebElement selectOffensiveWord;

	@FindBy(id = "templateOrder2")
	public WebElement selectWatchWord;

	@FindBy(id = "templateOrder3")
	public WebElement selectGreatWord;

	@FindBy(name = "submit3")
	public WebElement saveBtn;

	@FindBy(xpath = ".//input[@name='Add' and contains(@value,'Cancel')]")
	public WebElement cancleBtn ;

	@FindBy(xpath = ".//a[@href='offensiveEmailContacts']")
	public WebElement offensiveEmailContactTab ;

	@FindBy(name = "keyword")
	public WebElement keywordName;

	@FindBy(name = "Add")
	public WebElement addBtn;

	@FindBy(xpath = ".//input[@value='Modify']")
	public WebElement modifyCboxBtn ;

	@FindBy(xpath = ".//input[@value='Close']")
	public WebElement closeBtn ;

	@FindBy(xpath = ".//input[@value='Add New Contact']")
	public WebElement addNewContact ;

	@FindBy(id = "addContact")
	public WebElement newUserRBtn;

	@FindBy(id = "existContact")
	public WebElement existUserRBtn;

	@FindBy(id = "firstName")
	public WebElement firstName;

	@FindBy(id = "lastName")
	public WebElement lastName;

	@FindBy(id = "emailID")
	public WebElement emailID;

	@FindBy(name = "chkb1")
	public WebElement franBuzzPostChk;

	@FindBy(name = "chkb2")
	public WebElement forumPostChk;

	@FindBy(xpath = ".//input[@value='Create' and @type='submit']")
	public WebElement createBtn ;

	@FindBy(xpath = ".//input[@value='Save']")
	public WebElement saveBtnAtCbox ;

	@FindBy(id = "corporateuser")
	public WebElement corporateuser;

	public AdminConfigurationConfigureOffensiveWatchGreatKeywordsforForumandFranbuzzPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
