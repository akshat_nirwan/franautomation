package com.builds.uimaps.admin;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminConfigurationConfigurePasswordSettingsPage {

	@FindBy(name = "isForcelyChangePasswordOn")
	public List<WebElement> isForcelyChangePassword;

	@FindBy(name = "frequentlyChangePassword")
	public List<WebElement> frequentlyChangePassword;

	@FindBy(id = "minChars")
	public WebElement minNoOfChar;

	@FindBy(id = "maxChars")
	public WebElement maxNoOfChar;

	@FindBy(name = "isEnable")
	public List<WebElement> useSpecialAndCapital;

	@FindBy(id = "specialChars")
	public WebElement minNoOfSpecialChar;

	@FindBy(id = "capitalChars")
	public WebElement minNoOfCapitalChar;

	@FindBy(id = "noOfDigits")
	public WebElement minNoOfDigit;

	@FindBy(id = "noOfPasswordUsed")
	public WebElement noOfPasswordUsed;

	@FindBy(id = "Submit")
	public WebElement save;

	@FindBy(xpath = ".//input[contains(@value , 'No')]")
	public WebElement frameNo ;

	@FindBy(xpath = ".//*[@id='mainHeaderPart']//div[@class='header-logo']")
	public WebElement logo ;

	@FindBy(name = "changeLockUser")
	public List<WebElement> changeLockUser;

	public AdminConfigurationConfigurePasswordSettingsPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
