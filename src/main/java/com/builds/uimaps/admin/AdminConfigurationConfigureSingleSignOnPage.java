package com.builds.uimaps.admin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminConfigurationConfigureSingleSignOnPage {

	public AdminConfigurationConfigureSingleSignOnPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	@FindBy(linkText = "Add New Link")
	public WebElement addNewLink ;

	@FindBy(xpath = ".//a[@qat_tabname='Links']")
	public WebElement lnksTab ;

	@FindBy(xpath = ".//*[@id='samlAuthentication' and @value='Y']")
	public WebElement samlAuthenticationYes ;

	@FindBy(xpath = ".//*[@id='samlAuthentication' and @value='N']")
	public WebElement samlAuthenticationNo ;

	@FindBy(id = "rHome")
	public WebElement thehubHome;

	@FindBy(name = "title")
	public WebElement title;

	@FindBy(id = "description")
	public WebDriver description;

	@FindBy(name = "imageName")
	public WebElement imageName;

	@FindBy(id = "targetURL")
	public WebElement targetURL;

	@FindBy(xpath = ".//tr[@id='newtr3']//input[@name='singleSignOn' and @value='Y']")
	public WebElement loginAuthinticationYes ;

	@FindBy(xpath = ".//tr[@id='newtr3']//input[@name='singleSignOn' and @value='N']")
	public WebElement loginAuthinticationNo ;

	@FindBy(xpath = ".//tr[@id='newtr5']/td[@id='newtr9']/input[@name='method' and @value='G']")
	public WebElement methodGet ;

	@FindBy(xpath = ".//tr[@id='newtr5']/td[@id='newtr9']/input[@name='method' and @value='P']")
	public WebElement methodPost ;

	@FindBy(xpath = ".//input[1][@name='tokenName_1']")
	public WebElement urlTocken1 ;

	@FindBy(xpath = ".//input[2][@name='tokenName_1']")
	public WebElement urlTocken2 ;

	@FindBy(xpath = ".//input[@value='Continue']")
	public WebElement continueBtn ;

	@FindBy(xpath = ".//input[@value='Skip Test']")
	public WebElement skipTestBtn ;

	@FindBy(xpath = ".//input[@value='now']")
	public WebElement nowRdBtn ;

	@FindBy(xpath = ".//input[@value='later']")
	public WebElement laterRdBtn ;

	@FindBy(xpath = ".//input[@name='Button3']")
	public WebElement submitBtn;

	@FindBy(xpath = ".//img[@alt='Search Corporate Users']")
	public WebElement searchCorporateImgBtn ;

	@FindBy(xpath = ".//input[@name='Submit' and @value='Add']")
	public WebElement addBtn ;

	@FindBy(id = "CchkMain")
	public WebElement CchkMain;

	@FindBy(xpath = ".//input[@name='Save' and @value='Save']")
	public WebElement saveBtn ;

	@FindBy(id = "searchCorpUsers")
	public WebElement searchCorpUsers;

}
