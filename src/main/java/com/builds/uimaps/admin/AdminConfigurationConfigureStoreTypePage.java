package com.builds.uimaps.admin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminConfigurationConfigureStoreTypePage {

	@FindBy(xpath = ".//input[@value='Add Store Type']")
	public WebElement addStoreType ;

	@FindBy(name = "templateOrder")
	public WebElement listing;

	@FindBy(name = "stName")
	public WebElement storeType;

	/*
	 * @FindBy(name="Add") public WebElement addBtn=DDDDDDDDDDDDDDDDDDDDD
	 */

	@FindBy(xpath = ".//input[(@name='Add' or @name='Ajouter') and @value='Add']")
	public WebElement addBtn ;

	@FindBy(xpath = ".//input[@value='Close']")
	public WebElement closeBtn ;

	@FindBy(xpath = ".//input[@value='Delete']")
	public WebElement deleteBtn ;

	@FindBy(xpath = ".//input[@value='Modify']")
	public WebElement modifyBtn ;

	@FindBy(xpath = ".//input[(@name='Add' or @name='Ajouter') and @value='Cancel']")
	public WebElement cancelBtn ;

	@FindBy(xpath = ".//img[@title='Move Up']")
	public WebElement moveUp ;

	@FindBy(xpath = ".//input[@value='Change Type Sequence']")
	public WebElement changeTypeSequence ;

	public AdminConfigurationConfigureStoreTypePage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
