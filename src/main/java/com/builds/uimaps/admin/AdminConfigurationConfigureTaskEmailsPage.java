package com.builds.uimaps.admin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminConfigurationConfigureTaskEmailsPage {

	public AdminConfigurationConfigureTaskEmailsPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = ".//a[@href='configureCompletionMail']")
	public WebElement configureTaskCompletionTab ;

	@FindBy(xpath = ".//input[@name='isConfigure_Yes' and @value='Y']")
	public WebElement sendEmailNotificationYes ;

	@FindBy(name = "subject")
	public WebElement subject;

	@FindBy(name = "Submit")
	public WebElement configureBtn;

}
