package com.builds.uimaps.admin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminConfigurationConfigureTaskTypePage {
	@FindBy(xpath = ".//input[@value='Add Task Type']")
	public WebElement addTaskType ;

	@FindBy(name = "templateOrder")
	public WebElement listing;

	@FindBy(name = "ttName")
	public WebElement taskType;

	/*
	 * @FindBy(name="Add") public WebElement addBtn;
	 */

	@FindBy(xpath = ".//input[@name='Add' or @name='Ajouter']")
	public WebElement addBtn ;

	@FindBy(xpath = ".//input[@value='Modify']")
	public WebElement modifyBtn ;

	@FindBy(xpath = ".//input[@value='Delete']")
	public WebElement deleteBtn ;

	@FindBy(xpath = ".//input[@value='Close']")
	public WebElement closeBtn ;

	@FindBy(xpath = ".//img[@title='Move Up']")
	public WebElement moveUp ;

	@FindBy(xpath = ".//input[@value='Change Type Sequence']")
	public WebElement changeTypeSequence ;

	@FindBy(xpath = ".//input[@value='Cancel']")
	public WebElement cancelBtn ;

	public AdminConfigurationConfigureTaskTypePage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
