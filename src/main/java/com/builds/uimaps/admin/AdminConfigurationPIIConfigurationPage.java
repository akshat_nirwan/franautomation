package com.builds.uimaps.admin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminConfigurationPIIConfigurationPage {

	@FindBy(xpath = ".//input[@name='passwordFunctionality' and @value='off']")
	public WebElement passwordFunctionalityOff ;

	@FindBy(xpath = ".//input[@name='passwordFunctionality' and @value='on']")
	public WebElement passwordFunctionalityOn ;

	@FindBy(name = "confirmPassword")
	public WebElement confirmPasswordText;

	@FindBy(name = "oldPassword")
	public WebElement oldPassword;

	@FindBy(name = "newpassword")
	public WebElement newpassword;

	@FindBy(name = "Save")
	public WebElement saveBtn;

	public AdminConfigurationPIIConfigurationPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
