package com.builds.uimaps.admin;

import org.openqa.selenium.WebDriver;

public class AdminConfigurationPages {

	WebDriver driver;

	public AdminConfigurationPages(WebDriver driver) {
		this.driver = driver;
	}

	public AdminConfigurationConfigureStoreTypePage getConfigureStoreTypePage() {
		return new AdminConfigurationConfigureStoreTypePage(driver);
	}

}
