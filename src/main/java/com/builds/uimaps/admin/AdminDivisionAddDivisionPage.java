package com.builds.uimaps.admin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminDivisionAddDivisionPage {

	@FindBy(name = "divisionName")
	public WebElement DivisionName;

	@FindBy(id = "selectAllCountries")
	public WebElement selectAllCountries;

	@FindBy(name = "zipmode")
	public WebElement ZipCode;

	@FindBy(name = "ziplist")
	public WebElement zipTextArea;

	@FindBy(name = "button")
	public WebElement submit;

	@FindBy(id = "ms-parentdivisionKey")
	public WebElement selectDivisionName;

	@FindBy(id = "matchType1")
	public WebElement creationDateSelect;

	@FindBy(id = "searchButton")
	public WebElement searchButton;
	
	@FindBy(xpath=".//input[@name='zipmode' and @value='1']")
	public WebElement zipMode;
	
	@FindBy(xpath=".//textarea[@name='ziplist']")
	public WebElement zipList;

	public AdminDivisionAddDivisionPage(WebDriver driver) {
		PageFactory.initElements(driver, this);

	}
}
