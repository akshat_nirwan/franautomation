package com.builds.uimaps.admin;

import org.openqa.selenium.WebDriver;

public class AdminDivisionManagementPages {

	WebDriver driver;

	public AdminDivisionManagementPages(WebDriver driver) {
		this.driver = driver;
	}

	public AdminDivisionAddDivisionPage getAddDivisionPage() {
		return new AdminDivisionAddDivisionPage(driver);
	}

	public AdminDivisionManageDivisionPage getManageDivisionPage() {
		return new AdminDivisionManageDivisionPage(driver);
	}

}
