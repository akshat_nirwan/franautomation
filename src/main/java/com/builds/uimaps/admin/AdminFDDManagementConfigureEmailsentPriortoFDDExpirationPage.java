package com.builds.uimaps.admin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminFDDManagementConfigureEmailsentPriortoFDDExpirationPage {
	@FindBy(xpath = ".//input[@name='isConfigure' and @value='Y']")
	public WebElement sendEmailNotificationRadioYes ;

	@FindBy(xpath = ".//input[@name='isConfigure' and value='N']")
	public WebElement sendEmailNotificationRadioNo ;

	@FindBy(id = "subject")
	public WebElement subjectTxt;

	@FindBy(name = "submit")
	public WebElement submitBtn;

	@FindBy(name = "days")
	public WebElement timeInterval;

	@FindBy(xpath = ".//iframe[@id='templateText_ifr']")
	public WebElement htmlFrame ;

	@FindBy(xpath = ".//iframe[@id='emailContent_ifr']")
	public WebElement htmlFrame1 ;

	@FindBy(xpath = "html/body")
	public WebElement mailText ;

	// Anukaran
	@FindBy(name = "Submit")
	public WebElement saveBtn;

	@FindBy(name = ".//*[@id='keywords']/button[@role='presentation']")
	public WebElement addKeywordBtn;

	public AdminFDDManagementConfigureEmailsentPriortoFDDExpirationPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
