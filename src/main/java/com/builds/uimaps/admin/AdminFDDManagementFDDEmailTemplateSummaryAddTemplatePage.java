package com.builds.uimaps.admin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminFDDManagementFDDEmailTemplateSummaryAddTemplatePage {

	@FindBy(name = "mailTitle")
	public WebElement mailTitle;

	@FindBy(name = "mailSubject")
	public WebElement mailSubject;

	@FindBy(name = "defaultFlag")
	public WebElement setasDefaultTemplate;

	@FindBy(xpath = ".//iframe[@id='ta_ifr']")
	public WebElement htmlFrame ;

	@FindBy(xpath = "html/body")
	public WebElement mailText ;

	@FindBy(xpath = ".//input[@name='Submit' and @value='Add']")
	public WebElement addBtn ;

	@FindBy(name = "Submit")
	public WebElement submitBtn;

	// Anukaran
	@FindBy(xpath = ".//*[@id='pageid']//u[contains(text(),'Show All')]")
	public WebElement showAllBtn ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='siteMainTable']//input[@value='Add Template']")
	public WebElement addTemplateBtn ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='siteMainTable']//label[contains(text(),'Graphical')]//input[@name='typeRadio']")
	public WebElement graphicalRadio ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='siteMainTable']//label[contains(text(),'Plain Text')]//input[@name='typeRadio']")
	public WebElement typePlainText ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='siteMainTable']//input[@value='HTML File Upload' and @name='typeRadio']")
	public WebElement typeUploadHtml ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='HTML_FILE_ATTACHMENT']")
	public WebElement htmlFileAttachment ;

	@FindBy(xpath = ".//*[@id='ta']")
	public WebElement plainTxtMailBody ;

	public AdminFDDManagementFDDEmailTemplateSummaryAddTemplatePage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
