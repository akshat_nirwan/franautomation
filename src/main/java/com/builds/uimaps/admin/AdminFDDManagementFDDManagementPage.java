package com.builds.uimaps.admin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminFDDManagementFDDManagementPage {

	@FindBy(xpath = ".//input[@value='Upload FDD']")
	public WebElement uploadFDDBtn ;

	@FindBy(id = "ufocName")
	public WebElement fddNameTxt;

	@FindBy(id = "version")
	public WebElement versionTxt;

	@FindBy(name = "status")
	public WebElement statusDrp;

	@FindBy(id = "searchButton")
	public WebElement searchBtn;

	// Anukaran
	@FindBy(xpath = ".//*[@id='siteMainTable']//input[@value='Upload FDD']")
	public WebElement uploadFDDbtn ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='ufocAction_ufocName']")
	public WebElement fddName ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='ufocAction_version']")
	public WebElement fddVersion ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='ufocAction_expiryDate']")
	public WebElement fddDate ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='ufocAction_item23ReceiptID']")
	public WebElement fddItem23ReceiptSales;

	// Anukaran
	@FindBy(xpath = ".//*[@id='ufocAction_item23ReceiptIDFim']")
	public WebElement fddItem23ReceiptInfoMgr;

	// Anukaran
	@FindBy(xpath = ".//*[@id='ufocAction_brandID']")
	public WebElement fddDivision ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='ufocAction_file']")
	public WebElement fddUploadFile ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='ufocAction_file']")
	public WebElement fddComments ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='ufocAction_0']")
	public WebElement fddAddFdd ;

	public AdminFDDManagementFDDManagementPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
