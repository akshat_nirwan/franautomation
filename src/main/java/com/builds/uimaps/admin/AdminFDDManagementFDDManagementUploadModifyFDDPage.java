package com.builds.uimaps.admin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminFDDManagementFDDManagementUploadModifyFDDPage {

	@FindBy(id = "ufocAction_ufocName")
	public WebElement fddNameTxt;

	@FindBy(id = "ufocAction_version")
	public WebElement fddVersionTxt;

	@FindBy(id = "ufocAction_expiryDate")
	public WebElement fddExpiryTxt;

	@FindBy(id = "ufocAction_item23ReceiptID")
	public WebElement fddItemReceiptFSDrp;

	@FindBy(id = "ufocAction_countryID")
	public WebElement countryDrp;

	@FindBy(id = "ufocAction_item23ReceiptIDFim")
	public WebElement fddItemReceiptFIMDrp;

	@FindBy(name = "selectAll")
	public WebElement selectAll;

	@FindBy(id = "ufocAction_file")
	public WebElement fddFileUploadBtn;

	@FindBy(id = "ufocAction_comments")
	public WebElement commentsTxtArea;

	@FindBy(id = "ufocAction_0")
	public WebElement addFDDBtn;

	@FindBy(id = "ufocAction_brandID")
	public WebElement drpDivisionBrand;

	@FindBy(xpath = ".//*[@alt='Close the Calendar']")
	public WebElement calCloseButton ;

	public AdminFDDManagementFDDManagementUploadModifyFDDPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
