package com.builds.uimaps.admin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminFDDManagementITEM23RECEIPTSummaryAddITEM23RECEIPTPage {

	@FindBy(name = "templateTitle")
	public WebElement templateTitle;

	@FindBy(id = "newTemplateText")
	public WebElement item23ReceiptTextArea;

	@FindBy(name = "businessName")
	public WebElement businessName;

	@FindBy(id = "address")
	public WebElement addressTxt;

	@FindBy(id = "country")
	public WebElement countryDrp;

	@FindBy(id = "state")
	public WebElement stateDrp;

	@FindBy(name = "phone")
	public WebElement phone;

	@FindBy(xpath = ".//*[@id='siteMainTable']/tbody/tr/td/table/tbody/tr[2]/td[2]/input[1]")
	public WebElement previewBtn ;

	@FindBy(xpath = ".//*[@id='button' and @value='Save']")
	public WebElement saveBtn ;

	public AdminFDDManagementITEM23RECEIPTSummaryAddITEM23RECEIPTPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
