package com.builds.uimaps.admin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminFDDManagementITEM23RECEIPTSummaryPage {

	@FindBy(xpath = ".//*[@id='siteMainTable']/tbody/tr/td/table/tbody/tr[1]/td[2]/table/tbody/tr[6]/td/table/tbody/tr[1]/td/table/tbody/tr/td[2]/input")
	public WebElement addItem23ReceiptBtn ;

	public AdminFDDManagementITEM23RECEIPTSummaryPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
