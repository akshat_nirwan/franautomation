package com.builds.uimaps.admin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminFDDManagementLogonCredentialsDurationPage {

	@FindBy(xpath = ".//*[@id='siteMainTable']//select[@name='dataValue']")
	public WebElement durationDrpDwn;

	@FindBy(xpath = ".//input[@value='Save']")
	public WebElement saveBtn;

	public AdminFDDManagementLogonCredentialsDurationPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
