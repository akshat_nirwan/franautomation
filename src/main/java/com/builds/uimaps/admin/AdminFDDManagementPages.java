package com.builds.uimaps.admin;

import org.openqa.selenium.WebDriver;

public class AdminFDDManagementPages {

	WebDriver driver;

	public AdminFDDManagementPages(WebDriver driver) {
		this.driver = driver;
	}

	public AdminFDDManagementFDDManagementPage getFDDManagementPage() {
		return new AdminFDDManagementFDDManagementPage(driver);
	}

	public AdminFDDManagementFDDManagementUploadModifyFDDPage getFDDManagementUploadModifyFDDPage() {
		return new AdminFDDManagementFDDManagementUploadModifyFDDPage(driver);
	}

	public AdminFDDManagementFDDEmailTemplateSummaryPage getFDDEmailTemplateSummaryPage() {
		return new AdminFDDManagementFDDEmailTemplateSummaryPage(driver);
	}

}
