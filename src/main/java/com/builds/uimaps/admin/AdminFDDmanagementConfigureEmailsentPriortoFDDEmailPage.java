package com.builds.uimaps.admin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminFDDmanagementConfigureEmailsentPriortoFDDEmailPage {

	@FindBy(xpath = ".//input[@value='Y' and @name='isConfigure']")
	public WebElement sendEmailNotificationYes ;

	@FindBy(xpath = ".//input[@value='N' and @name='isConfigure']")
	public WebElement sendEmailNotificationNo ;

	@FindBy(name = "mailSubject")
	public WebElement mailSubject;

	@FindBy(name = "timeInterval")
	public WebElement timeInterval;

	@FindBy(xpath = ".//iframe[@id='templateText_ifr']")
	public WebElement htmlFrame ;

	@FindBy(xpath = "html/body")
	public WebElement mailText ;

	@FindBy(name = "Submit")
	public WebElement saveBtn;

	public AdminFDDmanagementConfigureEmailsentPriortoFDDEmailPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
