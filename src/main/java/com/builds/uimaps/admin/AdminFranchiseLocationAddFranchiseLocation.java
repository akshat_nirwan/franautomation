package com.builds.uimaps.admin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminFranchiseLocationAddFranchiseLocation {

	@FindBy(id = "franchiseeName")
	public WebElement franchiseeName;
	@FindBy(id = "centerName")
	public WebElement centerName;
	@FindBy(id = "areaID")
	public WebElement areaID;
	@FindBy(id = "licenseNo")
	public WebElement licenseNo;
	@FindBy(id = "storeTypeId")
	public WebElement storeTypeId;
	@FindBy(id = "status")
	public WebElement status;
	@FindBy(id = "versionID")
	public WebElement versionID;
	@FindBy(id = "reportPeriodStartDate")
	public WebElement reportPeriodStartDate;
	@FindBy(id = "openingDate")
	public WebElement openingDate;
	@FindBy(id = "address")
	public WebElement address;
	@FindBy(id = "address2")
	public WebElement address2;
	@FindBy(id = "city")
	public WebElement city;
	@FindBy(id = "countryID")
	public WebElement countryID;

	@FindBy(id = "zipcode")
	public WebElement zipcode;
	@FindBy(id = "regionNo")
	public WebElement regionNo;
	@FindBy(id = "storePhone")
	public WebElement storePhone;
	@FindBy(id = "storePhoneExtn")
	public WebElement storePhoneExtn;
	@FindBy(id = "storeFax")
	public WebElement storeFax;
	@FindBy(id = "storeMobile")
	public WebElement storeMobile;
	@FindBy(id = "storeEmail")
	public WebElement storeEmail;
	@FindBy(id = "contactTitle")
	public WebElement contactTitle;
	@FindBy(id = "contactFirstName")
	public WebElement contactFirstName;
	@FindBy(id = "contactLastName")
	public WebElement contactLastName;
	@FindBy(id = "phone1")
	public WebElement phone1;
	@FindBy(id = "phone1Extn")
	public WebElement phone1Extn;
	@FindBy(id = "emailID")
	public WebElement emailID;
	@FindBy(id = "fax")
	public WebElement fax;
	@FindBy(id = "mobile")
	public WebElement mobile;
	@FindBy(id = "fimEntityID")
	public WebElement fimEntityID;

	@FindBy(id = "entityType")
	public WebElement entityType;
	@FindBy(id = "fimEntityDetail_0fimCbEntityType")
	public WebElement fimEntityDetail_0fimCbEntityType;
	@FindBy(id = "fimEntityDetail_0fimTtEntityName")
	public WebElement fimEntityDetail_0fimTtEntityName;
	@FindBy(id = "fimEntityDetail_0fimTtTaxpayer")
	public WebElement fimEntityDetail_0fimTtTaxpayer;
	@FindBy(id = "fimEntityDetail_0fimCbCountryOfFormation")
	public WebElement fimEntityDetail_0fimCbCountryOfFormation;

	@FindBy(id = "fimEntityDetail_0fimCbStateOfFormation")
	public WebElement fimEntityDetail_0fimCbStateOfFormation;
	@FindBy(css = "button.ms-choice")
	public WebElement buttonmsChoice;
	@FindBy(css = "input.searchInputMultiple")
	public WebElement inputsearchInputMultiple;
	@FindBy(id = "nametd")
	public WebElement nametd;
	@FindBy(id = "ownerType")
	public WebElement ownerType;
	@FindBy(name = "salutation")
	public WebElement salutation;
	@FindBy(id = "ownerFirstName")
	public WebElement ownerFirstName;
	@FindBy(id = "ownerLastName")
	public WebElement ownerLastName;
	@FindBy(id = "Submit")
	public WebElement submit;

	public AdminFranchiseLocationAddFranchiseLocation(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
