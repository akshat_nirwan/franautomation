package com.builds.uimaps.admin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminFranchiseLocationAddFranchiseLocationPage {

	@FindBy(id = "franchiseeName")
	public WebElement franchiseeID;

	@FindBy(id = "centerName")
	public WebElement centerName;

	@FindBy(id = "licenseNo")
	public WebElement licenseNo;

	@FindBy(id = "storeTypeId")
	public WebElement storeTypeId;

	@FindBy(id = "status")
	public WebElement status;

	@FindBy(id = "versionID")
	public WebElement versionID;

	@FindBy(id = "reportPeriodStartDate")
	public WebElement reportPeriodStartDate;

	@FindBy(id = "openingDate")
	public WebElement openingDate;

	@FindBy(id = "grandStoreOpeningDate")
	public WebElement openerOpeningDate;

	@FindBy(id = "address")
	public WebElement address;

	@FindBy(id = "address2")
	public WebElement address2;

	@FindBy(id = "city")
	public WebElement city;

	@FindBy(id = "countryID")
	public WebElement countryID;

	@FindBy(id = "regionNo")
	public WebElement stateID;

	@FindBy(id = "zipcode")
	public WebElement zipcode;

	@FindBy(id = "regionNo")
	public WebElement regionNo;

	@FindBy(id = "areaID")
	public WebElement areaRegion;

	@FindBy(id = "storePhone")
	public WebElement storePhone;
	@FindBy(id = "storePhoneExtn")
	public WebElement storePhoneExtn;
	@FindBy(id = "storeFax")
	public WebElement storeFax;
	@FindBy(id = "storeMobile")
	public WebElement storeMobile;
	@FindBy(id = "storeEmail")
	public WebElement storeEmail;
	@FindBy(id = "contactTitle")
	public WebElement contactTitle;
	@FindBy(id = "contactFirstName")
	public WebElement contactFirstName;
	@FindBy(id = "contactLastName")
	public WebElement contactLastName;
	@FindBy(id = "phone1")
	public WebElement phone1;
	@FindBy(id = "phone1Extn")
	public WebElement phone1Extn;
	@FindBy(id = "emailID")
	public WebElement emailID;
	@FindBy(id = "fax")
	public WebElement fax;
	@FindBy(id = "mobile")
	public WebElement mobile;
	@FindBy(id = "fimEntityID")
	public WebElement fimEntityID;

	@FindBy(id = "entityType")
	public WebElement entityType;
	@FindBy(id = "fimEntityDetail_0fimCbEntityType")
	public WebElement fimEntityDetail_0fimCbEntityType;
	@FindBy(id = "fimEntityDetail_0fimTtEntityName")
	public WebElement fimEntityDetail_0fimTtEntityName;
	@FindBy(id = "fimEntityDetail_0fimTtTaxpayer")
	public WebElement fimEntityDetail_0fimTtTaxpayer;
	@FindBy(id = "fimEntityDetail_0fimCbCountryOfFormation")
	public WebElement fimEntityDetail_0fimCbCountryOfFormation;

	@FindBy(id = "fimEntityDetail_0fimCbStateOfFormation")
	public WebElement fimEntityDetail_0fimCbStateOfFormation;

	@FindBy(xpath = ".//div[@id='ms-parentextOwnerId']")
	public WebElement buttonmsChoice ;

	@FindBy(xpath=".//*[@id='ms-parentextOwnerId']/div/div/input")
	public WebElement inputsearchInputMultiple ;
	
	@FindBy(id="ms-parentextOwnerId")
	public WebElement  selectOwnerMulti;

	@FindBy(id = "nametd")
	public WebElement nametd;

	@FindBy(xpath = ".//input[@id='ownerType' and @value='0']")
	public WebElement ownerTypeNewOwner ;

	@FindBy(xpath = ".//input[@id='ownerType' and @value='1']")
	public WebElement ownerTypeExistingOwners ;

	@FindBy(name = "salutation")
	public WebElement salutation;

	@FindBy(id = "ownerFirstName")
	public WebElement ownerFirstName;

	@FindBy(id = "ownerLastName")
	public WebElement ownerLastName;

	@FindBy(id = "Submit")
	public WebElement submit;

	@FindBy(name = "franchiseMenu")
	public WebElement franchiseIDDrpDown;

	@FindBy(xpath = ".//div[@id='ms-parentextOwnerId']//button")
	public WebElement selectBox ;

	@FindBy(xpath = ".//*[@id='ms-parentextOwnerId']/div/div/input")
	public WebElement searchTxtBox ;

	@FindBy(xpath = ".//li[@style='display: list-item;']/label/input")
	public WebElement check ;

	@FindBy(id = "selectAll")
	public WebElement checkAll;

	@FindBy(xpath = ".//table[@id='muId']//input[contains(@id,'_muid')]")
	public WebElement muidTxtBox ;

	@FindBy(id = "brands")
	public WebElement drpDivisionBrand;

	@FindBy(id = "storeWebsite")
	public WebElement storeWebSite;
	
	@FindBy(xpath="//td/input[2][@id='ownerType']")
	public WebElement existingowner;
	
	@FindBy(xpath="//tr[@class='TextLbl']/td/input[@class='fTextBox']")
	public WebElement existingownerName;
	
	@FindBy(xpath=".//input[@id = 'ownerType' and @value = '1']")
	public WebElement existingownercheckbox;
	
	@FindBy(xpath=".//span[contains(text(),'Select')]")
	public WebElement SelectOwner;
	
	@FindBy(xpath=".//input[@class ='searchInputMultiple' and @type = 'text']")
	public WebElement Search_bar;
	
	@FindBy(xpath=".//input[@class ='search-btn on']")
	public WebElement Search_bar_button;
	
	@FindBy(xpath=".//input[@id = 'ownerType' and @value = '0']")
	public WebElement Newownercheckbox;

	public AdminFranchiseLocationAddFranchiseLocationPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
