package com.builds.uimaps.admin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminFranchiseLocationManageFranchiseLocationsAddUserPage {
	@FindBy(id = "userName")
	public WebElement loginId;

	@FindBy(xpath = ".//input[@id='password' and @type='password']")
	public WebElement password ;

	@FindBy(xpath = ".//input[@id='confirmPassword' and @type='password']")
	public WebElement confirmPassword ;

	@FindBy(id = "userType")
	public WebElement type;

	@FindBy(id = "expiryDays")
	public WebElement expiryDays;

	@FindBy(id = "ms-parentroleID")
	public WebElement rolesId;

	@FindBy(id = "timezone")
	public WebElement timeZone;

	@FindBy(id = "users")
	public WebElement userType;

	@FindBy(id = "region")
	public WebElement region;

	@FindBy(id = "jobTitle")
	public WebElement jobTitle;

	@FindBy(id = "salutation")
	public WebElement salutation;

	@FindBy(id = "userLanguage")
	public WebElement userLanguage;

	@FindBy(id = "firstName")
	public WebElement firstName;

	@FindBy(id = "lastName")
	public WebElement lastName;

	@FindBy(id = "address")
	public WebElement address;

	@FindBy(id = "city")
	public WebElement city;

	@FindBy(id = "country")
	public WebElement country;

	@FindBy(id = "zipcode")
	public WebElement zipcode;

	@FindBy(id = "state")
	public WebElement state;

	@FindBy(id = "phone1")
	public WebElement phone1;

	@FindBy(id = "phoneExt1")
	public WebElement phoneExt1;

	@FindBy(id = "phone2")
	public WebElement phone2;

	@FindBy(id = "phoneExt2")
	public WebElement phoneExt2;

	@FindBy(id = "fax")
	public WebElement fax;

	@FindBy(id = "mobile")
	public WebElement mobile;

	@FindBy(id = "email")
	public WebElement email;

	@FindBy(id = "loginUserIp")
	public WebElement loginUserIp;

	@FindBy(id = "isBillable")
	public WebElement isBillable;

	@FindBy(id = "birthMonth")
	public WebElement birthMonth;

	@FindBy(id = "birthDate")
	public WebElement birthDate;

	@FindBy(id = "userPictureName")
	public WebElement userPictureName;

	@FindBy(id = "sendNotification")
	public WebElement sendNotification;

	@FindBy(id = "visibleToStore")
	public WebElement visibleToStore;

	@FindBy(id = "Submit")
	public WebElement submit;

	@FindBy(xpath = ".//a[@href='smDocuments']/span")
	public WebElement documentsTab ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='timezone']")
	public WebElement timezone ;

	@FindBy(xpath = ".//input[@name='foSearchString']")
	public WebElement searchKey ;

	@FindBy(xpath = ".//img[@title='Search by Store No. / Owner']")
	public WebElement searchBtn ;

	@FindBy(xpath = ".//*[@id='showFilter']/a")
	public WebElement showFilter ;

	@FindBy(xpath = ".//*[@id='ms-parentfilterfranchiseeNo']")
	public WebElement filterFranchiseeIdDrpDwn ;

	@FindBy(xpath = ".//*[@id='search']")
	public WebElement filterSearchBtn ;

	@FindBy(xpath = ".//*[@id='ms-parentregionKey']")
	public WebElement selectarea ;

	@FindBy(id = "searchButton")
	public WebElement searchBtn12;
	
	@FindBy(id = "salutation")
	public WebElement Salutation;

	public AdminFranchiseLocationManageFranchiseLocationsAddUserPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
