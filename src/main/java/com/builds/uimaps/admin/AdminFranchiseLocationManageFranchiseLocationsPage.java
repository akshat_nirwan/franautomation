package com.builds.uimaps.admin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminFranchiseLocationManageFranchiseLocationsPage {

	@FindBy(id = "searchButton")
	public WebElement searchButton;

	@FindBy(name = "countryId")
	public WebElement countryId;

	@FindBy(name = "regionKey")
	public WebElement areaRegion;

	@FindBy(name = "divisionKey")
	public WebElement division;

	@FindBy(name = "franchiseeOwner")
	public WebElement franchiseeOwner;

	@FindBy(name = "franchiseMenu")
	public WebElement franchiseId;

	@FindBy(name = "entityID")
	public WebElement entityID;

	@FindBy(name = "franchiseStatus")
	public WebElement franchiseStatus;

	@FindBy(name = "franchiseType")
	public WebElement franchiseType;

	@FindBy(xpath = ".//input[@value='Add Franchise Location' and @type='button']")
	public WebElement addFranchiseLocation ;

	public AdminFranchiseLocationManageFranchiseLocationsPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
