package com.builds.uimaps.admin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminFranchiseLocationManageFranchiseLocationsPageUI {
	
	WebDriver driver;
	
	@FindBy(id="ms-parentcountryId")
	public WebElement Country;
	
	@FindBy(id="ms-parentregionKey")
	public WebElement AreaRegion;

	@FindBy(id="ms-parentdivisionKey")
	public WebElement Brand;

	@FindBy(id="ms-parentfranchiseeOwner")
	public WebElement Owner;
	
	@FindBy(id="ms-parentfranchiseMenu")
	public WebElement FranshieID;
	
	@FindBy(id="ms-parententityID")
	public WebElement Entity;
	
	@FindBy(id="ms-parentfranchiseStatus")
	public WebElement FranchiseStatus;
	
	@FindBy(id="ms-parentfranchiseType")
	public WebElement FranchiseeType;
	
	@FindBy(id="searchButton")
	public WebElement Search;
	
		
	@FindBy(xpath="//span[@id='Action_dynamicmenu01']")
	public WebElement ModifyLocation;
	
	@FindBy(xpath="//span[@id='Action_dynamicmenu11']")
	public WebElement DeactivateLocation;
	
	@FindBy(xpath="//span[@id='Action_dynamicmenu21']")
	public WebElement Users;
	
	
	public AdminFranchiseLocationManageFranchiseLocationsPageUI(WebDriver driver)
	{
		PageFactory.initElements(driver, this);
	}
	

}
