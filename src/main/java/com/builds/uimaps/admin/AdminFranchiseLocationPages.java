package com.builds.uimaps.admin;

import org.openqa.selenium.WebDriver;

public class AdminFranchiseLocationPages {

	public AdminFranchiseLocationPages(WebDriver driver) {
		this.driver = driver;
	}

	WebDriver driver;

	public AdminFranchiseLocationAddFranchiseLocationPage getAddFranchiseLocationPage() {
		return new AdminFranchiseLocationAddFranchiseLocationPage(driver);
	}

}
