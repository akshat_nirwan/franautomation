package com.builds.uimaps.admin;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminHiddenLinksConfigureFieldOpsSettingsPage {

	@FindBy(name = "franCanCreateVisit")
	public List<WebElement> franCanCreateVisit;

	@FindBy(name = "privateComments")
	public List<WebElement> privateComments;

	@FindBy(xpath = ".//*[@name='privateComments' and @value='Y']")
	public WebElement privateCommentsYes ;

	@FindBy(xpath = ".//input[@value='Save']")
	public WebElement saveBtn ;

	// Allow owner scoring
	@FindBy(name = "ownerScore")
	public List<WebElement> ownerScore;

	// questionNumbering
	@FindBy(name = "questionNumbering")
	public List<WebElement> questionNumbering;

	public AdminHiddenLinksConfigureFieldOpsSettingsPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
