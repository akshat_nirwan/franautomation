package com.builds.uimaps.admin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminHiddenLinksConfigureFranBuzzNamePage {

	@FindBy(xpath = ".//input[@value='Modify']")
	public WebElement modifyBtn ;

	@FindBy(xpath = ".//input[@value='Back']")
	public WebElement backBtn ;

	@FindBy(name = "franbuzzName")
	public WebElement franbuzzNameTextBox;

	@FindBy(xpath = ".//input[@value='Save']")
	public WebElement SaveBtn ;

	@FindBy(xpath = ".//input[@value='Reset']")
	public WebElement resetBtn ;

	@FindBy(xpath = ".//input[@value='Cancel']")
	public WebElement cancelBtn ;

	public AdminHiddenLinksConfigureFranBuzzNamePage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
