package com.builds.uimaps.admin;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminHiddenLinksConfigureNewHierarchyLevelPage {

	@FindBy(name = "isDivisionConfigure")
	public List<WebElement> isDivisionConfigure;

	@FindBy(xpath = ".//input[@name='isDivisionConfigureDisplay' and @value='Y']")
	public WebElement isDivisionConfigureDisplayY ;

	@FindBy(id = "divisionLabel")
	public WebElement divisionLabel;

	@FindBy(id = "divisionUserLabel")
	public WebElement divisionUserLabel;

	@FindBy(id = "divisionUserAbbr")
	public WebElement divisionUserAbbr;

	@FindBy(name = "isFddBrandConfigured")
	public List<WebElement> lstEnableMultiBrandFDDRadio;

	@FindBy(name = "allowMultipleDivision")
	public List<WebElement> rdoAllowMultipleDivision;

	@FindBy(xpath = ".//input[@name='isFddBrandConfigured' and @value='Y']")
	public WebElement lstEnableMultiBrandFDDRadiocheck ;

	@FindBy(id = "newUserOn")
	public WebElement newUserOn;

	@FindBy(id = "newUserOff")
	public WebElement newUserOff;

	@FindBy(xpath = ".//*[contains(@id,'row_')]/td[3]/a/img")
	public List<WebElement> addNewUserLevelImg;

	@FindBy(xpath = ".//input[contains(@id,'labelNewUser')]")
	public List<WebElement> configurationLabelList;

	@FindBy(xpath = ".//input[contains(@id,'abbrNewUser')]")
	public List<WebElement> configurationAbbrList;

	@FindBy(xpath = ".//*[@id='labelNewUser_0_ID#1']")
	public WebElement configureLabel01;

	@FindBy(xpath = ".//*[@id='abbrNewUser_0_ID#1']")
	public WebElement configureAbbreviation01;

	@FindBy(name = "Submit")
	public WebElement submitBtn;

	public AdminHiddenLinksConfigureNewHierarchyLevelPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
