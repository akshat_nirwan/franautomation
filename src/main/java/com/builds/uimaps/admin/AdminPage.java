package com.builds.uimaps.admin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminPage {

	// Admin > Users Page

	@FindBy(xpath = ".//*[@qat_adminlink='Corporate User']")
	public WebElement corporateUserLnk ;

	@FindBy(xpath = ".//*[@qat_adminlink='Regional User' or contains(text(), 'Regional User')]")
	public WebElement regionalUserLnk ;

	@FindBy(xpath = ".//*[@qat_adminlink='Franchise User']")
	public WebElement franchiseUserLnk ;

	@FindBy(xpath = ".//*[@qat_adminlink='Suppliers']")
	public WebElement suppliersLnk ;

	@FindBy(xpath = ".//*[@qat_adminlink='Roles']")
	public WebElement rolesLnk ;

	@FindBy(xpath = ".//*[@qat_adminlink='Export']")
	public WebElement exportLnk ;

	@FindBy(xpath = ".//*[@qat_adminlink='Import Users']")
	public WebElement importUsers ;

	@FindBy(xpath = "//input[@value='Add Franchise User']")
	public WebElement addfranchiseUserBtn ;

	/*
	 * 
	 * Admin > Franchise Sales Links Below
	 * 
	 */

	@FindBy(xpath = ".//*[@qat_adminlink='Status']")
	public WebElement statusLnk ;

	@FindBy(xpath = ".//*[@qat_adminlink='Configure Service Lists']")
	public WebElement configureServiceListsLnk ;

	@FindBy(xpath = ".//*[@qat_adminlink='Configure Co-Applicant Relationship']")
	public WebElement configureCoApplicantRelationshipLnk ;

	@FindBy(xpath = ".//*[@qat_adminlink='Source']")
	public WebElement sourceLnk ;

	@FindBy(linkText = "Forecast Rating")
	public WebElement forecastRatingLnk ;

	/*
	 * @FindBy(linkText="Configure Lead Killed Reason") public WebElement
	 * configureLeadKilledReasonLnk=DDDDDDDDDDDDDDDDDDDDD
	 */

	@FindBy(xpath = ".//*[@qat_adminlink='Configure Lead Killed Reason']")
	public WebElement configureLeadKilledReasonLnk;// =DDDDDDDDDDDDDDDDDDDDD
													// Lead Killed Reason']";

	@FindBy(linkText = "Marketing Code")
	public WebElement marketingCodeLnk ;

	@FindBy(linkText = "Brokers Type Configuration")
	public WebElement brokersTypeConfigurationLnk ;

	@FindBy(linkText = "Brokers Agency")
	public WebElement brokersAgencyLnk ;

	@FindBy(linkText = "Configure Lead Rating")
	public WebElement configureLeadRatingLnk ;

	@FindBy(xpath = ".//*[@id='Franchise_Sales']/ul/li[19]/a")
	public WebElement configureOptOutStatus ;

	@FindBy(xpath = ".//*[@qat_adminlink='Configure Qualification Checklists']")
	public WebElement configureQualificationChecklists ;

	/*
	 * @FindBy(linkText="Sales Territories") public WebElement salesTerritories=
	 * DDDDDDDDDDDDDDDDDDDDD
	 */
	@FindBy(xpath = ".//*[@qat_adminlink='Sales Territories']")
	public WebElement salesTerritories ;

	@FindBy(linkText = "Assign Lead Owners")
	public WebElement assignLeadOwners ;

	@FindBy(xpath = ".//*[@id='siteMainTable']/tbody/tr/td/table/tbody/tr[2]/td[2]/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr[2]/td[1]/input")
	public WebElement assignLeadOwnerbySalesTerritories ;

	@FindBy(xpath = ".//*[@qat_adminlink='Setup Franchise Sales Lead Owners']")
	public WebElement setupFranchiseSalesLeadOwners ;

	@FindBy(linkText = "Define Unregistered States / Provinces")
	public WebElement defineUnregisteredStatesProvinces ;

	@FindBy(linkText = "Setup Email Campaign Triggers")
	public WebElement setupEmailCampaignTriggers ;

	@FindBy(linkText = "Setup Automated Tasks")
	public WebElement setupAutomatedTasks ;

	@FindBy(linkText = "Configure Task Emails")
	public WebElement configureTaskEmails ;

	@FindBy(linkText = "Configure Duplicate Criteria")
	public WebElement configureDuplicateCriteria ;

	@FindBy(xpath = ".//div[@id='Franchise_Sales']//a[@href='builderWebForm?module=fs&moduleId=2&fromWhere=admin']")
	public WebElement manageFormGenerator ;

	@FindBy(xpath = ".//div[@id='Franchise_Sales']//a[contains(text(),'Configure Search Criteria for Top Search')]")
	public WebElement configureSearchCriteriaforTopSearch ;

	@FindBy(xpath = ".//*[@id='Franchise_Sales']/ul//*[contains(text(),'Manage Web Form Generator')]")
	public WebElement manageWebFormGenerator ;

	@FindBy(xpath = ".//*[@qat_adminlink='Configure Proven Match Integration Details']")
	public WebElement ConfigureProvenMatchIntegrationDetails ;

	@FindBy(xpath = ".//*[@qat_adminlink='Configure Nathan Profiler Integration Details']")
	public WebElement ConfigureNathanProfilerIntegrationDetails ;

	/*
	 * 
	 * Admin > Intranet Links Below
	 * 
	 */

	@FindBy(linkText = "Stories, RSS Feeds and Home Page View")
	public WebElement storiesRSSFeedsandHomePageViewLnk ;

	@FindBy(linkText = "Auto Archive Settings")
	public WebElement autoArchiveSettingsLnk ;

	@FindBy(linkText = "Archive Alerts")
	public WebElement archiveAlertsLnk ;

	@FindBy(linkText = "Archive Messages")
	public WebElement archiveMessagesLnk ;

	@FindBy(linkText = "News")
	public WebElement newsLnk ;

	@FindBy(linkText = "EPoll")
	public WebElement ePollLnk ;

	@FindBy(xpath = ".//*[@qat_adminlink='Library']")
	public WebElement libraryLnk ;

	@FindBy(xpath = ".//*[@id='Intranet']//a[@qat_adminlink='Library Logs']")
	public WebElement libraryLogs ;

	@FindBy(linkText = "Administer Forum")
	public WebElement administerForumLnk ;

	@FindBy(linkText = "Related Links")
	public WebElement relatedLinksLnk ;

	@FindBy(xpath = ".//*[@id='Intranet']//a[@qat_adminlink='News Logs']")
	public WebElement newsLogs ;

	@FindBy(linkText = "Configure FranBuzz Name")
	public WebElement configureFranBuzzNameLnk ;

	@FindBy(linkText = "Configure Single Sign On")
	public WebElement configureSingleSignOn ;

	@FindBy(linkText = "Configure What's New Email")
	public WebElement configureWhatsNewEmailLnk ;

	@FindBy(linkText = "Library Logs")
	public WebElement libraryLogsLnk ;

	@FindBy(linkText = "News Logs")
	public WebElement newsLogsLnk ;

	@FindBy(linkText = "Searched Logs")
	public WebElement searchedLogsLnk ;

	@FindBy(xpath = ".//*[@id='Intranet']//a[@qat_adminlink='Configure Server']")
	public WebElement hubConfigureServer ;

	/*
	 * Admin > Add New Franchise Location
	 */

	@FindBy(linkText = "Add New Franchise Location")
	public WebElement addNewFranchiseLocationLnk ;

	@FindBy(linkText = "Manage Franchise Locations")
	public WebElement manageFranchiseLocationsLnk ;

	/*
	 * Admin > Hidden Link > Delete Franchisee Location
	 */
	@FindBy(linkText = "Delete Franchise Locations")
	public WebElement deleteFranchiseeLocationLnk;
		
	/*
	 * Admin > Area / Region
	 */

	@FindBy(xpath = ".//*[@qat_adminlink='Manage Area / Region']")
	public WebElement manageAreaRegionLnk ;

	@FindBy(xpath = ".//*[@qat_adminlink='Add New Area / Region']")
	public WebElement addNewAreaRegionLnk ;

	/*
	 * admin > Division_US Management / Manage Division_US / Add Division_US
	 */

	@FindBy(linkText = "Division Management")
	public WebElement divisionManagementLnk ;

	@FindBy(xpath = ".//*[@qat_adminlink='Manage Division']")
	public WebElement manageDivisionLnk ;

	@FindBy(xpath = ".//*[@qat_adminlink='Add Division']")
	public WebElement addDivisionLnk ;

	// Add Divisional User
	// @FindBy(linkText="Divisional Users")

	@FindBy(xpath = ".//*[@qat_adminlink='Divisional Users' or contains(text(),'Divisional Users')]")
	// public WebElement divisionalUser=DDDDDDDDDDDDDDDDDDDDD
	// Users']";
	public WebElement divisionalUser ;

	@FindBy(css = "input.cm_new_button_link")
	// public WebElement addDivisionalUserLnk="//linkText=#Divisional Users";
	public WebElement addDivisionalUserLnk ;

	/*
	 * Admin Hidden Links """
	 */

	@FindBy(linkText = "Configure Password Settings")
	public WebElement ConfigurePasswordSettings ;

	@FindBy(linkText = "Configure FranBuzz Name")
	public WebElement configureFranBuzzName ;

	@FindBy(linkText = "Configure Field Ops Settings")
	public WebElement configureFieldOpsSettings ;

	@FindBy(xpath = ".//*[@qat_adminlink='Configure New Hierarchy Level']")
	public WebElement configureNewHierarchyLevel ;

	@FindBy(xpath = ".//*[@qat_adminlink='Configure New Hierarchy Level']")
	public WebElement configureProvenMatchIntegration ;

	@FindBy(xpath = ".//*[@qat_adminlink='Configure New Hierarchy Level']")
	public WebElement configureNathanProfilerIntegration ;
	
	@FindBy(xpath=".//*[@qat_adminlink='Configure Franchise Sales Email Parsing Server']")
	public WebElement configureSalesEmailParsingServer;

	/*
	 * Pwise
	 */

	@FindBy(linkText = "Manage Visit Form")
	public WebElement manageVisitFormLnk ;

	@FindBy(linkText = "Question Library")
	public WebElement questionLibraryLnk ;

	@FindBy(linkText = "Action Library")
	public WebElement actionLibraryLnk ;

	@FindBy(xpath = ".//a[@href='qaTabIntegration']")
	public WebElement qaTabIntegrationLnk ;

	@FindBy(linkText = "Manage Auditor")
	public WebElement manageAuditorLnk ;

	@FindBy(linkText = "Configure Reminder Alerts for Tasks")
	public WebElement configureReminderAlertsForTasksLnk ;

	@FindBy(linkText = "Configure Score")
	public WebElement configureScoreLnk ;

	@FindBy(xpath = ".//*[@id='Inspection_Management']/ul/li/a[contains(text () , 'Configure Alert Email Triggers for Tasks')]")
	public WebElement configureAlertEmailTriggersForTasksLnk ;

	@FindBy(linkText = "Configure Media Library")
	public WebElement configureMediaLibraryLnk ;

	@FindBy(linkText = "Manage Consultant")
	public WebElement manageConsultantLnk ;

	@FindBy(linkText = "Configure Visit Email Content")
	public WebElement ConfigureScheduleVisitEmailContentLnk ;

	@FindBy(linkText = "Delete Field Ops Visits")
	public WebElement deleteFieldOpsVisitsLnk ;

	/*
	 * admin > support
	 */

	@FindBy(linkText = "Manage FAQs")
	public WebElement ManageFAQsLnk ;

	@FindBy(linkText = "Manage Department")
	public WebElement manageDepartmentLnk ;

	@FindBy(linkText = "Manage Contact Information")
	public WebElement manageContactInformationLnk ;

	@FindBy(linkText = "Manage Message Templates")
	public WebElement manageMessageTemplatesLnk ;

	@FindBy(linkText = "Configure Ticket Status")
	public WebElement configureTicketStatusLnk ;

	/*
	 * admin > franchise opener
	 */

	@FindBy(linkText = "Task Checklist")
	public WebElement taskChecklistLnk ;

	@FindBy(linkText = "Equipment Checklist")
	public WebElement equipmentChecklistLnk ;

	@FindBy(linkText = "Document Checklist")
	public WebElement documentChecklistLnk ;

	@FindBy(linkText = "Picture Checklist")
	public WebElement pictureChecklistLnk ;

	@FindBy(xpath = ".//*[@id='Franchise_Opener']//a[.='Secondary Checklists' or .='Secondary Checklist']")
	public WebElement secondaryChecklistLnk ;

	@FindBy(linkText = "Responsible Department")
	public WebElement responsibleDepartmentLnk ;

	@FindBy(xpath = ".//*[@id='Franchise_Opener']/ul/li/a[contains(text () , 'Overdue Alert Frequency')]")
	public WebElement overdueAlertFrequencyLnk ;

	@FindBy(linkText = "Alert Email Content")
	public WebElement alertEmailContentLnk ;

	@FindBy(linkText = "Customize Profiles")
	public WebElement customizeProfilesLnk ;

	@FindBy(linkText = "Manage Reference Dates")
	public WebElement manageReferenceDatesLnk ;

	@FindBy(linkText = "Configure Project Status")
	public WebElement configureProjectStatusLnk ;

	@FindBy(linkText = "Manage Groups")
	public WebElement manageGroupsLnk ;

	@FindBy(linkText = "Configure Checklist Display Setting")
	public WebElement configureChecklistDisplaySettings ;

	@FindBy(linkText = "Configure Alert Email Triggers for Tasks")
	public WebElement configureAlertEmailTriggersTask ;

	@FindBy(linkText = "Configure Milestone Date Triggers")
	public WebElement ConfigureMilestoneDateTriggers ;

	/*
	 * admin > training
	 */

	@FindBy(linkText = "Course Management")
	public WebElement courseManagementLnk ;

	@FindBy(linkText = "Plan & Certificates")
	public WebElement PlanAndCertificatesLnk ;

	@FindBy(linkText = "Questions Library")
	public WebElement questionLibraryTrainingLnk ;

	@FindBy(linkText = "Configure Server")
	public WebElement configureServerTrainingLnk ;

	@FindBy(linkText = "Configure Invite Email")
	public WebElement configureInviteEmailLnk ;

	/*
	 * admin > FIM
	 */

	@FindBy(linkText = "Manage Form Generator")
	public WebElement manageFormGeneratorLnk ;

	@FindBy(linkText = "Customize Regional Form")
	public WebElement customizeRegionalFormLnk ;

	@FindBy(linkText = "Triggers and Auditing")
	public WebElement triggersAndAuditingLnk ;

	@FindBy(linkText = "Configure FIM Email Capture")
	public WebElement configureFIMEmailCaptureLnk ;

	@FindBy(linkText = "Configure Ownership Transfer Status")
	public WebElement configureOwnershipTransferStatusLnk ;

	@FindBy(linkText = "Configure Other Addresses Heading")
	public WebElement configureOtherAddressesHeadingLnk ;

	@FindBy(linkText = "Configure Search Criteria for Top Search")
	public WebElement configureSearchCriteriaforTopSearchLnk ;

	@FindBy(linkText = "Configure Opt-out Message")
	public WebElement ConfigureOptoutMessageLnk ;

	@FindBy(linkText = "Configure Server")
	public WebElement configureServerLnk ;

	/*
	 * @FindBy(linkText="Configure Area / Region Label") public WebElement
	 * configureAreaRegionLblLnk=DDDDDDDDDDDDDDDDDDDDD
	 */

	@FindBy(xpath = ".//a[@qat_adminlink='Configure Area / Region Label']")
	public WebElement configureAreaRegionLblLnk;

	/*
	 * admin > configuration
	 */

	@FindBy(xpath = ".//a[@qat_adminlink='Configure Quick Links']")
	public WebElement configureQuickLnk;

	@FindBy(linkText = "Configure Store Type")
	public WebElement configureStoreTypeLnk ;

	@FindBy(linkText = "Configure Task Type")
	public WebElement configureTaskType ;

	@FindBy(xpath = ".//*[@qat_adminlink='Configure Offensive / Watch / Great Keywords']")
	public WebElement configureOffensiveWord ;

	@FindBy(linkText = "Configure Dictionary")
	public WebElement configureDictionaryLnk ;

	@FindBy(linkText = "Configure Decimal Places")
	public WebElement configureDecimalPlacesLnk ;

	@FindBy(linkText = "PII Configuration")
	public WebElement piiConfigurationLnk ;

	@FindBy(linkText = "Configure Calendar Start Day")
	public WebElement configureCalendarStartDayLnk ;

	@FindBy(linkText = "Configure Calendar Event Categories")
	public WebElement configureCalendarEventCategoriesLnk ;

	@FindBy(linkText = "Configure Task Display On Calendar")
	public WebElement configureTaskDisplayOnCalendarLnk ;

	@FindBy(linkText = "Configure Call Status")
	public WebElement configureCallStatusLnk ;

	@FindBy(linkText = "Configure Communication Type")
	public WebElement configureCommunicationTypeLnk ;

	@FindBy(xpath = ".//a[@qat_adminlink='Configure Site Clearance']")
	public WebElement configureSiteClearance;

	@FindBy(linkText = "Configure Info Mgr Tabs")
	public WebElement configureInfoMgrTabsLnk ;

	/*
	 * FDD Management
	 */

	@FindBy(xpath = ".//*[@id='UFOC_Management']//a[contains(text(),'FDD Management')]")
	public WebElement fddManagementLnk ;

	@FindBy(linkText = "FDD Email Template summary")
	public WebElement fddEmailTemplateSummaryLnk ;

	@FindBy(linkText = "Log on credentials duration")
	public WebElement logOnCredentialsDurationLnk ;

	@FindBy(linkText = "ITEM 23 - RECEIPT Summary")
	public WebElement item23ReceiptSummaryLnk ;

	@FindBy(linkText = "Configure Email sent Prior to FDD Email")
	public WebElement configureEmailSentPriorToFDDEmailLnk ;

	@FindBy(linkText = "Configure Email sent Prior to FDD Expiration")
	public WebElement configureEmailsentPriortoFDDExpirationLnk ;

	/*
	 * Access Control Access Control
	 */

	@FindBy(linkText = "Deleted Logs")
	public WebElement deletedLogsLnk ;

	@FindBy(linkText = "Login Logs")
	public WebElement loginLogsLnk ;

	@FindBy(xpath = ".//*[@qat_adminlink='Searched Logs']")
	public WebElement hubSearchedLogsLnk;
	
	@FindBy(xpath = ".//a[@qat_adminlink='User Account Summary']")
	public WebElement userAccountSummary ;

	@FindBy(linkText = "Link Lead With Existing Lead / Owner")
	public WebElement linkLeadWithExisting ;

	public AdminPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	/*
	 * admin > fin********By Gaurav Tomar********
	 */


	@FindBy(xpath = ".//a[contains(text () , 'Finance Setup Preferences')]")
	public WebElement financeSetupPreferencesLnk ;
	
	@FindBy(xpath = ".//a[@href='nachaAreaStatus']")
	public WebElement enableDisableAreaFranchiseforEFTLnk ;

	@FindBy(linkText = "Agreement Versions")
	public WebElement agreementVersionsLnk ;

	@FindBy(linkText = "Configure Additional Invoice Item(s)")
	public WebElement configureAdditionalInvoiceItemLnk ;

	@FindBy(linkText = "Configure Categories for Sales Report")
	public WebElement configureCategoriesforSalesReportLnk ;

	@FindBy(linkText = "Configure Headings for Sales")
	public WebElement configureHeadingsforSalesLnk ;
	
	@FindBy(linkText = "Configure Non-Financial / KPI Categories for Sales Report")
	public WebElement configureNonFinancialKPICategoriesforSalesReportLnk ;

	@FindBy(linkText = "Configure Financials Document(s) to be Uploaded")
	public WebElement configureFinancialsDocumenttobeUploadedLnk ;

	@FindBy(linkText = "Configure Profit & Loss Categories")
	public WebElement configureProfitLossCategoriesLnk ;

	@FindBy(linkText = "Enable / Disable Franchise(s) for EFT")
	public WebElement enableDisableFranchiseforEFTLnk ;

	@FindBy(linkText = "Delete Sales Reports")
	public WebElement deleteSalesReport ;

	@FindBy(xpath = ".//div[@id='Financials']//a[@qat_adminlink='Configure Tax Rates']")
	public WebElement configureTaxRates ;

	@FindBy(xpath = ".//div[@id='Hidden_Links']//a[@qat_adminlink='Configure Tax Rates']")
	public WebElement hiddenLinkConfigureTaxRate;
	
	/*
	 * Admin CRM
	 * 
	 */

	@FindBy(linkText = "Contact Type Configuration")
	public WebElement contactTypeConfiguration ;

	@FindBy(linkText = "Configure Status")
	public WebElement configureStatus ;

	@FindBy(linkText = "Configure Medium")
	public WebElement configureMedium ;

	@FindBy(linkText = "Contact Source")
	public WebElement contactSource ;

	@FindBy(linkText = "Configure Lead Type")
	public WebElement configureLeadType ;

	@FindBy(linkText = "Configure Industry")
	public WebElement configureIndustry ;

	@FindBy(linkText = "Configure Opportunity Stages")
	public WebElement configureOpportunityStages ;

	@FindBy(xpath = ".//a[contains(text () ,'Configure Email Content Sent to Owner')]")
	public WebElement configureEmailContent ;

	@FindBy(xpath = ".//a[contains(text () ,'Configure Email Content Sent to Lead Owner')]")
	public WebElement salesConfigureEmailContent ;

	@FindBy(xpath = ".//a[contains(text () ,'Configure Email Content Sent to Contact Owner')]")
	public WebElement configureEmailContentcrm ;

	@FindBy(xpath = ".//a[contains(text () , 'Configure Subscription Type ')]")
	public WebElement configureCampaignEmailCategory ;

	@FindBy(xpath = ".//a[contains(text () , 'Manage Product / Service & Category')]")
	public WebElement manageProductServiceCategory ;

	/*
	 * @FindBy(xpath=".//a[contains(text () , 'Manage Web Form Generator')]")
	 * public WebElement manageWebFormGeneratorLink=
	 * DDDDDDDDDDDDDDDDDDDDD
	 */

	/*
	 * @FindBy(xpath=".//a[contains(text () , 'Manage Form Generator')]") public
	 * String infoMgrManageWebFormGeneratorLink=
	 * DDDDDDDDDDDDDDDDDDDDD
	 */

	@FindBy(xpath = ".//*[@qat_adminlink='Manage Web Form Generator']")
	public WebElement manageWebFormGeneratorLink ;

	@FindBy(xpath = ".//*[@qat_adminlink='Manage Form Generator FIM']")
	public WebElement infoMgrManageWebFormGeneratorLink ;

	@FindBy(xpath = ".//*[@id='Contact_Manager']//a[@qat_adminlink='Manage Form Generator']")
	public WebElement crmManageFormGenerator ;

	/* Harish Dwivedi */

	@FindBy(xpath = ".//a[contains(text () , 'Configure Last Contacted Field')]")
	public WebElement manageConfigureLastContactedFieldLink ;

	// Anukaran

	@FindBy(linkText = "Associate")
	public WebElement associateWithZiplnk ;

	@FindBy(linkText = "Add New")
	public WebElement AddNewZiplnk ;

	@FindBy(linkText = "Assign Zip / Postal Codes to Franchise Users")
	public WebElement assingnZipToFranUser ;

	@FindBy(linkText = "Setup Default Owner")
	public WebElement setDefaultOwner ;

	@FindBy(linkText = "Configure Default Contact Setting")
	public WebElement setDefaultContact ;

	@FindBy(linkText = "Configure Countries")
	public WebElement configureCountriesLnk ;

	@FindBy(xpath = ".//a[@qat_tabname='Quick Links']")
	public WebElement quickLink ;

	@FindBy(xpath = ".//td[@class='tab-dropdownMenu tm']")
	public WebElement moreBtn ;
	
	// Vipin Grover
	@FindBy(xpath = ".//td/*[@id='ms-parentfranchiseMenu']")
	public WebElement franchiseid ;
	
	@FindBy(xpath = ".//*[@id='searchButton']")
	public WebElement searchbuttonoffranchisee ;
	
	@FindBy(xpath = ".//td/div[@id='menuBar']/layer[@id='Actions_dynamicmenu1Bar']/a/img")
	public WebElement actionmenuoffranchisee ;
	
	@FindBy(xpath = ".//span[contains(text(),'Users')]")
	public WebElement usersoffranchisee ;
	
	@FindBy(xpath = ".//*[@value='Add Owner']")
	public WebElement addowner ;
	
	@FindBy(xpath = ".//*[@name='firstName']")
	public WebElement firstName ;
	
	@FindBy(xpath = ".//*[@name='lastName']")
	public WebElement lastName ;
	
	@FindBy(xpath = ".//*[@type='Submit']")
	public WebElement save ;

}
