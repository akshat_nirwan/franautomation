package com.builds.uimaps.admin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminUI {

	// Admin > Users Page

	@FindBy(xpath = ".//*[@qat_adminlink='Corporate User']")
	public WebElement corporateUserLink;

	@FindBy(xpath = ".//*[@qat_adminlink='Regional User' or contains(text(), 'Regional User')]")
	public WebElement regionalUserLink;

	@FindBy(xpath = ".//*[@qat_adminlink='Franchise User']")
	public WebElement franchiseUserLink;

	@FindBy(xpath = ".//*[@qat_adminlink='Suppliers']")
	public WebElement suppliersLink;

	@FindBy(xpath = ".//*[@qat_adminlink='Roles']")
	public WebElement rolesLink;

	@FindBy(xpath = ".//*[@qat_adminlink='Export']")
	public WebElement exportLink;

	@FindBy(xpath = ".//*[@qat_adminlink='Import Users']")
	public WebElement importUsers;

	@FindBy(xpath = "//input[@value='Add Franchise User']")
	public WebElement addfranchiseUserBtn;

	/*
	 * 
	 * Admin > Franchise Sales Links Below
	 * 
	 */

	@FindBy(xpath = ".//*[@qat_adminlink='Status']")
	public WebElement statusLink;

	@FindBy(xpath = ".//*[@qat_adminlink='Configure Service Lists']")
	public WebElement configureServiceListsLink;

	@FindBy(xpath = ".//*[@qat_adminlink='Configure Co-Applicant Relationship']")
	public WebElement configureCoApplicantRelationshipLink;

	@FindBy(xpath = ".//*[@qat_adminlink='Source']")
	public WebElement sourceLink;

	@FindBy(linkText = "Forecast Rating")
	public WebElement forecastRatingLink;

	/*
	 * @FindBy(linkText="Configure Lead Killed Reason") public WebElement
	 * configureLeadKilledReasonLnk=DDDDDDDDDDDDDDDDDDDDD
	 */

	@FindBy(xpath = ".//*[@qat_adminlink='Configure Lead Killed Reason']")
	public WebElement configureLeadKilledReasonLink;// =DDDDDDDDDDDDDDDDDDDDD
													// Lead Killed Reason']";

	@FindBy(linkText = "Marketing Code")
	public WebElement marketingCodeLink;

	@FindBy(linkText = "Brokers Type Configuration")
	public WebElement brokersTypeConfigurationLink;

	@FindBy(linkText = "Brokers Agency")
	public WebElement brokersAgencyLink;

	@FindBy(linkText = "Configure Lead Rating")
	public WebElement configureLeadRatingLink;

	@FindBy(xpath = ".//*[@id='Franchise_Sales']/ul/li[19]/a")
	public WebElement configureOptOutStatus;

	@FindBy(xpath = ".//*[@qat_adminlink='Configure Qualification Checklists']")
	public WebElement configureQualificationChecklists;

	/*
	 * @FindBy(linkText="Sales Territories") public WebElement salesTerritories=
	 * DDDDDDDDDDDDDDDDDDDDD
	 */
	@FindBy(xpath = ".//*[@qat_adminlink='Sales Territories']")
	public WebElement salesTerritories;

	@FindBy(linkText = "Configure Marketing Costs")
	public WebElement configureMarketingCosts;

	@FindBy(xpath = ".//*[@qat_adminlink='Configure Virtual Brochure Campaign Settings']")
	public WebElement configureVirtualBrochureCampaignSettings;

	@FindBy(xpath = ".//*[@qat_adminlink='Manage Candidate Portal']")
	public WebElement manageCandidatePortal;

	@FindBy(xpath = ".//*[@qat_adminlink='Configure DocuSign Settings']")
	public WebElement configureDocuSignSettings;

	@FindBy(xpath = ".//*[@qat_adminlink='Configure Boefly Integration Details']")
	public WebElement configureBoeflyIntegrationDetails;

	@FindBy(xpath = ".//*[@qat_adminlink='Configure SMS']")
	public WebElement configureSMS;

	@FindBy(xpath = ".//*[@qat_adminlink='Configure Search Criteria for Top Search']")
	public WebElement configureSearchCriteriaForAPIPlugins;

	@FindBy(xpath = ".//*[@qat_adminlink='Configure Email Capture']")
	public WebElement configureEmailCapture;

	@FindBy(linkText = "Assign Lead Owners")
	public WebElement assignLeadOwners;

	@FindBy(xpath = ".//*[@id='siteMainTable']/tbody/tr/td/table/tbody/tr[2]/td[2]/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr[2]/td[1]/input")
	public WebElement assignLeadOwnerbySalesTerritories;

	@FindBy(xpath = ".//*[@qat_adminlink='Setup Franchise Sales Lead Owners']")
	public WebElement setupFranchiseSalesLeadOwners;

	@FindBy(linkText = "Define Unregistered States / Provinces")
	public WebElement defineUnregisteredStatesProvinces;

	@FindBy(linkText = "Setup Email Campaign Triggers")
	public WebElement setupEmailCampaignTriggers;

	@FindBy(linkText = "Setup Automated Tasks")
	public WebElement setupAutomatedTasks;

	@FindBy(linkText = "Configure Task Emails")
	public WebElement configureTaskEmails;

	@FindBy(linkText = "Configure Duplicate Criteria")
	public WebElement configureDuplicateCriteria;

	@FindBy(xpath = ".//div[@id='Franchise_Sales']//a[@href='builderWebForm?module=fs&moduleId=2&fromWhere=admin']")
	public WebElement manageFormGenerator;

	@FindBy(xpath = ".//div[@id='Franchise_Sales']//a[contains(text(),'Configure Search Criteria for Top Search')]")
	public WebElement configureSearchCriteriaforTopSearch;

	@FindBy(xpath = ".//*[@id='Franchise_Sales']/ul//*[contains(text(),'Manage Web Form Generator')]")
	public WebElement manageWebFormGenerator;

	@FindBy(xpath = ".//*[@qat_adminlink='Configure Proven Match Integration Details']")
	public WebElement ConfigureProvenMatchIntegrationDetails;

	@FindBy(xpath = ".//*[@qat_adminlink='Configure Nathan Profiler Integration Details']")
	public WebElement ConfigureNathanProfilerIntegrationDetails;

	/*
	 * 
	 * Admin > Intranet Links Below
	 * 
	 */

	@FindBy(linkText = "Stories, RSS Feeds and Home Page View")
	public WebElement storiesRSSFeedsandHomePageViewLink;

	@FindBy(linkText = "Auto Archive Settings")
	public WebElement autoArchiveSettingsLink;

	@FindBy(linkText = "Archive Alerts")
	public WebElement archiveAlertsLink;

	@FindBy(linkText = "Archive Messages")
	public WebElement archiveMessagesLink;

	@FindBy(linkText = "News")
	public WebElement newsLink;

	@FindBy(linkText = "EPoll")
	public WebElement ePollLink;

	@FindBy(linkText = "Library")
	public WebElement libraryLink;

	@FindBy(xpath = ".//*[@id='Intranet']//a[@qat_adminlink='Library Logs']")
	public WebElement libraryLogs;

	@FindBy(linkText = "Administer Forum")
	public WebElement administerForumLink;

	@FindBy(linkText = "Related Links")
	public WebElement relatedLinksLink;

	@FindBy(xpath = ".//*[@id='Intranet']//a[@qat_adminlink='News Logs']")
	public WebElement newsLogs;

	@FindBy(linkText = "Configure FranBuzz Name")
	public WebElement configureFranBuzzNameLnk;

	@FindBy(linkText = "Configure Single Sign On")
	public WebElement configureSingleSignOn;

	@FindBy(linkText = "Configure What's New Email")
	public WebElement configureWhatsNewEmailLink;

	@FindBy(linkText = "Library Logs")
	public WebElement libraryLogsLink;

	@FindBy(linkText = "News Logs")
	public WebElement newsLogsLink;

	@FindBy(linkText = "Searched Logs")
	public WebElement searchedLogsLink;

	@FindBy(linkText = "Configure Server")
	public WebElement configureServerFimLink;

	/*
	 * Admin > Add New Franchise Location
	 */

	@FindBy(linkText = "Add New Franchise Location")
	public WebElement addNewFranchiseLocationLink;

	@FindBy(linkText = "Manage Franchise Locations")
	public WebElement manageFranchiseLocationsLink;

	/*
	 * Admin > Area / Region
	 */

	@FindBy(xpath = ".//*[@qat_adminlink='Manage Area / Region']")
	public WebElement manageAreaRegionLnk;

	@FindBy(xpath = ".//*[@qat_adminlink='Add New Area / Region']")
	public WebElement addNewAreaRegionLnk;

	/*
	 * admin > Division_US Management / Manage Division_US / Add Division_US
	 */

	@FindBy(linkText = "Division Management")
	public WebElement divisionManagementLink;

	@FindBy(xpath = ".//*[@qat_adminlink='Manage Division']")
	public WebElement manageDivisionLink;

	@FindBy(xpath = ".//*[@qat_adminlink='Add Division']")
	public WebElement addDivisionLink;

	// Add Divisional User
	// @FindBy(linkText="Divisional Users")

	@FindBy(xpath = ".//*[@qat_adminlink='Divisional Users' or contains(text(),'Divisional Users')]")
	public WebElement divisionalUser;
	// public WebElement divisionalUser=DDDDDDDDDDDDDDDDDDDDD
	// Users']";

	@FindBy(css = "input.cm_new_button_link")
	public WebElement addDivisionalUserLink;
	// public WebElement addDivisionalUserLnk="//linkText=#Divisional Users";

	/*
	 * Admin Hidden Links """
	 */

	@FindBy(linkText = "Configure Password Settings")
	public WebElement configurePasswordSettings;

	@FindBy(linkText = "Configure FranBuzz Name")
	public WebElement configureFranBuzzName;

	@FindBy(linkText = "Configure Field Ops Settings")
	public WebElement configureFieldOpsSettings;

	@FindBy(xpath = ".//*[@qat_adminlink='Configure New Hierarchy Level']")
	public WebElement configureNewHierarchyLevel;

	@FindBy(xpath = ".//*[@qat_adminlink='Configure New Hierarchy Level']")
	public WebElement configureProvenMatchIntegration;

	@FindBy(xpath = ".//*[@qat_adminlink='Configure New Hierarchy Level']")
	public WebElement configureNathanProfilerIntegration;

	/*
	 * Pwise
	 */

	@FindBy(linkText = "Manage Visit Form")
	public WebElement manageVisitFormLink;

	@FindBy(linkText = "Question Library")
	public WebElement questionLibraryLink;

	@FindBy(linkText = "Action Library")
	public WebElement actionLibraryLink;

	@FindBy(xpath = ".//a[@href='qaTabIntegration']")
	public WebElement qaTabIntegrationLink;

	@FindBy(linkText = "Manage Auditor")
	public WebElement manageAuditorLink;

	@FindBy(linkText = "Configure Reminder Alerts for Tasks")
	public WebElement configureReminderAlertsForTasksLink;

	@FindBy(linkText = "Configure Score")
	public WebElement configureScoreLink;

	@FindBy(xpath = ".//*[@id='Inspection_Management']/ul/li/a[contains(text () , 'Configure Alert Email Triggers for Tasks')]")
	public WebElement configureAlertEmailTriggersForTasksLink;

	@FindBy(linkText = "Configure Media Library")
	public WebElement configureMediaLibraryLink;

	@FindBy(linkText = "Manage Consultant")
	public WebElement manageConsultantLink;

	@FindBy(linkText = "Configure Visit Email Content")
	public WebElement ConfigureScheduleVisitEmailContentLink;

	@FindBy(linkText = "Delete Field Ops Visits")
	public WebElement deleteFieldOpsVisitsLink;

	/*
	 * admin > support
	 */

	@FindBy(linkText = "Manage FAQs")
	public WebElement ManageFAQsLink;

	@FindBy(linkText = "Manage Department")
	public WebElement manageDepartmentLink;

	@FindBy(linkText = "Manage Contact Information")
	public WebElement manageContactInformationLink;

	@FindBy(linkText = "Manage Message Templates")
	public WebElement manageMessageTemplatesLink;

	@FindBy(linkText = "Configure Ticket Status")
	public WebElement configureTicketStatusLink;

	/*
	 * admin > franchise opener
	 */

	@FindBy(linkText = "Task Checklist")
	public WebElement taskChecklistLink;

	@FindBy(linkText = "Equipment Checklist")
	public WebElement equipmentChecklistLink;

	@FindBy(linkText = "Document Checklist")
	public WebElement documentChecklistLink;

	@FindBy(linkText = "Picture Checklist")
	public WebElement pictureChecklistLink;

	@FindBy(xpath = ".//*[@id='Franchise_Opener']//a[.='Secondary Checklists' or .='Secondary Checklist']")
	public WebElement secondaryChecklistLink;

	@FindBy(linkText = "Responsible Department")
	public WebElement responsibleDepartmentLink;

	@FindBy(xpath = ".//*[@id='Franchise_Opener']/ul/li/a[contains(text () , 'Overdue Alert Frequency')]")
	public WebElement overdueAlertFrequencyLink;

	@FindBy(linkText = "Alert Email Content")
	public WebElement alertEmailContentLink;

	@FindBy(linkText = "Customize Profiles")
	public WebElement customizeProfilesLink;

	@FindBy(linkText = "Manage Reference Dates")
	public WebElement manageReferenceDatesLink;

	@FindBy(linkText = "Configure Project Status")
	public WebElement configureProjectStatusLink;

	@FindBy(linkText = "Manage Groups")
	public WebElement manageGroupsLink;

	@FindBy(linkText = "Configure Checklist Display Setting")
	public WebElement configureChecklistDisplaySettings;

	@FindBy(linkText = "Configure Alert Email Triggers for Tasks")
	public WebElement configureAlertEmailTriggersTask;

	@FindBy(linkText = "Configure Milestone Date Triggers")
	public WebElement ConfigureMilestoneDateTriggers;

	/*
	 * admin > training
	 */

	@FindBy(linkText = "Course Management")
	public WebElement courseManagementLink;

	@FindBy(linkText = "Plan & Certificates")
	public WebElement PlanAndCertificatesLink;

	@FindBy(linkText = "Questions Library")
	public WebElement questionLibraryTrainingLink;

	@FindBy(linkText = "Configure Server")
	public WebElement configureServerTrainingLink;

	@FindBy(linkText = "Configure Invite Email")
	public WebElement configureInviteEmailLink;

	/*
	 * admin > FIM
	 */

	@FindBy(linkText = "Manage Form Generator")
	public WebElement manageFormGeneratorLink;

	@FindBy(linkText = "Customize Regional Form")
	public WebElement customizeRegionalFormLink;

	@FindBy(linkText = "Triggers and Auditing")
	public WebElement triggersAndAuditingLink;

	@FindBy(linkText = "Configure FIM Email Capture")
	public WebElement configureFIMEmailCaptureLink;

	@FindBy(linkText = "Configure Ownership Transfer Status")
	public WebElement configureOwnershipTransferStatusLink;

	@FindBy(linkText = "Configure Other Addresses Heading")
	public WebElement configureOtherAddressesHeadingLink;

	@FindBy(linkText = "Configure Search Criteria for Top Search")
	public WebElement configureSearchCriteriaforTopSearchLink;

	@FindBy(linkText = "Configure Opt-out Message")
	public WebElement ConfigureOptoutMessageLink;

	@FindBy(linkText = "Configure Server")
	public WebElement configureServerLink;

	/*
	 * @FindBy(linkText="Configure Area / Region Label") public WebElement
	 * configureAreaRegionLblLnk=DDDDDDDDDDDDDDDDDDDDD
	 */

	@FindBy(xpath = ".//a[@qat_adminlink='Configure Area / Region Label']")
	public WebElement configureAreaRegionLblLink;

	/*
	 * admin > configuration
	 */

	@FindBy(xpath = ".//a[@qat_adminlink='Configure Quick Links']")
	public WebElement configureQuickLink;

	@FindBy(linkText = "Configure Store Type")
	public WebElement configureStoreTypeLink;

	@FindBy(linkText = "Configure Task Type")
	public WebElement configureTaskType;

	@FindBy(xpath = ".//*[@id='Configuration']//a[@href='configureOffensiveKeywords']")
	public WebElement configureOffensiveWord;

	@FindBy(linkText = "Configure Dictionary")
	public WebElement configureDictionaryLink;

	@FindBy(linkText = "Configure Decimal Places")
	public WebElement configureDecimalPlacesLink;

	@FindBy(linkText = "PII Configuration")
	public WebElement piiConfigurationLink;

	@FindBy(linkText = "Configure Calendar Start Day")
	public WebElement configureCalendarStartDayLink;

	@FindBy(linkText = "Configure Calendar Event Categories")
	public WebElement configureCalendarEventCategoriesLink;

	@FindBy(linkText = "Configure Task Display On Calendar")
	public WebElement configureTaskDisplayOnCalendarLink;

	@FindBy(linkText = "Configure Call Status")
	public WebElement configureCallStatusLink;

	@FindBy(linkText = "Configure Communication Type")
	public WebElement configureCommunicationTypeLink;

	@FindBy(xpath = ".//a[@qat_adminlink='Configure Site Clearance']")
	public WebElement configureSiteClearance;

	@FindBy(linkText = "Configure Info Mgr Tabs")
	public WebElement configureInfoMgrTabsLink;

	/*
	 * FDD Management
	 */

	@FindBy(xpath = ".//*[@id='UFOC_Management']//a[contains(text(),'FDD Management')]")
	public WebElement fddManagementLink;

	@FindBy(linkText = "FDD Email Template summary")
	public WebElement fddEmailTemplateSummaryLink;

	@FindBy(linkText = "Log on credentials duration")
	public WebElement logOnCredentialsDurationLink;

	@FindBy(linkText = "ITEM 23 - RECEIPT Summary")
	public WebElement item23ReceiptSummaryLink;

	@FindBy(linkText = "Configure Email sent Prior to FDD Email")
	public WebElement configureEmailSentPriorToFDDEmailLink;

	@FindBy(linkText = "Configure Email sent Prior to FDD Expiration")
	public WebElement configureEmailsentPriortoFDDExpirationLink;

	/*
	 * Access Control Access Control
	 */

	@FindBy(linkText = "Deleted Logs")
	public WebElement deletedLogsLink;

	@FindBy(linkText = "Login Logs")
	public WebElement loginLogsLink;

	@FindBy(xpath = ".//a[@qat_adminlink='User Account Summary']")
	public WebElement userAccountSummary;

	@FindBy(linkText = "Link Lead With Existing Lead / Owner")
	public WebElement linkLeadWithExisting;

	/*
	 * admin > fin********By Gaurav Tomar********
	 */

	@FindBy(xpath = ".//a[@href='nachaAreaStatus']")
	public WebElement enableDisableAreaFranchiseforEFTLink;

	@FindBy(linkText = "Agreement Versions")
	public WebElement agreementVersionsLink;

	@FindBy(linkText = "Configure Additional Invoice Item(s)")
	public WebElement configureAdditionalInvoiceItemLink;

	@FindBy(linkText = "Configure Categories for Sales Report")
	public WebElement configureCategoriesforSalesReportLink;

	@FindBy(linkText = "Configure Non-Financial / KPI Categories for Sales Report")
	public WebElement configureNonFinancialKPICategoriesforSalesReportLink;

	@FindBy(linkText = "Configure Financials Document(s) to be Uploaded")
	public WebElement configureFinancialsDocumenttobeUploadedLink;

	@FindBy(linkText = "Configure Profit & Loss Categories")
	public WebElement configureProfitLossCategoriesLink;

	@FindBy(linkText = "Enable / Disable Franchise(s) for EFT")
	public WebElement enableDisableFranchiseforEFTLink;

	@FindBy(linkText = "Delete Sales Reports")
	public WebElement deleteSalesReport;

	@FindBy(linkText = "Configure Tax Rates")
	public WebElement configureTaxRates;

	/*
	 * Admin CRM
	 * 
	 */

	@FindBy(linkText = "Contact Type Configuration")
	public WebElement contactTypeConfiguration;

	@FindBy(linkText = "Configure Status")
	public WebElement configureStatus;

	@FindBy(linkText = "Configure Medium")
	public WebElement configureMedium;

	@FindBy(linkText = "Contact Source")
	public WebElement contactSource;

	@FindBy(linkText = "Configure Lead Type")
	public WebElement configureLeadType;

	@FindBy(linkText = "Configure Industry")
	public WebElement configureIndustry;

	@FindBy(linkText = "Configure Opportunity Stages")
	public WebElement configureOpportunityStages;

	@FindBy(xpath = ".//a[contains(text () ,'Configure Email Content Sent to Owner')]")
	public WebElement configureEmailContent;

	@FindBy(xpath = ".//a[contains(text () ,'Configure Email Content Sent to Lead Owner')]")
	public WebElement salesConfigureEmailContent;

	@FindBy(xpath = ".//a[contains(text () ,'Configure Email Content Sent to Contact Owner')]")
	public WebElement configureEmailContentcrm;

	@FindBy(xpath = ".//a[contains(text () , 'Configure Campaign Email Category')]")
	public WebElement configureCampaignEmailCategory;

	@FindBy(xpath = ".//a[contains(text () , 'Manage Product / Service & Category')]")
	public WebElement manageProductServiceCategory;

	/*
	 * @FindBy(xpath=".//a[contains(text () , 'Manage Web Form Generator')]")
	 * public WebElement manageWebFormGeneratorLink=
	 * DDDDDDDDDDDDDDDDDDDDD
	 */

	/*
	 * @FindBy(xpath=".//a[contains(text () , 'Manage Form Generator')]") public
	 * WebElement infoMgrManageWebFormGeneratorLink=
	 * DDDDDDDDDDDDDDDDDDDDD
	 */

	@FindBy(xpath = ".//*[@qat_adminlink='Manage Web Form Generator']")
	public WebElement manageWebFormGeneratorLink;

	@FindBy(xpath = ".//*[@qat_adminlink='Configure Webform Settings']")
	public WebElement configureWebformSettings;

	@FindBy(xpath = ".//*[@qat_adminlink='Configure Web Form Email Content']")
	public WebElement configureWebFormEmailContent;

	@FindBy(xpath = ".//*[@qat_adminlink='Configure Weekly Updates Email']")
	public WebElement configureWeeklyUpdatesEmail;

	@FindBy(xpath = ".//*[@qat_adminlink='Configure Brokers Email Campaign Setting']")
	public WebElement configureBrokersEmailCampaignSetting;

	@FindBy(xpath = ".//*[@qat_adminlink='Configure Heat Meter']")
	public WebElement configureHeatMeter;

	@FindBy(xpath = ".//*[@qat_adminlink='Manage Form Generator FIM']")
	public WebElement infoMgrManageWebFormGeneratorLink;

	@FindBy(xpath = ".//*[@id='Contact_Manager']//a[@qat_adminlink='Manage Form Generator']")
	public WebElement crmManageFormGenerator;

	/* Harish Dwivedi */

	@FindBy(xpath = ".//a[contains(text () , 'Configure Last Contacted Field')]")
	public WebElement manageConfigureLastContactedFieldLink;

	@FindBy(xpath = ".//a[contains(text () , 'Finance Setup Preferences')]")
	public WebElement financeSetupPreferencesLink;

	// Anukaran

	@FindBy(linkText = "Associate")
	public WebElement associateWithZipLink;

	@FindBy(linkText = "Add New")
	public WebElement AddNewZiplnk;

	@FindBy(linkText = "Assign Zip / Postal Codes to Franchise Users")
	public WebElement assingnZipToFranUser;

	@FindBy(linkText = "Setup Default Owner")
	public WebElement setDefaultOwner;

	@FindBy(linkText = "Configure Default Contact Setting")
	public WebElement setDefaultContact;

	@FindBy(linkText = "Configure Countries")
	public WebElement configureCountriesLink;

	@FindBy(xpath = ".//a[@qat_tabname='Quick Links']")
	public WebElement quickLink;

	@FindBy(xpath = ".//td[@class='tab-dropdownMenu tm']")
	public WebElement moreBtn;

	public AdminUI(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
