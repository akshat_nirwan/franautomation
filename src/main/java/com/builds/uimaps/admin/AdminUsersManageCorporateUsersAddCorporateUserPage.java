package com.builds.uimaps.admin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.builds.uimaps.common.CommonUI;

public class AdminUsersManageCorporateUsersAddCorporateUserPage extends CommonUI {
	@FindBy(id = "userName")
	public WebElement userName;

	@FindBy(xpath = ".//input[@id='password' and @type='password']")
	public WebElement password ;

	@FindBy(xpath = ".//input[@id='confirmPassword' and @type='password']")
	public WebElement confirmPassword ;

	@FindBy(id = "ms-parentroleID")
	public WebElement roleMultiSelectBtn;

	@FindBy(id = "userType")
	public WebElement type;

	@FindBy(id = "expiryDays")
	public WebElement expiryDays;

	@FindBy(css = "button.ms-choice")
	public WebElement rolesBtn;

	@FindBy(css = "input.searchInputMultiple")
	public WebElement searchRoles;

	@FindBy(id = "ms-parentroleID")
	public WebElement role;

	@FindBy(id = "selectAll")
	public WebElement selectAll;

	@FindBy(id = "timezone")
	public WebElement timeZone;

	@FindBy(name = "jobTitle")
	public WebElement jobTitle;

	@FindBy(id = "userLanguage")
	public WebElement userLanguage;

	@FindBy(id = "firstName")
	public WebElement firstName;

	@FindBy(id = "lastName")
	public WebElement lastName;

	@FindBy(id = "address")
	public WebElement address;

	@FindBy(id = "city")
	public WebElement city;

	@FindBy(id = "country")
	public WebElement country;

	@FindBy(id = "zipcode")
	public WebElement zipcode;

	@FindBy(id = "state")
	public WebElement state;

	@FindBy(id = "phone1")
	public WebElement phone1;

	@FindBy(id = "phoneExt1")
	public WebElement phoneExt1;

	@FindBy(id = "phone2")
	public WebElement phone2;

	@FindBy(id = "phoneExt2")
	public WebElement phoneExt2;

	@FindBy(id = "fax")
	public WebElement fax;

	@FindBy(id = "mobile")
	public WebElement mobile;

	@FindBy(id = "email")
	public WebElement email;

	@FindBy(id = "loginUserIp")
	public WebElement loginUserIp;

	@FindBy(id = "isBillable")
	public WebElement isBillable;

	@FindBy(id = "auditor")
	public WebElement consultant;

	@FindBy(name = "Close")
	public WebElement CloseColorBox;

	@FindBy(id = "userPictureName")
	public WebElement userPictureName;

	@FindBy(id = "sendNotification")
	public WebElement sendNotification;

	@FindBy(id = "Submit")
	public WebElement submit;

	@FindBy(xpath = ".//*[@id='search']")
	public WebElement searchCorpUser ;

	@FindBy(xpath = ".//*[@id='searchButton']")
	public WebElement searchCorpUserBtn ;

	@FindBy(xpath = ".//input[@onclick='reassignWindow()']")
	public WebElement reassignBtn ;

	@FindBy(xpath = ".//input[@onclick='deactivateUser()']")
	public WebElement deactivateBtn ;

	@FindBy(xpath = ".//input[@onclick='deactivateUser()']")
	public WebElement deleteBtn ;

	@FindBy(xpath = ".//*[@id='fsTaskCountlist']")
	public WebElement reassignToTaskDrp ;

	@FindBy(xpath = ".//*[@id='leadCountList']")
	public WebElement reassignToLeadDrp ;

	@FindBy(xpath = ".//*[@id='Reassign']")
	public WebElement reassignToBtn ;

	@FindBy(xpath = ".//a[@qat_tabname='Deactivated Users']/span")
	public WebElement deactivatedUsersTab ;

	@FindBy(xpath = ".//a[@qat_tabname='Active Users']/span")
	public WebElement activeUsersTab ;

	@FindBy(xpath = ".//*[@id='selectedUsersForFS']")
	public WebElement usersAsSalesLeadOwners ;

	public AdminUsersManageCorporateUsersAddCorporateUserPage(WebDriver driver) {
		// PageFactory.initElements(driver, this);
		super(driver);
	}
}
