package com.builds.uimaps.admin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminUsersManageCorporateUsersPage {

	@FindBy(xpath = ".//input[@value='Add Corporate User']")
	public WebElement addCorporateUserbtn ;

	@FindBy(id = "searchButton")
	public WebElement searchButton;

	@FindBy(id = "countryId")
	public WebElement countryName;

	@FindBy(id = "search")
	public WebElement search;

	@FindBy(xpath = ".//input[@value='Send Message']")
	public WebElement sendMessagesBtn ;

	public AdminUsersManageCorporateUsersPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
