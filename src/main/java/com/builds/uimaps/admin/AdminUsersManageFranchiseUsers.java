package com.builds.uimaps.admin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.builds.uimaps.common.CommonUI;

public class AdminUsersManageFranchiseUsers extends CommonUI {

	@FindBy(xpath = ".//input[@value='Add Franchise User' and @type='button']")
	public WebElement addFranchiseUser ;

	@FindBy(name = "regionKey")
	public WebElement regionDrp;

	@FindBy(name = "countryId")
	public WebElement countryDrp;

	@FindBy(name = "franchiseMenu")
	public WebElement franchiseIdDrp;

	@FindBy(name = "franchiseeOwner")
	public WebElement franchiseeOwnerDrp;

	@FindBy(id = "muidKey")
	public WebElement muidDrp;

	@FindBy(name = "search")
	public WebElement SearchUserByNameTxt;

	@FindBy(name = "searchButton")
	public WebElement searchButton;

	public AdminUsersManageFranchiseUsers(WebDriver driver) {
		super(driver);
	}
}
