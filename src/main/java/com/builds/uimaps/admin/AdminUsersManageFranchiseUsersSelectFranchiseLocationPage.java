package com.builds.uimaps.admin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminUsersManageFranchiseUsersSelectFranchiseLocationPage {

	@FindBy(id = "franchiseeMuid")
	public WebElement franchiseIdRadio;

	@FindBy(id = "franchiseeNo")
	public WebElement franchsieIdDrp;

	@FindBy(name = "addUser")
	public WebElement addUserBtn;

	public AdminUsersManageFranchiseUsersSelectFranchiseLocationPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
