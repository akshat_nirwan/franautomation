package com.builds.uimaps.admin;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminUsersManageManageFranchiseUsersPage {

	@FindBy(xpath = "//input[@value='Add Franchise User']")
	public WebElement addFranchiseUserBtn ;

	@FindBy(id = "franchiseeMuid")
	public List<WebElement> franchiseIdMuid;

	@FindBy(id = "franchiseeNo")
	public WebElement franchiseeNo;

	// Muid User

	@FindBy(xpath = ".//*[@id='ms-parentmuidValue']/button")
	public WebElement MUIDSelectBtn ;

	@FindBy(xpath = ".//*[@id='ms-parentmuidValue']/div/div/input")
	public WebElement MUIDSearchBx ;

	@FindBy(xpath = ".//*[@id='ms-parentmuidValue']/div//input[@id='selectAll']")
	public WebElement MUIDSelectAll ;

	@FindBy(xpath = ".//input[@value='Add User']")
	public WebElement addUserBtn ;

	@FindBy(id = "userName")
	public WebElement userName;

	@FindBy(xpath = ".//input[@id='password' and @type='password']")
	public WebElement password ;

	@FindBy(xpath = ".//input[@id='confirmPassword' and @type='password']")
	public WebElement confirmPassword ;

	@FindBy(id = "userType")
	public WebElement type;

	@FindBy(id = "expiryDays")
	public WebElement expiryDays;
	
	@FindBy(id="ms-parentroleID")
	public WebElement selectRole;

	@FindBy(id = "timezone")
	public WebElement timeZone;

	@FindBy(id = "users")
	public WebElement userType;

	@FindBy(id = "region")
	public WebElement region;

	@FindBy(xpath = "//input[@name='jobTitle']")
	public WebElement jobTitle ;

	@FindBy(id = "salutation")
	public WebElement salutation;

	@FindBy(id = "userLanguage")
	public WebElement userLanguage;

	@FindBy(id = "firstName")
	public WebElement firstName;

	@FindBy(id = "lastName")
	public WebElement lastName;

	@FindBy(id = "address")
	public WebElement address;

	@FindBy(id = "city")
	public WebElement city;

	@FindBy(id = "country")
	public WebElement country;

	@FindBy(id = "zipcode")
	public WebElement zipcode;

	@FindBy(id = "state")
	public WebElement state;

	@FindBy(id = "phone1")
	public WebElement phone1;

	@FindBy(id = "phoneExt1")
	public WebElement phoneExt1;

	@FindBy(id = "phone2")
	public WebElement phone2;

	@FindBy(id = "phoneExt2")
	public WebElement phoneExt2;

	@FindBy(id = "fax")
	public WebElement fax;

	@FindBy(id = "mobile")
	public WebElement mobile;

	@FindBy(id = "email")
	public WebElement email;

	@FindBy(id = "loginUserIp")
	public WebElement loginUserIp;

	@FindBy(id = "isBillable")
	public WebElement isBillable;

	@FindBy(id = "birthMonth")
	public WebElement birthMonth;

	@FindBy(id = "birthDate")
	public WebElement birthDate;

	@FindBy(id = "userPictureName")
	public WebElement userPictureName;

	@FindBy(id = "sendNotification")
	public WebElement sendNotification;

	@FindBy(name = "visibleToStore")
	public WebElement visibleToStore;

	@FindBy(id = "Submit")
	public WebElement submit;

	@FindBy(id = "muidValue")
	public WebElement muidValueSelect;

	@FindBy(id = "ms-parentroleID")
	public WebElement roleMultiSelectBtn;
	
	@FindBy(xpath=".//input[@type='button' and @value='Deactivate']")
	public WebElement deactivatedBtnAtCBox;
	
	@FindBy(xpath=".//input[@type='button' and @value='Delete']")
	public WebElement deleteBtnAtCbox;
	
	@FindBy(id="ms-parentfranchiseMenu")
	public WebElement Franchise_IDMulti;
	
	@FindBy(id="searchButton")
	public WebElement searchButton;

	public AdminUsersManageManageFranchiseUsersPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
