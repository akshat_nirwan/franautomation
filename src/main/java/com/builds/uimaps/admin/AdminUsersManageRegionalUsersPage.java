package com.builds.uimaps.admin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminUsersManageRegionalUsersPage {
	@FindBy(xpath = ".//input[@value='Add Regional User']")
	public WebElement addRegionalUserBtn ;

	@FindBy(id = "search")
	public WebElement searchUserNameTxt;

	@FindBy(id = "searchButton")
	public WebElement searchBtn;

	public AdminUsersManageRegionalUsersPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
