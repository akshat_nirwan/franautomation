package com.builds.uimaps.admin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminUsersRolesAddNewRolePage {
	@FindBy(name = "roleType")
	public WebElement roleType;

	@FindBy(name = "roleName")
	public WebElement roleName;

	@FindBy(name = "allPrivileges")
	public WebElement allPrivileges;

	@FindBy(name = "Submit")
	public WebElement submit;

	// Training Role
	@FindBy(xpath = ".//*[.='Can Manage Training']/../td/input")
	public WebElement canManageTraining ;

	// FieldOps
	
	@FindBy(xpath = ".//*[.='Grants Access to Field Ops']/../td/input")
	public WebElement canManageFieldOps ;

	@FindBy(xpath = ".//tr/td[contains(text(),'Take part in Courses')]/ancestor::tr/td[4]/input")
	public WebElement takepartincourse ;

	@FindBy(xpath = ".//tr/td[contains(text(),'Can View Events')]/ancestor::tr/td[4]/input")
	public WebElement canviewevents ;

	@FindBy(xpath = ".//td[contains(text(),'Can View Messages')]/ancestor::tr/td/input[@id='14' or @id='99' or @id='45']")
	public WebElement canViewMessages ;

	@FindBy(xpath = ".//td[contains(text(),'Can View Library')]/ancestor::tr/td/input[@id='14' or @id='99' or @id='45']")
	public WebElement canViewLibrary ;

	@FindBy(xpath = ".//td[contains(text(),'Can View What')]/ancestor::tr/td/input[@id='14' or @id='99' or @id='45']")
	public WebElement canViewWhatNew ;

	@FindBy(xpath = ".//td[contains(text(),'Can View EPoll')]/ancestor::tr/td/input[@id='14' or @id='99' or @id='45']")
	public WebElement canViewEPoll ;

	@FindBy(xpath = ".//td[contains(text(),'Can View Related Links')]/ancestor::tr/td/input[@id='14' or @id='99' or @id='45']")
	public WebElement canViewRelatedLinks ;

	@FindBy(xpath = ".//td[contains(text(),'Can View Alerts')]/ancestor::tr/td/input[@id='14' or @id='99' or @id='45']")
	public WebElement canViewAlerts ;

	@FindBy(xpath = ".//td[contains(text(),'Can View News')]/ancestor::tr/td/input[@id='14' or @id='99' or @id='45']")
	public WebElement canViewNews ;

	@FindBy(xpath = ".//td[contains(text(),'Can View Corporate Users')]/ancestor::tr/td/input[@id='14' or @id='99' or @id='45']")
	public WebElement canViewCorporateUsers ;

	@FindBy(xpath = ".//td[contains(text(),'Can View Franchise Users')]/ancestor::tr/td/input[@id='14' or @id='99' or @id='45']")
	public WebElement canViewFranchiseUsers ;

	@FindBy(xpath = ".//td[contains(text(),'Can View Suppliers')]/ancestor::tr/td/input[@id='14' or @id='99' or @id='45']")
	public WebElement canViewSuppliers ;

	@FindBy(xpath = ".//td[contains(text(),'Can View Regional Users')]/ancestor::tr/td/input[@id='14' or @id='99' or @id='45']")
	public WebElement canViewRegionalUsers ;
	
	
//CRM vipinS
	@FindBy(xpath = ".//td[contains(text(),'Can Manage Form Generator')]/ancestor::tr/td/input[@id='204' or @id='5' or @id ='4']")
	public WebElement Can_Manage_Form_Generator ;
	
	@FindBy(xpath = ".//td[contains(text(),'Can Configure Tab Generator')]/ancestor::tr/td/input[@id='204' or @id='5' or @id ='4']")
	public WebElement Can_Configure_Tab_Generator ;
	
	@FindBy(xpath = ".//td[contains(text(),'Can Manage Web Form Generator')]/ancestor::tr/td/input[@id='204' or @id='5' or @id ='4']")
	public WebElement 	Can_Manage_Web_Form_Generator ;
	
	@FindBy(xpath = ".//td[contains(text(),'Can View All Contacts')]/ancestor::tr/td/input[@id='204' or @id='5' or @id ='4']")
	public WebElement Can_View_All_Contacts ;
	
	@FindBy(xpath = ".//td[contains(text(),'Can Add / Modify Contacts')]/ancestor::tr/td/input[@id='204' or @id='5' or @id ='4']")
	public WebElement 	Can_Add_Modify_Contacts ;
	
	@FindBy(xpath = ".//td[contains(text(),'Can Convert Contact to Lead')]/ancestor::tr/td/input[@id='204' or @id='5' or @id ='4']")
	public WebElement 	Can_Convert_Contact_to_Lead ;
	
	@FindBy(xpath = ".//td[contains(text(),'Can Delete Contacts')]/ancestor::tr/td/input[@id='204' or @id='5' or @id ='4']")
	public WebElement Can_Delete_Contacts ;
	
	@FindBy(xpath = ".//td[contains(text(),'Can View All Opportunities')]/ancestor::tr/td/input[@id='204' or @id='5' or @id ='4']")
	public WebElement Can_View_All_Opportunities ;
	
	@FindBy(xpath = ".//td[contains(text(),'Can View All Contact Upload History')]/ancestor::tr/td/input[@id='204' or @id='5' or @id ='4']")
	public WebElement 	Can_View_All_Contact_Upload_History ;
	
	@FindBy(xpath = ".//td[contains(text(),'Can View All Leads')]/ancestor::tr/td/input[@id='204' or @id='5' or @id ='4']")
	public WebElement Can_View_All_Leads ;
	
	@FindBy(xpath = ".//td[contains(text(),'Can Add / Modify Leads')]/ancestor::tr/td/input[@id='204' or @id='5' or @id ='4']")
	public WebElement Can_Add_Modify_Leads ;
	
	@FindBy(xpath = ".//td[contains(text(),'Can Convert Lead to Contact')]/ancestor::tr/td/input[@id='204' or @id='5' or @id ='4']")
	public WebElement Can_Convert_Lead_to_Contact ;
	
	@FindBy(xpath = ".//td[contains(text(),'Can Delete Leads')]/ancestor::tr/td/input[@id='204' or @id='5' or @id ='4']")
	public WebElement 	Can_Delete_Leads ;
	
	@FindBy(xpath = ".//td[contains(text(),'Can Manage Private Groups')]/ancestor::tr/td/input[@id='204' or @id='5' or @id ='4']")
	public WebElement Can_Manage_Private_Groups ;
	
	@FindBy(xpath = ".//td[contains(text(),'Can View CM Document')]/ancestor::tr/td/input[@id='204' or @id='5' or @id ='4']")
	public WebElement Can_View_CM_Document ;
	
	@FindBy(xpath = ".//td[contains(text(),'Can Add / Modify CM Document')]/ancestor::tr/td/input[@id='204' or @id='5' or @id ='4']")
	public WebElement Can_Add_Modify_CM_Document ;
	
	@FindBy(xpath = ".//td[contains(text(),'Can Delete CM Document')]/ancestor::tr/td/input[@id='204' or @id='5' or @id ='4']")
	public WebElement Can_Delete_CM_Document ;
	
	@FindBy(xpath = ".//td[contains(text(),'Can View Accounts')]/ancestor::tr/td/input[@id='204' or @id='5' or @id ='4']")
	public WebElement Can_View_Accounts ;
	
	@FindBy(xpath = ".//td[contains(text(),'Can Add / Modify Accounts')]/ancestor::tr/td/input[@id='204' or @id='5' or @id ='4']")
	public WebElement Can_Add_Modify_Accounts ;
	
	
	@FindBy(xpath = ".//td[contains(text(),'Can Delete Accounts')]/ancestor::tr/td/input[@id='204' or @id='5' or @id ='4']")
	public WebElement Can_Delete_Accounts ;
	
	@FindBy(xpath = ".//td[contains(text(),'Can View Tasks')]/ancestor::tr/td/input[@id='204' or @id='5' or @id ='4']")
	public WebElement Can_View_Tasks ;
	
	@FindBy(xpath = ".//td[contains(text(),'Can Add / Modify Tasks')]/ancestor::tr/td/input[@id='204' or @id='5' or @id ='4']")
	public WebElement Can_Add_Modify_Tasks ;
	
	@FindBy(xpath = ".//td[contains(text(),'Can Delete Tasks')]/ancestor::tr/td/input[@id='204' or @id='5' or @id ='4']")
	public WebElement Can_Delete_Tasks ;
	
	@FindBy(xpath = ".//td[contains(text(),'Can View Transaction')]/ancestor::tr/td/input[@id='204' or @id='5' or @id ='4']")
	public WebElement Can_View_Transaction ;
	
	@FindBy(xpath = ".//td[contains(text(),'Can Add/Modify Transaction')]/ancestor::tr/td/input[@id='204' or @id='5' or @id ='4']")
	public WebElement Can_Add_Modify_Transaction ;
	
	@FindBy(xpath = ".//td[contains(text(),'Can Delete Transaction')]/ancestor::tr/td/input[@id='204' or @id='5' or @id ='4']")
	public WebElement 	Can_Delete_Transaction ;
	
	@FindBy(xpath = ".//td[contains(text(),'Can Add / Modify / Delete Media in Media Library')]/ancestor::tr/td/input[@id='204' or @id='5' or @id ='4']")
	public WebElement Can_Add_Modify_Delete_Media_in_Media_Library ;
	
	@FindBy(xpath = ".//td[contains(text(),'Can View Email Subscription Report')]/ancestor::tr/td/input[@id='204' or @id='5' or @id ='4']")
	public WebElement Can_View_Email_Subscription_Report ;
	
	@FindBy(xpath = ".//td[contains(text(),'Can View Private Folders / Templates of Other Users')]/ancestor::tr/td/input[@id='204' or @id='5' or @id ='4']")
	public WebElement Can_View_Private_Campaign_Tempaltes_Other_User ;
	
	
	//VipinG
	
	@FindBy(xpath = ".//td[contains(text(),'Can View Campaign Center')]/ancestor::tr/td/input[@id='204' or @id='5' or @id ='4']")
	public WebElement Can_View_Campaign_Center ;
	
	public AdminUsersRolesAddNewRolePage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
