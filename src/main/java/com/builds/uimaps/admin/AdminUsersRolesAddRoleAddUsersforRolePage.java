package com.builds.uimaps.admin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminUsersRolesAddRoleAddUsersforRolePage {
	@FindBy(xpath = ".//input[@value='Assign Later']")
	public WebElement assignLater ;

	public AdminUsersRolesAddRoleAddUsersforRolePage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
