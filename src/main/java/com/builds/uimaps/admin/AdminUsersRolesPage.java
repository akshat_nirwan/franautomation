package com.builds.uimaps.admin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminUsersRolesPage {
	@FindBy(xpath = ".//input[contains(@value,'Add New Role')]")
	public WebElement addNewRoleBtn ;

	public AdminUsersRolesPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
