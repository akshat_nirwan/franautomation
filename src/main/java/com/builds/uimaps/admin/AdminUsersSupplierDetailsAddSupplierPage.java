package com.builds.uimaps.admin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminUsersSupplierDetailsAddSupplierPage {
	@FindBy(id = "supplierName")
	public WebElement supplierName;

	@FindBy(id = "rating")
	public WebElement rating;

	@FindBy(id = "firstName")
	public WebElement firstName;

	@FindBy(id = "lastName")
	public WebElement lastName;

	@FindBy(id = "address")
	public WebElement address;

	@FindBy(id = "city")
	public WebElement city;

	@FindBy(id = "country")
	public WebElement country;

	@FindBy(id = "zipcode")
	public WebElement zipcode;

	@FindBy(id = "state")
	public WebElement state;

	@FindBy(id = "phone1")
	public WebElement phone1;

	@FindBy(id = "phone1Extn")
	public WebElement phone1Extn;

	@FindBy(id = "phone2")
	public WebElement phone2;

	@FindBy(id = "phone2Extn")
	public WebElement phone2Extn;

	@FindBy(id = "fax")
	public WebElement fax;

	@FindBy(id = "mobile")
	public WebElement mobile;

	@FindBy(id = "emailID")
	public WebElement emailID;

	@FindBy(id = "baseUrl")
	public WebElement homePageUrl;

	@FindBy(id = "description")
	public WebElement description;

	@FindBy(name = "vendorType")
	public WebElement vendorType;

	@FindBy(name = "checkFlagView")
	public WebElement checkFlagView;

	@FindBy(css = "button.ms-choice")
	public WebElement buttonmsChoice;

	@FindBy(id = "selectItemmultpileOwners")
	public WebElement selectItemmultpileOwners;

	@FindBy(css = "input.searchInputMultiple")
	public WebElement inputsearchInputMultiple;

	@FindBy(id = "category_new")
	public WebElement category_new;

	@FindBy(id = "Submit")
	public WebElement submit;

	@FindBy(id = "selectAll")
	public WebElement selectAll;

	@FindBy(xpath = ".//input[@value='No']")
	public WebElement radioNo ;

	@FindBy(xpath = ".//*[@id='category_new']")
	public WebElement categoryNewTxBx ;
	
	@FindBy(xpath=".//input[@name='checkFlagView']")
	public WebElement viewableByFranchisees;

	public AdminUsersSupplierDetailsAddSupplierPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
