package com.builds.uimaps.admin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminUsersSuppliersDetailsPage {
	@FindBy(xpath = ".//input[@value='Add New Supplier']")
	public WebElement addNewSupplier ;

	public AdminUsersSuppliersDetailsPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
