package com.builds.uimaps.admin;

public class CorporateUserAPI {
	private String userID;
	private String password;
	private String userLevel;
	private String userLanguage;
	private String expirationTime;
	private String role;
	private String jobTitle;
	private String firstname;
	private String lastname;
	private String isAuditor;
	private String address;
	private String city;
	private String state;
	private String zipCode;
	private String country;
	private String phone1;
	private String phone2;
	private String phoneExt2;	
	private String fax;
	private String mobile;
	private String email;
	private String sendNotification;
	private String loginUserIp;
    private String timezone;
	private String isDaylight;
	
	public String getTimezone() {
		return timezone;
	}

	public void setTimeZone(String timezone) {
		this.timezone = timezone;
	}
	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUserLevel() {
		return userLevel;
	}

	public void setUserLevel(String userLevel) {
		this.userLevel = userLevel;
	}

	public String getUserLanguage() {
		return userLanguage;
	}

	public void setUserLanguage(String userLanguage) {
		this.userLanguage = userLanguage;
	}

	public String getExpirationTime() {
		return expirationTime;
	}

	public void setExpirationTime(String expirationTime) {
		this.expirationTime = expirationTime;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getJobTitle() {
		return jobTitle;
	}

	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getIsAuditor() {
		return isAuditor;
	}

	public void setIsAuditor(String isAuditor) {
		this.isAuditor = isAuditor;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPhone1() {
		return phone1;
	}

	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}

	public String getPhone2() {
		return phone2;
	}

	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}

	public String getPhoneExt2() {
		return phoneExt2;
	}

	public void setPhoneExt2(String phoneExt2) {
		this.phoneExt2 = phoneExt2;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSendNotification() {
		return sendNotification;
	}

	public void setSendNotification(String sendNotification) {
		this.sendNotification = sendNotification;
	}

	public String getLoginUserIp() {
		return loginUserIp;
	}

	public void setLoginUserIp(String loginUserIp) {
		this.loginUserIp = loginUserIp;
	}

	public String getIsDaylight() {
		return isDaylight;
	}

	public void setIsDaylight(String isDaylight) {
		this.isDaylight = isDaylight;
	}


	
    
}
