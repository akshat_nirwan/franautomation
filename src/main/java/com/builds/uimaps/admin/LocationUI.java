package com.builds.uimaps.admin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LocationUI {

	WebDriver driver;
	
	@FindBy(id="franchiseeName")
	public WebElement FranchiseID;
	
	@FindBy(id="centerName")
	public WebElement CenterName;
		
	@FindBy(id="areaID")
	public WebElement AreaRegion;
	
	@FindBy(id="brands")
	public WebElement Brands;
	
	@FindBy(id="ms-parentsubdivision_1")
	public WebElement FranchiseeConsultant;
	
	@FindBy(id="licenseNo")
	public WebElement LicenseNo;
	
	@FindBy(id="storeTypeId")
	public WebElement StoreType;
	
	@FindBy(id="status")
	public WebElement CorporateLocation;	
	
	@FindBy(id="versionID")
	public WebElement AgreementVersion;
	
	@FindBy(id="reportPeriodStartDate")
	public WebElement RoyaltyReportingStartDate;
	
	@FindBy(id="taxRateId")
	public WebElement TaxRate;
	
	@FindBy(id="fbc")
	public WebElement FBC;
	
	@FindBy(xpath=".//*[@id='openingDate' or @id='grandStoreOpeningDate']")
	public WebElement OpeningDate;
	
	@FindBy(id="grandStoreOpeningDate")
	public WebElement ExpectedOpeningDate;
	
	@FindBy(id="address")
	public WebElement StreetAddress;
	
	@FindBy(id="address2")
	public WebElement Address2;
	
	@FindBy(id="city")
	public WebElement City;
	
	@FindBy(id="countryID")
	public WebElement Country;
	
	@FindBy(id="zipcode")
	public WebElement ZipCode;
	
	@FindBy(id="regionNo")
	public WebElement State;
	
	@FindBy(id="storePhone")
	public WebElement Phone;
	
	@FindBy(id="storePhoneExtn")
	public WebElement PhoneExtension;
	
	@FindBy(id="storeFax")
	public WebElement Fax;
	
	@FindBy(id="storeMobile")
	public WebElement Mobile;
	
	@FindBy(id="storeEmail")
	public WebElement Email;
	
	@FindBy(id="storeWebsite")
	public WebElement Website;
	
	@FindBy(id="contactTitle")
	public WebElement Title;
	
	@FindBy(id="contactFirstName")
	public WebElement FirstName;
	
	@FindBy(id="contactLastName")
	public WebElement LastName;
	
	@FindBy(id="phone1")
	public WebElement CenterPhone;
	
	@FindBy(id="phone1Extn")
	public WebElement CenterPhoneExtesion;
	
	@FindBy(id="emailID")
	public WebElement CenterEmail;
	
	@FindBy(id="fax")
	public WebElement CenterFax;
	
	@FindBy(id="mobile")
	public WebElement CenterMobile;
	
	@FindBy(xpath="//html//td[@id='ownerlabelID']/input[@value=0]")
	public WebElement OwnerTypeIndividual;
	
	@FindBy(xpath="//html//td[@id='ownerlabelID']/input[@value=1]")
	public WebElement OwnerTypeIndividualEntity;
	
	@FindBy(xpath="//html//td[@id='entitylabelID']/input[@value=0]")
	public WebElement NewEntity;
	
	@FindBy(xpath="//html//td[@id='entitylabelID']/input[@value=1]")
	public WebElement ExistingEntity;	
	
	@FindBy(id="fimEntityID")
	public WebElement ExistingEntityName;
	
	@FindBy(id="fimEntityDetail_0fimCbEntityType")
	public WebElement EntityType;
	
	@FindBy(id="fimEntityDetail_0fimTtEntityName")
	public WebElement EntityName;
	
	@FindBy(id="fimEntityDetail_0fimTtTaxpayer")
	public WebElement TaxpayerID;
	
	@FindBy(id="fimEntityDetail_0fimCbCountryOfFormation")
	public WebElement CountryofFormationResidency;
	
	@FindBy(id="fimEntityDetail_0fimCbStateOfFormation")
	public WebElement StateofFormationResidency;
	
	@FindBy(xpath=".//input[@id='ownerType' and @value=0]")
	public WebElement NewOwner;
	
	@FindBy(xpath=".//input[@id='ownerType' and @value=1]")
	public WebElement ExistingOwner;
	
	@FindBy(xpath="//html//td[@colspan='-1']/input[2]")
	public WebElement ExistingLeads;
	
	@FindBy(name="salutation")
	public WebElement Salutation;
	
	@FindBy(id="ownerFirstName")
	public WebElement OwnerFirstName;
	
	@FindBy(id="ownerLastName")
	public WebElement OwnerLastName;
	
	@FindBy(id="//img[@src='../static/1003/images/addmore.png']")
	public WebElement AddMore;
	
	@FindBy(id="ms-parentextOwnerId")
	public WebElement SelectOwners;
	
	@FindBy(id="chip-list")
	public WebElement LeadSearchBox;
	
	@FindBy(id="shiftdoc")
	public WebElement MoveDocuments;
		
	@FindBy(xpath="//input[contains(@id,'_muid')]")
	public WebElement MUID;
	
	@FindBy(id="Submit")
	public WebElement Submit;
	
	@FindBy(name="Reset")
	public WebElement Reset;
	
	@FindBy(id="Button1")
	public WebElement Cancel;
				
	public LocationUI(WebDriver driver)
	{
		PageFactory.initElements(driver, this);
	}
}
