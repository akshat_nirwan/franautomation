package com.builds.uimaps.admin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ManageCorporateUsersPage {

	@FindBy(xpath = ".//input[@value='Send Message' and @name='button']")
	public WebElement sendMessageBtn;

	public ManageCorporateUsersPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
