package com.builds.uimaps.admin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ManageCorporateUsersTestingUI {

	@FindBy(xpath=".//input[@value='Add Corporate User']")
	public WebElement addCorporateUserButton;
	
	
	public ManageCorporateUsersTestingUI(WebDriver driver)
	{
		PageFactory.initElements(driver, this);
	}
	
}
