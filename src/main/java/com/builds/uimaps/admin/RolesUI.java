package com.builds.uimaps.admin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class RolesUI {
	
	/*
	 * Akshatr
	 */
	
	@FindBy(xpath=".//input[@type='button' and @value='Add New Role']")
	public WebElement addNewRole_Btn;
	
	public RolesUI(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
}
