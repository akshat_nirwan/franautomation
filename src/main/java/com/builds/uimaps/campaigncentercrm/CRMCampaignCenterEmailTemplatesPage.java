package com.builds.uimaps.campaigncentercrm;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CRMCampaignCenterEmailTemplatesPage {

	@FindBy(xpath = ".//a[@original-title='Manage all your marketing campaigns']")
	public WebElement campaignCenterLinks ;

	@FindBy(id = "HeaderMenuContainerCampaign")
	public WebElement menuOptions;

	@FindBy(xpath = ".//div[@id='PrintHeaderMenuContainerCampaign']/div[@id='HeaderMenuContainerCampaign']//a[.='Templates']")
	public WebElement templatesLink ;

	@FindBy(xpath = ".//div[@data-role='ico_AddCircleOutline']")
	public WebElement createLink ;

	@FindBy(xpath = ".//a[.='Template']")
	public WebElement templateLink ;

	// code Your Link
	@FindBy(xpath = ".//a[.='Code your own']")
	public WebElement codeYourOwn ;

	@FindBy(id = "mailTitle")
	public WebElement templateName;

	@FindBy(id = "mailSubject")
	public WebElement mailSubject;

	@FindBy(xpath = ".//label[@for='PublicUsers']")
	public WebElement publicToAllUser ;

	@FindBy(xpath = ".//label[@for='CorporateUsers']")
	public WebElement publicToAllCorporateUser ;

	@FindBy(xpath = ".//label[@for='PrivateUsers']")
	public WebElement privateTemplate ;

	@FindBy(xpath = ".//label[@for='Graphical']")
	public WebElement graphicalEmailType ;

	@FindBy(xpath = ".//label[@for='PlaneText']")
	public WebElement plainTextEmailType ;

	@FindBy(xpath = ".//label[@for='UploadHTML']")
	public WebElement uploadHTMLEmailType ;

	@FindBy(id = "folderNo")
	public WebElement selectFolderName;

	@FindBy(id = "folderName")
	public WebElement folderName;

	@FindBy(id = "folderAccessibility")
	public WebElement folderAccessibility;

	@FindBy(id = "folderSummary")
	public WebElement folderSummary;

	@FindBy(id = "submitBtn")
	public WebElement saveBtn;

	@FindBy(id = "ta")
	public WebElement textAreaPlainText;

	@FindBy(xpath = ".//label[@for='MarkFavourite']")
	public WebElement markAsFavourite ;

	@FindBy(xpath = ".//div[@data-role='ico_AddCircleOutline']")
	public WebElement attachmentsCircleOutLine ;

	@FindBy(id = "attachmentName")
	public WebElement attachmentName;

	@FindBy(id = "attachmentButton")
	public WebElement attachmentButton;

	@FindBy(id = "HTML_FILE_ATTACHMENT")
	public WebElement htmlFileAttachmenet;

	@FindBy(id = "CodeYourOwnButton")
	public WebElement addBtn;

	@FindBy(xpath = ".//*[@id='tinymce']/p")
	public WebElement editorTextArea ;

	@FindBy(id = "PrintHeaderMenuContainerCampaign")
	public WebElement dashBoradLnk;

	@FindBy(xpath = ".//*[@id='PrintHeaderMenuContainerCampaign']//li[.='Templates']")
	public WebElement templateLnk ;

	@FindBy(xpath = ".//*[.='Corporate Templates']")
	public WebElement corporateTemplatesTab ;

	@FindBy(xpath = ".//label[@for='PublicUsers']")
	public WebElement publicToAllUsersTemplate ;

	@FindBy(xpath = ".//label[@for='CorporateUsers']")
	public WebElement publicToAllMyFranchiseLocation ;

	@FindBy(xpath = ".//form[@id='form1']//*[@id='folderNo']")
	public WebElement addFolder ;

	@FindBy(xpath = ".//div[@data-role='ico_AddCircleOutline']")
	public WebElement addAttchmentCircle ;

	@FindBy(id = "CodeYourOwnButton")
	public WebElement doneBtn;

	@FindBy(xpath = ".//*[.='Folders']")
	public WebElement foldersTab ;

	@FindBy(xpath = ".//*[@id='FilterTabShowHide']//*[.='Archived']")
	public WebElement archivedTab ;

	@FindBy(xpath = ".//input[@name='email']")
	public WebElement testEmailField ;

	@FindBy(id = "testButton")
	public WebElement testButton;

	@FindBy(xpath = ".//button[contains(text () ,'Close')]")
	public WebElement closeBtn ;

	@FindBy(xpath = ".//*[@id='FilterTabShowHide']//*[.='Location Templates']")
	public WebElement locationTemplateTab ;

	@FindBy(xpath = ".//div[@class='switch']/label[@for='recomended']")
	public WebElement recommendedTemplate ;

	@FindBy(xpath = ".//*[@id='FilterTabShowHide']//*[.='Recommended']")
	public WebElement recommendedTab ;

	@FindBy(xpath = ".//li/a[.='Folder']")
	public WebElement folderLnk ;

	@FindBy(id = "submitBtn")
	public WebElement saveFolder;

	@FindBy(id = "submitAndAddTemplate")
	public WebElement saveAndAddTemplate;

	@FindBy(id = "folderDescriptionLink")
	public WebElement folderDescriptionLink;

	public CRMCampaignCenterEmailTemplatesPage(WebDriver driver) {

		PageFactory.initElements(driver, this);
	}
}
