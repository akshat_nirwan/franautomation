package com.builds.uimaps.campaigncentercrm;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CRMCampaignCenterPage {

	@FindBy(xpath = ".//li/a[@original-title='Manage all your marketing campaigns' or contains(text(),'Campaign Center' and @qat_submodule='Campaign Center')]")
	public WebElement campaignCenterLinks ;

	@FindBy(id = "HeaderMenuContainerCampaign")
	public WebElement menuOptions;

	@FindBy(xpath = ".//*[@id='HeaderMenuContainerCampaign']//a[.='Templates']")
	public WebElement templatesLink ;

	@FindBy(xpath = ".//div[@data-role='ico_AddCircleOutline']")
	public WebElement createLink ;

	@FindBy(xpath = ".//a[.='Template']")
	public WebElement templateLink ;

	// code Your Link
	@FindBy(xpath = ".//a[.='Code your own']")
	public WebElement codeYourOwn ;

	@FindBy(id = "mailTitle")
	public WebElement templateName;

	@FindBy(id = "mailSubject")
	public WebElement mailSubject;

	@FindBy(xpath = ".//label[@for='PublicUsers']")
	public WebElement publicToAllUser ;

	@FindBy(xpath = ".//label[@for='CorporateUsers']")
	public WebElement publicToAllCorporateUser ;

	@FindBy(xpath = ".//label[@for='PrivateUsers']")
	public WebElement privateTemplate ;

	@FindBy(xpath = ".//label[@for='Graphical']")
	public WebElement graphicalEmailType ;

	@FindBy(xpath = ".//*[@id='tinymce']/p")
	public WebElement editorTextArea ;

	@FindBy(xpath = ".//label[@for='PlaneText']")
	public WebElement plainTextEmailType ;

	@FindBy(xpath = ".//label[@for='UploadHTML']")
	public WebElement uploadHTMLEmailType ;

	@FindBy(id = "HTML_FILE_ATTACHMENT")
	public WebElement uploadFileButton;

	@FindBy(id = "folderNo")
	public WebElement selectFolderName;

	@FindBy(id = "folderName")
	public WebElement folderName;

	@FindBy(id = "folderAccessibility")
	public WebElement folderAccessibility;

	@FindBy(id = "folderSummary")
	public WebElement folderSummary;

	@FindBy(id = "submitBtn")
	public WebElement saveBtn;

	@FindBy(id = "ta")
	public WebElement textAreaPlainText;

	@FindBy(xpath = ".//label[@for='MarkFavourite']")
	public WebElement markAsFavourite ;

	@FindBy(xpath = ".//div[@data-role='ico_AddCircleOutline']")
	public WebElement attachmentsCircleOutLine ;

	@FindBy(id = "attachmentName")
	public WebElement attachmentName;

	@FindBy(id = "attachmentButton")
	public WebElement attachmentButton;

	@FindBy(id = "HTML_FILE_ATTACHMENT")
	public WebElement htmlFileAttachmenet;

	@FindBy(id = "CodeYourOwnButton")
	public WebElement doneBtn;

	// Campaigns
	@FindBy(xpath = ".//div[@id='PrintHeaderMenuContainerCampaign']/div[@id='HeaderMenuContainerCampaign']//a[.='Campaigns']")
	public WebElement campaignLinks ;

	@FindBy(xpath = ".//button[.='Create Campaign']")
	public WebElement createCampaignButton ;

	@FindBy(xpath = ".//input[@id='campaignTitle' and @name = 'campaignTitle']")
	public WebElement campaignTitle;

	@FindBy(xpath = ".//label[@for='recordType_email']")
	public WebElement emailCampaign ;

	@FindBy(id = "campaignAccessibility")
	public WebElement campaignAccessibility;

	@FindBy(xpath = ".//label[@for='quickCampaign1']")
	public WebElement promotionalTypeCampaign ;

	@FindBy(xpath = ".//label[@for='quickCampaign2']")
	public WebElement statusDrivenTypeCampaign ;

	@FindBy(xpath = ".//button[.='Start']")
	public WebElement startBtn ;

	@FindBy(xpath = ".//label[@for='PlaneText']")
	public WebElement planeText ;

	@FindBy(id = "ta")
	public WebElement textArea;

	@FindBy(id = "campaignStartLag")
	public WebElement dayGap;

	@FindBy(xpath = ".//*[@id='FiltersType']/label[@for='firstMailDaysOptionsNo']")
	public WebElement sendTheFirstEmailSwitch ;

	@FindBy(id = "CodeYourOwnButton")
	public WebElement saveAndContinue;

	@FindBy(id = "associateLater")
	public WebElement associateLater;

	@FindBy(xpath = ".//button[.='Finalize And Launch Campaign']")
	public WebElement finalizeAndLaunchCampaignBtn ;

	// WorkFlowCreate
	@FindBy(xpath = ".//div[@id='PrintHeaderMenuContainerCampaign']/div[@id='HeaderMenuContainerCampaign']//a[.='Workflows']")
	public WebElement workFlowsLinks ;

	@FindBy(xpath = ".//button[.='Create Workflow']")
	public WebElement createWorkFlows ;

	@FindBy(id = "workFlowName")
	public WebElement workFlowName;

	@FindBy(xpath = ".//label[@for='recordType_Lead']")
	public WebElement recordTypeLead ;

	@FindBy(xpath = ".//label[@for='TextCampaignsRadioOption01']")
	public WebElement standardWorkFlow ;

	@FindBy(xpath = ".//button[contains(text () , 'Next')]")
	public WebElement nextBtn ;

	//
	@FindBy(id = "CreateCampaignLink")
	public WebElement createCampaignLink;

	@FindBy(id = "serviceID")
	public WebElement selectCategory;

	@FindBy(xpath = ".//div[@id='summaryDiv']//*[contains(text () ,'View All')]")
	public WebElement viewAllButton ;

	// apply Filter
	@FindBy(xpath = ".//div[contains(@data-target , 'FilterContainer')]")
	public WebElement showFilter ;

	@FindBy(id = "searchCampaign")
	public WebElement searchCampaign;

	@FindBy(xpath = ".//*[@id='FilterContainer']//button[contains(text () ,'Apply Filters')]")
	public WebElement applyFilterAtGroup ;

	@FindBy(xpath = ".//*[@id='search']")
	public WebElement searchFilterBtn ;

	@FindBy(xpath = ".//div[contains(@data-target , 'FilterContainer')]")
	public WebElement hideFilter ;

	@FindBy(xpath = ".//a[.='Promotional']")
	public WebElement promotionalCampaignTab ;

	@FindBy(xpath = ".//a[.='Status-Driven']")
	public WebElement statusDrivenCampaignTab ;

	@FindBy(id = "campaignDescriptionLink")
	public WebElement campaignDescriptionLnk;

	@FindBy(id = "campaignDescription")
	public WebElement campaignDescription;

	@FindBy(xpath = ".//*[@id='campaignDescriptionLink' and . ='Hide Description']")
	public WebElement hideDescriptionLink ;

	@FindBy(xpath = ".//li[.='Template']")
	public WebElement createTemplateLink ;

	@FindBy(xpath = ".//label[@for='PublicUsers']")
	public WebElement publicToAllUsersTemplate ;

	@FindBy(xpath = ".//form[@id='form1']//*[@id='folderNo']")
	public WebElement addFolder ;

	@FindBy(xpath = ".//div[@data-role='ico_AddCircleOutline']")
	public WebElement addAttchmentCircle ;

	@FindBy(xpath = ".//button[contains(text () ,'Manage Template')]")
	public WebElement manageTemplate ;

	@FindBy(id = "searchTemplate")
	public WebElement searchTemplate;

	@FindBy(xpath = ".//*[.='Folders']")
	public WebElement foldersTab ;

	@FindBy(id = "searchFolderName")
	public WebElement searchFolder;

	@FindBy(xpath = ".//li[.='Recipients Group']")
	public WebElement recipientsGroupLnk ;

	@FindBy(id = "groupName")
	public WebElement groupName;

	@FindBy(name = "groupDescription")
	public WebElement groupDescription;

	@FindBy(id = "groupDescriptionLink")
	public WebElement groupDescriptionLink;

	@FindBy(xpath = ".//label[.='Lead']")
	public WebElement leadTypeTab ;

	@FindBy(xpath = ".//label[.='Public to all Users']")
	public WebElement accessibilityPAllUser ;

	@FindBy(xpath = ".//*[@id='saveGroup' or .='Save & Continue']")
	public WebElement saveAndContinueBtn ;

	@FindBy(xpath = ".//*[@id='fc-drop-parentselColList']/button")
	public WebElement fieldSelectBtn ;

	@FindBy(xpath = ".//*[@id='fc-drop-parentselColList']/div/div/input")
	public WebElement searchField ;

	@FindBy(xpath = ".//*[@id='CM_CONTACT_DETAILS_contactType_MATCH']")
	public WebElement assignToCriteria ;

	@FindBy(xpath = ".//*[@id='fc-drop-parentCM_CONTACT_DETAILS_contactType']/button")
	public WebElement selectAllCriteria ;

	@FindBy(xpath = ".//*[@id='fc-drop-parentCM_CONTACT_DETAILS_contactType']/div/div/input")
	public WebElement searchAreaCriteria ;

	@FindBy(xpath = ".//*[@id='fc-drop-parentCM_CONTACT_DETAILS_contactType']//span[.='Select All']")
	public WebElement selectAllContactType ;

	@FindBy(id = "associatewithGroup")
	public WebElement associateWithGroupBtn;

	@FindBy(xpath = ".//button[.='OK']")
	public WebElement okBtn ;

	@FindBy(xpath = ".//*[@id='CM_LEAD_DETAILS_ownerType_MATCH']")
	public WebElement assignToCriteria1 ;

	@FindBy(xpath = ".//*[@id='fc-drop-parentCM_LEAD_DETAILS_ownerType']/button")
	public WebElement selectAssignToUserBtn ;

	@FindBy(xpath = ".//*[@id='fc-drop-parentCM_LEAD_DETAILS_ownerType']//span[.='Select All']")
	public WebElement selectAssignToSelectAll ;

	@FindBy(xpath = ".//button[contains(text () , 'Save & Continue')]")
	public WebElement continueBtn ;

	@FindBy(id = "searchSystem")
	public WebElement searchAtGroup;

	@FindBy(xpath = ".//label[.='Exact Search']")
	public WebElement exactSearchGroup ;

	@FindBy(xpath = ".//*[@id='systemSearchBtn']")
	public WebElement searchAccountBtn ;

	@FindBy(xpath = ".//iframe[@class='newLayoutcboxIframe']")
	public WebElement frame ;

	@FindBy(id = "groupFor")
	public WebElement selectGroupType;

	@FindBy(id = "Accessibility")
	public WebElement selectPublicToAllUsers;

	@FindBy(id = "folderDescriptionLink")
	public WebElement folderDescriptionLink;

	@FindBy(xpath = ".//div[contains(text () , 'Create Group')]")
	public WebElement createGroupLnk ;

	@FindBy(id = "campStatus")
	public WebElement campStatus;
	
	@FindBy(xpath = "//span[contains(text(),'Corporate Campaigns')]")
	public WebElement corporatCampaign;
	
	@FindBy(xpath = "//*[contains(text(),'Location Templates')]")
	public WebElement locationtemplate;
	
	@FindBy(xpath = "//*[contains(text(),'Location Campaigns')]")
	public WebElement locationCampaign;

	public CRMCampaignCenterPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
