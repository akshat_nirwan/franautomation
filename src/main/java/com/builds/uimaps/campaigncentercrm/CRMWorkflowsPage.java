package com.builds.uimaps.campaigncentercrm;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CRMWorkflowsPage {

	@FindBy(xpath = ".//a[@original-title='Manage all your workflows']")
	public WebElement workflowsLink ;

	@FindBy(xpath = ".//*[@id='open-overlay' and .='Create Workflow']")
	public WebElement createWorkFLowLink ;

	@FindBy(id = "workFlowName")
	public WebElement workFlowName;

	@FindBy(xpath = ".//label[@for='recordType_Lead' and .='Lead']")
	public WebElement recordTypeLead ;

	@FindBy(xpath = ".//label[@for='recordType_Contact' and .='Contact']")
	public WebElement recordTypeContact ;

	@FindBy(xpath = ".//label[@for='TextCampaignsRadioOption01']")
	public WebElement workflowTypeStandard ;

	@FindBy(xpath = ".//label[@for='TextCampaignsRadioOption02']")
	public WebElement workflowTypeDateBased ;

	@FindBy(xpath = ".//label[@for='TextCampaignsRadioOption03']")
	public WebElement workflowTypeEventBased ;

	@FindBy(xpath = ".//button[contains(text () ,'Next')]")
	public WebElement nextButton ;

	// When Lead Is Added :: Trigger
	@FindBy(xpath = ".//label[@for='RadioOption01']")
	public WebElement leadIsAdded ;

	@FindBy(xpath = ".//label[@for='RadioOption02']")
	public WebElement leadIsModified ;

	@FindBy(xpath = ".//label[@for='RadioOption03']")
	public WebElement leadIsAddedOrModified ;

	@FindBy(id = "when-next")
	public WebElement nextButtonTrigger;

	@FindBy(xpath = ".//label[@id='criteria-radio-1']")
	public WebElement allLeads ;

	@FindBy(xpath = ".//label[@id='criteria-radio-2']")
	public WebElement leadsMatchingCondition ;

	@FindBy(xpath = ".//*[@id='fc-drop-parentselectedColList']")
	public WebElement availabelFieldsSelect ;

	@FindBy(id = "if-next")
	public WebElement nextBtnCondition;

	@FindBy(xpath = ".//div[contains(text () ,'Action')]")
	public WebElement actionBtn ;

	@FindBy(id = "campaign")
	public WebElement sendCampaign;

	@FindBy(xpath = ".//*[@id='fc-drop-parenttempFranchiseeNo']")
	public WebElement selectLocationAtCampaign ;

	@FindBy(xpath = ".//*[@id='SendAllRecipients']/label")
	public WebElement masterCampaign ;

	@FindBy(id = "tempCampaignId")
	public WebElement selectCampaign;

	@FindBy(id = "addWorkFlowAction")
	public WebElement saveBtn;

	@FindBy(id = "changeStatus")
	public WebElement changesStatus;

	@FindBy(id = "statusIdCombo")
	public WebElement selectStatus;

	@FindBy(id = "sendMail")
	public WebElement sendEmail;

	@FindBy(xpath = ".//input[@id='ID']/following-sibling::label[.='Lead Owner ID']")
	public WebElement leadOwnerIDFromReply ;

	@FindBy(id = "templateID")
	public WebElement selectTemplate;

	@FindBy(id = "associateTemplate")
	public WebElement associateTemplateWithEmailButton;

	@FindBy(id = "task")
	public WebElement createTask;

	@FindBy(id = "subject")
	public WebElement subject;

	@FindBy(id = "priority")
	public WebElement priority;

	@FindBy(id = "status")
	public WebElement taskStatus;

	@FindBy(xpath = ".//label[@for='device']")
	public WebElement assignToLeadOwnerSwitch ;

	@FindBy(xpath = ".//*[@id='fc-drop-parentassignTo']")
	public WebElement customOwner ;

	@FindBy(xpath = ".//label[@for='timelessTaskID']")
	public WebElement timeLessTaskSwitch ;

	@FindBy(xpath = ".//label[@for='startTaskID']")
	public WebElement startTaskImmediatelySwitch ;

	@FindBy(xpath = ".//button[.='Create']")
	public WebElement createTaskButton ;

	@FindBy(id = "createWorkFlow")
	public WebElement createWorkFlowButton;

	@FindBy(id = "CM_LEAD_DETAILS_emailIds_MATCH")
	public WebElement selectLeadInfoEmailId;

	@FindBy(xpath = ".//*[@id='fc-drop-parentCM_LEAD_DETAILS_cmLeadStatusID']")
	public WebElement selectLeadStatus ;

	@FindBy(xpath = ".//*[@id='fc-drop-parentCM_LEAD_DETAILS_unsubscribe']")
	public WebElement selectEmailSubscribe ;

	@FindBy(xpath = ".//*[@id='fc-drop-parentCM_LEAD_DETAILS_country']")
	public WebElement countrySelect ;

	@FindBy(id = "CM_LEAD_DETAILS_country_MATCH")
	public WebElement countryMatch;

	@FindBy(id = "CM_LEAD_DETAILS_cmLeadStatusID_MATCH")
	public WebElement selectLeadStatusMatch;

	@FindBy(id = "CM_LEAD_DETAILS_unsubscribe_MATCH")
	public WebElement selectEmailSubScriptionMatch;

	@FindBy(id = "CM_LEAD_DETAILS_birthdate_MATCH")
	public WebElement selectBirthDateMatch;

	@FindBy(id = "CM_LEAD_DETAILS_birthdate_fromDate")
	public WebElement birthdayFromDate;

	@FindBy(id = "CM_LEAD_DETAILS_birthdate_toDate")
	public WebElement birthdayToDate;

	@FindBy(id = "CM_CONTACT_DETAILS_birthdate_MATCH")
	public WebElement selectBirthDateContact;

	@FindBy(id = "CM_CONTACT_DETAILS_birthdate_fromDate")
	public WebElement selectFromDateContact;

	@FindBy(id = "CM_CONTACT_DETAILS_birthdate_toDate")
	public WebElement selectToDateContact;

	@FindBy(id = "CM_CONTACT_DETAILS_emailIds_MATCH")
	public WebElement selectEmailIdMatchContact;

	@FindBy(id = "CM_CONTACT_DETAILS_country_MATCH")
	public WebElement selectCountryMatchCaontact;

	@FindBy(xpath = ".//*[@id='fc-drop-parentCM_CONTACT_DETAILS_country']")
	public WebElement selectCountryContact ;

	@FindBy(id = "CM_CONTACT_DETAILS_cmLeadStatusID_MATCH")
	public WebElement selectContactStatusMatch;

	@FindBy(xpath = ".//*[@id='fc-drop-parentCM_CONTACT_DETAILS_cmLeadStatusID']")
	public WebElement selectContactStatus ;

	@FindBy(id = "CM_CONTACT_DETAILS_unsubscribe_MATCH")
	public WebElement selectEmailSubscribeContact;

	@FindBy(xpath = ".//*[@id='fc-drop-parentCM_CONTACT_DETAILS_unsubscribe']")
	public WebElement selectEmailSubscribeStatusContact ;

	@FindBy(id = "CM_CONTACT_DETAILS_contactType_MATCH")
	public WebElement selectContactTypeMatch;

	@FindBy(xpath = ".//*[@id='fc-drop-parentCM_CONTACT_DETAILS_contactType']")
	public WebElement selectContactType ;

	@FindBy(id = "typeId")
	public WebElement selectADateField;

	@FindBy(id = "dayFilter")
	public WebElement dateOfExecutionSelect;

	@FindBy(id = "daysDifference")
	public WebElement daysDifference;

	// event Based WorkFlow
	@FindBy(xpath = ".//*[@id='fc-drop-parentexecutePoint']")
	public WebElement selectTriggerEventBased ;

	@FindBy(xpath = ".//*[@id='fc-drop-parentwebForms']")
	public WebElement selectWebFormName ;

	@FindBy(id = "CM_LEAD_DETAILS_leadFirstName_MATCH")
	public WebElement firstNameSelectMatch;

	@FindBy(id = "CM_LEAD_DETAILS_leadFirstName")
	public WebElement firstNameField;

	@FindBy(id = "CM_CONTACT_DETAILS_contactFirstName_MATCH")
	public WebElement firstNameSelectContactMatch;

	@FindBy(id = "CM_CONTACT_DETAILS_contactFirstName")
	public WebElement contactFirstNameField;

	@FindBy(xpath = ".//*[@id='fc-drop-parenteventStatusCombo']")
	public WebElement selectStatusEventBased ;

	@FindBy(xpath = ".//div[contains(@data-target , 'FilterContainer')]")
	public WebElement showFilter ;

	@FindBy(id = "searchWorkFlow")
	public WebElement searchWorkFlow;

	@FindBy(id = "search")
	public WebElement applyFilter;

	@FindBy(xpath = ".//div[contains(@data-target , 'FilterContainer')]")
	public WebElement hideFilter ;

	public CRMWorkflowsPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
