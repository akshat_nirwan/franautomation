package com.builds.uimaps.common;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.builds.utilities.FranconnectUtil;

public class AddAreaRegionUI {
	
	@FindBy(id="category")
	public WebElement category_dropDown;
	
	@FindBy(xpath=".//input[@type='text' and @name='regioname']")
	public WebElement areaRegionName_textBox;
	
	@FindBy(id="groupBy")
	public WebElement groupBy_dropDown;
	
	@FindBy(name = "country")
	public WebElement countryUSA;
	
	@FindBy(name = "groupBy")
	public WebElement groupBy;
	
	// Akshat Checkbox
	public WebElement select_Country_GroupByCounty(WebDriver driver , String country) {
		List<WebElement> listElement = driver.findElements(By.xpath(".//input[@name='country']/parent::*"));
		WebElement country_checkbox = null;

		for (WebElement webElement : listElement) {

			if (webElement.getText().trim().equalsIgnoreCase(country)) {
				country_checkbox = webElement.findElement(By.xpath("./input[@name='country']"));
			}
		}

		return country_checkbox;
	}
	
	public WebElement states(WebDriver driver , String state) throws Exception
	{ 
	FranconnectUtil fc = new FranconnectUtil();
	return fc.utobj().getElementByXpath(driver, ".//td[contains(text(), '"+state+"')]/preceding-sibling::td[1]");
//	return fc.utobj().getElementByXpath(driver, ".//td[contains(text(), '"+country+"')]");
//	return fc.utobj().getElementByXpath(driver, ".//td[contains(text(), '"+country+"')]/ancestor::tr[1]/td[1])");
	}
	
	public WebElement countries(WebDriver driver , String country) throws Exception
	{ 
	FranconnectUtil fc = new FranconnectUtil();
	return fc.utobj().getElementByXpath(driver, ".//td[contains(text(), '"+country+"')]/input");
	}
	
	// CHECKBOX : Akshat
	public WebElement selectCounty(WebDriver driver, String county) {
		List<WebElement> listElement = driver.findElements(By.xpath(".//input[@name='countyId']/parent::*"));
		WebElement country_checkbox = null;

		for (WebElement webElement : listElement) {

			if (webElement.getText().trim().equalsIgnoreCase(county)) {
				country_checkbox = webElement.findElement(By.xpath("./input[@name='countyId']"));
			}
		}

		return country_checkbox;
	}
	
	// CHECKBOX : Akshat
	public WebElement selectState(WebDriver driver , String country) {
		List<WebElement> listElement = driver.findElements(By.xpath(".//input[@name='stateId']/parent::*"));
		WebElement country_checkbox = null;

		for (WebElement webElement : listElement) {

			if (webElement.getText().trim().equalsIgnoreCase(country)) {
				country_checkbox = webElement.findElement(By.xpath("./input[@name='stateId']"));
			}
		}

		return country_checkbox;
	}
	
	@FindBy(xpath=".//input[@type='button' and @value=' Next ']")
	public WebElement NextBtn2;
	
	@FindBy(xpath=".//input[@type='button' and @value=' Next ']")
	public WebElement NextBtn;
	
	
	@FindBy(xpath=".//input[@type='button' and @value='Submit']")
	public WebElement submit_btn;
	
	@FindBy(xpath=".//input[@type='button' and @value='Cancel']")
	public WebElement cancel_btn;
	
	@FindBy(xpath=".//input[@type='radio' and @name='zipmode' and @value='1']")
	public WebElement zipCodes_EnterCommaSeparatedListZipCodesManually_radioBtn;   

	@FindBy(xpath=".//input[@type='radio' and @name='zipmode' and @value='2']")
	public WebElement zipCodes_SpecifyFileToUpload_radioBtn;   
	
	@FindBy(xpath=".//textarea[@class='fTextBox' and @name='ziplist']")
	public WebElement zipCodes_EnterCommaSeparatedListZipCodesManually_textBox;
	
	@FindBy(xpath=".//input[@type='file' and @name='zipfile']")
	public WebElement zipCodes_SpecifyFileToUpload_chooseFileBtn;
	
	public AddAreaRegionUI(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
