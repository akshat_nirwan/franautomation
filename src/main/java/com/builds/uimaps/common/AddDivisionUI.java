package com.builds.uimaps.common;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AddDivisionUI {
	
	@FindBy(xpath=".//input[@type='text' and @name='divisionName']")
	public WebElement divisionName_textBox;
	
	@FindBy(xpath=".//input[@type='button' and @value='Submit']")
	public WebElement submitBtn;
	
	@FindBy(xpath=".//input[@type='button' and @value='Cancel']")
	public WebElement cancelBtn;
	
	public AddDivisionUI(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
