package com.builds.uimaps.common;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ConfigureNewHierarchyLevelUI {
	
	@FindBy(xpath=".//input[@type='radio' and @value='Y']")
	public WebElement enableNewHierarchyLevel_Yes;
	
	@FindBy(xpath=".//input[@type='radio' and @value='N']")
	public WebElement enableNewHierarchyLevel_No;
	
	@FindBy(xpath=".//input[@type='radio' and @name='allowMultipleDivisionFsDisplay' and @value='Y']")
	public WebElement leadsExclusiveToNewHierarchyLevel_No;
	
	@FindBy(xpath=".//input[@type='radio' and @name='allowMultipleDivisionFsDisplay' and @value='N']")
	public WebElement leadsExclusiveToNewHierarchyLevel_Yes;
	
	@FindBy(xpath=".//input[@type='button' and @value='Save']")
	public WebElement SaveBtn;
	
	@FindBy(xpath=".//input[@type='button' and @value='Cancel']")
	public WebElement CancelBtn;
	
	public ConfigureNewHierarchyLevelUI(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
