package com.builds.uimaps.common;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FCHomePage {

	@FindBy(xpath = ".//li[@id='module_fs']/a")
	public WebElement fsModule;

	@FindBy(xpath = ".//*[@id='module_fim']")
	public WebElement fimModule ;

	@FindBy(xpath = ".//*[@id='module_storeopener']")
	public WebElement openerModule ;

	@FindBy(xpath = ".//div[@id='top_menu']/ul/li/a[@original-title='Communicate and Share Information with Other Users']")
	public WebElement intranetModule ;

	@FindBy(xpath = ".//*[@id='module_audit']/a")
	public WebElement fieldopsModule ;

	@FindBy(xpath = ".//div[@id='dropdown']/span/a") 
	public WebElement userOptions;
	
	@FindBy(xpath=".//a[@href='logout']")
	public WebElement btLoggedOut;

	@FindBy(xpath = ".//*[@id='dropdown']//a[contains(@href, 'fromSearch')]")
	public WebElement search ;

	@FindBy(xpath = ".//*[@id='dropdown']//a[contains(@href, 'options')]")
	public WebElement options ;

	@FindBy(xpath = ".//a[@qat_tabname='Users']")
	public WebElement searchUserTab ;

	@FindBy(id = "userID")
	public WebElement userId;

	@FindBy(id = "Submit")
	public WebElement submitBtn;

	//@FindBy(xpath = ".//a[contains(@href,'nextUrl=administration&menuName=admin')]")
	@FindBy(xpath = ".//a[contains(@href,'nextUrl=administration&menuName=admin') or .='Admin' or .='Unit Admin']")
	public WebElement adminLink ;

	@FindBy(xpath = ".//*[@id='dropdown']/div/div/span[4]/a")
	public WebElement logoutLink ;

	@FindBy(xpath = ".//a[@href='logout']")
	public WebElement logout;

	@FindBy(xpath = ".//*[@id='module_training']")
	public WebElement trainingModule ;

	@FindBy(linkText = "SmartConnect")
	public WebElement smartConnectModule ;

	@FindBy(xpath = ".//*[@id='module_intranet']/a")
	public WebElement theHubModule ;

	@FindBy(linkText = "Tasks")
	public WebElement theHubTask ;

	@FindBy(xpath = ".//a[@original-title='Get Updates Since Your Last Login']")
	public WebElement theHubWhatsNew ;

	@FindBy(xpath = ".//a[@original-title='Take a Quick Overview of Latest Updates']")
	public WebElement theHubHome ;

	@FindBy(xpath = ".//a[@original-title='Manage New Locations']")
	public WebElement storeSummary ;

	@FindBy(xpath = ".//a[@original-title='Search Checklist(s) Subject to Defined Parameters']")
	public WebElement openerSearch ;

	@FindBy(xpath = ".//a[@original-title='Manage Tasks']")
	public WebElement openerTasks ;

	@FindBy(xpath = ".//a[@original-title='Generate Analytical Reports about Onboarding/ Store Development']")
	public WebElement openerReports ;

	@FindBy(xpath = ".//a[@original-title='Manage Archived Stores']")
	public WebElement openerArchived ;

	@FindBy(xpath = ".//*[@id='test1']/span/a")
	public WebElement moreLink ;

	@FindBy(xpath = ".//*[@id='module_support']/a[@original-title='Ensure Timely Resolution of Franchisee Issues']")
	public WebElement supportModule ;

	@FindBy(xpath = ".//*[@id='module_support']//a[@original-title='Take Department-wise Rundown of Support Tickets']")
	public WebElement supportDashBoard ;

	@FindBy(xpath = ".//*[@id='module_support']/a")
	public WebElement supportModuleMoreLink ;

	@FindBy(xpath = ".//*[@qat_submodule='Tickets']")
	public WebElement supportTickets ;

	@FindBy(xpath = ".//*[@id='module_support']//a[@original-title='Browse Through FAQs to Resolve Your Concerns']")
	public WebElement faqs ;

	@FindBy(xpath = ".//*[@id='module_support']//a[@original-title='View Contact Details of Support Departments']")
	public WebElement contactInfo ;

	@FindBy(xpath = ".//*[@id='module_support']//a[@original-title='Resolve Your Queries Through Support Tickets']")
	public WebElement askCorporate ;

	
	@FindBy(xpath = ".//*[@qat_submodule='DashBoard' and contains(@href,'homeFim')]")
	public WebElement Dashboard;
	
	@FindBy(xpath = ".//*[@qat_submodule='Franchisees']")
	public WebElement franchisees ;

	@FindBy(xpath = ".//*[@qat_submodule='Terminated']")
	public WebElement terminated ;

	@FindBy(xpath = ".//*[@qat_submodule='Corporate Locations']")
	public WebElement corporateLocations ;

	@FindBy(xpath = ".//a[@qat_submodule='Export' and contains(@href,'subMenuName=fimexport')]")
	public WebElement exportpage ;

	//// CRM Module
	@FindBy(xpath = ".//a[@original-title='Take a quick overview']")
	public WebElement homeLnk ;

	@FindBy(xpath = ".//*[@id='test2']/span/a/span")
	public WebElement productLink ;

	@FindBy(xpath = ".//*[@id='moreMenu-options2']//a[.='CRM']")
	public WebElement crmLink ;

	@FindBy(xpath = ".//*[@id='moreMenu-options2']//a[.='Home']")
	public WebElement HomeLink ;

	@FindBy(xpath = ".//a[@original-title='View and manage details of existing leads'  or  contains(text(),'Leads')]")
	public WebElement leadsLink ;

	@FindBy(xpath = ".//a[@original-title='Manage Account details associated with contacts and leads' or contains(text(),'Accounts')]")
	public WebElement accountsLink ;

	@FindBy(xpath = ".//a[@original-title='View and manage tasks associated with leads, contacts and opportunities']")
	public WebElement tasksLink ;

	@FindBy(xpath = ".//a[@original-title='View and manage details of existing contacts' or @original-title='View and manage details of existing Contacts']")
	public WebElement contactsLink ;

	@FindBy(xpath = ".//a[@original-title='View and manage details of opportunities']")
	public WebElement opportunitiesLink ;

	@FindBy(xpath = ".//a[@original-title='Categorize contacts and leads in groups and manage them']")
	public WebElement groupsLink ;

	@FindBy(xpath = ".//a[@original-title='Manage all your marketing campaigns' and contains(text(),'Campaign Center')]")
	public WebElement campaignCenterLink ;

	@FindBy(xpath = ".//a[@original-title='View and manage events, meetings and appointments' and contains(text(),'Calendar')]")
	public WebElement calendarLink ;

	@FindBy(xpath = ".//a[@original-title='Manage all your workflows']")
	public WebElement workflowsLink ;

	@FindBy(xpath = ".//a[@original-title='Export contact and lead data to different file format']")
	public WebElement exportLink ;
	
	// Info Mgr

	@FindBy(xpath = ".//a[@original-title='FDD']")
	public WebElement fDD ;

	@FindBy(xpath = ".//a[@qat_submodule='Reports' and contains(@href,'fimReportHome')]")
	public WebElement infoMgrReports ;
		
	@FindBy(xpath = ".//a[@original-title='Generate Analytical Reports']")
	public WebElement reports ;

	@FindBy(xpath = ".//*[@qat_submodule='Multi-Unit / Entity']")
	public WebElement multiUnitEntity ;

	@FindBy(xpath = ".//*[@qat_submodule='Regional']")
	public WebElement regional ;

	/*
	 * @FindBy(xpath=".//a[@original-title='Manage In Development Locations']")
	 * public WebElement inDevelopment=
	 * DDDDDDDDDDDDDDDDDDDDD
	 */

	@FindBy(xpath = ".//*[@qat_submodule='In Development']")
	public WebElement inDevelopment ;

	@FindBy(xpath = ".//a[@original-title='Search Locations Subject to Defined Parameters']")
	public WebElement infoMgrSearch ;

	// Harish Dwivedi Bug Fix . appeneded in Group LInk
	/*
	 * @FindBy(xpath=
	 * ".//a[@original-title='Group Your Franchisees to Manage Them.' or @original-title='Group Your Franchisees to Manage Them']"
	 * ) public WebElement infoMgrGroup=
	 * DDDDDDDDDDDDDDDDDDDDD
	 * ;
	 */

	@FindBy(xpath = ".//a[contains(@href ,'groupSummaryFim')]")
	public WebElement infoMgrGroup ;

	@FindBy(xpath = ".//*[@qat_submodule='Tasks' and contains(@href,'moduleID=3')]")
	public WebElement infoMgrTask ;

	@FindBy(xpath = ".//a[@original-title='Manage all your campaigns']")
	public WebElement infoMgrCampaignCenter ;

	// marketing page
	@FindBy(xpath = ".//*[@class='sec2 active']/span[@class='close']")
	public WebElement marketingLink ;

	// Finance Page
	@FindBy(xpath = ".//*[@id='module_financials']/a[@original-title='Manage Finance Performance Details']")
	public WebElement financeModule ;

	// @FindBy(xpath=".//*[@id='module_financials']/a[@qat_module='financials']")
	@FindBy(xpath = ".//*[@id='module_financials']/a[contains(text(),'Finance')]")
	public WebElement moreFinance ;

	@FindBy(xpath = ".//*[@qat_submodule='Import']")
	public WebElement importpage ;
	
	@FindBy(xpath=".//a[@class='marketinglabel']/span")
	public WebElement marketinglabel;
	
	@FindBy(xpath=".//a[contains(@href,'nextUrl=cmHome')]")
	public WebElement crmModule;
	
	@FindBy(xpath=".//*[@id='module_cm']/a")
	public WebElement crmTab;
	
	@FindBy(xpath=".//*[@id= 'subMenu']//a[contains(text(), 'Search')]")
	public WebElement crmSearch;
	
	@FindBy(xpath=".//*[@original-title='Admin' or @original-title='Unit Admin']")
	public List<WebElement> adminGearIcon;
	

	//VipinG
	@FindBy(xpath = ".//a[@original-title='Create and view reports']")
	public WebElement reportLink ;
	
	
	public FCHomePage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
