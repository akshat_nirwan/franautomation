package com.builds.uimaps.common;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {

	@FindBy(name = "user_id")
	public WebElement userid;

	@FindBy(name = "password")
	public WebElement password;

	@FindBy(id = "ulogin")
	public WebElement login;
	
	@FindBy(xpath = ".//*[.='Login']")
	public WebElement loginBtBtn;

	@FindBy(xpath = ".//*[@id='loginform']/table/tbody/tr[4]/td/table/tbody/tr[2]/td")
	public WebElement incorrectMsg ;

	public LoginPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
