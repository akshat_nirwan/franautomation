package com.builds.uimaps.common;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ManageDivisionUI {
	
	@FindBy(xpath=".//input[@type='button' and @value='Add Division']")
	public WebElement addDivisionBtn;
	
	@FindBy(xpath=".//input[@type='button' and @value='Print']")
	public WebElement printBtn;
	
	@FindBy(xpath=".//input[@type='button' and @value='Cancel']")
	public WebElement cancelBtn;
	
	
	
	public ManageDivisionUI(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
