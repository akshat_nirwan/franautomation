package com.builds.uimaps.common;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class RegionalUserUI extends UserUI {

	@FindBy(id = "ms-parentroleID")
	public WebElement Area_Region_MultiSelect;

	@FindBy(name = "visibleToStore")
	public WebElement associateThisUserWithChecklistItems_Check;

	public RegionalUserUI(WebDriver driver) {
		super(driver);
	}

}
