package com.builds.uimaps.common;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Search {
	
	@FindBy(xpath=".//a[@qat_tabname='News']")
	public WebElement newsTab;
	
	@FindBy(xpath=".//a[@qat_tabname='Documents']")
	public WebElement documentTab;
	
	@FindBy(xpath=".//input[@name='search1' and @value='1' and @type='radio']")
	public WebElement documentRadioBtn;
	
	@FindBy(name="searchWord")
	public WebElement searchDocByWords;
	
	@FindBy(id="searchTitle")
	public WebElement searchWords;
	
	@FindBy(name="category")
	public WebElement newsCategory;
	
	@FindBy(name="searchField")
	public WebElement searchIn;
	
	@FindBy(xpath=".//input[@value='Search']")
	public WebElement searchBtn;
	
	@FindBy(id="searchAllDocument")
	public WebElement searchAllDocument;
	
	@FindBy(xpath=".//img[@title='Search All Documents']")
	public WebElement searchAllDocumentIcon;
	
	public Search(WebDriver driver){
		PageFactory.initElements(driver, this);
	}
	
}
