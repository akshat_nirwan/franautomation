package com.builds.uimaps.common;

public class UserLoginPage {
	public static String user_id = ".//*[@id='user_id_div']/input";
	public static String password = ".//*[@id='password_div']/input";
	public static String loginbtn = ".//*[@id='loginform']/table/tbody/tr[4]/td/table/tbody/tr[4]/td[2]/a/span";
	public static String ForgotPassword = ".//*[@id='loginform']/table/tbody/tr[6]/td/table/tbody/tr/td/a[1]/u";
}
