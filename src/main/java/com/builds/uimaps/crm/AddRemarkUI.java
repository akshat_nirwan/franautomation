package com.builds.uimaps.crm;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AddRemarkUI {
	
	@FindBy(id = "remark")
	public WebElement remark ;
	
	public AddRemarkUI(WebDriver driver)
	{
		PageFactory.initElements(driver, this);
	}
	

}
