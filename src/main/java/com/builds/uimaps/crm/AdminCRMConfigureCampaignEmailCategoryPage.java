package com.builds.uimaps.crm;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminCRMConfigureCampaignEmailCategoryPage {

	@FindBy(xpath = "//a[@class='link-btn-blue']")
	public WebElement addCategoty ;

	@FindBy(id = "serviceNameDetail")
	public WebElement categoryName;
	
	@FindBy(id = "serviceDescription")
	public WebElement categoryDescription;

	@FindBy(xpath = ".//input[@value='Add']")
	public WebElement addBtn ;

	@FindBy(xpath = ".//input[@value='Close']")
	public WebElement closeBtn ;
	
	@FindBy(xpath = ".//input[@value='Print']")
	public WebElement PrintBtn ;
	
	@FindBy(xpath = ".//input[@value='Back']")
	public WebElement BackBtn ;

	@FindBy(xpath = ".//input[@value='Modify']")
	public WebElement modifyBtn ;

	public AdminCRMConfigureCampaignEmailCategoryPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
