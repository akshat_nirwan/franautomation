package com.builds.uimaps.crm;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminCRMConfigureEmailContentPage {

	@FindBy(linkText = "Leads")
	public WebElement leads ;

	@FindBy(linkText = "Contacts")
	public WebElement contacts ;

	@FindBy(id = "fromEmail")
	public WebElement fromMail;

	@FindBy(id = "emailContent")
	public WebElement emailContent;

	@FindBy(xpath = ".//input[@value='Yes' and @name='isNotified']")
	public WebElement sendEmailNotification ;

	@FindBy(xpath = ".//*[@id='ms-parentotherEmailFields']")
	public WebElement emailFields ;

	@FindBy(xpath = ".//input[@value='Save']")
	public WebElement saveBtn ;

	@FindBy(xpath = ".//input[@value='Preview']")
	public WebElement previewBtn ;

	@FindBy(xpath = ".//input[@value='Close']")
	public WebElement closeBtn ;

	@FindBy(xpath = ".//*[@id='ms-parentotherEmailFields']//*[@id='selectAll']")
	public WebElement selectAll ;

	@FindBy(xpath = ".//*[@id='ms-parentotherEmailFields']//*[contains(text(),'Select All')]")
	public WebElement emailFieldsGetText ;

	public AdminCRMConfigureEmailContentPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
