package com.builds.uimaps.crm;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminCRMConfigureIndustryPage {

	@FindBy(linkText = "Add Industry")
	public WebElement addIndustry ;

	@FindBy(id = "industryType")
	public WebElement indusrty;

	@FindBy(id = "remarks")
	public WebElement description;

	@FindBy(xpath = ".//input[@value='Add']")
	public WebElement addBtn ;

	@FindBy(xpath = ".//input[@value='Save']")
	public WebElement saveBtn ;

	@FindBy(xpath = ".//*[@id='pageid']//*[.='Next']")
	public WebElement nextPage ;

	/* Anukaran mishra */
	@FindBy(xpath = "html/body/div[2]/table/tbody/tr/td/form/table/tbody/tr[3]/td/input[1]")
	public WebElement addBtn1 ;

	@FindBy(xpath = ".//input[@value='Close']")
	public WebElement closeBtn ;

	@FindBy(xpath = ".//a[.='Industry']")
	public WebElement industryTab ;

	public AdminCRMConfigureIndustryPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
