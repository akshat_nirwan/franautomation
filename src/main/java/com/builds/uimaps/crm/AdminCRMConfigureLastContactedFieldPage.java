package com.builds.uimaps.crm;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/* Harish Dwivedi */

public class AdminCRMConfigureLastContactedFieldPage {

	@FindBy(id = "formName")
	public WebElement formName;

	@FindBy(id = "formDisplayTitle")
	public WebElement formDisplayTitle;

	@FindBy(id = "enable_1")
	public WebElement enable_1CheckBox;

	@FindBy(id = "enable_2")
	public WebElement enable_2CheckBox;

	@FindBy(id = "enable_3")
	public WebElement enable_3CheckBox;

	@FindBy(id = "enable_7")
	public WebElement enable_4CheckBox;

	@FindBy(xpath = ".//input[@value='Modify' and @type='button']")
	public WebElement ModifyButton ;

	public AdminCRMConfigureLastContactedFieldPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
