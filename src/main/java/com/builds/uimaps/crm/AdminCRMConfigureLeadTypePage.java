package com.builds.uimaps.crm;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminCRMConfigureLeadTypePage {

	@FindBy(xpath=".//a[text() ='Configure Lead Type']")
	public WebElement ConfigureLeadType;
	
	
	@FindBy(xpath = ".//a[text() ='Add Lead Type ']")
	public WebElement addLead ;

	@FindBy(xpath = ".//input[@name='cmLeadTypeName']")
	public WebElement leadType ;

	@FindBy(xpath = ".//input[@value='Add']")
	public WebElement addBtn ;

	@FindBy(xpath = ".//input[@value='Close']")
	public WebElement closeBtn ;

	@FindBy(xpath = ".//*[@value='Save']")
	public WebElement saveBtn ;

	// anukaran
	@FindBy(name = "cmLeadTypeName")
	public WebElement leadModifyName;
	// Anukaran
	@FindBy(xpath = ".//*[@id='save']")
	public WebElement leadTypeModify ;

	@FindBy(name = "Ok")
	public WebElement Close;

	public AdminCRMConfigureLeadTypePage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
