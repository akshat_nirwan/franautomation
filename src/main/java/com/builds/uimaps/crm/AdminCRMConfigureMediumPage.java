package com.builds.uimaps.crm;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminCRMConfigureMediumPage {

	@FindBy(linkText = "Add Medium")
	public WebElement addMedium ;

	@FindBy(xpath = ".//input[@name='cmSource1Name']")
	public WebElement medium ;

	@FindBy(xpath = ".//textarea[@name='remarks']")
	public WebElement description ;

	@FindBy(xpath = ".//input[@value='Add' and @name='Add']")
	public WebElement addBtn ;

	@FindBy(xpath = ".//input[@value='Save' and @name='Add']")
	public WebElement saveBtn ;

	@FindBy(xpath = ".//input[@value='Ok' and @name='Ok']")
	public WebElement okBtn ;

	// Anukaran
	@FindBy(xpath = ".//input[@value='Close' and @name='Close']")
	public WebElement CloseBtn ;

	public AdminCRMConfigureMediumPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
