package com.builds.uimaps.crm;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminCRMConfigureOpportunityStagesPage {

	@FindBy(linkText = "Add Stage")
	public WebElement addStage ;

	@FindBy(id = "dataValueP")
	public WebElement stage;

	@FindBy(xpath = ".//input[@value='Add' and @name='Add']")
	public WebElement addBtn ;

	@FindBy(xpath = ".//input[@value='Modify' and @name='Modify']")
	public WebElement modifyBtn ;

	public AdminCRMConfigureOpportunityStagesPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
