package com.builds.uimaps.crm;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminCRMConfigureStatusPageUI {

	@FindBy(linkText = "Add Status")
	public WebElement addStatus ;

	@FindBy(id = "dataValueP")
	public WebElement statusTxBx;

	@FindBy(xpath = ".//input[@value='Add' and @name='Add']")
	public WebElement addBtn ;

	@FindBy(xpath = ".//input[@value='Modify' and @name='Modify']")
	public WebElement modifyBtn ;

	@FindBy(linkText = "Leads")
	public WebElement leads ;

	@FindBy(linkText = "Contacts")
	public WebElement contacts ;

	@FindBy(xpath = ".//input[@value='Cancel']")
	public WebElement CancelStatusBtn ;

	public AdminCRMConfigureStatusPageUI(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
