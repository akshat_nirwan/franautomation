package com.builds.uimaps.crm;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminCRMContactTypeConfigurationPage {

	@FindBy(id = "mform")
	public WebElement addContactType ;

	@FindBy(xpath = ".//input[@name='contactTypeName']")
	public WebElement contactType ;

	@FindBy(id = "save")
	public WebElement addBtn ;

	@FindBy(xpath = ".//input[@value='Close']")
	public WebElement closeBtn ;

	@FindBy(id = "cboxClose")
	public WebElement cboxClose;

	@FindBy(xpath = ".//input[@value='Modify']")
	public WebElement modifyBtn ;

	@FindBy(linkText = "Contacts")
	public WebElement contactTab ;

	public AdminCRMContactTypeConfigurationPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
