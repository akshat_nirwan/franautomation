package com.builds.uimaps.crm;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminCRMManageFormGeneratorPage {

	@FindBy(id = "addsectionlink")
	public WebElement btnAddSection;

	@FindBy(name = "sectionValue")
	public WebElement txtSectionName;

	@FindBy(id = "submitButton")
	public WebElement btnAddSectionName;

	@FindBy(name = "displayName")
	public WebElement txtFieldName;

	@FindBy(name = "dbColumnType")
	public WebElement drpFieldType;

	@FindBy(xpath = ".//*[@id='row_1']/td[2]/input[1]")
	public WebElement option1 ;

	@FindBy(xpath = ".//*[@id='row_2']/td[2]/input[1]")
	public WebElement option2 ;

	@FindBy(id = "submitButton")
	public WebElement btnSumbitField;

	@FindBy(linkText = "Add New Field")
	public WebElement btnAddField ;

	@FindBy(xpath = ".//*[@name='isTabularSection'][2]")
	public WebElement radioTabular ;

	@FindBy(xpath = ".//*[@id='builderForm']//select//option[@value='file']")
	public WebElement documentopt ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='builderForm']//select[@name='dbColumnType']")

	public WebElement FieldType ;
	// Anukaran
	@FindBy(xpath = ".//*[@id='containerDiv']//input[@name='isTabularSection' and @value='yes']")
	public WebElement tabularSection ;

	@FindBy(xpath = ".//input[@value='Continue']")
	public WebElement continueButton ;

	@FindBy(xpath = ".//*[@id='pettabs']//*[contains(text () , 'Contact')]")
	public WebElement contactTab ;

	@FindBy(xpath = ".//a[contains(text () , 'Primary Info')]")
	public WebElement primaryInfoLnk ;

	@FindBy(xpath = ".//td[contains(text () , 'Contact Information')]/ancestor::tr/td/a[contains(text () ,'Add New Field')]")
	public WebElement addNewFieldAtContactInfo ;

	public AdminCRMManageFormGeneratorPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
