package com.builds.uimaps.crm;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminCRMManageProductServiceCategoryPage {

	@FindBy(linkText = "Manage Product / Service")
	public WebElement manageProductServiceTab ;

	@FindBy(linkText = "Manage Category")
	public WebElement manageCategoryTab ;

	@FindBy(id = "searchTemplate1")
	public WebElement productServiceSearch;

	@FindBy(id = "searchTemplate")
	public WebElement categorySearch;

	@FindBy(linkText = "Add Product / Service")
	public WebElement addProductServiceBtn ;

	@FindBy(id = "productName")
	public WebElement productName;

	@FindBy(id = "oneLineItemDescription")
	public WebElement oneLineItemDescription;

	@FindBy(id = "rate")
	public WebElement rate;

	@FindBy(id = "categoryID")
	public WebElement categoryIDSelect;

	@FindBy(id = "ActiveCheck")
	public WebElement activeCheck;

	// @FindBy(xpath=".//input[@name='buttonSubmit']")
	@FindBy(id = "submitButton")
	public WebElement addBtn;

	// @FindBy(linkText="Add Category")
	@FindBy(name = "addCategory")
	public WebElement addCategoryBtn;

	@FindBy(id = "categoryName")
	public WebElement categoryName;

	@FindBy(id = "description")
	public WebElement description;

	// @FindBy(xpath=".//input[@name='buttonSubmit']")
	@FindBy(xpath = ".//input[@name='submitButton']")
	public WebElement saveBtn ;

	// Anukaran
	@FindBy(xpath = ".//input[@value='Cancel']")
	public WebElement cancelBtn ;

	public AdminCRMManageProductServiceCategoryPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
