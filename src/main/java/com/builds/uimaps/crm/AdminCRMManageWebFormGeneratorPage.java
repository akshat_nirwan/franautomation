package com.builds.uimaps.crm;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminCRMManageWebFormGeneratorPage {

	@FindBy(xpath = ".//input[@value='Create New Form']")
	public WebElement createNewForm ;

	@FindBy(id = "formName")
	public WebElement formName;

	@FindBy(id = "formDisplayTitle")
	public WebElement formDisplayTitle;

	@FindBy(id = "displayFormTitleCheckBox")
	public WebElement displayFormTitleCheckBox;

	@FindBy(id = "formDescription")
	public WebElement formDescription;

	@FindBy(xpath = ".//input[@name='formFor' and @value='lead']")
	public WebElement formTypeLeads ;

	@FindBy(xpath = ".//input[@name='formFor' and @value='cm']")
	public WebElement formTypeContact ;

	@FindBy(id = "formFormat")
	public WebElement formFormat;
	
	//VipinG
	@FindBy(xpath= ".//select[@id='formFormat']")
	public WebElement formFormatV;
	
	@FindBy(id = "submissionType")
	public WebElement submissionType;

	@FindBy(id = "columnCount")
	public WebElement columnCount;

	@FindBy(id = "filedLabelAlignment")
	public WebElement filedLabelAlignment;

	@FindBy(id = "formLanguage")
	public WebElement formLanguage;

	@FindBy(id = "iframeWidth")
	public WebElement iframeWidth;

	@FindBy(id = "iframeHeight")
	public WebElement iframeHeight;

	@FindBy(id = "formUrl")
	public WebElement formUrl;

	@FindBy(id = "detailsNextBtn")
	public WebElement saveNextBtn;

	@FindBy(id = "sectionBtn")
	public WebElement addSection;

	@FindBy(id = "fieldValue")
	public WebElement sectionName;

	@FindBy(id = "noOfCols")
	public WebElement noOfCols;

	@FindBy(id = "fieldLabelAlignment")
	public WebElement fieldLabelAlignmentSection;

	@FindBy(xpath = ".//input[@value='Add' and @id='submitButton']")
	public WebElement addBtn ;

	@FindBy(id = "sectionBtnPrw")
	public WebElement saveAndPreviewBtn;

	@FindBy(id = "designNextBtn")
	public WebElement saveAndNextBtnDesign;

	@FindBy(id = "availableFields_Tab")
	public WebElement availableFieldsTab;

	@FindBy(xpath = ".//a[contains(text () ,'Form Tools')]")
	public WebElement formTools ;

	@FindBy(xpath = ".//li[@id='header_fld']/a[contains(text () ,'Text')]")
	public WebElement formToolText ;

	@FindBy(xpath = ".//li[@id='recaptcha_fld']/a[contains(text () ,'Captcha')]")
	public WebElement formToolCaptcha ;

	@FindBy(xpath = ".//li[@id='disclamer_checkbox_fld']/a[contains(text () ,'CheckBox')]")
	public WebElement formToolIAgreeCheckBox ;
	
	//VipinG
	@FindBy(xpath = ".//li[@id='disclamer_checkbox_fld']/a[contains(text () ,'I Agree')]")
	public WebElement formToolIAgreeCheckBoxNew ;

	@FindBy(xpath = ".//a[contains(text () ,'Lead Info')]")
	public WebElement leadInfo ;

	@FindBy(xpath = ".//a[contains(text () ,'Primary Info')]")
	public WebElement primaryInfo ;

	@FindBy(xpath = ".//input[contains(@placeholder ,'Search fields')]")
	public WebElement searchField ;

	@FindBy(xpath = ".//*[@id='page_header']//p[.='Double click to add header.']")
	public WebElement addHeader ;

	@FindBy(xpath = ".//input[@value='Save']")
	public WebElement saveBtn ;

	@FindBy(xpath = ".//*[contains(@id ,'sectionHeader') and contains(@id , 'defaultSection')]")
	public WebElement defaultSectionHeaderAddText ;

	@FindBy(id = "fieldLabel")
	public WebElement labelAddText;

	@FindBy(id = "headerLabelAlignment")
	public WebElement headerLabelAlignment;

	// @FindBy(xpath=".//*[contains(@id ,'defaultSection') and contains(@id
	// ,'Flds')]/li[@class='endEmptyList']")
	@FindBy(xpath = ".//*[contains(@id ,'defaultSection') and contains(@id ,'Flds')]")
	public WebElement defaultDropHere ;

	@FindBy(xpath = ".//*[@id='page_footer']//p[.='Double click to add footer.']")
	public WebElement addFooterDefault ;

	@FindBy(xpath = ".//select[@id='cmLeadStatusID']")
	public WebElement cmLeadStatusID ;

	@FindBy(xpath = ".//select[@id='cmSource1ID']")
	public WebElement contactMediumSelect ;

	@FindBy(xpath = ".//select[@id='cmSource2ID' and @class='multiList']")
	public WebElement leadSource ;

	@FindBy(xpath = ".//select[@id='cmSource3ID' and @class='multiList']")
	public WebElement leadSourceDetailsSelect ;

	@FindBy(xpath = " .//input[@name='ownerType' and @value='C' and @type='radio']")
	public WebElement assignToCorporate ;

	@FindBy(xpath = " .//input[@name='ownerType' and @value='R' and @type='radio']")
	public WebElement assignToRegional ;

	@FindBy(xpath = " .//input[@name='ownerType' and @value='F' and @type='radio']")
	public WebElement assignToFranchise ;

	@FindBy(xpath = " .//input[@name='ownerType' and @value='D' and @type='radio']")
	public WebElement assignToDefault ;

	@FindBy(xpath = ".//select[@name='contactOwnerID2']")
	public WebElement selectCorporateUser ;

	@FindBy(xpath = ".//select[@id='areaID']")
	public WebElement selectAreaRegion ;

	@FindBy(xpath = ".//select[@id='contactOwnerID3']")
	public WebElement selectRegionalUser ;

	@FindBy(xpath = ".//select[@id='franchiseeNo']")
	public WebElement selectFranchiseID ;

	@FindBy(xpath = ".//select[@id='contactOwnerID1']")
	public WebElement selectFranchiseUser ;

	@FindBy(xpath = ".//input[@name='afterSubmissionMsgType' and @value='msg']")
	public WebElement afterSubmissionMsg ;

	@FindBy(xpath = ".//input[@name='afterSubmissionMsgType' and @value='url']")
	public WebElement afterSubmissionUrl ;

	@FindBy(id = "formCUrl")
	public WebElement redirectedUrl;

	@FindBy(xpath = ".//input[@id='settingsNextBtn' and @value='Finish']")
	public WebElement finishBtn ;

	@FindBy(id = "hostURL")
	public WebElement hostURL;

	@FindBy(id = "hostCodeBox")
	public WebElement hostCodeBox;

	@FindBy(id = "confirmationNextBtn")
	public WebElement okBtn;

	// add Tab
	@FindBy(id = "tabBtn")
	public WebElement addTab;

	@FindBy(id = "fieldValue")
	public WebElement tabName;

	@FindBy(xpath = ".//*[contains(@id , 'defaultTab') and contains(@id , 'Tab') and contains(text () ,'Default Tab')]")
	public WebElement defaultTab ;

	@FindBy(xpath = ".//input[@value='Preview Form' and @name='preBtn']")
	public WebElement previewFormAtSettingPage ;

	@FindBy(xpath = ".//div[@id='confirmationDiv']//input[@value='Preview Form' and @name='preBtn']")
	public WebElement previewFormAtConfirmationPage ;

	@FindBy(id = "searchMyForm")
	public WebElement searchMyForm;

	@FindBy(xpath = ".//img[@alt='Search on Form Name']")
	public WebElement searchMyFormBtn ;

	@FindBy(xpath = ".//select[@id='contactType']")
	public WebElement contactType ;

	// Launch & Test
	@FindBy(xpath = ".//a[.='Default Tab']")
	public WebElement defaultTabLaunch ;

	@FindBy(id = "leadLastName")
	public WebElement leadLastName;

	@FindBy(id = "leadFirstName")
	public WebElement leadFirstName;

	@FindBy(id = "title")
	public WebElement title;

	@FindBy(id = "companyName")
	public WebElement companyName;

	@FindBy(id = "address")
	public WebElement address;

	@FindBy(id = "city")
	public WebElement city;

	@FindBy(id = "country")
	public WebElement country;

	@FindBy(id = "zipcode")
	public WebElement zipcode;

	@FindBy(id = "state")
	public WebElement state;

	@FindBy(id = "phoneNumbers")
	public WebElement phoneNumbers;

	@FindBy(id = "extn")
	public WebElement extn;

	@FindBy(id = "faxNumbers")
	public WebElement faxNumbers;

	@FindBy(id = "mobileNumbers")
	public WebElement mobileNumbers;

	@FindBy(id = "alternateEmail")
	public WebElement alternateEmail;

	@FindBy(id = "position")
	public WebElement jobTitle;

	@FindBy(id = "birthdate")
	public WebElement birthdate;

	@FindBy(id = "cmSource2ID")
	public WebElement leadSourceLaunch;

	@FindBy(id = "cmSource3ID")
	public WebElement leadSourceDetailsLaunch;

	@FindBy(id = "nextButton")
	public WebElement nextBtn;

	@FindBy(id = ".//input[@value='Y']")
	public WebElement iAgreeChkBx;

	@FindBy(id = "submitButton")
	public WebElement submitBtn;

	@FindBy(id = "emailIds")
	public WebElement emailIds;

	// Anukaran
	@FindBy(xpath = ".//*[@id='pettabs']//*[contains(text(),'Opportunity')]")
	public WebElement formTypeOpportunity ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='instructiondetail']//input[@value='Continue']")
	public WebElement formContiue ;
	// Anukaran
	@FindBy(xpath = ".//*[@id='tabdetails']//a[contains(text(),'Remark')]")
	public WebElement oppremarks ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='siteMainTable']//a[contains(text(),'Add Document')]")
	public WebElement addDoc ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='docTitle']")
	public WebElement docSubject ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='abhi1']/td[2]/input[@name='docLabel']")
	public WebElement docLabel ;

	/* Anukaran */
	@FindBy(id = "contactFirstName")
	public WebElement contactFirstName;

	@FindBy(id = "contactLastName")
	public WebElement contactLastName;

	@FindBy(id = "cmSource2ID")
	public WebElement cmSourceDetails;

	@FindBy(id = "cmSource3ID")
	public WebElement howDidYouHearAboutUs;

	@FindBy(id = "anniversarydate")
	public WebElement anniversarydateField;

	/*
	 * Lead Info field
	 */

	@FindBy(linkText = "First Name")
	public WebElement firstNameLeadInfo;

	@FindBy(linkText = "Title")
	public WebElement titleLeadInfo;

	@FindBy(linkText = "Company")
	public WebElement companyLeadInfo;

	@FindBy(linkText = "Address")
	public WebElement addressLeadInfo;

	@FindBy(linkText = "City")
	public WebElement cityLeadInfo;

	@FindBy(linkText = "Country")
	public WebElement countryLeadInfo;

	@FindBy(linkText = "Zip / Postal Code")
	public WebElement zipPostalCodeLeadInfo;

	@FindBy(linkText = "State / Province")
	public WebElement stateProvinceLeadInfo;

	@FindBy(linkText = "Phone")
	public WebElement phoneLeadInfo;

	@FindBy(linkText = "Phone Extension")
	public WebElement phoneExtensionLeadInfo;

	@FindBy(linkText = "Fax")
	public WebElement faxLeadInfo;

	@FindBy(linkText = "Mobile")
	public WebElement mobileLeadInfo;

	@FindBy(linkText = "Email")
	public WebElement emailLeadInfo;

	@FindBy(linkText = "Alternate Email")
	public WebElement alternateEmailLeadInfo;

	@FindBy(linkText = "Job Title")
	public WebElement jobTitleLeadInfo;

	@FindBy(linkText = "Suffix")
	public WebElement suffixLeadInfo;

	@FindBy(linkText = "Birthdate")
	public WebElement birthdateLeadInfo;

	@FindBy(linkText = "Anniversary Date")
	public WebElement anniversaryDateLeadInfo;

	@FindBy(linkText = "How did you hear about us ? (Lead Source)")
	public WebElement hdyhauLSLeadInfo;

	@FindBy(linkText = "Best Time To Contact")
	public WebElement bestTimetoContactLeadInfo;

	@FindBy(linkText = "Comments")
	public WebElement commentsLeadInfo;

	public AdminCRMManageWebFormGeneratorPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
