package com.builds.uimaps.crm;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminCRMSourceSummaryPage {

	@FindBy(linkText = "Source Summary")
	public WebElement sourceSummaryTab ;

	@FindBy(linkText = "Franchise(s) Source Summary")
	public WebElement franchiseSourceSummaryTab ;

	@FindBy(xpath = "//td[@class='text_b']//a[1]")
	public WebElement addSource ;
	
	@FindBy(id = "mform")
	public WebElement addSource_Details_onFranchisee ;

	@FindBy(xpath = "//td[@class='text_b']//a[2]")
	public WebElement addSourceDetails ;

	@FindBy(xpath = ".//input[@name='cmSource2Name']")
	public WebElement source ;

	@FindBy(xpath = ".//textarea[@name='remarks']")
	public WebElement description ;

	@FindBy(id = "includePage")
	public WebElement displayOnWebPage;

	@FindBy(id = "cmSource2NameDisplay")
	public WebElement displayOnWebPageTxBx;

	@FindBy(xpath = ".//input[@value='Add' and @name='Add']")
	public WebElement addBtn ;

	@FindBy(xpath = ".//input[@value='Save']")
	public WebElement saveBtn ;

	@FindBy(id = "cmSource1IDMulti")
	public WebElement sourceSelect;

	@FindBy(id = "cmSource3Name")
	public WebElement sourceDetails;

	@FindBy(xpath = ".//textarea[@name='remarks']")
	public WebElement descriptionRemarks ;

	@FindBy(id = "includePage")
	public WebElement includePage;

	@FindBy(id = "cmSource3NameDisplay")
	public WebElement cmSource3NameDisplay;

	// Anukaran
	@FindBy(xpath = ".//input[@value='Close' and @name='close']")
	public WebElement closeBtn ;

	public AdminCRMSourceSummaryPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
