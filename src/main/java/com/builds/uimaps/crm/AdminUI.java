package com.builds.uimaps.crm;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminUI {
	
	@FindBy(xpath="//*[contains(text(),'Corporate User')]")
	public WebElement CorporateUser;
	
	public AdminUI(WebDriver driver)
	{
		
		PageFactory.initElements(driver,this);
	}
}
