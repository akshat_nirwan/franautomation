package com.builds.uimaps.crm;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CRMAccountsPage {

	@FindBy(id = "add_new")
	public WebElement addNew;

	
	@FindBy(xpath = ".//*[@id='showFilter']/a")
	public WebElement showfilters ;

	@FindBy(xpath = ".//td/a[.='Lead']")
	public WebElement leadLnk ;

	@FindBy(xpath = ".//td/a[.='Account']")
	public WebElement accountLnk ;

	@FindBy(xpath = ".//td/a[.='Contact']")
	public WebElement contactLnk ;

	@FindBy(xpath = ".//*[@id='searchSystem']")
	public WebElement SearchAccounts ;

	@FindBy(xpath = ".//td/a[.='Opportunity']")
	public WebElement OpportunityLnk ;

	@FindBy(xpath = "//td[@qat_tableid='addData']//*[@name='accountName']")
	public WebElement accountName;
	
	@FindBy(xpath = "//*[@name='accountName' and @placeholder='Search Accounts']")
	public WebElement accountNameinaccountinfopage;

	@FindBy(xpath = ".//input[@id='accountType' and @value='Y']")
	public WebElement B2BAccountType ;

	@FindBy(xpath = ".//input[@id='accountType' and @value='N']")
	public WebElement B2CAccountType ;

	@FindBy(id = "type")
	public WebElement visibilityType;

	@FindBy(id = "isParent")
	public WebElement isParent;

	@FindBy(id = "parentAccountID")
	public WebElement parentAccountID;

	@FindBy(id = "searchIcon")
	public WebElement searchIcon;

	@FindBy(id = "noOfEmployee")
	public WebElement noOfEmployee;

	@FindBy(id = "industry")
	public WebElement industry;

	@FindBy(id = "sicCode")
	public WebElement sicCode;

	@FindBy(id = "servicesProvided")
	public WebElement servicesProvided;

	@FindBy(id = "stockSymbol")
	public WebElement stockSymbol;

	@FindBy(id = "revenue")
	public WebElement revenue;

	@FindBy(id = "website")
	public WebElement website;

	@FindBy(xpath = ".//*[@id='addContact' and @value='Yes']")
	public WebElement addContactYes ;

	@FindBy(xpath = ".//*[@id='addContact' and @value='No']")
	public WebElement addContactNo ;

	@FindBy(id = "address")
	public WebElement address;

	@FindBy(id = "city")
	public WebElement city;

	@FindBy(id = "country")
	public WebElement country;

	@FindBy(id = "zipcode")
	public WebElement zipcode;

	@FindBy(id = "state")
	public WebElement state;

	@FindBy(id = "phoneNumbers")
	public WebElement phoneNumbers;

	@FindBy(id = "extn")
	public WebElement extn;

	@FindBy(id = "faxNumbers")
	public WebElement faxNumbers;

	@FindBy(id = "mobileNumbers")
	public WebElement mobileNumbers;

	@FindBy(id = "emailIds")
	public WebElement emailIds;

	@FindBy(id = "comments")
	public WebElement comments;

	@FindBy(xpath = ".//input[@value='Save']")
	public WebElement saveBtn ;

	@FindBy(xpath = ".//a[@original-title='Manage Account details associated with contacts and leads']")
	public WebElement accountsLink ;

	@FindBy(xpath = ".//a[@id='showFilter']")
	public WebElement showFilter ;

	@FindBy(xpath = ".//a[@id='hideFilter']")
	public WebElement hideFilter ;

	@FindBy(xpath = ".//*[@id='ms-parentvisibilityCombo']")
	public WebElement selectVisibility ;

	@FindBy(id = "matchType")
	public WebElement addOnSelect;

	@FindBy(xpath = ".//*[@id='ms-parentindustryType']")
	public WebElement selectIndustry ;

	@FindBy(xpath = ".//input[@name='duplicateOnly' and @value='no']")
	public WebElement showOnlyDuplicateNo ;

	@FindBy(id = "accountType")
	public WebElement selectAccountType;

	@FindBy(id = "parentChild")
	public WebElement selectShowAccounts;

	@FindBy(xpath = ".//*[@id='search']")
	public WebElement searchBtn ;

	@FindBy(id = "resultsPerPage")
	public WebElement resultsPerPage;

	@FindBy(id = "contactType")
	public WebElement contactType;

	@FindBy(id = "title")
	public WebElement title;

	@FindBy(id = "contactFirstName")
	public WebElement contactFirstName;

	@FindBy(id = "contactLastName")
	public WebElement contactLastName;

	@FindBy(id = "position")
	public WebElement position;

	@FindBy(id = "primaryContactMethod")
	public WebElement primaryContactMethodSelect;

	@FindBy(id = "alternateEmail")
	public WebElement alternateEmail;

	@FindBy(id = "birthdate")
	public WebElement birthdate;

	@FindBy(id = "anniversarydate")
	public WebElement anniversarydate;

	@FindBy(id = "cmLeadTypeID")
	public WebElement leadType;

	@FindBy(id = "cmRatingID")
	public WebElement rating;

	@FindBy(id = "cmSource2ID")
	public WebElement contactSource;

	@FindBy(id = "cmSource3ID")
	public WebElement contactSourceDetails;

	@FindBy(id = "cmSource1ID")
	public WebElement contactMedium;

	@FindBy(id = "bestTimeToContact")
	public WebElement bestTimeToContact;

	@FindBy(xpath = ".//*[@id='ms-parentgroupId']")
	public WebElement groupSelect ;

	@FindBy(xpath = ".//*[@id='ownerType' and @value='C']")
	public WebElement assignToCorporate ;

	@FindBy(xpath = ".//*[@id='ownerType' and @value='R']")
	public WebElement assignToRegional ;

	@FindBy(xpath = ".//*[@id='ownerType' and @value='F']")
	public WebElement assignToFranchise ;

	@FindBy(xpath = ".//select[@name='contactOwnerID2']")
	public WebElement selectCorporateUser ;

	@FindBy(xpath = ".//select[@id='areaID']")
	public WebElement selectAreaRegion ;

	@FindBy(xpath = ".//select[@id='contactOwnerID3']")
	public WebElement selectRegionalUser;

	@FindBy(id = "franchiseeNo")
	public WebElement selectFranchiseId;

	@FindBy(xpath = ".//select[@id='contactOwnerID1']")
	public WebElement selectFranchiseUser ;

	@FindBy(id = "suffix")
	public WebElement suffix;

	@FindBy(linkText = "Child Accounts")
	public WebElement childAccounts ;

	@FindBy(xpath = ".//a/span[.='Contacts']")
	public WebElement contactsTab ;

	@FindBy(xpath = ".//*[@id='modifyopp']/a[.='Modify']")
	public WebElement modifyTab ;

	@FindBy(xpath = ".//*[@id='deleteopp']/a[.='Delete']")
	public WebElement deleteAccounTab ;

	@FindBy(id = "remark")
	public WebElement remarks;

	@FindBy(xpath = ".//input[@value='Add']")
	public WebElement addBtn ;

	@FindBy(xpath = ".//input[@value='Close']")
	public WebElement closeBtn ;

	@FindBy(xpath = ".//input[@value='Modify']")
	public WebElement modifyTabBtm ;

	@FindBy(xpath = ".//a[.='Add Contact']")
	public WebElement addContact ;

	@FindBy(id = "radioOwner")
	public WebElement radioOwner;

	@FindBy(id = "radioUser")
	public WebElement radioUser;

	@FindBy(xpath = ".//*[@id='ms-parentassignTo']")
	public WebElement selectUser ;

	@FindBy(id = "status")
	public WebElement statusTask;

	@FindBy(id = "subject")
	public WebElement subject;

	@FindBy(xpath = ".//input[@name='timelessTaskId']")
	public WebElement timeLessTask ;

	@FindBy(id = "priority")
	public WebElement priority;

	@FindBy(xpath = ".//input[@value='Create']")
	public WebElement createBtn ;

	@FindBy(id = "callSubject")
	public WebElement callSubject;

	@FindBy(id = "callDate")
	public WebElement callDate;

	@FindBy(id = "sTime")
	public WebElement selectHour;

	@FindBy(id = "sMinute")
	public WebElement selectMinute;

	@FindBy(id = "APM")
	public WebElement selectAPM;

	@FindBy(id = "callStatus")
	public WebElement callStatus;

	@FindBy(id = "callType")
	public WebElement communicationType;

	@FindBy(xpath = ".//input[@value='No']")
	public WebElement noBtn ;

	@FindBy(linkText = "Detailed History")
	public WebElement detailedHistoryLink ;

	@FindBy(xpath = ".//*[@id='leadNames']/a[contains(text(),'Calls')]")
	public WebElement callsTabDH ;

	@FindBy(xpath = ".//a[contains(text () ,'Remarks') or contains(text () ,'Remarks History')]")
	public WebElement remarksTabAtCbox ;

	@FindBy(xpath = ".//input[@name='radioCheck' and @value='0']")
	public WebElement loggedUserId ;

	@FindBy(xpath = ".//input[@name='radioCheck' and @value='1']")
	public WebElement leadOwnerId ;

	@FindBy(xpath = ".//input[@name='radioCheck' and @value='2']")
	public WebElement customId ;

	@FindBy(xpath = ".//input[@name='customID']")
	public WebElement customIdField ;

	@FindBy(id = "mailTemplateID")
	public WebElement mailTemplateID;

	@FindBy(id = "mailSentTo")
	public WebElement mailSentTo;

	@FindBy(id = "mailCC")
	public WebElement mailCC;

	@FindBy(id = "showBcc")
	public WebElement showBcc;

	@FindBy(id = "mailBCC")
	public WebElement mailBCC;

	@FindBy(xpath = ".//input[@name='subject']")
	public WebElement subjectMail ;

	@FindBy(xpath = ".//input[@value='Send']")
	public WebElement sendEmailBtn ;

	@FindBy(xpath = ".//*[@id='sendButton1']/img")
	public WebElement sendEmailImage ;

	@FindBy(xpath = ".//*[@id='tinymce']/p")
	public WebElement editorTextArea ;

	@FindBy(xpath = ".//*[@id='leadNames']/a[.='Emails']")
	public WebElement emailsTab ;

	@FindBy(xpath = ".//select[@name='contactOwnerID2']")
	public WebElement selectCorporateUserContact ;

	@FindBy(xpath = ".//input[@name='name' and @value='second']")
	public WebElement secondNameSelect ;

	@FindBy(xpath = ".//input[@name='Merge']")
	public WebElement mergeBtn ;

	@FindBy(xpath = ".//textarea[@name='remarks']")
	public WebElement remarksAtMerge ;

	// changeOwner
	@FindBy(xpath = ".//*[@name='ownerType' and @value='R']")
	public WebElement selectRegionalUser1 ;

	@FindBy(xpath = ".//input[@value='Assign']")
	public WebElement assignBtn ;

	@FindBy(id = "statusChangeTo")
	public WebElement statusChangeTo;

	@FindBy(xpath = ".//input[@value='Change']")
	public WebElement changeBtn ;

	@FindBy(xpath = ".//div[contains(@data-target , 'FilterContainer')]")
	public WebElement filterContainer ;

	@FindBy(id = "searchCampaign")
	public WebElement searchCampaign;

	@FindBy(id = "search")
	public WebElement applyFilter;

	@FindBy(xpath = ".//*[@id='sendAssociate' and contains(text () ,'Send')]")
	public WebElement sendBtn ;

	@FindBy(id = "confirm")
	public WebElement confirmBtn;

	@FindBy(xpath = ".//span[.='Add to New Group']")
	public WebElement addToNewGroup ;

	// add Group info
	@FindBy(id = "groupName")
	public WebElement groupName;

	@FindBy(id = "groupDescription")
	public WebElement groupDescription;

	@FindBy(xpath = ".//label[.='Contact']")
	public WebElement contactTypeTab ;

	@FindBy(xpath = ".//label[.='Public to all Corporate Users']")
	public WebElement accessibility ;

	@FindBy(xpath = ".//button[.='Save & Continue']")
	public WebElement saveAndContinueBtn ;

	@FindBy(xpath = ".//*[@id='fc-drop-parentselColList']/button")
	public WebElement fieldSelectBtn ;

	@FindBy(xpath = ".//*[@id='fc-drop-parentselColList']/div/div/input")
	public WebElement searchField ;

	@FindBy(xpath = ".//*[@id='CM_CONTACT_DETAILS_contactType_MATCH']")
	public WebElement assignToCriteria ;

	@FindBy(xpath = ".//*[@id='fc-drop-parentCM_CONTACT_DETAILS_contactType']/button")
	public WebElement selectAllCriteria ;

	@FindBy(xpath = ".//*[@id='fc-drop-parentCM_CONTACT_DETAILS_contactType']//span[.='Select All']")
	public WebElement selectAllContactType ;

	@FindBy(id = "associatewithGroup")
	public WebElement associateWithGroupBtn;

	@FindBy(xpath = ".//button[.='OK']")
	public WebElement okBtn ;

	@FindBy(id = "searchSystem")
	public WebElement searchAtGroup;

	@FindBy(xpath = ".//label[.='Exact Search']")
	public WebElement exactSearchGroup ;

	@FindBy(xpath = ".//*[@id='systemSearchBtn']") 
	public WebElement searchAccountBtn ;
	
	@FindBy(xpath = ".//a[@class='searchvalue']")  
	public WebElement searchExistingAccountBtn ;

	@FindBy(id = "ms-parentcontactOwner")  //vipinS.//*[@id='ms-parentctCombo']
	public WebElement contactTypeFilter ;

	@FindBy(xpath = ".//a[@original-title='Search desired contacts and leads with customizable search']")
	public WebElement searchLink ;

	@FindBy(xpath = ".//span[.='Contacts']")
	public WebElement contactsTabAtSearch ;

	@FindBy(xpath = ".//input[@name='archivedleads' and @value='yes']")
	public WebElement archiveContacts ;

	@FindBy(xpath = ".//*[@id='ms-parentC:CONTACT_OWNER_ID:COMBO']")
	public WebElement assignTo ;

	@FindBy(xpath = ".//input[@name='Submit' and @value='Search']")
	public WebElement searchBtnAtSearch ;

	@FindBy(xpath = ".//input[@value='Log a Task']")
	public WebElement logaTaskBtmBtn ;

	@FindBy(xpath = ".//input[@value='Send Email']")
	public WebElement sendEmailBtmBtn ;

	@FindBy(xpath = ".//input[@value='Merge Contacts']")
	public WebElement mergeContactsBtmBtn ;

	@FindBy(xpath = ".//input[@value='Change Owner']")
	public WebElement changeOwnerBtmBtn ;

	@FindBy(xpath = ".//input[@value='Change Status']")
	public WebElement changeStatusBtmBtn ;

	@FindBy(name = "AssociateWithCampaignButton")
	public WebElement associatedWithEmailCampaignBtmBtn;

	@FindBy(xpath = ".//a/span[.='Opportunities']")
	public WebElement opportunitiesTab ;

	@FindBy(id = "opportunityName")
	public WebElement opportunityName;

	@FindBy(id = "opportunityType")
	public WebElement opportunityType;

	@FindBy(id = "opportunitySource")
	public WebElement opportunitySource;

	@FindBy(xpath = ".//*[@id='prodCombo']/button")
	public WebElement selectProductAndService ;

	@FindBy(xpath = ".//*[@id='prodCombo']//span[.='Uncheck All']")
	public WebElement uncheckAll ;

	@FindBy(xpath = ".//input[@placeholder='Enter Product / Service Name']")
	public WebElement searchProduct ;

	@FindBy(xpath = ".//*[@id='prodCombo']//span[.='Check All']")
	public WebElement checkAll ;

	@FindBy(xpath = ".//input[@name='checkBox']")
	public WebElement selectAllCheckBox;

	// @FindBy(xpath=".//*[@id='prodCombo']//span[contains(text () ,'Ok')]")
	@FindBy(xpath = ".//*[@id='prodCombo']//span[contains(text () ,'OK') or contains(text () , 'Ok')]")
	public WebElement okayBtn ;

	@FindBy(xpath = ".//input[@value='Submit']")
	public WebElement submitBtn ;

	@FindBy(xpath = ".//*[@qat_tabname='Transactions']")
	public WebElement transactionTab ;

	@FindBy(id = "stage")
	public WebElement stage;

	@FindBy(xpath = ".//input[@value='Change Stage']")
	public WebElement changeStatusBottmBtn ;

	@FindBy(xpath = ".//input[@value='Delete']")
	public WebElement deleteBtmBtn ;

	@FindBy(xpath = ".//a[.='New Transaction']")
	public WebElement newTransactionLink ;

	@FindBy(xpath = ".//*[@id='customizedAjaxSearch']/div")
	public WebElement ajaxResult ;

	@FindBy(xpath = ".//a[@original-title='View and manage details of existing contacts']")
	public WebElement contactsLinks ;

	@FindBy(xpath = ".//a/span[.='Account Info']")
	public WebElement accountInfoTab ;

	@FindBy(xpath = ".//a[.='Remove']")
	public WebElement removeAccountLink ;

	@FindBy(xpath = ".//input[@value='Associate']")
	public WebElement associateBtn ;

	@FindBy(xpath = ".//a[.='Add Opportunity']")
	public WebElement opportunityLink ;

	@FindBy(id = "opportunityOwner")
	public WebElement opportunityOwner;

	@FindBy(xpath = ".//input[@id='opportunityOwner']/following-sibling::a")
	public WebElement searchOppOwnerBtn ;

	@FindBy(id = "salesAmount")
	public WebElement salesAmount;

	@FindBy(xpath = ".//a[@original-title='View and manage details of opportunities']")
	public WebElement opportunitiesLinks ;

	@FindBy(xpath = ".//input[@value='Change Stage']")
	public WebElement cahngeStageBtmBtn ;

	@FindBy(xpath = ".//*[@id='deleteopp']/a[.='Delete']")
	public WebElement deleteTab ;

	@FindBy(xpath = ".//*[@id='logtask']/a")
	public WebElement logATaskTab ;

	@FindBy(xpath = ".//*[@id='at']/a")
	public WebElement logACallTab ;

	@FindBy(xpath = ".//*[@id='hm']/a")
	public WebElement addRemraksTab ;

	@FindBy(id = "remark")
	public WebElement remarkField;

	@FindBy(xpath = ".//td[@class='pvs_hdr2_new']/a[.='Log a Task']")
	public WebElement logATaskLinks ;

	@FindBy(xpath = ".//input[@value='Process']")
	public WebElement processTask ;

	@FindBy(xpath = ".//a[@original-title='View and manage transaction details']")
	public WebElement transactionLink ;

	@FindBy(xpath = ".//a[.='Add Transaction']")
	public WebElement addTransactionLink ;

	@FindBy(xpath = ".//*[@id='divisionID']")
	public WebElement divisionType ;

	@FindBy(xpath = ".//*[@id='opportunityNameTD']/a")
	public WebElement searchOpportunityBtn ;

	@FindBy(id = "invoiceDate")
	public WebElement invoiceDate;
	
	@FindBy(xpath = ".//a[@class='searchvalue']")
	public WebElement accountSearchBtn ;

	public CRMAccountsPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
