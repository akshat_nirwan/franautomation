package com.builds.uimaps.crm;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CRMCalendarPage {

	@FindBy(name = "CalendarUser")
	public WebElement calendarUserSelect;

	@FindBy(linkText = "Create")
	public WebElement createLink ;

	@FindBy(id = "schedule")
	public WebElement scheduleSelect;

	@FindBy(id = "subject")
	public WebElement subject;

	@FindBy(id = "description")
	public WebElement description;

	@FindBy(id = "startDatetime")
	public WebElement startDatetime;

	@FindBy(id = "endDatetime")
	public WebElement endDatetime;

	@FindBy(id = "location")
	public WebElement location;

	@FindBy(xpath = ".//input[@name='alldayEvent1']")
	public WebElement alldayEvent ;

	@FindBy(id = "scheduleCategory")
	public WebElement categorySelect;

	@FindBy(id = "priority")
	public WebElement priority;

	@FindBy(xpath = ".//input[@value='Create']")
	public WebElement createBtn ;

	@FindBy(id = "scheduleScope")
	public WebElement scheduleScope;

	@FindBy(xpath = ".//input[@value='Save']")
	public WebElement saveBtn ;

	@FindBy(xpath = ".//a[@href='calendarSearch']")
	public WebElement searchBtn ;

	@FindBy(id = "scheduleType")
	public WebElement scheduleType;

	@FindBy(xpath = ".//input[@name='subject' and @type='text']")
	public WebElement searchWords ;

	@FindBy(xpath = ".//input[@name='subchk']")
	public WebElement lookInSubCheck ;

	@FindBy(xpath = ".//input[@name='search' and @type='submit']")
	public WebElement searchBtnAtSearch ;

	public CRMCalendarPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
