package com.builds.uimaps.crm;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CRMCampaignCenterPage {

	@FindBy(xpath = ".//div[@class='list-name' and contains(text () ,'Create')]")
	public WebElement createBtn ;

	@FindBy(id = "CreateCampaignLink")
	public WebElement campaignLnk;

	@FindBy(id = "campaignTitle")
	public WebElement campaignName;

	@FindBy(xpath = ".//a[contains(text () ,'Add Description')]")
	public WebElement addDescription ;

	@FindBy(id = "campaignDescription")
	public WebElement campaignDescription;

	@FindBy(id = "campaignAccessibility")
	public WebElement campaignAccessibility;

	@FindBy(id = "serviceID")
	public WebElement category;

	@FindBy(xpath = ".//label[@for='quickCampaign1']")
	public WebElement campaignType ;

	@FindBy(xpath = ".//button[.='Start']")
	public WebElement startBtn ;

	@FindBy(linkText = "Code your own")
	public WebElement codeYourOwn ;

	@FindBy(id = "mailTitle")
	public WebElement templateName;

	@FindBy(id = "mailSubject")
	public WebElement mailSubject;

	@FindBy(xpath = ".//label[@for='PlaneText']")
	public WebElement planeText ;

	@FindBy(id = "ta")
	public WebElement textArea;

	@FindBy(id = "CodeYourOwnButton")
	public WebElement saveAndContinue;

	@FindBy(id = "associateLater")
	public WebElement associateLater;

	@FindBy(xpath = ".//button[.='Finalize And Launch Campaign']")
	public WebElement finalizeAndLaunchCampaignBtn ;

	// Anukaran
	@FindBy(xpath = ".//label[@for='quickCampaign2']")
	public WebElement campaignType2 ;

	@FindBy(xpath = ".//button[contains(text(),'Create Another Campaign') and @class='btn-style finalizeCampaign']")
	public WebElement createAnotherCampaign ;

	public CRMCampaignCenterPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
