package com.builds.uimaps.crm;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CRMContactsPage {

	@FindBy(id = "add_new")
	public WebElement addNew;

	@FindBy(xpath = ".//td/a[.='Lead']")
	public WebElement leadLnk ;

	@FindBy(xpath = ".//td/a[.='Account']")
	public WebElement accountLnk ;

	@FindBy(xpath = ".//td/a[.='Contact']")
	public WebElement contactLnk ;

	@FindBy(xpath = ".//td/a[.='Opportunity']")
	public WebElement OpportunityLnk ;

	@FindBy(id = "contactType")
	public WebElement contactType;

	@FindBy(id = "title")
	public WebElement title;

	@FindBy(id = "contactFirstName")
	public WebElement contactFirstName;

	@FindBy(id = "contactLastName")
	public WebElement contactLastName;

	@FindBy(id = "suffix")
	public WebElement suffix;

	@FindBy(id = "position")
	public WebElement position;

	@FindBy(id = "birthdate")
	public WebElement birthdate;

	@FindBy(id = "anniversarydate")
	public WebElement anniversarydate;

	@FindBy(id = "address")
	public WebElement address;

	@FindBy(id = "city")
	public WebElement city;

	@FindBy(id = "country")
	public WebElement country;

	@FindBy(id = "zipcode")
	public WebElement zipcode;

	@FindBy(id = "state")
	public WebElement state;

	@FindBy(id = "phoneNumbers")
	public WebElement phoneNumbers;

	@FindBy(id = "extn")
	public WebElement extn;

	@FindBy(id = "faxNumbers")
	public WebElement faxNumbers;

	@FindBy(id = "mobileNumbers")
	public WebElement mobileNumbers;

	@FindBy(id = "emailIds")
	public WebElement emailIds;

	@FindBy(id = "alternateEmail")
	public WebElement alternateEmail;

	@FindBy(id = "primaryContactMethod")
	public WebElement primaryContactMethodSelect;

	@FindBy(id = "bestTimeToContact")
	public WebElement bestTimeToContact;

	@FindBy(id = "cmLeadTypeID")
	public WebElement leadType;

	@FindBy(id = "cmRatingID")
	public WebElement rating;

	@FindBy(id = "cmSource2ID")
	public WebElement leadSource;

	@FindBy(id = "cmSource3ID")
	public WebElement leadSourceDetails;

	@FindBy(id = "cmSource1ID")
	public WebElement contactMedium;

	@FindBy(xpath = ".//*[@id='ownerType' and @value='C']")
	public WebElement assignToCorporate ;

	@FindBy(xpath = ".//*[@id='ownerType' and @value='R']")
	public WebElement assignToRegional ;

	@FindBy(xpath = ".//*[@id='ownerType' and @value='F']")
	public WebElement assignToFranchise ;

	@FindBy(xpath = ".//select[@name='contactOwnerID2']")
	public WebElement selectCorporateUserContact ;

	@FindBy(id = "comments")
	public WebElement comments;

	@FindBy(xpath = ".//input[@value='Save']")
	public WebElement saveBtn ;

	@FindBy(xpath = ".//*[@id='ms-parentgroupId']")
	public WebElement groupSelect ;

	@FindBy(xpath = ".//select[@id='areaID']")
	public WebElement selectAreaRegion ;

	@FindBy(xpath = ".//*[@id='contactOwnerID3']")
	public WebElement selectRegionalUser ;

	@FindBy(id = "franchiseeNo")
	public WebElement selectFranchiseId;

	@FindBy(id = "contactOwnerID1")
	public WebElement selectFranchiseUser;

	//@FindBy(xpath = "//*[@name='accountName']")
	@FindBy(xpath="//td[@qat_tableid='addData']//*[@name='accountName']")
	public WebElement accountName;

	@FindBy(xpath = ".//a[@original-title='View and manage details of existing contacts']")
	public WebElement contactsLinks ;

	@FindBy(xpath = ".//*[@id='ms-parentcontactOwner']")
	public WebElement contactOwnerSelect ;
	
	@FindBy(xpath = ".//input[@class ='searchInputMultiple']")
	public WebElement contactOwnerSelect_search ;
	
	@FindBy(xpath = "//input[@class='search-btn on']")
	public WebElement contactOwnerSelect_search_btn ;
	
	@FindBy(id = "selectAll")
	public WebElement search_btn_after_selectall ;

	@FindBy(xpath = ".//*[@id='ms-parentfCombo']")
	public WebElement franchiseIdSelect ;

	@FindBy(xpath = ".//*[@id='ms-parentctCombo']")
	public WebElement contactTypeSelect ;

	@FindBy(xpath = ".//*[@id='ms-parentcmSource2IDCombo']")
	public WebElement contactSourceSelect ;

	@FindBy(xpath = ".//*[@id='ms-parentleadStatus']")
	public WebElement statusSelect ;

	@FindBy(id = "matchType")
	public WebElement addDateSelect;

	@FindBy(id = "ms-parentsubscriptionSearch")
	public WebElement emailStatusSelect;

	@FindBy(xpath = ".//input[@name='duplicate' and @value='no']")
	public WebElement shoOnlyDuplicateNo ;

	@FindBy(xpath = ".//a[@id='showFilter']")
	public WebElement showFilter ;

	@FindBy(xpath = ".//a[@id='hideFilter']")
	public WebElement hideFilter ;

	@FindBy(id = "search")
	public WebElement searchBtn;

	@FindBy(id = "radioOwner")
	public WebElement radioOwner;

	@FindBy(id = "radioUser")
	public WebElement radioUser;

	@FindBy(xpath = ".//*[@id='ms-parentassignTo']")
	public WebElement selectUser ;

	@FindBy(id = "status")
	public WebElement statusTask;

	@FindBy(id = "subject")
	public WebElement subject;

	@FindBy(xpath = ".//input[@name='timelessTaskId']")
	public WebElement timeLessTask ;

	@FindBy(id = "priority")
	public WebElement priority;

	@FindBy(xpath = ".//input[@value='Create']")
	public WebElement createBtn ;

	@FindBy(id = "callSubject")
	public WebElement callSubject;

	@FindBy(id = "callDate")
	public WebElement callDate;

	@FindBy(id = "sTime")
	public WebElement selectHour;

	@FindBy(id = "sMinute")
	public WebElement selectMinute;

	@FindBy(id = "APM")
	public WebElement selectAPM;

	@FindBy(id = "callStatus")
	public WebElement callStatus;

	@FindBy(id = "callType")
	public WebElement communicationType;

	@FindBy(xpath = ".//input[@value='No']")
	public WebElement noBtn ;

	@FindBy(linkText = "Detailed History")
	public WebElement detailedHistoryLink ;

	@FindBy(xpath = ".//*[@id='leadNames']/a[.='Calls']")
	public WebElement callsTabDH ;

	@FindBy(linkText = "Remarks")
	public WebElement remarksTabAtCbox ;

	@FindBy(xpath = ".//input[@value='Close']")
	public WebElement closeBtn ;

	@FindBy(xpath = ".//*[@id='prodCombo']/button")
	public WebElement selectProductAndService ;

	@FindBy(xpath = ".//*[@id='prodCombo']//span[.='Uncheck All']")
	public WebElement uncheckAll ;

	@FindBy(xpath = ".//input[@placeholder='Enter Product / Service Name']")
	public WebElement searchProduct ;

	@FindBy(xpath = ".//*[@id='prodCombo']//span[.='Check All']")
	public WebElement checkAll ;

	@FindBy(xpath = ".//*[@id='prodCombo']//span[contains(text () ,'Ok')]")
	public WebElement okayBtn ;

	@FindBy(xpath = ".//input[@value='Submit']")
	public WebElement submitBtn ;

	@FindBy(id = "remark")
	public WebElement remarks;

	@FindBy(xpath = ".//input[@value='Add' or @name='Submit2']")
	public WebElement addBtn ;

	@FindBy(xpath = ".//input[@name='radioCheck' and @value='1']")
	public WebElement leadOwnerId ;

	@FindBy(xpath = ".//input[@name='radioCheck' and @value='2']")
	public WebElement customId ;

	@FindBy(xpath = ".//input[@name='customID']")
	public WebElement customIdField ;

	@FindBy(id = "mailTemplateID")
	public WebElement mailTemplateID;

	@FindBy(id = "mailSentTo")
	public WebElement mailSentTo;

	@FindBy(id = "mailCC")
	public WebElement mailCC;
	/////
	@FindBy(id = "showBcc")
	public WebElement showBcc;

	@FindBy(id = "mailBCC")
	public WebElement mailBCC;

	@FindBy(xpath = ".//input[@name='subject']")
	public WebElement subjectMail ;

	@FindBy(xpath = ".//input[@value='Send']")
	public WebElement sendEmailBtn ;

	@FindBy(xpath = ".//*[@id='sendButton1']/img")
	public WebElement sendEmailImage ;

	@FindBy(xpath = ".//*[@id='tinymce']/p")
	public WebElement editorTextArea ;

	@FindBy(xpath = ".//*[@id='leadNames']/a[.='Emails']")
	public WebElement emailsTab ;

	@FindBy(xpath = ".//*[@name='ownerType' and @value='R']")
	public WebElement selectRegionalUser1 ;

	@FindBy(xpath = ".//input[@value='Assign']")
	public WebElement assignBtn ;

	@FindBy(id = "statusChangeTo")
	public WebElement statusChangeTo;

	@FindBy(xpath = ".//input[@value='Change']")
	public WebElement changeBtn ;

	@FindBy(xpath = ".//textarea[@name='remarks']")
	public WebElement remarksAtMerge ;

	@FindBy(xpath = ".//div[contains(@data-target , 'FilterContainer')]")
	public WebElement filterContainer ;

	@FindBy(id = "searchCampaign")
	public WebElement searchCampaign;

	@FindBy(id = "search")
	public WebElement applyFilter;

	@FindBy(xpath = ".//*[@id='sendAssociate' and contains(text () ,'Send')]")
	public WebElement sendBtn ;

	@FindBy(id = "confirm")
	public WebElement confirmBtn;

	@FindBy(xpath = ".//span[.='Add to New Group']")
	public WebElement addToNewGroup ;

	@FindBy(id = "groupName")
	public WebElement groupName;

	@FindBy(id = "groupDescription")
	public WebElement groupDescription;

	@FindBy(xpath = ".//label[.='Contact']")
	public WebElement contactTypeTab ;

	@FindBy(xpath = ".//label[.='Public to all Corporate Users']")
	public WebElement accessibility ;

	@FindBy(xpath = ".//button[.='Save & Continue']")
	public WebElement saveAndContinueBtn ;

	@FindBy(xpath = ".//*[@id='fc-drop-parentselColList']/button")
	public WebElement fieldSelectBtn ;

	@FindBy(xpath = ".//*[@id='fc-drop-parentselColList']/div/div/input")
	public WebElement searchField ;

	@FindBy(xpath = ".//*[@id='CM_CONTACT_DETAILS_contactType_MATCH']")
	public WebElement assignToCriteria ;

	@FindBy(xpath = ".//*[@id='fc-drop-parentCM_CONTACT_DETAILS_contactType']/button")
	public WebElement selectAllCriteria ;

	@FindBy(xpath = ".//*[@id='fc-drop-parentCM_CONTACT_DETAILS_contactType']//span[.='Select All']")
	public WebElement selectAllContactType ;

	@FindBy(id = "associatewithGroup")
	public WebElement associateWithGroupBtn;

	@FindBy(xpath = ".//button[.='OK']")
	public WebElement okBtn ;

	@FindBy(id = "searchSystem")
	public WebElement searchAtGroup;

	@FindBy(xpath = ".//label[.='Exact Search']")
	public WebElement exactSearchGroup ;

	@FindBy(xpath = ".//*[@id='systemSearchBtn']")
	public WebElement searchAccountBtn ;

	@FindBy(xpath = ".//a[@original-title='Search desired contacts and leads with customizable search']")
	public WebElement searchLink ;

	@FindBy(xpath = ".//span[.='Contacts']")
	public WebElement contactsTabAtSearch ;

	@FindBy(xpath = ".//input[@name='archivedleads' and @value='yes']")
	public WebElement archiveContacts ;

	@FindBy(xpath = ".//*[@id='ms-parentC:CONTACT_OWNER_ID:COMBO']")
	public WebElement assignTo ;

	@FindBy(xpath = ".//input[@name='Submit' and @value='Search']")
	public WebElement searchBtnAtSearch ;

	@FindBy(xpath = ".//input[@value='Log a Task']")
	public WebElement logATaskBtmBtn ;

	@FindBy(xpath = ".//input[@value='Send Email']")
	public WebElement sendEmailBtmBtn ;

	@FindBy(xpath = ".//input[@value='Merge Contacts']")
	public WebElement mergeContactsBtmBtn ;

	@FindBy(xpath = ".//input[@value='Change Owner']")
	public WebElement changeOwnerBtmBtn ;

	@FindBy(xpath = ".//input[@value='Change Status']")
	public WebElement changeStatusBtmBtn ;

	@FindBy(xpath = ".//input[@name='AssociateWithRegularCampaignButton' or @name='AssociateWithCampaignButton']")
	public WebElement associateWithEmailCampaignBtmBtn ;

	@FindBy(xpath = ".//a[.='Modify']")
	public WebElement modifyContactTab ;

	@FindBy(xpath = ".//td[@class='btm-menu-rgt-link']/a[.='Log a Task']")
	public WebElement logATaskTab ;

	@FindBy(xpath = ".//a[.='Send Email']")
	public WebElement senEmailTab ;

	@FindBy(linkText = "Owner Change")
	public WebElement ownerChange ;

	@FindBy(xpath = ".//*[@id='at']/a")
	public WebElement logACallTab ;

	@FindBy(xpath = ".//*[@id='hm']/a")
	public WebElement addRemraksTab ;

	@FindBy(id = "remark")
	public WebElement remarkField;

	@FindBy(xpath = ".//select[@id='cmLeadStatusID']")
	public WebElement contactStatus ;

	@FindBy(xpath = ".//input[@value='Change Status']")
	public WebElement changeStatus ;

	@FindBy(xpath = ".//a[.='Assign']")
	public WebElement assignLink ;

	@FindBy(xpath = ".//a[.='Associate with Campaign']")
	public WebElement associateWithCampignLink ;

	@FindBy(xpath = ".//a[@class='bText12lnk' and .='Log a Task']")
	public WebElement logATaskLink ;

	@FindBy(xpath = ".//input[@value='Process']")
	public WebElement processTask ;

	@FindBy(name = "addToGroup")
	public WebElement addtoGroupBtmBtn;

	@FindBy(xpath = ".//input[@value='Archive Contact']")
	public WebElement archiveBtmBtn ;

	@FindBy(xpath = ".//a/span[.='Account Info']")
	public WebElement accountInfoTab ;

	@FindBy(xpath = ".//input[@value='Associate']")
	public WebElement associateBtn ;

	@FindBy(xpath = ".//a/span[.='Emails']")
	public WebElement emailsTab1 ;

	@FindBy(xpath = ".//a[.='Send Email']")
	public WebElement sendEmailAtEmails ;

	@FindBy(xpath = ".//input[@name='radioCheck' and @value='0']")
	public WebElement loggedUserId ;

	@FindBy(xpath = ".//a/span[.='Documents']")
	public WebElement documentsTab ;

	@FindBy(id = "cmDocumentTitle")
	public WebElement documentTitle;

	@FindBy(id = "cmDocumentAttachment")
	public WebElement cmDocumentAttachment;

	@FindBy(xpath = ".//input[@value='Add More']")
	public WebElement addMOreBtn ;

	@FindBy(xpath = ".//a/span[contains(text () , 'Other Addresses')]")
	public WebElement otherAddressTab ;

	@FindBy(id = "title")
	public WebElement titleAddress;

	@FindBy(xpath = ".//a[.='Modify']")
	public WebElement modifyAddressBtn ;

	@FindBy(xpath = ".//a[.='Delete']")
	public WebElement deleteAddress ;

	@FindBy(xpath = ".//input[@value='Add More']")
	public WebElement addMoreAddress ;

	// Anukaran
	@FindBy(xpath = ".//input[@name='contactTypeName']")
	public WebElement ContactTypeName ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='save']")
	public WebElement ModifyBtn ;

	// Anukaran
	@FindBy(xpath = ".//input[@name='contactTypeName']")
	public WebElement AddcontactType ;

	// Anukaran
	@FindBy(xpath = ".//input[@name='Modify']")
	public WebElement ModifyStatusBtn ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='dataValueP']")
	public WebElement ContactStatusName ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='status']")
	public WebElement LogATaskStatus ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='subject']")
	public WebElement LogATaskStatusName ;

	// Anukaran
	@FindBy(name = "calendarTaskCheckBox")
	public WebElement calendarTaskCheckBox;

	// Anukaran
	@FindBy(name = "add")
	public WebElement AddTaskBtn;

	// Anukaran
	@FindBy(xpath = ".//a[@original-title='View and manage events, meetings and appointments']")
	public WebElement calendarLink ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='siteMainTable']//input[@name='Submitcontact' and @value='Save']")
	public WebElement saveDuplicateCriContact ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='siteMainTable']//input[@name='Submitcompany' and @value='Save']")
	public WebElement saveDuplicateCriAccount ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='siteMainTable']//select[@name='selectfranchisee_no']")
	public WebElement selectFranID ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='siteMainTable']//input[@name='moveAtoO']")
	public WebElement moveToOwned ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='siteMainTable']//input[@name='moveOtoA']")
	public WebElement moveToAssigned ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='siteMainTable']/select[@name='franchiseuser']")
	public WebElement selectAssignTo ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='siteMainTable']//input[@name='Submit3' and @value='Update']")
	public WebElement UpdateBtn ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='siteMainTable']//select[@name='selectstate']")
	public WebElement selectStateDropDown ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='siteMainTable']//input[@value='Cancel']")
	public WebElement CancelBtn ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='pettabs']//a/span[contains(text(),'Default Location Owner')]")
	public WebElement defaultlocationOwnerTab ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='franchiseMenu']")
	public WebElement franchiseMenu ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='franchiseUser']")
	public WebElement franchiseUser ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='formContactType']")
	public WebElement formContactType ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='contactStatusId']")
	public WebElement contactStatusId ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='contactSource1Id']")
	public WebElement contactSource1Id ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='contactSource2Id']")
	public WebElement contactSource2Id ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='contactSource3Id']")
	public WebElement contactSource3Id ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='contactRegularCampaign']")
	public WebElement contactRegularCampaign ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='contactPromotionalCampaign']")
	public WebElement contactPromotionalCampaign ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='firstName_0']")
	public WebElement hContactFirstName ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='lastName_0']")
	public WebElement hContactLastName ;

	// Anukaran
	@FindBy(xpath = ".//*[@name='emailId_0']")
	public WebElement hContactEmail ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='phoneNumbers_0']")
	public WebElement hContactPhone ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='comment_0']")
	public WebElement hContactComment ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='Submit']")
	public WebElement hContactSubmit ;

	// Anukaran
	@FindBy(xpath = "@name='viewDetail'")
	public WebElement hContactVeiwDetails ;

	@FindBy(xpath = ".//*[@id='ownerType' and @value='B']")
	public WebElement assignToDivision ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='divisionID']")
	public WebElement selectDivisionId ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='contactOwnerID4']")
	public WebElement selectDivisionUser ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='ms-parentC_CONTACT_TYPE']/button")
	public WebElement selectSearchContactType ;

	@FindBy(xpath = ".//a[@original-title='Manage Account details associated with contacts and leads']")
	public WebElement AccountsLinks ;

	@FindBy(xpath = ".//a[.='Modify']")
	public WebElement modifyContactBtn ;

	@FindBy(xpath = ".//a[.='Primary Info']")
	public WebElement primaryInfoTab ;

	@FindBy(id = "Submit")
	public WebElement submitContactBtn;

	@FindBy(id = "cmLeadStatusID")
	public WebElement selectStatus;

	// Anukaran
	@FindBy(xpath = ".//*[@id='siteMainTable']//a[contains(text(),'Create')]")
	public WebElement createCalendarEvent ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='schedule']")
	public WebElement schedule ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='subject']")
	public WebElement meetingSubject ;

	// Anukaran
	@FindBy(xpath = ".//input[@name='searchCorpUsers']")
	public WebElement searchCorpUsers ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='userLists']//img[@title='Search Corporate Users']")
	public WebElement searchKey ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='scheduleScope']")
	public WebElement meetingScheduleScope ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='Participantdivlabel']/a")
	public WebElement participantsLnk ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='corpDiv']//input[@name='userToCorp']")
	public WebElement allParticipantsCheckBox ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='mceu_23']/button")
	public WebElement insertKeyword ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='select']")
	public WebElement selectKeyword ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='remDetails']//input[@value='Complete']")
	public WebElement complete ;

	// Anukaran
	@FindBy(xpath = ".//a[@original-title='Export contact and lead data to different file format']")
	public WebElement exportLink ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='siteMainTable']//input[@id='sModulecontact']")
	public WebElement contactExportbtn ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='siteMainTable']//input[@name='ExportAsExcel']")
	public WebElement proceedBtn ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='siteMainTable']//input[@value='Search Data']")
	public WebElement searchDataBtn ;

	@FindBy(xpath = ".//a[@original-title='View and manage transaction details']")
	public WebElement transactionsLinks ;

	// Anukaran
	@FindBy(xpath = ".//a[@original-title='View and manage tasks associated with leads, contacts and opportunities']")
	public WebElement tasksLinks ;

	@FindBy(xpath = ".//*[@id='siteMainTable']//a[contains(text(),'Add Task')]")
	public WebElement taskLnk ;

	// Anukaran
	@FindBy(xpath = ".//input[@name='associateWith1' and @value='0']")
	public WebElement contactTaskRadio ;

	// Anukaran
	@FindBy(xpath = ".//*[@value='Create Task']")
	public WebElement createTaskBtn ;

	@FindBy(xpath = ".//*[@id='searchSystem']")
	public WebElement searchContacts ;

	@FindBy(xpath = ".//a[@class='searchvalue']")
	public WebElement accountSearchBtn ;

	@FindBy(xpath = ".//select[@name='stateselect']")
	public WebElement selectStateAtAssociateZip ;

	@FindBy(xpath = ".//input[@value='Save and Add Another Contact']")
	public WebElement saveAddAnotherLead ;

	@FindBy(xpath = ".//input[@name='name' and @value='second']")
	public WebElement secondNameSelect ;

	@FindBy(xpath = ".//input[@name='Merge']")
	public WebElement mergeBtn ;

	@FindBy(xpath = ".//input[@name='archivedleads' and @value='yes']")
	public WebElement archiveLeads ;

	@FindBy(xpath = ".//input[@name='assignto' and @value='second']")
	public WebElement secondassigntoSelect ;

	@FindBy(xpath = ".//td//div[@title='Associate']")
	public WebElement sendCampaignBtn ;

	@FindBy(name = "AssociateWithCampaignButton")
	public WebElement associateWitCampaignBottomBtn;

	@FindBy(xpath = ".//a[contains(text(),'Associate with Campaign') or contains(text(),'Associate With Campaign')]")
	public WebElement campaignLinkAtLeadInfo ;

	@FindBy(xpath = ".//*[@id='hm']/a")
	public WebElement addRemarks ;

	@FindBy(id = "companyName")
	public WebElement companyName;
	
	@FindBy(xpath = "//*[@name='Submit22']")
	public WebElement deletecontact;
	
	@FindBy(xpath = "//*[@name='Submit22']")
	public WebElement archive;

	
	public CRMContactsPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
