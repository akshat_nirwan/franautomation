package com.builds.uimaps.crm;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CRMExportPage {

	@FindBy(xpath = ".//*[@value='cmLeadCall']")
	public WebElement chkbxActHistoryCall ;

	@FindBy(xpath = ".//*[@value='cmLeadTasks']")
	public WebElement chkbxActHistoryTask ;

	@FindBy(xpath = ".//*[@value='Search Data']")
	public WebElement srchDataBtn ;

	@FindBy(xpath = ".//*[@name='ExportAsExcel']")
	public WebElement ProceedBtn ;

	@FindBy(xpath = ".//label[@for='exlead']")
	public WebElement exportLead ;

	@FindBy(xpath = ".//*[@name='CM_LEAD_DETAILS1_leadAddDateFrom']")
	public WebElement leadDatefrom ;

	// @FindBy(xpath=".//*[@name='CM_LEAD_CALL10_callDateTo']")
	@FindBy(xpath = ".//input[contains(@name , 'callDateTo')]")
	public WebElement leadcallDateto ;

	@FindBy(xpath = ".//*[@name='CM_LEAD_DETAILS1_leadAddDateTo']")
	public WebElement leadDateto ;

	// @FindBy(xpath=".//*[@name='CM_LEAD_CALL10_callDateFrom']")
	@FindBy(xpath = ".//input[contains(@name , 'callDateFrom')]")
	public WebElement leadcallDatefrom ;

	@FindBy(name = "CM_LEAD_DETAILS1_leadFirstName")
	public WebElement firstname;

	@FindBy(xpath = ".//input[contains(@name , 'subject')]")
	public WebElement ExportTaskSubject ;

	@FindBy(name = "CM_LEAD_DETAILS1_leadLastName")
	public WebElement lastname;

	@FindBy(xpath = ".//*[@value='View Data']")
	public WebElement viewdata ;

	public CRMExportPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
