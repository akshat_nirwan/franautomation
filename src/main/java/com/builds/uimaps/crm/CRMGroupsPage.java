package com.builds.uimaps.crm;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CRMGroupsPage {

	@FindBy(xpath = ".//div[contains(text () ,'Create Group')]")
	public WebElement createGroupBtn ;

	@FindBy(id = "groupName")
	public WebElement groupName;

	@FindBy(name = "groupDescription")
	public WebElement groupDescription;

	@FindBy(id = "groupDescriptionLink")
	public WebElement groupDecLnk;

	@FindBy(xpath = ".//label[.='Contact']")
	public WebElement contactTypeTab ;

	@FindBy(xpath = ".//label[.='Public to all Corporate Users']")
	public WebElement accessibilityPAllCorporateUser ;

	@FindBy(xpath = ".//*[@id='saveGroup' or .='Save & Continue']")
	public WebElement saveAndContinueBtn ;

	@FindBy(id = "savebtn")
	public WebElement modifySaveGroup;

	@FindBy(xpath = ".//*[@id='fc-drop-parentselColList']/button")
	public WebElement fieldSelectBtn ;

	@FindBy(xpath = ".//*[@id='fc-drop-parentselColList']/div/div/input")
	public WebElement searchField ;

	@FindBy(xpath = ".//*[@id='CM_CONTACT_DETAILS_contactType_MATCH']")
	public WebElement assignToCriteria ;

	@FindBy(xpath = ".//*[@id='fc-drop-parentCM_CONTACT_DETAILS_contactType']/button")
	public WebElement selectAllCriteria ;

	@FindBy(xpath = ".//*[@id='fc-drop-parentCM_CONTACT_DETAILS_contactType']/div/div/input")
	public WebElement searchAreaCriteria ;

	@FindBy(xpath = ".//*[@id='fc-drop-parentCM_CONTACT_DETAILS_contactType']//span[.='Select All']")
	public WebElement selectAllContactType ;

	@FindBy(id = "associatewithGroup")
	public WebElement associateWithGroupBtn;

	@FindBy(xpath = ".//button[.='OK']")
	public WebElement okBtn ;

	@FindBy(id = "searchSystem")
	public WebElement searchAtGroup1;

	@FindBy(xpath = ".//label[.='Exact Search']")
	public WebElement exactSearchGroup ;

	@FindBy(xpath = ".//*[@id='systemSearchBtn']")
	public WebElement searchAccountBtn ;

	@FindBy(xpath = ".//*[@id='ms-parentctCombo']")
	public WebElement contactTypeFilter ;

	@FindBy(xpath = ".//*[@id='CM_LEAD_DETAILS_ownerType_MATCH']")
	public WebElement assignToCriteria1 ;

	@FindBy(xpath = ".//*[@id='fc-drop-parentCM_LEAD_DETAILS_ownerType']/button")
	public WebElement selectAssignToUserBtn ;

	@FindBy(xpath = ".//*[@id='fc-drop-parentCM_LEAD_DETAILS_ownerType']//span[.='Select All']")
	public WebElement selectAssignToSelectAll ;

	@FindBy(xpath = ".//label[.='Lead']")
	public WebElement leadTypeTab ;

	@FindBy(xpath = ".//label[.='Public to all Users']")
	public WebElement accessibilityPAllUser ;

	@FindBy(xpath = ".//label[.='Private']")
	public WebElement accessibilityPrivate ;

	@FindBy(xpath = ".//div[contains(@data-target , 'FilterContainer')]")
	public WebElement filterContainer ;

	@FindBy(id = "searchCampaign")
	public WebElement searchCampaign;

	@FindBy(id = "search")
	public WebElement applyFilter;

	@FindBy(xpath = ".//*[@id='sendAssociate' and contains(text () ,'Send')]")
	public WebElement sendBtn ;

	@FindBy(id = "confirm")
	public WebElement confirmBtn;

	@FindBy(xpath = ".//*[@id='FilterTabShowHide']//a/span[.='Archived']")
	public WebElement archivedTab ;

	@FindBy(xpath = ".//input[@value='Save']")
	public WebElement saveBtn ;

	@FindBy(xpath = ".//a[.='Modify']")
	public WebElement modifyBtn ;

	@FindBy(xpath = ".//*[@id='ms-parentgroupId']")
	public WebElement groupsNameSelect ;

	@FindBy(xpath = ".//a[@original-title='Categorize contacts and leads in groups and manage them']")
	public WebElement groupsLink ;

	@FindBy(xpath = ".//*[@id='fc-drop-parentCM_LEAD_DETAILS_ownerType']/div/div/input")
	public WebElement searchCriteria1 ;

	@FindBy(xpath = ".//a[.='Remove From Group']")
	public WebElement removeFromGroupBtn ;

	@FindBy(xpath = ".//*[@id='FilterTabShowHide']//a/span[.='Active']")
	public WebElement activeTab ;

	@FindBy(id = "add_new")
	public WebElement addNew;

	@FindBy(xpath = ".//td/a[.='Contact']")
	public WebElement contactLnk ;

	@FindBy(xpath = ".//*[@id='CM_CONTACT_DETAILS_ownerType_MATCH']")
	public WebElement configCreteria ;

	@FindBy(xpath = ".//*[@id='fc-drop-parentCM_CONTACT_DETAILS_ownerType']/button")
	public WebElement selectUserTypeConfigCriteria ;

	@FindBy(xpath = ".//*[@id='fc-drop-parentCM_CONTACT_DETAILS_ownerType']//span[.='Select All']")
	public WebElement selectAllUserTypeConfigCriteria ;

	@FindBy(xpath = ".//div[contains(@data-target , 'FilterContainer')]")
	public WebElement showFilter ;

	@FindBy(id = "searchTemplate")
	public WebElement groupNameSearch;

	@FindBy(id = "date-box1")
	public WebElement creationDate;

	@FindBy(id = "recordType")
	public WebElement groupType;

	@FindBy(xpath = ".//*[@id='fc-drop-parentgroupType']")
	public WebElement accessabilityTypeBtn ;

	@FindBy(xpath = ".//*[@id='FilterContainer']//button[.='Apply Filters']")
	public WebElement applyFilterAtGroup ;

	@FindBy(name = "groupFor")
	public WebElement groupTypeAddGroup;

	@FindBy(id = "Accessibility")
	public WebElement selectAccessibility;

	@FindBy(xpath = ".//*[@id='saveGroup']")
	public WebElement saveAndAssociateBtn ;

	// smart group
	@FindBy(xpath = ".//*[@id='saveGroupForm']//label[@for='smartList']")
	public WebElement isSmartGroup ;

	@FindBy(xpath = ".//*[@id='saveGroup']/span[contains(text () , 'Save & Continue')]")
	public WebElement saveAndContinueCheck ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='fc-drop-parentgroupType']/button")
	public WebElement fieldSelectGrpTypeBtn ;

	@FindBy(xpath = ".//*[@id='fc-drop-parentgroupType']/div/div/input")
	public WebElement searchGrpTypeField ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='CM_CONTACT_DETAILS_contactFirstName_MATCH']")
	public WebElement containsCriteria ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='CM_CONTACT_DETAILS_contactFirstName']")
	public WebElement containsFirstName ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='CM_CONTACT_DETAILS_contactAddDate_MATCH']")
	public WebElement assignToAddDate ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='CM_CONTACT_DETAILS_contactAddDate_fromDate']")
	public WebElement fromAddDate ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='CM_CONTACT_DETAILS_contactAddDate_toDate']")
	public WebElement toAddDate ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='CM_CONTACT_DETAILS_cmSource2ID_MATCH']")
	public WebElement assignToContactSource ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='CM_CONTACT_DETAILS_cmSource2ID']")
	public WebElement contactSource ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='CM_CONTACT_DETAILS_cmSource3ID_MATCH']")
	public WebElement assignToContactSrcDetails ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='fc-drop-parentCM_CONTACT_DETAILS_cmSource3ID']")
	public WebElement contactSrcDetailsSlctAll ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='CM_CONTACT_DETAILS_cmSource3ID']")
	public WebElement contactSrcDetailsSlctAll1 ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='fc-drop-parentCM_CONTACT_DETAILS_cmSource3ID']")

	public WebElement contactSourceDetails ;
	// Anukaran
	@FindBy(xpath = ".//*[@id='CM_LEAD_DETAILS_leadAddDate_MATCH']")
	public WebElement assignToAddDateLead ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='CM_LEAD_DETAILS_leadAddDate_fromDate']")
	public WebElement fromAddDateLead ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='CM_LEAD_DETAILS_leadAddDate_toDate']")
	public WebElement toAddDateLead ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='CM_LEAD_DETAILS_cmSource2ID_MATCH']")
	public WebElement assignToLeadSource ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='CM_LEAD_DETAILS_cmSource2ID']")
	public WebElement leadSource ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='CM_LEAD_DETAILS_cmSource3ID_MATCH']")
	public WebElement assignToLeadSrcDetails ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='fc-drop-parentCM_LEAD_DETAILS_cmSource3ID']")
	public WebElement leadSrcDetailsSlctAll ;

	@FindBy(xpath = ".//button[contains(text(),'Close')]")
	public WebElement closeNewCbox ;

	public CRMGroupsPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
