package com.builds.uimaps.crm;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CRMHomePage {

	@FindBy(xpath = ".//*[@id='siteMainTable']//span[.='Manage']")
	public WebElement manageBtn ;

	@FindBy(xpath = ".//*[@id='siteMainTable']//div[contains(text () , 'Add New')]")
	public WebElement addNew ;

	@FindBy(xpath = ".//a[.='Lead']")
	public WebElement addLeadLnk ;

	@FindBy(xpath = "//a[@href='javascript:void(0);'][contains(text(),'Contact')]")
	public WebElement addContactLnk ;

	@FindBy(xpath = ".//*[@id='siteMainTable']//p[.='Accounts']")
	public WebElement accountsTab ;

	@FindBy(xpath = ".//*[@id='siteMainTable']//p[.='Leads']")
	public WebElement leadsTab ;

	@FindBy(xpath = ".//*[@id='siteMainTable']//p[.='Contacts']")
	public WebElement contactsTab ;

	@FindBy(xpath = ".//*[@id='siteMainTable']//p[.='Opportunities']")
	public WebElement opportunitiesTab ;

	@FindBy(id = "leadFirstName")
	public WebElement leadFirstName;

	@FindBy(id = "leadLastName")
	public WebElement leadLastName;

	@FindBy(id = "address")
	public WebElement address;

	@FindBy(id = "city")
	public WebElement city;

	@FindBy(id = "country")
	public WebElement country;

	@FindBy(id = "zipcode")
	public WebElement zipcode;

	@FindBy(id = "state")
	public WebElement state;

	@FindBy(id = "primaryContactMethod")
	public WebElement primaryContactMethod;

	@FindBy(id = "cmSource1ID")
	public WebElement contactMediumSelect;

	@FindBy(id = "phoneNumbers")
	public WebElement phoneNumbers;

	@FindBy(id = "faxNumbers")
	public WebElement faxNumbers;

	@FindBy(id = "emailIds")
	public WebElement emailIds;

	@FindBy(id = "mobileNumbers")
	public WebElement mobileNumbers;

	@FindBy(id = "cmSource2ID")
	public WebElement leadSource;

	@FindBy(id = "cmSource3ID")
	public WebElement leadSourceDetails;

	@FindBy(xpath = ".//*[@id='ownerType' and @value='C']")
	public WebElement assignToCorporate ;

	@FindBy(xpath = ".//select[@name='leadOwnerID2']")
	public WebElement selectCorporateUser ;

	@FindBy(xpath = ".//*[@id='ownerType' and @value='R']")
	public WebElement assignToRegional ;

	@FindBy(id = "areaID")
	public WebElement selectAreaRegion;

	@FindBy(id = "leadOwnerID3")
	public WebElement selectRegionalUser;

	@FindBy(xpath = ".//*[@id='ownerType' and @value='F']")
	public WebElement assignToFranchise ;

	@FindBy(id = "franchiseeNo")
	public WebElement selectFranchiseId;

	@FindBy(xpath = ".//select[@id='leadOwnerID1']")
	public WebElement selcetFranchiseUser ;

	@FindBy(id = "comments")
	public WebElement comments;

	@FindBy(xpath = ".//*[@id='Submit']")
	public WebElement saveBtn ;

	@FindBy(id = "addanotherlead")
	public WebElement addAnotherLeadBtn;

	@FindBy(xpath = ".//button[@type='button' and @name='close']")
	public WebElement closeBtn ;

	// add Contact
	@FindBy(xpath = ".//*[@id='contactType']")
	public WebElement selectContactType ;

	@FindBy(id = "contactFirstName")
	public WebElement contactFirstName;

	@FindBy(id = "contactLastName")
	public WebElement contactLastName;

	@FindBy(xpath = ".//input[@id='ckb10']")
	public WebElement leadBySource ;

	@FindBy(xpath = ".//div[@class='grid-cell']/button[.='Apply']")
	public WebElement applyBtn ;

	@FindBy(xpath = ".//input[@id='sourcecontact']/following::label[.='Leads']")
	public WebElement leadBySourceLeads ;

	@FindBy(xpath = ".//input[@id='sourcecontact']/following::label[.='Contacts']")
	public WebElement leadBySourceContact ;

	@FindBy(xpath = ".//select[@name='contactOwnerID2']")
	public WebElement selectCorporateUserContact ;

	@FindBy(xpath = ".//a[@original-title='View and manage details of existing leads']")
	public WebElement leadsLink ;

	@FindBy(xpath = ".//a[@original-title='Take a quick overview']")
	public WebElement homeLnk ;

	@FindBy(xpath = ".//a[@original-title='View and manage details of existing contacts']")
	public WebElement contactsLink ;

	@FindBy(xpath = ".//*[@id='modifyLead']/a[.='Modify']")
	public WebElement modifyLeadBtn ;

	@FindBy(id = "Submit")
	public WebElement saveLeadBtn;

	@FindBy(id = "cmLeadStatusID")
	public WebElement selectStatus;

	@FindBy(xpath = ".//a[.='Modify']")
	public WebElement modifyContactBtn ;

	@FindBy(xpath = ".//a[.='Primary Info']")
	public WebElement primaryInfoTab ;

	public CRMHomePage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
