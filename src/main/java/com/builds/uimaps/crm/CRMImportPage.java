package com.builds.uimaps.crm;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CRMImportPage {

	@FindBy(xpath = ".//input[@name='salesFileName']")
	public WebElement importFile;

	@FindBy(xpath = ".//input[@name='continue']")
	public WebElement continueBtn;

	@FindBy(xpath = ".//input[@name='back']")
	public WebElement backBtn;

	@FindBy(xpath = ".//*[@id='mail']")
	public WebElement caanadianLeadDisclaimer;

	@FindBy(xpath = ".//*[@id='SALES_DATA__LEAD_FIRST_NAME$CM_LEAD_DETAILS']")
	public WebElement firstName;

	@FindBy(xpath = ".//*[@id='SALES_DATA__LEAD_LAST_NAME$CM_LEAD_DETAILS']")
	public WebElement lastName;

	@FindBy(xpath = ".//*[@id='SALES_DATA__EMAIL_IDS$CM_LEAD_DETAILS']")
	public WebElement email;

	@FindBy(xpath = ".//*[@id='SALES_DATA__COMPANY_NAME$CM_LEAD_DETAILS']")
	public WebElement companyName;

	@FindBy(xpath = ".//*[@id='SALES_DATA__ADDRESS$CM_LEAD_DETAILS']")
	public WebElement address1;

	@FindBy(xpath = ".//*[@id='SALES_DATA__ADDRESS2$CM_LEAD_DETAILS']")
	public WebElement address2;

	@FindBy(xpath = ".//*[@id='SALES_DATA__CITY$CM_LEAD_DETAILS']")
	public WebElement city;

	@FindBy(xpath = ".//*[@id='SALES_DATA__COUNTRY$CM_LEAD_DETAILS']")
	public WebElement country;

	@FindBy(xpath = ".//*[@id='SALES_DATA__STATE$CM_LEAD_DETAILS']")
	public WebElement stateProvince;

	@FindBy(xpath = ".//*[@id='SALES_DATA__ZIPCODE$CM_LEAD_DETAILS']")
	public WebElement zipPostalCode;

	@FindBy(xpath = ".//*[@id='SALES_DATA__PHONE_NUMBERS$CM_LEAD_DETAILS']")
	public WebElement workPhone;

	@FindBy(xpath = ".//*[@id='SALES_DATA__EXTN$CM_LEAD_DETAILS']")
	public WebElement workPhoneExt;

	@FindBy(xpath = ".//*[@id='SALES_DATA__HOME_PHONE']")
	public WebElement homePhone;

	@FindBy(xpath = ".//*[@id='SALES_DATA__HOME_PHONE_EXT']")
	public WebElement homePhoneExt;

	@FindBy(xpath = ".//*[@id='SALES_DATA__FAX_NUMBERS$CM_LEAD_DETAILS']")
	public WebElement fax;

	@FindBy(xpath = ".//*[@id='SALES_DATA__COMMENTS$CM_LEAD_DETAILS']")
	public WebElement comments;

	@FindBy(xpath = ".//*[@id='SALES_DATA__MOBILE_NUMBERS$CM_LEAD_DETAILS']")
	public WebElement mobile;

	@FindBy(xpath = ".//*[@id='SALES_DATA__BEST_TIME_TO_CONTACT$CM_LEAD_DETAILS']")
	public WebElement bestTimetoContact;

	@FindBy(xpath = ".//*[@id='SALES_DATA__CM_LEAD_STATUS_ID$CM_LEAD_DETAILS']")
	public WebElement leadStatus;

	@FindBy(xpath = ".//*[@id='SALES_DATA__CM_SOURCE_2_ID$CM_LEAD_DETAILS']")
	public WebElement leadSourceCatagory;

	@FindBy(xpath = ".//*[@id='SALES_DATA__CM_SOURCE_3_ID$CM_LEAD_DETAILS']")
	public WebElement leadSourceDetails;

	@FindBy(xpath = ".//*[@id='SALES_DATA__LEAD_OWNER_ID$CM_LEAD_DETAILS']")
	public WebElement assignTo;

	@FindBy(xpath = ".//*[@id='SALES_DATA__CM_SOURCE_1_ID$CM_LEAD_DETAILS']")
	public WebElement preferedModeOfContact;

	@FindBy(xpath = ".//*[@id='SALES_DATA__DIVISION']")
	public WebElement division;

	@FindBy(xpath = ".//*[@id='SYSTEM_DATA__DEFAULT_LEAD_ADD_DATE']")
	public WebElement inquiryDate;

	@FindBy(xpath = ".//*[@id='contactOwnerID2']")
	public WebElement defaultLeadOwner;

	@FindBy(xpath = ".//*[@id='SYSTEM_DATA__DEFAULT_CM_LEAD_STATUS_ID']")
	public WebElement defaultLeadStatus;

	@FindBy(xpath = ".//*[@id='SYSTEM_DATA__DEFAULT_CM_SOURCE_2_ID']")
	public WebElement defaultLeadSourceCatagory;

	@FindBy(xpath = ".//*[@id='SYSTEM_DATA__DEFAULT_CM_SOURCE_3_ID']")
	public WebElement defaultLeadSourceDetails;

	@FindBy(xpath = ".//*[@id='SYSTEM_DATA__DEFAULT_LEAD_LAST_NAME']")
	public WebElement defaultLeadLastName;

	public CRMImportPage(WebDriver driver) {

		PageFactory.initElements(driver, this);
	}

}
