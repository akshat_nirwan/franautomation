package com.builds.uimaps.crm;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CRMLeadsPage {

	@FindBy(id = "add_new")
	public WebElement addNew;

	@FindBy(xpath = ".//td/a[.='Lead']")
	public WebElement leadLnk ;

	@FindBy(xpath = ".//td/a[.='Account']")
	public WebElement accountLnk ;

	@FindBy(xpath = ".//td/a[.='Contact']")
	public WebElement contactLnk ;

	@FindBy(xpath = ".//td/a[.='Opportunity']")
	public WebElement OpportunityLnk ;

	@FindBy(xpath = ".//*[@id='searchSystem' and @placeholder='Search Leads']")
	public WebElement searchLeads ;

	@FindBy(xpath = ".//a[@id='showFilter']")
	public WebElement showFilter ;

	@FindBy(xpath = ".//*[@id='ms-parentleadOwner']")
	public WebElement leadOwnerSelect ;

	@FindBy(id="ms-parentleadOwner")
	public WebElement selectleadOwner;
	
	@FindBy(xpath = ".//*[@id='ms-parentfCombo']")
	public WebElement franchiseSelect ;

	@FindBy(xpath = ".//*[@id='ms-parentleadStatus']")
	public WebElement statusSelect ;

	@FindBy(id = "matchType")
	public WebElement addDateSelect;

	@FindBy(xpath = ".//*[@id='ms-parentemailStatus']")
	public WebElement emailStatusSelect ;

	@FindBy(xpath = ".//select[@id='viewBy']")
	public WebElement viewLeadsSelect ;

	@FindBy(xpath = ".//*[@id='search']")
	public WebElement searchBtn ;

	@FindBy(xpath = ".//*[@id='saveView']")
	public WebElement saveViewBtn ;

	@FindBy(xpath = ".//a[@id='hideFilter']")
	public WebElement hideFilter ;

	// add Lead
	@FindBy(id = "title")
	public WebElement title;

	@FindBy(id = "leadFirstName")
	public WebElement leadFirstName;

	@FindBy(id = "leadLastName")
	public WebElement leadLastName;

	@FindBy(id = "companyName")
	public WebElement companyName;

	@FindBy(id = "primaryContactMethod")
	public WebElement primaryContactMethodSelect;

	@FindBy(xpath = ".//*[@id='ownerType' and @value='C']")
	public WebElement assignToCorporate ;

	@FindBy(xpath = ".//*[@id='ownerType' and @value='R']")
	public WebElement assignToRegional ;

	@FindBy(xpath = ".//*[@id='ownerType' and @value='F']")
	public WebElement assignToFranchise ;

	@FindBy(xpath = ".//select[@name='leadOwnerID2']")
	public WebElement selectCorporateUser ;

	@FindBy(xpath = ".//select[@id='areaID']")
	public WebElement selectAreaRegion ;

	@FindBy(xpath = ".//select[@id='leadOwnerID3']")
	public WebElement selectRegionalUser ;

	@FindBy(id = "franchiseeNo")
	public WebElement selectFranchiseId;

	@FindBy(xpath = ".//select[@id='leadOwnerID1']")
	public WebElement selectFranchiseUser ;

	@FindBy(id = "address")
	public WebElement address;

	@FindBy(id = "city")
	public WebElement city;

	@FindBy(id = "country")
	public WebElement country;

	@FindBy(id = "zipcode")
	public WebElement zipcode;

	@FindBy(id = "state")
	public WebElement state;

	@FindBy(id = "phoneNumbers")
	public WebElement phoneNumbers;

	@FindBy(id = "extn")
	public WebElement extn;

	@FindBy(id = "faxNumbers")
	public WebElement faxNumbers;

	@FindBy(id = "mobileNumbers")
	public WebElement mobileNumbers;

	@FindBy(id = "emailIds")
	public WebElement emailIds;

	@FindBy(id = "alternateEmail")
	public WebElement alternateEmail;

	@FindBy(id = "suffix")
	public WebElement suffix;

	@FindBy(id = "position")
	public WebElement position;

	@FindBy(id = "birthdate")
	public WebElement birthdate;

	@FindBy(id = "anniversarydate")
	public WebElement anniversarydate;

	@FindBy(id = "cmLeadTypeID")
	public WebElement leadType;

	@FindBy(id = "cmRatingID")
	public WebElement rating;

	@FindBy(id = "cmSource2ID")
	public WebElement leadSource;

	@FindBy(id = "cmSource3ID")
	public WebElement leadSourceDetails;

	@FindBy(id = "cmSource1ID")
	public WebElement contactMedium;

	@FindBy(id = "bestTimeToContact")
	public WebElement bestTimeToContact;

	@FindBy(xpath = ".//*[@id='ms-parentgroupId']")
	public WebElement groupSelect ;

	@FindBy(id = "comments")
	public WebElement comments;

	@FindBy(xpath = ".//input[@value='Save']")
	public WebElement saveBtn ;

	@FindBy(xpath = ".//input[@value='Save and Add Another Lead']")
	public WebElement saveAddAnotherLead ;

	@FindBy(xpath = ".//a[@original-title='View and manage details of existing leads']")
	public WebElement leadsLink ;

	@FindBy(xpath = ".//a[@original-title='Manage Account details associated with contacts and leads']")
	public WebElement accountsLink ;

	@FindBy(xpath = ".//a[@original-title='View and manage details of opportunities']")
	public WebElement opportunitiesLink ;

	@FindBy(xpath = ".//select[@id='cmLeadStatusID']")
	public WebElement leadStatus ;

	// convert Lead
	@FindBy(xpath = ".//input[@name='userOption' and @value='0']")
	public WebElement leadOwnerRadio ;

	@FindBy(xpath = ".//input[@name='userOption' and @value='1']")
	public WebElement otherUserRadio ;

	@FindBy(id = "leadAssignTo")
	public WebElement leadAssignToOther;

	@FindBy(id = "contactFirstName")
	public WebElement contactFirstName;

	@FindBy(id = "contactLastName")
	public WebElement contactLastName;

	@FindBy(xpath = ".//input[@name='companyOption' and @value='N']")
	public WebElement createNewAccount ;

	@FindBy(id = "accountName")
	public WebElement accountName;

	@FindBy(xpath = ".//input[@value='Convert']")
	public WebElement convertBtn ;

	@FindBy(xpath = ".//input[@value='Go to Leads']")
	public WebElement goToLeadBtn ;

	@FindBy(xpath = ".//a[.='Account Info']")
	public WebElement acountInfoTab ;

	@FindBy(xpath = ".//*[@id='searchSystem']")
	public WebElement searchAccounts ;

	@FindBy(xpath = ".//input[@value='searchExact']")
	public WebElement searchExact ;

	@FindBy(xpath = ".//*[@id='systemSearchBtn']")
	public WebElement systemSearchButton ;

	// LOG A CALL
	@FindBy(id = "callSubject")
	public WebElement callSubject;

	@FindBy(id = "callDate")
	public WebElement callDate;

	@FindBy(id = "sTime")
	public WebElement selectHour;

	@FindBy(id = "sMinute")
	public WebElement selectMinute;

	@FindBy(id = "APM")
	public WebElement selectAPM;

	@FindBy(id = "callStatus")
	public WebElement callStatus;

	@FindBy(id = "callType")
	public WebElement communicationType;

	@FindBy(xpath = ".//input[@value='No']")
	public WebElement noBtn ;

	@FindBy(xpath = ".//input[contains(@value , 'Close') or @value='Close']")
	public WebElement closeBtn ;

	@FindBy(linkText = "Detailed History")
	public WebElement detailedHistoryLink ;

	@FindBy(xpath = ".//*[@id='leadNames']/a[.='Calls']")
	public WebElement callsTabDH ;

	// log a Task
	@FindBy(id = "radioOwner")
	public WebElement radioOwner;

	@FindBy(id = "radioUser")
	public WebElement radioUser;

	@FindBy(xpath = ".//*[@id='ms-parentassignTo']")
	public WebElement selectUser ;

	@FindBy(id = "status")
	public WebElement statusTask;

	@FindBy(id = "subject")
	public WebElement subject;

	@FindBy(xpath = ".//input[@name='timelessTaskId']")
	public WebElement timeLessTask ;

	@FindBy(id = "priority")
	public WebElement priority;

	@FindBy(xpath = ".//input[@value='Create']")
	public WebElement createBtn ;

	@FindBy(xpath = ".//*[@id='leadNames']/a[contains(text () ,'Open Task')]")
	public WebElement openTaskTab ;

	@FindBy(xpath = ".//input[@value='Log a Task']")
	public WebElement logTaskBtmBtn ;

	@FindBy(xpath = ".//input[@name='radioCheck' and @value='0']")
	public WebElement loggedUserId ;

	@FindBy(xpath = ".//input[@name='radioCheck' and @value='1']")
	public WebElement leadOwnerId ;

	@FindBy(xpath = ".//input[@name='radioCheck' and @value='2']")
	public WebElement customId ;

	@FindBy(xpath = ".//input[@name='customID']")
	public WebElement customIdField ;

	@FindBy(id = "mailTemplateID")
	public WebElement mailTemplateID;

	@FindBy(id = "mailSentTo")
	public WebElement mailSentTo;

	@FindBy(id = "mailCC")
	public WebElement mailCC;

	@FindBy(id = "showBcc")
	public WebElement showBcc;

	@FindBy(id = "mailBCC")
	public WebElement mailBCC;

	@FindBy(xpath = ".//input[@name='subject']")
	public WebElement subjectMail ;

	@FindBy(xpath = ".//input[@value='Send']")
	public WebElement sendEmailBtn ;

	@FindBy(xpath = ".//*[@id='sendButton1']/img")
	public WebElement sendEmailImage ;

	@FindBy(xpath = ".//*[@id='tinymce']/p")
	public WebElement editorTextArea ;

	@FindBy(id = "cmLeadStatusID")
	public WebElement selectStatus;

	@FindBy(id = "remarks")
	public WebElement remarks;

	@FindBy(xpath = ".//input[@value='Change']")
	public WebElement changeBtn ;

	@FindBy(xpath = ".//*[@name='ownerType' and @value='F']")
	public WebElement selectFranchiseAtCbox ;

	@FindBy(id = "contactOwnerID1")
	public WebElement franchiseUserSelect;

	@FindBy(xpath = ".//input[@value='Assign']")
	public WebElement assignBtn ;

	// create New Opportunity
	@FindBy(id = "isOpportunity")
	public WebElement isOpportunity;

	@FindBy(id = "opportunityName")
	public WebElement opportunityName;

	@FindBy(id = "stage")
	public WebElement stage;

	@FindBy(id = "salesAmount")
	public WebElement salesAmount;

	@FindBy(id = "closerDate")
	public WebElement closerDate;

	@FindBy(id = "opportunitySource")
	public WebElement opportunitySource;

	@FindBy(xpath = ".//span[.='Account Info']")
	public WebElement accountInfoTab ;

	@FindBy(xpath = ".//span[.='Opportunities']")
	public WebElement opportunityTab ;

	@FindBy(xpath = ".//a[@original-title='Search desired contacts and leads with customizable search']")
	public WebElement searchLink ;

	@FindBy(xpath = ".//span[.='Leads']")
	public WebElement leadsTab ;

	@FindBy(xpath = ".//*[@id='C:CONTACT_FIRST_NAME:TEXT']")
	public WebElement contactSearchFirstName ;

	@FindBy(xpath = ".//*[@id='C:CONTACT_LAST_NAME:TEXT']")
	public WebElement contactSearchLastName ;

	@FindBy(xpath = ".//*[@id='LEAD_FIRST_NAME:TEXT']")
	public WebElement leadSearchFirstName ;

	@FindBy(xpath = ".//*[@id='LEAD_LAST_NAME:TEXT']")
	public WebElement leadSearchLastName ;

	@FindBy(xpath = ".//input[@name='archivedleads' and @value='yes']")
	public WebElement archiveLeads ;

	@FindBy(xpath = ".//*[@id='ms-parentLEAD_OWNER_ID_COMBO']")
	public WebElement selectAssignTo ;

	@FindBy(xpath = ".//input[@name='Submit' and @value='Search']")
	public WebElement searchBtnAtSearch ;

	@FindBy(xpath = ".//span[.='Add to New Group']")
	public WebElement addToNewGroup ;

	// add Group info
	@FindBy(id = "groupName")
	public WebElement groupName;

	@FindBy(id = "groupDescription")
	public WebElement groupDescription;

	@FindBy(xpath = ".//label[.='Lead']")
	public WebElement leadTypeTab ;

	@FindBy(xpath = ".//label[.='Public to all Corporate Users']")
	public WebElement accessibility ;

	@FindBy(id = "Accessibility")
	public WebElement selectAccessibility;

	@FindBy(xpath = ".//button[.='Save & Continue']")
	public WebElement saveAndContinueBtn ;

	@FindBy(xpath = ".//*[@id='fc-drop-parentselColList']/button")
	public WebElement fieldSelectBtn ;

	@FindBy(xpath = ".//*[@id='fc-drop-parentselColList']/div/div/input")
	public WebElement searchField ;

	@FindBy(xpath = ".//*[@id='CM_LEAD_DETAILS_ownerType_MATCH']")
	public WebElement assignToCriteria ;

	@FindBy(xpath = ".//*[@id='fc-drop-parentCM_LEAD_DETAILS_ownerType']/button")
	public WebElement selectAssignToUserBtn ;

	@FindBy(xpath = ".//*[@id='fc-drop-parentCM_LEAD_DETAILS_ownerType']//span[.='Select All']")
	public WebElement selectAssignToSelectAll ;

	@FindBy(id = "associatewithGroup")
	public WebElement associateWithGroupBtn;

	@FindBy(xpath = ".//button[.='OK']")
	public WebElement okBtn ;

	@FindBy(id = "searchSystem")
	public WebElement searchAtGroup;

	@FindBy(xpath = ".//label[.='Exact Search']")
	public WebElement exactSearch ;

	@FindBy(xpath = ".//input[@value='Send Email']")
	public WebElement sendEmailBtmBtn ;

	@FindBy(xpath = ".//*[@id='FilterTabShowHide']//a[.='Promotional']")
	public WebElement promotionalLink ;

	@FindBy(xpath = ".//div[contains(@data-target , 'FilterContainer')]")
	public WebElement filterContainer ;

	@FindBy(id = "searchCampaign")
	public WebElement searchCampaign;

	@FindBy(id = "search")
	public WebElement applyFilter;

	@FindBy(xpath = ".//*[@id='sendAssociate' and contains(text () ,'Send')]")
	public WebElement sendBtn ;

	@FindBy(id = "confirm")
	public WebElement confirmBtn;

	@FindBy(xpath = ".//*[@id='searchSystem' and @placeholder='Search for Email Campaigns and Email Templates']")
	public WebElement searchCampaignTx ;

	@FindBy(xpath = ".//input[@value='Change Status']")
	public WebElement changeStatus ;

	@FindBy(xpath = ".//input[@id='accountType' and @value='Y']")
	public WebElement B2BAccountType ;

	@FindBy(xpath = ".//input[@id='accountType' and @value='N']")
	public WebElement B2CAccountType ;

	@FindBy(id = "type")
	public WebElement visibilityType;

	@FindBy(id = "isParent")
	public WebElement isParent;

	@FindBy(id = "parentAccountID")
	public WebElement parentAccountID;

	@FindBy(id = "searchIcon")
	public WebElement searchIcon;

	@FindBy(id = "noOfEmployee")
	public WebElement noOfEmployee;

	@FindBy(id = "industry")
	public WebElement industry;

	@FindBy(id = "sicCode")
	public WebElement sicCode;

	@FindBy(id = "servicesProvided")
	public WebElement servicesProvided;

	@FindBy(id = "stockSymbol")
	public WebElement stockSymbol;

	@FindBy(id = "revenue")
	public WebElement revenue;

	@FindBy(id = "website")
	public WebElement website;

	@FindBy(xpath = ".//*[@id='addContact' and @value='Yes']")
	public WebElement addContactYes ;

	@FindBy(xpath = ".//*[@id='addContact' and @value='No']")
	public WebElement addContactNo ;

	// add Account
	@FindBy(id = "contactType")
	public WebElement contactType;

	@FindBy(xpath = ".//select[@name='contactOwnerID2']")
	public WebElement selectCorporateUserContact ;

	@FindBy(linkText = "Modify")
	public WebElement modifyTab ;

	@FindBy(linkText = "Log a task")
	public WebElement logATaskTab ;

	@FindBy(xpath = ".//*[@id='modifyLead']/a[.='Send Email']")
	public WebElement sendEmailTab ;

	@FindBy(linkText = "Owner Change")
	public WebElement ownerChange ;

	// @FindBy(xpath=".//button[contains(text () ,'Associate')]")
	@FindBy(xpath = ".//td//div[@title='Associate']")
	public WebElement sendCampaignBtn ;

	@FindBy(name = "AssociateWithCampaignButton")
	public WebElement associateWitCampaignBottomBtn;

	@FindBy(xpath = ".//*[@id='hm']/a")
	public WebElement addRemarks ;

	@FindBy(id = "remark")
	public WebElement remarkField;

	@FindBy(linkText = "Remarks")
	public WebElement remarksTabAtCbox ;

	@FindBy(xpath = ".//*[@id='at']/a")
	public WebElement logACall ;

	@FindBy(xpath = ".//a[.='Log a Task']")
	public WebElement logATaskLink ;

	@FindBy(xpath = ".//a[contains(text(),'Associate with Campaign') or contains(text(),'Associate With Campaign')]")
	public WebElement campaignLinkAtLeadInfo ;

	@FindBy(xpath = ".//input[@value='Change Owner']")
	public WebElement changeOwnerBtmBtn ;

	@FindBy(xpath = ".//*[@id='contactOwnerID3']")
	public WebElement selectRegionalUserR ;

	@FindBy(name = "addToGroup")
	public WebElement addToGroupLinkBtmBtnAtLeadInfo;

	@FindBy(xpath = ".//input[@name='AssociateWithRegularCampaignButton']")
	public WebElement AssociateWithRegularCampaignButton ;

	@FindBy(xpath = ".//a[.='Emails']")
	public WebElement emailsTab ;

	@FindBy(xpath = ".//a[.='Send Email']")
	public WebElement sendEmailAtEmails ;

	@FindBy(xpath = ".//input[@name='ownerType' and @value='R']")
	public WebElement selectRegional ;

	@FindBy(id = "searchMyForm")
	public WebElement searchMyForm;

	@FindBy(xpath = ".//img[@alt='Search on Form Name']")
	public WebElement searchMyFormBtn ;

	@FindBy(id = "cmSource2ID")
	public WebElement leadSourceLaunch;

	@FindBy(id = "cmSource3ID")
	public WebElement leadSourceDetailsLaunch;

	@FindBy(id = "nextButton")
	public WebElement nextBtn;

	@FindBy(id = ".//input[@value='Y']")
	public WebElement iAgreeChkBx;

	@FindBy(xpath = ".//*[@id='submitButton' and contains(@value , 'Submit')]")
	public WebElement submitBtn ;

	@FindBy(id = "resultsPerPage")
	public WebElement resultsPerPage;

	/* Harish Dwivedi */
	@FindBy(xpath = ".//*[@id='leadNames']/a[contains(text () ,'Status Change')]")
	public WebElement StatusChange ;

	@FindBy(id = "Accessibility")
	public WebElement groupAccessibility;

	@FindBy(id = "saveGroup")
	public WebElement saveGroupButton;

	@FindBy(xpath = ".//td[@class='tab-selected1']/a[.='Lead Info']")
	public WebElement leadInfoTab ;

	/* Anukaran Mishra */
	@FindBy(xpath = ".//*[@id='siteMainTable']/tbody/tr/td/table[2]/tbody/tr[4]/td[1]/table[1]/tbody/tr[1]/td[2]/table/tbody/tr[2]/td[2]/input[2]")
	public WebElement ConvertLead ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='siteMainTable']//input[@value='E']")
	public WebElement radioAssociateWithExistingAcc ;

	@FindBy(id = "btnConvert")
	public WebElement btnConvert;

	@FindBy(xpath = ".//a[@original-title='View and manage details of existing contacts' or @original-title='View and manage details of existing Contacts']")
	public WebElement contactsLinks ;

	@FindBy(id = "searchSystem")
	public WebElement searchSystem;

	@FindBy(xpath = ".//*[@name='companyOption' and @value='E']")
	public WebElement AssociateExistingAccount ;

	@FindBy(xpath = ".//*[@id='systemSearchBtn']")
	public WebElement searchAccountBtn ;

	@FindBy(id = "accountNameSearch")
	public WebElement accountNameSearch;

	// Anukaran
	@FindBy(xpath = ".//input[@id='dataValueP' and @name='dataValueP']")
	public WebElement LeadStatusName ;

	// Anukaran
	@FindBy(xpath = ".//input[@value='Modify' and @name='Modify']")
	public WebElement ModifyStatusBtn ;

	@FindBy(xpath = ".//input[@value='Change Status' and @name='changeStatusButton']")
	public WebElement ChangeStatusBtn ;

	@FindBy(id = "showQuickLinks")
	public WebElement showQuickLinks;

	@FindBy(xpath = ".//*[@id='contentQuickLinks']//a[contains(text () , 'Add Lead')]")
	public WebElement addLeadQuickLink ;

	@FindBy(xpath = ".//*[@id='showBar']/img")
	public WebElement showQuickLinkBar ;

	@FindBy(xpath = ".//*[@id='contentQuickLinks']//a[contains(text () , 'Add Contact')]")
	public WebElement addContactQuickLinks ;

	@FindBy(xpath = ".//*[@id='modifyLead']/a[.='Modify']")
	public WebElement modifyLeadBtn ;

	@FindBy(id = "Submit")
	public WebElement saveLeadBtn;

	@FindBy(id = "statusChangeTo")
	public WebElement statusChangeTo;

	// Anukaran
	@FindBy(xpath = ".//*[@id='sModulelead']")
	public WebElement leadExportBtn ;

	@FindBy(xpath = ".//*[@id='divisionID']")
	public WebElement leadDivision ;

	@FindBy(xpath = ".//*[@id='leadOwnerID4']")
	public WebElement leadDivisionUser ;

	@FindBy(xpath = ".//input[@name='name' and @value='second']")
	public WebElement secondNameSelect ;

	@FindBy(xpath = ".//textarea[@name='remarks']")
	public WebElement remarksAtMerge ;

	@FindBy(xpath = ".//input[@name='Merge']")
	public WebElement mergeBtn ;

	@FindBy(xpath = ".//input[@name='assignto' and @value='second']")
	public WebElement secondassigntoSelect ;

	@FindBy(xpath = ".//input[@name='statusChange']")
	public WebElement changeButton ;

	@FindBy(xpath = ".//input[@name='print']")
	public WebElement processTask ;

	@FindBy(name = "another")
	public WebElement another;

	@FindBy(xpath = ".//*[@id='ownerType' and @value='B']")
	public WebElement assignToDivision ;

	@FindBy(xpath = ".//*[@qat_adminlink='Configure Summary Display Columns']")
	public WebElement configureSummaryDisplayColumns;

	@FindBy(xpath = ".//*[@id='searchSystem' and @placeholder='Search Contacts']")
	public WebElement searchContact ;

	@FindBy(xpath = ".//input[@value='Go to Summary']")
	public WebElement goTosummaryBtn ;
	
	@FindBy(xpath="//*[@value='Archive']")
	public WebElement archive;
	
	@FindBy(xpath="//*[@name='Submit22']")
	public WebElement deleteLead;
	
	@FindBy(xpath="//*[@id='ms-parentleadOwner']/div/div[1]/input[1]")
	public WebElement searchText;

	@FindBy(xpath="//*[@class='search-btn on']")
	public WebElement seacrhicon;
	
	@FindBy(xpath="//ul[@class='keySearchedResult']//input[@id='selectAll']")
	public WebElement check;
	
	//VipinG
	@FindBy(xpath=".//u[contains(text(),'Leads')]")
	public WebElement Leadslinkofleadsummarypage;
	
	public CRMLeadsPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
