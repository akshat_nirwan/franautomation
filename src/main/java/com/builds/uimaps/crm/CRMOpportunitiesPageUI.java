package com.builds.uimaps.crm;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CRMOpportunitiesPageUI {

	@FindBy(id = "add_new")
	public WebElement addNew;

	@FindBy(xpath = ".//td/a[.='Opportunity']")
	public WebElement OpportunityLnk ;

	@FindBy(id = "opportunityName")
	public WebElement opportunityName;

	@FindBy(id = "oppAccountID")
	public WebElement oppAccountID;

	@FindBy(xpath = ".//*[@id='oppAccountIDTD']/a[@class='searchvalue']")
	public WebElement searchAccountBtn ;

	@FindBy(id = "oppContactID")
	public WebElement oppContactID;

	@FindBy(id = "searchSystem")
	public WebElement SearchBox;

	@FindBy(id = "systemSearchBtn")
	public WebElement systemSearchBtn;

	@FindBy(xpath = ".//label[.='Exact Search']")
	public WebElement exactSearchGroup ;

	@FindBy(xpath = ".//*[@id='oppContactIDTD']/a[@class='searchvalue']")
	public WebElement searchContactBtn ;

	@FindBy(id = "opportunityOwner")
	public WebElement opportunityOwner;

	@FindBy(xpath = ".//input[@id='opportunityOwner']/following-sibling::a")
	public WebElement searchOppOwnerBtn ;

	@FindBy(id = "stage")
	public WebElement stage;

	@FindBy(xpath = ".//select[@id='opportunityType']")
	public WebElement opportunityType ;

	@FindBy(id = "probability")
	public WebElement probability;

	@FindBy(id = "opportunitySource")
	public WebElement opportunitySource;

	@FindBy(id = "closerDate")
	public WebElement closerDate;

	@FindBy(id = "salesAmount")
	public WebElement salesAmount;

	@FindBy(xpath = ".//input[@value='Save']")
	public WebElement saveBtn ;

	@FindBy(xpath = ".//a[@original-title='View and manage details of opportunities']")
	public WebElement opportunitiesLinks ;

	@FindBy(xpath = ".//a[@id='showFilter']")
	public WebElement showFilter ;

	@FindBy(xpath = ".//a[@id='hideFilter']")
	public WebElement hideFilter ;

	@FindBy(xpath = ".//*[@id='ms-parentowner']")
	public WebElement assignToSelect ;

	@FindBy(xpath = ".//*[@id='ms-parentstage']")
	public WebElement stageSelect ;

	@FindBy(id = "matchType")
	public WebElement addOn;

	@FindBy(id = "search")
	public WebElement searchBtn;

	@FindBy(xpath = ".//input[@value='Change Stage']")
	public WebElement cahngeStageBtmBtn ;

	public CRMOpportunitiesPageUI(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
