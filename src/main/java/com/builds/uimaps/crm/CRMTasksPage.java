package com.builds.uimaps.crm;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CRMTasksPage {

	@FindBy(xpath = ".//a[.='Add Task']")
	public WebElement addTaskLink ;

	@FindBy(xpath = ".//input[@value='2' and @name='associateWith1']")
	public WebElement associatedWithLead ;

	@FindBy(xpath = ".//input[@value='0' and @name='associateWith1']")
	public WebElement associateWithContact ;

	@FindBy(xpath = ".//input[@value='1' and @name='associateWith1']")
	public WebElement associatedWithOpportunity ;

	@FindBy(xpath="//*[@name='cmLeadName']")
	public WebElement leadSearch;

	@FindBy(id = "name")
	public WebElement contactSearch;

	@FindBy(id = "opportunityName")
	public WebElement opportunitySearch;

	@FindBy(xpath = ".//a[@original-title='View and manage tasks associated with leads, contacts and opportunities']")
	public WebElement taskLinks ;

	@FindBy(id = "radioOwner")
	public WebElement radioOwner;

	@FindBy(id = "radioUser")
	public WebElement radioUser;

	@FindBy(xpath = ".//*[@id='ms-parentassignTo']")
	public WebElement selectUser ;

	@FindBy(id = "status")
	public WebElement statusTask;

	@FindBy(id = "subject")
	public WebElement subject;

	@FindBy(xpath = ".//input[@name='timelessTaskId']")
	public WebElement timeLessTask ;

	@FindBy(id = "priority")
	public WebElement priority;

	@FindBy(xpath = ".//input[@name ='add']")
	public WebElement createBtn ;

	@FindBy(id = "comments")
	public WebElement comments;

	@FindBy(xpath = ".//*[@id='showFilter']/a[.='Show Filters']")
	public WebElement showFilter ;

	@FindBy(xpath = ".//*[@id='showFilter']/a[.='Hide Filters']")
	public WebElement hideFilters ;

	@FindBy(id = "subject")
	public WebElement subjectFilter;

	@FindBy(xpath = ".//*[@id='ms-parentstatus']")
	public WebElement taskStatusFilter ;

	@FindBy(id = "startDate")
	public WebElement startdateFilter;

	@FindBy(id = "endDate")
	public WebElement endDate;

	@FindBy(id = "reminder")
	public WebElement reminderFilter;

	@FindBy(xpath = ".//*[@id='ms-parentpriority']")
	public WebElement prioprityFilter ;

	@FindBy(xpath = ".//*[@id='ms-parentmineall']")
	public WebElement viewMineFilter ;

	@FindBy(xpath = ".//*[@id='ms-parenttaskForeignType']")
	public WebElement associatedWithFilter ;

	@FindBy(id = "go")
	public WebElement searchBtnFilter;

	@FindBy(id = "add_new")
	public WebElement addNew;

	@FindBy(xpath = ".//td/a[.='Contact']")
	public WebElement contactLnk ;

	@FindBy(xpath = ".//a[@original-title='View and manage details of opportunities']")
	public WebElement opportunitiesLinks ;

	@FindBy(xpath = ".//td/a[.='Opportunity']")
	public WebElement OpportunityLnk ;

	@FindBy(xpath = ".//input[@name='add']")
	public WebElement saveBtn ;

	@FindBy(xpath = ".//input[@name='print']")
	public WebElement processTask ;

	@FindBy(xpath = ".//input[@name='statusChange']")
	public WebElement changeButton ;

	@FindBy(xpath = ".//input[@name='print2' and @value='Change Status']")
	public WebElement chnageBottomBtn ;

	@FindBy(xpath = ".//input[@name='print3' and @value='Delete']")
	public WebElement deleteBottomButton ;

	@FindBy(xpath = ".//*[.='Modify']")
	public WebElement modifyTask ;

	@FindBy(xpath = ".//*[.='Complete']")
	public WebElement completeTask ;

	@FindBy(xpath = ".//a[@href='configureCompletionMail']")
	public WebElement configureCompletionMail ;

	@FindBy(xpath = ".//input[@id='subject']")
	public WebElement subjectField ;

	@FindBy(xpath = ".//input[@name='Submit']")
	public WebElement configureBtn ;
	
	@FindBy(name = "close")
	public WebElement closebtn ;
	
	@FindBy(name = "another")
	public WebElement Createanother ;
	
	//VipinG
	@FindBy(xpath = "//*[@name='sTime']")
	public WebElement Starttimehour;
	
	@FindBy(xpath = "//*[@name='sMinute']")
	public WebElement Starttimeminute ;
	
	@FindBy(xpath = "//*[@name='APM']")
	public WebElement Startimeapm ;
	
	@FindBy(xpath = "//*[@name='eTime']")
	public WebElement Endtimehour;
	
	@FindBy(xpath = "//*[@name='eMinute']")
	public WebElement Endtimeminute ;
	
	@FindBy(xpath = "//*[@name='MPM']")
	public WebElement Endtimeapm ;
	
	//VipinG
	@FindBy(xpath = ".//input[@id='searchSystem']")
	public WebElement searchLeadOnSystem ;
	
	@FindBy(xpath=".//input[@id='systemSearchBtn']")
	public WebElement searchbtnonLeadSystem;
	
	@FindBy(xpath=".//*[@name='checkBox']")
	public WebElement clickonleadcheckbox;
	
	@FindBy(xpath="//*[@id='Actions_dynamicmenu0Bar']")
	public WebElement clickonActionMenuLeadSummary;
	
	@FindBy(xpath=".//*[@id='Action_dynamicmenu30']")
	public WebElement clickonLogATask;
	
	@FindBy(xpath=".//*[contains(text(),'Log a Task')]")
	public WebElement clickonLogATaskunderLeadinfopage;
	
	@FindBy(xpath="//*[@id='Action_dynamicmenu00']")
	public WebElement modify;
	
	
	public CRMTasksPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
