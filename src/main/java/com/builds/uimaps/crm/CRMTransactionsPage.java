package com.builds.uimaps.crm;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CRMTransactionsPage {

	@FindBy(xpath = ".//a[@id='showFilter']")
	public WebElement showFilter ;

	@FindBy(xpath = ".//a[@id='hideFilter']")
	public WebElement hideFilter ;

	@FindBy(xpath = ".//*[@id='ms-parentoCombo']")
	public WebElement opportunityOwner ;

	@FindBy(xpath = ".//*[@id='ms-parentfCombo']")
	public WebElement franchiseIdSelect ;

	@FindBy(id = "matchType")
	public WebElement transactionDate;

	@FindBy(id = "search")
	public WebElement searchBtn;

	public CRMTransactionsPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
