package com.builds.uimaps.crm;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CRM_AssociateWithCampaignConfirmationPageUI {
	
	@FindBy(id = "confirm")
	public WebElement Confirmbtn;
	
	@FindBy(id = "cancelConfirmation")
	public WebElement Cancelconfirm;
	
	@FindBy(id = "backConfirmation")
	public WebElement Backconfirm;
	
	public CRM_AssociateWithCampaignConfirmationPageUI(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	

}
