package com.builds.uimaps.crm;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CRM_CampaignAddUI {
	
	
	
	@FindBy(xpath = ".//input[@id='campaignTitle' and @type ='text']")
	public WebElement campaignName;
    
	@FindBy(id = "campaignDescriptionLink")
	public WebElement Descriptionlink;

	@FindBy(id = "campaignDescription")
	public WebElement Description;

	@FindBy(id = "campaignAccessibility")
	public WebElement Accessibility;

	@FindBy(id = "quickCampaign1")
	public WebElement PromotionalRadio;
	
	@FindBy(id = "quickCampaign2")
	public WebElement StatusDrivenRadio;
	
	@FindBy(xpath = ".//button[contains(text(),'Start')]")
	public WebElement Start;

	@FindBy(xpath = ".//button[contains(text(),'Cancel')]")
	public WebElement Cancel;

	@FindBy(xpath = ".//button[contains(text(),'Save')]")
	public WebElement Save;
	
	@FindBy(xpath = ".//button[contains(text(),'Close')]")
	public WebElement close;
	
	
	public CRM_CampaignAddUI(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}


}
