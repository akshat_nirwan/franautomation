package com.builds.uimaps.crm;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CRM_CampaignCenterUI {

	@FindBy(xpath = ".//*[@data-role='ico_AddCircleOutline']")
	public WebElement clickCreatebtn;

	@FindBy(id = "CreateCampaignLink")
	public WebElement CreateCampaign_Link;
	
	@FindBy(xpath = ".//iframe[@class = 'newLayoutcboxIframe']")
	public WebElement CreateCampaign_frame;

	@FindBy(xpath = "//ul[@class='dropdown-list open-menu']/li[contains(text(),'Template')]")
	public WebElement CreateTemplate_Link;

	@FindBy(xpath = "//li[@onclick='javascript:createGroup();']")
	public WebElement CreateRecipientsGroup_Link;

	@FindBy(id = "open-overlay")
	public WebElement CreateWorkflow_Link;
	
	@FindBy(id = "overlay-div")
	public WebElement CreateWorkflow_frame;
	
	@FindBy(xpath = "//div[@id='PrintHeaderMenuContainerCampaign']//div[@class='svg-icon white']")
	public WebElement DashboradSidelink;
	
	@FindBy(xpath = "//ul[@class='dropdown-list direction-right open-menu']//a[@href='cmCampaignSummary']")
	public WebElement Dashboard_link;
	
	@FindBy(id = "//ul[@class='dropdown-list direction-right open-menu']//a[@href='campaignCenterDashboardReport']")
	public WebElement Campaign_Link;
	
	@FindBy(id = "//div[@id='PrintHeaderMenuContainerCampaign']//li[3]")
	public WebElement Template_Link;
	
	@FindBy(id = "//div[@id='PrintHeaderMenuContainerCampaign']//li[4]")
	public WebElement RecipientGroup_Link;
	
	@FindBy(id = "//div[@id='PrintHeaderMenuContainerCampaign']//li[5]")
	public WebElement Workflow_Link;
	
	@FindBy(id = "//div[@id='PrintHeaderMenuContainerCampaign']//li[6]")
	public WebElement Media_Link;

	
	public CRM_CampaignCenterUI(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
}
