package com.builds.uimaps.crm;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CRM_CampaignFinalandLunchUI {
	
	@FindBy(xpath = "//div[@id='siteMainDiv']//button[2]")
	public WebElement finalizebottombtn;
	
	@FindBy(id = "backFinal")
	public WebElement Backbottombtn;
	
	@FindBy(id = "timeZone")
	public WebElement TimeZone;
	
	@FindBy(xpath = "//div[@class='flex page-heading-section']//button[@class='btn-style finalizeCampaign']")
	public WebElement finalizeupperbtn   ;
	
	@FindBy(id = "campaignTitle")
	public WebElement campaignName;
	
	@FindBy(xpath = ".//span[@style='font-size: 18px;']")
	public WebElement confirmpagetext;
	
	@FindBy(xpath = "//div[@class='svg-icon data-action']")
	public WebElement FilterBtn;
	
	@FindBy(id = "searchCampaign")
	public WebElement SearchCampaignfield;
	
	@FindBy(xpath = ".//button[contains(text(),'View Campaigns')]")
	public WebElement Viewcampaign;
	
	
	
	public CRM_CampaignFinalandLunchUI(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	

}
