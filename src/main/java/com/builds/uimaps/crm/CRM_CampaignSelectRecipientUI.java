package com.builds.uimaps.crm;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CRM_CampaignSelectRecipientUI {

	@FindBy(xpath = ".//*[@type='button'][contains(text(),'Continue')]")
	public WebElement Continuebtn;
	
	@FindBy(xpath = ".//*[@type='button'][contains(text(),'Back')]")
	public WebElement Backbtn;
	
	@FindBy(id = "associateLater")
	public WebElement AssociateLater;
	
	@FindBy(id = "Contacts")
	public WebElement RecipientTypeContact;
	
	@FindBy(id = "Leads")
	public WebElement RecipientTypeLead;
	
	@FindBy(xpath = "//label[@for='group-segment']")
	public WebElement SendToRecipientGroup;
	
	@FindBy(xpath = "//label[@for='custom-segment']")
	public WebElement clickCreatebtn;
	
	@FindBy(xpath = ".//*[@data-role='ico_AddCircleOutline']")
	public WebElement SendToFilterRecipient;
	
	@FindBy(xpath = "//label[@for='Entire-list']")
	public WebElement SendToAllRecipient;
	
	@FindBy(id = "confirm")
	public WebElement confirmOfAssociateWithCampagin;
	
	
	public String getXpathOfCampaignAction(String campaignName)
	{
		return ".//a[.='"+campaignName+"']/ancestor::tr[1]//div[@type='button']";
	}
	
	public CRM_CampaignSelectRecipientUI(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	
}
