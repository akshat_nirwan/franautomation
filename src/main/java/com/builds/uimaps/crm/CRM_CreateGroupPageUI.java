package com.builds.uimaps.crm;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CRM_CreateGroupPageUI {
	
	@FindBy(xpath = "//div[@class='dropdown-action btn-style btn-lg no-arrow col-1of3']")
	public WebElement CreateGroupbtn;
	
	@FindBy(xpath = "//div[@id='newLayoutcolorbox']")
	public WebElement CreateGroupFrame;
	
	@FindBy(id = "groupName")
	public WebElement GroupName;
	
	@FindBy(id = "groupFor")
	public WebElement GroupType;
	
	@FindBy(id = "Accessibility")
	public WebElement GroupAccess;
	
	@FindBy(id = "savebtn")
	public WebElement savebtn;
	
	@FindBy(id = "saveText")
	public WebElement saveAndassociate;
	
	@FindBy(xpath = ".//div[@class = 'dialog-footer flex'][contains(text(),'Cancel')]")
	public WebElement cancelbtn;
	
	@FindBy(xpath = ".//*[@id ='groupDescription']")
	public WebElement Description;
	
	@FindBy(id = "groupDescriptionLink")
	public WebElement GroupDescriptionLink;
	
	@FindBy(id = "saveGroup")
	public WebElement Associatebtn_AddGroup;
	
	
	
	public CRM_CreateGroupPageUI(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	

}
