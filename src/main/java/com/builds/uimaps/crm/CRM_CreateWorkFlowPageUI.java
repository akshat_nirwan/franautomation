package com.builds.uimaps.crm;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CRM_CreateWorkFlowPageUI {
	
	@FindBy(xpath = ".//button[contains(text(),'Create Workflow')]")
	public WebElement Create_WorkFlowbtn ;
	
	@FindBy(id = "workFlowName")
	public WebElement WorkFlow_Name ;
	
	@FindBy(xpath = ".//input[@id='recordType_Lead']/ancestor::div[1]/label[1]")
	public WebElement WorkFlow_Lead ;
	
	@FindBy(xpath = ".//input[@id='recordType_Contact']/ancestor::div[1]/label[2]")
	public WebElement WorkFlow_Contact ;
	
	
	@FindBy(xpath = ".//button[contains(text(),'Cancel')]")
	public WebElement cancelbtn_onCreatePage ;
	
	@FindBy(xpath = ".//button[contains(text(),'Next')]")
	public WebElement Next_onCreatePage ;
	
	@FindBy(xpath = ".//p[contains(text(),'Standard')]")
	public WebElement Standard_radio ;
	
	@FindBy(xpath = ".//p[contains(text(),'Event')]")
	public WebElement Event_Radio ;
	
	@FindBy(xpath = ".//p[contains(text(),'Event')]")
	public WebElement Date_Radio ;
	
	
	//Standard WorkFlow Add page
	//Trigger portion
	@FindBy(xpath = "//label[@for='executeOnEvent1']")
	public WebElement Added_radio ;
	
	@FindBy(xpath = "//label[@for='executeOnEvent2']")
	public WebElement Modified_radio ;
	
	@FindBy(xpath = "//label[@for='executeOnEvent3']")
	public WebElement Add_Modified_radio ;
	
	@FindBy(xpath = "//label[@for='status']")
	public WebElement Active_inactivebtn ;
	
	@FindBy(id = "when-next")
	public WebElement Nextbtn_trigger ;
	
	//condition
	@FindBy(id = "criteria-radio-1")
	public WebElement AllLeads_radio ;
	
	@FindBy(id = "if-next")
	public WebElement Nextbtn_condition ;
	
	@FindBy(id = "criteria-radio-2")
	public WebElement Lead_Matching_radio ;
	
	@FindBy(xpath = "//span[@class='placeholder']")
	public WebElement filter_Leads ;
	
	@FindBy(xpath = "//div[@class='fc-drop-search']//input[@type='text']")
	public WebElement searchBar_infilter ;
	
	@FindBy(xpath = "//input[@type='button']")
	public WebElement Searchbtn;
	
	
	
	//action menu for choose option
	@FindBy(xpath = ".//div[contains(text(),'Action')]")
	public WebElement Condition_Actionbtn;
	
	@FindBy(id = "changeStatus")
	public WebElement Change_Status;
	
	@FindBy(id = "statusIdCombo")
	public WebElement Status_field;
	
	@FindBy(id = "addWorkFlowAction")
	public WebElement Save_onStatus;
	
	@FindBy(id = "closeOverlay")
	public WebElement Close_onStatus;
	
	
	@FindBy(id = "sendMail")
	public WebElement SendEmail;
	
	@FindBy(id = "task")
	public WebElement Task;
	
	@FindBy(id = "campaign")
	public WebElement Campaign;
	
	@FindBy(id = "createWorkFlow")
	public WebElement Workflow_addbtn;
	
	@FindBy(xpath = "//button[contains(text(),'Cancel')]")
	public WebElement Cancelbtn ;
	
	
	public CRM_CreateWorkFlowPageUI(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}


}
