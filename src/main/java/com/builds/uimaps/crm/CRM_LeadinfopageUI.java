package com.builds.uimaps.crm;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CRM_LeadinfopageUI {
	
		
	@FindBy(xpath = ".//a[contains(text(), 'Modify')]")
	public WebElement Modifybtn ;
	
	@FindBy(id = "deleteLead")
	public WebElement LogaTaskbtn ;
	
	@FindBy(xpath = "//a[@class='btm-menu-rgt-link'][contains(text(),'Send Email')]")
	public WebElement SendEmail ;
	
	@FindBy(id = "cmSendSms")
	public WebElement SendSMS ;
	
	@FindBy(xpath = "//a[@href='javascript:leadNextId()']")
	public WebElement Nextbutton ;
	
	@FindBy(xpath = "//a[@href='javascript:leadPrevId()']")
	public WebElement Previousbutton ;
	
	@FindBy(name = "changeStatusButton")
	public WebElement changestatusbutton ;
	
	@FindBy(name = "convertLead")
	public WebElement convertLead ;
	
	@FindBy(xpath = "//a[contains(text(),'Assign')]")
	public WebElement Assignbtn ;
	
	// field on Assign to popup 
	@FindBy(xpath = ".//select[@name ='contactOwnerID2']")
	public WebElement Assignpage_Onwerfield ;
	
	@FindBy(xpath = ".//*[@value = 'Assign']")
	public WebElement Assignpage_Assignbutton ;
	
	
	@FindBy(xpath = ".//input[@value = 'Close']")
	public WebElement Assignpage_closebtn ;
	
	@FindBy(xpath = "//a[contains(text(),'Associate With Campaign')]")
	public WebElement AssociateWithCampaign ;
	
	@FindBy(xpath = "//a[contains(text(),'Log a Task')]")
	public WebElement LogaTaskbottom ;
	
	@FindBy(xpath = "//a[contains(text(),'Export As Excel')]")
	public WebElement ExportAsExcel ;
	
	@FindBy(xpath = "//a[@href='javascript:void(0)'][contains(text(),'Print')]")
	public WebElement PrintConcentbtn ;
	
	// detail history page options
	@FindBy(xpath = ".//a[contains(text(),'Detailed History')]")
	public WebElement DetailedHistory ;
	
	@FindBy(xpath = ".//*[@id='table1']//a[contains(text(),'Owner Change')]")
	public WebElement DetailedHistory_Ownerchange ;
		
	@FindBy(xpath = ".//*[@id='table1']//a[contains(text(),'Status Change')]")
	public WebElement DetailedHistory_StatusChange ;
	
	
	
	
	
	
	
	
	
	
	
	
	@FindBy(xpath = "//html//tr[@height='30']//input[1]")
	public WebElement changeOwnerbottom ;
	
	@FindBy(xpath = ".//*[@name = 'addToGroup']")
	public WebElement addToGroupbtn ;
	
	@FindBy(xpath = ".//span[contains(text(),'Add to New Group')]")
	public WebElement AddToNewGroup;
	
	@FindBy(xpath = ".//*[@name = 'AssociateWithRegularCampaignButton']")
	public WebElement AssociateWithRegularCampaignButton ;
	
	@FindBy(xpath = ".//*[@value= 'Print']")
	public WebElement printbtnBottom ;
	
	@FindBy(id= "at")
	public WebElement LogAcall;
	
	@FindBy(id = "hm")
	public WebElement Addremark ;
	
	// Email Tab UI on lead info page
	
	@FindBy(xpath = ".//span[contains(text(),'Emails')]")
	public WebElement Emails_tab ;
	
	@FindBy(xpath = ".//*[@value = 'Back']")
	public WebElement Backbtn_Emails_tab ;
	
	@FindBy(xpath = ".//a[@class='text']")
	public WebElement sendEmail_Emails_tab ;
	
	@FindBy(id = "searchMail")
	public WebElement Searchbar_Emails_tab ;
	
	@FindBy(xpath = "//img[@onclick='javascript:searchMails();']")
	public WebElement Searchbtn_Emails_tab ;
	
	@FindBy(xpath = ".//img[@title = 'Reply']")
	public WebElement Reply_Emails_tab ;
	
	//SMS tab UI on lead info page
	@FindBy(xpath = "//a/span[contains(text(),'SMS')]")
	public WebElement SMS_tab ;
	
	@FindBy(xpath = ".//a[contains(text(),'Send SMS')]")
	public WebElement Send_SMSTab;
	
	@FindBy(xpath = ".//input[@value = 'Back']")
	public WebElement BackBtn_SMSTab ;
	
	@FindBy(xpath = "//input[@value = 'Close']")
	public WebElement Close_SMSDetailPopUP ;
	
	@FindBy(xpath = "//input[@value = 'Continue']")
	public WebElement Continue_SMSDetailPopUP ;

	//MOre Action Menu UI on Lead info page
	
	@FindBy(xpath = ".//a[@href='#'][contains(text(),'Actions')]")      //id = "actionListButtonsForCMLead
	public WebElement MoreActionMenu ;
	
	@FindBy(xpath = ".//div[@id='actionListButtonsForCMLead']//tr//tr[1]")
	public WebElement MoreActionMenu_changeOwner ;
	
	@FindBy(xpath = ".//div[@id='actionListButtonsForCMLead']//tr//tr[2]")
	public WebElement MoreActionMenu_DetailedHistory ;
	
	@FindBy(xpath = ".//div[@id='actionListButtonsForCMLead']//tr//tr[3]")
	public WebElement MoreActionMenu_Associatewithcampaign ;
	
	@FindBy(xpath = ".//div[@id='actionListButtonsForCMLead']//tr//tr[4]")
	public WebElement MoreActionMenu_ArchivedLead ;
	
	@FindBy(xpath = ".//div[@id='actionListButtonsForCMLead']//tr//tr[5]")
	public WebElement MoreActionMenu_DeleteLead ;
	
	@FindBy(xpath = ".//div[@id='actionListButtonsForCMLead']//tr//tr[6]")
	public WebElement MoreActionMenu_AddtoGroup ;
	
	
	@FindBy(id = "ActivityHis")
	public WebElement Activity_History ; 
	
	@FindBy(xpath = "//td[contains(text(),'Email Subscription')]/ancestor::tr[1]/td[2]")
	public WebElement Email_Subscription_Status ;
	
	
	//VipinG
	@FindBy(xpath = "//td/a[contains(text(),'Actions')]")
	public WebElement actionslead ;
	
	
	
	public CRM_LeadinfopageUI(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}


	
	

}
