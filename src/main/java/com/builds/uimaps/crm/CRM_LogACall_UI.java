package com.builds.uimaps.crm;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CRM_LogACall_UI {
	
	
	
	@FindBy(id= "callSubject")
	public WebElement callsubject;
	
	@FindBy(id= "callDate")
	public WebElement Calltime;
	
	@FindBy(id= "time-ui-datepicker-div")
	public WebElement timeiframe;
	
	@FindBy(xpath= "//button[@type='button'][contains(text(),'Done')]")  //button[@type='button'][contains(text(),'Now')]
	public WebElement donebtnontime;
	
	@FindBy(id= "callStatus")
	public WebElement Status;
	
	@FindBy(id= "callType")
	public WebElement Type;
	
	@FindBy(id= "comments")
	public WebElement commnet;
	
	@FindBy(name = "add223")
	public WebElement ResetButton;
	
	@FindBy(id= "add222")
	public WebElement save;
	
	
	
	public CRM_LogACall_UI(WebDriver driver)
	{
		PageFactory.initElements(driver, this);
	}
	
	
	

}
