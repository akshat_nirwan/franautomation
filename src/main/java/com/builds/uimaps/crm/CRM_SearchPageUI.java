package com.builds.uimaps.crm;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CRM_SearchPageUI {
	
	@FindBy(xpath = "//span[@style='font-size:12px'][contains(text(),'Leads')]")
	public WebElement LeadTab ;
	
	@FindBy(xpath = ".//span[contains(text(),'Contacts')]")
	public WebElement ContactTab ;

	@FindBy(xpath = ".//input[@name = 'archivedleads'][@value ='yes']")
	public WebElement ArchivedLeadRadio ;
	
	@FindBy(xpath = "//input[@type='submit']")
	public WebElement SubmitBtn ;
	
	@FindBy(xpath = "//a[contains(text(),'')]/ancestor::tr[1]/td//input[@type = 'checkbox']")
	public WebElement ArchivedLead ;
	
	
	
	public CRM_SearchPageUI(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	
	
}
