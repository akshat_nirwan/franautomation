package com.builds.uimaps.crm;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CRM_SelectaAndCustomizeTemplateUI {
	
	@FindBy(xpath = "//div[@class='svg-icon cursor-pointer']")
	public WebElement Editcampaign_name;
	
	@FindBy(xpath = ".//iframe[@class = 'newLayoutcboxIframe']")
	public WebElement CreateCampaign_frame;
	
	@FindBy(xpath = "//ul[@id='TemplateSlider']/li[1]")
	public WebElement firstTemplate;
	
	@FindBy(xpath = "//ul[@id='TemplateSlider']/li[1]//a[contains(text(),' Customize ')]")
	public WebElement firstTemplate_customizebtn;
	
	public CRM_SelectaAndCustomizeTemplateUI(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	
}
