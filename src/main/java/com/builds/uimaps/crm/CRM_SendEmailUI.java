package com.builds.uimaps.crm;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CRM_SendEmailUI {
	
	@FindBy(name="subject")
	public WebElement EmailSubject;   
	
	@FindBy(id ="mailCC")
	public WebElement CCbox;
		
	@FindBy(xpath =".//input[@class = 'cm_new_button'][@value = 'Cancel']")
	public WebElement cancelbtn;
	
	@FindBy(xpath =".//input[@class = 'cm_new_button'][@value = 'Reset']")
	public WebElement Resetbtn;
	
	@FindBy(id="sendButton2")
	public WebElement sendbtn;

	@FindBy(id="ta_ifr")
	public WebElement frame;
	
	@FindBy(xpath=".//body[@id = 'tinymce']/p")
	public WebElement textfield;
	
	@FindBy(xpath=".//span/input[@value = '2']")
	public WebElement CustomRadio;
	
	@FindBy(xpath="//span[@class='bText12']//input[@type='text']")
	public WebElement Customtext;
	
	@FindBy(id ="mailTemplateID")
	public WebElement TemplateEmail;
	
	@FindBy(id ="defaultFlag")
	public WebElement SignatureRadio;
	
	@FindBy(xpath =".//td[contains(text(),'Attachment :')]")
	public WebElement AttachmentBtn;
	
	@FindBy(xpath =".//input[@name = 'attachmentName']")
	public WebElement choosefile;
	
	@FindBy(xpath =".//input[@value = 'Attach']")
	public WebElement Attachbtn;
	
	@FindBy(xpath =".//input[@value = 'Done']")
	public WebElement Done;
	
	@FindBy(xpath =".//a[text()='Add Signature']")
	public WebElement add_signature_btn;
	
	@FindBy(xpath =".//input[@name='signatureName']")
	public WebElement Signature_Name;
	
	@FindBy(xpath =".//input[@value='Add Signature']")
	public WebElement Add_signaturebtn;
	
	@FindBy(xpath =".//input[@value='Cancel']")
	public WebElement Cancelbtn_onsignature;
	
	@FindBy(id ="ta_ifr")
	public WebElement Signature_textAreaframe;
	
	@FindBy(id ="ta")
	public WebElement Signature_textArea;
	
	@FindBy(xpath =".//input[@name='defaultsginature']")
	public WebElement SetAsDefualt;
	
	@FindBy(xpath ="//button[@id='keywords-button']//i[@class='mce-ico mce-i-none']")
	public WebElement keyWord;
	
	@FindBy(id ="select")
	public WebElement Availabe_keyword;
	
	@FindBy(xpath ="//select[@id='select']//option[text()='First Name']")
	public WebElement firstname_keyword;
	
	
	public CRM_SendEmailUI(WebDriver driver)
	{
		PageFactory.initElements(driver, this);
	}
	
	
}
