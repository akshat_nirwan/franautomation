package com.builds.uimaps.crm;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CRM_TemplateInfoPageUI {
	
	@FindBy(id = "TemplateInfo-dialog")
	public WebElement TemplateInfo_Frame;
	
	@FindBy(id = "mailTitle")
	public WebElement TemaplateName;
	
	@FindBy(id = "mailSubject")
	public WebElement Templatesubject;
	
	@FindBy(id = "Accessibility")
	public WebElement TemplateAccess;
	
	@FindBy(id = "folderNo")
	public WebElement TemplateFolder;
	
	@FindBy(xpath = "//label[@for='MarkFavourite']")
	public WebElement MakeasFavorite;
	
	@FindBy(xpath = "//label[@for='MarkRecomended']")
	public WebElement MarkasRecomended;
	
	@FindBy(xpath= ".//*[@class = 'btn-style link-btn dialog-close-icon'][contains(text(),'Cancel ')]")
	public WebElement Cancelbtn;
	
	@FindBy(xpath = "//div[@class='dialog-footer flex']//button[@class='btn-style'][contains(text(),'Done ')]")
	public WebElement Savebtn;
	
	@FindBy(id = "associateTemplate")
	public WebElement SaveAndcontinue;
	
	@FindBy(id = "save")
	public WebElement Save;
	
	@FindBy(xpath = ".//span[contains(text(),'Cancel')]")
	public WebElement cancel;
	
	@FindBy(id = "OpenPreviewDialog")
	public WebElement PreviewAndTest;
	
	@FindBy(id = "TemplateInfo-dialogLink")
	public WebElement TemplateInfo;
	
	
	public CRM_TemplateInfoPageUI(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
