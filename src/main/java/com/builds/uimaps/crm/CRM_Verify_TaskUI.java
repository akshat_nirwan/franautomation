package com.builds.uimaps.crm;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

//VipinG
public class CRM_Verify_TaskUI {
	
	@FindBy(xpath="//*[@id='dropdown']/span/a")
	public WebElement User;
	
	@FindBy(xpath="//a[contains(text(),'Options')]")
	public WebElement Options;
	
	@FindBy(xpath="//td[contains(text(),'More')]")
	public WebElement More;
	
	@FindBy(xpath="//*[@qat_tabname='Notifications']")
	public WebElement Notifications;
	
	@FindBy(xpath="//*[@name='notifyFlag_16']")
	public WebElement testcreationcheckbox;
	
	@FindBy(xpath="//*[@value='Save']")
	public WebElement save;
	
	@FindBy(xpath="//*[@id='showBar']")
	public WebElement arrow;
	
	@FindBy(xpath="//a//span[contains(text(),'Week')]")
	public WebElement Week;
	
	@FindBy(xpath="//a//span[contains(text(),'Day')]")
	public WebElement Day;
	
	@FindBy(xpath="//a//span[contains(text(),'Month')]")
	public WebElement Month;
	
	@FindBy(xpath=".//*[@id='searchSystem']")
	public WebElement taskSearch;
	
	@FindBy(xpath=".//a[contains(text(),'Activity Reports')]")
	public WebElement activityReports;
	
	@FindBy(xpath="//*[@id='topDiv']//ul/li/a[contains(text(),'Task History')]")
	public WebElement taskHistory;
	
	@FindBy(xpath="//*[@id='ms-parentownerId']//span[@class='placeholder']")
	public WebElement assignedToReports;
	
	@FindBy(xpath="//*[@value='View Report']")
	public WebElement viewReport;
	
	@FindBy(xpath="//*[@id='ms-parentownerId']/div/div[1]/input[1]")
	public WebElement typeandEnter;
	
	@FindBy(xpath="//*[@id='ms-parentownerId']/div/div[1]/input[2]")
	public WebElement typeandEnterSearch;
	
	@FindBy(xpath="//div[@id='ms-parentownerId']//*[@id='selectAll']")
	public WebElement selectAll;
	
	@FindBy(xpath="//*[@name='close']")
	public WebElement close;
	
	@FindBy(xpath="//*[@id='siteMainTable']/tbody/tr/td/table/tbody/tr[6]/td/table/tbody/tr[1]/td/table/tbody/tr[2]/td/table/tbody/tr[2]/td[5]")
	public WebElement taskStatus;
	
	@FindBy(xpath="//*[@name='CalendarUser']")
	public WebElement calendarmultiselectbox;
	
	public CRM_Verify_TaskUI(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	
}
