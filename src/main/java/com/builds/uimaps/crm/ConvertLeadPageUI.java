package com.builds.uimaps.crm;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ConvertLeadPageUI {
	
	@FindBy(id="contactFirstName")
	public WebElement FirstName;
	
	@FindBy(id="contactLastName")
	public WebElement LastName;
	
	@FindBy(id="contactType")
	public WebElement contactType;
	
	@FindBy(id="leadStatus")
	public WebElement Contactstatus;
	
	@FindBy(xpath =".//input[@name = 'contactOption'][@value = 'M']")
	public WebElement AssociateWithMatchingcontact;
	
	@FindBy(id= "leadAssignTo")
	public WebElement AssignTo;
	
	@FindBy(xpath =".//input[@name= 'userOption'][@value = '1']")
	public WebElement OtherUser;
	
	@FindBy(xpath =".//*[@value = 'E']")
	public WebElement ExistingAccount;
	
	@FindBy(id ="accountNameSearch")
	public WebElement Accountsearch;
			
	@FindBy(id="accountName")
	public WebElement AccountName;
	
	@FindBy(id="isOpportunity")
	public WebElement opportunityCreate;
	
	@FindBy(id="opportunityName")
	public WebElement OpportunityName;
	
	@FindBy(id="stage")
	public WebElement Stage;
	
	@FindBy(id="salesAmount")
	public WebElement SalesAmaount;
	
	@FindBy(id="closerDate")
	public WebElement CloserDate;
	
	@FindBy(id="opportunitySource")
	public WebElement OpportunitySource;
	
	@FindBy(id="btnConvert")  
	public WebElement Convertbtn;
	
	@FindBy(id="btnCancel")
	public WebElement Cancelbtn;
	
	@FindBy(xpath = ".//span[contains(text(),'All Selected')]")
	public WebElement Grouptab ;
	
	@FindBy(xpath = "//div[@class='ms-search']//input[@type='text']")
	public WebElement Searchtab_insideGroup ;
	
	@FindBy(xpath = "//div[@class='ms-search']//input[@type='button']")
	public WebElement Searchbtn_insideGroup ;
	
	@FindBy(xpath = "//input[@id='selectItemgroupId']")
	public WebElement checkbox_insideGroup ;

	public ConvertLeadPageUI(WebDriver driver)
	{
		PageFactory.initElements(driver, this);
	}
	

}
