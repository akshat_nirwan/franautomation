package com.builds.uimaps.crm;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePageUI {
	
	@FindBy(xpath="//*[contains(text(),'FranConnect Administrator')]")
	public WebElement FranconnectAdministrator;
	
	@FindBy(xpath="//*[text()='Admin']")
	public WebElement Admin;
	
	public HomePageUI(WebDriver driver)
	{
		PageFactory.initElements(driver,this);
	}
}
