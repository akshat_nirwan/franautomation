package com.builds.uimaps.crm;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LeadUI {
	
	@FindBy(xpath="//span[@class='hText16black'][contains(text(),'Lead Information')]")
	public WebElement leadsummaryverify;
    
		
	@FindBy(id= "title")
	public WebElement title;
	
	@FindBy(id= "leadFirstName")
	public WebElement firstname;
	
	@FindBy(id = "leadLastName")
	public WebElement lastname;
	
	@FindBy(id="primaryContactMethod")
	public WebElement primarycontact;
	
	@FindBy(id="companyName")
	public WebElement compnayname;
	
	@FindBy(id="ownerType")
	public WebElement assign;

	@FindBy(name ="leadOwnerID2")
	public WebElement owner;
	
	@FindBy(id="address")
	public WebElement address;
	
	@FindBy(id="city")
	public WebElement city;
	
	@FindBy(id="country")
	public WebElement country;
	
	@FindBy(id="zipcode")
	public WebElement zipcode;
	
	@FindBy(id="state")
	public WebElement state;
	
	@FindBy(id="phoneNumbers")
	public WebElement phonenumber;
	
	@FindBy(id="extn")
	public WebElement extension;
	
	@FindBy(id="faxNumbers")
	public WebElement fax;
	
	@FindBy(id="mobileNumbers")
	public WebElement mobile;
	
	@FindBy(id ="emailIds")
	public WebElement email;
	
	@FindBy(id="alternateEmail")
	public WebElement alternarteemail;
	
	@FindBy(id="suffix")
	public WebElement suffix;
	
	@FindBy(id="position")
	public WebElement jobtitle;
	
	@FindBy(id="birthdate")
	public WebElement birthday;
	
	@FindBy(id="anniversarydate")
	public WebElement anniversary;
	
	@FindBy(id="cmLeadTypeID")
	public WebElement LeadType;
	
	@FindBy(id="cmRatingID")
	public WebElement Rating;
	
	@FindBy(id="cmSource2ID")
	public WebElement source;
	
	@FindBy(id="cmSource3ID")
	public WebElement source1;
	
	@FindBy(id="cmSource1ID")
	public WebElement contactMedium;
	
	@FindBy(id="bestTimeToContact")
	public WebElement Besttime;
	
	//group tag options
	@FindBy(xpath="//div[@id='ms-parentgroupId']//span[@class='placeholder']")
	public WebElement Group;
	
	@FindBy(xpath="//div[@class='ms-search']//input[@type='text']")
	public WebElement Searchtab_insideGroup;
	
	@FindBy(xpath="//div[@class='ms-search']//input[@type='button']")
	public WebElement Searchbtn_insideGroup;
	
	@FindBy(xpath=".//input[@id='selectAll']")
	public WebElement checkbox_insideGroup;
	
	@FindBy(id="comments")
	public WebElement comment;	
	
	@FindBy(id="Submit")
	public WebElement submit;
		
	@FindBy(id="resetButton")
	public WebElement Reset;
	
	@FindBy(name ="back")
	public WebElement Backbutton;
		
	@FindBy(id ="convertLead")
	public WebElement SaveAndConvertLead;
	
	@FindBy(id ="addanotherlead")
	public WebElement Save_And_Add_Another_Lead;
	
	//subscription radio
	
	@FindBy(id ="inMailingList1")
	public WebElement opt_in;
	
	@FindBy(id ="inMailingList0")
	public WebElement opt_out;
	
	@FindBy(id ="inMailingList2")
	public WebElement opt_in_pending;
	
	

	public LeadUI(WebDriver driver)
	{
		PageFactory.initElements(driver, this);
	}
	
}
