package com.builds.uimaps.crm;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LeadsummarypageUI {
	
	@FindBy(id="add_new")
	public WebElement addNew;
	
	@FindBy(xpath=".//td[@class='vp5  fline']/a[.='Lead']")
	public WebElement lead;
	
	@FindBy(id ="showFilter")
	public WebElement ShowFilter;
	
	@FindBy(id ="hideFilter")
	public WebElement HideFilter;
	
	//Action menu all links 
	@FindBy(xpath="//a[@href='#'][contains(text(),'Actions')]")
	public WebElement Actionbtn;
	
	
	@FindBy(xpath="//a[@href='javascript:void(0);'][contains(text(),'More Actions')]")
	public WebElement MoreActionbtn;
	
	@FindBy(name = "taskButton")
	public WebElement log_aTask;
	
	@FindBy(name = "sendMail2")
	public WebElement SendEmail;
	
	@FindBy(name = "AssociateWithCampaignButton")
	public WebElement AssociateWithCampaign;
	
	@FindBy(xpath = ".//input[@value='Change Status']")
	public WebElement changeStatus;
	
	@FindBy(name = ".//input[@value='Merge Leads']")
	public WebElement MergeLead;
	
	
	//lead filter all links

	@FindBy(id ="viewBy")
	public WebElement View_leads;
	
	@FindBy(id ="ms-parentleadDuplicateCriteria")
	public WebElement Duplicate_criteria;
	
	@FindBy(xpath =".//input[@value='LEAD_FIRST_NAME']")
	public WebElement Duplicate_criteria_firstName;
	
	@FindBy(id ="ms-parentleadOwner")
	public WebElement Lead_ownerr;
	
	@FindBy(id ="ms-parentdivisionID")
	public WebElement Brand;
	
	@FindBy(id ="ms-parentfCombo")
	public WebElement FranchiseId;
	
	@FindBy(id ="ms-parentleadStatus")
	public WebElement Status;
	
	@FindBy(id ="matchType")
	public WebElement Add_Date;
	
	@FindBy(id ="ms-parentemailStatus")
	public WebElement Email_Status;
	
	@FindBy(id ="search")
	public WebElement Search_btn;
	
	@FindBy(id ="saveView")
	public WebElement Save_View;
	
	public LeadsummarypageUI(WebDriver driver)
	{
		PageFactory.initElements(driver, this);
	}

}
