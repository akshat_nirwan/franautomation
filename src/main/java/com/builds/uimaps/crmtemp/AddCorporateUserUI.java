package com.builds.uimaps.crmtemp;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AddCorporateUserUI {

	@FindBy(xpath="//*[@value='Add Corporate User']")
	public WebElement addcorporateuser;
	
	public AddCorporateUserUI(WebDriver driver)
	{
		PageFactory.initElements(driver,this);
	}
	
	
}
