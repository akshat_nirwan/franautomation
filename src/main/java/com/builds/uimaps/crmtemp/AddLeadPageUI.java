package com.builds.uimaps.crmtemp;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AddLeadPageUI {

	@FindBy(xpath="//*[@id='Submit']")
	public WebElement save;
	
	@FindBy(xpath="//*[@id='title']")
	public WebElement title;
	
	@FindBy(xpath="//*[@id='leadFirstName']")
	public WebElement leadFirstName;
	
	@FindBy(xpath="//*[@id='leadLastName']")
	public WebElement leadLastName;
	
	@FindBy(xpath="//*[@id='primaryContactMethod']")
	public WebElement primaryContactMethod;
	
	@FindBy(xpath="//*[@id='companyName']")
	public WebElement companyName;
	
	@FindBy(xpath="//*[@id='ownerType' and @value='C']")
	public WebElement Corporate;
	
	@FindBy(xpath="//*[@id='ownerType' and @value='R']")
	public WebElement Regional;
	
	@FindBy(xpath="//*[@name='leadOwnerID2']")
	public WebElement CorporateUser;
	
	@FindBy(xpath="//*[@id='faxNumbers']")
	public WebElement faxNumbers;
	
	@FindBy(xpath="//*[@id='emailIds']")
	public WebElement emailIds;
	
	public AddLeadPageUI(WebDriver driver) {
		PageFactory.initElements(driver,this);
	}
	
}
