package com.builds.uimaps.crmtemp;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CRMModuleUI {

	@FindBy(xpath="//a[contains(text(),'Leads')]")
	public WebElement ClickOnLead;
	
	public CRMModuleUI(WebDriver driver)
	{
		PageFactory.initElements(driver,this);
	}
	
}
