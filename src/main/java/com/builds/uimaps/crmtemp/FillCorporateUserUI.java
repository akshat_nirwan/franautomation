package com.builds.uimaps.crmtemp;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FillCorporateUserUI {

	@FindBy(xpath="//*[@id='userName']")
	public WebElement userName;
	
	@FindBy(xpath="//*[@id='password' and @type='password']")
	public WebElement password;
	
	@FindBy(xpath="//*[@id='confirmPassword' and @type='password']")
	public WebElement confirmPassword;
	
	//group tag options
	@FindBy(xpath="//span[@class='placeholder']")
	public WebElement Group;
		
	@FindBy(xpath="//div[@class='ms-search']//input[@type='text']")
	public WebElement Searchtab_insideGroup;
		
	@FindBy(xpath="//div[@class='ms-search']//input[@type='button']")
	public WebElement Searchbtn_insideGroup;
		
	@FindBy(xpath=".//input[@id='selectAll']")
	public WebElement checkbox_insideGroup;
	
	@FindBy(xpath="//*[@id='timezone']")
	public WebElement timezone;
	
	@FindBy(xpath="//*[@id='firstName']")
	public WebElement firstName;
	
	@FindBy(xpath="//*[@id='lastName']")
	public WebElement lastName;
	
	@FindBy(xpath="//*[@id='city']")
	public WebElement city;
	
	@FindBy(xpath="//*[@id='state']")
	public WebElement state;
	
	@FindBy(xpath="//*[@id='phone1']")
	public WebElement phone1;
	
	@FindBy(xpath="//*[@id='email']")
	public WebElement email;
	
	@FindBy(xpath="//*[@id='Submit']")
	public WebElement Submit;
	
	
	public FillCorporateUserUI(WebDriver driver)
	{
		PageFactory.initElements(driver,this);
	}
	
	
}
