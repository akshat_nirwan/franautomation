package com.builds.uimaps.crmtemp;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LeadInfoPageUI {

	@FindBy(xpath="//*[contains(text(),'Lead is added through FranConnect Application.')]")
	public WebElement ActivityHistory;
	
	@FindBy(xpath="//*[contains(text(),'First Name :')]//following::td[1]")
	public WebElement FirstNameText;
	
	@FindBy(xpath="//*[contains(text(),'First Name :')]//following::td[3]")
	public WebElement LastNameText;
	
	
	public LeadInfoPageUI(WebDriver driver) {
		PageFactory.initElements(driver,this);
	}
}
