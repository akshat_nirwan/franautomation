package com.builds.uimaps.crmtemp;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LeadSummaryUI {

	@FindBy(xpath="//span[contains(text(),'Add New')]")
	public WebElement addnew;
	
	public LeadSummaryUI(WebDriver driver)
	{
		PageFactory.initElements(driver,this);
	}
}
