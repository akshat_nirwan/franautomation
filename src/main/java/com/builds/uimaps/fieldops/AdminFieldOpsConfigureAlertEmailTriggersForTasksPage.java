package com.builds.uimaps.fieldops;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminFieldOpsConfigureAlertEmailTriggersForTasksPage {

	@FindBy(linkText = "Add Alert")
	public WebElement addAlert ;

	@FindBy(id = "fromEmail")
	public WebElement fromEmail;

	@FindBy(id = "frequencyAlertDays")
	public WebElement frequencyAlertDays;

	@FindBy(id = "criticalLevel")
	public WebElement criticalLevel;

	@FindBy(id = "alertEmailSubject")
	public WebElement alertEmailSubject;

	@FindBy(id = "emailContent")
	public WebElement emailContent;

	@FindBy(id = "ms-parentusers")
	public WebElement selectMultiBtn ;

	@FindBy(id = "saveButton")
	public WebElement saveBtn;

	@FindBy(xpath = "//input[@name='close']")
	public WebElement closeBtn ;

	@FindBy(xpath = ".//a[.='Show All']")
	public WebElement showAllLink ;

	public AdminFieldOpsConfigureAlertEmailTriggersForTasksPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
