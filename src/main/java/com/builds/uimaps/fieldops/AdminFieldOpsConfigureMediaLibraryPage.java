package com.builds.uimaps.fieldops;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminFieldOpsConfigureMediaLibraryPage {

	@FindBy(linkText = "Add Media")
	public WebElement addMedia ;

	@FindBy(linkText = "Manage Media Folders")
	public WebElement manageMediaFolder ;

	@FindBy(linkText = "Add Media Folder")
	public WebElement addMediaFolder ;

	@FindBy(xpath = ".//input[@name='folderName']")
	public WebElement folderName ;

	@FindBy(xpath = ".//textarea[@name='folderSummary']")
	public WebElement folderSummary ;

	@FindBy(id = "submitButton")
	public WebElement submitBtn;

	// Add Media
	@FindBy(id = "folderNo")
	public WebElement selectFolder;

	@FindBy(id = "mediaTitle")
	public WebElement mediaTitle;

	@FindBy(id = "description")
	public WebElement mediaDescription;

	@FindBy(id = "mediaName")
	public WebElement uploadMediaName;

	@FindBy(id = "submitBtn")
	public WebElement addBtn;

	@FindBy(xpath = "html/body/table/tbody/tr[2]/td[2]/table[1]/tbody/tr[2]/td/table/tbody/tr[4]/td/input")
	public WebElement closeBtn ;

	@FindBy(xpath = ".//input[@value='Back']")
	public WebElement backBtn ;

	@FindBy(id = "searchTemplateImage")
	public WebElement searchByMediaName;

	@FindBy(id = "search")
	public WebElement searchBtn;

	@FindBy(xpath = ".//tr/td/a/img[@title='Modify Media']")
	public WebElement modifyMedia ;

	@FindBy(xpath = ".//tr/td/a/img[@title='Delete Media']")
	public WebElement deleteMedia ;

	@FindBy(id = "mediaFolder")
	public WebElement searchMediaFolder;

	@FindBy(id = "heading")
	public WebElement mediaFolderNameHeading;

	public AdminFieldOpsConfigureMediaLibraryPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
