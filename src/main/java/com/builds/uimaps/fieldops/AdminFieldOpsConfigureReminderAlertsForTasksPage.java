package com.builds.uimaps.fieldops;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminFieldOpsConfigureReminderAlertsForTasksPage {

	@FindBy(id = "reminderConf")
	public List<WebElement> reminderType;

	@FindBy(id = "addToCc")
	public List<WebElement> SendAlertMailAssignor;

	@FindBy(xpath = ".//input[@name='alertDaysPrior']")
	public WebElement alertDaysPrior ;

	@FindBy(xpath = ".//input[@name='frequencyAlertDays']")
	public WebElement frequencyAlertDays ;

	@FindBy(name = "fromEmail")
	public WebElement fromEmail;

	@FindBy(name = "emailContent")
	public WebElement StartAlertEmailContent;

	@FindBy(name = "overdueEmailContent")
	public WebElement overdueEmailContent;

	@FindBy(name = "emailContentUnassigned")
	public WebElement emailContentUnassigned;

	@FindBy(name = "overdueEmailContentUnassigned")
	public WebElement overdueEmailContentUnassigned;

	@FindBy(xpath = ".//input[@value='Submit']")
	public WebElement submit ;

	public AdminFieldOpsConfigureReminderAlertsForTasksPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
