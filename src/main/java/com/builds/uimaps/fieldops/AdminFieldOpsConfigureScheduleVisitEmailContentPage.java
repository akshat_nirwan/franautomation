package com.builds.uimaps.fieldops;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminFieldOpsConfigureScheduleVisitEmailContentPage {

	@FindBy(name = "showColorBox")
	public List<WebElement> showPopupEachVisit;

	@FindBy(xpath = ".//input[@name='subject']")
	public WebElement subject ;

	@FindBy(xpath = ".//*[@id='tinymce']/p")
	public WebElement emailContent ;

	@FindBy(id = "attachAuditForm1")
	public WebElement attachVisitForm1;

	@FindBy(xpath = ".//input[@value='Configure' and @name='Submit']")
	public WebElement configureBtn ;

	public AdminFieldOpsConfigureScheduleVisitEmailContentPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
