package com.builds.uimaps.fieldops;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminFieldOpsConfigureScorePage {

	@FindBy(id = "highScore1")
	public WebElement highScore1;

	@FindBy(id = "highScore2")
	public WebElement highScore2;

	@FindBy(id = "highScore3")
	public WebElement highScore3;

	@FindBy(name = "grade1")
	public WebElement grade1;

	@FindBy(name = "grade2")
	public WebElement grade2;

	@FindBy(name = "grade3")
	public WebElement grade3;

	@FindBy(name = "label1")
	public WebElement label1;

	@FindBy(name = "label2")
	public WebElement label2;

	@FindBy(name = "label3")
	public WebElement label3;

	@FindBy(id = "addNewImage")
	public WebElement addNewImage;

	@FindBy(xpath = ".//input[@value='OK']")
	public WebElement okBtn ;

	@FindBy(xpath = ".//input[@value='Save']")
	public WebElement saveBtn ;

	@FindBy(xpath = ".//*[@id='showGrade' and @value='Y']")
	public WebElement gradeOptions ;

	@FindBy(xpath = ".//*[@id='showLabel' and @value='Y']")
	public WebElement labelOptionsYes ;

	@FindBy(xpath = ".//*[@id='showLabel' and @value='N']")
	public WebElement labelOptionsNo ;

	@FindBy(xpath = ".//*[@id='showGrade' and @value='N']")
	public WebElement percenrageOptions ;

	@FindBy(xpath = ".//*[@id='showGrade' and @value='S']")
	public WebElement scoreOptions ;

	public AdminFieldOpsConfigureScorePage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
