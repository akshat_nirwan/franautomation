package com.builds.uimaps.fieldops;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminFieldOpsDeleteFieldOpsVisitsPage {

	@FindBy(xpath = ".//*[@id='ms-parentfranchiseeNo']")
	public WebElement franchiseIDSelectBtn ;

	@FindBy(xpath = ".//*[@id='ms-parentfranchiseeNo']//input[@id='selectAll']")
	public WebElement franchiseIDselectAll ;

	@FindBy(xpath = ".//*[@id='ms-parentfranchiseeNo']/div/div/input")
	public WebElement franchaseIDTxBx ;

	@FindBy(id = "visitNumber")
	public WebElement visitNumber;

	@FindBy(id = "allSelect")
	public WebElement allSelect;

	@FindBy(xpath = ".//input[@value='View' and @type='button']")
	public WebElement viewBtn ;

	@FindBy(xpath = ".//input[@value='Delete']")
	public WebElement deleteBtn ;

	@FindBy(xpath = ".//*[@id='addquestionlink']/span[contains(text () , 'Add Question')]")
	public WebElement addQuestion ;

	@FindBy(xpath = ".//input[@value='Finish']")
	public WebElement finishBtn ;

	// save view
	@FindBy(xpath = ".//*[@id='ms-parentmasterVisitId']")
	public WebElement visitFormSelectBtn ;

	@FindBy(xpath = ".//*[@id='ms-parentmasterVisitId']/div/div/input")
	public WebElement visitFormTxBx ;

	@FindBy(xpath = ".//*[@id='ms-parentmasterVisitId']//input[@id='selectAll']")
	public WebElement visitFormSelectAll ;

	@FindBy(xpath = ".//*[@id='ms-parentassignTo']")
	public WebElement consultantSelectBtn ;

	@FindBy(xpath = ".//*[@id='ms-parentassignTo']/div/div/input")
	public WebElement consultantTxBx ;

	@FindBy(xpath = ".//*[@id='ms-parentassignTo']//input[@id='selectAll']")
	public WebElement consultantSelectAll ;

	@FindBy(xpath = ".//*[@id='ms-parentvisitStatus']")
	public WebElement statusSelectBtn ;

	@FindBy(xpath = ".//*[@id='ms-parentvisitStatus']/div/div/input")
	public WebElement statusTxBx ;

	@FindBy(xpath = ".//*[@id='ms-parentvisitStatus']//input[@id='selectAll']")
	public WebElement statusSelectAll ;

	@FindBy(xpath = ".//*[@id='ms-parentcomboDivision']")
	public WebElement divisionSelectDropDown ;

	@FindBy(xpath = ".//input[@value='Save View']")
	public WebElement saveViewBtn ;

	// default set filter
	@FindBy(xpath = ".//*[@id='ms-parentmasterVisitId']")
	public WebElement visitFormSelect ;

	@FindBy(xpath = ".//*[@id='ms-parentfranchiseeNo']")
	public WebElement franchiseIdSelect ;

	@FindBy(xpath = ".//*[@id='ms-parentassignTo']")
	public WebElement consultantSelect ;

	@FindBy(xpath = ".//*[@id='ms-parentvisitStatus']")
	public WebElement statusSelect ;

	@FindBy(xpath = ".//*[@id='ms-parentcomboDivision']")
	public WebElement divisionSelect ;

	public AdminFieldOpsDeleteFieldOpsVisitsPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
