package com.builds.uimaps.fieldops;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminFieldOpsManageConsultantPage {

	@FindBy(xpath = ".//*[@id='optMoveRight']/input")
	public WebElement optMoveRight ;

	@FindBy(xpath = ".//*[@id='optMoveLeft']/input")
	public WebElement optMoveLeft ;

	public AdminFieldOpsManageConsultantPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
