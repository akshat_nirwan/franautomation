package com.builds.uimaps.fieldops;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminFieldOpsManageVisitFormCustomizeVisitFormPage {

	// Add Question
	// @FindBy(xpath=".//*[@id='addquestionlink']/span[contains(text () , 'Add
	// Question')]")
	@FindBy(xpath = ".//*[@id='addquestionlink']")
	public WebElement addQuestion ;

	@FindBy(id = "qType")
	public List<WebElement> addQuestionNewOrLibraray;

	@FindBy(xpath = ".//textarea[@name='question']")
	public WebElement questionTextArea ;

	@FindBy(xpath = "//textarea[@name='questionDescription']")
	public WebElement helpTextForUser ;

	@FindBy(id = "questionMandantory")
	public WebElement questionMandantory;

	@FindBy(id = "criticalLevel")
	public WebElement criticalLevel;

	@FindBy(id = "nextQuestionButton")
	public WebElement nextBtn;

	@FindBy(xpath = ".//*[@id='responseDivDisableData']//select[@name='responseType']")
	public WebElement selectResponseType ;

	@FindBy(id = "withComments")
	public WebElement userComments;

	@FindBy(id = "withAttachments")
	public WebElement allowAttachments;

	@FindBy(id = "submitButton")
	public WebElement addBtn;

	@FindBy(xpath = ".//div[@id='libQuetion']//input[@id='submitButton']")
	public WebElement addBtnQstnLib ;

	// Modify Question

	@FindBy(xpath = ".//*[@id='modify_0']/img")
	public WebElement modifyBtn ;

	@FindBy(id = "submitButton")
	public WebElement saveBtn;

	// Delete Question
	@FindBy(xpath = ".//*[@id='delete_0']/img")
	public WebElement deleteBtn ;

	// Add Section

	@FindBy(xpath = ".//*[@id='addsectionlink']/span[contains(text () , 'Add Section')]")
	public WebElement addSection ;

	@FindBy(name = "sectionValue")
	public WebElement sectionName;

	// Modify Section
	@FindBy(xpath = "//a/span[contains(text () , 'Modify Section')]")
	public WebElement modifySection ;

	// Delete Section
	@FindBy(xpath = ".//a/span[contains(text () , 'Delete Section')]")
	public WebElement deleteSection ;

	// Add Question into a section
	@FindBy(xpath = ".//*[@id='addqueslink']/span[contains(text () , 'Add Question')]")
	public WebElement addQuestionInSection ;

	// preview form
	@FindBy(xpath = ".//*[@id='previewlink']/span[contains(text () , 'Preview Form')]")
	public WebElement previewForm ;

	// Add a Tab
	@FindBy(xpath = ".//*[@id='addtablink']/span[contains(text () , 'Add Tab')]")
	public WebElement addTab ;

	@FindBy(name = "sectionValue")
	public WebElement tabName;

	// modify Tab
	@FindBy(xpath = "//td/a[@title='Modify']")
	public WebElement modifyTab ;

	// deleteTab
	@FindBy(xpath = "//td/a[@title='Delete']")
	public WebElement deleteTab ;

	// number type question
	@FindBy(id = "scoreIncluded")
	public WebElement scoreInclude;

	@FindBy(id = "excludeNonResponse")
	public WebElement excludeNonResponse;

	@FindBy(id = "notApplicableResponse")
	public WebElement notApplicableResponse;

	@FindBy(id = "complianceNotApplicable")
	public WebElement complianceNotApplicable;

	@FindBy(id = "complianceByOption")
	public WebElement complianceByOption;

	@FindBy(xpath = ".//*[@id='addOn_1']/a/img")
	public WebElement addOnImg ;

	@FindBy(id = "rangeDetails_1rangeTo")
	public WebElement rangeDetails_1rangeTo;

	@FindBy(id = "rangeDetails_1score")
	public WebElement rangeDetails_1score;

	@FindBy(id = "rangeDetails_2score")
	public WebElement rangeDetails_2score;

	@FindBy(id = "withComments")
	public WebElement allowUserComments;

	@FindBy(id = "minScore")
	public WebElement minRange;

	@FindBy(id = "maxScore")
	public WebElement maxRange;

	// Smart Question
	@FindBy(xpath = ".//*[@id='ms-parentmoduleName']/button")
	public WebElement moduleTypeBtn ;

	@FindBy(xpath = ".//*[@id='ms-parentmoduleName']/div/div/input")
	public WebElement moduleTypeSearch ;

	@FindBy(xpath = ".//*[@id='ms-parentmoduleName']/div/ul/li[3]/label/input")
	public WebElement moduleTypeSelect ;

	@FindBy(xpath = ".//*[@id='ms-parentmoduleField']/button")
	public WebElement moduleFieldBtn ;

	@FindBy(xpath = ".//*[@id='ms-parentmoduleField']/div/div/input")
	public WebElement moduleFieldSearch ;

	@FindBy(xpath = ".//*[@id='ms-parentmoduleField']/div/ul/li[2]/label/input")
	public WebElement moduleFieldSelect ;

	@FindBy(name = "cancelStep2")
	public WebElement finishBtn;

	@FindBy(id = "uploadType")
	public WebElement uploadType;

	@FindBy(id = "uploadAttachmentPath")
	public WebElement uploadAttachmentPath;

	@FindBy(id = "rangeDetails_1rangeFrom")
	public WebElement rangeFrom;

	@FindBy(id = "rangeDetails_1rangeTo")
	public WebElement rangeTo;

	@FindBy(id = "rangeDetails_1score")
	public WebElement rangeScore;

	@FindBy(xpath = ".//tr[@id='row_1']//*[@name='optionResponseType']")
	public WebElement inputLabels1 ;

	@FindBy(xpath = ".//tr[@id='row_2']//*[@name='optionResponseType']")
	public WebElement inputLabels2 ;

	@FindBy(name = "optionResponseResult")
	public WebElement calculationResultLabel;

	@FindBy(id = "categoryID")
	public WebElement selectCategory;

	@FindBy(id = "addToQueLib")
	public WebElement addToQueLib;
	
	@FindBy(id="ms-parentmoduleName")
	public WebElement smartQModuleSelect;
	
	@FindBy(id="ms-parentmoduleField")
	public WebElement parentmoduleField;

	public AdminFieldOpsManageVisitFormCustomizeVisitFormPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
