package com.builds.uimaps.fieldops;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminFieldOpsManageVisitFormPage {

	@FindBy(id = "addsectionlink")
	public WebElement addVisitForm;

	@FindBy(xpath = ".//input[@value='Back']")
	public WebElement back ;

	@FindBy(id = "tabDisplayName")
	public WebElement visitFormName;

	@FindBy(id = "formDescription")
	public WebElement description;

	@FindBy(id = "frequency")
	public WebElement selectFrequency;

	@FindBy(id = "isPrivate")
	public WebElement isPrivate;

	@FindBy(name = "formFormat")
	public WebElement type;

	@FindBy(id = "formDesignList")
	public WebElement listLayout;

	@FindBy(id = "allowScore")
	public WebElement showResponseScore;

	@FindBy(name = "submitStep1")
	public WebElement saveAndNextBtn;

	@FindBy(name = "resetStep1")
	public WebElement resetBtn;

	@FindBy(name = "cancelStep1")
	public WebElement backFromVisitForm;

	@FindBy(name = "allowComment")
	public WebElement allowComent;

	@FindBy(id = "Response1")
	public WebElement configureColumnHeaderResponse;

	@FindBy(id = "Comment1")
	public WebElement configureColumnHeaderComments;

	@FindBy(xpath = ".//input[@value='Finish']")
	public WebElement finishBtn ;

	@FindBy(xpath = ".//*[@id='pageid']/a/u[contains(text() , 'Show All')]")
	public WebElement showAll ;

	// Add A visit form TabView
	@FindBy(name = "allowComment")
	public List<WebElement> allowComments;

	@FindBy(id = "Response1")
	public WebElement response;

	@FindBy(id = "Comment1")
	public WebElement comments;

	@FindBy(id = "formDesignTable")
	public WebElement tableLayout;

	// Add Question From Library
	@FindBy(id = "qType")
	public List<WebElement> qType;

	// Add Question In visit Form
	@FindBy(xpath = ".//*[@id='addquestionlink']/span")
	public WebElement addQuestionBtn ;

	public AdminFieldOpsManageVisitFormPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
