package com.builds.uimaps.fieldops;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminFieldOpsQATabIntegrationPage {

	@FindBy(name = "dataValue")
	public List<WebElement> enableQATab;

	@FindBy(xpath = ".//input[@value='Submit']")
	public WebElement submit ;

	@FindBy(xpath = ".//*[@id='module_fim']/a/i")
	public WebElement infoMgr ;

	@FindBy(linkText = "Franchisees")
	public WebElement franchisees ;

	@FindBy(name = "fimSearchString")
	public WebElement searchFranchisee;

	@FindBy(xpath = ".//a[@href='fimQAHistory']")
	public WebElement qaHistory ;

	@FindBy(xpath = ".//td/a/img[@class='topSearch']")
	public WebElement topSearchBtn ;

	@FindBy(xpath = ".//a/span[contains(text (), 'Create Visit')]")
	public WebElement createVisitText ;
	
	@FindBy(xpath=".//img[@alt='Search FIM on Name']")
	public WebElement searchBoxBtn;

	@FindBy(id = "fimTtInspectorName")
	public WebElement fieldConsultant;

	public AdminFieldOpsQATabIntegrationPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
