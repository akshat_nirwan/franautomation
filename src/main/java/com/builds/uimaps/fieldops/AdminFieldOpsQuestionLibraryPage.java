package com.builds.uimaps.fieldops;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminFieldOpsQuestionLibraryPage {

	@FindBy(linkText = "Add Category")
	public WebElement addCategory ;

	@FindBy(name = "categoryName")
	public WebElement categoryName;

	@FindBy(id = "submitButton")
	public WebElement saveBtn;

	// Modify
	@FindBy(id = "categoryID")
	public WebElement selectCategory;

	@FindBy(xpath = ".//tr/td/a[contains(text () , 'Modify')]")
	public WebElement modifyCategory ;

	@FindBy(xpath = ".//tr/td/a[contains(text () , 'Delete Category')]")
	public WebElement deleteCategory ;

	// Add a question
	@FindBy(xpath = ".//tr/td/a[contains(text () , 'Add Question')]")
	public WebElement addQuestion ;

	// Move To
	@FindBy(id = "categoryCombo")
	public WebElement categoryCombo;

	// Delete All Question
	@FindBy(xpath = ".//tr/td/a[contains(text () , 'Delete All Questions')]")
	public WebElement deleteAllQuestions ;

	// ......................................................Add Question rating
	// type
	@FindBy(xpath = ".//textarea[@name='question']")
	public WebElement questionTextArea ;

	@FindBy(xpath = "//textarea[@name='questionDescription']")
	public WebElement helpTextForUser ;

	@FindBy(id = "questionMandantory")
	public WebElement questionMandantory;

	@FindBy(id = "allowComment")
	public WebElement allowPrivateComment;

	@FindBy(id = "criticalLevel")
	public WebElement criticalLevel;

	@FindBy(id = "nextQuestionButton")
	public WebElement nextBtn;

	@FindBy(xpath = ".//*[@id='responseDivDisableData']//select[@name='responseType']")
	public WebElement selectResponseType ;

	@FindBy(id = "withComments")
	public WebElement allowUserComments;

	@FindBy(id = "withAttachments")
	public WebElement allowAttachments;

	@FindBy(id = "scoreIncluded")
	public WebElement scoreInclude;

	@FindBy(id = "excludeNonResponse")
	public WebElement excludeNonResponse;

	@FindBy(id = "notApplicableResponse")
	public WebElement notApplicableResponse;

	@FindBy(id = "complianceNotApplicable")
	public WebElement complianceNotApplicable;

	@FindBy(id = "complianceByOption")
	public WebElement complianceByOption;

	@FindBy(id = "complianceByValue")
	public WebElement complianceByValue;

	@FindBy(id = "complianceInput")
	public WebElement complianceInput;

	@FindBy(id = "minScore")
	public WebElement minRange;

	@FindBy(id = "maxScore")
	public WebElement maxRange;

	@FindBy(id = "isActionRequired")
	public WebElement assignTaskonNoncompliance;

	@FindBy(id = "actionMandatory")
	public WebElement isOptional;

	@FindBy(id = "newAction")
	public WebElement newAction;

	@FindBy(id = "existingAction")
	public WebElement actionFromLibrary;

	@FindBy(id = "actionSubject")
	public WebElement actionSubject;

	@FindBy(id = "actionPriority")
	public WebElement actionPriority;

	@FindBy(id = "scheduleCompletionBox")
	public WebElement scheduleCompletionBox;

	@FindBy(id = "descriptionBox")
	public WebElement descriptionBox;

	// Single Select type Radio Button
	@FindBy(xpath = ".//td[contains(text () , 'Choices')]//following::td/input[@name='optionResponseName']/../../../tr[@id='row_1']/td/input[@name='optionResponseName']")
	public WebElement firstChoice ;

	@FindBy(xpath = ".//td[contains(text () , 'Choices')]//following::td/input[@name='optionResponseName']/../../../tr[@id='row_2']/td/input[@name='optionResponseName']")
	public WebElement secondChoice ;

	@FindBy(xpath = ".//td[contains(text () , 'Choices')]//following::td/input[@name='optionResponseName']/../../../tr[@id='row_1']/td/input[@name='score']")
	public WebElement firstScore ;

	@FindBy(xpath = ".//td[contains(text () , 'Choices')]//following::td/input[@name='optionResponseName']/../../../tr[@id='row_2']/td/input[@name='score']")
	public WebElement secondScore ;

	// Smart Question
	@FindBy(xpath = ".//*[@id='ms-parentmoduleName']/button")
	public WebElement moduleClick ;

	@FindBy(xpath = ".//*[@id='ms-parentmoduleName']/div/div/input")
	public WebElement moduleSearch ;

	@FindBy(xpath = ".//*[@id='ms-parentmoduleField']/button")
	public WebElement moduleFieldClick ;

	@FindBy(xpath = ".//*[@id='ms-parentmoduleField']/div/div/input")
	public WebElement moduleFieldSearch ;

	public AdminFieldOpsQuestionLibraryPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
