package com.builds.uimaps.fieldops;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FieldOpsHomePage {

	@FindBy(linkText = "Create Visit")
	public WebElement createVisit ;

	// visit form
	@FindBy(xpath = ".//*[@id='ms-parentmasterVisitId']/button")
	public WebElement selectClick ;

	// visit form
	@FindBy(xpath = ".//*[@id='ms-parentmasterVisitId']")
	public WebElement selectVisitForm ;

	@FindBy(xpath = ".//*[@id='ms-parentmasterVisitId']/div/div/input")
	public WebElement searchBox ;

	// franchiseeID
	@FindBy(xpath = ".//*[@id='ms-parentfranchiseeNo']/button")
	public WebElement franchiseeSelect ;

	@FindBy(xpath = ".//*[@id='ms-parentfranchiseeNo']")
	public WebElement selectfranchiseeFromMultiSelect ;

	@FindBy(xpath = ".//*[@id='ms-parentfranchiseeNo']/div/div/input")
	public WebElement searchBoxFranchisee ;

	// Consultant
	@FindBy(xpath = ".//*[@id='ms-parentassignTo']/button")
	public WebElement selectConsultant ;

	@FindBy(xpath = ".//*[@id='ms-parentassignTo']")
	public WebElement selectConsultantVisit ;

	@FindBy(xpath = (".//*[@id='ms-parentassignTo']/div/div/input"))
	public WebElement consultantSearchBox ;

	@FindBy(id = "scheduleDateTime")
	public WebElement scheduleDateTime;

	@FindBy(id = "notes")
	public WebElement comments;

	@FindBy(id = "sendMailCheck")
	public WebElement sendMailCheckNotiFranchiseeOwner;

	@FindBy(id = "sendMailCheckForCompletion")
	public WebElement visitCompletionEmailFranchiseeOwner;

	@FindBy(xpath = ".//input[@value='Start Now']")
	public WebElement srartNow ;

	@FindBy(xpath = ".//input[@value='Schedule']")
	public WebElement schedule ;

	@FindBy(xpath = ".//*[@id='addquestionlink']/span[contains(text(),'Add Question')]")
	public WebElement addQuestion ;

	@FindBy(xpath = ".//input[@value='Finish']")
	public WebElement finishBtn ;

	// Verification
	@FindBy(xpath = ".//*[@id='ms-parentfranchiseeNo']/button")
	public WebElement selectFranID ;

	@FindBy(xpath = ".//*[@id='ms-parentfranchiseeNo']/div/div/input")
	public WebElement franIdSearchBox ;

	// Login After Logout
	@FindBy(id = "user_id")
	public WebElement userId;

	@FindBy(id = "password")
	public WebElement password;

	@FindBy(id = "ulogin")
	public WebElement loginBtn;

	// Verify Private Comment
	@FindBy(xpath = ".//*[@placeholder='This Private comment will not be visible to Franchise']")
	public WebElement privateBox ;

	@FindBy(linkText = "Visit Summary Report")
	public WebElement visitSummaryReport ;

	@FindBy(xpath = ".//*[@id='ms-parentformName']/button")
	public WebElement visitFormInReports ;

	@FindBy(xpath = ".//*[@id='ms-parentfranchiseeNo']")
	public WebElement selectFranchiseId ;

	@FindBy(xpath = ".//*[@id='module_audit']//a[@original-title='Take a Quick Overview of Recent Audits and Tasks' or .='Home']")
	public WebElement homePageLnk ;

	@FindBy(xpath = ".//select[@id='resultsPerPage']")
	public WebElement resultsPerPage ;

	public FieldOpsHomePage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
