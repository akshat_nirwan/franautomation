package com.builds.uimaps.fieldops;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FieldOpsReportsPage {

	@FindBy(xpath = ".//a[contains(text () , 'Visit Form Details Report')]")
	public WebElement visitFormDetailsReportLnk ;

	@FindBy(xpath = ".//*[@id='ms-parentformName']")
	public WebElement selectVisitFormName ;

	@FindBy(xpath = ".//input[@name='selectItemformName' and @value='-1']")
	public WebElement isVisitSelected ;

	@FindBy(id = "frequencyDateFrom")
	public WebElement frequencyDateFrom;

	@FindBy(id = "frequencyDateTo")
	public WebElement frequencyDateTo;

	@FindBy(xpath = ".//*[@id='ms-parentcomboDivision']")
	public WebElement divisionSelect ;

	@FindBy(xpath = ".//*[@id='ms-parentfranchiseId']")
	public WebElement franchiseSelect ;

	@FindBy(xpath = ".//*[@id='ms-parentauditor']")
	public WebElement consultantSelect ;

	@FindBy(xpath = ".//*[@id='ms-parentstatus']")
	public WebElement selectStatus ;

	@FindBy(xpath = ".//*[@id='searchButton' and @value='View Report']")
	public WebElement viewReport ;

	@FindBy(id = "reportiframe")
	public WebElement reportiframe;

	public FieldOpsReportsPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
