package com.builds.uimaps.fieldops;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FieldOpsTasksPage {

	@FindBy(linkText = "Add Task")
	public WebElement addTask ;

	@FindBy(id = "franchiseeId")
	public WebElement selectFranchiseeId;

	@FindBy(id = "radioVisitSelect")
	public List<WebElement> linkToVisit;

	@FindBy(id = "reminderPop")
	public WebElement reminderPop;

	@FindBy(id = "ms-parentactionItem")
	public WebElement subject;

	@FindBy(id = "description")
	public WebElement actionRequired;

	@FindBy(id = "status")
	public WebElement taskStatus;

	@FindBy(id = "targetDate")
	public WebElement dueDate;

	@FindBy(id = "franchiseeUser")
	public WebElement actionBy;

	@FindBy(id = "priority")
	public WebElement priority;

	@FindBy(xpath = ".//input[@value='Submit']")
	public WebElement submit ;

	@FindBy(id = "showFilter")
	public WebElement showFilter;

	@FindBy(xpath = ".//div[@id='ms-parentfranchiseId']")
	public WebElement selectFranIDForSearch ;

	@FindBy(xpath = ".//*[@id='ms-parentfranchiseId']/div/div/input")
	public WebElement franchiseBox ;

	@FindBy(xpath = ".//*[@id='ms-parentstatus']")
	public WebElement taskStatusSelectAtFilter ;

	@FindBy(xpath = ".//*[@id='ms-parentvisitID']")
	public WebElement visitFormSelectAtFilter ;

	@FindBy(xpath = ".//*[@id='ms-parentassignTo']")
	public WebElement assignToSelectAtFilter ;

	@FindBy(xpath = ".//*[@id='ms-parentauditor']")
	public WebElement consultantToSelectAtFilter ;

	@FindBy(xpath = ".//*[@id='ms-parentpriority']")
	public WebElement prioritySelectAtFilter ;

	@FindBy(xpath = ".//input[@value='Save View' and @name='Button']")
	public WebElement saveViewBtn ;

	@FindBy(id = "Submit")
	public WebElement searchBtn;

	@FindBy(linkText = "Hide Filters")
	public WebElement hideFilter ;

	// assign To
	@FindBy(id = "franchiseeUser")
	public WebElement selectAssignTo;

	@FindBy(xpath = ".//input[@value='Update']")
	public WebElement updateBtn ;

	// Change Status
	@FindBy(xpath = ".//input[@value='Modify']")
	public WebElement statusModifyBtn ;

	// Link to Visit
	@FindBy(xpath = ".//*[@id='addquestionlink']/span[contains(text () , 'Add Question')]")
	public WebElement addQuestion ;

	@FindBy(xpath = ".//input[@value='Finish']")
	public WebElement finishBtn ;

	@FindBy(id = "visitId")
	public WebElement selectVisitID;

	@FindBy(id = "subject")
	public WebElement addNewAction;

	@FindBy(linkText = "Visits")
	public WebElement visitBtn ;

	// Verification
	@FindBy(xpath = ".//*[@id='ms-parentfranchiseeNo']/button")
	public WebElement selectFranID ;

	@FindBy(xpath = ".//*[@id='ms-parentfranchiseeNo']/div/div/input")
	public WebElement franIdSearchBox ;

	@FindBy(id = "nextSubmitButton")
	public WebElement nextBtnSubmit;

	@FindBy(id = "submitButton")
	public WebElement submitBtn;

	@FindBy(xpath = ".//input[@value='OK']")
	public WebElement okBtn ;

	// Filter at The Hub Page
	@FindBy(id = "category")
	public WebElement selectCategory;

	@FindBy(id = "status")
	public WebElement selectTaskStatus;

	@FindBy(id = "subject")
	public WebElement hubSubject;

	@FindBy(id = "startDate")
	public WebElement fromDate;

	@FindBy(id = "endDate")
	public WebElement toDate;

	@FindBy(id = "reminder")
	public WebElement selectReminder;

	@FindBy(id = "priority")
	public WebElement selectPriority;

	@FindBy(xpath = ".//*[@id='ms-parentmineall']/button")
	public WebElement viewSelectBtn ;

	@FindBy(xpath = ".//*[@id='ms-parentmineall']/div/div/input")
	public WebElement viewSearchBx ;

	@FindBy(xpath = ".//*[@id='ms-parentmineall']/div//input[@id='selectAll']")
	public WebElement viewSelectAll ;

	@FindBy(xpath = ".//*[@id='ms-parenttaskType']/button")
	public WebElement taskTypeSelectBtn ;

	@FindBy(xpath = ".//*[@id='ms-parenttaskType']/div/div/input")
	public WebElement taskTypeSearchBx ;

	@FindBy(xpath = ".//*[@id='ms-parenttaskType']/div//input[@id='selectAll']")
	public WebElement taskTypeSelectAll ;

	@FindBy(id = "go")
	public WebElement hubSearchBtn;

	@FindBy(xpath = ".//*[@id='ms-parentfranchiseeNo']/button")
	public WebElement franchiseIDSelectBtn ;

	@FindBy(xpath = ".//*[@id='ms-parentfranchiseeNo']//input[@id='selectAll']")
	public WebElement franchiseIDselectAll ;

	@FindBy(xpath = ".//*[@id='ms-parentfranchiseeNo']/div/div/input")
	public WebElement franchaseIDTxBx ;

	@FindBy(xpath = ".//input[@value='Save View']")
	public WebElement saveViewBtn1 ;

	@FindBy(xpath = ".//*[@id='ms-parentstatus']/button")
	public WebElement taskStatusSelectBtn ;

	@FindBy(xpath = ".//*[@id='ms-parentstatus']/div/div/input")
	public WebElement taskStatusSearctBx ;

	@FindBy(xpath = ".//*[@id='ms-parentstatus']/div//input[@id='selectAll']")
	public WebElement taskStatusSelectAll ;

	@FindBy(xpath = ".//*[@id='ms-parentpriority']/button")
	public WebElement prioritySelectBtn ;

	@FindBy(xpath = ".//*[@id='ms-parentpriority']/div/div/input")
	public WebElement prioritySearchBx ;

	@FindBy(xpath = ".//*[@id='ms-parentpriority']/div//input[@id='selectAll']")
	public WebElement prioritySelectAll ;

	@FindBy(xpath = ".//input[@value='View']")
	public WebElement viewBtn ;

	@FindBy(xpath = ".//input[@name='tskchk']")
	public WebElement lookInCheck ;

	@FindBy(xpath = ".//input[@name='subchk']")
	public WebElement lookInSubjectChck ;

	@FindBy(xpath = ".//*[@id='ms-parentmineall']")
	public WebElement viewSelect ;

	@FindBy(xpath = ".//*[@id='ms-parenttaskType']")
	public WebElement taskTypeSelect ;

	@FindBy(xpath = ".//*[@id='module_audit']//a[@original-title='View and Manage Remedial Actions' or .='Tasks']")
	public WebElement tasksPageLnk ;

	@FindBy(xpath = ".//*[@id='hideprint']//input[@name='close']")
	public WebElement closeBtn ;

	@FindBy(xpath = ".//*[@id='module_audit']//a[@original-title='Generate and View Customizable Performance Reports']")
	public WebElement reportsLnk ;

	@FindBy(xpath = ".//*[contains(text () , 'Corrective Action Plan(CAP) Report')]")
	public WebElement CAPReportsLnk ;

	public FieldOpsTasksPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
