package com.builds.uimaps.fieldops;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FieldOpsVisitsPage {

	@FindBy(linkText = "Create Visit")
	public WebElement createVisit ;

	// visit form
	@FindBy(xpath = ".//*[@id='ms-parentmasterVisitId']/button")
	public WebElement selectClick ;

	// visit form
	@FindBy(xpath = ".//*[@id='ms-parentmasterVisitId']")
	public WebElement selectVisitForm ;

	@FindBy(xpath = ".//*[@id='ms-parentfranchiseeNo']")
	public WebElement selectfranchiseeFromMultiSelect ;

	@FindBy(xpath = ".//*[@id='ms-parentmasterVisitId']")
	public WebElement drpVisitFormMainDiv ;

	@FindBy(xpath = ".//*[@id='ms-parentmasterVisitId']/div/div/input")
	public WebElement searchBox ;

	// franchiseeID
	@FindBy(xpath = ".//*[@id='ms-parentfranchiseeNo']/button")
	public WebElement franchiseeSelect ;

	@FindBy(xpath = ".//*[@id='ms-parentfranchiseeNo']/div/div/input")
	public WebElement searchBoxFranchisee ;

	// Consultant
	@FindBy(xpath = ".//*[@id='ms-parentassignTo']/button")
	public WebElement selectConsultant ;

	@FindBy(xpath = ".//*[@id='ms-parentassignTo']")
	public WebElement selectConsultantMultiSelect ;

	@FindBy(xpath = (".//*[@id='ms-parentassignTo']/div/div/input"))
	public WebElement consultantSearchBox ;

	@FindBy(xpath = (".//*[@id='ms-parentassignTo']"))
	public WebElement consultantSearchBoxDiv ;
	///////////////////////////////

	@FindBy(id = "scheduleDateTime")
	public WebElement scheduleDateTime;

	@FindBy(id = "notes")
	public WebElement comments;

	@FindBy(id = "sendMailCheck")
	public WebElement sendMailCheckNotiFranchiseeOwner;

	@FindBy(id = "sendMailCheckForCompletion")
	public WebElement visitCompletionEmailFranchiseeOwner;

	@FindBy(xpath = ".//input[@value='Start Now']")
	public WebElement srartNow ;

	@FindBy(xpath = ".//input[@value='Schedule']")
	public WebElement schedule ;

	@FindBy(xpath = ".//*[@id='addquestionlink']/span[contains(text () , 'Add Question')]")
	public WebElement addQuestion ;

	@FindBy(xpath = ".//input[@value='Finish']")
	public WebElement finishBtn ;

	// Verification
	@FindBy(xpath = ".//*[@id='ms-parentfranchiseeNo']/button")
	public WebElement selectFranID ;

	@FindBy(xpath = ".//*[@id='ms-parentfranchiseeNo']/div/div/input")
	public WebElement franIdSearchBox ;

	// start Visit
	@FindBy(id = "nextSubmitButton")
	public WebElement nextBtnSubmit;

	@FindBy(id = "submitButton")
	public WebElement submitBtnAtVisit;

	@FindBy(id = "submitButton")
	public WebElement submitAllBtn;

	@FindBy(xpath = ".//input[@value='OK']")
	public WebElement okBtn ;

	// Modify Visit
	@FindBy(id = "notes")
	public WebElement mNotes;

	@FindBy(id = "status")
	public WebElement selectStatus;

	@FindBy(name = "buttonSubmit")
	public WebElement submitBtnM;

	// Practise Visit
	@FindBy(id = "includeVisitPractise")
	public WebElement includeVisitPractise;

	// Info Mgr
	@FindBy(linkText = "Info Mgr")
	public WebElement infoMgr ;

	@FindBy(linkText = "Franchisees")
	public WebElement franchisees ;

	@FindBy(name = "fimSearchString")
	public WebElement searchFranchisee;

	@FindBy(xpath = ".//a[@href='fimQAHistory']")
	public WebElement qaHistory ;

	@FindBy(xpath = ".//td/a/img[@class='topSearch']")
	public WebElement topSearchBtn ;

	@FindBy(id = "visitNumber")
	public WebElement visitNumber;

	// Verify Status Save as draft
	@FindBy(xpath = ".//*[@id='saveAsDraftBtn']")
	public WebElement saveAsDraftBtn ;

	@FindBy(xpath = ".//*[@id='ms-parentmasterVisitId']/button")
	public WebElement visitFormSelectBtn ;

	@FindBy(xpath = ".//*[@id='ms-parentmasterVisitId']/div/div/input")
	public WebElement visitFormSearchBx ;

	@FindBy(xpath = ".//*[@id='ms-parentmasterVisitId']/div//input[@id='selectAll']")
	public WebElement visitFormSelectAll ;

	@FindBy(xpath = ".//*[@id='ms-parentOwnersComboMail']")
	public WebElement ownerSelectBtn ;

	@FindBy(xpath = ".//*[@id='ms-parentOwnersComboMail']/div/div/input")
	public WebElement ownerSearchBx ;

	@FindBy(xpath = ".//*[@id='ms-parentOwnersComboMail']/div//input[@id='selectAll']")
	public WebElement ownerSelectAll ;

	@FindBy(xpath = ".//input[@value='Send']")
	public WebElement sendBtn ;

	@FindBy(xpath = ".//*[@id='ms-parentOwnersCombo']")
	public WebElement ownerSelect ;

	@FindBy(xpath = ".//*[@id='ms-parentfranchiseeNo']")
	public WebElement selectFranchiseId ;

	@FindBy(xpath = ".//*[@id='module_audit']//a[@original-title='View and Schedule Franchisee Store Audits' or .='Visits']")
	public WebElement visitPageLnk ;

	@FindBy(xpath = ".//input[@name='buttonSubmit' and @value='Start Now']")
	public WebElement startNowButton ;

	@FindBy(xpath = ".//*[@id='addTaskTd']//a[contains(text () , 'Add More Tasks')]")
	public WebElement addMoreTaskButton ;

	@FindBy(id = "actionItem0")
	public WebElement selectAction;

	@FindBy(id = "franchiseeUser0")
	public WebElement actionBy;

	@FindBy(xpath = ".//*[@id='module_audit']//a[@original-title='Generate and View Customizable Performance Reports']")
	public WebElement reportsLnk ;

	@FindBy(xpath = ".//*[contains(text () , 'Corrective Action Plan(CAP) Report')]")
	public WebElement CAPReportsLnk ;

	@FindBy(id = "targetDate0")
	public WebElement dueDate;

	@FindBy(xpath = ".//*[contains(@id, 'Response2_')]")
	public WebElement ownerResponse ;

	@FindBy(xpath = ".//*[contains(@id, 'Comment2_')]")
	public WebElement ownerComment ;

	@FindBy(xpath = ".//*[contains(@id , 'Response_')]")
	public WebElement sampleQuestionAnswer ;

	@FindBy(id = "cancelButton")
	public WebElement exitVisit;
	
	@FindBy(xpath=".//img[@alt='Search FIM on Name']")
	public WebElement searchBoxBtn;

	public FieldOpsVisitsPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
