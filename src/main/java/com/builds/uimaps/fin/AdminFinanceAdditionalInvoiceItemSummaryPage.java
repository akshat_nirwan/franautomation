package com.builds.uimaps.fin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminFinanceAdditionalInvoiceItemSummaryPage {

	@FindBy(xpath = ".//input[@value='Add New Item']")
	public WebElement addNewItem ;

	@FindBy(name = "name")
	public WebElement invoiceItem;

	@FindBy(id = "defaultValue")
	public WebElement defaultValue;

	@FindBy(name = "submitButton")
	public WebElement saveBtn;

	@FindBy(id = "fixed")
	public WebElement fixed;

	@FindBy(id = "variable")
	public WebElement variable;

	@FindBy(xpath = ".//input[@name='Close' and @value='Close']")
	public WebElement closeBtn ;
	
	@FindBy(xpath=".//td[contains(text(), 'Confirmation')]/ancestor::tr[2]//td[contains(text(), 'successfully.')]")
	public WebElement confirmationText;
	
	public AdminFinanceAdditionalInvoiceItemSummaryPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
