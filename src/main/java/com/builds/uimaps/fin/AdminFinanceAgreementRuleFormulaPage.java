package com.builds.uimaps.fin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminFinanceAgreementRuleFormulaPage {
	@FindBy(xpath = "//span[1][contains(text(), 'Rule Formula')]")
	public WebElement RuleFormulaBtn;

	@FindBy(xpath = "//input[@value='Add Rule Formula']")
	public WebElement AddRuleFormulaBtn ;

	@FindBy(id = "agreementRuleTypeSales Based")
	public WebElement RFSalesAmountBased;

	@FindBy(id = "agreementRuleTypeAnniversary Based")
	public WebElement RFStoreAgeBased;

	@FindBy(xpath = ".//input[contains(@id , 'agreementRuleTypeQuantity')]")
	public WebElement RFQuantityBased ;

	@FindBy(xpath = "//input[@value='Continue']")
	public WebElement ContinueBtn ;

	@FindBy(id = "agreementRuleName")
	public WebElement RuleFormulaName;

	@FindBy(xpath = "//td[@class='TextLbl']/input[@value='P']")
	public WebElement PercentageRadioBtn;

	@FindBy(xpath = "//td[@class='TextLbl']/input[@value='F']")
	public WebElement FlatRadioBtn;

	@FindBy(xpath = "//input[@id='growthBased' and @value='Y']")
	public WebElement GrothBasedYesRadioBtn;

	@FindBy(xpath = "//input[@id='growthBased' and @value='N']")
	public WebElement GrothBasedNoRadioBtn;

	@FindBy(xpath = "//a[@title='Add More']")
	public WebElement AddMoreBtn;

	@FindBy(xpath = "//tr[@id='row_0']/td[2]/input")
	public WebElement SetRangeToFieldZeroRow;

	@FindBy(xpath = "//tr[@id='row_0']/td[3]/input")
	public WebElement AppliedValueForZeroRow ;

	@FindBy(xpath = "//tr[@id='row_1']/td[1]/input")
	public WebElement SetRangeFromFieldFirstRow;

	@FindBy(xpath = "//tr[@id='row_1']/td[3]/input")
	public WebElement AppliedValueForFirstRow ;

	@FindBy(id = "SubmitButton")
	public WebElement SaveBtn;

	@FindBy(id = "20")
	public WebElement selectViewPerPage;

	@FindBy(xpath = ".//*[@name='20']")
	public WebElement viewPerPageSelect ;

	public AdminFinanceAgreementRuleFormulaPage(WebDriver driver) {
		PageFactory.initElements(driver, this);

	}
}
