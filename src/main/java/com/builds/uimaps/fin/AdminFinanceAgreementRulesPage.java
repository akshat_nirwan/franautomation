package com.builds.uimaps.fin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminFinanceAgreementRulesPage

{

	@FindBy(xpath = "//a[@href='finAgreementRulesSummary']/span")
	public WebElement AgreementRulesBtn ;

	@FindBy(xpath = "//input[@class='cm_new_button_link']")
	public WebElement AddAgreementRule ;

	@FindBy(id = "agreementRuleTypeSales Based")
	public WebElement ARSalesAmountBased;

	@FindBy(id = "agreementRuleTypeTime Based")
	public WebElement ARSalesReportPeriodBased;

	@FindBy(id = "agreementRuleTypeAnniversary Based")
	public WebElement ARStoreAgeBased;

	@FindBy(xpath = ".//input[contains(@id , 'agreementRuleTypeQuantity')]")
	public WebElement ARQuantityBased ;

	@FindBy(id = "agreementRuleTypeCategory Sales Based")
	public WebElement ARCategorySubCategoryQuantitySalesBased;

	@FindBy(id = "agreementRuleTypePrevious Year")
	public WebElement ARPreviousYearsSalesBased;

	@FindBy(xpath = "//input[@value='Continue']")
	public WebElement continueBtn ;

	@FindBy(xpath = ".//input[@value ='Category Sales Based']")
	public WebElement categorySalesBased ;

	// @FindBy(xpath=".//input[@value='Category Quantity Based']")
	@FindBy(xpath = ".//input[contains(@value , 'Category Quantity')]")
	public WebElement categoryQuantityBased ;

	@FindBy(xpath = ".//input[@value='Custom']")
	public WebElement customRule ;

	@FindBy(id = "agreementRuleName")
	public WebElement AgreementRuleName;

	@FindBy(name = "minCapChk")
	public WebElement CheckboxMinimumCap;

	@FindBy(id = "minCap")
	public WebElement MinimumCap;

	@FindBy(name = "maxCapChk")
	public WebElement CheckBoxMaximumCap;

	@FindBy(id = "maxCap")
	public WebElement MaximumCap;

	@FindBy(name = "description")
	public WebElement Description;

	@FindBy(id = "finAgreementRuleDetails_0calculationType1")
	public WebElement defaultCalType;

	@FindBy(id = "finAgreementRuleDetails_1calculationType1")
	public WebElement serviceCalType;

	@FindBy(id = "finAgreementRuleDetails_2calculationType1")
	public WebElement serviceSubCat1CalType;

	@FindBy(id = "finAgreementRuleDetails_3calculationType1")
	public WebElement serviceSubCat2CalType;

	@FindBy(id = "finAgreementRuleDetails_0feeTableId")
	public WebElement selectCat1RuleFormula;

	@FindBy(id = "finAgreementRuleDetails_1feeTableId")
	public WebElement selectCat2RuleFormula;

	@FindBy(id = "finAgreementRuleDetails_2feeTableId")
	public WebElement selectCat3RuleFormula;

	@FindBy(id = "finAgreementRuleDetails_3feeTableId")
	public WebElement selectCat4RuleFormula;

	@FindBy(id = "finAgreementRuleDetails_4feeTableId")
	public WebElement selectCat5RuleFormula;

	@FindBy(id = "finAgreementRuleDetails_5feeTableId")
	public WebElement selectCat6RuleFormula;

	@FindBy(xpath = "//span[@class='placeholder']")
	public WebElement categoryBtn ;

	@FindBy(xpath = "//input[@class='searchInputMultiple']")
	public WebElement searchCategory ;

	@FindBy(xpath = "//input[@id='selectItemcategories' AND @value='1']")
	public WebElement selectCatogory ;

	@FindBy(xpath = ".//input[@id='appliedAs' AND @value='P']")
	public WebElement AppliedAsPercent ;

	@FindBy(xpath = ".//input[@id='accountingBasis' AND @value='CURR']")
	public WebElement AccountingBasisPeriodicSales ;

	@FindBy(id = "finAgreementRuleDetails_1appliedValue")
	public WebElement appliedValuePercent;

	@FindBy(xpath = "//td[@class='TextLbl']/input[@value='P']")
	public WebElement RulePercentageRadioBtn;

	@FindBy(xpath = "//td[@class='TextLbl']/input[@value='F']")
	public WebElement RuleFlatRadioBtn;

	@FindBy(xpath = "//input[@value='YTD']")
	public WebElement YTDAccountingBasis;

	@FindBy(xpath = "//input[@value='CUMM']")
	public WebElement CumulativeAccountingBasis;

	@FindBy(xpath = "//input[@value='CURR']")
	public WebElement PeriodicSalesAccountingBasis;

	@FindBy(xpath = "//input[@value='RRSD']")
	public WebElement YTDfromRoyaltyReportingStartDateAccountingBasis;

	@FindBy(id = "finAgreementRuleDetails_0rangeTo")
	public WebElement RangeToConf;

	@FindBy(xpath = "//a[@title='Add More']")
	public WebElement RuleAddMoreBtn;

	@FindBy(xpath = "//tr[@id='salesrow_-1_0']/td[6]/a")
	public WebElement defaultAddMoreBtn ;

	@FindBy(xpath = "//tr[@id='salesrow_1_0']/td[6]/a")
	public WebElement serviceAddMoreBtn ;

	@FindBy(xpath = "//tr[@id='row_0']/td[2]/input")
	public WebElement RuleSetRangeToFieldZeroRow;

	@FindBy(xpath = "//tr[@id='row_0']/td[3]/input")
	public WebElement RuleAppliedValueForZeroRow ;

	@FindBy(xpath = "//tr[@id='row_1']/td[1]/input")
	public WebElement RuleSetRangeFromFieldFirstRow;

	@FindBy(xpath = "//tr[@id='row_1']/td[3]/input")
	public WebElement RuleAppliedValueForFirstRow ;

	@FindBy(name = "Submit")
	public WebElement submitBtn;

	@FindBy(id = "ms-parentcategories")
	public WebElement categoryDropDown;

	@FindBy(xpath = ".//div//[@class='ms-search']//input")
	public WebElement searchCategoryDropDown ;

	@FindBy(id = "ms-parentcategories")
	public WebElement category;

	@FindBy(xpath = ".//input[@value='S' and @name='minMaxType']")
	public WebElement selectStandard ;

	@FindBy(id = "includeSubCategory")
	public WebElement checkboxIncludeSubCategory;

	@FindBy(id = "finAgreementRuleDetails_0_-1rangeTo")
	public WebElement RuleSetRangeToFieldDefaultZerothRow;

	@FindBy(id = "finAgreementRuleDetails_0_-1appliedValue")
	public WebElement RuleAppliedValueForDefaultZeroRow;

	@FindBy(id = "finAgreementRuleDetails_1_-1appliedValue")
	public WebElement RuleAppliedValueForDefaultFirstRow;

	@FindBy(id = "finAgreementRuleDetails_0_1rangeTo")
	public WebElement RuleSetRangeToFieldServiceZeroRow;

	@FindBy(id = "finAgreementRuleDetails_0_1appliedValue")
	public WebElement RuleAppliedValueForServiceZeroRow;

	@FindBy(id = "finAgreementRuleDetails_1_1appliedValue")
	public WebElement RuleAppliedValueForServiceFirstRow;

	@FindBy(id = "finAgreementRuleDetails_0appliedValue")
	public WebElement appliedValueDefaultCategory;

	@FindBy(id = "finAgreementRuleDetails_1appliedValue")
	public WebElement appliedValueServiceCat;

	@FindBy(id = "finAgreementRuleDetails_2appliedValue")
	public WebElement appliedValueServiceSubCat1;

	@FindBy(id = "finAgreementRuleDetails_3appliedValue")
	public WebElement appliedValueServiceSubCat2;

	@FindBy(id = "finAgreementRuleDetails_4appliedValue")
	public WebElement appliedValueServiceSubCat3;

	@FindBy(xpath = ".//input[@value='T']")
	public WebElement selectStoreAgeBased ;

	@FindBy(xpath = "//tr[@id='row_0']/td[6]/a/img")
	public WebElement addMoreMaxMinCap ;

	@FindBy(id = "20")
	public WebElement selectViewPerPage;

	@FindBy(id = "toDate0")
	public WebElement storeAgeToMonth;

	@FindBy(id = "minCapChk0")
	public WebElement CheckboxMinCapZeroCustom;

	@FindBy(id = "minCap0")
	public WebElement minCapValueZeroCustom;

	@FindBy(id = "maxCapChk0")
	public WebElement CheckBoxMaxCapZeroCustom;

	@FindBy(id = "maxCap0")
	public WebElement MaxCapValueZeroCustom;

	@FindBy(id = "minCapChk1")
	public WebElement CheckboxMinCapFirstCustom;

	@FindBy(id = "minCap1")
	public WebElement minCapValueFirstCustom;

	@FindBy(id = "maxCapChk1")
	public WebElement CheckBoxMaxCapFirstCustom;

	@FindBy(id = "maxCap1")
	public WebElement MaxCapValueFirstCustom;

	@FindBy(xpath = "//td[contains(text(), 'Max / Min Cap')]")
	public WebElement clickMaxMin ;

	@FindBy(xpath = ".//*[@name='20']")
	public WebElement viewPerPageSelect ;

	public AdminFinanceAgreementRulesPage(WebDriver driver) {
		PageFactory.initElements(driver, this);

	}
}
