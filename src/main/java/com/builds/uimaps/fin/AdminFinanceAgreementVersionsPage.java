package com.builds.uimaps.fin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminFinanceAgreementVersionsPage

{
	@FindBy(xpath = "//a[Contains(text(), 'Agreement Versions')]")
	public WebElement AgreemenmtVersionsBtn;

	@FindBy(xpath = "//a[@href='finAgreementVersions']/span[1]")
	public WebElement AgreemenmtVersionBtn ;

	@FindBy(xpath = "//input[@value='Add Agreement']")
	public WebElement AddAgreement ;

	@FindBy(id = "agreementName")
	public WebElement AgreementName;

	@FindBy(id = "startDate")
	public WebElement AgreementStartDate;

	@FindBy(id = "endDate")
	public WebElement AgreementEndDate;

	@FindBy(id = "agreementFrequency")
	public WebElement AgreementFrequency;

	@FindBy(id = "description")
	public WebElement AVDescription;

	@FindBy(id = "finAgreementTypeItems_0agreementItemType")
	public WebElement RoyaltyType;

	@FindBy(id = "finAgreementItems_0agreementItemValue")
	public WebElement RoyaltyValue;

	@FindBy(id = "finAgreementTypeItems_1agreementItemType")
	public WebElement AdvType;

	@FindBy(id = "finAgreementItems_1agreementItemValue")
	public WebElement AdvValue;

	@FindBy(id = "finAgreementTypeItems_2agreementItemType")
	public WebElement RoyaltyAreaFranchiseType;

	@FindBy(id = "finAgreementItems_2agreementItemValue")
	public WebElement RoyaltyAreaFranchiseValue;

	@FindBy(id = "finAgreementTypeItems_3agreementItemType")
	public WebElement AdvAreaFranchiseType;

	@FindBy(id = "finAgreementItems_3agreementItemValue")
	public WebElement AdvAreaFranchiseValue;

	@FindBy(id = "finAgreementItems_20noOfDays")
	public WebElement LateFeeDaysNoOfDays;

	@FindBy(id = "finAgreementTypeItems_20agreementItemType")
	public WebElement selectLateFeeType;

	@FindBy(id = "finAgreementItems_20agreementItemValue")
	public WebElement LateFeePercentValue;

	@FindBy(id = "finAgreementTypeItems_21agreementItemType")
	public WebElement selectLateFeeAFType;

	@FindBy(id = "finAgreementItems_21agreementItemValue")
	public WebElement LateFeePercentValueAreaFranchise;

	@FindBy(id = "SubmitButton")
	public WebElement SubmitBtn;

	@FindBy(xpath = ".//select[@class='multiList']")
	public WebElement selectViewPerPage ;

	@FindBy(xpath = "//tr[td[a[contains(text(), 'AgreementVersion')]]]/td[8]/div/layer/a/img")
	public WebElement actionAgreementVersion ;

	@FindBy(id = "Action_dynamicmenu21")
	public WebElement configureYearlyCap;

	@FindBy(xpath = "//input[@value='Add Yearly Cap']")
	public WebElement addYearlyCap ;

	@FindBy(xpath = ".//a[contains(text(), 'Created On')]")
	public WebElement createdOn ;

	@FindBy(id="progressIframe")
	public WebElement yearlyCapIframe;

	@FindBy(id = "year")
	public WebElement capYear;

	@FindBy(id = "amount")
	public WebElement capAmount;

	@FindBy(id = "feeType")
	public WebElement feeType;

	@FindBy(id = "Submit")
	public WebElement saveCapFee;

	@FindBy(xpath = "//input[@class='cm_new_button']")
	public WebElement closeBtn ;

	@FindBy(id = "franId")
	public WebElement searchFranID;

	@FindBy(xpath = ".//span[@id='avSpan']//input[@value='1']")
	public WebElement checkAllSFAV ;

	@FindBy(xpath = ".//input[@value='Map']")
	public WebElement mapFranchise ;

	@FindBy(xpath = ".//*[@name='20']")
	public WebElement viewPerPageSelect ;

	@FindBy(xpath = ".//*[@id='ms-parentfranchiseMenu']")
	public WebElement selectFranchiseLocation ;

	@FindBy(id = "searchButton")
	public WebElement searchButton;

	@FindBy(xpath = ".//*[@id='pageid']/a[2]/u[.='Next']")
	public WebElement nextBtn ;

	@FindBy(xpath = "//a[contains(text(), 'View Logs')]")
	public WebElement viewLogsForYearlyCap;
	
	
	public AdminFinanceAgreementVersionsPage(WebDriver driver) {
		PageFactory.initElements(driver, this);

	}
}
