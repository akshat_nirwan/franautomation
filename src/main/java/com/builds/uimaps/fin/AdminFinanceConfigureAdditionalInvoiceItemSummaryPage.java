package com.builds.uimaps.fin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminFinanceConfigureAdditionalInvoiceItemSummaryPage

{

	@FindBy(xpath = "//a[contains(text(), 'Configure Additional Invoice Item(s)')]")
	public WebElement ConfigureAdditionalInvoiceItem ;

	@FindBy(xpath = "//input[@value='Add New Item']")
	public WebElement addNewItem ;

	@FindBy(name = "name")
	public WebElement AdditionalInvoiceItemName;

	@FindBy(id = "defaultValue")
	public WebElement DefaultAmountValue;

	@FindBy(id = "fixed")
	public WebElement FixedRadioBtn;

	@FindBy(id = "variable")
	public WebElement VariableRadioBtn;

	@FindBy(name = "submitButton")
	public WebElement SaveBtn;

	@FindBy(xpath = ".//input[@name='Close' and @value='Close']")
	public WebElement closeBtn ;

	AdminFinanceConfigureAdditionalInvoiceItemSummaryPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
