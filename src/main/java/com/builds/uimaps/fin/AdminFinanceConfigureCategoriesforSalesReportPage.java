package com.builds.uimaps.fin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminFinanceConfigureCategoriesforSalesReportPage

{
	@FindBy(xpath = "//a[contains(text(), 'Configure Categories for Sales Report')]")
	public WebElement ConfigureCategoriesforSalesReport ;

	@FindBy(xpath = ".//*[@id='siteMainTable']/tbody/tr/td/table/tbody/tr/td[2]/div/div/div/div/div/div[1]/div[2]/div/div/div")
	public WebElement clickAction ;

	
	//@FindBy(xpath = "//input[@value='Add New Category']")
	//public WebElement AddNewCategory ;

	@FindBy(xpath = "//input[@value='Add New Sub Category']")
	public WebElement AddNewSubCategory ;

	@FindBy(xpath = ".//a[contains(text () , 'Add New Category')]")
	public WebElement addNewCategoryLnk ;

	@FindBy(name = "name")
	public WebElement CategoryName;

	@FindBy(id = "salesCategory")
	public WebElement mainCategory;

	@FindBy(name = "name")
	public WebElement subCategoryName;

	@FindBy(id = "calculationMethod1")
	public WebElement CanculationTypeAddition;

	@FindBy(id = "calculationMethod-1")
	public WebElement CanculationTypeSubtraction;

	@FindBy(name = "submitButton")
	public WebElement saveBtn;

	@FindBy(name = "Close")
	public WebElement closeBtn;

	@FindBy(xpath = ".//div[@class='dropdown-action no-arrow three-dot-nav']//div[@data-role='ico_ThreeDots']")
	public WebElement clickButtonThreeDots ;

	@FindBy(xpath = ".//a[.='Add New Sub Category']")
	public WebElement addNewSubCategoryButton ;

	@FindBy(xpath = ".//a[.='Add New Category']")
	public WebElement addNewCategory ;

	@FindBy(id = "calculationMethod1")
	public WebElement calculationMethod1;

	@FindBy(xpath = ".//label[@for='calculationMethod1']")
	public WebElement additionLabel ;

	@FindBy(id = "calculationMethod-1")
	public WebElement calculationMethod2;

	@FindBy(xpath = ".//label[@for='calculationMethod-1']")
	public WebElement substractionLabel ;

	@FindBy(xpath =".//a[.='Order Main Categories']")
	public WebElement orderMainCategories;
	
	@FindBy(xpath =".//a[.='Order Sub Categories']")
	public WebElement orderSubCategories;
	
	@FindBy(name="templateOrder")
	public WebElement selectCategory;
	
	@FindBy(xpath = ".//img[@title='Move to Top']")
	public WebElement moveToTop;
	
	@FindBy(xpath = ".//img[@title='Move Up']")
	public WebElement moveUp;
	
	@FindBy(xpath = ".//img[@title='Move Down']")
	public WebElement moveDown;
	
	@FindBy(xpath = ".//img[@title='Move to Bottom']")
	public WebElement moveToBottom;
	
	@FindBy(xpath = ".//input[@value='Change Category Sequence']")
	public WebElement changeCategorySequence;
	
	@FindBy(id="mainCatOrder")
	public WebElement selectMainCatForSubCat;
	
	
	public AdminFinanceConfigureCategoriesforSalesReportPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
