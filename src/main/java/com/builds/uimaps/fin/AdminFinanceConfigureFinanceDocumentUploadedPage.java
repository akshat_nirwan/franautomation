package com.builds.uimaps.fin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminFinanceConfigureFinanceDocumentUploadedPage {

	@FindBy(xpath = ".//input[@value='Add Document Name']")
	public WebElement addDocumentNameBtn ;

	@FindBy(name = "name")
	public WebElement txField;

	@FindBy(id = "save")
	public WebElement saveBtn;

	@FindBy(id = "resultsPerPage")
	public WebElement resultsPerPage;

	@FindBy(name = "submitButton")
	public WebElement submitButton;
	
	@FindBy(name="Close")
	public WebElement closeButton;

	public AdminFinanceConfigureFinanceDocumentUploadedPage(WebDriver driver) {

		PageFactory.initElements(driver, this);
	}
}
