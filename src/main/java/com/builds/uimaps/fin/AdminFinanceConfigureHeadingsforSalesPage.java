package com.builds.uimaps.fin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminFinanceConfigureHeadingsforSalesPage {
	

	@FindBy(xpath = "//a[contains(text(), 'Configure Headings for Sales')]")
	public WebElement ConfigureHeadingsforSales;

	

	AdminFinanceConfigureHeadingsforSalesPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
