package com.builds.uimaps.fin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminFinanceDeleteSalesReportsPage {

	@FindBy(xpath = ".//a[@qat_tabname='Payments']/span[contains(text(), 'Payments')]")
	public WebElement payments ;

	@FindBy(xpath = ".//a[@qat_tabname='Royalties']/span[contains(text(), 'Royalties')]")
	public WebElement royalties ;

	@FindBy(xpath = ".//a[@qat_tabname='Sales']/span[contains(text(), 'Sales')]")
	public WebElement sales ;

	@FindBy(id = "delete")
	public WebElement delete;

	@FindBy(name = "delete")
	public WebElement deleteSales;

	@FindBy(id = "resultsPerPage")
	public WebElement selectViewPerPage;

	@FindBy(id = "resultsPerPage")
	public WebElement selectViewPerPageAdmin;

	@FindBy(name="check")
	public WebElement deleteAllSalesCheckbox ;
	
	//@FindBy(xpath= ".//*[contains(text(), 'No records found.')]")
//	public WebElement noRecordFound;

	public AdminFinanceDeleteSalesReportsPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
