package com.builds.uimaps.fin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminFinanceEnableDisableFranchiseforEFTPage {

	@FindBy(xpath = "//a[contains(text(), 'Enable / Disable Franchise(s) for EFT')]")
	public WebElement enableDisableFranchiseforEFT ;

	@FindBy(id = "franchiseID")
	public WebElement selectfranchiseID;

	@FindBy(id = "areaID")
	public WebElement areaID;

	@FindBy(id = "searchButton")
	public WebElement searchBtn;

	@FindBy(id="resetButton")
	public WebElement resetButton;
	
	@FindBy(id = "finNachaFranchiseeDetails_1dataValue")
	public WebElement selectTransactionCode;

	@FindBy(id = "finNachaFranchiseeDetails_2dataValue")
	public WebElement receivingDFIIdentification;

	@FindBy(id = "finNachaFranchiseeDetails_3dataValue")
	public WebElement DFIAccountNumber;

	@FindBy(id = "finNachaFranchiseeDetails_5dataValue")
	public WebElement individualIdentificationNumber;

	@FindBy(id = "finNachaFranchiseeDetails_6dataValue")
	public WebElement individualName;

	@FindBy(name = "isEnableCheck")
	public WebElement checkbxEnableFranchiseForEFTTransaction;

	// for Area
	@FindBy(id="finNachaAreaDetails_1dataValue")
	public WebElement selectTransactionCodeArea;

	@FindBy(id="finNachaAreaDetails_2dataValue")
	public WebElement receivingDFIIdentificationArea;

	@FindBy(id="finNachaAreaDetails_3dataValue")
	public WebElement DFIAccountNumberArea;

	@FindBy(id="finNachaAreaDetails_5dataValue")
	public WebElement individualIdentificationNumberArea;

	@FindBy(id="finNachaAreaDetails_6dataValue")
	public WebElement individualNameArea;

	@FindBy(xpath = "//input[@value='Submit']")
	public WebElement submitBtn ;

	@FindBy(xpath = "//input[@value='OK']")
	public WebElement okBtn ;

	@FindBy(id = "20")
	public WebElement selectViewPerPage;

	@FindBy(xpath = ".//*[@name='20']")
	public WebElement viewPerPageSelect ;

	@FindBy(name = "Enable")
	public WebElement enableBottomBtn;

	@FindBy(name = "Disable")
	public WebElement disableBottomBtn;

	@FindBy(xpath = ".//input[@name='confirmation']")
	public WebElement okayBtn ;

	public AdminFinanceEnableDisableFranchiseforEFTPage(WebDriver driver) {

		PageFactory.initElements(driver, this);
	}

}
