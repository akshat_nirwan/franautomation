package com.builds.uimaps.fin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminFinanceFinanceSetupPreferencesPage {

	@FindBy(xpath = ".//*[@id='UPLDEditSpan0']/img")
	public WebElement salesReportSubmission ;
	
	@FindBy(xpath = ".//*[@id='UPLDSpan']/input[@value='0']")
	public WebElement webFormType ;

	@FindBy(xpath = ".//*[@id='UPLDSpan']/input[@value='1']")
	public WebElement csvUploadFileType ;

	@FindBy(xpath = ".//*[@id='UPLDSpan']/input[@value='2']")
	public WebElement xlsUploadFileType ;

	@FindBy(xpath = ".//*[@id='UPLDSaveCancelSpan']/a/img[@alt='Save']")
	public WebElement saveBtn ;

	@FindBy(xpath = ".//*[@id='INVEditSpan']/a/img[@alt='Change']")
	public WebElement changeGenerateInvoice ;

	@FindBy(xpath = ".//*[@id='INVSpan']//input[@name='invThread' and @value='1']")
	public WebElement yesInvoiceDate ;

	@FindBy(xpath = ".//*[@id='INVSpan']//input[@name='invThread' and @value='0']")
	public WebElement noInvoiceDate ;

	@FindBy(xpath = ".//*[@id='co_inv']/select[@name='invCycle']")
	public WebElement selectFrequency ;

	@FindBy(xpath = ".//*[@id='INVSaveCancelSpan']/a/img[@alt='Save']")
	public WebElement saveGenerateInvoice ;

	@FindBy(xpath = ".//span[@id='EFTEditSpan']//img[@alt='Change']")
	public WebElement editEFTOption;

	@FindBy(xpath = ".//span[@id='EFTSpan']//input[@value='1']")
	public WebElement yesEFTOption;

	@FindBy(xpath = ".//span[@id='EFTSpan']//input[@value='0']")
	public WebElement noEFTOption;
	
	@FindBy(xpath = ".//div[@id='eft_freq']/select[@name='eftCycle']")
	public WebElement selectEFTFrequency;
	
	@FindBy(xpath = ".//span[@id='EFTSaveCancelSpan']//img[@alt='Save']")
	public WebElement saveEFTOption;

	@FindBy(xpath = ".//span[@id='EFTSaveCancelSpan']//img[@alt='Cancel']")
	public WebElement cancelEFTOption;
	
	@FindBy(xpath = ".//*[@id='ADJEditSpan']/a/img[@alt='Change']")
	public WebElement adjustmentModify;

	@FindBy(xpath = ".//*[@id='ADJSpan']/input[@name='adjustment' and @value='1']")
	public WebElement yesAdjust;
	
	@FindBy(xpath = ".//*[@id='ADJSpan']/input[@name='adjustment' and @value='0']")
	public WebElement noAdjust;
	
	@FindBy(xpath = ".//a[@id='CTFININTEditSpan0']/img")
	public WebElement finCRMInt;
	
	@FindBy(xpath= ".//span[@id='CTFININTSpan']/input[1]")
	public WebElement ctfinintYesBtn;
	
	@FindBy(xpath= ".//span[@id='CTFININTSpan']/input[2]")
	public WebElement ctfinintNoBtn;

	@FindBy(xpath = ".//span[@id='CTFININTSaveCancelSpan']/a[2]/img[@alt='Save']")
	public WebElement ctfinIntSaveBtn;
	
	@FindBy(xpath = ".//*[@id='UPNLEditSpan0']/img")
	public WebElement profitLossSubmission;
	
	@FindBy(xpath = ".//*[@id='UPNLSpan']/input[@value='0']")
	public WebElement plWebFormType;

	@FindBy(xpath = ".//*[@id='UPNLSpan']/input[@value='1']")
	public WebElement plcsvUploadFileType;

	@FindBy(xpath = ".//*[@id='UPNLSpan']/input[@value='2']")
	public WebElement plxlsUploadFileType;

	@FindBy(xpath = ".//*[@id='UPNLSpan']/input[@value='3']")
	public WebElement QuickBooks;
	
	@FindBy(xpath = ".//*[@id='UPNLSaveCancelSpan']/a/img[@alt='Save']")
	public WebElement plSaveBtn;
	
	@FindBy(xpath = ".//span[@id='FREQEditSpan']/a[1]/img")
	public WebElement royaltyReportFrequency;
	
	@FindBy(xpath = ".//span[@id='FREQSpan']//input[@value='596']")
	public WebElement daily;

	@FindBy(xpath = ".//span[@id='FREQSpan']//input[@value='597']")
	public WebElement weekly;

	@FindBy(xpath = ".//span[@id='FREQSpan']//input[@value='598']")
	public WebElement every15Days;

	@FindBy(xpath = ".//span[@id='FREQSpan']//input[@value='599']")
	public WebElement monthly;
	
	@FindBy(xpath = ".//span[@id='FREQSaveCancelSpan']/a/img[@alt='Save']")
	public WebElement freSaveBtn;
	
	@FindBy(xpath = ".//span[@id='FILLEditSpan']/a[1]/img")
	public WebElement editFillPreviousReports;
	
	@FindBy(xpath = ".//*[@id='FILLSpan']/input[@value='1']")
	public WebElement fillPRYes;
	
	@FindBy(xpath= ".//*[@id='FILLSpan']/input[@value='0']")
	public WebElement fillPRNo;
	
	@FindBy(xpath =".//*[@id='COAPPREditSpan']/a[1]/img")
	public WebElement editCorpApovProcess;
	
	@FindBy(xpath =".//div[@id='COAPPRAlert']/span")
	public WebElement alertCorpApovProcess;
	
	@FindBy(xpath= ".//*[@id='NONFINEditSpan']/a[1]/img")
	public WebElement editEnableNonFinCategory;
	
	@FindBy(xpath = ".//span[@id='NONFINSpan']//input[@value='1']")
	public WebElement yesForEnableNonFinCat;
	
	@FindBy(xpath = ".//span[@id='NONFINSpan']//input[@value='0']")
	public WebElement noForEnableNonFinCat;
	
	@FindBy (xpath= ".//div[@id='kpi_val']/select")
	public WebElement enableKPI;
	
	@FindBy(xpath=".//span[@id='NONFINSaveCancelSpan']/a[2]/img")
	public WebElement saveEnableNonFinCat;
	
	@FindBy(xpath =".//*[@id='CURREditSpan']/a[1]/img")
	public WebElement editAllowSalesInLocal;
	
	@FindBy(xpath=".//span[@id='CURRSpan']/input[@value='1']")
	public WebElement yesAllowSalesInLocal;
	
	@FindBy(xpath =".//span[@id='negativeAmtEditSpan']/a[1]/img")
	public WebElement editAllowNegativeRoyaltyCalc;
	
	@FindBy(xpath= ".//span[@id='YTDMonthEditSpan']/a[1]/img")
	public WebElement editFISCALYearStartMonth;
	
	@FindBy(xpath = ".//div[@id='ytd_freq']/select")
	public WebElement selectFISCALStartMonth;
	
	@FindBy(xpath=".//span[@id='YTDMonthSaveCancelSpan']/a[2]/img")
	public WebElement saveFiscalMonth;
	
	@FindBy(xpath = ".//div[@id='YTDMonthAlert']/span[contains(text(), 'This preference is currently locked.')]")
	public WebElement preferenceLocked;
	
	@FindBy(xpath=".//span[@id='salesAcknowledgeEditSpan']/a[1]/img")
	public WebElement editAckProcessForSales;
		
	@FindBy(xpath= ".//a[@id='StartOfWeekEditSpan0']/img[@alt='Change']")
	public WebElement editStartFirstDayOfWeek;
	
	@FindBy(xpath=".//span[@id='StartOfWeekSpan']//select[@name='startOfWeek']")
	public WebElement selectFirstDayOfWeek;
	
	@FindBy(xpath = ".//div[@id='StartOfWeekSpan']/span[contains(text(), 'This preference is currently locked.')]")
	public WebElement prefLockedFirstDay;

	@FindBy(xpath = ".//span[@id='StartOfWeekSaveCancelSpan']//img[@alt='Save']")
	public WebElement saveFirstDay;
	
	@FindBy(xpath= ".//span[@id='AFEditSpan']/a[1]/img")
	public WebElement editAFSharePayment;
	
	@FindBy(xpath= ".//span[@id='AFSpan']//input[@value='1']")
	public WebElement yesAFSharePayment;
	
	@FindBy(xpath= ".//span[@id='AFSpan']//input[@value='0']")
	public WebElement noAFSharePayment;
	
	@FindBy(xpath = ".//div[@id='af_freq']/select[@name='afCycle']")
	public WebElement selectPayFreqForAFShare;
	
	@FindBy(xpath = ".//div[@id='af_wk']/select[@name='afCycleParam1WK']")
	public WebElement selectWeeklyDay;
	
	@FindBy(xpath = ".//div[@id='af_15']/select[@name='afCycleParam115']")
	public WebElement selectEvery15Day;
	
	@FindBy(xpath = ".//div[@id='af_mo']/select[@name='afCycleParam1MO']")
	public WebElement selectMonthDay;
	
	@FindBy(xpath = ".//div[@id='af_qtr']/select[@name='afCycleParam1Qtr']")
	public WebElement selectQuarterDay;
	
	@FindBy(xpath = ".//span[@id='AFSaveCancelSpan']//img[@alt='Save']")
	public WebElement saveAFShare;
	
	@FindBy(xpath=".//span[@id='ADVPAYEditSpan']//img[@alt='Change']")
	public WebElement editPaymentFeesOrAdditionalFeesSep;
	
	@FindBy(xpath=".//*[@id='CTFININTEditSpan0']/img")
	public WebElement financeAndCrmTransaction;
	
	@FindBy(xpath=".//span[@class='txt_alert_f' and contains(text(),'This preference is currently locked. Click')]")
	public WebElement isFinanceCRMTransaction;
	
	@FindBy(xpath=".//input[@name='ctfinint' and @value='1']")
	public WebElement noFinanceCRMTransaction;
	
	@FindBy(xpath=".//span[@id='CTFININTSaveCancelSpan']/a/img[@alt='Save']")
	public WebElement saveCRMFinanceTransaction;
	
	@FindBy(xpath=".//span[@id='LateFeeEditSpan']//img[@alt='Change']")
	public WebElement editAllowLateFee;
	
	@FindBy(xpath= ".//span[@id='LateFeeSpan']//input[@value='0']")
	public WebElement yesLateFee;
	
	@FindBy(xpath= ".//span[@id='LateFeeSpan']//input[@value='1']")
	public WebElement noLateFee;
	
	public AdminFinanceFinanceSetupPreferencesPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
