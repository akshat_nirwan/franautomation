package com.builds.uimaps.fin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminFinanceNonFinancialKPICategoriesSummaryPage {

	@FindBy(xpath = ".//input[@value='Add New Non-Financial Category']")
	public WebElement addNewNonFinancialCategory ;

	@FindBy(name = "name")
	public WebElement category;

	@FindBy(name = "submitButton")
	public WebElement submitButton;

	@FindBy(xpath = ".//input[@value='Close']")
	public WebElement closeBtn ;

	@FindBy(xpath = ".//select[@name='type']")
	public WebElement selectDataType ;

	public AdminFinanceNonFinancialKPICategoriesSummaryPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
