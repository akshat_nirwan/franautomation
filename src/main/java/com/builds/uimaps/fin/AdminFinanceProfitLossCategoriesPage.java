package com.builds.uimaps.fin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminFinanceProfitLossCategoriesPage {

	@FindBy(xpath = ".//div[@class='dropdown-action no-arrow three-dot-nav']//div[@data-role='ico_ThreeDots']")
	public WebElement clickButtonThreeDots ;
	
	@FindBy(linkText = "Add New Main Category")
	public WebElement addNewMainCategory ;
	
	@FindBy(linkText = "Order Main Category(s)")
	public WebElement orderMainCategory ;
	
	@FindBy(linkText = "Order Sub-Categories")
	public WebElement orderSubCategory;
	
	@FindBy(linkText = "Configure Additional Display Rows")
	public WebElement configureAdditionalDisplayRows ;
	
	@FindBy(id = "orderMain")
	public WebElement addNewRow;
	
	@FindBy(name = "categoryName")
	public WebElement categoryName;
	
	@FindBy(id = "calculationType1")
	public WebElement calculationMethod1;

	@FindBy(xpath = ".//label[@for='calculationType1']")
	public WebElement additionLabel ;

	@FindBy(id = "calculationType-1")
	public WebElement calculationMethod2;

	@FindBy(xpath = ".//label[@for='calculationType-1']")
	public WebElement substractionLabel ;

	@FindBy(id = "addModifyButton")
	public WebElement addButton;

	@FindBy(xpath = ".//input[@value='Close']")
	public WebElement closeBtn ;

	@FindBy(name = "displayName")
	public WebElement displayName;

	@FindBy(xpath = ".//*[@id='ms-parentcategoryID']")
	public WebElement selectCategory ;

	@FindBy(xpath = ".//input[@name='Add' and @value='Add']")
	public WebElement addBtn ;

	@FindBy(xpath = ".//input[@name = 'Modify' and @value='Modify']")
	public WebElement modifyBtn ;

	@FindBy(xpath=".//select[@name='templateOrder']")
	public WebElement selectCatForReorder;
	
	@FindBy(xpath = ".//img[@title='Move to Top']")
	public WebElement moveToTop;
	
	@FindBy(xpath = ".//img[@title='Move Up']")
	public WebElement moveUp;
	
	@FindBy(xpath = ".//img[@title='Move Down']")
	public WebElement moveDown;
	
	@FindBy(xpath = ".//img[@title='Move to Bottom']")
	public WebElement moveToBottom;
	
	@FindBy(xpath = ".//input[@value='Change Category Sequence']")
	public WebElement changeCategorySequence;
	
	public AdminFinanceProfitLossCategoriesPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
