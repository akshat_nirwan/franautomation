package com.builds.uimaps.fin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminFinancialsAgreementRulesPage {

	@FindBy(xpath = ".//*[@id='siteMainTable']/tbody/tr/td/table[1]/tbody/tr[5]/td[2]/div/table/tbody/tr[1]/td/table/tbody/tr/td[2]/a/span")
	public WebElement agreementRuleBtn ;

	@FindBy(xpath = ".//input[@value='Add Agreement Rule']")
	public WebElement addAgreementRule ;

	@FindBy(xpath = ".//input[@value='Continue']")
	public WebElement continueBtn ;

	@FindBy(id = "agreementRuleName")
	public WebElement agreementRuleName;

	@FindBy(id = "finAgreementRuleDetails_0appliedValue")
	public WebElement finAgreementRuleDetails_0appliedValue;

	@FindBy(name = "Submit")
	public WebElement submitBtn;

	public AdminFinancialsAgreementRulesPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
