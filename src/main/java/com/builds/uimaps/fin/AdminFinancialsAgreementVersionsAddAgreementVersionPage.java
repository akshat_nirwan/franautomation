package com.builds.uimaps.fin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminFinancialsAgreementVersionsAddAgreementVersionPage {

	@FindBy(xpath = ".//input[@value='Add Agreement']")
	public WebElement addAgreement ;

	@FindBy(id = "agreementName")
	public WebElement agreementName;

	@FindBy(id = "startDate")
	public WebElement startDate;

	@FindBy(id = "endDate")
	public WebElement endDate;

	@FindBy(id = "description")
	public WebElement description;

	@FindBy(id = "SubmitButton")
	public WebElement SubmitButton;

	public AdminFinancialsAgreementVersionsAddAgreementVersionPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
