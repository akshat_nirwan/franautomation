package com.builds.uimaps.fin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminFinancialsProfitLossCategoriesPage {

	@FindBy(linkText = "Add Income Category")
	public WebElement addIncomeCategoryLnk ;

	@FindBy(linkText = "Add Expense Category")
	public WebElement addExpenseCategoryLnk ;

	@FindBy(linkText = "Add Overhead Category")
	public WebElement addOverheadCategoryLnk ;

	@FindBy(xpath = ".//*[@id='siteMainTable']/tbody/tr/td/form[3]/table/tbody/tr[7]/td[2]/table/tbody/tr/td[1]/select")
	public WebElement listingIncomeCategory ;

	@FindBy(xpath = ".//*[@id='siteMainTable']/tbody/tr/td/form[4]/table/tbody/tr[4]/td[2]/table/tbody/tr/td[1]/select")
	public WebElement listingExpenseCategory ;

	@FindBy(xpath = ".//*[@id='siteMainTable']/tbody/tr/td/form[5]/table/tbody/tr[5]/td[2]/table/tbody/tr/td[1]/select")
	public WebElement listingOverheadCategory ;

	@FindBy(name = "categoryName")
	public WebElement categoryName;

	@FindBy(name = "Add")
	public WebElement addBtn;

	@FindBy(xpath = ".//input[@value='Close']")
	public WebElement closeBtn ;

	public AdminFinancialsProfitLossCategoriesPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
