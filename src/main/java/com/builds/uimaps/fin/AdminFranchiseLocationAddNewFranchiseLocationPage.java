package com.builds.uimaps.fin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminFranchiseLocationAddNewFranchiseLocationPage {

	@FindBy(id = "franchiseeName")
	public WebElement franchiseID;

	@FindBy(id = "centerName")
	public WebElement centerName;

	@FindBy(id = "areaID")
	public WebElement areaRegionName;

	@FindBy(id = "licenseNo")
	public WebElement licenseNo;

	@FindBy(id = "storeTypeId")
	public WebElement storeType;

	@FindBy(id = "versionID")
	public WebElement versionID;

	@FindBy(id = "status")
	public WebElement status;

	@FindBy(id = "reportPeriodStartDate")
	public WebElement RRSD;

	@FindBy(id = "taxRateId")
	public WebElement taxRate;
	
	@FindBy(id = "openingDate")
	public WebElement openingDate;

	@FindBy(id = "fbc")
	public WebElement FBC;

	@FindBy(id = "address")
	public WebElement address;

	@FindBy(id = "address2")
	public WebElement address2;

	@FindBy(id = "city")
	public WebElement city;

	@FindBy(id = "countryID")
	public WebElement Country;

	@FindBy(id = "zipcode")
	public WebElement zipcode;

	@FindBy(id = "regionNo")
	public WebElement StateProvince;

	@FindBy(id = "storePhone")
	public WebElement storePhone;

	@FindBy(id = "storePhoneExtn")
	public WebElement storePhoneExtn;

	@FindBy(id = "storeFax")
	public WebElement storeFax;

	@FindBy(id = "storeMobile")
	public WebElement storeMobile;

	@FindBy(id = "storeEmail")
	public WebElement storeEmail;

	@FindBy(id = "contactTitle")
	public WebElement contactTitle;

	@FindBy(id = "contactFirstName")
	public WebElement contactFirstName;

	@FindBy(id = "contactLastName")
	public WebElement contactLastName;

	@FindBy(id = "phone1")
	public WebElement phone1;

	@FindBy(id = "phone1Extn")
	public WebElement phone1Extn;

	@FindBy(id = "emailID")
	public WebElement emailID;

	@FindBy(id = "fax")
	public WebElement fax;

	@FindBy(id = "mobile")
	public WebElement mobile;

	@FindBy(id = "fimEntityID")
	public WebElement fimEntityID;

	@FindBy(id = "entityType")
	public WebElement entityType;

	@FindBy(id = "fimEntityDetail_0fimCbEntityType")
	public WebElement fimEntityDetail_0fimCbEntityType;

	@FindBy(id = "fimEntityDetail_0fimTtEntityName")
	public WebElement fimEntityDetail_0fimTtEntityName;

	@FindBy(id = "fimEntityDetail_0fimTtTaxpayer")
	public WebElement fimEntityDetail_0fimTtTaxpayer;

	@FindBy(id = "fimEntityDetail_0fimCbCountryOfFormation")
	public WebElement fimEntityDetail_0fimCbCountryOfFormation;

	@FindBy(id = "fimEntityDetail_0fimCbStateOfFormation")
	public WebElement fimEntityDetail_0fimCbStateOfFormation;

	@FindBy(css = "button.ms-choice")
	public WebElement buttonmsChoice;

	@FindBy(css = "input.searchInputMultiple")
	public WebElement inputsearchInputMultiple;

	@FindBy(id = "nametd")
	public WebElement nametd;

	@FindBy(id = "ownerType")
	public WebElement ownerType;

	@FindBy(name = "salutation")
	public WebElement salutation;

	@FindBy(id = "ownerFirstName")
	public WebElement ownerFirstName;

	@FindBy(id = "ownerLastName")
	public WebElement ownerLastName;

	@FindBy(id = "Submit")
	public WebElement submit;

	@FindBy(name = "franchiseMenu")
	public WebElement FranchiseIDSearch;

	@FindBy(xpath = "//input[@value='Add Franchise User']")
	public WebElement addFranchiseUser ;

	@FindBy(id = "franchiseeNo")
	public WebElement selectFranchiseID;

	@FindBy(name = "addUser")
	public WebElement addUserBtn;

	@FindBy(id = "userName")
	public WebElement loginid;

	@FindBy(xpath = "//input[@class='fTextBox' and @id='password']")
	public WebElement enterPass ;

	@FindBy(id = "confirmPassword")
	public WebElement confirmPass;

	@FindBy(id = "ms-parentroleID")
	public WebElement clickRole;

	@FindBy(xpath = "//input[@class='searchInputMultiple']")
	public WebElement searchRole ;

	@FindBy(xpath = "//span[@class='placeholder']")
	public WebElement selectRole ;

	@FindBy(name = "jobTitle")
	public WebElement jobTitle;

	@FindBy(id = "email")
	public WebElement ownerEmail;

	@FindBy(id = "searchButton")
	public WebElement searchFranchiseBtn;

	public AdminFranchiseLocationAddNewFranchiseLocationPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
