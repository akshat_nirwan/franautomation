package com.builds.uimaps.fin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FinanceKPIPage {

	@FindBy(xpath = ".//input[@value='Add Goals']")
	public WebElement addGoals ;

	@FindBy(xpath = ".//select[@name='reportPeriodCombo']")
	public WebElement selectGoalYear ;

	@FindBy(xpath = ".//*[@id='ms-parentcomboFranchisee']")
	public WebElement selectFranchise ;

	@FindBy(xpath = ".//a[@qat_tabname='Goals']")
	public WebElement goalsTab ;
	
	@FindBy(xpath = ".//a[@qat_tabname='Home']")
	public WebElement KpiHomeTab;
	
	@FindBy(xpath = ".//a[@qat_tabname='Dashboard']")
	public WebElement kpiDashboardTab;

	@FindBy(xpath = ".//input[@name='buttonSubmit']")
	public WebElement saveBtn ;

	@FindBy(name = "Back")
	public WebElement okayBtn;

	@FindBy(xpath = ".//*[@id='ms-parentfranchiseID']")
	public WebElement searchFranchiseId ;
	
	@FindBy(xpath = ".//*[@id='ms-parentcomboFranchisee1']")
	public WebElement searchFranchiseIdAtPop ;
	
	@FindBy(xpath = ".//input[@id='search']")
	public WebElement searchBtn ;

	@FindBy(xpath = ".//input[@value='Ok']")
	public WebElement okayFrame ;

	@FindBy(xpath = ".//input[@name='approveGoal']")
	public WebElement approveGoalBtn ;
	
	@FindBy(id="franchiseID")
	public WebElement selectFranchiseID;
		
	@FindBy(xpath = ".//input[@value='Get DashBoard']")
	public WebElement getKpiDashboard;

	public FinanceKPIPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
