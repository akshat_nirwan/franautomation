package com.builds.uimaps.fin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FinanceLateFeesPage {

	@FindBy()
	public WebElement test;

	public FinanceLateFeesPage(WebDriver driver) {

		PageFactory.initElements(driver, this);
	}
}
