package com.builds.uimaps.fin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FinanceOwnerSalesRoyaltiesPaymentsPage {

	@FindBy(xpath = ".//a[@class='userx']")
	public WebElement administratorlink;

	@FindBy(xpath = ".//a[contains(text(), 'Logout')]")
	public WebElement logout;

	@FindBy(id = "user_id")
	public WebElement loginID;

	@FindBy(id = "password")
	public WebElement password;

	@FindBy(id = "ulogin")
	public WebElement loginBtn;

	@FindBy(xpath = ".//div[@id='test1']/span/a")
	public WebElement moreBtn ;

	@FindBy(xpath = ".//li[@id='module_financials']/a")
	public WebElement financeBtn ;

	@FindBy(xpath = "//a[contains(text(), 'Sales')]")
	public WebElement salesReport ;

	@FindBy(xpath = "//a[contains(text(), 'Royalties')]")
	public WebElement royaltiesTab ;

	@FindBy(xpath = "//a[contains(text(), 'Payments')]")
	public WebElement paymentsTab ;

	@FindBy(xpath = "//a[contains(text(), 'Home')]")
	public WebElement homeFinance;

	@FindBy(id = "resultsPerPage")
	public WebElement selectViewPerPage;

	public FinanceOwnerSalesRoyaltiesPaymentsPage(WebDriver driver) {

		PageFactory.initElements(driver, this);

	}

}
