package com.builds.uimaps.fin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FinancePaymentsPage {

	@FindBy(xpath = ".//a[@qat_tabname='Credits / Debits']")
	public WebElement creaditDebitTab ;

	@FindBy(xpath = ".//input[@value='Add New Memo']")
	public WebElement addCreditsDebitsMemo ;

	@FindBy(id = "FRANCHISE_ID")
	public WebElement memoFranchiseID;

	@FindBy(name = "memoType")
	public WebElement memoType;

	@FindBy(id = "amount")
	public WebElement memoAmount;

	@FindBy(id = "memoDate")
	public WebElement memoDate;

	@FindBy(id = "memo")
	public WebElement memoComments;

	@FindBy(xpath = "//input[@value='Save']")
	public WebElement saveMemeoBtn ;

	@FindBy(id = "resultsPerPage")
	public WebElement resultsPerPage;

	@FindBy(id = "dateReceived")
	public WebElement recievedDate;

	@FindBy(name = "amount")
	public WebElement paymentAmount;

	@FindBy(id = "refNo")
	public WebElement refrenceNo;

	@FindBy(xpath = ".//input[@name='collectionMethod' and @value='409']")
	public WebElement collectionMethodsCash ;

	@FindBy(xpath = ".//input[@name='collectionMethod' and @value='408']")
	public WebElement collectionMethodsCheque ;

	@FindBy(xpath = ".//input[@name='collectionMethod' and @value='410']")
	public WebElement collectionMethodsCreditCard ;

	@FindBy(id = "memo")
	public WebElement comments;

	@FindBy(name = "apply")
	public WebElement saveBtn;

	@FindBy(id = "chequeNo")
	public WebElement chequeNo;

	@FindBy(id = "creditCardType")
	public WebElement selectCreditType;

	public FinancePaymentsPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
