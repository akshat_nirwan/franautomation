package com.builds.uimaps.fin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FinanceProfitLossStatementPage {

	@FindBy(xpath = ".//li[@id='module_financials']/ul/li[4]/a")
	public WebElement plStatementTab ;

	@FindBy(xpath = ".//td[@class='tab-selected1']/a/span")
	public WebElement profitLossSummary ;

	@FindBy(xpath = ".//input[@value='New Profit / Loss Statement']")
	public WebElement newProfitLossStatement;

	@FindBy(xpath = ".//input[@value='Upload Profit / Loss Statement']")
	public WebElement uploadProfitLossStatement;
	
	@FindBy(xpath= ".//a[contains(text(), 'Download Sample')]")
	public WebElement downloadSample;
	
	@FindBy(xpath =".//input[@name='uploadFileName']")
	public WebElement clickBrowseBtnUploadFile;
	
	@FindBy(id = "comboFranchisee")
	public WebElement selectFranchise;

	@FindBy(id = "comboReportPeriod")
	public WebElement selectStatementPeriod;

	@FindBy(id = "continue")
	public WebElement continueBtn;

	@FindBy(name = "finPlReportIncome_0amount")
	public WebElement finPlReportIncomeAmount;

	@FindBy(name = "finPlReportExpense_0amount")
	public WebElement finPlReportExpenseAmount;

	@FindBy(name = "finPlReportOverhead_0amount")
	public WebElement finPlReportOverheadAmount;

	@FindBy(name = "netProfit")
	public WebElement netProfitLoss;

	@FindBy(name = "buttonSubmit")
	public WebElement submitBtn;

	@FindBy(xpath = ".//input[@value='OK']")
	public WebElement okBtnPLS;

	@FindBy(xpath = ".//td[contains(text(), 'Net Profit ($)')]")
	public WebElement clickNetPrft ;

	@FindBy(xpath = ".//div[@id='ms-parentfranchiseID']/button[@class='ms-choice']")
	public WebElement clickFranchiseIDfilter ;

	@FindBy(xpath = ".//div[@id='ms-parentstoreType']/button[@class='ms-choice']")
	public WebElement storeTypeFilter;

	@FindBy(xpath = ".//div[@id='ms-parentmenu']/button")
	public WebElement gaolYearFilter ;

	@FindBy(xpath = ".//input[@id='search']")
	public WebElement searchBtn ;

	@FindBy(xpath = ".//a[@id='showFilter']")
	public WebElement showFilter;

	@FindBy(name = "plPeriodFrom")
	public WebElement plPeriodFrom;

	@FindBy(name = "plPeriodTo")
	public WebElement plPeriodTo;

	@FindBy(id = "plSubmitDateFrom")
	public WebElement plSubmitDateFrom;

	@FindBy(id = "plSubmitDateTo")
	public WebElement plSubmitDateTo;

	@FindBy(name = "incomeCondition")
	public WebElement incomeCondition;

	@FindBy(id = "incomeValue")
	public WebElement incomeValue;

	@FindBy(name = "expenseCondition")
	public WebElement expenseCondition;

	@FindBy(id = "expenseValue")
	public WebElement expenseValue;

	@FindBy(name = "overheadCondition")
	public WebElement overheadCondition;

	@FindBy(id = "overheadValue")
	public WebElement overheadValue;

	@FindBy(name = "profitCondition")
	public WebElement profitCondition;

	@FindBy(id = "profitValue")
	public WebElement profitValue;

	@FindBy(name = "submissionDate")
	public WebElement submissionDate;

	@FindBy(name = "finPlReportDetails_1amount")
	public WebElement incomeTxtBx;

	@FindBy(name = "finPlReportDetails_2amount")
	public WebElement expenseTxtBx;

	@FindBy(name = "finPlReportDetails_3amount")
	public WebElement OverheadTxBx;

	@FindBy(name = "back")
	public WebElement backBtn;

	@FindBy(xpath = ".//*[@id='ms-parentfranchiseID']")
	public WebElement selectFranchiseId ;

	@FindBy(xpath = ".//*[contains(text () , 'DashBoard')]")
	public WebElement dashboardTab ;

	@FindBy(xpath = ".//select[@id='franchiseID']")
	public WebElement selectFranchiseIdAtDash ;

	@FindBy(id = "franchiseProfitHome_month")
	public WebElement selectMonth;

	@FindBy(name = "continue")
	public WebElement contFileUploadBtn;
	
	@FindBy(xpath = ".//input[@name='backButton']")
	public WebElement okBtn;
	
	@FindBy(xpath = ".//form[@name='uploadData']//tr[4]/td[2]/input[@value='Upload Data']")
	public WebElement uploadBtn;
	
	@FindBy(name="skipFlag")
	public WebElement skipDataMapping;
	
	public FinanceProfitLossStatementPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
