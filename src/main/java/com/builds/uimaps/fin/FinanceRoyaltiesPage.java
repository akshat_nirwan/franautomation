package com.builds.uimaps.fin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FinanceRoyaltiesPage {

	@FindBy(xpath = ".//div[@id='test1']/span/a")
	public WebElement moreBtn ;

	@FindBy(xpath = ".//li[@id='module_financials']/a")
	public WebElement financeBtn ;

	@FindBy(xpath = "//a[contains(text(), 'Royalties')]")
	public WebElement royaltiesTab ;

	@FindBy(xpath = "//input[@name='search']")
	public WebElement searchBtn ;

	@FindBy(xpath = "//a[contains(text(), 'Payments')]")
	public WebElement paymentsTab ;

	@FindBy(xpath = ".//input[@name='PAYMENT_FOR' and @value='A1']")
	public WebElement selectPaymentAppliedforRoyalty ;

	@FindBy(xpath = "//input[@value='A2']")
	public WebElement selectPaymentAppliedforAdv ;

	@FindBy(xpath = "//input[@value='2']")
	public WebElement selectPaymentAppliedforAdditionalfees ;

	@FindBy(xpath = ".//input[@value='3']")
	public WebElement selectPaymentAppliedforTax ;

	@FindBy(xpath = ".//input[@value='L21']")
	public WebElement selectPaymentAppliedforLateFee ;

	@FindBy(xpath = "//input[@value='Y' and @name ='PAYMENT_FOR']")
	public WebElement selectPaymentAppliedforAll ;

	@FindBy(xpath = "//input[@value='Continue']")
	public WebElement continueBtn ;

	@FindBy(xpath = "//input[@name='amount']")
	public WebElement paymentAmount ;

	@FindBy(id = "refNo")
	public WebElement referenceNo;

	@FindBy(xpath = "//input[@value='408']")
	public WebElement collectionMethodCheque ;

	@FindBy(xpath = "//input[@value='410']")
	public WebElement collectionMethodCreditCard ;

	@FindBy(xpath = "//input[@value='409']")
	public WebElement collectionMethodCash ;

	@FindBy(id = "chequeNo")
	public WebElement chequeNo;

	@FindBy(id = "creditCardType")
	public WebElement creditCardType;

	@FindBy(id = "memo")
	public WebElement comments;

	@FindBy(name = "amountTOTAL_PAY")
	public WebElement amountToApply;

	@FindBy(name = "TotalAppliedAmt")
	public WebElement appliedAmount;

	@FindBy(name = "CREDITED_AMT")
	public WebElement amountToCredit;

	@FindBy(name = "apply")
	public WebElement saveBtn;

	@FindBy(xpath = ".//u[contains(text(), 'Add Line Items')]")
	public WebElement addLineItems ;

	@FindBy(id = "popifr")
	public WebElement lineItems;

	@FindBy(id = "itemDescription")
	public WebElement itemDescription;

	@FindBy(name = "adjType")
	public WebElement adjType;

	@FindBy(id = "amount")
	public WebElement adjAmount;

	@FindBy(name = "submitButton")
	public WebElement saveLineItem;

	@FindBy(name = "payment")
	public WebElement applyPaymentInvoiceDetails;

	@FindBy(xpath = "//table[@class='subTabs']//span[contains(text(), 'Credits / Debits')]")
	public WebElement creditsDebitsLnk ;

	@FindBy(xpath = ".//input[@value='Add New Memo']")
	public WebElement addCreditsDebitsMemo ;

	@FindBy(id = "FRANCHISE_ID")
	public WebElement memoFranchiseID;

	@FindBy(name = "memoType")
	public WebElement memoType;

	@FindBy(id = "amount")
	public WebElement memoAmount;

	@FindBy(id = "memoDate")
	public WebElement memoDate;

	@FindBy(id = "memo")
	public WebElement memoComments;

	@FindBy(xpath = "//input[@value='Save']")
	public WebElement saveMemeoBtn ;

	@FindBy(id = "franchiseeNo")
	public WebElement franchiseIDforPayment;

	@FindBy(xpath = ".//span[contains(text(), 'Apply / Receive Payment')]")
	public WebElement applyReceivePaymentLnk ;

	@FindBy(xpath = ".//a[@qat_tabname='Payment Summary']")
	public WebElement paymentSummaryTab ;

	@FindBy(xpath = ".//a[@id='showFilter']")
	public WebElement showFilterLnk ;

	@FindBy(xpath = ".//div[@id='ms-parentfranchiseID']//button[@class='ms-choice']")
	public WebElement clickFranchiseIDfilter ;

	@FindBy(xpath = ".//div[@id='ms-parentstoreType']//button[@class='ms-choice']")
	public WebElement storeTypeFilter;

	@FindBy(id = "invoiceDateFrom")
	public WebElement invoiceDateFrom;

	@FindBy(id = "invoiceDateTo")
	public WebElement invoiceDateTo;

	@FindBy(id = "reportPeriodStartFrom")
	public WebElement reportPeriodStartFrom;

	@FindBy(id = "reportPeriodStartTo")
	public WebElement reportPeriodStartTo;

	@FindBy(xpath = ".//div[@id='ms-parentpaymentStatus']//button[@class='ms-choice']//span")
	public WebElement paymentStatusBtn ;

	@FindBy(xpath = ".//div[@id='ms-parentfranchiseID']//input[@id='selectAll']")
	public WebElement checkSelectAllCheckbox ;

	@FindBy(xpath = ".//div[@id='ms-parentpaymentStatus']//input[@class='searchInputMultiple']")
	public WebElement paymentStatusSearch;

	@FindBy(xpath = ".//input[@id='search']")
	public WebElement searchFilterBtn ;

	@FindBy(id = "dateAppliedFrom")
	public WebElement dateAppliedFrom;

	@FindBy(id = "dateAppliedTo")
	public WebElement dateAppliedTo;

	@FindBy(id = "invoiceNo")
	public WebElement invoiceNo;

	@FindBy(xpath = ".//div[@id='ms-parentfranchiseID']//button[@class='ms-choice']")
	public WebElement franchiseIDFilteratPaymentSummary;

	@FindBy(id = "memoDateFrom")
	public WebElement memoDateFrom;

	@FindBy(id = "memoDateTo")
	public WebElement memoDateTo;

	@FindBy(xpath = ".//input[@value='C']")
	public WebElement creditMemoCheck;

	@FindBy(xpath = ".//input[@value='D']")
	public WebElement debitMemoCheck ;

	@FindBy(xpath = ".//input[@value='P']")
	public WebElement paymentMemoCheck;

	public FinanceRoyaltiesPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
