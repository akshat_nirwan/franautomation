package com.builds.uimaps.fin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FinanceSalesPage {

	@FindBy(xpath = ".//*[@id='test1']/span/a/span/span")
	public WebElement moreBtn ;

	@FindBy(xpath = ".//a[@qat_module='financials']")
	public WebElement financeBtn ;

	@FindBy(xpath = ".//*[@id='module_financials']//a[@ qat_submodule='Sales']")
	public WebElement salesReport ;

	@FindBy(xpath = ".//input[@value='Enter Sales Report' or contains(@value,'Sales') and not(@type='hidden')]")
	public WebElement enterSalesReport ;
	
	@FindBy(xpath=".//input[@value='Upload Sales Report']")
	public WebElement uploadSalesReport;

	@FindBy(xpath= ".//a[contains(text(), 'Download Sample')]")
	public WebElement downloadSample;
	
	@FindBy(name = "Approve")
	public WebElement approveBtn;

	@FindBy(id = "comboFranchisee")
	public WebElement selectFranchise;

	@FindBy(name = "reportPeriodCombo")
	public WebElement selectReportPeriod;

	//@FindBy(xpath= ".//*[contains(text(), '2017')]")
	//public WebElement selectedReportPeriod;
	
	@FindBy(id = "continue")
	public WebElement ContinueBtn;

	@FindBy(name = "finSalesReportDetails_0quantity")
	public WebElement serviceQuantity;

	@FindBy(name = "finSalesReportDetails_1quantity")
	public WebElement serviceQuantity1;

	@FindBy(name = "finSalesReportDetails_0amount")
	public WebElement serviceAmount;

	@FindBy(name = "finSalesReportDetails_1amount")
	public WebElement serviceAmount1;

	@FindBy(name = "finSalesReport_0totalQuantity")
	public WebElement totalQuantityCalculated;

	@FindBy(name = "finSalesReport_0totalSales")
	public WebElement totalSalesCalculated;

	@FindBy(name = "finSalesReport_0memo")
	public WebElement salesComment;;

	@FindBy(xpath = ".//input[@value='Save']")
	public WebElement SaveBtn ;

	@FindBy(name = "buttonSubmit")
	public WebElement submitBtn;

	@FindBy(name = "backButton")
	public WebElement okayBtn;

	@FindBy(id = "resultsPerPage")
	public WebElement resultsPerPage;

	@FindBy(name = "finAddlFeesDetails_0amount")
	public WebElement additionalFees;

	@FindBy(id = "showFilter")
	public WebElement showFilter;

	@FindBy(xpath = ".//*[@id='ms-parentfranchiseID']")
	public WebElement selectFranchiseIdFilter ;

	@FindBy(xpath = ".//input[@id='search']")
	public WebElement searchBtnFilter ;

	@FindBy(id="resetButton")
	public WebElement resetBtn;
	
	@FindBy(id = "hideFilter")
	public WebElement hideFilter;

	@FindBy(xpath = ".//td[contains(text () , 'Report ID')]/following-sibling::td[1]")
	public WebElement reportId ;

	/*********** Acknowledgement and Confirmation Page ***********/

	@FindBy(xpath = "//tr[td[contains(text(), 'Total Sales ($)')]]/td[2]")
	public WebElement totalSalesAmount ;

	@FindBy(xpath = "//tr[td[contains(text(), 'Royalty ($')]]/td[2]")
	public WebElement totalRoyalty ;

	@FindBy(xpath = "//tr[td[contains(text(), 'Adv. ($)')]]/td[2]")
	public WebElement totalAdv ;

	@FindBy(xpath = "//tr[td[contains(text(), 'Royalty (Area Franchise) ($)')]]/td[2]")
	public WebElement totalRoyaltyAreaFranchise ;

	@FindBy(xpath = "//tr[td[contains(text(), 'Adv. (Area Franchise) ($)')]]/td[2]")
	public WebElement totalAdvAreaFranchise ;

	@FindBy(name = "disclaimer")
	public WebElement acknowledgement;

	@FindBy(xpath = "//tr[td[contains(text(), 'Report ID')]]/td[4]")
	public WebElement reportID ;

	@FindBy(xpath = "//input[@value='OK']")
	public WebElement okBtn ;

	@FindBy(xpath = ".//input[@value='Cancel']")
	public WebElement cancelAtAck ;

	@FindBy(xpath = "//a[contains(text(), 'Report ID')]")
	public WebElement reportIDcolumn ;

	@FindBy(name = "Acknowledge")
	public WebElement AcknowledgeBtn;

	@FindBy(name = "Reject")
	public WebElement rejectBtn;

	@FindBy(id = "20")
	public WebElement selectViewPerPage;

	@FindBy(xpath = "//a[contains(text(), 'Royalties')]")
	public WebElement royaltiesTab;

	@FindBy(xpath = "//a[contains(text(), 'Store Summary')]")
	public WebElement storeSummaryTab;

	public WebElement commentsIFrame;

	@FindBy(xpath = ".//table[@id='popupMainTable']//textarea[@id='comments']")
	public WebElement commentsApproveReject ;

	@FindBy(xpath = ".//textarea[@id='comments' and @class='fTextBox']")
	public WebElement commentsDeleteSales;

	@FindBy(id = "comments")
	public WebElement commentTxtArea;

	@FindBy(xpath = "//input[@value='Save']")
	public WebElement saveBtnApproveReject ;

	@FindBy(xpath = "//input[@value='Delete Report']")
	public WebElement deleteBtn ;

	@FindBy(name = "Upload")
	public WebElement uploadBtn;

	@FindBy(name = "back")
	public WebElement backBtn;

	@FindBy(xpath = "//input[@value='Close']")
	public WebElement cloaseBtn ;

	@FindBy(xpath = ".//a[@id='showFilter']")
	public WebElement showFilters ;

	@FindBy(xpath = "//input[@name='search']")
	public WebElement searchBtn ;

	@FindBy(xpath = ".//input[@type='checkbox' and @name='check']")
	public WebElement selectAllReportID ;

	@FindBy(id = "franchiseID")
	public WebElement searchFranchiseID;

	@FindBy(id = "getSummary")
	public WebElement getSummary;

	@FindBy(xpath = ".//a[@id='showFilter']")
	public WebElement showFilterLnk ;

	@FindBy(xpath = ".//div[@id='ms-parentfranchiseID']/button[@class='ms-choice']")
	public WebElement clickFranchiseIDfilter ;

	@FindBy(xpath = ".//div[@id='ms-parentstoreType']/button[@class='ms-choice']")
	public WebElement storeTypeFilter;

	@FindBy(id = "salesDateFrom")
	public WebElement reportPeriodFrom;

	@FindBy(id = "salesDateTo")
	public WebElement reportPeriodTo;

	@FindBy(id = "reportSubmitDateFrom")
	public WebElement submissionDateFrom;

	@FindBy(id = "reportSubmitDateTo")
	public WebElement submissionDateTo;

	@FindBy(id = "reportStatus")
	public WebElement reportStatus;

	@FindBy(name = "menu")
	public WebElement totalSalesFilter;

	@FindBy(xpath = ".//input[@value='M']")
	public WebElement royaltyReportCheckbox;

	@FindBy(xpath = ".//input[@value='X']")
	public WebElement adjReportCheckbox;

	@FindBy(id = "textbox1")
	public WebElement totalSalesFilterAmount;

	@FindBy(xpath = ".//input[@id='search']")
	public WebElement searchFilterBtn;

	@FindBy(xpath = ".//*[@id='module_financials']//a[@qat_submodule='Royalties']")
	public WebElement royalityPage;

	@FindBy(xpath = ".//input[@name='check']")
	public WebElement allCheckedValue;

	@FindBy(name = "finSalesReport_0dateReceived")
	public WebElement rcvdDate;

	@FindBy(xpath = ".//select[@name='reportPeriodCombo']/option[@selected='']")
	public WebElement getSelectdReportId;

	@FindBy(name="skipFlag")
	public WebElement skipDataMapping;
	
	@FindBy(name = "salesFileName")
	public WebElement clickBrowseBtnUploadFile;
	
	@FindBy(xpath=".//form[@name='uploadData']//tr[4]/td[2]/input[@value='Upload Data']")
	public WebElement fileUploadBtn;
	
	@FindBy(name="continue")
	public WebElement continueUploadBtn;
	
	@FindBy(xpath = ".//input[@name='backButton']")
	public WebElement OKBtn;
		
	public FinanceSalesPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
