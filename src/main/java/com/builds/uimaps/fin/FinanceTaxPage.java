package com.builds.uimaps.fin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FinanceTaxPage {

	@FindBy(xpath = ".//span[contains(text(), 'Tax Types')]")
	public WebElement taxTypes ;

	@FindBy(xpath = ".//input[@value='Add Tax Type']")
	public WebElement addTaxTypes ;

	@FindBy(id = "taxTypeName")
	public WebElement taxTypeName;

	@FindBy(id = "description")
	public WebElement description;

	@FindBy(name = "Addbutton888")
	public WebElement addTaxBtn;

	@FindBy(name = "button")
	public WebElement closeBtn;

	@FindBy(xpath = ".//a[@href='finTaxRatesSummary']")
	public WebElement taxRates ;

	@FindBy(xpath = ".//a[contains(text(), 'Created On')]")
	public WebElement createdOnBtn ;

	@FindBy(xpath = ".//a[contains(text(), 'Creation Date')]")
	public WebElement creationDateBtn ;

	@FindBy(xpath = ".//input[@value='Add Tax Rate']")
	public WebElement addTaxRate ;

	@FindBy(id = "20")
	public WebElement selectViewPerPage;

	@FindBy(id = "taxRateName")
	public WebElement taxRateName;

	@FindBy(xpath = ".//input[@value='Save']")
	public WebElement saveTaxBtn ;

	@FindBy(xpath = ".//input[@value='Close']")
	public WebElement closeTaxBtn ;

	@FindBy(xpath = ".//input[@value='Modify']")
	public WebElement modifyTax ;

	@FindBy(xpath = ".//input[@value='Close']")
	public WebElement closeModifyTaxBtn ;

	@FindBy(id = "franId")
	public WebElement selectFranchiseID;

	@FindBy(name = "checkAllNOTR")
	public WebElement checkAllNOTR;

	@FindBy(xpath = ".//input[@value='Map']")
	public WebElement mapBtn ;

	/*
	 * @FindBy(xpath = ".//span[contains(text(), 'Tax Types')]") public WebElement
	 * taxTypes=DDDDDDDDDDDDDDDDDDDDD
	 * 
	 * @FindBy(xpath = ".//span[contains(text(), 'Tax Types')]") public WebElement
	 * taxTypes=DDDDDDDDDDDDDDDDDDDDD
	 * 
	 * @FindBy(xpath = ".//span[contains(text(), 'Tax Types')]") public WebElement
	 * taxTypes=DDDDDDDDDDDDDDDDDDDDD
	 * 
	 * @FindBy(xpath = ".//span[contains(text(), 'Tax Types')]") public WebElement
	 * taxTypes=DDDDDDDDDDDDDDDDDDDDD
	 * 
	 * @FindBy(xpath = ".//span[contains(text(), 'Tax Types')]") public WebElement
	 * taxTypes=DDDDDDDDDDDDDDDDDDDDD
	 * 
	 * @FindBy(xpath = ".//span[contains(text(), 'Tax Types')]") public WebElement
	 * taxTypes=DDDDDDDDDDDDDDDDDDDDD
	 * 
	 * @FindBy(xpath = ".//span[contains(text(), 'Tax Types')]") public WebElement
	 * taxTypes=DDDDDDDDDDDDDDDDDDDDD
	 * 
	 */

	@FindBy(id = "enableTaxRates")
	public WebElement yesBtnConfigTaxRatesHidnLnk;
	
	@FindBy (id= "Submit")
	public WebElement submitBtn;
	
	public FinanceTaxPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
