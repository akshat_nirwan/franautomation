package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminCorporateUserUI {
	
	// User Details
	
	@FindBy(id="userName")
	public WebElement loginID; 
	
	@FindBy(xpath=".//input[@type='password' and @id='password']")
	public WebElement password;
	
	@FindBy(id="confirmPassword")
	public WebElement confirmPassword;
	
	@FindBy(id="userType")
	public WebElement userType;
	
	@FindBy(id="expiryDays")
	public WebElement expirationTime;
	
	@FindBy(id="ms-parentroleID")
	public WebElement role;
	
	@FindBy(id="timezone")
	public WebElement timeZone;
	
	@FindBy(id="isDaylight")
	public WebElement allowDST;
	
	// Personal Details

	@FindBy(id="jobTitle")
	public WebElement jobTitle;
	
	@FindBy(id="userLanguage")
	public WebElement language;
	
	@FindBy(id="firstName")
	public WebElement firstName;
	
	@FindBy(id=	"lastName")
	public WebElement lastName;
	
	@FindBy(id="address")
	public WebElement address;
	
	@FindBy(id="city")
	public WebElement city;
	
	@FindBy(id="country")
	public WebElement country;
	
	@FindBy(id="zipcode")
	public WebElement zipPostalCode;
	
	@FindBy(id="state")
	public WebElement stateProvince;
	
	@FindBy(id="phone1")
	public WebElement phone1;
	
	@FindBy(id="phoneExt1")
	public WebElement phone1Extension;
	
	@FindBy(id="phone2")
	public WebElement phone2;
	
	@FindBy(id="phoneExt2")
	public WebElement phone2Extension;
	
	@FindBy(id="fax")
	public WebElement fax;
	
	@FindBy(id="mobile")
	public WebElement mobile;
	
	@FindBy(id="email")
	public WebElement email;
	
	@FindBy(id="loginUserIp")
	public WebElement ipAddress;
	
	@FindBy(id="isBillable")
	public WebElement isBillable;
	
	@FindBy(id="auditor")
	public WebElement consultant;
	
	@FindBy(id="userPictureName")
	public WebElement uploadUserPicture;
	
	@FindBy(id="sendNotification")
	public WebElement sendNotification;
	
	// Buttons
	
	@FindBy(id="Submit")
	public WebElement addBtn;
	
	@FindBy(name="Reset")
	public WebElement resetBtn;
	
	@FindBy(id="Button1")
	public WebElement cancelBtn;
	
	// Manage Corporate Users
	
	@FindBy(xpath=".//input[@type='button' and @value='Add Corporate User']")
	public WebElement addCorporateUserBtn;
	
	@FindBy(xpath=".//input[@type='button' and @value='Send Message']")
	public WebElement sendMessageBtn;
	
	@FindBy(xpath=".//input[@type='button' and @value='Change Password']")
	public WebElement changePasswordBtn;
	
	@FindBy(xpath=".//input[@type='button' and @value='Print']")
	public WebElement printBtn;
	
	@FindBy(linkText="Deactivated Users")
	public WebElement deactivatedUsers_Tab;
	
	@FindBy(linkText="Active Users")
	public WebElement activeUsers_Tab;
	
	// Search
	
	@FindBy(id="search")
	public WebElement searchByUserName_TextBox;
	
	@FindBy(id="searchButton")
	public WebElement searchBtn;
	
	//Deletion of Corporate User - on confirmation box - iframe
	
	@FindBy(xpath=".//input[@type='button' and value='Delete']")
	public WebElement delete_Btn;
	
	@FindBy(xpath=".//input[@type='button' and @name='Close']")
	public WebElement closeBtn_onConfirmation;
	
	
	public AdminCorporateUserUI(WebDriver driver)
	{
		PageFactory.initElements(driver, this);
	}

}
