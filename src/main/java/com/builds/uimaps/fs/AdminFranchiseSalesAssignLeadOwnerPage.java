package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminFranchiseSalesAssignLeadOwnerPage {
	@FindBy(name = "area1")
	public WebElement defaultOwner;

	@FindBy(xpath = ".//input[@name='submit' and @value='Update']")
	public WebElement updateBtn ;

	@FindBy(name = "Button22")
	public WebElement continueBtn;

	@FindBy(name = "Button")
	public WebElement backBtn;

	public AdminFranchiseSalesAssignLeadOwnerPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
