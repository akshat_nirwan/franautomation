package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminFranchiseSalesBrokersAgencySummaryAddBrokersAgencyPage {

	@FindBy(xpath = ".//input[@value='Add Broker Agency' or @value='Add Brokers Agency']")
	public WebElement addBrokerAgencyLnk ;

	@FindBy(id = "agencyName")
	public WebElement agencyName;

	@FindBy(id = "salutation")
	public WebElement salutation;

	@FindBy(id = "ownerFirstName")
	public WebElement ownerFirstName;

	@FindBy(id = "ownerLastName")
	public WebElement ownerLastName;

	@FindBy(id = "address1")
	public WebElement address1;

	@FindBy(id = "address2")
	public WebElement address2;

	@FindBy(id = "Submit")
	public WebElement submitBtn;

	public AdminFranchiseSalesBrokersAgencySummaryAddBrokersAgencyPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
