package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminFranchiseSalesBrokersAgencySummaryPage {

	@FindBy(xpath = ".//input[@value='Add Broker Agency' or @value='Add Brokers Agency']")
	public WebElement addBrokerAgencyLnk ;

	public AdminFranchiseSalesBrokersAgencySummaryPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
