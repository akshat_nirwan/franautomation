package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminFranchiseSalesBrokersTypeConfigurationPage {

	@FindBy(xpath = ".//input[@value='Add Brokers Type' or @value='Add Broker Type']")
	public WebElement addBrokerType ;

	@FindBy(name = "contactTypeName")
	public WebElement brokerType;

	@FindBy(xpath = ".//input[@value='Add']")
	public WebElement addBtn ;

	@FindBy(xpath = ".//input[@value='Close']")
	public WebElement closeBtn ;

	@FindBy(xpath = ".//*[@id='siteMainTable']//input[@name='emailverification1']/ancestor::td/input[2]")
	public WebElement isSubMenu ;

	@FindBy(xpath = ".//*[@id='siteMainTable']//input[@name='Save']")
	public WebElement moduleSave ;

	@FindBy(xpath = ".//input[@value='Modify']")
	public WebElement modifybtn ;

	@FindBy(xpath = ".//input[@value='Delete']")
	public WebElement deletebtn ;

	public AdminFranchiseSalesBrokersTypeConfigurationPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
