package com.builds.uimaps.fs;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminFranchiseSalesConfigureCoApplicantRelationshipPage {

	@FindBy(xpath = ".//*[@id='siteMainTable']/tbody/tr/td/table/tbody/tr[4]/td[2]/table/tbody/tr/td/form/table/tbody/tr[1]/td/table/tbody/tr/td[2]/input")
	public WebElement addCoApplicantRelationship ;

	@FindBy(name = "coApplicantRelationship")
	public WebElement coApplicantRelationshipName;

	
	@FindBy(name = "Submit")
	public WebElement add;

	
	@FindBy(name = "Submit")
	public WebElement close;

	@FindBy(xpath = ".//*[@id='siteMainTable']/tbody/tr/td/table/tbody/tr[4]/td[2]/table/tbody/tr/td/form/table/tbody/tr[2]/td/table/tbody//td[1]")
	public List<WebElement> coapplicantRelationshiplisting;

	public AdminFranchiseSalesConfigureCoApplicantRelationshipPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
