package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminFranchiseSalesConfigureMarketingCostsAddMarketingCostsPage {

	@FindBy(name = "costSheetYear")
	public WebElement yearDrp;

	@FindBy(name = "costSheetType")
	public WebElement costSheetTypeDrp;

	@FindBy(xpath = ".//div[@id='forYear']/table/tbody/tr[4]/td[14]/input")
	public WebElement totalCost1 ;

	public AdminFranchiseSalesConfigureMarketingCostsAddMarketingCostsPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
