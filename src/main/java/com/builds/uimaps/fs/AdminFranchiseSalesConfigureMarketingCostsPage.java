package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminFranchiseSalesConfigureMarketingCostsPage {

	@FindBy(xpath = ".//input[@value='Add Marketing Costs']")
	public WebElement addMarketingCost ;

	@FindBy(name = "costSheetYear")
	public WebElement costSheetYear;

	@FindBy(xpath = ".//a[contains(@href,'fsCostSheetSummary') and @qat_adminlink='Hidden_Links']")
	public WebElement marketingCostLink ;

	@FindBy(xpath = ".//div[@id='forMonth']//tr//input")
	public WebElement costVal ;

	@FindBy(xpath = ".//input[@value='Add']")
	public WebElement addBtn ;

	@FindBy(xpath = ".//input[@value='Modify']")
	public WebElement modifyBtn ;

	public AdminFranchiseSalesConfigureMarketingCostsPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
