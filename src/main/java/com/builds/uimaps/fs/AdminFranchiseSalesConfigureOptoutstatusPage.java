package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminFranchiseSalesConfigureOptoutstatusPage {

	@FindBy(id = "leadStatusID")
	public WebElement leadStatus;

	@FindBy(name = "SaveButton")
	public WebElement saveBtn;

	public AdminFranchiseSalesConfigureOptoutstatusPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
