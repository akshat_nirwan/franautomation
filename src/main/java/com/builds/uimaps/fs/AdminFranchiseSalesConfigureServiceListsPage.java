package com.builds.uimaps.fs;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminFranchiseSalesConfigureServiceListsPage {

	@FindBy(xpath = ".//input[@value='Add Subscription Type']")
	public WebElement addService ;

	@FindBy(id = "serviceNameDetail")
	public WebElement serviceNameDetail;

	@FindBy(name = "Add")
	public WebElement add;

	@FindBy(xpath = ".//input[@value='Close']")
	public WebElement close ;

	@FindBy(xpath = ".//*[@id='siteMainTable']/tbody/tr/td/table/tbody/tr[4]/td[2]/table/tbody/tr/td/form/table/tbody/tr[2]/td/table/tbody//td[1]")
	public List<WebElement> serviceNameList;

	public AdminFranchiseSalesConfigureServiceListsPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
