package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminFranchiseSalesForecastRatingPage {

	@FindBy(xpath = ".//input[@value='Add Forecast Rating']")
	public WebElement addForecastRating ;

	@FindBy(name = "forecastRatingName")
	public WebElement forecastRatingName;

	@FindBy(xpath = ".//input[@value='Add']")
	public WebElement addBtn ;

	@FindBy(xpath = ".//input[@value='Modify']")
	public WebElement modifyBtn ;

	@FindBy(xpath = ".//input[@value='Delete']")
	public WebElement deleteBtn ;

	@FindBy(xpath = ".//input[@value='Close']")
	public WebElement closeBtn ;

	@FindBy(name = "templateOrder")
	public WebElement listing;

	public AdminFranchiseSalesForecastRatingPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
