package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminFranchiseSalesLeadKilledReasonPage {

	@FindBy(xpath = ".//input[@value='Add Lead Killed Reason']")
	public WebElement addLeadKilledReason ;

	@FindBy(xpath = ".//input[@value='Ajouter raison du prospect ']")
	public WebElement addLeadKilledReason2fr ;

	@FindBy(id = "leadKilledReasonName")
	public WebElement leadKilledReasonName;

	@FindBy(id = "leadStatusID")
	public WebElement leadStatusID;

	@FindBy(name = "modify")
	public WebElement add;

	@FindBy(xpath = ".//input[@value='Close']")
	public WebElement closeBtn ;

	@FindBy(xpath = ".//input[@value='Modify']")
	public WebElement modifyBtn ;

	@FindBy(xpath = ".//input[@value='Delete']")
	public WebElement deleteBtn ;

	public AdminFranchiseSalesLeadKilledReasonPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
