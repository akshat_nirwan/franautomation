package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminFranchiseSalesLeadMarketingCodePage {
	@FindBy(xpath = ".//input[@value='Add Lead Marketing Code']")
	public WebElement addLeadMarketingCode ;

	@FindBy(name = "templateOrder")
	public WebElement listing;

	@FindBy(name = "leadCodeName")
	public WebElement leadMarketingCodeName;

	@FindBy(name = "Addbutton8888")
	public WebElement addBtn;

	@FindBy(xpath = ".//input[@value='Modify']")
	public WebElement modifyBtn ;

	@FindBy(xpath = ".//input[@value='Delete']")
	public WebElement deleteBtn ;

	@FindBy(xpath = ".//input[@value='Close']")
	public WebElement closeBtn ;

	public AdminFranchiseSalesLeadMarketingCodePage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
