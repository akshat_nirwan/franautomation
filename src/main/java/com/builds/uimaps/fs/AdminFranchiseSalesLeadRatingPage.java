package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminFranchiseSalesLeadRatingPage {

	@FindBy(xpath = ".//input[@value='Add Lead Rating']")
	public WebElement addLeadRating ;

	@FindBy(name = "leadRatingName")
	public WebElement leadRatingName;

	@FindBy(name = "Addbutton8888")
	public WebElement addBtn;

	@FindBy(xpath = ".//input[@value='Modify']")
	public WebElement modifyBtn ;

	@FindBy(xpath = ".//input[@value='Delete']")
	public WebElement deleteBtn ;

	@FindBy(name = "templateOrder")
	public WebElement listing;

	public AdminFranchiseSalesLeadRatingPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
