package com.builds.uimaps.fs;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminFranchiseSalesLeadSourceCategoryPage {

	@FindBy(xpath = ".//input[@value='Add Lead Source Category']")
	public WebElement addLeadSourceCategory ;

	@FindBy(xpath = ".//input[@name='leadSource2Name' and @type = 'text']")
	public WebElement leadSource2Name ;

	@FindBy(name = "webpageFlag")
	public WebElement includeInWebPage;

	@FindBy(id = "add")
	public WebElement addBtn;

	@FindBy(xpath = ".//input[@value='Close']")
	public WebElement closeBtn ;

	@FindBy(xpath = ".//*[@id='siteMainTable']/tbody/tr/td/table/tbody/tr[4]/td[2]/form/table/tbody/tr[6]/td/table/tbody/tr[1]/td/table/tbody/tr[2]/td/table/tbody//td[2]")
	public List<WebElement> leadSourceListing;

	@FindBy(xpath = ".//*[@id='siteMainTable']/tbody/tr/td/table/tbody/tr[4]/td[2]/form/table/tbody/tr[6]/td/table/tbody/tr[1]/td/table/tbody/tr[2]/td/table/tbody//td[1]/input[1]")
	public List<WebElement> checkboxListing;

	@FindBy(xpath = ".//input[@value='Modify']")
	public WebElement modifyBtn ;

	@FindBy(xpath = ".//input[@value='Delete']")
	public WebElement deleteBtn ;

	public AdminFranchiseSalesLeadSourceCategoryPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
