package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminFranchiseSalesManageFormGeneratorPage {

	@FindBy(xpath = ".//input[@value='Continue']")
	public WebElement continueBtn ;

	@FindBy(xpath = ".//*[@id='taborder']")
	public WebElement ModifyTabPosBtn ;

	@FindBy(xpath = ".//input[@value='Back']")
	public WebElement BackBtn ;

	@FindBy(id = "addsectionlink")
	public WebElement addNewTabBtn;

	@FindBy(xpath = ".//*[@name='is-active' and @value='Y']")
	public WebElement isactiveYes ;

	@FindBy(name = "fldValidationType1")
	public WebElement numericValidation;

	@FindBy(xpath = ".//*[@name='is-active' and @value='N']")
	public WebElement isactiveNo ;

	@FindBy(xpath = ".//*[@name='is-exportable' and @value='true']")
	public WebElement isexportableYes ;

	@FindBy(xpath = ".//*[@name='addMore' and @value='true']")
	public WebElement accessmultipleinputYes ;

	@FindBy(xpath = ".//*[@name='roleBase' and @value='true']")
	public WebElement maketabaccessibletoallYes ;

	@FindBy(id = "tabDisplayName")
	public WebElement displayName;

	@FindBy(id = "formNames")
	public WebElement dropdown;

	@FindBy(id = "submoduleName")
	public WebElement submoduleNameDrp;

	@FindBy(name = "submitButton")
	public WebElement submitBtn;

	@FindBy(xpath = ".//*[@id='addsectionlink']/span")
	public WebElement addSectionBtn ;

	@FindBy(xpath = ".//*[@href='administration']")
	public WebElement AdminLink ;

	@FindBy(name = "sectionValue")
	public WebElement sectionName;

	@FindBy(name = "isTabularSection")
	public WebElement sectiontype;

	@FindBy(name = "submitButton")
	public WebElement submitSectionCboxBtn;

	@FindBy(name = "dbColumnLength")
	public WebElement dbColumnLength;

	@FindBy(xpath = ".//*[contains(text(),'Add New Field')]")
	public WebElement addNewFieldBtn ;

	@FindBy(xpath = ".//*[@id='siteMainTable']/tbody/tr/td/form[2]/table/tbody/tr[11]/td[2]/table/tbody/tr/td/table/tbody/tr[1]/td/table/tbody/tr/td/table/tbody/tr/td[5]/a")
	public WebElement deleteSectionBtn ;

	@FindBy(xpath = ".//*[@id='siteMainTable']/tbody/tr/td/form[2]/table/tbody/tr[11]/td[2]/table/tbody/tr/td/table/tbody/tr[1]/td/table/tbody/tr/td/table/tbody/tr/td[7]/a")
	public WebElement addDocumentBtn ;

	@FindBy(xpath = ".//*[@id='siteMainTable']/tbody/tr/td/form[2]/table/tbody/tr[11]/td[2]/table/tbody/tr/td/table/tbody/tr[1]/td/table/tbody/tr/td/table/tbody/tr/td[8]/a")
	public WebElement modifySectionBtn ;

	@FindBy(xpath = ".//*[@id='previewlink']/span")
	public WebElement previewformbtn ;

	@FindBy(name = "displayName")
	public WebElement fieldDisplayName;

	@FindBy(name = "displayName1")
	public WebElement fieldDisplayName1;

	@FindBy(name = "dependentField")
	public WebElement ParentdependentField;

	@FindBy(xpath = ".//*[@id='tabularFieldlink']/span")
	public WebElement configuretabularview ;

	@FindBy(id = "submitButton")
	public WebElement submitFieldCBoxButton;

	@FindBy(id = "docTitle")
	public WebElement documentTitle;

	@FindBy(name = "docLabel")
	public WebElement documentLabel;

	@FindBy(id = "submitButton")
	public WebElement addDocumentCboxBtn;

	/* Harish Dwivedi FG */

	@FindBy(xpath = ".//*[@id='siteMainTable']/tbody/tr/td/form[2]/table/tbody/tr[11]/td[2]/table/tbody/tr[2]/td/table/tbody/tr[1]/td/table/tbody/tr/td/table/tbody/tr/td[6]/a")
	public WebElement addDocumentSaleBtn ;

	@FindBy(xpath = ".//*[@id='siteMainTable']/tbody/tr/td/form[2]/table/tbody/tr[3]/td[2]/table/tbody/tr/td/input[1]")
	public WebElement addDocumentSalePrimaryInfoBtn ;

	@FindBy(xpath = ".//*[@id='siteMainTable']/tbody/tr/td/form[2]/table/tbody/tr[11]/td[2]/table/tbody/tr/td/table/tbody/tr[1]/td/table/tbody/tr/td/table/tbody/tr/td[5]/a")
	public WebElement addFiledtoBtnmultiple ;

	@FindBy(name = "dbColumnType")
	public WebElement dbColumnType;

	@FindBy(name = "fldValidationType")
	public WebElement fldValidationType;

	@FindBy(name = "noOfCols")
	public WebElement noOfCols;

	@FindBy(name = "noOfRows")
	public WebElement noOfRows;

	@FindBy(name = "optionNameTemp")
	public WebElement optionNameTemp;

	@FindBy(id = "ms-parent")
	public WebElement multiCheckBox;

	@FindBy(xpath = ".//*[@id='siteMainTable']/tbody/tr/td/form[2]/table/tbody/tr[11]/td[2]/table/tbody/tr[1]/td/table/tbody/tr[1]/td/table/tbody/tr/td/table/tbody/tr/td[5]/a")
	public WebElement PrimaryInfoAddNewFied ;

	@FindBy(xpath = ".//a[contains(text () ,'Primary Info')]")
	public WebElement primaryInfo ;

	@FindBy(xpath = ".//input[contains(@placeholder ,'Search fields')]")
	public WebElement searchField ;

	@FindBy(xpath = ".//*[contains(@id ,'defaultSection') and contains(@id ,'Flds')]/li[@class='endEmptyList']")
	public WebElement defaultDropHere ;

	@FindBy(id = "designNextBtn")
	public WebElement saveAndNextBtnDesign;

	@FindBy(xpath = ".//*[@id='siteMainTable']/tbody/tr/td/form[2]/table/tbody/tr[11]/td[2]/table/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr[2]/td[2]/table/tbody/tr/td/table/tbody/tr[2]/td[5]/a/img")
	public WebElement mandatoryButton ;

	@FindBy(name = "Submit")
	public WebElement tabLeadLabelsubmit;

	@FindBy(id = "notes")
	public WebElement notesArchive;

	@FindBy(xpath = ".//input[@value='Close']")
	public WebElement closeBtn ;

	@FindBy(xpath = ".//input[@name='archivedleads' and value='yes']")
	public WebElement sendarchivedleadsRadioYes ;

	@FindBy(xpath = ".//*[@id='fsLeadTable']/tbody/tr/td[2]/form/table[2]/tbody/tr[5]/td/table/tbody/tr[2]/td/table/tbody/tr[1]/td[1]/input")
	public WebElement checkBoxAll ;

	@FindBy(xpath = ".//*[@id='siteMainTable']/tbody/tr/td/table/tbody/tr[3]/td[2]/table/tbody/tr[2]/td/div/table/tbody/tr/td[2]/table/tbody/tr/td[1]/a[3]")
	public WebElement logATaskLinks ;

	@FindBy(xpath = ".//*[@id='siteMainTable']/tbody/tr/td/table/tbody/tr[3]/td[2]/table/tbody/tr[2]/td/div/table/tbody/tr/td[2]/table/tbody/tr/td[1]/a[4]")
	public WebElement logACallLinks ;

	@FindBy(xpath = ".//*[@id='subject']")
	public WebElement callSubject ;

	@FindBy(linkText = "Detailed History")
	public WebElement detailedHistoryLink ;

	@FindBy(xpath = ".//*[@id='leadNames']/a[.='Calls']")
	public WebElement callsTabDH ;

	@FindBy(xpath = ".//input[@value='No']")
	public WebElement callConfirmScheduleTaskNoBtn ;

	@FindBy(xpath = ".//*[@id='siteMainTable']/tbody/tr/td/table/tbody/tr[3]/td[2]/table/tbody/tr[2]/td/div/table/tbody/tr/td[2]/table/tbody/tr/td[1]/a[5]")
	public WebElement logAremarkinks ;

	@FindBy(id = "subject")
	public WebElement taskSubject;

	@FindBy(name = "add")
	public WebElement addTaskBtn;

	@FindBy(linkText = "Log a Task")
	public WebElement logATask ;

	@FindBy(linkText = "Print History")
	public WebElement contactHistorySection ;

	@FindBy(xpath = ".//*[@id='ms-parentfieldNamesForTabNew']/div/div/input")
	public WebElement searchBoxSynch ;

	@FindBy(linkText = "Primary Info")
	public WebElement PrimaryInfoSectionSearch ;

	@FindBy(xpath = ".//*[@id='Button']")
	public WebElement closeSectionCboxBtn ;

	@FindBy(xpath = ".//*[@qat_adminlink='Configure Summary Display Columns']")
	public WebElement configureSummaryDisplayColumns;

	public AdminFranchiseSalesManageFormGeneratorPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
