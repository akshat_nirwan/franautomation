package com.builds.uimaps.fs;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminFranchiseSalesManageSalesTerritoriesPage {

	@FindBy(name = "countryId")
	public WebElement countryId;

	/*
	 * @FindBy(name="regionKey") public WebElement searchBySalesTerritory;
	 */

	@FindBy(id = "ms-parentregionKey")
	public WebElement salesTerritoryMs;

	@FindBy(xpath = ".//input[@value='Add New Sales Territory']")
	public WebElement addNewSalesTerritory ;

	@FindBy(xpath = ".//input[@value='Close']")
	public WebElement closeBtn ;

	@FindBy(xpath = ".//*[@onclick='if(parent.$.fn.colorbox){ parent.$.fn.colorbox.close() }else{window.close(); }']")
	public WebElement closeCboxBtn ;

	@FindBy(xpath = ".//*[@id='siteMainTable']/tbody/tr/td/form[2]/table/tbody/tr[7]/td[2]/table/tbody/tr[2]/td/table/tbody//td[1]/a")
	public List<WebElement> domesticSalesTerritoriesList;

	@FindBy(xpath = ".//*[@id='siteMainTable']/tbody/tr/td/form[2]/table/tbody/tr[10]/td[2]/table/tbody/tr[2]/td/table/tbody//td[1]/a")
	public List<WebElement> internationalSalesTerritoriesList;

	public AdminFranchiseSalesManageSalesTerritoriesPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
