package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminFranchiseSalesManageSalesTerritoryCreateSalesTerritoryPage {

	@FindBy(id = "category")
	public WebElement category;

	@FindBy(name = "regioname")
	public WebElement salesTerritoryName;

	@FindBy(name = "groupBy")
	public WebElement groupBy;

	@FindBy(name = "all")
	public WebElement selectAllCountry;

	@FindBy(name = "country")
	public WebElement country;

	@FindBy(xpath = ".//input[@name='zipmode' and @value='1']")
	public WebElement commaSeparatedListZip ;

	@FindBy(name = "ziplist")
	public WebElement ziplist;

	@FindBy(xpath = ".//input[@type='button' and @value='Submit']")
	public WebElement submitBtn ;

	@FindBy(xpath = ".//input[@value=' Next ' or @value='Next']")
	public WebElement nextBtn;

	@FindBy(xpath = ".//input[@value=' Suivant ']")
	public WebElement nextBtn2fr;

	@FindBy(id = "ms-parentregionKey")
	public WebElement salesTerritoryMS;

	@FindBy(name = "regionKey")
	public WebElement searchTerritoryName;

	@FindBy(id = "search")
	public WebElement searchBtn;

	@FindBy(xpath = ".//*[@id='0_1']")
	public WebElement firstStateChkBox;

	@FindBy(xpath = ".//td/input[@id='all']/ancestor::tr/following-sibling::tr[1]/td//table/tbody/tr[1]/td[1]/input[@name='country']")
	public WebElement firstCountryChkBox ;

	@FindBy(xpath = ".//input[@value='Proceed']")
	public WebElement proceedBtn ;

	@FindBy(xpath = ".//input[@value='Proceed']")
	public WebElement proceedBtn2fr ;

	@FindBy(xpath = ".//input[@value='Override and Proceed']")
	public WebElement overrideandProceedBtn ;

	@FindBy(xpath = ".//input[@value='Override and Proceed']")
	public WebElement overrideandProceedBtn2fr ;

	public AdminFranchiseSalesManageSalesTerritoryCreateSalesTerritoryPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
