/* Harish Dwivedi */
package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminFranchiseSalesManageWebFormGeneratorPage {

	@FindBy(xpath = ".//input[@value='Continue']")
	public WebElement continueBtn ;

	@FindBy(id = "addsectionlink")
	public WebElement addNewTabBtn;

	@FindBy(id = "tabDisplayName")
	public WebElement displayName;

	@FindBy(id = "submoduleName")
	public WebElement submoduleNameDrp;

	@FindBy(id = "submitButton")
	public WebElement submitBtn;

	@FindBy(xpath = ".//*[@id='addsectionlink']/span")
	public WebElement addSectionBtn ;

	@FindBy(name = "fieldValue")
	public WebElement sectionName;

	@FindBy(name = "submitButton")
	public WebElement submitSectionCboxBtn;

	@FindBy(xpath = ".//*[@id='siteMainTable']/tbody/tr/td/form[2]/table/tbody/tr[11]/td[2]/table/tbody/tr/td/table/tbody/tr[1]/td/table/tbody/tr/td/table/tbody/tr/td[6]/a")
	public WebElement addNewFieldBtn ;

	@FindBy(xpath = ".//*[@id='siteMainTable']/tbody/tr/td/form[2]/table/tbody/tr[11]/td[2]/table/tbody/tr/td/table/tbody/tr[1]/td/table/tbody/tr/td/table/tbody/tr/td[5]/a")
	public WebElement deleteSectionBtn ;

	@FindBy(xpath = ".//*[@id='siteMainTable']/tbody/tr/td/form[2]/table/tbody/tr[11]/td[2]/table/tbody/tr/td/table/tbody/tr[1]/td/table/tbody/tr/td/table/tbody/tr/td[7]/a")
	public WebElement addDocumentBtn ;

	@FindBy(xpath = ".//*[@id='siteMainTable']/tbody/tr/td/form[2]/table/tbody/tr[11]/td[2]/table/tbody/tr/td/table/tbody/tr[1]/td/table/tbody/tr/td/table/tbody/tr/td[8]/a")
	public WebElement modifySectionBtn ;

	@FindBy(name = "displayName")
	public WebElement fieldDisplayName;

	@FindBy(id = "submitButton")
	public WebElement submitFieldCBoxButton;

	@FindBy(id = "docTitle")
	public WebElement documentTitle;

	@FindBy(name = "docLabel")
	public WebElement documentLabel;

	@FindBy(id = "submitButton")
	public WebElement addDocumentCboxBtn;

	/* Harish Dwivedi FG */

	@FindBy(xpath = ".//*[@id='siteMainTable']/tbody/tr/td/form[2]/table/tbody/tr[11]/td[2]/table/tbody/tr[2]/td/table/tbody/tr[1]/td/table/tbody/tr/td/table/tbody/tr/td[6]/a")
	public WebElement addDocumentSaleBtn ;

	@FindBy(xpath = ".//*[@id='siteMainTable']/tbody/tr/td/form[2]/table/tbody/tr[3]/td[2]/table/tbody/tr/td/input[1]")
	public WebElement addDocumentSalePrimaryInfoBtn ;

	@FindBy(xpath = ".//*[@id='siteMainTable']/tbody/tr/td/form[2]/table/tbody/tr[11]/td[2]/table/tbody/tr/td/table/tbody/tr[1]/td/table/tbody/tr/td/table/tbody/tr/td[5]/a")
	public WebElement addFiledtoBtnmultiple ;

	@FindBy(name = "dbColumnType")
	public WebElement dbColumnType;

	@FindBy(name = "optionNameTemp")
	public WebElement optionNameTemp;

	@FindBy(id = "ms-parent")
	public WebElement multiCheckBox;

	@FindBy(xpath = ".//*[@id='siteMainTable']/tbody/tr/td/form[2]/table/tbody/tr[11]/td[2]/table/tbody/tr[1]/td/table/tbody/tr[1]/td/table/tbody/tr/td/table/tbody/tr/td[5]/a")
	public WebElement PrimaryInfoAddNewFied ;

	@FindBy(xpath = ".//a[contains(text () ,'Primary Info')]")
	public WebElement primaryInfo ;

	@FindBy(xpath = ".//a[contains(text () ,'Qualification Details')]")
	public WebElement qualificationDetails ;

	@FindBy(xpath = ".//a[contains(text () ,'Personal Profile')]")
	public WebElement personalProfile ;

	@FindBy(xpath = ".//*[@id='ui-accordion-subModules-header-4'][contains(text(),'Real Estate')]")
	public WebElement realEstate ;

	@FindBy(xpath = ".//input[contains(@placeholder ,'Search fields')]")
	public WebElement searchField ;

	@FindBy(xpath = ".//input[@data-searchid='fsLeadQualificationDetail']")
	public WebElement searchFieldQualification ;

	@FindBy(xpath = ".//*[contains(@id ,'defaultSection') and contains(@id ,'Flds')]/li[@class='endEmptyList']")
	public WebElement defaultDropHere ;

	@FindBy(id = "designNextBtn")
	public WebElement saveAndNextBtnDesign;

	@FindBy(xpath = ".//*[@id='designNextBtn']")
	public WebElement saveAndNextBtn ;

	@FindBy(xpath = ".//*[@id='siteMainTable']/tbody/tr/td/form[2]/table/tbody/tr[11]/td[2]/table/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr[2]/td[2]/table/tbody/tr/td/table/tbody/tr[2]/td[5]/a/img")
	public WebElement mandatoryButton ;

	@FindBy(name = "Submit")
	public WebElement tabLeadLabelsubmit;

	@FindBy(id = "notes")
	public WebElement notesArchive;

	@FindBy(xpath = ".//input[@value='Close']")
	public WebElement closeBtn ;

	@FindBy(xpath = ".//input[@name='archivedleads' and value='yes']")
	public WebElement sendarchivedleadsRadioYes ;

	@FindBy(xpath = ".//*[@id='fsLeadTable']/tbody/tr/td[2]/form/table[2]/tbody/tr[5]/td/table/tbody/tr[2]/td/table/tbody/tr[1]/td[1]/input")
	public WebElement checkBoxAll ;

	@FindBy(xpath = ".//*[@id='siteMainTable']/tbody/tr/td/table/tbody/tr[3]/td[2]/table/tbody/tr[2]/td/div/table/tbody/tr/td[2]/table/tbody/tr/td[1]/a[3]")
	public WebElement logATaskLinks ;

	@FindBy(xpath = ".//*[@id='siteMainTable']/tbody/tr/td/table/tbody/tr[3]/td[2]/table/tbody/tr[2]/td/div/table/tbody/tr/td[2]/table/tbody/tr/td[1]/a[4]")
	public WebElement logACallLinks ;

	@FindBy(xpath = ".//*[@id='subject']")
	public WebElement callSubject ;

	@FindBy(linkText = "Detailed History")
	public WebElement detailedHistoryLink ;

	@FindBy(xpath = ".//*[@id='leadNames']/a[.='Calls']")
	public WebElement callsTabDH ;

	@FindBy(xpath = ".//input[@value='No']")
	public WebElement callConfirmScheduleTaskNoBtn ;

	@FindBy(xpath = ".//*[@id='siteMainTable']/tbody/tr/td/table/tbody/tr[3]/td[2]/table/tbody/tr[2]/td/div/table/tbody/tr/td[2]/table/tbody/tr/td[1]/a[5]")
	public WebElement logAremarkinks ;

	@FindBy(id = "subject")
	public WebElement taskSubject;

	@FindBy(name = "add")
	public WebElement addTaskBtn;

	@FindBy(linkText = "Log a Task")
	public WebElement logATask ;

	@FindBy(linkText = "Print History")
	public WebElement contactHistorySection ;

	@FindBy(xpath = ".//input[@value='Create New Form']")
	public WebElement createNewForm ;

	@FindBy(id = "formName")
	public WebElement formName;

	@FindBy(id = "formDisplayTitle")
	public WebElement formDisplayTitle;

	@FindBy(id = "displayFormTitleCheckBox")
	public WebElement displayFormTitleCheckBox;

	@FindBy(id = "formDescription")
	public WebElement formDescription;

	@FindBy(xpath = ".//input[@name='formFor' and @value='lead']")
	public WebElement formTypeLeads ;

	@FindBy(xpath = ".//input[@name='formFor' and @value='cm']")
	public WebElement formTypeContact ;

	@FindBy(id = "formFormat")
	public WebElement formFormat;

	@FindBy(id = "submissionType")
	public WebElement submissionType;

	@FindBy(id = "columnCount")
	public WebElement columnCount;

	@FindBy(id = "filedLabelAlignment")
	public WebElement filedLabelAlignment;

	@FindBy(id = "formLanguage")
	public WebElement formLanguage;

	@FindBy(id = "iframeWidth")
	public WebElement iframeWidth;

	@FindBy(id = "iframeHeight")
	public WebElement iframeHeight;

	@FindBy(id = "formUrl")
	public WebElement formUrl;

	@FindBy(id = "detailsNextBtn")
	public WebElement saveNextBtn;

	@FindBy(id = "sectionBtn")
	public WebElement addSection;

	@FindBy(id = "noOfCols")
	public WebElement noOfCols;

	@FindBy(id = "fieldLabelAlignment")
	public WebElement fieldLabelAlignmentSection;

	@FindBy(xpath = ".//input[@value='Add' and @id='submitButton']")
	public WebElement addBtn ;

	@FindBy(id = "sectionBtnPrw")
	public WebElement saveAndPreviewBtn;

	@FindBy(id = "availableFields_Tab")
	public WebElement availableFieldsTab;

	@FindBy(xpath = ".//a[contains(text () ,'Form Tools')]")
	public WebElement formTools ;

	@FindBy(xpath = ".//li[@id='header_fld']/a[contains(text () ,'Text')]")
	public WebElement formToolText ;

	@FindBy(xpath = ".//li[@id='recaptcha_fld']/a[contains(text () ,'Captcha')]")
	public WebElement formToolCaptcha ;

	@FindBy(xpath = ".//li[@id='disclamer_checkbox_fld']/a[contains(text () ,'CheckBox')]")
	public WebElement formToolIAgreeCheckBox ;

	@FindBy(xpath = ".//a[contains(text () ,'Lead Info')]")
	public WebElement leadInfo ;

	@FindBy(xpath = ".//*[@id='page_header']//p[.='Double click to add header.']")
	public WebElement addHeader ;

	@FindBy(xpath = ".//input[@value='Save']")
	public WebElement saveBtn ;

	@FindBy(xpath = ".//*[contains(@id ,'sectionHeader') and contains(@id , 'defaultSection')]")
	public WebElement defaultSectionHeaderAddText ;

	@FindBy(id = "fieldLabel")
	public WebElement labelAddText;

	@FindBy(id = "headerLabelAlignment")
	public WebElement headerLabelAlignment;

	@FindBy(xpath = ".//*[@id='page_footer']//p[.='Double click to add footer.']")
	public WebElement addFooterDefault ;

	@FindBy(xpath = ".//select[@id='cmLeadStatusID']")
	public WebElement cmLeadStatusID ;

	@FindBy(xpath = ".//select[@id='cmSource1ID']")
	public WebElement contactMediumSelect ;

	@FindBy(xpath = ".//select[@id='cmSource2ID' and @class='multiList']")
	public WebElement leadSource ;

	@FindBy(xpath = ".//select[@id='cmSource3ID' and @class='multiList']")
	public WebElement leadSourceDetailsSelect ;

	@FindBy(xpath = " .//input[@name='ownerType' and @value='C' and @type='radio']")
	public WebElement assignToCorporate ;

	@FindBy(xpath = " .//input[@name='ownerType' and @value='R' and @type='radio']")
	public WebElement assignToRegional ;

	@FindBy(xpath = " .//input[@name='ownerType' and @value='F' and @type='radio']")
	public WebElement assignToFranchise ;

	@FindBy(xpath = " .//input[@name='ownerType' and @value='D' and @type='radio']")
	public WebElement assignToDefault ;

	@FindBy(xpath = ".//select[@name='contactOwnerID2']")
	public WebElement selectCorporateUser ;

	@FindBy(xpath = ".//select[@id='areaID']")
	public WebElement selectAreaRegion ;

	@FindBy(xpath = ".//select[@id='contactOwnerID3']")
	public WebElement selectRegionalUser ;

	@FindBy(xpath = ".//select[@id='franchiseeNo']")
	public WebElement selectFranchiseID ;

	@FindBy(xpath = ".//select[@id='contactOwnerID1']")
	public WebElement selectFranchiseUser ;

	@FindBy(xpath = ".//input[@name='afterSubmissionMsgType' and @value='msg']")
	public WebElement afterSubmissionMsg ;

	@FindBy(xpath = ".//input[@name='afterSubmissionMsgType' and @value='url']")
	public WebElement afterSubmissionUrl ;

	@FindBy(id = "formCUrl")
	public WebElement redirectedUrl;

	@FindBy(xpath = ".//input[@id='settingsNextBtn' and @value='Finish']")
	public WebElement finishBtn ;

	@FindBy(id = "hostURL")
	public WebElement hostURL;

	@FindBy(id = "hostCodeBox")
	public WebElement hostCodeBox;

	@FindBy(id = "confirmationNextBtn")
	public WebElement okBtn;

	// add Tab
	@FindBy(id = "tabBtn")
	public WebElement addTab;

	@FindBy(id = "fieldValue")
	public WebElement tabName;

	@FindBy(xpath = ".//*[contains(@id , 'defaultTab') and contains(@id , 'Tab') and contains(text () ,'Default Tab')]")
	public WebElement defaultTab ;

	@FindBy(xpath = ".//input[@value='Preview Form' and @name='preBtn']")
	public WebElement previewFormAtSettingPage ;

	@FindBy(xpath = ".//div[@id='confirmationDiv']//input[@value='Preview Form' and @name='preBtn']")
	public WebElement previewFormAtConfirmationPage ;

	@FindBy(id = "searchMyForm")
	public WebElement searchMyForm;

	@FindBy(xpath = ".//img[@alt='Search on Form Name']")
	public WebElement searchMyFormBtn ;

	@FindBy(xpath = ".//select[@id='contactType']")
	public WebElement contactType ;

	// Launch & Test
	@FindBy(xpath = ".//a[.='Default Tab']")
	public WebElement defaultTabLaunch ;

	@FindBy(id = "lastName")
	public WebElement leadLastName;

	@FindBy(id = "firstName")
	public WebElement leadFirstName;

	@FindBy(id = "title")
	public WebElement title;

	@FindBy(id = "companyName")
	public WebElement companyName;

	@FindBy(id = "address")
	public WebElement address;

	@FindBy(id = "address2")
	public WebElement address2;

	@FindBy(id = "city")
	public WebElement city;

	@FindBy(id = "country")
	public WebElement country;

	@FindBy(id = "zipcode")
	public WebElement zipcode;

	@FindBy(id = "zip")
	public WebElement zip;

	@FindBy(id = "stateID")
	public WebElement stateID;

	@FindBy(id = "countyID")
	public WebElement countyID;

	@FindBy(id = "bestTimeToContact")
	public WebElement bestTimeToContact;

	@FindBy(id = "phone")
	public WebElement phone;

	@FindBy(id = "phoneExt")
	public WebElement phoneExt;

	@FindBy(id = "homePhone")
	public WebElement homePhone;

	@FindBy(id = "homePhoneExt")
	public WebElement homePhoneExt;

	@FindBy(id = "fax")
	public WebElement fax;

	@FindBy(id = "mobile")
	public WebElement mobile;

	@FindBy(id = "comments")
	public WebElement comments;

	@FindBy(id = "leadSource2ID")
	public WebElement leadSource2ID;

	@FindBy(id = "leadSource3ID")
	public WebElement leadSource3ID;

	@FindBy(id = "liquidCapitalMax")
	public WebElement liquidCapitalMax;

	@FindBy(id = "investTimeframe")
	public WebElement investTimeframe;

	@FindBy(id = "background")
	public WebElement background;

	@FindBy(id = "sourceOfFunding")
	public WebElement sourceOfFunding;

	@FindBy(id = "noOfUnitReq")
	public WebElement noOfUnitReq;

	@FindBy(id = "preferredCity1")
	public WebElement preferredCity1;

	@FindBy(id = "preferredCountry1")
	public WebElement preferredCountry1;

	@FindBy(id = "preferredCity2")
	public WebElement preferredCity2;

	@FindBy(id = "preferredCountry2")
	public WebElement preferredCountry2;

	@FindBy(id = "preferredStateId2")
	public WebElement preferredStateId2;

	@FindBy(id = "fsLeadQualificationDetail_0dateSubmission")
	public WebElement dateSubmission;

	@FindBy(id = "fsLeadQualificationDetail_0date")
	public WebElement date;

	@FindBy(xpath = ".//span[contains(text(),'Male')]/ancestor::span/label")
	public WebElement gender_Male ;

	@FindBy(id = "fsLeadQualificationDetail_0presentAddress")
	public WebElement presentAddress;

	@FindBy(id = "fsLeadQualificationDetail_0city")
	public WebElement fsLeadQualificationDetail_0city;

	@FindBy(id = "fsLeadQualificationDetail_0country")
	public WebElement fsLeadQualificationDetail_0country;

	@FindBy(id = "fsLeadQualificationDetail_0stateID")
	public WebElement fsLeadQualificationDetail_0stateID;

	@FindBy(id = "fsLeadQualificationDetail_0zipCode")
	public WebElement fsLeadQualificationDetail_0zipCode;

	@FindBy(id = "fsLeadQualificationDetail_0workPhone")
	public WebElement fsLeadQualificationDetail_0workPhone;

	@FindBy(id = "fsLeadQualificationDetail_0phoneExt")
	public WebElement fsLeadQualificationDetail_0phoneExt;

	@FindBy(id = "fsLeadQualificationDetail_0homePhone")
	public WebElement fsLeadQualificationDetail_0homePhone;

	@FindBy(id = "fsLeadQualificationDetail_0emailID")
	public WebElement fsLeadQualificationDetail_0emailID;

	@FindBy(xpath = ".//label[@for='fsLeadQualificationDetail_0usCitizen0']")
	public WebElement usCitizenYes ;

	@FindBy(id = "fsLeadQualificationDetail_0ssn")
	public WebElement ssn;

	@FindBy(id = "fsLeadQualificationDetail_0previousAddress")
	public WebElement previousAddress;

	@FindBy(id = "fsLeadQualificationDetail_0birthMonth")
	public WebElement fsLeadQualificationDetail_0birthMonth;

	@FindBy(id = "fsLeadQualificationDetail_0birthDate")
	public WebElement fsLeadQualificationDetail_0birthDate;

	@FindBy(xpath = ".//span[contains(text(),'Home Ownership')]/ancestor::label/following-sibling::span/span[1]/label")
	public WebElement homeOwnership ;

	@FindBy(xpath = ".//span[contains(text(),'Marital Status')]/ancestor::label/following-sibling::span/span[2]/label")
	public WebElement maritalStatus ;

	@FindBy(id = "fsLeadQualificationDetail_0spouseName")
	public WebElement fsLeadQualificationDetail_0spouseName;

	@FindBy(id = "fsLeadQualificationDetail_0spouseSsn")
	public WebElement fsLeadQualificationDetail_0spouseSsn;

	@FindBy(xpath = ".//span[contains(text(),'Spouse US Citizen')]/ancestor::label/following-sibling::span/span[1]/label")
	public WebElement spouseUsCitizenYes ;

	@FindBy(xpath = ".//*[@id='fsLeadQualificationDetail_0spouseBirthMonth']")
	public WebElement fsLeadQualificationDetail_0spouseBirthMonth;

	@FindBy(xpath = ".//*[@id='fsLeadQualificationDetail_0cashOnHand']")
	public WebElement fsLeadQualificationDetail_0cashOnHand ;

	@FindBy(xpath = ".//*[@id='fsLeadQualificationDetail_0mortgages']")
	public WebElement fsLeadQualificationDetail_0mortgages ;

	@FindBy(id = "fsLeadQualificationDetail_0marketableSecurities")
	public WebElement fsLeadQualificationDetail_0marketableSecurities;

	@FindBy(id = "fsLeadQualificationDetail_0accountsPayable")
	public WebElement fsLeadQualificationDetail_0accountsPayable;

	@FindBy(id = "fsLeadQualificationDetail_0accountsReceivable")
	public WebElement fsLeadQualificationDetail_0accountsReceivable;

	@FindBy(id = "fsLeadQualificationDetail_0notesPayable")
	public WebElement fsLeadQualificationDetail_0notesPayable;

	@FindBy(id = "fsLeadQualificationDetail_0retirementPlans")
	public WebElement fsLeadQualificationDetail_0retirementPlans;

	@FindBy(id = "fsLeadQualificationDetail_0spouseBirthDate")
	public WebElement fsLeadQualificationDetail_0spouseBirthDate;

	@FindBy(id = "fsLeadQualificationDetail_0loansOnLifeInsurance")
	public WebElement fsLeadQualificationDetail_0loansOnLifeInsurance;

	@FindBy(id = "fsLeadQualificationDetail_0realEstate")
	public WebElement fsLeadQualificationDetail_0realEstate;

	@FindBy(id = "fsLeadQualificationDetail_0creditCardBalance")
	public WebElement fsLeadQualificationDetail_0creditCardBalance;

	@FindBy(id = "fsLeadQualificationDetail_0personalProperty")
	public WebElement fsLeadQualificationDetail_0personalProperty;

	@FindBy(id = "fsLeadQualificationDetail_0unpaidTaxes")
	public WebElement fsLeadQualificationDetail_0unpaidTaxes;

	@FindBy(id = "fsLeadQualificationDetail_0businessHoldings")
	public WebElement fsLeadQualificationDetail_0businessHoldings;

	@FindBy(id = "fsLeadQualificationDetail_0lifeInsurance")
	public WebElement fsLeadQualificationDetail_0lifeInsurance;

	@FindBy(id = "fsLeadQualificationDetail_0otherAssets")
	public WebElement fsLeadQualificationDetail_0otherAssets;

	@FindBy(id = "fsLeadQualificationDetail_0otherLiabilities")
	public WebElement fsLeadQualificationDetail_0otherLiabilities;

	@FindBy(id = "fsLeadQualificationDetail_0liablitiesDescription")
	public WebElement fsLeadQualificationDetail_0liablitiesDescription;

	@FindBy(id = "fsLeadQualificationDetail_0totalAssets")
	public WebElement fsLeadQualificationDetail_0totalAssets;

	@FindBy(id = "fsLeadQualificationDetail_0totalLiabilities")
	public WebElement fsLeadQualificationDetail_0totalLiabilities;

	@FindBy(id = "fsLeadQualificationDetail_0totalNetworth")
	public WebElement fsLeadQualificationDetail_0totalNetworth;

	@FindBy(id = "fsLeadQualificationDetail_0reAddress1")
	public WebElement fsLeadQualificationDetail_0reAddress1;

	@FindBy(id = "fsLeadQualificationDetail_0reAddress2")
	public WebElement fsLeadQualificationDetail_0reAddress2;

	@FindBy(id = "fsLeadQualificationDetail_0reDatePurchased1")
	public WebElement fsLeadQualificationDetail_0reDatePurchased1;

	@FindBy(id = "fsLeadQualificationDetail_0reDatePurchased2")
	public WebElement fsLeadQualificationDetail_0reDatePurchased2;

	@FindBy(id = "fsLeadQualificationDetail_0reOrigCost1")
	public WebElement fsLeadQualificationDetail_0reOrigCost1;

	@FindBy(id = "fsLeadQualificationDetail_0reMortgageBalance1")
	public WebElement fsLeadQualificationDetail_0reMortgageBalance1;

	@FindBy(id = "fsLeadQualificationDetail_0rePresentValue1")
	public WebElement fsLeadQualificationDetail_0rePresentValue1;

	@FindBy(id = "fsLeadQualificationDetail_0reOrigCost2")
	public WebElement fsLeadQualificationDetail_0reOrigCost2;

	@FindBy(id = "fsLeadQualificationDetail_0rePresentValue2")
	public WebElement fsLeadQualificationDetail_0rePresentValue2;

	@FindBy(id = "fsLeadQualificationDetail_0reMortgageBalance2")
	public WebElement fsLeadQualificationDetail_0reMortgageBalance2;

	@FindBy(id = "fsLeadQualificationDetail_0reAddress3")
	public WebElement fsLeadQualificationDetail_0reAddress3;

	@FindBy(id = "fsLeadQualificationDetail_0reDatePurchased3")
	public WebElement fsLeadQualificationDetail_0reDatePurchased3;

	@FindBy(id = "fsLeadQualificationDetail_0reOrigCost3")
	public WebElement fsLeadQualificationDetail_0reOrigCost3;

	@FindBy(id = "fsLeadQualificationDetail_0rePresentValue3")
	public WebElement fsLeadQualificationDetail_0rePresentValue3;

	@FindBy(id = "fsLeadQualificationDetail_0reMortgageBalance3")
	public WebElement fsLeadQualificationDetail_0reMortgageBalance3;

	@FindBy(id = "fsLeadQualificationDetail_0annualSalary")
	public WebElement fsLeadQualificationDetail_0annualSalary;

	@FindBy(id = "fsLeadQualificationDetail_0annualInvestment")
	public WebElement fsLeadQualificationDetail_0annualInvestment;

	@FindBy(id = "fsLeadQualificationDetail_0annualReIncome")
	public WebElement fsLeadQualificationDetail_0annualReIncome;

	@FindBy(id = "fsLeadQualificationDetail_0otherAnnualSource")
	public WebElement fsLeadQualificationDetail_0otherAnnualSource;

	@FindBy(id = "fsLeadQualificationDetail_0annualSourceTotal")
	public WebElement fsLeadQualificationDetail_0annualSourceTotal;

	@FindBy(id = "fsLeadQualificationDetail_0loanCoSign")
	public WebElement fsLeadQualificationDetail_0loanCoSign;

	@FindBy(id = "fsLeadQualificationDetail_0legalJudgement")
	public WebElement fsLeadQualificationDetail_0legalJudgement;

	@FindBy(id = "fsLeadQualificationDetail_0otherSpecialDebt")
	public WebElement fsLeadQualificationDetail_0otherSpecialDebt;

	@FindBy(id = "fsLeadQualificationDetail_0totalContingent")
	public WebElement fsLeadQualificationDetail_0totalContingent;

	@FindBy(id = "fsLeadQualificationDetail_0whenReadyIfApproved")
	public WebElement fsLeadQualificationDetail_0whenReadyIfApproved;

	@FindBy(id = "fsLeadQualificationDetail_0skillsExperience")
	public WebElement fsLeadQualificationDetail_0skillsExperience;

	@FindBy(id = "fsLeadQualificationDetail_0enableReachGoals")
	public WebElement fsLeadQualificationDetail_0enableReachGoals;

	@FindBy(id = "fsLeadQualificationDetail_0responsibleForDailyOperations")
	public WebElement fsLeadQualificationDetail_0responsibleForDailyOperations;

	@FindBy(id = "fsLeadQualificationDetail_0cashAvailable")
	public WebElement fsLeadQualificationDetail_0cashAvailable;

	@FindBy(xpath = ".//span[contains(text(),'Have you been approved for financing?')]/ancestor::label/following-sibling::span/span[1]/label")
	public WebElement haveYouBeenApprovedForFinancing ;

	@FindBy(xpath = ".//span[contains(text(),'Would this business be your sole income source')]/ancestor::label/following-sibling::span/span[1]/label")
	public WebElement wouldtYourSoloSource ;

	@FindBy(xpath = ".//span[contains(text(),'Do you have any contingent liabilities for guarantees, endorsements, leases etc ?')]/ancestor::label/following-sibling::span/span[1]/label")
	public WebElement contigentLiabilities ;

	@FindBy(xpath = ".//span[contains(text(),'Are you now, or have you ever been party to any lawsuit - either as defendant or plaintiff ?')]/ancestor::label/following-sibling::span/span[1]/label")
	public WebElement anyLawsuit ;

	@FindBy(xpath = ".//span[contains(text(),'Have you ever been convicted of any offense (including misdemeanors for which you have fined $ 200 or more) ?')]/ancestor::label/following-sibling::span/span[1]/label")
	public WebElement convictedOffense ;

	@FindBy(xpath = ".//span[contains(text(),'Have you ever been convicted of a felony')]/ancestor::label/following-sibling::span/span[1]/label")
	public WebElement convictedFelony ;

	@FindBy(id = "fsLeadQualificationDetail_0amountApprovedForFinance")
	public WebElement fsLeadQualificationDetail_0amountApprovedForFinance;

	@FindBy(id = "fsLeadQualificationDetail_0explainConviction")
	public WebElement fsLeadQualificationDetail_0explainConviction;

	@FindBy(id = "fsLeadPersonalProfile_0dateSubmission")
	public WebElement fsLeadPersonalProfile_0dateSubmission;

	@FindBy(xpath = ".//label[@for='fsLeadPersonalProfile_0gender0']")
	public WebElement fsLeadPersonalProfile_0gender0 ;

	@FindBy(id = "fsLeadPersonalProfile_0homeAddress")
	public WebElement fsLeadPersonalProfile_0homeAddress;

	@FindBy(id = "fsLeadPersonalProfile_0howLongAtAddress")
	public WebElement fsLeadPersonalProfile_0howLongAtAddress;

	@FindBy(id = "fsLeadPersonalProfile_0homeCity")
	public WebElement fsLeadPersonalProfile_0homeCity;

	@FindBy(id = "fsLeadPersonalProfile_0homeCountry")
	public WebElement fsLeadPersonalProfile_0homeCountry;

	@FindBy(id = "fsLeadPersonalProfile_0homeZip")
	public WebElement fsLeadPersonalProfile_0homeZip;

	@FindBy(id = "fsLeadPersonalProfile_0homePhone")
	public WebElement fsLeadPersonalProfile_0homePhone;

	@FindBy(id = "fsLeadPersonalProfile_0homePhoneExt")
	public WebElement fsLeadPersonalProfile_0homePhoneExt;

	@FindBy(id = "fsLeadPersonalProfile_0email")
	public WebElement fsLeadPersonalProfile_0email;

	@FindBy(xpath = ".//label[@for='fsLeadPersonalProfile_0homeOwnership0']")
	public WebElement fsLeadPersonalProfile_0homeOwnership0 ;

	@FindBy(xpath = ".//label[@for='fsLeadQualificationDetail_0approvedForFinancing0']")
	public WebElement fsLeadQualificationDetail_0approvedForFinancing0 ;

	@FindBy(xpath = ".//label[@for='fsLeadQualificationDetail_0liabilites0']")
	public WebElement fsLeadQualificationDetail_0liabilites0 ;

	@FindBy(xpath = ".//label[@for='fsLeadQualificationDetail_0lawsuit0']")
	public WebElement fsLeadQualificationDetail_0lawsuit0 ;

	@FindBy(xpath = ".//label[@for='fsLeadQualificationDetail_0convicted0']")
	public WebElement fsLeadQualificationDetail_0convicted0 ;

	@FindBy(xpath = ".//label[@for='fsLeadQualificationDetail_0convictedOfFelony0']")
	public WebElement fsLeadQualificationDetail_0convictedOfFelony0 ;

	@FindBy(xpath = ".//label[@for='fsLeadQualificationDetail_0soleIncomeSource0']")
	public WebElement fsLeadQualificationDetail_0soleIncomeSource0 ;

	@FindBy(id = "fsLeadPersonalProfile_0spouseName")
	public WebElement fsLeadPersonalProfile_0spouseName;

	@FindBy(id = "fsLeadPersonalProfile_0seekingOwnBusiness")
	public WebElement fsLeadPersonalProfile_0seekingOwnBusiness;

	@FindBy(xpath = ".//label[@for='fsLeadPersonalProfile_0fullTimeBusiness0']")
	public WebElement personalProfileFullTimBuisness ;

	@FindBy(xpath = ".//label[@for='fsLeadPersonalProfile_0callAtWork0']")
	public WebElement fsLeadPersonalProfile_0callAtWork0 ;

	@FindBy(id = "fsLeadPersonalProfile_0otherInvestigation")
	public WebElement fsLeadPersonalProfile_0otherInvestigation;

	@FindBy(id = "fsLeadPersonalProfile_0presentEmployer")
	public WebElement fsLeadPersonalProfile_0presentEmployer;

	@FindBy(id = "fsLeadPersonalProfile_0title")
	public WebElement fsLeadPersonalProfile_0title;

	@FindBy(id = "fsLeadPersonalProfile_0dateStarted")
	public WebElement fsLeadPersonalProfile_0dateStarted;

	@FindBy(id = "fsLeadPersonalProfile_0employerAddress")
	public WebElement fsLeadPersonalProfile_0employerAddress;

	@FindBy(id = "fsLeadPersonalProfile_0employerCity")
	public WebElement fsLeadPersonalProfile_0employerCity;

	@FindBy(id = "fsLeadPersonalProfile_0employerCountry")
	public WebElement fsLeadPersonalProfile_0employerCountry;

	@FindBy(id = "fsLeadPersonalProfile_0employerZip")
	public WebElement fsLeadPersonalProfile_0employerZip;

	@FindBy(id = "fsLeadPersonalProfile_0businessPhone")
	public WebElement fsLeadPersonalProfile_0businessPhone;

	@FindBy(id = "fsLeadPersonalProfile_0salary")
	public WebElement fsLeadPersonalProfile_0salary;

	@FindBy(xpath = ".//label[@for='sLeadPersonalProfile_0callAtWork0']")
	public WebElement leadPersonalProfileCallAtWork ;

	@FindBy(id = "fsLeadPersonalProfile_0responsibility")
	public WebElement fsLeadPersonalProfile_0responsibility;

	@FindBy(xpath = ".//label[@for='fsLeadPersonalProfile_0limitProforma0']")
	public WebElement fsLeadPersonalProfile_0limitProforma0 ;

	@FindBy(xpath = ".//label[@for='fsLeadPersonalProfile_0similarWork0']")
	public WebElement fsLeadPersonalProfile_0similarWork0 ;

	@FindBy(id = "fsLeadPersonalProfile_0financeProforma")
	public WebElement fsLeadPersonalProfile_0financeProforma;

	@FindBy(xpath = ".//label[@for='fsLeadPersonalProfile_0partner0']")
	public WebElement fsLeadPersonalProfile_0partner0 ;

	@FindBy(id = "fsLeadPersonalProfile_0supportHowLong")
	public WebElement fsLeadPersonalProfile_0supportHowLong;

	@FindBy(id = "fsLeadPersonalProfile_0otherSalary")
	public WebElement fsLeadPersonalProfile_0otherSalary;

	@FindBy(id = "fsLeadPersonalProfile_0otherIncomeExplaination")
	public WebElement fsLeadPersonalProfile_0otherIncomeExplaination;

	@FindBy(xpath = ".//label[@for='fsLeadPersonalProfile_0soleSource0']")
	public WebElement fsLeadPersonalProfile_0soleSource0 ;

	@FindBy(id = "fsLeadPersonalProfile_0howSoon")
	public WebElement fsLeadPersonalProfile_0howSoon;

	@FindBy(xpath = ".//label[@for='fsLeadPersonalProfile_0runYourself0']")
	public WebElement fsLeadPersonalProfile_0runYourself0 ;

	@FindBy(id = "fsLeadPersonalProfile_0responsibleForOperation")
	public WebElement fsLeadPersonalProfile_0responsibleForOperation;

	@FindBy(xpath = ".//label[@for='fsLeadPersonalProfile_0convictedForFelony0']")
	public WebElement fsLeadPersonalProfile_0convictedForFelony0 ;

	@FindBy(xpath = ".//label[@for='fsLeadPersonalProfile_0bankruptcy0']")
	public WebElement fsLeadPersonalProfile_0bankruptcy0 ;

	@FindBy(xpath = ".//label[@for='fsLeadPersonalProfile_0convicted0']")
	public WebElement fsLeadPersonalProfile_0convicted0 ;

	@FindBy(id = "fsLeadPersonalProfile_0otherFacts")
	public WebElement fsLeadPersonalProfile_0otherFacts;

	@FindBy(id = "fsLeadRealEstate_0siteAddress2")
	public WebElement fsLeadRealEstate_0siteAddress2;

	@FindBy(id = "fsLeadRealEstate_0siteCity")
	public WebElement fsLeadRealEstate_0siteCity;

	@FindBy(id = "fsLeadRealEstate_0siteCountry")
	public WebElement fsLeadRealEstate_0siteCountry;

	@FindBy(id = "fsLeadRealEstate_0siteState")
	public WebElement fsLeadRealEstate_0siteState;

	@FindBy(id = "fsLeadRealEstate_0buildingSize")
	public WebElement fsLeadRealEstate_0buildingSize;

	@FindBy(id = "fsLeadRealEstate_0buildingDimentionsX")
	public WebElement fsLeadRealEstate_0buildingDimentionsX;

	@FindBy(id = "fsLeadRealEstate_0buildingDimentionsZ")
	public WebElement fsLeadRealEstate_0buildingDimentionsZ;

	@FindBy(id = "fsLeadRealEstate_0dealType")
	public WebElement fsLeadRealEstate_0dealType;

	@FindBy(id = "fsLeadRealEstate_0loiSigned")
	public WebElement fsLeadRealEstate_0loiSigned;

	@FindBy(id = "fsLeadRealEstate_0leaseCommencement")
	public WebElement fsLeadRealEstate_0leaseCommencement;

	@FindBy(id = "fsLeadRealEstate_0initialTerm")
	public WebElement fsLeadRealEstate_0initialTerm;

	@FindBy(xpath = ".//label[@for='fsLeadRealEstate_0purchaseOption0']")
	public WebElement fsLeadRealEstate_0purchaseOption0 ;

	@FindBy(xpath = ".//label[@for='fsLeadRealEstate_0generalContractorSelector0']")
	public WebElement fsLeadRealEstate_0generalContractorSelector0 ;

	@FindBy(id = "fsLeadRealEstate_0addressGeneralContractor")
	public WebElement fsLeadRealEstate_0addressGeneralContractor;

	@FindBy(id = "fsLeadRealEstate_0permitApplied")
	public WebElement fsLeadRealEstate_0permitApplied;

	@FindBy(id = "fsLeadRealEstate_0certificate")
	public WebElement fsLeadRealEstate_0certificate;

	@FindBy(id = "fsLeadRealEstate_0grandOpeningDate")
	public WebElement fsLeadRealEstate_0grandOpeningDate;

	@FindBy(xpath = ".//label[@class='checkbox-htmlElement disclaimer-checkbox']")
	public WebElement disclaimerCheckbox ;

	@FindBy(id = "state")
	public WebElement state;

	@FindBy(id = "phoneNumbers")
	public WebElement phoneNumbers;

	@FindBy(id = "extn")
	public WebElement extn;

	@FindBy(id = "faxNumbers")
	public WebElement faxNumbers;

	@FindBy(id = "mobileNumbers")
	public WebElement mobileNumbers;

	@FindBy(id = "alternateEmail")
	public WebElement alternateEmail;

	@FindBy(id = "position")
	public WebElement jobTitle;

	@FindBy(id = "birthdate")
	public WebElement birthdate;

	@FindBy(id = "cmSource2ID")
	public WebElement leadSourceLaunch;

	@FindBy(id = "cmSource3ID")
	public WebElement leadSourceDetailsLaunch;

	@FindBy(id = "nextButton")
	public WebElement nextBtn;

	@FindBy(id = ".//input[@value='Y']")
	public WebElement iAgreeChkBx;

	@FindBy(id = "emailIds")
	public WebElement emailIds;

	// Primary Info Fields

	@FindBy(xpath = ".//*[@id='salutation']")
	public WebElement salutationField ;

	@FindBy(id = "emailID")
	public WebElement emailID;

	public AdminFranchiseSalesManageWebFormGeneratorPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
