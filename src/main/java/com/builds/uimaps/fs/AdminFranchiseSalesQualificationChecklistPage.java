package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminFranchiseSalesQualificationChecklistPage {
	@FindBy(xpath = ".//input[@value='Add Qualification Checklist']")
	public WebElement addQualificationChecklist ;

	@FindBy(xpath = ".//input[@value='Ajouter Liste de qualification']")
	public WebElement addQualificationChecklist2fr ;

	@FindBy(name = "qfnCheckListName")
	public WebElement qualificationChecklistName;

	@FindBy(name = "Addbutton8888")
	public WebElement addBtn;

	@FindBy(xpath = ".//input[@value='Modify']")
	public WebElement modifyBtn ;

	@FindBy(xpath = ".//input[@value='Delete']")
	public WebElement deleteBtn ;

	@FindBy(xpath = ".//input[@value='Close']")
	public WebElement closeBtn ;

	@FindBy(name = "templateOrder")
	public WebElement listing;

	public AdminFranchiseSalesQualificationChecklistPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
