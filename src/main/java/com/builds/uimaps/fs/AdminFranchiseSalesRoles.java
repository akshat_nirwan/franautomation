package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminFranchiseSalesRoles {

	@FindBy(xpath = ".//img[@class='santa']")
	public WebElement floatingListBtn ;

	@FindBy(xpath = ".//a[contains(text(),'Can Access Sales Information')]")
	public WebElement canAccesSalesInfo ;

	@FindBy(xpath = ".//input[@value='Add New Role']")
	public WebElement AddNewRoleBtn ;

	@FindBy(xpath = ".//select[@name='roleType']")
	public WebElement roleTypeDrpDwn ;

	@FindBy(xpath = ".//*[@name='roleName']")
	public WebElement roleName ;

	@FindBy(name = "allPrivileges")
	public WebElement allPrivileges;

	@FindBy(id = "AllSubPriv101")
	public WebElement canManageAdministrator;

	@FindBy(id = "AllSubPriv441")
	public WebElement canManagePersonalSettings;

	@FindBy(xpath = ".//input[@name='Submit']")
	public WebElement submitBtn ;

	@FindBy(xpath = ".//input[@value='Assign Later']")
	public WebElement assignLater ;

	@FindBy(xpath = ".//input[@value='Assign']")
	public WebElement assignBtn ;

	@FindBy(xpath = ".//select[@name='oldRoleID']")
	public WebElement assignRoleUserCurentlyDrpDwn ;

	@FindBy(xpath = ".//input[@value='Back']")
	public WebElement backBtn ;

	@FindBy(xpath = ".//input[@value='Delete']")
	public WebElement deleteBtn ;

	@FindBy(xpath = ".//input[@name='isDivisionConfigureDisplay' and @value='Y\']")
	public WebElement configureNewHierarchyLevelYes ;

	@FindBy(xpath = ".//input[@name='Submit']")
	public WebElement submitBtn1 ;

	public AdminFranchiseSalesRoles(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
