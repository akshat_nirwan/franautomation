package com.builds.uimaps.fs;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminFranchiseSalesSetupTaskTriggersPage {

	@FindBy(linkText = "Add Task Triggers")
	public WebElement addTaskTriggers ;

	@FindBy(id = "leadStatusID")
	public WebElement leadStatusID;

	@FindBy(id = "activityType")
	public WebElement activityType;

	@FindBy(id = "leadOwner")
	public WebElement assignToLeadOwner;

	@FindBy(id = "subject")
	public WebElement subjectField;

	@FindBy(id = "priority")
	public WebElement priorityDrp;

	@FindBy(id = "hours1")
	public WebElement durationInHours;

	@FindBy(id = "mins1")
	public WebElement durationInMins;

	@FindBy(id = "taskDesc")
	public WebElement taskDescription;

	@FindBy(xpath = ".//input[@id='setReminder' and @Value='No Reminder']")
	public WebElement setReminder ;

	@FindBy(id = "Submit")
	public WebElement createBtn;

	@FindBy(xpath = ".//input[@value='Modify']")
	public WebElement modifyBtn ;

	@FindBy(xpath = ".//input[@value='Ok']")
	public WebElement okBtn ;

	@FindBy(xpath = ".//*[@id='siteMainTable']/tbody/tr/td/table/tbody/tr[6]/td[2]/table/tbody/tr[4]/td/table/tbody/tr[3]/td/table/tbody/tr[2]/td/table/tbody//td[2]/a")
	public List<WebElement> taskSubjectListings;

	@FindBy(xpath = ".//*[@id='siteMainTable']/tbody/tr/td/form[2]/table/tbody/tr[7]/td[2]/table/tbody/tr[4]/td/table/tbody/tr[3]/td/table/tbody/tr[2]/td/table/tbody//td[2]/a")
	public List<WebElement> activityDriverTaskSubjectListings;

	@FindBy(linkText = "Activity Driven Task Triggers")
	public WebElement activityDrivenTaskTriggerLnk ;

	public AdminFranchiseSalesSetupTaskTriggersPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
