package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminFranchiseSalesUnregisteredStatesProvincesPage {

	@FindBy(xpath = ".//*[@id='leadCampaign']")
	public WebElement campaign ;

	@FindBy(xpath = ".//*[@id='leadStatus']")
	public WebElement status ;

	@FindBy(name = "update")
	public WebElement updateBtn;

	@FindBy(name = "close")
	public WebElement closeBtn;

	public AdminFranchiseSalesUnregisteredStatesProvincesPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
