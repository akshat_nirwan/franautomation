package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminSalesAssignLeadOwnerByTerritorySetupPriorityForOwnerAssignmentPage {
	@FindBy(name = "changeSequenceOrder")
	public WebElement changeSequence;

	@FindBy(xpath = ".//img[@title='Move to Up']")
	public WebElement MovetoUp ;

	@FindBy(xpath = ".//*[@id='siteMainTable']/tbody/tr/td/form[2]/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table/tbody/tr/td/input[1]")
	public WebElement saveBtn ;

	public AdminSalesAssignLeadOwnerByTerritorySetupPriorityForOwnerAssignmentPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
