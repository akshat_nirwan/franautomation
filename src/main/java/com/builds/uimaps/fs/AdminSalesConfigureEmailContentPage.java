package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminSalesConfigureEmailContentPage {

	@FindBy(xpath = ".//input[@name='isConfigure' and @value='Y']")
	public WebElement sendEmailnotificationYes ;

	@FindBy(xpath = ".//*[@id='ms-parentotherEmailFields']/button")
	public WebElement emailFields ;

	@FindBy(xpath = ".//*[@id='selectAll']")
	public WebElement selectAll ;

	@FindBy(xpath = ".//input[@name='Submit']")
	public WebElement saveBtn ;

	public AdminSalesConfigureEmailContentPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
