package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminSalesConfigureSearchCriteriaforTopSearchPage {

	@FindBy(id = "fieldsCombo_0")
	public WebElement fieldNameDrp;

	@FindBy(id = "criteriaCombo_0")
	public WebElement fieldCriteriaDrp;

	@FindBy(xpath = ".//input[@value='Update' and @type='button']")
	public WebElement updateBtn ;

	public AdminSalesConfigureSearchCriteriaforTopSearchPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
