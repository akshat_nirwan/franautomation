package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminSalesLeadStatusUI {

	@FindBy(xpath = ".//input[@value='Add Lead Status']")
	public WebElement addLeadStatusBtn;

	@FindBy(name = "leadStatusName")
	public WebElement leadStatus;

	@FindBy(xpath = "html/body/div[2]/table/tbody/tr/td/table[2]/tbody/tr[2]/td/table/tbody/tr[2]/td/table[2]/tbody/tr[1]/td[2]/input")
	public WebElement leadStatusName ;

	@FindBy(id = "statusCategory")
	public WebElement statusFor;

	@FindBy(name = "isExcluded1")
	public WebElement isExcluded1;

	@FindBy(name = "Addbutton888")
	public WebElement addBtn;

	@FindBy(xpath = ".//input[@value='Modify']")
	public WebElement modifyBtn ;

	@FindBy(xpath = ".//input[@value='Delete']")
	public WebElement deleteBtn ;

	@FindBy(xpath = ".//input[@value='Close']")
	public WebElement closeBtn ;

	@FindBy(name = "templateOrder")
	public WebElement allStatusListing;

	public AdminSalesLeadStatusUI(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
