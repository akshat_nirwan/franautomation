package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminSalesSetupLeadOwnersPage {

	@FindBy(xpath = ".//*[@id='allUsers']")
	public WebElement alluserSalesLeadOwners ;

	@FindBy(xpath = ".//*[@id='selectedUsersForFS']")
	public WebElement selectedUsersForFS ;

	@FindBy(xpath = ".//input[@value='<<']")
	public WebElement moveToAvailable ;

	public AdminSalesSetupLeadOwnersPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
