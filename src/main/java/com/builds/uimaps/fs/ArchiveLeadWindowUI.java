package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.builds.uimaps.common.CommonUI;

public class ArchiveLeadWindowUI extends CommonUI {
	@FindBy(id = "notes")
	public WebElement notes;

	@FindBy(id = "Submit")
	public WebElement arvhiveButton;

	public ArchiveLeadWindowUI(WebDriver driver) {
		super(driver);
	}
}
