package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AssignLeadOwnerbyAreaDeveloperUI {
	@FindBy(linkText="Set Priority")
	public WebElement setPriority;
	
	public AssignLeadOwnerbyAreaDeveloperUI(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
