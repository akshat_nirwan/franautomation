package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.builds.utilities.FranconnectUtil;

public class AssignLeadOwnerbyLeadSourceUI {
	@FindBy(linkText="Set Priority")
	public WebElement setPriority;
	
	public AssignLeadOwnerbyLeadSourceUI(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	public WebElement DefaultOwner_Against_LeadSourceName(WebDriver driver , String leadSourceName ) throws Exception
	{
		FranconnectUtil fc = new FranconnectUtil();
		return fc.utobj().getElementByXpath(driver, ".//td[contains(text(),'"+leadSourceName+"')]/following-sibling::td//div[@class='ms-parent'] ");
	}
	
	@FindBy(xpath=".//input[@type='submit' and @value='Update']")
	public WebElement updateBtn;
	
	@FindBy(xpath=".//input[@type='button' and @value='Back']")
	public WebElement backBtn;
	
	@FindBy(linkText="Set Priority")
	public WebElement setPriority_link;
	
	// Setup priority for Owner Assignment
	@FindBy(className="multiList")
	public WebElement priorityList;
	
	@FindBy(xpath=".//*[@title='Move to Up']")
	public WebElement upArrow_Btn;
	
	@FindBy(xpath=".//input[@type='button' and @value='Save']")
	public WebElement saveBtn;
	
	public WebElement actionBtn_Against_LeadSourceName(WebDriver driver , String leadSourceName ) throws Exception
	{
		FranconnectUtil fc = new FranconnectUtil();
		return fc.utobj().getElementByXpath(driver, ".//tr/td[contains(text(),'"+leadSourceName+"')]/following-sibling::td[3]//img");
	}
	
	public WebElement actionBtn_Against_DivisionName(WebDriver driver , String divisionName ) throws Exception
	{
		FranconnectUtil fc = new FranconnectUtil();
		return fc.utobj().getElementByXpath(driver, ".//tr/td[contains(text(),'"+divisionName+"')]/following-sibling::td[3]//img");
	}
	
	// Assign For Sales Territory
	public WebElement DefaultOwner_Against_TerritoryName(WebDriver driver, String territoryName) throws Exception
	{
			FranconnectUtil fc = new FranconnectUtil();
			return fc.utobj().getElementByXpath(driver, ".//td[contains(text(),'"+territoryName+"')]/following-sibling::td//div[@class='ms-parent']");
	}
	
	//Assign For Source
	public WebElement DefaultOwner_Against_SourceName(WebDriver driver, String sourceName) throws Exception
	{
			FranconnectUtil fc = new FranconnectUtil();
			return fc.utobj().getElementByXpath(driver, ".//td[contains(text(),'"+sourceName+"')]/following-sibling::td//div[@class='ms-parent']");
	}
	
	@FindBy(xpath=".//input[@type='button' and @value='Save']")
	public WebElement saveBtn_AssignFor_Any;
	
	// Assign For Division
	public WebElement DefaultOwner_Against_Division(WebDriver driver, String division) throws Exception
	{
			FranconnectUtil fc = new FranconnectUtil();
			return fc.utobj().getElementByXpath(driver, ".//td[contains(text(),'"+division+"')]/following-sibling::td//div[@class='ms-parent']");
	}

}
