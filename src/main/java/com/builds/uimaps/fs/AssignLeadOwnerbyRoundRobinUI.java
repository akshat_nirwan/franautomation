package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AssignLeadOwnerbyRoundRobinUI {

	@FindBy(id="allOwners")
	public WebElement availableLeadOwners_List;
	
	@FindBy(id="templateOrder")
	public WebElement configuredLeadOwners_List ;
	
	@FindBy(xpath=".//*[@name='button' and @value='Update']")
	public WebElement updateBtn ;
	
	@FindBy(xpath=".//input[@class='cm_new_button' and @value='>>']")
	public WebElement moveToConfiguredLeadOwnersList;
	
	@FindBy(xpath=".//input[@class='cm_new_button' and @value='<<']")
	public WebElement moveToAvailableLeadOwners;
	
	public AssignLeadOwnerbyRoundRobinUI(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	
}
