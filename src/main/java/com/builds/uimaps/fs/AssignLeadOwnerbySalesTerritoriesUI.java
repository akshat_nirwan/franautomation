package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.builds.utilities.FranconnectUtil;

public class AssignLeadOwnerbySalesTerritoriesUI {

	@FindBy(linkText="Set Priority")
	public WebElement setPriority;
	
	@FindBy(xpath=".//input[@type='submit' and @value='Update']")
	public WebElement updateBtn;
	
	@FindBy(xpath=".//input[@type='button' and @value='Back']")
	public WebElement BackBtn;
	
	
	public WebElement DefaultOwner_Against_SalesTerritories(WebDriver driver , String salesTerritory) throws Exception
	{
		FranconnectUtil fc = new FranconnectUtil();
		return fc.utobj().getElementByXpath(driver, ".//td[contains(text(),'"+salesTerritory+"')]/following-sibling::td//div[@class='ms-parent'] ");
	}
	
	// Assign For Division
		public WebElement DefaultOwner_Against_Division(WebDriver driver, String division) throws Exception
		{
				FranconnectUtil fc = new FranconnectUtil();
				return fc.utobj().getElementByXpath(driver, ".//td[contains(text(),'"+division+"')]/following-sibling::td//div[@class='ms-parent']");
		}
		
		//Assign For Source
		public WebElement DefaultOwner_Against_SourceName(WebDriver driver, String sourceName) throws Exception
		{
				FranconnectUtil fc = new FranconnectUtil();
				return fc.utobj().getElementByXpath(driver, ".//td[contains(text(),'"+sourceName+"')]/following-sibling::td//div[@class='ms-parent']");
		}
	
	@FindBy(xpath=".//input[@type='button' and @value='Save']")
	public WebElement saveBtn_AssignFor_Any;
		
	public AssignLeadOwnerbySalesTerritoriesUI(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
}
