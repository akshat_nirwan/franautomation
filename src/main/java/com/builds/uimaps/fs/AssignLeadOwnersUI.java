package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AssignLeadOwnersUI {

	@FindBy(name="area1")
	public WebElement DefaultOwner_Select;
	
	@FindBy(name="submit")
	public WebElement udpate_btn;
	
	@FindBy(name="Button22")
	public WebElement continue_btn;
	
	@FindBy(xpath=".//tr//td[.='Assign Lead Owner by Round Robin']/ancestor::tr/td/input[@name='radiobutton']")
	public WebElement assignLeadOwnerbyRoundRobin_Radio;
	
	@FindBy(xpath=".//tr//td[.='Assign Lead Owner by Sales Territories']/ancestor::tr/td/input[@name='radiobutton']")
	public WebElement assignLeadOwnerbySalesTerritories_Radio;
	
	@FindBy(xpath=".//tr//td[.='Assign Lead Owner by Lead Source']/ancestor::tr/td/input[@name='radiobutton']")
	public WebElement assignLeadOwnerbyLeadSource_Radio;
	
	@FindBy(xpath=".//tr//td[.='Assign Lead Owner by Division']/ancestor::tr/td/input[@name='radiobutton']")
	public WebElement assignLeadOwnerbyDivision_Radio;
	
	public AssignLeadOwnersUI(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	
	
}
