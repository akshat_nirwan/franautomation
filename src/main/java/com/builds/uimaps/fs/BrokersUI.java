package com.builds.uimaps.fs;

import java.util.List;

import org.apache.commons.lang.WordUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.builds.uimaps.common.CommonUI;

public class BrokersUI extends CommonUI {

	// @FindBy(id="createBroker")
	// public WebElement createBrokerBtn;

	@FindBy(id = "contactType")
	public WebElement brokerType;

	@FindBy(id = "firstName")
	public WebElement firstName;

	@FindBy(id = "lastName")
	public WebElement lastName;

	@FindBy(id = "address1")
	public WebElement address1;

	@FindBy(id = "address2")
	public WebElement address2;

	@FindBy(id = "city")
	public WebElement city;

	@FindBy(id = "countryID")
	public WebElement country;

	@FindBy(id = "zipcode")
	public WebElement zipPostalCode;

	@FindBy(id = "stateID")
	public WebElement stateProvince;

	@FindBy(id = "homePhone")
	public WebElement homePhone;

	@FindBy(id = "workPhone")
	public WebElement workPhone;

	@FindBy(id = "fax")
	public WebElement fax;

	@FindBy(id = "mobile")
	public WebElement mobile;

	@FindBy(id = "bestTimeToContact")
	public WebElement bestTimeToContact;

	@FindBy(id = "primaryPhoneToCall")
	public WebElement primaryPhoneToCall;

	@FindBy(id = "emailID")
	public WebElement email;

	@FindBy(id = "brokerAgencyId")
	public WebElement agency;

	@FindBy(id = "priority")
	public WebElement priority;

	@FindBy(name = "comments")
	public WebElement comments;

	@FindBy(id = "campaignID")
	public WebElement associateWithCampaign;

	// Brokers Summary Search

	@FindBy(id = "brokerNameSearch")
	public WebElement brokerNameSearch;

	@FindBy(xpath = ".//button[@class='ms-choice']")
	public WebElement brokersAgencyDrpDwn;

	@FindBy(xpath = ".//input[@class='searchInputMultiple']")
	public WebElement searchBrokerAgency;

	@FindBy(xpath = ".//input[@id='selectItembrokerAgencySearch']")
	public WebElement selectItembrokerAgencySearch_chkBox;

	@FindBy(id = "ms-parentbrokerAgencySearch")
	public WebElement brokerAgencySearch_MulitSelect;

	// BUTTONS

	@FindBy(xpath = ".//input[@class='cm_new_button_link']")
	public WebElement addBrokersBtn;

	@FindBy(id = "Submit")
	public WebElement submitBtn;

	@FindBy(id = "search")
	public WebElement searchBtn;

	@FindBy(xpath = ".//input[@class='cm_new_button' and @value='Delete']")
	public WebElement deleteBtn;

	@FindBy(xpath = ".//input[@class='cm_new_button' and @value='Archive']")
	public WebElement archiveBtn;

	@FindBy(xpath = ".//input[@type='button' and @value='Actions']")
	public WebElement ActionsMenu_Left_Input_ByValue;

	@FindBy(xpath = ".//div[@id='actionListButtons']//tr[@class='ActionMenuFontColor']/td")
	public List<WebElement> actionsIn_LeftActionMenu_ByXpath;

	@FindBy(xpath = ".//input[@class='cm_new_button' and @value='Print']")
	public WebElement printBtn;

	// CHECKBOX

	public String selectCheckBox(String brokerFirstName, String brokerLastName) {
		return ".//a[.='" + WordUtils.capitalize(brokerFirstName) + " " + WordUtils.capitalize(brokerLastName)
				+ "']/ancestor::tr[1]//input[@name='checkbox']";
	}

	@FindBy(linkText = "Archived Brokers")
	public WebElement archivedBrokersTab;

	@FindBy(linkText = "Brokers")
	public WebElement brokersTab;

	public BrokersUI(WebDriver driver) {
		super(driver);

	}

}
