package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.builds.uimaps.common.CommonUI;

public class CallUI extends CommonUI {

	@FindBy(id = "subject")
	public WebElement subject;

	@FindBy(id = "date")
	public WebElement date;

	@FindBy(id = "sTime")
	public WebElement timeHH;

	@FindBy(id = "sMinute")
	public WebElement timeMM;

	@FindBy(id = "APM")
	public WebElement timeAMPM;

	@FindBy(id = "callStatus")
	public WebElement callStatus;

	@FindBy(id = "callType")
	public WebElement communicationtype;

	@FindBy(id = "comments")
	public WebElement comments;

	@FindBy(name = "submitButton")
	public WebElement addButton;
	
	// Confirm Schedule a Task
	@FindBy(xpath = ".//input[@class='cm_new_button' and @value='Yes']")
	public WebElement Yes_ConfirmScheduleTask;
	@FindBy(xpath = ".//input[@class='cm_new_button' and @value='No']")
	public WebElement No_ConfirmScheduleTask;
	
	// Call Detail iFrame
	@FindBy(xpath=".//input[@type='button' and @value='Close']")
	public WebElement closeBtn_callDetailsIframe;

	public CallUI(WebDriver driver) {
		super(driver);
	}
}
