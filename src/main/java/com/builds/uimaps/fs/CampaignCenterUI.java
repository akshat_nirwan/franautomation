package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CampaignCenterUI {

	@FindBy(xpath = ".//*[@data-role='ico_AddCircleOutline']")
	public WebElement clickCreate_IconXpath;

	@FindBy(xpath = ".//li/a[.='Campaign']")
	public WebElement CreateCampaign_Link;

	@FindBy(xpath = ".//li/a[.='Template']")
	public WebElement CreateTemplate_Link;

	@FindBy(xpath = ".//li/a[.='Recipients Group']")
	public WebElement CreateRecipientsGroup_Link;
	
	@FindBy(xpath=".//td/div/div/div[@class='svg-icon white' and @data-role='ico_Menu']")
	public WebElement menuBtn_leftTopCorner;
	
	@FindBy(xpath=".//ul[@class='dropdown-list direction-right open-menu']/li/a[.='Campaigns']")
	public WebElement campaignsBtn_TopLeftMenu;
	
	@FindBy(xpath=".//*[@class='btn-style full-mBtn' and contains(text(),'Manage Campaigns')]")
	public WebElement manageCampaigns_Btn;

	@FindBy(xpath = ".//li/a[.='Workflow']")
	public WebElement CreateWorkflow_Link;

	@FindBy(xpath = ".//button[contains(text(),'Create Campaign')]")
	public WebElement CreateCampaign_Button;
	
	@FindBy(xpath=".//a[.='Campaign']")
	public WebElement campaign_Btn;

	@FindBy(xpath = ".//button[contains(text(),'Create Template')]")
	public WebElement CreateTemplate_Button;

	@FindBy(xpath = ".//button[contains(text(),'Create Group')]")
	public WebElement CreateRecipientsGroup_Button;

	@FindBy(xpath = ".//button[contains(text(),'Create Workflow')]")
	public WebElement CreateWorkflow_Button;

	@FindBy(xpath = ".//*[@id='PrintHeaderMenuContainer']//div[@data-role='ico_Menu']")
	public WebElement leftActionMenu;

	@FindBy(xpath = ".//ul[@class='dropdown-list direction-right open-menu']//a[contains(text(),'Dashboard')]")
	public WebElement Dashboard_LinkInLeftAction;

	@FindBy(xpath = ".//ul[@class='dropdown-list direction-right open-menu']//a[contains(text(),'Campaigns')]")
	public WebElement Campaigns_LinkInLeftAction;

	@FindBy(xpath = ".//ul[@class='dropdown-list direction-right open-menu']//a[contains(text(),'Templates')]")
	public WebElement Templates_LinkInLeftAction;

	@FindBy(xpath = ".//ul[@class='dropdown-list direction-right open-menu']//a[contains(text(),'Recipient Groups')]")
	public WebElement Recipient_Groups_LinkInLeftAction;

	@FindBy(xpath = ".//ul[@class='dropdown-list direction-right open-menu']//a[contains(text(),'Workflows')]")
	public WebElement Workflows_LinkInLeftAction;

	public CampaignCenterUI(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
