package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CampaignCenter_SelectRecipientsUI {

	@FindBy(xpath=".//button[@type='button' and contains(text(),'Continue')]")
	public WebElement continue_Btn;
	
	public CampaignCenter_SelectRecipientsUI(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
