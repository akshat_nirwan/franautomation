package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CampaignUI {

	@FindBy(id = "CampaignName")
	public WebElement campaignName;

	@FindBy(id = "Description")
	public WebElement Description;

	@FindBy(id = "Accessibility")
	public WebElement Accessibility;

	@FindBy(xpath = ".//button[contains(text(),'Start')]")
	public WebElement Start;

	@FindBy(xpath = ".//button[contains(text(),'Cancel')]")
	public WebElement Cancel;

	public CampaignUI(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
