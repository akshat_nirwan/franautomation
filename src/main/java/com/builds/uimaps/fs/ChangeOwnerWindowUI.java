package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ChangeOwnerWindowUI {

	@FindBy(id = "leadOwnerID")
	public WebElement leadOwnerID_Select;

	public ChangeOwnerWindowUI(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
