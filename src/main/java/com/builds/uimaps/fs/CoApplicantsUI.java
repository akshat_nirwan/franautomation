package com.builds.uimaps.fs;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CoApplicantsUI {

	public CoApplicantsUI(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	// @FindBy(xpath=".//*[@id='siteMainTable']/tbody/tr/td/table[2]/tbody/tr[3]/td[2]/table/tbody/tr[1]/td/table/tbody/tr/td[2]/input")
	// public WebElement addCoApplicantBtn;

	@FindBy(id = "chooseExisting")
	public WebElement addCoApplicantWithAdditionLead_RadioBtn;

	@FindBy(id = "firstName")
	public WebElement firstName;

	@FindBy(id = "lastName")
	public WebElement lastName;

	@FindBy(id = "coApplicantRelationshipID")
	public WebElement coApplicantRelationshipID;

	@FindBy(id = "phone")
	public WebElement phone;

	@FindBy(id = "ext")
	public WebElement ext;

	@FindBy(id = "fax")
	public WebElement fax;

	@FindBy(id = "emailID")
	public WebElement emailID;

	@FindBy(id = "address")
	public WebElement address;

	@FindBy(id = "city")
	public WebElement city;

	@FindBy(id = "country")
	public WebElement country;

	@FindBy(id = "stateID")
	public WebElement stateID;

	@FindBy(id = "zip")
	public WebElement zip;

	@FindBy(xpath = ".//input[@name='Submit' and @value='Add']")
	public WebElement addBtn;

	@FindBy(xpath = ".//*[@id='siteMainTable']/tbody/tr/td/table[2]/tbody/tr[3]/td[2]/table/tbody/tr[2]/td/table/tbody//td[1]/a")
	public List<WebElement> list;

	@FindBy(id = "addAdditionalContact")
	public WebElement addCoApplicantWithoutSeparateLead_RadioBtn;

	@FindBy(id = "addNewLead1")
	public WebElement newLead_radioBtn;

	@FindBy(id = "chooseExisting1")
	public WebElement existingLead_radioBtn;

	@FindBy(id = "coApplicantSearch")
	public WebElement coApplicantSearch;

	@FindBy(xpath = ".//input[@class='cm_new_button_link']")
	public WebElement addCoApplicant_Btn;

	@FindBy(id = "_date181657510")
	public WebElement date;

	public String getXpathOfCoApplicantInListBox(String CoApplicantName) {
		return ".//table[@class='txt_alert_f']//span[@class='text']//*[.='" + CoApplicantName + "'";
	}

}
