package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ComplianceUI {

	// Disclosure Requirements

	@FindBy(id = "fddDate")
	public WebElement dateOfFDD;

	@FindBy(id = "recByFrancDate1")
	public WebElement dateFDDReceivedByFranchisee;

	@FindBy(id = "bussDayExp10Date")
	public WebElement dateHoldingPeriodRequirementsExpireForFDD;

	@FindBy(id = "versionOfUfoc")
	public WebElement versionOfFDD;

	@FindBy(id = "ipAddress")
	public WebElement ipAddress;

	@FindBy(id = "browserType")
	public WebElement browserType;

	@FindBy(id = "firstFrancPaymentDate")
	public WebElement dateOfFirstFranchiseePayment;

	@FindBy(xpath = ".//*[@id='stateRegReq'][1]")
	public WebElement stateProvinceRegistrationRequiredYes;
	@FindBy(xpath = ".//*[@id='stateRegReq'][2]")
	public WebElement stateProvinceRegistrationRequiredNo;
	@FindBy(xpath = ".//*[@id='stateRegReq'][3]")
	public WebElement stateProvinceRegistrationRequiredNA;

	@FindBy(id = "secondFrancPaymentDate")
	public WebElement dateOfSecondFranchiseePayment;

	@FindBy(xpath = ".//*[@id='stateAddendumReq'][1]")
	public WebElement stateProvinceAddendumRequiredYes;
	@FindBy(xpath = ".//*[@id='stateAddendumReq'][2]")
	public WebElement stateProvinceAddendumRequiredNo;
	@FindBy(xpath = ".//*[@id='stateAddendumReq'][3]")
	public WebElement stateProvinceAddendumRequiredNA;

	@FindBy(xpath = ".//*[@id='francCommiteeApproval'][@value='1']")
	public WebElement franchiseCommitteeApprovalYes;
	@FindBy(xpath = ".//*[@id='francCommiteeApproval'][@value='2']")
	public WebElement franchiseCommitteeApprovalNo;
	@FindBy(xpath = ".//*[@id='francCommiteeApproval'][@value='3']")
	public WebElement franchiseCommitteeApprovalNA;

	// Franchise Agreement
	@FindBy(id = "faRequestedDate")
	public WebElement faRequestedDate;

	@FindBy(id = "faReceivedDate")
	public WebElement faReceivedDate;

	@FindBy(id = "francRecAgrDate")
	public WebElement dateFranchiseeReceivedAgreements;

	@FindBy(id = "bussDay5ExpDate")
	public WebElement dateHoldingPeriodRequirementsAreMet;

	@FindBy(id = "francSignAgrDate")
	public WebElement dateAgreementSignedByFranchisee;

	@FindBy(id = "bussDayRule10Check")
	public WebElement dateHoldingPeriodRuleOnCheckMet;

	@FindBy(id = "bussDayRule10AgrDate")
	public WebElement dateHoldingPeriodRuleOnAgreementsMet;

	@FindBy(id = "versionFrancAgr")
	public WebElement versionOfFranchiseeAgreement;

	// Franchise Fee and Signed Agreements Received
	@FindBy(id = "franFeeAmt")
	public WebElement amountFranchiseFee;

	@FindBy(id = "franFeeDate")
	public WebElement dateFranchiseFee;

	// Area Development Fee and Signed Agreements Received
	@FindBy(id = "areaFeeAmt")
	public WebElement amountAreaDevelopmentFee;

	@FindBy(id = "areaFeeDate")
	public WebElement dateAreaDevelopmentFee;

	@FindBy(id = "adaDate")
	public WebElement adaExecutionDate;

	@FindBy(id = "faExecutionDate")
	public WebElement faExecutionDate;

	// Contract Signing Details
	@FindBy(xpath = ".//*[@id='contractRecSign'][1]")
	public WebElement contractReceivedSignedYes;
	@FindBy(xpath = ".//*[@id='contractRecSign'][2]")
	public WebElement contractReceivedSignedNo;
	@FindBy(xpath = ".//*[@id='contractRecSign'][3]")
	public WebElement contractReceivedSignedNA;

	@FindBy(xpath = ".//*[@id='leaseRiderProperlySign'][1]")
	public WebElement leaseRiderSignedYes;
	@FindBy(xpath = ".//*[@id='leaseRiderProperlySign'][2]")
	public WebElement leaseRiderSignedNo;
	@FindBy(xpath = ".//*[@id='leaseRiderProperlySign'][3]")
	public WebElement leaseRiderSignedNA;

	@FindBy(xpath = ".//*[@id='licAgrProperlySign'][1]")
	public WebElement licenseAgreementSignedYes;
	@FindBy(xpath = ".//*[@id='licAgrProperlySign'][2]")
	public WebElement licenseAgreementSignedNo;
	@FindBy(xpath = ".//*[@id='licAgrProperlySign'][3]")
	public WebElement licenseAgreementSignedNA;

	@FindBy(xpath = ".//*[@id='promNotePropSign'][1]")
	public WebElement promissoryNoteSignedYes;
	@FindBy(xpath = ".//*[@id='promNotePropSign'][2]")
	public WebElement promissoryNoteSignedNo;
	@FindBy(xpath = ".//*[@id='promNotePropSign'][3]")
	public WebElement promissoryNoteSignedNA;

	@FindBy(xpath = ".//*[@id='perCovenantAgrProperlySign'][@value='1']")
	public WebElement personalCovenantsAgreementSignedYes;
	@FindBy(xpath = ".//*[@id='perCovenantAgrProperlySign'][@value='2']")
	public WebElement personalCovenantsAgreementSignedNo;
	@FindBy(xpath = ".//*[@id='perCovenantAgrProperlySign'][@value='3']")
	public WebElement personalCovenantsAgreementSignedNA;

	@FindBy(xpath = ".//*[@id='ufocRecProperlySign'][1]")
	public WebElement fddReceiptSignedYes;
	@FindBy(xpath = ".//*[@id='ufocRecProperlySign'][2]")
	public WebElement fddReceiptSignedNo;
	@FindBy(xpath = ".//*[@id='ufocRecProperlySign'][3]")
	public WebElement fddReceiptSignedNA;

	@FindBy(xpath = ".//*[@id='guaranteeProperlySign'][1]")
	public WebElement guaranteeSignedYes;
	@FindBy(xpath = ".//*[@id='guaranteeProperlySign'][2]")
	public WebElement guaranteeSignedNo;
	@FindBy(xpath = ".//*[@id='guaranteeProperlySign'][3]")
	public WebElement guaranteeSignedNA;

	@FindBy(xpath = ".//*[@id='otherDocProperlySign'][1]")
	public WebElement otherDocumentsSignedYes;
	@FindBy(xpath = ".//*[@id='otherDocProperlySign'][2]")
	public WebElement otherDocumentsSignedNo;
	@FindBy(xpath = ".//*[@id='otherDocProperlySign'][3]")
	public WebElement otherDocumentsSignedNA;

	@FindBy(xpath = ".//*[@id='stateReqAddendumProperlySign'][1]")
	public WebElement stateProvinceRequiredAddendumSignedYes;
	@FindBy(xpath = ".//*[@id='stateReqAddendumProperlySign'][2]")
	public WebElement stateProvinceRequiredAddendumSignedNo;
	@FindBy(xpath = ".//*[@id='stateReqAddendumProperlySign'][3]")
	public WebElement stateProvinceRequiredAddendumSignedNA;

	@FindBy(xpath = ".//*[@id='handWrittenChanges'][1]")
	public WebElement handWrittenChangesYes;
	@FindBy(xpath = ".//*[@id='handWrittenChanges'][2]")
	public WebElement handWrittenChangesNo;
	@FindBy(xpath = ".//*[@id='handWrittenChanges'][3]")
	public WebElement handWrittenChangesNA;

	@FindBy(xpath = ".//*[@id='otherAttendaProperlySign'][1]")
	public WebElement otherAddendumSignedYes;
	@FindBy(xpath = ".//*[@id='otherAttendaProperlySign'][2]")
	public WebElement otherAddendumSignedNo;
	@FindBy(xpath = ".//*[@id='otherAttendaProperlySign'][3]")
	public WebElement otherAddendumSignedNA;

	@FindBy(xpath = ".//*[@id='proofControlOverRealEstate'][1]")
	public WebElement proofOfControlOverRealEstateYes;
	@FindBy(xpath = ".//*[@id='proofControlOverRealEstate'][2]")
	public WebElement proofOfControlOverRealEstateNo;
	@FindBy(xpath = ".//*[@id='proofControlOverRealEstate'][3]")
	public WebElement proofOfControlOverRealEstateNA;

	// @FindBy(xpath=".//*[@id='ufocRecProperlySign'][1]")
	// public WebElement
	// ufocRecProperlySign=DDDDDDDDDDDDDDDDDDDDD

	// Buttons
	@FindBy(name = "Submit")
	public WebElement submitBtn;

	@FindBy(id = "button")
	public WebElement saveBtn;

	@FindBy(linkText = "Modify")
	public WebElement modifyComplianceBtn;

	@FindBy(name = "printButton")
	public WebElement printComplianceBtn;

	public ComplianceUI(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
