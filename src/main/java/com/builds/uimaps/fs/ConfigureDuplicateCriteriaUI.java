package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.builds.uimaps.common.CommonUI;

public class ConfigureDuplicateCriteriaUI extends CommonUI {

/*	@FindBy(name = "duplicateCriteriaLeft")
	public WebElement duplicateCriteriaLeft;

	@FindBy(name = "duplicateCriteriaLeft2")
	public WebElement duplicateCriteriaLeft2;

	@FindBy(name = "duplicateCriteriaRight")
	public WebElement duplicateCriteriaRight;

	@FindBy(name = "duplicateCriteriaRight2")
	public WebElement duplicateCriteriaRight2;*/
	
	@FindBy(xpath=".//select[@name='duplicateCriteriaLeft']/ancestor::td/following-sibling::td//input[@type='button' and @value='>>']")
	public WebElement moveRight_ArrowButton_DuplicateCriteria1;
	
	@FindBy(xpath=".//select[@name='duplicateCriteriaLeft']/ancestor::td/following-sibling::td//input[@type='button' and @value='<<']")
	public WebElement moveLeft_ArrowButton_DuplicateCriteria1;
	
	@FindBy(xpath=".//select[@name='duplicateCriteriaLeft2']/ancestor::td/following-sibling::td//input[@type='button' and @value='>>']")
	public WebElement moveRight_ArrowButton_DuplicateCriteria2;
	
	@FindBy(xpath=".//select[@name='duplicateCriteriaLeft2']/ancestor::td/following-sibling::td//input[@type='button' and @value='<<']")
	public WebElement moveLeft_ArrowButton_DuplicateCriteria2;
	
	@FindBy(xpath=".//input[@type='button' and @value='Submit']")
	public WebElement submitBtn;
	
	@FindBy(xpath=".//input[@type='button' and @value='Cancel']")
	public WebElement cancelBtn;

	@FindBy(xpath=".//a[.='Configure Duplicate Lead Owner Assignment ']")
	public WebElement configureDuplicateLeadOwnerAssignment_Link; 
	
	@FindBy(xpath=".//select[@name='duplicateCriteriaLeft' and @class='multiList']")
	public WebElement availableFields_DuplicateCriteria1;
	
	@FindBy(xpath=".//select[@name='duplicateCriteriaLeft2' and @class='multiList']")
	public WebElement availableFields_DuplicateCriteria2;
	
	@FindBy(xpath=".//select[@name='duplicateCriteriaRight' and @class='multiList']")
	public WebElement configuredCriteria_DuplicateCriteria1;
	
	@FindBy(xpath=".//select[@name='duplicateCriteriaRight2' and @class='multiList']")
	public WebElement configuredCriteria_DuplicateCriteria2;
	
	
	public ConfigureDuplicateCriteriaUI(WebDriver driver) {
		super(driver);
	}
}
