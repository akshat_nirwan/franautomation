package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ConfigureDuplicateLeadOwnerAssignmentUI {
	
	public ConfigureDuplicateLeadOwnerAssignmentUI(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath=".//input[@type='radio' and @value='1']")
	public WebElement reassignExistingLeadToOwnerOfNewLead_radioBtn;
	
	@FindBy(xpath=".//input[@type='radio' and @value='2']")
	public WebElement assignNewLeadToOwnerOfExistingLead_radioBtn;
	
	@FindBy(xpath=".//input[@type='radio' and @value='3']")
	public WebElement assignNewLeadToOwnerBasedOnLeadAssignmentRules_radioBtn;  //  do not change the existing lead's owner (Lead Owners can be different)
	
	@FindBy(xpath=".//input[@type='radio' and @value='4']")
	public WebElement onlyAssignNewLeadToSameOwnerAsTheExistingLeadIfTheyAreAssignedToSameDivision_radioBtn;
	
	@FindBy(xpath=".//input[@type='submit' and @value='Submit']")
	public WebElement submitBtn;
	
	@FindBy(xpath=".//input[@type='button' and @value='Cancel']")
	public WebElement cancelBtn;
	
}
