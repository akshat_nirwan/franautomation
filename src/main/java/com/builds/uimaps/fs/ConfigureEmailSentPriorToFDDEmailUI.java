package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ConfigureEmailSentPriorToFDDEmailUI {

	@FindBy(xpath = ".//input[@name='isConfigure' and @value='Y']")
	public WebElement sendEmailNotificationYes;

	@FindBy(xpath = ".//input[@name='isConfigure' and @value='N']")
	public WebElement sendEmailNotificationNo;

	@FindBy(name = "mailSubject")
	public WebElement emailSubject;

	@FindBy(name = "timeInterval")
	public WebElement timeIntervalMinutes;

	@FindBy(id = "tinymce")
	public WebElement emailContentPriorToFDDEmail;

	@FindBy(name = "Submit")
	public WebElement saveBtn;

	public ConfigureEmailSentPriorToFDDEmailUI(WebDriver driver) {

		PageFactory.initElements(driver, this);
	}

}
