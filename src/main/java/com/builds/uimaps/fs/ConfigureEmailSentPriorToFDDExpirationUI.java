package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ConfigureEmailSentPriorToFDDExpirationUI {
	
	@FindBy(xpath=".//input[@name='isConfigure' and @value='Y']")
	public WebElement sendEmailNotification_Yes;
	
	@FindBy(xpath=".//input[@name='isConfigure' and @value='N']")
	public WebElement sendEmailNotification_No;
	
	@FindBy(id="subject")
	public WebElement emailSubject;
	
	@FindBy(id="days")
	public WebElement daysPrior;
	
	@FindBy(id="email")
	public WebElement fromEmail;
	
	@FindBy(id="ms-parentuserID")
	public WebElement recipients_dropdown;
	
	public String email_iframeID = "emailContent_ifr";
	
	@FindBy(id="tinymce")
	public WebElement emailBody;
	
	@FindBy(name="Submit")
	public WebElement configureBtn;
	
	
	
	public ConfigureEmailSentPriorToFDDExpirationUI(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
