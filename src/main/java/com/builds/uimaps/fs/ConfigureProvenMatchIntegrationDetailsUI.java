package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.builds.uimaps.common.CommonUI;

public class ConfigureProvenMatchIntegrationDetailsUI extends CommonUI {

	@FindBy(id = "franchisorId")
	public WebElement FranchisorID;

	public ConfigureProvenMatchIntegrationDetailsUI(WebDriver driver) {
		super(driver);
	}

}
