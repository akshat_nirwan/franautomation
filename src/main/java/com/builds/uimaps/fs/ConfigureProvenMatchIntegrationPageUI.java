package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ConfigureProvenMatchIntegrationPageUI {

	@FindBy(xpath=".//input[@id='isIntegrated' and @value='on']")
	public WebElement on;
	
	@FindBy(xpath=".//input[@id='isIntegrated' and @value='off']")
	public WebElement off;
	
	public ConfigureProvenMatchIntegrationPageUI(WebDriver driver)
	{	
		PageFactory.initElements(driver, this);
	}
}
