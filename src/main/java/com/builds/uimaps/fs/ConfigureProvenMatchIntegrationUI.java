package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.builds.uimaps.common.CommonUI;

public class ConfigureProvenMatchIntegrationUI extends CommonUI {

	@FindBy(xpath = ".//input[@name='isIntegrated' and @value='on']")
	public WebElement EnableProvenMatchIntegration_Radio_ON;

	@FindBy(xpath = ".//input[@name='isIntegrated' and @value='off']")
	public WebElement EnableProvenMatchIntegration_Radio_OFF;

	public ConfigureProvenMatchIntegrationUI(WebDriver driver) {
		super(driver);
	}

}
