package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ConfigureSalesEmailParsingServerUI {
	
	public ConfigureSalesEmailParsingServerUI(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath=".//input[@type='button' and @value='Add Server']")
	public WebElement addServer_Btn;
	
	// Add Pop Server
	@FindBy(xpath=".//input[@type='text' and @name='serverName']")
	public WebElement serverName_TextBox;
	
	@FindBy(xpath=".//input[@type='text' and @name='user']")
	public WebElement user_TextBox;
	
	@FindBy(xpath=".//input[@type='password' and @name='password']")
	public WebElement password_TextBox;
	
	@FindBy(xpath=".//input[@type='button' and @name='add']")
	public WebElement add_Btn;

}
