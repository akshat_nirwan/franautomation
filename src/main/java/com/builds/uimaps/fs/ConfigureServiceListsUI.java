package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ConfigureServiceListsUI {

	public ConfigureServiceListsUI(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = ".//input[@class='cm_new_button_link']")
	public WebElement addServiceBtn;

	@FindBy(name = "print")
	public WebElement printBtn;

	@FindBy(id = "serviceNameDetail")
	public WebElement serviceName;

	@FindBy(name = "Add")
	public WebElement addBtn;

	@FindBy(name = "Close")
	public WebElement closeBtn;

}
