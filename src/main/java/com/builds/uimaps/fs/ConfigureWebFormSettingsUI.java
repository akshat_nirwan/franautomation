package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ConfigureWebFormSettingsUI {

	@FindBy(id = "leadSource2ID")
	public WebElement leadSourceCategory;

	@FindBy(id = "leadSource3ID")
	public WebElement leadSourceDetails;

	public ConfigureWebFormSettingsUI(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
