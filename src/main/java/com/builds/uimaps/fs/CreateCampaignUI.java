package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CreateCampaignUI {
	
	@FindBy(id="CampaignName")
	public WebElement campaignName_text;
	
	@FindBy(id="Description")
	public WebElement description_text;
	
	@FindBy(id="Accessibility")
	public WebElement accessibility_dropDown;
	
	@FindBy(id="serviceID")
	public WebElement subscriptionType_dropDown;
	
	@FindBy(xpath=".//p[contains(text(),'Promotional')]/ancestor::label[@class='radio-lbl']")
	public WebElement Promotional_CampaignType_RadioBtn;
	
	@FindBy(xpath=".//p[contains(text(),'Status Driven')]/ancestor::label[@class='radio-lbl']")
	public WebElement StatusDriven_CampaignType_RadioBtn;
	
	@FindBy(xpath=".//button[@type='button' and  contains(text(),' Start')]")
	public WebElement startCampaign_Btn;
	
	@FindBy(xpath=".//button[@type='button' and  contains(text(),'Cancel')]")
	public WebElement cancelCampaign_Btn;
	
	public CreateCampaignUI(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
