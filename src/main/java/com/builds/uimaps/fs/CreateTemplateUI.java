package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CreateTemplateUI {

	@FindBy(id = "associateButton")
	public WebElement continue_Btn;
	
	@FindBy(xpath=".//button[@type='button' and contains(text(),'Save and Continue')]")
	public WebElement saveAndContinue_Btn;

	@FindBy(id = "codeYourOwn")
	public WebElement codeYourOwn;

	public CreateTemplateUI(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
