package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class DetailedHistoryUI {

	
	@FindBy(linkText = "Remarks and Call History")
	public WebElement RemarksandCallHistory_link;

	
	@FindBy(linkText = "Owner Change History")
	public WebElement OwnerChangeHistory_link;

	
	@FindBy(linkText = "Status Change History")
	public WebElement StatusChangeHistory_link;

	
	@FindBy(linkText = "Activity History")
	public WebElement ActivityHistory_link;

	
	@FindBy(linkText = "Message History")
	public WebElement MessageHistory_link;

	
	@FindBy(linkText = "SMS History")
	public WebElement SMSHistory_link;
	
	@FindBy(linkText= "Tasks")
	public WebElement Tasks_link;

	public DetailedHistoryUI(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
