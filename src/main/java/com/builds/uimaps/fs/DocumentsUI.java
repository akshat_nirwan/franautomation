package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class DocumentsUI {

	@FindBy(id = "documentTitle")
	public WebElement docTitle;

	@FindBy(xpath = ".//*[@id='fsDocumentAttachment']")
	public WebElement uploadDocument;

	public String getXpathOfDocumentTitle(int x) {
		return ".//tr[@id='row_" + x + "']//td[@align='left']//input[contains(@id,'documentTitle')]";
	}

	public String getXpathOfUploadDocument(int x) {
		return ".//tr[@id='row_" + x + "']//td[@align='left']//input[contains(@id,'fsDocumentAttachment')]";
	}

	// Buttons

	@FindBy(linkText = "Add Document")
	public WebElement addDocument;

	@FindBy(name = "Submit")
	public WebElement saveBtn;

	@FindBy(name = "Add More")
	public WebElement addMoreBtn;

	@FindBy(name = "printButton")
	public WebElement printBtn;

	@FindBy(name = "Close")
	public WebElement closeBtn;

	public DocumentsUI(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
