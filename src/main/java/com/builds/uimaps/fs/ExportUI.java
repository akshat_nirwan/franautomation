package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ExportUI {

	@FindBy(xpath = ".//input[@value='View Data']")
	public WebElement ViewData_Button;

	@FindBy(xpath = ".//input[@value='Search Data']")
	public WebElement searchData_Button;

	@FindBy(xpath = ".//input[@value='View']")
	public WebElement View_Button;

	@FindBy(xpath = ".//input[@value='Select Fields']")
	public WebElement SelectFields_Button;

	@FindBy(name = "FS_LEAD_DETAILS1_requestDateFrom")
	public WebElement FS_LEAD_DETAILS1_requestDateFrom;

	@FindBy(name = "FS_LEAD_DETAILS1_requestDateTo")
	public WebElement FS_LEAD_DETAILS1_requestDateTo;

	@FindBy(name = "FS_LEAD_DETAILS1_firstName")
	public WebElement FS_LEAD_DETAILS1_firstName;

	@FindBy(name = "FS_LEAD_DETAILS1_lastName")
	public WebElement FS_LEAD_DETAILS1_lastName;

	@FindBy(name = "FS_LEAD_DETAILS1_address")
	public WebElement FS_LEAD_DETAILS1_address;

	@FindBy(name = "FS_LEAD_DETAILS1_address2")
	public WebElement FS_LEAD_DETAILS1_address2;

	@FindBy(name = "FS_LEAD_DETAILS1_city")
	public WebElement FS_LEAD_DETAILS1_city;

	@FindBy(id = "ms-parentFS_LEAD_DETAILS1_country")
	public WebElement FS_LEAD_DETAILS1_country;

	@FindBy(id = "ms-parentFS_LEAD_DETAILS1_stateID")
	public WebElement FS_LEAD_DETAILS1_stateID;

	@FindBy(id = "ms-parentFS_LEAD_DETAILS1_countyID")
	public WebElement FS_LEAD_DETAILS1_countyID;

	@FindBy(id = "ms-parentFS_LEAD_DETAILS1_primaryPhoneToCall")
	public WebElement FS_LEAD_DETAILS1_primaryPhoneToCall;

	@FindBy(name = "FS_LEAD_DETAILS1_bestTimeToContact")
	public WebElement FS_LEAD_DETAILS1_bestTimeToContact;

	@FindBy(name = "FS_LEAD_DETAILS1_phone")
	public WebElement FS_LEAD_DETAILS1_phone;

	@FindBy(name = "FS_LEAD_DETAILS1_phoneExt")
	public WebElement FS_LEAD_DETAILS1_phoneExt;

	@FindBy(name = "FS_LEAD_DETAILS1_homePhone")
	public WebElement FS_LEAD_DETAILS1_homePhone;

	@FindBy(name = "FS_LEAD_DETAILS1_homePhoneExt")
	public WebElement FS_LEAD_DETAILS1_homePhoneExt;

	@FindBy(name = "FS_LEAD_DETAILS1_fax")
	public WebElement FS_LEAD_DETAILS1_fax;

	@FindBy(name = "FS_LEAD_DETAILS1_mobile")
	public WebElement FS_LEAD_DETAILS1_mobile;

	@FindBy(name = "FS_LEAD_DETAILS1_emailID")
	public WebElement FS_LEAD_DETAILS1_emailID;

	@FindBy(name = "FS_LEAD_DETAILS1_companyName")
	public WebElement FS_LEAD_DETAILS1_companyName;

	@FindBy(name = "FS_LEAD_DETAILS1_comments")
	public WebElement FS_LEAD_DETAILS1_comments;

	public ExportUI(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
