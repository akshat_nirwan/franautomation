package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FDDEmailTemplateSummaryUI {

	@FindBy(xpath = ".//input[@value='Add Template']")
	public WebElement addTemplateBtn;

	@FindBy(name = "mailTitle")
	public WebElement title;

	@FindBy(name = "mailSubject")
	public WebElement subject;

	@FindBy(name = "defaultFlag")
	public WebElement setAsDefaultTemplate_Checkbox;

	// Radio Buttons
	@FindBy(xpath = ".//input[@type='radio' and @value='defaultTemplate']")
	public WebElement emailType_Graphical;

	@FindBy(xpath = ".//input[@type='radio' and @value='Text']")
	public WebElement emailType_plainText;

	@FindBy(xpath = ".//input[@type='radio' and @value='HTML File Upload']")
	public WebElement emailType_uploadHtmlZipFile;

	public String iframeID = "ta_ifr";

	@FindBy(id = "ta")
	public WebElement plainTextBody;

	@FindBy(id = "HTML_FILE_ATTACHMENT")
	public WebElement htmlZipFileAttachment;

	/*
	 * @FindBy(xpath=".//a[contains(text(),'Attachment')]") public WebElement
	 * attachmentLink;
	 */

	@FindBy(linkText="Show All")
	public WebElement showAllLink;
	
	@FindBy(name = "Submit")
	public WebElement addBtn;

	@FindBy(name = "resetButton")
	public WebElement resetBtn;

	@FindBy(name = "Cancel")
	public WebElement cancelBtn;

	public FDDEmailTemplateSummaryUI(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
