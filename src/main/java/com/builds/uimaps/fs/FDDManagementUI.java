package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.builds.utilities.FranconnectUtil;

public class FDDManagementUI {

	@FindBy(xpath = ".//input[@value='Upload FDD']")
	public WebElement uploadFDDBtn;

	@FindBy(name = "ufocName")
	public WebElement fddName;

	@FindBy(name = "version")
	public WebElement version;

	@FindBy(name = "expiryDate")
	public WebElement dateOfExpiry;

	@FindBy(name = "item23ReceiptID")
	public WebElement item23ReceiptSales;

	@FindBy(name = "item23ReceiptIDFim")
	public WebElement item23ReceiptFim;

	@FindBy(name = "countryID")
	public WebElement countries;

	@FindBy(name = "brandID")
	public WebElement division;

	@FindBy(id = "ufocAction_file")
	public WebElement uploadFile;

	@FindBy(name = "comments")
	public WebElement comments;

	public WebElement associateWith(String stateProvinces, WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		return fc.utobj().getElementByXpath(driver, ".//span[contains(.,'" + stateProvinces + "')]/input[@type='checkbox']");
	}

	@FindBy(id = "ufocAction_0")
	public WebElement addFDDBtn;

	public FDDManagementUI(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
