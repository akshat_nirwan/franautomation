package com.builds.uimaps.fs;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FSBrokersPage {

	@FindBy(xpath = ".//a[@original-title='Manage Details of Brokers']")
	public WebElement brokersLnk ;

	@FindBy(id = "contactType")
	public WebElement brokerType;
	@FindBy(id = "firstName")
	public WebElement firstName;
	@FindBy(id = "lastName")
	public WebElement lastName;
	@FindBy(id = "address1")
	public WebElement address1;
	@FindBy(id = "address2")
	public WebElement address2;
	@FindBy(id = "city")
	public WebElement city;
	@FindBy(id = "countryID")
	public WebElement countryID;
	@FindBy(id = "zipcode")
	public WebElement zipcode;
	@FindBy(id = "stateID")
	public WebElement stateID;
	@FindBy(id = "homePhone")
	public WebElement homePhone;
	@FindBy(id = "workPhone")
	public WebElement workPhone;
	@FindBy(id = "fax")
	public WebElement fax;
	@FindBy(id = "mobile")
	public WebElement mobile;
	@FindBy(id = "bestTimeToContact")
	public WebElement bestTimeToContact;
	@FindBy(id = "primaryPhoneToCall")
	public WebElement primaryPhoneToCall;
	@FindBy(id = "emailID")
	public WebElement emailID;
	@FindBy(id = "brokerAgencyId")
	public WebElement brokerAgencyId;
	@FindBy(id = "priority")
	public WebElement priority;
	@FindBy(name = "comments")
	public WebElement comments;

	@FindBy(id = "Submit")
	public WebElement submitBtn;

	@FindBy(id = "brokerNameSearch")
	public WebElement brokerNameSearch;

	@FindBy(name = "checkBox1")
	public WebElement checkBoxAll;

	@FindBy(id = "search")
	public WebElement searchBtn;

	@FindBy(xpath = ".//input[@value='Add Brokers']")
	public WebElement addBrokersBtn ;

	@FindBy(xpath = ".//a[contains(text(),'Add Broker')]")
	public WebElement addBrokersQuickLinkBtn ;

	@FindBy(xpath = ".//a[contains(text(),'Add Campaign')]")
	public WebElement addCampaignQuickLinkBtn ;

	@FindBy(xpath = ".//input[@value='Ajouter Courtiers']")
	public WebElement addBrokersBtn2fr ;

	@FindBy(xpath = ".//*[@id='siteMainTable']/tbody/tr/td/form[9]/table/tbody/tr[3]/td/table/tbody/tr[2]/td/table/tbody//td[2]/a")
	public List<WebElement> listing;

	@FindBy(id = "cboxIframe")
	public WebElement cboxIframe ;

	/*
	 * Log a task
	 */

	@FindBy(id = "subject")
	public WebElement taskSubject;

	@FindBy(name = "schduleTime")
	public WebElement noReminderTask;

	@FindBy(name = "add")
	public WebElement addTaskBtn;

	/*
	 * Log a Call
	 */

	@FindBy(id = "subject")
	public WebElement callSubject;

	@FindBy(name = "submitButton")
	public WebElement addCallBtn;

	@FindBy(xpath = ".//input[@type='button' and @value='No']")
	public WebElement noToScheduleTask ;

	/*
	 * Archive
	 */

	@FindBy(id = "notes")
	public WebElement notesArchive;

	@FindBy(id = "Submit")
	public WebElement notesSubmitBtn;

	@FindBy(xpath = ".//input[@value='Close']")
	public WebElement cboxCloseBtn ;

	@FindBy(xpath = ".//input[@value='Ok']")
	public WebElement okCboxBtn ;

	@FindBy(name = "logTask")
	public WebElement logaTaskBottomBtn;

	@FindBy(name = "sendMail")
	public WebElement sendMailBottomBtn;

	@FindBy(name = "associateWithCampaign")
	public WebElement associateWithCampaignBottomBtn;

	@FindBy(xpath = ".//input[@value='Archive']")
	public WebElement archiveBottomBtn ;

	@FindBy(xpath = ".//input[@value='Delete']")
	public WebElement deleteBottomBtn ;

	@FindBy(xpath = ".//input[@class='cm_new_button' and @value='Cancel']")
	public WebElement cancelBtn ;

	@FindBy(xpath = ".//*[@id='ms-parentassignTo']")
	public WebElement brokerTaskOwner ;

	public FSBrokersPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
