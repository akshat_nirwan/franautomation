package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FSBrokersSummaryAddBrokersPage {
	@FindBy(id = "firstName")
	public WebElement firstName;
	@FindBy(id = "lastName")
	public WebElement lastName;
	@FindBy(id = "address1")
	public WebElement address1;
	@FindBy(id = "address2")
	public WebElement address2;
	@FindBy(id = "city")
	public WebElement city;
	@FindBy(id = "countryID")
	public WebElement countryID;
	@FindBy(id = "zipcode")
	public WebElement zipcode;
	@FindBy(id = "stateID")
	public WebElement stateID;
	@FindBy(id = "homePhone")
	public WebElement homePhone;
	@FindBy(id = "workPhone")
	public WebElement workPhone;
	@FindBy(id = "fax")
	public WebElement fax;
	@FindBy(id = "mobile")
	public WebElement mobile;
	@FindBy(id = "bestTimeToContact")
	public WebElement bestTimeToContact;
	@FindBy(id = "primaryPhoneToCall")
	public WebElement primaryPhoneToCall;
	@FindBy(id = "emailID")
	public WebElement emailID;
	@FindBy(id = "brokerAgencyId")
	public WebElement brokerAgencyId;
	@FindBy(id = "priority")
	public WebElement priority;
	@FindBy(name = "comments")
	public WebElement comments;
	@FindBy(id = "Submit")
	public WebElement submit;

	public FSBrokersSummaryAddBrokersPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
