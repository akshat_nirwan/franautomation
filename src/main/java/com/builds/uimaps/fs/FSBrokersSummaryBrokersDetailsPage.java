package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FSBrokersSummaryBrokersDetailsPage {

	@FindBy(linkText = "Modify")
	public WebElement modifyLnkTop ;

	@FindBy(linkText = "Send Email")
	public WebElement sendEmailLnkTop ;

	@FindBy(linkText = "Log a Task")
	public WebElement logATaskLnkTop ;

	@FindBy(linkText = "Log a Call")
	public WebElement logACallLnkTop ;

	@FindBy(linkText = "Log a Task")
	public WebElement logATaskOpenTaskLnk ;

	@FindBy(linkText = "Detailed History")
	public WebElement detailedHistoryActivityHistoryLnk ;

	@FindBy(linkText = "Log a Call")
	public WebElement logACallActivityHistoryLnk ;

	@FindBy(linkText = "Add Remarks")
	public WebElement addRemarksLnk ;

	@FindBy(name = "back")
	public WebElement backBtn;

	public FSBrokersSummaryBrokersDetailsPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
