package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FSBrokersSummaryPage {
	@FindBy(xpath = ".//input[@value='Add Brokers' and @type='button']")
	public WebElement addBrokers ;

	@FindBy(css = "button.ms-choice")
	public WebElement rolesBtn;

	@FindBy(css = "input.searchInputMultiple")
	public WebElement searchTxtBox;

	@FindBy(id = "selectAll")
	public WebElement selectAll;

	@FindBy(id = "search")
	public WebElement searchBtn;

	@FindBy(xpath = ".//*[@id='pettabs']/ul/li[2]/a/span")
	public WebElement archivedBrokersLnk ;

	public FSBrokersSummaryPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
