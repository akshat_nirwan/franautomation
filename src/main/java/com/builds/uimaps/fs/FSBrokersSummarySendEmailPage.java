package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FSBrokersSummarySendEmailPage {

	@FindBy(id = "mailTemplateID")
	public WebElement existingEmailTemplatesDrp;

	@FindBy(name = "subject")
	public WebElement subject;

	@FindBy(xpath = ".//iframe[@class='cke_wysiwyg_frame cke_reset' and @title='Rich Text Editor, ta']")
	public WebElement htmlFrame ;

	@FindBy(xpath = "html/body")
	public WebElement mailText ;

	@FindBy(id = "sendButton2")
	public WebElement sendBtn2;

	public FSBrokersSummarySendEmailPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
