package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FSCampaignCenterPage {

	@FindBy(xpath = ".//*[@id='CreateCampaignLink']/a[.='Campaign']")
	public WebElement createCampaign;

	@FindBy(xpath = ".//*[@id='siteMainTable']//a[.='Template']")
	public WebElement createTemplate;
	
	@FindBy(xpath = ".//*[@id='siteMainTable']//a[.='Recipients Group']")
	public WebElement recipientGroup ;

	@FindBy(id = "createCampaign")
	public WebElement btnCreateCampaign;

	@FindBy(id = "CampaignName")
	public WebElement campaignName;

	@FindBy(id = "groupName")
	public WebElement groupName;

	@FindBy(id = "groupDescription")
	public WebElement groupDescription;

	@FindBy(xpath = ".//button[contains(text(),'Create')]")
	public WebElement createGroupBtn ;

	@FindBy(xpath = ".//button[contains(text(),'Manage Campaigns') or contains(text(), 'Create Campaign')]")
	public WebElement manageCampaignBtn ;

	@FindBy(xpath = ".//button[contains(text(),'Create Campaign')]")
	public WebElement createCampaignBtn ;

	@FindBy(xpath = ".//button[contains(text(),'Manage Templates')]")
	public WebElement managetTemplateBtn ;

	@FindBy(xpath = ".//button[contains(text(),'Create Template')]")
	public WebElement createTemplateTab ;

	@FindBy(id = "createTemplate")
	public WebElement createTemplateBtn;

	@FindBy(id = "Description")
	public WebElement description;

	@FindBy(id = "Accessibility")
	public WebElement campaignAccessibility;

	@FindBy(xpath = ".//button[contains(text(),'Start')]")
	public WebElement startBtn ;

	@FindBy(xpath = ".//a[.='Code your own']")
	public WebElement codeYourOwn ;

	@FindBy(id = "mailTitle")
	public WebElement templateName;

	@FindBy(xpath = ".//*[@id='CodeYourOwn']//input[@name='mailTitle']")
	public WebElement templateNameTitle ;

	@FindBy(id = "mailSubject")
	public WebElement mailSubject;

	@FindBy(xpath = ".//*[@id='CodeYourOwn']//input[@name='mailSubject']")
	public WebElement emailSubject ;

	@FindBy(xpath = ".//label[@for='PlaneText']")
	public WebElement plainTextEmailType ;

	@FindBy(xpath = ".//*[@id='CodeYourOwn']//label[@for='typeRadio2']")
	public WebElement CodeOwnplainTextEmailType ;

	@FindBy(id = "ta")
	public WebElement textAreaPlainText;

	@FindBy(xpath = ".//*[@id='keywords1']/button")
	public WebElement keywordsBtn ;

	@FindBy(id = "captivateTemplateID1")
	public WebElement emailTypeDropDown;

	@FindBy(xpath = ".//button[contains(text(),'Save and Continue')]")
	public WebElement saveAndContinue ;

	@FindBy(xpath = ".//button[.='Finalize And Launch Campaign']")
	public WebElement finalizeAndLaunchCampaignBtn ;

	@FindBy(xpath = ".//*[@id='siteMainTable']//button[.='Continue']")
	public WebElement continueBtn ;

	@FindBy(xpath = ".//button[contains(text(),'Launch Campaign')]")
	public WebElement LaunchCampaignBtn ;

	@FindBy(id = "associateLater")
	public WebElement associateLater;

	@FindBy(xpath = ".//button[contains(text(),'Associate Later')]")
	public WebElement codeOwnAssociateLater ;

	@FindBy(xpath = ".//*[@id='form1']//button[.='Save']")
	public WebElement saveBtn ;

	@FindBy(xpath = ".//div[contains(text(),'Action')]")
	public WebElement actionBtn ;

	@FindBy(xpath = ".//button[contains(text(),'View Campaigns')]")
	public WebElement veiwCampaings;

	@FindBy(xpath = ".//a[contains(text(),'Modify')]")
	public WebElement ModifyCampaign ;

	@FindBy(xpath = ".//a[contains(text(),'Test Campaign')]")
	public WebElement testCampaign ;

	@FindBy(xpath = ".//a[contains(text(),'Archive') and @onclick]")
	public WebElement archiveCampaign ;

	@FindBy(xpath = ".//a[contains(text(),'Delete')]")
	public WebElement deleteCampaign ;

	@FindBy(xpath = ".//button[contains(text(),'Modify')]")
	public WebElement modifyBasicInfo ;

	@FindBy(xpath = ".//button[contains(text(),'Save')]")
	public WebElement saveModifyCbox ;

	@FindBy(xpath = ".//button[contains(text(),'Save Campaign')]")
	public WebElement saveModifyCampaign ;

	@FindBy(id = "email")
	public WebElement testEmailId;

	@FindBy(xpath = ".//button[contains(text(),'Close')]")
	public WebElement closeCbox ;

	@FindBy(xpath = ".//button[contains(text(),'Test')]")
	public WebElement testBtn ;

	@FindBy(xpath = ".//*[@id='FilterTabShowHide']//a[contains(text(),'Archived Campaigns')]")
	public WebElement archivedCampaingsTab ;

	@FindBy(xpath = ".//*[@id='FilterTabShowHide']/li[1]/a[contains(text(),'Campaigns')]")
	public WebElement CampaingsTab ;

	@FindBy(xpath = ".//*[@id='campaignSearchForm']//a[contains(text(),'Unarchive')]")
	public WebElement unarchiveAction ;

	@FindBy(xpath = ".//div[contains(text(),'Create')]")
	public WebElement createBtn ;

	@FindBy(xpath = ".//*[@id='existTemp']/a")
	public WebElement chooseFromExisting ;

	@FindBy(id = "associateButton")
	public WebElement associateAndContinueBtn;

	@FindBy(xpath = ".//div[@data-target='#FilterContainer']/*/*[1]")
	public WebElement campaignFilter ;

	@FindBy(xpath = ".//label[@for='FilterRecipients']")
	public WebElement fiterRecipients;

	@FindBy(id = "searchCampaign")
	public WebElement searchCampaignName;

	@FindBy(id = "search")
	public WebElement searchFilter;

	@FindBy(id = "select")
	public WebElement keywordsDropDown;

	@FindBy(xpath = ".//div[@data-role='ico_Associate' and @title='Associate']")
	public WebElement associateCampaign ;

	@FindBy(xpath = ".//button[contains(text(),'Confirm')]")
	public WebElement confirmBtn ;

	@FindBy(id = "keywords-button")
	public WebElement insertKeyWord;

	@FindBy(id = "copyTextBox")
	public WebElement copyKeywordValue;

	@FindBy(xpath = ".//*[@id='tinymce']/p")
	public WebElement editor;

	public FSCampaignCenterPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
