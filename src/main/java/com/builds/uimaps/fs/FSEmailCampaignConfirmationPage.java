package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FSEmailCampaignConfirmationPage {

	@FindBy(xpath = ".//input[@value='Confirm']")
	public WebElement confirmBtn ;

	@FindBy(xpath = ".//input[@value='Cancel']")
	public WebElement cancelBtn ;

	@FindBy(xpath = ".//input[@value='Back']")
	public WebElement backBtn ;

	public FSEmailCampaignConfirmationPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
