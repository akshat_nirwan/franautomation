package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FSEmailCampaignsPage {

	public FSEmailCampaignsPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "createInfomercial")
	public WebElement createInfomercial;

	@FindBy(xpath = ".//iframe[@class='newLayoutcboxIframe']")
	public WebElement iframe ;

	@FindBy(id = "informName")
	public WebElement informName;

	@FindBy(id = "informIdenty")
	public WebElement informIdenty;

	@FindBy(id = "fileName")
	public WebElement fileNameUpload;

	@FindBy(id = "imageName")
	public WebElement uploadThumb;

	@FindBy(name = "save")
	public WebElement saveBtn;

	@FindBy(xpath = ".//button[contains(text(),'Close')]")
	public WebElement closeBtn ;

	@FindBy(xpath = ".//*[@id='FilterTabShowHide']/li/a[@href='fsInfomercials']")
	public WebElement informacialTab ;

	@FindBy(xpath = ".//div[contains(text(),'View Per Page')]")
	public WebElement clickViewPerPage ;

	@FindBy(xpath = ".//a[contains(@href , 'setResultsPerPage(500)')]")
	public WebElement clickViewPerPage500 ;

	@FindBy(xpath = ".//div[@id='PrintHeaderMenuContainer']//div[@data-role='ico_Menu']")
	public WebElement headerBtn ;

	@FindBy(xpath = ".//*[@id='PrintHeaderMenuContainer']//a[@href='fstemplateSummary']")
	public WebElement templateOption ;

	@FindBy(xpath = ".//*[@id='FilterTabShowHide']/li/a[@href='fstemplateSummary']")
	public WebElement emailTamplateTab ;

}
