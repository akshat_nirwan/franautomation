package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FSGroupsAddGroupPage {

	@FindBy(id = "groupName")
	public WebElement groupName;

	@FindBy(id = "groupDescription")
	public WebElement groupDescription;

	@FindBy(id = "submitButton")
	public WebElement addBtn;

	public FSGroupsAddGroupPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
