package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FSGroupsArchivedGroupsPage {

	@FindBy(id = "searchTemplate")
	public WebElement searchTxt;

	@FindBy(id = "matchType1")
	public WebElement creationDataDrp;

	@FindBy(id = "search")
	public WebElement searchBtn;

	@FindBy(name = "checkBox")
	public WebElement checkAll;

	@FindBy(name = "Restore")
	public WebElement restoreBtn;

	@FindBy(xpath = ".//*[@id='pettabs']/ul/li[1]/a/span")
	public WebElement groupsTab ;

	public FSGroupsArchivedGroupsPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
