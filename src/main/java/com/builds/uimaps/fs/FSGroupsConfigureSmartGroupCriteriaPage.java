package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FSGroupsConfigureSmartGroupCriteriaPage {

	@FindBy(xpath = ".//Select[id='selColList']/optgroup[]/")
	public WebElement availableField ;

	@FindBy(name = "go")
	public WebElement goBtn;

	@FindBy(name = "LEAD_SECONDARY_CONTACT_DETAILS_firstName")
	public WebElement firstNameTxt;

	@FindBy(name = "LEAD_SECONDARY_CONTACT_DETAILS_lastName")
	public WebElement lastNameTxt;

	@FindBy(name = "LEAD_SECONDARY_CONTACT_DETAILS_emailID")
	public WebElement emailId;

	@FindBy(name = "Next")
	public WebElement OKBtn;

	public FSGroupsConfigureSmartGroupCriteriaPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
