
package com.builds.uimaps.fs;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FSGroupsPage {

	@FindBy(xpath = ".//*[@id='createGroup']")
	public WebElement addGroupsBtn ;

	@FindBy(id = "groupName")
	public WebElement groupName;

	@FindBy(id = "groupDescription")
	public WebElement groupDescription;

	@FindBy(id = "submitButton")
	public WebElement submitButton;

	@FindBy(id = "groupType")
	public WebElement accessibility;

	@FindBy(xpath = ".//*[@id='applyFilter1']")
	public WebElement applyFilter;

	@FindBy(xpath = ".//button[contains(text(),'Save')]")
	public WebElement saveButton ;

	@FindBy(xpath = ".//button[@onclick='submitRecipeints();']")
	public WebElement continueButton ;

	@FindBy(xpath = ".//a[@original-title='Group Your Leads to Manage Them']")
	public WebElement groupLnk ;

	@FindBy(xpath = ".//*[@id='addGroupForm1']//button[2]")
	public WebElement addBtn ;

	@FindBy(id = "searchTemplate")
	public WebElement searchTxt;

	@FindBy(id = "matchType1")
	public WebElement creationDataDrp;

	@FindBy(id = "search")
	public WebElement searchBtn;

	@FindBy(xpath = ".//*[@id='addGroupForm1']//label[@for='groupChoiceTemp']")
	public WebElement groupTypeSmart ;

	@FindBy(xpath = ".//input[@value='Add & Set Criteria']")
	public WebElement addAndSetCriteriaBtn ;

	@FindBy(xpath = ".//button[contains(text(),'Delete')]")
	public WebElement deleteBtn ;

	@FindBy(xpath = ".//button[contains(text(),'Archive')]")
	public WebElement archiveBtn ;

	@FindBy(xpath = ".//*[@id='siteMainTable']//button[contains(text(),'Restore')]")
	public WebElement restoreBtn ;

	@FindBy(xpath = ".//*[@id='pettabs']/ul/li[2]/a/span")
	public WebElement archivedGroupsTab ;

	@FindBy(xpath = ".//input[@value='Modify']")
	public WebElement modifyBtn ;

	@FindBy(xpath = ".//*[@id='form1']/table/tbody/tr[2]/td/table/tbody//td[2]/a")
	public List<WebElement> listing;

	@FindBy(xpath = ".//*[@id='siteMainTable']/tbody//form[2]//div[@class='svg-icon data-action']")
	public WebElement openSearchFilters ;

	@FindBy(xpath = ".//*[@id='fc-drop-parentselColList']/button")
	public WebElement availableFields ;

	@FindBy(xpath = ".//*[@id='fc-drop-parentFS_LEAD_DETAILS_country']/button")
	public WebElement FS_LEAD_DETAILS_country ;

	@FindBy(xpath = ".//*[@id='fc-drop-parentFS_LEAD_DETAILS_stateID']/button")
	public WebElement FS_LEAD_DETAILS_stateID ;

	@FindBy(xpath = ".//div[@id='fc-drop-parentFS_LEAD_DETAILS_country']/div/div/input[@placeholder='Search...']")
	public WebElement FS_LEAD_DETAILS_countrySearchInput ;

	@FindBy(xpath = ".//div[@id='fc-drop-parentFS_LEAD_DETAILS_stateID']/div/div/input[@placeholder='Search...']")
	public WebElement FS_LEAD_DETAILS_stateIDSearchInput ;

	@FindBy(xpath = ".//*[@id='fc-drop-parentFS_LEAD_DETAILS_country']/div/ul/li[1]/label/span[contains(text(),'Select All')]")
	public WebElement selectAllCountry ;

	@FindBy(xpath = ".//*[@id='fc-drop-parentFS_LEAD_DETAILS_stateID']/div/ul/li[1]/label/span[contains(text(),'Select All')]")
	public WebElement selectAllState ;

	@FindBy(xpath = ".//input[@name='FS_LEAD_DETAILS_emailID']")
	public WebElement sendAvaiEmailId ;

	@FindBy(xpath = ".//span[contains(text(),'Primary Info')]/ancestor::li/following-sibling::li/label/span[contains(text(),'First Name')]")
	public WebElement firstNameAvlbFields ;

	@FindBy(xpath = ".//span[contains(text(),'Primary Info')]/ancestor::li/following-sibling::li/label/span[contains(text(),'Last Name')]")
	public WebElement lastNameAvlbFields ;

	@FindBy(xpath = ".//span[contains(text(),'Primary Info')]/ancestor::li/following-sibling::li/label/span[contains(text(),'Country')]")
	public WebElement countryAvlbFields ;

	@FindBy(xpath = ".//span[contains(text(),'Primary Info')]/ancestor::li/following-sibling::li/label/span[contains(text(),'State / Province')]")
	public WebElement stateAvlbFields ;

	@FindBy(xpath = ".//span[contains(text(),'Primary Info')]/ancestor::li/following-sibling::li/label/span[contains(text(),'Email')]")
	public WebElement emailAvlbFields ;

	@FindBy(xpath = ".//*[@id='FS_LEAD_DETAILS_firstName_DIV']/input")
	public WebElement containsFirstName ;

	@FindBy(xpath = ".//input[@placeholder='Search...']")
	public WebElement availableFieldsInput ;
	
	// akshat
	@FindBy(xpath = ".//input[@class='searchInputMultiple']")
	public WebElement selectCriteria_availableFields_Input ;
	
	@FindBy(xpath = ".//input[@class='search-btn on']")
	public WebElement selectCriteria_availableFields_searchButton ;
	

	@FindBy(xpath = ".//*[@id='FS_LEAD_DETAILS_lastName_DIV']/input")
	public WebElement containsLastName ;

	@FindBy(xpath = ".//input[@value='Remove from Group']")
	public WebElement removeFromGrpBtn ;

	@FindBy(xpath = ".//input[@type='Submit']")
	public WebElement addToGrpBtn ;

	@FindBy(xpath = ".//input[@value='Ok']")
	public WebElement okBtn ;

	@FindBy(xpath = ".//div[@id='fc-drop-parentgroupType']")
	public WebElement groupTypeDiv;

	@FindBy(xpath = ".//input[@id='date-box1']")
	public WebElement creationDateInput;

	@FindBy(xpath = ".//button[contains(text(),'Cancel')]")
	public WebElement cancelBtn;

	public FSGroupsPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
