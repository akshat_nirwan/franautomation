package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FSHomePage {

	@FindBy(linkText = "Manage Widgets")
	public WebElement manageWidgets ;

	@FindBy(xpath = ".//*[@id='recentLeads']/table/tbody/tr[1]/td/table/tbody/tr/td/table/tbody/tr/td[2]/a[1]")
	// @FindBy(linkText="Add Lead")
	public WebElement addLeadLnk ;

	@FindBy(linkText = "More")
	public WebElement moreLink ;

	@FindBy(xpath = ".//div[@class='dataText']")
	public WebElement widgetsSection ;

	@FindBy(linkText = "Click Here to View More")
	public WebElement clickHereToViewMore ;

	@FindBy(id = "dashboardc")
	public WebElement dashboardc;

	@FindBy(id = "eventCalenderc")
	public WebElement eventCalenderc;

	@FindBy(id = "heatMeterc")
	public WebElement heatMeterc;

	@FindBy(id = "forecastsc")
	public WebElement forecastsc;

	@FindBy(id = "candidatePortalHomec")
	public WebElement candidatePortalHomec;

	@FindBy(id = "recentlyViewedLeadsc")
	public WebElement recentlyViewedLeadsc;

	@FindBy(id = "salesFunnelc")
	public WebElement salesFunnelc;

	@FindBy(id = "tasksFSc")
	public WebElement tasksFSc;

	@FindBy(id = "todaysEventc")
	public WebElement todaysEventc;

	@FindBy(name = "save")
	public WebElement saveWidgetBtn;

	@FindBy(id = "recentLeads")
	public WebElement recentLeadDiv;

	@FindBy(id = "dateCombo")
	public WebElement salesFunnel;

	public FSHomePage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
