package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FSImportPage {

	@FindBy(xpath = ".//input[@name='salesFileName']")
	public WebElement importFile;

	@FindBy(xpath = ".//input[@name='continue']")
	public WebElement continueBtn;

	@FindBy(xpath = ".//input[@name='back']")
	public WebElement backBtn;

	@FindBy(xpath = ".//*[@id='mail']")
	public WebElement caanadianLeadDisclaimer;

	@FindBy(xpath = ".//*[@id='SALES_DATA__FIRST_NAME']")
	public WebElement firstName;

	@FindBy(xpath = ".//*[@id='SALES_DATA__LAST_NAME']")
	public WebElement lastName;

	@FindBy(xpath = ".//*[@id='SALES_DATA__EMAIL_ID']")
	public WebElement email;

	@FindBy(xpath = ".//*[@id='SALES_DATA__COMPANY_NAME']")
	public WebElement companyName;

	@FindBy(xpath = ".//*[@id='SALES_DATA__ADDRESS']")
	public WebElement address1;

	@FindBy(xpath = ".//*[@id='SALES_DATA__ADDRESS2']")
	public WebElement address2;

	@FindBy(xpath = ".//*[@id='SALES_DATA__CITY']")
	public WebElement city;

	@FindBy(xpath = ".//*[@id='SALES_DATA__COUNTRY_ID']")
	public WebElement country;

	@FindBy(xpath = ".//*[@id='SALES_DATA__STATE_ID']")
	public WebElement stateProvince;

	@FindBy(xpath = ".//*[@id='SALES_DATA__ZIP']")
	public WebElement zipPostalCode;

	@FindBy(xpath = ".//*[@id='SALES_DATA__PHONE']")
	public WebElement workPhone;

	@FindBy(xpath = ".//*[@id='SALES_DATA__PHONE_EXT']")
	public WebElement workPhoneExt;

	@FindBy(xpath = ".//*[@id='SALES_DATA__HOME_PHONE']")
	public WebElement homePhone;

	@FindBy(xpath = ".//*[@id='SALES_DATA__HOME_PHONE_EXT']")
	public WebElement homePhoneExt;

	@FindBy(xpath = ".//*[@id='SALES_DATA__FAX']")
	public WebElement fax;

	@FindBy(xpath = ".//*[@id='SALES_DATA__COMMENTS']")
	public WebElement comments;

	@FindBy(xpath = ".//*[@id='SALES_DATA__MOBILE']")
	public WebElement mobile;

	@FindBy(xpath = ".//*[@id='SALES_DATA__BEST_TIME_TO_CONTACT']")
	public WebElement bestTimetoContact;

	@FindBy(xpath = ".//*[@id='SALES_DATA__LEAD_STATUS_ID']")
	public WebElement leadStatus;

	@FindBy(xpath = ".//*[@id='SALES_DATA__LEAD_SOURCE2_ID']")
	public WebElement leadSourceCatagory;

	@FindBy(xpath = ".//*[@id='SALES_DATA__LEAD_SOURCE3_ID']")
	public WebElement leadSourceDetails;

	@FindBy(xpath = ".//*[@id='SALES_DATA__LEAD_OWNER_ID']")
	public WebElement assignTo;

	@FindBy(xpath = ".//*[@id='SALES_DATA__PRIMARY_PHONE_TO_CALL']")
	public WebElement preferedModeOfContact;

	@FindBy(xpath = ".//*[@id='SALES_DATA__LIQUID_CAPITAL_MIN']")
	public WebElement currentNetWorth;

	@FindBy(xpath = ".//*[@id='SALES_DATA__LIQUID_CAPITAL_MAX']")
	public WebElement cashAvailableForInves;

	@FindBy(xpath = ".//*[@id='SALES_DATA__INVEST_TIMEFRAME']")
	public WebElement investmentTimeframe;

	@FindBy(xpath = ".//*[@id='SALES_DATA__SOURCE_OF_FUNDING']")
	public WebElement sourceOfInvestment;

	@FindBy(xpath = ".//*[@id='SALES_DATA__BACKGROUND']")
	public WebElement background;

	@FindBy(xpath = ".//*[@id='SALES_DATA__DIVISION']")
	public WebElement division;

	@FindBy(xpath = ".//*[@id='SYSTEM_DATA__DEFAULT_REQUEST_DATE']")
	public WebElement inquiryDate;

	@FindBy(xpath = ".//*[@id='SYSTEM_DATA__DEFAULT_LEAD_OWNER_ID']")
	public WebElement defaultLeadOwner;

	@FindBy(xpath = ".//*[@id='SYSTEM_DATA__DEFAULT_LEAD_STATUS_ID']")
	public WebElement defaultLeadStatus;

	@FindBy(xpath = ".//*[@id='SYSTEM_DATA__DEFAULT_LEAD_SOURCE2_ID']")
	public WebElement defaultLeadSourceCatagory;

	@FindBy(xpath = ".//*[@id='SYSTEM_DATA__DEFAULT_LEAD_SOURCE3_ID']")
	public WebElement defaultLeadSourceDetails;

	public FSImportPage(WebDriver driver) {

		PageFactory.initElements(driver, this);
	}

}
