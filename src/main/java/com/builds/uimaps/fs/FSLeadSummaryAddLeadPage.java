package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FSLeadSummaryAddLeadPage {

	@FindBy(id = "salutation")
	public WebElement salutation;

	@FindBy(id = "ownerType")
	public WebElement ownerType;

	@FindBy(id = "firstName")
	public WebElement firstName;

	@FindBy(id = "lastName")
	public WebElement lastName;

	@FindBy(id = "address")
	public WebElement address;

	@FindBy(id = "address2")
	public WebElement address2;

	@FindBy(id = "city")
	public WebElement city;

	@FindBy(id = "country")
	public WebElement country;

	@FindBy(id = "stateID")
	public WebElement state;

	@FindBy(id = "zip")
	public WebElement zip;

	@FindBy(id = "countyID")
	public WebElement county;

	@FindBy(id = "primaryPhoneToCall")
	public WebElement preferredModeofContact;

	@FindBy(id = "bestTimeToContact")
	public WebElement bestTimeToContact;

	@FindBy(id = "phone")
	public WebElement phone;

	@FindBy(id = "phoneExt")
	public WebElement phoneExt;

	@FindBy(id = "homePhone")
	public WebElement homePhone;

	@FindBy(id = "homePhoneExt")
	public WebElement homePhoneExt;

	@FindBy(id = "fax")
	public WebElement fax;

	@FindBy(id = "mobile")
	public WebElement mobile;

	@FindBy(id = "emailID")
	public WebElement emailID;

	@FindBy(id = "companyName")
	public WebElement companyName;

	@FindBy(id = "comments")
	public WebElement comments;

	@FindBy(id = "leadOwnerID")
	public WebElement leadOwnerID;

	@FindBy(id = "leadRatingID")
	public WebElement leadRatingID;

	@FindBy(id = "marketingCodeId")
	public WebElement marketingCode;

	@FindBy(id = "leadSource2ID")
	public WebElement leadSource2ID;

	@FindBy(id = "leadSource3ID")
	public WebElement leadSource3ID;

	@FindBy(id = "brokerID")
	public WebElement brokerDetails;

	@FindBy(name = "otherLeadSourceDetail")
	public WebElement otherLeadSourceDetail;

	@FindBy(id = "liquidCapitalMin")
	public WebElement currentNetWorth;

	@FindBy(id = "liquidCapitalMax")
	public WebElement cashAvailableForInvestment;

	@FindBy(id = "investTimeframe")
	public WebElement investTimeframe;

	@FindBy(id = "background")
	public WebElement background;

	@FindBy(id = "sourceOfFunding")
	public WebElement sourceOfFunding;

	@FindBy(id = "nextCallDate")
	public WebElement nextCallDate;

	@FindBy(id = "noOfUnitReq")
	public WebElement noOfUnitReq;

	@FindBy(id = "locationImage")
	public WebElement locationImage;

	@FindBy(id = "temppreferredCity1")
	public WebElement temppreferredCity1;

	@FindBy(id = "temppreferredCountry1")
	public WebElement temppreferredCountry1;

	@FindBy(id = "temppreferredStateId1")
	public WebElement temppreferredStateId1;

	@FindBy(id = "temppreferredCity2")
	public WebElement temppreferredCity2;

	@FindBy(id = "temppreferredCountry2")
	public WebElement temppreferredCountry2;

	@FindBy(id = "temppreferredStateId2")
	public WebElement temppreferredStateId2;

	@FindBy(id = "templocationId1")
	public WebElement templocationId1;

	@FindBy(id = "forecastClosureDate")
	public WebElement forecastClosureDate;

	@FindBy(id = "probability")
	public WebElement probability;

	@FindBy(id = "forecastRating")
	public WebElement forecastRating;

	@FindBy(id = "forecastRevenue")
	public WebElement forecastRevenue;

	@FindBy(id = "campaignID")
	public WebElement campaignID;

	@FindBy(xpath = ".//input[@name='Submit' and @value='Submit']")
	public WebElement save;

	// Anukaran
	@FindBy(xpath = ".//*[@id='siteMainTable']//td[contains(text(),'Click green icon to check for duplicate leads')]/ancestor::tr/td/a/img")
	public WebElement duplcateIcon ;

	@FindBy(xpath = ".//*[@id='fsLeadTable']//a[contains(text(),'Add')]")
	public WebElement addLeadLnk ;

	@FindBy(id = "searchString")
	public WebElement topSearchField;

	@FindBy(xpath = ".//*[@id='leadStatusID']")
	public WebElement leadStatus ;

	@FindBy(xpath = ".//input[@value='Change Status']")
	public WebElement chngLeadStatusBtn ;

	@FindBy(xpath = ".//*[@id='locationImage']")
	public WebElement maximizePreferedLocation ;

	@FindBy(id = "ms-parentbrandMapping_0brandID")
	public WebElement divisionDropDown;

	@FindBy(xpath = ".//*[@id='assignAutomaticCampaign1']")
	public WebElement assignAutomaticCampaign ;

	@FindBy(id = "autoComplete")
	public WebElement selectOwner;

	@FindBy(xpath = ".//input[@name='ownerSetting' and @value='YES']")
	public WebElement linkLeadYes;

	@FindBy(xpath = ".//a[contains(text(),'Modify')]")
	public WebElement modifyLead;

	@FindBy(xpath = ".//input[@name='automatic' and @value='As per Assignment Scheme']")
	public WebElement automatic;

	@FindBy(id = "liquidCapitalMax")
	public WebElement cashAvailableForInves;

	@FindBy(id = "investTimeframe")
	public WebElement investmentTimeframe;

	@FindBy(id = "sourceOfFunding")
	public WebElement sourceOfInvestment;

	@FindBy(xpath = ".//*[@id='automatic']")
	public WebElement basedOnAssignment;

	@FindBy(xpath = "")
	public WebElement assignAutomaticCampaign1;

	public FSLeadSummaryAddLeadPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
