package com.builds.uimaps.fs;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FSLeadSummaryCoApplicantsPage {

	@FindBy(xpath = ".//*[@id='siteMainTable']/tbody/tr/td/table[2]/tbody/tr[3]/td[2]/table/tbody/tr[1]/td/table/tbody/tr/td[2]/input")
	public WebElement addCoApplicantBtn ;

	@FindBy(id = "chooseExisting")
	public WebElement chooseExisting;

	@FindBy(id = "firstName")
	public WebElement firstName;

	@FindBy(id = "lastName")
	public WebElement lastName;

	@FindBy(id = "coApplicantRelationshipID")
	public WebElement coApplicantRelationshipID;

	@FindBy(id = "phone")
	public WebElement phone;

	@FindBy(id = "ext")
	public WebElement ext;

	@FindBy(id = "fax")
	public WebElement fax;

	@FindBy(id = "emailID")
	public WebElement emailID;

	@FindBy(id = "address")
	public WebElement address;

	@FindBy(id = "city")
	public WebElement city;

	@FindBy(id = "country")
	public WebElement country;

	@FindBy(id = "stateID")
	public WebElement stateID;

	@FindBy(id = "zip")
	public WebElement zip;

	@FindBy(xpath = ".//input[@name='Submit' and @value='Add']")
	public WebElement submit ;

	@FindBy(xpath = ".//*[@id='siteMainTable']/tbody/tr/td/table[2]/tbody/tr[3]/td[2]/table/tbody/tr[2]/td/table/tbody//td[1]/a")
	public List<WebElement> list;

	public FSLeadSummaryCoApplicantsPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
