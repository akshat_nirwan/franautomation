package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FSLeadSummaryCompliancePage {

	@FindBy(id = "fddDate")
	public WebElement fddDate;

	@FindBy(id = "recByFrancDate1")
	public WebElement recByFrancDate1;

	@FindBy(id = "bussDayExp10Date")
	public WebElement bussDayExp10Date;

	@FindBy(id = "versionOfUfoc")
	public WebElement versionOfUfoc;

	@FindBy(id = "ipAddress")
	public WebElement ipAddress;

	@FindBy(id = "browserType")
	public WebElement browserType;

	@FindBy(id = "firstFrancPaymentDate")
	public WebElement firstFrancPaymentDate;

	@FindBy(id = "stateRegReq")
	public WebElement stateRegReq;

	@FindBy(id = "secondFrancPaymentDate")
	public WebElement secondFrancPaymentDate;

	@FindBy(xpath = ".//*[@id='francCommiteeApproval'][@value='1']")
	public WebElement francCommiteeApprovalYes ;

	@FindBy(id = "faRequestedDate")
	public WebElement faRequestedDate;

	@FindBy(id = "faReceivedDate")
	public WebElement faReceivedDate;

	@FindBy(id = "francRecAgrDate")
	public WebElement francRecAgrDate;

	@FindBy(id = "bussDay5ExpDate")
	public WebElement bussDay5ExpDate;

	@FindBy(id = "francSignAgrDate")
	public WebElement francSignAgrDate;

	@FindBy(id = "bussDayRule10Check")
	public WebElement bussDayRule10Check;

	@FindBy(id = "bussDayRule10AgrDate")
	public WebElement bussDayRule10AgrDate;

	@FindBy(id = "versionFrancAgr")
	public WebElement versionFrancAgr;

	@FindBy(id = "franFeeAmt")
	public WebElement franFeeAmt;

	@FindBy(id = "franFeeDate")
	public WebElement franFeeDate;

	@FindBy(id = "areaFeeAmt")
	public WebElement areaFeeAmt;

	@FindBy(id = "areaFeeDate")
	public WebElement areaFeeDate;
	
	@FindBy(id = "adaDate")
	public WebElement adaExecutionDate;

	@FindBy(id = "faExecutionDate")
	public WebElement faExecutionDate;

	@FindBy(xpath = ".//*[@id='contractRecSign' and @value='1']")
	public WebElement contractRecSign ;

	@FindBy(xpath = ".//*[@id='leaseRiderProperlySign'][1]")
	public WebElement leaseRiderProperlySign ;

	@FindBy(xpath = ".//*[@id='perCovenantAgrProperlySign'][@value='1']")
	public WebElement perCovenantAgrProperlySign ;

	@FindBy(xpath = ".//*[@id='guaranteeProperlySign'][1]")
	public WebElement guaranteeProperlySign ;

	@FindBy(xpath = ".//*[@id='otherDocProperlySign'][1]")
	public WebElement otherDocProperlySign ;

	@FindBy(xpath = ".//*[@id='stateAddendumReq'][1]")
	public WebElement stateReqAddendumProperlySign ;

	@FindBy(xpath = ".//*[@id='handWrittenChanges'][1]")
	public WebElement handWrittenChanges ;

	@FindBy(xpath = ".//*[@id='otherAttendaProperlySign'][1]")
	public WebElement otherAttendaProperlySign ;

	@FindBy(xpath = ".//*[@id='proofControlOverRealEstate'][1]")
	public WebElement proofControlOverRealEstate ;

	@FindBy(xpath = ".//*[@id='licAgrProperlySign'][1]")
	public WebElement licAgrProperlySign ;

	@FindBy(xpath = ".//*[@id='promNotePropSign'][1]")
	public WebElement promNotePropSign ;

	@FindBy(xpath = ".//*[@id='ufocRecProperlySign'][1]")
	public WebElement ufocRecProperlySign ;

	@FindBy(name = "Submit")
	public WebElement submitBtn;

	@FindBy(id = "button")
	public WebElement saveBtn;

	public FSLeadSummaryCompliancePage(WebDriver driver) {

		PageFactory.initElements(driver, this);
	}

}
