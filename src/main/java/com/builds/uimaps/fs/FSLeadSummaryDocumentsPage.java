package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FSLeadSummaryDocumentsPage {

	@FindBy(id = "documentTitle")
	public WebElement docTitleText;

	@FindBy(id = "fsDocumentAttachment")
	public WebElement uploadDoc;

	@FindBy(name = "Submit")
	public WebElement saveBtn;

	@FindBy(linkText = "Add Document")
	public WebElement addDocLnk ;

	@FindBy(name = "Add More")
	public WebElement addMoreBtn;

	public FSLeadSummaryDocumentsPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
