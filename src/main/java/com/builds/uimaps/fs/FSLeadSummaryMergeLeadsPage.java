package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FSLeadSummaryMergeLeadsPage {

	@FindBy(id = "Merge22")
	public WebElement mergeBtn;

	public FSLeadSummaryMergeLeadsPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
