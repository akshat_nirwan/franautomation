package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FSLeadSummaryPersonalProfilePage {
	@FindBy(id = "firstName")
	public WebElement firstName;
	@FindBy(id = "lastName")
	public WebElement lastName;
	@FindBy(id = "gender")
	public WebElement gender;
	@FindBy(id = "homeAddress")
	public WebElement homeAddress;
	@FindBy(id = "howLongAtAddress")
	public WebElement howLongAtAddress;
	@FindBy(id = "homeCity")
	public WebElement homeCity;
	@FindBy(id = "homeCountry")
	public WebElement homeCountry;
	@FindBy(id = "homeState")
	public WebElement homeState;
	@FindBy(id = "homeZip")
	public WebElement homeZip;
	@FindBy(id = "birthMonth")
	public WebElement birthMonth;
	@FindBy(id = "birthDate")
	public WebElement birthDate;
	@FindBy(id = "homePhone")
	public WebElement homePhone;
	@FindBy(id = "homePhoneExt")
	public WebElement homePhoneExt;
	// Anukaran
	@FindBy(xpath = ".//*[@id='homeOwnership' and @value='Own']")
	public WebElement homeOwnership ;

	@FindBy(id = "timeToCall")
	public WebElement timeToCall;
	@FindBy(id = "email")
	public WebElement email;
	@FindBy(id = "spouseName")
	public WebElement spouseName;
	@FindBy(id = "heardProformaFrom")
	public WebElement heardProformaFrom;
	@FindBy(id = "seekingOwnBusiness")
	public WebElement seekingOwnBusiness;
	@FindBy(id = "fullTimeBusiness")
	public WebElement fullTimeBusiness;
	@FindBy(id = "otherInvestigation")
	public WebElement otherInvestigation;
	@FindBy(id = "presentEmployer")
	public WebElement presentEmployer;
	@FindBy(id = "title")
	public WebElement title;
	@FindBy(id = "dateStarted")
	public WebElement dateStarted;
	@FindBy(id = "employerAddress")
	public WebElement employerAddress;
	@FindBy(id = "employerCity")
	public WebElement employerCity;
	@FindBy(id = "employerCountry")
	public WebElement employerCountry;

	@FindBy(id = "employerState")
	public WebElement employerState;
	@FindBy(id = "employerZip")
	public WebElement employerZip;
	@FindBy(id = "businessPhone")
	public WebElement businessPhone;
	@FindBy(id = "callAtWork")
	public WebElement callAtWork;
	@FindBy(id = "hourPerWeek")
	public WebElement hourPerWeek;
	@FindBy(id = "salary")
	public WebElement salary;
	@FindBy(id = "responsibility")
	public WebElement responsibility;
	@FindBy(id = "selfEmployed")
	public WebElement selfEmployed;
	@FindBy(id = "limitProforma")
	public WebElement limitProforma;
	@FindBy(id = "similarWork")
	public WebElement similarWork;
	@FindBy(id = "financeProforma")
	public WebElement financeProforma;
	@FindBy(id = "partner")
	public WebElement partner;
	@FindBy(id = "supportHowLong")
	public WebElement supportHowLong;
	@FindBy(id = "income")
	public WebElement income;
	@FindBy(id = "otherSalary")
	public WebElement otherSalary;
	@FindBy(id = "otherIncomeExplaination")
	public WebElement otherIncomeExplaination;
	@FindBy(id = "soleSource")
	public WebElement soleSource;
	@FindBy(id = "howSoon")
	public WebElement howSoon;
	@FindBy(id = "runYourself")
	public WebElement runYourself;
	@FindBy(id = "responsibleForOperation")
	public WebElement responsibleForOperation;
	@FindBy(id = "convictedForFelony")
	public WebElement convictedForFelony;
	@FindBy(id = "liabilites")
	public WebElement liabilites;
	@FindBy(id = "bankruptcy")
	public WebElement bankruptcy;
	@FindBy(id = "lawsuit")
	public WebElement lawsuit;
	@FindBy(id = "convicted")
	public WebElement convicted;
	@FindBy(id = "familyFeelings")
	public WebElement familyFeelings;
	@FindBy(id = "otherFacts")
	public WebElement otherFacts;
	@FindBy(id = "personalPersonalityStyle")
	public WebElement personalPersonalityStyle;
	@FindBy(id = "backgroundOverview")
	public WebElement backgroundOverview;
	@FindBy(id = "goalDream")
	public WebElement goalDream;
	@FindBy(id = "concerns")
	public WebElement concerns;
	@FindBy(id = "timing")
	public WebElement timing;
	@FindBy(id = "hotButtons")
	public WebElement hotButtons;
	@FindBy(id = "otherOptions")
	public WebElement otherOptions;
	@FindBy(xpath = ".//input[@value='Save']")
	public WebElement saveBtn ;
	// Anukaran
	@FindBy(xpath = ".//*[@id='maritalStatus']")
	public WebElement maritalStatus ;
	// Anukaran
	@FindBy(xpath = ".//*[@id='title']")
	public WebElement employmentTitle ;
	// Anukaran
	@FindBy(xpath = ".//*[@id='otherOptions']")
	public WebElement otherComments ;

	@FindBy(xpath = ".//*[@id='percentOwn']")
	public WebElement percentOwn ;

	public FSLeadSummaryPersonalProfilePage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
