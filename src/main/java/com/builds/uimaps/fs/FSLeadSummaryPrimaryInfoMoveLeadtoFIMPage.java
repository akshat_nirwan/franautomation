package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FSLeadSummaryPrimaryInfoMoveLeadtoFIMPage {
	@FindBy(id = "franchiseeName")
	public WebElement franchiseeId;

	@FindBy(id = "centerName")
	public WebElement centerName;

	@FindBy(id = "areaID")
	public WebElement areaRegionDrp;

	@FindBy(id = "storeTypeId")
	public WebElement storeTypeDrp;

	@FindBy(id = "openingDate")
	public WebElement openingDate;

	@FindBy(id = "city")
	public WebElement city;

	@FindBy(id = "countryID")
	public WebElement countryDrp;

	@FindBy(id = "regionNo")
	public WebElement stateDrp;

	@FindBy(id = "storePhone")
	public WebElement storePhone;

	@FindBy(id = "ownerFirstName")
	public WebElement ownerFirstName;

	@FindBy(id = "ownerLastName")
	public WebElement ownerLastName;

	@FindBy(id = "Submit")
	public WebElement submitBtn;

	public FSLeadSummaryPrimaryInfoMoveLeadtoFIMPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
