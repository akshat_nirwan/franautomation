package com.builds.uimaps.fs;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FSLeadSummaryPrimaryInfoPage {

	@FindBy(xpath = ".//*[@id='for_bar']//td[text()='Lead Owner : ']/ancestor::tr[1]/td[2]")
	public WebElement leadOwnerName ;

	@FindBy(linkText = "Detailed History")
	public WebElement detailedHistory ;

	@FindBy(xpath = ".//*[@id='outerTable']//a[text()='Next']")
	public WebElement nextLnk ;

	@FindBy(linkText = "Modify")
	public WebElement modifyLnk ;

	@FindBy(linkText = "Send Email")
	public WebElement sendEmailLnk ;

	@FindBy(linkText = "Log a Task")
	public WebElement logATaskLnk ;

	@FindBy(linkText = "Log a Call")
	public WebElement logACallLnk ;

	@FindBy(linkText = "Add Remarks")
	public WebElement addRemarksLnk ;

	@FindBy(id = "leadStatusID")
	public WebElement leadStatusDrp;

	@FindBy(name = "changeStatusButton")
	public WebElement changeStatusBtn;

	@FindBy(id = "leadKilledReasonTemp")
	public WebElement killedReasonDrp;

	@FindBy(id = "leadKilledCommentsTemp")
	public WebElement leadKilledCommentsTxt;

	@FindBy(id = "leadOpenActivitesSummary")
	public WebElement openTaskIFrame;

	@FindBy(id = "leadLogCallSummary")
	public WebElement activityHistoryIFrame;

	// ***********************************************

	@FindBy(name = "changeOwner")
	public WebElement changeOwnerBtn;

	@FindBy(name = "change_status")
	public WebElement changeOwnerStatusBtn;

	@FindBy(name = "sendUfoc")
	public WebElement sendFddBtn;

	@FindBy(name = "AddToGroup")
	public WebElement addToGroupBtn;

	@FindBy(name = "MailMerge")
	public WebElement mailMergeBtn;

	@FindBy(name = "MoveToFIM")
	public WebElement moveToFIMBtn;

	@FindBy(name = "MoveToFo")
	public WebElement moveToFOBtn;

	@FindBy(name = "addLead")
	public WebElement addLeadBtn;

	@FindBy(name = "printButton")
	public WebElement printBtn;

	// **************Log a Task Lnk *********************************

	@FindBy(xpath = ".//input[@id='radioOwner' and @value='0']")
	public WebElement assignTaskToLeadOwner ;

	@FindBy(id = "ms-parentassignTo")
	public WebElement userBtn;

	@FindBy(css = "input.searchInputMultiple")
	public WebElement searchUsers;

	@FindBy(id = "selectAll")
	public WebElement selectAll;

	@FindBy(id = "subject")
	public WebElement taskSubject;

	@FindBy(name = "add")
	public WebElement addTaskBtn;

	@FindBy(id = "status")
	public WebElement taskStatusDrp;

	@FindBy(name = "print")
	public WebElement processBtn;

	@FindBy(id = "taskType")
	public WebElement taskTypeDrp;

	@FindBy(id = "timelessTaskId")
	public WebElement timeLessTaskCheck;

	@FindBy(id = "priority")
	public WebElement priority;

	@FindBy(id = "startDate")
	public WebElement startDate;

	@FindBy(name = "calendarTaskCheckBox")
	public WebElement addToCalendar;

	@FindBy(id = "comments")
	public WebElement comments;

	@FindBy(id = "schduleTime1")
	public WebElement noReminder;

	@FindBy(id = "schduleTime2")
	public WebElement minutesPrior;

	@FindBy(id = "schduleTime3")
	public WebElement setTimeYourself;

	@FindBy(id = "REMINDER_DATE")
	public WebElement reminderDate;

	@FindBy(xpath = ".//a[.='Complete']/span")
	public WebElement taskCompleteLnk ;

	// **************Log a Call Lnk *********************************

	@FindBy(id = "sTime")
	public WebElement hhTime;

	@FindBy(id = "sMinute")
	public WebElement mmTime;

	@FindBy(id = "APM")
	public WebElement ampmTime;

	@FindBy(id = "subject")
	public WebElement callSubject;

	@FindBy(name = "submitButton")
	public WebElement addCallBtn;

	@FindBy(xpath = ".//input[@value='No']")
	public WebElement callConfirmScheduleTaskNoBtn ;

	// ******************Add Remarks*****************************

	@FindBy(id = "remarks")
	public WebElement remarksTextFields;

	@FindBy(xpath = ".//input[@value='Submit']")
	public WebElement submitRemarks ;

	@FindBy(xpath = ".//input[@value='Close']")
	public WebElement closeRemarksBtn ;

	// *********************More Action*****************************

	@FindBy(xpath = ".//input[@type='button' and @value='More-Actions']")
	public WebElement moreActionsLink ;

	@FindBy(xpath = ".//div[@id='actionListButtons1']/table/tbody/tr[2]/td[2]/table/tbody//td")
	public List<WebElement> menu;

	// -------------------------------

	@FindBy(id = "leadOwnerID")
	public WebElement changeLeadOwnerDrp ;

	@FindBy(name = "change_status")
	public WebElement changeOwnerCboxStatusBtn;

	@FindBy(xpath = ".//input[@value='Close']")
	public WebElement closeChangeOwnerBtn ;

	// -------------------------------

	@FindBy(id = "leadKilledReasonTemp")
	public WebElement leadKilledReasonDrp;

	@FindBy(id = "leadKilledCommentsTemp")
	public WebElement leadKilledCommentsTxtArea;

	@FindBy(id = "Submit")
	public WebElement submitBtn;

	// ---------------------------

	@FindBy(name = "Ok")
	public WebElement okDeleteBtn;

	// -------------------------------

	@FindBy(xpath = ".//*[@id='table1']/tbody/tr[2]/td/table/tbody/tr[1]/td/table/tbody/tr/td[2]/a/span")
	public WebElement addToNewGroupLnk ;

	@FindBy(xpath = ".//*[@id='table1']/tbody/tr[2]/td/table/tbody/tr[2]/td/table[2]/tbody/tr[2]/td/table/tbody/tr[2]/td[1]/input")
	public WebElement firstGroupCboxChk ;

	@FindBy(xpath = ".//input[@value='Add To Group']")
	public WebElement addToGroupCboxBtn ;

	@FindBy(xpath = ".//input[@value='Ok']")
	public WebElement okCboxBtn ;

	// ------------------------------

	@FindBy(id = "searchCampaign")
	public WebElement searchCampaignTxt;

	@FindBy(id = "search")
	public WebElement searchCampaignBtn;

	@FindBy(xpath = ".//*[@id='resize']/tbody/tr[2]/td[2]/table[3]/tbody/tr/td/form/table/tbody/tr[2]/td[1]/label/input")
	public WebElement firstCampaign ;

	@FindBy(xpath = ".//input[@value='Associate']")
	public WebElement associateCampaignBtn ;

	@FindBy(xpath = ".//input[@value='Close']")
	public WebElement closeCampaignCboxBtn ;

	// Coapplicant

	@FindBy(id = "coApplicantRelationshipID")
	public WebElement coApplicantRelationshipDrp;

	@FindBy(id = "coApplicantSearch")
	public WebElement coApplicantSearchTxt;

	@FindBy(xpath = ".//input[@value='Add']")
	public WebElement addBtnCbox ;

	@FindBy(xpath = ".//input[@value='Close']")
	public WebElement closeBtnCbox ;

	@FindBy(xpath = ".//*[@id='contentdivTopSearch']/table/tbody/tr[1]/td/a/span/u")
	public WebElement leadNameLink ;

	@FindBy(linkText = "Print History")
	public WebElement contactHistorySection ;

	@FindBy(linkText = "Qualification Details")
	public WebElement qualificationDetailsTab ;

	@FindBy(linkText = "Personal Profile")
	public WebElement personalProfileTab ;

	@FindBy(linkText = "Candidate Portal")
	public WebElement candidatePortalTab ;

	@FindBy(linkText = "Co-Applicants")
	public WebElement coApplicantTab ;

	@FindBy(linkText = "Compliance")
	public WebElement complianceTab ;

	@FindBy(linkText = "Documents")
	public WebElement documentTab ;

	@FindBy(linkText = "Real Estate")
	public WebElement realEstateTab ;

	@FindBy(linkText = "Virtual Brochure")
	public WebElement virtualBrochureTab ;

	@FindBy(linkText = "Visit")
	public WebElement visitTab ;

	@FindBy(linkText = "Primary Info")
	public WebElement primaryInfoTab ;

	@FindBy(linkText = "Activity Timeline")
	public WebElement activityTimeLineSection ;

	public FSLeadSummaryPrimaryInfoPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
