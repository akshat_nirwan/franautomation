package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FSLeadSummaryPrimaryInfoSendFDDPage {

	@FindBy(xpath = ".//input[@value='Send']")
	public WebElement sendBtn ;

	@FindBy(id = "mailTemplateID")
	public WebElement fddEmailTemplatesDrp;

	public FSLeadSummaryPrimaryInfoSendFDDPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
