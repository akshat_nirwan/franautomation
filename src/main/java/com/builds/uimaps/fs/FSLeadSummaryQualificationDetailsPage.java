package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FSLeadSummaryQualificationDetailsPage {
	@FindBy(id = "date")
	public WebElement date;
	@FindBy(id = "firstName")
	public WebElement firstName;
	@FindBy(xpath = ".//input[@id='gender' and @value='Male']")
	public WebElement gender_male ;

	@FindBy(xpath = ".//input[@id='gender' and @value='Female']")
	public WebElement gender_female ;

	@FindBy(id = "presentAddress")
	public WebElement presentAddress;
	@FindBy(id = "howLong")
	public WebElement howLong;
	@FindBy(id = "city")
	public WebElement city;
	@FindBy(id = "country")
	public WebElement country;

	@FindBy(id = "stateID")
	public WebElement stateID;
	@FindBy(id = "zipCode")
	public WebElement zipCode;
	@FindBy(id = "workPhone")
	public WebElement workPhone;
	@FindBy(id = "phoneExt")
	public WebElement phoneExt;
	@FindBy(id = "homePhone")
	public WebElement homePhone;
	@FindBy(id = "homePhoneExt")
	public WebElement homePhoneExt;
	@FindBy(id = "emailID")
	public WebElement emailID;

	@FindBy(xpath = ".//input[@id='usCitizen' and @value='Yes']")
	public WebElement usCitizen_yes ;
	@FindBy(xpath = ".//input[@id='usCitizen' and @value='No']")
	public WebElement usCitizen_No ;

	@FindBy(id = "ssn")
	public WebElement ssn;
	@FindBy(id = "previousAddress")
	public WebElement previousAddress;
	@FindBy(id = "previousCity")
	public WebElement previousCity;
	@FindBy(id = "previousCountry")
	public WebElement previousCountry;

	@FindBy(id = "previousStateID")
	public WebElement previousStateID;
	@FindBy(id = "previousZipCode")
	public WebElement previousZipCode;
	@FindBy(id = "birthMonth")
	public WebElement birthMonth;
	@FindBy(id = "birthDate")
	public WebElement birthDate;
	@FindBy(id = "timeToCall")
	public WebElement bestTimeToCall;

	@FindBy(xpath = ".//input[@id='homeOwnership' and @value='Own']")
	public WebElement homeOwnership_own ;
	@FindBy(xpath = ".//input[@id='homeOwnership' and @value='Renting']")
	public WebElement homeOwnership_renting ;

	@FindBy(id = "spouseName")
	public WebElement spouseName;

	@FindBy(xpath = ".//input[@id='maritalStatus' and @value='Single']")
	public WebElement maritalStatus_single ;
	@FindBy(xpath = ".//input[@id='maritalStatus' and @value='Married']")
	public WebElement maritalStatus_married ;

	@FindBy(id = "spouseSsn")
	public WebElement spouseSsn;

	@FindBy(xpath = ".//input[@id='spouseUsCitizen' and @value='Yes']")
	public WebElement spouseUsCitizen_yes ;
	@FindBy(xpath = ".//input[@id='spouseUsCitizen' and @value='No']")
	public WebElement spouseUsCitizen_no ;

	@FindBy(name = "spouseBirthMonth")
	public WebElement spouseBirthMonth;
	@FindBy(name = "spouseBirthDate")
	public WebElement spouseBirthDate;
	@FindBy(name = "cashOnHand")
	public WebElement cashOnHand;
	@FindBy(name = "mortgages")
	public WebElement mortgages;
	@FindBy(name = "marketableSecurities")
	public WebElement marketableSecurities;
	@FindBy(name = "accountsPayable")
	public WebElement accountsPayable;
	@FindBy(name = "accountsReceivable")
	public WebElement accountsReceivable;
	@FindBy(name = "notesPayable")
	public WebElement notesPayable;
	@FindBy(name = "retirementPlans")
	public WebElement retirementPlans;
	@FindBy(name = "loansOnLifeInsurance")
	public WebElement loansOnLifeInsurance;
	@FindBy(name = "realEstate")
	public WebElement realEstate;
	@FindBy(name = "creditCardBalance")
	public WebElement creditCardBalance;
	@FindBy(name = "personalProperty")
	public WebElement personalProperty;
	@FindBy(name = "unpaidTaxes")
	public WebElement unpaidTaxes;
	@FindBy(name = "businessHoldings")
	public WebElement businessHoldings;
	@FindBy(name = "lifeInsurance")
	public WebElement lifeInsurance;
	@FindBy(name = "otherAssets")
	public WebElement otherAssets;
	@FindBy(name = "otherLiabilities")
	public WebElement otherLiabilities;
	@FindBy(id = "liablitiesDescription")
	public WebElement liablitiesDescription;
	@FindBy(id = "assestsDescription")
	public WebElement assestsDescription;
	@FindBy(name = "totalAssets")
	public WebElement totalAssets;
	@FindBy(name = "totalLiabilities")
	public WebElement totalLiabilities;
	@FindBy(name = "totalNetworth")
	public WebElement totalNetworth;
	@FindBy(id = "reAddress1")
	public WebElement reAddress1;
	@FindBy(id = "reDatePurchased1")
	public WebElement reDatePurchased1;
	@FindBy(id = "reOrigCost1")
	public WebElement reOrigCost1;
	@FindBy(id = "rePresentValue1")
	public WebElement rePresentValue1;
	@FindBy(id = "reMortgageBalance1")
	public WebElement reMortgageBalance1;
	@FindBy(id = "reAddress2")
	public WebElement reAddress2;
	@FindBy(id = "reDatePurchased2")
	public WebElement reDatePurchased2;
	@FindBy(id = "reOrigCost2")
	public WebElement reOrigCost2;
	@FindBy(id = "rePresentValue2")
	public WebElement rePresentValue2;
	@FindBy(id = "reMortgageBalance2")
	public WebElement reMortgageBalance2;
	@FindBy(id = "reAddress3")
	public WebElement reAddress3;
	@FindBy(id = "reDatePurchased3")
	public WebElement reDatePurchased3;
	@FindBy(id = "reOrigCost3")
	public WebElement reOrigCost3;
	@FindBy(id = "rePresentValue3")
	public WebElement rePresentValue3;
	@FindBy(id = "reMortgageBalance3")
	public WebElement reMortgageBalance3;
	@FindBy(name = "annualSalary")
	public WebElement annualSalary;
	@FindBy(name = "annualInvestment")
	public WebElement annualInvestment;
	@FindBy(name = "annualReIncome")
	public WebElement annualReIncome;
	@FindBy(name = "otherAnnualSource")
	public WebElement otherAnnualSource;
	@FindBy(id = "otherAnnualSourceDescription")
	public WebElement otherAnnualSourceDescription;
	@FindBy(name = "annualSourceTotal")
	public WebElement annualSourceTotal;
	@FindBy(name = "loanCoSign")
	public WebElement loanCoSign;
	@FindBy(name = "legalJudgement")
	public WebElement legalJudgement;
	@FindBy(name = "incomeTaxes")
	public WebElement incomeTaxes;
	@FindBy(name = "otherSpecialDebt")
	public WebElement otherSpecialDebt;
	@FindBy(name = "totalContingent")
	public WebElement totalContingent;
	@FindBy(id = "whenReadyIfApproved")
	public WebElement whenReadyIfApproved;
	@FindBy(id = "skillsExperience")
	public WebElement skillsExperience;
	@FindBy(id = "enableReachGoals")
	public WebElement enableReachGoals;
	@FindBy(id = "responsibleForDailyOperations")
	public WebElement responsibleForDailyOperations;

	@FindBy(id = "cashAvailable")
	public WebElement cashAvailable;

	@FindBy(xpath = ".//input[@id='approvedForFinancing' and @value='Yes']")
	public WebElement approvedForFinancing_yes ;
	@FindBy(xpath = ".//input[@id='approvedForFinancing' and @value='No']")
	public WebElement approvedForFinancing_no ;

	@FindBy(id = "amountApprovedForFinance")
	public WebElement amountApproved;

	@FindBy(xpath = ".//input[@id='soleIncomeSource' and @value='Yes']")
	public WebElement soleIncomeSource_yes ;
	@FindBy(xpath = ".//input[@id='soleIncomeSource' and @value='No']")
	public WebElement soleIncomeSource_no ;

	@FindBy(xpath = ".//input[@id='liabilites' and @value='Yes']")
	public WebElement liablities_yes ;
	@FindBy(xpath = ".//input[@id='liabilites' and @value='No']")
	public WebElement liablities_no ;

	@FindBy(xpath = ".//input[@id='lawsuit' and @value='Yes']")
	public WebElement lawsuit_yes ;
	@FindBy(xpath = ".//input[@id='lawsuit' and @value='No']")
	public WebElement lawsuit_no ;

	@FindBy(xpath = ".//input[@id='convicted' and @value='Yes']")
	public WebElement convicted_yes ;
	@FindBy(xpath = ".//input[@id='convicted' and @value='No']")
	public WebElement convicted_no ;

	@FindBy(xpath = ".//input[@id='convictedOfFelony' and @value='Yes']")
	public WebElement convictedOfFelony_yes ;
	@FindBy(xpath = ".//input[@id='convictedOfFelony' and @value='No']")
	public WebElement convictedOfFelony_no ;

	@FindBy(id = "explainConviction")
	public WebElement explainConviction;

	@FindBy(xpath = ".//input[@id='filedBankruptcy' and @value='Yes']")
	public WebElement filedBankruptcy_yes ;
	@FindBy(xpath = ".//input[@id='filedBankruptcy' and @value='No']")
	public WebElement filedBankruptcy_no ;

	@FindBy(id = "dateFiled")
	public WebElement dateFiled;
	@FindBy(id = "dateDischarged")
	public WebElement dateDischarged;
	@FindBy(id = "locationPreference1")
	public WebElement locationPreference1;
	@FindBy(id = "locationPreference2")
	public WebElement locationPreference2;
	@FindBy(id = "locationPreference3")
	public WebElement locationPreference3;
	@FindBy(id = "businessQuestion1")
	public WebElement businessQuestion1;
	@FindBy(id = "businessQuestion2")
	public WebElement businessQuestion2;
	@FindBy(id = "businessQuestion3")
	public WebElement businessQuestion3;

	@FindBy(id = "finFirstName")
	public WebElement appFirstName;
	@FindBy(id = "lastName")
	public WebElement appLastName;
	@FindBy(id = "address")
	public WebElement appAddress;
	@FindBy(id = "finCity")
	public WebElement appCity;
	@FindBy(id = "phone")
	public WebElement appPhone;
	@FindBy(id = "finCountry")
	public WebElement appCountry;
	@FindBy(id = "finState")
	public WebElement appState;
	@FindBy(id = "phoneExt2")
	public WebElement appPhoneExt2;

	@FindBy(xpath = ".//input[@id='calledOffice' and @value='Yes']")
	public WebElement appCanApplicantbeCalled_yes ;
	@FindBy(xpath = ".//input[@id='calledOffice' and @value='No']")
	public WebElement appCanApplicantbeCalled_no ;

	@FindBy(id = "liquidCapitalMin")
	public WebElement currentNetWorth;
	@FindBy(id = "liquidCapitalMax")
	public WebElement cashAvailableforInvestment;
	@FindBy(id = "investTimeframe")
	public WebElement investTimeframe;
	@FindBy(id = "background")
	public WebElement employmentbackground;

	@FindBy(xpath = ".//input[@id='backgroundCheck' and @value='Yes']")
	public WebElement backgroundCheckApproval_yes ;

	@FindBy(xpath = ".//input[@id='backgroundCheck' and @value='No']")
	public WebElement backgroundCheckApproval_no ;

	@FindBy(name = "qfnChkAckValue_0")
	public WebElement LendableNetWorth_Value;

	@FindBy(xpath = ".//input[@name='qfnChkAckCompleted_0' and @value='Y']")
	public WebElement LendableNetWorth_Completed_yes ;

	@FindBy(xpath = ".//input[@name='qfnChkAckCompleted_0' and @value='N']")
	public WebElement LendableNetWorth_Completed_No ;

	@FindBy(id = "qfnChkAckCompletionDate_0")
	public WebElement LendableNetWorth_CompletionDate;

	@FindBy(id = "qfnChkAssociateDoc_0")
	public WebElement LendableNetWorth_associatedDocument;

	@FindBy(name = "qfnChkAckVerifiedBy_0")
	public WebElement LendableNetWorth_VerifiedBy;

	@FindBy(id = "qfnChkAckDate_0")
	public WebElement LendableNetWorth_Date;

	@FindBy(name = "qfnChkAckValue_1")
	public WebElement BackgroundandCriminalCheck_Value;

	@FindBy(xpath = ".//input[@name='qfnChkAckCompleted_1' and @value='Y']")
	public WebElement BackgroundandCriminalCheck_Completed_yes ;

	@FindBy(xpath = ".//input[@name='qfnChkAckCompleted_1' and @value='N']")
	public WebElement BackgroundandCriminalCheck_Completed_No ;

	@FindBy(id = "qfnChkAckCompletionDate_1")
	public WebElement BackgroundandCriminalCheck_CompletionDate;

	@FindBy(id = "qfnChkAssociateDoc_1")
	public WebElement BackgroundandCriminalCheck_associatedDocument;

	@FindBy(name = "qfnChkAckVerifiedBy_1")
	public WebElement BackgroundandCriminalCheck_VerifiedBy;

	@FindBy(id = "qfnChkAckDate_1")
	public WebElement BackgroundandCriminalCheck_Date;

	@FindBy(name = "qfnChkAckValue_2")
	public WebElement CreditCheck_Value;

	@FindBy(xpath = ".//input[@name='qfnChkAckCompleted_2' and @value='Y']")
	public WebElement CreditCheck_Completed_yes ;

	@FindBy(xpath = ".//input[@name='qfnChkAckCompleted_2' and @value='N']")
	public WebElement CreditCheck_Completed_No ;

	@FindBy(id = "qfnChkAckCompletionDate_2")
	public WebElement CreditCheck_CompletionDate;

	@FindBy(id = "qfnChkAssociateDoc_2")
	public WebElement CreditCheck_associatedDocument;

	@FindBy(name = "qfnChkAckVerifiedBy_2")
	public WebElement CreditCheck_VerifiedBy;

	@FindBy(id = "qfnChkAckDate_2")
	public WebElement CreditCheck_Date;

	@FindBy(name = "qfnChkAckValue_3")
	public WebElement TerritoryApproved_Value;

	@FindBy(xpath = ".//input[@name='qfnChkAckCompleted_3' and @value='Y']")
	public WebElement TerritoryApproved_Completed_yes ;

	@FindBy(xpath = ".//input[@name='qfnChkAckCompleted_3' and @value='N']")
	public WebElement TerritoryApproved_Completed_No ;

	@FindBy(id = "qfnChkAckCompletionDate_3")
	public WebElement TerritoryApproved_CompletionDate;

	@FindBy(id = "qfnChkAssociateDoc_3")
	public WebElement TerritoryApproved_associatedDocument;

	@FindBy(name = "qfnChkAckVerifiedBy_3")
	public WebElement TerritoryApproved_VerifiedBy;

	@FindBy(id = "qfnChkAckDate_3")
	public WebElement TerritoryApproved_Date;

	@FindBy(name = "qfnChkAckValue_4")
	public WebElement FranchiseAgreementonFile_Value;

	@FindBy(xpath = ".//input[@name='qfnChkAckCompleted_4' and @value='Y']")
	public WebElement FranchiseAgreementonFile_Completed_yes ;

	@FindBy(xpath = ".//input[@name='qfnChkAckCompleted_4' and @value='N']")
	public WebElement FranchiseAgreementonFile_Completed_No ;

	@FindBy(id = "qfnChkAckCompletionDate_4")
	public WebElement FranchiseAgreementonFile_CompletionDate;

	@FindBy(id = "qfnChkAssociateDoc_4")
	public WebElement FranchiseAgreementonFile_associatedDocument;

	@FindBy(name = "qfnChkAckVerifiedBy_4")
	public WebElement FranchiseAgreementonFile_VerifiedBy;

	@FindBy(id = "qfnChkAckDate_4")
	public WebElement FranchiseAgreementonFile_Date;

	@FindBy(name = "qfnChkAckValue_5")
	public WebElement FDDReceiptonFile_Value;

	@FindBy(xpath = ".//input[@name='qfnChkAckCompleted_5' and @value='Y']")
	public WebElement FDDReceiptonFile_Completed_yes ;

	@FindBy(xpath = ".//input[@name='qfnChkAckCompleted_5' and @value='N']")
	public WebElement FDDReceiptonFile_Completed_No ;

	@FindBy(id = "qfnChkAckCompletionDate_5")
	public WebElement FDDReceiptonFile_CompletionDate;

	@FindBy(id = "qfnChkAssociateDoc_5")
	public WebElement FDDReceiptonFile_associatedDocument;

	@FindBy(name = "qfnChkAckVerifiedBy_5")
	public WebElement FDDReceiptonFile_VerifiedBy;

	@FindBy(id = "qfnChkAckDate_5")
	public WebElement FDDReceiptonFile_Date;

	@FindBy(xpath = ".//input[@name='button' and @value='Save']")
	public WebElement saveBtn ;

	@FindBy(linkText = "Delete")
	public WebElement delete ;

	@FindBy(linkText = "Modify")
	public WebElement modify ;

	@FindBy(xpath = ".//*[@name='button' and @value='Save']")
	public WebElement saveButton ;

	@FindBy(xpath = ".//*[@id='city']")
	public WebElement homeCity ;

	@FindBy(xpath = ".//*[@id='previousCountry']")
	public WebElement homeCountry ;

	@FindBy(xpath = ".//*[@id='liquidCapitalMin']")
	public WebElement currentNetLiquidWorth ;

	@FindBy(xpath = ".//*[@id='liquidCapitalMax']")
	public WebElement casshAvailableForInvestment ;

	@FindBy(xpath = ".//*[@id='investTimeframe']")
	public WebElement investmentTimeframe ;

	@FindBy(xpath = ".//*[@id='previousStateID']")
	public WebElement homeState ;

	@FindBy(xpath = ".//*[@id='emailID']")
	public WebElement Email ;

	@FindBy(xpath = ".//*[@id='siteAddress1']")
	public WebElement siteAddress ;

	@FindBy(xpath = ".//*[@id='siteCity']")
	public WebElement siteCity ;

	public FSLeadSummaryQualificationDetailsPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
