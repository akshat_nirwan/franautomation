package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FSLeadSummarySendEmailPage {

	@FindBy(id = "mailTemplateID")
	public WebElement existingEmailTemplatesDrp;

	@FindBy(name = "subject")
	public WebElement subject;

	@FindBy(xpath = ".//iframe[@id='ta_ifr']")
	public WebElement htmlFrame ;

	@FindBy(xpath = "html/body")
	public WebElement mailText ;

	@FindBy(xpath = ".//input[@value='Send']")
	public WebElement sendBtn ;

	public FSLeadSummarySendEmailPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
