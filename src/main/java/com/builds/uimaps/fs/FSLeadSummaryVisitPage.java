package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FSLeadSummaryVisitPage {

	@FindBy(id = "duedate")
	public WebElement visitSchedule;
	@FindBy(id = "visitdate")
	public WebElement visitdate;
	@FindBy(id = "type")
	public WebElement type;
	@FindBy(id = "calender_checkbox")
	public WebElement addToCalendar;
	@FindBy(id = "radioOwner")
	public WebElement assignToLeadOwner;
	@FindBy(name = "radioUser")
	public WebElement assignToOtherUser;
	@FindBy(xpath = ".//*[@id='ms-parentassignTo']")
	public WebElement assignToOtherUserName ;
	@FindBy(xpath = ".//*[@id='type' and @value='Individual']")
	public WebElement typeIndividual ;
	@FindBy(xpath = ".//*[@id='type' and @value='Group']")
	public WebElement typeGroup;

	@FindBy(css = "button.ms-choice")
	public WebElement userListBtn;
	@FindBy(css = "input.searchInputMultiple")
	public WebElement searchUser;
	@FindBy(id = "selectAll")
	public WebElement selectAll;

	@FindBy(id = "radioLoggedUser")
	public WebElement assignToLoggedInUser;
	@FindBy(id = "visitor1Name")
	public WebElement visitor1Name;
	@FindBy(id = "relationship1")
	public WebElement relationship1;
	@FindBy(id = "visitor2Name")
	public WebElement visitor2Name;
	@FindBy(id = "relationship2")
	public WebElement relationship2;
	@FindBy(id = "visitor3Name")
	public WebElement visitor3Name;
	@FindBy(id = "relationship3")
	public WebElement relationship3;
	@FindBy(id = "agreedReimbursement")
	public WebElement agreedReimbursement;
	@FindBy(id = "actualReimbursement")
	public WebElement actualReimbursement;
	@FindBy(id = "checkSent")
	public WebElement paymentSentDate;
	@FindBy(id = "visitConfirmed")
	public WebElement visitConfirmed;
	@FindBy(id = "comments")
	public WebElement comments;

	@FindBy(xpath = ".//input[@value='Save']")
	public WebElement saveBtn ;

	@FindBy(linkText = "Modify")
	public WebElement modifyVisit ;

	@FindBy(linkText = "Delete")
	public WebElement deleteVisit ;

	@FindBy(linkText = "Modify")
	public WebElement sendEmail ;

	public FSLeadSummaryVisitPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
