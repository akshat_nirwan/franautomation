package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FSLeadTabPage {

	@FindBy(xpath = ".//*[@id='outerTable']//td/a/span[.='Primary Info']")
	public WebElement primaryInfo ;

	@FindBy(xpath = ".//*[@id='outerTable']//td/a/span[.='Candiate Portal']")
	public WebElement candiatePortal ;

	@FindBy(xpath = ".//*[@id='outerTable']//td/a/span[.='Co-Applicant']")
	public WebElement coapplicant ;

	@FindBy(xpath = ".//*[@id='outerTable']//td/a/span[.='Compliance']")
	public WebElement compliance ;

	@FindBy(xpath = ".//*[@id='outerTable']//td/a/span[.='Documents']")
	public WebElement documents ;

	@FindBy(xpath = ".//*[@id='outerTable']//td/a/span[.='Personal Profile']")
	public WebElement personalProfile ;

	@FindBy(xpath = ".//*[@id='outerTable']//td/a/span[.='Qualification Details']")
	public WebElement qualficationDetails ;

	@FindBy(xpath = ".//*[@id='outerTable']//td/a/span[.='Real Estate']")
	public WebElement realEstate ;

	@FindBy(xpath = ".//*[@id='outerTable']//td/a/span[.='Virtual Brochure']")
	public WebElement virtualBrochure ;

	@FindBy(xpath = ".//*[@id='outerTable']//td/a/span[.='Visit']")
	public WebElement visit ;

	public FSLeadTabPage(WebDriver driver) {

		PageFactory.initElements(driver, this);
	}
}
