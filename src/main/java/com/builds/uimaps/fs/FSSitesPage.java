package com.builds.uimaps.fs;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FSSitesPage {

	@FindBy(xpath = ".//a[@original-title='Manage and Track Physical Locations of Stores']")
	public WebElement fsSitesLnk ;

	@FindBy(xpath = ".//input[@value='Add Site']")
	public WebElement addSitesBtn ;

	@FindBy(id = "locationTitle")
	public WebElement locationTitle;

	@FindBy(id = "siteAddress1")
	public WebElement siteAddress1;

	@FindBy(id = "siteAddress2")
	public WebElement siteAddress2;

	@FindBy(id = "city")
	public WebElement city;

	@FindBy(id = "countryID")
	public WebElement countryID;

	@FindBy(id = "stateID")
	public WebElement stateID;

	@FindBy(id = "buildingSize")
	public WebElement buildingSize;

	@FindBy(id = "buildingDimentionsX")
	public WebElement buildingDimentionsX;

	@FindBy(id = "buildingDimentionsY")
	public WebElement buildingDimentionsY;

	@FindBy(id = "buildingDimentionsZ")
	public WebElement buildingDimentionsZ;

	@FindBy(id = "parkingSpaces")
	public WebElement parkingSpaces;

	@FindBy(id = "comments")
	public WebElement comments;

	@FindBy(xpath = ".//input[@value='Save']")
	public WebElement submitButton ;

	@FindBy(id = "siteRemark")
	public WebElement siteRemarkadd;

	@FindBy(id = "viewByLocationType")
	public WebElement locationTypeSearch;

	@FindBy(id = "search")
	public WebElement searchBtn;

	@FindBy(xpath = ".//input[@value='OK']")
	public WebElement okBtn ;

	@FindBy(xpath = ".//*[@id='siteMainTable']/tbody/tr/td/table/tbody/tr[5]/td[2]/table/tbody/tr[2]/td/table/tbody/./td[1]/a")
	public List<WebElement> siteListing;

	@FindBy(xpath = ".//input[@value='Add Remarks']")
	public WebElement addRemarkBtn ;

	@FindBy(xpath = ".//input[@name='change']")
	public WebElement addRemark ;

	@FindBy(xpath = ".//input[@name='close']")
	public WebElement closeBtn ;

	@FindBy(xpath = ".//*[@id='siteRemark']")
	public WebElement siteRemark ;

	@FindBy(xpath = ".//input[@name='change']")
	public WebElement modifyBtn ;

	@FindBy(xpath = ".//*[@id='siteSetting' and @value='yes']")
	public WebElement enableSiteClearanceYes;

	public FSSitesPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
