package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FSTasksPage {

	@FindBy(linkText = "Add Task")
	public WebElement addTaskLnk ;

	@FindBy(id = "leadSearchName")
	public WebElement leadSearchName;

	@FindBy(xpath = ".//input[@value='Search']")
	public WebElement searchBtn ;

	@FindBy(xpath = ".//input[@value='Create Task']")
	public WebElement createTaskBtn ;

	@FindBy(name = "checkBox")
	public WebElement checkBoxCboxiframeAll;

	@FindBy(name = "allSelect")
	public WebElement checkBoxAll1;

	@FindBy(xpath = ".//input[@name='checkb']")
	public WebElement checkBox ;

	@FindBy(name = "statusChange")
	public WebElement statusChangeCbox;

	/*
	 * Log a task
	 * 
	 */

	@FindBy(xpath = ".//input[@id='radioOwner' and @value='0']")
	public WebElement assignTaskToLeadOwner ;

	@FindBy(id = "subject")
	public WebElement taskSubject;

	@FindBy(name = "schduleTime")
	public WebElement noReminderTask;

	@FindBy(name = "add")
	public WebElement addTaskBtn;
	// ------------------------------\

	@FindBy(linkText = "Show Filters")
	public WebElement showFilterLnk ;

	@FindBy(linkText = "Hide Filters")
	public WebElement hideFilterLnk ;

	@FindBy(id = "ms-parentstatus")
	public WebElement taskStatusDrp;

	@FindBy(id = "status")
	public WebElement statusDropDown;

	@FindBy(id = "ms-parentmineall")
	public WebElement viewDrp;

	@FindBy(xpath = ".//input[@value='Process']")
	public WebElement processBtn ;

	@FindBy(xpath = ".//input[@value='Change Status']")
	public WebElement changeStatusBottomBtn ;

	@FindBy(xpath = ".//input[@value='Delete']")
	public WebElement deleteBottomBtn ;

	public FSTasksPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
