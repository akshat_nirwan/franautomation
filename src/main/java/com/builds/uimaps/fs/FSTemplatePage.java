package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FSTemplatePage {

	@FindBy(xpath = ".//*[@id='codeYourOwn']/a")
	public WebElement codeYourOwnLink ;

	@FindBy(name = "mailTitle")
	public WebElement templateName;

	@FindBy(name = "mailSubject")
	public WebElement emailSubject;

	@FindBy(xpath = ".//*[@id='CodeYourOwn']//label[contains(text(),'Public')]")
	public WebElement publicTemplate ;

	@FindBy(xpath = ".//*[@id='CodeYourOwn']//label[contains(text(),'Private')]")
	public WebElement privateTemplate ;

	@FindBy(xpath = ".//*[@id='CodeYourOwn']//label[contains(text(),'Graphical')]")
	public WebElement graphicalText ;

	@FindBy(xpath = ".//*[@id='CodeYourOwn']//label[contains(text(),'Plain Text')]")
	public WebElement plainText ;

	@FindBy(xpath = ".//*[@id='CodeYourOwn']//label[contains(text(),'Upload HTML / ZIP file')]")
	public WebElement uploadHTML ;

	@FindBy(name = ".//*[@id='tinymce']/p")
	public WebElement graphicalContent;

	@FindBy(xpath = ".//*[@id='ta']")
	public WebElement plainTextEditor ;

	@FindBy(xpath = ".//button[contains(text(),'Save')]")
	public WebElement saveBtn ;

	public FSTemplatePage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
