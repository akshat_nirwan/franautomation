package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FSUnregisteredStatesProvincesPage {

	@FindBy(xpath = ".//*[@id='ms-parentcountryCombo']")
	public WebElement campaignDownDwn ;

	@FindBy(xpath = ".//input[@name='stateId' and @value='1']")
	public WebElement firstStateChkBox ;

	@FindBy(xpath = ".//input[@name='stateId' and @value='2']")
	public WebElement secondStateChkBox ;

	@FindBy(xpath = ".//*[@id='leadCampaign']")
	public WebElement campaignDrpDwn ;

	@FindBy(xpath = ".//*[@id='leadStatus']")
	public WebElement statusDrpDwn ;

	@FindBy(xpath = "	.//input[@name='update']")
	public WebElement updateBtn ;

	public FSUnregisteredStatesProvincesPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
