package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FieldstobeExportedUI {

	@FindBy(xpath = ".//input[@value='requestDate']")
	public WebElement requestDate;

	@FindBy(xpath = ".//input[@value='salutation']")
	public WebElement salutation;

	@FindBy(xpath = ".//input[@value='firstName']")
	public WebElement firstName;

	@FindBy(xpath = ".//input[@value='lastName']")
	public WebElement lastName;

	@FindBy(xpath = ".//input[@value='address']")
	public WebElement address;

	@FindBy(xpath = ".//input[@value='address2']")
	public WebElement address2;

	@FindBy(xpath = ".//input[@value='city']")
	public WebElement city;

	@FindBy(xpath = ".//input[@value='country']")
	public WebElement country;

	@FindBy(xpath = ".//input[@value='stateID']")
	public WebElement stateID;

	@FindBy(xpath = ".//input[@value='emailID']")
	public WebElement emailID;

	public FieldstobeExportedUI(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
