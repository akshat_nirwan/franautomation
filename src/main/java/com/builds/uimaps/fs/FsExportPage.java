package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FsExportPage {

	@FindBy(xpath = ".//*[@name='TASKS20_subject']")
	public WebElement ExportTaskSubject ;

	@FindBy(xpath = ".//*[@value='fsleadCall']")
	public WebElement chkbxActHistoryCall ;

	@FindBy(xpath = ".//*[@value='fsleadRemarks']")
	public WebElement chkbxActHistoryRemarks ;

	@FindBy(xpath = ".//*[@value='fsTasks']")
	public WebElement chkbxActHistoryTasks ;

	@FindBy(xpath = ".//*[@value='Search Data']")
	public WebElement srchDataBtn ;

	@FindBy(xpath = ".//*[@name='FS_LEAD_DETAILS1_firstName']")
	public WebElement fnametxt ;

	@FindBy(xpath = ".//*[@name='FS_LEAD_DETAILS1_lastName']")
	public WebElement lnametxt ;

	@FindBy(name = "FS_LEAD_CALL20_dateTo")
	public WebElement dateTo;

	@FindBy(name = "FS_LEAD_CALL20_dateFrom")
	public WebElement datefrom;

	@FindBy(xpath = ".//*[@name='TASKS19_dateFrom']")
	public WebElement taskdatefrom ;

	@FindBy(name = ".//*[@name='TASKS19_dateTo']")
	public WebElement taskdateto;

	@FindBy(xpath = ".//*[@value='View Data']")
	public WebElement viewdata ;

	@FindBy(xpath = ".//*[@id='ms-parentFS_LEAD_CALL20_loggedByID']")
	public WebElement loggedBy ;

	@FindBy(xpath = ".//*[@name='FS_LEAD_CALL20_subject']")
	public WebElement callSubject ;

	@FindBy(xpath = ".//*[@name='FS_LEAD_CALL20_dateFrom']")
	public WebElement callDateFrom ;

	@FindBy(xpath = ".//*[@name='FS_LEAD_CALL20_dateTo']")
	public WebElement callDateTo ;

	@FindBy(xpath = ".//*[@id='ms-parentFS_LEAD_CALL20_callStatus']")
	public WebElement callStatus ;

	@FindBy(xpath = ".//*[@id='ms-parentFS_LEAD_CALL20_callType']")
	public WebElement callcommunicationType ;

	@FindBy(xpath = ".//*[@name='FS_LEAD_CALL20_comments']")
	public WebElement callComments ;

	@FindBy(xpath = ".//*[@value='Select Fields']")
	public WebElement selectFields ;

	@FindBy(xpath = ".//*[@value='firstName']")
	public WebElement selectFieldsFirstName ;

	@FindBy(xpath = ".//*[@value='lastName']")
	public WebElement selectFieldsLastName ;

	@FindBy(xpath = ".//*[@name='fsleadCall']")
	public WebElement selectFieldsCall ;

	@FindBy(xpath = ".//*[@value='View']")
	public WebElement selectFieldsVeiw ;

	@FindBy(xpath = ".//*[@name='FS_LEAD_REMARKS19_remarks']")
	public WebElement remark ;

	@FindBy(xpath = ".//*[@id='ms-parentFS_LEAD_DETAILS1_leadOwnerID']")
	public WebElement leadOwner ;

	@FindBy(xpath = ".//*[@id='ms-parentFS_LEAD_DETAILS1_leadOwnerID']/div/div/input")
	public WebElement leadOwnerInput ;

	public FsExportPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
