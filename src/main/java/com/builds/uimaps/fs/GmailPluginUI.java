package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class GmailPluginUI {
	
	public GmailPluginUI(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(id="identifierId")
	public WebElement userName_textBox;
	
	@FindBy(xpath=".//span[contains(text(),'Next')]")
	public WebElement nextBtn;
	
	@FindBy(xpath=".//input[@type='password' and @name='password']")
	public WebElement password_textBox;
	
	// Gmail Plugin 
	
	@FindBy(xpath=".//span[contains(text(),'CONTINUE')]")
	public WebElement continueBtn;
	
	@FindBy(xpath=".//input[@name='loginId' and @type='text']")
	public WebElement pluginLoginID_textBox;
	
	@FindBy(xpath=".//input[@name='password' and @type='password']")
	public WebElement pluginPassword_textBox;
	
	@FindBy(xpath=".//input[@name='companyCode' and @type='string']")
	public WebElement companyCode_textBox;
	
	@FindBy(xpath=".//span[contains(text(),'LOGIN')]")
	public WebElement plugin_loginBtn;
	
	@FindBy(xpath=".//button[@class='short-button md-primary md-raised md-button md-ink-ripple']/span[contains(text(),'New')]")
	public WebElement newBtn;
	
	@FindBy(xpath=".//span[contains(text(),'Sales Lead')]")
	public WebElement salesLead;
	
	@FindBy(xpath=".//span/div[@class='md-text ng-binding' and contains(text(), 'Select Source Category')]")
	public WebElement sourceCategory_dropdown;
	
	@FindBy(xpath=".//div[contains(text(), 'Select Source Detail')]")
	public WebElement sourceDetail_dropdown;
	
	@FindBy(xpath=".//input[@name='firstName' and @type='text']")
	public WebElement firstName;
	
	@FindBy(xpath=".//input[@name='lastName' and @type='text']")
	public WebElement lastName;
	
	@FindBy(id="input_23")
	public WebElement emailID;
	
	@FindBy(id="input_37")
	public WebElement addressLine1;
	
	@FindBy(id="input_39")
	public WebElement city;
	
	@FindBy(xpath=".//span/div[contains(text(),'Select Country')]")
	public WebElement country;
	
	@FindBy(xpath=".//span/div[contains(text(),'Select State')]")
	public WebElement state;
	
	@FindBy(id="input_30")
	public WebElement zipCode;
	
	// Plugin Button 
	
	@FindBy(xpath=".//span[contains(text(),'Save')]")
	public WebElement saveBtn_plugin;
	
	

}
