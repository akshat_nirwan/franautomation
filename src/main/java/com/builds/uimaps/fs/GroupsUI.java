package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.builds.uimaps.common.CommonUI;

public class GroupsUI extends CommonUI {

	@FindBy(id = "createGroup")
	public WebElement createGroupBtn;

	@FindBy(id = "groupName")
	public WebElement groupName;

	@FindBy(id = "groupDescription")
	public WebElement groupDescription;

	@FindBy(id = "groupType")
	public WebElement visibility;

	@FindBy(xpath = ".//label[@for='groupChoiceTemp']")
	public WebElement smartGroup;

	@FindBy(id = "groupChoiceTemp")
	public WebElement groupChoice_Input_ById;

	@FindBy(xpath = ".//*[@id='addGroupForm1']//button[2]")
	public WebElement Create_Button_ByXPATH;

	@FindBy(id = "fc-drop-parentgroupType")
	public WebElement dropdown_Div_ById;

	@FindBy(xpath = ".//*[@id='fc-drop-parentgroupType']//input[@class='searchInputMultiple']")
	public WebElement textField_dropdown_Div_ById;

	@FindBy(linkText = "Archived Groups")
	public WebElement archivedGroupsTab;

	@FindBy(xpath = ".//ul[@id='FilterTabShowHide']//li[.='Groups']")
	public WebElement groupsTab;

	@FindBy(xpath = ".//div[@class='dropdown-action']/div[@class='list-name']")
	public WebElement clickViewPerPage;

	public String clickGroup_ico_ThreeDots(String groupName) {
		return ".//a[.='" + groupName + "']//ancestor::tr[1]//div[@data-role='ico_ThreeDots']";
	}

	public String selectMenuFromRightActionDiv(String action) {
		return ".//*[@class='dropdown-list direction open-menu']//a[contains(text(),'" + action + "')]";
	}

	public String selectMenuFromTopActionDiv(String action) {
		return ".//ul[@class='dropdown-list direction open-menu']//a[contains(text(),'" + action + "')]";
	}

	public String checkboxAgainstGroup(String groupName) {
		return ".//*[.='" + groupName + "']//ancestor::tr[1]//label";
	}

	public String getXPathOfLeadCount(String groupName, String position) {
		return ".//a[.='" + groupName + "']//ancestor::tr[1]//td[" + position + "]/a";
	}

	public String getRowXPathOfGroup() {
		return ".//*[@qat_maintable='withheader']//tr";
	}

	public String getXPathOfViewPerPageCount(String count) {
		return ".//ul[@class='dropdown-list direction open-menu']//a[.=" + count + "]";
	}

	@FindBy(xpath = ".//div[@class='dropdown-action no-arrow three-dot-nav']//div[@data-role='ico_ThreeDots']")
	public WebElement top_Action_Batch_ByXpath;

	@FindBy(className = "fc-drop-choice")
	public WebElement AvailableFields_Button_ByClass;

	@FindBy(className = "searchInputMultiple")
	public WebElement searchAvailableFields_Input_ByClass;

	public String clickFieldNameInFilter(String SubModuleName, String fieldName) {
		return ".//span[.='" + SubModuleName + "']//ancestor::ul[1]//span[.='" + fieldName + "']";
	}

	public String getFieldXpathInFilters(String concatString, String fieldType, String position) {
		return ".//div[.='" + concatString + "']//ancestor::div[1]//" + fieldType + "[" + position + "]";
	}

	public String getdeleteFieldXpath(String concatString) {
		return ".//div[.='" + concatString + "']//ancestor::div[1]//div[contains(@class,'svg-icon')]";
	}

	/*
	 * Filters for leads field
	 */

	@FindBy(id = "firstName")
	public WebElement firstName;

	@FindBy(id = "lastName")
	public WebElement lastName;

	@FindBy(id = "emailID")
	public WebElement emailID;

	public GroupsUI(WebDriver driver) {
		super(driver);
	}

}
