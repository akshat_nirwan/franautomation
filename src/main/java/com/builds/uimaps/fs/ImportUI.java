package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ImportUI {
	
	public ImportUI(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath=".//input[@type='radio' and @value='lead']")
	public WebElement leads_RadioBtn;
	
	@FindBy(xpath=".//input[@type='radio' and @value='broker']")
	public WebElement broker_RadioBtn;
	
	@FindBy(xpath=".//input[@type='radio' and @value='csv']")
	public WebElement csv_RadioBtn;
	
	@FindBy(xpath=".//input[@type='radio' and @value='tab']")
	public WebElement tab_RadioBtn;
	
	@FindBy(xpath=".//input[@type='file' and @name='salesFileName']")
	public WebElement chooseFile;
	
	@FindBy(xpath=".//input[@type='button' and @value='Continue']")
	public WebElement continueBtn;
	
	// Map Columns to System Fields
	
	@FindBy(xpath=".//input[@class='fTextBoxDate' and @id='SYSTEM_DATA__DEFAULT_REQUEST_DATE']")
	public WebElement inquiryDate;
	
	@FindBy(xpath=".//input[@type='radio' and @id='assignment']")
	public WebElement leadOwner_basedOnAssignmentRules_RadioBtn;
	
	@FindBy(xpath=".//input[@type='radio' and @id='default']")
	public WebElement leadOwner_select_RadioBtn;
	
	@FindBy(xpath=".//select[@class='multiList' and @id='SYSTEM_DATA__DEFAULT_LEAD_OWNER_ID']")
	public WebElement leadOwner_select_DropDown;
	
	@FindBy(xpath=".//select[@class='multiList' and @id='SYSTEM_DATA__DEFAULT_LEAD_STATUS_ID']")
	public WebElement leadStatus_DropDown;
	
	@FindBy(xpath=".//select[@class='multiList' and @id='SYSTEM_DATA__DEFAULT_LEAD_SOURCE2_ID']")
	public WebElement leadSourceCategory_DropDown;
	
	@FindBy(xpath=".//select[@class='multiList' and @id='SYSTEM_DATA__DEFAULT_LEAD_SOURCE3_ID']")
	public WebElement leadSourceDetails_DropDown;
	
	@FindBy(xpath=".//select[@class='multiList' and @id='SYSTEM_DATA__UNSUBSCRIBE']")
	public WebElement emailSubscriptionStatus_DropDown;
	
	@FindBy(xpath=".//input[@type='checkbox' and @id='mail']")
	public WebElement disclaimer_checkbox;
	
	
}
