package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Item23ReceiptSummaryUI {

	@FindBy(xpath = ".//input[@value='Add ITEM 23 - RECEIPT']")
	public WebElement addItem23ReceiptBtn;

	@FindBy(name = "templateTitle")
	public WebElement title;

	@FindBy(id = "newTemplateText")
	public WebElement item23Receipt;

	@FindBy(name = "businessName")
	public WebElement businessName;

	@FindBy(id = "address")
	public WebElement address;

	@FindBy(id = "country")
	public WebElement country;

	@FindBy(id = "state")
	public WebElement state;

	@FindBy(name = "phone")
	public WebElement phone;

	@FindBy(linkText = "Keywords for replacement")
	public WebElement keywordsForReplacement;

	@FindBy(xpath = ".//input[@value='Preview']")
	public WebElement previewBtn;

	@FindBy(xpath = ".//input[@value='Save']")
	public WebElement saveBtn;

	public Item23ReceiptSummaryUI(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
