package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LaunchCampaignUI {

	@FindBy(xpath = ".//button[contains(text(),'Launch Campaign')]")
	public WebElement launchCampaign;

	public LaunchCampaignUI(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
