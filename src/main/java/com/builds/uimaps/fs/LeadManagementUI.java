package com.builds.uimaps.fs;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.builds.uimaps.common.CommonUI;

public class LeadManagementUI extends CommonUI {

	@FindBy(name = "changeStatusButton1")
	public WebElement changeStatus;

	@FindBy(name = "assignLead2")
	public WebElement changeOwner;
	
	@FindBy(id="resultsPerPage")
	public WebElement viewPerPage_dropDown;

	@FindBy(name = "sendMail2")
	public WebElement sendMail;
	
	@FindBy(id="searchString")
	public WebElement leadSearch_TextBox; 

	@FindBy(name = "addToGroup")
	public WebElement addToGroup;
	
	@FindBy(id="showFilter")
	public WebElement showFilters_linkTextBtn;
	
	@FindBy(id="ms-parentmyLeads")
	public WebElement leadOwners_filter_dropDown_divID;
	
	@FindBy(name = "associtaCampaign1")
	public WebElement associateCampaign;
	
	@FindBy(id="saveView")
	public WebElement saveView_Btn;

	@FindBy(name = "mailMerge")
	public WebElement mailMerge;

	@FindBy(name = "mergeLeads")
	public WebElement mergeLeads;

	@FindBy(name = "archive")
	public WebElement archiveLeads;

	@FindBy(name = "deleteButton1")
	public WebElement delete;

	@FindBy(name = "Print")
	public WebElement Print;

	@FindBy(xpath = ".//a[contains(@href,'addLeadPrimaryInfoForm?addAnotherLead')]")
	public WebElement addLead;

	@FindBy(id = "showFilter")
	public WebElement showFilter;

	@FindBy(xpath = ".//input[@type='Submit' and @value='Add To Group']")
	public WebElement addToGroup_Button;

	@FindBy(xpath = ".//*[@qat_maintable='withheader']//input[@name='checkBox']")
	public WebElement checkAll;

	public String getXpathOfTheLeadCheckBox(String leadName) {
		return ".//a[.='" + leadName + "']//ancestor::tr[@qat_maintable='withoutheader']//input[1]";
	}

	public String getXpathOfTheLeads_RigthActionIcon(String leadName) {
		return ".//a[.='" + leadName + "']//ancestor::tr[@qat_maintable='withoutheader']//div[@id='menuBar']/layer/a";
	}

	@FindBy(xpath = ".//input[@value='Remove from Group']")
	public WebElement removeFromGroup;

	@FindBy(xpath = ".//input[@class='cm_new_button_action showAction' and @value='Actions']")
	public WebElement ActionsMenu_Left_Input_ByValue;

	@FindBy(xpath = ".//div[@id='actionListButtons']//tr[@class='ActionMenuFontColor']/td")
	public List<WebElement> actionsIn_LeftActionMenu_ByXpath;

	@FindBy(xpath = ".//div[contains(@id,'Actions_')]/span[contains(@id,'Action_')]")
	public List<WebElement> actionsInRigthActionMenu_ByXpath;

	public String getXpathOfTheLead(String leadName) {
		
		// return ".//*[contains(text(),'"+ leadName +"')]/parent::*[contains(@onmouseover,'showLeadDetails')]";
		return ".//*[contains(text(),'"+ leadName +"')]/parent::*[contains(@onmouseover,'showLeadDetails')] | .//*[contains(text(),'"+ leadName +"')][contains(@onmouseover,'showLeadDetails')]";
		//return ".//*[contains(text(),'" + leadName + "') and (contains(@onmouseover,'showLeadDetails'))]";
		// return ".//a/span[contains(text(),'" + leadName + "')]";
	}

	public LeadManagementUI(WebDriver driver) {
		super(driver);
	}

	public String getXpathOfCheckBoxOfGroup(String groupName) {
		return ".//td[contains(text(),'" + groupName + "')]//ancestor::tr[1]//input[@name='groups']";
	}

}
