package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LeadSourceCategoryUI {
	
	@FindBy(xpath=".//input[@type='button' and @value='Add Lead Source Category']")
	public WebElement addLeadSourceCategory_Btn;
	
	
	// Add Lead Source Category - iFrame
	@FindBy(xpath=".//input[@type='text' and @name='leadSource2Name']")
	public WebElement enterLeadSourceCategory_Text;
	
	@FindBy(xpath=".//input[@type='checkbox' and @class='textbox']")
	public WebElement includeInWebPage_checkBox;
	
	@FindBy(xpath=".//input[@type='button' and @value='Add']")
	public WebElement addBtn;
	
	@FindBy(xpath=".//input[@type='button' and @value='Close']")
	public WebElement close_Btn;
	
	
	public LeadSourceCategoryUI(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
