package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LeadStatusReportUI {
	
	@FindBy(id="myLeads")
	public WebElement viewLeadsBelongingTo_selectTag;
	
	@FindBy(id="ms-parentmyLeads")
	public WebElement viewLeadsBelongingTo_divID;
	
	@FindBy(id="viewReportButton")
	public WebElement viewReportBtn;
	
	
	public LeadStatusReportUI(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
